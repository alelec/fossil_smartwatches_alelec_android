package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.hz3;
import java.lang.ref.WeakReference;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rx3 extends c04 implements bm0, Drawable.Callback, hz3.b {
    @DexIgnore
    public static /* final */ int[] L0; // = {16842910};
    @DexIgnore
    public static /* final */ ShapeDrawable M0; // = new ShapeDrawable(new OvalShape());
    @DexIgnore
    public PorterDuffColorFilter A0;
    @DexIgnore
    public ColorStateList B0;
    @DexIgnore
    public ColorStateList C;
    @DexIgnore
    public PorterDuff.Mode C0; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public ColorStateList D;
    @DexIgnore
    public int[] D0;
    @DexIgnore
    public float E;
    @DexIgnore
    public boolean E0;
    @DexIgnore
    public float F;
    @DexIgnore
    public ColorStateList F0;
    @DexIgnore
    public ColorStateList G;
    @DexIgnore
    public WeakReference<a> G0; // = new WeakReference<>(null);
    @DexIgnore
    public float H;
    @DexIgnore
    public TextUtils.TruncateAt H0;
    @DexIgnore
    public ColorStateList I;
    @DexIgnore
    public boolean I0;
    @DexIgnore
    public CharSequence J;
    @DexIgnore
    public int J0;
    @DexIgnore
    public boolean K;
    @DexIgnore
    public boolean K0;
    @DexIgnore
    public Drawable L;
    @DexIgnore
    public ColorStateList M;
    @DexIgnore
    public float N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public boolean P;
    @DexIgnore
    public Drawable Q;
    @DexIgnore
    public Drawable R;
    @DexIgnore
    public ColorStateList S;
    @DexIgnore
    public float T;
    @DexIgnore
    public CharSequence U;
    @DexIgnore
    public boolean V;
    @DexIgnore
    public boolean W;
    @DexIgnore
    public Drawable X;
    @DexIgnore
    public bx3 Y;
    @DexIgnore
    public bx3 Z;
    @DexIgnore
    public float a0;
    @DexIgnore
    public float b0;
    @DexIgnore
    public float c0;
    @DexIgnore
    public float d0;
    @DexIgnore
    public float e0;
    @DexIgnore
    public float f0;
    @DexIgnore
    public float g0;
    @DexIgnore
    public float h0;
    @DexIgnore
    public /* final */ Context i0;
    @DexIgnore
    public /* final */ Paint j0; // = new Paint(1);
    @DexIgnore
    public /* final */ Paint k0;
    @DexIgnore
    public /* final */ Paint.FontMetrics l0; // = new Paint.FontMetrics();
    @DexIgnore
    public /* final */ RectF m0; // = new RectF();
    @DexIgnore
    public /* final */ PointF n0; // = new PointF();
    @DexIgnore
    public /* final */ Path o0; // = new Path();
    @DexIgnore
    public /* final */ hz3 p0;
    @DexIgnore
    public int q0;
    @DexIgnore
    public int r0;
    @DexIgnore
    public int s0;
    @DexIgnore
    public int t0;
    @DexIgnore
    public int u0;
    @DexIgnore
    public int v0;
    @DexIgnore
    public boolean w0;
    @DexIgnore
    public int x0;
    @DexIgnore
    public int y0; // = 255;
    @DexIgnore
    public ColorFilter z0;

    @DexIgnore
    public interface a {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore
    public rx3(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        M(context);
        this.i0 = context;
        hz3 hz3 = new hz3(this);
        this.p0 = hz3;
        this.J = "";
        hz3.e().density = context.getResources().getDisplayMetrics().density;
        this.k0 = null;
        setState(L0);
        g2(L0);
        this.I0 = true;
        if (tz3.f3498a) {
            M0.setTint(-1);
        }
    }

    @DexIgnore
    public static boolean k1(int[] iArr, int i) {
        if (iArr == null) {
            return false;
        }
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static boolean o1(ColorStateList colorStateList) {
        return colorStateList != null && colorStateList.isStateful();
    }

    @DexIgnore
    public static boolean p1(Drawable drawable) {
        return drawable != null && drawable.isStateful();
    }

    @DexIgnore
    public static boolean q1(pz3 pz3) {
        ColorStateList colorStateList;
        return (pz3 == null || (colorStateList = pz3.b) == null || !colorStateList.isStateful()) ? false : true;
    }

    @DexIgnore
    public static rx3 v0(Context context, AttributeSet attributeSet, int i, int i2) {
        rx3 rx3 = new rx3(context, attributeSet, i, i2);
        rx3.r1(attributeSet, i, i2);
        return rx3;
    }

    @DexIgnore
    public final void A0(Canvas canvas, Rect rect) {
        if (!this.K0) {
            this.j0.setColor(this.q0);
            this.j0.setStyle(Paint.Style.FILL);
            this.m0.set(rect);
            canvas.drawRoundRect(this.m0, H0(), H0(), this.j0);
        }
    }

    @DexIgnore
    public void A1(ColorStateList colorStateList) {
        if (this.D != colorStateList) {
            this.D = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void A2(int i) {
        z2(new pz3(this.i0, i));
    }

    @DexIgnore
    public final void B0(Canvas canvas, Rect rect) {
        if (J2()) {
            o0(rect, this.m0);
            RectF rectF = this.m0;
            float f = rectF.left;
            float f2 = rectF.top;
            canvas.translate(f, f2);
            this.Q.setBounds(0, 0, (int) this.m0.width(), (int) this.m0.height());
            if (tz3.f3498a) {
                this.R.setBounds(this.Q.getBounds());
                this.R.jumpToCurrentState();
                this.R.draw(canvas);
            } else {
                this.Q.draw(canvas);
            }
            canvas.translate(-f, -f2);
        }
    }

    @DexIgnore
    public void B1(int i) {
        A1(gf0.c(this.i0, i));
    }

    @DexIgnore
    public void B2(float f) {
        if (this.e0 != f) {
            this.e0 = f;
            invalidateSelf();
            s1();
        }
    }

    @DexIgnore
    public final void C0(Canvas canvas, Rect rect) {
        this.j0.setColor(this.u0);
        this.j0.setStyle(Paint.Style.FILL);
        this.m0.set(rect);
        if (!this.K0) {
            canvas.drawRoundRect(this.m0, H0(), H0(), this.j0);
            return;
        }
        g(new RectF(rect), this.o0);
        super.o(canvas, this.j0, this.o0, t());
    }

    @DexIgnore
    @Deprecated
    public void C1(float f) {
        if (this.F != f) {
            this.F = f;
            setShapeAppearanceModel(C().w(f));
        }
    }

    @DexIgnore
    public void C2(int i) {
        B2(this.i0.getResources().getDimension(i));
    }

    @DexIgnore
    public final void D0(Canvas canvas, Rect rect) {
        Paint paint = this.k0;
        if (paint != null) {
            paint.setColor(pl0.h(-16777216, 127));
            canvas.drawRect(rect, this.k0);
            if (I2() || H2()) {
                l0(rect, this.m0);
                canvas.drawRect(this.m0, this.k0);
            }
            if (this.J != null) {
                canvas.drawLine((float) rect.left, rect.exactCenterY(), (float) rect.right, rect.exactCenterY(), this.k0);
            }
            if (J2()) {
                o0(rect, this.m0);
                canvas.drawRect(this.m0, this.k0);
            }
            this.k0.setColor(pl0.h(-65536, 127));
            n0(rect, this.m0);
            canvas.drawRect(this.m0, this.k0);
            this.k0.setColor(pl0.h(-16711936, 127));
            p0(rect, this.m0);
            canvas.drawRect(this.m0, this.k0);
        }
    }

    @DexIgnore
    @Deprecated
    public void D1(int i) {
        C1(this.i0.getResources().getDimension(i));
    }

    @DexIgnore
    public void D2(float f) {
        if (this.d0 != f) {
            this.d0 = f;
            invalidateSelf();
            s1();
        }
    }

    @DexIgnore
    public final void E0(Canvas canvas, Rect rect) {
        int i;
        if (this.J != null) {
            Paint.Align t02 = t0(rect, this.n0);
            r0(rect, this.m0);
            if (this.p0.d() != null) {
                this.p0.e().drawableState = getState();
                this.p0.j(this.i0);
            }
            this.p0.e().setTextAlign(t02);
            boolean z = Math.round(this.p0.f(e1().toString())) > Math.round(this.m0.width());
            if (z) {
                int save = canvas.save();
                canvas.clipRect(this.m0);
                i = save;
            } else {
                i = 0;
            }
            CharSequence charSequence = this.J;
            if (z && this.H0 != null) {
                charSequence = TextUtils.ellipsize(charSequence, this.p0.e(), this.m0.width(), this.H0);
            }
            int length = charSequence.length();
            PointF pointF = this.n0;
            canvas.drawText(charSequence, 0, length, pointF.x, pointF.y, this.p0.e());
            if (z) {
                canvas.restoreToCount(i);
            }
        }
    }

    @DexIgnore
    public void E1(float f) {
        if (this.h0 != f) {
            this.h0 = f;
            invalidateSelf();
            s1();
        }
    }

    @DexIgnore
    public void E2(int i) {
        D2(this.i0.getResources().getDimension(i));
    }

    @DexIgnore
    public Drawable F0() {
        return this.X;
    }

    @DexIgnore
    public void F1(int i) {
        E1(this.i0.getResources().getDimension(i));
    }

    @DexIgnore
    public void F2(boolean z) {
        if (this.E0 != z) {
            this.E0 = z;
            L2();
            onStateChange(getState());
        }
    }

    @DexIgnore
    public ColorStateList G0() {
        return this.D;
    }

    @DexIgnore
    public void G1(Drawable drawable) {
        Drawable J02 = J0();
        if (J02 != drawable) {
            float m02 = m0();
            this.L = drawable != null ? am0.r(drawable).mutate() : null;
            float m03 = m0();
            K2(J02);
            if (I2()) {
                k0(this.L);
            }
            invalidateSelf();
            if (m02 != m03) {
                s1();
            }
        }
    }

    @DexIgnore
    public boolean G2() {
        return this.I0;
    }

    @DexIgnore
    public float H0() {
        return this.K0 ? F() : this.F;
    }

    @DexIgnore
    public void H1(int i) {
        G1(gf0.d(this.i0, i));
    }

    @DexIgnore
    public final boolean H2() {
        return this.W && this.X != null && this.w0;
    }

    @DexIgnore
    public float I0() {
        return this.h0;
    }

    @DexIgnore
    public void I1(float f) {
        if (this.N != f) {
            float m02 = m0();
            this.N = f;
            float m03 = m0();
            invalidateSelf();
            if (m02 != m03) {
                s1();
            }
        }
    }

    @DexIgnore
    public final boolean I2() {
        return this.K && this.L != null;
    }

    @DexIgnore
    public Drawable J0() {
        Drawable drawable = this.L;
        if (drawable != null) {
            return am0.q(drawable);
        }
        return null;
    }

    @DexIgnore
    public void J1(int i) {
        I1(this.i0.getResources().getDimension(i));
    }

    @DexIgnore
    public final boolean J2() {
        return this.P && this.Q != null;
    }

    @DexIgnore
    public float K0() {
        return this.N;
    }

    @DexIgnore
    public void K1(ColorStateList colorStateList) {
        this.O = true;
        if (this.M != colorStateList) {
            this.M = colorStateList;
            if (I2()) {
                am0.o(this.L, colorStateList);
            }
            onStateChange(getState());
        }
    }

    @DexIgnore
    public final void K2(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback(null);
        }
    }

    @DexIgnore
    public ColorStateList L0() {
        return this.M;
    }

    @DexIgnore
    public void L1(int i) {
        K1(gf0.c(this.i0, i));
    }

    @DexIgnore
    public final void L2() {
        this.F0 = this.E0 ? tz3.d(this.I) : null;
    }

    @DexIgnore
    public float M0() {
        return this.E;
    }

    @DexIgnore
    public void M1(int i) {
        N1(this.i0.getResources().getBoolean(i));
    }

    @DexIgnore
    @TargetApi(21)
    public final void M2() {
        this.R = new RippleDrawable(tz3.d(c1()), this.Q, M0);
    }

    @DexIgnore
    public float N0() {
        return this.a0;
    }

    @DexIgnore
    public void N1(boolean z) {
        if (this.K != z) {
            boolean I2 = I2();
            this.K = z;
            boolean I22 = I2();
            if (I2 != I22) {
                if (I22) {
                    k0(this.L);
                } else {
                    K2(this.L);
                }
                invalidateSelf();
                s1();
            }
        }
    }

    @DexIgnore
    public ColorStateList O0() {
        return this.G;
    }

    @DexIgnore
    public void O1(float f) {
        if (this.E != f) {
            this.E = f;
            invalidateSelf();
            s1();
        }
    }

    @DexIgnore
    public float P0() {
        return this.H;
    }

    @DexIgnore
    public void P1(int i) {
        O1(this.i0.getResources().getDimension(i));
    }

    @DexIgnore
    public Drawable Q0() {
        Drawable drawable = this.Q;
        if (drawable != null) {
            return am0.q(drawable);
        }
        return null;
    }

    @DexIgnore
    public void Q1(float f) {
        if (this.a0 != f) {
            this.a0 = f;
            invalidateSelf();
            s1();
        }
    }

    @DexIgnore
    public CharSequence R0() {
        return this.U;
    }

    @DexIgnore
    public void R1(int i) {
        Q1(this.i0.getResources().getDimension(i));
    }

    @DexIgnore
    public float S0() {
        return this.g0;
    }

    @DexIgnore
    public void S1(ColorStateList colorStateList) {
        if (this.G != colorStateList) {
            this.G = colorStateList;
            if (this.K0) {
                f0(colorStateList);
            }
            onStateChange(getState());
        }
    }

    @DexIgnore
    public float T0() {
        return this.T;
    }

    @DexIgnore
    public void T1(int i) {
        S1(gf0.c(this.i0, i));
    }

    @DexIgnore
    public float U0() {
        return this.f0;
    }

    @DexIgnore
    public void U1(float f) {
        if (this.H != f) {
            this.H = f;
            this.j0.setStrokeWidth(f);
            if (this.K0) {
                super.g0(f);
            }
            invalidateSelf();
        }
    }

    @DexIgnore
    public int[] V0() {
        return this.D0;
    }

    @DexIgnore
    public void V1(int i) {
        U1(this.i0.getResources().getDimension(i));
    }

    @DexIgnore
    public ColorStateList W0() {
        return this.S;
    }

    @DexIgnore
    public final void W1(ColorStateList colorStateList) {
        if (this.C != colorStateList) {
            this.C = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void X0(RectF rectF) {
        p0(getBounds(), rectF);
    }

    @DexIgnore
    public void X1(Drawable drawable) {
        Drawable Q0 = Q0();
        if (Q0 != drawable) {
            float q02 = q0();
            this.Q = drawable != null ? am0.r(drawable).mutate() : null;
            if (tz3.f3498a) {
                M2();
            }
            float q03 = q0();
            K2(Q0);
            if (J2()) {
                k0(this.Q);
            }
            invalidateSelf();
            if (q02 != q03) {
                s1();
            }
        }
    }

    @DexIgnore
    public TextUtils.TruncateAt Y0() {
        return this.H0;
    }

    @DexIgnore
    public void Y1(CharSequence charSequence) {
        if (this.U != charSequence) {
            this.U = bn0.c().h(charSequence);
            invalidateSelf();
        }
    }

    @DexIgnore
    public bx3 Z0() {
        return this.Z;
    }

    @DexIgnore
    public void Z1(float f) {
        if (this.g0 != f) {
            this.g0 = f;
            invalidateSelf();
            if (J2()) {
                s1();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.hz3.b
    public void a() {
        s1();
        invalidateSelf();
    }

    @DexIgnore
    public float a1() {
        return this.c0;
    }

    @DexIgnore
    public void a2(int i) {
        Z1(this.i0.getResources().getDimension(i));
    }

    @DexIgnore
    public float b1() {
        return this.b0;
    }

    @DexIgnore
    public void b2(int i) {
        X1(gf0.d(this.i0, i));
    }

    @DexIgnore
    public ColorStateList c1() {
        return this.I;
    }

    @DexIgnore
    public void c2(float f) {
        if (this.T != f) {
            this.T = f;
            invalidateSelf();
            if (J2()) {
                s1();
            }
        }
    }

    @DexIgnore
    public bx3 d1() {
        return this.Y;
    }

    @DexIgnore
    public void d2(int i) {
        c2(this.i0.getResources().getDimension(i));
    }

    @DexIgnore
    @Override // com.fossil.c04
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        if (!bounds.isEmpty() && getAlpha() != 0) {
            int i = 0;
            int i2 = this.y0;
            if (i2 < 255) {
                i = px3.a(canvas, (float) bounds.left, (float) bounds.top, (float) bounds.right, (float) bounds.bottom, i2);
            }
            A0(canvas, bounds);
            x0(canvas, bounds);
            if (this.K0) {
                super.draw(canvas);
            }
            z0(canvas, bounds);
            C0(canvas, bounds);
            y0(canvas, bounds);
            w0(canvas, bounds);
            if (this.I0) {
                E0(canvas, bounds);
            }
            B0(canvas, bounds);
            D0(canvas, bounds);
            if (this.y0 < 255) {
                canvas.restoreToCount(i);
            }
        }
    }

    @DexIgnore
    public CharSequence e1() {
        return this.J;
    }

    @DexIgnore
    public void e2(float f) {
        if (this.f0 != f) {
            this.f0 = f;
            invalidateSelf();
            if (J2()) {
                s1();
            }
        }
    }

    @DexIgnore
    public pz3 f1() {
        return this.p0.d();
    }

    @DexIgnore
    public void f2(int i) {
        e2(this.i0.getResources().getDimension(i));
    }

    @DexIgnore
    public float g1() {
        return this.e0;
    }

    @DexIgnore
    public boolean g2(int[] iArr) {
        if (!Arrays.equals(this.D0, iArr)) {
            this.D0 = iArr;
            if (J2()) {
                return t1(getState(), iArr);
            }
        }
        return false;
    }

    @DexIgnore
    public int getAlpha() {
        return this.y0;
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        return this.z0;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return (int) this.E;
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return Math.min(Math.round(this.a0 + m0() + this.d0 + this.p0.f(e1().toString()) + this.e0 + q0() + this.h0), this.J0);
    }

    @DexIgnore
    @Override // com.fossil.c04
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    @Override // com.fossil.c04
    @TargetApi(21)
    public void getOutline(Outline outline) {
        if (this.K0) {
            super.getOutline(outline);
            return;
        }
        Rect bounds = getBounds();
        if (!bounds.isEmpty()) {
            outline.setRoundRect(bounds, this.F);
        } else {
            outline.setRoundRect(0, 0, getIntrinsicWidth(), getIntrinsicHeight(), this.F);
        }
        outline.setAlpha(((float) getAlpha()) / 255.0f);
    }

    @DexIgnore
    public float h1() {
        return this.d0;
    }

    @DexIgnore
    public void h2(ColorStateList colorStateList) {
        if (this.S != colorStateList) {
            this.S = colorStateList;
            if (J2()) {
                am0.o(this.Q, colorStateList);
            }
            onStateChange(getState());
        }
    }

    @DexIgnore
    public final ColorFilter i1() {
        ColorFilter colorFilter = this.z0;
        return colorFilter != null ? colorFilter : this.A0;
    }

    @DexIgnore
    public void i2(int i) {
        h2(gf0.c(this.i0, i));
    }

    @DexIgnore
    public void invalidateDrawable(Drawable drawable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    @DexIgnore
    @Override // com.fossil.c04
    public boolean isStateful() {
        return o1(this.C) || o1(this.D) || o1(this.G) || (this.E0 && o1(this.F0)) || q1(this.p0.d()) || u0() || p1(this.L) || p1(this.X) || o1(this.B0);
    }

    @DexIgnore
    public boolean j1() {
        return this.E0;
    }

    @DexIgnore
    public void j2(boolean z) {
        if (this.P != z) {
            boolean J2 = J2();
            this.P = z;
            boolean J22 = J2();
            if (J2 != J22) {
                if (J22) {
                    k0(this.Q);
                } else {
                    K2(this.Q);
                }
                invalidateSelf();
                s1();
            }
        }
    }

    @DexIgnore
    public final void k0(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback(this);
            am0.m(drawable, am0.f(this));
            drawable.setLevel(getLevel());
            drawable.setVisible(isVisible(), false);
            if (drawable == this.Q) {
                if (drawable.isStateful()) {
                    drawable.setState(V0());
                }
                am0.o(drawable, this.S);
                return;
            }
            if (drawable.isStateful()) {
                drawable.setState(getState());
            }
            Drawable drawable2 = this.L;
            if (drawable == drawable2 && this.O) {
                am0.o(drawable2, this.M);
            }
        }
    }

    @DexIgnore
    public void k2(a aVar) {
        this.G0 = new WeakReference<>(aVar);
    }

    @DexIgnore
    public final void l0(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (I2() || H2()) {
            float f = this.a0 + this.b0;
            if (am0.f(this) == 0) {
                float f2 = f + ((float) rect.left);
                rectF.left = f2;
                rectF.right = f2 + this.N;
            } else {
                float f3 = ((float) rect.right) - f;
                rectF.right = f3;
                rectF.left = f3 - this.N;
            }
            float exactCenterY = rect.exactCenterY();
            float f4 = this.N;
            float f5 = exactCenterY - (f4 / 2.0f);
            rectF.top = f5;
            rectF.bottom = f5 + f4;
        }
    }

    @DexIgnore
    public boolean l1() {
        return this.V;
    }

    @DexIgnore
    public void l2(TextUtils.TruncateAt truncateAt) {
        this.H0 = truncateAt;
    }

    @DexIgnore
    public float m0() {
        return (I2() || H2()) ? this.b0 + this.N + this.c0 : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public boolean m1() {
        return p1(this.Q);
    }

    @DexIgnore
    public void m2(bx3 bx3) {
        this.Z = bx3;
    }

    @DexIgnore
    public final void n0(Rect rect, RectF rectF) {
        rectF.set(rect);
        if (J2()) {
            float f = this.h0 + this.g0 + this.T + this.f0 + this.e0;
            if (am0.f(this) == 0) {
                rectF.right = ((float) rect.right) - f;
            } else {
                rectF.left = f + ((float) rect.left);
            }
        }
    }

    @DexIgnore
    public boolean n1() {
        return this.P;
    }

    @DexIgnore
    public void n2(int i) {
        m2(bx3.d(this.i0, i));
    }

    @DexIgnore
    public final void o0(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (J2()) {
            float f = this.h0 + this.g0;
            if (am0.f(this) == 0) {
                float f2 = ((float) rect.right) - f;
                rectF.right = f2;
                rectF.left = f2 - this.T;
            } else {
                float f3 = f + ((float) rect.left);
                rectF.left = f3;
                rectF.right = f3 + this.T;
            }
            float exactCenterY = rect.exactCenterY();
            float f4 = this.T;
            float f5 = exactCenterY - (f4 / 2.0f);
            rectF.top = f5;
            rectF.bottom = f5 + f4;
        }
    }

    @DexIgnore
    public void o2(float f) {
        if (this.c0 != f) {
            float m02 = m0();
            this.c0 = f;
            float m03 = m0();
            invalidateSelf();
            if (m02 != m03) {
                s1();
            }
        }
    }

    @DexIgnore
    public boolean onLayoutDirectionChanged(int i) {
        boolean onLayoutDirectionChanged = super.onLayoutDirectionChanged(i);
        if (I2()) {
            onLayoutDirectionChanged |= am0.m(this.L, i);
        }
        if (H2()) {
            onLayoutDirectionChanged |= am0.m(this.X, i);
        }
        if (J2()) {
            onLayoutDirectionChanged |= am0.m(this.Q, i);
        }
        if (!onLayoutDirectionChanged) {
            return true;
        }
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        boolean onLevelChange = super.onLevelChange(i);
        if (I2()) {
            onLevelChange |= this.L.setLevel(i);
        }
        if (H2()) {
            onLevelChange |= this.X.setLevel(i);
        }
        if (J2()) {
            onLevelChange |= this.Q.setLevel(i);
        }
        if (onLevelChange) {
            invalidateSelf();
        }
        return onLevelChange;
    }

    @DexIgnore
    @Override // com.fossil.c04, com.fossil.hz3.b
    public boolean onStateChange(int[] iArr) {
        if (this.K0) {
            super.onStateChange(iArr);
        }
        return t1(iArr, V0());
    }

    @DexIgnore
    public final void p0(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (J2()) {
            float f = this.h0 + this.g0 + this.T + this.f0 + this.e0;
            if (am0.f(this) == 0) {
                float f2 = (float) rect.right;
                rectF.right = f2;
                rectF.left = f2 - f;
            } else {
                int i = rect.left;
                rectF.left = (float) i;
                rectF.right = f + ((float) i);
            }
            rectF.top = (float) rect.top;
            rectF.bottom = (float) rect.bottom;
        }
    }

    @DexIgnore
    public void p2(int i) {
        o2(this.i0.getResources().getDimension(i));
    }

    @DexIgnore
    public float q0() {
        return J2() ? this.f0 + this.T + this.g0 : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public void q2(float f) {
        if (this.b0 != f) {
            float m02 = m0();
            this.b0 = f;
            float m03 = m0();
            invalidateSelf();
            if (m02 != m03) {
                s1();
            }
        }
    }

    @DexIgnore
    public final void r0(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (this.J != null) {
            float m02 = this.a0 + m0() + this.d0;
            float q02 = this.h0 + q0() + this.e0;
            if (am0.f(this) == 0) {
                rectF.left = m02 + ((float) rect.left);
                rectF.right = ((float) rect.right) - q02;
            } else {
                rectF.left = q02 + ((float) rect.left);
                rectF.right = ((float) rect.right) - m02;
            }
            rectF.top = (float) rect.top;
            rectF.bottom = (float) rect.bottom;
        }
    }

    @DexIgnore
    public final void r1(AttributeSet attributeSet, int i, int i2) {
        TypedArray k = jz3.k(this.i0, attributeSet, tw3.Chip, i, i2, new int[0]);
        this.K0 = k.hasValue(tw3.Chip_shapeAppearance);
        W1(oz3.a(this.i0, k, tw3.Chip_chipSurfaceColor));
        A1(oz3.a(this.i0, k, tw3.Chip_chipBackgroundColor));
        O1(k.getDimension(tw3.Chip_chipMinHeight, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        if (k.hasValue(tw3.Chip_chipCornerRadius)) {
            C1(k.getDimension(tw3.Chip_chipCornerRadius, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        }
        S1(oz3.a(this.i0, k, tw3.Chip_chipStrokeColor));
        U1(k.getDimension(tw3.Chip_chipStrokeWidth, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        t2(oz3.a(this.i0, k, tw3.Chip_rippleColor));
        y2(k.getText(tw3.Chip_android_text));
        z2(oz3.f(this.i0, k, tw3.Chip_android_textAppearance));
        int i3 = k.getInt(tw3.Chip_android_ellipsize, 0);
        if (i3 == 1) {
            l2(TextUtils.TruncateAt.START);
        } else if (i3 == 2) {
            l2(TextUtils.TruncateAt.MIDDLE);
        } else if (i3 == 3) {
            l2(TextUtils.TruncateAt.END);
        }
        N1(k.getBoolean(tw3.Chip_chipIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "chipIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "chipIconVisible") != null)) {
            N1(k.getBoolean(tw3.Chip_chipIconEnabled, false));
        }
        G1(oz3.d(this.i0, k, tw3.Chip_chipIcon));
        if (k.hasValue(tw3.Chip_chipIconTint)) {
            K1(oz3.a(this.i0, k, tw3.Chip_chipIconTint));
        }
        I1(k.getDimension(tw3.Chip_chipIconSize, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        j2(k.getBoolean(tw3.Chip_closeIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "closeIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "closeIconVisible") != null)) {
            j2(k.getBoolean(tw3.Chip_closeIconEnabled, false));
        }
        X1(oz3.d(this.i0, k, tw3.Chip_closeIcon));
        h2(oz3.a(this.i0, k, tw3.Chip_closeIconTint));
        c2(k.getDimension(tw3.Chip_closeIconSize, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        u1(k.getBoolean(tw3.Chip_android_checkable, false));
        z1(k.getBoolean(tw3.Chip_checkedIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "checkedIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "checkedIconVisible") != null)) {
            z1(k.getBoolean(tw3.Chip_checkedIconEnabled, false));
        }
        w1(oz3.d(this.i0, k, tw3.Chip_checkedIcon));
        w2(bx3.c(this.i0, k, tw3.Chip_showMotionSpec));
        m2(bx3.c(this.i0, k, tw3.Chip_hideMotionSpec));
        Q1(k.getDimension(tw3.Chip_chipStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        q2(k.getDimension(tw3.Chip_iconStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        o2(k.getDimension(tw3.Chip_iconEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        D2(k.getDimension(tw3.Chip_textStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        B2(k.getDimension(tw3.Chip_textEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        e2(k.getDimension(tw3.Chip_closeIconStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        Z1(k.getDimension(tw3.Chip_closeIconEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        E1(k.getDimension(tw3.Chip_chipEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        s2(k.getDimensionPixelSize(tw3.Chip_android_maxWidth, Integer.MAX_VALUE));
        k.recycle();
    }

    @DexIgnore
    public void r2(int i) {
        q2(this.i0.getResources().getDimension(i));
    }

    @DexIgnore
    public final float s0() {
        this.p0.e().getFontMetrics(this.l0);
        Paint.FontMetrics fontMetrics = this.l0;
        return (fontMetrics.ascent + fontMetrics.descent) / 2.0f;
    }

    @DexIgnore
    public void s1() {
        a aVar = this.G0.get();
        if (aVar != null) {
            aVar.a();
        }
    }

    @DexIgnore
    public void s2(int i) {
        this.J0 = i;
    }

    @DexIgnore
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, runnable, j);
        }
    }

    @DexIgnore
    @Override // com.fossil.c04
    public void setAlpha(int i) {
        if (this.y0 != i) {
            this.y0 = i;
            invalidateSelf();
        }
    }

    @DexIgnore
    @Override // com.fossil.c04
    public void setColorFilter(ColorFilter colorFilter) {
        if (this.z0 != colorFilter) {
            this.z0 = colorFilter;
            invalidateSelf();
        }
    }

    @DexIgnore
    @Override // com.fossil.bm0, com.fossil.c04
    public void setTintList(ColorStateList colorStateList) {
        if (this.B0 != colorStateList) {
            this.B0 = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    @Override // com.fossil.bm0, com.fossil.c04
    public void setTintMode(PorterDuff.Mode mode) {
        if (this.C0 != mode) {
            this.C0 = mode;
            this.A0 = qy3.a(this, this.B0, mode);
            invalidateSelf();
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        if (I2()) {
            visible |= this.L.setVisible(z, z2);
        }
        if (H2()) {
            visible |= this.X.setVisible(z, z2);
        }
        if (J2()) {
            visible |= this.Q.setVisible(z, z2);
        }
        if (visible) {
            invalidateSelf();
        }
        return visible;
    }

    @DexIgnore
    public Paint.Align t0(Rect rect, PointF pointF) {
        pointF.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        Paint.Align align = Paint.Align.LEFT;
        if (this.J != null) {
            float m02 = this.a0 + m0() + this.d0;
            if (am0.f(this) == 0) {
                pointF.x = m02 + ((float) rect.left);
                align = Paint.Align.LEFT;
            } else {
                pointF.x = ((float) rect.right) - m02;
                align = Paint.Align.RIGHT;
            }
            pointF.y = ((float) rect.centerY()) - s0();
        }
        return align;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00e6  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x012e  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0133  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0153  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0156  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean t1(int[] r7, int[] r8) {
        /*
        // Method dump skipped, instructions count: 344
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rx3.t1(int[], int[]):boolean");
    }

    @DexIgnore
    public void t2(ColorStateList colorStateList) {
        if (this.I != colorStateList) {
            this.I = colorStateList;
            L2();
            onStateChange(getState());
        }
    }

    @DexIgnore
    public final boolean u0() {
        return this.W && this.X != null && this.V;
    }

    @DexIgnore
    public void u1(boolean z) {
        if (this.V != z) {
            this.V = z;
            float m02 = m0();
            if (!z && this.w0) {
                this.w0 = false;
            }
            float m03 = m0();
            invalidateSelf();
            if (m02 != m03) {
                s1();
            }
        }
    }

    @DexIgnore
    public void u2(int i) {
        t2(gf0.c(this.i0, i));
    }

    @DexIgnore
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }

    @DexIgnore
    public void v1(int i) {
        u1(this.i0.getResources().getBoolean(i));
    }

    @DexIgnore
    public void v2(boolean z) {
        this.I0 = z;
    }

    @DexIgnore
    public final void w0(Canvas canvas, Rect rect) {
        if (H2()) {
            l0(rect, this.m0);
            RectF rectF = this.m0;
            float f = rectF.left;
            float f2 = rectF.top;
            canvas.translate(f, f2);
            this.X.setBounds(0, 0, (int) this.m0.width(), (int) this.m0.height());
            this.X.draw(canvas);
            canvas.translate(-f, -f2);
        }
    }

    @DexIgnore
    public void w1(Drawable drawable) {
        if (this.X != drawable) {
            float m02 = m0();
            this.X = drawable;
            float m03 = m0();
            K2(this.X);
            k0(this.X);
            invalidateSelf();
            if (m02 != m03) {
                s1();
            }
        }
    }

    @DexIgnore
    public void w2(bx3 bx3) {
        this.Y = bx3;
    }

    @DexIgnore
    public final void x0(Canvas canvas, Rect rect) {
        if (!this.K0) {
            this.j0.setColor(this.r0);
            this.j0.setStyle(Paint.Style.FILL);
            this.j0.setColorFilter(i1());
            this.m0.set(rect);
            canvas.drawRoundRect(this.m0, H0(), H0(), this.j0);
        }
    }

    @DexIgnore
    public void x1(int i) {
        w1(gf0.d(this.i0, i));
    }

    @DexIgnore
    public void x2(int i) {
        w2(bx3.d(this.i0, i));
    }

    @DexIgnore
    public final void y0(Canvas canvas, Rect rect) {
        if (I2()) {
            l0(rect, this.m0);
            RectF rectF = this.m0;
            float f = rectF.left;
            float f2 = rectF.top;
            canvas.translate(f, f2);
            this.L.setBounds(0, 0, (int) this.m0.width(), (int) this.m0.height());
            this.L.draw(canvas);
            canvas.translate(-f, -f2);
        }
    }

    @DexIgnore
    public void y1(int i) {
        z1(this.i0.getResources().getBoolean(i));
    }

    @DexIgnore
    public void y2(CharSequence charSequence) {
        if (charSequence == null) {
            charSequence = "";
        }
        if (!TextUtils.equals(this.J, charSequence)) {
            this.J = charSequence;
            this.p0.i(true);
            invalidateSelf();
            s1();
        }
    }

    @DexIgnore
    public final void z0(Canvas canvas, Rect rect) {
        if (this.H > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && !this.K0) {
            this.j0.setColor(this.t0);
            this.j0.setStyle(Paint.Style.STROKE);
            if (!this.K0) {
                this.j0.setColorFilter(i1());
            }
            RectF rectF = this.m0;
            float f = this.H;
            rectF.set(((float) rect.left) + (f / 2.0f), ((float) rect.top) + (f / 2.0f), ((float) rect.right) - (f / 2.0f), ((float) rect.bottom) - (f / 2.0f));
            float f2 = this.F - (this.H / 2.0f);
            canvas.drawRoundRect(this.m0, f2, f2, this.j0);
        }
    }

    @DexIgnore
    public void z1(boolean z) {
        if (this.W != z) {
            boolean H2 = H2();
            this.W = z;
            boolean H22 = H2();
            if (H2 != H22) {
                if (H22) {
                    k0(this.X);
                } else {
                    K2(this.X);
                }
                invalidateSelf();
                s1();
            }
        }
    }

    @DexIgnore
    public void z2(pz3 pz3) {
        this.p0.h(pz3, this.i0);
    }
}
