package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vf1 implements rb1<BitmapDrawable> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ rd1 f3757a;
    @DexIgnore
    public /* final */ rb1<Bitmap> b;

    @DexIgnore
    public vf1(rd1 rd1, rb1<Bitmap> rb1) {
        this.f3757a = rd1;
        this.b = rb1;
    }

    @DexIgnore
    @Override // com.fossil.rb1
    public ib1 b(ob1 ob1) {
        return this.b.b(ob1);
    }

    @DexIgnore
    /* renamed from: c */
    public boolean a(id1<BitmapDrawable> id1, File file, ob1 ob1) {
        return this.b.a(new yf1(id1.get().getBitmap(), this.f3757a), file, ob1);
    }
}
