package com.fossil;

import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.c64;
import com.fossil.f64;
import com.fossil.h34;
import com.fossil.y24;
import com.fossil.y54;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class d64<T> extends a64<T> implements Serializable {
    @DexIgnore
    public /* final */ Type runtimeType;
    @DexIgnore
    public transient c64 typeResolver;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends y54.b<T> {
        @DexIgnore
        public a(Method method) {
            super(method);
        }

        @DexIgnore
        @Override // com.fossil.x54
        public d64<T> a() {
            return d64.this;
        }

        @DexIgnore
        @Override // com.fossil.x54
        public String toString() {
            return a() + CodelessMatcher.CURRENT_CLASS_NAME + super.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends y54.a<T> {
        @DexIgnore
        public b(Constructor constructor) {
            super(constructor);
        }

        @DexIgnore
        @Override // com.fossil.x54
        public d64<T> a() {
            return d64.this;
        }

        @DexIgnore
        @Override // com.fossil.y54.a
        public Type[] b() {
            return d64.this.resolveInPlace(super.b());
        }

        @DexIgnore
        @Override // com.fossil.x54
        public String toString() {
            return a() + "(" + d14.i(", ").g(b()) + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends e64 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void c(GenericArrayType genericArrayType) {
            a(genericArrayType.getGenericComponentType());
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void d(ParameterizedType parameterizedType) {
            a(parameterizedType.getActualTypeArguments());
            a(parameterizedType.getOwnerType());
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void e(TypeVariable<?> typeVariable) {
            throw new IllegalArgumentException(d64.this.runtimeType + "contains a type variable and is not safe for the operation");
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void f(WildcardType wildcardType) {
            a(wildcardType.getLowerBounds());
            a(wildcardType.getUpperBounds());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends e64 {
        @DexIgnore
        public /* final */ /* synthetic */ h34.a b;

        @DexIgnore
        public d(d64 d64, h34.a aVar) {
            this.b = aVar;
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void b(Class<?> cls) {
            this.b.a(cls);
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void c(GenericArrayType genericArrayType) {
            this.b.a(f64.i(d64.of(genericArrayType.getGenericComponentType()).getRawType()));
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void d(ParameterizedType parameterizedType) {
            this.b.a((Class) parameterizedType.getRawType());
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void e(TypeVariable<?> typeVariable) {
            a(typeVariable.getBounds());
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void f(WildcardType wildcardType) {
            a(wildcardType.getUpperBounds());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Type[] f741a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public e(Type[] typeArr, boolean z) {
            this.f741a = typeArr;
            this.b = z;
        }

        @DexIgnore
        public boolean a(Type type) {
            for (Type type2 : this.f741a) {
                boolean isSubtypeOf = d64.of(type2).isSubtypeOf(type);
                boolean z = this.b;
                if (isSubtypeOf == z) {
                    return z;
                }
            }
            return !this.b;
        }

        @DexIgnore
        public boolean b(Type type) {
            d64<?> of = d64.of(type);
            for (Type type2 : this.f741a) {
                boolean isSubtypeOf = of.isSubtypeOf(type2);
                boolean z = this.b;
                if (isSubtypeOf == z) {
                    return z;
                }
            }
            return !this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f extends d64<T>.k {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient h34<d64<? super T>> c;

        @DexIgnore
        public f() {
            super();
        }

        @DexIgnore
        public /* synthetic */ f(d64 d64, a aVar) {
            this();
        }

        @DexIgnore
        private Object readResolve() {
            return d64.this.getTypes().classes();
        }

        @DexIgnore
        @Override // com.fossil.d64.k
        public d64<T>.k classes() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.l24, com.fossil.l24, com.fossil.o24, com.fossil.q24, com.fossil.q24, com.fossil.q24, com.fossil.d64.k, com.fossil.d64.k, com.fossil.d64.k
        public Set<d64<? super T>> delegate() {
            h34<d64<? super T>> h34 = this.c;
            if (h34 != null) {
                return h34;
            }
            h34<d64<? super T>> d = k24.b(i.f742a.a().d(d64.this)).a(j.IGNORE_TYPE_VARIABLE_OR_WILDCARD).d();
            this.c = d;
            return d;
        }

        @DexIgnore
        @Override // com.fossil.d64.k
        public d64<T>.k interfaces() {
            throw new UnsupportedOperationException("classes().interfaces() not supported.");
        }

        @DexIgnore
        @Override // com.fossil.d64.k
        public Set<Class<? super T>> rawTypes() {
            return h34.copyOf((Collection) i.b.a().c(d64.this.getRawTypes()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g extends d64<T>.k {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ transient d64<T>.k c;
        @DexIgnore
        public transient h34<d64<? super T>> d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements j14<Class<?>> {
            @DexIgnore
            public a(g gVar) {
            }

            @DexIgnore
            /* renamed from: a */
            public boolean apply(Class<?> cls) {
                return cls.isInterface();
            }
        }

        @DexIgnore
        public g(d64<T>.k kVar) {
            super();
            this.c = kVar;
        }

        @DexIgnore
        private Object readResolve() {
            return d64.this.getTypes().interfaces();
        }

        @DexIgnore
        @Override // com.fossil.d64.k
        public d64<T>.k classes() {
            throw new UnsupportedOperationException("interfaces().classes() not supported.");
        }

        @DexIgnore
        @Override // com.fossil.l24, com.fossil.l24, com.fossil.o24, com.fossil.q24, com.fossil.q24, com.fossil.q24, com.fossil.d64.k, com.fossil.d64.k, com.fossil.d64.k
        public Set<d64<? super T>> delegate() {
            h34<d64<? super T>> h34 = this.d;
            if (h34 != null) {
                return h34;
            }
            h34<d64<? super T>> d2 = k24.b(this.c).a(j.INTERFACE_ONLY).d();
            this.d = d2;
            return d2;
        }

        @DexIgnore
        @Override // com.fossil.d64.k
        public d64<T>.k interfaces() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.d64.k
        public Set<Class<? super T>> rawTypes() {
            return k24.b(i.b.c(d64.this.getRawTypes())).a(new a(this)).d();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> extends d64<T> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;

        @DexIgnore
        public h(Type type) {
            super(type, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class i<K> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ i<d64<?>> f742a; // = new a();
        @DexIgnore
        public static /* final */ i<Class<?>> b; // = new b();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends i<d64<?>> {
            @DexIgnore
            public a() {
                super(null);
            }

            @DexIgnore
            /* renamed from: i */
            public Iterable<? extends d64<?>> e(d64<?> d64) {
                return d64.getGenericInterfaces();
            }

            @DexIgnore
            /* renamed from: j */
            public Class<?> f(d64<?> d64) {
                return d64.getRawType();
            }

            @DexIgnore
            /* renamed from: k */
            public d64<?> g(d64<?> d64) {
                return d64.getGenericSuperclass();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends i<Class<?>> {
            @DexIgnore
            public b() {
                super(null);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.d64.i
            public /* bridge */ /* synthetic */ Class f(Class<?> cls) {
                Class<?> cls2 = cls;
                j(cls2);
                return cls2;
            }

            @DexIgnore
            /* renamed from: i */
            public Iterable<? extends Class<?>> e(Class<?> cls) {
                return Arrays.asList(cls.getInterfaces());
            }

            @DexIgnore
            public Class<?> j(Class<?> cls) {
                return cls;
            }

            @DexIgnore
            /* renamed from: k */
            public Class<?> g(Class<?> cls) {
                return cls.getSuperclass();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class c extends e<K> {
            @DexIgnore
            public c(i iVar, i iVar2) {
                super(iVar2);
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.d64$i$c */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.d64.i
            public y24<K> c(Iterable<? extends K> iterable) {
                y24.b builder = y24.builder();
                for (Object obj : iterable) {
                    if (!f(obj).isInterface()) {
                        builder.g(obj);
                    }
                }
                return super.c(builder.i());
            }

            @DexIgnore
            @Override // com.fossil.d64.i
            public Iterable<? extends K> e(K k) {
                return h34.of();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class d extends i44<K> {
            @DexIgnore
            public /* final */ /* synthetic */ Comparator b;
            @DexIgnore
            public /* final */ /* synthetic */ Map c;

            @DexIgnore
            public d(Comparator comparator, Map map) {
                this.b = comparator;
                this.c = map;
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.util.Comparator */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.i44, java.util.Comparator
            public int compare(K k, K k2) {
                return this.b.compare(this.c.get(k), this.c.get(k2));
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class e<K> extends i<K> {
            @DexIgnore
            public /* final */ i<K> c;

            @DexIgnore
            public e(i<K> iVar) {
                super(null);
                this.c = iVar;
            }

            @DexIgnore
            @Override // com.fossil.d64.i
            public Class<?> f(K k) {
                return this.c.f(k);
            }

            @DexIgnore
            @Override // com.fossil.d64.i
            public K g(K k) {
                return this.c.g(k);
            }
        }

        @DexIgnore
        public i() {
        }

        @DexIgnore
        public /* synthetic */ i(a aVar) {
            this();
        }

        @DexIgnore
        public static <K, V> y24<K> h(Map<K, V> map, Comparator<? super V> comparator) {
            return new d(comparator, map).immutableSortedCopy(map.keySet());
        }

        @DexIgnore
        public final i<K> a() {
            return new c(this, this);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r0v11, resolved type: int */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public final int b(K k, Map<? super K, Integer> map) {
            Integer num = map.get(k);
            if (num != null) {
                return num.intValue();
            }
            boolean isInterface = f(k).isInterface();
            Iterator<? extends K> it = e(k).iterator();
            int i = isInterface;
            while (it.hasNext()) {
                i = Math.max(i, b(it.next(), map));
            }
            K g = g(k);
            int i2 = i;
            if (g != null) {
                i2 = Math.max(i, b(g, map));
            }
            int i3 = (i2 == 1 ? 1 : 0) + 1;
            map.put(k, Integer.valueOf(i3));
            return i3;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.d64$i<K> */
        /* JADX WARN: Multi-variable type inference failed */
        public y24<K> c(Iterable<? extends K> iterable) {
            HashMap j = x34.j();
            Iterator<? extends K> it = iterable.iterator();
            while (it.hasNext()) {
                b(it.next(), j);
            }
            return h(j, i44.natural().reverse());
        }

        @DexIgnore
        public final y24<K> d(K k) {
            return c(y24.of(k));
        }

        @DexIgnore
        public abstract Iterable<? extends K> e(K k);

        @DexIgnore
        public abstract Class<?> f(K k);

        @DexIgnore
        public abstract K g(K k);
    }

    @DexIgnore
    public enum j implements j14<d64<?>> {
        IGNORE_TYPE_VARIABLE_OR_WILDCARD {
            @DexIgnore
            public boolean apply(d64<?> d64) {
                return !(d64.runtimeType instanceof TypeVariable) && !(d64.runtimeType instanceof WildcardType);
            }
        },
        INTERFACE_ONLY {
            @DexIgnore
            public boolean apply(d64<?> d64) {
                return d64.getRawType().isInterface();
            }
        };

        @DexIgnore
        public /* synthetic */ j(a aVar) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.j14
        @CanIgnoreReturnValue
        public abstract /* synthetic */ boolean apply(T t);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends q24<d64<? super T>> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient h34<d64<? super T>> b;

        @DexIgnore
        public k() {
        }

        @DexIgnore
        public d64<T>.k classes() {
            return new f(d64.this, null);
        }

        @DexIgnore
        @Override // com.fossil.l24, com.fossil.l24, com.fossil.o24, com.fossil.q24, com.fossil.q24, com.fossil.q24
        public Set<d64<? super T>> delegate() {
            h34<d64<? super T>> h34 = this.b;
            if (h34 != null) {
                return h34;
            }
            h34<d64<? super T>> d = k24.b(i.f742a.d(d64.this)).a(j.IGNORE_TYPE_VARIABLE_OR_WILDCARD).d();
            this.b = d;
            return d;
        }

        @DexIgnore
        public d64<T>.k interfaces() {
            return new g(this);
        }

        @DexIgnore
        public Set<Class<? super T>> rawTypes() {
            return h34.copyOf((Collection) i.b.c(d64.this.getRawTypes()));
        }
    }

    @DexIgnore
    public d64() {
        Type capture = capture();
        this.runtimeType = capture;
        i14.v(!(capture instanceof TypeVariable), "Cannot construct a TypeToken for a type variable.\nYou probably meant to call new TypeToken<%s>(getClass()) that can resolve the type variable for you.\nIf you do need to create a TypeToken of a type variable, please use TypeToken.of() instead.", capture);
    }

    @DexIgnore
    public d64(Class<?> cls) {
        Type capture = super.capture();
        if (capture instanceof Class) {
            this.runtimeType = capture;
        } else {
            this.runtimeType = of((Class) cls).resolveType(capture).runtimeType;
        }
    }

    @DexIgnore
    public d64(Type type) {
        i14.l(type);
        this.runtimeType = type;
    }

    @DexIgnore
    public /* synthetic */ d64(Type type, a aVar) {
        this(type);
    }

    @DexIgnore
    public static e any(Type[] typeArr) {
        return new e(typeArr, true);
    }

    @DexIgnore
    private d64<? super T> boundAsSuperclass(Type type) {
        d64<? super T> d64 = (d64<? super T>) of(type);
        if (d64.getRawType().isInterface()) {
            return null;
        }
        return d64;
    }

    @DexIgnore
    private y24<d64<? super T>> boundsAsInterfaces(Type[] typeArr) {
        y24.b builder = y24.builder();
        for (Type type : typeArr) {
            d64<?> of = of(type);
            if (of.getRawType().isInterface()) {
                builder.g(of);
            }
        }
        return builder.i();
    }

    @DexIgnore
    public static e every(Type[] typeArr) {
        return new e(typeArr, false);
    }

    @DexIgnore
    private d64<? extends T> getArraySubtype(Class<?> cls) {
        return (d64<? extends T>) of(newArrayClassOrGenericArrayType(getComponentType().getSubtype(cls.getComponentType()).runtimeType));
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r1v1. Raw type applied. Possible types: java.lang.Class<?>, java.lang.Class<? super ?> */
    private d64<? super T> getArraySupertype(Class<? super T> cls) {
        d64<?> componentType = getComponentType();
        i14.n(componentType, "%s isn't a super type of %s", cls, this);
        return (d64<? super T>) of(newArrayClassOrGenericArrayType(componentType.getSupertype(cls.getComponentType()).runtimeType));
    }

    @DexIgnore
    private Type getOwnerTypeIfPresent() {
        Type type = this.runtimeType;
        if (type instanceof ParameterizedType) {
            return ((ParameterizedType) type).getOwnerType();
        }
        if (type instanceof Class) {
            return ((Class) type).getEnclosingClass();
        }
        return null;
    }

    @DexIgnore
    private h34<Class<? super T>> getRawTypes() {
        h34.a builder = h34.builder();
        new d(this, builder).a(this.runtimeType);
        return builder.j();
    }

    @DexIgnore
    private d64<? extends T> getSubtypeFromLowerBounds(Class<?> cls, Type[] typeArr) {
        if (typeArr.length > 0) {
            return (d64<? extends T>) of(typeArr[0]).getSubtype(cls);
        }
        throw new IllegalArgumentException(cls + " isn't a subclass of " + this);
    }

    @DexIgnore
    private d64<? super T> getSupertypeFromUpperBounds(Class<? super T> cls, Type[] typeArr) {
        for (Type type : typeArr) {
            d64<?> of = of(type);
            if (of.isSubtypeOf(cls)) {
                return (d64<? super T>) of.getSupertype(cls);
            }
        }
        throw new IllegalArgumentException(cls + " isn't a super type of " + this);
    }

    @DexIgnore
    private boolean is(Type type) {
        if (this.runtimeType.equals(type)) {
            return true;
        }
        if (!(type instanceof WildcardType)) {
            return false;
        }
        WildcardType wildcardType = (WildcardType) type;
        return every(wildcardType.getUpperBounds()).b(this.runtimeType) && every(wildcardType.getLowerBounds()).a(this.runtimeType);
    }

    @DexIgnore
    private boolean isOwnedBySubtypeOf(Type type) {
        Iterator it = getTypes().iterator();
        while (it.hasNext()) {
            Type ownerTypeIfPresent = ((d64) it.next()).getOwnerTypeIfPresent();
            if (ownerTypeIfPresent != null && of(ownerTypeIfPresent).isSubtypeOf(type)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    private boolean isSubtypeOfArrayType(GenericArrayType genericArrayType) {
        Type type = this.runtimeType;
        if (type instanceof Class) {
            Class cls = (Class) type;
            if (!cls.isArray()) {
                return false;
            }
            return of((Class) cls.getComponentType()).isSubtypeOf(genericArrayType.getGenericComponentType());
        } else if (type instanceof GenericArrayType) {
            return of(((GenericArrayType) type).getGenericComponentType()).isSubtypeOf(genericArrayType.getGenericComponentType());
        } else {
            return false;
        }
    }

    @DexIgnore
    private boolean isSubtypeOfParameterizedType(ParameterizedType parameterizedType) {
        Class<? super Object> rawType = of(parameterizedType).getRawType();
        if (!someRawTypeIsSubclassOf(rawType)) {
            return false;
        }
        TypeVariable<Class<? super Object>>[] typeParameters = rawType.getTypeParameters();
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        for (int i2 = 0; i2 < typeParameters.length; i2++) {
            if (!resolveType(typeParameters[i2]).is(actualTypeArguments[i2])) {
                return false;
            }
        }
        return Modifier.isStatic(((Class) parameterizedType.getRawType()).getModifiers()) || parameterizedType.getOwnerType() == null || isOwnedBySubtypeOf(parameterizedType.getOwnerType());
    }

    @DexIgnore
    private boolean isSupertypeOfArray(GenericArrayType genericArrayType) {
        Type type = this.runtimeType;
        if (type instanceof Class) {
            Class cls = (Class) type;
            return !cls.isArray() ? cls.isAssignableFrom(Object[].class) : of(genericArrayType.getGenericComponentType()).isSubtypeOf(cls.getComponentType());
        } else if (type instanceof GenericArrayType) {
            return of(genericArrayType.getGenericComponentType()).isSubtypeOf(((GenericArrayType) this.runtimeType).getGenericComponentType());
        } else {
            return false;
        }
    }

    @DexIgnore
    private boolean isWrapper() {
        return w54.b().contains(this.runtimeType);
    }

    @DexIgnore
    public static Type newArrayClassOrGenericArrayType(Type type) {
        return f64.e.JAVA7.newArrayType(type);
    }

    @DexIgnore
    public static <T> d64<T> of(Class<T> cls) {
        return new h(cls);
    }

    @DexIgnore
    public static d64<?> of(Type type) {
        return new h(type);
    }

    @DexIgnore
    private Type[] resolveInPlace(Type[] typeArr) {
        for (int i2 = 0; i2 < typeArr.length; i2++) {
            typeArr[i2] = resolveType(typeArr[i2]).getType();
        }
        return typeArr;
    }

    @DexIgnore
    private d64<?> resolveSupertype(Type type) {
        d64<?> resolveType = resolveType(type);
        resolveType.typeResolver = this.typeResolver;
        return resolveType;
    }

    @DexIgnore
    private Type resolveTypeArgsForSubclass(Class<?> cls) {
        if ((this.runtimeType instanceof Class) && (cls.getTypeParameters().length == 0 || getRawType().getTypeParameters().length != 0)) {
            return cls;
        }
        d64 genericType = toGenericType(cls);
        return new c64().l(genericType.getSupertype(getRawType()).runtimeType, this.runtimeType).i(genericType.runtimeType);
    }

    @DexIgnore
    private boolean someRawTypeIsSubclassOf(Class<?> cls) {
        Iterator it = getRawTypes().iterator();
        while (it.hasNext()) {
            if (cls.isAssignableFrom((Class) it.next())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static <T> d64<? extends T> toGenericType(Class<T> cls) {
        if (cls.isArray()) {
            return (d64<? extends T>) of(f64.k(toGenericType(cls.getComponentType()).runtimeType));
        }
        TypeVariable<Class<T>>[] typeParameters = cls.getTypeParameters();
        Type type = (!cls.isMemberClass() || Modifier.isStatic(cls.getModifiers())) ? null : toGenericType(cls.getEnclosingClass()).runtimeType;
        return (typeParameters.length > 0 || !(type == null || type == cls.getEnclosingClass())) ? (d64<? extends T>) of(f64.n(type, cls, typeParameters)) : of((Class) cls);
    }

    @DexIgnore
    public final y54<T, T> constructor(Constructor<?> constructor) {
        i14.i(constructor.getDeclaringClass() == getRawType(), "%s not declared by %s", constructor, getRawType());
        return new b(constructor);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof d64) {
            return this.runtimeType.equals(((d64) obj).runtimeType);
        }
        return false;
    }

    @DexIgnore
    public final d64<?> getComponentType() {
        Type j2 = f64.j(this.runtimeType);
        if (j2 == null) {
            return null;
        }
        return of(j2);
    }

    @DexIgnore
    public final y24<d64<? super T>> getGenericInterfaces() {
        Type type = this.runtimeType;
        if (type instanceof TypeVariable) {
            return boundsAsInterfaces(((TypeVariable) type).getBounds());
        }
        if (type instanceof WildcardType) {
            return boundsAsInterfaces(((WildcardType) type).getUpperBounds());
        }
        y24.b builder = y24.builder();
        for (Type type2 : getRawType().getGenericInterfaces()) {
            builder.g(resolveSupertype(type2));
        }
        return builder.i();
    }

    @DexIgnore
    public final d64<? super T> getGenericSuperclass() {
        Type type = this.runtimeType;
        if (type instanceof TypeVariable) {
            return boundAsSuperclass(((TypeVariable) type).getBounds()[0]);
        }
        if (type instanceof WildcardType) {
            return boundAsSuperclass(((WildcardType) type).getUpperBounds()[0]);
        }
        Type genericSuperclass = getRawType().getGenericSuperclass();
        if (genericSuperclass == null) {
            return null;
        }
        return (d64<? super T>) resolveSupertype(genericSuperclass);
    }

    @DexIgnore
    public final Class<? super T> getRawType() {
        return getRawTypes().iterator().next();
    }

    @DexIgnore
    public final d64<? extends T> getSubtype(Class<?> cls) {
        i14.h(!(this.runtimeType instanceof TypeVariable), "Cannot get subtype of type variable <%s>", this);
        Type type = this.runtimeType;
        if (type instanceof WildcardType) {
            return getSubtypeFromLowerBounds(cls, ((WildcardType) type).getLowerBounds());
        }
        if (isArray()) {
            return getArraySubtype(cls);
        }
        i14.i(getRawType().isAssignableFrom(cls), "%s isn't a subclass of %s", cls, this);
        return (d64<? extends T>) of(resolveTypeArgsForSubclass(cls));
    }

    @DexIgnore
    public final d64<? super T> getSupertype(Class<? super T> cls) {
        i14.i(someRawTypeIsSubclassOf(cls), "%s is not a super class of %s", cls, this);
        Type type = this.runtimeType;
        return type instanceof TypeVariable ? getSupertypeFromUpperBounds(cls, ((TypeVariable) type).getBounds()) : type instanceof WildcardType ? getSupertypeFromUpperBounds(cls, ((WildcardType) type).getUpperBounds()) : cls.isArray() ? getArraySupertype(cls) : (d64<? super T>) resolveSupertype(toGenericType(cls).runtimeType);
    }

    @DexIgnore
    public final Type getType() {
        return this.runtimeType;
    }

    @DexIgnore
    public final d64<T>.k getTypes() {
        return new k();
    }

    @DexIgnore
    public int hashCode() {
        return this.runtimeType.hashCode();
    }

    @DexIgnore
    public final boolean isArray() {
        return getComponentType() != null;
    }

    @DexIgnore
    public final boolean isPrimitive() {
        Type type = this.runtimeType;
        return (type instanceof Class) && ((Class) type).isPrimitive();
    }

    @DexIgnore
    public final boolean isSubtypeOf(d64<?> d64) {
        return isSubtypeOf(d64.getType());
    }

    @DexIgnore
    public final boolean isSubtypeOf(Type type) {
        i14.l(type);
        if (type instanceof WildcardType) {
            return any(((WildcardType) type).getLowerBounds()).b(this.runtimeType);
        }
        Type type2 = this.runtimeType;
        if (type2 instanceof WildcardType) {
            return any(((WildcardType) type2).getUpperBounds()).a(type);
        }
        if (type2 instanceof TypeVariable) {
            return type2.equals(type) || any(((TypeVariable) this.runtimeType).getBounds()).a(type);
        }
        if (type2 instanceof GenericArrayType) {
            return of(type).isSupertypeOfArray((GenericArrayType) this.runtimeType);
        }
        if (type instanceof Class) {
            return someRawTypeIsSubclassOf((Class) type);
        }
        if (type instanceof ParameterizedType) {
            return isSubtypeOfParameterizedType((ParameterizedType) type);
        }
        if (type instanceof GenericArrayType) {
            return isSubtypeOfArrayType((GenericArrayType) type);
        }
        return false;
    }

    @DexIgnore
    public final boolean isSupertypeOf(d64<?> d64) {
        return d64.isSubtypeOf(getType());
    }

    @DexIgnore
    public final boolean isSupertypeOf(Type type) {
        return of(type).isSubtypeOf(getType());
    }

    @DexIgnore
    public final y54<T, Object> method(Method method) {
        i14.i(someRawTypeIsSubclassOf(method.getDeclaringClass()), "%s not declared by %s", method, this);
        return new a(method);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public final d64<T> rejectTypeVariables() {
        new c().a(this.runtimeType);
        return this;
    }

    @DexIgnore
    public final d64<?> resolveType(Type type) {
        i14.l(type);
        c64 c64 = this.typeResolver;
        if (c64 == null) {
            c64 = c64.d(this.runtimeType);
            this.typeResolver = c64;
        }
        return of(c64.i(type));
    }

    @DexIgnore
    public String toString() {
        return f64.t(this.runtimeType);
    }

    @DexIgnore
    public final d64<T> unwrap() {
        return isWrapper() ? of(w54.c((Class) this.runtimeType)) : this;
    }

    @DexIgnore
    public final <X> d64<T> where(b64<X> b64, d64<X> d64) {
        return new h(new c64().m(a34.of(new c64.d(b64.f399a), d64.runtimeType)).i(this.runtimeType));
    }

    @DexIgnore
    public final <X> d64<T> where(b64<X> b64, Class<X> cls) {
        return where(b64, of((Class) cls));
    }

    @DexIgnore
    public final d64<T> wrap() {
        return isPrimitive() ? of(w54.d((Class) this.runtimeType)) : this;
    }

    @DexIgnore
    public Object writeReplace() {
        return of(new c64().i(this.runtimeType));
    }
}
