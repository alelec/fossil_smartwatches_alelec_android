package com.fossil;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ux2<E> extends sx2<E> {
    @DexIgnore
    public /* final */ transient sx2<E> d;

    @DexIgnore
    public ux2(sx2<E> sx2) {
        this.d = sx2;
    }

    @DexIgnore
    @Override // com.fossil.sx2, com.fossil.tx2
    public final boolean contains(@NullableDecl Object obj) {
        return this.d.contains(obj);
    }

    @DexIgnore
    @Override // java.util.List
    public final E get(int i) {
        sw2.a(i, size());
        return this.d.get(zza(i));
    }

    @DexIgnore
    @Override // com.fossil.sx2
    public final int indexOf(@NullableDecl Object obj) {
        int lastIndexOf = this.d.lastIndexOf(obj);
        if (lastIndexOf >= 0) {
            return zza(lastIndexOf);
        }
        return -1;
    }

    @DexIgnore
    @Override // com.fossil.sx2
    public final int lastIndexOf(@NullableDecl Object obj) {
        int indexOf = this.d.indexOf(obj);
        if (indexOf >= 0) {
            return zza(indexOf);
        }
        return -1;
    }

    @DexIgnore
    public final int size() {
        return this.d.size();
    }

    @DexIgnore
    public final int zza(int i) {
        return (size() - 1) - i;
    }

    @DexIgnore
    @Override // com.fossil.sx2
    /* renamed from: zza */
    public final sx2<E> subList(int i, int i2) {
        sw2.e(i, i2, size());
        return ((sx2) this.d.subList(size() - i2, size() - i)).zzd();
    }

    @DexIgnore
    @Override // com.fossil.sx2
    public final sx2<E> zzd() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final boolean zzh() {
        return this.d.zzh();
    }
}
