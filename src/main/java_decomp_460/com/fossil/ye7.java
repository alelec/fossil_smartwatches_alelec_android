package com.fossil;

import android.os.Build;
import android.os.Looper;
import android.os.Process;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ye7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static int f4308a; // = 6;
    @DexIgnore
    public static a b; // = new ze7();

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(String str, String str2);

        @DexIgnore
        void b(String str, String str2);

        @DexIgnore
        int c();

        @DexIgnore
        void d(String str, String str2);

        @DexIgnore
        void i(String str, String str2);
    }

    /*
    static {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("VERSION.RELEASE:[" + Build.VERSION.RELEASE);
            sb.append("] VERSION.CODENAME:[" + Build.VERSION.CODENAME);
            sb.append("] VERSION.INCREMENTAL:[" + Build.VERSION.INCREMENTAL);
            sb.append("] BOARD:[" + Build.BOARD);
            sb.append("] DEVICE:[" + Build.DEVICE);
            sb.append("] DISPLAY:[" + Build.DISPLAY);
            sb.append("] FINGERPRINT:[" + Build.FINGERPRINT);
            sb.append("] HOST:[" + Build.HOST);
            sb.append("] MANUFACTURER:[" + Build.MANUFACTURER);
            sb.append("] MODEL:[" + Build.MODEL);
            sb.append("] PRODUCT:[" + Build.PRODUCT);
            sb.append("] TAGS:[" + Build.TAGS);
            sb.append("] TYPE:[" + Build.TYPE);
            sb.append("] USER:[" + Build.USER + "]");
        } catch (Throwable th) {
            th.printStackTrace();
        }
        sb.toString();
    }
    */

    @DexIgnore
    public static void a(String str, String str2, Object... objArr) {
        a aVar = b;
        if (aVar != null && aVar.c() <= 4) {
            String format = objArr == null ? str2 : String.format(str2, objArr);
            if (format == null) {
                format = "";
            }
            a aVar2 = b;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            aVar2.i(str, format);
        }
    }

    @DexIgnore
    public static void b(String str, String str2) {
        a(str, str2, null);
    }

    @DexIgnore
    public static void c(String str, String str2) {
        a aVar = b;
        if (aVar != null && aVar.c() <= 3) {
            a aVar2 = b;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            aVar2.d(str, str2);
        }
    }

    @DexIgnore
    public static void d(String str, String str2) {
        a aVar = b;
        if (aVar != null && aVar.c() <= 2) {
            a aVar2 = b;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            aVar2.b(str, str2);
        }
    }

    @DexIgnore
    public static void e(String str, String str2) {
        a aVar = b;
        if (aVar != null && aVar.c() <= 1) {
            if (str2 == null) {
                str2 = "";
            }
            a aVar2 = b;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            aVar2.a(str, str2);
        }
    }
}
