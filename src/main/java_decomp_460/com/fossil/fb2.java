package com.fossil;

import android.util.Log;
import com.fossil.m62;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fb2 implements ht3<Map<g72<?>, String>> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ db2 f1097a;

    @DexIgnore
    public fb2(db2 db2) {
        this.f1097a = db2;
    }

    @DexIgnore
    @Override // com.fossil.ht3
    public final void onComplete(nt3<Map<g72<?>, String>> nt3) {
        db2.p(this.f1097a).lock();
        try {
            if (db2.z(this.f1097a)) {
                if (nt3.q()) {
                    db2.o(this.f1097a, new zi0(db2.A(this.f1097a).size()));
                    for (bb2 bb2 : db2.A(this.f1097a).values()) {
                        db2.B(this.f1097a).put(bb2.a(), z52.f);
                    }
                } else if (nt3.l() instanceof o62) {
                    o62 o62 = (o62) nt3.l();
                    if (db2.C(this.f1097a)) {
                        db2.o(this.f1097a, new zi0(db2.A(this.f1097a).size()));
                        for (bb2 bb22 : db2.A(this.f1097a).values()) {
                            g72 a2 = bb22.a();
                            z52 connectionResult = o62.getConnectionResult((q62<? extends m62.d>) bb22);
                            if (db2.r(this.f1097a, bb22, connectionResult)) {
                                db2.B(this.f1097a).put(a2, new z52(16));
                            } else {
                                db2.B(this.f1097a).put(a2, connectionResult);
                            }
                        }
                    } else {
                        db2.o(this.f1097a, o62.zaj());
                    }
                    db2.n(this.f1097a, db2.D(this.f1097a));
                } else {
                    Log.e("ConnectionlessGAC", "Unexpected availability exception", nt3.l());
                    db2.o(this.f1097a, Collections.emptyMap());
                    db2.n(this.f1097a, new z52(8));
                }
                if (db2.E(this.f1097a) != null) {
                    db2.B(this.f1097a).putAll(db2.E(this.f1097a));
                    db2.n(this.f1097a, db2.D(this.f1097a));
                }
                if (db2.F(this.f1097a) == null) {
                    db2.G(this.f1097a);
                    db2.H(this.f1097a);
                } else {
                    db2.s(this.f1097a, false);
                    db2.I(this.f1097a).a(db2.F(this.f1097a));
                }
                db2.J(this.f1097a).signalAll();
                db2.p(this.f1097a).unlock();
            }
        } finally {
            db2.p(this.f1097a).unlock();
        }
    }
}
