package com.fossil;

import android.os.Build;
import android.text.TextUtils;
import com.facebook.appevents.UserDataStore;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Locale;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hd0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Charset f1463a; // = dx1.a();
    @DexIgnore
    public static /* final */ Locale b; // = dx1.b();
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ UUID d; // = UUID.fromString("3dda0001-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ UUID f; // = UUID.fromString("3dda0002-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID g; // = UUID.fromString("3dda0003-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID h; // = UUID.fromString("3dda0004-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID i; // = UUID.fromString("3dda0005-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID j; // = UUID.fromString("3dda0006-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID k; // = UUID.fromString("3dda0007-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID l; // = UUID.fromString("00002a24-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID m; // = UUID.fromString("00002a25-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID n; // = UUID.fromString("00002a26-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID o; // = UUID.fromString("00002a28-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID p; // = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID q; // = UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ rt r; // = rt.SIXTEEN_BYTES_MSB_ECDH_SHARED_SECRET_KEY;
    @DexIgnore
    public static /* final */ ry1 s; // = new ry1(0, 0);
    @DexIgnore
    public static /* final */ ry1 t; // = new ry1(1, 0);
    @DexIgnore
    public static /* final */ ry1 u; // = new ry1(1, 0);
    @DexIgnore
    public static /* final */ ry1 v; // = new ry1(0, 0);
    @DexIgnore
    public static /* final */ ve w; // = new ve(12, 12, 0, 600);
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ hd0 y; // = new hd0();

    /*
    static {
        String property = System.getProperty("line.separator");
        if (property == null) {
            property = "\n";
        }
        c = property;
        UUID.fromString("00001805-0000-1000-8000-00805f9b34fb");
        UUID.fromString("00002a2b-0000-1000-8000-00805f9b34fb");
        UUID.fromString("00002a0f-0000-1000-8000-00805f9b34fb");
        UUID.fromString("0000180d-0000-1000-8000-00805f9b34fb");
        String q2 = vt7.q("3dda0001-957f-7d4a-34a6-74696673696d", ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, "", false, 4, null);
        Locale locale = b;
        if (q2 != null) {
            String upperCase = q2.toUpperCase(locale);
            pq7.b(upperCase, "(this as java.lang.String).toUpperCase(locale)");
            e = upperCase;
            StringBuilder e2 = e.e("SDK/Android##;5.13.5;");
            e2.append(Build.MODEL);
            e2.append(";Android ");
            e2.append(Build.VERSION.RELEASE);
            x = e2.toString();
            return;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }
    */

    @DexIgnore
    public final UUID a() {
        return p;
    }

    @DexIgnore
    public final rt b() {
        return r;
    }

    @DexIgnore
    public final Charset c() {
        return f1463a;
    }

    @DexIgnore
    public final ry1 d() {
        return v;
    }

    @DexIgnore
    public final Locale e() {
        return b;
    }

    @DexIgnore
    public final ry1 f() {
        return s;
    }

    @DexIgnore
    public final UUID g() {
        return n;
    }

    @DexIgnore
    public final UUID h() {
        return l;
    }

    @DexIgnore
    public final UUID i() {
        return m;
    }

    @DexIgnore
    public final UUID j() {
        return o;
    }

    @DexIgnore
    public final ry1 k() {
        return t;
    }

    @DexIgnore
    public final UUID l() {
        return q;
    }

    @DexIgnore
    public final ve m() {
        return w;
    }

    @DexIgnore
    public final String n() {
        Locale locale = Locale.getDefault();
        pq7.b(locale, "locale");
        String language = locale.getLanguage();
        String country = locale.getCountry();
        if (TextUtils.isEmpty(language)) {
            return "";
        }
        if (pq7.a(language, "iw")) {
            language = "he";
        }
        if (pq7.a(language, "in")) {
            language = "id";
        }
        if (pq7.a(language, "ji")) {
            language = "yi";
        }
        pq7.b(country, UserDataStore.COUNTRY);
        if (!vt7.l(country)) {
            hr7 hr7 = hr7.f1520a;
            Locale locale2 = Locale.US;
            pq7.b(locale2, "Locale.US");
            language = String.format(locale2, "%s-%s", Arrays.copyOf(new Object[]{language, country}, 2));
            pq7.b(language, "java.lang.String.format(locale, format, *args)");
        }
        pq7.b(language, "localeString");
        return language;
    }

    @DexIgnore
    public final UUID o() {
        return j;
    }

    @DexIgnore
    public final UUID p() {
        return i;
    }

    @DexIgnore
    public final UUID q() {
        return f;
    }

    @DexIgnore
    public final UUID r() {
        return g;
    }

    @DexIgnore
    public final UUID s() {
        return h;
    }

    @DexIgnore
    public final UUID t() {
        return k;
    }

    @DexIgnore
    public final UUID u() {
        return d;
    }

    @DexIgnore
    public final String v() {
        return e;
    }

    @DexIgnore
    public final String w() {
        return c;
    }

    @DexIgnore
    public final ry1 x() {
        return u;
    }

    @DexIgnore
    public final String y() {
        return x;
    }
}
