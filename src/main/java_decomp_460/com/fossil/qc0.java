package com.fossil;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qc0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ qc0 f2957a; // = new qc0();

    @DexIgnore
    public final Typeface a(String str, String str2) {
        AssetManager assets;
        String[] list;
        String str3;
        Context a2 = id0.i.a();
        if (a2 == null || (assets = a2.getAssets()) == null || (list = assets.list(str)) == null) {
            return null;
        }
        int length = list.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                str3 = null;
                break;
            }
            str3 = list[i];
            if (vt7.j(cp7.g(new File(str3)), str2, true)) {
                break;
            }
            i++;
        }
        if (str3 == null) {
            return null;
        }
        return Typeface.createFromAsset(assets, str + '/' + str3);
    }
}
