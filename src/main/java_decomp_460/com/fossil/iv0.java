package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.uu0;
import com.fossil.vu0;
import com.fossil.zu0;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class iv0<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> {
    @DexIgnore
    public /* final */ vu0<T> mDiffer;
    @DexIgnore
    public /* final */ vu0.b<T> mListener; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements vu0.b<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.vu0.b
        public void a(List<T> list, List<T> list2) {
            iv0.this.onCurrentListChanged(list, list2);
        }
    }

    @DexIgnore
    public iv0(uu0<T> uu0) {
        vu0<T> vu0 = new vu0<>(new tu0(this), uu0);
        this.mDiffer = vu0;
        vu0.a(this.mListener);
    }

    @DexIgnore
    public iv0(zu0.d<T> dVar) {
        vu0<T> vu0 = new vu0<>(new tu0(this), new uu0.a(dVar).a());
        this.mDiffer = vu0;
        vu0.a(this.mListener);
    }

    @DexIgnore
    public List<T> getCurrentList() {
        return this.mDiffer.b();
    }

    @DexIgnore
    public T getItem(int i) {
        return this.mDiffer.b().get(i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.mDiffer.b().size();
    }

    @DexIgnore
    public void onCurrentListChanged(List<T> list, List<T> list2) {
    }

    @DexIgnore
    public void submitList(List<T> list) {
        this.mDiffer.e(list);
    }

    @DexIgnore
    public void submitList(List<T> list, Runnable runnable) {
        this.mDiffer.f(list, runnable);
    }
}
