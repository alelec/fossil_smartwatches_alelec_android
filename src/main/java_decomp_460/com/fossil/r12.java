package com.fossil;

import com.fossil.v12;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r12 extends v12 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ t32 f3065a;
    @DexIgnore
    public /* final */ Map<vy1, v12.b> b;

    @DexIgnore
    public r12(t32 t32, Map<vy1, v12.b> map) {
        if (t32 != null) {
            this.f3065a = t32;
            if (map != null) {
                this.b = map;
                return;
            }
            throw new NullPointerException("Null values");
        }
        throw new NullPointerException("Null clock");
    }

    @DexIgnore
    @Override // com.fossil.v12
    public t32 d() {
        return this.f3065a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v12)) {
            return false;
        }
        v12 v12 = (v12) obj;
        return this.f3065a.equals(v12.d()) && this.b.equals(v12.g());
    }

    @DexIgnore
    @Override // com.fossil.v12
    public Map<vy1, v12.b> g() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.f3065a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "SchedulerConfig{clock=" + this.f3065a + ", values=" + this.b + "}";
    }
}
