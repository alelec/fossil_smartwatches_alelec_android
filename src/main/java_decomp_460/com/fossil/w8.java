package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class w8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f3894a;

    /*
    static {
        int[] iArr = new int[fm1.values().length];
        f3894a = iArr;
        iArr[fm1.WEATHER.ordinal()] = 1;
        f3894a[fm1.HEART_RATE.ordinal()] = 2;
        f3894a[fm1.STEPS.ordinal()] = 3;
        f3894a[fm1.DATE.ordinal()] = 4;
        f3894a[fm1.CHANCE_OF_RAIN.ordinal()] = 5;
        f3894a[fm1.SECOND_TIMEZONE.ordinal()] = 6;
        f3894a[fm1.ACTIVE_MINUTES.ordinal()] = 7;
        f3894a[fm1.CALORIES.ordinal()] = 8;
        f3894a[fm1.BATTERY.ordinal()] = 9;
        f3894a[fm1.EMPTY.ordinal()] = 10;
    }
    */
}
