package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y45 extends x45 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D;
    @DexIgnore
    public /* final */ NestedScrollView A;
    @DexIgnore
    public long B;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        D = sparseIntArray;
        sparseIntArray.put(2131362082, 1);
        D.put(2131363360, 2);
        D.put(2131363493, 3);
        D.put(2131362705, 4);
        D.put(2131362085, 5);
        D.put(2131363367, 6);
        D.put(2131363495, 7);
        D.put(2131362707, 8);
        D.put(2131362189, 9);
        D.put(2131362262, 10);
    }
    */

    @DexIgnore
    public y45(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 11, C, D));
    }

    @DexIgnore
    public y45(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[5], (TodayHeartRateChart) objArr[9], (FlexibleButton) objArr[10], (ImageView) objArr[4], (ImageView) objArr[8], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[6], (View) objArr[3], (View) objArr[7]);
        this.B = -1;
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.A = nestedScrollView;
        nestedScrollView.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.B != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.B = 1;
        }
        w();
    }
}
