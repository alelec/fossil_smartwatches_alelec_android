package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ja4 extends ta4.d.AbstractC0224d.a.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ua4<ta4.d.AbstractC0224d.a.b.e> f1733a;
    @DexIgnore
    public /* final */ ta4.d.AbstractC0224d.a.b.c b;
    @DexIgnore
    public /* final */ ta4.d.AbstractC0224d.a.b.AbstractC0230d c;
    @DexIgnore
    public /* final */ ua4<ta4.d.AbstractC0224d.a.b.AbstractC0226a> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.d.AbstractC0224d.a.b.AbstractC0228b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public ua4<ta4.d.AbstractC0224d.a.b.e> f1734a;
        @DexIgnore
        public ta4.d.AbstractC0224d.a.b.c b;
        @DexIgnore
        public ta4.d.AbstractC0224d.a.b.AbstractC0230d c;
        @DexIgnore
        public ua4<ta4.d.AbstractC0224d.a.b.AbstractC0226a> d;

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.AbstractC0228b
        public ta4.d.AbstractC0224d.a.b a() {
            String str = "";
            if (this.f1734a == null) {
                str = " threads";
            }
            if (this.b == null) {
                str = str + " exception";
            }
            if (this.c == null) {
                str = str + " signal";
            }
            if (this.d == null) {
                str = str + " binaries";
            }
            if (str.isEmpty()) {
                return new ja4(this.f1734a, this.b, this.c, this.d);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.AbstractC0228b
        public ta4.d.AbstractC0224d.a.b.AbstractC0228b b(ua4<ta4.d.AbstractC0224d.a.b.AbstractC0226a> ua4) {
            if (ua4 != null) {
                this.d = ua4;
                return this;
            }
            throw new NullPointerException("Null binaries");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.AbstractC0228b
        public ta4.d.AbstractC0224d.a.b.AbstractC0228b c(ta4.d.AbstractC0224d.a.b.c cVar) {
            if (cVar != null) {
                this.b = cVar;
                return this;
            }
            throw new NullPointerException("Null exception");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.AbstractC0228b
        public ta4.d.AbstractC0224d.a.b.AbstractC0228b d(ta4.d.AbstractC0224d.a.b.AbstractC0230d dVar) {
            if (dVar != null) {
                this.c = dVar;
                return this;
            }
            throw new NullPointerException("Null signal");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.AbstractC0228b
        public ta4.d.AbstractC0224d.a.b.AbstractC0228b e(ua4<ta4.d.AbstractC0224d.a.b.e> ua4) {
            if (ua4 != null) {
                this.f1734a = ua4;
                return this;
            }
            throw new NullPointerException("Null threads");
        }
    }

    @DexIgnore
    public ja4(ua4<ta4.d.AbstractC0224d.a.b.e> ua4, ta4.d.AbstractC0224d.a.b.c cVar, ta4.d.AbstractC0224d.a.b.AbstractC0230d dVar, ua4<ta4.d.AbstractC0224d.a.b.AbstractC0226a> ua42) {
        this.f1733a = ua4;
        this.b = cVar;
        this.c = dVar;
        this.d = ua42;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b
    public ua4<ta4.d.AbstractC0224d.a.b.AbstractC0226a> b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b
    public ta4.d.AbstractC0224d.a.b.c c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b
    public ta4.d.AbstractC0224d.a.b.AbstractC0230d d() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b
    public ua4<ta4.d.AbstractC0224d.a.b.e> e() {
        return this.f1733a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.d.AbstractC0224d.a.b)) {
            return false;
        }
        ta4.d.AbstractC0224d.a.b bVar = (ta4.d.AbstractC0224d.a.b) obj;
        return this.f1733a.equals(bVar.e()) && this.b.equals(bVar.c()) && this.c.equals(bVar.d()) && this.d.equals(bVar.b());
    }

    @DexIgnore
    public int hashCode() {
        return ((((((this.f1733a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Execution{threads=" + this.f1733a + ", exception=" + this.b + ", signal=" + this.c + ", binaries=" + this.d + "}";
    }
}
