package com.fossil;

import com.fossil.gz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mz1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(long j);

        @DexIgnore
        public abstract a b(pz1 pz1);

        @DexIgnore
        public abstract a c(Integer num);

        @DexIgnore
        public abstract mz1 d();

        @DexIgnore
        public abstract a e(long j);

        @DexIgnore
        public abstract a f(long j);
    }

    @DexIgnore
    public static a a(String str) {
        gz1.b bVar = new gz1.b();
        bVar.g(str);
        return bVar;
    }

    @DexIgnore
    public static a b(byte[] bArr) {
        gz1.b bVar = new gz1.b();
        bVar.h(bArr);
        return bVar;
    }

    @DexIgnore
    public abstract Integer c();

    @DexIgnore
    public abstract long d();

    @DexIgnore
    public abstract long e();

    @DexIgnore
    public abstract pz1 f();

    @DexIgnore
    public abstract byte[] g();

    @DexIgnore
    public abstract String h();

    @DexIgnore
    public abstract long i();
}
