package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zy4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f4555a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ve5 f4556a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ve5 ve5) {
            super(ve5.n());
            pq7.c(ve5, "binding");
            this.f4556a = ve5;
        }

        @DexIgnore
        public final void a(String str) {
            pq7.c(str, "name");
            FlexibleTextView flexibleTextView = this.f4556a.q;
            pq7.b(flexibleTextView, "binding.ftvTitle");
            flexibleTextView.setText(str);
            String d = qn5.l.a().d("primaryText");
            if (d != null) {
                this.f4556a.q.setBackgroundColor(Color.parseColor(d));
            }
        }
    }

    @DexIgnore
    public zy4(int i) {
        this.f4555a = i;
    }

    @DexIgnore
    public final int a() {
        return this.f4555a;
    }

    @DexIgnore
    public boolean b(List<? extends Object> list, int i) {
        pq7.c(list, "items");
        return list.get(i) instanceof String;
    }

    @DexIgnore
    public void c(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        Object obj = null;
        pq7.c(list, "items");
        pq7.c(viewHolder, "holder");
        a aVar = (a) (!(viewHolder instanceof a) ? null : viewHolder);
        Object obj2 = list.get(i);
        if (obj2 instanceof String) {
            obj = obj2;
        }
        String str = (String) obj;
        if (aVar != null && str != null) {
            aVar.a(str);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder d(ViewGroup viewGroup) {
        pq7.c(viewGroup, "parent");
        ve5 z = ve5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemHeaderMemberBinding.\u2026(inflater, parent, false)");
        return new a(z);
    }
}
