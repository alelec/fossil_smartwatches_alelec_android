package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pz4 {
    @DexIgnore
    public final String a(Date date) {
        String str;
        if (date == null) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = lk5.c.get();
            str = simpleDateFormat != null ? simpleDateFormat.format(date) : null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("DateLongStringConverter", "fromOffsetDateTime - e=" + e);
            str = null;
        }
        return str;
    }

    @DexIgnore
    public final Date b(String str) {
        Date date;
        if (str == null || str.length() == 0) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = lk5.c.get();
            date = simpleDateFormat != null ? simpleDateFormat.parse(str) : null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("DateLongStringConverter", "toOffsetDateTime - e=" + e);
            e.printStackTrace();
            date = null;
        }
        return date;
    }
}
