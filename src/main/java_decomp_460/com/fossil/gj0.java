package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.oj0;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gj0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f1313a; // = 0;
    @DexIgnore
    public /* final */ hj0 b;
    @DexIgnore
    public /* final */ ij0 c;
    @DexIgnore
    public int d; // = 8;
    @DexIgnore
    public oj0 e; // = null;
    @DexIgnore
    public int[] f; // = new int[8];
    @DexIgnore
    public int[] g; // = new int[8];
    @DexIgnore
    public float[] h; // = new float[8];
    @DexIgnore
    public int i; // = -1;
    @DexIgnore
    public int j; // = -1;
    @DexIgnore
    public boolean k; // = false;

    @DexIgnore
    public gj0(hj0 hj0, ij0 ij0) {
        this.b = hj0;
        this.c = ij0;
    }

    @DexIgnore
    public final void a(oj0 oj0, float f2, boolean z) {
        if (f2 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            int i2 = this.i;
            if (i2 == -1) {
                this.i = 0;
                this.h[0] = f2;
                this.f[0] = oj0.b;
                this.g[0] = -1;
                oj0.j++;
                oj0.a(this.b);
                this.f1313a++;
                if (!this.k) {
                    int i3 = this.j + 1;
                    this.j = i3;
                    int[] iArr = this.f;
                    if (i3 >= iArr.length) {
                        this.k = true;
                        this.j = iArr.length - 1;
                        return;
                    }
                    return;
                }
                return;
            }
            int i4 = -1;
            int i5 = 0;
            while (i2 != -1 && i5 < this.f1313a) {
                int[] iArr2 = this.f;
                int i6 = iArr2[i2];
                int i7 = oj0.b;
                if (i6 == i7) {
                    float[] fArr = this.h;
                    fArr[i2] = fArr[i2] + f2;
                    if (fArr[i2] == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        if (i2 == this.i) {
                            this.i = this.g[i2];
                        } else {
                            int[] iArr3 = this.g;
                            iArr3[i4] = iArr3[i2];
                        }
                        if (z) {
                            oj0.c(this.b);
                        }
                        if (this.k) {
                            this.j = i2;
                        }
                        oj0.j--;
                        this.f1313a--;
                        return;
                    }
                    return;
                }
                if (iArr2[i2] < i7) {
                    i4 = i2;
                }
                i2 = this.g[i2];
                i5++;
            }
            int i8 = this.j;
            if (this.k) {
                int[] iArr4 = this.f;
                if (iArr4[i8] != -1) {
                    i8 = iArr4.length;
                }
            } else {
                i8++;
            }
            int[] iArr5 = this.f;
            if (i8 >= iArr5.length && this.f1313a < iArr5.length) {
                int i9 = 0;
                while (true) {
                    int[] iArr6 = this.f;
                    if (i9 >= iArr6.length) {
                        break;
                    } else if (iArr6[i9] == -1) {
                        i8 = i9;
                        break;
                    } else {
                        i9++;
                    }
                }
            }
            int[] iArr7 = this.f;
            if (i8 >= iArr7.length) {
                i8 = iArr7.length;
                int i10 = this.d * 2;
                this.d = i10;
                this.k = false;
                this.j = i8 - 1;
                this.h = Arrays.copyOf(this.h, i10);
                this.f = Arrays.copyOf(this.f, this.d);
                this.g = Arrays.copyOf(this.g, this.d);
            }
            this.f[i8] = oj0.b;
            this.h[i8] = f2;
            if (i4 != -1) {
                int[] iArr8 = this.g;
                iArr8[i8] = iArr8[i4];
                iArr8[i4] = i8;
            } else {
                this.g[i8] = this.i;
                this.i = i8;
            }
            oj0.j++;
            oj0.a(this.b);
            this.f1313a++;
            if (!this.k) {
                this.j++;
            }
            int i11 = this.j;
            int[] iArr9 = this.f;
            if (i11 >= iArr9.length) {
                this.k = true;
                this.j = iArr9.length - 1;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0049 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.oj0 b(com.fossil.kj0 r15) {
        /*
            r14 = this;
            r2 = 0
            r4 = 1
            r0 = 0
            r10 = 0
            int r7 = r14.i
            r1 = r10
            r8 = r10
            r3 = r0
            r5 = r0
            r11 = r0
            r6 = r2
            r9 = r2
            r12 = r7
        L_0x000e:
            r0 = -1
            if (r12 == r0) goto L_0x009b
            int r0 = r14.f1313a
            if (r11 >= r0) goto L_0x009b
            float[] r0 = r14.h
            r2 = r0[r12]
            com.fossil.ij0 r7 = r14.c
            com.fossil.oj0[] r7 = r7.c
            int[] r13 = r14.f
            r13 = r13[r12]
            r7 = r7[r13]
            int r13 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r13 >= 0) goto L_0x0052
            r13 = -1165815185(0xffffffffba83126f, float:-0.001)
            int r13 = (r2 > r13 ? 1 : (r2 == r13 ? 0 : -1))
            if (r13 <= 0) goto L_0x0036
            r0[r12] = r10
            com.fossil.hj0 r0 = r14.b
            r7.c(r0)
        L_0x0035:
            r2 = r10
        L_0x0036:
            int r0 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r0 == 0) goto L_0x0049
            com.fossil.oj0$a r0 = r7.g
            com.fossil.oj0$a r13 = com.fossil.oj0.a.UNRESTRICTED
            if (r0 != r13) goto L_0x0076
            if (r6 != 0) goto L_0x0061
            boolean r0 = r14.k(r7, r15)
        L_0x0046:
            r5 = r0
            r6 = r7
            r8 = r2
        L_0x0049:
            int[] r0 = r14.g
            r2 = r0[r12]
            int r0 = r11 + 1
            r11 = r0
            r12 = r2
            goto L_0x000e
        L_0x0052:
            r13 = 981668463(0x3a83126f, float:0.001)
            int r13 = (r2 > r13 ? 1 : (r2 == r13 ? 0 : -1))
            if (r13 >= 0) goto L_0x0036
            r0[r12] = r10
            com.fossil.hj0 r0 = r14.b
            r7.c(r0)
            goto L_0x0035
        L_0x0061:
            int r0 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x006a
            boolean r0 = r14.k(r7, r15)
            goto L_0x0046
        L_0x006a:
            if (r5 != 0) goto L_0x0049
            boolean r0 = r14.k(r7, r15)
            if (r0 == 0) goto L_0x0049
            r5 = r4
            r6 = r7
            r8 = r2
            goto L_0x0049
        L_0x0076:
            if (r6 != 0) goto L_0x0049
            int r0 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r0 >= 0) goto L_0x0049
            if (r9 != 0) goto L_0x0086
            boolean r0 = r14.k(r7, r15)
        L_0x0082:
            r1 = r2
            r3 = r0
            r9 = r7
            goto L_0x0049
        L_0x0086:
            int r0 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x008f
            boolean r0 = r14.k(r7, r15)
            goto L_0x0082
        L_0x008f:
            if (r3 != 0) goto L_0x0049
            boolean r0 = r14.k(r7, r15)
            if (r0 == 0) goto L_0x0049
            r1 = r2
            r3 = r4
            r9 = r7
            goto L_0x0049
        L_0x009b:
            if (r6 == 0) goto L_0x009e
        L_0x009d:
            return r6
        L_0x009e:
            r6 = r9
            goto L_0x009d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gj0.b(com.fossil.kj0):com.fossil.oj0");
    }

    @DexIgnore
    public final void c() {
        int i2 = this.i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.f1313a) {
            oj0 oj0 = this.c.c[this.f[i2]];
            if (oj0 != null) {
                oj0.c(this.b);
            }
            i2 = this.g[i2];
            i3++;
        }
        this.i = -1;
        this.j = -1;
        this.k = false;
        this.f1313a = 0;
    }

    @DexIgnore
    public final boolean d(oj0 oj0) {
        int i2 = this.i;
        if (i2 == -1) {
            return false;
        }
        int i3 = 0;
        while (i2 != -1 && i3 < this.f1313a) {
            if (this.f[i2] == oj0.b) {
                return true;
            }
            i2 = this.g[i2];
            i3++;
        }
        return false;
    }

    @DexIgnore
    public void e(float f2) {
        int i2 = this.i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.f1313a) {
            float[] fArr = this.h;
            fArr[i2] = fArr[i2] / f2;
            i2 = this.g[i2];
            i3++;
        }
    }

    @DexIgnore
    public final float f(oj0 oj0) {
        int i2 = this.i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.f1313a) {
            if (this.f[i2] == oj0.b) {
                return this.h[i2];
            }
            i2 = this.g[i2];
            i3++;
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public oj0 g(boolean[] zArr, oj0 oj0) {
        oj0.a aVar;
        int i2 = this.i;
        int i3 = 0;
        oj0 oj02 = null;
        float f2 = 0.0f;
        while (i2 != -1 && i3 < this.f1313a) {
            if (this.h[i2] < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                oj0 oj03 = this.c.c[this.f[i2]];
                if ((zArr == null || !zArr[oj03.b]) && oj03 != oj0 && ((aVar = oj03.g) == oj0.a.SLACK || aVar == oj0.a.ERROR)) {
                    float f3 = this.h[i2];
                    if (f3 < f2) {
                        f2 = f3;
                        oj02 = oj03;
                    }
                }
            }
            i2 = this.g[i2];
            i3++;
        }
        return oj02;
    }

    @DexIgnore
    public final oj0 h(int i2) {
        int i3 = this.i;
        int i4 = 0;
        while (i3 != -1 && i4 < this.f1313a) {
            if (i4 == i2) {
                return this.c.c[this.f[i3]];
            }
            i3 = this.g[i3];
            i4++;
        }
        return null;
    }

    @DexIgnore
    public final float i(int i2) {
        int i3 = this.i;
        int i4 = 0;
        while (i3 != -1 && i4 < this.f1313a) {
            if (i4 == i2) {
                return this.h[i3];
            }
            i3 = this.g[i3];
            i4++;
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public void j() {
        int i2 = this.i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.f1313a) {
            float[] fArr = this.h;
            fArr[i2] = fArr[i2] * -1.0f;
            i2 = this.g[i2];
            i3++;
        }
    }

    @DexIgnore
    public final boolean k(oj0 oj0, kj0 kj0) {
        return oj0.j <= 1;
    }

    @DexIgnore
    public final void l(oj0 oj0, float f2) {
        if (f2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            m(oj0, true);
            return;
        }
        int i2 = this.i;
        if (i2 == -1) {
            this.i = 0;
            this.h[0] = f2;
            this.f[0] = oj0.b;
            this.g[0] = -1;
            oj0.j++;
            oj0.a(this.b);
            this.f1313a++;
            if (!this.k) {
                int i3 = this.j + 1;
                this.j = i3;
                int[] iArr = this.f;
                if (i3 >= iArr.length) {
                    this.k = true;
                    this.j = iArr.length - 1;
                    return;
                }
                return;
            }
            return;
        }
        int i4 = -1;
        int i5 = 0;
        while (i2 != -1 && i5 < this.f1313a) {
            int[] iArr2 = this.f;
            int i6 = iArr2[i2];
            int i7 = oj0.b;
            if (i6 == i7) {
                this.h[i2] = f2;
                return;
            }
            if (iArr2[i2] < i7) {
                i4 = i2;
            }
            i2 = this.g[i2];
            i5++;
        }
        int i8 = this.j;
        if (this.k) {
            int[] iArr3 = this.f;
            if (iArr3[i8] != -1) {
                i8 = iArr3.length;
            }
        } else {
            i8++;
        }
        int[] iArr4 = this.f;
        if (i8 >= iArr4.length && this.f1313a < iArr4.length) {
            int i9 = 0;
            while (true) {
                int[] iArr5 = this.f;
                if (i9 >= iArr5.length) {
                    break;
                } else if (iArr5[i9] == -1) {
                    i8 = i9;
                    break;
                } else {
                    i9++;
                }
            }
        }
        int[] iArr6 = this.f;
        if (i8 >= iArr6.length) {
            i8 = iArr6.length;
            int i10 = this.d * 2;
            this.d = i10;
            this.k = false;
            this.j = i8 - 1;
            this.h = Arrays.copyOf(this.h, i10);
            this.f = Arrays.copyOf(this.f, this.d);
            this.g = Arrays.copyOf(this.g, this.d);
        }
        this.f[i8] = oj0.b;
        this.h[i8] = f2;
        if (i4 != -1) {
            int[] iArr7 = this.g;
            iArr7[i8] = iArr7[i4];
            iArr7[i4] = i8;
        } else {
            this.g[i8] = this.i;
            this.i = i8;
        }
        oj0.j++;
        oj0.a(this.b);
        this.f1313a++;
        if (!this.k) {
            this.j++;
        }
        if (this.f1313a >= this.f.length) {
            this.k = true;
        }
        int i11 = this.j;
        int[] iArr8 = this.f;
        if (i11 >= iArr8.length) {
            this.k = true;
            this.j = iArr8.length - 1;
        }
    }

    @DexIgnore
    public final float m(oj0 oj0, boolean z) {
        if (this.e == oj0) {
            this.e = null;
        }
        int i2 = this.i;
        if (i2 == -1) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        int i3 = 0;
        int i4 = -1;
        while (i2 != -1 && i3 < this.f1313a) {
            if (this.f[i2] == oj0.b) {
                if (i2 == this.i) {
                    this.i = this.g[i2];
                } else {
                    int[] iArr = this.g;
                    iArr[i4] = iArr[i2];
                }
                if (z) {
                    oj0.c(this.b);
                }
                oj0.j--;
                this.f1313a--;
                this.f[i2] = -1;
                if (this.k) {
                    this.j = i2;
                }
                return this.h[i2];
            }
            i3++;
            i4 = i2;
            i2 = this.g[i2];
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final void n(hj0 hj0, hj0 hj02, boolean z) {
        int i2 = this.i;
        while (true) {
            int i3 = 0;
            int i4 = i2;
            while (i4 != -1 && i3 < this.f1313a) {
                int i5 = this.f[i4];
                oj0 oj0 = hj02.f1485a;
                if (i5 == oj0.b) {
                    float f2 = this.h[i4];
                    m(oj0, z);
                    gj0 gj0 = hj02.d;
                    int i6 = gj0.i;
                    int i7 = 0;
                    while (i6 != -1 && i7 < gj0.f1313a) {
                        a(this.c.c[gj0.f[i6]], gj0.h[i6] * f2, z);
                        i6 = gj0.g[i6];
                        i7++;
                    }
                    hj0.b += hj02.b * f2;
                    if (z) {
                        hj02.f1485a.c(hj0);
                    }
                    i2 = this.i;
                } else {
                    i4 = this.g[i4];
                    i3++;
                }
            }
            return;
        }
    }

    @DexIgnore
    public void o(hj0 hj0, hj0[] hj0Arr) {
        int i2 = this.i;
        while (true) {
            int i3 = 0;
            int i4 = i2;
            while (i4 != -1 && i3 < this.f1313a) {
                oj0 oj0 = this.c.c[this.f[i4]];
                if (oj0.c != -1) {
                    float f2 = this.h[i4];
                    m(oj0, true);
                    hj0 hj02 = hj0Arr[oj0.c];
                    if (!hj02.e) {
                        gj0 gj0 = hj02.d;
                        int i5 = gj0.i;
                        int i6 = 0;
                        while (i5 != -1 && i6 < gj0.f1313a) {
                            a(this.c.c[gj0.f[i5]], gj0.h[i5] * f2, true);
                            i5 = gj0.g[i5];
                            i6++;
                        }
                    }
                    hj0.b += hj02.b * f2;
                    hj02.f1485a.c(hj0);
                    i2 = this.i;
                } else {
                    i4 = this.g[i4];
                    i3++;
                }
            }
            return;
        }
    }

    @DexIgnore
    public String toString() {
        int i2 = this.i;
        String str = "";
        int i3 = 0;
        while (i2 != -1 && i3 < this.f1313a) {
            str = ((str + " -> ") + this.h[i2] + " : ") + this.c.c[this.f[i2]];
            i2 = this.g[i2];
            i3++;
        }
        return str;
    }
}
