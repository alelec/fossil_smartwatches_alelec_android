package com.fossil;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.UserManager;
import android.util.Log;
import com.facebook.internal.ServerProtocol;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h62 {
    @Deprecated

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ int f1430a; // = 12451000;
    @DexIgnore
    public static boolean b;
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public static /* final */ AtomicBoolean d; // = new AtomicBoolean();
    @DexIgnore
    public static /* final */ AtomicBoolean e; // = new AtomicBoolean();

    @DexIgnore
    @Deprecated
    public static void a(Context context) {
        if (!d.getAndSet(true)) {
            try {
                NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
                if (notificationManager != null) {
                    notificationManager.cancel(10436);
                }
            } catch (SecurityException e2) {
            }
        }
    }

    @DexIgnore
    @Deprecated
    public static int b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.w("GooglePlayServicesUtil", "Google Play services is missing.");
            return 0;
        }
    }

    @DexIgnore
    @Deprecated
    public static String c(int i) {
        return z52.F(i);
    }

    @DexIgnore
    public static Context d(Context context) {
        try {
            return context.createPackageContext("com.google.android.gms", 3);
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }

    @DexIgnore
    public static Resources e(Context context) {
        try {
            return context.getPackageManager().getResourcesForApplication("com.google.android.gms");
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }

    @DexIgnore
    public static boolean f(Context context) {
        if (!c) {
            try {
                PackageInfo e2 = ag2.a(context).e("com.google.android.gms", 64);
                i62.a(context);
                if (e2 == null || i62.f(e2, false) || !i62.f(e2, true)) {
                    b = false;
                } else {
                    b = true;
                }
            } catch (PackageManager.NameNotFoundException e3) {
                Log.w("GooglePlayServicesUtil", "Cannot find Google Play services package name.", e3);
            } finally {
                c = true;
            }
        }
        return b || !if2.a();
    }

    @DexIgnore
    @Deprecated
    public static int g(Context context) {
        return h(context, f1430a);
    }

    @DexIgnore
    @Deprecated
    public static int h(Context context, int i) {
        try {
            context.getResources().getString(j62.common_google_play_services_unknown_issue);
        } catch (Throwable th) {
            Log.e("GooglePlayServicesUtil", "The Google Play services resources were not found. Check your project configuration to ensure that the resources are included.");
        }
        if (!"com.google.android.gms".equals(context.getPackageName()) && !e.get()) {
            int b2 = qe2.b(context);
            if (b2 == 0) {
                throw new IllegalStateException("A required meta-data tag in your app's AndroidManifest.xml does not exist.  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />");
            } else if (b2 != f1430a) {
                int i2 = f1430a;
                StringBuilder sb = new StringBuilder(320);
                sb.append("The meta-data tag in your app's AndroidManifest.xml does not have the right value.  Expected ");
                sb.append(i2);
                sb.append(" but found ");
                sb.append(b2);
                sb.append(".  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />");
                throw new IllegalStateException(sb.toString());
            }
        }
        return o(context, !if2.d(context) && !if2.f(context), i);
    }

    @DexIgnore
    @Deprecated
    public static boolean i(Context context, int i) {
        return pf2.a(context, i);
    }

    @DexIgnore
    @Deprecated
    public static boolean j(Context context, int i) {
        if (i == 18) {
            return true;
        }
        if (i == 1) {
            return l(context, "com.google.android.gms");
        }
        return false;
    }

    @DexIgnore
    @TargetApi(18)
    public static boolean k(Context context) {
        Bundle applicationRestrictions;
        return mf2.e() && (applicationRestrictions = ((UserManager) context.getSystemService("user")).getApplicationRestrictions(context.getPackageName())) != null && ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equals(applicationRestrictions.getString("restricted_profile"));
    }

    @DexIgnore
    @TargetApi(21)
    public static boolean l(Context context, String str) {
        boolean equals = str.equals("com.google.android.gms");
        if (mf2.h()) {
            try {
                for (PackageInstaller.SessionInfo sessionInfo : context.getPackageManager().getPackageInstaller().getAllSessions()) {
                    if (str.equals(sessionInfo.getAppPackageName())) {
                        return true;
                    }
                }
            } catch (Exception e2) {
                return false;
            }
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(str, 8192);
            if (equals) {
                return applicationInfo.enabled;
            }
            if (applicationInfo.enabled && !k(context)) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e3) {
        }
    }

    @DexIgnore
    @Deprecated
    public static boolean m(int i) {
        return i == 1 || i == 2 || i == 3 || i == 9;
    }

    @DexIgnore
    @TargetApi(19)
    @Deprecated
    public static boolean n(Context context, int i, String str) {
        return pf2.b(context, i, str);
    }

    @DexIgnore
    public static int o(Context context, boolean z, int i) {
        rc2.a(i >= 0);
        String packageName = context.getPackageName();
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo = null;
        if (z) {
            try {
                packageInfo = packageManager.getPackageInfo("com.android.vending", 8256);
            } catch (PackageManager.NameNotFoundException e2) {
                Log.w("GooglePlayServicesUtil", String.valueOf(packageName).concat(" requires the Google Play Store, but it is missing."));
                return 9;
            }
        }
        try {
            PackageInfo packageInfo2 = packageManager.getPackageInfo("com.google.android.gms", 64);
            i62.a(context);
            if (!i62.f(packageInfo2, true)) {
                Log.w("GooglePlayServicesUtil", String.valueOf(packageName).concat(" requires Google Play services, but their signature is invalid."));
                return 9;
            } else if (z && (!i62.f(packageInfo, true) || !packageInfo.signatures[0].equals(packageInfo2.signatures[0]))) {
                Log.w("GooglePlayServicesUtil", String.valueOf(packageName).concat(" requires Google Play Store, but its signature is invalid."));
                return 9;
            } else if (wf2.a(packageInfo2.versionCode) < wf2.a(i)) {
                int i2 = packageInfo2.versionCode;
                StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 82);
                sb.append("Google Play services out of date for ");
                sb.append(packageName);
                sb.append(".  Requires ");
                sb.append(i);
                sb.append(" but found ");
                sb.append(i2);
                Log.w("GooglePlayServicesUtil", sb.toString());
                return 2;
            } else {
                ApplicationInfo applicationInfo = packageInfo2.applicationInfo;
                if (applicationInfo == null) {
                    try {
                        applicationInfo = packageManager.getApplicationInfo("com.google.android.gms", 0);
                    } catch (PackageManager.NameNotFoundException e3) {
                        Log.wtf("GooglePlayServicesUtil", String.valueOf(packageName).concat(" requires Google Play services, but they're missing when getting application info."), e3);
                        return 1;
                    }
                }
                return !applicationInfo.enabled ? 3 : 0;
            }
        } catch (PackageManager.NameNotFoundException e4) {
            Log.w("GooglePlayServicesUtil", String.valueOf(packageName).concat(" requires Google Play services, but they are missing."));
            return 1;
        }
    }
}
