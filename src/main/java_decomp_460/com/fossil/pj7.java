package com.fossil;

import com.zendesk.service.ErrorResponse;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pj7 implements ErrorResponse {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Throwable f2838a;
    @DexIgnore
    public q88 b;

    @DexIgnore
    public pj7(q88 q88) {
        this.b = q88;
    }

    @DexIgnore
    public pj7(Throwable th) {
        this.f2838a = th;
    }

    @DexIgnore
    public static pj7 d(q88 q88) {
        return new pj7(q88);
    }

    @DexIgnore
    public static pj7 e(Throwable th) {
        return new pj7(th);
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public int a() {
        q88 q88 = this.b;
        if (q88 != null) {
            return q88.b();
        }
        return -1;
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public String b() {
        Throwable th = this.f2838a;
        if (th != null) {
            return th.getMessage();
        }
        StringBuilder sb = new StringBuilder();
        q88 q88 = this.b;
        if (q88 != null) {
            if (tj7.b(q88.f())) {
                sb.append(this.b.f());
            } else {
                sb.append(this.b.b());
            }
        }
        return sb.toString();
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public boolean c() {
        Throwable th = this.f2838a;
        return th != null && (th instanceof IOException);
    }
}
