package com.fossil;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ec3 extends IInterface {
    @DexIgnore
    void I(ad3 ad3) throws RemoteException;

    @DexIgnore
    rg2 getView() throws RemoteException;

    @DexIgnore
    void onCreate(Bundle bundle) throws RemoteException;

    @DexIgnore
    void onDestroy() throws RemoteException;

    @DexIgnore
    void onLowMemory() throws RemoteException;

    @DexIgnore
    void onPause() throws RemoteException;

    @DexIgnore
    void onResume() throws RemoteException;

    @DexIgnore
    void onSaveInstanceState(Bundle bundle) throws RemoteException;

    @DexIgnore
    void onStart() throws RemoteException;

    @DexIgnore
    void onStop() throws RemoteException;
}
