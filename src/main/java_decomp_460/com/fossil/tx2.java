package com.fossil;

import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.Collection;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tx2<E> extends AbstractCollection<E> implements Serializable {
    @DexIgnore
    public static /* final */ Object[] b; // = new Object[0];

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @Deprecated
    public final boolean add(E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @Deprecated
    public final boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract boolean contains(@NullableDecl Object obj);

    @DexIgnore
    @Deprecated
    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @Deprecated
    public final boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @Deprecated
    public final boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final Object[] toArray() {
        return toArray(b);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v3, types: [java.lang.Object[]] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // java.util.AbstractCollection, java.util.Collection
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T> T[] toArray(T[] r5) {
        /*
            r4 = this;
            com.fossil.sw2.b(r5)
            int r0 = r4.size()
            int r1 = r5.length
            if (r1 >= r0) goto L_0x0035
            java.lang.Object[] r1 = r4.zze()
            if (r1 == 0) goto L_0x0021
            int r0 = r4.zzf()
            int r2 = r4.zzg()
            java.lang.Class r3 = r5.getClass()
            java.lang.Object[] r5 = java.util.Arrays.copyOfRange(r1, r0, r2, r3)
        L_0x0020:
            return r5
        L_0x0021:
            java.lang.Class r1 = r5.getClass()
            java.lang.Class r1 = r1.getComponentType()
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r1, r0)
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            r5 = r0
        L_0x0030:
            r0 = 0
            r4.zzb(r5, r0)
            goto L_0x0020
        L_0x0035:
            int r1 = r5.length
            if (r1 <= r0) goto L_0x0030
            r1 = 0
            r5[r0] = r1
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tx2.toArray(java.lang.Object[]):java.lang.Object[]");
    }

    @DexIgnore
    public int zzb(Object[] objArr, int i) {
        cz2 cz2 = (cz2) iterator();
        while (cz2.hasNext()) {
            objArr[i] = cz2.next();
            i++;
        }
        return i;
    }

    @DexIgnore
    /* renamed from: zzb */
    public abstract cz2<E> iterator();

    @DexIgnore
    public sx2<E> zzc() {
        return isEmpty() ? sx2.zza() : sx2.zza(toArray());
    }

    @DexIgnore
    @NullableDecl
    public Object[] zze() {
        return null;
    }

    @DexIgnore
    public int zzf() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public int zzg() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract boolean zzh();
}
