package com.fossil;

import android.content.Context;
import android.util.Log;
import com.fossil.o71;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n71 implements o71.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ o71 f2477a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public n71(Context context) {
        pq7.c(context, "context");
        o71 a2 = o71.f2640a.a(context, this);
        this.f2477a = a2;
        this.b = a2.a();
        c();
        this.f2477a.start();
    }

    @DexIgnore
    @Override // com.fossil.o71.b
    public void a(boolean z) {
        this.b = z;
        c();
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public final void c() {
        if (q81.c.a() && q81.c.b() <= 4) {
            Log.println(4, "NetworkObserver", this.b ? "ONLINE" : "OFFLINE");
        }
    }

    @DexIgnore
    public final void d() {
        if (!this.c) {
            this.c = true;
            this.f2477a.stop();
        }
    }
}
