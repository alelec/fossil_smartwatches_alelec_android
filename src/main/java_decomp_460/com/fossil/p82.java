package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p82 extends f92 {
    @DexIgnore
    public /* final */ /* synthetic */ h82 b;
    @DexIgnore
    public /* final */ /* synthetic */ us3 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p82(q82 q82, d92 d92, h82 h82, us3 us3) {
        super(d92);
        this.b = h82;
        this.c = us3;
    }

    @DexIgnore
    @Override // com.fossil.f92
    public final void a() {
        this.b.m(this.c);
    }
}
