package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eu7 {
    @DexIgnore
    public static final <T> rv7<T> a(iv7 iv7, tn7 tn7, lv7 lv7, vp7<? super iv7, ? super qn7<? super T>, ? extends Object> vp7) {
        return gu7.a(iv7, tn7, lv7, vp7);
    }

    @DexIgnore
    public static final xw7 c(iv7 iv7, tn7 tn7, lv7 lv7, vp7<? super iv7, ? super qn7<? super tl7>, ? extends Object> vp7) {
        return gu7.c(iv7, tn7, lv7, vp7);
    }

    @DexIgnore
    public static final <T> T e(tn7 tn7, vp7<? super iv7, ? super qn7<? super T>, ? extends Object> vp7) throws InterruptedException {
        return (T) fu7.a(tn7, vp7);
    }

    @DexIgnore
    public static final <T> Object g(tn7 tn7, vp7<? super iv7, ? super qn7<? super T>, ? extends Object> vp7, qn7<? super T> qn7) {
        return gu7.e(tn7, vp7, qn7);
    }
}
