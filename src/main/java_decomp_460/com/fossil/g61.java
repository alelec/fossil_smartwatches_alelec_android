package com.fossil;

import android.webkit.MimeTypeMap;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.a18;
import com.fossil.e61;
import com.fossil.z08;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class g61<T> implements e61<T> {
    @DexIgnore
    public static /* final */ z08 b;
    @DexIgnore
    public static /* final */ z08 c;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ a18.a f1265a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "coil.fetch.HttpFetcher", f = "HttpFetcher.kt", l = {106}, m = "fetch$suspendImpl")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ g61 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(g61 g61, qn7 qn7) {
            super(qn7);
            this.this$0 = g61;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return g61.d(this.this$0, null, null, null, null, this);
        }
    }

    /*
    static {
        z08.a aVar = new z08.a();
        aVar.c();
        aVar.d();
        b = aVar.a();
        z08.a aVar2 = new z08.a();
        aVar2.c();
        aVar2.e();
        c = aVar2.a();
    }
    */

    @DexIgnore
    public g61(a18.a aVar) {
        pq7.c(aVar, "callFactory");
        this.f1265a = aVar;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0130  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ java.lang.Object d(com.fossil.g61 r9, com.fossil.g51 r10, java.lang.Object r11, com.fossil.f81 r12, com.fossil.x51 r13, com.fossil.qn7 r14) {
        /*
        // Method dump skipped, instructions count: 314
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.g61.d(com.fossil.g61, com.fossil.g51, java.lang.Object, com.fossil.f81, com.fossil.x51, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.e61
    public boolean a(T t) {
        pq7.c(t, "data");
        return e61.a.a(this, t);
    }

    @DexIgnore
    @Override // com.fossil.e61
    public Object c(g51 g51, T t, f81 f81, x51 x51, qn7<? super d61> qn7) {
        return d(this, g51, t, f81, x51, qn7);
    }

    @DexIgnore
    public final String e(q18 q18, w18 w18) {
        r18 contentType = w18.contentType();
        String r18 = contentType != null ? contentType.toString() : null;
        if (r18 != null && !vt7.s(r18, "text/plain", false, 2, null)) {
            return r18;
        }
        MimeTypeMap singleton = MimeTypeMap.getSingleton();
        pq7.b(singleton, "MimeTypeMap.getSingleton()");
        String g = w81.g(singleton, q18.toString());
        return g != null ? g : r18;
    }

    @DexIgnore
    public abstract q18 f(T t);
}
