package com.fossil;

import com.fossil.dl7;
import com.fossil.ku7;
import com.fossil.lz7;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v08 implements u08 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ AtomicReferenceFieldUpdater f3693a; // = AtomicReferenceFieldUpdater.newUpdater(v08.class, Object.class, "_state");
    @DexIgnore
    public volatile Object _state;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends b {
        @DexIgnore
        public /* final */ ku7<tl7> f;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.ku7<? super com.fossil.tl7> */
        /* JADX WARN: Multi-variable type inference failed */
        public a(Object obj, ku7<? super tl7> ku7) {
            super(obj);
            this.f = ku7;
        }

        @DexIgnore
        @Override // com.fossil.lz7
        public String toString() {
            return "LockCont[" + this.e + ", " + this.f + ']';
        }

        @DexIgnore
        @Override // com.fossil.v08.b
        public void w(Object obj) {
            this.f.g(obj);
        }

        @DexIgnore
        @Override // com.fossil.v08.b
        public Object x() {
            return ku7.a.a(this.f, tl7.f3441a, null, 2, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b extends lz7 implements dw7 {
        @DexIgnore
        public /* final */ Object e;

        @DexIgnore
        public b(Object obj) {
            this.e = obj;
        }

        @DexIgnore
        @Override // com.fossil.dw7
        public final void dispose() {
            r();
        }

        @DexIgnore
        public abstract void w(Object obj);

        @DexIgnore
        public abstract Object x();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends jz7 {
        @DexIgnore
        public Object e;

        @DexIgnore
        public c(Object obj) {
            this.e = obj;
        }

        @DexIgnore
        @Override // com.fossil.lz7
        public String toString() {
            return "LockedQueue[" + this.e + ']';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends rz7 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ c f3694a;

        @DexIgnore
        public d(c cVar) {
            this.f3694a = cVar;
        }

        @DexIgnore
        @Override // com.fossil.rz7
        public cz7<?> a() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.rz7
        public Object c(Object obj) {
            Object obj2 = this.f3694a.w() ? w08.e : this.f3694a;
            if (obj != null) {
                v08 v08 = (v08) obj;
                v08.f3693a.compareAndSet(v08, this, obj2);
                if (v08._state == this.f3694a) {
                    return w08.f3864a;
                }
                return null;
            }
            throw new il7("null cannot be cast to non-null type kotlinx.coroutines.sync.MutexImpl");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends lz7.b {
        @DexIgnore
        public /* final */ /* synthetic */ Object d;
        @DexIgnore
        public /* final */ /* synthetic */ v08 e;
        @DexIgnore
        public /* final */ /* synthetic */ Object f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(lz7 lz7, lz7 lz72, Object obj, ku7 ku7, a aVar, v08 v08, Object obj2) {
            super(lz72);
            this.d = obj;
            this.e = v08;
            this.f = obj2;
        }

        @DexIgnore
        /* renamed from: i */
        public Object g(lz7 lz7) {
            if (this.e._state == this.d) {
                return null;
            }
            return kz7.a();
        }
    }

    @DexIgnore
    public v08(boolean z) {
        this._state = z ? w08.d : w08.e;
    }

    @DexIgnore
    @Override // com.fossil.u08
    public Object a(Object obj, qn7<? super tl7> qn7) {
        if (d(obj)) {
            return tl7.f3441a;
        }
        Object c2 = c(obj, qn7);
        return c2 != yn7.d() ? tl7.f3441a : c2;
    }

    @DexIgnore
    @Override // com.fossil.u08
    public void b(Object obj) {
        while (true) {
            Object obj2 = this._state;
            if (obj2 instanceof t08) {
                if (obj == null) {
                    if (!(((t08) obj2).f3347a != w08.c)) {
                        throw new IllegalStateException("Mutex is not locked".toString());
                    }
                } else {
                    t08 t08 = (t08) obj2;
                    if (!(t08.f3347a == obj)) {
                        throw new IllegalStateException(("Mutex is locked by " + t08.f3347a + " but expected " + obj).toString());
                    }
                }
                if (f3693a.compareAndSet(this, obj2, w08.e)) {
                    return;
                }
            } else if (obj2 instanceof rz7) {
                ((rz7) obj2).c(this);
            } else if (obj2 instanceof c) {
                if (obj != null) {
                    c cVar = (c) obj2;
                    if (!(cVar.e == obj)) {
                        throw new IllegalStateException(("Mutex is locked by " + cVar.e + " but expected " + obj).toString());
                    }
                }
                c cVar2 = (c) obj2;
                lz7 s = cVar2.s();
                if (s == null) {
                    d dVar = new d(cVar2);
                    if (f3693a.compareAndSet(this, obj2, dVar) && dVar.c(this) == null) {
                        return;
                    }
                } else {
                    b bVar = (b) s;
                    Object x = bVar.x();
                    if (x != null) {
                        Object obj3 = bVar.e;
                        if (obj3 == null) {
                            obj3 = w08.b;
                        }
                        cVar2.e = obj3;
                        bVar.w(x);
                        return;
                    }
                }
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
    }

    @DexIgnore
    public final /* synthetic */ Object c(Object obj, qn7<? super tl7> qn7) {
        boolean z;
        lu7 b2 = nu7.b(xn7.c(qn7));
        a aVar = new a(obj, b2);
        while (true) {
            Object obj2 = this._state;
            if (obj2 instanceof t08) {
                t08 t08 = (t08) obj2;
                if (t08.f3347a != w08.c) {
                    f3693a.compareAndSet(this, obj2, new c(t08.f3347a));
                } else {
                    if (f3693a.compareAndSet(this, obj2, obj == null ? w08.d : new t08(obj))) {
                        tl7 tl7 = tl7.f3441a;
                        dl7.a aVar2 = dl7.Companion;
                        b2.resumeWith(dl7.m1constructorimpl(tl7));
                        break;
                    }
                }
            } else if (obj2 instanceof c) {
                c cVar = (c) obj2;
                if (cVar.e != obj) {
                    e eVar = new e(aVar, aVar, obj2, b2, aVar, this, obj);
                    while (true) {
                        int v = cVar.n().v(aVar, cVar, eVar);
                        if (v != 1) {
                            if (v == 2) {
                                z = false;
                                break;
                            }
                        } else {
                            z = true;
                            break;
                        }
                    }
                    if (z) {
                        nu7.c(b2, aVar);
                        break;
                    }
                } else {
                    throw new IllegalStateException(("Already locked by " + obj).toString());
                }
            } else if (obj2 instanceof rz7) {
                ((rz7) obj2).c(this);
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
        Object t = b2.t();
        if (t == yn7.d()) {
            go7.c(qn7);
        }
        return t;
    }

    @DexIgnore
    public boolean d(Object obj) {
        boolean z = true;
        while (true) {
            Object obj2 = this._state;
            if (obj2 instanceof t08) {
                if (((t08) obj2).f3347a != w08.c) {
                    return false;
                }
                if (f3693a.compareAndSet(this, obj2, obj == null ? w08.d : new t08(obj))) {
                    return true;
                }
            } else if (obj2 instanceof c) {
                if (((c) obj2).e == obj) {
                    z = false;
                }
                if (z) {
                    return false;
                }
                throw new IllegalStateException(("Already locked by " + obj).toString());
            } else if (obj2 instanceof rz7) {
                ((rz7) obj2).c(this);
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
    }

    @DexIgnore
    public String toString() {
        while (true) {
            Object obj = this._state;
            if (obj instanceof t08) {
                return "Mutex[" + ((t08) obj).f3347a + ']';
            } else if (obj instanceof rz7) {
                ((rz7) obj).c(this);
            } else if (obj instanceof c) {
                return "Mutex[" + ((c) obj).e + ']';
            } else {
                throw new IllegalStateException(("Illegal state " + obj).toString());
            }
        }
    }
}
