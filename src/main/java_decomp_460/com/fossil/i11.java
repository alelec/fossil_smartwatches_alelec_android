package com.fossil;

import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class i11 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f1570a; // = x01.f("WorkerFactory");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends i11 {
        @DexIgnore
        @Override // com.fossil.i11
        public ListenableWorker a(Context context, String str, WorkerParameters workerParameters) {
            return null;
        }
    }

    @DexIgnore
    public static i11 c() {
        return new a();
    }

    @DexIgnore
    public abstract ListenableWorker a(Context context, String str, WorkerParameters workerParameters);

    @DexIgnore
    public final ListenableWorker b(Context context, String str, WorkerParameters workerParameters) {
        ListenableWorker listenableWorker;
        Class<? extends U> cls;
        ListenableWorker a2 = a(context, str, workerParameters);
        if (a2 == null) {
            try {
                cls = Class.forName(str).asSubclass(ListenableWorker.class);
            } catch (Throwable th) {
                x01 c = x01.c();
                String str2 = f1570a;
                c.b(str2, "Invalid class: " + str, th);
                cls = null;
            }
            if (cls != null) {
                try {
                    listenableWorker = (ListenableWorker) cls.getDeclaredConstructor(Context.class, WorkerParameters.class).newInstance(context, workerParameters);
                } catch (Throwable th2) {
                    x01 c2 = x01.c();
                    String str3 = f1570a;
                    c2.b(str3, "Could not instantiate " + str, th2);
                    listenableWorker = a2;
                }
                if (listenableWorker != null || !listenableWorker.j()) {
                    return listenableWorker;
                }
                throw new IllegalStateException(String.format("WorkerFactory (%s) returned an instance of a ListenableWorker (%s) which has already been invoked. createWorker() must always return a new instance of a ListenableWorker.", getClass().getName(), str));
            }
        }
        listenableWorker = a2;
        if (listenableWorker != null) {
        }
        return listenableWorker;
    }
}
