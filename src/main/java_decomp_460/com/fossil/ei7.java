package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ResolveInfo;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.os.Environment;
import android.os.Process;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.facebook.appevents.UserDataStore;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.facebook.internal.Utility;
import com.facebook.places.model.PlaceFields;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.zip.GZIPInputStream;
import org.apache.http.HttpHost;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ei7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static String f946a; // = null;
    @DexIgnore
    public static String b; // = null;
    @DexIgnore
    public static String c; // = null;
    @DexIgnore
    public static Random d; // = null;
    @DexIgnore
    public static DisplayMetrics e; // = null;
    @DexIgnore
    public static String f; // = null;
    @DexIgnore
    public static String g; // = "";
    @DexIgnore
    public static String h; // = "";
    @DexIgnore
    public static int i; // = -1;
    @DexIgnore
    public static th7 j; // = null;
    @DexIgnore
    public static String k; // = null;
    @DexIgnore
    public static String l; // = null;
    @DexIgnore
    public static volatile int m; // = -1;
    @DexIgnore
    public static String n; // = null;
    @DexIgnore
    public static long o; // = -1;
    @DexIgnore
    public static String p; // = "";
    @DexIgnore
    public static String q; // = "__MTA_FIRST_ACTIVATE__";
    @DexIgnore
    public static int r; // = -1;
    @DexIgnore
    public static long s; // = -1;
    @DexIgnore
    public static int t;

    @DexIgnore
    public static String A(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                Object obj = applicationInfo.metaData.get("InstallChannel");
                if (obj != null) {
                    return obj.toString();
                }
                j.l("Could not read InstallChannel meta-data from AndroidManifest.xml");
            }
        } catch (Throwable th) {
            j.d("Could not read InstallChannel meta-data from AndroidManifest.xml");
        }
        return null;
    }

    @DexIgnore
    public static Random B() {
        Random random;
        synchronized (ei7.class) {
            try {
                if (d == null) {
                    d = new Random();
                }
                random = d;
            } catch (Throwable th) {
                throw th;
            }
        }
        return random;
    }

    @DexIgnore
    public static long C() {
        long j2 = o;
        if (j2 <= 0) {
            j2 = 1;
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader("/proc/meminfo"), 8192);
                j2 = (long) (Integer.valueOf(bufferedReader.readLine().split("\\s+")[1]).intValue() * 1024);
                bufferedReader.close();
            } catch (Exception e2) {
            }
            o = j2;
        }
        return j2;
    }

    @DexIgnore
    public static String D(Context context) {
        if (context == null) {
            return null;
        }
        return context.getClass().getName();
    }

    @DexIgnore
    public static String E(Context context) {
        TelephonyManager telephonyManager;
        String str = f;
        if (str != null) {
            return str;
        }
        try {
            if (!ji7.e(context, "android.permission.READ_PHONE_STATE")) {
                j.d("Could not get permission of android.permission.READ_PHONE_STATE");
            } else if (G(context) && (telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE)) != null) {
                f = telephonyManager.getSimOperator();
            }
        } catch (Throwable th) {
            j.e(th);
        }
        return f;
    }

    @DexIgnore
    public static String F(Context context) {
        if (t(g)) {
            return g;
        }
        try {
            String str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            g = str;
            if (str == null) {
                return "";
            }
        } catch (Throwable th) {
            j.e(th);
        }
        return g;
    }

    @DexIgnore
    public static boolean G(Context context) {
        return context.getPackageManager().checkPermission("android.permission.READ_PHONE_STATE", context.getPackageName()) == 0;
    }

    @DexIgnore
    public static String H(Context context) {
        try {
            if (!ji7.e(context, "android.permission.INTERNET") || !ji7.e(context, "android.permission.ACCESS_NETWORK_STATE")) {
                j.d("can not get the permission of android.permission.ACCESS_WIFI_STATE");
                return "";
            }
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                String typeName = activeNetworkInfo.getTypeName();
                String extraInfo = activeNetworkInfo.getExtraInfo();
                if (typeName != null) {
                    if (typeName.equalsIgnoreCase("WIFI")) {
                        return "WIFI";
                    }
                    if (typeName.equalsIgnoreCase("MOBILE")) {
                        if (extraInfo == null) {
                            return "MOBILE";
                        }
                    } else if (extraInfo == null) {
                        return typeName;
                    }
                    return extraInfo;
                }
            }
            return "";
        } catch (Throwable th) {
            j.e(th);
        }
    }

    @DexIgnore
    public static Integer I(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
            if (telephonyManager != null) {
                return Integer.valueOf(telephonyManager.getNetworkType());
            }
        } catch (Throwable th) {
        }
        return null;
    }

    @DexIgnore
    public static String J(Context context) {
        if (t(h)) {
            return h;
        }
        try {
            String str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            h = str;
            if (str == null || str.length() == 0) {
                return "unknown";
            }
        } catch (Throwable th) {
            j.e(th);
        }
        return h;
    }

    @DexIgnore
    public static int K(Context context) {
        int i2 = i;
        if (i2 != -1) {
            return i2;
        }
        try {
            if (hi7.a()) {
                i = 1;
            }
        } catch (Throwable th) {
            j.e(th);
        }
        i = 0;
        return 0;
    }

    @DexIgnore
    public static String L(Context context) {
        String path;
        if (t(k)) {
            return k;
        }
        try {
            if (ji7.e(context, "android.permission.WRITE_EXTERNAL_STORAGE")) {
                String externalStorageState = Environment.getExternalStorageState();
                if (!(externalStorageState == null || !externalStorageState.equals("mounted") || (path = Environment.getExternalStorageDirectory().getPath()) == null)) {
                    StatFs statFs = new StatFs(path);
                    long availableBlocks = (long) statFs.getAvailableBlocks();
                    String str = String.valueOf((((long) statFs.getBlockSize()) * availableBlocks) / 1000000) + "/" + String.valueOf((((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())) / 1000000);
                    k = str;
                    return str;
                }
                return null;
            }
            j.m("can not get the permission of android.permission.WRITE_EXTERNAL_STORAGE");
            return null;
        } catch (Throwable th) {
            j.e(th);
        }
    }

    @DexIgnore
    public static String M(Context context) {
        try {
            if (l != null) {
                return l;
            }
            int myPid = Process.myPid();
            Iterator<ActivityManager.RunningAppProcessInfo> it = ((ActivityManager) context.getSystemService(Constants.ACTIVITY)).getRunningAppProcesses().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ActivityManager.RunningAppProcessInfo next = it.next();
                if (next.pid == myPid) {
                    l = next.processName;
                    break;
                }
            }
            return l;
        } catch (Throwable th) {
        }
    }

    @DexIgnore
    public static String N(Context context) {
        return i(context, sh7.f3263a);
    }

    @DexIgnore
    public static Integer O(Context context) {
        int i2;
        int i3 = 0;
        synchronized (ei7.class) {
            try {
                if (m <= 0) {
                    m = ii7.a(context, "MTA_EVENT_INDEX", 0);
                    ii7.e(context, "MTA_EVENT_INDEX", m + 1000);
                } else if (m % 1000 == 0) {
                    try {
                        int i4 = m;
                        if (m < 2147383647) {
                            i3 = i4 + 1000;
                        }
                        ii7.e(context, "MTA_EVENT_INDEX", i3);
                    } catch (Throwable th) {
                        j.l(th);
                    }
                }
                i2 = m + 1;
                m = i2;
            } catch (Throwable th2) {
                throw th2;
            }
        }
        return Integer.valueOf(i2);
    }

    @DexIgnore
    public static String P(Context context) {
        try {
            return String.valueOf(c(context) / 1000000) + "/" + String.valueOf(C() / 1000000);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public static JSONObject Q(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("n", fi7.a());
            String d2 = fi7.d();
            if (d2 != null && d2.length() > 0) {
                jSONObject.put("na", d2);
            }
            int b2 = fi7.b();
            if (b2 > 0) {
                jSONObject.put("fx", b2 / 1000000);
            }
            int c2 = fi7.c();
            if (c2 > 0) {
                jSONObject.put(UserDataStore.FIRST_NAME, c2 / 1000000);
            }
        } catch (Throwable th) {
            Log.w("MtaSDK", "get cpu error", th);
        }
        return jSONObject;
    }

    @DexIgnore
    public static String R(Context context) {
        List<Sensor> sensorList;
        if (t(p)) {
            return p;
        }
        try {
            SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
            if (!(sensorManager == null || (sensorList = sensorManager.getSensorList(-1)) == null)) {
                StringBuilder sb = new StringBuilder(sensorList.size() * 10);
                for (int i2 = 0; i2 < sensorList.size(); i2++) {
                    sb.append(sensorList.get(i2).getType());
                    if (i2 != sensorList.size() - 1) {
                        sb.append(",");
                    }
                }
                p = sb.toString();
            }
        } catch (Throwable th) {
            j.e(th);
        }
        return p;
    }

    @DexIgnore
    public static int S(Context context) {
        int i2;
        synchronized (ei7.class) {
            try {
                if (r != -1) {
                    i2 = r;
                } else {
                    T(context);
                    i2 = r;
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return i2;
    }

    @DexIgnore
    public static void T(Context context) {
        int a2 = ii7.a(context, q, 1);
        r = a2;
        if (a2 == 1) {
            ii7.e(context, q, 0);
        }
    }

    @DexIgnore
    public static boolean U(Context context) {
        if (s < 0) {
            s = ii7.b(context, "mta.qq.com.checktime", 0);
        }
        return Math.abs(System.currentTimeMillis() - s) > LogBuilder.MAX_INTERVAL;
    }

    @DexIgnore
    public static void V(Context context) {
        long currentTimeMillis = System.currentTimeMillis();
        s = currentTimeMillis;
        ii7.f(context, "mta.qq.com.checktime", currentTimeMillis);
    }

    @DexIgnore
    public static int a(Context context) {
        return ii7.a(context, "mta.qq.com.difftime", 0);
    }

    @DexIgnore
    public static String b(Context context) {
        if (context == null) {
            return null;
        }
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 0);
        ActivityInfo activityInfo = resolveActivity.activityInfo;
        if (activityInfo == null || activityInfo.packageName.equals("android")) {
            return null;
        }
        return resolveActivity.activityInfo.packageName;
    }

    @DexIgnore
    public static long c(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) context.getSystemService(Constants.ACTIVITY)).getMemoryInfo(memoryInfo);
        return memoryInfo.availMem;
    }

    @DexIgnore
    public static int d() {
        return B().nextInt(Integer.MAX_VALUE);
    }

    @DexIgnore
    public static int e(Context context, boolean z) {
        if (z) {
            t = a(context);
        }
        return t;
    }

    @DexIgnore
    public static Long f(String str, String str2, int i2, int i3, Long l2) {
        if (str == null || str2 == null) {
            return l2;
        }
        if (str2.equalsIgnoreCase(CodelessMatcher.CURRENT_CLASS_NAME) || str2.equalsIgnoreCase("|")) {
            str2 = "\\" + str2;
        }
        String[] split = str.split(str2);
        if (split.length != i3) {
            return l2;
        }
        try {
            Long l3 = 0L;
            for (String str3 : split) {
                l3 = Long.valueOf(((long) i2) * (l3.longValue() + Long.valueOf(str3).longValue()));
            }
            return l3;
        } catch (NumberFormatException e2) {
            return l2;
        }
    }

    @DexIgnore
    public static String g(int i2) {
        Calendar instance = Calendar.getInstance();
        instance.roll(6, i2);
        return new SimpleDateFormat("yyyyMMdd").format(instance.getTime());
    }

    @DexIgnore
    public static String h(long j2) {
        return new SimpleDateFormat("yyyyMMdd").format(new Date(j2));
    }

    @DexIgnore
    public static String i(Context context, String str) {
        if (!fg7.L()) {
            return str;
        }
        if (l == null) {
            l = M(context);
        }
        if (l == null) {
            return str;
        }
        return str + LocaleConverter.LOCALE_DELIMITER + l;
    }

    @DexIgnore
    public static String j(String str) {
        if (str == null) {
            return "0";
        }
        try {
            MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                int i2 = b2 & 255;
                if (i2 < 16) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(Integer.toHexString(i2));
            }
            return stringBuffer.toString();
        } catch (Throwable th) {
            return "0";
        }
    }

    @DexIgnore
    public static HttpHost k(Context context) {
        if (context == null) {
            return null;
        }
        try {
            if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0) {
                return null;
            }
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return null;
            }
            if (activeNetworkInfo.getTypeName() != null && activeNetworkInfo.getTypeName().equalsIgnoreCase("WIFI")) {
                return null;
            }
            String extraInfo = activeNetworkInfo.getExtraInfo();
            if (extraInfo == null) {
                return null;
            }
            if (extraInfo.equals("cmwap") || extraInfo.equals("3gwap") || extraInfo.equals("uniwap")) {
                return new HttpHost("10.0.0.172", 80);
            }
            if (extraInfo.equals("ctwap")) {
                return new HttpHost("10.0.0.200", 80);
            }
            String defaultHost = Proxy.getDefaultHost();
            if (defaultHost != null && defaultHost.trim().length() > 0) {
                return new HttpHost(defaultHost, Proxy.getDefaultPort());
            }
            return null;
        } catch (Throwable th) {
            j.e(th);
        }
    }

    @DexIgnore
    public static void l(Context context, int i2) {
        t = i2;
        ii7.e(context, "mta.qq.com.difftime", i2);
    }

    @DexIgnore
    public static boolean m(jg7 jg7) {
        if (jg7 == null) {
            return false;
        }
        return t(jg7.a());
    }

    @DexIgnore
    public static byte[] n(byte[] bArr) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
        byte[] bArr2 = new byte[4096];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bArr.length * 2);
        while (true) {
            int read = gZIPInputStream.read(bArr2);
            if (read != -1) {
                byteArrayOutputStream.write(bArr2, 0, read);
            } else {
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                byteArrayInputStream.close();
                gZIPInputStream.close();
                byteArrayOutputStream.close();
                return byteArray;
            }
        }
    }

    @DexIgnore
    public static long o(String str) {
        return f(str, CodelessMatcher.CURRENT_CLASS_NAME, 100, 3, 0L).longValue();
    }

    @DexIgnore
    public static th7 p() {
        th7 th7;
        synchronized (ei7.class) {
            try {
                if (j == null) {
                    th7 th72 = new th7("MtaSDK");
                    j = th72;
                    th72.k(false);
                }
                th7 = j;
            } catch (Throwable th) {
                throw th;
            }
        }
        return th7;
    }

    @DexIgnore
    public static String q(Context context) {
        String str;
        synchronized (ei7.class) {
            try {
                if (f946a == null || f946a.trim().length() == 0) {
                    String a2 = ji7.a(context);
                    f946a = a2;
                    if (a2 == null || a2.trim().length() == 0) {
                        f946a = Integer.toString(B().nextInt(Integer.MAX_VALUE));
                    }
                    str = f946a;
                } else {
                    str = f946a;
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return str;
    }

    @DexIgnore
    public static long r() {
        try {
            Calendar instance = Calendar.getInstance();
            instance.set(11, 0);
            instance.set(12, 0);
            instance.set(13, 0);
            instance.set(14, 0);
            return instance.getTimeInMillis() + LogBuilder.MAX_INTERVAL;
        } catch (Throwable th) {
            j.e(th);
            return System.currentTimeMillis() + LogBuilder.MAX_INTERVAL;
        }
    }

    @DexIgnore
    public static String s(Context context) {
        String str;
        synchronized (ei7.class) {
            try {
                if (c == null || c.trim().length() == 0) {
                    c = ji7.f(context);
                }
                str = c;
            } catch (Throwable th) {
                throw th;
            }
        }
        return str;
    }

    @DexIgnore
    public static boolean t(String str) {
        return (str == null || str.trim().length() == 0) ? false : true;
    }

    @DexIgnore
    public static DisplayMetrics u(Context context) {
        if (e == null) {
            e = new DisplayMetrics();
            ((WindowManager) context.getApplicationContext().getSystemService("window")).getDefaultDisplay().getMetrics(e);
        }
        return e;
    }

    @DexIgnore
    public static String v() {
        if (t(n)) {
            return n;
        }
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        long availableBlocks = (long) statFs.getAvailableBlocks();
        String str = String.valueOf((availableBlocks * ((long) statFs.getBlockSize())) / 1000000) + "/" + String.valueOf(w() / 1000000);
        n = str;
        return str;
    }

    @DexIgnore
    public static long w() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
    }

    @DexIgnore
    public static boolean x(Context context) {
        NetworkInfo[] allNetworkInfo;
        try {
            if (ji7.e(context, "android.permission.ACCESS_WIFI_STATE")) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getApplicationContext().getSystemService("connectivity");
                if (!(connectivityManager == null || (allNetworkInfo = connectivityManager.getAllNetworkInfo()) == null)) {
                    for (int i2 = 0; i2 < allNetworkInfo.length; i2++) {
                        if (allNetworkInfo[i2].getTypeName().equalsIgnoreCase("WIFI") && allNetworkInfo[i2].isConnected()) {
                            return true;
                        }
                    }
                }
                return false;
            }
            j.m("can not get the permission of android.permission.ACCESS_WIFI_STATE");
            return false;
        } catch (Throwable th) {
            j.e(th);
        }
    }

    @DexIgnore
    public static String z(Context context) {
        String str = b;
        if (str != null) {
            return str;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                String string = applicationInfo.metaData.getString("TA_APPKEY");
                if (string != null) {
                    b = string;
                    return string;
                }
                j.h("Could not read APPKEY meta-data from AndroidManifest.xml");
            }
        } catch (Throwable th) {
            j.h("Could not read APPKEY meta-data from AndroidManifest.xml");
        }
        return null;
    }
}
