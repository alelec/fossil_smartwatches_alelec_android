package com.fossil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ip5 extends BaseDbProvider implements hp5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f1649a; // = "ip5";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends HashMap<Integer, UpgradeCommand> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ip5$a$a")
        /* renamed from: com.fossil.ip5$a$a  reason: collision with other inner class name */
        public class C0121a implements UpgradeCommand {
            @DexIgnore
            public C0121a(a aVar) {
            }

            @DexIgnore
            @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                new b(sQLiteDatabase).execute(new Void[0]);
            }
        }

        @DexIgnore
        public a() {
            put(2, new C0121a(this));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends AsyncTask<Void, Void, Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ SQLiteDatabase f1650a;

        @DexIgnore
        public b(SQLiteDatabase sQLiteDatabase) {
            this.f1650a = sQLiteDatabase;
        }

        @DexIgnore
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            try {
                FLogger.INSTANCE.getLocal().d(ip5.f1649a, "Inside upgrade db from 1 to 2");
                Cursor query = this.f1650a.query(true, "hourNotification", new String[]{"extraId", "createdAt", AppFilter.COLUMN_HOUR, AppFilter.COLUMN_IS_VIBRATION_ONLY}, null, null, null, null, null, null);
                List<gp5> arrayList = new ArrayList();
                if (query != null) {
                    query.moveToFirst();
                    while (!query.isAfterLast()) {
                        String string = query.getString(query.getColumnIndex("extraId"));
                        String string2 = query.getString(query.getColumnIndex("createdAt"));
                        int i = query.getInt(query.getColumnIndex(AppFilter.COLUMN_HOUR));
                        int i2 = query.getInt(query.getColumnIndex(AppFilter.COLUMN_IS_VIBRATION_ONLY));
                        gp5 gp5 = new gp5();
                        gp5.i(string);
                        gp5.g(Long.valueOf(string2).longValue());
                        gp5.k(i);
                        gp5.n(i2 == 1);
                        arrayList.add(gp5);
                        query.moveToNext();
                    }
                    query.close();
                }
                FLogger.INSTANCE.getLocal().d(ip5.f1649a, "Inside upgrade db from 1 to 2, creating hour notification copy table");
                this.f1650a.execSQL("CREATE TABLE hour_notification_copy (id VARCHAR PRIMARY KEY, extraId VARCHAR, hour INTEGER, createdAt VARCHAR, isVibrationOnly INTEGER, deviceFamily VARCHAR);");
                if (!arrayList.isEmpty()) {
                    arrayList = ip5.r(arrayList);
                }
                if (!arrayList.isEmpty()) {
                    for (gp5 gp52 : arrayList) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("extraId", gp52.c());
                        contentValues.put(AppFilter.COLUMN_HOUR, Integer.valueOf(gp52.d()));
                        contentValues.put("createdAt", Long.valueOf(gp52.a()));
                        contentValues.put("deviceFamily", gp52.b());
                        contentValues.put(AppFilter.COLUMN_IS_VIBRATION_ONLY, Boolean.valueOf(gp52.f()));
                        contentValues.put("id", gp52.e());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = ip5.f1649a;
                        local.d(str, "Insert new values " + contentValues + " into copy table");
                        this.f1650a.insert("hour_notification_copy", null, contentValues);
                    }
                }
                this.f1650a.execSQL("DROP TABLE hourNotification;");
                this.f1650a.execSQL("ALTER TABLE hour_notification_copy RENAME TO hourNotification;");
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = ip5.f1649a;
                local2.e(str2, "Error inside " + ip5.f1649a + ".upgrade - e=" + e);
            }
            return null;
        }
    }

    @DexIgnore
    public ip5(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    public static gp5 o(String str, List<gp5> list) {
        for (gp5 gp5 : list) {
            if (gp5.c().equalsIgnoreCase(str)) {
                return gp5;
            }
        }
        return null;
    }

    @DexIgnore
    public static List<gp5> r(List<gp5> list) {
        gp5 o;
        ArrayList arrayList = new ArrayList();
        if (list != null && !list.isEmpty()) {
            MFDeviceFamily[] a2 = nk5.o.a();
            for (MFDeviceFamily mFDeviceFamily : a2) {
                if (mFDeviceFamily != MFDeviceFamily.DEVICE_FAMILY_Q_MOTION) {
                    List<AppFilter> e = e47.b.e(mFDeviceFamily);
                    List<ContactGroup> allContactGroups = mn5.p.a().d().getAllContactGroups(mFDeviceFamily.ordinal());
                    if (e != null && !e.isEmpty()) {
                        for (AppFilter appFilter : e) {
                            gp5 o2 = o(appFilter.getType(), list);
                            if (o2 != null) {
                                gp5 gp5 = new gp5(o2.d(), o2.f(), o2.c(), o2.b());
                                FLogger.INSTANCE.getLocal().d(f1649a, "Migrating 1.10.3 ... Checking deviceFamily=" + mFDeviceFamily.name() + " Found hands setting of app filter=" + appFilter.getType());
                                gp5.h(mFDeviceFamily.name());
                                StringBuilder sb = new StringBuilder();
                                sb.append(appFilter.getType());
                                sb.append(mFDeviceFamily);
                                gp5.m(sb.toString());
                                arrayList.add(gp5);
                            }
                        }
                    }
                    if (allContactGroups != null && !allContactGroups.isEmpty()) {
                        for (ContactGroup contactGroup : allContactGroups) {
                            if (!(contactGroup.getContacts() == null || contactGroup.getContacts().isEmpty() || (o = o(String.valueOf(contactGroup.getContacts().get(0).getContactId()), list)) == null)) {
                                FLogger.INSTANCE.getLocal().d(f1649a, "Migrating 1.10.3 ... Checking deviceFamily=" + mFDeviceFamily.name() + "Found hands setting of contactId=" + contactGroup.getContacts().get(0).getContactId());
                                gp5 gp52 = new gp5(o.d(), o.f(), o.c(), o.b());
                                gp52.h(mFDeviceFamily.name());
                                gp52.m(contactGroup.getContacts().get(0).getContactId() + mFDeviceFamily.name());
                                arrayList.add(gp52);
                            }
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.hp5
    public gp5 d(String str, String str2) {
        try {
            QueryBuilder<gp5, Integer> queryBuilder = q().queryBuilder();
            queryBuilder.where().eq("extraId", str).and().eq("deviceFamily", str2);
            gp5 queryForFirst = queryBuilder.queryForFirst();
            return queryForFirst == null ? new gp5(1, false, str, nk5.o.e(PortfolioApp.d0.J()).name()) : queryForFirst;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = f1649a;
            local.e(str3, "Error inside " + f1649a + ".getHourNotificationByExtraId - e=" + e);
            return p(str);
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{gp5.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new a();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    public final gp5 p(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = f1649a;
        local.d(str2, "getHourNotificationByExtraIdOnly() id = " + str);
        try {
            QueryBuilder<gp5, Integer> queryBuilder = q().queryBuilder();
            queryBuilder.where().eq("extraId", str);
            gp5 queryForFirst = queryBuilder.queryForFirst();
            if (queryForFirst != null) {
                return queryForFirst;
            }
            FLogger.INSTANCE.getLocal().d(f1649a, "getHourNotificationByExtraIdOnly() - notification is null - return default notification for this action");
            return new gp5(1, false, str, nk5.o.e(PortfolioApp.d0.J()).name());
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = f1649a;
            local2.e(str3, "Error inside " + f1649a + ".getHourNotificationByExtraIdOnly - e=" + e);
            return new gp5(1, false, str, nk5.o.e(PortfolioApp.d0.J()).name());
        }
    }

    @DexIgnore
    public final Dao<gp5, Integer> q() throws SQLException {
        return this.databaseHelper.getDao(gp5.class);
    }
}
