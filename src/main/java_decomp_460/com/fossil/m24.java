package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class m24<K, V> extends n24<K, V> implements ConcurrentMap<K, V> {
    @DexIgnore
    @Override // com.fossil.n24, com.fossil.n24, com.fossil.o24
    public abstract /* bridge */ /* synthetic */ Object delegate();

    @DexIgnore
    @Override // com.fossil.n24, com.fossil.n24, com.fossil.o24
    public abstract /* bridge */ /* synthetic */ Map delegate();

    @DexIgnore
    @Override // com.fossil.n24, com.fossil.n24, com.fossil.o24
    public abstract ConcurrentMap<K, V> delegate();

    @DexIgnore
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public V putIfAbsent(K k, V v) {
        return delegate().putIfAbsent(k, v);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public boolean remove(Object obj, Object obj2) {
        return delegate().remove(obj, obj2);
    }

    @DexIgnore
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public V replace(K k, V v) {
        return delegate().replace(k, v);
    }

    @DexIgnore
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public boolean replace(K k, V v, V v2) {
        return delegate().replace(k, v, v2);
    }
}
