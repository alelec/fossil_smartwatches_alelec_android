package com.fossil;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface cl3 extends IInterface {
    @DexIgnore
    void D1(xr3 xr3) throws RemoteException;

    @DexIgnore
    void E1(vg3 vg3, or3 or3) throws RemoteException;

    @DexIgnore
    void K1(vg3 vg3, String str, String str2) throws RemoteException;

    @DexIgnore
    void N0(long j, String str, String str2, String str3) throws RemoteException;

    @DexIgnore
    void P1(or3 or3) throws RemoteException;

    @DexIgnore
    List<fr3> S(String str, String str2, String str3, boolean z) throws RemoteException;

    @DexIgnore
    void S0(or3 or3) throws RemoteException;

    @DexIgnore
    List<xr3> T0(String str, String str2, String str3) throws RemoteException;

    @DexIgnore
    List<xr3> V0(String str, String str2, or3 or3) throws RemoteException;

    @DexIgnore
    String n0(or3 or3) throws RemoteException;

    @DexIgnore
    void n2(Bundle bundle, or3 or3) throws RemoteException;

    @DexIgnore
    List<fr3> o1(String str, String str2, boolean z, or3 or3) throws RemoteException;

    @DexIgnore
    List<fr3> p1(or3 or3, boolean z) throws RemoteException;

    @DexIgnore
    void r1(or3 or3) throws RemoteException;

    @DexIgnore
    void s(xr3 xr3, or3 or3) throws RemoteException;

    @DexIgnore
    void u2(fr3 fr3, or3 or3) throws RemoteException;

    @DexIgnore
    byte[] w2(vg3 vg3, String str) throws RemoteException;
}
