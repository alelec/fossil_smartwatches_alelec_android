package com.fossil;

import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class te2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static a f3399a;

    @DexIgnore
    public interface a {
        @DexIgnore
        ScheduledExecutorService a();
    }

    @DexIgnore
    public static a a() {
        a aVar;
        synchronized (te2.class) {
            try {
                if (f3399a == null) {
                    f3399a = new ue2();
                }
                aVar = f3399a;
            } catch (Throwable th) {
                throw th;
            }
        }
        return aVar;
    }
}
