package com.fossil;

import java.security.cert.X509Certificate;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import javax.security.auth.x500.X500Principal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z38 implements c48 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<X500Principal, Set<X509Certificate>> f4416a; // = new LinkedHashMap();

    @DexIgnore
    public z38(X509Certificate... x509CertificateArr) {
        for (X509Certificate x509Certificate : x509CertificateArr) {
            X500Principal subjectX500Principal = x509Certificate.getSubjectX500Principal();
            Set<X509Certificate> set = this.f4416a.get(subjectX500Principal);
            if (set == null) {
                set = new LinkedHashSet<>(1);
                this.f4416a.put(subjectX500Principal, set);
            }
            set.add(x509Certificate);
        }
    }

    @DexIgnore
    @Override // com.fossil.c48
    public X509Certificate a(X509Certificate x509Certificate) {
        Set<X509Certificate> set = this.f4416a.get(x509Certificate.getIssuerX500Principal());
        if (set == null) {
            return null;
        }
        for (X509Certificate x509Certificate2 : set) {
            try {
                x509Certificate.verify(x509Certificate2.getPublicKey());
                return x509Certificate2;
            } catch (Exception e) {
            }
        }
        return null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof z38) && ((z38) obj).f4416a.equals(this.f4416a);
    }

    @DexIgnore
    public int hashCode() {
        return this.f4416a.hashCode();
    }
}
