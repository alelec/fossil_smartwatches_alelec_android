package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h83 implements xw2<k83> {
    @DexIgnore
    public static h83 c; // = new h83();
    @DexIgnore
    public /* final */ xw2<k83> b;

    @DexIgnore
    public h83() {
        this(ww2.b(new j83()));
    }

    @DexIgnore
    public h83(xw2<k83> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((k83) c.zza()).zza();
    }

    @DexIgnore
    public static double b() {
        return ((k83) c.zza()).zzb();
    }

    @DexIgnore
    public static long c() {
        return ((k83) c.zza()).zzc();
    }

    @DexIgnore
    public static long d() {
        return ((k83) c.zza()).zzd();
    }

    @DexIgnore
    public static String e() {
        return ((k83) c.zza()).zze();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ k83 zza() {
        return this.b.zza();
    }
}
