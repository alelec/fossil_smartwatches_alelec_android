package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h94 implements i94 {
    @DexIgnore
    public static /* final */ Pattern f; // = Pattern.compile("[^\\p{Alnum}]");
    @DexIgnore
    public static /* final */ String g; // = Pattern.quote("/");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ j94 f1454a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ lg4 d;
    @DexIgnore
    public String e;

    @DexIgnore
    public h94(Context context, String str, lg4 lg4) {
        if (context == null) {
            throw new IllegalArgumentException("appContext must not be null");
        } else if (str != null) {
            this.b = context;
            this.c = str;
            this.d = lg4;
            this.f1454a = new j94();
        } else {
            throw new IllegalArgumentException("appIdentifier must not be null");
        }
    }

    @DexIgnore
    public static String c(String str) {
        if (str == null) {
            return null;
        }
        return f.matcher(str).replaceAll("").toLowerCase(Locale.US);
    }

    @DexIgnore
    @Override // com.fossil.i94
    public String a() {
        String str;
        synchronized (this) {
            if (this.e != null) {
                str = this.e;
            } else {
                SharedPreferences t = r84.t(this.b);
                String id = this.d.getId();
                String string = t.getString("firebase.installation.id", null);
                if (string == null) {
                    SharedPreferences o = r84.o(this.b);
                    String string2 = o.getString("crashlytics.installation.id", null);
                    x74 f2 = x74.f();
                    f2.b("No cached FID; legacy id is " + string2);
                    if (string2 == null) {
                        this.e = b(id, t);
                    } else {
                        this.e = string2;
                        i(string2, id, t, o);
                    }
                    str = this.e;
                } else {
                    if (string.equals(id)) {
                        this.e = t.getString("crashlytics.installation.id", null);
                        x74 f3 = x74.f();
                        f3.b("Found matching FID, using Crashlytics IID: " + this.e);
                        if (this.e == null) {
                            this.e = b(id, t);
                        }
                    } else {
                        this.e = b(id, t);
                    }
                    str = this.e;
                }
            }
        }
        return str;
    }

    @DexIgnore
    public final String b(String str, SharedPreferences sharedPreferences) {
        String c2;
        synchronized (this) {
            c2 = c(UUID.randomUUID().toString());
            x74 f2 = x74.f();
            f2.b("Created new Crashlytics IID: " + c2);
            sharedPreferences.edit().putString("crashlytics.installation.id", c2).putString("firebase.installation.id", str).apply();
        }
        return c2;
    }

    @DexIgnore
    public String d() {
        return this.c;
    }

    @DexIgnore
    public String e() {
        return this.f1454a.a(this.b);
    }

    @DexIgnore
    public String f() {
        return String.format(Locale.US, "%s/%s", j(Build.MANUFACTURER), j(Build.MODEL));
    }

    @DexIgnore
    public String g() {
        return j(Build.VERSION.INCREMENTAL);
    }

    @DexIgnore
    public String h() {
        return j(Build.VERSION.RELEASE);
    }

    @DexIgnore
    public final void i(String str, String str2, SharedPreferences sharedPreferences, SharedPreferences sharedPreferences2) {
        synchronized (this) {
            x74 f2 = x74.f();
            f2.b("Migrating legacy Crashlytics IID: " + str);
            sharedPreferences.edit().putString("crashlytics.installation.id", str).putString("firebase.installation.id", str2).apply();
            sharedPreferences2.edit().remove("crashlytics.installation.id").remove("crashlytics.advertising.id").apply();
        }
    }

    @DexIgnore
    public final String j(String str) {
        return str.replaceAll(g, "");
    }
}
