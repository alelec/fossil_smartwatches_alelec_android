package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface qn7<T> {
    @DexIgnore
    tn7 getContext();

    @DexIgnore
    void resumeWith(Object obj);
}
