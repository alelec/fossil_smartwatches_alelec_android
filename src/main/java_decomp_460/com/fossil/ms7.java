package com.fossil;

import com.fossil.ks7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ms7<T, R> extends ks7<R>, rp7<T, R> {

    @DexIgnore
    public interface a<T, R> extends ks7.a<R>, rp7<T, R> {
    }

    @DexIgnore
    Object getDelegate(T t);

    @DexIgnore
    a<T, R> getGetter();
}
