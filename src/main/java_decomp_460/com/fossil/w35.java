package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w35 extends v35 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z;
    @DexIgnore
    public long x;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        z = sparseIntArray;
        sparseIntArray.put(2131361987, 1);
        z.put(2131362244, 2);
        z.put(2131361979, 3);
        z.put(2131361967, 4);
        z.put(2131363052, 5);
        z.put(2131363372, 6);
    }
    */

    @DexIgnore
    public w35(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 7, y, z));
    }

    @DexIgnore
    public w35(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[4], (ImageView) objArr[3], (ConstraintLayout) objArr[1], (FlexibleEditText) objArr[2], (ConstraintLayout) objArr[0], (RecyclerViewEmptySupport) objArr[5], (FlexibleTextView) objArr[6]);
        this.x = -1;
        this.u.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.x != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.x = 1;
        }
        w();
    }
}
