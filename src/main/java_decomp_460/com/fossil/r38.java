package com.fossil;

import android.os.Build;
import android.util.Log;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import retrofit.android.AndroidLog;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r38 extends w38 {
    @DexIgnore
    public /* final */ v38<Socket> c;
    @DexIgnore
    public /* final */ v38<Socket> d;
    @DexIgnore
    public /* final */ v38<Socket> e;
    @DexIgnore
    public /* final */ v38<Socket> f;
    @DexIgnore
    public /* final */ c g; // = c.b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends a48 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Object f3078a;
        @DexIgnore
        public /* final */ Method b;

        @DexIgnore
        public a(Object obj, Method method) {
            this.f3078a = obj;
            this.b = method;
        }

        @DexIgnore
        @Override // com.fossil.a48
        public List<Certificate> a(List<Certificate> list, String str) throws SSLPeerUnverifiedException {
            try {
                return (List) this.b.invoke(this.f3078a, (X509Certificate[]) list.toArray(new X509Certificate[list.size()]), "RSA", str);
            } catch (InvocationTargetException e) {
                SSLPeerUnverifiedException sSLPeerUnverifiedException = new SSLPeerUnverifiedException(e.getMessage());
                sSLPeerUnverifiedException.initCause(e);
                throw sSLPeerUnverifiedException;
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return obj instanceof a;
        }

        @DexIgnore
        public int hashCode() {
            return 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements c48 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ X509TrustManager f3079a;
        @DexIgnore
        public /* final */ Method b;

        @DexIgnore
        public b(X509TrustManager x509TrustManager, Method method) {
            this.b = method;
            this.f3079a = x509TrustManager;
        }

        @DexIgnore
        @Override // com.fossil.c48
        public X509Certificate a(X509Certificate x509Certificate) {
            try {
                TrustAnchor trustAnchor = (TrustAnchor) this.b.invoke(this.f3079a, x509Certificate);
                if (trustAnchor != null) {
                    return trustAnchor.getTrustedCert();
                }
                return null;
            } catch (IllegalAccessException e) {
                throw b28.b("unable to get issues and signature", e);
            } catch (InvocationTargetException e2) {
                return null;
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return this.f3079a.equals(bVar.f3079a) && this.b.equals(bVar.b);
        }

        @DexIgnore
        public int hashCode() {
            return this.f3079a.hashCode() + (this.b.hashCode() * 31);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Method f3080a;
        @DexIgnore
        public /* final */ Method b;
        @DexIgnore
        public /* final */ Method c;

        @DexIgnore
        public c(Method method, Method method2, Method method3) {
            this.f3080a = method;
            this.b = method2;
            this.c = method3;
        }

        @DexIgnore
        public static c b() {
            Method method;
            Method method2;
            Method method3;
            try {
                Class<?> cls = Class.forName("dalvik.system.CloseGuard");
                method3 = cls.getMethod("get", new Class[0]);
                method2 = cls.getMethod("open", String.class);
                method = cls.getMethod("warnIfOpen", new Class[0]);
            } catch (Exception e) {
                method = null;
                method2 = null;
                method3 = null;
            }
            return new c(method3, method2, method);
        }

        @DexIgnore
        public Object a(String str) {
            Method method = this.f3080a;
            if (method != null) {
                try {
                    Object invoke = method.invoke(null, new Object[0]);
                    this.b.invoke(invoke, str);
                    return invoke;
                } catch (Exception e) {
                }
            }
            return null;
        }

        @DexIgnore
        public boolean c(Object obj) {
            if (obj == null) {
                return false;
            }
            try {
                this.c.invoke(obj, new Object[0]);
                return true;
            } catch (Exception e) {
                return false;
            }
        }
    }

    @DexIgnore
    public r38(Class<?> cls, v38<Socket> v38, v38<Socket> v382, v38<Socket> v383, v38<Socket> v384) {
        this.c = v38;
        this.d = v382;
        this.e = v383;
        this.f = v384;
    }

    @DexIgnore
    public static w38 u() {
        Class<?> cls;
        v38 v38;
        v38 v382;
        try {
            cls = Class.forName("com.android.org.conscrypt.SSLParametersImpl");
            try {
                v38 v383 = new v38(null, "setUseSessionTickets", Boolean.TYPE);
                v38 v384 = new v38(null, "setHostname", String.class);
                if (v()) {
                    v382 = new v38(byte[].class, "getAlpnSelectedProtocol", new Class[0]);
                    v38 = new v38(null, "setAlpnProtocols", byte[].class);
                } else {
                    v38 = null;
                    v382 = null;
                }
                return new r38(cls, v383, v384, v382, v38);
            } catch (ClassNotFoundException e2) {
                return null;
            }
        } catch (ClassNotFoundException e3) {
            cls = Class.forName("org.apache.harmony.xnet.provider.jsse.SSLParametersImpl");
        }
    }

    @DexIgnore
    public static boolean v() {
        if (Security.getProvider("GMSCore_OpenSSL") != null) {
            return true;
        }
        try {
            Class.forName("android.net.Network");
            return true;
        } catch (ClassNotFoundException e2) {
            return false;
        }
    }

    @DexIgnore
    @Override // com.fossil.w38
    public a48 c(X509TrustManager x509TrustManager) {
        try {
            Class<?> cls = Class.forName("android.net.http.X509TrustManagerExtensions");
            return new a(cls.getConstructor(X509TrustManager.class).newInstance(x509TrustManager), cls.getMethod("checkServerTrusted", X509Certificate[].class, String.class, String.class));
        } catch (Exception e2) {
            return super.c(x509TrustManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.w38
    public c48 d(X509TrustManager x509TrustManager) {
        try {
            Method declaredMethod = x509TrustManager.getClass().getDeclaredMethod("findTrustAnchorByIssuerAndSignature", X509Certificate.class);
            declaredMethod.setAccessible(true);
            return new b(x509TrustManager, declaredMethod);
        } catch (NoSuchMethodException e2) {
            return super.d(x509TrustManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.w38
    public void g(SSLSocket sSLSocket, String str, List<t18> list) {
        if (str != null) {
            this.c.e(sSLSocket, Boolean.TRUE);
            this.d.e(sSLSocket, str);
        }
        v38<Socket> v38 = this.f;
        if (v38 != null && v38.g(sSLSocket)) {
            byte[] e2 = w38.e(list);
            this.f.f(sSLSocket, e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.w38
    public void h(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        try {
            socket.connect(inetSocketAddress, i);
        } catch (AssertionError e2) {
            if (b28.A(e2)) {
                throw new IOException(e2);
            }
            throw e2;
        } catch (SecurityException e3) {
            IOException iOException = new IOException("Exception in connect");
            iOException.initCause(e3);
            throw iOException;
        } catch (ClassCastException e4) {
            if (Build.VERSION.SDK_INT == 26) {
                IOException iOException2 = new IOException("Exception in connect");
                iOException2.initCause(e4);
                throw iOException2;
            }
            throw e4;
        }
    }

    @DexIgnore
    @Override // com.fossil.w38
    public SSLContext l() {
        boolean z = true;
        try {
            if (Build.VERSION.SDK_INT < 16 || Build.VERSION.SDK_INT >= 22) {
                z = false;
            }
        } catch (NoClassDefFoundError e2) {
        }
        if (z) {
            try {
                return SSLContext.getInstance("TLSv1.2");
            } catch (NoSuchAlgorithmException e3) {
            }
        }
        try {
            return SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e4) {
            throw new IllegalStateException("No TLS provider", e4);
        }
    }

    @DexIgnore
    @Override // com.fossil.w38
    public String m(SSLSocket sSLSocket) {
        v38<Socket> v38 = this.e;
        if (v38 == null || !v38.g(sSLSocket)) {
            return null;
        }
        byte[] bArr = (byte[]) this.e.f(sSLSocket, new Object[0]);
        return bArr != null ? new String(bArr, b28.i) : null;
    }

    @DexIgnore
    @Override // com.fossil.w38
    public Object n(String str) {
        return this.g.a(str);
    }

    @DexIgnore
    @Override // com.fossil.w38
    public boolean o(String str) {
        if (Build.VERSION.SDK_INT < 23) {
            return super.o(str);
        }
        try {
            Class<?> cls = Class.forName("android.security.NetworkSecurityPolicy");
            return t(str, cls, cls.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]));
        } catch (ClassNotFoundException | NoSuchMethodException e2) {
            return super.o(str);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e3) {
            throw b28.b("unable to determine cleartext support", e3);
        }
    }

    @DexIgnore
    @Override // com.fossil.w38
    public void q(int i, String str, Throwable th) {
        int min;
        int i2 = i == 5 ? 5 : 3;
        if (th != null) {
            str = str + '\n' + Log.getStackTraceString(th);
        }
        int i3 = 0;
        int length = str.length();
        while (i3 < length) {
            int indexOf = str.indexOf(10, i3);
            if (indexOf == -1) {
                indexOf = length;
            }
            while (true) {
                min = Math.min(indexOf, i3 + AndroidLog.LOG_CHUNK_SIZE);
                Log.println(i2, "OkHttp", str.substring(i3, min));
                if (min >= indexOf) {
                    break;
                }
                i3 = min;
            }
            i3 = min + 1;
        }
    }

    @DexIgnore
    @Override // com.fossil.w38
    public void r(String str, Object obj) {
        if (!this.g.c(obj)) {
            q(5, str, null);
        }
    }

    @DexIgnore
    public final boolean s(String str, Class<?> cls, Object obj) throws InvocationTargetException, IllegalAccessException {
        try {
            return ((Boolean) cls.getMethod("isCleartextTrafficPermitted", new Class[0]).invoke(obj, new Object[0])).booleanValue();
        } catch (NoSuchMethodException e2) {
            return super.o(str);
        }
    }

    @DexIgnore
    public final boolean t(String str, Class<?> cls, Object obj) throws InvocationTargetException, IllegalAccessException {
        try {
            return ((Boolean) cls.getMethod("isCleartextTrafficPermitted", String.class).invoke(obj, str)).booleanValue();
        } catch (NoSuchMethodException e2) {
            return s(str, cls, obj);
        }
    }
}
