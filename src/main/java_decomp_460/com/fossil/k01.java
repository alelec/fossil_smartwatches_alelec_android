package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k01 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ViewPager2 f1849a;
    @DexIgnore
    public /* final */ m01 b;
    @DexIgnore
    public /* final */ RecyclerView c;

    @DexIgnore
    public k01(ViewPager2 viewPager2, m01 m01, RecyclerView recyclerView) {
        this.f1849a = viewPager2;
        this.b = m01;
        this.c = recyclerView;
    }

    @DexIgnore
    public boolean a() {
        return this.b.g();
    }
}
