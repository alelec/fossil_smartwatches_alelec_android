package com.fossil;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.t47;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a07 extends pv5 implements zz6, View.OnClickListener, TextWatcher, View.OnKeyListener, t47.g {
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public yz6 g;
    @DexIgnore
    public g37<f65> h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public ScaleAnimation l; // = new ScaleAnimation(1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public ScaleAnimation m; // = new ScaleAnimation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public ScaleAnimation s; // = new ScaleAnimation(1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public ScaleAnimation t; // = new ScaleAnimation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public TranslateAnimation u;
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final a07 a() {
            return new a07();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ a07 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(a07 a07) {
            this.b = a07;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            f65 R6 = this.b.R6();
            if (R6 != null) {
                FlexibleTextView flexibleTextView = R6.F;
                pq7.b(flexibleTextView, "it.tvTitle");
                flexibleTextView.setVisibility(8);
                ViewPropertyAnimator translationY = R6.E.animate().translationY(-this.b.W6());
                pq7.b(translationY, "it.scVerifyContent.anima\u2026slationY(-mtvTitleHeight)");
                translationY.setDuration(125);
                ViewPropertyAnimator translationY2 = R6.y.animate().translationY(-this.b.V6());
                pq7.b(translationY2, "it.ftvEmailNotification.\u2026slationY(-mivEmailHeight)");
                translationY2.setDuration(125);
                ViewPropertyAnimator translationY3 = R6.x.animate().translationY(-this.b.V6());
                pq7.b(translationY3, "it.ftvEmail.animate()\n  \u2026slationY(-mivEmailHeight)");
                translationY3.setDuration(125);
                ViewPropertyAnimator translationY4 = R6.r.animate().translationY(-this.b.V6());
                pq7.b(translationY4, "it.clVerificationCode.an\u2026slationY(-mivEmailHeight)");
                translationY4.setDuration(125);
                ViewPropertyAnimator translationY5 = R6.z.animate().translationY(-this.b.V6());
                pq7.b(translationY5, "it.ftvEnterCodesGuide.an\u2026slationY(-mivEmailHeight)");
                translationY5.setDuration(125);
                ViewPropertyAnimator translationY6 = R6.A.animate().translationY(-this.b.V6());
                pq7.b(translationY6, "it.ftvInvalidCode.animat\u2026slationY(-mivEmailHeight)");
                translationY6.setDuration(125);
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            f65 R6 = this.b.R6();
            if (R6 != null) {
                FlexibleTextView flexibleTextView = R6.F;
                pq7.b(flexibleTextView, "it.tvTitle");
                flexibleTextView.setVisibility(0);
                RTLImageView rTLImageView = R6.C;
                pq7.b(rTLImageView, "it.ivEmail");
                rTLImageView.setVisibility(0);
                this.b.T6().setDuration(250);
                R6.F.startAnimation(this.b.T6());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ a07 c;

        @DexIgnore
        public c(View view, a07 a07) {
            this.b = view;
            this.c = a07;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.b.getWindowVisibleDisplayFrame(rect);
            int height = this.b.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                f65 R6 = this.c.R6();
                if (R6 != null && !this.c.k) {
                    R6.C.startAnimation(this.c.s);
                    this.c.k = true;
                    return;
                }
                return;
            }
            f65 R62 = this.c.R6();
            if (R62 != null && this.c.k) {
                a07 a07 = this.c;
                FlexibleTextView flexibleTextView = R62.F;
                pq7.b(flexibleTextView, "it.tvTitle");
                a07.Y6((float) flexibleTextView.getHeight());
                a07 a072 = this.c;
                RTLImageView rTLImageView = R62.C;
                pq7.b(rTLImageView, "it.ivEmail");
                a072.X6((float) rTLImageView.getHeight());
                if (this.c.u == null) {
                    a07 a073 = this.c;
                    FlexibleTextView flexibleTextView2 = R62.F;
                    pq7.b(flexibleTextView2, "it.tvTitle");
                    float x = flexibleTextView2.getX();
                    FlexibleTextView flexibleTextView3 = R62.F;
                    pq7.b(flexibleTextView3, "it.tvTitle");
                    a073.u = new TranslateAnimation(1, x, 1, flexibleTextView3.getX(), 1, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1, this.c.W6());
                    this.c.a7();
                }
                R62.E.startAnimation(this.c.u);
                this.c.k = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ f65 b;

        @DexIgnore
        public d(f65 f65) {
            this.b = f65;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.b.s.f(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ f65 b;

        @DexIgnore
        public e(f65 f65) {
            this.b = f65;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.b.u.f(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ f65 b;

        @DexIgnore
        public f(f65 f65) {
            this.b = f65;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.b.v.f(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ f65 b;

        @DexIgnore
        public g(f65 f65) {
            this.b = f65;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.b.t.f(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ a07 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public h(a07 a07) {
            this.b = a07;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            f65 R6 = this.b.R6();
            if (R6 != null) {
                FlexibleTextView flexibleTextView = R6.F;
                pq7.b(flexibleTextView, "it.tvTitle");
                flexibleTextView.setVisibility(0);
                RTLImageView rTLImageView = R6.C;
                pq7.b(rTLImageView, "it.ivEmail");
                rTLImageView.setVisibility(0);
                R6.C.startAnimation(this.b.S6());
                this.b.T6().setDuration(500);
                R6.F.startAnimation(this.b.U6());
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            f65 R6 = this.b.R6();
            if (R6 != null) {
                FlexibleTextView flexibleTextView = R6.F;
                pq7.b(flexibleTextView, "it.tvTitle");
                flexibleTextView.setVisibility(4);
                RTLImageView rTLImageView = R6.C;
                pq7.b(rTLImageView, "it.ivEmail");
                rTLImageView.setVisibility(4);
                ViewPropertyAnimator translationY = R6.E.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                pq7.b(translationY, "it.scVerifyContent.anima\u2026        .translationY(0f)");
                translationY.setDuration(500);
                ViewPropertyAnimator translationY2 = R6.y.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                pq7.b(translationY2, "it.ftvEmailNotification.\u2026        .translationY(0f)");
                translationY2.setDuration(500);
                ViewPropertyAnimator translationY3 = R6.x.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                pq7.b(translationY3, "it.ftvEmail.animate()\n  \u2026        .translationY(0f)");
                translationY3.setDuration(500);
                ViewPropertyAnimator translationY4 = R6.r.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                pq7.b(translationY4, "it.clVerificationCode.an\u2026        .translationY(0f)");
                translationY4.setDuration(500);
                ViewPropertyAnimator translationY5 = R6.z.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                pq7.b(translationY5, "it.ftvEnterCodesGuide.an\u2026        .translationY(0f)");
                translationY5.setDuration(500);
                ViewPropertyAnimator translationY6 = R6.A.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                pq7.b(translationY6, "it.ftvInvalidCode.animat\u2026        .translationY(0f)");
                translationY6.setDuration(500);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.zz6
    public void B1(SignUpEmailAuth signUpEmailAuth) {
        pq7.c(signUpEmailAuth, "emailAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.B;
            pq7.b(activity, "it");
            aVar.a(activity, signUpEmailAuth);
        }
    }

    @DexIgnore
    @Override // com.fossil.zz6
    public void D5() {
        RTLImageView rTLImageView;
        FlexibleTextView flexibleTextView;
        if (isActive()) {
            f65 R6 = R6();
            if (!(R6 == null || (flexibleTextView = R6.F) == null)) {
                flexibleTextView.setText(um5.c(PortfolioApp.h0.c(), 2131887008));
            }
            f65 R62 = R6();
            if (R62 != null && (rTLImageView = R62.C) != null) {
                rTLImageView.setImageDrawable(PortfolioApp.h0.c().getDrawable(2131231332));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.zz6
    public void M1(boolean z) {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        FlexibleButton flexibleButton3;
        if (isActive()) {
            f65 R6 = R6();
            if (!(R6 == null || (flexibleButton3 = R6.q) == null)) {
                flexibleButton3.setEnabled(z);
            }
            if (z) {
                f65 R62 = R6();
                if (R62 != null && (flexibleButton2 = R62.q) != null) {
                    flexibleButton2.d("flexible_button_primary");
                    return;
                }
                return;
            }
            f65 R63 = R6();
            if (R63 != null && (flexibleButton = R63.q) != null) {
                flexibleButton.d("flexible_button_disabled");
            }
        }
    }

    @DexIgnore
    public final void Q6() {
        f65 R6;
        if (isActive() && (R6 = R6()) != null) {
            FlexibleEditText flexibleEditText = R6.s;
            pq7.b(flexibleEditText, "it.etFirstCode");
            String valueOf = String.valueOf(flexibleEditText.getText());
            FlexibleEditText flexibleEditText2 = R6.u;
            pq7.b(flexibleEditText2, "it.etSecondCode");
            String valueOf2 = String.valueOf(flexibleEditText2.getText());
            FlexibleEditText flexibleEditText3 = R6.v;
            pq7.b(flexibleEditText3, "it.etThirdCode");
            String valueOf3 = String.valueOf(flexibleEditText3.getText());
            FlexibleEditText flexibleEditText4 = R6.t;
            pq7.b(flexibleEditText4, "it.etFourthCode");
            String valueOf4 = String.valueOf(flexibleEditText4.getText());
            yz6 yz6 = this.g;
            if (yz6 != null) {
                yz6.n(new String[]{valueOf, valueOf2, valueOf3, valueOf4});
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        if (str.hashCode() == 766014770 && str.equals("EMAIL_OTP_VERIFICATION") && i2 == 2131361942) {
            yz6 yz6 = this.g;
            if (yz6 != null) {
                yz6.q();
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final f65 R6() {
        g37<f65> g37 = this.h;
        if (g37 != null) {
            return g37.a();
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final ScaleAnimation S6() {
        return this.t;
    }

    @DexIgnore
    @Override // com.fossil.zz6
    public void T0(String str) {
        f65 R6;
        pq7.c(str, "emailAddress");
        if (isActive() && (R6 = R6()) != null) {
            FlexibleTextView flexibleTextView = R6.x;
            pq7.b(flexibleTextView, "it.ftvEmail");
            flexibleTextView.setText(str);
        }
    }

    @DexIgnore
    public final ScaleAnimation T6() {
        return this.l;
    }

    @DexIgnore
    @Override // com.fossil.zz6
    public void U1() {
        FragmentActivity activity;
        if (!isActive() || (activity = getActivity()) == null) {
            return;
        }
        if (this.k) {
            xk5 xk5 = xk5.f4136a;
            f65 R6 = R6();
            RTLImageView rTLImageView = R6 != null ? R6.B : null;
            if (rTLImageView != null) {
                pq7.b(activity, "it");
                xk5.a(rTLImageView, activity);
                return;
            }
            throw new il7("null cannot be cast to non-null type android.view.View");
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public final ScaleAnimation U6() {
        return this.m;
    }

    @DexIgnore
    public final float V6() {
        return this.j;
    }

    @DexIgnore
    public final float W6() {
        return this.i;
    }

    @DexIgnore
    public final void X6(float f2) {
        this.j = f2;
    }

    @DexIgnore
    public final void Y6(float f2) {
        this.i = f2;
    }

    @DexIgnore
    /* renamed from: Z6 */
    public void M5(yz6 yz6) {
        pq7.c(yz6, "presenter");
        this.g = yz6;
    }

    @DexIgnore
    public final void a7() {
        TranslateAnimation translateAnimation = this.u;
        if (translateAnimation != null) {
            translateAnimation.setAnimationListener(new h(this));
        }
    }

    @DexIgnore
    public void afterTextChanged(Editable editable) {
        boolean z = false;
        boolean z2 = true;
        FragmentActivity activity = getActivity();
        View currentFocus = activity != null ? activity.getCurrentFocus() : null;
        f65 R6 = R6();
        if (R6 != null) {
            Integer valueOf = currentFocus != null ? Integer.valueOf(currentFocus.getId()) : null;
            if (valueOf != null && valueOf.intValue() == 2131362234) {
                if (currentFocus != null) {
                    EditText editText = (EditText) currentFocus;
                    Editable text = editText.getText();
                    int length = text.length();
                    if (length == 0) {
                        FlexibleButton flexibleButton = R6.q;
                        pq7.b(flexibleButton, "it.btContinue");
                        flexibleButton.setEnabled(false);
                        R6.q.d("flexible_button_disabled");
                    } else if (length == 1) {
                        editText.setSelection(1);
                        FlexibleEditText flexibleEditText = R6.u;
                        pq7.b(flexibleEditText, "it.etSecondCode");
                        Editable text2 = flexibleEditText.getText();
                        if (text2 != null) {
                            pq7.b(text2, "it.etSecondCode.text!!");
                            if (text2.length() != 0) {
                                z2 = false;
                            }
                            if (z2) {
                                R6.u.requestFocus();
                            }
                            Q6();
                            return;
                        }
                        pq7.i();
                        throw null;
                    } else if (length == 2) {
                        char charAt = text.charAt(0);
                        char charAt2 = text.charAt(1);
                        FlexibleEditText flexibleEditText2 = R6.u;
                        pq7.b(flexibleEditText2, "it.etSecondCode");
                        Editable text3 = flexibleEditText2.getText();
                        if (text3 != null) {
                            pq7.b(text3, "it.etSecondCode.text!!");
                            if (text3.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                R6.s.setTextKeepState(String.valueOf(charAt));
                                R6.s.clearFocus();
                                R6.u.requestFocus();
                                R6.u.setText(String.valueOf(charAt2));
                            } else {
                                R6.s.setTextKeepState(String.valueOf(charAt2));
                            }
                            editText.setSelection(1);
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type android.widget.EditText");
                }
            } else if (valueOf != null && valueOf.intValue() == 2131362245) {
                if (currentFocus != null) {
                    EditText editText2 = (EditText) currentFocus;
                    Editable text4 = editText2.getText();
                    int length2 = text4.length();
                    if (length2 == 0) {
                        FlexibleButton flexibleButton2 = R6.q;
                        pq7.b(flexibleButton2, "it.btContinue");
                        flexibleButton2.setEnabled(false);
                        R6.q.d("flexible_button_disabled");
                    } else if (length2 == 1) {
                        editText2.setSelection(1);
                        FlexibleEditText flexibleEditText3 = R6.v;
                        pq7.b(flexibleEditText3, "it.etThirdCode");
                        Editable text5 = flexibleEditText3.getText();
                        if (text5 != null) {
                            pq7.b(text5, "it.etThirdCode.text!!");
                            if (text5.length() != 0) {
                                z2 = false;
                            }
                            if (z2) {
                                R6.v.requestFocus();
                            }
                            Q6();
                            return;
                        }
                        pq7.i();
                        throw null;
                    } else if (length2 == 2) {
                        char charAt3 = text4.charAt(0);
                        char charAt4 = text4.charAt(1);
                        FlexibleEditText flexibleEditText4 = R6.v;
                        pq7.b(flexibleEditText4, "it.etThirdCode");
                        Editable text6 = flexibleEditText4.getText();
                        if (text6 != null) {
                            pq7.b(text6, "it.etThirdCode.text!!");
                            if (text6.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                editText2.setTextKeepState(String.valueOf(charAt3));
                                editText2.clearFocus();
                                R6.v.requestFocus();
                                R6.v.setText(String.valueOf(charAt4));
                            } else {
                                editText2.setTextKeepState(String.valueOf(charAt4));
                            }
                            editText2.setSelection(1);
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type android.widget.EditText");
                }
            } else if (valueOf != null && valueOf.intValue() == 2131362247) {
                if (currentFocus != null) {
                    EditText editText3 = (EditText) currentFocus;
                    Editable text7 = editText3.getText();
                    int length3 = text7.length();
                    if (length3 == 0) {
                        FlexibleButton flexibleButton3 = R6.q;
                        pq7.b(flexibleButton3, "it.btContinue");
                        flexibleButton3.setEnabled(false);
                        R6.q.d("flexible_button_disabled");
                    } else if (length3 == 1) {
                        editText3.setSelection(1);
                        FlexibleEditText flexibleEditText5 = R6.t;
                        pq7.b(flexibleEditText5, "it.etFourthCode");
                        Editable text8 = flexibleEditText5.getText();
                        if (text8 != null) {
                            pq7.b(text8, "it.etFourthCode.text!!");
                            if (text8.length() == 0) {
                                R6.t.requestFocus();
                            }
                            Q6();
                            return;
                        }
                        pq7.i();
                        throw null;
                    } else if (length3 == 2) {
                        char charAt5 = text7.charAt(0);
                        char charAt6 = text7.charAt(1);
                        FlexibleEditText flexibleEditText6 = R6.t;
                        pq7.b(flexibleEditText6, "it.etFourthCode");
                        Editable text9 = flexibleEditText6.getText();
                        if (text9 != null) {
                            pq7.b(text9, "it.etFourthCode.text!!");
                            if (text9.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                editText3.setText(String.valueOf(charAt5));
                                editText3.clearFocus();
                                R6.t.requestFocus();
                                R6.t.setText(String.valueOf(charAt6));
                            } else {
                                editText3.setText(String.valueOf(charAt6));
                            }
                            editText3.setSelection(1);
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type android.widget.EditText");
                }
            } else if (valueOf == null || valueOf.intValue() != 2131362236) {
            } else {
                if (currentFocus != null) {
                    EditText editText4 = (EditText) currentFocus;
                    int length4 = editText4.getText().length();
                    if (length4 == 0) {
                        FlexibleButton flexibleButton4 = R6.q;
                        pq7.b(flexibleButton4, "it.btContinue");
                        flexibleButton4.setEnabled(false);
                        R6.q.d("flexible_button_disabled");
                    } else if (length4 == 1) {
                        editText4.setSelection(1);
                        Q6();
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type android.widget.EditText");
                }
            }
        }
    }

    @DexIgnore
    public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    @DexIgnore
    @Override // com.fossil.zz6
    public void c5(boolean z) {
        f65 R6;
        FlexibleTextView flexibleTextView;
        if (isActive() && (R6 = R6()) != null && (flexibleTextView = R6.A) != null) {
            if (z) {
                pq7.b(flexibleTextView, "it");
                flexibleTextView.setVisibility(0);
                return;
            }
            pq7.b(flexibleTextView, "it");
            if (flexibleTextView.getVisibility() != 4) {
                flexibleTextView.setVisibility(4);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.zz6
    public void h() {
        if (isActive()) {
            a();
        }
    }

    @DexIgnore
    @Override // com.fossil.zz6
    public void i() {
        if (isActive()) {
            b();
        }
    }

    @DexIgnore
    @Override // com.fossil.zz6
    public void l5(int i2, String str) {
        pq7.c(str, "errorMessage");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.O(childFragmentManager, i2, str);
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131361941) {
                yz6 yz6 = this.g;
                if (yz6 != null) {
                    yz6.r();
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            } else if (id == 2131362296) {
                yz6 yz62 = this.g;
                if (yz62 != null) {
                    yz62.p();
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            } else if (id == 2131362666) {
                yz6 yz63 = this.g;
                if (yz63 != null) {
                    yz63.o();
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.l.setDuration(500);
        this.l.setFillAfter(true);
        this.m.setDuration(500);
        this.m.setFillAfter(true);
        this.s.setDuration(500);
        this.s.setFillAfter(true);
        this.t.setDuration(500);
        this.t.setFillAfter(true);
        this.s.setAnimationListener(new b(this));
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        f65 f65 = (f65) aq0.f(layoutInflater, 2131558553, viewGroup, false, A6());
        pq7.b(f65, "binding");
        View n = f65.n();
        pq7.b(n, "binding.root");
        n.getViewTreeObserver().addOnGlobalLayoutListener(new c(n, this));
        this.h = new g37<>(this, f65);
        f65 R6 = R6();
        if (R6 != null) {
            return R6.n();
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        f65 R6;
        boolean z = true;
        pq7.c(view, "view");
        pq7.c(keyEvent, "keyEvent");
        if (i2 == 67 && keyEvent.getAction() == 0 && (R6 = R6()) != null) {
            if (view.getId() == 2131362245) {
                FlexibleEditText flexibleEditText = R6.u;
                pq7.b(flexibleEditText, "it.etSecondCode");
                Editable text = flexibleEditText.getText();
                if (text != null) {
                    pq7.b(text, "it.etSecondCode.text!!");
                    if (text.length() != 0) {
                        z = false;
                    }
                    if (z) {
                        R6.s.requestFocus();
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else if (view.getId() == 2131362247) {
                FlexibleEditText flexibleEditText2 = R6.v;
                pq7.b(flexibleEditText2, "it.etThirdCode");
                Editable text2 = flexibleEditText2.getText();
                if (text2 != null) {
                    pq7.b(text2, "it.etThirdCode.text!!");
                    if (text2.length() != 0) {
                        z = false;
                    }
                    if (z) {
                        R6.u.requestFocus();
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else if (view.getId() == 2131362236) {
                FlexibleEditText flexibleEditText3 = R6.t;
                pq7.b(flexibleEditText3, "it.etFourthCode");
                Editable text3 = flexibleEditText3.getText();
                if (text3 != null) {
                    pq7.b(text3, "it.etFourthCode.text!!");
                    if (text3.length() != 0) {
                        z = false;
                    }
                    if (z) {
                        R6.v.requestFocus();
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }
        return super.onKey(view, i2, keyEvent);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        FragmentActivity activity;
        Window window;
        super.onPause();
        if (!(this.k || (activity = getActivity()) == null || (window = activity.getWindow()) == null)) {
            window.setSoftInputMode(3);
        }
        yz6 yz6 = this.g;
        if (yz6 != null) {
            yz6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        yz6 yz6 = this.g;
        if (yz6 != null) {
            yz6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        f65 R6 = R6();
        if (R6 != null) {
            R6.B.setOnClickListener(this);
            R6.w.setOnClickListener(this);
            R6.q.setOnClickListener(this);
            R6.s.addTextChangedListener(this);
            R6.u.addTextChangedListener(this);
            R6.v.addTextChangedListener(this);
            R6.t.addTextChangedListener(this);
            R6.s.setOnKeyListener(this);
            R6.u.setOnKeyListener(this);
            R6.v.setOnKeyListener(this);
            R6.t.setOnKeyListener(this);
            R6.s.setOnFocusChangeListener(new d(R6));
            R6.u.setOnFocusChangeListener(new e(R6));
            R6.v.setOnFocusChangeListener(new f(R6));
            R6.t.setOnFocusChangeListener(new g(R6));
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.zz6
    public void z1(String str, int i2, int i3) {
        View n;
        pq7.c(str, "emailAddress");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.F(childFragmentManager, i2, i3, str);
            f65 R6 = R6();
            if (R6 != null && (n = R6.n()) != null) {
                n.setVisibility(8);
            }
        }
    }
}
