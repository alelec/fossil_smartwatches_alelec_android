package com.fossil;

import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xy6 implements Factory<wy6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<WatchAppRepository> f4213a;
    @DexIgnore
    public /* final */ Provider<ComplicationRepository> b;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> c;
    @DexIgnore
    public /* final */ Provider<CategoryRepository> d;
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> e;
    @DexIgnore
    public /* final */ Provider<RingStyleRepository> f;
    @DexIgnore
    public /* final */ Provider<v36> g;
    @DexIgnore
    public /* final */ Provider<d26> h;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> i;
    @DexIgnore
    public /* final */ Provider<on5> j;
    @DexIgnore
    public /* final */ Provider<WatchLocalizationRepository> k;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> l;
    @DexIgnore
    public /* final */ Provider<s77> m;
    @DexIgnore
    public /* final */ Provider<FileRepository> n;
    @DexIgnore
    public /* final */ Provider<uo5> o;
    @DexIgnore
    public /* final */ Provider<zo5> p;
    @DexIgnore
    public /* final */ Provider<UserRepository> q;
    @DexIgnore
    public /* final */ Provider<k97> r;

    @DexIgnore
    public xy6(Provider<WatchAppRepository> provider, Provider<ComplicationRepository> provider2, Provider<DianaAppSettingRepository> provider3, Provider<CategoryRepository> provider4, Provider<DianaWatchFaceRepository> provider5, Provider<RingStyleRepository> provider6, Provider<v36> provider7, Provider<d26> provider8, Provider<NotificationSettingsDatabase> provider9, Provider<on5> provider10, Provider<WatchLocalizationRepository> provider11, Provider<AlarmsRepository> provider12, Provider<s77> provider13, Provider<FileRepository> provider14, Provider<uo5> provider15, Provider<zo5> provider16, Provider<UserRepository> provider17, Provider<k97> provider18) {
        this.f4213a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
        this.i = provider9;
        this.j = provider10;
        this.k = provider11;
        this.l = provider12;
        this.m = provider13;
        this.n = provider14;
        this.o = provider15;
        this.p = provider16;
        this.q = provider17;
        this.r = provider18;
    }

    @DexIgnore
    public static xy6 a(Provider<WatchAppRepository> provider, Provider<ComplicationRepository> provider2, Provider<DianaAppSettingRepository> provider3, Provider<CategoryRepository> provider4, Provider<DianaWatchFaceRepository> provider5, Provider<RingStyleRepository> provider6, Provider<v36> provider7, Provider<d26> provider8, Provider<NotificationSettingsDatabase> provider9, Provider<on5> provider10, Provider<WatchLocalizationRepository> provider11, Provider<AlarmsRepository> provider12, Provider<s77> provider13, Provider<FileRepository> provider14, Provider<uo5> provider15, Provider<zo5> provider16, Provider<UserRepository> provider17, Provider<k97> provider18) {
        return new xy6(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14, provider15, provider16, provider17, provider18);
    }

    @DexIgnore
    public static wy6 c(WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, DianaAppSettingRepository dianaAppSettingRepository, CategoryRepository categoryRepository, DianaWatchFaceRepository dianaWatchFaceRepository, RingStyleRepository ringStyleRepository, v36 v36, d26 d26, NotificationSettingsDatabase notificationSettingsDatabase, on5 on5, WatchLocalizationRepository watchLocalizationRepository, AlarmsRepository alarmsRepository, s77 s77, FileRepository fileRepository, uo5 uo5, zo5 zo5, UserRepository userRepository, k97 k97) {
        return new wy6(watchAppRepository, complicationRepository, dianaAppSettingRepository, categoryRepository, dianaWatchFaceRepository, ringStyleRepository, v36, d26, notificationSettingsDatabase, on5, watchLocalizationRepository, alarmsRepository, s77, fileRepository, uo5, zo5, userRepository, k97);
    }

    @DexIgnore
    /* renamed from: b */
    public wy6 get() {
        return c(this.f4213a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get(), this.j.get(), this.k.get(), this.l.get(), this.m.get(), this.n.get(), this.o.get(), this.p.get(), this.q.get(), this.r.get());
    }
}
