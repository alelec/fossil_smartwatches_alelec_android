package com.fossil;

import androidx.lifecycle.LiveData;
import com.fossil.h47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.ServerError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class c47<ResultType, RequestType> {
    @DexIgnore
    public boolean isFromCache; // = true;
    @DexIgnore
    public /* final */ js0<h47<ResultType>> result;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.util.NetworkBoundResource$1", f = "NetworkBoundResource.kt", l = {34, 36}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ c47 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.c47$a$a")
        @eo7(c = "com.portfolio.platform.util.NetworkBoundResource$1$1", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.c47$a$a  reason: collision with other inner class name */
        public static final class C0025a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ LiveData $dbSource;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.c47$a$a$a")
            /* renamed from: com.fossil.c47$a$a$a  reason: collision with other inner class name */
            public static final class C0026a<T> implements ls0<S> {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ C0025a f555a;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.c47$a$a$a$a")
                /* renamed from: com.fossil.c47$a$a$a$a  reason: collision with other inner class name */
                public static final class C0027a<T> implements ls0<S> {

                    @DexIgnore
                    /* renamed from: a  reason: collision with root package name */
                    public /* final */ /* synthetic */ C0026a f556a;

                    @DexIgnore
                    public C0027a(C0026a aVar) {
                        this.f556a = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ls0
                    public final void onChanged(ResultType resulttype) {
                        this.f556a.f555a.this$0.this$0.setValue(h47.e.d(resulttype));
                    }
                }

                @DexIgnore
                public C0026a(C0025a aVar) {
                    this.f555a = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ls0
                public final void onChanged(ResultType resulttype) {
                    this.f555a.this$0.this$0.result.q(this.f555a.$dbSource);
                    if (this.f555a.this$0.this$0.shouldFetch(resulttype)) {
                        C0025a aVar = this.f555a;
                        aVar.this$0.this$0.fetchFromNetwork(aVar.$dbSource);
                        return;
                    }
                    this.f555a.this$0.this$0.result.p(this.f555a.$dbSource, new C0027a(this));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0025a(a aVar, LiveData liveData, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
                this.$dbSource = liveData;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0025a aVar = new C0025a(this.this$0, this.$dbSource, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((C0025a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.result.p(this.$dbSource, new C0026a(this));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(c47 c47, qn7 qn7) {
            super(2, qn7);
            this.this$0 = c47;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0046  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 2
                r2 = 1
                java.lang.Object r3 = com.fossil.yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0048
                if (r0 == r2) goto L_0x0024
                if (r0 != r6) goto L_0x001c
                java.lang.Object r0 = r7.L$1
                androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
            L_0x0019:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x001b:
                return r0
            L_0x001c:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0024:
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
                r2 = r0
                r1 = r8
            L_0x002d:
                r0 = r1
                androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                com.fossil.jx7 r1 = com.fossil.bw7.c()
                com.fossil.c47$a$a r4 = new com.fossil.c47$a$a
                r5 = 0
                r4.<init>(r7, r0, r5)
                r7.L$0 = r2
                r7.L$1 = r0
                r7.label = r6
                java.lang.Object r0 = com.fossil.eu7.g(r1, r4, r7)
                if (r0 != r3) goto L_0x0019
                r0 = r3
                goto L_0x001b
            L_0x0048:
                com.fossil.el7.b(r8)
                com.fossil.iv7 r0 = r7.p$
                com.fossil.c47 r1 = r7.this$0
                r7.L$0 = r0
                r7.label = r2
                java.lang.Object r1 = r1.loadFromDb(r7)
                if (r1 != r3) goto L_0x005b
                r0 = r3
                goto L_0x001b
            L_0x005b:
                r2 = r0
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.c47.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1", f = "NetworkBoundResource.kt", l = {58, 65, 68, 76, 76, 79, 85, 88, 98, 111}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ LiveData $dbSource;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ c47 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$1", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.c47$b$a$a")
            /* renamed from: com.fossil.c47$b$a$a  reason: collision with other inner class name */
            public static final class C0028a<T> implements ls0<S> {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ a f557a;

                @DexIgnore
                public C0028a(a aVar) {
                    this.f557a = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ls0
                public final void onChanged(ResultType resulttype) {
                    this.f557a.this$0.this$0.setValue(h47.e.c(resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.result.p(this.this$0.$dbSource, new C0028a(this));
                    this.this$0.this$0.result.q(this.this$0.$dbSource);
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.c47$b$b")
        @eo7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$2", f = "NetworkBoundResource.kt", l = {71}, m = "invokeSuspend")
        /* renamed from: com.fossil.c47$b$b  reason: collision with other inner class name */
        public static final class C0029b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.c47$b$b$a")
            /* renamed from: com.fossil.c47$b$b$a */
            public static final class a<T> implements ls0<S> {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ C0029b f558a;

                @DexIgnore
                public a(C0029b bVar) {
                    this.f558a = bVar;
                }

                @DexIgnore
                @Override // com.fossil.ls0
                public final void onChanged(ResultType resulttype) {
                    this.f558a.this$0.this$0.setValue(h47.e.d(resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0029b(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0029b bVar = new C0029b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((C0029b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object loadFromDb;
                js0 js0;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    FLogger.INSTANCE.getLocal().d("NetworkBoundResource", "set value dbSource fetched from network response null");
                    js0 js02 = this.this$0.this$0.result;
                    c47 c47 = this.this$0.this$0;
                    this.L$0 = iv7;
                    this.L$1 = js02;
                    this.label = 1;
                    loadFromDb = c47.loadFromDb(this);
                    if (loadFromDb == d) {
                        return d;
                    }
                    js0 = js02;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    js0 = (js0) this.L$1;
                    loadFromDb = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                js0.p((LiveData) loadFromDb, new a(this));
                return tl7.f3441a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$3", f = "NetworkBoundResource.kt", l = {79, 79}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ iq5 $response;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(b bVar, iq5 iq5, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$response = iq5;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, this.$response, qn7);
                cVar.p$ = (iv7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 2
                    r4 = 1
                    java.lang.Object r3 = com.fossil.yn7.d()
                    int r0 = r6.label
                    if (r0 == 0) goto L_0x0038
                    if (r0 == r4) goto L_0x0020
                    if (r0 != r5) goto L_0x0018
                    java.lang.Object r0 = r6.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r7)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r6.L$1
                    com.fossil.c47 r0 = (com.fossil.c47) r0
                    java.lang.Object r1 = r6.L$0
                    com.fossil.iv7 r1 = (com.fossil.iv7) r1
                    com.fossil.el7.b(r7)
                    r2 = r1
                L_0x002c:
                    r6.L$0 = r2
                    r6.label = r5
                    java.lang.Object r0 = r0.saveCallResult(r7, r6)
                    if (r0 != r3) goto L_0x0015
                    r0 = r3
                    goto L_0x0017
                L_0x0038:
                    com.fossil.el7.b(r7)
                    com.fossil.iv7 r2 = r6.p$
                    com.fossil.c47$b r0 = r6.this$0
                    com.fossil.c47 r1 = r0.this$0
                    com.fossil.iq5 r0 = r6.$response
                    com.fossil.kq5 r0 = (com.fossil.kq5) r0
                    r6.L$0 = r2
                    r6.L$1 = r1
                    r6.label = r4
                    java.lang.Object r7 = r1.processResponse(r0, r6)
                    if (r7 != r3) goto L_0x0053
                    r0 = r3
                    goto L_0x0017
                L_0x0053:
                    r0 = r1
                    goto L_0x002c
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.c47.b.c.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$4", f = "NetworkBoundResource.kt", l = {85, 85}, m = "invokeSuspend")
        public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ iq5 $response;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(b bVar, iq5 iq5, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$response = iq5;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                d dVar = new d(this.this$0, this.$response, qn7);
                dVar.p$ = (iv7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 2
                    r4 = 1
                    java.lang.Object r3 = com.fossil.yn7.d()
                    int r0 = r6.label
                    if (r0 == 0) goto L_0x0037
                    if (r0 == r4) goto L_0x0020
                    if (r0 != r5) goto L_0x0018
                    java.lang.Object r0 = r6.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r7)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r6.L$1
                    com.fossil.c47 r0 = (com.fossil.c47) r0
                    java.lang.Object r1 = r6.L$0
                    com.fossil.iv7 r1 = (com.fossil.iv7) r1
                    com.fossil.el7.b(r7)
                L_0x002b:
                    r6.L$0 = r1
                    r6.label = r5
                    java.lang.Object r0 = r0.saveCallResult(r7, r6)
                    if (r0 != r3) goto L_0x0015
                    r0 = r3
                    goto L_0x0017
                L_0x0037:
                    com.fossil.el7.b(r7)
                    com.fossil.iv7 r1 = r6.p$
                    com.fossil.c47$b r0 = r6.this$0
                    com.fossil.c47 r2 = r0.this$0
                    com.fossil.iq5 r0 = r6.$response
                    com.fossil.kq5 r0 = (com.fossil.kq5) r0
                    r6.L$0 = r1
                    r6.L$1 = r2
                    r6.label = r4
                    java.lang.Object r7 = r2.processResponse(r0, r6)
                    if (r7 != r3) goto L_0x0052
                    r0 = r3
                    goto L_0x0017
                L_0x0052:
                    r0 = r2
                    goto L_0x002b
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.c47.b.d.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$5", f = "NetworkBoundResource.kt", l = {93}, m = "invokeSuspend")
        public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a<T> implements ls0<S> {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ e f559a;

                @DexIgnore
                public a(e eVar) {
                    this.f559a = eVar;
                }

                @DexIgnore
                @Override // com.fossil.ls0
                public final void onChanged(ResultType resulttype) {
                    this.f559a.this$0.this$0.setValue(h47.e.d(resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public e(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                e eVar = new e(this.this$0, qn7);
                eVar.p$ = (iv7) obj;
                return eVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object loadFromDb;
                js0 js0;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    FLogger.INSTANCE.getLocal().d("NetworkBoundResource", "set value dbSource fetched from network success");
                    js0 js02 = this.this$0.this$0.result;
                    c47 c47 = this.this$0.this$0;
                    this.L$0 = iv7;
                    this.L$1 = js02;
                    this.label = 1;
                    loadFromDb = c47.loadFromDb(this);
                    if (loadFromDb == d) {
                        return d;
                    }
                    js0 = js02;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    loadFromDb = obj;
                    js0 = (js0) this.L$1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                js0.p((LiveData) loadFromDb, new a(this));
                return tl7.f3441a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$6", f = "NetworkBoundResource.kt", l = {99}, m = "invokeSuspend")
        public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a<T> implements ls0<S> {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ f f560a;

                @DexIgnore
                public a(f fVar) {
                    this.f560a = fVar;
                }

                @DexIgnore
                @Override // com.fossil.ls0
                public final void onChanged(ResultType resulttype) {
                    this.f560a.this$0.this$0.setValue(h47.e.c(resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public f(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                f fVar = new f(this.this$0, qn7);
                fVar.p$ = (iv7) obj;
                return fVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object loadFromDb;
                js0 js0;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    js0 js02 = this.this$0.this$0.result;
                    c47 c47 = this.this$0.this$0;
                    this.L$0 = iv7;
                    this.L$1 = js02;
                    this.label = 1;
                    loadFromDb = c47.loadFromDb(this);
                    if (loadFromDb == d) {
                        return d;
                    }
                    js0 = js02;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    js0 = (js0) this.L$1;
                    loadFromDb = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                js0.p((LiveData) loadFromDb, new a(this));
                return tl7.f3441a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$7", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
        public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ iq5 $response;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a<T> implements ls0<S> {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ g f561a;

                @DexIgnore
                public a(g gVar) {
                    this.f561a = gVar;
                }

                @DexIgnore
                @Override // com.fossil.ls0
                public final void onChanged(ResultType resulttype) {
                    String message;
                    g gVar = this.f561a;
                    c47 c47 = gVar.this$0.this$0;
                    h47.a aVar = h47.e;
                    int a2 = ((hq5) gVar.$response).a();
                    ServerError c = ((hq5) this.f561a.$response).c();
                    if (c == null || (message = c.getUserMessage()) == null) {
                        ServerError c2 = ((hq5) this.f561a.$response).c();
                        message = c2 != null ? c2.getMessage() : null;
                    }
                    if (message == null) {
                        message = "";
                    }
                    c47.setValue(aVar.b(a2, message, resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public g(b bVar, iq5 iq5, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$response = iq5;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                g gVar = new g(this.this$0, this.$response, qn7);
                gVar.p$ = (iv7) obj;
                return gVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.result.p(this.this$0.$dbSource, new a(this));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$response$1", f = "NetworkBoundResource.kt", l = {65}, m = "invokeSuspend")
        public static final class h extends ko7 implements rp7<qn7<? super q88<RequestType>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public h(b bVar, qn7 qn7) {
                super(1, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(qn7<?> qn7) {
                pq7.c(qn7, "completion");
                return new h(this.this$0, qn7);
            }

            @DexIgnore
            @Override // com.fossil.rp7
            public final Object invoke(Object obj) {
                return ((h) create((qn7) obj)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    c47 c47 = this.this$0.this$0;
                    this.label = 1;
                    Object createCall = c47.createCall(this);
                    return createCall == d ? d : createCall;
                } else if (i == 1) {
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(c47 c47, LiveData liveData, qn7 qn7) {
            super(2, qn7);
            this.this$0 = c47;
            this.$dbSource = liveData;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$dbSource, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x006c  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00bf  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00de  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x0117  */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x014a  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x0180  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x019a  */
        /* JADX WARNING: Removed duplicated region for block: B:63:0x01d1  */
        /* JADX WARNING: Removed duplicated region for block: B:65:0x01d8  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0027  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
            // Method dump skipped, instructions count: 502
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.c47.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public c47() {
        js0<h47<ResultType>> js0 = new js0<>();
        this.result = js0;
        js0.o(h47.e.a(null));
        this.isFromCache = true;
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    private final void fetchFromNetwork(LiveData<ResultType> liveData) {
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new b(this, liveData, null), 3, null);
    }

    @DexIgnore
    public static /* synthetic */ Object processResponse$suspendImpl(c47 c47, kq5 kq5, qn7 qn7) {
        Object a2 = kq5.a();
        if (a2 != null) {
            return a2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    private final void setValue(h47<? extends ResultType> h47) {
        if (!pq7.a(this.result.e(), h47)) {
            this.result.o(h47);
        }
    }

    @DexIgnore
    public final LiveData<h47<ResultType>> asLiveData() {
        js0<h47<ResultType>> js0 = this.result;
        if (js0 != null) {
            return js0;
        }
        throw new il7("null cannot be cast to non-null type androidx.lifecycle.LiveData<com.portfolio.platform.util.Resource<ResultType>>");
    }

    @DexIgnore
    public abstract Object createCall(qn7<? super q88<RequestType>> qn7);

    @DexIgnore
    public abstract Object loadFromDb(qn7<? super LiveData<ResultType>> qn7);

    @DexIgnore
    public void onFetchFailed(Throwable th) {
    }

    @DexIgnore
    public Object processContinueFetching(RequestType requesttype, qn7<? super Boolean> qn7) {
        return ao7.a(false);
    }

    @DexIgnore
    public Object processResponse(kq5<RequestType> kq5, qn7<? super RequestType> qn7) {
        return processResponse$suspendImpl(this, kq5, qn7);
    }

    @DexIgnore
    public abstract Object saveCallResult(RequestType requesttype, qn7<? super tl7> qn7);

    @DexIgnore
    public abstract boolean shouldFetch(ResultType resulttype);
}
