package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class ot extends Enum<ot> {
    @DexIgnore
    public static /* final */ ot c;
    @DexIgnore
    public static /* final */ /* synthetic */ ot[] d;
    @DexIgnore
    public static /* final */ nt e; // = new nt(null);
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        ot otVar = new ot("REQUEST", 0, (byte) 1);
        ot otVar2 = new ot("NOTIFY", 1, (byte) 2);
        c = otVar2;
        d = new ot[]{otVar, otVar2};
    }
    */

    @DexIgnore
    public ot(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static ot valueOf(String str) {
        return (ot) Enum.valueOf(ot.class, str);
    }

    @DexIgnore
    public static ot[] values() {
        return (ot[]) d.clone();
    }
}
