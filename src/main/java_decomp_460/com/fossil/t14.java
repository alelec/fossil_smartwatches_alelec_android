package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class t14<K, V> implements Map.Entry<K, V> {
    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        return f14.a(getKey(), entry.getKey()) && f14.a(getValue(), entry.getValue());
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public abstract K getKey();

    @DexIgnore
    @Override // java.util.Map.Entry
    public abstract V getValue();

    @DexIgnore
    public int hashCode() {
        int i = 0;
        K key = getKey();
        V value = getValue();
        int hashCode = key == null ? 0 : key.hashCode();
        if (value != null) {
            i = value.hashCode();
        }
        return i ^ hashCode;
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public V setValue(V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public String toString() {
        return ((Object) getKey()) + SimpleComparison.EQUAL_TO_OPERATION + ((Object) getValue());
    }
}
