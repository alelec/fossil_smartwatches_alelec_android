package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class TimezoneChange implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<TimezoneChange> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ int mTimestamp;
    @DexIgnore
    public /* final */ int mTimezoneOffsetInSecond;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<TimezoneChange> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public TimezoneChange createFromParcel(Parcel parcel) {
            return new TimezoneChange(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public TimezoneChange[] newArray(int i) {
            return new TimezoneChange[i];
        }
    }

    @DexIgnore
    public TimezoneChange(int i, int i2) {
        this.mTimestamp = i;
        this.mTimezoneOffsetInSecond = i2;
    }

    @DexIgnore
    public TimezoneChange(Parcel parcel) {
        this.mTimestamp = parcel.readInt();
        this.mTimezoneOffsetInSecond = parcel.readInt();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof TimezoneChange)) {
            return false;
        }
        TimezoneChange timezoneChange = (TimezoneChange) obj;
        return this.mTimestamp == timezoneChange.mTimestamp && this.mTimezoneOffsetInSecond == timezoneChange.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public int getTimestamp() {
        return this.mTimestamp;
    }

    @DexIgnore
    public int getTimezoneOffsetInSecond() {
        return this.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.mTimestamp + 527) * 31) + this.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public String toString() {
        return "TimezoneChange{mTimestamp=" + this.mTimestamp + ",mTimezoneOffsetInSecond=" + this.mTimezoneOffsetInSecond + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mTimestamp);
        parcel.writeInt(this.mTimezoneOffsetInSecond);
    }
}
