package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class WorkoutSummary implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<WorkoutSummary> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ long mActiveDuration;
    @DexIgnore
    public /* final */ Cadence mAverageCadence;
    @DexIgnore
    public /* final */ Short mAverageHeartrate;
    @DexIgnore
    public /* final */ Float mAveragePace;
    @DexIgnore
    public /* final */ Float mAverageSpeed;
    @DexIgnore
    public /* final */ long mCalorie;
    @DexIgnore
    public /* final */ Long mDistanceInCentimeters;
    @DexIgnore
    public /* final */ int mDuration;
    @DexIgnore
    public /* final */ long mId;
    @DexIgnore
    public /* final */ Short mMaxHeartrate;
    @DexIgnore
    public /* final */ Float mMaximumPace;
    @DexIgnore
    public /* final */ Float mMaximumSpeed;
    @DexIgnore
    public /* final */ long mSteps;
    @DexIgnore
    public /* final */ WorkoutType mType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<WorkoutSummary> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutSummary createFromParcel(Parcel parcel) {
            return new WorkoutSummary(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutSummary[] newArray(int i) {
            return new WorkoutSummary[i];
        }
    }

    @DexIgnore
    public WorkoutSummary(long j, WorkoutType workoutType, int i, long j2, long j3, Long l, long j4, Short sh, Short sh2, Float f, Float f2, Float f3, Float f4, Cadence cadence) {
        this.mId = j;
        this.mType = workoutType;
        this.mDuration = i;
        this.mActiveDuration = j2;
        this.mSteps = j3;
        this.mDistanceInCentimeters = l;
        this.mCalorie = j4;
        this.mAverageHeartrate = sh;
        this.mMaxHeartrate = sh2;
        this.mAveragePace = f;
        this.mMaximumPace = f2;
        this.mAverageSpeed = f3;
        this.mMaximumSpeed = f4;
        this.mAverageCadence = cadence;
    }

    @DexIgnore
    public WorkoutSummary(Parcel parcel) {
        this.mId = parcel.readLong();
        this.mType = WorkoutType.values()[parcel.readInt()];
        this.mDuration = parcel.readInt();
        this.mActiveDuration = parcel.readLong();
        this.mSteps = parcel.readLong();
        if (parcel.readByte() == 0) {
            this.mDistanceInCentimeters = null;
        } else {
            this.mDistanceInCentimeters = Long.valueOf(parcel.readLong());
        }
        this.mCalorie = parcel.readLong();
        if (parcel.readByte() == 0) {
            this.mAverageHeartrate = null;
        } else {
            this.mAverageHeartrate = Short.valueOf((short) parcel.readInt());
        }
        if (parcel.readByte() == 0) {
            this.mMaxHeartrate = null;
        } else {
            this.mMaxHeartrate = Short.valueOf((short) parcel.readInt());
        }
        if (parcel.readByte() == 0) {
            this.mAveragePace = null;
        } else {
            this.mAveragePace = Float.valueOf(parcel.readFloat());
        }
        if (parcel.readByte() == 0) {
            this.mMaximumPace = null;
        } else {
            this.mMaximumPace = Float.valueOf(parcel.readFloat());
        }
        if (parcel.readByte() == 0) {
            this.mAverageSpeed = null;
        } else {
            this.mAverageSpeed = Float.valueOf(parcel.readFloat());
        }
        if (parcel.readByte() == 0) {
            this.mMaximumSpeed = null;
        } else {
            this.mMaximumSpeed = Float.valueOf(parcel.readFloat());
        }
        if (parcel.readByte() == 0) {
            this.mAverageCadence = null;
        } else {
            this.mAverageCadence = new Cadence(parcel);
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Cadence cadence;
        Float f;
        Float f2;
        Float f3;
        Float f4;
        Short sh;
        Short sh2;
        Long l;
        if (!(obj instanceof WorkoutSummary)) {
            return false;
        }
        WorkoutSummary workoutSummary = (WorkoutSummary) obj;
        if (this.mId != workoutSummary.mId || this.mType != workoutSummary.mType || this.mDuration != workoutSummary.mDuration || this.mActiveDuration != workoutSummary.mActiveDuration || this.mSteps != workoutSummary.mSteps) {
            return false;
        }
        if (((this.mDistanceInCentimeters != null || workoutSummary.mDistanceInCentimeters != null) && ((l = this.mDistanceInCentimeters) == null || !l.equals(workoutSummary.mDistanceInCentimeters))) || this.mCalorie != workoutSummary.mCalorie) {
            return false;
        }
        if ((this.mAverageHeartrate != null || workoutSummary.mAverageHeartrate != null) && ((sh2 = this.mAverageHeartrate) == null || !sh2.equals(workoutSummary.mAverageHeartrate))) {
            return false;
        }
        if ((this.mMaxHeartrate != null || workoutSummary.mMaxHeartrate != null) && ((sh = this.mMaxHeartrate) == null || !sh.equals(workoutSummary.mMaxHeartrate))) {
            return false;
        }
        if ((this.mAveragePace != null || workoutSummary.mAveragePace != null) && ((f4 = this.mAveragePace) == null || !f4.equals(workoutSummary.mAveragePace))) {
            return false;
        }
        if ((this.mMaximumPace != null || workoutSummary.mMaximumPace != null) && ((f3 = this.mMaximumPace) == null || !f3.equals(workoutSummary.mMaximumPace))) {
            return false;
        }
        if ((this.mAverageSpeed != null || workoutSummary.mAverageSpeed != null) && ((f2 = this.mAverageSpeed) == null || !f2.equals(workoutSummary.mAverageSpeed))) {
            return false;
        }
        if ((this.mMaximumSpeed != null || workoutSummary.mMaximumSpeed != null) && ((f = this.mMaximumSpeed) == null || !f.equals(workoutSummary.mMaximumSpeed))) {
            return false;
        }
        return (this.mAverageCadence == null && workoutSummary.mAverageCadence == null) || ((cadence = this.mAverageCadence) != null && cadence.equals(workoutSummary.mAverageCadence));
    }

    @DexIgnore
    public long getActiveDuration() {
        return this.mActiveDuration;
    }

    @DexIgnore
    public Cadence getAverageCadence() {
        return this.mAverageCadence;
    }

    @DexIgnore
    public Short getAverageHeartrate() {
        return this.mAverageHeartrate;
    }

    @DexIgnore
    public Float getAveragePace() {
        return this.mAveragePace;
    }

    @DexIgnore
    public Float getAverageSpeed() {
        return this.mAverageSpeed;
    }

    @DexIgnore
    public long getCalorie() {
        return this.mCalorie;
    }

    @DexIgnore
    public Long getDistanceInCentimeters() {
        return this.mDistanceInCentimeters;
    }

    @DexIgnore
    public int getDuration() {
        return this.mDuration;
    }

    @DexIgnore
    public long getId() {
        return this.mId;
    }

    @DexIgnore
    public Short getMaxHeartrate() {
        return this.mMaxHeartrate;
    }

    @DexIgnore
    public Float getMaximumPace() {
        return this.mMaximumPace;
    }

    @DexIgnore
    public Float getMaximumSpeed() {
        return this.mMaximumSpeed;
    }

    @DexIgnore
    public long getSteps() {
        return this.mSteps;
    }

    @DexIgnore
    public WorkoutType getType() {
        return this.mType;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        long j = this.mId;
        int i2 = (int) (j ^ (j >>> 32));
        int hashCode = this.mType.hashCode();
        int i3 = this.mDuration;
        long j2 = this.mActiveDuration;
        int i4 = (int) (j2 ^ (j2 >>> 32));
        long j3 = this.mSteps;
        int i5 = (int) (j3 ^ (j3 >>> 32));
        Long l = this.mDistanceInCentimeters;
        int hashCode2 = l == null ? 0 : l.hashCode();
        long j4 = this.mCalorie;
        int i6 = (int) (j4 ^ (j4 >>> 32));
        Short sh = this.mAverageHeartrate;
        int hashCode3 = sh == null ? 0 : sh.hashCode();
        Short sh2 = this.mMaxHeartrate;
        int hashCode4 = sh2 == null ? 0 : sh2.hashCode();
        Float f = this.mAveragePace;
        int hashCode5 = f == null ? 0 : f.hashCode();
        Float f2 = this.mMaximumPace;
        int hashCode6 = f2 == null ? 0 : f2.hashCode();
        Float f3 = this.mAverageSpeed;
        int hashCode7 = f3 == null ? 0 : f3.hashCode();
        Float f4 = this.mMaximumSpeed;
        int hashCode8 = f4 == null ? 0 : f4.hashCode();
        Cadence cadence = this.mAverageCadence;
        if (cadence != null) {
            i = cadence.hashCode();
        }
        return ((((((((((((((((hashCode2 + ((((((((((i2 + 527) * 31) + hashCode) * 31) + i3) * 31) + i4) * 31) + i5) * 31)) * 31) + i6) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSummary{mId=" + this.mId + ",mType=" + this.mType + ",mDuration=" + this.mDuration + ",mActiveDuration=" + this.mActiveDuration + ",mSteps=" + this.mSteps + ",mDistanceInCentimeters=" + this.mDistanceInCentimeters + ",mCalorie=" + this.mCalorie + ",mAverageHeartrate=" + this.mAverageHeartrate + ",mMaxHeartrate=" + this.mMaxHeartrate + ",mAveragePace=" + this.mAveragePace + ",mMaximumPace=" + this.mMaximumPace + ",mAverageSpeed=" + this.mAverageSpeed + ",mMaximumSpeed=" + this.mMaximumSpeed + ",mAverageCadence=" + this.mAverageCadence + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.mId);
        parcel.writeInt(this.mType.ordinal());
        parcel.writeInt(this.mDuration);
        parcel.writeLong(this.mActiveDuration);
        parcel.writeLong(this.mSteps);
        if (this.mDistanceInCentimeters != null) {
            parcel.writeByte((byte) 1);
            parcel.writeLong(this.mDistanceInCentimeters.longValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeLong(this.mCalorie);
        if (this.mAverageHeartrate != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mAverageHeartrate.shortValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mMaxHeartrate != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mMaxHeartrate.shortValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mAveragePace != null) {
            parcel.writeByte((byte) 1);
            parcel.writeFloat(this.mAveragePace.floatValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mMaximumPace != null) {
            parcel.writeByte((byte) 1);
            parcel.writeFloat(this.mMaximumPace.floatValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mAverageSpeed != null) {
            parcel.writeByte((byte) 1);
            parcel.writeFloat(this.mAverageSpeed.floatValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mMaximumSpeed != null) {
            parcel.writeByte((byte) 1);
            parcel.writeFloat(this.mMaximumSpeed.floatValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mAverageCadence != null) {
            parcel.writeByte((byte) 1);
            this.mAverageCadence.writeToParcel(parcel, i);
            return;
        }
        parcel.writeByte((byte) 0);
    }
}
