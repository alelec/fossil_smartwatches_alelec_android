package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FitnessData implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<FitnessData> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ ActiveMinute mActiveMinute;
    @DexIgnore
    public /* final */ Calorie mCalorie;
    @DexIgnore
    public /* final */ Distance mDistance;
    @DexIgnore
    public /* final */ int mEndTime;
    @DexIgnore
    public /* final */ ArrayList<GoalTracking> mGoals;
    @DexIgnore
    public /* final */ HeartRate mHeartrate;
    @DexIgnore
    public /* final */ ArrayList<Resting> mResting;
    @DexIgnore
    public /* final */ ArrayList<SleepSession> mSleeps;
    @DexIgnore
    public /* final */ int mStartTime;
    @DexIgnore
    public /* final */ Step mStep;
    @DexIgnore
    public /* final */ int mTimezoneOffsetInSecond;
    @DexIgnore
    public /* final */ ArrayList<WorkoutSession> mWorkouts;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<FitnessData> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public FitnessData createFromParcel(Parcel parcel) {
            return new FitnessData(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public FitnessData[] newArray(int i) {
            return new FitnessData[i];
        }
    }

    @DexIgnore
    public FitnessData(int i, int i2, int i3, Step step, Calorie calorie, Distance distance, ArrayList<SleepSession> arrayList, ArrayList<WorkoutSession> arrayList2, ActiveMinute activeMinute, ArrayList<Resting> arrayList3, HeartRate heartRate, ArrayList<GoalTracking> arrayList4) {
        this.mStartTime = i;
        this.mEndTime = i2;
        this.mTimezoneOffsetInSecond = i3;
        this.mStep = step;
        this.mCalorie = calorie;
        this.mDistance = distance;
        this.mSleeps = arrayList;
        this.mWorkouts = arrayList2;
        this.mActiveMinute = activeMinute;
        this.mResting = arrayList3;
        this.mHeartrate = heartRate;
        this.mGoals = arrayList4;
    }

    @DexIgnore
    public FitnessData(Parcel parcel) {
        this.mStartTime = parcel.readInt();
        this.mEndTime = parcel.readInt();
        this.mTimezoneOffsetInSecond = parcel.readInt();
        this.mStep = new Step(parcel);
        this.mCalorie = new Calorie(parcel);
        this.mDistance = new Distance(parcel);
        ArrayList<SleepSession> arrayList = new ArrayList<>();
        this.mSleeps = arrayList;
        parcel.readList(arrayList, FitnessData.class.getClassLoader());
        ArrayList<WorkoutSession> arrayList2 = new ArrayList<>();
        this.mWorkouts = arrayList2;
        parcel.readList(arrayList2, FitnessData.class.getClassLoader());
        this.mActiveMinute = new ActiveMinute(parcel);
        ArrayList<Resting> arrayList3 = new ArrayList<>();
        this.mResting = arrayList3;
        parcel.readList(arrayList3, FitnessData.class.getClassLoader());
        if (parcel.readByte() == 0) {
            this.mHeartrate = null;
        } else {
            this.mHeartrate = new HeartRate(parcel);
        }
        ArrayList<GoalTracking> arrayList4 = new ArrayList<>();
        this.mGoals = arrayList4;
        parcel.readList(arrayList4, FitnessData.class.getClassLoader());
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        HeartRate heartRate;
        if (!(obj instanceof FitnessData)) {
            return false;
        }
        FitnessData fitnessData = (FitnessData) obj;
        if (this.mStartTime != fitnessData.mStartTime || this.mEndTime != fitnessData.mEndTime || this.mTimezoneOffsetInSecond != fitnessData.mTimezoneOffsetInSecond || !this.mStep.equals(fitnessData.mStep) || !this.mCalorie.equals(fitnessData.mCalorie) || !this.mDistance.equals(fitnessData.mDistance) || !this.mSleeps.equals(fitnessData.mSleeps) || !this.mWorkouts.equals(fitnessData.mWorkouts) || !this.mActiveMinute.equals(fitnessData.mActiveMinute) || !this.mResting.equals(fitnessData.mResting)) {
            return false;
        }
        return ((this.mHeartrate == null && fitnessData.mHeartrate == null) || ((heartRate = this.mHeartrate) != null && heartRate.equals(fitnessData.mHeartrate))) && this.mGoals.equals(fitnessData.mGoals);
    }

    @DexIgnore
    public ActiveMinute getActiveMinute() {
        return this.mActiveMinute;
    }

    @DexIgnore
    public Calorie getCalorie() {
        return this.mCalorie;
    }

    @DexIgnore
    public Distance getDistance() {
        return this.mDistance;
    }

    @DexIgnore
    public int getEndTime() {
        return this.mEndTime;
    }

    @DexIgnore
    public ArrayList<GoalTracking> getGoals() {
        return this.mGoals;
    }

    @DexIgnore
    public HeartRate getHeartrate() {
        return this.mHeartrate;
    }

    @DexIgnore
    public ArrayList<Resting> getResting() {
        return this.mResting;
    }

    @DexIgnore
    public ArrayList<SleepSession> getSleeps() {
        return this.mSleeps;
    }

    @DexIgnore
    public int getStartTime() {
        return this.mStartTime;
    }

    @DexIgnore
    public Step getStep() {
        return this.mStep;
    }

    @DexIgnore
    public int getTimezoneOffsetInSecond() {
        return this.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public ArrayList<WorkoutSession> getWorkouts() {
        return this.mWorkouts;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.mStartTime;
        int i2 = this.mEndTime;
        int i3 = this.mTimezoneOffsetInSecond;
        int hashCode = this.mStep.hashCode();
        int hashCode2 = this.mCalorie.hashCode();
        int hashCode3 = this.mDistance.hashCode();
        int hashCode4 = this.mSleeps.hashCode();
        int hashCode5 = this.mWorkouts.hashCode();
        int hashCode6 = this.mActiveMinute.hashCode();
        int hashCode7 = this.mResting.hashCode();
        HeartRate heartRate = this.mHeartrate;
        return (((heartRate == null ? 0 : heartRate.hashCode()) + ((((((((((((((((((((i + 527) * 31) + i2) * 31) + i3) * 31) + hashCode) * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31)) * 31) + this.mGoals.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "FitnessData{mStartTime=" + this.mStartTime + ",mEndTime=" + this.mEndTime + ",mTimezoneOffsetInSecond=" + this.mTimezoneOffsetInSecond + ",mStep=" + this.mStep + ",mCalorie=" + this.mCalorie + ",mDistance=" + this.mDistance + ",mSleeps=" + this.mSleeps + ",mWorkouts=" + this.mWorkouts + ",mActiveMinute=" + this.mActiveMinute + ",mResting=" + this.mResting + ",mHeartrate=" + this.mHeartrate + ",mGoals=" + this.mGoals + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mStartTime);
        parcel.writeInt(this.mEndTime);
        parcel.writeInt(this.mTimezoneOffsetInSecond);
        this.mStep.writeToParcel(parcel, i);
        this.mCalorie.writeToParcel(parcel, i);
        this.mDistance.writeToParcel(parcel, i);
        parcel.writeList(this.mSleeps);
        parcel.writeList(this.mWorkouts);
        this.mActiveMinute.writeToParcel(parcel, i);
        parcel.writeList(this.mResting);
        if (this.mHeartrate != null) {
            parcel.writeByte((byte) 1);
            this.mHeartrate.writeToParcel(parcel, i);
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeList(this.mGoals);
    }
}
