package com.fossil.fitness;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class WorkoutSessionManager {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CppProxy extends WorkoutSessionManager {
        @DexIgnore
        public static /* final */ /* synthetic */ boolean $assertionsDisabled; // = false;
        @DexIgnore
        public /* final */ AtomicBoolean destroyed; // = new AtomicBoolean(false);
        @DexIgnore
        public /* final */ long nativeRef;

        @DexIgnore
        public CppProxy(long j) {
            if (j != 0) {
                this.nativeRef = j;
                return;
            }
            throw new RuntimeException("nativeRef is zero");
        }

        @DexIgnore
        public static native ArrayList<GpsDataPoint> getGpsRoute(long j);

        @DexIgnore
        public static native WorkoutSessionManager initialize(String str, long j, WorkoutState workoutState, long j2);

        @DexIgnore
        private native void nativeDestroy(long j);

        @DexIgnore
        private native boolean native_clearAccumulateDistance(long j);

        @DexIgnore
        private native boolean native_correctGpsState(long j, long j2, ArrayList<WorkoutRunningHistory> arrayList);

        @DexIgnore
        private native void native_dispose(long j);

        @DexIgnore
        private native LocationDistance native_getAccumulateDistance(long j);

        @DexIgnore
        private native long native_getCurrentSessionId(long j);

        @DexIgnore
        private native LocationDistance native_getTotalDistance(long j);

        @DexIgnore
        private native void native_onWorkoutStateChanged(long j, WorkoutState workoutState);

        @DexIgnore
        public void _djinni_private_destroy() {
            if (!this.destroyed.getAndSet(true)) {
                nativeDestroy(this.nativeRef);
            }
        }

        @DexIgnore
        @Override // com.fossil.fitness.WorkoutSessionManager
        public boolean clearAccumulateDistance() {
            return native_clearAccumulateDistance(this.nativeRef);
        }

        @DexIgnore
        @Override // com.fossil.fitness.WorkoutSessionManager
        public boolean correctGpsState(long j, ArrayList<WorkoutRunningHistory> arrayList) {
            return native_correctGpsState(this.nativeRef, j, arrayList);
        }

        @DexIgnore
        @Override // com.fossil.fitness.WorkoutSessionManager
        public void dispose() {
            native_dispose(this.nativeRef);
        }

        @DexIgnore
        public void finalize() throws Throwable {
            _djinni_private_destroy();
            super.finalize();
        }

        @DexIgnore
        @Override // com.fossil.fitness.WorkoutSessionManager
        public LocationDistance getAccumulateDistance() {
            return native_getAccumulateDistance(this.nativeRef);
        }

        @DexIgnore
        @Override // com.fossil.fitness.WorkoutSessionManager
        public long getCurrentSessionId() {
            return native_getCurrentSessionId(this.nativeRef);
        }

        @DexIgnore
        @Override // com.fossil.fitness.WorkoutSessionManager
        public LocationDistance getTotalDistance() {
            return native_getTotalDistance(this.nativeRef);
        }

        @DexIgnore
        @Override // com.fossil.fitness.WorkoutSessionManager
        public void onWorkoutStateChanged(WorkoutState workoutState) {
            native_onWorkoutStateChanged(this.nativeRef, workoutState);
        }
    }

    @DexIgnore
    public static ArrayList<GpsDataPoint> getGpsRoute(long j) {
        return CppProxy.getGpsRoute(j);
    }

    @DexIgnore
    public static WorkoutSessionManager initialize(String str, long j, WorkoutState workoutState, long j2) {
        return CppProxy.initialize(str, j, workoutState, j2);
    }

    @DexIgnore
    public abstract boolean clearAccumulateDistance();

    @DexIgnore
    public abstract boolean correctGpsState(long j, ArrayList<WorkoutRunningHistory> arrayList);

    @DexIgnore
    public abstract void dispose();

    @DexIgnore
    public abstract LocationDistance getAccumulateDistance();

    @DexIgnore
    public abstract long getCurrentSessionId();

    @DexIgnore
    public abstract LocationDistance getTotalDistance();

    @DexIgnore
    public abstract void onWorkoutStateChanged(WorkoutState workoutState);
}
