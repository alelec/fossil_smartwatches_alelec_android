package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DataCaching {
    @DexIgnore
    public /* final */ byte[] mEncryptionKey;
    @DexIgnore
    public /* final */ String mWorkingDirectory;

    @DexIgnore
    public DataCaching(String str, byte[] bArr) {
        this.mWorkingDirectory = str;
        this.mEncryptionKey = bArr;
    }

    @DexIgnore
    public byte[] getEncryptionKey() {
        return this.mEncryptionKey;
    }

    @DexIgnore
    public String getWorkingDirectory() {
        return this.mWorkingDirectory;
    }

    @DexIgnore
    public String toString() {
        return "DataCaching{mWorkingDirectory=" + this.mWorkingDirectory + ",mEncryptionKey=" + this.mEncryptionKey + "}";
    }
}
