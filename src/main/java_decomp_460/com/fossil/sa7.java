package com.fossil;

import android.graphics.Bitmap;
import androidx.lifecycle.LiveData;
import com.fossil.ka7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sa7 extends hq4 {
    @DexIgnore
    public /* final */ LiveData<List<DianaWatchFaceUser>> h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public List<fb7> j;
    @DexIgnore
    public List<fb7> k;
    @DexIgnore
    public List<fb7> l;
    @DexIgnore
    public /* final */ LiveData<List<ka7.b>> m;
    @DexIgnore
    public /* final */ PortfolioApp n;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository o;
    @DexIgnore
    public /* final */ FileRepository p;
    @DexIgnore
    public /* final */ s77 q;
    @DexIgnore
    public /* final */ UserRepository r;
    @DexIgnore
    public /* final */ zm5 s;
    @DexIgnore
    public /* final */ CustomizeRealDataRepository t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<I, O> implements gi0<List<? extends DianaWatchFaceUser>, LiveData<List<ka7.b>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sa7 f3228a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sa7$a$a")
        /* renamed from: com.fossil.sa7$a$a  reason: collision with other inner class name */
        public static final class C0218a extends ko7 implements vp7<hs0<List<ka7.b>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sa7$a$a$a")
            /* renamed from: com.fossil.sa7$a$a$a  reason: collision with other inner class name */
            public static final class C0219a extends ko7 implements vp7<iv7, qn7<? super List<? extends fb7>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0218a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0219a(C0218a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0219a aVar = new C0219a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super List<? extends fb7>> qn7) {
                    return ((C0219a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object b;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        s77 s77 = this.this$0.this$0.f3228a.q;
                        this.L$0 = iv7;
                        this.label = 1;
                        b = s77.b(this);
                        if (b == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        b = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return dc7.c((List) b, null, 1, null);
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sa7$a$a$b")
            /* renamed from: com.fossil.sa7$a$a$b */
            public static final class b extends ko7 implements vp7<iv7, qn7<? super List<? extends fb7>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0218a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(C0218a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    b bVar = new b(this.this$0, qn7);
                    bVar.p$ = (iv7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super List<? extends fb7>> qn7) {
                    return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object g;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        s77 s77 = this.this$0.this$0.f3228a.q;
                        this.L$0 = iv7;
                        this.label = 1;
                        g = s77.g(this);
                        if (g == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        g = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return dc7.g((List) g);
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sa7$a$a$c")
            /* renamed from: com.fossil.sa7$a$a$c */
            public static final class c extends ko7 implements vp7<iv7, qn7<? super List<? extends fb7>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0218a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(C0218a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    c cVar = new c(this.this$0, qn7);
                    cVar.p$ = (iv7) obj;
                    return cVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super List<? extends fb7>> qn7) {
                    return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object f;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        s77 s77 = this.this$0.this$0.f3228a.q;
                        this.L$0 = iv7;
                        this.label = 1;
                        f = s77.f(this);
                        if (f == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        f = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return dc7.e((List) f);
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sa7$a$a$d")
            /* renamed from: com.fossil.sa7$a$a$d */
            public static final class d extends ko7 implements vp7<iv7, qn7<? super byte[]>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ DianaWatchFaceUser $it;
                @DexIgnore
                public /* final */ /* synthetic */ List $uiModelList$inlined;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0218a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public d(DianaWatchFaceUser dianaWatchFaceUser, qn7 qn7, C0218a aVar, List list) {
                    super(2, qn7);
                    this.$it = dianaWatchFaceUser;
                    this.this$0 = aVar;
                    this.$uiModelList$inlined = list;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    d dVar = new d(this.$it, qn7, this.this$0, this.$uiModelList$inlined);
                    dVar.p$ = (iv7) obj;
                    return dVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super byte[]> qn7) {
                    return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        File fileByFileName = this.this$0.this$0.f3228a.p.getFileByFileName(this.$it.getId());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("MyFaceViewModel", "process face " + this.$it.getName() + " wfFile " + fileByFileName);
                        if (fileByFileName != null) {
                            return z58.c(fileByFileName);
                        }
                        return null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sa7$a$a$e")
            /* renamed from: com.fossil.sa7$a$a$e */
            public static final class e extends ko7 implements vp7<iv7, qn7<? super cl7<? extends List<? extends s87>, ? extends Bitmap>>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $uiModelList$inlined;
                @DexIgnore
                public /* final */ /* synthetic */ byte[] $wfByteArray;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public Object L$3;
                @DexIgnore
                public Object L$4;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0218a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public e(byte[] bArr, qn7 qn7, C0218a aVar, List list) {
                    super(2, qn7);
                    this.$wfByteArray = bArr;
                    this.this$0 = aVar;
                    this.$uiModelList$inlined = list;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    e eVar = new e(this.$wfByteArray, qn7, this.this$0, this.$uiModelList$inlined);
                    eVar.p$ = (iv7) obj;
                    return eVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super cl7<? extends List<? extends s87>, ? extends Bitmap>> qn7) {
                    return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:12:0x0048  */
                @Override // com.fossil.zn7
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                    /*
                        r8 = this;
                        r7 = 2
                        r3 = 1
                        r4 = 0
                        java.lang.Object r5 = com.fossil.yn7.d()
                        int r0 = r8.label
                        if (r0 == 0) goto L_0x009b
                        if (r0 == r3) goto L_0x003a
                        if (r0 != r7) goto L_0x0032
                        java.lang.Object r0 = r8.L$4
                        android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
                        java.lang.Object r1 = r8.L$3
                        com.fossil.ub7 r1 = (com.fossil.ub7) r1
                        java.lang.Object r1 = r8.L$2
                        com.fossil.ob7 r1 = (com.fossil.ob7) r1
                        java.lang.Object r1 = r8.L$1
                        com.fossil.ob7 r1 = (com.fossil.ob7) r1
                        java.lang.Object r1 = r8.L$0
                        com.fossil.iv7 r1 = (com.fossil.iv7) r1
                        com.fossil.el7.b(r9)
                        r1 = r9
                        r3 = r0
                    L_0x0028:
                        r0 = r1
                        java.util.List r0 = (java.util.List) r0
                        com.fossil.cl7 r0 = com.fossil.hl7.a(r0, r3)
                        if (r0 == 0) goto L_0x00c7
                    L_0x0031:
                        return r0
                    L_0x0032:
                        java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                        java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                        r0.<init>(r1)
                        throw r0
                    L_0x003a:
                        java.lang.Object r0 = r8.L$0
                        com.fossil.iv7 r0 = (com.fossil.iv7) r0
                        com.fossil.el7.b(r9)
                        r2 = r0
                        r1 = r9
                    L_0x0043:
                        r0 = r1
                        com.fossil.ob7 r0 = (com.fossil.ob7) r0
                        if (r0 == 0) goto L_0x00c7
                        com.fossil.sa7$a$a r1 = r8.this$0
                        com.fossil.sa7$a r1 = r1.this$0
                        com.fossil.sa7 r1 = r1.f3228a
                        java.util.List r1 = com.fossil.sa7.n(r1)
                        if (r1 == 0) goto L_0x00c3
                        com.fossil.sa7$a$a r3 = r8.this$0
                        com.fossil.sa7$a r3 = r3.this$0
                        com.fossil.sa7 r3 = r3.f3228a
                        java.util.List r3 = com.fossil.sa7.s(r3)
                        if (r3 == 0) goto L_0x00bf
                        com.fossil.sa7$a$a r6 = r8.this$0
                        com.fossil.sa7$a r6 = r6.this$0
                        com.fossil.sa7 r6 = r6.f3228a
                        java.util.List r6 = com.fossil.sa7.o(r6)
                        if (r6 == 0) goto L_0x00bb
                        com.fossil.ub7 r6 = com.fossil.cc7.j(r0, r1, r3, r6)
                        com.fossil.hb7 r1 = r0.b()
                        if (r1 == 0) goto L_0x00b9
                        byte[] r1 = r1.a()
                        if (r1 == 0) goto L_0x00b9
                        android.graphics.Bitmap r1 = com.fossil.j37.c(r1)
                        r3 = r1
                    L_0x0081:
                        com.fossil.sa7$a$a r1 = r8.this$0
                        com.fossil.sa7$a r1 = r1.this$0
                        com.fossil.sa7 r1 = r1.f3228a
                        r8.L$0 = r2
                        r8.L$1 = r0
                        r8.L$2 = r0
                        r8.L$3 = r6
                        r8.L$4 = r3
                        r8.label = r7
                        java.lang.Object r1 = r1.z(r6, r8)
                        if (r1 != r5) goto L_0x0028
                        r0 = r5
                        goto L_0x0031
                    L_0x009b:
                        com.fossil.el7.b(r9)
                        com.fossil.iv7 r0 = r8.p$
                        com.fossil.sa7$a$a r1 = r8.this$0
                        com.fossil.sa7$a r1 = r1.this$0
                        com.fossil.sa7 r1 = r1.f3228a
                        com.portfolio.platform.PortfolioApp r1 = com.fossil.sa7.r(r1)
                        byte[] r2 = r8.$wfByteArray
                        r8.L$0 = r0
                        r8.label = r3
                        java.lang.Object r1 = r1.N0(r2, r8)
                        if (r1 != r5) goto L_0x00d1
                        r0 = r5
                        goto L_0x0031
                    L_0x00b9:
                        r3 = r4
                        goto L_0x0081
                    L_0x00bb:
                        com.fossil.pq7.i()
                        throw r4
                    L_0x00bf:
                        com.fossil.pq7.i()
                        throw r4
                    L_0x00c3:
                        com.fossil.pq7.i()
                        throw r4
                    L_0x00c7:
                        java.util.List r0 = com.fossil.hm7.e()
                        com.fossil.cl7 r0 = com.fossil.hl7.a(r0, r4)
                        goto L_0x0031
                    L_0x00d1:
                        r2 = r0
                        goto L_0x0043
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.fossil.sa7.a.C0218a.e.invokeSuspend(java.lang.Object):java.lang.Object");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0218a(List list, qn7 qn7, a aVar) {
                super(2, qn7);
                this.$it = list;
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0218a aVar = new C0218a(this.$it, qn7, this.this$0);
                aVar.p$ = (hs0) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<List<ka7.b>> hs0, qn7<? super tl7> qn7) {
                return ((C0218a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r2v18, types: [java.lang.Iterable] */
            /* JADX WARN: Type inference failed for: r3v21, types: [java.lang.Iterable] */
            /* JADX WARN: Type inference failed for: r2v32, types: [java.lang.Iterable] */
            /* JADX WARNING: Removed duplicated region for block: B:11:0x005d  */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x00b7  */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x0105  */
            /* JADX WARNING: Removed duplicated region for block: B:33:0x0191  */
            /* JADX WARNING: Removed duplicated region for block: B:40:0x01d4  */
            /* JADX WARNING: Removed duplicated region for block: B:47:0x0215  */
            /* JADX WARNING: Removed duplicated region for block: B:57:0x0278  */
            /* JADX WARNING: Removed duplicated region for block: B:67:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Unknown variable types count: 3 */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r16) {
                /*
                // Method dump skipped, instructions count: 670
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.sa7.a.C0218a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public a(sa7 sa7) {
            this.f3228a = sa7;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<List<ka7.b>> apply(List<? extends DianaWatchFaceUser> list) {
            FLogger.INSTANCE.getLocal().d("MyFaceViewModel", "face changes");
            return or0.c(null, 0, new C0218a(list, null, this), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.faces.page.MyFaceViewModel$_watchFaceListLiveData$1", f = "MyFaceViewModel.kt", l = {41, 41}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<hs0<List<? extends DianaWatchFaceUser>>, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public hs0 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sa7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(sa7 sa7, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sa7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (hs0) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(hs0<List<? extends DianaWatchFaceUser>> hs0, qn7<? super tl7> qn7) {
            return ((b) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 2
                r2 = 1
                java.lang.Object r4 = com.fossil.yn7.d()
                int r0 = r6.label
                if (r0 == 0) goto L_0x003c
                if (r0 == r2) goto L_0x0020
                if (r0 != r5) goto L_0x0018
                java.lang.Object r0 = r6.L$0
                com.fossil.hs0 r0 = (com.fossil.hs0) r0
                com.fossil.el7.b(r7)
            L_0x0015:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r6.L$1
                com.fossil.hs0 r0 = (com.fossil.hs0) r0
                java.lang.Object r1 = r6.L$0
                com.fossil.hs0 r1 = (com.fossil.hs0) r1
                com.fossil.el7.b(r7)
                r2 = r7
                r3 = r0
            L_0x002d:
                r0 = r2
                androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                r6.L$0 = r1
                r6.label = r5
                java.lang.Object r0 = r3.a(r0, r6)
                if (r0 != r4) goto L_0x0015
                r0 = r4
                goto L_0x0017
            L_0x003c:
                com.fossil.el7.b(r7)
                com.fossil.hs0 r0 = r6.p$
                com.fossil.sa7 r1 = r6.this$0
                com.portfolio.platform.data.source.DianaWatchFaceRepository r1 = com.fossil.sa7.p(r1)
                r6.L$0 = r0
                r6.L$1 = r0
                r6.label = r2
                java.lang.Object r2 = r1.getAllDianaWatchFaceUserLiveData(r6)
                if (r2 != r4) goto L_0x0055
                r0 = r4
                goto L_0x0017
            L_0x0055:
                r1 = r0
                r3 = r0
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.sa7.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public sa7(PortfolioApp portfolioApp, DianaWatchFaceRepository dianaWatchFaceRepository, FileRepository fileRepository, s77 s77, UserRepository userRepository, zm5 zm5, CustomizeRealDataRepository customizeRealDataRepository) {
        pq7.c(portfolioApp, "mApp");
        pq7.c(dianaWatchFaceRepository, "dianaWatchFaceRepository");
        pq7.c(fileRepository, "fileRepository");
        pq7.c(s77, "wfAssetRepository");
        pq7.c(userRepository, "userRepository");
        pq7.c(zm5, "customizeRealDataManager");
        pq7.c(customizeRealDataRepository, "customizeRealDataRepository");
        this.n = portfolioApp;
        this.o = dianaWatchFaceRepository;
        this.p = fileRepository;
        this.q = s77;
        this.r = userRepository;
        this.s = zm5;
        this.t = customizeRealDataRepository;
        LiveData<List<DianaWatchFaceUser>> c = or0.c(bw7.b(), 0, new b(this, null), 2, null);
        this.h = c;
        LiveData<List<ka7.b>> c2 = ss0.c(c, new a(this));
        pq7.b(c2, "Transformations.switchMap(this) { transform(it) }");
        this.m = c2;
    }

    @DexIgnore
    public final LiveData<List<ka7.b>> A() {
        return this.m;
    }

    @DexIgnore
    public final /* synthetic */ Object z(ub7 ub7, qn7<? super List<? extends s87>> qn7) {
        return t87.f3377a.b(ub7, this.q, this.t, this.r, this.s, qn7);
    }
}
