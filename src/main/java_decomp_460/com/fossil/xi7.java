package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xi7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;
    @DexIgnore
    public /* final */ /* synthetic */ jg7 c;
    @DexIgnore
    public /* final */ /* synthetic */ lg7 d;

    @DexIgnore
    public xi7(Context context, jg7 jg7, lg7 lg7) {
        this.b = context;
        this.c = jg7;
        this.d = lg7;
    }

    @DexIgnore
    public final void run() {
        try {
            kg7 kg7 = new kg7(this.b, ig7.a(this.b, false, this.c), this.d.f2191a, this.c);
            kg7.i().c = this.d.c;
            new ch7(kg7).b();
        } catch (Throwable th) {
            ig7.m.e(th);
            ig7.f(this.b, th);
        }
    }
}
