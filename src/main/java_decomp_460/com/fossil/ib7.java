package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ib7 implements gb7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public byte[] f1607a;
    @DexIgnore
    public String b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public String d;
    @DexIgnore
    public Integer e;
    @DexIgnore
    public nb7 f;
    @DexIgnore
    public jb7 g;
    @DexIgnore
    public lb7 h;
    @DexIgnore
    public int i;

    @DexIgnore
    public ib7(byte[] bArr, String str, boolean z, String str2, Integer num, nb7 nb7, jb7 jb7, lb7 lb7, int i2) {
        pq7.c(bArr, "ringByteData");
        pq7.c(str, "nameOfRing");
        pq7.c(nb7, "colour");
        pq7.c(jb7, "metric");
        pq7.c(lb7, "type");
        this.f1607a = bArr;
        this.b = str;
        this.c = z;
        this.d = str2;
        this.e = num;
        this.f = nb7;
        this.g = jb7;
        this.h = lb7;
        this.i = i2;
    }

    @DexIgnore
    @Override // com.fossil.gb7
    public int a() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.gb7
    public jb7 b() {
        return this.g;
    }

    @DexIgnore
    public final nb7 c() {
        return this.f;
    }

    @DexIgnore
    public final boolean d() {
        return this.c;
    }

    @DexIgnore
    public final String e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(ib7.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ib7 ib7 = (ib7) obj;
            if (!pq7.a(this.b, ib7.b)) {
                return false;
            }
            if (this.c != ib7.c) {
                return false;
            }
            if (!pq7.a(this.d, ib7.d)) {
                return false;
            }
            if (!pq7.a(this.e, ib7.e)) {
                return false;
            }
            if (this.f != ib7.f) {
                return false;
            }
            if (!pq7.a(b(), ib7.b())) {
                return false;
            }
            if (getType() != ib7.getType()) {
                return false;
            }
            return a() == ib7.a();
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.watchface.model.theme.communication.WFComplicationData");
    }

    @DexIgnore
    public final String f() {
        return this.b;
    }

    @DexIgnore
    public final Integer g() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.gb7
    public lb7 getType() {
        return this.h;
    }

    @DexIgnore
    public final byte[] h() {
        return this.f1607a;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        int hashCode = this.b.hashCode();
        int a2 = a.a(this.c);
        String str = this.d;
        int hashCode2 = str != null ? str.hashCode() : 0;
        Integer num = this.e;
        if (num != null) {
            i2 = num.intValue();
        }
        return ((((((((((hashCode2 + (((hashCode * 31) + a2) * 31)) * 31) + i2) * 31) + this.f.hashCode()) * 31) + b().hashCode()) * 31) + getType().hashCode()) * 31) + a();
    }

    @DexIgnore
    public final void i(String str) {
        this.d = str;
    }

    @DexIgnore
    public final void j(Integer num) {
        this.e = num;
    }

    @DexIgnore
    public final void k(byte[] bArr) {
        pq7.c(bArr, "<set-?>");
        this.f1607a = bArr;
    }

    @DexIgnore
    public String toString() {
        return "WFComplicationData(ringByteData=" + Arrays.toString(this.f1607a) + ", nameOfRing=" + this.b + ", enableRingGoal=" + this.c + ", location=" + this.d + ", offsetMin=" + this.e + ", colour=" + this.f + ", metric=" + b() + ", type=" + getType() + ", index=" + a() + ")";
    }
}
