package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w80 extends z80 {
    @DexIgnore
    public static long h; // = 100;
    @DexIgnore
    public static /* final */ w80 i; // = new w80();

    @DexIgnore
    public w80() {
        super("spg", 102400, 10485760, "spg", "spg", new zw1("", "", ""), 1800, new b90(), ld0.f, false);
    }

    @DexIgnore
    @Override // com.fossil.z80
    public long e() {
        return h;
    }
}
