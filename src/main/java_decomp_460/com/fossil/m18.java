package com.fossil;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class m18 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ m18 f2289a; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends m18 {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements c {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.m18.c
        public m18 a(a18 a18) {
            return m18.this;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        m18 a(a18 a18);
    }

    @DexIgnore
    public static c k(m18 m18) {
        return new b();
    }

    @DexIgnore
    public void a(a18 a18) {
    }

    @DexIgnore
    public void b(a18 a18, IOException iOException) {
    }

    @DexIgnore
    public void c(a18 a18) {
    }

    @DexIgnore
    public void d(a18 a18, InetSocketAddress inetSocketAddress, Proxy proxy, t18 t18) {
    }

    @DexIgnore
    public void e(a18 a18, InetSocketAddress inetSocketAddress, Proxy proxy, t18 t18, IOException iOException) {
    }

    @DexIgnore
    public void f(a18 a18, InetSocketAddress inetSocketAddress, Proxy proxy) {
    }

    @DexIgnore
    public void g(a18 a18, e18 e18) {
    }

    @DexIgnore
    public void h(a18 a18, e18 e18) {
    }

    @DexIgnore
    public void i(a18 a18, String str, List<InetAddress> list) {
    }

    @DexIgnore
    public void j(a18 a18, String str) {
    }

    @DexIgnore
    public void l(a18 a18, long j) {
    }

    @DexIgnore
    public void m(a18 a18) {
    }

    @DexIgnore
    public void n(a18 a18, v18 v18) {
    }

    @DexIgnore
    public void o(a18 a18) {
    }

    @DexIgnore
    public void p(a18 a18, long j) {
    }

    @DexIgnore
    public void q(a18 a18) {
    }

    @DexIgnore
    public void r(a18 a18, Response response) {
    }

    @DexIgnore
    public void s(a18 a18) {
    }

    @DexIgnore
    public void t(a18 a18, o18 o18) {
    }

    @DexIgnore
    public void u(a18 a18) {
    }
}
