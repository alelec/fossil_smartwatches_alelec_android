package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p62 {
    @DexIgnore
    public static /* final */ int API_NOT_CONNECTED; // = 17;
    @DexIgnore
    public static /* final */ int CANCELED; // = 16;
    @DexIgnore
    public static /* final */ int DEVELOPER_ERROR; // = 10;
    @DexIgnore
    public static /* final */ int ERROR; // = 13;
    @DexIgnore
    public static /* final */ int INTERNAL_ERROR; // = 8;
    @DexIgnore
    public static /* final */ int INTERRUPTED; // = 14;
    @DexIgnore
    public static /* final */ int INVALID_ACCOUNT; // = 5;
    @DexIgnore
    public static /* final */ int NETWORK_ERROR; // = 7;
    @DexIgnore
    public static /* final */ int RESOLUTION_REQUIRED; // = 6;
    @DexIgnore
    @Deprecated
    public static /* final */ int SERVICE_DISABLED; // = 3;
    @DexIgnore
    @Deprecated
    public static /* final */ int SERVICE_VERSION_UPDATE_REQUIRED; // = 2;
    @DexIgnore
    public static /* final */ int SIGN_IN_REQUIRED; // = 4;
    @DexIgnore
    public static /* final */ int SUCCESS; // = 0;
    @DexIgnore
    public static /* final */ int SUCCESS_CACHE; // = -1;
    @DexIgnore
    public static /* final */ int TIMEOUT; // = 15;

    @DexIgnore
    public static String getStatusCodeString(int i) {
        switch (i) {
            case -1:
                return "SUCCESS_CACHE";
            case 0:
                return "SUCCESS";
            case 1:
            case 9:
            case 11:
            case 12:
            default:
                StringBuilder sb = new StringBuilder(32);
                sb.append("unknown status code: ");
                sb.append(i);
                return sb.toString();
            case 2:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            case 3:
                return "SERVICE_DISABLED";
            case 4:
                return "SIGN_IN_REQUIRED";
            case 5:
                return "INVALID_ACCOUNT";
            case 6:
                return "RESOLUTION_REQUIRED";
            case 7:
                return "NETWORK_ERROR";
            case 8:
                return "INTERNAL_ERROR";
            case 10:
                return "DEVELOPER_ERROR";
            case 13:
                return "ERROR";
            case 14:
                return "INTERRUPTED";
            case 15:
                return "TIMEOUT";
            case 16:
                return "CANCELED";
            case 17:
                return "API_NOT_CONNECTED";
            case 18:
                return "DEAD_CLIENT";
        }
    }
}
