package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dm7 extends cm7 {
    @DexIgnore
    public static final <T> List<T> d(T[] tArr) {
        pq7.c(tArr, "$this$asList");
        List<T> a2 = fm7.a(tArr);
        pq7.b(a2, "ArraysUtilJVM.asList(this)");
        return a2;
    }

    @DexIgnore
    public static final int e(int[] iArr, int i, int i2, int i3) {
        pq7.c(iArr, "$this$binarySearch");
        return Arrays.binarySearch(iArr, i2, i3, i);
    }

    @DexIgnore
    public static /* synthetic */ int f(int[] iArr, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i2 = 0;
        }
        if ((i4 & 4) != 0) {
            i3 = iArr.length;
        }
        return e(iArr, i, i2, i3);
    }

    @DexIgnore
    public static final byte[] g(byte[] bArr, byte[] bArr2, int i, int i2, int i3) {
        pq7.c(bArr, "$this$copyInto");
        pq7.c(bArr2, ShareConstants.DESTINATION);
        System.arraycopy(bArr, i2, bArr2, i, i3 - i2);
        return bArr2;
    }

    @DexIgnore
    public static final <T> T[] h(T[] tArr, T[] tArr2, int i, int i2, int i3) {
        pq7.c(tArr, "$this$copyInto");
        pq7.c(tArr2, ShareConstants.DESTINATION);
        System.arraycopy(tArr, i2, tArr2, i, i3 - i2);
        return tArr2;
    }

    @DexIgnore
    public static /* synthetic */ byte[] i(byte[] bArr, byte[] bArr2, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i = 0;
        }
        if ((i4 & 4) != 0) {
            i2 = 0;
        }
        if ((i4 & 8) != 0) {
            i3 = bArr.length;
        }
        g(bArr, bArr2, i, i2, i3);
        return bArr2;
    }

    @DexIgnore
    public static /* synthetic */ Object[] j(Object[] objArr, Object[] objArr2, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i = 0;
        }
        if ((i4 & 4) != 0) {
            i2 = 0;
        }
        if ((i4 & 8) != 0) {
            i3 = objArr.length;
        }
        h(objArr, objArr2, i, i2, i3);
        return objArr2;
    }

    @DexIgnore
    public static final byte[] k(byte[] bArr, int i, int i2) {
        pq7.c(bArr, "$this$copyOfRangeImpl");
        bm7.b(i2, bArr.length);
        byte[] copyOfRange = Arrays.copyOfRange(bArr, i, i2);
        pq7.b(copyOfRange, "java.util.Arrays.copyOfR\u2026this, fromIndex, toIndex)");
        return copyOfRange;
    }

    @DexIgnore
    public static final <T> T[] l(T[] tArr, int i, int i2) {
        pq7.c(tArr, "$this$copyOfRangeImpl");
        bm7.b(i2, tArr.length);
        T[] tArr2 = (T[]) Arrays.copyOfRange(tArr, i, i2);
        pq7.b(tArr2, "java.util.Arrays.copyOfR\u2026this, fromIndex, toIndex)");
        return tArr2;
    }

    @DexIgnore
    public static final <T> void m(T[] tArr, T t, int i, int i2) {
        pq7.c(tArr, "$this$fill");
        Arrays.fill(tArr, i, i2, t);
    }

    @DexIgnore
    public static final <R> List<R> n(Object[] objArr, Class<R> cls) {
        pq7.c(objArr, "$this$filterIsInstance");
        pq7.c(cls, "klass");
        ArrayList arrayList = new ArrayList();
        o(objArr, arrayList, cls);
        return arrayList;
    }

    @DexIgnore
    public static final <C extends Collection<? super R>, R> C o(Object[] objArr, C c, Class<R> cls) {
        pq7.c(objArr, "$this$filterIsInstanceTo");
        pq7.c(c, ShareConstants.DESTINATION);
        pq7.c(cls, "klass");
        for (Object obj : objArr) {
            if (cls.isInstance(obj)) {
                c.add(obj);
            }
        }
        return c;
    }

    @DexIgnore
    public static final byte[] p(byte[] bArr, byte b) {
        pq7.c(bArr, "$this$plus");
        int length = bArr.length;
        byte[] copyOf = Arrays.copyOf(bArr, length + 1);
        copyOf[length] = (byte) b;
        pq7.b(copyOf, Constants.RESULT);
        return copyOf;
    }

    @DexIgnore
    public static final byte[] q(byte[] bArr, byte[] bArr2) {
        pq7.c(bArr, "$this$plus");
        pq7.c(bArr2, MessengerShareContentUtility.ELEMENTS);
        int length = bArr.length;
        int length2 = bArr2.length;
        byte[] copyOf = Arrays.copyOf(bArr, length + length2);
        System.arraycopy(bArr2, 0, copyOf, length, length2);
        pq7.b(copyOf, Constants.RESULT);
        return copyOf;
    }

    @DexIgnore
    public static final <T> T[] r(T[] tArr, T t) {
        pq7.c(tArr, "$this$plus");
        int length = tArr.length;
        T[] tArr2 = (T[]) Arrays.copyOf(tArr, length + 1);
        tArr2[length] = t;
        pq7.b(tArr2, Constants.RESULT);
        return tArr2;
    }

    @DexIgnore
    public static final <T> T[] s(T[] tArr, T[] tArr2) {
        pq7.c(tArr, "$this$plus");
        pq7.c(tArr2, MessengerShareContentUtility.ELEMENTS);
        int length = tArr.length;
        int length2 = tArr2.length;
        T[] tArr3 = (T[]) Arrays.copyOf(tArr, length + length2);
        System.arraycopy(tArr2, 0, tArr3, length, length2);
        pq7.b(tArr3, Constants.RESULT);
        return tArr3;
    }

    @DexIgnore
    public static final void t(int[] iArr) {
        pq7.c(iArr, "$this$sort");
        if (iArr.length > 1) {
            Arrays.sort(iArr);
        }
    }

    @DexIgnore
    public static final <T> void u(T[] tArr) {
        pq7.c(tArr, "$this$sort");
        if (tArr.length > 1) {
            Arrays.sort(tArr);
        }
    }

    @DexIgnore
    public static final <T> void v(T[] tArr, Comparator<? super T> comparator) {
        pq7.c(tArr, "$this$sortWith");
        pq7.c(comparator, "comparator");
        if (tArr.length > 1) {
            Arrays.sort(tArr, comparator);
        }
    }

    @DexIgnore
    public static final Integer[] w(int[] iArr) {
        pq7.c(iArr, "$this$toTypedArray");
        Integer[] numArr = new Integer[iArr.length];
        int length = iArr.length;
        for (int i = 0; i < length; i++) {
            numArr[i] = Integer.valueOf(iArr[i]);
        }
        return numArr;
    }
}
