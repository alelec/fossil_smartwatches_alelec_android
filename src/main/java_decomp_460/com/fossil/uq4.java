package com.fossil;

import com.fossil.tq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uq4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ vq4 f3633a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<R extends tq4.c, E extends tq4.a> implements tq4.d<R, E> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ tq4.d<R, E> f3634a;
        @DexIgnore
        public /* final */ uq4 b;

        @DexIgnore
        public a(tq4.d<R, E> dVar, uq4 uq4) {
            this.f3634a = dVar;
            this.b = uq4;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(E e) {
            this.b.c(e, this.f3634a);
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(R r) {
            this.b.d(r, this.f3634a);
        }
    }

    @DexIgnore
    public uq4(vq4 vq4) {
        this.f3633a = vq4;
    }

    @DexIgnore
    public static /* synthetic */ void b(tq4 tq4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UseCaseHandler", "execute: getIdlingResource = " + t37.b());
        tq4.c();
        if (!t37.b().a()) {
            t37.a();
        }
    }

    @DexIgnore
    public <Q extends tq4.b, R extends tq4.c, E extends tq4.a> void a(tq4<Q, R, E> tq4, Q q, tq4.d<R, E> dVar) {
        tq4.d(q);
        tq4.e(new a(dVar, this));
        t37.c();
        this.f3633a.execute(new mo4(tq4));
    }

    @DexIgnore
    public <R extends tq4.c, E extends tq4.a> void c(E e, tq4.d<R, E> dVar) {
        this.f3633a.b(e, dVar);
    }

    @DexIgnore
    public <R extends tq4.c, E extends tq4.a> void d(R r, tq4.d<R, E> dVar) {
        this.f3633a.a(r, dVar);
    }
}
