package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jv7 {
    @DexIgnore
    public static final iv7 a(tn7 tn7) {
        if (tn7.get(xw7.r) == null) {
            tn7 = tn7.plus(dx7.b(null, 1, null));
        }
        return new ez7(tn7);
    }

    @DexIgnore
    public static final iv7 b() {
        return new ez7(ux7.b(null, 1, null).plus(bw7.c()));
    }

    @DexIgnore
    public static final void c(iv7 iv7, CancellationException cancellationException) {
        xw7 xw7 = (xw7) iv7.h().get(xw7.r);
        if (xw7 != null) {
            xw7.D(cancellationException);
            return;
        }
        throw new IllegalStateException(("Scope cannot be cancelled because it does not have a job: " + iv7).toString());
    }

    @DexIgnore
    public static /* synthetic */ void d(iv7 iv7, CancellationException cancellationException, int i, Object obj) {
        if ((i & 1) != 0) {
            cancellationException = null;
        }
        c(iv7, cancellationException);
    }

    @DexIgnore
    public static final <R> Object e(vp7<? super iv7, ? super qn7<? super R>, ? extends Object> vp7, qn7<? super R> qn7) {
        tz7 tz7 = new tz7(qn7.getContext(), qn7);
        Object c = e08.c(tz7, tz7, vp7);
        if (c == yn7.d()) {
            go7.c(qn7);
        }
        return c;
    }

    @DexIgnore
    public static final void f(iv7 iv7) {
        bx7.g(iv7.h());
    }

    @DexIgnore
    public static final boolean g(iv7 iv7) {
        xw7 xw7 = (xw7) iv7.h().get(xw7.r);
        if (xw7 != null) {
            return xw7.isActive();
        }
        return true;
    }

    @DexIgnore
    public static final iv7 h(iv7 iv7, tn7 tn7) {
        return new ez7(iv7.h().plus(tn7));
    }
}
