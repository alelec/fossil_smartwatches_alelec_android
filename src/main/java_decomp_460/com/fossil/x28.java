package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x28 extends w18 {
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ k48 d;

    @DexIgnore
    public x28(String str, long j, k48 k48) {
        this.b = str;
        this.c = j;
        this.d = k48;
    }

    @DexIgnore
    @Override // com.fossil.w18
    public long contentLength() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.w18
    public r18 contentType() {
        String str = this.b;
        if (str != null) {
            return r18.d(str);
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.w18
    public k48 source() {
        return this.d;
    }
}
