package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gl1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ r8 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<gl1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final gl1[] a(byte[] bArr) {
            Parcelable a2;
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (i < (bArr.length - 1) - 2) {
                r8 a3 = r8.g.a(bArr[i]);
                int i2 = i + 1;
                int i3 = i2 + 2;
                ByteBuffer order = ByteBuffer.wrap(dm7.k(bArr, i2, i3)).order(ByteOrder.LITTLE_ENDIAN);
                pq7.b(order, "ByteBuffer.wrap(rawData\n\u2026(ByteOrder.LITTLE_ENDIAN)");
                int n = hy1.n(order.getShort());
                byte[] k = dm7.k(bArr, i3, i3 + n);
                if (a3 == null) {
                    a2 = null;
                } else {
                    int i4 = p8.b[a3.ordinal()];
                    if (i4 == 1) {
                        a2 = hl1.CREATOR.a(k);
                    } else if (i4 == 2) {
                        a2 = ql1.CREATOR.a(k);
                    } else if (i4 == 3) {
                        a2 = il1.CREATOR.a(k);
                    } else {
                        throw new al7();
                    }
                }
                if (a2 != null) {
                    arrayList.add(a2);
                }
                i += n + 3;
            }
            Object[] array = arrayList.toArray(new gl1[0]);
            if (array != null) {
                return (gl1[]) array;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public gl1 createFromParcel(Parcel parcel) {
            r8 r8Var = r8.values()[parcel.readInt()];
            parcel.setDataPosition(parcel.dataPosition() - 4);
            int i = p8.f2797a[r8Var.ordinal()];
            if (i == 1) {
                return hl1.CREATOR.createFromParcel(parcel);
            }
            if (i == 2) {
                return ql1.CREATOR.createFromParcel(parcel);
            }
            if (i == 3) {
                return il1.CREATOR.createFromParcel(parcel);
            }
            throw new al7();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public gl1[] newArray(int i) {
            return new gl1[i];
        }
    }

    @DexIgnore
    public gl1(r8 r8Var) {
        this.b = r8Var;
    }

    @DexIgnore
    public final byte[] a() {
        byte[] b2 = b();
        byte[] array = ByteBuffer.allocate(b2.length + 3).order(ByteOrder.LITTLE_ENDIAN).put(this.b.b).putShort((short) b2.length).put(b2).array();
        pq7.b(array, "ByteBuffer.allocate(SUB_\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public abstract byte[] b();

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((gl1) obj).b;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.AlarmSubEntry");
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(new JSONObject(), jd0.e, ey1.a(this.b));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b.ordinal());
        }
    }
}
