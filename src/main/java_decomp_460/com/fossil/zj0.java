package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.uj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zj0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static boolean[] f4479a; // = new boolean[3];

    @DexIgnore
    public static void a(int i, uj0 uj0) {
        boolean z = false;
        uj0.H0();
        ak0 f = uj0.s.f();
        ak0 f2 = uj0.t.f();
        ak0 f3 = uj0.u.f();
        ak0 f4 = uj0.v.f();
        boolean z2 = (i & 8) == 8;
        boolean z3 = uj0.C[0] == uj0.b.MATCH_CONSTRAINT && d(uj0, 0);
        if (!(f.h == 4 || f3.h == 4)) {
            if (uj0.C[0] == uj0.b.FIXED || (z3 && uj0.C() == 8)) {
                if (uj0.s.d == null && uj0.u.d == null) {
                    f.p(1);
                    f3.p(1);
                    if (z2) {
                        f3.j(f, 1, uj0.x());
                    } else {
                        f3.i(f, uj0.D());
                    }
                } else if (uj0.s.d != null && uj0.u.d == null) {
                    f.p(1);
                    f3.p(1);
                    if (z2) {
                        f3.j(f, 1, uj0.x());
                    } else {
                        f3.i(f, uj0.D());
                    }
                } else if (uj0.s.d == null && uj0.u.d != null) {
                    f.p(1);
                    f3.p(1);
                    f.i(f3, -uj0.D());
                    if (z2) {
                        f.j(f3, -1, uj0.x());
                    } else {
                        f.i(f3, -uj0.D());
                    }
                } else if (!(uj0.s.d == null || uj0.u.d == null)) {
                    f.p(2);
                    f3.p(2);
                    if (z2) {
                        uj0.x().a(f);
                        uj0.x().a(f3);
                        f.o(f3, -1, uj0.x());
                        f3.o(f, 1, uj0.x());
                    } else {
                        f.n(f3, (float) (-uj0.D()));
                        f3.n(f, (float) uj0.D());
                    }
                }
            } else if (z3) {
                int D = uj0.D();
                f.p(1);
                f3.p(1);
                if (uj0.s.d == null && uj0.u.d == null) {
                    if (z2) {
                        f3.j(f, 1, uj0.x());
                    } else {
                        f3.i(f, D);
                    }
                } else if (uj0.s.d == null || uj0.u.d != null) {
                    if (uj0.s.d != null || uj0.u.d == null) {
                        if (!(uj0.s.d == null || uj0.u.d == null)) {
                            if (z2) {
                                uj0.x().a(f);
                                uj0.x().a(f3);
                            }
                            if (uj0.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                f.p(3);
                                f3.p(3);
                                f.n(f3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                f3.n(f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            } else {
                                f.p(2);
                                f3.p(2);
                                f.n(f3, (float) (-D));
                                f3.n(f, (float) D);
                                uj0.y0(D);
                            }
                        }
                    } else if (z2) {
                        f.j(f3, -1, uj0.x());
                    } else {
                        f.i(f3, -D);
                    }
                } else if (z2) {
                    f3.j(f, 1, uj0.x());
                } else {
                    f3.i(f, D);
                }
            }
        }
        if (uj0.C[1] == uj0.b.MATCH_CONSTRAINT && d(uj0, 1)) {
            z = true;
        }
        if (f2.h != 4 && f4.h != 4) {
            if (uj0.C[1] == uj0.b.FIXED || (z && uj0.C() == 8)) {
                if (uj0.t.d == null && uj0.v.d == null) {
                    f2.p(1);
                    f4.p(1);
                    if (z2) {
                        f4.j(f2, 1, uj0.w());
                    } else {
                        f4.i(f2, uj0.r());
                    }
                    tj0 tj0 = uj0.w;
                    if (tj0.d != null) {
                        tj0.f().p(1);
                        f2.h(1, uj0.w.f(), -uj0.Q);
                    }
                } else if (uj0.t.d != null && uj0.v.d == null) {
                    f2.p(1);
                    f4.p(1);
                    if (z2) {
                        f4.j(f2, 1, uj0.w());
                    } else {
                        f4.i(f2, uj0.r());
                    }
                    if (uj0.Q > 0) {
                        uj0.w.f().h(1, f2, uj0.Q);
                    }
                } else if (uj0.t.d == null && uj0.v.d != null) {
                    f2.p(1);
                    f4.p(1);
                    if (z2) {
                        f2.j(f4, -1, uj0.w());
                    } else {
                        f2.i(f4, -uj0.r());
                    }
                    if (uj0.Q > 0) {
                        uj0.w.f().h(1, f2, uj0.Q);
                    }
                } else if (uj0.t.d != null && uj0.v.d != null) {
                    f2.p(2);
                    f4.p(2);
                    if (z2) {
                        f2.o(f4, -1, uj0.w());
                        f4.o(f2, 1, uj0.w());
                        uj0.w().a(f2);
                        uj0.x().a(f4);
                    } else {
                        f2.n(f4, (float) (-uj0.r()));
                        f4.n(f2, (float) uj0.r());
                    }
                    if (uj0.Q > 0) {
                        uj0.w.f().h(1, f2, uj0.Q);
                    }
                }
            } else if (z) {
                int r = uj0.r();
                f2.p(1);
                f4.p(1);
                if (uj0.t.d == null && uj0.v.d == null) {
                    if (z2) {
                        f4.j(f2, 1, uj0.w());
                    } else {
                        f4.i(f2, r);
                    }
                } else if (uj0.t.d == null || uj0.v.d != null) {
                    if (uj0.t.d != null || uj0.v.d == null) {
                        if (uj0.t.d != null && uj0.v.d != null) {
                            if (z2) {
                                uj0.w().a(f2);
                                uj0.x().a(f4);
                            }
                            if (uj0.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                f2.p(3);
                                f4.p(3);
                                f2.n(f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                f4.n(f2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                return;
                            }
                            f2.p(2);
                            f4.p(2);
                            f2.n(f4, (float) (-r));
                            f4.n(f2, (float) r);
                            uj0.b0(r);
                            if (uj0.Q > 0) {
                                uj0.w.f().h(1, f2, uj0.Q);
                            }
                        }
                    } else if (z2) {
                        f2.j(f4, -1, uj0.w());
                    } else {
                        f2.i(f4, -r);
                    }
                } else if (z2) {
                    f4.j(f2, 1, uj0.w());
                } else {
                    f4.i(f2, r);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00b0, code lost:
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c0, code lost:
        if (r4.f0 == 2) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0119, code lost:
        if (r0[r24].d.b == r13) goto L_0x011c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003a, code lost:
        if (r4.e0 == 2) goto L_0x003c;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:198:0x00e5 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean b(com.fossil.vj0 r21, com.fossil.kj0 r22, int r23, int r24, com.fossil.sj0 r25) {
        /*
        // Method dump skipped, instructions count: 972
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zj0.b(com.fossil.vj0, com.fossil.kj0, int, int, com.fossil.sj0):boolean");
    }

    @DexIgnore
    public static void c(vj0 vj0, kj0 kj0, uj0 uj0) {
        if (vj0.C[0] != uj0.b.WRAP_CONTENT && uj0.C[0] == uj0.b.MATCH_PARENT) {
            int i = uj0.s.e;
            int D = vj0.D() - uj0.u.e;
            tj0 tj0 = uj0.s;
            tj0.i = kj0.r(tj0);
            tj0 tj02 = uj0.u;
            tj02.i = kj0.r(tj02);
            kj0.f(uj0.s.i, i);
            kj0.f(uj0.u.i, D);
            uj0.f3600a = 2;
            uj0.f0(i, D);
        }
        if (vj0.C[1] != uj0.b.WRAP_CONTENT && uj0.C[1] == uj0.b.MATCH_PARENT) {
            int i2 = uj0.t.e;
            int r = vj0.r() - uj0.v.e;
            tj0 tj03 = uj0.t;
            tj03.i = kj0.r(tj03);
            tj0 tj04 = uj0.v;
            tj04.i = kj0.r(tj04);
            kj0.f(uj0.t.i, i2);
            kj0.f(uj0.v.i, r);
            if (uj0.Q > 0 || uj0.C() == 8) {
                tj0 tj05 = uj0.w;
                tj05.i = kj0.r(tj05);
                kj0.f(uj0.w.i, uj0.Q + i2);
            }
            uj0.b = 2;
            uj0.t0(i2, r);
        }
    }

    @DexIgnore
    public static boolean d(uj0 uj0, int i) {
        char c = 1;
        uj0.b[] bVarArr = uj0.C;
        if (bVarArr[i] != uj0.b.MATCH_CONSTRAINT) {
            return false;
        }
        if (uj0.G != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            if (i != 0) {
                c = 0;
            }
            if (bVarArr[c] == uj0.b.MATCH_CONSTRAINT) {
            }
            return false;
        }
        if (i == 0) {
            if (!(uj0.e == 0 && uj0.h == 0 && uj0.i == 0)) {
                return false;
            }
        } else if (!(uj0.f == 0 && uj0.k == 0 && uj0.l == 0)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static void e(uj0 uj0, int i, int i2) {
        int i3 = i * 2;
        int i4 = i3 + 1;
        uj0.A[i3].f().f = uj0.u().s.f();
        uj0.A[i3].f().g = (float) i2;
        uj0.A[i3].f().b = 1;
        uj0.A[i4].f().f = uj0.A[i3].f();
        uj0.A[i4].f().g = (float) uj0.t(i);
        uj0.A[i4].f().b = 1;
    }
}
