package com.fossil;

import com.fossil.wz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class g02 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract g02 a();

        @DexIgnore
        public abstract a b(ty1 ty1);

        @DexIgnore
        public abstract a c(uy1<?> uy1);

        @DexIgnore
        public abstract a d(wy1<?, byte[]> wy1);

        @DexIgnore
        public abstract a e(h02 h02);

        @DexIgnore
        public abstract a f(String str);
    }

    @DexIgnore
    public static a a() {
        return new wz1.b();
    }

    @DexIgnore
    public abstract ty1 b();

    @DexIgnore
    public abstract uy1<?> c();

    @DexIgnore
    public byte[] d() {
        return e().apply(c().b());
    }

    @DexIgnore
    public abstract wy1<?, byte[]> e();

    @DexIgnore
    public abstract h02 f();

    @DexIgnore
    public abstract String g();
}
