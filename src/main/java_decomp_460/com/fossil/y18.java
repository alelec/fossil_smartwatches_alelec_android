package com.fossil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum y18 {
    TLS_1_3("TLSv1.3"),
    TLS_1_2("TLSv1.2"),
    TLS_1_1("TLSv1.1"),
    TLS_1_0("TLSv1"),
    SSL_3_0("SSLv3");
    
    @DexIgnore
    public /* final */ String javaName;

    @DexIgnore
    public y18(String str) {
        this.javaName = str;
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0018  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.y18 forJavaName(java.lang.String r6) {
        /*
            r4 = 4
            r3 = 3
            r2 = 2
            r1 = 1
            int r0 = r6.hashCode()
            r5 = 79201641(0x4b88569, float:4.338071E-36)
            if (r0 == r5) goto L_0x004b
            r5 = 79923350(0x4c38896, float:4.5969714E-36)
            if (r0 == r5) goto L_0x0041
            switch(r0) {
                case -503070503: goto L_0x0037;
                case -503070502: goto L_0x002d;
                case -503070501: goto L_0x0023;
                default: goto L_0x0015;
            }
        L_0x0015:
            r0 = -1
        L_0x0016:
            if (r0 == 0) goto L_0x0075
            if (r0 == r1) goto L_0x0072
            if (r0 == r2) goto L_0x006f
            if (r0 == r3) goto L_0x006c
            if (r0 != r4) goto L_0x0055
            com.fossil.y18 r0 = com.fossil.y18.SSL_3_0
        L_0x0022:
            return r0
        L_0x0023:
            java.lang.String r0 = "TLSv1.3"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0015
            r0 = 0
            goto L_0x0016
        L_0x002d:
            java.lang.String r0 = "TLSv1.2"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0015
            r0 = r1
            goto L_0x0016
        L_0x0037:
            java.lang.String r0 = "TLSv1.1"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0015
            r0 = r2
            goto L_0x0016
        L_0x0041:
            java.lang.String r0 = "TLSv1"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0015
            r0 = r3
            goto L_0x0016
        L_0x004b:
            java.lang.String r0 = "SSLv3"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0015
            r0 = r4
            goto L_0x0016
        L_0x0055:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unexpected TLS version: "
            r0.append(r1)
            r0.append(r6)
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x006c:
            com.fossil.y18 r0 = com.fossil.y18.TLS_1_0
            goto L_0x0022
        L_0x006f:
            com.fossil.y18 r0 = com.fossil.y18.TLS_1_1
            goto L_0x0022
        L_0x0072:
            com.fossil.y18 r0 = com.fossil.y18.TLS_1_2
            goto L_0x0022
        L_0x0075:
            com.fossil.y18 r0 = com.fossil.y18.TLS_1_3
            goto L_0x0022
            switch-data {-503070503->0x0037, -503070502->0x002d, -503070501->0x0023, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.y18.forJavaName(java.lang.String):com.fossil.y18");
    }

    @DexIgnore
    public static List<y18> forJavaNames(String... strArr) {
        ArrayList arrayList = new ArrayList(strArr.length);
        for (String str : strArr) {
            arrayList.add(forJavaName(str));
        }
        return Collections.unmodifiableList(arrayList);
    }

    @DexIgnore
    public String javaName() {
        return this.javaName;
    }
}
