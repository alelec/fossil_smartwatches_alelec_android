package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum iz4 {
    JOIN_TOO_MUCH,
    NEED_ACTIVE_DEVICE,
    SUGGEST_FIND_FRIEND
}
