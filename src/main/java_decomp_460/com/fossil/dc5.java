package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dc5 extends cc5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d B; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray C;
    @DexIgnore
    public long A;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        C = sparseIntArray;
        sparseIntArray.put(2131362056, 1);
        C.put(2131363030, 2);
        C.put(2131363062, 3);
        C.put(2131363392, 4);
        C.put(2131363427, 5);
        C.put(2131363374, 6);
        C.put(2131363426, 7);
        C.put(2131363428, 8);
        C.put(2131363425, 9);
    }
    */

    @DexIgnore
    public dc5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 10, B, C));
    }

    @DexIgnore
    public dc5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[1], (ScrollView) objArr[0], (RecyclerView) objArr[2], (RecyclerView) objArr[3], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[8]);
        this.A = -1;
        this.r.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.A = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.A != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.A = 1;
        }
        w();
    }
}
