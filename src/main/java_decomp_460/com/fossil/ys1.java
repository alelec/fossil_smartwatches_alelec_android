package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ys1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ys1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ys1 createFromParcel(Parcel parcel) {
            return new ys1(parcel.readLong(), parcel.readLong());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ys1[] newArray(int i) {
            return new ys1[i];
        }
    }

    @DexIgnore
    public ys1(long j, long j2) {
        this.b = j;
        this.c = j2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final long getCaloriesOffset() {
        return this.c;
    }

    @DexIgnore
    public final long getStepOffset() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(new JSONObject(), jd0.E4, Long.valueOf(this.b)), jd0.U1, Long.valueOf(this.c));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.b);
        parcel.writeLong(this.c);
    }
}
