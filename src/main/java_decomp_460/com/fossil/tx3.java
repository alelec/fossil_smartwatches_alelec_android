package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ux3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tx3 {
    @DexIgnore
    public static /* final */ int j;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ a f3487a;
    @DexIgnore
    public /* final */ View b;
    @DexIgnore
    public /* final */ Path c; // = new Path();
    @DexIgnore
    public /* final */ Paint d; // = new Paint(7);
    @DexIgnore
    public /* final */ Paint e;
    @DexIgnore
    public ux3.e f;
    @DexIgnore
    public Drawable g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;

    @DexIgnore
    public interface a {
        @DexIgnore
        void c(Canvas canvas);

        @DexIgnore
        boolean d();
    }

    /*
    static {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 21) {
            j = 2;
        } else if (i2 >= 18) {
            j = 1;
        } else {
            j = 0;
        }
    }
    */

    @DexIgnore
    public tx3(a aVar) {
        this.f3487a = aVar;
        View view = (View) aVar;
        this.b = view;
        view.setWillNotDraw(false);
        Paint paint = new Paint(1);
        this.e = paint;
        paint.setColor(0);
    }

    @DexIgnore
    public void a() {
        if (j == 0) {
            this.h = true;
            this.i = false;
            this.b.buildDrawingCache();
            Bitmap drawingCache = this.b.getDrawingCache();
            if (!(drawingCache != null || this.b.getWidth() == 0 || this.b.getHeight() == 0)) {
                drawingCache = Bitmap.createBitmap(this.b.getWidth(), this.b.getHeight(), Bitmap.Config.ARGB_8888);
                this.b.draw(new Canvas(drawingCache));
            }
            if (drawingCache != null) {
                Paint paint = this.d;
                Shader.TileMode tileMode = Shader.TileMode.CLAMP;
                paint.setShader(new BitmapShader(drawingCache, tileMode, tileMode));
            }
            this.h = false;
            this.i = true;
        }
    }

    @DexIgnore
    public void b() {
        if (j == 0) {
            this.i = false;
            this.b.destroyDrawingCache();
            this.d.setShader(null);
            this.b.invalidate();
        }
    }

    @DexIgnore
    public void c(Canvas canvas) {
        if (n()) {
            int i2 = j;
            if (i2 == 0) {
                ux3.e eVar = this.f;
                canvas.drawCircle(eVar.f3659a, eVar.b, eVar.c, this.d);
                if (p()) {
                    ux3.e eVar2 = this.f;
                    canvas.drawCircle(eVar2.f3659a, eVar2.b, eVar2.c, this.e);
                }
            } else if (i2 == 1) {
                int save = canvas.save();
                canvas.clipPath(this.c);
                this.f3487a.c(canvas);
                if (p()) {
                    canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) this.b.getWidth(), (float) this.b.getHeight(), this.e);
                }
                canvas.restoreToCount(save);
            } else if (i2 == 2) {
                this.f3487a.c(canvas);
                if (p()) {
                    canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) this.b.getWidth(), (float) this.b.getHeight(), this.e);
                }
            } else {
                throw new IllegalStateException("Unsupported strategy " + j);
            }
        } else {
            this.f3487a.c(canvas);
            if (p()) {
                canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) this.b.getWidth(), (float) this.b.getHeight(), this.e);
            }
        }
        d(canvas);
    }

    @DexIgnore
    public final void d(Canvas canvas) {
        if (o()) {
            Rect bounds = this.g.getBounds();
            float width = this.f.f3659a - (((float) bounds.width()) / 2.0f);
            float height = this.f.b - (((float) bounds.height()) / 2.0f);
            canvas.translate(width, height);
            this.g.draw(canvas);
            canvas.translate(-width, -height);
        }
    }

    @DexIgnore
    public Drawable e() {
        return this.g;
    }

    @DexIgnore
    public int f() {
        return this.e.getColor();
    }

    @DexIgnore
    public final float g(ux3.e eVar) {
        return lz3.b(eVar.f3659a, eVar.b, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) this.b.getWidth(), (float) this.b.getHeight());
    }

    @DexIgnore
    public ux3.e h() {
        ux3.e eVar = this.f;
        if (eVar == null) {
            return null;
        }
        ux3.e eVar2 = new ux3.e(eVar);
        if (!eVar2.a()) {
            return eVar2;
        }
        eVar2.c = g(eVar2);
        return eVar2;
    }

    @DexIgnore
    public final void i() {
        if (j == 1) {
            this.c.rewind();
            ux3.e eVar = this.f;
            if (eVar != null) {
                this.c.addCircle(eVar.f3659a, eVar.b, eVar.c, Path.Direction.CW);
            }
        }
        this.b.invalidate();
    }

    @DexIgnore
    public boolean j() {
        return this.f3487a.d() && !n();
    }

    @DexIgnore
    public void k(Drawable drawable) {
        this.g = drawable;
        this.b.invalidate();
    }

    @DexIgnore
    public void l(int i2) {
        this.e.setColor(i2);
        this.b.invalidate();
    }

    @DexIgnore
    public void m(ux3.e eVar) {
        if (eVar == null) {
            this.f = null;
        } else {
            ux3.e eVar2 = this.f;
            if (eVar2 == null) {
                this.f = new ux3.e(eVar);
            } else {
                eVar2.c(eVar);
            }
            if (lz3.c(eVar.c, g(eVar), 1.0E-4f)) {
                this.f.c = Float.MAX_VALUE;
            }
        }
        i();
    }

    @DexIgnore
    public final boolean n() {
        ux3.e eVar = this.f;
        boolean z = eVar == null || eVar.a();
        return j == 0 ? !z && this.i : !z;
    }

    @DexIgnore
    public final boolean o() {
        return (this.h || this.g == null || this.f == null) ? false : true;
    }

    @DexIgnore
    public final boolean p() {
        return !this.h && Color.alpha(this.e.getColor()) != 0;
    }
}
