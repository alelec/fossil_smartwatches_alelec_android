package com.fossil;

import com.fossil.p18;
import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v18 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ q18 f3697a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ p18 c;
    @DexIgnore
    public /* final */ RequestBody d;
    @DexIgnore
    public /* final */ Map<Class<?>, Object> e;
    @DexIgnore
    public volatile z08 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public q18 f3698a;
        @DexIgnore
        public String b;
        @DexIgnore
        public p18.a c;
        @DexIgnore
        public RequestBody d;
        @DexIgnore
        public Map<Class<?>, Object> e;

        @DexIgnore
        public a() {
            this.e = Collections.emptyMap();
            this.b = "GET";
            this.c = new p18.a();
        }

        @DexIgnore
        public a(v18 v18) {
            this.e = Collections.emptyMap();
            this.f3698a = v18.f3697a;
            this.b = v18.b;
            this.d = v18.d;
            this.e = v18.e.isEmpty() ? Collections.emptyMap() : new LinkedHashMap<>(v18.e);
            this.c = v18.c.f();
        }

        @DexIgnore
        public a a(String str, String str2) {
            this.c.a(str, str2);
            return this;
        }

        @DexIgnore
        public v18 b() {
            if (this.f3698a != null) {
                return new v18(this);
            }
            throw new IllegalStateException("url == null");
        }

        @DexIgnore
        public a c(z08 z08) {
            String z082 = z08.toString();
            if (z082.isEmpty()) {
                i(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER);
            } else {
                e(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER, z082);
            }
            return this;
        }

        @DexIgnore
        public a d() {
            g("GET", null);
            return this;
        }

        @DexIgnore
        public a e(String str, String str2) {
            this.c.h(str, str2);
            return this;
        }

        @DexIgnore
        public a f(p18 p18) {
            this.c = p18.f();
            return this;
        }

        @DexIgnore
        public a g(String str, RequestBody requestBody) {
            if (str == null) {
                throw new NullPointerException("method == null");
            } else if (str.length() == 0) {
                throw new IllegalArgumentException("method.length() == 0");
            } else if (requestBody != null && !v28.b(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (requestBody != null || !v28.e(str)) {
                this.b = str;
                this.d = requestBody;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        @DexIgnore
        public a h(RequestBody requestBody) {
            g("POST", requestBody);
            return this;
        }

        @DexIgnore
        public a i(String str) {
            this.c.g(str);
            return this;
        }

        @DexIgnore
        public <T> a j(Class<? super T> cls, T t) {
            if (cls != null) {
                if (t == null) {
                    this.e.remove(cls);
                } else {
                    if (this.e.isEmpty()) {
                        this.e = new LinkedHashMap();
                    }
                    this.e.put(cls, cls.cast(t));
                }
                return this;
            }
            throw new NullPointerException("type == null");
        }

        @DexIgnore
        public a k(String str) {
            if (str != null) {
                if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                    str = "http:" + str.substring(3);
                } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                    str = "https:" + str.substring(4);
                }
                l(q18.l(str));
                return this;
            }
            throw new NullPointerException("url == null");
        }

        @DexIgnore
        public a l(q18 q18) {
            if (q18 != null) {
                this.f3698a = q18;
                return this;
            }
            throw new NullPointerException("url == null");
        }
    }

    @DexIgnore
    public v18(a aVar) {
        this.f3697a = aVar.f3698a;
        this.b = aVar.b;
        this.c = aVar.c.e();
        this.d = aVar.d;
        this.e = b28.v(aVar.e);
    }

    @DexIgnore
    public RequestBody a() {
        return this.d;
    }

    @DexIgnore
    public z08 b() {
        z08 z08 = this.f;
        if (z08 != null) {
            return z08;
        }
        z08 k = z08.k(this.c);
        this.f = k;
        return k;
    }

    @DexIgnore
    public String c(String str) {
        return this.c.c(str);
    }

    @DexIgnore
    public List<String> d(String str) {
        return this.c.j(str);
    }

    @DexIgnore
    public p18 e() {
        return this.c;
    }

    @DexIgnore
    public boolean f() {
        return this.f3697a.n();
    }

    @DexIgnore
    public String g() {
        return this.b;
    }

    @DexIgnore
    public a h() {
        return new a(this);
    }

    @DexIgnore
    public <T> T i(Class<? extends T> cls) {
        return (T) cls.cast(this.e.get(cls));
    }

    @DexIgnore
    public q18 j() {
        return this.f3697a;
    }

    @DexIgnore
    public String toString() {
        return "Request{method=" + this.b + ", url=" + this.f3697a + ", tags=" + this.e + '}';
    }
}
