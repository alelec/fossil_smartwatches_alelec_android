package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.r62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rr2 extends ec2<uq2> {
    @DexIgnore
    public /* final */ String E;
    @DexIgnore
    public /* final */ mr2<uq2> F; // = new sr2(this);

    @DexIgnore
    public rr2(Context context, Looper looper, r62.b bVar, r62.c cVar, String str, ac2 ac2) {
        super(context, looper, 23, ac2, bVar, cVar);
        this.E = str;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public Bundle F() {
        Bundle bundle = new Bundle();
        bundle.putString("client_name", this.E);
        return bundle;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public String p() {
        return "com.google.android.gms.location.internal.IGoogleLocationManagerService";
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public /* synthetic */ IInterface q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.internal.IGoogleLocationManagerService");
        return queryLocalInterface instanceof uq2 ? (uq2) queryLocalInterface : new vq2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.m62.f, com.fossil.ec2, com.fossil.yb2
    public int s() {
        return 11925000;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public String x() {
        return "com.google.android.location.internal.GoogleLocationManagerService.START";
    }
}
