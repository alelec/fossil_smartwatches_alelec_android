package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.nm1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gm extends lp {
    @DexIgnore
    public /* final */ boolean C; // = true;
    @DexIgnore
    public int D;
    @DexIgnore
    public /* final */ nm1 E; // = new nm1((byte) 26, nm1.a.MALE, 170, 65, nm1.b.LEFT_WRIST);
    @DexIgnore
    public /* final */ ArrayList<ow> F; // = by1.a(this.i, hm7.c(ow.DEVICE_CONFIG, ow.FILE_CONFIG));

    @DexIgnore
    public gm(k5 k5Var, i60 i60) {
        super(k5Var, i60, yp.L, null, false, 24);
    }

    @DexIgnore
    public static final /* synthetic */ void G(gm gmVar) {
        int i = gmVar.D;
        if (i < 2) {
            gmVar.D = i + 1;
            if (q3.f.f(gmVar.x.a())) {
                lp.h(gmVar, new nm(gmVar.w, gmVar.x, gmVar.E, true, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, gmVar.z, 32), new wj(gmVar), new ik(gmVar), new uk(gmVar), null, null, 48, null);
            } else {
                lp.i(gmVar, new cv(l8.c.b, gmVar.w, 0, 4), new hl(gmVar), new ul(gmVar), null, null, null, 56, null);
            }
        } else {
            gmVar.I();
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        lp.i(this, new vu(this.w), new xi(this), new jj(this), null, null, null, 56, null);
    }

    @DexIgnore
    public final void I() {
        u.f3499a.a(this.w.x);
        l(nr.a(this.v, null, zq.SUCCESS, null, null, 13));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public boolean t() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.F;
    }
}
