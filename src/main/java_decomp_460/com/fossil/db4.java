package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class db4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f762a;
    @DexIgnore
    public /* final */ a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        String a(File file) throws IOException;
    }

    @DexIgnore
    public db4(Context context, a aVar) {
        this.f762a = context;
        this.b = aVar;
    }

    @DexIgnore
    public static JSONObject c(String str, fb4 fb4) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("base_address", fb4.f1098a);
        jSONObject.put("size", fb4.b);
        jSONObject.put("name", fb4.d);
        jSONObject.put("uuid", str);
        return jSONObject;
    }

    @DexIgnore
    public static byte[] d(JSONArray jSONArray) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("binary_images", jSONArray);
            return jSONObject.toString().getBytes(Charset.forName("UTF-8"));
        } catch (JSONException e) {
            x74.f().j("Binary images string is null", e);
            return new byte[0];
        }
    }

    @DexIgnore
    public static boolean f(fb4 fb4) {
        return (fb4.c.indexOf(120) == -1 || fb4.d.indexOf(47) == -1) ? false : true;
    }

    @DexIgnore
    public byte[] a(BufferedReader bufferedReader) throws IOException {
        return d(h(bufferedReader));
    }

    @DexIgnore
    public final File b(File file) {
        if (Build.VERSION.SDK_INT < 9 || !file.getAbsolutePath().startsWith("/data")) {
            return file;
        }
        try {
            return new File(this.f762a.getPackageManager().getApplicationInfo(this.f762a.getPackageName(), 0).nativeLibraryDir, file.getName());
        } catch (PackageManager.NameNotFoundException e) {
            x74.f().e("Error getting ApplicationInfo", e);
            return file;
        }
    }

    @DexIgnore
    public final File e(String str) {
        File file = new File(str);
        return !file.exists() ? b(file) : file;
    }

    @DexIgnore
    public final JSONObject g(String str) {
        fb4 a2 = gb4.a(str);
        if (a2 == null || !f(a2)) {
            return null;
        }
        try {
            try {
                return c(this.b.a(e(a2.d)), a2);
            } catch (JSONException e) {
                x74.f().c("Could not create a binary image json string", e);
                return null;
            }
        } catch (IOException e2) {
            x74 f = x74.f();
            f.c("Could not generate ID for file " + a2.d, e2);
            return null;
        }
    }

    @DexIgnore
    public final JSONArray h(BufferedReader bufferedReader) throws IOException {
        JSONArray jSONArray = new JSONArray();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return jSONArray;
            }
            JSONObject g = g(readLine);
            if (g != null) {
                jSONArray.put(g);
            }
        }
    }
}
