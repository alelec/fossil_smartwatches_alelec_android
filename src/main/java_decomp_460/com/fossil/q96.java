package com.fossil;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.imagefilters.FilterType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q96 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f2943a;
    @DexIgnore
    public /* final */ ArrayList<a> b;
    @DexIgnore
    public c c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Bitmap f2944a;
        @DexIgnore
        public /* final */ FilterType b;

        @DexIgnore
        public a(Bitmap bitmap, FilterType filterType) {
            pq7.c(bitmap, "image");
            pq7.c(filterType, "type");
            this.f2944a = bitmap;
            this.b = filterType;
        }

        @DexIgnore
        public final Bitmap a() {
            return this.f2944a;
        }

        @DexIgnore
        public final FilterType b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!pq7.a(this.f2944a, aVar.f2944a) || !pq7.a(this.b, aVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            Bitmap bitmap = this.f2944a;
            int hashCode = bitmap != null ? bitmap.hashCode() : 0;
            FilterType filterType = this.b;
            if (filterType != null) {
                i = filterType.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "ImageFilter(image=" + this.f2944a + ", type=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public ImageView f2945a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public /* final */ /* synthetic */ q96 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                c i;
                if (!(this.b.d.getItemCount() <= this.b.getAdapterPosition() || this.b.getAdapterPosition() == -1 || (i = this.b.d.i()) == null)) {
                    Object obj = this.b.d.b.get(this.b.getAdapterPosition());
                    pq7.b(obj, "mFilterList[adapterPosition]");
                    i.k4((a) obj);
                }
                b bVar = this.b;
                bVar.d.n(bVar.getAdapterPosition());
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(q96 q96, View view) {
            super(view);
            pq7.c(view, "view");
            this.d = q96;
            View findViewById = view.findViewById(2131362671);
            pq7.b(findViewById, "view.findViewById(R.id.iv_background_preview)");
            this.f2945a = (ImageView) findViewById;
            View findViewById2 = view.findViewById(2131363370);
            pq7.b(findViewById2, "view.findViewById(R.id.tv_name)");
            this.b = (TextView) findViewById2;
            View findViewById3 = view.findViewById(2131363449);
            pq7.b(findViewById3, "view.findViewById(R.id.v_background_selected)");
            this.c = findViewById3;
            this.f2945a.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void a(a aVar, int i) {
            String c2;
            pq7.c(aVar, "imageFilter");
            this.f2945a.setImageBitmap(aVar.a());
            switch (r96.f3090a[aVar.b().ordinal()]) {
                case 1:
                    View view = this.itemView;
                    pq7.b(view, "itemView");
                    c2 = um5.c(view.getContext(), 2131886527);
                    break;
                case 2:
                    View view2 = this.itemView;
                    pq7.b(view2, "itemView");
                    c2 = um5.c(view2.getContext(), 2131886523);
                    break;
                case 3:
                    View view3 = this.itemView;
                    pq7.b(view3, "itemView");
                    c2 = um5.c(view3.getContext(), 2131886525);
                    break;
                case 4:
                    View view4 = this.itemView;
                    pq7.b(view4, "itemView");
                    c2 = um5.c(view4.getContext(), 2131886524);
                    break;
                case 5:
                    View view5 = this.itemView;
                    pq7.b(view5, "itemView");
                    c2 = um5.c(view5.getContext(), 2131886528);
                    break;
                case 6:
                    View view6 = this.itemView;
                    pq7.b(view6, "itemView");
                    c2 = um5.c(view6.getContext(), 2131886526);
                    break;
                default:
                    c2 = "";
                    break;
            }
            this.b.setText(c2);
            if (i == this.d.f2943a) {
                this.c.setVisibility(0);
            } else {
                this.c.setVisibility(8);
            }
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void k4(a aVar);
    }

    @DexIgnore
    public q96(ArrayList<a> arrayList, c cVar) {
        pq7.c(arrayList, "mFilterList");
        this.b = arrayList;
        this.c = cVar;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ q96(ArrayList arrayList, c cVar, int i, kq7 kq7) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : cVar);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public final c i() {
        return this.c;
    }

    @DexIgnore
    /* renamed from: j */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        if (getItemCount() > i && i != -1) {
            a aVar = this.b.get(i);
            pq7.b(aVar, "mFilterList[position]");
            bVar.a(aVar, i);
        }
    }

    @DexIgnore
    /* renamed from: k */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558683, viewGroup, false);
        pq7.b(inflate, "LayoutInflater.from(pare\u2026na_filter, parent, false)");
        return new b(this, inflate);
    }

    @DexIgnore
    public final void l(List<a> list) {
        pq7.c(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ImageFilterAdapter", "setData size = " + list.size());
        this.b.clear();
        this.b.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void m(c cVar) {
        this.c = cVar;
    }

    @DexIgnore
    public final void n(int i) {
        try {
            if (this.f2943a != i) {
                int i2 = this.f2943a;
                this.f2943a = i;
                notifyItemChanged(i);
                notifyItemChanged(i2);
            }
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().e("ImageFilterAdapter", e.getMessage());
            e.printStackTrace();
        }
    }
}
