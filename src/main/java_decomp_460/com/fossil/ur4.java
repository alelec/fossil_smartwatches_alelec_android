package com.fossil;

import android.content.Context;
import com.fossil.vr4;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ur4 implements vr4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<String> f3636a; // = hm7.i("Agree terms of use and privacy");

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean a() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean b() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean c() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean d() {
        return !pq7.a(h(), "CN");
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean e() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean f() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean g() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public String h() {
        return vr4.a.a(this);
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean i() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean j() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public void k(pv5 pv5) {
        pq7.c(pv5, "fragment");
        HelpActivity.a aVar = HelpActivity.B;
        Context requireContext = pv5.requireContext();
        pq7.b(requireContext, "fragment.requireContext()");
        aVar.a(requireContext);
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean l() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean m(List<String> list) {
        pq7.c(list, "agreeRequirements");
        return list.containsAll(this.f3636a);
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean n() {
        return false;
    }
}
