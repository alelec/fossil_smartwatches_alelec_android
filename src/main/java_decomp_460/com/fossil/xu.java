package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xu extends mu {
    @DexIgnore
    public se L; // = new se(0, 0, 0);
    @DexIgnore
    public long M; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public ve N;

    @DexIgnore
    public xu(ve veVar, k5 k5Var) {
        super(fu.i, hs.t, k5Var, 0, 8);
        this.N = veVar;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return gy1.c(super.A(), this.L.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        se seVar = new se(hy1.n(order.getShort(0)), hy1.n(order.getShort(2)), hy1.n(order.getShort(4)));
        this.L = seVar;
        return gy1.c(jSONObject, seVar.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.mu, com.fossil.ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.N.b).putShort((short) this.N.c).putShort((short) this.N.d).putShort((short) this.N.e).array();
        pq7.b(array, "ByteBuffer.allocate(8).o\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final se Q() {
        return this.L;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void f(long j) {
        this.M = j;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public long x() {
        return this.M;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return gy1.c(super.z(), this.N.toJSONObject());
    }
}
