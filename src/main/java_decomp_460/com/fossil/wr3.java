package com.fossil;

import com.fossil.ju2;
import com.fossil.lu2;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wr3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f3988a;
    @DexIgnore
    public int b;
    @DexIgnore
    public Boolean c;
    @DexIgnore
    public Boolean d;
    @DexIgnore
    public Long e;
    @DexIgnore
    public Long f;

    @DexIgnore
    public wr3(String str, int i) {
        this.f3988a = str;
        this.b = i;
    }

    @DexIgnore
    public static Boolean b(double d2, ju2 ju2) {
        try {
            return h(new BigDecimal(d2), ju2, Math.ulp(d2));
        } catch (NumberFormatException e2) {
            return null;
        }
    }

    @DexIgnore
    public static Boolean c(long j, ju2 ju2) {
        try {
            return h(new BigDecimal(j), ju2, 0.0d);
        } catch (NumberFormatException e2) {
            return null;
        }
    }

    @DexIgnore
    public static Boolean d(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() != z);
    }

    @DexIgnore
    public static Boolean e(String str, ju2 ju2) {
        if (!gr3.S(str)) {
            return null;
        }
        try {
            return h(new BigDecimal(str), ju2, 0.0d);
        } catch (NumberFormatException e2) {
            return null;
        }
    }

    @DexIgnore
    public static Boolean f(String str, lu2.a aVar, boolean z, String str2, List<String> list, String str3, kl3 kl3) {
        if (str == null) {
            return null;
        }
        if (aVar == lu2.a.IN_LIST) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z && aVar != lu2.a.REGEXP) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (sr3.f3289a[aVar.ordinal()]) {
            case 1:
                try {
                    return Boolean.valueOf(Pattern.compile(str3, z ? 0 : 66).matcher(str).matches());
                } catch (PatternSyntaxException e2) {
                    if (kl3 == null) {
                        return null;
                    }
                    kl3.I().b("Invalid regular expression in REGEXP audience filter. expression", str3);
                    return null;
                }
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    @DexIgnore
    public static Boolean g(String str, lu2 lu2, kl3 kl3) {
        List<String> list;
        String str2 = null;
        rc2.k(lu2);
        if (str == null || !lu2.C() || lu2.D() == lu2.a.UNKNOWN_MATCH_TYPE) {
            return null;
        }
        if (lu2.D() == lu2.a.IN_LIST) {
            if (lu2.K() == 0) {
                return null;
            }
        } else if (!lu2.E()) {
            return null;
        }
        lu2.a D = lu2.D();
        boolean I = lu2.I();
        String G = (I || D == lu2.a.REGEXP || D == lu2.a.IN_LIST) ? lu2.G() : lu2.G().toUpperCase(Locale.ENGLISH);
        if (lu2.K() == 0) {
            list = null;
        } else {
            List<String> J = lu2.J();
            if (!I) {
                ArrayList arrayList = new ArrayList(J.size());
                for (String str3 : J) {
                    arrayList.add(str3.toUpperCase(Locale.ENGLISH));
                }
                J = Collections.unmodifiableList(arrayList);
            }
            list = J;
        }
        if (D == lu2.a.REGEXP) {
            str2 = G;
        }
        return f(str, D, I, G, list, str2, kl3);
    }

    @DexIgnore
    public static Boolean h(BigDecimal bigDecimal, ju2 ju2, double d2) {
        BigDecimal bigDecimal2;
        BigDecimal bigDecimal3;
        BigDecimal bigDecimal4;
        boolean z = false;
        boolean z2 = true;
        rc2.k(ju2);
        if (!ju2.C() || ju2.D() == ju2.b.UNKNOWN_COMPARISON_TYPE) {
            return null;
        }
        if (ju2.D() == ju2.b.BETWEEN) {
            if (!ju2.J() || !ju2.L()) {
                return null;
            }
        } else if (!ju2.H()) {
            return null;
        }
        ju2.b D = ju2.D();
        if (ju2.D() == ju2.b.BETWEEN) {
            if (!gr3.S(ju2.K()) || !gr3.S(ju2.M())) {
                return null;
            }
            try {
                bigDecimal4 = new BigDecimal(ju2.K());
                bigDecimal3 = new BigDecimal(ju2.M());
                bigDecimal2 = null;
            } catch (NumberFormatException e2) {
                return null;
            }
        } else if (!gr3.S(ju2.I())) {
            return null;
        } else {
            try {
                bigDecimal2 = new BigDecimal(ju2.I());
                bigDecimal3 = null;
                bigDecimal4 = null;
            } catch (NumberFormatException e3) {
                return null;
            }
        }
        if (D == ju2.b.BETWEEN) {
            if (bigDecimal4 == null) {
                return null;
            }
        } else if (bigDecimal2 == null) {
            return null;
        }
        int i = sr3.b[D.ordinal()];
        if (i == 1) {
            if (bigDecimal.compareTo(bigDecimal2) != -1) {
                z2 = false;
            }
            return Boolean.valueOf(z2);
        } else if (i == 2) {
            if (bigDecimal.compareTo(bigDecimal2) != 1) {
                z2 = false;
            }
            return Boolean.valueOf(z2);
        } else if (i != 3) {
            if (i != 4) {
                return null;
            }
            return Boolean.valueOf((bigDecimal.compareTo(bigDecimal4) == -1 || bigDecimal.compareTo(bigDecimal3) == 1) ? false : true);
        } else if (d2 != 0.0d) {
            if (bigDecimal.compareTo(bigDecimal2.subtract(new BigDecimal(d2).multiply(new BigDecimal(2)))) == 1 && bigDecimal.compareTo(bigDecimal2.add(new BigDecimal(d2).multiply(new BigDecimal(2)))) == -1) {
                z = true;
            }
            return Boolean.valueOf(z);
        } else {
            if (bigDecimal.compareTo(bigDecimal2) != 0) {
                z2 = false;
            }
            return Boolean.valueOf(z2);
        }
    }

    @DexIgnore
    public abstract int a();

    @DexIgnore
    public abstract boolean i();

    @DexIgnore
    public abstract boolean j();
}
