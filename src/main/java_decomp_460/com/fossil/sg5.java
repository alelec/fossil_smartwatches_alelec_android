package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.watchface.WatchFaceEditorView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sg5 extends rg5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d N; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray O;
    @DexIgnore
    public long M;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        O = sparseIntArray;
        sparseIntArray.put(2131363291, 1);
        O.put(2131363376, 2);
        O.put(2131362281, 3);
        O.put(2131362262, 4);
        O.put(2131363535, 5);
        O.put(2131362118, 6);
        O.put(2131363238, 7);
        O.put(2131362617, 8);
        O.put(2131362319, 9);
        O.put(2131362829, 10);
        O.put(2131363528, 11);
        O.put(2131363527, 12);
        O.put(2131362318, 13);
        O.put(2131363221, 14);
        O.put(2131362836, 15);
        O.put(2131363491, 16);
        O.put(2131363489, 17);
        O.put(2131363490, 18);
        O.put(2131363488, 19);
        O.put(2131363487, 20);
        O.put(2131363532, 21);
    }
    */

    @DexIgnore
    public sg5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 22, N, O));
    }

    @DexIgnore
    public sg5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[6], (FlexibleButton) objArr[4], (RTLImageView) objArr[3], (FrameLayout) objArr[13], (FlexibleTextView) objArr[9], (RTLImageView) objArr[8], (LinearLayout) objArr[10], (LinearLayout) objArr[15], (ConstraintLayout) objArr[0], (BottomNavigationView) objArr[14], (View) objArr[7], (RTLImageView) objArr[1], (FlexibleTextView) objArr[2], (View) objArr[20], (View) objArr[19], (View) objArr[17], (View) objArr[18], (View) objArr[16], (ViewPager2) objArr[12], (ImageView) objArr[11], (View) objArr[5], (WatchFaceEditorView) objArr[21]);
        this.M = -1;
        this.y.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.M = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.M != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.M = 1;
        }
        w();
    }
}
