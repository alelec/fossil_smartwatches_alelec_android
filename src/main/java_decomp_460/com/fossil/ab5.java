package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ab5 extends za5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d B; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray C;
    @DexIgnore
    public long A;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        C = sparseIntArray;
        sparseIntArray.put(2131361906, 1);
        C.put(2131362546, 2);
        C.put(2131363097, 3);
        C.put(2131362129, 4);
        C.put(2131362468, 5);
        C.put(2131362783, 6);
        C.put(2131362395, 7);
        C.put(2131363214, 8);
        C.put(2131363064, 9);
    }
    */

    @DexIgnore
    public ab5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 10, B, C));
    }

    @DexIgnore
    public ab5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (RTLImageView) objArr[1], (ImageView) objArr[4], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[2], (View) objArr[6], (ConstraintLayout) objArr[0], (RecyclerViewAlphabetIndex) objArr[9], (FlexibleEditText) objArr[3], (RecyclerView) objArr[8]);
        this.A = -1;
        this.w.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.A = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.A != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.A = 1;
        }
        w();
    }
}
