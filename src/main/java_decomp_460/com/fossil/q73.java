package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q73 implements n73 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f2934a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.referrer.enable_logging_install_referrer_cmp_from_apk", true);

    @DexIgnore
    @Override // com.fossil.n73
    public final boolean zza() {
        return f2934a.o().booleanValue();
    }
}
