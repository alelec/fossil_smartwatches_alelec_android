package com.fossil;

import android.content.Intent;
import android.text.TextUtils;
import android.util.SparseArray;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.manager.SoLibraryLoader;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dt5 extends iq4<tt5, ut5, st5> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public /* final */ a d; // = new a(this);
    @DexIgnore
    public /* final */ MicroAppRepository e;
    @DexIgnore
    public /* final */ on5 f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ PortfolioApp h;
    @DexIgnore
    public /* final */ HybridPresetRepository i;
    @DexIgnore
    public /* final */ NotificationsRepository j;
    @DexIgnore
    public /* final */ ck5 k;
    @DexIgnore
    public /* final */ AlarmsRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements wq5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ dt5 f831a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(dt5 dt5) {
            this.f831a = dt5;
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && vt7.j(stringExtra, this.f831a.h.J(), true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                if (intExtra == 1) {
                    FLogger.INSTANCE.getLocal().d(dt5.m, "sync success, remove device now");
                    wq5.d.j(this, CommunicateMode.SYNC);
                    this.f831a.j(new ut5());
                } else if (intExtra == 2 || intExtra == 4) {
                    wq5.d.j(this, CommunicateMode.SYNC);
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>();
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = dt5.m;
                    local.d(str, "sync fail due to " + intExtra2);
                    if (intExtra2 == 1101 || intExtra2 == 1112 || intExtra2 == 1113) {
                        this.f831a.i(new st5(zh5.FAIL_DUE_TO_LACK_PERMISSION, integerArrayListExtra));
                    } else {
                        this.f831a.i(new st5(zh5.FAIL_DUE_TO_SYNC_FAIL, null));
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase", f = "HybridSyncUseCase.kt", l = {85, 93, 132}, m = "run")
    public static final class b extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public boolean Z$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ dt5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(dt5 dt5, qn7 qn7) {
            super(qn7);
            this.this$0 = dt5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase$run$isAA$1", f = "HybridSyncUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        public c(qn7 qn7) {
            super(2, qn7);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return ao7.a(SoLibraryLoader.f().c(PortfolioApp.h0.c()) != null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase$saveNotificationSettingToDevice$1", f = "HybridSyncUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ SparseArray $data;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isMovemberModel;
        @DexIgnore
        public /* final */ /* synthetic */ String $serialNumber;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dt5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(dt5 dt5, SparseArray sparseArray, boolean z, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = dt5;
            this.$data = sparseArray;
            this.$isMovemberModel = z;
            this.$serialNumber = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$data, this.$isMovemberModel, this.$serialNumber, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                AppNotificationFilterSettings b = e47.b.b(this.$data, this.$isMovemberModel);
                PortfolioApp.h0.c().s1(b, this.$serialNumber);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String h = this.this$0.h();
                local.d(h, "saveNotificationSettingToDevice, total: " + b.getNotificationFilters().size() + " items");
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = dt5.class.getSimpleName();
        pq7.b(simpleName, "HybridSyncUseCase::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public dt5(MicroAppRepository microAppRepository, on5 on5, DeviceRepository deviceRepository, PortfolioApp portfolioApp, HybridPresetRepository hybridPresetRepository, NotificationsRepository notificationsRepository, ck5 ck5, AlarmsRepository alarmsRepository) {
        pq7.c(microAppRepository, "mMicroAppRepository");
        pq7.c(on5, "mSharedPrefs");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(portfolioApp, "mApp");
        pq7.c(hybridPresetRepository, "mPresetRepository");
        pq7.c(notificationsRepository, "mNotificationsRepository");
        pq7.c(ck5, "mAnalyticsHelper");
        pq7.c(alarmsRepository, "mAlarmsRepository");
        this.e = microAppRepository;
        this.f = on5;
        this.g = deviceRepository;
        this.h = portfolioApp;
        this.i = hybridPresetRepository;
        this.j = notificationsRepository;
        this.k = ck5;
        this.l = alarmsRepository;
    }

    @DexIgnore
    public static /* synthetic */ void p(dt5 dt5, String str, Integer num, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            num = 2;
        }
        dt5.o(str, num);
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "HybridSyncUseCase";
    }

    @DexIgnore
    public final void o(String str, Integer num) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, "broadcastSyncFail serial=" + str);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", num);
        intent.putExtra("SERIAL", str);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0055 A[Catch:{ Exception -> 0x0111 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0091 A[Catch:{ Exception -> 0x0111 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0179  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01c3  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01ff  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x022e  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x024a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0025  */
    /* renamed from: q */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.tt5 r15, com.fossil.qn7<java.lang.Object> r16) {
        /*
        // Method dump skipped, instructions count: 987
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.dt5.k(com.fossil.tt5, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final xw7 r(SparseArray<List<BaseFeatureModel>> sparseArray, String str, boolean z) {
        return gu7.d(jv7.a(bw7.a()), null, null, new d(this, sparseArray, z, str, null), 3, null);
    }

    @DexIgnore
    public final void s(int i2, boolean z, UserProfile userProfile, String str) {
        if (f() != null) {
            wq5.d.e(this.d, CommunicateMode.SYNC);
        }
        userProfile.setNewDevice(z);
        userProfile.setSyncMode(i2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, ".startDeviceSync - syncMode=" + i2);
        if (!this.f.z0()) {
            SKUModel skuModelBySerialPrefix = this.g.getSkuModelBySerialPrefix(nk5.o.m(str));
            this.f.t1(Boolean.TRUE);
            this.k.o(i2, skuModelBySerialPrefix);
            ul5 c2 = ck5.f.c("sync_session");
            ck5.f.a("sync_session", c2);
            c2.i();
        }
        if (!PortfolioApp.h0.c().T1(str, userProfile)) {
            p(this, str, null, 2, null);
        } else if (i2 == 13) {
            this.f.Z0(str, System.currentTimeMillis(), false);
        }
    }
}
