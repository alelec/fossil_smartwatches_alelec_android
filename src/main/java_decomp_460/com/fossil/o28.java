package com.fossil;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o28 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ x08 f2624a;
    @DexIgnore
    public /* final */ m28 b;
    @DexIgnore
    public /* final */ a18 c;
    @DexIgnore
    public /* final */ m18 d;
    @DexIgnore
    public List<Proxy> e; // = Collections.emptyList();
    @DexIgnore
    public int f;
    @DexIgnore
    public List<InetSocketAddress> g; // = Collections.emptyList();
    @DexIgnore
    public /* final */ List<x18> h; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<x18> f2625a;
        @DexIgnore
        public int b; // = 0;

        @DexIgnore
        public a(List<x18> list) {
            this.f2625a = list;
        }

        @DexIgnore
        public List<x18> a() {
            return new ArrayList(this.f2625a);
        }

        @DexIgnore
        public boolean b() {
            return this.b < this.f2625a.size();
        }

        @DexIgnore
        public x18 c() {
            if (b()) {
                List<x18> list = this.f2625a;
                int i = this.b;
                this.b = i + 1;
                return list.get(i);
            }
            throw new NoSuchElementException();
        }
    }

    @DexIgnore
    public o28(x08 x08, m28 m28, a18 a18, m18 m18) {
        this.f2624a = x08;
        this.b = m28;
        this.c = a18;
        this.d = m18;
        h(x08.l(), x08.g());
    }

    @DexIgnore
    public static String b(InetSocketAddress inetSocketAddress) {
        InetAddress address = inetSocketAddress.getAddress();
        return address == null ? inetSocketAddress.getHostName() : address.getHostAddress();
    }

    @DexIgnore
    public void a(x18 x18, IOException iOException) {
        if (!(x18.b().type() == Proxy.Type.DIRECT || this.f2624a.i() == null)) {
            this.f2624a.i().connectFailed(this.f2624a.l().F(), x18.b().address(), iOException);
        }
        this.b.b(x18);
    }

    @DexIgnore
    public boolean c() {
        return d() || !this.h.isEmpty();
    }

    @DexIgnore
    public final boolean d() {
        return this.f < this.e.size();
    }

    @DexIgnore
    public a e() throws IOException {
        if (c()) {
            ArrayList arrayList = new ArrayList();
            while (d()) {
                Proxy f2 = f();
                int size = this.g.size();
                for (int i = 0; i < size; i++) {
                    x18 x18 = new x18(this.f2624a, f2, this.g.get(i));
                    if (this.b.c(x18)) {
                        this.h.add(x18);
                    } else {
                        arrayList.add(x18);
                    }
                }
                if (!arrayList.isEmpty()) {
                    break;
                }
            }
            if (arrayList.isEmpty()) {
                arrayList.addAll(this.h);
                this.h.clear();
            }
            return new a(arrayList);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public final Proxy f() throws IOException {
        if (d()) {
            List<Proxy> list = this.e;
            int i = this.f;
            this.f = i + 1;
            Proxy proxy = list.get(i);
            g(proxy);
            return proxy;
        }
        throw new SocketException("No route to " + this.f2624a.l().m() + "; exhausted proxy configurations: " + this.e);
    }

    @DexIgnore
    public final void g(Proxy proxy) throws IOException {
        int i;
        String str;
        this.g = new ArrayList();
        if (proxy.type() == Proxy.Type.DIRECT || proxy.type() == Proxy.Type.SOCKS) {
            str = this.f2624a.l().m();
            i = this.f2624a.l().z();
        } else {
            SocketAddress address = proxy.address();
            if (address instanceof InetSocketAddress) {
                InetSocketAddress inetSocketAddress = (InetSocketAddress) address;
                str = b(inetSocketAddress);
                i = inetSocketAddress.getPort();
            } else {
                throw new IllegalArgumentException("Proxy.address() is not an InetSocketAddress: " + address.getClass());
            }
        }
        if (i < 1 || i > 65535) {
            throw new SocketException("No route to " + str + ":" + i + "; port is out of range");
        } else if (proxy.type() == Proxy.Type.SOCKS) {
            this.g.add(InetSocketAddress.createUnresolved(str, i));
        } else {
            this.d.j(this.c, str);
            List<InetAddress> a2 = this.f2624a.c().a(str);
            if (!a2.isEmpty()) {
                this.d.i(this.c, str, a2);
                int size = a2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    this.g.add(new InetSocketAddress(a2.get(i2), i));
                }
                return;
            }
            throw new UnknownHostException(this.f2624a.c() + " returned no addresses for " + str);
        }
    }

    @DexIgnore
    public final void h(q18 q18, Proxy proxy) {
        List<Proxy> u;
        if (proxy != null) {
            this.e = Collections.singletonList(proxy);
        } else {
            List<Proxy> select = this.f2624a.i().select(q18.F());
            if (select == null || select.isEmpty()) {
                u = b28.u(Proxy.NO_PROXY);
            } else {
                u = b28.t(select);
            }
            this.e = u;
        }
        this.f = 0;
    }
}
