package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.util.StateSet;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gz3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ArrayList<b> f1396a; // = new ArrayList<>();
    @DexIgnore
    public b b; // = null;
    @DexIgnore
    public ValueAnimator c; // = null;
    @DexIgnore
    public /* final */ Animator.AnimatorListener d; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends AnimatorListenerAdapter {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            gz3 gz3 = gz3.this;
            if (gz3.c == animator) {
                gz3.c = null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int[] f1398a;
        @DexIgnore
        public /* final */ ValueAnimator b;

        @DexIgnore
        public b(int[] iArr, ValueAnimator valueAnimator) {
            this.f1398a = iArr;
            this.b = valueAnimator;
        }
    }

    @DexIgnore
    public void a(int[] iArr, ValueAnimator valueAnimator) {
        b bVar = new b(iArr, valueAnimator);
        valueAnimator.addListener(this.d);
        this.f1396a.add(bVar);
    }

    @DexIgnore
    public final void b() {
        ValueAnimator valueAnimator = this.c;
        if (valueAnimator != null) {
            valueAnimator.cancel();
            this.c = null;
        }
    }

    @DexIgnore
    public void c() {
        ValueAnimator valueAnimator = this.c;
        if (valueAnimator != null) {
            valueAnimator.end();
            this.c = null;
        }
    }

    @DexIgnore
    public void d(int[] iArr) {
        b bVar;
        int size = this.f1396a.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                bVar = null;
                break;
            }
            bVar = this.f1396a.get(i);
            if (StateSet.stateSetMatches(bVar.f1398a, iArr)) {
                break;
            }
            i++;
        }
        b bVar2 = this.b;
        if (bVar != bVar2) {
            if (bVar2 != null) {
                b();
            }
            this.b = bVar;
            if (bVar != null) {
                e(bVar);
            }
        }
    }

    @DexIgnore
    public final void e(b bVar) {
        ValueAnimator valueAnimator = bVar.b;
        this.c = valueAnimator;
        valueAnimator.start();
    }
}
