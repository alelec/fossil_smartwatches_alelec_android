package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cz4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f702a;
        @DexIgnore
        public T b;
        @DexIgnore
        public /* final */ /* synthetic */ js0 c;

        @DexIgnore
        public a(js0 js0) {
            this.c = js0;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public void onChanged(T t) {
            if (!this.f702a) {
                this.f702a = true;
                this.b = t;
                this.c.l(t);
            } else if ((t == null && this.b != null) || (!pq7.a(t, this.b))) {
                this.b = t;
                this.c.l(t);
            }
        }
    }

    @DexIgnore
    public static final <T> LiveData<T> a(LiveData<T> liveData) {
        pq7.c(liveData, "$this$distinct");
        js0 js0 = new js0();
        js0.p(liveData, new a(js0));
        return js0;
    }
}
