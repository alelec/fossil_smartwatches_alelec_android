package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vc6 extends qc6 {
    @DexIgnore
    public String e; // = "empty";
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h;
    @DexIgnore
    public /* final */ ArrayList<MicroApp> i; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ rc6 k;
    @DexIgnore
    public /* final */ MicroAppRepository l;
    @DexIgnore
    public /* final */ on5 m;
    @DexIgnore
    public /* final */ PortfolioApp n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$search$1", f = "SearchMicroAppPresenter.kt", l = {79}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $query;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vc6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vc6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$search$1$1", f = "SearchMicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.vc6$a$a  reason: collision with other inner class name */
        public static final class C0261a extends ko7 implements vp7<iv7, qn7<? super List<MicroApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0261a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0261a aVar = new C0261a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<MicroApp>> qn7) {
                return ((C0261a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    MicroAppRepository microAppRepository = this.this$0.this$0.l;
                    a aVar = this.this$0;
                    return pm7.j0(microAppRepository.queryMicroAppByName(aVar.$query, aVar.this$0.n.J()));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(vc6 vc6, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = vc6;
            this.$query = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$query, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            List arrayList;
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                arrayList = new ArrayList();
                if (this.$query.length() > 0) {
                    dv7 i2 = this.this$0.i();
                    C0261a aVar = new C0261a(this, null);
                    this.L$0 = iv7;
                    this.L$1 = arrayList;
                    this.label = 1;
                    g = eu7.g(i2, aVar, this);
                    if (g == d) {
                        return d;
                    }
                }
                this.this$0.k.B(this.this$0.D(arrayList));
                this.this$0.h = this.$query;
                return tl7.f3441a;
            } else if (i == 1) {
                List list = (List) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            arrayList = (List) g;
            this.this$0.k.B(this.this$0.D(arrayList));
            this.this$0.h = this.$query;
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$start$1", f = "SearchMicroAppPresenter.kt", l = {40, 44}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vc6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$start$1$allMicroApps$1", f = "SearchMicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends MicroApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends MicroApp>> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.l.getAllMicroApp(PortfolioApp.h0.c().J());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vc6$b$b")
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$start$1$allSearchedMicroApps$1", f = "SearchMicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.vc6$b$b  reason: collision with other inner class name */
        public static final class C0262b extends ko7 implements vp7<iv7, qn7<? super List<? extends MicroApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0262b(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0262b bVar = new C0262b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends MicroApp>> qn7) {
                return ((C0262b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    MicroAppRepository microAppRepository = this.this$0.this$0.l;
                    List<String> G = this.this$0.this$0.m.G();
                    pq7.b(G, "sharedPreferencesManager.microAppSearchedIdsRecent");
                    return microAppRepository.getMicroAppByIds(G, this.this$0.this$0.n.J());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(vc6 vc6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = vc6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0074  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 0
                r5 = 2
                r4 = 1
                java.lang.Object r3 = com.fossil.yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0076
                if (r0 == r4) goto L_0x003f
                if (r0 != r5) goto L_0x0037
                java.lang.Object r0 = r7.L$1
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
                r0 = r8
            L_0x001b:
                java.util.List r0 = (java.util.List) r0
                com.fossil.vc6 r1 = r7.this$0
                java.util.ArrayList r1 = com.fossil.vc6.s(r1)
                r1.clear()
                com.fossil.vc6 r1 = r7.this$0
                java.util.ArrayList r1 = com.fossil.vc6.s(r1)
                r1.addAll(r0)
                com.fossil.vc6 r0 = r7.this$0
                com.fossil.vc6.x(r0)
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0036:
                return r0
            L_0x0037:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x003f:
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
                r2 = r0
                r1 = r8
            L_0x0048:
                r0 = r1
                java.util.List r0 = (java.util.List) r0
                com.fossil.vc6 r1 = r7.this$0
                java.util.ArrayList r1 = com.fossil.vc6.r(r1)
                r1.clear()
                com.fossil.vc6 r1 = r7.this$0
                java.util.ArrayList r1 = com.fossil.vc6.r(r1)
                r1.addAll(r0)
                com.fossil.vc6 r1 = r7.this$0
                com.fossil.dv7 r1 = com.fossil.vc6.q(r1)
                com.fossil.vc6$b$b r4 = new com.fossil.vc6$b$b
                r4.<init>(r7, r6)
                r7.L$0 = r2
                r7.L$1 = r0
                r7.label = r5
                java.lang.Object r0 = com.fossil.eu7.g(r1, r4, r7)
                if (r0 != r3) goto L_0x001b
                r0 = r3
                goto L_0x0036
            L_0x0076:
                com.fossil.el7.b(r8)
                com.fossil.iv7 r0 = r7.p$
                com.fossil.vc6 r1 = r7.this$0
                com.fossil.dv7 r1 = com.fossil.vc6.q(r1)
                com.fossil.vc6$b$a r2 = new com.fossil.vc6$b$a
                r2.<init>(r7, r6)
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r7)
                if (r1 != r3) goto L_0x0092
                r0 = r3
                goto L_0x0036
            L_0x0092:
                r2 = r0
                goto L_0x0048
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.vc6.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public vc6(rc6 rc6, MicroAppRepository microAppRepository, on5 on5, PortfolioApp portfolioApp) {
        pq7.c(rc6, "mView");
        pq7.c(microAppRepository, "mMicroAppRepository");
        pq7.c(on5, "sharedPreferencesManager");
        pq7.c(portfolioApp, "mApp");
        this.k = rc6;
        this.l = microAppRepository;
        this.m = on5;
        this.n = portfolioApp;
    }

    @DexIgnore
    public final void A() {
        if (this.j.isEmpty()) {
            this.k.B(D(pm7.j0(this.i)));
        } else {
            this.k.K(D(pm7.j0(this.j)));
        }
        if (!TextUtils.isEmpty(this.h)) {
            rc6 rc6 = this.k;
            String str = this.h;
            if (str != null) {
                rc6.z(str);
                String str2 = this.h;
                if (str2 != null) {
                    p(str2);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public Bundle B(Bundle bundle) {
        if (bundle != null) {
            bundle.putString(ViewHierarchy.DIMENSION_TOP_KEY, this.e);
            bundle.putString("middle", this.f);
            bundle.putString("bottom", this.g);
        }
        return bundle;
    }

    @DexIgnore
    public void C() {
        this.k.M5(this);
    }

    @DexIgnore
    public final List<cl7<MicroApp, String>> D(List<MicroApp> list) {
        ArrayList arrayList = new ArrayList();
        for (MicroApp microApp : list) {
            String id = microApp.getId();
            if (pq7.a(id, this.e)) {
                arrayList.add(new cl7(microApp, ViewHierarchy.DIMENSION_TOP_KEY));
            } else if (pq7.a(id, this.f)) {
                arrayList.add(new cl7(microApp, "middle"));
            } else if (pq7.a(id, this.g)) {
                arrayList.add(new cl7(microApp, "bottom"));
            } else {
                arrayList.add(new cl7(microApp, ""));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final void E(String str, String str2, String str3) {
        pq7.c(str, "watchAppTop");
        pq7.c(str2, "watchAppMiddle");
        pq7.c(str3, "watchAppBottom");
        this.e = str;
        this.g = str3;
        this.f = str2;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        xw7 unused = gu7.d(k(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.qc6
    public void n() {
        this.h = "";
        this.k.D();
        A();
    }

    @DexIgnore
    @Override // com.fossil.qc6
    public void o(MicroApp microApp) {
        pq7.c(microApp, "selectedMicroApp");
        List<String> G = this.m.G();
        pq7.b(G, "sharedPreferencesManager.microAppSearchedIdsRecent");
        if (!G.contains(microApp.getId())) {
            G.add(0, microApp.getId());
            if (G.size() > 5) {
                G = G.subList(0, 5);
            }
            this.m.F1(G);
        }
        this.k.v1(microApp);
    }

    @DexIgnore
    @Override // com.fossil.qc6
    public void p(String str) {
        pq7.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        xw7 unused = gu7.d(k(), null, null, new a(this, str, null), 3, null);
    }
}
