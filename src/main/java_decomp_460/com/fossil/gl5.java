package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.fl5;
import com.portfolio.platform.data.NetworkState;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gl5 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements fl5.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ MutableLiveData f1325a;

        @DexIgnore
        public a(MutableLiveData mutableLiveData) {
            this.f1325a = mutableLiveData;
        }

        @DexIgnore
        @Override // com.fossil.fl5.a
        public final void e(fl5.g gVar) {
            pq7.c(gVar, "report");
            if (gVar.c()) {
                this.f1325a.l(NetworkState.Companion.getLOADING());
            } else if (gVar.b()) {
                this.f1325a.l(NetworkState.Companion.error(gl5.c(gVar)));
            } else {
                this.f1325a.l(NetworkState.Companion.getLOADED());
            }
        }
    }

    @DexIgnore
    public static final LiveData<NetworkState> b(fl5 fl5) {
        pq7.c(fl5, "$this$createStatusLiveData");
        MutableLiveData mutableLiveData = new MutableLiveData();
        fl5.a(new a(mutableLiveData));
        return mutableLiveData;
    }

    @DexIgnore
    public static final String c(fl5.g gVar) {
        fl5.d[] values = fl5.d.values();
        ArrayList arrayList = new ArrayList();
        for (fl5.d dVar : values) {
            Throwable a2 = gVar.a(dVar);
            String message = a2 != null ? a2.getMessage() : null;
            if (message != null) {
                arrayList.add(message);
            }
        }
        return arrayList.isEmpty() ^ true ? (String) pm7.F(arrayList) : "";
    }
}
