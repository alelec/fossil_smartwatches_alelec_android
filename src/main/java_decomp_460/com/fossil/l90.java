package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum l90 {
    GET_CHALLENGE_INFO("req_chl_info"),
    LIST_CHALLENGES("req_chl_list"),
    SYNC_DATA("sync_pkg");
    
    @DexIgnore
    public static /* final */ k90 g; // = new k90(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public l90(String str) {
        this.b = str;
    }
}
