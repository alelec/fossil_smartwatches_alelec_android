package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class st1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ry1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<st1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public st1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(nq1.class.getClassLoader());
            if (readParcelable != null) {
                pq7.b(readParcelable, "parcel.readParcelable<Ri\u2026class.java.classLoader)!!");
                nq1 nq1 = (nq1) readParcelable;
                nt1 nt1 = (nt1) parcel.readParcelable(nt1.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(ry1.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new st1(nq1, nt1, (ry1) readParcelable2);
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public st1[] newArray(int i) {
            return new st1[i];
        }
    }

    @DexIgnore
    public st1(nq1 nq1, nt1 nt1, ry1 ry1) {
        super(nq1, nt1);
        this.d = ry1;
    }

    @DexIgnore
    public st1(nq1 nq1, ry1 ry1) {
        super(nq1, null);
        this.d = ry1;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public JSONObject a() {
        return g80.k(super.a(), jd0.t3, this.d.toString());
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        try {
            i9 i9Var = i9.d;
            jq1 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return i9Var.a(s, ry1, new ob0(((lq1) deviceRequest).c(), new ry1(this.d.getMajor(), this.d.getMinor())).a());
            }
            throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (sx1 e) {
            d90.i.i(e);
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(st1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.d, ((st1) obj).d) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.RingMyPhoneMicroAppData");
    }

    @DexIgnore
    public final ry1 getMicroAppVersion() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public int hashCode() {
        return (super.hashCode() * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }
}
