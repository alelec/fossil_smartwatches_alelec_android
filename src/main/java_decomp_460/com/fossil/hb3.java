package com.fossil;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hb3 extends mq2 implements fb3 {
    @DexIgnore
    public hb3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.IDeviceOrientationListener");
    }
}
