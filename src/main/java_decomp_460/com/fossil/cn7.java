package com.fossil;

import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cn7 {
    @DexIgnore
    public static final <T> Set<T> a(T t) {
        Set<T> singleton = Collections.singleton(t);
        pq7.b(singleton, "java.util.Collections.singleton(element)");
        return singleton;
    }
}
