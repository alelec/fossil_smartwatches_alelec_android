package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vr7 extends um7 {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public vr7(int i, int i2, int i3) {
        this.e = i3;
        this.b = i2;
        boolean z = true;
        if (i3 <= 0 ? i < i2 : i > i2) {
            z = false;
        }
        this.c = z;
        this.d = !z ? this.b : i;
    }

    @DexIgnore
    @Override // com.fossil.um7
    public int b() {
        int i = this.d;
        if (i != this.b) {
            this.d = this.e + i;
        } else if (this.c) {
            this.c = false;
        } else {
            throw new NoSuchElementException();
        }
        return i;
    }

    @DexIgnore
    public boolean hasNext() {
        return this.c;
    }
}
