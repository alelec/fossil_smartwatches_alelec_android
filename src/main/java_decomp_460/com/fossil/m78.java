package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m78 extends l78 {
    @DexIgnore
    public static /* final */ m78 NOP_LOGGER; // = new m78();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -517220405410904473L;

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final void debug(String str) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78
    public final void debug(String str, Object obj) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78
    public final void debug(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final void debug(String str, Throwable th) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78
    public final void debug(String str, Object... objArr) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final void error(String str) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78
    public final void error(String str, Object obj) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78
    public final void error(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final void error(String str, Throwable th) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final void error(String str, Object... objArr) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78
    public String getName() {
        return "NOP";
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final void info(String str) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final void info(String str, Object obj) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78
    public final void info(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final void info(String str, Throwable th) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78
    public final void info(String str, Object... objArr) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final boolean isDebugEnabled() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final boolean isErrorEnabled() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final boolean isInfoEnabled() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final boolean isTraceEnabled() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final boolean isWarnEnabled() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final void trace(String str) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78
    public final void trace(String str, Object obj) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78
    public final void trace(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final void trace(String str, Throwable th) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78
    public final void trace(String str, Object... objArr) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final void warn(String str) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78
    public final void warn(String str, Object obj) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final void warn(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78, com.fossil.e78
    public final void warn(String str, Throwable th) {
    }

    @DexIgnore
    @Override // com.fossil.l78, com.fossil.o78
    public final void warn(String str, Object... objArr) {
    }
}
