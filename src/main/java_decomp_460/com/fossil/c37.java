package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c37 extends iq4<a, c, b> {
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f552a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public a(String str, String str2) {
            pq7.c(str, Constants.EMAIL);
            pq7.c(str2, "otpCode");
            this.f552a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.f552a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f553a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(int i, String str) {
            pq7.c(str, "errorMessage");
            this.f553a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.f553a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.usecase.VerifyEmailOtp", f = "VerifyEmailOtp.kt", l = {21}, m = "run")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ c37 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(c37 c37, qn7 qn7) {
            super(qn7);
            this.this$0 = c37;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore
    public c37(UserRepository userRepository) {
        pq7.c(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "VerifyEmailOtp";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.c37.a r8, com.fossil.qn7<java.lang.Object> r9) {
        /*
            r7 = this;
            r6 = 600(0x258, float:8.41E-43)
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.fossil.c37.d
            if (r0 == 0) goto L_0x0047
            r0 = r9
            com.fossil.c37$d r0 = (com.fossil.c37.d) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0047
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0016:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0056
            if (r0 != r5) goto L_0x004e
            java.lang.Object r0 = r1.L$1
            com.fossil.c37$a r0 = (com.fossil.c37.a) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.c37 r0 = (com.fossil.c37) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x002e:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x007b
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = "VerifyEmailOtp"
            java.lang.String r2 = "verify OTP success"
            r0.d(r1, r2)
            com.fossil.c37$c r0 = new com.fossil.c37$c
            r0.<init>()
        L_0x0046:
            return r0
        L_0x0047:
            com.fossil.c37$d r0 = new com.fossil.c37$d
            r0.<init>(r7, r9)
            r1 = r0
            goto L_0x0016
        L_0x004e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0056:
            com.fossil.el7.b(r2)
            if (r8 != 0) goto L_0x0063
            com.fossil.c37$b r0 = new com.fossil.c37$b
            java.lang.String r1 = ""
            r0.<init>(r6, r1)
            goto L_0x0046
        L_0x0063:
            com.portfolio.platform.data.source.UserRepository r0 = r7.d
            java.lang.String r2 = r8.a()
            java.lang.String r4 = r8.b()
            r1.L$0 = r7
            r1.L$1 = r8
            r1.label = r5
            java.lang.Object r0 = r0.verifyEmailOtp(r2, r4, r1)
            if (r0 != r3) goto L_0x002e
            r0 = r3
            goto L_0x0046
        L_0x007b:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00ae
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "verify OTP failed "
            r2.append(r3)
            com.fossil.hq5 r0 = (com.fossil.hq5) r0
            com.portfolio.platform.data.model.ServerError r3 = r0.c()
            r2.append(r3)
            java.lang.String r3 = "VerifyEmailOtp"
            java.lang.String r2 = r2.toString()
            r1.d(r3, r2)
            com.fossil.c37$b r1 = new com.fossil.c37$b
            int r0 = r0.a()
            java.lang.String r2 = ""
            r1.<init>(r0, r2)
            r0 = r1
            goto L_0x0046
        L_0x00ae:
            com.fossil.c37$b r0 = new com.fossil.c37$b
            java.lang.String r1 = ""
            r0.<init>(r6, r1)
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.c37.k(com.fossil.c37$a, com.fossil.qn7):java.lang.Object");
    }
}
