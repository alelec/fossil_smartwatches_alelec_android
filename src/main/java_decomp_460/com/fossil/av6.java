package com.fossil;

import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.av5;
import com.fossil.cv5;
import com.fossil.dv5;
import com.fossil.ev5;
import com.fossil.fv5;
import com.fossil.gv5;
import com.fossil.ht5;
import com.fossil.hu5;
import com.fossil.hv5;
import com.fossil.iq4;
import com.fossil.iu5;
import com.fossil.ju5;
import com.fossil.ku5;
import com.fossil.o27;
import com.fossil.ru5;
import com.fossil.su5;
import com.fossil.tu5;
import com.fossil.uu5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.zendesk.sdk.support.help.HelpSearchRecyclerViewAdapter;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class av6 extends uu6 {
    @DexIgnore
    public static /* final */ String Q;
    @DexIgnore
    public static /* final */ a R; // = new a(null);
    @DexIgnore
    public o27 A;
    @DexIgnore
    public ck5 B;
    @DexIgnore
    public SummariesRepository C;
    @DexIgnore
    public SleepSummariesRepository D;
    @DexIgnore
    public GoalTrackingRepository E;
    @DexIgnore
    public hu5 F;
    @DexIgnore
    public iu5 G;
    @DexIgnore
    public v27 H;
    @DexIgnore
    public WatchLocalizationRepository I;
    @DexIgnore
    public AlarmsRepository J;
    @DexIgnore
    public hu4 K;
    @DexIgnore
    public vt4 L;
    @DexIgnore
    public pr4 M;
    @DexIgnore
    public WorkoutSettingRepository N;
    @DexIgnore
    public /* final */ vu6 O;
    @DexIgnore
    public /* final */ ls5 P;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;
    @DexIgnore
    public cv5 g;
    @DexIgnore
    public fv5 h;
    @DexIgnore
    public av5 i;
    @DexIgnore
    public ht5 j;
    @DexIgnore
    public mj5 k;
    @DexIgnore
    public UserRepository l;
    @DexIgnore
    public DeviceRepository m;
    @DexIgnore
    public on5 n;
    @DexIgnore
    public ru5 o;
    @DexIgnore
    public su5 p;
    @DexIgnore
    public uq4 q;
    @DexIgnore
    public tu5 r;
    @DexIgnore
    public uu5 s;
    @DexIgnore
    public ku5 t;
    @DexIgnore
    public ju5 u;
    @DexIgnore
    public dv5 v;
    @DexIgnore
    public xn5 w;
    @DexIgnore
    public ev5 x;
    @DexIgnore
    public hv5 y;
    @DexIgnore
    public gv5 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return av6.Q;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.login.LoginPresenter$checkOnboarding$1", f = "LoginPresenter.kt", l = {572, 573, 577, 582, 585}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ av6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.login.LoginPresenter$checkOnboarding$1$currentUser$1", f = "LoginPresenter.kt", l = {585}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository S = this.this$0.this$0.S();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = S.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(av6 av6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = av6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0095  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00ea  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x012f  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0149  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x017f  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0182  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x0186  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 393
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.av6.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.e<o27.c, o27.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ av6 f346a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpSocialAuth b;

        @DexIgnore
        public c(av6 av6, SignUpSocialAuth signUpSocialAuth) {
            this.f346a = av6;
            this.b = signUpSocialAuth;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(o27.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f346a.O.h();
            this.f346a.Z(bVar.a(), "");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(o27.c cVar) {
            pq7.c(cVar, "responseValue");
            boolean a2 = cVar.a();
            if (a2) {
                this.f346a.Y(this.b);
            } else if (!a2) {
                this.f346a.O.e5(this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1", f = "LoginPresenter.kt", l = {507, 517}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ av6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1$1", f = "LoginPresenter.kt", l = {508, 509, 510, 511, 512, 513}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:13:0x0054  */
            /* JADX WARNING: Removed duplicated region for block: B:17:0x0070  */
            /* JADX WARNING: Removed duplicated region for block: B:21:0x008c  */
            /* JADX WARNING: Removed duplicated region for block: B:25:0x00a8  */
            /* JADX WARNING: Removed duplicated region for block: B:9:0x0038  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 1
                    r4 = 0
                    java.lang.Object r1 = com.fossil.yn7.d()
                    int r0 = r6.label
                    switch(r0) {
                        case 0: goto L_0x00ab;
                        case 1: goto L_0x008e;
                        case 2: goto L_0x0072;
                        case 3: goto L_0x0056;
                        case 4: goto L_0x003a;
                        case 5: goto L_0x001d;
                        case 6: goto L_0x0013;
                        default: goto L_0x000b;
                    }
                L_0x000b:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0013:
                    java.lang.Object r0 = r6.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r7)
                L_0x001a:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x001c:
                    return r0
                L_0x001d:
                    java.lang.Object r0 = r6.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r7)
                L_0x0024:
                    com.fossil.av6$d r2 = r6.this$0
                    com.fossil.av6 r2 = r2.this$0
                    com.portfolio.platform.data.source.DeviceRepository r2 = r2.L()
                    r6.L$0 = r0
                    r0 = 6
                    r6.label = r0
                    r0 = 0
                    java.lang.Object r0 = com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r2, r4, r6, r5, r0)
                    if (r0 != r1) goto L_0x001a
                    r0 = r1
                    goto L_0x001c
                L_0x003a:
                    java.lang.Object r0 = r6.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r7)
                L_0x0041:
                    com.fossil.av6$d r2 = r6.this$0
                    com.fossil.av6 r2 = r2.this$0
                    com.portfolio.platform.data.source.WorkoutSettingRepository r2 = r2.U()
                    r6.L$0 = r0
                    r3 = 5
                    r6.label = r3
                    java.lang.Object r2 = r2.downloadWorkoutSettings(r6)
                    if (r2 != r1) goto L_0x0024
                    r0 = r1
                    goto L_0x001c
                L_0x0056:
                    java.lang.Object r0 = r6.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r7)
                L_0x005d:
                    com.fossil.av6$d r2 = r6.this$0
                    com.fossil.av6 r2 = r2.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository r2 = r2.O()
                    r6.L$0 = r0
                    r3 = 4
                    r6.label = r3
                    java.lang.Object r2 = r2.fetchGoalSetting(r6)
                    if (r2 != r1) goto L_0x0041
                    r0 = r1
                    goto L_0x001c
                L_0x0072:
                    java.lang.Object r0 = r6.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r7)
                L_0x0079:
                    com.fossil.av6$d r2 = r6.this$0
                    com.fossil.av6 r2 = r2.this$0
                    com.portfolio.platform.data.source.SleepSummariesRepository r2 = r2.Q()
                    r6.L$0 = r0
                    r3 = 3
                    r6.label = r3
                    java.lang.Object r2 = r2.fetchLastSleepGoal(r6)
                    if (r2 != r1) goto L_0x005d
                    r0 = r1
                    goto L_0x001c
                L_0x008e:
                    java.lang.Object r0 = r6.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r7)
                L_0x0095:
                    com.fossil.av6$d r2 = r6.this$0
                    com.fossil.av6 r2 = r2.this$0
                    com.portfolio.platform.data.source.SummariesRepository r2 = r2.R()
                    r6.L$0 = r0
                    r3 = 2
                    r6.label = r3
                    java.lang.Object r2 = r2.fetchActivitySettings(r6)
                    if (r2 != r1) goto L_0x0079
                    r0 = r1
                    goto L_0x001c
                L_0x00ab:
                    com.fossil.el7.b(r7)
                    com.fossil.iv7 r0 = r6.p$
                    com.fossil.av6$d r2 = r6.this$0
                    com.fossil.av6 r2 = r2.this$0
                    com.portfolio.platform.data.source.WatchLocalizationRepository r2 = r2.T()
                    r6.L$0 = r0
                    r6.label = r5
                    java.lang.Object r2 = r2.getWatchLocalizationFromServer(r4, r6)
                    if (r2 != r1) goto L_0x0095
                    r0 = r1
                    goto L_0x001c
                    switch-data {0->0x00ab, 1->0x008e, 2->0x0072, 3->0x0056, 4->0x003a, 5->0x001d, 6->0x0013, }
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.av6.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1$alarms$1", f = "LoginPresenter.kt", l = {517}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super List<Alarm>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<Alarm>> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    AlarmsRepository J = this.this$0.this$0.J();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object activeAlarms = J.getActiveAlarms(this);
                    return activeAlarms == d ? d : activeAlarms;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(av6 av6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = av6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0054  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 0
                r5 = 2
                r4 = 1
                java.lang.Object r1 = com.fossil.yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0056
                if (r0 == r4) goto L_0x0038
                if (r0 != r5) goto L_0x0030
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
                r0 = r8
            L_0x0017:
                java.util.List r0 = (java.util.List) r0
                if (r0 != 0) goto L_0x0020
                java.util.ArrayList r0 = new java.util.ArrayList
                r0.<init>()
            L_0x0020:
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.h0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                java.util.List r0 = com.fossil.dj5.a(r0)
                r1.q1(r0)
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x002f:
                return r0
            L_0x0030:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0038:
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
            L_0x003f:
                com.fossil.av6 r2 = r7.this$0
                com.fossil.dv7 r2 = com.fossil.av6.z(r2)
                com.fossil.av6$d$b r3 = new com.fossil.av6$d$b
                r3.<init>(r7, r6)
                r7.L$0 = r0
                r7.label = r5
                java.lang.Object r0 = com.fossil.eu7.g(r2, r3, r7)
                if (r0 != r1) goto L_0x0017
                r0 = r1
                goto L_0x002f
            L_0x0056:
                com.fossil.el7.b(r8)
                com.fossil.iv7 r0 = r7.p$
                com.fossil.av6 r2 = r7.this$0
                com.fossil.dv7 r2 = com.fossil.av6.z(r2)
                com.fossil.av6$d$a r3 = new com.fossil.av6$d$a
                r3.<init>(r7, r6)
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r2 = com.fossil.eu7.g(r2, r3, r7)
                if (r2 != r1) goto L_0x003f
                r0 = r1
                goto L_0x002f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.av6.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadSocialProfile$2", f = "LoginPresenter.kt", l = {610}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super kz4<it4>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ av6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(av6 av6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = av6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super kz4<it4>> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                hu4 V = this.this$0.V();
                this.L$0 = iv7;
                this.label = 1;
                Object c = V.c(this);
                return c == d ? d : c;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements iq4.e<fv5.c, fv5.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ av6 f347a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(av6 av6) {
            this.f347a = av6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(fv5.a aVar) {
            pq7.c(aVar, "errorValue");
            this.f347a.O.h();
            this.f347a.W(aVar.a(), "");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(fv5.c cVar) {
            pq7.c(cVar, "responseValue");
            PortfolioApp.h0.c().M().S0(this.f347a);
            this.f347a.a0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements iq4.e<cv5.c, cv5.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ av6 f348a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g(av6 av6) {
            this.f348a = av6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(cv5.a aVar) {
            pq7.c(aVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = av6.R.a();
            local.d(a2, "Inside .loginEmail failed with error=" + aVar.a());
            this.f348a.O.h();
            this.f348a.Z(aVar.a(), aVar.b());
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(cv5.c cVar) {
            pq7.c(cVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(av6.R.a(), "Inside .loginEmail success ");
            PortfolioApp.h0.c().M().S0(this.f348a);
            this.f348a.a0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements iq4.e<dv5.d, dv5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ av6 f349a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public h(av6 av6) {
            this.f349a = av6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(dv5.c cVar) {
            pq7.c(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = av6.R.a();
            local.d(a2, "Inside .loginFacebook failed with error=" + cVar.a());
            this.f349a.O.h();
            if (2 != cVar.a()) {
                this.f349a.Z(cVar.a(), "");
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(dv5.d dVar) {
            pq7.c(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = av6.R.a();
            local.d(a2, "Inside .loginFacebook success with result=" + dVar.a());
            this.f349a.E(dVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements iq4.e<ev5.d, ev5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ av6 f350a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(av6 av6) {
            this.f350a = av6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(ev5.c cVar) {
            pq7.c(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = av6.R.a();
            local.d(a2, "Inside .loginGoogle failed with error=" + cVar.a());
            this.f350a.O.h();
            if (2 != cVar.a()) {
                this.f350a.Z(cVar.a(), "");
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ev5.d dVar) {
            pq7.c(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = av6.R.a();
            local.d(a2, "Inside .loginGoogle success with result=" + dVar.a());
            this.f350a.E(dVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements iq4.e<gv5.d, gv5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ av6 f351a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public j(av6 av6) {
            this.f351a = av6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(gv5.c cVar) {
            pq7.c(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = av6.R.a();
            local.e(a2, "Inside .loginWechat failed with error=" + cVar.a());
            this.f351a.O.h();
            this.f351a.Z(cVar.a(), "");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(gv5.d dVar) {
            pq7.c(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = av6.R.a();
            local.d(a2, "Inside .loginWechat success with result=" + dVar.a());
            this.f351a.E(dVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements iq4.e<hv5.d, hv5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ av6 f352a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public k(av6 av6) {
            this.f352a = av6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(hv5.c cVar) {
            pq7.c(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = av6.R.a();
            local.d(a2, "Inside .loginWeibo failed with error=" + cVar.a());
            this.f352a.O.h();
            this.f352a.Z(cVar.a(), "");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(hv5.d dVar) {
            pq7.c(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = av6.R.a();
            local.d(a2, "Inside .loginWeibo success with result=" + dVar.a());
            this.f352a.E(dVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements iq4.e<av5.c, av5.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ av6 f353a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onError$1", f = "LoginPresenter.kt", l = {495}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ av5.a $errorValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ l this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(l lVar, av5.a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = lVar;
                this.$errorValue = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$errorValue, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository S = this.this$0.f353a.S();
                    this.L$0 = iv7;
                    this.label = 1;
                    if (S.clearAllUser(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.f353a.O.h();
                this.this$0.f353a.W(this.$errorValue.a(), this.$errorValue.b());
                return tl7.f3441a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1", f = "LoginPresenter.kt", l = {407, 413, 419, 422, MFNetworkReturnCode.RATE_LIMIT_EXEEDED, 444, 452, 484}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MFUser $currentUser;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ l this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$1", f = "LoginPresenter.kt", l = {407}, m = "invokeSuspend")
            public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(b bVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    a aVar = new a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        UserRepository S = this.this$0.this$0.f353a.S();
                        this.L$0 = iv7;
                        this.label = 1;
                        if (S.clearAllUser(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return tl7.f3441a;
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.av6$l$b$b")
            @eo7(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$2", f = "LoginPresenter.kt", l = {HelpSearchRecyclerViewAdapter.TYPE_PADDING, 424}, m = "invokeSuspend")
            /* renamed from: com.fossil.av6$l$b$b  reason: collision with other inner class name */
            public static final class C0011b extends ko7 implements vp7<iv7, qn7<? super iq5<UserSettings>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0011b(b bVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0011b bVar = new C0011b(this.this$0, qn7);
                    bVar.p$ = (iv7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super iq5<UserSettings>> qn7) {
                    return ((C0011b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    iv7 iv7;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 = this.p$;
                        PortfolioApp c = PortfolioApp.h0.c();
                        this.L$0 = iv7;
                        this.label = 1;
                        if (c.g2(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 = (iv7) this.L$0;
                        el7.b(obj);
                    } else if (i == 2) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    UserRepository S = this.this$0.this$0.f353a.S();
                    this.L$0 = iv7;
                    this.label = 2;
                    Object userSettingFromServer = S.getUserSettingFromServer(this);
                    return userSettingFromServer == d ? d : userSettingFromServer;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class c implements iq4.e<uy6, sy6> {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ b f354a;
                @DexIgnore
                public /* final */ /* synthetic */ dr7 b;
                @DexIgnore
                public /* final */ /* synthetic */ dr7 c;
                @DexIgnore
                public /* final */ /* synthetic */ List d;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                @eo7(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1", f = "LoginPresenter.kt", l = {472}, m = "invokeSuspend")
                public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ sy6 $errorValue;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ c this$0;

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.av6$l$b$c$a$a")
                    @eo7(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1$1", f = "LoginPresenter.kt", l = {472}, m = "invokeSuspend")
                    /* renamed from: com.fossil.av6$l$b$c$a$a  reason: collision with other inner class name */
                    public static final class C0012a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                        @DexIgnore
                        public Object L$0;
                        @DexIgnore
                        public int label;
                        @DexIgnore
                        public iv7 p$;
                        @DexIgnore
                        public /* final */ /* synthetic */ a this$0;

                        @DexIgnore
                        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                        public C0012a(a aVar, qn7 qn7) {
                            super(2, qn7);
                            this.this$0 = aVar;
                        }

                        @DexIgnore
                        @Override // com.fossil.zn7
                        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                            pq7.c(qn7, "completion");
                            C0012a aVar = new C0012a(this.this$0, qn7);
                            aVar.p$ = (iv7) obj;
                            return aVar;
                        }

                        @DexIgnore
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                        @Override // com.fossil.vp7
                        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                            return ((C0012a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                        }

                        @DexIgnore
                        @Override // com.fossil.zn7
                        public final Object invokeSuspend(Object obj) {
                            Object d = yn7.d();
                            int i = this.label;
                            if (i == 0) {
                                el7.b(obj);
                                iv7 iv7 = this.p$;
                                UserRepository S = this.this$0.this$0.f354a.this$0.f353a.S();
                                this.L$0 = iv7;
                                this.label = 1;
                                if (S.clearAllUser(this) == d) {
                                    return d;
                                }
                            } else if (i == 1) {
                                iv7 iv72 = (iv7) this.L$0;
                                el7.b(obj);
                            } else {
                                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                            }
                            return tl7.f3441a;
                        }
                    }

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public a(c cVar, sy6 sy6, qn7 qn7) {
                        super(2, qn7);
                        this.this$0 = cVar;
                        this.$errorValue = sy6;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        a aVar = new a(this.this$0, this.$errorValue, qn7);
                        aVar.p$ = (iv7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                        return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            el7.b(obj);
                            iv7 iv7 = this.p$;
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String a2 = av6.R.a();
                            local.d(a2, "onLoginSuccess download device setting fail " + this.$errorValue.a());
                            dv7 i2 = this.this$0.f354a.this$0.f353a.i();
                            C0012a aVar = new C0012a(this, null);
                            this.L$0 = iv7;
                            this.label = 1;
                            if (eu7.g(i2, aVar, this) == d) {
                                return d;
                            }
                        } else if (i == 1) {
                            iv7 iv72 = (iv7) this.L$0;
                            el7.b(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        this.this$0.f354a.this$0.f353a.O.h();
                        this.this$0.f354a.this$0.f353a.W(this.$errorValue.a(), this.$errorValue.b());
                        return tl7.f3441a;
                    }
                }

                @DexIgnore
                public c(b bVar, dr7 dr7, dr7 dr72, List list) {
                    this.f354a = bVar;
                    this.b = dr7;
                    this.c = dr72;
                    this.d = list;
                }

                @DexIgnore
                /* renamed from: b */
                public void a(sy6 sy6) {
                    pq7.c(sy6, "errorValue");
                    xw7 unused = gu7.d(this.f354a.this$0.f353a.k(), null, null, new a(this, sy6, null), 3, null);
                }

                @DexIgnore
                /* renamed from: c */
                public void onSuccess(uy6 uy6) {
                    pq7.c(uy6, "responseValue");
                    FLogger.INSTANCE.getLocal().d(av6.R.a(), "onLoginSuccess download device setting success");
                    PortfolioApp.h0.c().n1(this.b.element, this.c.element);
                    for (cl7 cl7 : this.d) {
                        PortfolioApp.h0.c().F1((String) cl7.getFirst(), (String) cl7.getSecond());
                    }
                    this.f354a.this$0.f353a.b0(this.b.element);
                    FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.b.element, av6.R.a(), "[Sync Start] AUTO SYNC after login");
                    PortfolioApp.h0.c().S1(this.f354a.this$0.f353a.M(), false, 13);
                    this.f354a.this$0.f353a.D();
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$response$1", f = "LoginPresenter.kt", l = {MFNetworkReturnCode.RATE_LIMIT_EXEEDED}, m = "invokeSuspend")
            public static final class d extends ko7 implements vp7<iv7, qn7<? super iq5<ApiResponse<Device>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public d(b bVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    d dVar = new d(this.this$0, qn7);
                    dVar.p$ = (iv7) obj;
                    return dVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super iq5<ApiResponse<Device>>> qn7) {
                    return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        DeviceRepository L = this.this$0.this$0.f353a.L();
                        this.L$0 = iv7;
                        this.label = 1;
                        Object downloadDeviceList = L.downloadDeviceList(this);
                        return downloadDeviceList == d ? d : downloadDeviceList;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(l lVar, MFUser mFUser, qn7 qn7) {
                super(2, qn7);
                this.this$0 = lVar;
                this.$currentUser = mFUser;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, this.$currentUser, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:17:0x00c8  */
            /* JADX WARNING: Removed duplicated region for block: B:27:0x0126  */
            /* JADX WARNING: Removed duplicated region for block: B:36:0x01a9  */
            /* JADX WARNING: Removed duplicated region for block: B:40:0x01cc  */
            /* JADX WARNING: Removed duplicated region for block: B:44:0x0222  */
            /* JADX WARNING: Removed duplicated region for block: B:59:0x02af  */
            /* JADX WARNING: Removed duplicated region for block: B:67:0x031a  */
            /* JADX WARNING: Removed duplicated region for block: B:75:0x0369  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r15) {
                /*
                // Method dump skipped, instructions count: 898
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.av6.l.b.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public l(av6 av6) {
            this.f353a = av6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(av5.a aVar) {
            pq7.c(aVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = av6.R.a();
            local.d(a2, "onLoginSuccess download userInfo failed " + aVar.a());
            xw7 unused = gu7.d(this.f353a.k(), null, null, new a(this, aVar, null), 3, null);
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(av5.c cVar) {
            pq7.c(cVar, "responseValue");
            xw7 unused = gu7.d(this.f353a.k(), null, null, new b(this, cVar.a(), null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements iq4.e<ht5.e, ht5.d> {
        @DexIgnore
        /* renamed from: b */
        public void a(ht5.d dVar) {
            pq7.c(dVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ht5.e eVar) {
            pq7.c(eVar, "responseValue");
        }
    }

    /*
    static {
        String simpleName = av6.class.getSimpleName();
        pq7.b(simpleName, "LoginPresenter::class.java.simpleName");
        Q = simpleName;
    }
    */

    @DexIgnore
    public av6(vu6 vu6, ls5 ls5) {
        pq7.c(vu6, "mView");
        pq7.c(ls5, "mContext");
        this.O = vu6;
        this.P = ls5;
    }

    @DexIgnore
    public final void D() {
        xw7 unused = gu7.d(k(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    public final void E(SignUpSocialAuth signUpSocialAuth) {
        pq7.c(signUpSocialAuth, "auth");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = uz6.U.a();
        local.d(a2, "checkSocialAccountIsExisted " + signUpSocialAuth);
        o27 o27 = this.A;
        if (o27 != null) {
            o27.e(new o27.a(signUpSocialAuth.getService(), signUpSocialAuth.getToken()), new c(this, signUpSocialAuth));
        } else {
            pq7.n("mCheckAuthenticationSocialExisting");
            throw null;
        }
    }

    @DexIgnore
    public final void F() {
        FLogger.INSTANCE.getLocal().d(Q, "downloadOptionalsResources");
        Date date = new Date();
        xw7 unused = gu7.d(k(), null, null, new d(this, null), 3, null);
        su5 su5 = this.p;
        if (su5 != null) {
            su5.e(new su5.a(date), null);
            ru5 ru5 = this.o;
            if (ru5 != null) {
                ru5.e(new ru5.a(date), null);
                tu5 tu5 = this.r;
                if (tu5 != null) {
                    tu5.e(new tu5.a(date), null);
                    uu5 uu5 = this.s;
                    if (uu5 != null) {
                        uu5.e(new uu5.a(date), null);
                        ku5 ku5 = this.t;
                        if (ku5 != null) {
                            ku5.e(new ku5.a(date), null);
                            ju5 ju5 = this.u;
                            if (ju5 != null) {
                                ju5.e(new ju5.a(date), null);
                                iu5 iu5 = this.G;
                                if (iu5 != null) {
                                    iu5.e(new iu5.a(date), null);
                                    hu5 hu5 = this.F;
                                    if (hu5 != null) {
                                        hu5.e(new hu5.a(date), null);
                                    } else {
                                        pq7.n("mFetchDailyGoalTrackingSummaries");
                                        throw null;
                                    }
                                } else {
                                    pq7.n("mFetchGoalTrackingData");
                                    throw null;
                                }
                            } else {
                                pq7.n("mFetchDailyHeartRateSummaries");
                                throw null;
                            }
                        } else {
                            pq7.n("mFetchHeartRateSamples");
                            throw null;
                        }
                    } else {
                        pq7.n("mFetchSleepSummaries");
                        throw null;
                    }
                } else {
                    pq7.n("mFetchSleepSessions");
                    throw null;
                }
            } else {
                pq7.n("mFetchActivities");
                throw null;
            }
        } else {
            pq7.n("mFetchSummaries");
            throw null;
        }
    }

    @DexIgnore
    public final /* synthetic */ Object G(qn7<? super kz4<it4>> qn7) {
        return eu7.g(bw7.b(), new e(this, null), qn7);
    }

    @DexIgnore
    public final vt4 H() {
        vt4 vt4 = this.L;
        if (vt4 != null) {
            return vt4;
        }
        pq7.n("fcmRepository");
        throw null;
    }

    @DexIgnore
    public final pr4 I() {
        pr4 pr4 = this.M;
        if (pr4 != null) {
            return pr4;
        }
        pq7.n("flagRepository");
        throw null;
    }

    @DexIgnore
    public final AlarmsRepository J() {
        AlarmsRepository alarmsRepository = this.J;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        pq7.n("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final ck5 K() {
        ck5 ck5 = this.B;
        if (ck5 != null) {
            return ck5;
        }
        pq7.n("mAnalyticsHelper");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository L() {
        DeviceRepository deviceRepository = this.m;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        pq7.n("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final mj5 M() {
        mj5 mj5 = this.k;
        if (mj5 != null) {
            return mj5;
        }
        pq7.n("mDeviceSettingFactory");
        throw null;
    }

    @DexIgnore
    public final v27 N() {
        v27 v27 = this.H;
        if (v27 != null) {
            return v27;
        }
        pq7.n("mGetSecretKeyUseCase");
        throw null;
    }

    @DexIgnore
    public final GoalTrackingRepository O() {
        GoalTrackingRepository goalTrackingRepository = this.E;
        if (goalTrackingRepository != null) {
            return goalTrackingRepository;
        }
        pq7.n("mGoalTrackingRepository");
        throw null;
    }

    @DexIgnore
    public final on5 P() {
        on5 on5 = this.n;
        if (on5 != null) {
            return on5;
        }
        pq7.n("mSharePrefs");
        throw null;
    }

    @DexIgnore
    public final SleepSummariesRepository Q() {
        SleepSummariesRepository sleepSummariesRepository = this.D;
        if (sleepSummariesRepository != null) {
            return sleepSummariesRepository;
        }
        pq7.n("mSleepSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final SummariesRepository R() {
        SummariesRepository summariesRepository = this.C;
        if (summariesRepository != null) {
            return summariesRepository;
        }
        pq7.n("mSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository S() {
        UserRepository userRepository = this.l;
        if (userRepository != null) {
            return userRepository;
        }
        pq7.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final WatchLocalizationRepository T() {
        WatchLocalizationRepository watchLocalizationRepository = this.I;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        pq7.n("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public final WorkoutSettingRepository U() {
        WorkoutSettingRepository workoutSettingRepository = this.N;
        if (workoutSettingRepository != null) {
            return workoutSettingRepository;
        }
        pq7.n("mWorkoutSettingRepository");
        throw null;
    }

    @DexIgnore
    public final hu4 V() {
        hu4 hu4 = this.K;
        if (hu4 != null) {
            return hu4;
        }
        pq7.n("socialProfileRepository");
        throw null;
    }

    @DexIgnore
    public final void W(int i2, String str) {
        pq7.c(str, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = uz6.U.a();
        local.d(a2, "handleError errorCode=" + i2 + " message=" + str);
        if (i2 != 408) {
            this.O.C(i2, str);
        } else if (!o37.b(PortfolioApp.h0.c())) {
            this.O.C(601, "");
        } else {
            this.O.C(i2, "");
        }
    }

    @DexIgnore
    public final boolean X() {
        String str = this.e;
        if (str == null || str.length() == 0) {
            this.O.P1(false, "");
            return false;
        } else if (!b47.a(this.e)) {
            vu6 vu6 = this.O;
            String c2 = um5.c(PortfolioApp.h0.c(), 2131887009);
            pq7.b(c2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            vu6.P1(true, c2);
            return false;
        } else {
            this.O.P1(false, "");
            return true;
        }
    }

    @DexIgnore
    public final void Y(SignUpSocialAuth signUpSocialAuth) {
        pq7.c(signUpSocialAuth, "auth");
        fv5 fv5 = this.h;
        if (fv5 != null) {
            fv5.e(new fv5.b(signUpSocialAuth.getService(), signUpSocialAuth.getToken(), signUpSocialAuth.getClientId()), new f(this));
        } else {
            pq7.n("mLoginSocialUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void Z(int i2, String str) {
        pq7.c(str, "errorMessage");
        if ((400 <= i2 && 407 >= i2) || (407 <= i2 && 499 >= i2)) {
            this.O.W1();
        } else if (i2 != 408) {
            this.O.C(i2, str);
        } else if (!o37.b(PortfolioApp.h0.c())) {
            this.O.C(601, "");
        } else {
            this.O.C(i2, "");
        }
    }

    @DexIgnore
    public final void a0() {
        FLogger.INSTANCE.getLocal().d(Q, "onLoginSuccess download user info");
        av5 av5 = this.i;
        if (av5 != null) {
            av5.e(new av5.b(), new l(this));
        } else {
            pq7.n("mDownloadUserInfoUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void b0(String str) {
        pq7.c(str, "activeSerial");
        ht5 ht5 = this.j;
        if (ht5 != null) {
            ht5.e(new ht5.c(str), new m());
        } else {
            pq7.n("mReconnectDeviceUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void c0() {
        this.O.M5(this);
    }

    @DexIgnore
    public final void d0() {
        Locale locale = Locale.getDefault();
        pq7.b(locale, "Locale.getDefault()");
        if (!TextUtils.isEmpty(locale.getLanguage())) {
            Locale locale2 = Locale.getDefault();
            pq7.b(locale2, "Locale.getDefault()");
            if (!TextUtils.isEmpty(locale2.getCountry())) {
                StringBuilder sb = new StringBuilder();
                Locale locale3 = Locale.getDefault();
                pq7.b(locale3, "Locale.getDefault()");
                sb.append(locale3.getLanguage());
                sb.append(LocaleConverter.LOCALE_DELIMITER);
                Locale locale4 = Locale.getDefault();
                pq7.b(locale4, "Locale.getDefault()");
                sb.append(locale4.getCountry());
                String sb2 = sb.toString();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = Q;
                local.d(str, "language: " + sb2);
                if (vt7.j(sb2, "zh_CN", true) || vt7.j(sb2, "zh_SG", true) || vt7.j(sb2, "zh_TW", true)) {
                    this.O.O4(true);
                    return;
                } else {
                    this.O.O4(false);
                    return;
                }
            }
        }
        this.O.O4(false);
    }

    @DexIgnore
    public final void e0() {
        if (!X() || TextUtils.isEmpty(this.f)) {
            this.O.O0();
        } else {
            this.O.c3();
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        e0();
        d0();
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void n() {
        if (PortfolioApp.h0.c().p0()) {
            this.O.R();
        } else {
            W(601, "");
        }
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void o() {
        vu6 vu6 = this.O;
        String str = this.e;
        if (str == null) {
            str = "";
        }
        vu6.H5(str);
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void p(SignUpSocialAuth signUpSocialAuth) {
        pq7.c(signUpSocialAuth, "auth");
        this.O.i();
        E(signUpSocialAuth);
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void q(String str, String str2) {
        pq7.c(str, Constants.EMAIL);
        pq7.c(str2, "password");
        if (X()) {
            this.O.i();
            cv5 cv5 = this.g;
            if (cv5 != null) {
                cv5.e(new cv5.b(str, str2), new g(this));
            } else {
                pq7.n("mLoginEmailUseCase");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void r() {
        this.O.i();
        dv5 dv5 = this.v;
        if (dv5 != null) {
            dv5.e(new dv5.b(new WeakReference(this.P)), new h(this));
        } else {
            pq7.n("mLoginFacebookUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void s() {
        this.O.i();
        ev5 ev5 = this.x;
        if (ev5 != null) {
            ev5.e(new ev5.b(new WeakReference(this.P)), new i(this));
        } else {
            pq7.n("mLoginGoogleUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void t() {
        if (!dk5.g.j(this.P, "com.tencent.mm")) {
            dk5.g.k(this.P, "com.tencent.mm");
            return;
        }
        this.O.i();
        gv5 gv5 = this.z;
        if (gv5 != null) {
            gv5.e(new gv5.b(new WeakReference(this.P)), new j(this));
        } else {
            pq7.n("mLoginWechatUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void u() {
        this.O.i();
        hv5 hv5 = this.y;
        if (hv5 != null) {
            hv5.e(new hv5.b(new WeakReference(this.P)), new k(this));
        } else {
            pq7.n("mLoginWeiboUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void v(boolean z2) {
        e0();
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void w(String str) {
        pq7.c(str, Constants.EMAIL);
        this.e = str;
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void x(String str) {
        pq7.c(str, "password");
        this.f = str;
        e0();
    }
}
