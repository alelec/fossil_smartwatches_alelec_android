package com.fossil;

import dagger.internal.Factory;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e02 implements Factory<Executor> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ e02 f866a; // = new e02();

    @DexIgnore
    public static e02 a() {
        return f866a;
    }

    @DexIgnore
    public static Executor b() {
        Executor a2 = d02.a();
        lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    /* renamed from: c */
    public Executor get() {
        return b();
    }
}
