package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qc2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ b f2958a; // = new xd2();

    @DexIgnore
    public interface a<R extends z62, T> {
        @DexIgnore
        T a(R r);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        n62 a(Status status);
    }

    @DexIgnore
    public static <R extends z62, T extends y62<R>> nt3<T> a(t62<R> t62, T t) {
        return b(t62, new zd2(t));
    }

    @DexIgnore
    public static <R extends z62, T> nt3<T> b(t62<R> t62, a<R, T> aVar) {
        b bVar = f2958a;
        ot3 ot3 = new ot3();
        t62.b(new wd2(t62, ot3, aVar, bVar));
        return ot3.a();
    }

    @DexIgnore
    public static <R extends z62> nt3<Void> c(t62<R> t62) {
        return b(t62, new yd2());
    }
}
