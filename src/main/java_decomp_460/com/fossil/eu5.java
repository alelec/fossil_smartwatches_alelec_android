package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eu5 implements Factory<du5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<LocationSource> f989a;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> b;

    @DexIgnore
    public eu5(Provider<LocationSource> provider, Provider<PortfolioApp> provider2) {
        this.f989a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static eu5 a(Provider<LocationSource> provider, Provider<PortfolioApp> provider2) {
        return new eu5(provider, provider2);
    }

    @DexIgnore
    public static du5 c(LocationSource locationSource, PortfolioApp portfolioApp) {
        return new du5(locationSource, portfolioApp);
    }

    @DexIgnore
    /* renamed from: b */
    public du5 get() {
        return c(this.f989a.get(), this.b.get());
    }
}
