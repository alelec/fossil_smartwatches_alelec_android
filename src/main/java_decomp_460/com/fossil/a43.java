package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a43 implements Iterator<String> {
    @DexIgnore
    public Iterator<String> b; // = this.c.b.iterator();
    @DexIgnore
    public /* final */ /* synthetic */ y33 c;

    @DexIgnore
    public a43(y33 y33) {
        this.c = y33;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.b.hasNext();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Iterator
    public final /* synthetic */ String next() {
        return this.b.next();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
