package com.fossil;

import com.fossil.m34;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n34<E> extends h34<E> {
    @DexIgnore
    @Deprecated
    public static <E> m34.a<E> builder() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <E> m34<E> copyOf(E[] eArr) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <E> m34<E> of(E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <E> m34<E> of(E e, E e2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <E> m34<E> of(E e, E e2, E e3) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <E> m34<E> of(E e, E e2, E e3, E e4) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <E> m34<E> of(E e, E e2, E e3, E e4, E e5) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <E> m34<E> of(E e, E e2, E e3, E e4, E e5, E e6, E... eArr) {
        throw new UnsupportedOperationException();
    }
}
