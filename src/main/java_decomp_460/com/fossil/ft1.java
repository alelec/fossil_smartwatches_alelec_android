package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ft1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1190a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public ft1(String str, String str2) {
        this.f1190a = str;
        this.b = str2;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ft1)) {
            return false;
        }
        ft1 ft1 = (ft1) obj;
        if (!pq7.a(this.f1190a, ft1.f1190a)) {
            return false;
        }
        return !(pq7.a(this.b, ft1.b) ^ true);
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.f1190a.hashCode();
        String str = this.b;
        return (str != null ? str.hashCode() : 0) + (hashCode * 31);
    }

    @DexIgnore
    public String toString() {
        StringBuilder e = e.e("UserCredential(userId=");
        e.append(this.f1190a);
        e.append(", refreshToken=");
        e.append(this.b);
        e.append(")");
        return e.toString();
    }
}
