package com.fossil;

import java.io.ObjectStreamException;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class o78 implements e78, Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 7535258609338176893L;
    @DexIgnore
    public String name;

    @DexIgnore
    public abstract /* synthetic */ void debug(g78 g78, String str);

    @DexIgnore
    public abstract /* synthetic */ void debug(g78 g78, String str, Object obj);

    @DexIgnore
    public abstract /* synthetic */ void debug(g78 g78, String str, Object obj, Object obj2);

    @DexIgnore
    public abstract /* synthetic */ void debug(g78 g78, String str, Throwable th);

    @DexIgnore
    public abstract /* synthetic */ void debug(g78 g78, String str, Object... objArr);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ void debug(String str);

    @DexIgnore
    public abstract /* synthetic */ void debug(String str, Object obj);

    @DexIgnore
    public abstract /* synthetic */ void debug(String str, Object obj, Object obj2);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ void debug(String str, Throwable th);

    @DexIgnore
    public abstract /* synthetic */ void debug(String str, Object... objArr);

    @DexIgnore
    public abstract /* synthetic */ void error(g78 g78, String str);

    @DexIgnore
    public abstract /* synthetic */ void error(g78 g78, String str, Object obj);

    @DexIgnore
    public abstract /* synthetic */ void error(g78 g78, String str, Object obj, Object obj2);

    @DexIgnore
    public abstract /* synthetic */ void error(g78 g78, String str, Throwable th);

    @DexIgnore
    public abstract /* synthetic */ void error(g78 g78, String str, Object... objArr);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ void error(String str);

    @DexIgnore
    public abstract /* synthetic */ void error(String str, Object obj);

    @DexIgnore
    public abstract /* synthetic */ void error(String str, Object obj, Object obj2);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ void error(String str, Throwable th);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ void error(String str, Object... objArr);

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public abstract /* synthetic */ void info(g78 g78, String str);

    @DexIgnore
    public abstract /* synthetic */ void info(g78 g78, String str, Object obj);

    @DexIgnore
    public abstract /* synthetic */ void info(g78 g78, String str, Object obj, Object obj2);

    @DexIgnore
    public abstract /* synthetic */ void info(g78 g78, String str, Throwable th);

    @DexIgnore
    public abstract /* synthetic */ void info(g78 g78, String str, Object... objArr);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ void info(String str);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ void info(String str, Object obj);

    @DexIgnore
    public abstract /* synthetic */ void info(String str, Object obj, Object obj2);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ void info(String str, Throwable th);

    @DexIgnore
    public abstract /* synthetic */ void info(String str, Object... objArr);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ boolean isDebugEnabled();

    @DexIgnore
    public abstract /* synthetic */ boolean isDebugEnabled(g78 g78);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ boolean isErrorEnabled();

    @DexIgnore
    public abstract /* synthetic */ boolean isErrorEnabled(g78 g78);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ boolean isInfoEnabled();

    @DexIgnore
    public abstract /* synthetic */ boolean isInfoEnabled(g78 g78);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ boolean isTraceEnabled();

    @DexIgnore
    public abstract /* synthetic */ boolean isTraceEnabled(g78 g78);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ boolean isWarnEnabled();

    @DexIgnore
    public abstract /* synthetic */ boolean isWarnEnabled(g78 g78);

    @DexIgnore
    public Object readResolve() throws ObjectStreamException {
        return f78.i(getName());
    }

    @DexIgnore
    public abstract /* synthetic */ void trace(g78 g78, String str);

    @DexIgnore
    public abstract /* synthetic */ void trace(g78 g78, String str, Object obj);

    @DexIgnore
    public abstract /* synthetic */ void trace(g78 g78, String str, Object obj, Object obj2);

    @DexIgnore
    public abstract /* synthetic */ void trace(g78 g78, String str, Throwable th);

    @DexIgnore
    public abstract /* synthetic */ void trace(g78 g78, String str, Object... objArr);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ void trace(String str);

    @DexIgnore
    public abstract /* synthetic */ void trace(String str, Object obj);

    @DexIgnore
    public abstract /* synthetic */ void trace(String str, Object obj, Object obj2);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ void trace(String str, Throwable th);

    @DexIgnore
    public abstract /* synthetic */ void trace(String str, Object... objArr);

    @DexIgnore
    public abstract /* synthetic */ void warn(g78 g78, String str);

    @DexIgnore
    public abstract /* synthetic */ void warn(g78 g78, String str, Object obj);

    @DexIgnore
    public abstract /* synthetic */ void warn(g78 g78, String str, Object obj, Object obj2);

    @DexIgnore
    public abstract /* synthetic */ void warn(g78 g78, String str, Throwable th);

    @DexIgnore
    public abstract /* synthetic */ void warn(g78 g78, String str, Object... objArr);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ void warn(String str);

    @DexIgnore
    public abstract /* synthetic */ void warn(String str, Object obj);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ void warn(String str, Object obj, Object obj2);

    @DexIgnore
    @Override // com.fossil.e78
    public abstract /* synthetic */ void warn(String str, Throwable th);

    @DexIgnore
    public abstract /* synthetic */ void warn(String str, Object... objArr);
}
