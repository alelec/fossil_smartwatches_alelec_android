package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d2 implements Parcelable.Creator<e2> {
    @DexIgnore
    public /* synthetic */ d2(kq7 kq7) {
    }

    @DexIgnore
    public e2 a(Parcel parcel) {
        return new e2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public e2 createFromParcel(Parcel parcel) {
        return new e2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public e2[] newArray(int i) {
        return new e2[i];
    }
}
