package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zx7 extends CancellationException implements bv7<zx7> {
    @DexIgnore
    public /* final */ xw7 coroutine;

    @DexIgnore
    public zx7(String str) {
        this(str, null);
    }

    @DexIgnore
    public zx7(String str, xw7 xw7) {
        super(str);
        this.coroutine = xw7;
    }

    @DexIgnore
    @Override // com.fossil.bv7
    public zx7 createCopy() {
        String message = getMessage();
        if (message == null) {
            message = "";
        }
        zx7 zx7 = new zx7(message, this.coroutine);
        zx7.initCause(this);
        return zx7;
    }
}
