package com.fossil;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sf2 implements ThreadFactory {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3251a;
    @DexIgnore
    public /* final */ ThreadFactory b;

    @DexIgnore
    public sf2(String str) {
        this(str, 0);
    }

    @DexIgnore
    public sf2(String str, int i) {
        this.b = Executors.defaultThreadFactory();
        rc2.l(str, "Name must not be null");
        this.f3251a = str;
    }

    @DexIgnore
    public Thread newThread(Runnable runnable) {
        Thread newThread = this.b.newThread(new uf2(runnable, 0));
        newThread.setName(this.f3251a);
        return newThread;
    }
}
