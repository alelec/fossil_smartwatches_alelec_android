package com.fossil;

import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gf1 implements jb1<InputStream> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ od1 f1300a;

    @DexIgnore
    public gf1(od1 od1) {
        this.f1300a = od1;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002a A[Catch:{ all -> 0x005b }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0033 A[SYNTHETIC, Splitter:B:14:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x004d A[SYNTHETIC, Splitter:B:28:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x005f  */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.io.InputStream r7, java.io.File r8, com.fossil.ob1 r9) {
        /*
            r6 = this;
            r3 = 0
            r1 = 1
            r4 = 0
            com.fossil.od1 r0 = r6.f1300a
            r2 = 65536(0x10000, float:9.18355E-41)
            java.lang.Class<byte[]> r5 = byte[].class
            java.lang.Object r0 = r0.g(r2, r5)
            byte[] r0 = (byte[]) r0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0046, all -> 0x0049 }
            r2.<init>(r8)     // Catch:{ IOException -> 0x0046, all -> 0x0049 }
        L_0x0014:
            int r3 = r7.read(r0)     // Catch:{ IOException -> 0x0020, all -> 0x005d }
            r5 = -1
            if (r3 == r5) goto L_0x003d
            r5 = 0
            r2.write(r0, r5, r3)     // Catch:{ IOException -> 0x0020, all -> 0x005d }
            goto L_0x0014
        L_0x0020:
            r1 = move-exception
        L_0x0021:
            java.lang.String r3 = "StreamEncoder"
            r5 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r5)     // Catch:{ all -> 0x005b }
            if (r3 == 0) goto L_0x0031
            java.lang.String r3 = "StreamEncoder"
            java.lang.String r5 = "Failed to encode data onto the OutputStream"
            android.util.Log.d(r3, r5, r1)     // Catch:{ all -> 0x005b }
        L_0x0031:
            if (r2 == 0) goto L_0x005f
            r2.close()     // Catch:{ IOException -> 0x0058 }
            r1 = r4
        L_0x0037:
            com.fossil.od1 r2 = r6.f1300a
            r2.f(r0)
            return r1
        L_0x003d:
            r2.close()
            r2.close()     // Catch:{ IOException -> 0x0044 }
            goto L_0x0037
        L_0x0044:
            r2 = move-exception
            goto L_0x0037
        L_0x0046:
            r1 = move-exception
            r2 = r3
            goto L_0x0021
        L_0x0049:
            r1 = move-exception
            r2 = r3
        L_0x004b:
            if (r2 == 0) goto L_0x0050
            r2.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0050:
            com.fossil.od1 r2 = r6.f1300a
            r2.f(r0)
            throw r1
        L_0x0056:
            r2 = move-exception
            goto L_0x0050
        L_0x0058:
            r1 = move-exception
            r1 = r4
            goto L_0x0037
        L_0x005b:
            r1 = move-exception
            goto L_0x004b
        L_0x005d:
            r1 = move-exception
            goto L_0x004b
        L_0x005f:
            r1 = r4
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gf1.a(java.io.InputStream, java.io.File, com.fossil.ob1):boolean");
    }
}
