package com.fossil;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sm2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ConcurrentHashMap<vm2, List<Throwable>> f3280a; // = new ConcurrentHashMap<>(16, 0.75f, 10);
    @DexIgnore
    public /* final */ ReferenceQueue<Throwable> b; // = new ReferenceQueue<>();

    @DexIgnore
    public final List<Throwable> a(Throwable th, boolean z) {
        Reference<? extends Throwable> poll = this.b.poll();
        while (poll != null) {
            this.f3280a.remove(poll);
            poll = this.b.poll();
        }
        List<Throwable> list = this.f3280a.get(new vm2(th, null));
        if (list != null) {
            return list;
        }
        Vector vector = new Vector(2);
        List<Throwable> putIfAbsent = this.f3280a.putIfAbsent(new vm2(th, this.b), vector);
        return putIfAbsent == null ? vector : putIfAbsent;
    }
}
