package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class in3 extends jn3 {
    @DexIgnore
    public boolean b;

    @DexIgnore
    public in3(pm3 pm3) {
        super(pm3);
        this.f1780a.i(this);
    }

    @DexIgnore
    public void n() {
    }

    @DexIgnore
    public final void o() {
        if (!s()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    @DexIgnore
    public final void p() {
        if (this.b) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!r()) {
            this.f1780a.s();
            this.b = true;
        }
    }

    @DexIgnore
    public final void q() {
        if (!this.b) {
            n();
            this.f1780a.s();
            this.b = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    @DexIgnore
    public abstract boolean r();

    @DexIgnore
    public final boolean s() {
        return this.b;
    }
}
