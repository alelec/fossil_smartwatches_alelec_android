package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum s71 {
    ENABLED(true, true),
    READ_ONLY(true, false),
    WRITE_ONLY(false, true),
    DISABLED(false, false);
    
    @DexIgnore
    public /* final */ boolean readEnabled;
    @DexIgnore
    public /* final */ boolean writeEnabled;

    @DexIgnore
    public s71(boolean z, boolean z2) {
        this.readEnabled = z;
        this.writeEnabled = z2;
    }

    @DexIgnore
    public final boolean getReadEnabled() {
        return this.readEnabled;
    }

    @DexIgnore
    public final boolean getWriteEnabled() {
        return this.writeEnabled;
    }
}
