package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xu7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f4183a;
    @DexIgnore
    public /* final */ Object b;

    @DexIgnore
    public xu7(Object obj, Object obj2) {
        this.f4183a = obj;
        this.b = obj2;
    }

    @DexIgnore
    public String toString() {
        return "CompletedIdempotentResult[" + this.b + ']';
    }
}
