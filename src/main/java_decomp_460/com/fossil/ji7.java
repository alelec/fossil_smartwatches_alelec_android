package com.fossil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.facebook.places.PlaceManager;
import com.facebook.places.model.PlaceFields;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ji7 {
    @DexIgnore
    public static String a(Context context) {
        try {
            if (e(context, "android.permission.READ_PHONE_STATE")) {
                String deviceId = ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getDeviceId();
                if (deviceId != null) {
                    return deviceId;
                }
            } else {
                Log.e("MtaSDK", "Could not get permission of android.permission.READ_PHONE_STATE");
            }
        } catch (Throwable th) {
            Log.e("MtaSDK", "get device id error", th);
        }
        return null;
    }

    @DexIgnore
    public static String b(String str) {
        if (str == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT < 8) {
            return str;
        }
        try {
            return new String(zh7.d(ai7.a(str.getBytes("UTF-8"), 0)), "UTF-8");
        } catch (Throwable th) {
            Log.e("MtaSDK", "decode error", th);
            return str;
        }
    }

    @DexIgnore
    public static JSONArray c(Context context, int i) {
        List<ScanResult> scanResults;
        try {
            if (!e(context, "android.permission.INTERNET") || !e(context, "android.permission.ACCESS_NETWORK_STATE")) {
                Log.e("MtaSDK", "can not get the permisson of android.permission.INTERNET");
                return null;
            }
            WifiManager wifiManager = (WifiManager) context.getSystemService(PlaceManager.PARAM_WIFI);
            if (!(wifiManager == null || (scanResults = wifiManager.getScanResults()) == null || scanResults.size() <= 0)) {
                Collections.sort(scanResults, new ki7());
                JSONArray jSONArray = new JSONArray();
                int i2 = 0;
                while (i2 < scanResults.size() && i2 < i) {
                    ScanResult scanResult = scanResults.get(i2);
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("bs", scanResult.BSSID);
                    jSONObject.put("ss", scanResult.SSID);
                    jSONArray.put(jSONObject);
                    i2++;
                }
                return jSONArray;
            }
            return null;
        } catch (Throwable th) {
            Log.e("MtaSDK", "isWifiNet error", th);
        }
    }

    @DexIgnore
    public static void d(JSONObject jSONObject, String str, String str2) {
        if (str2 != null) {
            try {
                if (str2.length() > 0) {
                    jSONObject.put(str, str2);
                }
            } catch (Throwable th) {
                Log.e("MtaSDK", "jsonPut error", th);
            }
        }
    }

    @DexIgnore
    public static boolean e(Context context, String str) {
        try {
            return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
        } catch (Throwable th) {
            Log.e("MtaSDK", "checkPermission error", th);
            return false;
        }
    }

    @DexIgnore
    public static String f(Context context) {
        if (e(context, "android.permission.ACCESS_WIFI_STATE")) {
            try {
                WifiManager wifiManager = (WifiManager) context.getSystemService(PlaceManager.PARAM_WIFI);
                return wifiManager == null ? "" : wifiManager.getConnectionInfo().getMacAddress();
            } catch (Exception e) {
                Log.e("MtaSDK", "get wifi address error", e);
                return "";
            }
        } else {
            Log.e("MtaSDK", "Could not get permission of android.permission.ACCESS_WIFI_STATE");
            return "";
        }
    }

    @DexIgnore
    public static String g(String str) {
        if (str == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT < 8) {
            return str;
        }
        try {
            return new String(ai7.c(zh7.b(str.getBytes("UTF-8")), 0), "UTF-8");
        } catch (Throwable th) {
            Log.e("MtaSDK", "encode error", th);
            return str;
        }
    }

    @DexIgnore
    public static WifiInfo h(Context context) {
        WifiManager wifiManager;
        if (!e(context, "android.permission.ACCESS_WIFI_STATE") || (wifiManager = (WifiManager) context.getApplicationContext().getSystemService(PlaceManager.PARAM_WIFI)) == null) {
            return null;
        }
        return wifiManager.getConnectionInfo();
    }

    @DexIgnore
    public static String i(Context context) {
        try {
            WifiInfo h = h(context);
            if (h != null) {
                return h.getBSSID();
            }
        } catch (Throwable th) {
            Log.e("MtaSDK", "encode error", th);
        }
        return null;
    }

    @DexIgnore
    public static String j(Context context) {
        try {
            WifiInfo h = h(context);
            if (h != null) {
                return h.getSSID();
            }
        } catch (Throwable th) {
            Log.e("MtaSDK", "encode error", th);
        }
        return null;
    }

    @DexIgnore
    public static boolean k(Context context) {
        try {
            if (!e(context, "android.permission.INTERNET") || !e(context, "android.permission.ACCESS_NETWORK_STATE")) {
                Log.e("MtaSDK", "can not get the permisson of android.permission.INTERNET");
                return false;
            }
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager != null) {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
                    return true;
                }
                Log.w("MtaSDK", "Network error");
                return false;
            }
            return false;
        } catch (Throwable th) {
            Log.e("MtaSDK", "isNetworkAvailable error", th);
        }
    }
}
