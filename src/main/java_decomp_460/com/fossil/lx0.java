package com.fossil;

import android.database.Cursor;
import android.database.SQLException;
import android.os.CancellationSignal;
import android.util.Pair;
import java.io.Closeable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface lx0 extends Closeable {
    @DexIgnore
    Object beginTransaction();  // void declaration

    @DexIgnore
    px0 compileStatement(String str);

    @DexIgnore
    Object endTransaction();  // void declaration

    @DexIgnore
    void execSQL(String str) throws SQLException;

    @DexIgnore
    void execSQL(String str, Object[] objArr) throws SQLException;

    @DexIgnore
    List<Pair<String, String>> getAttachedDbs();

    @DexIgnore
    String getPath();

    @DexIgnore
    boolean inTransaction();

    @DexIgnore
    boolean isOpen();

    @DexIgnore
    Cursor query(ox0 ox0);

    @DexIgnore
    Cursor query(ox0 ox0, CancellationSignal cancellationSignal);

    @DexIgnore
    Cursor query(String str);

    @DexIgnore
    Object setTransactionSuccessful();  // void declaration
}
