package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fw7 extends ex7<xw7> {
    @DexIgnore
    public /* final */ dw7 f;

    @DexIgnore
    public fw7(xw7 xw7, dw7 dw7) {
        super(xw7);
        this.f = dw7;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
        w(th);
        return tl7.f3441a;
    }

    @DexIgnore
    @Override // com.fossil.lz7
    public String toString() {
        return "DisposeOnCompletion[" + this.f + ']';
    }

    @DexIgnore
    @Override // com.fossil.zu7
    public void w(Throwable th) {
        this.f.dispose();
    }
}
