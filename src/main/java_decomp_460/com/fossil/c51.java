package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import androidx.lifecycle.Lifecycle;
import coil.memory.RequestDelegate;
import com.fossil.a18;
import com.fossil.g71;
import com.fossil.s81;
import com.fossil.tn7;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.x71;
import com.fossil.y41;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c51 implements a51, s81 {
    @DexIgnore
    public /* final */ iv7 b; // = jv7.a(ux7.b(null, 1, null).plus(bw7.c().S()));
    @DexIgnore
    public /* final */ CoroutineExceptionHandler c; // = new a(CoroutineExceptionHandler.q);
    @DexIgnore
    public /* final */ t61 d; // = new t61(this, this.m);
    @DexIgnore
    public /* final */ g71 e; // = new g71();
    @DexIgnore
    public /* final */ v51 f; // = new v51(this.l);
    @DexIgnore
    public /* final */ n71 g; // = new n71(this.j);
    @DexIgnore
    public /* final */ y41 h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ Context j;
    @DexIgnore
    public /* final */ z41 k;
    @DexIgnore
    public /* final */ g51 l;
    @DexIgnore
    public /* final */ s61 m;
    @DexIgnore
    public /* final */ c71 s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends nn7 implements CoroutineExceptionHandler {
        @DexIgnore
        public a(tn7.c cVar) {
            super(cVar);
        }

        @DexIgnore
        @Override // kotlinx.coroutines.CoroutineExceptionHandler
        public void handleException(tn7 tn7, Throwable th) {
            pq7.c(tn7, "context");
            pq7.c(th, "exception");
            x81.a("RealImageLoader", th);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public f81 f562a;
        @DexIgnore
        public /* final */ iv7 b;
        @DexIgnore
        public /* final */ g81 c;
        @DexIgnore
        public /* final */ i71 d;
        @DexIgnore
        public /* final */ x71 e;

        @DexIgnore
        public b(iv7 iv7, g81 g81, i71 i71, x71 x71) {
            pq7.c(iv7, "scope");
            pq7.c(g81, "sizeResolver");
            pq7.c(i71, "targetDelegate");
            pq7.c(x71, "request");
            this.b = iv7;
            this.c = g81;
            this.d = i71;
            this.e = x71;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "coil.RealImageLoader$execute$2", f = "RealImageLoader.kt", l = {258}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super Drawable>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Object $data;
        @DexIgnore
        public /* final */ /* synthetic */ x71 $request;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ c51 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends qq7 implements rp7<Throwable, tl7> {
            @DexIgnore
            public /* final */ /* synthetic */ RequestDelegate $requestDelegate;
            @DexIgnore
            public /* final */ /* synthetic */ i71 $targetDelegate;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.c51$c$a$a")
            @eo7(c = "coil.RealImageLoader$execute$2$1$1", f = "RealImageLoader.kt", l = {251}, m = "invokeSuspend")
            /* renamed from: com.fossil.c51$c$a$a  reason: collision with other inner class name */
            public static final class C0030a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Throwable $throwable;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0030a(a aVar, Throwable th, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$throwable = th;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0030a aVar = new C0030a(this.this$0, this.$throwable, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0030a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        this.this$0.$requestDelegate.a();
                        Throwable th = this.$throwable;
                        if (th == null) {
                            return tl7.f3441a;
                        }
                        if (th instanceof CancellationException) {
                            if (q81.c.a() && q81.c.b() <= 4) {
                                Log.println(4, "RealImageLoader", "\ud83c\udfd7  Cancelled - " + this.this$0.this$0.$data);
                            }
                            x71.a m = this.this$0.this$0.$request.m();
                            if (m != null) {
                                m.onCancel(this.this$0.this$0.$data);
                            }
                            return tl7.f3441a;
                        }
                        if (q81.c.a() && q81.c.b() <= 4) {
                            Log.println(4, "RealImageLoader", "\ud83d\udea8 Failed - " + this.this$0.this$0.$data + " - " + this.$throwable);
                        }
                        Drawable j = this.$throwable instanceof v71 ? this.this$0.this$0.$request.j() : this.this$0.this$0.$request.i();
                        a aVar = this.this$0;
                        i71 i71 = aVar.$targetDelegate;
                        n81 w = aVar.this$0.$request.w();
                        this.L$0 = iv7;
                        this.L$1 = j;
                        this.label = 1;
                        if (i71.f(j, w, this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Drawable drawable = (Drawable) this.L$1;
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    x71.a m2 = this.this$0.this$0.$request.m();
                    if (m2 != null) {
                        m2.b(this.this$0.this$0.$data, this.$throwable);
                    }
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, RequestDelegate requestDelegate, i71 i71) {
                super(1);
                this.this$0 = cVar;
                this.$requestDelegate = requestDelegate;
                this.$targetDelegate = i71;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.rp7
            public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
                invoke(th);
                return tl7.f3441a;
            }

            @DexIgnore
            public final void invoke(Throwable th) {
                xw7 unused = gu7.d(this.this$0.this$0.b, bw7.c().S(), null, new C0030a(this, th, null), 2, null);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "coil.RealImageLoader$execute$2$deferred$1", f = "RealImageLoader.kt", l = {503, 548, 568, 215, 578, 230}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super Drawable>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Lifecycle $lifecycle;
            @DexIgnore
            public /* final */ /* synthetic */ i71 $targetDelegate;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$10;
            @DexIgnore
            public Object L$11;
            @DexIgnore
            public Object L$12;
            @DexIgnore
            public Object L$13;
            @DexIgnore
            public Object L$14;
            @DexIgnore
            public Object L$15;
            @DexIgnore
            public Object L$16;
            @DexIgnore
            public Object L$17;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public Object L$9;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(c cVar, Lifecycle lifecycle, i71 i71, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$lifecycle = lifecycle;
                this.$targetDelegate = i71;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, this.$lifecycle, this.$targetDelegate, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Drawable> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r0v48, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARNING: Removed duplicated region for block: B:108:0x06cb  */
            /* JADX WARNING: Removed duplicated region for block: B:109:0x06cf  */
            /* JADX WARNING: Removed duplicated region for block: B:12:0x00f3  */
            /* JADX WARNING: Removed duplicated region for block: B:138:0x0803  */
            /* JADX WARNING: Removed duplicated region for block: B:139:0x0807  */
            /* JADX WARNING: Removed duplicated region for block: B:148:0x08bd  */
            /* JADX WARNING: Removed duplicated region for block: B:170:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:171:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x01ec  */
            /* JADX WARNING: Removed duplicated region for block: B:29:0x027a  */
            /* JADX WARNING: Removed duplicated region for block: B:45:0x03b3  */
            /* JADX WARNING: Removed duplicated region for block: B:52:0x03ec  */
            /* JADX WARNING: Removed duplicated region for block: B:54:0x03ef  */
            /* JADX WARNING: Removed duplicated region for block: B:57:0x040f  */
            /* JADX WARNING: Removed duplicated region for block: B:64:0x04d1  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x006c  */
            /* JADX WARNING: Removed duplicated region for block: B:95:0x0667  */
            /* JADX WARNING: Removed duplicated region for block: B:97:0x0677  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r32) {
                /*
                // Method dump skipped, instructions count: 2280
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.c51.c.b.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(c51 c51, x71 x71, Object obj, qn7 qn7) {
            super(2, qn7);
            this.this$0 = c51;
            this.$request = x71;
            this.$data = obj;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$request, this.$data, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Drawable> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.n();
                g71.a f = this.this$0.e.f(this.$request);
                Lifecycle b2 = f.b();
                dv7 c = f.c();
                i71 b3 = this.this$0.d.b(this.$request);
                rv7<? extends Drawable> a2 = eu7.a(iv7, c, lv7.LAZY, new b(this, b2, b3, null));
                RequestDelegate a3 = this.this$0.d.a(this.$request, b3, b2, c, a2);
                a2.A(new a(this, a3, b3));
                this.L$0 = iv7;
                this.L$1 = b2;
                this.L$2 = c;
                this.L$3 = b3;
                this.L$4 = a2;
                this.L$5 = a3;
                this.label = 1;
                Object l = a2.l(this);
                return l == d ? d : l;
            } else if (i == 1) {
                RequestDelegate requestDelegate = (RequestDelegate) this.L$5;
                rv7 rv7 = (rv7) this.L$4;
                i71 i71 = (i71) this.L$3;
                dv7 dv7 = (dv7) this.L$2;
                Lifecycle lifecycle = (Lifecycle) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "coil.RealImageLoader$load$job$1", f = "RealImageLoader.kt", l = {ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ t71 $request;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ c51 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(c51 c51, t71 t71, qn7 qn7) {
            super(2, qn7);
            this.this$0 = c51;
            this.$request = t71;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$request, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                c51 c51 = this.this$0;
                Object y = this.$request.y();
                t71 t71 = this.$request;
                this.L$0 = iv7;
                this.label = 1;
                if (c51.p(y, t71, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public c51(Context context, z41 z41, g51 g51, s61 s61, c71 c71, a18.a aVar, y41 y41) {
        pq7.c(context, "context");
        pq7.c(z41, Constants.DEFAULTS);
        pq7.c(g51, "bitmapPool");
        pq7.c(s61, "referenceCounter");
        pq7.c(c71, "memoryCache");
        pq7.c(aVar, "callFactory");
        pq7.c(y41, "registry");
        this.j = context;
        this.k = z41;
        this.l = g51;
        this.m = s61;
        this.s = c71;
        y41.a e2 = y41.e();
        e2.c(String.class, new r61());
        e2.c(Uri.class, new m61());
        e2.c(Uri.class, new q61(this.j));
        e2.c(Integer.class, new p61(this.j));
        e2.b(Uri.class, new h61(aVar));
        e2.b(q18.class, new i61(aVar));
        e2.b(File.class, new f61());
        e2.b(Uri.class, new y51(this.j));
        e2.b(Uri.class, new a61(this.j));
        e2.b(Uri.class, new j61(this.j, this.f));
        e2.b(Drawable.class, new b61(this.j, this.f));
        e2.b(Bitmap.class, new z51(this.j));
        e2.a(new p51(this.j));
        this.h = e2.d();
        this.j.registerComponentCallbacks(this);
    }

    @DexIgnore
    @Override // com.fossil.a51
    public z41 a() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.a51
    public z71 b(t71 t71) {
        pq7.c(t71, "request");
        xw7 xw7 = gu7.d(this.b, this.c, null, new d(this, t71, null), 2, null);
        return t71.u() instanceof k81 ? new a81(w81.i(((k81) t71.u()).getView()).d(xw7), (k81) t71.u()) : new r71(xw7);
    }

    @DexIgnore
    public final void n() {
        if (!(!this.i)) {
            throw new IllegalStateException("The image loader is shutdown!".toString());
        }
    }

    @DexIgnore
    public void o() {
        onTrimMemory(80);
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
        s81.a.a(this, configuration);
    }

    @DexIgnore
    public void onLowMemory() {
        s81.a.b(this);
    }

    @DexIgnore
    @Override // com.fossil.s81
    public void onTrimMemory(int i2) {
        this.s.a(i2);
        this.l.a(i2);
    }

    @DexIgnore
    public final /* synthetic */ Object p(Object obj, x71 x71, qn7<? super Drawable> qn7) {
        return eu7.g(bw7.c().S(), new c(this, x71, obj, null), qn7);
    }

    @DexIgnore
    public final boolean q(BitmapDrawable bitmapDrawable, boolean z, f81 f81, e81 e81, x71 x71) {
        boolean z2 = true;
        pq7.c(bitmapDrawable, "cached");
        pq7.c(f81, "size");
        pq7.c(e81, "scale");
        pq7.c(x71, "request");
        Bitmap bitmap = bitmapDrawable.getBitmap();
        if (f81 instanceof c81) {
            pq7.b(bitmap, "bitmap");
            c81 c81 = (c81) f81;
            double c2 = t51.c(bitmap.getWidth(), bitmap.getHeight(), c81.d(), c81.c(), e81);
            if (c2 != 1.0d && !this.e.a(x71)) {
                return false;
            }
            if (c2 > 1.0d && z) {
                return false;
            }
        }
        g71 g71 = this.e;
        pq7.b(bitmap, "bitmap");
        Bitmap.Config config = bitmap.getConfig();
        pq7.b(config, "bitmap.config");
        if (!g71.c(x71, config)) {
            return false;
        }
        if (x71.c() && bitmap.getConfig() == Bitmap.Config.RGB_565) {
            return true;
        }
        if (w81.q(bitmap.getConfig()) != w81.q(x71.d())) {
            z2 = false;
        }
        return z2;
    }

    @DexIgnore
    @Override // com.fossil.a51
    public void shutdown() {
        synchronized (this) {
            if (!this.i) {
                this.i = true;
                jv7.d(this.b, null, 1, null);
                this.j.unregisterComponentCallbacks(this);
                this.g.d();
                o();
            }
        }
    }
}
