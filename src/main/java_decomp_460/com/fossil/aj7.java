package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class aj7 extends ze0 {
    @DexIgnore
    public ListView b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Fragment f279a;

        @DexIgnore
        public a(aj7 aj7, Fragment fragment) {
            this.f279a = fragment;
        }

        @DexIgnore
        @Override // com.fossil.aj7.g
        public void a(cj7 cj7) {
            cj7.c(this.f279a);
        }

        @DexIgnore
        @Override // com.fossil.aj7.g
        public Context getContext() {
            return this.f279a.getContext();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ FragmentActivity f280a;

        @DexIgnore
        public b(aj7 aj7, FragmentActivity fragmentActivity) {
            this.f280a = fragmentActivity;
        }

        @DexIgnore
        @Override // com.fossil.aj7.g
        public void a(cj7 cj7) {
            cj7.b(this.f280a);
        }

        @DexIgnore
        @Override // com.fossil.aj7.g
        public Context getContext() {
            return this.f280a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ g b;

        @DexIgnore
        public c(g gVar) {
            this.b = gVar;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (view.getTag() instanceof cj7) {
                this.b.a((cj7) view.getTag());
                aj7.this.dismiss();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f281a;

        /*
        static {
            int[] iArr = new int[fj7.values().length];
            f281a = iArr;
            try {
                iArr[fj7.Camera.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f281a[fj7.Gallery.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends ArrayAdapter<cj7> {
        @DexIgnore
        public Context b;

        @DexIgnore
        public e(aj7 aj7, Context context, int i, List<cj7> list) {
            super(context, i, list);
            this.b = context;
        }

        @DexIgnore
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(this.b).inflate(kj7.belvedere_dialog_row, viewGroup, false);
            }
            cj7 cj7 = (cj7) getItem(i);
            f a2 = f.a(cj7, this.b);
            ((ImageView) view.findViewById(jj7.belvedere_dialog_row_image)).setImageDrawable(gl0.f(this.b, a2.b()));
            ((TextView) view.findViewById(jj7.belvedere_dialog_row_text)).setText(a2.c());
            view.setTag(cj7);
            return view;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f282a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public f(int i, String str) {
            this.f282a = i;
            this.b = str;
        }

        @DexIgnore
        public static f a(cj7 cj7, Context context) {
            int i = d.f281a[cj7.a().ordinal()];
            return i != 1 ? i != 2 ? new f(-1, context.getString(lj7.belvedere_dialog_unknown)) : new f(ij7.ic_image, context.getString(lj7.belvedere_dialog_gallery)) : new f(ij7.ic_camera, context.getString(lj7.belvedere_dialog_camera));
        }

        @DexIgnore
        public int b() {
            return this.f282a;
        }

        @DexIgnore
        public String c() {
            return this.b;
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(cj7 cj7);

        @DexIgnore
        Context getContext();
    }

    @DexIgnore
    public static void w6(FragmentManager fragmentManager, List<cj7> list) {
        if (list != null && list.size() != 0) {
            aj7 aj7 = new aj7();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("extra_intent", new ArrayList<>(list));
            aj7.setArguments(bundle);
            aj7.show(fragmentManager.j(), "BelvedereDialog");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        ArrayList parcelableArrayList = getArguments().getParcelableArrayList("extra_intent");
        if (getParentFragment() != null) {
            v6(new a(this, getParentFragment()), parcelableArrayList);
        } else if (getActivity() != null) {
            v6(new b(this, getActivity()), parcelableArrayList);
        } else {
            Log.w("BelvedereDialog", "Not able to find a valid context for starting an BelvedereIntent");
            dismiss();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(1, getTheme());
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(kj7.belvedere_dialog, viewGroup, false);
        this.b = (ListView) inflate.findViewById(jj7.belvedere_dialog_listview);
        return inflate;
    }

    @DexIgnore
    public final void v6(g gVar, List<cj7> list) {
        this.b.setAdapter((ListAdapter) new e(this, gVar.getContext(), kj7.belvedere_dialog_row, list));
        this.b.setOnItemClickListener(new c(gVar));
    }
}
