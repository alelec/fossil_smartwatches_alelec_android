package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class la6 implements Factory<ka6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<on5> f2164a;

    @DexIgnore
    public la6(Provider<on5> provider) {
        this.f2164a = provider;
    }

    @DexIgnore
    public static la6 a(Provider<on5> provider) {
        return new la6(provider);
    }

    @DexIgnore
    public static ka6 c(on5 on5) {
        return new ka6(on5);
    }

    @DexIgnore
    /* renamed from: b */
    public ka6 get() {
        return c(this.f2164a.get());
    }
}
