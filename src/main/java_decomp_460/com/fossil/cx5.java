package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cx5 extends du0<DailyHeartRateSummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar c; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public DailyHeartRateSummary h;
    @DexIgnore
    public /* final */ PortfolioApp i;
    @DexIgnore
    public /* final */ dx5 j;
    @DexIgnore
    public /* final */ FragmentManager k;
    @DexIgnore
    public /* final */ pv5 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Date f680a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public String f;
        @DexIgnore
        public int g;
        @DexIgnore
        public String h;

        @DexIgnore
        public a(Date date, boolean z, String str, String str2, int i, String str3, int i2, String str4) {
            pq7.c(str, "mDayOfWeek");
            pq7.c(str2, "mDayOfMonth");
            pq7.c(str3, "mDailyRestingUnit");
            pq7.c(str4, "mDailyMaxUnit");
            this.f680a = date;
            this.b = z;
            this.c = str;
            this.d = str2;
            this.e = i;
            this.f = str3;
            this.g = i2;
            this.h = str4;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(Date date, boolean z, String str, String str2, int i, String str3, int i2, String str4, int i3, kq7 kq7) {
            this((i3 & 1) != 0 ? null : date, (i3 & 2) != 0 ? false : z, (i3 & 4) != 0 ? "" : str, (i3 & 8) != 0 ? "" : str2, (i3 & 16) != 0 ? 0 : i, (i3 & 32) != 0 ? "" : str3, (i3 & 64) == 0 ? i2 : 0, (i3 & 128) == 0 ? str4 : "");
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final int b() {
            return this.g;
        }

        @DexIgnore
        public final String c() {
            return this.f;
        }

        @DexIgnore
        public final int d() {
            return this.e;
        }

        @DexIgnore
        public final Date e() {
            return this.f680a;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final String g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        public final void i(String str) {
            pq7.c(str, "<set-?>");
            this.h = str;
        }

        @DexIgnore
        public final void j(int i) {
            this.g = i;
        }

        @DexIgnore
        public final void k(String str) {
            pq7.c(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final void l(int i) {
            this.e = i;
        }

        @DexIgnore
        public final void m(Date date) {
            this.f680a = date;
        }

        @DexIgnore
        public final void n(String str) {
            pq7.c(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void o(String str) {
            pq7.c(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void p(boolean z) {
            this.b = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Date f681a;
        @DexIgnore
        public /* final */ xe5 b;
        @DexIgnore
        public /* final */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ cx5 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date date = this.b.f681a;
                if (date != null) {
                    this.b.d.j.Q(date);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(cx5 cx5, xe5 xe5, View view) {
            super(view);
            pq7.c(xe5, "binding");
            pq7.c(view, "root");
            this.d = cx5;
            this.b = xe5;
            this.c = view;
            xe5.n().setOnClickListener(new a(this));
        }

        @DexIgnore
        public void b(DailyHeartRateSummary dailyHeartRateSummary) {
            a s = this.d.s(dailyHeartRateSummary);
            this.f681a = s.e();
            FlexibleTextView flexibleTextView = this.b.t;
            pq7.b(flexibleTextView, "binding.ftvDayOfWeek");
            flexibleTextView.setText(s.g());
            FlexibleTextView flexibleTextView2 = this.b.s;
            pq7.b(flexibleTextView2, "binding.ftvDayOfMonth");
            flexibleTextView2.setText(s.f());
            if (s.b() == 0 && s.d() == 0) {
                ConstraintLayout constraintLayout = this.b.q;
                pq7.b(constraintLayout, "binding.clContainer");
                constraintLayout.setVisibility(8);
                FlexibleTextView flexibleTextView3 = this.b.x;
                pq7.b(flexibleTextView3, "binding.ftvNoRecord");
                flexibleTextView3.setVisibility(0);
            } else {
                ConstraintLayout constraintLayout2 = this.b.q;
                pq7.b(constraintLayout2, "binding.clContainer");
                constraintLayout2.setVisibility(0);
                FlexibleTextView flexibleTextView4 = this.b.x;
                pq7.b(flexibleTextView4, "binding.ftvNoRecord");
                flexibleTextView4.setVisibility(8);
                FlexibleTextView flexibleTextView5 = this.b.z;
                pq7.b(flexibleTextView5, "binding.ftvRestingValue");
                flexibleTextView5.setText(String.valueOf(s.d()));
                FlexibleTextView flexibleTextView6 = this.b.y;
                pq7.b(flexibleTextView6, "binding.ftvRestingUnit");
                flexibleTextView6.setText(s.c());
                FlexibleTextView flexibleTextView7 = this.b.w;
                pq7.b(flexibleTextView7, "binding.ftvMaxValue");
                flexibleTextView7.setText(String.valueOf(s.b()));
                FlexibleTextView flexibleTextView8 = this.b.v;
                pq7.b(flexibleTextView8, "binding.ftvMaxUnit");
                flexibleTextView8.setText(s.a());
            }
            ConstraintLayout constraintLayout3 = this.b.r;
            pq7.b(constraintLayout3, "binding.container");
            constraintLayout3.setSelected(!s.h());
            if (s.h()) {
                this.b.r.setBackgroundColor(this.d.e);
                this.b.t.setBackgroundColor(this.d.e);
                this.b.s.setBackgroundColor(this.d.e);
                this.b.t.setTextColor(this.d.f);
                this.b.s.setTextColor(this.d.d);
                return;
            }
            this.b.r.setBackgroundColor(this.d.g);
            this.b.t.setBackgroundColor(this.d.g);
            this.b.s.setBackgroundColor(this.d.g);
            this.b.t.setTextColor(this.d.f);
            this.b.s.setTextColor(this.d.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Date f682a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public c(Date date, Date date2, String str, String str2) {
            pq7.c(str, "mWeekly");
            pq7.c(str2, "mWeeklyValue");
            this.f682a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(Date date, Date date2, String str, String str2, int i, kq7 kq7) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.f682a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        public final void e(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void f(Date date) {
            this.f682a = date;
        }

        @DexIgnore
        public final void g(String str) {
            pq7.c(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void h(String str) {
            pq7.c(str, "<set-?>");
            this.d = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends b {
        @DexIgnore
        public Date e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public /* final */ ze5 g;
        @DexIgnore
        public /* final */ /* synthetic */ cx5 h;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d b;

            @DexIgnore
            public a(d dVar) {
                this.b = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.e != null && this.b.f != null) {
                    dx5 dx5 = this.b.h.j;
                    Date date = this.b.e;
                    if (date != null) {
                        Date date2 = this.b.f;
                        if (date2 != null) {
                            dx5.q0(date, date2);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public d(com.fossil.cx5 r4, com.fossil.ze5 r5) {
            /*
                r3 = this;
                java.lang.String r0 = "binding"
                com.fossil.pq7.c(r5, r0)
                r3.h = r4
                com.fossil.xe5 r0 = r5.r
                if (r0 == 0) goto L_0x0029
                java.lang.String r1 = "binding.dailyItem!!"
                com.fossil.pq7.b(r0, r1)
                android.view.View r1 = r5.n()
                java.lang.String r2 = "binding.root"
                com.fossil.pq7.b(r1, r2)
                r3.<init>(r4, r0, r1)
                r3.g = r5
                androidx.constraintlayout.widget.ConstraintLayout r0 = r5.q
                com.fossil.cx5$d$a r1 = new com.fossil.cx5$d$a
                r1.<init>(r3)
                r0.setOnClickListener(r1)
                return
            L_0x0029:
                com.fossil.pq7.i()
                r0 = 0
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.cx5.d.<init>(com.fossil.cx5, com.fossil.ze5):void");
        }

        @DexIgnore
        @Override // com.fossil.cx5.b
        public void b(DailyHeartRateSummary dailyHeartRateSummary) {
            c t = this.h.t(dailyHeartRateSummary);
            this.f = t.a();
            this.e = t.b();
            FlexibleTextView flexibleTextView = this.g.s;
            pq7.b(flexibleTextView, "binding.ftvWeekly");
            flexibleTextView.setText(t.c());
            FlexibleTextView flexibleTextView2 = this.g.t;
            pq7.b(flexibleTextView2, "binding.ftvWeeklyValue");
            flexibleTextView2.setText(t.d());
            super.b(dailyHeartRateSummary);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ cx5 b;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        public e(cx5 cx5, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.b = cx5;
            this.c = viewHolder;
            this.d = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            pq7.c(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - mFragment.id=" + this.b.l.getId() + ", isAdded=" + this.b.l.isAdded());
            this.c.itemView.removeOnAttachStateChangeListener(this);
            Fragment Z = this.b.k.Z(this.b.l.D6());
            if (Z == null) {
                FLogger.INSTANCE.getLocal().d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                xq0 j = this.b.k.j();
                j.b(view.getId(), this.b.l, this.b.l.D6());
                j.k();
            } else if (this.d) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - oldFragment.id=" + Z.getId() + ", isAdded=" + Z.isAdded());
                xq0 j2 = this.b.k.j();
                j2.q(Z);
                j2.k();
                xq0 j3 = this.b.k.j();
                j3.b(view.getId(), this.b.l, this.b.l.D6());
                j3.k();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - oldFragment.id=" + Z.getId() + ", isAdded=" + Z.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.b.l.getId() + ", isAdded2=" + this.b.l.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            pq7.c(view, "v");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ FrameLayout f683a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(FrameLayout frameLayout, View view) {
            super(view);
            this.f683a = frameLayout;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cx5(bx5 bx5, PortfolioApp portfolioApp, dx5 dx5, FragmentManager fragmentManager, pv5 pv5) {
        super(bx5);
        pq7.c(bx5, "dailyHeartRateSummaryDifference");
        pq7.c(portfolioApp, "mApp");
        pq7.c(dx5, "mOnItemClick");
        pq7.c(fragmentManager, "mFragmentManager");
        pq7.c(pv5, "mFragment");
        this.i = portfolioApp;
        this.j = dx5;
        this.k = fragmentManager;
        this.l = pv5;
        String d2 = qn5.l.a().d("primaryText");
        this.d = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = qn5.l.a().d("nonBrandSurface");
        this.e = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = qn5.l.a().d("secondaryText");
        this.f = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = qn5.l.a().d("nonBrandActivityDetailBackground");
        this.g = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i2) {
        if (getItemViewType(i2) != 0) {
            return super.getItemId(i2);
        }
        if (this.l.getId() == 0) {
            return 1010101;
        }
        return (long) this.l.getId();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) getItem(i2);
        if (dailyHeartRateSummary != null) {
            Calendar calendar = this.c;
            pq7.b(calendar, "mCalendar");
            calendar.setTime(dailyHeartRateSummary.getDate());
            Calendar calendar2 = this.c;
            pq7.b(calendar2, "mCalendar");
            Boolean p0 = lk5.p0(calendar2.getTime());
            pq7.b(p0, "DateHelper.isToday(mCalendar.time)");
            if (p0.booleanValue() || this.c.get(7) == 7) {
                return 2;
            }
        }
        return 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        boolean z = true;
        pq7.c(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardHeartRatesAdapter", "onBindViewHolder - position=" + i2);
        int itemViewType = getItemViewType(i2);
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            pq7.b(view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            pq7.b(view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardHeartRatesAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            pq7.b(view3, "holder.itemView");
            view3.setId((int) getItemId(i2));
            viewHolder.itemView.addOnAttachStateChangeListener(new e(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((b) viewHolder).b((DailyHeartRateSummary) getItem(i2));
        } else if (itemViewType != 2) {
            ((b) viewHolder).b((DailyHeartRateSummary) getItem(i2));
        } else {
            ((d) viewHolder).b((DailyHeartRateSummary) getItem(i2));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        pq7.c(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i2 == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new f(frameLayout, frameLayout);
        } else if (i2 == 1) {
            xe5 z = xe5.z(from, viewGroup, false);
            pq7.b(z, "ItemHeartRateDayBinding.\u2026tInflater, parent, false)");
            View n = z.n();
            pq7.b(n, "itemActivityDayBinding.root");
            return new b(this, z, n);
        } else if (i2 != 2) {
            xe5 z2 = xe5.z(from, viewGroup, false);
            pq7.b(z2, "ItemHeartRateDayBinding.\u2026tInflater, parent, false)");
            View n2 = z2.n();
            pq7.b(n2, "itemActivityDayBinding.root");
            return new b(this, z2, n2);
        } else {
            ze5 z3 = ze5.z(from, viewGroup, false);
            pq7.b(z3, "ItemHeartRateWeekBinding\u2026tInflater, parent, false)");
            return new d(this, z3);
        }
    }

    @DexIgnore
    public final a s(DailyHeartRateSummary dailyHeartRateSummary) {
        boolean z = false;
        a aVar = new a(null, false, null, null, 0, null, 0, null, 255, null);
        if (dailyHeartRateSummary != null) {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "calendar");
            instance.setTime(dailyHeartRateSummary.getDate());
            int i2 = instance.get(7);
            Boolean p0 = lk5.p0(instance.getTime());
            pq7.b(p0, "DateHelper.isToday(calendar.time)");
            if (p0.booleanValue()) {
                String c2 = um5.c(this.i, 2131886644);
                pq7.b(c2, "LanguageHelper.getString\u2026artRateToday_Text__Today)");
                aVar.o(c2);
            } else {
                aVar.o(jl5.b.i(i2));
            }
            aVar.m(instance.getTime());
            aVar.n(String.valueOf(instance.get(5)));
            Resting resting = dailyHeartRateSummary.getResting();
            aVar.l(resting != null ? resting.getValue() : 0);
            String c3 = um5.c(this.i, 2131886677);
            pq7.b(c3, "LanguageHelper.getString\u2026tepsToday_Label__Resting)");
            if (c3 != null) {
                String lowerCase = c3.toLowerCase();
                pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                aVar.k(lowerCase);
                aVar.j(dailyHeartRateSummary.getMax());
                String c4 = um5.c(this.i, 2131886675);
                pq7.b(c4, "LanguageHelper.getString\u2026in_StepsToday_Label__Max)");
                if (c4 != null) {
                    String lowerCase2 = c4.toLowerCase();
                    pq7.b(lowerCase2, "(this as java.lang.String).toLowerCase()");
                    aVar.i(lowerCase2);
                    if (aVar.d() + aVar.b() == 0) {
                        z = true;
                    }
                    aVar.p(z);
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
        }
        return aVar;
    }

    @DexIgnore
    public final c t(DailyHeartRateSummary dailyHeartRateSummary) {
        String str;
        c cVar = new c(null, null, null, null, 15, null);
        if (dailyHeartRateSummary != null) {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "calendar");
            instance.setTime(dailyHeartRateSummary.getDate());
            Boolean p0 = lk5.p0(instance.getTime());
            int i2 = instance.get(5);
            int i3 = instance.get(2);
            String N = lk5.N(i3);
            int i4 = instance.get(1);
            cVar.e(instance.getTime());
            instance.add(5, -6);
            int i5 = instance.get(5);
            int i6 = instance.get(2);
            String N2 = lk5.N(i6);
            int i7 = instance.get(1);
            cVar.f(instance.getTime());
            pq7.b(p0, "isToday");
            if (p0.booleanValue()) {
                str = um5.c(this.i, 2131886646);
                pq7.b(str, "LanguageHelper.getString\u2026ateToday_Title__ThisWeek)");
            } else if (i3 == i6) {
                str = N2 + ' ' + i5 + " - " + N2 + ' ' + i2;
            } else if (i7 == i4) {
                str = N2 + ' ' + i5 + " - " + N + ' ' + i2;
            } else {
                str = N2 + ' ' + i5 + ", " + i7 + " - " + N + ' ' + i2 + ", " + i4;
            }
            cVar.g(str);
            String valueOf = dailyHeartRateSummary.getAvgRestingHeartRateOfWeek() == null ? "0" : String.valueOf(dailyHeartRateSummary.getAvgRestingHeartRateOfWeek());
            hr7 hr7 = hr7.f1520a;
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886643);
            pq7.b(c2, "LanguageHelper.getString\u2026y_Text__NumberRestingBpm)");
            String format = String.format(c2, Arrays.copyOf(new Object[]{valueOf}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            cVar.h(format);
        }
        return cVar;
    }

    @DexIgnore
    public final void u(cu0<DailyHeartRateSummary> cu0) {
        List<DailyHeartRateSummary> D;
        Integer num = null;
        if (!(cu0 == null || (D = cu0.D()) == null)) {
            Calendar instance = Calendar.getInstance();
            pq7.b(D, "summaries");
            if (!D.isEmpty()) {
                pq7.b(instance, "calendar");
                instance.setTime(((DailyHeartRateSummary) pm7.F(D)).getDate());
                if (!lk5.p0(instance.getTime()).booleanValue()) {
                    instance.setTime(new Date());
                    Date time = instance.getTime();
                    pq7.b(time, "calendar.time");
                    Date time2 = instance.getTime();
                    pq7.b(time2, "calendar.time");
                    long time3 = time2.getTime();
                    Date time4 = instance.getTime();
                    pq7.b(time4, "calendar.time");
                    new DailyHeartRateSummary(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, time, time3, time4.getTime(), 0, 0, 0, null);
                } else {
                    Object F = pm7.F(D);
                    pq7.b(F, "summaries.first()");
                    this.h = (DailyHeartRateSummary) F;
                }
            } else {
                pq7.b(instance, "calendar");
                Date time5 = instance.getTime();
                pq7.b(time5, "calendar.time");
                Date time6 = instance.getTime();
                pq7.b(time6, "calendar.time");
                long time7 = time6.getTime();
                Date time8 = instance.getTime();
                pq7.b(time8, "calendar.time");
                new DailyHeartRateSummary(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, time5, time7, time8.getTime(), 0, 0, 0, null);
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList - size=");
        if (cu0 != null) {
            num = Integer.valueOf(cu0.size());
        }
        sb.append(num);
        local.d("DashboardHeartRatesAdapter", sb.toString());
        super.i(cu0);
    }
}
