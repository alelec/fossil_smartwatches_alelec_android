package com.fossil;

import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import com.fossil.a42;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xl3 extends in3 {
    @DexIgnore
    public static /* final */ Pair<String, Long> D; // = new Pair<>("", 0L);
    @DexIgnore
    public /* final */ dm3 A; // = new dm3(this, "deferred_attribution_cache", null);
    @DexIgnore
    public /* final */ bm3 B; // = new bm3(this, "deferred_attribution_cache_timestamp", 0);
    @DexIgnore
    public /* final */ yl3 C; // = new yl3(this, "default_event_parameters", null);
    @DexIgnore
    public SharedPreferences c;
    @DexIgnore
    public am3 d;
    @DexIgnore
    public /* final */ bm3 e; // = new bm3(this, "last_upload", 0);
    @DexIgnore
    public /* final */ bm3 f; // = new bm3(this, "last_upload_attempt", 0);
    @DexIgnore
    public /* final */ bm3 g; // = new bm3(this, "backoff", 0);
    @DexIgnore
    public /* final */ bm3 h; // = new bm3(this, "last_delete_stale", 0);
    @DexIgnore
    public /* final */ bm3 i; // = new bm3(this, "midnight_offset", 0);
    @DexIgnore
    public /* final */ bm3 j; // = new bm3(this, "first_open_time", 0);
    @DexIgnore
    public /* final */ bm3 k; // = new bm3(this, "app_install_time", 0);
    @DexIgnore
    public /* final */ dm3 l; // = new dm3(this, "app_instance_id", null);
    @DexIgnore
    public String m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public long o;
    @DexIgnore
    public /* final */ bm3 p; // = new bm3(this, "time_before_start", ButtonService.CONNECT_TIMEOUT);
    @DexIgnore
    public /* final */ bm3 q; // = new bm3(this, "session_timeout", 1800000);
    @DexIgnore
    public /* final */ zl3 r; // = new zl3(this, "start_new_session", true);
    @DexIgnore
    public /* final */ dm3 s; // = new dm3(this, "non_personalized_ads", null);
    @DexIgnore
    public /* final */ zl3 t; // = new zl3(this, "allow_remote_dynamite", false);
    @DexIgnore
    public /* final */ bm3 u; // = new bm3(this, "last_pause_time", 0);
    @DexIgnore
    public boolean v;
    @DexIgnore
    public zl3 w; // = new zl3(this, "app_backgrounded", false);
    @DexIgnore
    public zl3 x; // = new zl3(this, "deep_link_retrieval_complete", false);
    @DexIgnore
    public bm3 y; // = new bm3(this, "deep_link_retrieval_attempts", 0);
    @DexIgnore
    public /* final */ dm3 z; // = new dm3(this, "firebase_feature_rollouts", null);

    @DexIgnore
    public xl3(pm3 pm3) {
        super(pm3);
    }

    @DexIgnore
    public final void A(String str) {
        h();
        SharedPreferences.Editor edit = B().edit();
        edit.putString("admob_app_id", str);
        edit.apply();
    }

    @DexIgnore
    public final SharedPreferences B() {
        h();
        o();
        return this.c;
    }

    @DexIgnore
    public final String C() {
        h();
        return B().getString("gmp_app_id", null);
    }

    @DexIgnore
    public final String D() {
        h();
        return B().getString("admob_app_id", null);
    }

    @DexIgnore
    public final Boolean E() {
        h();
        if (!B().contains("use_service")) {
            return null;
        }
        return Boolean.valueOf(B().getBoolean("use_service", false));
    }

    @DexIgnore
    public final void F() {
        h();
        Boolean G = G();
        SharedPreferences.Editor edit = B().edit();
        edit.clear();
        edit.apply();
        if (G != null) {
            x(G.booleanValue());
        }
    }

    @DexIgnore
    public final Boolean G() {
        h();
        if (B().contains("measurement_enabled")) {
            return Boolean.valueOf(B().getBoolean("measurement_enabled", true));
        }
        return null;
    }

    @DexIgnore
    public final String H() {
        h();
        String string = B().getString("previous_os_version", null);
        i().o();
        String str = Build.VERSION.RELEASE;
        if (!TextUtils.isEmpty(str) && !str.equals(string)) {
            SharedPreferences.Editor edit = B().edit();
            edit.putString("previous_os_version", str);
            edit.apply();
        }
        return string;
    }

    @DexIgnore
    public final boolean I() {
        return this.c.contains("deferred_analytics_collection");
    }

    @DexIgnore
    @Override // com.fossil.in3
    public final void n() {
        SharedPreferences sharedPreferences = e().getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
        this.c = sharedPreferences;
        boolean z2 = sharedPreferences.getBoolean("has_been_opened", false);
        this.v = z2;
        if (!z2) {
            SharedPreferences.Editor edit = this.c.edit();
            edit.putBoolean("has_been_opened", true);
            edit.apply();
        }
        this.d = new am3(this, "health_monitor", Math.max(0L, xg3.c.a(null).longValue()));
    }

    @DexIgnore
    @Override // com.fossil.in3
    public final boolean r() {
        return true;
    }

    @DexIgnore
    public final Pair<String, Boolean> t(String str) {
        h();
        long c2 = zzm().c();
        if (this.m != null && c2 < this.o) {
            return new Pair<>(this.m, Boolean.valueOf(this.n));
        }
        this.o = c2 + m().p(str, xg3.b);
        a42.d(true);
        try {
            a42.a b = a42.b(e());
            if (b != null) {
                this.m = b.a();
                this.n = b.b();
            }
            if (this.m == null) {
                this.m = "";
            }
        } catch (Exception e2) {
            d().M().b("Unable to get advertising id", e2);
            this.m = "";
        }
        a42.d(false);
        return new Pair<>(this.m, Boolean.valueOf(this.n));
    }

    @DexIgnore
    public final void u(boolean z2) {
        h();
        SharedPreferences.Editor edit = B().edit();
        edit.putBoolean("use_service", z2);
        edit.apply();
    }

    @DexIgnore
    public final boolean v(long j2) {
        return j2 - this.q.a() > this.u.a();
    }

    @DexIgnore
    public final String w(String str) {
        h();
        String str2 = (String) t(str).first;
        MessageDigest I0 = kr3.I0();
        if (I0 == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new BigInteger(1, I0.digest(str2.getBytes())));
    }

    @DexIgnore
    public final void x(boolean z2) {
        h();
        SharedPreferences.Editor edit = B().edit();
        edit.putBoolean("measurement_enabled", z2);
        edit.apply();
    }

    @DexIgnore
    public final void y(String str) {
        h();
        SharedPreferences.Editor edit = B().edit();
        edit.putString("gmp_app_id", str);
        edit.apply();
    }

    @DexIgnore
    public final void z(boolean z2) {
        h();
        d().N().b("App measurement setting deferred collection", Boolean.valueOf(z2));
        SharedPreferences.Editor edit = B().edit();
        edit.putBoolean("deferred_analytics_collection", z2);
        edit.apply();
    }
}
