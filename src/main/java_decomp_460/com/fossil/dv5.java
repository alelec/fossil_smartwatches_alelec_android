package com.fossil;

import com.fossil.iq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dv5 extends iq4<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public /* final */ un5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return dv5.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ WeakReference<ls5> f838a;

        @DexIgnore
        public b(WeakReference<ls5> weakReference) {
            pq7.c(weakReference, "activityContext");
            this.f838a = weakReference;
        }

        @DexIgnore
        public final WeakReference<ls5> a() {
            return this.f838a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f839a;
        @DexIgnore
        public /* final */ z52 b;

        @DexIgnore
        public c(int i, z52 z52) {
            this.f839a = i;
            this.b = z52;
        }

        @DexIgnore
        public final int a() {
            return this.f839a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ SignUpSocialAuth f840a;

        @DexIgnore
        public d(SignUpSocialAuth signUpSocialAuth) {
            pq7.c(signUpSocialAuth, "auth");
            this.f840a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.f840a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements yn5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ dv5 f841a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(dv5 dv5) {
            this.f841a = dv5;
        }

        @DexIgnore
        @Override // com.fossil.yn5
        public void a(SignUpSocialAuth signUpSocialAuth) {
            pq7.c(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = dv5.f.a();
            local.d(a2, "Inside .onLoginSuccess with result=" + signUpSocialAuth);
            this.f841a.j(new d(signUpSocialAuth));
        }

        @DexIgnore
        @Override // com.fossil.yn5
        public void b(int i, z52 z52, String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = dv5.f.a();
            local.d(a2, "Inside .onLoginFailed with errorCode=" + i + ", connectionResult=" + z52);
            this.f841a.i(new c(i, z52));
        }
    }

    /*
    static {
        String simpleName = dv5.class.getSimpleName();
        pq7.b(simpleName, "LoginFacebookUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public dv5(un5 un5) {
        pq7.c(un5, "mLoginFacebookManager");
        this.d = un5;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return e;
    }

    @DexIgnore
    /* renamed from: n */
    public Object k(b bVar, qn7<Object> qn7) {
        try {
            FLogger.INSTANCE.getLocal().d(e, "running UseCase");
            if (!PortfolioApp.h0.c().p0()) {
                return i(new c(601, null));
            }
            un5 un5 = this.d;
            WeakReference<ls5> a2 = bVar != null ? bVar.a() : null;
            if (a2 != null) {
                ls5 ls5 = a2.get();
                if (ls5 != null) {
                    pq7.b(ls5, "requestValues?.activityContext!!.get()!!");
                    un5.c(ls5, new e(this));
                    return tl7.f3441a;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "Inside .run failed with exception=" + e2);
            return new c(600, null);
        }
    }
}
