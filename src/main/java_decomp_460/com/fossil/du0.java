package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.ut0;
import com.fossil.zu0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class du0<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ut0<T> f832a;
    @DexIgnore
    public /* final */ ut0.c<T> b; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ut0.c<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.ut0.c
        public void a(cu0<T> cu0, cu0<T> cu02) {
            du0.this.g(cu02);
            du0.this.h(cu0, cu02);
        }
    }

    @DexIgnore
    public du0(zu0.d<T> dVar) {
        ut0<T> ut0 = new ut0<>(this, dVar);
        this.f832a = ut0;
        ut0.a(this.b);
    }

    @DexIgnore
    @Deprecated
    public void g(cu0<T> cu0) {
    }

    @DexIgnore
    public T getItem(int i) {
        return this.f832a.b(i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f832a.c();
    }

    @DexIgnore
    public void h(cu0<T> cu0, cu0<T> cu02) {
    }

    @DexIgnore
    public void i(cu0<T> cu0) {
        this.f832a.f(cu0);
    }
}
