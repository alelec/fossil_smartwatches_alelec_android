package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qc3 extends ds2 implements pc3 {
    @DexIgnore
    public qc3() {
        super("com.google.android.gms.maps.internal.IOnMapReadyCallback");
    }

    @DexIgnore
    @Override // com.fossil.ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        zb3 od3;
        if (i != 1) {
            return false;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        if (readStrongBinder == null) {
            od3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IGoogleMapDelegate");
            od3 = queryLocalInterface instanceof zb3 ? (zb3) queryLocalInterface : new od3(readStrongBinder);
        }
        l2(od3);
        parcel2.writeNoException();
        return true;
    }
}
