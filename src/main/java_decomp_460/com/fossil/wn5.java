package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import com.fossil.q47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.WechatToken;
import com.portfolio.platform.data.source.remote.WechatApiService;
import com.portfolio.platform.manager.SoLibraryLoader;
import java.net.SocketTimeoutException;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wn5 implements tf7 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ a e; // = new a(null);
    @DexIgnore
    public WechatApiService b; // = ((WechatApiService) tq5.g.b(WechatApiService.class));
    @DexIgnore
    public boolean c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return wn5.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ wn5 b;
        @DexIgnore
        public /* final */ /* synthetic */ yn5 c;

        @DexIgnore
        public b(wn5 wn5, yn5 yn5) {
            this.b = wn5;
            this.c = yn5;
        }

        @DexIgnore
        public final void run() {
            if (!this.b.e()) {
                this.c.b(500, null, "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements q47.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ wn5 f3975a;
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;
        @DexIgnore
        public /* final */ /* synthetic */ yn5 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1", f = "MFLoginWechatManager.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $authToken;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.wn5$c$a$a")
            /* renamed from: com.fossil.wn5$c$a$a  reason: collision with other inner class name */
            public static final class C0275a implements c88<WechatToken> {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ a f3976a;

                @DexIgnore
                /* JADX WARN: Incorrect args count in method signature: ()V */
                public C0275a(a aVar) {
                    this.f3976a = aVar;
                }

                @DexIgnore
                @Override // com.fossil.c88
                public void onFailure(Call<WechatToken> call, Throwable th) {
                    pq7.c(call, "call");
                    pq7.c(th, "t");
                    FLogger.INSTANCE.getLocal().d(wn5.e.a(), "getWechatToken onFailure");
                    if (th instanceof SocketTimeoutException) {
                        this.f3976a.this$0.c.b(MFNetworkReturnCode.CLIENT_TIMEOUT, null, "");
                    } else {
                        this.f3976a.this$0.c.b(601, null, "");
                    }
                }

                @DexIgnore
                @Override // com.fossil.c88
                public void onResponse(Call<WechatToken> call, q88<WechatToken> q88) {
                    pq7.c(call, "call");
                    pq7.c(q88, "response");
                    if (q88.e()) {
                        FLogger.INSTANCE.getLocal().d(wn5.e.a(), "getWechatToken isSuccessful");
                        WechatToken a2 = q88.a();
                        if (a2 != null) {
                            pq7.b(a2, "response.body()!!");
                            WechatToken wechatToken = a2;
                            this.f3976a.this$0.f3975a.h(wechatToken.getOpenId());
                            SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                            signUpSocialAuth.setToken(wechatToken.getAccessToken());
                            signUpSocialAuth.setService("wechat");
                            this.f3976a.this$0.c.a(signUpSocialAuth);
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                    FLogger.INSTANCE.getLocal().d(wn5.e.a(), "getWechatToken isNotSuccessful");
                    this.f3976a.this$0.c.b(600, null, q88.f());
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, String str, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$authToken = str;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$authToken, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                String str;
                String d;
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    Access c = SoLibraryLoader.f().c(PortfolioApp.h0.c());
                    WechatApiService d2 = this.this$0.f3975a.d();
                    String str2 = (c == null || (d = c.getD()) == null) ? "" : d;
                    if (c == null || (str = c.getE()) == null) {
                        str = "";
                    }
                    d2.getWechatToken(str2, str, this.$authToken, "authorization_code").D(new C0275a(this));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public c(wn5 wn5, Handler handler, yn5 yn5) {
            this.f3975a = wn5;
            this.b = handler;
            this.c = yn5;
        }

        @DexIgnore
        @Override // com.fossil.q47.a
        public void a() {
            this.f3975a.i(true);
            this.b.removeCallbacksAndMessages(null);
            FLogger.INSTANCE.getLocal().d(wn5.e.a(), "Wechat onAuthFailed");
            this.c.b(600, null, "");
        }

        @DexIgnore
        @Override // com.fossil.q47.a
        public void b() {
            this.f3975a.i(true);
            this.b.removeCallbacksAndMessages(null);
            FLogger.INSTANCE.getLocal().d(wn5.e.a(), "Wechat onAuthAppNotInstalled");
            this.c.b(600, null, "");
        }

        @DexIgnore
        @Override // com.fossil.q47.a
        public void c() {
            this.f3975a.i(true);
            this.b.removeCallbacksAndMessages(null);
            FLogger.INSTANCE.getLocal().d(wn5.e.a(), "Wechat onAuthCancel");
            this.c.b(2, null, "");
        }

        @DexIgnore
        @Override // com.fossil.q47.a
        public void d(String str) {
            pq7.c(str, "authToken");
            this.f3975a.i(true);
            this.b.removeCallbacksAndMessages(null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = wn5.e.a();
            local.d(a2, "Wechat onAuthSuccess: " + str);
            FLogger.INSTANCE.getLocal().d(wn5.e.a(), "Wechat step 1: Login using wechat success");
            xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new a(this, str, null), 3, null);
        }
    }

    /*
    static {
        String simpleName = wn5.class.getSimpleName();
        pq7.b(simpleName, "MFLoginWechatManager::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public wn5() {
        tq5.g.d("https://api.weixin.qq.com/sns/");
        tq5.g.f(null);
    }

    @DexIgnore
    @Override // com.fossil.tf7
    public void a(df7 df7) {
        pq7.c(df7, "baseReq");
        q47.i().a(df7);
    }

    @DexIgnore
    @Override // com.fossil.tf7
    public void b(ef7 ef7) {
        pq7.c(ef7, "baseResp");
        q47.i().b(ef7);
    }

    @DexIgnore
    public final WechatApiService d() {
        return this.b;
    }

    @DexIgnore
    public final boolean e() {
        return this.c;
    }

    @DexIgnore
    public final void f(Activity activity, yn5 yn5) {
        pq7.c(activity, Constants.ACTIVITY);
        pq7.c(yn5, Constants.CALLBACK);
        q47.i().d(activity);
        q47.i().c(activity.getIntent(), this);
        this.c = false;
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new b(this, yn5), 120000);
        q47.i().j(new c(this, handler, yn5));
    }

    @DexIgnore
    public final void g(Intent intent) {
        pq7.c(intent, "intent");
        q47.i().c(intent, this);
    }

    @DexIgnore
    public final void h(String str) {
    }

    @DexIgnore
    public final void i(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public final void j(String str) {
        pq7.c(str, "appId");
        q47.i().h(str);
    }
}
