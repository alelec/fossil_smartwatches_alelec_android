package com.fossil;

import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class er5 implements Factory<dr5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<PortfolioApp> f981a;
    @DexIgnore
    public /* final */ Provider<tt4> b;
    @DexIgnore
    public /* final */ Provider<zt4> c;
    @DexIgnore
    public /* final */ Provider<on5> d;

    @DexIgnore
    public er5(Provider<PortfolioApp> provider, Provider<tt4> provider2, Provider<zt4> provider3, Provider<on5> provider4) {
        this.f981a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static er5 a(Provider<PortfolioApp> provider, Provider<tt4> provider2, Provider<zt4> provider3, Provider<on5> provider4) {
        return new er5(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static dr5 c(PortfolioApp portfolioApp, tt4 tt4, zt4 zt4, on5 on5) {
        return new dr5(portfolioApp, tt4, zt4, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public dr5 get() {
        return c(this.f981a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
