package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g83 implements xw2<f83> {
    @DexIgnore
    public static g83 c; // = new g83();
    @DexIgnore
    public /* final */ xw2<f83> b;

    @DexIgnore
    public g83() {
        this(ww2.b(new i83()));
    }

    @DexIgnore
    public g83(xw2<f83> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((f83) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ f83 zza() {
        return this.b.zza();
    }
}
