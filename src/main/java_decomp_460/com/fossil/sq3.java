package com.fossil;

import android.app.ActivityManager;
import android.os.Bundle;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sq3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ jq3 f3287a;

    @DexIgnore
    public sq3(jq3 jq3) {
        this.f3287a = jq3;
    }

    @DexIgnore
    public final void a() {
        this.f3287a.h();
        if (this.f3287a.l().v(this.f3287a.zzm().b())) {
            this.f3287a.l().r.a(true);
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
            if (runningAppProcessInfo.importance == 100) {
                this.f3287a.d().N().a("Detected application was in foreground");
                c(this.f3287a.zzm().b(), false);
            }
        }
    }

    @DexIgnore
    public final void b(long j, boolean z) {
        this.f3287a.h();
        this.f3287a.F();
        if (this.f3287a.l().v(j)) {
            this.f3287a.l().r.a(true);
        }
        this.f3287a.l().u.b(j);
        if (this.f3287a.l().r.b()) {
            c(j, z);
        }
    }

    @DexIgnore
    public final void c(long j, boolean z) {
        this.f3287a.h();
        if (this.f3287a.f1780a.o()) {
            this.f3287a.l().u.b(j);
            this.f3287a.d().N().b("Session started, time", Long.valueOf(this.f3287a.zzm().c()));
            Long valueOf = Long.valueOf(j / 1000);
            this.f3287a.p().S("auto", "_sid", valueOf, j);
            this.f3287a.l().r.a(false);
            Bundle bundle = new Bundle();
            bundle.putLong("_sid", valueOf.longValue());
            if (this.f3287a.m().s(xg3.q0) && z) {
                bundle.putLong("_aib", 1);
            }
            this.f3287a.p().N("auto", "_s", j, bundle);
            if (z53.a() && this.f3287a.m().s(xg3.v0)) {
                String a2 = this.f3287a.l().z.a();
                if (!TextUtils.isEmpty(a2)) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("_ffr", a2);
                    this.f3287a.p().N("auto", "_ssr", j, bundle2);
                }
            }
        }
    }
}
