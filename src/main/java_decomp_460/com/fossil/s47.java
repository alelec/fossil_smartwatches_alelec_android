package com.fossil;

import android.widget.RadioGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class s47 implements RadioGroup.OnCheckedChangeListener {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ t47 f3204a;
    @DexIgnore
    public /* final */ /* synthetic */ Integer b;

    @DexIgnore
    public /* synthetic */ s47(t47 t47, Integer num) {
        this.f3204a = t47;
        this.b = num;
    }

    @DexIgnore
    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        this.f3204a.v6(this.b, radioGroup, i);
    }
}
