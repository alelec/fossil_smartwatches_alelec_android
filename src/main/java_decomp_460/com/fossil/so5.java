package com.fossil;

import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.SecureApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class so5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ApiServiceV2 f3283a;
    @DexIgnore
    public /* final */ SecureApiService b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRemote$deletePresets$2", f = "DianaPresetRemote.kt", l = {24}, m = "invokeSuspend")
    public static final class a extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ so5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(so5 so5, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = so5;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new a(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((a) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3283a;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object deleteDianaPresets = apiServiceV2.deleteDianaPresets(gj4, this);
                return deleteDianaPresets == d ? d : deleteDianaPresets;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRemote$fetchDianaPreset$2", f = "DianaPresetRemote.kt", l = {16}, m = "invokeSuspend")
    public static final class b extends ko7 implements rp7<qn7<? super q88<ApiResponse<mo5>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ so5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(so5 so5, String str, qn7 qn7) {
            super(1, qn7);
            this.this$0 = so5;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new b(this.this$0, this.$serial, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<mo5>>> qn7) {
            return ((b) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3283a;
                String str = this.$serial;
                this.label = 1;
                Object fetchDianaPreset = apiServiceV2.fetchDianaPreset(str, 100, 0, this);
                return fetchDianaPreset == d ? d : fetchDianaPreset;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRemote$generateSharableLink$2", f = "DianaPresetRemote.kt", l = {28}, m = "invokeSuspend")
    public static final class c extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ so5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(so5 so5, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = so5;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new c(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((c) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3283a;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object generateSharableLink = apiServiceV2.generateSharableLink(gj4, this);
                return generateSharableLink == d ? d : generateSharableLink;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRemote$getDownloadSharingFaceUrl$2", f = "DianaPresetRemote.kt", l = {32}, m = "invokeSuspend")
    public static final class d extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $token;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ so5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(so5 so5, String str, String str2, qn7 qn7) {
            super(1, qn7);
            this.this$0 = so5;
            this.$id = str;
            this.$token = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new d(this.this$0, this.$id, this.$token, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((d) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3283a;
                String str = this.$id;
                String str2 = this.$token;
                this.label = 1;
                Object sharingFace = apiServiceV2.getSharingFace(str, str2, this);
                return sharingFace == d ? d : sharingFace;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRemote$upsertDianaPreset$2", f = "DianaPresetRemote.kt", l = {20}, m = "invokeSuspend")
    public static final class e extends ko7 implements rp7<qn7<? super q88<ApiResponse<mo5>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ so5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(so5 so5, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = so5;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new e(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<mo5>>> qn7) {
            return ((e) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                SecureApiService secureApiService = this.this$0.b;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object upsertPresets = secureApiService.upsertPresets(gj4, this);
                return upsertPresets == d ? d : upsertPresets;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public so5(ApiServiceV2 apiServiceV2, SecureApiService secureApiService) {
        pq7.c(apiServiceV2, "api");
        pq7.c(secureApiService, "secureApi");
        this.f3283a = apiServiceV2;
        this.b = secureApiService;
    }

    @DexIgnore
    public final Object c(gj4 gj4, qn7<? super iq5<gj4>> qn7) {
        return jq5.d(new a(this, gj4, null), qn7);
    }

    @DexIgnore
    public final Object d(String str, qn7<? super iq5<ApiResponse<mo5>>> qn7) {
        return jq5.d(new b(this, str, null), qn7);
    }

    @DexIgnore
    public final Object e(gj4 gj4, qn7<? super iq5<gj4>> qn7) {
        return jq5.d(new c(this, gj4, null), qn7);
    }

    @DexIgnore
    public final Object f(String str, String str2, qn7<? super iq5<gj4>> qn7) {
        return jq5.d(new d(this, str, str2, null), qn7);
    }

    @DexIgnore
    public final Object g(gj4 gj4, qn7<? super iq5<ApiResponse<mo5>>> qn7) {
        return jq5.d(new e(this, gj4, null), qn7);
    }
}
