package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ws extends rs {
    @DexIgnore
    public /* final */ xn1 A;

    @DexIgnore
    public ws(xn1 xn1, k5 k5Var) {
        super(hs.C, k5Var);
        this.A = xn1;
    }

    @DexIgnore
    @Override // com.fossil.ns
    public u5 D() {
        n6 n6Var = n6.ASYNC;
        xn1 xn1 = this.A;
        ByteBuffer order = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order, "ByteBuffer.allocate(4).o\u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(ot.c.b);
        order.put(lt.MUSIC_EVENT.b);
        order.put(xn1.getAction().a());
        order.put(xn1.getActionStatus().a());
        byte[] array = order.array();
        pq7.b(array, "musicEventData.array()");
        return new j6(n6Var, array, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(super.z(), jd0.u0, this.A.toJSONObject());
    }
}
