package com.fossil;

import com.fossil.iq4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vt5 extends iq4<b, d, c> {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public b d;
    @DexIgnore
    public /* final */ DeviceRepository e;
    @DexIgnore
    public /* final */ HybridPresetRepository f;
    @DexIgnore
    public /* final */ PortfolioApp g;
    @DexIgnore
    public /* final */ WatchFaceRepository h;
    @DexIgnore
    public /* final */ uo5 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return vt5.j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f3820a;

        @DexIgnore
        public b(String str) {
            pq7.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.f3820a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f3820a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f3821a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public c(String str, ArrayList<Integer> arrayList, String str2) {
            pq7.c(str, "lastErrorCode");
            this.f3821a = str;
            this.b = arrayList;
            this.c = str2;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.f3821a;
        }

        @DexIgnore
        public final ArrayList<Integer> c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$1", f = "UnlinkDeviceUseCase.kt", l = {60, 66, 68}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vt5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(vt5 vt5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = vt5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0068  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x007f  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0117  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 366
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.vt5.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = vt5.class.getSimpleName();
        pq7.b(simpleName, "UnlinkDeviceUseCase::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public vt5(DeviceRepository deviceRepository, HybridPresetRepository hybridPresetRepository, DianaPresetRepository dianaPresetRepository, PortfolioApp portfolioApp, WatchFaceRepository watchFaceRepository, uo5 uo5) {
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(hybridPresetRepository, "mHybridPresetRepository");
        pq7.c(dianaPresetRepository, "mDianaPresetRepository");
        pq7.c(portfolioApp, "mApp");
        pq7.c(watchFaceRepository, "watchFaceRepository");
        pq7.c(uo5, "dianaPresetRepository");
        this.e = deviceRepository;
        this.f = hybridPresetRepository;
        this.g = portfolioApp;
        this.h = watchFaceRepository;
        this.i = uo5;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return j;
    }

    @DexIgnore
    public final xw7 s() {
        return gu7.d(g(), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    /* renamed from: t */
    public Object k(b bVar, qn7<Object> qn7) {
        FLogger.INSTANCE.getLocal().d(j, "running UseCase");
        if (bVar == null) {
            return new c("", null, "");
        }
        this.d = bVar;
        String a2 = bVar.a();
        String J = this.g.J();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "remove device " + a2 + " currentActive " + J);
        s();
        return new Object();
    }
}
