package com.fossil;

import java.io.IOException;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w28 implements Interceptor.Chain {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<Interceptor> f3873a;
    @DexIgnore
    public /* final */ p28 b;
    @DexIgnore
    public /* final */ s28 c;
    @DexIgnore
    public /* final */ l28 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ v18 f;
    @DexIgnore
    public /* final */ a18 g;
    @DexIgnore
    public /* final */ m18 h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public int l;

    @DexIgnore
    public w28(List<Interceptor> list, p28 p28, s28 s28, l28 l28, int i2, v18 v18, a18 a18, m18 m18, int i3, int i4, int i5) {
        this.f3873a = list;
        this.d = l28;
        this.b = p28;
        this.c = s28;
        this.e = i2;
        this.f = v18;
        this.g = a18;
        this.h = m18;
        this.i = i3;
        this.j = i4;
        this.k = i5;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public int a() {
        return this.j;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public int b() {
        return this.k;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public v18 c() {
        return this.f;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public Response d(v18 v18) throws IOException {
        return j(v18, this.b, this.c, this.d);
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public e18 e() {
        return this.d;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public int f() {
        return this.i;
    }

    @DexIgnore
    public a18 g() {
        return this.g;
    }

    @DexIgnore
    public m18 h() {
        return this.h;
    }

    @DexIgnore
    public s28 i() {
        return this.c;
    }

    @DexIgnore
    public Response j(v18 v18, p28 p28, s28 s28, l28 l28) throws IOException {
        if (this.e < this.f3873a.size()) {
            this.l++;
            if (this.c != null && !this.d.t(v18.j())) {
                throw new IllegalStateException("network interceptor " + this.f3873a.get(this.e - 1) + " must retain the same host and port");
            } else if (this.c == null || this.l <= 1) {
                w28 w28 = new w28(this.f3873a, p28, s28, l28, this.e + 1, v18, this.g, this.h, this.i, this.j, this.k);
                Interceptor interceptor = this.f3873a.get(this.e);
                Response intercept = interceptor.intercept(w28);
                if (s28 != null && this.e + 1 < this.f3873a.size() && w28.l != 1) {
                    throw new IllegalStateException("network interceptor " + interceptor + " must call proceed() exactly once");
                } else if (intercept == null) {
                    throw new NullPointerException("interceptor " + interceptor + " returned null");
                } else if (intercept.a() != null) {
                    return intercept;
                } else {
                    throw new IllegalStateException("interceptor " + interceptor + " returned a response with no body");
                }
            } else {
                throw new IllegalStateException("network interceptor " + this.f3873a.get(this.e - 1) + " must call proceed() exactly once");
            }
        } else {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public p28 k() {
        return this.b;
    }
}
