package com.fossil;

import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b24 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ d14 f385a; // = d14.i(", ").k("null");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements b14<Object, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Collection b;

        @DexIgnore
        public a(Collection collection) {
            this.b = collection;
        }

        @DexIgnore
        @Override // com.fossil.b14
        public Object apply(Object obj) {
            return obj == this.b ? "(this Collection)" : obj;
        }
    }

    @DexIgnore
    public static <T> Collection<T> a(Iterable<T> iterable) {
        return (Collection) iterable;
    }

    @DexIgnore
    public static boolean b(Collection<?> collection, Collection<?> collection2) {
        return o34.b(collection2, k14.b(collection));
    }

    @DexIgnore
    public static StringBuilder c(int i) {
        a24.b(i, "size");
        return new StringBuilder((int) Math.min(((long) i) * 8, 1073741824L));
    }

    @DexIgnore
    public static boolean d(Collection<?> collection, Object obj) {
        i14.l(collection);
        try {
            return collection.contains(obj);
        } catch (ClassCastException | NullPointerException e) {
            return false;
        }
    }

    @DexIgnore
    public static String e(Collection<?> collection) {
        StringBuilder c = c(collection.size());
        c.append('[');
        f385a.c(c, o34.k(collection, new a(collection)));
        c.append(']');
        return c.toString();
    }
}
