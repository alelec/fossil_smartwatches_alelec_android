package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ou extends mu {
    @DexIgnore
    public static /* final */ nu N; // = new nu(null);
    @DexIgnore
    public /* final */ xl1 L;
    @DexIgnore
    public /* final */ ul1[] M;

    @DexIgnore
    public ou(k5 k5Var, xl1 xl1, ul1[] ul1Arr) {
        super(fu.n, hs.A, k5Var, 0, 8);
        this.L = xl1;
        this.M = ul1Arr;
        this.E = true;
    }

    @DexIgnore
    @Override // com.fossil.mu, com.fossil.ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate((this.M.length * 5) + 2).order(ByteOrder.LITTLE_ENDIAN).put(this.L.a()).put((byte) this.M.length).put(N.a(this.M)).array();
        pq7.b(array, "ByteBuffer.allocate(HEAD\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(g80.k(super.z(), jd0.W0, ey1.a(this.L)), jd0.b1, px1.a(this.M));
    }
}
