package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bu4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ft4 f510a;

    @DexIgnore
    public bu4(ft4 ft4) {
        pq7.c(ft4, "dao");
        this.f510a = ft4;
    }

    @DexIgnore
    public final void a() {
        this.f510a.a();
    }

    @DexIgnore
    public final Long[] b(List<dt4> list) {
        pq7.c(list, "notifications");
        return this.f510a.insert(list);
    }

    @DexIgnore
    public final LiveData<List<dt4>> c() {
        return this.f510a.b();
    }
}
