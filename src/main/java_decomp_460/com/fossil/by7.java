package com.fossil;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class by7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "kotlinx.coroutines.TimeoutKt", f = "Timeout.kt", l = {54}, m = "withTimeoutOrNull")
    public static final class a extends co7 {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;

        @DexIgnore
        public a(qn7 qn7) {
            super(qn7);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return by7.c(0, null, this);
        }
    }

    @DexIgnore
    public static final zx7 a(long j, xw7 xw7) {
        return new zx7("Timed out waiting for " + j + " ms", xw7);
    }

    @DexIgnore
    public static final <U, T extends U> Object b(ay7<U, ? super T> ay7, vp7<? super iv7, ? super qn7<? super T>, ? extends Object> vp7) {
        bx7.f(ay7, uv7.b(ay7.e.getContext()).G(ay7.f, ay7));
        return e08.d(ay7, ay7, vp7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T> java.lang.Object c(long r8, com.fossil.vp7<? super com.fossil.iv7, ? super com.fossil.qn7<? super T>, ? extends java.lang.Object> r10, com.fossil.qn7<? super T> r11) {
        /*
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r11 instanceof com.fossil.by7.a
            if (r0 == 0) goto L_0x0030
            r0 = r11
            com.fossil.by7$a r0 = (com.fossil.by7.a) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0030
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0015:
            java.lang.Object r3 = r2.result
            java.lang.Object r5 = com.fossil.yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x004b
            if (r0 != r6) goto L_0x0043
            java.lang.Object r0 = r2.L$1
            com.fossil.dr7 r0 = (com.fossil.dr7) r0
            java.lang.Object r1 = r2.L$0
            com.fossil.vp7 r1 = (com.fossil.vp7) r1
            long r6 = r2.J$0
            com.fossil.el7.b(r3)     // Catch:{ zx7 -> 0x0037 }
            r0 = r3
        L_0x002f:
            return r0
        L_0x0030:
            com.fossil.by7$a r0 = new com.fossil.by7$a
            r0.<init>(r11)
            r2 = r0
            goto L_0x0015
        L_0x0037:
            r1 = move-exception
            r2 = r1
        L_0x0039:
            com.fossil.xw7 r1 = r2.coroutine
            T r0 = r0.element
            com.fossil.ay7 r0 = (com.fossil.ay7) r0
            if (r1 != r0) goto L_0x0081
            r0 = r4
            goto L_0x002f
        L_0x0043:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004b:
            com.fossil.el7.b(r3)
            r0 = 0
            int r0 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
            if (r0 > 0) goto L_0x0056
            r0 = r4
            goto L_0x002f
        L_0x0056:
            com.fossil.dr7 r1 = new com.fossil.dr7
            r1.<init>()
            r1.element = r4
            r2.J$0 = r8     // Catch:{ zx7 -> 0x007e }
            r2.L$0 = r10     // Catch:{ zx7 -> 0x007e }
            r2.L$1 = r1     // Catch:{ zx7 -> 0x007e }
            r0 = 1
            r2.label = r0     // Catch:{ zx7 -> 0x007e }
            com.fossil.ay7 r0 = new com.fossil.ay7     // Catch:{ zx7 -> 0x007e }
            r0.<init>(r8, r2)     // Catch:{ zx7 -> 0x007e }
            r1.element = r0     // Catch:{ zx7 -> 0x007e }
            java.lang.Object r0 = b(r0, r10)     // Catch:{ zx7 -> 0x007e }
            java.lang.Object r3 = com.fossil.yn7.d()     // Catch:{ zx7 -> 0x007e }
            if (r0 != r3) goto L_0x007a
            com.fossil.go7.c(r2)     // Catch:{ zx7 -> 0x007e }
        L_0x007a:
            if (r0 != r5) goto L_0x002f
            r0 = r5
            goto L_0x002f
        L_0x007e:
            r2 = move-exception
            r0 = r1
            goto L_0x0039
        L_0x0081:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.by7.c(long, com.fossil.vp7, com.fossil.qn7):java.lang.Object");
    }
}
