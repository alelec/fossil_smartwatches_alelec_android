package com.fossil;

import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import com.fossil.jn5;
import com.fossil.t47;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qv5 extends pv5 implements t47.g {
    @DexIgnore
    public HashMap g;

    /*
    static {
        pq7.b(qv5.class.getSimpleName(), "BasePermissionFragment::class.java.simpleName");
    }
    */

    @DexIgnore
    public final void M(uh5... uh5Arr) {
        pq7.c(uh5Arr, "permissionCodes");
        ArrayList<uh5> arrayList = new ArrayList<>();
        for (uh5 uh5 : uh5Arr) {
            arrayList.add(uh5);
        }
        jn5.b.a(getContext(), arrayList, hm7.i(jn5.a.PAIR_DEVICE));
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i, Intent intent) {
        pq7.c(str, "tag");
        if (getActivity() instanceof ls5) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((ls5) activity).R5(str, i, intent);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
