package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class sb extends Enum<sb> {
    @DexIgnore
    public static /* final */ sb c;
    @DexIgnore
    public static /* final */ /* synthetic */ sb[] d;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        sb sbVar = new sb("FOSSIL", 0, (byte) 1);
        c = sbVar;
        d = new sb[]{sbVar, new sb("GOOGLE", 1, (byte) 2)};
    }
    */

    @DexIgnore
    public sb(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static sb valueOf(String str) {
        return (sb) Enum.valueOf(sb.class, str);
    }

    @DexIgnore
    public static sb[] values() {
        return (sb[]) d.clone();
    }
}
