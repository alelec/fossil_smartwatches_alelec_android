package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ja8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ HashMap<String, ka8> f1735a; // = new HashMap<>();

    @DexIgnore
    public final void a() {
        this.f1735a.clear();
    }

    @DexIgnore
    public final ka8 b(String str) {
        pq7.c(str, "id");
        return this.f1735a.get(str);
    }

    @DexIgnore
    public final void c(ka8 ka8) {
        pq7.c(ka8, "assetEntity");
        this.f1735a.put(ka8.e(), ka8);
    }
}
