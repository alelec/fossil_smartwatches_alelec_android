package com.fossil;

import android.os.Bundle;
import com.fossil.qg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zg2 implements qg2.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ Bundle f4466a;
    @DexIgnore
    public /* final */ /* synthetic */ qg2 b;

    @DexIgnore
    public zg2(qg2 qg2, Bundle bundle) {
        this.b = qg2;
        this.f4466a = bundle;
    }

    @DexIgnore
    @Override // com.fossil.qg2.a
    public final void a(sg2 sg2) {
        this.b.f2976a.onCreate(this.f4466a);
    }

    @DexIgnore
    @Override // com.fossil.qg2.a
    public final int getState() {
        return 1;
    }
}
