package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class su extends mu {
    @DexIgnore
    public se L; // = new se(0, 0, 0);

    @DexIgnore
    public su(k5 k5Var) {
        super(fu.h, hs.s, k5Var, 0, 8);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return gy1.c(super.A(), this.L.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        se seVar = new se(hy1.n(order.getShort(0)), hy1.n(order.getShort(2)), hy1.n(order.getShort(4)));
        this.L = seVar;
        return gy1.c(jSONObject, seVar.toJSONObject());
    }
}
