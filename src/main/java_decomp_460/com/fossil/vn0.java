package com.fossil;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vn0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ a f3789a;

    @DexIgnore
    public interface a {
        @DexIgnore
        boolean a(MotionEvent motionEvent);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements a {
        @DexIgnore
        public static /* final */ int v; // = ViewConfiguration.getLongPressTimeout();
        @DexIgnore
        public static /* final */ int w; // = ViewConfiguration.getTapTimeout();
        @DexIgnore
        public static /* final */ int x; // = ViewConfiguration.getDoubleTapTimeout();

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f3790a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public /* final */ Handler e;
        @DexIgnore
        public /* final */ GestureDetector.OnGestureListener f;
        @DexIgnore
        public GestureDetector.OnDoubleTapListener g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public boolean i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public boolean k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public MotionEvent m;
        @DexIgnore
        public MotionEvent n;
        @DexIgnore
        public boolean o;
        @DexIgnore
        public float p;
        @DexIgnore
        public float q;
        @DexIgnore
        public float r;
        @DexIgnore
        public float s;
        @DexIgnore
        public boolean t;
        @DexIgnore
        public VelocityTracker u;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends Handler {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public a(Handler handler) {
                super(handler.getLooper());
            }

            @DexIgnore
            public void handleMessage(Message message) {
                int i = message.what;
                if (i == 1) {
                    b bVar = b.this;
                    bVar.f.onShowPress(bVar.m);
                } else if (i == 2) {
                    b.this.d();
                } else if (i == 3) {
                    b bVar2 = b.this;
                    GestureDetector.OnDoubleTapListener onDoubleTapListener = bVar2.g;
                    if (onDoubleTapListener == null) {
                        return;
                    }
                    if (!bVar2.h) {
                        onDoubleTapListener.onSingleTapConfirmed(bVar2.m);
                    } else {
                        bVar2.i = true;
                    }
                } else {
                    throw new RuntimeException("Unknown message " + message);
                }
            }
        }

        @DexIgnore
        public b(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
            if (handler != null) {
                this.e = new a(handler);
            } else {
                this.e = new a();
            }
            this.f = onGestureListener;
            if (onGestureListener instanceof GestureDetector.OnDoubleTapListener) {
                g((GestureDetector.OnDoubleTapListener) onGestureListener);
            }
            e(context);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:100:0x020d  */
        /* JADX WARNING: Removed duplicated region for block: B:97:0x01f6  */
        @Override // com.fossil.vn0.a
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(android.view.MotionEvent r14) {
            /*
            // Method dump skipped, instructions count: 584
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.vn0.b.a(android.view.MotionEvent):boolean");
        }

        @DexIgnore
        public final void b() {
            this.e.removeMessages(1);
            this.e.removeMessages(2);
            this.e.removeMessages(3);
            this.u.recycle();
            this.u = null;
            this.o = false;
            this.h = false;
            this.k = false;
            this.l = false;
            this.i = false;
            if (this.j) {
                this.j = false;
            }
        }

        @DexIgnore
        public final void c() {
            this.e.removeMessages(1);
            this.e.removeMessages(2);
            this.e.removeMessages(3);
            this.o = false;
            this.k = false;
            this.l = false;
            this.i = false;
            if (this.j) {
                this.j = false;
            }
        }

        @DexIgnore
        public void d() {
            this.e.removeMessages(3);
            this.i = false;
            this.j = true;
            this.f.onLongPress(this.m);
        }

        @DexIgnore
        public final void e(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null");
            } else if (this.f != null) {
                this.t = true;
                ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
                int scaledTouchSlop = viewConfiguration.getScaledTouchSlop();
                int scaledDoubleTapSlop = viewConfiguration.getScaledDoubleTapSlop();
                this.c = viewConfiguration.getScaledMinimumFlingVelocity();
                this.d = viewConfiguration.getScaledMaximumFlingVelocity();
                this.f3790a = scaledTouchSlop * scaledTouchSlop;
                this.b = scaledDoubleTapSlop * scaledDoubleTapSlop;
            } else {
                throw new IllegalArgumentException("OnGestureListener must not be null");
            }
        }

        @DexIgnore
        public final boolean f(MotionEvent motionEvent, MotionEvent motionEvent2, MotionEvent motionEvent3) {
            if (!this.l || motionEvent3.getEventTime() - motionEvent2.getEventTime() > ((long) x)) {
                return false;
            }
            int x2 = ((int) motionEvent.getX()) - ((int) motionEvent3.getX());
            int y = ((int) motionEvent.getY()) - ((int) motionEvent3.getY());
            return (x2 * x2) + (y * y) < this.b;
        }

        @DexIgnore
        public void g(GestureDetector.OnDoubleTapListener onDoubleTapListener) {
            this.g = onDoubleTapListener;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ GestureDetector f3792a;

        @DexIgnore
        public c(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
            this.f3792a = new GestureDetector(context, onGestureListener, handler);
        }

        @DexIgnore
        @Override // com.fossil.vn0.a
        public boolean a(MotionEvent motionEvent) {
            return this.f3792a.onTouchEvent(motionEvent);
        }
    }

    @DexIgnore
    public vn0(Context context, GestureDetector.OnGestureListener onGestureListener) {
        this(context, onGestureListener, null);
    }

    @DexIgnore
    public vn0(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
        if (Build.VERSION.SDK_INT > 17) {
            this.f3789a = new c(context, onGestureListener, handler);
        } else {
            this.f3789a = new b(context, onGestureListener, handler);
        }
    }

    @DexIgnore
    public boolean a(MotionEvent motionEvent) {
        return this.f3789a.a(motionEvent);
    }
}
