package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.fossil.xc1;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hj1<R> implements bj1, pj1, gj1 {
    @DexIgnore
    public static /* final */ boolean D; // = Log.isLoggable("Request", 2);
    @DexIgnore
    public int A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public RuntimeException C;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1486a;
    @DexIgnore
    public /* final */ mk1 b;
    @DexIgnore
    public /* final */ Object c;
    @DexIgnore
    public /* final */ ej1<R> d;
    @DexIgnore
    public /* final */ cj1 e;
    @DexIgnore
    public /* final */ Context f;
    @DexIgnore
    public /* final */ qa1 g;
    @DexIgnore
    public /* final */ Object h;
    @DexIgnore
    public /* final */ Class<R> i;
    @DexIgnore
    public /* final */ yi1<?> j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ sa1 m;
    @DexIgnore
    public /* final */ qj1<R> n;
    @DexIgnore
    public /* final */ List<ej1<R>> o;
    @DexIgnore
    public /* final */ uj1<? super R> p;
    @DexIgnore
    public /* final */ Executor q;
    @DexIgnore
    public id1<R> r;
    @DexIgnore
    public xc1.d s;
    @DexIgnore
    public long t;
    @DexIgnore
    public volatile xc1 u;
    @DexIgnore
    public a v;
    @DexIgnore
    public Drawable w;
    @DexIgnore
    public Drawable x;
    @DexIgnore
    public Drawable y;
    @DexIgnore
    public int z;

    @DexIgnore
    public enum a {
        PENDING,
        RUNNING,
        WAITING_FOR_SIZE,
        COMPLETE,
        FAILED,
        CLEARED
    }

    @DexIgnore
    public hj1(Context context, qa1 qa1, Object obj, Object obj2, Class<R> cls, yi1<?> yi1, int i2, int i3, sa1 sa1, qj1<R> qj1, ej1<R> ej1, List<ej1<R>> list, cj1 cj1, xc1 xc1, uj1<? super R> uj1, Executor executor) {
        this.f1486a = D ? String.valueOf(super.hashCode()) : null;
        this.b = mk1.a();
        this.c = obj;
        this.f = context;
        this.g = qa1;
        this.h = obj2;
        this.i = cls;
        this.j = yi1;
        this.k = i2;
        this.l = i3;
        this.m = sa1;
        this.n = qj1;
        this.d = ej1;
        this.o = list;
        this.e = cj1;
        this.u = xc1;
        this.p = uj1;
        this.q = executor;
        this.v = a.PENDING;
        if (this.C == null && qa1.i()) {
            this.C = new RuntimeException("Glide request origin trace");
        }
    }

    @DexIgnore
    public static int u(int i2, float f2) {
        return i2 == Integer.MIN_VALUE ? i2 : Math.round(((float) i2) * f2);
    }

    @DexIgnore
    public static <R> hj1<R> x(Context context, qa1 qa1, Object obj, Object obj2, Class<R> cls, yi1<?> yi1, int i2, int i3, sa1 sa1, qj1<R> qj1, ej1<R> ej1, List<ej1<R>> list, cj1 cj1, xc1 xc1, uj1<? super R> uj1, Executor executor) {
        return new hj1<>(context, qa1, obj, obj2, cls, yi1, i2, i3, sa1, qj1, ej1, list, cj1, xc1, uj1, executor);
    }

    @DexIgnore
    public final void A() {
        if (l()) {
            Drawable drawable = null;
            if (this.h == null) {
                drawable = p();
            }
            if (drawable == null) {
                drawable = o();
            }
            if (drawable == null) {
                drawable = q();
            }
            this.n.f(drawable);
        }
    }

    @DexIgnore
    @Override // com.fossil.gj1
    public void a(dd1 dd1) {
        y(dd1, 5);
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public boolean b() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == a.COMPLETE;
        }
        return z2;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.hj1<R> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x008b, code lost:
        if (r6 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x008d, code lost:
        r5.u.k(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a1, code lost:
        if (r6 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a3, code lost:
        r5.u.k(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00b2, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b3, code lost:
        r1 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        return;
     */
    @DexIgnore
    @Override // com.fossil.gj1
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(com.fossil.id1<?> r6, com.fossil.gb1 r7) {
        /*
            r5 = this;
            r1 = 0
            com.fossil.mk1 r0 = r5.b
            r0.c()
            java.lang.Object r2 = r5.c     // Catch:{ all -> 0x00c2 }
            monitor-enter(r2)     // Catch:{ all -> 0x00c2 }
            r0 = 0
            r5.s = r0     // Catch:{ all -> 0x00ae }
            if (r6 != 0) goto L_0x0030
            com.fossil.dd1 r0 = new com.fossil.dd1     // Catch:{ all -> 0x00ae }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ae }
            r3.<init>()     // Catch:{ all -> 0x00ae }
            java.lang.String r4 = "Expected to receive a Resource<R> with an object of "
            r3.append(r4)     // Catch:{ all -> 0x00ae }
            java.lang.Class<R> r4 = r5.i     // Catch:{ all -> 0x00ae }
            r3.append(r4)     // Catch:{ all -> 0x00ae }
            java.lang.String r4 = " inside, but instead got null."
            r3.append(r4)     // Catch:{ all -> 0x00ae }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00ae }
            r0.<init>(r3)     // Catch:{ all -> 0x00ae }
            r5.a(r0)     // Catch:{ all -> 0x00ae }
            monitor-exit(r2)     // Catch:{ all -> 0x00ae }
        L_0x002f:
            return
        L_0x0030:
            java.lang.Object r3 = r6.get()     // Catch:{ all -> 0x00ae }
            if (r3 == 0) goto L_0x0042
            java.lang.Class<R> r0 = r5.i     // Catch:{ all -> 0x00ae }
            java.lang.Class r4 = r3.getClass()     // Catch:{ all -> 0x00ae }
            boolean r0 = r0.isAssignableFrom(r4)     // Catch:{ all -> 0x00ae }
            if (r0 != 0) goto L_0x0093
        L_0x0042:
            r0 = 0
            r5.r = r0     // Catch:{ all -> 0x00c6 }
            com.fossil.dd1 r1 = new com.fossil.dd1     // Catch:{ all -> 0x00c6 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c6 }
            r4.<init>()     // Catch:{ all -> 0x00c6 }
            java.lang.String r0 = "Expected to receive an object of "
            r4.append(r0)     // Catch:{ all -> 0x00c6 }
            java.lang.Class<R> r0 = r5.i     // Catch:{ all -> 0x00c6 }
            r4.append(r0)     // Catch:{ all -> 0x00c6 }
            java.lang.String r0 = " but instead got "
            r4.append(r0)     // Catch:{ all -> 0x00c6 }
            if (r3 == 0) goto L_0x00bc
            java.lang.Class r0 = r3.getClass()     // Catch:{ all -> 0x00c6 }
        L_0x0061:
            r4.append(r0)     // Catch:{ all -> 0x00c6 }
            java.lang.String r0 = "{"
            r4.append(r0)     // Catch:{ all -> 0x00c6 }
            r4.append(r3)     // Catch:{ all -> 0x00c6 }
            java.lang.String r0 = "} inside Resource{"
            r4.append(r0)     // Catch:{ all -> 0x00c6 }
            r4.append(r6)     // Catch:{ all -> 0x00c6 }
            java.lang.String r0 = "}."
            r4.append(r0)     // Catch:{ all -> 0x00c6 }
            if (r3 == 0) goto L_0x00bf
            java.lang.String r0 = ""
        L_0x007d:
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r1.<init>(r0)
            r5.a(r1)
            monitor-exit(r2)
            if (r6 == 0) goto L_0x002f
            com.fossil.xc1 r0 = r5.u
            r0.k(r6)
            goto L_0x002f
        L_0x0093:
            boolean r0 = r5.m()
            if (r0 != 0) goto L_0x00a9
            r0 = 0
            r5.r = r0
            com.fossil.hj1$a r0 = com.fossil.hj1.a.COMPLETE
            r5.v = r0
            monitor-exit(r2)
            if (r6 == 0) goto L_0x002f
            com.fossil.xc1 r0 = r5.u
            r0.k(r6)
            goto L_0x002f
        L_0x00a9:
            r5.z(r6, r3, r7)
            monitor-exit(r2)
            goto L_0x002f
        L_0x00ae:
            r0 = move-exception
            r6 = r1
        L_0x00b0:
            monitor-exit(r2)     // Catch:{ all -> 0x00c4 }
            throw r0     // Catch:{ all -> 0x00b2 }
        L_0x00b2:
            r0 = move-exception
            r1 = r6
        L_0x00b4:
            if (r1 == 0) goto L_0x00bb
            com.fossil.xc1 r2 = r5.u
            r2.k(r1)
        L_0x00bb:
            throw r0
        L_0x00bc:
            java.lang.String r0 = ""
            goto L_0x0061
        L_0x00bf:
            java.lang.String r0 = " To indicate failure return a null Resource object, rather than a Resource object containing null data."
            goto L_0x007d
        L_0x00c2:
            r0 = move-exception
            goto L_0x00b4
        L_0x00c4:
            r0 = move-exception
            goto L_0x00b0
        L_0x00c6:
            r0 = move-exception
            goto L_0x00b0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.hj1.c(com.fossil.id1, com.fossil.gb1):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0034, code lost:
        if (r0 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0036, code lost:
        r4.u.k(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    @DexIgnore
    @Override // com.fossil.bj1
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void clear() {
        /*
            r4 = this;
            r0 = 0
            java.lang.Object r1 = r4.c
            monitor-enter(r1)
            r4.i()     // Catch:{ all -> 0x003c }
            com.fossil.mk1 r2 = r4.b     // Catch:{ all -> 0x003c }
            r2.c()     // Catch:{ all -> 0x003c }
            com.fossil.hj1$a r2 = r4.v     // Catch:{ all -> 0x003c }
            com.fossil.hj1$a r3 = com.fossil.hj1.a.CLEARED     // Catch:{ all -> 0x003c }
            if (r2 != r3) goto L_0x0014
            monitor-exit(r1)     // Catch:{ all -> 0x003c }
        L_0x0013:
            return
        L_0x0014:
            r4.n()     // Catch:{ all -> 0x003c }
            com.fossil.id1<R> r2 = r4.r     // Catch:{ all -> 0x003c }
            if (r2 == 0) goto L_0x0020
            com.fossil.id1<R> r0 = r4.r     // Catch:{ all -> 0x003c }
            r2 = 0
            r4.r = r2     // Catch:{ all -> 0x003c }
        L_0x0020:
            boolean r2 = r4.k()     // Catch:{ all -> 0x003c }
            if (r2 == 0) goto L_0x002f
            com.fossil.qj1<R> r2 = r4.n     // Catch:{ all -> 0x003c }
            android.graphics.drawable.Drawable r3 = r4.q()     // Catch:{ all -> 0x003c }
            r2.j(r3)     // Catch:{ all -> 0x003c }
        L_0x002f:
            com.fossil.hj1$a r2 = com.fossil.hj1.a.CLEARED     // Catch:{ all -> 0x003c }
            r4.v = r2     // Catch:{ all -> 0x003c }
            monitor-exit(r1)     // Catch:{ all -> 0x003c }
            if (r0 == 0) goto L_0x0013
            com.fossil.xc1 r1 = r4.u
            r1.k(r0)
            goto L_0x0013
        L_0x003c:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.hj1.clear():void");
    }

    @DexIgnore
    @Override // com.fossil.pj1
    public void d(int i2, int i3) {
        Object obj;
        Throwable th;
        this.b.c();
        Object obj2 = this.c;
        synchronized (obj2) {
            try {
                if (D) {
                    t("Got onSizeReady in " + ek1.a(this.t));
                }
                if (this.v == a.WAITING_FOR_SIZE) {
                    this.v = a.RUNNING;
                    float G = this.j.G();
                    this.z = u(i2, G);
                    this.A = u(i3, G);
                    if (D) {
                        t("finished setup for calling load in " + ek1.a(this.t));
                    }
                    try {
                    } catch (Throwable th2) {
                        th = th2;
                        obj = obj2;
                        while (true) {
                            try {
                                break;
                            } catch (Throwable th3) {
                                th = th3;
                            }
                        }
                        throw th;
                    }
                    try {
                        this.s = this.u.f(this.g, this.h, this.j.E(), this.z, this.A, this.j.C(), this.i, this.m, this.j.p(), this.j.I(), this.j.S(), this.j.O(), this.j.v(), this.j.M(), this.j.K(), this.j.J(), this.j.u(), this, this.q);
                        if (this.v != a.RUNNING) {
                            this.s = null;
                        }
                        if (D) {
                            t("finished onSizeReady in " + ek1.a(this.t));
                        }
                    } catch (Throwable th4) {
                        th = th4;
                        obj = obj2;
                        while (true) {
                            break;
                        }
                        throw th;
                    }
                }
            } catch (Throwable th5) {
                th = th5;
                obj = obj2;
                while (true) {
                    break;
                }
                throw th;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.gj1
    public Object e() {
        this.b.c();
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public void f() {
        synchronized (this.c) {
            i();
            this.b.c();
            this.t = ek1.b();
            if (this.h == null) {
                if (jk1.s(this.k, this.l)) {
                    this.z = this.k;
                    this.A = this.l;
                }
                y(new dd1("Received null model"), p() == null ? 5 : 3);
            } else if (this.v == a.RUNNING) {
                throw new IllegalArgumentException("Cannot restart a running request");
            } else if (this.v == a.COMPLETE) {
                c(this.r, gb1.MEMORY_CACHE);
            } else {
                this.v = a.WAITING_FOR_SIZE;
                if (jk1.s(this.k, this.l)) {
                    d(this.k, this.l);
                } else {
                    this.n.k(this);
                }
                if ((this.v == a.RUNNING || this.v == a.WAITING_FOR_SIZE) && l()) {
                    this.n.h(q());
                }
                if (D) {
                    t("finished run method in " + ek1.a(this.t));
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public boolean g(bj1 bj1) {
        int i2;
        int i3;
        Object obj;
        Class<R> cls;
        yi1<?> yi1;
        sa1 sa1;
        int size;
        int i4;
        int i5;
        Object obj2;
        Class<R> cls2;
        yi1<?> yi12;
        sa1 sa12;
        int size2;
        if (!(bj1 instanceof hj1)) {
            return false;
        }
        synchronized (this.c) {
            i2 = this.k;
            i3 = this.l;
            obj = this.h;
            cls = this.i;
            yi1 = this.j;
            sa1 = this.m;
            size = this.o != null ? this.o.size() : 0;
        }
        hj1 hj1 = (hj1) bj1;
        synchronized (hj1.c) {
            i4 = hj1.k;
            i5 = hj1.l;
            obj2 = hj1.h;
            cls2 = hj1.i;
            yi12 = hj1.j;
            sa12 = hj1.m;
            size2 = hj1.o != null ? hj1.o.size() : 0;
        }
        return i2 == i4 && i3 == i5 && jk1.c(obj, obj2) && cls.equals(cls2) && yi1.equals(yi12) && sa1 == sa12 && size == size2;
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public boolean h() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == a.CLEARED;
        }
        return z2;
    }

    @DexIgnore
    public final void i() {
        if (this.B) {
            throw new IllegalStateException("You can't start or clear loads in RequestListener or Target callbacks. If you're trying to start a fallback request when a load fails, use RequestBuilder#error(RequestBuilder). Otherwise consider posting your into() or clear() calls to the main thread using a Handler instead.");
        }
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public boolean isRunning() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == a.RUNNING || this.v == a.WAITING_FOR_SIZE;
        }
        return z2;
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public boolean j() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == a.COMPLETE;
        }
        return z2;
    }

    @DexIgnore
    public final boolean k() {
        cj1 cj1 = this.e;
        return cj1 == null || cj1.k(this);
    }

    @DexIgnore
    public final boolean l() {
        cj1 cj1 = this.e;
        return cj1 == null || cj1.d(this);
    }

    @DexIgnore
    public final boolean m() {
        cj1 cj1 = this.e;
        return cj1 == null || cj1.e(this);
    }

    @DexIgnore
    public final void n() {
        i();
        this.b.c();
        this.n.a(this);
        xc1.d dVar = this.s;
        if (dVar != null) {
            dVar.a();
            this.s = null;
        }
    }

    @DexIgnore
    public final Drawable o() {
        if (this.w == null) {
            Drawable r2 = this.j.r();
            this.w = r2;
            if (r2 == null && this.j.q() > 0) {
                this.w = s(this.j.q());
            }
        }
        return this.w;
    }

    @DexIgnore
    public final Drawable p() {
        if (this.y == null) {
            Drawable s2 = this.j.s();
            this.y = s2;
            if (s2 == null && this.j.t() > 0) {
                this.y = s(this.j.t());
            }
        }
        return this.y;
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public void pause() {
        synchronized (this.c) {
            if (isRunning()) {
                clear();
            }
        }
    }

    @DexIgnore
    public final Drawable q() {
        if (this.x == null) {
            Drawable y2 = this.j.y();
            this.x = y2;
            if (y2 == null && this.j.z() > 0) {
                this.x = s(this.j.z());
            }
        }
        return this.x;
    }

    @DexIgnore
    public final boolean r() {
        cj1 cj1 = this.e;
        return cj1 == null || !cj1.c().b();
    }

    @DexIgnore
    public final Drawable s(int i2) {
        return yg1.a(this.g, i2, this.j.H() != null ? this.j.H() : this.f.getTheme());
    }

    @DexIgnore
    public final void t(String str) {
        Log.v("Request", str + " this: " + this.f1486a);
    }

    @DexIgnore
    public final void v() {
        cj1 cj1 = this.e;
        if (cj1 != null) {
            cj1.a(this);
        }
    }

    @DexIgnore
    public final void w() {
        cj1 cj1 = this.e;
        if (cj1 != null) {
            cj1.i(this);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void y(dd1 dd1, int i2) {
        boolean z2;
        boolean z3 = false;
        this.b.c();
        synchronized (this.c) {
            dd1.setOrigin(this.C);
            int g2 = this.g.g();
            if (g2 <= i2) {
                Log.w("Glide", "Load failed for " + this.h + " with size [" + this.z + "x" + this.A + "]", dd1);
                if (g2 <= 4) {
                    dd1.logRootCauses("Glide");
                }
            }
            this.s = null;
            this.v = a.FAILED;
            this.B = true;
            try {
                if (this.o != null) {
                    z2 = false;
                    for (ej1<R> ej1 : this.o) {
                        z2 = ej1.e(dd1, this.h, this.n, r()) | z2;
                    }
                } else {
                    z2 = false;
                }
                if (this.d != null && this.d.e(dd1, this.h, this.n, r())) {
                    z3 = true;
                }
                if (!z2 && !z3) {
                    A();
                }
                this.B = false;
                v();
            } catch (Throwable th) {
                this.B = false;
                throw th;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void z(id1<R> id1, R r2, gb1 gb1) {
        boolean z2;
        boolean r3 = r();
        this.v = a.COMPLETE;
        this.r = id1;
        if (this.g.g() <= 3) {
            Log.d("Glide", "Finished loading " + r2.getClass().getSimpleName() + " from " + gb1 + " for " + this.h + " with size [" + this.z + "x" + this.A + "] in " + ek1.a(this.t) + " ms");
        }
        this.B = true;
        try {
            if (this.o != null) {
                z2 = false;
                for (ej1<R> ej1 : this.o) {
                    z2 = ej1.g(r2, this.h, this.n, gb1, r3) | z2;
                }
            } else {
                z2 = false;
            }
            if (!(this.d != null && this.d.g(r2, this.h, this.n, gb1, r3)) && !z2) {
                this.n.b(r2, this.p.a(gb1, r3));
            }
            this.B = false;
            w();
        } catch (Throwable th) {
            this.B = false;
            throw th;
        }
    }
}
