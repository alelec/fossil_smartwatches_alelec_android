package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.telecom.TelecomManager;
import android.telephony.SmsManager;
import com.facebook.appevents.internal.InAppPurchaseEventManager;
import com.facebook.places.model.PlaceFields;
import com.fossil.jn5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.lang.reflect.Method;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ln5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f2224a;
    @DexIgnore
    public static ln5 b;
    @DexIgnore
    public static /* final */ a c; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ln5 a() {
            ln5 b;
            synchronized (this) {
                if (ln5.c.b() == null) {
                    ln5.c.c(new ln5(null));
                }
                b = ln5.c.b();
                if (b == null) {
                    pq7.i();
                    throw null;
                }
            }
            return b;
        }

        @DexIgnore
        public final ln5 b() {
            return ln5.b;
        }

        @DexIgnore
        public final void c(ln5 ln5) {
            ln5.b = ln5;
        }
    }

    /*
    static {
        String simpleName = ln5.class.getSimpleName();
        pq7.b(simpleName, "PhoneCallManager::class.java.simpleName");
        f2224a = simpleName;
    }
    */

    @DexIgnore
    public ln5() {
    }

    @DexIgnore
    public /* synthetic */ ln5(kq7 kq7) {
        this();
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    @TargetApi(26)
    public final void a() {
        try {
            Object systemService = PortfolioApp.h0.c().getSystemService("telecom");
            if (systemService != null) {
                ((TelecomManager) systemService).acceptRingingCall();
                return;
            }
            throw new il7("null cannot be cast to non-null type android.telecom.TelecomManager");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f2224a;
            local.d(str, "exception when accept call call " + e);
        }
    }

    @DexIgnore
    @SuppressLint({"MissingPermission", "PrivateApi"})
    public final void d() {
        if (Build.VERSION.SDK_INT >= 28) {
            try {
                Object systemService = PortfolioApp.h0.c().getSystemService("telecom");
                if (systemService != null) {
                    ((TelecomManager) systemService).endCall();
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.telecom.TelecomManager");
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e("reject >= P", e.getStackTrace().toString());
            }
        }
        try {
            Class<?> cls = Class.forName("com.android.internal.telephony.ITelephony");
            pq7.b(cls, "Class.forName(\"com.andro\u2026al.telephony.ITelephony\")");
            Class<?> cls2 = cls.getClasses()[0];
            Class<?> cls3 = Class.forName("android.os.ServiceManager");
            pq7.b(cls3, "Class.forName(\"android.os.ServiceManager\")");
            Class<?> cls4 = Class.forName("android.os.ServiceManagerNative");
            pq7.b(cls4, "Class.forName(\"android.os.ServiceManagerNative\")");
            Method method = cls3.getMethod("getService", String.class);
            pq7.b(method, "serviceManagerClass.getM\u2026ice\", String::class.java)");
            Method method2 = cls4.getMethod(InAppPurchaseEventManager.AS_INTERFACE, IBinder.class);
            pq7.b(method2, "serviceManagerNativeClas\u2026ce\", IBinder::class.java)");
            Binder binder = new Binder();
            binder.attachInterface(null, "fake");
            Object invoke = method.invoke(method2.invoke(null, binder), PlaceFields.PHONE);
            if (invoke != null) {
                Method method3 = cls2.getMethod(InAppPurchaseEventManager.AS_INTERFACE, IBinder.class);
                pq7.b(method3, "telephonyStubClass.getMe\u2026ce\", IBinder::class.java)");
                Object invoke2 = method3.invoke(null, (IBinder) invoke);
                Method method4 = cls.getMethod("endCall", new Class[0]);
                pq7.b(method4, "telephonyClass.getMethod(\"endCall\")");
                method4.invoke(invoke2, new Object[0]);
                return;
            }
            throw new il7("null cannot be cast to non-null type android.os.IBinder");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f2224a;
            local.d(str, "exception when decline call " + e2);
        }
    }

    @DexIgnore
    public final void e(String str, String str2, BroadcastReceiver broadcastReceiver) {
        pq7.c(str, "number");
        pq7.c(str2, "message");
        pq7.c(broadcastReceiver, "receiver");
        hr7 hr7 = hr7.f1520a;
        String format = String.format("DELIVERY_%s", Arrays.copyOf(new Object[]{PortfolioApp.h0.c().Q()}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = f2224a;
        local.d(str3, "sendSMSTo with " + format);
        PendingIntent broadcast = PendingIntent.getBroadcast(PortfolioApp.h0.c(), 0, new Intent(format), 0);
        PortfolioApp.h0.c().registerReceiver(broadcastReceiver, new IntentFilter(format));
        if (jn5.b.m(PortfolioApp.h0.c().getBaseContext(), jn5.c.SEND_SMS)) {
            SmsManager smsManager = SmsManager.getDefault();
            if (smsManager != null) {
                smsManager.sendTextMessage(str, null, str2, broadcast, null);
                return;
            }
            return;
        }
        Intent intent = new Intent();
        intent.putExtra("SEND_SMS_PERMISSION_REQUIRED", true);
        broadcastReceiver.onReceive(PortfolioApp.h0.c().getBaseContext(), intent);
    }
}
