package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ck extends qq7 implements vp7<fs, Float, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ nm b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ck(nm nmVar) {
        super(2);
        this.b = nmVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public tl7 invoke(fs fsVar, Float f) {
        float floatValue = f.floatValue();
        Iterator<T> it = this.b.G.iterator();
        int i = 0;
        while (it.hasNext()) {
            i = ((int) it.next().g) + i;
        }
        float f2 = (((float) i) + (((float) this.b.E) * floatValue)) / ((float) this.b.E);
        nm nmVar = this.b;
        if (f2 - nmVar.J > nmVar.N || f2 == 1.0f) {
            nm nmVar2 = this.b;
            nmVar2.J = f2;
            nmVar2.d(f2);
        }
        return tl7.f3441a;
    }
}
