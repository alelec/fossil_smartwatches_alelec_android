package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bs6 implements Factory<as6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ThemeRepository> f499a;

    @DexIgnore
    public bs6(Provider<ThemeRepository> provider) {
        this.f499a = provider;
    }

    @DexIgnore
    public static bs6 a(Provider<ThemeRepository> provider) {
        return new bs6(provider);
    }

    @DexIgnore
    public static as6 c(ThemeRepository themeRepository) {
        return new as6(themeRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public as6 get() {
        return c(this.f499a.get());
    }
}
