package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a0 extends jw0<k0> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ g0 f169a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a0(g0 g0Var, qw0 qw0) {
        super(qw0);
        this.f169a = g0Var;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.px0, java.lang.Object] */
    @Override // com.fossil.jw0
    public void bind(px0 px0, k0 k0Var) {
        k0 k0Var2 = k0Var;
        String str = k0Var2.f1848a;
        if (str == null) {
            px0.bindNull(1);
        } else {
            px0.bindString(1, str);
        }
        px0.bindLong(2, (long) this.f169a.c.a(k0Var2.b));
        String b = this.f169a.d.b(k0Var2.c);
        if (b == null) {
            px0.bindNull(3);
        } else {
            px0.bindString(3, b);
        }
        String str2 = k0Var2.d;
        if (str2 == null) {
            px0.bindNull(4);
        } else {
            px0.bindString(4, str2);
        }
        String str3 = k0Var2.e;
        if (str3 == null) {
            px0.bindNull(5);
        } else {
            px0.bindString(5, str3);
        }
        px0.bindLong(6, this.f169a.e.a(k0Var2.b()));
        px0.bindLong(7, this.f169a.e.a(k0Var2.a()));
        byte[] bArr = k0Var2.h;
        if (bArr == null) {
            px0.bindNull(8);
        } else {
            px0.bindBlob(8, bArr);
        }
    }

    @DexIgnore
    @Override // com.fossil.xw0
    public String createQuery() {
        return "INSERT OR REPLACE INTO `ThemeTemplateEntity` (`id`,`classifier`,`packageOSVersion`,`checksum`,`downloadUrl`,`updatedAt`,`createdAt`,`data`) VALUES (?,?,?,?,?,?,?,?)";
    }
}
