package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t83 implements xw2<w83> {
    @DexIgnore
    public static t83 c; // = new t83();
    @DexIgnore
    public /* final */ xw2<w83> b;

    @DexIgnore
    public t83() {
        this(ww2.b(new v83()));
    }

    @DexIgnore
    public t83(xw2<w83> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((w83) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ w83 zza() {
        return this.b.zza();
    }
}
