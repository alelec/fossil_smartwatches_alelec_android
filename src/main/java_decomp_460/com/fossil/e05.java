package com.fossil;

import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e05 {
    @DexIgnore
    public final Date a(long j) {
        return new Date(j);
    }

    @DexIgnore
    public final long b(Date date) {
        if (date != null) {
            return date.getTime();
        }
        return 0;
    }
}
