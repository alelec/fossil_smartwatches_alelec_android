package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ws2 extends ss2 implements us2 {
    @DexIgnore
    public ws2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
    }

    @DexIgnore
    @Override // com.fossil.us2
    public final void C1(String str, String str2, Bundle bundle, long j) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        qt2.c(d, bundle);
        d.writeLong(j);
        i(1, d);
    }

    @DexIgnore
    @Override // com.fossil.us2
    public final int zza() throws RemoteException {
        Parcel e = e(2, d());
        int readInt = e.readInt();
        e.recycle();
        return readInt;
    }
}
