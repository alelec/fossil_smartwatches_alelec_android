package com.fossil;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.os.SystemClock;
import android.util.Base64;
import com.facebook.applinks.AppLinkData;
import com.fossil.c02;
import com.fossil.h02;
import com.fossil.s32;
import com.j256.ormlite.field.FieldType;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j32 implements k22, s32 {
    @DexIgnore
    public static /* final */ ty1 f; // = ty1.b("proto");
    @DexIgnore
    public /* final */ p32 b;
    @DexIgnore
    public /* final */ t32 c;
    @DexIgnore
    public /* final */ t32 d;
    @DexIgnore
    public /* final */ l22 e;

    @DexIgnore
    public interface b<T, U> {
        @DexIgnore
        U apply(T t);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1707a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(String str, String str2) {
            this.f1707a = str;
            this.b = str2;
        }
    }

    @DexIgnore
    public interface d<T> {
        @DexIgnore
        T a();
    }

    @DexIgnore
    public j32(t32 t32, t32 t322, l22 l22, p32 p32) {
        this.b = p32;
        this.c = t32;
        this.d = t322;
        this.e = l22;
    }

    @DexIgnore
    public static /* synthetic */ Object C(Throwable th) {
        throw new r32("Timed out while trying to acquire the lock.", th);
    }

    @DexIgnore
    public static /* synthetic */ SQLiteDatabase D(Throwable th) {
        throw new r32("Timed out while trying to open db.", th);
    }

    @DexIgnore
    public static /* synthetic */ Long F(Cursor cursor) {
        if (cursor.moveToNext()) {
            return Long.valueOf(cursor.getLong(0));
        }
        return 0L;
    }

    @DexIgnore
    public static /* synthetic */ Long G(Cursor cursor) {
        if (!cursor.moveToNext()) {
            return null;
        }
        return Long.valueOf(cursor.getLong(0));
    }

    @DexIgnore
    public static /* synthetic */ Boolean L(j32 j32, h02 h02, SQLiteDatabase sQLiteDatabase) {
        Long k = j32.k(sQLiteDatabase, h02);
        if (k == null) {
            return Boolean.FALSE;
        }
        return (Boolean) u0(j32.f().rawQuery("SELECT 1 FROM events WHERE context_id = ? LIMIT 1", new String[]{k.toString()}), c32.a());
    }

    @DexIgnore
    public static /* synthetic */ List M(Cursor cursor) {
        ArrayList arrayList = new ArrayList();
        while (cursor.moveToNext()) {
            h02.a a2 = h02.a();
            a2.b(cursor.getString(1));
            a2.d(z32.b(cursor.getInt(2)));
            a2.c(p0(cursor.getString(3)));
            arrayList.add(a2.a());
        }
        return arrayList;
    }

    @DexIgnore
    public static /* synthetic */ List P(SQLiteDatabase sQLiteDatabase) {
        return (List) u0(sQLiteDatabase.rawQuery("SELECT distinct t._id, t.backend_name, t.priority, t.extras FROM transport_contexts AS t, events AS e WHERE e.context_id = t._id", new String[0]), b32.a());
    }

    @DexIgnore
    public static /* synthetic */ List Q(j32 j32, h02 h02, SQLiteDatabase sQLiteDatabase) {
        List<q22> i0 = j32.i0(sQLiteDatabase, h02);
        j32.o(i0, j32.o0(sQLiteDatabase, i0));
        return i0;
    }

    @DexIgnore
    public static /* synthetic */ Object S(j32 j32, List list, h02 h02, Cursor cursor) {
        while (cursor.moveToNext()) {
            long j = cursor.getLong(0);
            boolean z = cursor.getInt(7) != 0;
            c02.a a2 = c02.a();
            a2.j(cursor.getString(1));
            a2.i(cursor.getLong(2));
            a2.k(cursor.getLong(3));
            if (z) {
                a2.h(new b02(s0(cursor.getString(4)), cursor.getBlob(5)));
            } else {
                a2.h(new b02(s0(cursor.getString(4)), j32.q0(j)));
            }
            if (!cursor.isNull(6)) {
                a2.g(Integer.valueOf(cursor.getInt(6)));
            }
            list.add(q22.a(j, h02, a2.d()));
        }
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object T(Map map, Cursor cursor) {
        while (cursor.moveToNext()) {
            long j = cursor.getLong(0);
            Set set = (Set) map.get(Long.valueOf(j));
            if (set == null) {
                set = new HashSet();
                map.put(Long.valueOf(j), set);
            }
            set.add(new c(cursor.getString(1), cursor.getString(2)));
        }
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Long V(j32 j32, h02 h02, c02 c02, SQLiteDatabase sQLiteDatabase) {
        if (j32.m()) {
            return -1L;
        }
        long c2 = j32.c(sQLiteDatabase, h02);
        int e2 = j32.e.e();
        byte[] a2 = c02.e().a();
        boolean z = a2.length <= e2;
        ContentValues contentValues = new ContentValues();
        contentValues.put("context_id", Long.valueOf(c2));
        contentValues.put("transport_name", c02.j());
        contentValues.put("timestamp_ms", Long.valueOf(c02.f()));
        contentValues.put("uptime_ms", Long.valueOf(c02.k()));
        contentValues.put("payload_encoding", c02.e().b().a());
        contentValues.put("code", c02.d());
        contentValues.put("num_attempts", (Integer) 0);
        contentValues.put("inline", Boolean.valueOf(z));
        contentValues.put("payload", z ? a2 : new byte[0]);
        long insert = sQLiteDatabase.insert("events", null, contentValues);
        if (!z) {
            int ceil = (int) Math.ceil(((double) a2.length) / ((double) e2));
            for (int i = 1; i <= ceil; i++) {
                byte[] copyOfRange = Arrays.copyOfRange(a2, (i - 1) * e2, Math.min(i * e2, a2.length));
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put(LogBuilder.KEY_EVENT_ID, Long.valueOf(insert));
                contentValues2.put("sequence_num", Integer.valueOf(i));
                contentValues2.put("bytes", copyOfRange);
                sQLiteDatabase.insert("event_payloads", null, contentValues2);
            }
        }
        for (Map.Entry<String, String> entry : c02.i().entrySet()) {
            ContentValues contentValues3 = new ContentValues();
            contentValues3.put(LogBuilder.KEY_EVENT_ID, Long.valueOf(insert));
            contentValues3.put("name", entry.getKey());
            contentValues3.put("value", entry.getValue());
            sQLiteDatabase.insert("event_metadata", null, contentValues3);
        }
        return Long.valueOf(insert);
    }

    @DexIgnore
    public static /* synthetic */ byte[] X(Cursor cursor) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (cursor.moveToNext()) {
            byte[] blob = cursor.getBlob(0);
            arrayList.add(blob);
            i += blob.length;
        }
        byte[] bArr = new byte[i];
        int i2 = 0;
        for (int i3 = 0; i3 < arrayList.size(); i3++) {
            byte[] bArr2 = (byte[]) arrayList.get(i3);
            System.arraycopy(bArr2, 0, bArr, i2, bArr2.length);
            i2 += bArr2.length;
        }
        return bArr;
    }

    @DexIgnore
    public static /* synthetic */ Object b0(String str, SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.compileStatement(str).execute();
        sQLiteDatabase.compileStatement("DELETE FROM events WHERE num_attempts >= 10").execute();
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object g0(long j, h02 h02, SQLiteDatabase sQLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("next_request_ms", Long.valueOf(j));
        if (sQLiteDatabase.update("transport_contexts", contentValues, "backend_name = ? and priority = ?", new String[]{h02.b(), String.valueOf(z32.a(h02.d()))}) < 1) {
            contentValues.put("backend_name", h02.b());
            contentValues.put("priority", Integer.valueOf(z32.a(h02.d())));
            sQLiteDatabase.insert("transport_contexts", null, contentValues);
        }
        return null;
    }

    @DexIgnore
    public static byte[] p0(String str) {
        if (str == null) {
            return null;
        }
        return Base64.decode(str, 0);
    }

    @DexIgnore
    public static ty1 s0(String str) {
        return str == null ? f : ty1.b(str);
    }

    @DexIgnore
    public static String t0(Iterable<q22> iterable) {
        StringBuilder sb = new StringBuilder("(");
        Iterator<q22> it = iterable.iterator();
        while (it.hasNext()) {
            sb.append(it.next().c());
            if (it.hasNext()) {
                sb.append(',');
            }
        }
        sb.append(')');
        return sb.toString();
    }

    @DexIgnore
    public static <T> T u0(Cursor cursor, b<Cursor, T> bVar) {
        try {
            return bVar.apply(cursor);
        } finally {
            cursor.close();
        }
    }

    @DexIgnore
    @Override // com.fossil.k22
    public q22 Y(h02 h02, c02 c02) {
        c12.b("SQLiteEventStore", "Storing event with priority=%s, name=%s for destination %s", h02.d(), c02.j(), h02.b());
        long longValue = ((Long) l(e32.a(this, h02, c02))).longValue();
        if (longValue < 1) {
            return null;
        }
        return q22.a(longValue, h02, c02);
    }

    @DexIgnore
    @Override // com.fossil.s32
    public <T> T a(s32.a<T> aVar) {
        SQLiteDatabase f2 = f();
        b(f2);
        try {
            T a2 = aVar.a();
            f2.setTransactionSuccessful();
            return a2;
        } finally {
            f2.endTransaction();
        }
    }

    @DexIgnore
    public final void b(SQLiteDatabase sQLiteDatabase) {
        r0(y22.b(sQLiteDatabase), z22.a());
    }

    @DexIgnore
    public final long c(SQLiteDatabase sQLiteDatabase, h02 h02) {
        Long k = k(sQLiteDatabase, h02);
        if (k != null) {
            return k.longValue();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("backend_name", h02.b());
        contentValues.put("priority", Integer.valueOf(z32.a(h02.d())));
        contentValues.put("next_request_ms", (Integer) 0);
        if (h02.c() != null) {
            contentValues.put(AppLinkData.ARGUMENTS_EXTRAS_KEY, Base64.encodeToString(h02.c(), 0));
        }
        return sQLiteDatabase.insert("transport_contexts", null, contentValues);
    }

    @DexIgnore
    @Override // com.fossil.k22
    public long c0(h02 h02) {
        return ((Long) u0(f().rawQuery("SELECT next_request_ms FROM transport_contexts WHERE backend_name = ? and priority = ?", new String[]{h02.b(), String.valueOf(z32.a(h02.d()))}), h32.a())).longValue();
    }

    @DexIgnore
    @Override // com.fossil.k22
    public int cleanUp() {
        return ((Integer) l(u22.a(this.c.a() - this.e.c()))).intValue();
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.b.close();
    }

    @DexIgnore
    public SQLiteDatabase f() {
        p32 p32 = this.b;
        p32.getClass();
        return (SQLiteDatabase) r0(a32.b(p32), d32.a());
    }

    @DexIgnore
    @Override // com.fossil.k22
    public boolean f0(h02 h02) {
        return ((Boolean) l(i32.a(this, h02))).booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.k22
    public void g(Iterable<q22> iterable) {
        if (iterable.iterator().hasNext()) {
            f().compileStatement("DELETE FROM events WHERE _id in " + t0(iterable)).execute();
        }
    }

    @DexIgnore
    public final long h() {
        return f().compileStatement("PRAGMA page_count").simpleQueryForLong();
    }

    @DexIgnore
    @Override // com.fossil.k22
    public void h0(Iterable<q22> iterable) {
        if (iterable.iterator().hasNext()) {
            l(g32.a("UPDATE events SET num_attempts = num_attempts + 1 WHERE _id in " + t0(iterable)));
        }
    }

    @DexIgnore
    public final List<q22> i0(SQLiteDatabase sQLiteDatabase, h02 h02) {
        ArrayList arrayList = new ArrayList();
        Long k = k(sQLiteDatabase, h02);
        if (k == null) {
            return arrayList;
        }
        String l = k.toString();
        int d2 = this.e.d();
        u0(sQLiteDatabase.query("events", new String[]{FieldType.FOREIGN_ID_FIELD_SUFFIX, "transport_name", "timestamp_ms", "uptime_ms", "payload_encoding", "payload", "code", "inline"}, "context_id = ?", new String[]{l}, null, null, null, String.valueOf(d2)), v22.a(this, arrayList, h02));
        return arrayList;
    }

    @DexIgnore
    public final long j() {
        return f().compileStatement("PRAGMA page_size").simpleQueryForLong();
    }

    @DexIgnore
    public final Long k(SQLiteDatabase sQLiteDatabase, h02 h02) {
        StringBuilder sb = new StringBuilder("backend_name = ? and priority = ?");
        ArrayList arrayList = new ArrayList(Arrays.asList(h02.b(), String.valueOf(z32.a(h02.d()))));
        if (h02.c() != null) {
            sb.append(" and extras = ?");
            arrayList.add(Base64.encodeToString(h02.c(), 0));
        }
        return (Long) u0(sQLiteDatabase.query("transport_contexts", new String[]{FieldType.FOREIGN_ID_FIELD_SUFFIX}, sb.toString(), (String[]) arrayList.toArray(new String[0]), null, null, null), f32.a());
    }

    @DexIgnore
    public final <T> T l(b<SQLiteDatabase, T> bVar) {
        SQLiteDatabase f2 = f();
        f2.beginTransaction();
        try {
            T apply = bVar.apply(f2);
            f2.setTransactionSuccessful();
            return apply;
        } finally {
            f2.endTransaction();
        }
    }

    @DexIgnore
    public final boolean m() {
        return h() * j() >= this.e.f();
    }

    @DexIgnore
    public final List<q22> o(List<q22> list, Map<Long, Set<c>> map) {
        ListIterator<q22> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            q22 next = listIterator.next();
            if (map.containsKey(Long.valueOf(next.c()))) {
                c02.a l = next.b().l();
                for (c cVar : map.get(Long.valueOf(next.c()))) {
                    l.c(cVar.f1707a, cVar.b);
                }
                listIterator.set(q22.a(next.c(), next.d(), l.d()));
            }
        }
        return list;
    }

    @DexIgnore
    public final Map<Long, Set<c>> o0(SQLiteDatabase sQLiteDatabase, List<q22> list) {
        HashMap hashMap = new HashMap();
        StringBuilder sb = new StringBuilder("event_id IN (");
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i).c());
            if (i < list.size() - 1) {
                sb.append(',');
            }
        }
        sb.append(')');
        u0(sQLiteDatabase.query("event_metadata", new String[]{LogBuilder.KEY_EVENT_ID, "name", "value"}, sb.toString(), null, null, null, null), x22.a(hashMap));
        return hashMap;
    }

    @DexIgnore
    @Override // com.fossil.k22
    public Iterable<q22> q(h02 h02) {
        return (Iterable) l(s22.a(this, h02));
    }

    @DexIgnore
    public final byte[] q0(long j) {
        return (byte[]) u0(f().query("event_payloads", new String[]{"bytes"}, "event_id = ?", new String[]{String.valueOf(j)}, null, null, "sequence_num"), w22.a());
    }

    @DexIgnore
    public final <T> T r0(d<T> dVar, b<Throwable, T> bVar) {
        long a2 = this.d.a();
        while (true) {
            try {
                return dVar.a();
            } catch (SQLiteDatabaseLockedException e2) {
                if (this.d.a() >= ((long) this.e.b()) + a2) {
                    return bVar.apply(e2);
                }
                SystemClock.sleep(50);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.k22
    public void s(h02 h02, long j) {
        l(r22.a(j, h02));
    }

    @DexIgnore
    @Override // com.fossil.k22
    public Iterable<h02> w() {
        return (Iterable) l(t22.a());
    }
}
