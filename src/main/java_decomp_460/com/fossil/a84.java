package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a84 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ kb4 f212a; // = new kb4();
    @DexIgnore
    public /* final */ j64 b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public PackageManager d;
    @DexIgnore
    public String e;
    @DexIgnore
    public PackageInfo f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;
    @DexIgnore
    public String i;
    @DexIgnore
    public String j;
    @DexIgnore
    public String k;
    @DexIgnore
    public h94 l;
    @DexIgnore
    public c94 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements mt3<wc4, Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ String f213a;
        @DexIgnore
        public /* final */ /* synthetic */ qc4 b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;

        @DexIgnore
        public a(String str, qc4 qc4, Executor executor) {
            this.f213a = str;
            this.b = qc4;
            this.c = executor;
        }

        @DexIgnore
        /* renamed from: a */
        public nt3<Void> then(wc4 wc4) throws Exception {
            try {
                a84.this.i(wc4, this.f213a, this.b, this.c, true);
                return null;
            } catch (Exception e) {
                x74.f().e("Error performing auto configuration.", e);
                throw e;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements mt3<Void, wc4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ qc4 f214a;

        @DexIgnore
        public b(a84 a84, qc4 qc4) {
            this.f214a = qc4;
        }

        @DexIgnore
        /* renamed from: a */
        public nt3<wc4> then(Void r2) throws Exception {
            return this.f214a.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements ft3<Void, Object> {
        @DexIgnore
        public c(a84 a84) {
        }

        @DexIgnore
        @Override // com.fossil.ft3
        public Object then(nt3<Void> nt3) throws Exception {
            if (nt3.q()) {
                return null;
            }
            x74.f().e("Error fetching settings.", nt3.l());
            return null;
        }
    }

    @DexIgnore
    public a84(j64 j64, Context context, h94 h94, c94 c94) {
        this.b = j64;
        this.c = context;
        this.l = h94;
        this.m = c94;
    }

    @DexIgnore
    public static String g() {
        return w84.i();
    }

    @DexIgnore
    public final vc4 b(String str, String str2) {
        return new vc4(str, str2, e().d(), this.h, this.g, r84.h(r84.p(d()), str2, this.h, this.g), this.j, e94.determineFrom(this.i).getId(), this.k, "0");
    }

    @DexIgnore
    public void c(Executor executor, qc4 qc4) {
        this.m.d().s(executor, new b(this, qc4)).s(executor, new a(this.b.j().c(), qc4, executor));
    }

    @DexIgnore
    public Context d() {
        return this.c;
    }

    @DexIgnore
    public final h94 e() {
        return this.l;
    }

    @DexIgnore
    public String f() {
        return r84.u(this.c, "com.crashlytics.ApiEndpoint");
    }

    @DexIgnore
    public boolean h() {
        try {
            this.i = this.l.e();
            this.d = this.c.getPackageManager();
            String packageName = this.c.getPackageName();
            this.e = packageName;
            PackageInfo packageInfo = this.d.getPackageInfo(packageName, 0);
            this.f = packageInfo;
            this.g = Integer.toString(packageInfo.versionCode);
            this.h = this.f.versionName == null ? "0.0" : this.f.versionName;
            this.j = this.d.getApplicationLabel(this.c.getApplicationInfo()).toString();
            this.k = Integer.toString(this.c.getApplicationInfo().targetSdkVersion);
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            x74.f().e("Failed init", e2);
            return false;
        }
    }

    @DexIgnore
    public final void i(wc4 wc4, String str, qc4 qc4, Executor executor, boolean z) {
        if ("new".equals(wc4.f3917a)) {
            if (j(wc4, str, z)) {
                qc4.o(pc4.SKIP_CACHE_LOOKUP, executor);
            } else {
                x74.f().e("Failed to create app with Crashlytics service.", null);
            }
        } else if ("configured".equals(wc4.f3917a)) {
            qc4.o(pc4.SKIP_CACHE_LOOKUP, executor);
        } else if (wc4.f) {
            x74.f().b("Server says an update is required - forcing a full App update.");
            k(wc4, str, z);
        }
    }

    @DexIgnore
    public final boolean j(wc4 wc4, String str, boolean z) {
        return new dd4(f(), wc4.b, this.f212a, g()).i(b(wc4.e, str), z);
    }

    @DexIgnore
    public final boolean k(wc4 wc4, String str, boolean z) {
        return new gd4(f(), wc4.b, this.f212a, g()).i(b(wc4.e, str), z);
    }

    @DexIgnore
    public qc4 l(Context context, j64 j64, Executor executor) {
        qc4 l2 = qc4.l(context, j64.j().c(), this.l, this.f212a, this.g, this.h, f(), this.m);
        l2.p(executor).i(executor, new c(this));
        return l2;
    }
}
