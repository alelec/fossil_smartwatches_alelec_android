package com.fossil;

import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.FlutterEngineCache;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v37 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static FlutterEngine f3710a; // = FlutterEngineCache.getInstance().get("screenshot_engine_id");
    @DexIgnore
    public static FlutterEngine b; // = FlutterEngineCache.getInstance().get("map_engine_id");
    @DexIgnore
    public static /* final */ v37 c; // = new v37();

    @DexIgnore
    public final FlutterEngine a() {
        return b;
    }

    @DexIgnore
    public final FlutterEngine b() {
        return f3710a;
    }
}
