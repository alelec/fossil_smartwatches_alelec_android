package com.fossil;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mo3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ u93 b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ String d;
    @DexIgnore
    public /* final */ /* synthetic */ boolean e;
    @DexIgnore
    public /* final */ /* synthetic */ AppMeasurementDynamiteService f;

    @DexIgnore
    public mo3(AppMeasurementDynamiteService appMeasurementDynamiteService, u93 u93, String str, String str2, boolean z) {
        this.f = appMeasurementDynamiteService;
        this.b = u93;
        this.c = str;
        this.d = str2;
        this.e = z;
    }

    @DexIgnore
    public final void run() {
        this.f.b.O().J(this.b, this.c, this.d, this.e);
    }
}
