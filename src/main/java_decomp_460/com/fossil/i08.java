package com.fossil;

import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i08 extends mw7 {
    @DexIgnore
    public g08 c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public i08(int i, int i2, long j, String str) {
        this.d = i;
        this.e = i2;
        this.f = j;
        this.g = str;
        this.c = V();
    }

    @DexIgnore
    public i08(int i, int i2, String str) {
        this(i, i2, q08.d, str);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ i08(int i, int i2, String str, int i3, kq7 kq7) {
        this((i3 & 1) != 0 ? q08.b : i, (i3 & 2) != 0 ? q08.c : i2, (i3 & 4) != 0 ? "DefaultDispatcher" : str);
    }

    @DexIgnore
    @Override // com.fossil.dv7
    public void M(tn7 tn7, Runnable runnable) {
        try {
            g08.k(this.c, runnable, null, false, 6, null);
        } catch (RejectedExecutionException e2) {
            pv7.i.M(tn7, runnable);
        }
    }

    @DexIgnore
    @Override // com.fossil.dv7
    public void P(tn7 tn7, Runnable runnable) {
        try {
            g08.k(this.c, runnable, null, true, 2, null);
        } catch (RejectedExecutionException e2) {
            pv7.i.P(tn7, runnable);
        }
    }

    @DexIgnore
    @Override // com.fossil.mw7
    public Executor S() {
        return this.c;
    }

    @DexIgnore
    public final dv7 T(int i) {
        if (i > 0) {
            return new k08(this, i, 1);
        }
        throw new IllegalArgumentException(("Expected positive parallelism level, but have " + i).toString());
    }

    @DexIgnore
    public final g08 V() {
        return new g08(this.d, this.e, this.f, this.g);
    }

    @DexIgnore
    public final void X(Runnable runnable, o08 o08, boolean z) {
        try {
            this.c.j(runnable, o08, z);
        } catch (RejectedExecutionException e2) {
            pv7.i.z0(this.c.f(runnable, o08));
        }
    }
}
