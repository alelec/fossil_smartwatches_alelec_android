package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x35 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ ScrollView t;
    @DexIgnore
    public /* final */ RecyclerView u;
    @DexIgnore
    public /* final */ RecyclerView v;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public x35(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ImageView imageView, ScrollView scrollView, RecyclerView recyclerView, RecyclerView recyclerView2, FlexibleSwitchCompat flexibleSwitchCompat, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = imageView;
        this.t = scrollView;
        this.u = recyclerView;
        this.v = recyclerView2;
        this.w = flexibleSwitchCompat;
        this.x = flexibleTextView;
        this.y = flexibleTextView2;
        this.z = flexibleTextView3;
        this.A = flexibleTextView4;
        this.B = flexibleTextView5;
    }
}
