package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.cw1;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uv1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ cw1 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<uv1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final uv1 a(JSONObject jSONObject) throws IllegalArgumentException {
            try {
                cw1.a aVar = cw1.d;
                String string = jSONObject.getString(BaseFeatureModel.COLUMN_COLOR);
                pq7.b(string, "themeConfigObject.getStr\u2026g(UIScriptConstant.COLOR)");
                cw1 a2 = aVar.a(string);
                if (a2 != null) {
                    return new uv1(a2);
                }
                throw new IllegalArgumentException("Required value was null.".toString());
            } catch (JSONException e) {
                return null;
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public uv1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                return new uv1(cw1.valueOf(readString));
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public uv1[] newArray(int i) {
            return new uv1[i];
        }
    }

    @DexIgnore
    public uv1(cw1 cw1) {
        this.b = cw1;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = new JSONObject().put(BaseFeatureModel.COLUMN_COLOR, ey1.a(this.b));
        pq7.b(put, "JSONObject()\n           \u2026 fontColor.lowerCaseName)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(uv1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((uv1) obj).b;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.ComplicationTheme");
    }

    @DexIgnore
    public final cw1 getFontColor() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return a();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
    }
}
