package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qg5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ConstraintLayout f2978a;
    @DexIgnore
    public /* final */ ConstraintLayout b;
    @DexIgnore
    public /* final */ ConstraintLayout c;
    @DexIgnore
    public /* final */ ImageView d;
    @DexIgnore
    public /* final */ ScrollView e;
    @DexIgnore
    public /* final */ RecyclerView f;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat g;
    @DexIgnore
    public /* final */ FlexibleTextView h;
    @DexIgnore
    public /* final */ FlexibleTextView i;
    @DexIgnore
    public /* final */ FlexibleTextView j;
    @DexIgnore
    public /* final */ FlexibleTextView k;
    @DexIgnore
    public /* final */ FlexibleTextView l;
    @DexIgnore
    public /* final */ ConstraintLayout m;

    @DexIgnore
    public qg5(ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ImageView imageView, ScrollView scrollView, RecyclerView recyclerView, FlexibleSwitchCompat flexibleSwitchCompat, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, ConstraintLayout constraintLayout4) {
        this.f2978a = constraintLayout;
        this.b = constraintLayout2;
        this.c = constraintLayout3;
        this.d = imageView;
        this.e = scrollView;
        this.f = recyclerView;
        this.g = flexibleSwitchCompat;
        this.h = flexibleTextView;
        this.i = flexibleTextView2;
        this.j = flexibleTextView3;
        this.k = flexibleTextView4;
        this.l = flexibleTextView5;
        this.m = constraintLayout4;
    }

    @DexIgnore
    public static qg5 a(View view) {
        int i2 = 2131363031;
        ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(2131362053);
        if (constraintLayout != null) {
            ConstraintLayout constraintLayout2 = (ConstraintLayout) view.findViewById(2131362056);
            if (constraintLayout2 != null) {
                ImageView imageView = (ImageView) view.findViewById(2131362687);
                if (imageView != null) {
                    ScrollView scrollView = (ScrollView) view.findViewById(2131363010);
                    if (scrollView != null) {
                        RecyclerView recyclerView = (RecyclerView) view.findViewById(2131363031);
                        if (recyclerView != null) {
                            i2 = 2131363171;
                            FlexibleSwitchCompat flexibleSwitchCompat = (FlexibleSwitchCompat) view.findViewById(2131363171);
                            if (flexibleSwitchCompat != null) {
                                i2 = 2131363293;
                                FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(2131363293);
                                if (flexibleTextView != null) {
                                    i2 = 2131363295;
                                    FlexibleTextView flexibleTextView2 = (FlexibleTextView) view.findViewById(2131363295);
                                    if (flexibleTextView2 != null) {
                                        i2 = 2131363296;
                                        FlexibleTextView flexibleTextView3 = (FlexibleTextView) view.findViewById(2131363296);
                                        if (flexibleTextView3 != null) {
                                            i2 = 2131363374;
                                            FlexibleTextView flexibleTextView4 = (FlexibleTextView) view.findViewById(2131363374);
                                            if (flexibleTextView4 != null) {
                                                i2 = 2131363390;
                                                FlexibleTextView flexibleTextView5 = (FlexibleTextView) view.findViewById(2131363390);
                                                if (flexibleTextView5 != null) {
                                                    ConstraintLayout constraintLayout3 = (ConstraintLayout) view;
                                                    return new qg5(constraintLayout3, constraintLayout, constraintLayout2, imageView, scrollView, recyclerView, flexibleSwitchCompat, flexibleTextView, flexibleTextView2, flexibleTextView3, flexibleTextView4, flexibleTextView5, constraintLayout3);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        i2 = 2131363010;
                    }
                } else {
                    i2 = 2131362687;
                }
            } else {
                i2 = 2131362056;
            }
        } else {
            i2 = 2131362053;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
    }

    @DexIgnore
    public static qg5 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    @DexIgnore
    public static qg5 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(2131558849, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    @DexIgnore
    public ConstraintLayout b() {
        return this.f2978a;
    }
}
