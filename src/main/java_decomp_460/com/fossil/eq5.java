package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eq5 implements Factory<dq5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<vr5> f976a;
    @DexIgnore
    public /* final */ Provider<on5> b;

    @DexIgnore
    public eq5(Provider<vr5> provider, Provider<on5> provider2) {
        this.f976a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static eq5 a(Provider<vr5> provider, Provider<on5> provider2) {
        return new eq5(provider, provider2);
    }

    @DexIgnore
    public static dq5 c() {
        return new dq5();
    }

    @DexIgnore
    /* renamed from: b */
    public dq5 get() {
        dq5 c = c();
        fq5.a(c, this.f976a.get());
        fq5.b(c, this.b.get());
        return c;
    }
}
