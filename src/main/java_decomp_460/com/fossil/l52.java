package com.fossil;

import com.google.android.gms.common.api.Scope;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class l52 implements Comparator {
    @DexIgnore
    public static /* final */ Comparator b; // = new l52();

    @DexIgnore
    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return ((Scope) obj).c().compareTo(((Scope) obj2).c());
    }
}
