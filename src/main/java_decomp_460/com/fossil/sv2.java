package com.fossil;

import android.content.Context;
import android.database.ContentObserver;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sv2 implements nv2 {
    @DexIgnore
    public static sv2 c;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3312a;
    @DexIgnore
    public /* final */ ContentObserver b;

    @DexIgnore
    public sv2() {
        this.f3312a = null;
        this.b = null;
    }

    @DexIgnore
    public sv2(Context context) {
        this.f3312a = context;
        this.b = new uv2(this, null);
        context.getContentResolver().registerContentObserver(gv2.f1370a, true, this.b);
    }

    @DexIgnore
    public static sv2 a(Context context) {
        sv2 sv2;
        synchronized (sv2.class) {
            try {
                if (c == null) {
                    c = hl0.b(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0 ? new sv2(context) : new sv2();
                }
                sv2 = c;
            } catch (Throwable th) {
                throw th;
            }
        }
        return sv2;
    }

    @DexIgnore
    public static void b() {
        synchronized (sv2.class) {
            try {
                if (!(c == null || c.f3312a == null || c.b == null)) {
                    c.f3312a.getContentResolver().unregisterContentObserver(c.b);
                }
                c = null;
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    public final /* synthetic */ String c(String str) {
        return gv2.a(this.f3312a.getContentResolver(), str, null);
    }

    @DexIgnore
    /* renamed from: d */
    public final String zza(String str) {
        if (this.f3312a == null) {
            return null;
        }
        try {
            return (String) qv2.a(new rv2(this, str));
        } catch (IllegalStateException | SecurityException e) {
            String valueOf = String.valueOf(str);
            Log.e("GservicesLoader", valueOf.length() != 0 ? "Unable to read GServices for: ".concat(valueOf) : new String("Unable to read GServices for: "), e);
            return null;
        }
    }
}
