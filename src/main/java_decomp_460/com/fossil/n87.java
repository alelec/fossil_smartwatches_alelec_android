package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n87 implements Factory<m87> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<s77> f2481a;

    @DexIgnore
    public n87(Provider<s77> provider) {
        this.f2481a = provider;
    }

    @DexIgnore
    public static n87 a(Provider<s77> provider) {
        return new n87(provider);
    }

    @DexIgnore
    public static m87 c(s77 s77) {
        return new m87(s77);
    }

    @DexIgnore
    /* renamed from: b */
    public m87 get() {
        return c(this.f2481a.get());
    }
}
