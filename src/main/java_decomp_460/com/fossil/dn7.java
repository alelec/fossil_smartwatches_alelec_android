package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dn7 extends cn7 {
    @DexIgnore
    public static final <T> Set<T> b() {
        return tm7.INSTANCE;
    }

    @DexIgnore
    public static final <T> LinkedHashSet<T> c(T... tArr) {
        pq7.c(tArr, MessengerShareContentUtility.ELEMENTS);
        LinkedHashSet<T> linkedHashSet = new LinkedHashSet<>(ym7.b(tArr.length));
        em7.b0(tArr, linkedHashSet);
        return linkedHashSet;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Set<? extends T> */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> Set<T> d(Set<? extends T> set) {
        pq7.c(set, "$this$optimizeReadOnlySet");
        int size = set.size();
        return size != 0 ? size != 1 ? set : cn7.a(set.iterator().next()) : b();
    }
}
