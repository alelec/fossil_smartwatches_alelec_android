package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oa7 extends hq4 {
    @DexIgnore
    public /* final */ MutableLiveData<String> h;
    @DexIgnore
    public /* final */ LiveData<String> i;
    @DexIgnore
    public /* final */ MutableLiveData<u37<Boolean>> j;
    @DexIgnore
    public /* final */ LiveData<u37<Boolean>> k;
    @DexIgnore
    public /* final */ MutableLiveData<u37<String>> l;
    @DexIgnore
    public /* final */ LiveData<u37<String>> m;
    @DexIgnore
    public /* final */ MutableLiveData<u37<String>> n;
    @DexIgnore
    public /* final */ LiveData<u37<String>> o;

    @DexIgnore
    public oa7() {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        this.h = mutableLiveData;
        this.i = mutableLiveData;
        MutableLiveData<u37<Boolean>> mutableLiveData2 = new MutableLiveData<>(new u37(Boolean.FALSE));
        this.j = mutableLiveData2;
        this.k = mutableLiveData2;
        MutableLiveData<u37<String>> mutableLiveData3 = new MutableLiveData<>();
        this.l = mutableLiveData3;
        this.m = mutableLiveData3;
        MutableLiveData<u37<String>> mutableLiveData4 = new MutableLiveData<>();
        this.n = mutableLiveData4;
        this.o = mutableLiveData4;
    }

    @DexIgnore
    public final void n(String str) {
        pq7.c(str, "watchFaceId");
        this.l.l(new u37<>(str));
    }

    @DexIgnore
    public final void o(String str) {
        this.n.l(new u37<>(str));
    }

    @DexIgnore
    public final LiveData<u37<String>> p() {
        return this.m;
    }

    @DexIgnore
    public final LiveData<String> q() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<u37<String>> r() {
        return this.o;
    }

    @DexIgnore
    public final LiveData<u37<Boolean>> s() {
        return this.k;
    }

    @DexIgnore
    public final void t(String str) {
        pq7.c(str, "watchFaceId");
        this.h.l(str);
    }

    @DexIgnore
    public final void u(boolean z) {
        this.j.l(new u37<>(Boolean.valueOf(z)));
    }
}
