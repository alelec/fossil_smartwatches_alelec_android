package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fa4 extends ta4.d.a.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1092a;

    @DexIgnore
    @Override // com.fossil.ta4.d.a.b
    public String a() {
        return this.f1092a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ta4.d.a.b) {
            return this.f1092a.equals(((ta4.d.a.b) obj).a());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.f1092a.hashCode() ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "Organization{clsId=" + this.f1092a + "}";
    }
}
