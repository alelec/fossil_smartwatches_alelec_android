package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w73 implements t73 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f3893a;
    @DexIgnore
    public static /* final */ xv2<Boolean> b;
    @DexIgnore
    public static /* final */ xv2<Boolean> c;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        hw2.b("measurement.id.lifecycle.app_in_background_parameter", 0);
        f3893a = hw2.d("measurement.lifecycle.app_backgrounded_engagement", false);
        b = hw2.d("measurement.lifecycle.app_backgrounded_tracking", true);
        c = hw2.d("measurement.lifecycle.app_in_background_parameter", false);
        hw2.b("measurement.id.lifecycle.app_backgrounded_tracking", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.t73
    public final boolean zza() {
        return f3893a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.t73
    public final boolean zzb() {
        return b.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.t73
    public final boolean zzc() {
        return c.o().booleanValue();
    }
}
