package com.fossil;

import com.fossil.c44;
import com.fossil.h34;
import com.fossil.u24;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class g34<E> extends u24<E> implements c44<E> {
    @DexIgnore
    @LazyInit
    public transient y24<E> b;
    @DexIgnore
    @LazyInit
    public transient h34<c44.a<E>> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends h54<E> {
        @DexIgnore
        public int b;
        @DexIgnore
        public E c;
        @DexIgnore
        public /* final */ /* synthetic */ Iterator d;

        @DexIgnore
        public a(g34 g34, Iterator it) {
            this.d = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b > 0 || this.d.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public E next() {
            if (this.b <= 0) {
                c44.a aVar = (c44.a) this.d.next();
                this.c = (E) aVar.getElement();
                this.b = aVar.getCount();
            }
            this.b--;
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<E> extends u24.b<E> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ c44<E> f1254a;

        @DexIgnore
        public b() {
            this(r34.create());
        }

        @DexIgnore
        public b(c44<E> c44) {
            this.f1254a = c44;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        /* renamed from: e */
        public b<E> a(E e) {
            c44<E> c44 = this.f1254a;
            i14.l(e);
            c44.add(e);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b<E> f(E... eArr) {
            super.b(eArr);
            return this;
        }

        @DexIgnore
        public g34<E> g() {
            return g34.copyOf(this.f1254a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends h34.b<c44.a<E>> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public /* synthetic */ c(g34 g34, a aVar) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean contains(Object obj) {
            if (!(obj instanceof c44.a)) {
                return false;
            }
            c44.a aVar = (c44.a) obj;
            return aVar.getCount() > 0 && g34.this.count(aVar.getElement()) == aVar.getCount();
        }

        @DexIgnore
        @Override // com.fossil.h34.b
        public c44.a<E> get(int i) {
            return g34.this.getEntry(i);
        }

        @DexIgnore
        @Override // com.fossil.h34
        public int hashCode() {
            return g34.this.hashCode();
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean isPartialView() {
            return g34.this.isPartialView();
        }

        @DexIgnore
        public int size() {
            return g34.this.elementSet().size();
        }

        @DexIgnore
        @Override // com.fossil.u24, com.fossil.h34
        public Object writeReplace() {
            return new d(g34.this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<E> implements Serializable {
        @DexIgnore
        public /* final */ g34<E> multiset;

        @DexIgnore
        public d(g34<E> g34) {
            this.multiset = g34;
        }

        @DexIgnore
        public Object readResolve() {
            return this.multiset.entrySet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ int[] counts;
        @DexIgnore
        public /* final */ Object[] elements;

        @DexIgnore
        public e(c44<?> c44) {
            int size = c44.entrySet().size();
            this.elements = new Object[size];
            this.counts = new int[size];
            int i = 0;
            for (c44.a<?> aVar : c44.entrySet()) {
                this.elements[i] = aVar.getElement();
                this.counts[i] = aVar.getCount();
                i++;
            }
        }

        @DexIgnore
        public Object readResolve() {
            r34 create = r34.create(this.elements.length);
            int i = 0;
            while (true) {
                Object[] objArr = this.elements;
                if (i >= objArr.length) {
                    return g34.copyOf(create);
                }
                create.add(objArr[i], this.counts[i]);
                i++;
            }
        }
    }

    @DexIgnore
    public static <E> g34<E> a(E... eArr) {
        r34 create = r34.create();
        Collections.addAll(create, eArr);
        return copyFromEntries(create.entrySet());
    }

    @DexIgnore
    public static <E> b<E> builder() {
        return new b<>();
    }

    @DexIgnore
    public static <E> g34<E> copyFromEntries(Collection<? extends c44.a<? extends E>> collection) {
        return collection.isEmpty() ? of() : new q44(collection);
    }

    @DexIgnore
    public static <E> g34<E> copyOf(Iterable<? extends E> iterable) {
        if (iterable instanceof g34) {
            g34<E> g34 = (g34) iterable;
            if (!g34.isPartialView()) {
                return g34;
            }
        }
        return copyFromEntries((iterable instanceof c44 ? d44.b(iterable) : r34.create(iterable)).entrySet());
    }

    @DexIgnore
    public static <E> g34<E> copyOf(Iterator<? extends E> it) {
        r34 create = r34.create();
        p34.a(create, it);
        return copyFromEntries(create.entrySet());
    }

    @DexIgnore
    public static <E> g34<E> copyOf(E[] eArr) {
        return a(eArr);
    }

    @DexIgnore
    public static <E> g34<E> of() {
        return q44.EMPTY;
    }

    @DexIgnore
    public static <E> g34<E> of(E e2) {
        return a(e2);
    }

    @DexIgnore
    public static <E> g34<E> of(E e2, E e3) {
        return a(e2, e3);
    }

    @DexIgnore
    public static <E> g34<E> of(E e2, E e3, E e4) {
        return a(e2, e3, e4);
    }

    @DexIgnore
    public static <E> g34<E> of(E e2, E e3, E e4, E e5) {
        return a(e2, e3, e4, e5);
    }

    @DexIgnore
    public static <E> g34<E> of(E e2, E e3, E e4, E e5, E e6) {
        return a(e2, e3, e4, e5, e6);
    }

    @DexIgnore
    public static <E> g34<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E... eArr) {
        b bVar = new b();
        bVar.a(e2);
        bVar.a(e3);
        bVar.a(e4);
        bVar.a(e5);
        bVar.a(e6);
        bVar.a(e7);
        bVar.f(eArr);
        return bVar.g();
    }

    @DexIgnore
    @Override // com.fossil.c44
    @CanIgnoreReturnValue
    @Deprecated
    public final int add(E e2, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.u24
    public y24<E> asList() {
        y24<E> y24 = this.b;
        if (y24 != null) {
            return y24;
        }
        y24<E> createAsList = createAsList();
        this.b = createAsList;
        return createAsList;
    }

    @DexIgnore
    public final h34<c44.a<E>> b() {
        return isEmpty() ? h34.of() : new c(this, null);
    }

    @DexIgnore
    @Override // com.fossil.c44, com.fossil.u24
    public boolean contains(Object obj) {
        return count(obj) > 0;
    }

    @DexIgnore
    @Override // com.fossil.u24
    public int copyIntoArray(Object[] objArr, int i) {
        Iterator it = entrySet().iterator();
        while (it.hasNext()) {
            c44.a aVar = (c44.a) it.next();
            Arrays.fill(objArr, i, aVar.getCount() + i, aVar.getElement());
            i += aVar.getCount();
        }
        return i;
    }

    @DexIgnore
    @Override // com.fossil.c44
    public abstract /* synthetic */ int count(Object obj);

    @DexIgnore
    public y24<E> createAsList() {
        return isEmpty() ? y24.of() : new m44(this, toArray());
    }

    @DexIgnore
    @Override // com.fossil.c44
    public abstract /* synthetic */ Set<E> elementSet();

    @DexIgnore
    @Override // com.fossil.c44
    public h34<c44.a<E>> entrySet() {
        h34<c44.a<E>> h34 = this.c;
        if (h34 != null) {
            return h34;
        }
        h34<c44.a<E>> b2 = b();
        this.c = b2;
        return b2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return d44.c(this, obj);
    }

    @DexIgnore
    public abstract c44.a<E> getEntry(int i);

    @DexIgnore
    public int hashCode() {
        return x44.b(entrySet());
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.u24, com.fossil.u24, java.lang.Iterable
    public h54<E> iterator() {
        return new a(this, entrySet().iterator());
    }

    @DexIgnore
    @Override // com.fossil.c44
    @CanIgnoreReturnValue
    @Deprecated
    public final int remove(Object obj, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.c44
    @CanIgnoreReturnValue
    @Deprecated
    public final int setCount(E e2, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.c44
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean setCount(E e2, int i, int i2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public String toString() {
        return entrySet().toString();
    }

    @DexIgnore
    @Override // com.fossil.u24
    public Object writeReplace() {
        return new e(this);
    }
}
