package com.fossil;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import com.fossil.m47;
import com.fossil.vr4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tr4 implements vr4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<String> f3454a; // = hm7.i("Agree term of use", "Agree privacy", "Allow location data processing");

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean a() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean b() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean c() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean d() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean e() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean f() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean g() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public String h() {
        return vr4.a.a(this);
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean i() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean j() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public void k(pv5 pv5) {
        pq7.c(pv5, "fragment");
        String a2 = m47.a(m47.c.REPAIR_CENTER, null);
        pq7.b(a2, "url");
        o(a2, pv5);
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean l() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean m(List<String> list) {
        pq7.c(list, "agreeRequirements");
        return list.containsAll(this.f3454a);
    }

    @DexIgnore
    @Override // com.fossil.vr4
    public boolean n() {
        return true;
    }

    @DexIgnore
    public final void o(String str, pv5 pv5) {
        try {
            pv5.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (ActivityNotFoundException e) {
            FLogger.INSTANCE.getLocal().e("CitizenBrandLogic", "Exception when start url with no browser app");
        }
    }
}
