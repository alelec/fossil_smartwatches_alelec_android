package com.fossil;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mc7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ConcurrentMap<Class<?>, Map<Class<?>, Method>> f2358a; // = new ConcurrentHashMap();
    @DexIgnore
    public static /* final */ ConcurrentMap<Class<?>, Map<Class<?>, Set<Method>>> b; // = new ConcurrentHashMap();

    @DexIgnore
    public static Map<Class<?>, qc7> a(Object obj) {
        Class<?> cls = obj.getClass();
        HashMap hashMap = new HashMap();
        Map<Class<?>, Method> map = f2358a.get(cls);
        if (map == null) {
            map = new HashMap<>();
            d(cls, map);
        }
        if (!map.isEmpty()) {
            for (Map.Entry<Class<?>, Method> entry : map.entrySet()) {
                hashMap.put(entry.getKey(), new qc7(obj, entry.getValue()));
            }
        }
        return hashMap;
    }

    @DexIgnore
    public static Map<Class<?>, Set<pc7>> b(Object obj) {
        Class<?> cls = obj.getClass();
        HashMap hashMap = new HashMap();
        Map<Class<?>, Set<Method>> map = b.get(cls);
        if (map == null) {
            map = new HashMap<>();
            e(cls, map);
        }
        if (!map.isEmpty()) {
            for (Map.Entry<Class<?>, Set<Method>> entry : map.entrySet()) {
                HashSet hashSet = new HashSet();
                for (Method method : entry.getValue()) {
                    hashSet.add(new pc7(obj, method));
                }
                hashMap.put(entry.getKey(), hashSet);
            }
        }
        return hashMap;
    }

    @DexIgnore
    public static void c(Class<?> cls, Map<Class<?>, Method> map, Map<Class<?>, Set<Method>> map2) {
        Method[] declaredMethods = cls.getDeclaredMethods();
        for (Method method : declaredMethods) {
            if (!method.isBridge()) {
                if (method.isAnnotationPresent(tc7.class)) {
                    Class<?>[] parameterTypes = method.getParameterTypes();
                    if (parameterTypes.length == 1) {
                        Class<?> cls2 = parameterTypes[0];
                        if (cls2.isInterface()) {
                            throw new IllegalArgumentException("Method " + method + " has @Subscribe annotation on " + cls2 + " which is an interface.  Subscription must be on a concrete class type.");
                        } else if ((method.getModifiers() & 1) != 0) {
                            Set<Method> set = map2.get(cls2);
                            if (set == null) {
                                set = new HashSet<>();
                                map2.put(cls2, set);
                            }
                            set.add(method);
                        } else {
                            throw new IllegalArgumentException("Method " + method + " has @Subscribe annotation on " + cls2 + " but is not 'public'.");
                        }
                    } else {
                        throw new IllegalArgumentException("Method " + method + " has @Subscribe annotation but requires " + parameterTypes.length + " arguments.  Methods must require a single argument.");
                    }
                } else if (method.isAnnotationPresent(sc7.class)) {
                    Class<?>[] parameterTypes2 = method.getParameterTypes();
                    if (parameterTypes2.length != 0) {
                        throw new IllegalArgumentException("Method " + method + "has @Produce annotation but requires " + parameterTypes2.length + " arguments.  Methods must require zero arguments.");
                    } else if (method.getReturnType() != Void.class) {
                        Class<?> returnType = method.getReturnType();
                        if (returnType.isInterface()) {
                            throw new IllegalArgumentException("Method " + method + " has @Produce annotation on " + returnType + " which is an interface.  Producers must return a concrete class type.");
                        } else if (returnType.equals(Void.TYPE)) {
                            throw new IllegalArgumentException("Method " + method + " has @Produce annotation but has no return type.");
                        } else if ((method.getModifiers() & 1) == 0) {
                            throw new IllegalArgumentException("Method " + method + " has @Produce annotation on " + returnType + " but is not 'public'.");
                        } else if (!map.containsKey(returnType)) {
                            map.put(returnType, method);
                        } else {
                            throw new IllegalArgumentException("Producer for type " + returnType + " has already been registered.");
                        }
                    } else {
                        throw new IllegalArgumentException("Method " + method + " has a return type of void.  Must declare a non-void type.");
                    }
                } else {
                    continue;
                }
            }
        }
        f2358a.put(cls, map);
        b.put(cls, map2);
    }

    @DexIgnore
    public static void d(Class<?> cls, Map<Class<?>, Method> map) {
        c(cls, map, new HashMap());
    }

    @DexIgnore
    public static void e(Class<?> cls, Map<Class<?>, Set<Method>> map) {
        c(cls, new HashMap(), map);
    }
}
