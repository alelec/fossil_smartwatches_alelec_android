package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class l8 extends Enum<l8> {
    @DexIgnore
    public static /* final */ l8 c;
    @DexIgnore
    public static /* final */ /* synthetic */ l8[] d;
    @DexIgnore
    public /* final */ short b;

    /*
    static {
        l8 l8Var = new l8("ACTIVITY_FILE", 0, ob.ACTIVITY_FILE.b);
        c = l8Var;
        d = new l8[]{l8Var, new l8("HARDWARE_LOG", 1, ob.HARDWARE_LOG.b)};
    }
    */

    @DexIgnore
    public l8(String str, int i, short s) {
        this.b = (short) s;
    }

    @DexIgnore
    public static l8 valueOf(String str) {
        return (l8) Enum.valueOf(l8.class, str);
    }

    @DexIgnore
    public static l8[] values() {
        return (l8[]) d.clone();
    }
}
