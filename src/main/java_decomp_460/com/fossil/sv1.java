package com.fossil;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.imagefilters.EInkImageFactory;
import com.fossil.imagefilters.Format;
import com.fossil.vv1;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sv1 extends mv1 {
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public /* final */ vb0 e;
    @DexIgnore
    public /* final */ vv1 f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public uv1 h;
    @DexIgnore
    public tv1 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final sv1 a(JSONObject jSONObject, cc0[] cc0Arr) {
            try {
                vv1.a aVar = vv1.d;
                String string = jSONObject.getString("name");
                pq7.b(string, "jsonObject.getString(UIScriptConstant.NAME)");
                vv1 a2 = aVar.a(string);
                if (a2 != null) {
                    switch (wb0.f3911a[a2.ordinal()]) {
                        case 1:
                            return new aw1(jSONObject, cc0Arr);
                        case 2:
                            return new zv1(jSONObject, cc0Arr);
                        case 3:
                            return new wv1(jSONObject, cc0Arr);
                        case 4:
                            return new rv1(jSONObject, cc0Arr);
                        case 5:
                            return new yv1(jSONObject, cc0Arr);
                        case 6:
                            return new ov1(jSONObject, cc0Arr);
                        case 7:
                            return new qv1(jSONObject, cc0Arr);
                        case 8:
                            return new pv1(jSONObject, cc0Arr);
                        case 9:
                            return new xv1(jSONObject, cc0Arr);
                        default:
                            throw new al7();
                    }
                }
            } catch (Exception e) {
            }
            return null;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sv1(Parcel parcel) {
        super(parcel);
        boolean z = true;
        this.e = vb0.COMP;
        this.f = vv1.values()[parcel.readInt()];
        this.g = parcel.readByte() != ((byte) 1) ? false : z;
        Parcelable readParcelable = parcel.readParcelable(uv1.class.getClassLoader());
        if (readParcelable != null) {
            this.h = (uv1) readParcelable;
            this.i = (tv1) parcel.readParcelable(gw1.class.getClassLoader());
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ sv1(vv1 vv1, jv1 jv1, kv1 kv1, boolean z, uv1 uv1, tv1 tv1, int i2) {
        super(g80.e(0, 1), jv1, kv1);
        z = (i2 & 8) != 0 ? false : z;
        uv1 = (i2 & 16) != 0 ? new uv1(cw1.DEFAULT) : uv1;
        tv1 = (i2 & 32) != 0 ? null : tv1;
        this.e = vb0.COMP;
        this.f = vv1;
        this.g = z;
        this.h = uv1;
        if (!z) {
            this.i = tv1;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ sv1(JSONObject jSONObject, cc0[] cc0Arr, String str, int i2) throws IllegalArgumentException {
        super(jSONObject, (i2 & 4) != 0 ? null : str);
        cc0 cc0;
        byte[] bArr;
        tv1 tv1 = null;
        this.e = vb0.COMP;
        try {
            vv1.a aVar = vv1.d;
            String string = jSONObject.getString("name");
            pq7.b(string, "jsonObject.getString(UIScriptConstant.NAME)");
            vv1 a2 = aVar.a(string);
            if (a2 != null) {
                this.f = a2;
                this.g = jSONObject.optBoolean("goal_ring", false);
                uv1 a3 = uv1.CREATOR.a(jSONObject);
                this.h = a3 == null ? new uv1(cw1.DEFAULT) : a3;
                Object opt = jSONObject.opt("bg");
                if (opt instanceof String) {
                    int length = cc0Arr.length;
                    int i3 = 0;
                    while (true) {
                        if (i3 >= length) {
                            cc0 = null;
                            break;
                        }
                        cc0 = cc0Arr[i3];
                        if (pq7.a(opt, cc0.b)) {
                            break;
                        }
                        i3++;
                    }
                    if (!(cc0 == null || (bArr = cc0.c) == null)) {
                        Bitmap decode = EInkImageFactory.decode(bArr, Format.RLE);
                        pq7.b(decode, "EInkImageFactory.decode(imageData, Format.RLE)");
                        tv1 = new tv1((String) opt, cy1.b(decode, null, 1, null));
                        decode.recycle();
                    }
                }
                this.i = tv1;
                return;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        } catch (Exception e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    @DexIgnore
    public final cc0 a(String str) {
        tv1 tv1 = this.i;
        if (tv1 == null) {
            return null;
        }
        lv1 actualSize = c().toActualSize(240, 240);
        if (str == null) {
            str = tv1.getName();
        }
        return tv1.a(actualSize, str);
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public vb0 a() {
        return this.e;
    }

    @DexIgnore
    public final JSONObject b(String str) {
        JSONObject put = super.d().put("name", this.f.a()).put("goal_ring", this.g).put("data", e());
        pq7.b(put, "super.getUIScript()\n    \u2026nt.DATA, getDataScript())");
        JSONObject c = gy1.c(put, this.h.a());
        tv1 tv1 = this.i;
        if (tv1 != null) {
            if (str == null) {
                str = tv1.getName();
            }
            c.put("bg", str);
        }
        return c;
    }

    @DexIgnore
    @Override // java.lang.Object, com.fossil.mv1, com.fossil.mv1
    public abstract /* synthetic */ nx1 clone();

    @DexIgnore
    @Override // com.fossil.mv1
    public JSONObject d() {
        tv1 tv1 = this.i;
        return b(tv1 != null ? tv1.getName() : null);
    }

    @DexIgnore
    public Object e() {
        return null;
    }

    @DexIgnore
    public final tv1 getBackgroundImage() {
        return this.i;
    }

    @DexIgnore
    public final vv1 getComplicationType() {
        return this.f;
    }

    @DexIgnore
    public final uv1 getTheme() {
        return this.h;
    }

    @DexIgnore
    public final boolean isPercentageCircleEnable() {
        return this.g;
    }

    @DexIgnore
    public sv1 setBackgroundImage(tv1 tv1) {
        if (tv1 == null) {
            tv1 = null;
        } else {
            this.g = false;
        }
        this.i = tv1;
        return this;
    }

    @DexIgnore
    public sv1 setPercentageCircleEnable(boolean z) {
        this.g = z;
        if (z) {
            this.i = null;
        }
        return this;
    }

    @DexIgnore
    public sv1 setTheme(uv1 uv1) {
        this.h = uv1;
        return this;
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeInt(this.f.ordinal());
        }
        if (parcel != null) {
            parcel.writeByte(this.g ? (byte) 1 : 0);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.h, i2);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.i, i2);
        }
    }
}
