package com.fossil;

import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uf0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ArrayList<ro0> f3576a; // = new ArrayList<>();
    @DexIgnore
    public long b; // = -1;
    @DexIgnore
    public Interpolator c;
    @DexIgnore
    public so0 d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ to0 f; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends to0 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f3577a; // = false;
        @DexIgnore
        public int b; // = 0;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.so0
        public void b(View view) {
            int i = this.b + 1;
            this.b = i;
            if (i == uf0.this.f3576a.size()) {
                so0 so0 = uf0.this.d;
                if (so0 != null) {
                    so0.b(null);
                }
                d();
            }
        }

        @DexIgnore
        @Override // com.fossil.so0, com.fossil.to0
        public void c(View view) {
            if (!this.f3577a) {
                this.f3577a = true;
                so0 so0 = uf0.this.d;
                if (so0 != null) {
                    so0.c(null);
                }
            }
        }

        @DexIgnore
        public void d() {
            this.b = 0;
            this.f3577a = false;
            uf0.this.b();
        }
    }

    @DexIgnore
    public void a() {
        if (this.e) {
            Iterator<ro0> it = this.f3576a.iterator();
            while (it.hasNext()) {
                it.next().b();
            }
            this.e = false;
        }
    }

    @DexIgnore
    public void b() {
        this.e = false;
    }

    @DexIgnore
    public uf0 c(ro0 ro0) {
        if (!this.e) {
            this.f3576a.add(ro0);
        }
        return this;
    }

    @DexIgnore
    public uf0 d(ro0 ro0, ro0 ro02) {
        this.f3576a.add(ro0);
        ro02.h(ro0.c());
        this.f3576a.add(ro02);
        return this;
    }

    @DexIgnore
    public uf0 e(long j) {
        if (!this.e) {
            this.b = j;
        }
        return this;
    }

    @DexIgnore
    public uf0 f(Interpolator interpolator) {
        if (!this.e) {
            this.c = interpolator;
        }
        return this;
    }

    @DexIgnore
    public uf0 g(so0 so0) {
        if (!this.e) {
            this.d = so0;
        }
        return this;
    }

    @DexIgnore
    public void h() {
        if (!this.e) {
            Iterator<ro0> it = this.f3576a.iterator();
            while (it.hasNext()) {
                ro0 next = it.next();
                long j = this.b;
                if (j >= 0) {
                    next.d(j);
                }
                Interpolator interpolator = this.c;
                if (interpolator != null) {
                    next.e(interpolator);
                }
                if (this.d != null) {
                    next.f(this.f);
                }
                next.j();
            }
            this.e = true;
        }
    }
}
