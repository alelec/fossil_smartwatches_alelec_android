package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.wx3;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class by3 implements wx3.c {
    @DexIgnore
    public static /* final */ Parcelable.Creator<by3> CREATOR; // = new a();
    @DexIgnore
    public /* final */ long b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<by3> {
        @DexIgnore
        /* renamed from: a */
        public by3 createFromParcel(Parcel parcel) {
            return new by3(parcel.readLong(), null);
        }

        @DexIgnore
        /* renamed from: b */
        public by3[] newArray(int i) {
            return new by3[i];
        }
    }

    @DexIgnore
    public by3(long j) {
        this.b = j;
    }

    @DexIgnore
    public /* synthetic */ by3(long j, a aVar) {
        this(j);
    }

    @DexIgnore
    public static by3 a(long j) {
        return new by3(j);
    }

    @DexIgnore
    @Override // com.fossil.wx3.c
    public boolean S(long j) {
        return j >= this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof by3)) {
            return false;
        }
        return this.b == ((by3) obj).b;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.b)});
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.b);
    }
}
