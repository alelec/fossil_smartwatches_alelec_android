package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class es5 implements MembersInjector<cs5> {
    @DexIgnore
    public static void a(cs5 cs5, ApiServiceV2 apiServiceV2) {
        cs5.b = apiServiceV2;
    }

    @DexIgnore
    public static void b(cs5 cs5, DianaAppSettingRepository dianaAppSettingRepository) {
        cs5.e = dianaAppSettingRepository;
    }

    @DexIgnore
    public static void c(cs5 cs5, LocationSource locationSource) {
        cs5.c = locationSource;
    }

    @DexIgnore
    public static void d(cs5 cs5, PortfolioApp portfolioApp) {
        cs5.f645a = portfolioApp;
    }

    @DexIgnore
    public static void e(cs5 cs5, on5 on5) {
        cs5.f = on5;
    }

    @DexIgnore
    public static void f(cs5 cs5, UserRepository userRepository) {
        cs5.d = userRepository;
    }
}
