package com.fossil;

import com.bumptech.glide.load.ImageHeaderParser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jg1 implements ImageHeaderParser {
    @DexIgnore
    @Override // com.bumptech.glide.load.ImageHeaderParser
    public ImageHeaderParser.ImageType a(ByteBuffer byteBuffer) {
        return ImageHeaderParser.ImageType.UNKNOWN;
    }

    @DexIgnore
    @Override // com.bumptech.glide.load.ImageHeaderParser
    public ImageHeaderParser.ImageType b(InputStream inputStream) {
        return ImageHeaderParser.ImageType.UNKNOWN;
    }

    @DexIgnore
    @Override // com.bumptech.glide.load.ImageHeaderParser
    public int c(InputStream inputStream, od1 od1) throws IOException {
        int g = new eq0(inputStream).g("Orientation", 1);
        if (g == 0) {
            return -1;
        }
        return g;
    }
}
