package com.fossil;

import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.facebook.share.internal.VideoUploader;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityIntensities;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sk5 {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static /* final */ a c; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ on5 f3274a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final int a(long j) {
            if (j < ((long) 70)) {
                return 0;
            }
            return j < ((long) ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL) ? 1 : 2;
        }

        @DexIgnore
        public final String b(String str, MFSleepSession mFSleepSession) {
            pq7.c(str, ButtonService.USER_ID);
            pq7.c(mFSleepSession, "sleepSession");
            String value = oh5.values()[mFSleepSession.getSource()].getValue();
            pq7.b(value, "FitnessSourceType.values\u2026leepSession.source].value");
            if (value != null) {
                String lowerCase = value.toLowerCase();
                pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                return str + ":" + lowerCase + ":" + mFSleepSession.getRealEndTime();
            }
            throw new il7("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final int c(GoalTrackingSummary goalTrackingSummary) {
            if (goalTrackingSummary != null) {
                return goalTrackingSummary.getGoalTarget();
            }
            return 8;
        }

        @DexIgnore
        public final int d(ActivitySummary activitySummary, rh5 rh5) {
            pq7.c(rh5, "goalType");
            int i = rk5.f3122a[rh5.ordinal()];
            if (i == 1) {
                return activitySummary != null ? activitySummary.getStepGoal() : VideoUploader.RETRY_DELAY_UNIT_MS;
            }
            if (i != 2) {
                return i != 3 ? VideoUploader.RETRY_DELAY_UNIT_MS : activitySummary != null ? activitySummary.getCaloriesGoal() : ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL;
            }
            if (activitySummary != null) {
                return activitySummary.getActiveTimeGoal();
            }
            return 30;
        }

        @DexIgnore
        public final int e(MFSleepDay mFSleepDay) {
            if (mFSleepDay != null) {
                return mFSleepDay.getGoalMinutes();
            }
            return 480;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.helper.FitnessHelper$getCurrentSteps$2", f = "FitnessHelper.kt", l = {69, 80}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super Float>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $includeRealTimeStep;
        @DexIgnore
        public float F$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(sk5 sk5, Date date, boolean z, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sk5;
            this.$date = date;
            this.$includeRealTimeStep = z;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$date, this.$includeRealTimeStep, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Float> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0097 A[SYNTHETIC, Splitter:B:19:0x0097] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00f1  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
            // Method dump skipped, instructions count: 263
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.sk5.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = sk5.class.getSimpleName();
        pq7.b(simpleName, "FitnessHelper::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public sk5(on5 on5) {
        pq7.c(on5, "mSharedPreferencesManager");
        this.f3274a = on5;
    }

    @DexIgnore
    public final List<ActivitySample> b(List<ActivitySample> list, String str) {
        pq7.c(list, "activitySamples");
        pq7.c(str, ButtonService.USER_ID);
        long d = d(new Date());
        long j = 0;
        long j2 = 0;
        for (ActivitySample activitySample : list) {
            DateTime component4 = activitySample.component4();
            j += (long) activitySample.component5();
            j2 = component4.getMillis();
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "addRealTimeStepToActivitySample - steps=" + j + ", realTimeSteps=" + d);
        if (j < d) {
            try {
                ActivityIntensities activityIntensities = new ActivityIntensities();
                double d2 = (double) (d - j);
                activityIntensities.setIntensity(d2);
                long j3 = j2 + 1;
                Date date = new Date(j3);
                DateTime dateTime = new DateTime(j3);
                DateTime dateTime2 = new DateTime(j2 + ((long) 100));
                int c0 = lk5.c0();
                String value = oh5.Device.getValue();
                pq7.b(value, "FitnessSourceType.Device.value");
                list.add(new ActivitySample(str, date, dateTime, dateTime2, d2, 0.0d, 0.0d, 0, activityIntensities, c0, value, j3, j3, j3));
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = b;
                local2.e(str3, "addRealTimeStepToActivitySample - e=" + e);
                e.printStackTrace();
            }
        }
        return list;
    }

    @DexIgnore
    public final Object c(Date date, boolean z, qn7<? super Float> qn7) {
        return eu7.g(bw7.b(), new b(this, date, z, null), qn7);
    }

    @DexIgnore
    public final long d(Date date) {
        long j;
        String[] g;
        pq7.c(date, "date");
        String L = this.f3274a.L();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "getRealTimeStep - data=" + L + ", date=" + lk5.z(date));
        if (!TextUtils.isEmpty(L) && (g = i68.g(L, LocaleConverter.LOCALE_DELIMITER)) != null && g.length == 2) {
            try {
                if (lk5.m0(date, new Date(Long.parseLong(g[0])))) {
                    j = Long.parseLong(g[1]);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = b;
                    local2.d(str2, "XXX- getRealTimeStep - date=" + lk5.z(date) + " steps " + j);
                    return j;
                }
            } catch (Exception e) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = b;
                local3.e(str3, "getRealTimeStep - e=" + e);
                e.printStackTrace();
            }
        }
        j = 0;
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        String str22 = b;
        local22.d(str22, "XXX- getRealTimeStep - date=" + lk5.z(date) + " steps " + j);
        return j;
    }

    @DexIgnore
    public final void e(Date date, long j) {
        pq7.c(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "saveRealTimeStep - date=" + date + ", steps=" + j);
        if (j >= 0) {
            String L = this.f3274a.L();
            if (!TextUtils.isEmpty(L)) {
                String[] g = i68.g(L, LocaleConverter.LOCALE_DELIMITER);
                if (g != null && g.length == 2) {
                    try {
                        long parseLong = Long.parseLong(g[0]);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = b;
                        local2.d(str2, "saveRealTimeStep - lastSampleRawTimeStamp=" + date + ", savedRealTimeStepTimeStamp=" + new Date(parseLong));
                        if (lk5.m0(new Date(parseLong), date)) {
                            on5 on5 = this.f3274a;
                            on5.L1(String.valueOf(date.getTime()) + LocaleConverter.LOCALE_DELIMITER + j);
                            return;
                        }
                        FLogger.INSTANCE.getLocal().d(b, "saveRealTimeStep - Different date, clear realTimeStepStamp");
                        on5 on52 = this.f3274a;
                        on52.L1(String.valueOf(date.getTime()) + LocaleConverter.LOCALE_DELIMITER + j);
                    } catch (Exception e) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str3 = b;
                        local3.e(str3, "saveRealTimeStep - e=" + e);
                        e.printStackTrace();
                    }
                }
            } else {
                on5 on53 = this.f3274a;
                on53.L1(String.valueOf(date.getTime()) + LocaleConverter.LOCALE_DELIMITER + j);
            }
        }
    }
}
