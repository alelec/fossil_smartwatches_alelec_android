package com.fossil;

import com.fossil.s32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class y12 implements s32.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ b22 f4226a;
    @DexIgnore
    public /* final */ v02 b;
    @DexIgnore
    public /* final */ Iterable c;
    @DexIgnore
    public /* final */ h02 d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public y12(b22 b22, v02 v02, Iterable iterable, h02 h02, int i) {
        this.f4226a = b22;
        this.b = v02;
        this.c = iterable;
        this.d = h02;
        this.e = i;
    }

    @DexIgnore
    public static s32.a b(b22 b22, v02 v02, Iterable iterable, h02 h02, int i) {
        return new y12(b22, v02, iterable, h02, i);
    }

    @DexIgnore
    @Override // com.fossil.s32.a
    public Object a() {
        return b22.c(this.f4226a, this.b, this.c, this.d, this.e);
    }
}
