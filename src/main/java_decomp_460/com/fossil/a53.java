package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a53 implements xw2<z43> {
    @DexIgnore
    public static a53 c; // = new a53();
    @DexIgnore
    public /* final */ xw2<z43> b;

    @DexIgnore
    public a53() {
        this(ww2.b(new c53()));
    }

    @DexIgnore
    public a53(xw2<z43> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((z43) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ z43 zza() {
        return this.b.zza();
    }
}
