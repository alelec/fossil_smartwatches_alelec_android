package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.fossil.blesdk.device.DeviceImplementation$streamingEventListener$1$1", f = "DeviceImplementation.kt", l = {}, m = "invokeSuspend")
public final class ae extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public iv7 b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ /* synthetic */ yw d;
    @DexIgnore
    public /* final */ /* synthetic */ c2 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ae(yw ywVar, c2 c2Var, qn7 qn7) {
        super(2, qn7);
        this.d = ywVar;
        this.e = c2Var;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        ae aeVar = new ae(this.d, this.e, qn7);
        aeVar.b = (iv7) obj;
        return aeVar;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        ae aeVar = new ae(this.d, this.e, qn7);
        aeVar.b = iv7;
        return aeVar.invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        yn7.d();
        if (this.c == 0) {
            el7.b(obj);
            e60 e60 = this.d.b;
            e60.h.a(e60, this.e);
            return tl7.f3441a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
