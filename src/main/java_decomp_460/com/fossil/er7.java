package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class er7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ fr7 f982a;

    /*
    static {
        fr7 fr7;
        try {
            fr7 = (fr7) Class.forName("kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            fr7 = null;
        }
        if (fr7 == null) {
            fr7 = new fr7();
        }
        f982a = fr7;
    }
    */

    @DexIgnore
    public static gs7 a(nq7 nq7) {
        f982a.a(nq7);
        return nq7;
    }

    @DexIgnore
    public static es7 b(Class cls) {
        return f982a.b(cls);
    }

    @DexIgnore
    public static fs7 c(Class cls, String str) {
        return f982a.c(cls, str);
    }

    @DexIgnore
    public static is7 d(rq7 rq7) {
        f982a.d(rq7);
        return rq7;
    }

    @DexIgnore
    public static js7 e(sq7 sq7) {
        f982a.e(sq7);
        return sq7;
    }

    @DexIgnore
    public static ls7 f(wq7 wq7) {
        f982a.f(wq7);
        return wq7;
    }

    @DexIgnore
    public static ms7 g(xq7 xq7) {
        f982a.g(xq7);
        return xq7;
    }

    @DexIgnore
    public static String h(mq7 mq7) {
        return f982a.h(mq7);
    }

    @DexIgnore
    public static String i(qq7 qq7) {
        return f982a.i(qq7);
    }
}
