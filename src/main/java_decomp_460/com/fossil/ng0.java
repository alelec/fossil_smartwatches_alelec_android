package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import com.fossil.cg0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ng0 extends cg0 implements SubMenu {
    @DexIgnore
    public cg0 B;
    @DexIgnore
    public eg0 C;

    @DexIgnore
    public ng0(Context context, cg0 cg0, eg0 eg0) {
        super(context);
        this.B = cg0;
        this.C = eg0;
    }

    @DexIgnore
    @Override // com.fossil.cg0
    public cg0 F() {
        return this.B.F();
    }

    @DexIgnore
    @Override // com.fossil.cg0
    public boolean H() {
        return this.B.H();
    }

    @DexIgnore
    @Override // com.fossil.cg0
    public boolean I() {
        return this.B.I();
    }

    @DexIgnore
    @Override // com.fossil.cg0
    public boolean J() {
        return this.B.J();
    }

    @DexIgnore
    @Override // com.fossil.cg0
    public void V(cg0.a aVar) {
        this.B.V(aVar);
    }

    @DexIgnore
    @Override // com.fossil.cg0
    public boolean f(eg0 eg0) {
        return this.B.f(eg0);
    }

    @DexIgnore
    public MenuItem getItem() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.cg0
    public boolean h(cg0 cg0, MenuItem menuItem) {
        return super.h(cg0, menuItem) || this.B.h(cg0, menuItem);
    }

    @DexIgnore
    public Menu i0() {
        return this.B;
    }

    @DexIgnore
    @Override // com.fossil.cg0
    public boolean m(eg0 eg0) {
        return this.B.m(eg0);
    }

    @DexIgnore
    @Override // com.fossil.cg0
    public void setGroupDividerEnabled(boolean z) {
        this.B.setGroupDividerEnabled(z);
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setHeaderIcon(int i) {
        super.Y(i);
        return this;
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setHeaderIcon(Drawable drawable) {
        super.Z(drawable);
        return this;
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setHeaderTitle(int i) {
        super.b0(i);
        return this;
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setHeaderTitle(CharSequence charSequence) {
        super.c0(charSequence);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderView(View view) {
        super.d0(view);
        return this;
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setIcon(int i) {
        this.C.setIcon(i);
        return this;
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setIcon(Drawable drawable) {
        this.C.setIcon(drawable);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.cg0
    public void setQwertyMode(boolean z) {
        this.B.setQwertyMode(z);
    }

    @DexIgnore
    @Override // com.fossil.cg0
    public String v() {
        eg0 eg0 = this.C;
        int itemId = eg0 != null ? eg0.getItemId() : 0;
        if (itemId == 0) {
            return null;
        }
        return super.v() + ":" + itemId;
    }
}
