package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zn extends qq7 implements rp7<fs, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ lp b;
    @DexIgnore
    public /* final */ /* synthetic */ gp7 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zn(lp lpVar, gp7 gp7) {
        super(1);
        this.b = lpVar;
        this.c = gp7;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(fs fsVar) {
        fs fsVar2 = fsVar;
        lp lpVar = this.b;
        lpVar.m++;
        lpVar.v = nr.a(lpVar.v, null, zq.I.a(fsVar2.v), fsVar2.v, null, 9);
        mw mwVar = fsVar2.v;
        lw lwVar = mwVar.d;
        if (lwVar == lw.g || lwVar == lw.o || mwVar.e.d.b == f7.GATT_NULL) {
            this.b.o(fsVar2.v);
        } else {
            Short valueOf = fsVar2 instanceof dv ? Short.valueOf(((dv) fsVar2).L) : fsVar2 instanceof zs ? Short.valueOf(((zs) fsVar2).F) : null;
            if (valueOf != null) {
                lp.i(this.b, new bv(valueOf.shortValue(), this.b.w, 0, 4), new bn(this, fsVar2), new nn(this), null, null, null, 56, null);
            } else {
                this.c.invoke();
            }
        }
        return tl7.f3441a;
    }
}
