package com.fossil;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.WritableByteChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface j48 extends a58, WritableByteChannel {
    @DexIgnore
    j48 E(String str) throws IOException;

    @DexIgnore
    j48 J(byte[] bArr, int i, int i2) throws IOException;

    @DexIgnore
    long N(c58 c58) throws IOException;

    @DexIgnore
    j48 O(long j) throws IOException;

    @DexIgnore
    j48 Z(byte[] bArr) throws IOException;

    @DexIgnore
    j48 a0(l48 l48) throws IOException;

    @DexIgnore
    i48 d();

    @DexIgnore
    @Override // com.fossil.a58, java.io.Flushable
    void flush() throws IOException;

    @DexIgnore
    j48 k0(long j) throws IOException;

    @DexIgnore
    OutputStream l0();

    @DexIgnore
    j48 n(int i) throws IOException;

    @DexIgnore
    j48 p(int i) throws IOException;

    @DexIgnore
    j48 v(int i) throws IOException;

    @DexIgnore
    j48 x() throws IOException;
}
