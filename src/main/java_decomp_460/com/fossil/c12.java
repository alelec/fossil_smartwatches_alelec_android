package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c12 {
    @DexIgnore
    public static void a(String str, String str2, Object obj) {
        Log.d(d(str), String.format(str2, obj));
    }

    @DexIgnore
    public static void b(String str, String str2, Object... objArr) {
        Log.d(d(str), String.format(str2, objArr));
    }

    @DexIgnore
    public static void c(String str, String str2, Throwable th) {
        Log.e(d(str), str2, th);
    }

    @DexIgnore
    public static String d(String str) {
        return "TransportRuntime." + str;
    }

    @DexIgnore
    public static void e(String str, String str2) {
        Log.i(d(str), str2);
    }

    @DexIgnore
    public static void f(String str, String str2, Object obj) {
        Log.w(d(str), String.format(str2, obj));
    }
}
