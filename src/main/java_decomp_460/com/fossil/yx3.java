package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Paint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yx3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ xx3 f4384a;
    @DexIgnore
    public /* final */ xx3 b;
    @DexIgnore
    public /* final */ xx3 c;
    @DexIgnore
    public /* final */ xx3 d;
    @DexIgnore
    public /* final */ xx3 e;
    @DexIgnore
    public /* final */ xx3 f;
    @DexIgnore
    public /* final */ xx3 g;
    @DexIgnore
    public /* final */ Paint h;

    @DexIgnore
    public yx3(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(nz3.c(context, jw3.materialCalendarStyle, dy3.class.getCanonicalName()), tw3.MaterialCalendar);
        this.f4384a = xx3.a(context, obtainStyledAttributes.getResourceId(tw3.MaterialCalendar_dayStyle, 0));
        this.g = xx3.a(context, obtainStyledAttributes.getResourceId(tw3.MaterialCalendar_dayInvalidStyle, 0));
        this.b = xx3.a(context, obtainStyledAttributes.getResourceId(tw3.MaterialCalendar_daySelectedStyle, 0));
        this.c = xx3.a(context, obtainStyledAttributes.getResourceId(tw3.MaterialCalendar_dayTodayStyle, 0));
        ColorStateList a2 = oz3.a(context, obtainStyledAttributes, tw3.MaterialCalendar_rangeFillColor);
        this.d = xx3.a(context, obtainStyledAttributes.getResourceId(tw3.MaterialCalendar_yearStyle, 0));
        this.e = xx3.a(context, obtainStyledAttributes.getResourceId(tw3.MaterialCalendar_yearSelectedStyle, 0));
        this.f = xx3.a(context, obtainStyledAttributes.getResourceId(tw3.MaterialCalendar_yearTodayStyle, 0));
        Paint paint = new Paint();
        this.h = paint;
        paint.setColor(a2.getDefaultColor());
        obtainStyledAttributes.recycle();
    }
}
