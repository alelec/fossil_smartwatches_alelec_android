package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xx2 extends sx2<E> {
    @DexIgnore
    public /* final */ transient int d;
    @DexIgnore
    public /* final */ transient int e;
    @DexIgnore
    public /* final */ /* synthetic */ sx2 zzc;

    @DexIgnore
    public xx2(sx2 sx2, int i, int i2) {
        this.zzc = sx2;
        this.d = i;
        this.e = i2;
    }

    @DexIgnore
    @Override // java.util.List
    public final E get(int i) {
        sw2.a(i, this.e);
        return (E) this.zzc.get(this.d + i);
    }

    @DexIgnore
    public final int size() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.sx2
    /* renamed from: zza */
    public final sx2<E> subList(int i, int i2) {
        sw2.e(i, i2, this.e);
        sx2 sx2 = this.zzc;
        int i3 = this.d;
        return (sx2) sx2.subList(i + i3, i3 + i2);
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final Object[] zze() {
        return this.zzc.zze();
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final int zzf() {
        return this.zzc.zzf() + this.d;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final int zzg() {
        return this.zzc.zzf() + this.d + this.e;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final boolean zzh() {
        return true;
    }
}
