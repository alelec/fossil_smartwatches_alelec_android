package com.fossil;

import android.graphics.Rect;
import android.os.Build;
import android.util.Log;
import android.view.WindowInsets;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vo0 {
    @DexIgnore
    public static /* final */ vo0 b; // = new a().a().a().b().c();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ i f3797a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ d f3798a;

        @DexIgnore
        public a() {
            int i = Build.VERSION.SDK_INT;
            if (i >= 29) {
                this.f3798a = new c();
            } else if (i >= 20) {
                this.f3798a = new b();
            } else {
                this.f3798a = new d();
            }
        }

        @DexIgnore
        public a(vo0 vo0) {
            int i = Build.VERSION.SDK_INT;
            if (i >= 29) {
                this.f3798a = new c(vo0);
            } else if (i >= 20) {
                this.f3798a = new b(vo0);
            } else {
                this.f3798a = new d(vo0);
            }
        }

        @DexIgnore
        public vo0 a() {
            return this.f3798a.a();
        }

        @DexIgnore
        public a b(ql0 ql0) {
            this.f3798a.b(ql0);
            return this;
        }

        @DexIgnore
        public a c(ql0 ql0) {
            this.f3798a.c(ql0);
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends d {
        @DexIgnore
        public static Field c;
        @DexIgnore
        public static boolean d;
        @DexIgnore
        public static Constructor<WindowInsets> e;
        @DexIgnore
        public static boolean f;
        @DexIgnore
        public WindowInsets b;

        @DexIgnore
        public b() {
            this.b = d();
        }

        @DexIgnore
        public b(vo0 vo0) {
            this.b = vo0.n();
        }

        @DexIgnore
        public static WindowInsets d() {
            if (!d) {
                try {
                    c = WindowInsets.class.getDeclaredField("CONSUMED");
                } catch (ReflectiveOperationException e2) {
                    Log.i("WindowInsetsCompat", "Could not retrieve WindowInsets.CONSUMED field", e2);
                }
                d = true;
            }
            Field field = c;
            if (field != null) {
                try {
                    WindowInsets windowInsets = (WindowInsets) field.get(null);
                    if (windowInsets != null) {
                        return new WindowInsets(windowInsets);
                    }
                } catch (ReflectiveOperationException e3) {
                    Log.i("WindowInsetsCompat", "Could not get value from WindowInsets.CONSUMED field", e3);
                }
            }
            if (!f) {
                try {
                    e = WindowInsets.class.getConstructor(Rect.class);
                } catch (ReflectiveOperationException e4) {
                    Log.i("WindowInsetsCompat", "Could not retrieve WindowInsets(Rect) constructor", e4);
                }
                f = true;
            }
            Constructor<WindowInsets> constructor = e;
            if (constructor != null) {
                try {
                    return constructor.newInstance(new Rect());
                } catch (ReflectiveOperationException e5) {
                    Log.i("WindowInsetsCompat", "Could not invoke WindowInsets(Rect) constructor", e5);
                }
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.vo0.d
        public vo0 a() {
            return vo0.o(this.b);
        }

        @DexIgnore
        @Override // com.fossil.vo0.d
        public void c(ql0 ql0) {
            WindowInsets windowInsets = this.b;
            if (windowInsets != null) {
                this.b = windowInsets.replaceSystemWindowInsets(ql0.f2995a, ql0.b, ql0.c, ql0.d);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends d {
        @DexIgnore
        public /* final */ WindowInsets.Builder b;

        @DexIgnore
        public c() {
            this.b = new WindowInsets.Builder();
        }

        @DexIgnore
        public c(vo0 vo0) {
            WindowInsets n = vo0.n();
            this.b = n != null ? new WindowInsets.Builder(n) : new WindowInsets.Builder();
        }

        @DexIgnore
        @Override // com.fossil.vo0.d
        public vo0 a() {
            return vo0.o(this.b.build());
        }

        @DexIgnore
        @Override // com.fossil.vo0.d
        public void b(ql0 ql0) {
            this.b.setStableInsets(ql0.b());
        }

        @DexIgnore
        @Override // com.fossil.vo0.d
        public void c(ql0 ql0) {
            this.b.setSystemWindowInsets(ql0.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ vo0 f3799a;

        @DexIgnore
        public d() {
            this(new vo0((vo0) null));
        }

        @DexIgnore
        public d(vo0 vo0) {
            this.f3799a = vo0;
        }

        @DexIgnore
        public vo0 a() {
            return this.f3799a;
        }

        @DexIgnore
        public void b(ql0 ql0) {
        }

        @DexIgnore
        public void c(ql0 ql0) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends i {
        @DexIgnore
        public /* final */ WindowInsets b;
        @DexIgnore
        public ql0 c;

        @DexIgnore
        public e(vo0 vo0, WindowInsets windowInsets) {
            super(vo0);
            this.c = null;
            this.b = windowInsets;
        }

        @DexIgnore
        public e(vo0 vo0, e eVar) {
            this(vo0, new WindowInsets(eVar.b));
        }

        @DexIgnore
        @Override // com.fossil.vo0.i
        public final ql0 f() {
            if (this.c == null) {
                this.c = ql0.a(this.b.getSystemWindowInsetLeft(), this.b.getSystemWindowInsetTop(), this.b.getSystemWindowInsetRight(), this.b.getSystemWindowInsetBottom());
            }
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.vo0.i
        public vo0 g(int i, int i2, int i3, int i4) {
            a aVar = new a(vo0.o(this.b));
            aVar.c(vo0.k(f(), i, i2, i3, i4));
            aVar.b(vo0.k(e(), i, i2, i3, i4));
            return aVar.a();
        }

        @DexIgnore
        @Override // com.fossil.vo0.i
        public boolean i() {
            return this.b.isRound();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends e {
        @DexIgnore
        public ql0 d; // = null;

        @DexIgnore
        public f(vo0 vo0, WindowInsets windowInsets) {
            super(vo0, windowInsets);
        }

        @DexIgnore
        public f(vo0 vo0, f fVar) {
            super(vo0, fVar);
        }

        @DexIgnore
        @Override // com.fossil.vo0.i
        public vo0 b() {
            return vo0.o(this.b.consumeStableInsets());
        }

        @DexIgnore
        @Override // com.fossil.vo0.i
        public vo0 c() {
            return vo0.o(this.b.consumeSystemWindowInsets());
        }

        @DexIgnore
        @Override // com.fossil.vo0.i
        public final ql0 e() {
            if (this.d == null) {
                this.d = ql0.a(this.b.getStableInsetLeft(), this.b.getStableInsetTop(), this.b.getStableInsetRight(), this.b.getStableInsetBottom());
            }
            return this.d;
        }

        @DexIgnore
        @Override // com.fossil.vo0.i
        public boolean h() {
            return this.b.isConsumed();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g extends f {
        @DexIgnore
        public g(vo0 vo0, WindowInsets windowInsets) {
            super(vo0, windowInsets);
        }

        @DexIgnore
        public g(vo0 vo0, g gVar) {
            super(vo0, gVar);
        }

        @DexIgnore
        @Override // com.fossil.vo0.i
        public vo0 a() {
            return vo0.o(this.b.consumeDisplayCutout());
        }

        @DexIgnore
        @Override // com.fossil.vo0.i
        public tn0 d() {
            return tn0.a(this.b.getDisplayCutout());
        }

        @DexIgnore
        @Override // com.fossil.vo0.i
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof g)) {
                return false;
            }
            return Objects.equals(this.b, ((g) obj).b);
        }

        @DexIgnore
        @Override // com.fossil.vo0.i
        public int hashCode() {
            return this.b.hashCode();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h extends g {
        @DexIgnore
        public h(vo0 vo0, WindowInsets windowInsets) {
            super(vo0, windowInsets);
        }

        @DexIgnore
        public h(vo0 vo0, h hVar) {
            super(vo0, hVar);
        }

        @DexIgnore
        @Override // com.fossil.vo0.e, com.fossil.vo0.i
        public vo0 g(int i, int i2, int i3, int i4) {
            return vo0.o(this.b.inset(i, i2, i3, i4));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ vo0 f3800a;

        @DexIgnore
        public i(vo0 vo0) {
            this.f3800a = vo0;
        }

        @DexIgnore
        public vo0 a() {
            return this.f3800a;
        }

        @DexIgnore
        public vo0 b() {
            return this.f3800a;
        }

        @DexIgnore
        public vo0 c() {
            return this.f3800a;
        }

        @DexIgnore
        public tn0 d() {
            return null;
        }

        @DexIgnore
        public ql0 e() {
            return ql0.e;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof i)) {
                return false;
            }
            i iVar = (i) obj;
            return i() == iVar.i() && h() == iVar.h() && kn0.a(f(), iVar.f()) && kn0.a(e(), iVar.e()) && kn0.a(d(), iVar.d());
        }

        @DexIgnore
        public ql0 f() {
            return ql0.e;
        }

        @DexIgnore
        public vo0 g(int i, int i2, int i3, int i4) {
            return vo0.b;
        }

        @DexIgnore
        public boolean h() {
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return kn0.b(Boolean.valueOf(i()), Boolean.valueOf(h()), f(), e(), d());
        }

        @DexIgnore
        public boolean i() {
            return false;
        }
    }

    @DexIgnore
    public vo0(WindowInsets windowInsets) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 29) {
            this.f3797a = new h(this, windowInsets);
        } else if (i2 >= 28) {
            this.f3797a = new g(this, windowInsets);
        } else if (i2 >= 21) {
            this.f3797a = new f(this, windowInsets);
        } else if (i2 >= 20) {
            this.f3797a = new e(this, windowInsets);
        } else {
            this.f3797a = new i(this);
        }
    }

    @DexIgnore
    public vo0(vo0 vo0) {
        if (vo0 != null) {
            i iVar = vo0.f3797a;
            if (Build.VERSION.SDK_INT >= 29 && (iVar instanceof h)) {
                this.f3797a = new h(this, (h) iVar);
            } else if (Build.VERSION.SDK_INT >= 28 && (iVar instanceof g)) {
                this.f3797a = new g(this, (g) iVar);
            } else if (Build.VERSION.SDK_INT >= 21 && (iVar instanceof f)) {
                this.f3797a = new f(this, (f) iVar);
            } else if (Build.VERSION.SDK_INT < 20 || !(iVar instanceof e)) {
                this.f3797a = new i(this);
            } else {
                this.f3797a = new e(this, (e) iVar);
            }
        } else {
            this.f3797a = new i(this);
        }
    }

    @DexIgnore
    public static ql0 k(ql0 ql0, int i2, int i3, int i4, int i5) {
        int max = Math.max(0, ql0.f2995a - i2);
        int max2 = Math.max(0, ql0.b - i3);
        int max3 = Math.max(0, ql0.c - i4);
        int max4 = Math.max(0, ql0.d - i5);
        return (max == i2 && max2 == i3 && max3 == i4 && max4 == i5) ? ql0 : ql0.a(max, max2, max3, max4);
    }

    @DexIgnore
    public static vo0 o(WindowInsets windowInsets) {
        pn0.d(windowInsets);
        return new vo0(windowInsets);
    }

    @DexIgnore
    public vo0 a() {
        return this.f3797a.a();
    }

    @DexIgnore
    public vo0 b() {
        return this.f3797a.b();
    }

    @DexIgnore
    public vo0 c() {
        return this.f3797a.c();
    }

    @DexIgnore
    public int d() {
        return h().d;
    }

    @DexIgnore
    public int e() {
        return h().f2995a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof vo0)) {
            return false;
        }
        return kn0.a(this.f3797a, ((vo0) obj).f3797a);
    }

    @DexIgnore
    public int f() {
        return h().c;
    }

    @DexIgnore
    public int g() {
        return h().b;
    }

    @DexIgnore
    public ql0 h() {
        return this.f3797a.f();
    }

    @DexIgnore
    public int hashCode() {
        i iVar = this.f3797a;
        if (iVar == null) {
            return 0;
        }
        return iVar.hashCode();
    }

    @DexIgnore
    public boolean i() {
        return !h().equals(ql0.e);
    }

    @DexIgnore
    public vo0 j(int i2, int i3, int i4, int i5) {
        return this.f3797a.g(i2, i3, i4, i5);
    }

    @DexIgnore
    public boolean l() {
        return this.f3797a.h();
    }

    @DexIgnore
    @Deprecated
    public vo0 m(int i2, int i3, int i4, int i5) {
        a aVar = new a(this);
        aVar.c(ql0.a(i2, i3, i4, i5));
        return aVar.a();
    }

    @DexIgnore
    public WindowInsets n() {
        i iVar = this.f3797a;
        if (iVar instanceof e) {
            return ((e) iVar).b;
        }
        return null;
    }
}
