package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dm2 extends Handler {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static fm2 f805a;

    @DexIgnore
    public dm2(Looper looper) {
        super(looper);
    }

    @DexIgnore
    public dm2(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }

    @DexIgnore
    public void a(Message message) {
        super.dispatchMessage(message);
    }

    @DexIgnore
    public final void b(Message message, long j) {
        fm2 fm2 = f805a;
        if (fm2 != null) {
            fm2.d(this, message, j);
        }
    }

    @DexIgnore
    public final void dispatchMessage(Message message) {
        fm2 fm2 = f805a;
        if (fm2 == null) {
            a(message);
            return;
        }
        Object b = fm2.b(this, message);
        try {
            a(message);
            fm2.c(this, message, b);
        } catch (Throwable th) {
            fm2.c(this, message, b);
            throw th;
        }
    }

    @DexIgnore
    public boolean sendMessageAtTime(Message message, long j) {
        b(message, j);
        return super.sendMessageAtTime(message, j);
    }
}
