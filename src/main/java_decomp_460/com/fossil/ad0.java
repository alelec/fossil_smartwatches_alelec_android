package com.fossil;

import android.content.Context;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ad0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static cd0 f251a;
    @DexIgnore
    public static bd0 b;
    @DexIgnore
    public static dd0 c;
    @DexIgnore
    public static /* final */ ad0 d; // = new ad0();

    @DexIgnore
    public final bd0 a() {
        if (b == null) {
            String c2 = cx1.f.c();
            Context a2 = id0.i.a();
            ft1 i = cx1.f.i();
            m80 m80 = m80.c;
            m80.a("ApiServiceManager", "build apiServiceV3, baseUrl=" + c2 + ", context=" + a2 + ", userCredential=" + String.valueOf(i), new Object[0]);
            if (!(c2 == null || a2 == null || i == null)) {
                ed0 ed0 = new ed0();
                ed0.b(c2 + "/v2/");
                File cacheDir = a2.getCacheDir();
                pq7.b(cacheDir, "context.cacheDir");
                ed0.a(cacheDir);
                ed0.c(new wc0(i));
                ed0.c(new yc0());
                ed0.c(new xc0());
                b = (bd0) ed0.d(bd0.class);
            }
        }
        return b;
    }

    @DexIgnore
    public final cd0 b() {
        if (f251a == null) {
            String c2 = cx1.f.c();
            Context a2 = id0.i.a();
            m80 m80 = m80.c;
            m80.a("ApiServiceManager", "build authApiService, baseUrl=" + c2 + ", context=" + a2, new Object[0]);
            if (!(c2 == null || a2 == null)) {
                ed0 ed0 = new ed0();
                ed0.b(c2 + "/v2.1/");
                File cacheDir = a2.getCacheDir();
                pq7.b(cacheDir, "context.cacheDir");
                ed0.a(cacheDir);
                ed0.c(new yc0());
                ed0.c(new xc0());
                f251a = (cd0) ed0.d(cd0.class);
            }
        }
        return f251a;
    }
}
