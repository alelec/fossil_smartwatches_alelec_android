package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ik0 {
    @DexIgnore
    public static /* final */ int alpha; // = 2130968707;
    @DexIgnore
    public static /* final */ int coordinatorLayoutStyle; // = 2130969006;
    @DexIgnore
    public static /* final */ int font; // = 2130969196;
    @DexIgnore
    public static /* final */ int fontProviderAuthority; // = 2130969198;
    @DexIgnore
    public static /* final */ int fontProviderCerts; // = 2130969199;
    @DexIgnore
    public static /* final */ int fontProviderFetchStrategy; // = 2130969200;
    @DexIgnore
    public static /* final */ int fontProviderFetchTimeout; // = 2130969201;
    @DexIgnore
    public static /* final */ int fontProviderPackage; // = 2130969202;
    @DexIgnore
    public static /* final */ int fontProviderQuery; // = 2130969203;
    @DexIgnore
    public static /* final */ int fontStyle; // = 2130969204;
    @DexIgnore
    public static /* final */ int fontVariationSettings; // = 2130969205;
    @DexIgnore
    public static /* final */ int fontWeight; // = 2130969206;
    @DexIgnore
    public static /* final */ int keylines; // = 2130969321;
    @DexIgnore
    public static /* final */ int layout_anchor; // = 2130969330;
    @DexIgnore
    public static /* final */ int layout_anchorGravity; // = 2130969331;
    @DexIgnore
    public static /* final */ int layout_behavior; // = 2130969333;
    @DexIgnore
    public static /* final */ int layout_dodgeInsetEdges; // = 2130969377;
    @DexIgnore
    public static /* final */ int layout_insetEdge; // = 2130969387;
    @DexIgnore
    public static /* final */ int layout_keyline; // = 2130969388;
    @DexIgnore
    public static /* final */ int statusBarBackground; // = 2130969702;
    @DexIgnore
    public static /* final */ int ttcIndex; // = 2130969843;
}
