package com.fossil;

import com.fossil.ix1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xx1<T> extends wx1<T> {
    @DexIgnore
    public /* final */ ix1.a e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xx1(byte b, byte b2, ry1 ry1) {
        super(b, b2, ry1);
        pq7.c(ry1, "version");
        this.e = ix1.a.CRC32C;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ xx1(byte b, byte b2, ry1 ry1, int i, kq7 kq7) {
        this(b, (i & 2) != 0 ? (byte) 255 : b2, (i & 4) != 0 ? new ry1(2, 0) : ry1);
    }

    @DexIgnore
    @Override // com.fossil.wx1
    public ix1.a c() {
        return this.e;
    }
}
