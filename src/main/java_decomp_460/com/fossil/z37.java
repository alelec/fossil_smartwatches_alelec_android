package com.fossil;

import android.annotation.TargetApi;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.portfolio.platform.PortfolioApp;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.util.Calendar;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.security.auth.x500.X500Principal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z37 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ KeyStore f4415a;
    @DexIgnore
    public static /* final */ z37 b;

    /*
    static {
        z37 z37 = new z37();
        b = z37;
        f4415a = z37.a();
    }
    */

    @DexIgnore
    public final KeyStore a() {
        KeyStore instance = KeyStore.getInstance("AndroidKeyStore");
        instance.load(null);
        pq7.b(instance, "mKeyStore");
        return instance;
    }

    @DexIgnore
    public final KeyPair b(String str) {
        pq7.c(str, "alias");
        KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
        Calendar instance2 = Calendar.getInstance();
        Calendar instance3 = Calendar.getInstance();
        instance3.add(1, 20);
        KeyPairGeneratorSpec.Builder serialNumber = new KeyPairGeneratorSpec.Builder(PortfolioApp.h0.c()).setAlias(str).setSerialNumber(BigInteger.ONE);
        KeyPairGeneratorSpec.Builder subject = serialNumber.setSubject(new X500Principal("CN=" + str + " CA Certificate"));
        pq7.b(instance2, GoalPhase.COLUMN_START_DATE);
        KeyPairGeneratorSpec.Builder startDate = subject.setStartDate(instance2.getTime());
        pq7.b(instance3, GoalPhase.COLUMN_END_DATE);
        KeyPairGeneratorSpec.Builder endDate = startDate.setEndDate(instance3.getTime());
        pq7.b(endDate, "KeyPairGeneratorSpec.Bui\u2026.setEndDate(endDate.time)");
        instance.initialize(endDate.build());
        KeyPair generateKeyPair = instance.generateKeyPair();
        pq7.b(generateKeyPair, "generator.generateKeyPair()");
        return generateKeyPair;
    }

    @DexIgnore
    @TargetApi(23)
    public final SecretKey c(String str) {
        KeyGenerator instance = KeyGenerator.getInstance("AES", "AndroidKeyStore");
        KeyGenParameterSpec build = new KeyGenParameterSpec.Builder(str, 3).setBlockModes("GCM").setEncryptionPaddings("NoPadding").build();
        pq7.b(build, "KeyGenParameterSpec.Buil\u2026\n                .build()");
        instance.init(build);
        SecretKey generateKey = instance.generateKey();
        pq7.b(generateKey, "keyGenerator.generateKey()");
        return generateKey;
    }

    @DexIgnore
    public final KeyPair d(String str) {
        pq7.c(str, "alias");
        PrivateKey privateKey = (PrivateKey) f4415a.getKey(str, null);
        Certificate certificate = f4415a.getCertificate(str);
        PublicKey publicKey = certificate != null ? certificate.getPublicKey() : null;
        if (privateKey == null || publicKey == null) {
            return null;
        }
        return new KeyPair(publicKey, privateKey);
    }

    @DexIgnore
    @TargetApi(23)
    public final SecretKey e(String str) {
        pq7.c(str, "aliasName");
        if (!f4415a.containsAlias(str)) {
            return c(str);
        }
        KeyStore.Entry entry = f4415a.getEntry(str, null);
        if (entry != null) {
            SecretKey secretKey = ((KeyStore.SecretKeyEntry) entry).getSecretKey();
            pq7.b(secretKey, "secretKeyEntry.secretKey");
            return secretKey;
        }
        throw new il7("null cannot be cast to non-null type java.security.KeyStore.SecretKeyEntry");
    }

    @DexIgnore
    @TargetApi(23)
    public final KeyStore.SecretKeyEntry f(String str) {
        pq7.c(str, "aliasName");
        KeyStore.Entry entry = f4415a.getEntry(str, null);
        if (entry != null) {
            return (KeyStore.SecretKeyEntry) entry;
        }
        throw new il7("null cannot be cast to non-null type java.security.KeyStore.SecretKeyEntry");
    }

    @DexIgnore
    public final void g(String str) {
        pq7.c(str, "alias");
        f4415a.deleteEntry(str);
    }
}
