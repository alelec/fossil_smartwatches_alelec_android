package com.fossil;

import android.content.res.AssetFileDescriptor;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Movie;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kh0 extends Resources {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Resources f1916a;

    @DexIgnore
    public kh0(Resources resources) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.f1916a = resources;
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public XmlResourceParser getAnimation(int i) throws Resources.NotFoundException {
        return this.f1916a.getAnimation(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public boolean getBoolean(int i) throws Resources.NotFoundException {
        return this.f1916a.getBoolean(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public int getColor(int i) throws Resources.NotFoundException {
        return this.f1916a.getColor(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public ColorStateList getColorStateList(int i) throws Resources.NotFoundException {
        return this.f1916a.getColorStateList(i);
    }

    @DexIgnore
    public Configuration getConfiguration() {
        return this.f1916a.getConfiguration();
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public float getDimension(int i) throws Resources.NotFoundException {
        return this.f1916a.getDimension(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public int getDimensionPixelOffset(int i) throws Resources.NotFoundException {
        return this.f1916a.getDimensionPixelOffset(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public int getDimensionPixelSize(int i) throws Resources.NotFoundException {
        return this.f1916a.getDimensionPixelSize(i);
    }

    @DexIgnore
    public DisplayMetrics getDisplayMetrics() {
        return this.f1916a.getDisplayMetrics();
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public Drawable getDrawable(int i) throws Resources.NotFoundException {
        return this.f1916a.getDrawable(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public Drawable getDrawable(int i, Resources.Theme theme) throws Resources.NotFoundException {
        return this.f1916a.getDrawable(i, theme);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public Drawable getDrawableForDensity(int i, int i2) throws Resources.NotFoundException {
        return this.f1916a.getDrawableForDensity(i, i2);
    }

    @DexIgnore
    public Drawable getDrawableForDensity(int i, int i2, Resources.Theme theme) {
        return this.f1916a.getDrawableForDensity(i, i2, theme);
    }

    @DexIgnore
    public float getFraction(int i, int i2, int i3) {
        return this.f1916a.getFraction(i, i2, i3);
    }

    @DexIgnore
    public int getIdentifier(String str, String str2, String str3) {
        return this.f1916a.getIdentifier(str, str2, str3);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public int[] getIntArray(int i) throws Resources.NotFoundException {
        return this.f1916a.getIntArray(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public int getInteger(int i) throws Resources.NotFoundException {
        return this.f1916a.getInteger(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public XmlResourceParser getLayout(int i) throws Resources.NotFoundException {
        return this.f1916a.getLayout(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public Movie getMovie(int i) throws Resources.NotFoundException {
        return this.f1916a.getMovie(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getQuantityString(int i, int i2) throws Resources.NotFoundException {
        return this.f1916a.getQuantityString(i, i2);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getQuantityString(int i, int i2, Object... objArr) throws Resources.NotFoundException {
        return this.f1916a.getQuantityString(i, i2, objArr);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public CharSequence getQuantityText(int i, int i2) throws Resources.NotFoundException {
        return this.f1916a.getQuantityText(i, i2);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getResourceEntryName(int i) throws Resources.NotFoundException {
        return this.f1916a.getResourceEntryName(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getResourceName(int i) throws Resources.NotFoundException {
        return this.f1916a.getResourceName(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getResourcePackageName(int i) throws Resources.NotFoundException {
        return this.f1916a.getResourcePackageName(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getResourceTypeName(int i) throws Resources.NotFoundException {
        return this.f1916a.getResourceTypeName(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getString(int i) throws Resources.NotFoundException {
        return this.f1916a.getString(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getString(int i, Object... objArr) throws Resources.NotFoundException {
        return this.f1916a.getString(i, objArr);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String[] getStringArray(int i) throws Resources.NotFoundException {
        return this.f1916a.getStringArray(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public CharSequence getText(int i) throws Resources.NotFoundException {
        return this.f1916a.getText(i);
    }

    @DexIgnore
    public CharSequence getText(int i, CharSequence charSequence) {
        return this.f1916a.getText(i, charSequence);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public CharSequence[] getTextArray(int i) throws Resources.NotFoundException {
        return this.f1916a.getTextArray(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public void getValue(int i, TypedValue typedValue, boolean z) throws Resources.NotFoundException {
        this.f1916a.getValue(i, typedValue, z);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public void getValue(String str, TypedValue typedValue, boolean z) throws Resources.NotFoundException {
        this.f1916a.getValue(str, typedValue, z);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public void getValueForDensity(int i, int i2, TypedValue typedValue, boolean z) throws Resources.NotFoundException {
        this.f1916a.getValueForDensity(i, i2, typedValue, z);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public XmlResourceParser getXml(int i) throws Resources.NotFoundException {
        return this.f1916a.getXml(i);
    }

    @DexIgnore
    public TypedArray obtainAttributes(AttributeSet attributeSet, int[] iArr) {
        return this.f1916a.obtainAttributes(attributeSet, iArr);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public TypedArray obtainTypedArray(int i) throws Resources.NotFoundException {
        return this.f1916a.obtainTypedArray(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public InputStream openRawResource(int i) throws Resources.NotFoundException {
        return this.f1916a.openRawResource(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public InputStream openRawResource(int i, TypedValue typedValue) throws Resources.NotFoundException {
        return this.f1916a.openRawResource(i, typedValue);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public AssetFileDescriptor openRawResourceFd(int i) throws Resources.NotFoundException {
        return this.f1916a.openRawResourceFd(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public void parseBundleExtra(String str, AttributeSet attributeSet, Bundle bundle) throws XmlPullParserException {
        this.f1916a.parseBundleExtra(str, attributeSet, bundle);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public void parseBundleExtras(XmlResourceParser xmlResourceParser, Bundle bundle) throws XmlPullParserException, IOException {
        this.f1916a.parseBundleExtras(xmlResourceParser, bundle);
    }

    @DexIgnore
    public void updateConfiguration(Configuration configuration, DisplayMetrics displayMetrics) {
        super.updateConfiguration(configuration, displayMetrics);
        Resources resources = this.f1916a;
        if (resources != null) {
            resources.updateConfiguration(configuration, displayMetrics);
        }
    }
}
