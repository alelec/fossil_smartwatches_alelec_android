package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.fossil.f57;
import com.fossil.x71;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ty4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements x71.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ImageView f3492a;
        @DexIgnore
        public /* final */ /* synthetic */ f57 b;
        @DexIgnore
        public /* final */ /* synthetic */ sy4 c;

        @DexIgnore
        public a(ImageView imageView, f57 f57, sy4 sy4) {
            this.f3492a = imageView;
            this.b = f57;
            this.c = sy4;
        }

        @DexIgnore
        @Override // com.fossil.x71.a
        public void a(Object obj) {
            pq7.c(obj, "data");
            x71.a.C0278a.c(this, obj);
        }

        @DexIgnore
        @Override // com.fossil.x71.a
        public void b(Object obj, Throwable th) {
            pq7.c(th, "throwable");
            x71.a.C0278a.b(this, obj, th);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("Coil", "error: " + th.getMessage() + " - phoneDate: " + new Date() + " - realDate: " + xy4.f4212a.a());
            this.f3492a.setImageDrawable(this.b);
        }

        @DexIgnore
        @Override // com.fossil.x71.a
        public void c(Object obj, q51 q51) {
            pq7.c(obj, "data");
            pq7.c(q51, "source");
            x71.a.C0278a.d(this, obj, q51);
            this.f3492a.setTag(this.c);
        }

        @DexIgnore
        @Override // com.fossil.x71.a
        public void onCancel(Object obj) {
            x71.a.C0278a.a(this, obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements x71.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ImageView f3493a;
        @DexIgnore
        public /* final */ /* synthetic */ Drawable b;

        @DexIgnore
        public b(ImageView imageView, Drawable drawable) {
            this.f3493a = imageView;
            this.b = drawable;
        }

        @DexIgnore
        @Override // com.fossil.x71.a
        public void a(Object obj) {
            pq7.c(obj, "data");
            x71.a.C0278a.c(this, obj);
        }

        @DexIgnore
        @Override // com.fossil.x71.a
        public void b(Object obj, Throwable th) {
            pq7.c(th, "throwable");
            x71.a.C0278a.b(this, obj, th);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("Coil", "error: " + th.getMessage() + " - phoneDate: " + new Date() + " - realDate: " + xy4.f4212a.a());
            this.f3493a.setImageDrawable(this.b);
        }

        @DexIgnore
        @Override // com.fossil.x71.a
        public void c(Object obj, q51 q51) {
            pq7.c(obj, "data");
            pq7.c(q51, "source");
            x71.a.C0278a.d(this, obj, q51);
        }

        @DexIgnore
        @Override // com.fossil.x71.a
        public void onCancel(Object obj) {
            x71.a.C0278a.a(this, obj);
        }
    }

    @DexIgnore
    public static final void a(ImageView imageView, String str, Drawable drawable) {
        pq7.c(imageView, "$this$setUrl");
        e51.a(imageView);
        if (str == null || str.length() == 0) {
            imageView.setImageDrawable(drawable);
            return;
        }
        a51 b2 = x41.b();
        Context context = imageView.getContext();
        pq7.b(context, "context");
        u71 u71 = new u71(context, b2.a());
        u71.x(str);
        u71.z(imageView);
        u71.y(drawable);
        u71.v(new l81());
        u71.t(new b(imageView, drawable));
        b2.b(u71.w());
    }

    @DexIgnore
    public static final void b(ImageView imageView, String str, String str2, f57.b bVar, uy4 uy4) {
        pq7.c(imageView, "$this$setUrl");
        pq7.c(bVar, "drawableBuilder");
        pq7.c(uy4, "colorGenerator");
        sy4 sy4 = new sy4(str, str2);
        e51.a(imageView);
        String valueOf = str2 == null || str2.length() == 0 ? ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR : String.valueOf(yt7.w0(str2));
        if (str2 == null) {
            str2 = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
        }
        f57 e = bVar.e(valueOf, uy4.b(str2));
        pq7.b(e, "drawableBuilder.build(av\u2026tor.getColor(str ?: \"-\"))");
        if (str == null || str.length() == 0) {
            imageView.setImageDrawable(e);
            imageView.setTag(sy4);
            return;
        }
        a51 b2 = x41.b();
        Context context = imageView.getContext();
        pq7.b(context, "context");
        u71 u71 = new u71(context, b2.a());
        u71.x(str);
        u71.z(imageView);
        u71.y(e);
        u71.v(new l81());
        u71.t(new a(imageView, e, sy4));
        b2.b(u71.w());
    }
}
