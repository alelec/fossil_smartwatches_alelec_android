package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class b45 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout B;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout C;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout D;
    @DexIgnore
    public /* final */ RTLImageView E;
    @DexIgnore
    public /* final */ ImageView F;
    @DexIgnore
    public /* final */ LinearLayout G;
    @DexIgnore
    public /* final */ LinearLayout H;
    @DexIgnore
    public /* final */ ConstraintLayout I;
    @DexIgnore
    public /* final */ ScrollView J;
    @DexIgnore
    public /* final */ FlexibleTextView K;
    @DexIgnore
    public /* final */ FlexibleTextView L;
    @DexIgnore
    public /* final */ FlexibleTextView M;
    @DexIgnore
    public /* final */ View N;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText u;
    @DexIgnore
    public /* final */ FlexibleEditText v;
    @DexIgnore
    public /* final */ FlexibleEditText w;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText x;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText y;
    @DexIgnore
    public /* final */ FlexibleEditText z;

    @DexIgnore
    public b45(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextInputEditText flexibleTextInputEditText, FlexibleEditText flexibleEditText, FlexibleEditText flexibleEditText2, FlexibleTextInputEditText flexibleTextInputEditText2, FlexibleTextInputEditText flexibleTextInputEditText3, FlexibleEditText flexibleEditText3, FlexibleTextView flexibleTextView, FlexibleTextInputLayout flexibleTextInputLayout, FlexibleTextInputLayout flexibleTextInputLayout2, FlexibleTextInputLayout flexibleTextInputLayout3, RTLImageView rTLImageView, ImageView imageView, LinearLayout linearLayout, LinearLayout linearLayout2, ConstraintLayout constraintLayout3, ScrollView scrollView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, View view2) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleButton2;
        this.s = constraintLayout;
        this.t = constraintLayout2;
        this.u = flexibleTextInputEditText;
        this.v = flexibleEditText;
        this.w = flexibleEditText2;
        this.x = flexibleTextInputEditText2;
        this.y = flexibleTextInputEditText3;
        this.z = flexibleEditText3;
        this.A = flexibleTextView;
        this.B = flexibleTextInputLayout;
        this.C = flexibleTextInputLayout2;
        this.D = flexibleTextInputLayout3;
        this.E = rTLImageView;
        this.F = imageView;
        this.G = linearLayout;
        this.H = linearLayout2;
        this.I = constraintLayout3;
        this.J = scrollView;
        this.K = flexibleTextView2;
        this.L = flexibleTextView3;
        this.M = flexibleTextView4;
        this.N = view2;
    }
}
