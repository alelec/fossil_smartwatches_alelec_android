package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mj extends lp {
    @DexIgnore
    public /* final */ ArrayList<ow> C; // = by1.a(this.i, hm7.c(ow.FILE_CONFIG, ow.TRANSFER_DATA));
    @DexIgnore
    public /* final */ short D;

    @DexIgnore
    public mj(k5 k5Var, i60 i60, yp ypVar, short s, String str, boolean z) {
        super(k5Var, i60, ypVar, str, z);
        this.D = (short) s;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(g80.k(super.C(), jd0.A0, hy1.l(this.D, null, 1, null)), jd0.g4, kb.e.a(this.D));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public boolean q(lp lpVar) {
        return !(lpVar instanceof mj) || ((mj) lpVar).D != this.D;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public boolean r(fs fsVar) {
        hs hsVar = fsVar != null ? fsVar.x : null;
        return hsVar != null && aj.f274a[hsVar.ordinal()] == 1;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.C;
    }
}
