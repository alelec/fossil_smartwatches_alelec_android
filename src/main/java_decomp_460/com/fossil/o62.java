package com.fossil;

import android.text.TextUtils;
import com.fossil.m62;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o62 extends Exception {
    @DexIgnore
    public /* final */ zi0<g72<?>, z52> zaba;

    @DexIgnore
    public o62(zi0<g72<?>, z52> zi0) {
        this.zaba = zi0;
    }

    @DexIgnore
    public z52 getConnectionResult(q62<? extends m62.d> q62) {
        g72<? extends m62.d> a2 = q62.a();
        boolean z = this.zaba.get(a2) != null;
        String a3 = a2.a();
        StringBuilder sb = new StringBuilder(String.valueOf(a3).length() + 58);
        sb.append("The given API (");
        sb.append(a3);
        sb.append(") was not part of the availability request.");
        rc2.b(z, sb.toString());
        return this.zaba.get(a2);
    }

    @DexIgnore
    public z52 getConnectionResult(s62<? extends m62.d> s62) {
        g72<? extends m62.d> a2 = s62.a();
        boolean z = this.zaba.get(a2) != null;
        String a3 = a2.a();
        StringBuilder sb = new StringBuilder(String.valueOf(a3).length() + 58);
        sb.append("The given API (");
        sb.append(a3);
        sb.append(") was not part of the availability request.");
        rc2.b(z, sb.toString());
        return this.zaba.get(a2);
    }

    @DexIgnore
    public String getMessage() {
        ArrayList arrayList = new ArrayList();
        boolean z = true;
        for (g72<?> g72 : this.zaba.keySet()) {
            z52 z52 = this.zaba.get(g72);
            if (z52.A()) {
                z = false;
            }
            String a2 = g72.a();
            String valueOf = String.valueOf(z52);
            StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 2 + String.valueOf(valueOf).length());
            sb.append(a2);
            sb.append(": ");
            sb.append(valueOf);
            arrayList.add(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder();
        if (z) {
            sb2.append("None of the queried APIs are available. ");
        } else {
            sb2.append("Some of the queried APIs are unavailable. ");
        }
        sb2.append(TextUtils.join("; ", arrayList));
        return sb2.toString();
    }

    @DexIgnore
    public final zi0<g72<?>, z52> zaj() {
        return this.zaba;
    }
}
