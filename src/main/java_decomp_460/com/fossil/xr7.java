package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xr7 implements Iterable<Long>, jr7 {
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public xr7(long j, long j2, long j3) {
        if (j3 == 0) {
            throw new IllegalArgumentException("Step must be non-zero.");
        } else if (j3 != Long.MIN_VALUE) {
            this.b = j;
            this.c = no7.d(j, j2, j3);
            this.d = j3;
        } else {
            throw new IllegalArgumentException("Step must be greater than Long.MIN_VALUE to avoid overflow on negation.");
        }
    }

    @DexIgnore
    public final long a() {
        return this.b;
    }

    @DexIgnore
    public final long b() {
        return this.c;
    }

    @DexIgnore
    /* renamed from: c */
    public vm7 iterator() {
        return new yr7(this.b, this.c, this.d);
    }
}
