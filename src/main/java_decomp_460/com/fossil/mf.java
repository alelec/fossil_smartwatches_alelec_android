package com.fossil;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mf extends qq7 implements rp7<fs, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ bi b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mf(bi biVar) {
        super(1);
        this.b = biVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(fs fsVar) {
        CopyOnWriteArrayList copyOnWriteArrayList = this.b.H;
        ArrayList<j0> arrayList = ((iv) fsVar).X;
        ArrayList arrayList2 = new ArrayList();
        for (T t : arrayList) {
            if (t.g > 0) {
                arrayList2.add(t);
            }
        }
        copyOnWriteArrayList.addAll(arrayList2);
        bi biVar = this.b;
        int i = 0;
        for (j0 j0Var : biVar.H) {
            i = ((int) j0Var.g) + i;
        }
        biVar.J = hy1.o(i);
        this.b.Z();
        this.b.Y();
        return tl7.f3441a;
    }
}
