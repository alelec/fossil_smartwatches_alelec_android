package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.ActionProvider;
import android.view.CollapsibleActionView;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import com.fossil.sn0;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fg0 extends yf0 implements MenuItem {
    @DexIgnore
    public /* final */ hm0 d;
    @DexIgnore
    public Method e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends sn0 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ActionProvider f1120a;

        @DexIgnore
        public a(Context context, ActionProvider actionProvider) {
            super(context);
            this.f1120a = actionProvider;
        }

        @DexIgnore
        @Override // com.fossil.sn0
        public boolean hasSubMenu() {
            return this.f1120a.hasSubMenu();
        }

        @DexIgnore
        @Override // com.fossil.sn0
        public View onCreateActionView() {
            return this.f1120a.onCreateActionView();
        }

        @DexIgnore
        @Override // com.fossil.sn0
        public boolean onPerformDefaultAction() {
            return this.f1120a.onPerformDefaultAction();
        }

        @DexIgnore
        @Override // com.fossil.sn0
        public void onPrepareSubMenu(SubMenu subMenu) {
            this.f1120a.onPrepareSubMenu(fg0.this.d(subMenu));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends a implements ActionProvider.VisibilityListener {
        @DexIgnore
        public sn0.b c;

        @DexIgnore
        public b(fg0 fg0, Context context, ActionProvider actionProvider) {
            super(context, actionProvider);
        }

        @DexIgnore
        @Override // com.fossil.sn0
        public boolean isVisible() {
            return this.f1120a.isVisible();
        }

        @DexIgnore
        public void onActionProviderVisibilityChanged(boolean z) {
            sn0.b bVar = this.c;
            if (bVar != null) {
                bVar.onActionProviderVisibilityChanged(z);
            }
        }

        @DexIgnore
        @Override // com.fossil.sn0
        public View onCreateActionView(MenuItem menuItem) {
            return this.f1120a.onCreateActionView(menuItem);
        }

        @DexIgnore
        @Override // com.fossil.sn0
        public boolean overridesItemVisibility() {
            return this.f1120a.overridesItemVisibility();
        }

        @DexIgnore
        @Override // com.fossil.sn0
        public void refreshVisibility() {
            this.f1120a.refreshVisibility();
        }

        @DexIgnore
        @Override // com.fossil.sn0
        public void setVisibilityListener(sn0.b bVar) {
            this.c = bVar;
            ActionProvider actionProvider = this.f1120a;
            if (bVar == null) {
                this = null;
            }
            actionProvider.setVisibilityListener(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends FrameLayout implements pf0 {
        @DexIgnore
        public /* final */ CollapsibleActionView b;

        @DexIgnore
        public c(View view) {
            super(view.getContext());
            this.b = (CollapsibleActionView) view;
            addView(view);
        }

        @DexIgnore
        public View a() {
            return (View) this.b;
        }

        @DexIgnore
        @Override // com.fossil.pf0
        public void onActionViewCollapsed() {
            this.b.onActionViewCollapsed();
        }

        @DexIgnore
        @Override // com.fossil.pf0
        public void onActionViewExpanded() {
            this.b.onActionViewExpanded();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements MenuItem.OnActionExpandListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ MenuItem.OnActionExpandListener f1121a;

        @DexIgnore
        public d(MenuItem.OnActionExpandListener onActionExpandListener) {
            this.f1121a = onActionExpandListener;
        }

        @DexIgnore
        public boolean onMenuItemActionCollapse(MenuItem menuItem) {
            return this.f1121a.onMenuItemActionCollapse(fg0.this.c(menuItem));
        }

        @DexIgnore
        public boolean onMenuItemActionExpand(MenuItem menuItem) {
            return this.f1121a.onMenuItemActionExpand(fg0.this.c(menuItem));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements MenuItem.OnMenuItemClickListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ MenuItem.OnMenuItemClickListener f1122a;

        @DexIgnore
        public e(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
            this.f1122a = onMenuItemClickListener;
        }

        @DexIgnore
        public boolean onMenuItemClick(MenuItem menuItem) {
            return this.f1122a.onMenuItemClick(fg0.this.c(menuItem));
        }
    }

    @DexIgnore
    public fg0(Context context, hm0 hm0) {
        super(context);
        if (hm0 != null) {
            this.d = hm0;
            return;
        }
        throw new IllegalArgumentException("Wrapped Object can not be null.");
    }

    @DexIgnore
    public boolean collapseActionView() {
        return this.d.collapseActionView();
    }

    @DexIgnore
    public boolean expandActionView() {
        return this.d.expandActionView();
    }

    @DexIgnore
    public ActionProvider getActionProvider() {
        sn0 b2 = this.d.b();
        if (b2 instanceof a) {
            return ((a) b2).f1120a;
        }
        return null;
    }

    @DexIgnore
    public View getActionView() {
        View actionView = this.d.getActionView();
        return actionView instanceof c ? ((c) actionView).a() : actionView;
    }

    @DexIgnore
    public int getAlphabeticModifiers() {
        return this.d.getAlphabeticModifiers();
    }

    @DexIgnore
    public char getAlphabeticShortcut() {
        return this.d.getAlphabeticShortcut();
    }

    @DexIgnore
    public CharSequence getContentDescription() {
        return this.d.getContentDescription();
    }

    @DexIgnore
    public int getGroupId() {
        return this.d.getGroupId();
    }

    @DexIgnore
    public Drawable getIcon() {
        return this.d.getIcon();
    }

    @DexIgnore
    public ColorStateList getIconTintList() {
        return this.d.getIconTintList();
    }

    @DexIgnore
    public PorterDuff.Mode getIconTintMode() {
        return this.d.getIconTintMode();
    }

    @DexIgnore
    public Intent getIntent() {
        return this.d.getIntent();
    }

    @DexIgnore
    public int getItemId() {
        return this.d.getItemId();
    }

    @DexIgnore
    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.d.getMenuInfo();
    }

    @DexIgnore
    public int getNumericModifiers() {
        return this.d.getNumericModifiers();
    }

    @DexIgnore
    public char getNumericShortcut() {
        return this.d.getNumericShortcut();
    }

    @DexIgnore
    public int getOrder() {
        return this.d.getOrder();
    }

    @DexIgnore
    public SubMenu getSubMenu() {
        return d(this.d.getSubMenu());
    }

    @DexIgnore
    public CharSequence getTitle() {
        return this.d.getTitle();
    }

    @DexIgnore
    public CharSequence getTitleCondensed() {
        return this.d.getTitleCondensed();
    }

    @DexIgnore
    public CharSequence getTooltipText() {
        return this.d.getTooltipText();
    }

    @DexIgnore
    public void h(boolean z) {
        try {
            if (this.e == null) {
                this.e = this.d.getClass().getDeclaredMethod("setExclusiveCheckable", Boolean.TYPE);
            }
            this.e.invoke(this.d, Boolean.valueOf(z));
        } catch (Exception e2) {
            Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", e2);
        }
    }

    @DexIgnore
    public boolean hasSubMenu() {
        return this.d.hasSubMenu();
    }

    @DexIgnore
    public boolean isActionViewExpanded() {
        return this.d.isActionViewExpanded();
    }

    @DexIgnore
    public boolean isCheckable() {
        return this.d.isCheckable();
    }

    @DexIgnore
    public boolean isChecked() {
        return this.d.isChecked();
    }

    @DexIgnore
    public boolean isEnabled() {
        return this.d.isEnabled();
    }

    @DexIgnore
    public boolean isVisible() {
        return this.d.isVisible();
    }

    @DexIgnore
    public MenuItem setActionProvider(ActionProvider actionProvider) {
        sn0 bVar = Build.VERSION.SDK_INT >= 16 ? new b(this, this.f4309a, actionProvider) : new a(this.f4309a, actionProvider);
        hm0 hm0 = this.d;
        if (actionProvider == null) {
            bVar = null;
        }
        hm0.a(bVar);
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setActionView(int i) {
        this.d.setActionView(i);
        View actionView = this.d.getActionView();
        if (actionView instanceof CollapsibleActionView) {
            this.d.setActionView(new c(actionView));
        }
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setActionView(View view) {
        if (view instanceof CollapsibleActionView) {
            view = new c(view);
        }
        this.d.setActionView(view);
        return this;
    }

    @DexIgnore
    public MenuItem setAlphabeticShortcut(char c2) {
        this.d.setAlphabeticShortcut(c2);
        return this;
    }

    @DexIgnore
    public MenuItem setAlphabeticShortcut(char c2, int i) {
        this.d.setAlphabeticShortcut(c2, i);
        return this;
    }

    @DexIgnore
    public MenuItem setCheckable(boolean z) {
        this.d.setCheckable(z);
        return this;
    }

    @DexIgnore
    public MenuItem setChecked(boolean z) {
        this.d.setChecked(z);
        return this;
    }

    @DexIgnore
    public MenuItem setContentDescription(CharSequence charSequence) {
        this.d.setContentDescription(charSequence);
        return this;
    }

    @DexIgnore
    public MenuItem setEnabled(boolean z) {
        this.d.setEnabled(z);
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setIcon(int i) {
        this.d.setIcon(i);
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setIcon(Drawable drawable) {
        this.d.setIcon(drawable);
        return this;
    }

    @DexIgnore
    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.d.setIconTintList(colorStateList);
        return this;
    }

    @DexIgnore
    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.d.setIconTintMode(mode);
        return this;
    }

    @DexIgnore
    public MenuItem setIntent(Intent intent) {
        this.d.setIntent(intent);
        return this;
    }

    @DexIgnore
    public MenuItem setNumericShortcut(char c2) {
        this.d.setNumericShortcut(c2);
        return this;
    }

    @DexIgnore
    public MenuItem setNumericShortcut(char c2, int i) {
        this.d.setNumericShortcut(c2, i);
        return this;
    }

    @DexIgnore
    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        this.d.setOnActionExpandListener(onActionExpandListener != null ? new d(onActionExpandListener) : null);
        return this;
    }

    @DexIgnore
    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.d.setOnMenuItemClickListener(onMenuItemClickListener != null ? new e(onMenuItemClickListener) : null);
        return this;
    }

    @DexIgnore
    public MenuItem setShortcut(char c2, char c3) {
        this.d.setShortcut(c2, c3);
        return this;
    }

    @DexIgnore
    public MenuItem setShortcut(char c2, char c3, int i, int i2) {
        this.d.setShortcut(c2, c3, i, i2);
        return this;
    }

    @DexIgnore
    public void setShowAsAction(int i) {
        this.d.setShowAsAction(i);
    }

    @DexIgnore
    public MenuItem setShowAsActionFlags(int i) {
        this.d.setShowAsActionFlags(i);
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setTitle(int i) {
        this.d.setTitle(i);
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setTitle(CharSequence charSequence) {
        this.d.setTitle(charSequence);
        return this;
    }

    @DexIgnore
    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.d.setTitleCondensed(charSequence);
        return this;
    }

    @DexIgnore
    public MenuItem setTooltipText(CharSequence charSequence) {
        this.d.setTooltipText(charSequence);
        return this;
    }

    @DexIgnore
    public MenuItem setVisible(boolean z) {
        return this.d.setVisible(z);
    }
}
