package com.fossil;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yx2<K, V> extends fx2<K, V> implements Serializable {
    @DexIgnore
    public /* final */ transient wx2<K, ? extends tx2<V>> b;

    @DexIgnore
    public yx2(wx2<K, ? extends tx2<V>> wx2, int i) {
        this.b = wx2;
    }

    @DexIgnore
    @Override // com.fossil.cx2
    public /* bridge */ /* synthetic */ boolean equals(@NullableDecl Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.cx2
    public /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.cx2
    public /* bridge */ /* synthetic */ String toString() {
        return super.toString();
    }

    @DexIgnore
    @Override // com.fossil.cx2, com.fossil.jy2
    public final /* synthetic */ Map zza() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.cx2
    public final boolean zza(@NullableDecl Object obj) {
        return obj != null && super.zza(obj);
    }

    @DexIgnore
    @Override // com.fossil.cx2
    public final Map<K, Collection<V>> zzb() {
        throw new AssertionError("should never be called");
    }
}
