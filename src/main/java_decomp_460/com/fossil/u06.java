package com.fossil;

import com.fossil.tq4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u06 extends tq4<a, b, tq4.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements tq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<i06> f3501a;

        @DexIgnore
        public a(List<i06> list) {
            pq7.c(list, "appWrapperList");
            this.f3501a = list;
        }

        @DexIgnore
        public final List<i06> a() {
            return this.f3501a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tq4.c {
        @DexIgnore
        public b(boolean z) {
        }
    }

    /*
    static {
        String simpleName = u06.class.getSimpleName();
        pq7.b(simpleName, "SaveAppsNotification::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public u06(NotificationsRepository notificationsRepository) {
        pq7.c(notificationsRepository, "notificationsRepository");
        i14.o(notificationsRepository, "notificationsRepository cannot be null!", new Object[0]);
        pq7.b(notificationsRepository, "checkNotNull(notificatio\u2026ository cannot be null!\")");
        this.d = notificationsRepository;
    }

    @DexIgnore
    /* renamed from: f */
    public void a(a aVar) {
        pq7.c(aVar, "requestValues");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (i06 i06 : aVar.a()) {
            InstalledApp installedApp = i06.getInstalledApp();
            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
            if (isSelected == null) {
                pq7.i();
                throw null;
            } else if (isSelected.booleanValue()) {
                arrayList.add(i06);
            } else {
                arrayList2.add(i06);
            }
        }
        g(arrayList2);
        h(arrayList);
        FLogger.INSTANCE.getLocal().d(e, "Inside .SaveAppsNotification done");
        b().onSuccess(new b(true));
    }

    @DexIgnore
    public final void g(List<i06> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (i06 i06 : list) {
                AppFilter appFilter = new AppFilter();
                InstalledApp installedApp = i06.getInstalledApp();
                Integer valueOf = installedApp != null ? Integer.valueOf(installedApp.getDbRowId()) : null;
                if (valueOf != null) {
                    appFilter.setDbRowId(valueOf.intValue());
                    appFilter.setHour(i06.getCurrentHandGroup());
                    InstalledApp installedApp2 = i06.getInstalledApp();
                    String identifier = installedApp2 != null ? installedApp2.getIdentifier() : null;
                    if (identifier != null) {
                        appFilter.setType(identifier);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = e;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Removed: App name = ");
                        InstalledApp installedApp3 = i06.getInstalledApp();
                        sb.append(installedApp3 != null ? installedApp3.getIdentifier() : null);
                        sb.append(", row id = ");
                        InstalledApp installedApp4 = i06.getInstalledApp();
                        sb.append(installedApp4 != null ? Integer.valueOf(installedApp4.getDbRowId()) : null);
                        sb.append(", hour = ");
                        sb.append(i06.getCurrentHandGroup());
                        local.d(str, sb.toString());
                        arrayList.add(appFilter);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            this.d.removeListAppFilter(arrayList);
        }
    }

    @DexIgnore
    public final void h(List<i06> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (i06 i06 : list) {
                AppFilter appFilter = new AppFilter();
                appFilter.setHour(i06.getCurrentHandGroup());
                InstalledApp installedApp = i06.getInstalledApp();
                appFilter.setType(installedApp != null ? installedApp.getIdentifier() : null);
                appFilter.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = e;
                StringBuilder sb = new StringBuilder();
                sb.append("Saved: App name = ");
                InstalledApp installedApp2 = i06.getInstalledApp();
                sb.append(installedApp2 != null ? installedApp2.getIdentifier() : null);
                sb.append(", hour = ");
                sb.append(i06.getCurrentHandGroup());
                local.d(str, sb.toString());
                arrayList.add(appFilter);
            }
            this.d.saveListAppFilters(arrayList);
        }
    }
}
