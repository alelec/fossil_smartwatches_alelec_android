package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t92 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ z82 f3379a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ q62<?> c;

    @DexIgnore
    public t92(z82 z82, int i, q62<?> q62) {
        this.f3379a = z82;
        this.b = i;
        this.c = q62;
    }
}
