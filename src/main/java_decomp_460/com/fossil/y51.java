package com.fossil;

import android.content.Context;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import java.io.InputStream;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y51 implements e61<Uri> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f4243a;

    @DexIgnore
    public y51(Context context) {
        pq7.c(context, "context");
        this.f4243a = context;
    }

    @DexIgnore
    /* renamed from: d */
    public Object c(g51 g51, Uri uri, f81 f81, x51 x51, qn7<? super d61> qn7) {
        List<String> pathSegments = uri.getPathSegments();
        pq7.b(pathSegments, "data.pathSegments");
        String N = pm7.N(pm7.D(pathSegments, 1), "/", null, null, 0, null, null, 62, null);
        InputStream open = this.f4243a.getAssets().open(N);
        pq7.b(open, "context.assets.open(path)");
        k48 d = s48.d(s48.l(open));
        MimeTypeMap singleton = MimeTypeMap.getSingleton();
        pq7.b(singleton, "MimeTypeMap.getSingleton()");
        return new k61(d, w81.g(singleton, N), q51.DISK);
    }

    @DexIgnore
    /* renamed from: e */
    public boolean a(Uri uri) {
        pq7.c(uri, "data");
        return pq7.a(uri.getScheme(), "file") && pq7.a(w81.f(uri), "android_asset");
    }

    @DexIgnore
    /* renamed from: f */
    public String b(Uri uri) {
        pq7.c(uri, "data");
        String uri2 = uri.toString();
        pq7.b(uri2, "data.toString()");
        return uri2;
    }
}
