package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nt3<TResult> {
    @DexIgnore
    public nt3<TResult> a(Executor executor, gt3 gt3) {
        throw new UnsupportedOperationException("addOnCanceledListener is not implemented");
    }

    @DexIgnore
    public nt3<TResult> b(ht3<TResult> ht3) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    @DexIgnore
    public nt3<TResult> c(Executor executor, ht3<TResult> ht3) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    @DexIgnore
    public abstract nt3<TResult> d(it3 it3);

    @DexIgnore
    public abstract nt3<TResult> e(Executor executor, it3 it3);

    @DexIgnore
    public abstract nt3<TResult> f(jt3<? super TResult> jt3);

    @DexIgnore
    public abstract nt3<TResult> g(Executor executor, jt3<? super TResult> jt3);

    @DexIgnore
    public <TContinuationResult> nt3<TContinuationResult> h(ft3<TResult, TContinuationResult> ft3) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> nt3<TContinuationResult> i(Executor executor, ft3<TResult, TContinuationResult> ft3) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> nt3<TContinuationResult> j(ft3<TResult, nt3<TContinuationResult>> ft3) {
        throw new UnsupportedOperationException("continueWithTask is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> nt3<TContinuationResult> k(Executor executor, ft3<TResult, nt3<TContinuationResult>> ft3) {
        throw new UnsupportedOperationException("continueWithTask is not implemented");
    }

    @DexIgnore
    public abstract Exception l();

    @DexIgnore
    public abstract TResult m();

    @DexIgnore
    public abstract <X extends Throwable> TResult n(Class<X> cls) throws Throwable;

    @DexIgnore
    public abstract boolean o();

    @DexIgnore
    public abstract boolean p();

    @DexIgnore
    public abstract boolean q();

    @DexIgnore
    public <TContinuationResult> nt3<TContinuationResult> r(mt3<TResult, TContinuationResult> mt3) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> nt3<TContinuationResult> s(Executor executor, mt3<TResult, TContinuationResult> mt3) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }
}
