package com.fossil;

import com.fossil.fh4;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class eh4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ File f943a;
    @DexIgnore
    public /* final */ j64 b;

    @DexIgnore
    public enum a {
        ATTEMPT_MIGRATION,
        NOT_GENERATED,
        UNREGISTERED,
        REGISTERED,
        REGISTER_ERROR
    }

    @DexIgnore
    public eh4(j64 j64) {
        File filesDir = j64.g().getFilesDir();
        this.f943a = new File(filesDir, "PersistedInstallation." + j64.k() + ".json");
        this.b = j64;
    }

    @DexIgnore
    public fh4 a(fh4 fh4) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("Fid", fh4.d());
            jSONObject.put("Status", fh4.g().ordinal());
            jSONObject.put("AuthToken", fh4.b());
            jSONObject.put("RefreshToken", fh4.f());
            jSONObject.put("TokenCreationEpochInSecs", fh4.h());
            jSONObject.put("ExpiresInSecs", fh4.c());
            jSONObject.put("FisError", fh4.e());
            File createTempFile = File.createTempFile("PersistedInstallation", "tmp", this.b.g().getFilesDir());
            FileOutputStream fileOutputStream = new FileOutputStream(createTempFile);
            fileOutputStream.write(jSONObject.toString().getBytes("UTF-8"));
            fileOutputStream.close();
            if (!createTempFile.renameTo(this.f943a)) {
                throw new IOException("unable to rename the tmpfile to PersistedInstallation");
            }
        } catch (IOException | JSONException e) {
        }
        return fh4;
    }

    @DexIgnore
    public final JSONObject b() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[16384];
        try {
            FileInputStream fileInputStream = new FileInputStream(this.f943a);
            while (true) {
                try {
                    int read = fileInputStream.read(bArr, 0, 16384);
                    if (read < 0) {
                        JSONObject jSONObject = new JSONObject(byteArrayOutputStream.toString());
                        fileInputStream.close();
                        return jSONObject;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                } catch (Throwable th) {
                }
            }
            throw th;
        } catch (IOException | JSONException e) {
            return new JSONObject();
        }
    }

    @DexIgnore
    public fh4 c() {
        JSONObject b2 = b();
        String optString = b2.optString("Fid", null);
        int optInt = b2.optInt("Status", a.ATTEMPT_MIGRATION.ordinal());
        String optString2 = b2.optString("AuthToken", null);
        String optString3 = b2.optString("RefreshToken", null);
        long optLong = b2.optLong("TokenCreationEpochInSecs", 0);
        long optLong2 = b2.optLong("ExpiresInSecs", 0);
        String optString4 = b2.optString("FisError", null);
        fh4.a a2 = fh4.a();
        a2.d(optString);
        a2.g(a.values()[optInt]);
        a2.b(optString2);
        a2.f(optString3);
        a2.h(optLong);
        a2.c(optLong2);
        a2.e(optString4);
        return a2.a();
    }
}
