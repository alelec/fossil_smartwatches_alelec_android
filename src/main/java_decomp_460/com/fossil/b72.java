package com.fossil;

import android.util.Log;
import com.fossil.z62;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class b72<R extends z62> implements a72<R> {
    @DexIgnore
    @Override // com.fossil.a72
    public final void a(R r) {
        Status a2 = r.a();
        if (a2.D()) {
            c(r);
            return;
        }
        b(a2);
        if (r instanceof v62) {
            try {
                ((v62) r).release();
            } catch (RuntimeException e) {
                String valueOf = String.valueOf(r);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("ResultCallbacks", sb.toString(), e);
            }
        }
    }

    @DexIgnore
    public abstract void b(Status status);

    @DexIgnore
    public abstract void c(R r);
}
