package com.fossil;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a41 implements Executor {
    @DexIgnore
    public /* final */ ArrayDeque<a> b; // = new ArrayDeque<>();
    @DexIgnore
    public /* final */ Executor c;
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public volatile Runnable e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public /* final */ a41 b;
        @DexIgnore
        public /* final */ Runnable c;

        @DexIgnore
        public a(a41 a41, Runnable runnable) {
            this.b = a41;
            this.c = runnable;
        }

        @DexIgnore
        public void run() {
            try {
                this.c.run();
            } finally {
                this.b.b();
            }
        }
    }

    @DexIgnore
    public a41(Executor executor) {
        this.c = executor;
    }

    @DexIgnore
    public boolean a() {
        boolean z;
        synchronized (this.d) {
            z = !this.b.isEmpty();
        }
        return z;
    }

    @DexIgnore
    public void b() {
        synchronized (this.d) {
            a poll = this.b.poll();
            this.e = poll;
            if (poll != null) {
                this.c.execute(this.e);
            }
        }
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        synchronized (this.d) {
            this.b.add(new a(this, runnable));
            if (this.e == null) {
                b();
            }
        }
    }
}
