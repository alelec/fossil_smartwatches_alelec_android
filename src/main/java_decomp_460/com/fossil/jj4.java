package com.fossil;

import com.google.gson.JsonElement;
import java.math.BigInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jj4 extends JsonElement {
    @DexIgnore
    public static /* final */ Class<?>[] b; // = {Integer.TYPE, Long.TYPE, Short.TYPE, Float.TYPE, Double.TYPE, Byte.TYPE, Boolean.TYPE, Character.TYPE, Integer.class, Long.class, Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class};

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Object f1767a;

    @DexIgnore
    public jj4(Boolean bool) {
        t(bool);
    }

    @DexIgnore
    public jj4(Number number) {
        t(number);
    }

    @DexIgnore
    public jj4(Object obj) {
        t(obj);
    }

    @DexIgnore
    public jj4(String str) {
        t(str);
    }

    @DexIgnore
    public static boolean p(jj4 jj4) {
        Object obj = jj4.f1767a;
        if (obj instanceof Number) {
            Number number = (Number) obj;
            if ((number instanceof BigInteger) || (number instanceof Long) || (number instanceof Integer) || (number instanceof Short) || (number instanceof Byte)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static boolean r(Object obj) {
        if (obj instanceof String) {
            return true;
        }
        Class<?> cls = obj.getClass();
        for (Class<?> cls2 : b) {
            if (cls2.isAssignableFrom(cls)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.google.gson.JsonElement
    public boolean a() {
        return o() ? k().booleanValue() : Boolean.parseBoolean(f());
    }

    @DexIgnore
    @Override // com.google.gson.JsonElement
    public int b() {
        return q() ? n().intValue() : Integer.parseInt(f());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || jj4.class != obj.getClass()) {
            return false;
        }
        jj4 jj4 = (jj4) obj;
        if (this.f1767a == null) {
            return jj4.f1767a == null;
        }
        if (p(this) && p(jj4)) {
            return n().longValue() == jj4.n().longValue();
        }
        if (!(this.f1767a instanceof Number) || !(jj4.f1767a instanceof Number)) {
            return this.f1767a.equals(jj4.f1767a);
        }
        double doubleValue = n().doubleValue();
        double doubleValue2 = jj4.n().doubleValue();
        if (doubleValue != doubleValue2) {
            return Double.isNaN(doubleValue) && Double.isNaN(doubleValue2);
        }
        return true;
    }

    @DexIgnore
    @Override // com.google.gson.JsonElement
    public String f() {
        return q() ? n().toString() : o() ? k().toString() : (String) this.f1767a;
    }

    @DexIgnore
    public int hashCode() {
        long doubleToLongBits;
        if (this.f1767a == null) {
            return 31;
        }
        if (p(this)) {
            doubleToLongBits = n().longValue();
        } else {
            Object obj = this.f1767a;
            if (!(obj instanceof Number)) {
                return obj.hashCode();
            }
            doubleToLongBits = Double.doubleToLongBits(n().doubleValue());
        }
        return (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
    }

    @DexIgnore
    public Boolean k() {
        return (Boolean) this.f1767a;
    }

    @DexIgnore
    public double l() {
        return q() ? n().doubleValue() : Double.parseDouble(f());
    }

    @DexIgnore
    public long m() {
        return q() ? n().longValue() : Long.parseLong(f());
    }

    @DexIgnore
    public Number n() {
        Object obj = this.f1767a;
        return obj instanceof String ? new zj4((String) this.f1767a) : (Number) obj;
    }

    @DexIgnore
    public boolean o() {
        return this.f1767a instanceof Boolean;
    }

    @DexIgnore
    public boolean q() {
        return this.f1767a instanceof Number;
    }

    @DexIgnore
    public boolean s() {
        return this.f1767a instanceof String;
    }

    @DexIgnore
    public void t(Object obj) {
        if (obj instanceof Character) {
            this.f1767a = String.valueOf(((Character) obj).charValue());
            return;
        }
        uj4.a((obj instanceof Number) || r(obj));
        this.f1767a = obj;
    }
}
