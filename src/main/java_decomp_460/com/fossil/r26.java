package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r26 extends u47 implements q26 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public /* final */ zp0 k; // = new sr4(this);
    @DexIgnore
    public g37<z55> l;
    @DexIgnore
    public p26 m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return r26.t;
        }

        @DexIgnore
        public final r26 b() {
            return new r26();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ z55 f3070a;
        @DexIgnore
        public /* final */ /* synthetic */ r26 b;

        @DexIgnore
        public b(z55 z55, r26 r26, boolean z) {
            this.f3070a = z55;
            this.b = r26;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            p26 A6 = r26.A6(this.b);
            NumberPicker numberPicker2 = this.f3070a.t;
            pq7.b(numberPicker2, "binding.numberPickerOne");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.f3070a.u;
            pq7.b(numberPicker3, "binding.numberPickerThree");
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            A6.p(String.valueOf(value), String.valueOf(i2), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ z55 f3071a;
        @DexIgnore
        public /* final */ /* synthetic */ r26 b;

        @DexIgnore
        public c(z55 z55, r26 r26, boolean z) {
            this.f3071a = z55;
            this.b = r26;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            p26 A6 = r26.A6(this.b);
            NumberPicker numberPicker2 = this.f3071a.v;
            pq7.b(numberPicker2, "binding.numberPickerTwo");
            A6.p(String.valueOf(i2), String.valueOf(numberPicker2.getValue()), false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ z55 f3072a;
        @DexIgnore
        public /* final */ /* synthetic */ r26 b;

        @DexIgnore
        public d(z55 z55, r26 r26, boolean z) {
            this.f3072a = z55;
            this.b = r26;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            p26 A6 = r26.A6(this.b);
            NumberPicker numberPicker2 = this.f3072a.v;
            pq7.b(numberPicker2, "binding.numberPickerTwo");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.f3072a.u;
            pq7.b(numberPicker3, "binding.numberPickerThree");
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            A6.p(String.valueOf(i2), String.valueOf(value), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ z55 f3073a;
        @DexIgnore
        public /* final */ /* synthetic */ r26 b;

        @DexIgnore
        public e(z55 z55, r26 r26, boolean z) {
            this.f3073a = z55;
            this.b = r26;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            p26 A6 = r26.A6(this.b);
            NumberPicker numberPicker2 = this.f3073a.t;
            pq7.b(numberPicker2, "binding.numberPickerOne");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.f3073a.v;
            pq7.b(numberPicker3, "binding.numberPickerTwo");
            int value2 = numberPicker3.getValue();
            if (i2 != 1) {
                z = false;
            }
            A6.p(String.valueOf(value), String.valueOf(value2), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ r26 b;

        @DexIgnore
        public f(r26 r26) {
            this.b = r26;
        }

        @DexIgnore
        public final void onClick(View view) {
            r26.A6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ z55 b;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 c;

        @DexIgnore
        public g(z55 z55, dr7 dr7) {
            this.b = z55;
            this.c = dr7;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.b.w;
            pq7.b(constraintLayout, "it.rootBackground");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                ViewGroup.LayoutParams layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) ((CoordinatorLayout.e) layoutParams).f();
                    if (bottomSheetBehavior != null) {
                        bottomSheetBehavior.E(3);
                        z55 z55 = this.b;
                        pq7.b(z55, "it");
                        View n = z55.n();
                        pq7.b(n, "it.root");
                        n.getViewTreeObserver().removeOnGlobalLayoutListener(this.c.element);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                throw new il7("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new il7("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = r26.class.getSimpleName();
        pq7.b(simpleName, "InactivityNudgeTimeFragment::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ p26 A6(r26 r26) {
        p26 p26 = r26.m;
        if (p26 != null) {
            return p26;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.q26
    public void B5(boolean z) {
        g37<z55> g37 = this.l;
        if (g37 != null) {
            z55 a2 = g37.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.v;
                pq7.b(numberPicker, "binding.numberPickerTwo");
                numberPicker.setMinValue(0);
                NumberPicker numberPicker2 = a2.v;
                pq7.b(numberPicker2, "binding.numberPickerTwo");
                numberPicker2.setMaxValue(59);
                a2.v.setOnValueChangedListener(new b(a2, this, z));
                if (z) {
                    NumberPicker numberPicker3 = a2.t;
                    pq7.b(numberPicker3, "binding.numberPickerOne");
                    numberPicker3.setMinValue(0);
                    NumberPicker numberPicker4 = a2.t;
                    pq7.b(numberPicker4, "binding.numberPickerOne");
                    numberPicker4.setMaxValue(23);
                    a2.t.setOnValueChangedListener(new c(a2, this, z));
                    NumberPicker numberPicker5 = a2.u;
                    pq7.b(numberPicker5, "binding.numberPickerThree");
                    numberPicker5.setVisibility(8);
                    return;
                }
                NumberPicker numberPicker6 = a2.t;
                pq7.b(numberPicker6, "binding.numberPickerOne");
                numberPicker6.setMinValue(1);
                NumberPicker numberPicker7 = a2.t;
                pq7.b(numberPicker7, "binding.numberPickerOne");
                numberPicker7.setMaxValue(12);
                a2.t.setOnValueChangedListener(new d(a2, this, z));
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886102);
                String c3 = um5.c(PortfolioApp.h0.c(), 2131886104);
                NumberPicker numberPicker8 = a2.u;
                pq7.b(numberPicker8, "binding.numberPickerThree");
                numberPicker8.setVisibility(0);
                NumberPicker numberPicker9 = a2.u;
                pq7.b(numberPicker9, "binding.numberPickerThree");
                numberPicker9.setMinValue(0);
                NumberPicker numberPicker10 = a2.u;
                pq7.b(numberPicker10, "binding.numberPickerThree");
                numberPicker10.setMaxValue(1);
                a2.u.setDisplayedValues(new String[]{c2, c3});
                a2.u.setOnValueChangedListener(new e(a2, this, z));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void C6(int i) {
        p26 p26 = this.m;
        if (p26 != null) {
            p26.o(i);
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: D6 */
    public void M5(p26 p26) {
        pq7.c(p26, "presenter");
        this.m = p26;
    }

    @DexIgnore
    @Override // com.fossil.q26
    public void Y0(String str) {
        pq7.c(str, "description");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager requireFragmentManager = requireFragmentManager();
            pq7.b(requireFragmentManager, "requireFragmentManager()");
            s37.q(requireFragmentManager, str);
        }
    }

    @DexIgnore
    @Override // com.fossil.q26
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    @Override // com.fossil.q26
    public void h6(int i, boolean z) {
        int i2 = 0;
        int i3 = i / 60;
        if (!z) {
            if (i3 >= 12) {
                i2 = 1;
                i3 -= 12;
            }
            if (i3 == 0) {
                i3 = 12;
            }
        }
        g37<z55> g37 = this.l;
        if (g37 != null) {
            z55 a2 = g37.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.t;
                pq7.b(numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i3);
                NumberPicker numberPicker2 = a2.v;
                pq7.b(numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i % 60);
                NumberPicker numberPicker3 = a2.u;
                pq7.b(numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i2);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        z55 z55 = (z55) aq0.f(layoutInflater, 2131558550, viewGroup, false, this.k);
        String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(d2)) {
            z55.w.setBackgroundColor(Color.parseColor(d2));
        }
        z55.q.setOnClickListener(new f(this));
        this.l = new g37<>(this, z55);
        pq7.b(z55, "binding");
        return z55.n();
    }

    @DexIgnore
    @Override // com.fossil.u47, androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        p26 p26 = this.m;
        if (p26 != null) {
            p26.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        p26 p26 = this.m;
        if (p26 != null) {
            p26.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<z55> g37 = this.l;
        if (g37 != null) {
            z55 a2 = g37.a();
            if (a2 != null) {
                dr7 dr7 = new dr7();
                dr7.element = null;
                dr7.element = (T) new g(a2, dr7);
                pq7.b(a2, "it");
                View n = a2.n();
                pq7.b(n, "it.root");
                n.getViewTreeObserver().addOnGlobalLayoutListener(dr7.element);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.q26
    public void t(String str) {
        FlexibleTextView flexibleTextView;
        pq7.c(str, "title");
        g37<z55> g37 = this.l;
        if (g37 != null) {
            z55 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.r) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.u47
    public void z6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
