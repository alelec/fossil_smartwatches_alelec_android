package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class z04<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends z04<Object> implements Serializable {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 1;

        @DexIgnore
        private Object readResolve() {
            return INSTANCE;
        }

        @DexIgnore
        @Override // com.fossil.z04
        public boolean doEquivalent(Object obj, Object obj2) {
            return obj.equals(obj2);
        }

        @DexIgnore
        @Override // com.fossil.z04
        public int doHash(Object obj) {
            return obj.hashCode();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements j14<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ z04<T> equivalence;
        @DexIgnore
        public /* final */ T target;

        @DexIgnore
        public c(z04<T> z04, T t) {
            i14.l(z04);
            this.equivalence = z04;
            this.target = t;
        }

        @DexIgnore
        @Override // com.fossil.j14
        public boolean apply(T t) {
            return this.equivalence.equivalent(t, this.target);
        }

        @DexIgnore
        @Override // com.fossil.j14
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return this.equivalence.equals(cVar.equivalence) && f14.a(this.target, cVar.target);
        }

        @DexIgnore
        public int hashCode() {
            return f14.b(this.equivalence, this.target);
        }

        @DexIgnore
        public String toString() {
            return this.equivalence + ".equivalentTo(" + ((Object) this.target) + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends z04<Object> implements Serializable {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 1;

        @DexIgnore
        private Object readResolve() {
            return INSTANCE;
        }

        @DexIgnore
        @Override // com.fossil.z04
        public boolean doEquivalent(Object obj, Object obj2) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.z04
        public int doHash(Object obj) {
            return System.identityHashCode(obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ z04<? super T> equivalence;
        @DexIgnore
        public /* final */ T reference;

        @DexIgnore
        public e(z04<? super T> z04, T t) {
            i14.l(z04);
            this.equivalence = z04;
            this.reference = t;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof e) {
                e eVar = (e) obj;
                if (this.equivalence.equals(eVar.equivalence)) {
                    return this.equivalence.equivalent(this.reference, eVar.reference);
                }
            }
            return false;
        }

        @DexIgnore
        public T get() {
            return this.reference;
        }

        @DexIgnore
        public int hashCode() {
            return this.equivalence.hash(this.reference);
        }

        @DexIgnore
        public String toString() {
            return this.equivalence + ".wrap(" + ((Object) this.reference) + ")";
        }
    }

    @DexIgnore
    public static z04<Object> equals() {
        return b.INSTANCE;
    }

    @DexIgnore
    public static z04<Object> identity() {
        return d.INSTANCE;
    }

    @DexIgnore
    public abstract boolean doEquivalent(T t, T t2);

    @DexIgnore
    public abstract int doHash(T t);

    @DexIgnore
    public final boolean equivalent(T t, T t2) {
        if (t == t2) {
            return true;
        }
        if (t == null || t2 == null) {
            return false;
        }
        return doEquivalent(t, t2);
    }

    @DexIgnore
    public final j14<T> equivalentTo(T t) {
        return new c(this, t);
    }

    @DexIgnore
    public final int hash(T t) {
        if (t == null) {
            return 0;
        }
        return doHash(t);
    }

    @DexIgnore
    public final <F> z04<F> onResultOf(b14<F, ? extends T> b14) {
        return new c14(b14, this);
    }

    @DexIgnore
    public final <S extends T> z04<Iterable<S>> pairwise() {
        return new h14(this);
    }

    @DexIgnore
    public final <S extends T> e<S> wrap(S s) {
        return new e<>(s);
    }
}
