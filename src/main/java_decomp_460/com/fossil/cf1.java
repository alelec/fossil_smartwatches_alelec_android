package com.fossil;

import com.fossil.ua1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cf1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ef1 f605a;
    @DexIgnore
    public /* final */ a b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Map<Class<?>, C0032a<?>> f606a; // = new HashMap();

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cf1$a$a")
        /* renamed from: com.fossil.cf1$a$a  reason: collision with other inner class name */
        public static class C0032a<Model> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ List<af1<Model, ?>> f607a;

            @DexIgnore
            public C0032a(List<af1<Model, ?>> list) {
                this.f607a = list;
            }
        }

        @DexIgnore
        public void a() {
            this.f606a.clear();
        }

        @DexIgnore
        public <Model> List<af1<Model, ?>> b(Class<Model> cls) {
            C0032a<?> aVar = this.f606a.get(cls);
            if (aVar == null) {
                return null;
            }
            return aVar.f607a;
        }

        @DexIgnore
        public <Model> void c(Class<Model> cls, List<af1<Model, ?>> list) {
            if (this.f606a.put(cls, new C0032a<>(list)) != null) {
                throw new IllegalStateException("Already cached loaders for model: " + cls);
            }
        }
    }

    @DexIgnore
    public cf1(ef1 ef1) {
        this.b = new a();
        this.f605a = ef1;
    }

    @DexIgnore
    public cf1(mn0<List<Throwable>> mn0) {
        this(new ef1(mn0));
    }

    @DexIgnore
    public static <A> Class<A> b(A a2) {
        return (Class<A>) a2.getClass();
    }

    @DexIgnore
    public <Model, Data> void a(Class<Model> cls, Class<Data> cls2, bf1<? extends Model, ? extends Data> bf1) {
        synchronized (this) {
            this.f605a.b(cls, cls2, bf1);
            this.b.a();
        }
    }

    @DexIgnore
    public List<Class<?>> c(Class<?> cls) {
        List<Class<?>> g;
        synchronized (this) {
            g = this.f605a.g(cls);
        }
        return g;
    }

    @DexIgnore
    public <A> List<af1<A, ?>> d(A a2) {
        List<af1<A, ?>> e = e(b(a2));
        if (!e.isEmpty()) {
            int size = e.size();
            List<af1<A, ?>> emptyList = Collections.emptyList();
            boolean z = true;
            int i = 0;
            while (i < size) {
                af1<A, ?> af1 = e.get(i);
                if (af1.a(a2)) {
                    if (z) {
                        emptyList = new ArrayList<>(size - i);
                        z = false;
                    }
                    emptyList.add(af1);
                }
                i++;
                z = z;
            }
            if (!emptyList.isEmpty()) {
                return emptyList;
            }
            throw new ua1.c(a2, e);
        }
        throw new ua1.c(a2);
    }

    @DexIgnore
    public final <A> List<af1<A, ?>> e(Class<A> cls) {
        List<af1<A, ?>> b2;
        synchronized (this) {
            b2 = this.b.b(cls);
            if (b2 == null) {
                b2 = Collections.unmodifiableList(this.f605a.e(cls));
                this.b.c(cls, b2);
            }
        }
        return b2;
    }
}
