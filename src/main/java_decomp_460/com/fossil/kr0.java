package com.fossil;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kr0 {
    @DexIgnore
    public static kr0 c; // = new kr0();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<Class<?>, a> f2067a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, Boolean> b; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Map<Lifecycle.a, List<b>> f2068a; // = new HashMap();
        @DexIgnore
        public /* final */ Map<b, Lifecycle.a> b;

        @DexIgnore
        public a(Map<b, Lifecycle.a> map) {
            this.b = map;
            for (Map.Entry<b, Lifecycle.a> entry : map.entrySet()) {
                Lifecycle.a value = entry.getValue();
                List<b> list = this.f2068a.get(value);
                if (list == null) {
                    list = new ArrayList<>();
                    this.f2068a.put(value, list);
                }
                list.add(entry.getKey());
            }
        }

        @DexIgnore
        public static void b(List<b> list, LifecycleOwner lifecycleOwner, Lifecycle.a aVar, Object obj) {
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    list.get(size).a(lifecycleOwner, aVar, obj);
                }
            }
        }

        @DexIgnore
        public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar, Object obj) {
            b(this.f2068a.get(aVar), lifecycleOwner, aVar, obj);
            b(this.f2068a.get(Lifecycle.a.ON_ANY), lifecycleOwner, aVar, obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f2069a;
        @DexIgnore
        public /* final */ Method b;

        @DexIgnore
        public b(int i, Method method) {
            this.f2069a = i;
            this.b = method;
            method.setAccessible(true);
        }

        @DexIgnore
        public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar, Object obj) {
            try {
                int i = this.f2069a;
                if (i == 0) {
                    this.b.invoke(obj, new Object[0]);
                } else if (i == 1) {
                    this.b.invoke(obj, lifecycleOwner);
                } else if (i == 2) {
                    this.b.invoke(obj, lifecycleOwner, aVar);
                }
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Failed to call observer method", e.getCause());
            } catch (IllegalAccessException e2) {
                throw new RuntimeException(e2);
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            return this.f2069a == bVar.f2069a && this.b.getName().equals(bVar.b.getName());
        }

        @DexIgnore
        public int hashCode() {
            return (this.f2069a * 31) + this.b.getName().hashCode();
        }
    }

    @DexIgnore
    public final a a(Class<?> cls, Method[] methodArr) {
        int i;
        boolean z;
        a c2;
        Class<? super Object> superclass = cls.getSuperclass();
        HashMap hashMap = new HashMap();
        if (!(superclass == null || (c2 = c(superclass)) == null)) {
            hashMap.putAll(c2.b);
        }
        for (Class<?> cls2 : cls.getInterfaces()) {
            for (Map.Entry<b, Lifecycle.a> entry : c(cls2).b.entrySet()) {
                e(hashMap, entry.getKey(), entry.getValue(), cls);
            }
        }
        if (methodArr == null) {
            methodArr = b(cls);
        }
        int length = methodArr.length;
        boolean z2 = false;
        int i2 = 0;
        while (i2 < length) {
            Method method = methodArr[i2];
            ms0 ms0 = (ms0) method.getAnnotation(ms0.class);
            if (ms0 == null) {
                z = z2;
            } else {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length <= 0) {
                    i = 0;
                } else if (parameterTypes[0].isAssignableFrom(LifecycleOwner.class)) {
                    i = 1;
                } else {
                    throw new IllegalArgumentException("invalid parameter type. Must be one and instanceof LifecycleOwner");
                }
                Lifecycle.a value = ms0.value();
                if (parameterTypes.length > 1) {
                    if (!parameterTypes[1].isAssignableFrom(Lifecycle.a.class)) {
                        throw new IllegalArgumentException("invalid parameter type. second arg must be an event");
                    } else if (value == Lifecycle.a.ON_ANY) {
                        i = 2;
                    } else {
                        throw new IllegalArgumentException("Second arg is supported only for ON_ANY value");
                    }
                }
                if (parameterTypes.length <= 2) {
                    e(hashMap, new b(i, method), value, cls);
                    z = true;
                } else {
                    throw new IllegalArgumentException("cannot have more than 2 params");
                }
            }
            i2++;
            z2 = z;
        }
        a aVar = new a(hashMap);
        this.f2067a.put(cls, aVar);
        this.b.put(cls, Boolean.valueOf(z2));
        return aVar;
    }

    @DexIgnore
    public final Method[] b(Class<?> cls) {
        try {
            return cls.getDeclaredMethods();
        } catch (NoClassDefFoundError e) {
            throw new IllegalArgumentException("The observer class has some methods that use newer APIs which are not available in the current OS version. Lifecycles cannot access even other methods so you should make sure that your observer classes only access framework classes that are available in your min API level OR use lifecycle:compiler annotation processor.", e);
        }
    }

    @DexIgnore
    public a c(Class<?> cls) {
        a aVar = this.f2067a.get(cls);
        return aVar != null ? aVar : a(cls, null);
    }

    @DexIgnore
    public boolean d(Class<?> cls) {
        Boolean bool = this.b.get(cls);
        if (bool != null) {
            return bool.booleanValue();
        }
        Method[] b2 = b(cls);
        for (Method method : b2) {
            if (((ms0) method.getAnnotation(ms0.class)) != null) {
                a(cls, b2);
                return true;
            }
        }
        this.b.put(cls, Boolean.FALSE);
        return false;
    }

    @DexIgnore
    public final void e(Map<b, Lifecycle.a> map, b bVar, Lifecycle.a aVar, Class<?> cls) {
        Lifecycle.a aVar2 = map.get(bVar);
        if (aVar2 != null && aVar != aVar2) {
            Method method = bVar.b;
            throw new IllegalArgumentException("Method " + method.getName() + " in " + cls.getName() + " already declared with different @OnLifecycleEvent value: previous value " + aVar2 + ", new value " + aVar);
        } else if (aVar2 == null) {
            map.put(bVar, aVar);
        }
    }
}
