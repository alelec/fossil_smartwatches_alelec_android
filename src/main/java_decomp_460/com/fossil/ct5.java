package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ct5 extends iq4<a, c, b> {
    @DexIgnore
    public /* final */ PortfolioApp d;
    @DexIgnore
    public /* final */ GuestApiService e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f652a;

        @DexIgnore
        public a(String str) {
            pq7.c(str, "deviceModel");
            this.f652a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f652a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f653a;

        @DexIgnore
        public c(String str) {
            pq7.c(str, "latestFwVersion");
            this.f653a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f653a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase", f = "DownloadFirmwareByDeviceModelUsecase.kt", l = {36, 51}, m = "run")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ct5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ct5 ct5, qn7 qn7) {
            super(qn7);
            this.this$0 = ct5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$run$repoResponse$1", f = "DownloadFirmwareByDeviceModelUsecase.kt", l = {36}, m = "invokeSuspend")
    public static final class e extends ko7 implements rp7<qn7<? super q88<ApiResponse<Firmware>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceModel;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ ct5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ct5 ct5, String str, qn7 qn7) {
            super(1, qn7);
            this.this$0 = ct5;
            this.$deviceModel = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new e(this.this$0, this.$deviceModel, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<Firmware>>> qn7) {
            return ((e) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                GuestApiService guestApiService = this.this$0.e;
                String P = this.this$0.d.P();
                String str = this.$deviceModel;
                this.label = 1;
                Object firmwares = guestApiService.getFirmwares(P, str, "android", false, this);
                return firmwares == d ? d : firmwares;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public ct5(PortfolioApp portfolioApp, GuestApiService guestApiService) {
        pq7.c(portfolioApp, "mApp");
        pq7.c(guestApiService, "mGuestApiService");
        this.d = portfolioApp;
        this.e = guestApiService;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "DownloadFirmwareByDeviceModelUsecase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0175  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* renamed from: o */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.ct5.a r14, com.fossil.qn7<java.lang.Object> r15) {
        /*
        // Method dump skipped, instructions count: 464
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ct5.k(com.fossil.ct5$a, com.fossil.qn7):java.lang.Object");
    }
}
