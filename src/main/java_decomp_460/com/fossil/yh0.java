package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yh0 extends Resources {
    @DexIgnore
    public static boolean b;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ WeakReference<Context> f4315a;

    @DexIgnore
    public yh0(Context context, Resources resources) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.f4315a = new WeakReference<>(context);
    }

    @DexIgnore
    public static boolean a() {
        return b;
    }

    @DexIgnore
    public static boolean b() {
        return a() && Build.VERSION.SDK_INT <= 20;
    }

    @DexIgnore
    public final Drawable c(int i) {
        return super.getDrawable(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public Drawable getDrawable(int i) throws Resources.NotFoundException {
        Context context = this.f4315a.get();
        return context != null ? jh0.h().t(context, this, i) : super.getDrawable(i);
    }
}
