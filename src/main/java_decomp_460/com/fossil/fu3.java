package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fu3<TResult, TContinuationResult> implements gt3, it3, jt3<TContinuationResult>, hu3<TResult> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f1207a;
    @DexIgnore
    public /* final */ mt3<TResult, TContinuationResult> b;
    @DexIgnore
    public /* final */ lu3<TContinuationResult> c;

    @DexIgnore
    public fu3(Executor executor, mt3<TResult, TContinuationResult> mt3, lu3<TContinuationResult> lu3) {
        this.f1207a = executor;
        this.b = mt3;
        this.c = lu3;
    }

    @DexIgnore
    @Override // com.fossil.hu3
    public final void a(nt3<TResult> nt3) {
        this.f1207a.execute(new gu3(this, nt3));
    }

    @DexIgnore
    @Override // com.fossil.gt3
    public final void onCanceled() {
        this.c.v();
    }

    @DexIgnore
    @Override // com.fossil.it3
    public final void onFailure(Exception exc) {
        this.c.t(exc);
    }

    @DexIgnore
    @Override // com.fossil.jt3
    public final void onSuccess(TContinuationResult tcontinuationresult) {
        this.c.u(tcontinuationresult);
    }
}
