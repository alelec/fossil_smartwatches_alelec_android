package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.fossil.a21;
import com.fossil.g41;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z11 implements g21, k11, g41.b {
    @DexIgnore
    public static /* final */ String k; // = x01.f("DelayMetCommandHandler");
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ a21 e;
    @DexIgnore
    public /* final */ h21 f;
    @DexIgnore
    public /* final */ Object g; // = new Object();
    @DexIgnore
    public int h; // = 0;
    @DexIgnore
    public PowerManager.WakeLock i;
    @DexIgnore
    public boolean j; // = false;

    @DexIgnore
    public z11(Context context, int i2, String str, a21 a21) {
        this.b = context;
        this.c = i2;
        this.e = a21;
        this.d = str;
        this.f = new h21(this.b, a21.f(), this);
    }

    @DexIgnore
    @Override // com.fossil.g41.b
    public void a(String str) {
        x01.c().a(k, String.format("Exceeded time limits on execution for %s", str), new Throwable[0]);
        g();
    }

    @DexIgnore
    @Override // com.fossil.g21
    public void b(List<String> list) {
        g();
    }

    @DexIgnore
    public final void c() {
        synchronized (this.g) {
            this.f.e();
            this.e.h().c(this.d);
            if (this.i != null && this.i.isHeld()) {
                x01.c().a(k, String.format("Releasing wakelock %s for WorkSpec %s", this.i, this.d), new Throwable[0]);
                this.i.release();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.k11
    public void d(String str, boolean z) {
        x01.c().a(k, String.format("onExecuted %s, %s", str, Boolean.valueOf(z)), new Throwable[0]);
        c();
        if (z) {
            Intent f2 = x11.f(this.b, this.d);
            a21 a21 = this.e;
            a21.k(new a21.b(a21, f2, this.c));
        }
        if (this.j) {
            Intent a2 = x11.a(this.b);
            a21 a212 = this.e;
            a212.k(new a21.b(a212, a2, this.c));
        }
    }

    @DexIgnore
    public void e() {
        this.i = d41.b(this.b, String.format("%s (%s)", this.d, Integer.valueOf(this.c)));
        x01.c().a(k, String.format("Acquiring wakelock %s for WorkSpec %s", this.i, this.d), new Throwable[0]);
        this.i.acquire();
        o31 o = this.e.g().p().j().o(this.d);
        if (o == null) {
            g();
            return;
        }
        boolean b2 = o.b();
        this.j = b2;
        if (!b2) {
            x01.c().a(k, String.format("No constraints for %s", this.d), new Throwable[0]);
            f(Collections.singletonList(this.d));
            return;
        }
        this.f.d(Collections.singletonList(o));
    }

    @DexIgnore
    @Override // com.fossil.g21
    public void f(List<String> list) {
        if (list.contains(this.d)) {
            synchronized (this.g) {
                if (this.h == 0) {
                    this.h = 1;
                    x01.c().a(k, String.format("onAllConstraintsMet for %s", this.d), new Throwable[0]);
                    if (this.e.e().i(this.d)) {
                        this.e.h().b(this.d, 600000, this);
                    } else {
                        c();
                    }
                } else {
                    x01.c().a(k, String.format("Already started work for %s", this.d), new Throwable[0]);
                }
            }
        }
    }

    @DexIgnore
    public final void g() {
        synchronized (this.g) {
            if (this.h < 2) {
                this.h = 2;
                x01.c().a(k, String.format("Stopping work for WorkSpec %s", this.d), new Throwable[0]);
                this.e.k(new a21.b(this.e, x11.g(this.b, this.d), this.c));
                if (this.e.e().f(this.d)) {
                    x01.c().a(k, String.format("WorkSpec %s needs to be rescheduled", this.d), new Throwable[0]);
                    this.e.k(new a21.b(this.e, x11.f(this.b, this.d), this.c));
                } else {
                    x01.c().a(k, String.format("Processor does not have WorkSpec %s. No need to reschedule ", this.d), new Throwable[0]);
                }
            } else {
                x01.c().a(k, String.format("Already stopped work for %s", this.d), new Throwable[0]);
            }
        }
    }
}
