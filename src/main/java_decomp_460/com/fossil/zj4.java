package com.fossil;

import java.io.ObjectStreamException;
import java.math.BigDecimal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zj4 extends Number {
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    public zj4(String str) {
        this.value = str;
    }

    @DexIgnore
    private Object writeReplace() throws ObjectStreamException {
        return new BigDecimal(this.value);
    }

    @DexIgnore
    public double doubleValue() {
        return Double.parseDouble(this.value);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zj4)) {
            return false;
        }
        String str = this.value;
        String str2 = ((zj4) obj).value;
        return str == str2 || str.equals(str2);
    }

    @DexIgnore
    public float floatValue() {
        return Float.parseFloat(this.value);
    }

    @DexIgnore
    public int hashCode() {
        return this.value.hashCode();
    }

    @DexIgnore
    public int intValue() {
        try {
            return Integer.parseInt(this.value);
        } catch (NumberFormatException e) {
            try {
                return (int) Long.parseLong(this.value);
            } catch (NumberFormatException e2) {
                return new BigDecimal(this.value).intValue();
            }
        }
    }

    @DexIgnore
    public long longValue() {
        try {
            return Long.parseLong(this.value);
        } catch (NumberFormatException e) {
            return new BigDecimal(this.value).longValue();
        }
    }

    @DexIgnore
    public String toString() {
        return this.value;
    }
}
