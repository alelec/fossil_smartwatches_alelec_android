package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cv0;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gz4 extends cv0.i {
    @DexIgnore
    public /* final */ int f; // = 20;
    @DexIgnore
    public List<c> g; // = new ArrayList();
    @DexIgnore
    public int h; // = -1;
    @DexIgnore
    public float i; // = 0.5f;
    @DexIgnore
    public Map<Integer, List<c>> j; // = new LinkedHashMap();
    @DexIgnore
    public GestureDetector k; // = new GestureDetector(this.o.getContext(), this.m);
    @DexIgnore
    public f l; // = new f();
    @DexIgnore
    public /* final */ d m; // = new d(this);
    @DexIgnore
    public /* final */ View.OnTouchListener n; // = new e(this);
    @DexIgnore
    public /* final */ RecyclerView o;

    @DexIgnore
    public interface a {
        @DexIgnore
        void T3(int i, b bVar);
    }

    @DexIgnore
    public enum b {
        UNFRIEND,
        BLOCK,
        UNBLOCK,
        CANCEL
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f1399a;
        @DexIgnore
        public RectF b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ Integer d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ b f;
        @DexIgnore
        public /* final */ a g;

        @DexIgnore
        public c(String str, Integer num, int i, b bVar, a aVar) {
            pq7.c(str, "text");
            pq7.c(bVar, "type");
            pq7.c(aVar, "listener");
            this.c = str;
            this.d = num;
            this.e = i;
            this.f = bVar;
            this.g = aVar;
        }

        @DexIgnore
        public final int a(float f2, float f3) {
            RectF rectF = this.b;
            if (rectF != null) {
                if (rectF == null) {
                    pq7.i();
                    throw null;
                } else if (rectF.contains(f2, f3)) {
                    this.g.T3(this.f1399a, this.f);
                    return this.f1399a;
                }
            }
            return -1;
        }

        @DexIgnore
        public final void b(Canvas canvas, RectF rectF, int i) {
            pq7.c(canvas, "canvas");
            pq7.c(rectF, "rect");
            Paint paint = new Paint(1);
            paint.setColor(this.e);
            canvas.drawRect(rectF, paint);
            if (this.d == null) {
                paint.setColor(-1);
                paint.setTextSize(28.0f);
                Rect rect = new Rect();
                paint.setTextAlign(Paint.Align.LEFT);
                String str = this.c;
                paint.getTextBounds(str, 0, str.length(), rect);
                canvas.drawText(this.c, (((rectF.width() / 2.0f) - (((float) rect.width()) / 2.0f)) - ((float) rect.left)) + rectF.left, (((rectF.height() / 2.0f) + (((float) rect.height()) / 2.0f)) - ((float) rect.bottom)) + rectF.top, paint);
            } else {
                Bitmap decodeResource = BitmapFactory.decodeResource(PortfolioApp.h0.c().getResources(), this.d.intValue());
                pq7.b(decodeResource, "bitmap");
                canvas.drawBitmap(decodeResource, ((rectF.width() / 2.0f) - (((float) decodeResource.getWidth()) / 2.0f)) + rectF.left, rectF.top + ((rectF.height() / 2.0f) - (((float) decodeResource.getHeight()) / 2.0f)), paint);
            }
            this.b = rectF;
            this.f1399a = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public /* final */ /* synthetic */ gz4 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(gz4 gz4) {
            this.b = gz4;
        }

        @DexIgnore
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            pq7.c(motionEvent, Constants.EVENT);
            if (this.b.h == -1) {
                return true;
            }
            for (c cVar : this.b.g) {
                int a2 = cVar.a(motionEvent.getX(), motionEvent.getY());
                if (a2 == this.b.h) {
                    this.b.h = -1;
                    continue;
                }
                if (a2 != -1) {
                    RecyclerView.g adapter = this.b.o.getAdapter();
                    if (adapter != null) {
                        adapter.notifyItemChanged(a2);
                    }
                    this.b.l.clear();
                    return true;
                }
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnTouchListener {
        @DexIgnore
        public /* final */ /* synthetic */ gz4 b;

        @DexIgnore
        public e(gz4 gz4) {
            this.b = gz4;
        }

        @DexIgnore
        public final boolean onTouch(View view, MotionEvent motionEvent) {
            if (this.b.h >= 0) {
                pq7.b(motionEvent, Constants.EVENT);
                Point point = new Point((int) motionEvent.getRawX(), (int) motionEvent.getRawY());
                RecyclerView.ViewHolder findViewHolderForAdapterPosition = this.b.o.findViewHolderForAdapterPosition(this.b.h);
                if (findViewHolderForAdapterPosition != null) {
                    pq7.b(findViewHolderForAdapterPosition, "recyclerView.findViewHol\u2026urn@OnTouchListener false");
                    View view2 = findViewHolderForAdapterPosition.itemView;
                    pq7.b(view2, "swipedViewHolder.itemView");
                    Rect rect = new Rect();
                    view2.getGlobalVisibleRect(rect);
                    if (motionEvent.getAction() == 0 || motionEvent.getAction() == 1 || motionEvent.getAction() == 2) {
                        int i = rect.top;
                        int i2 = point.y;
                        if (i >= i2 || rect.bottom <= i2) {
                            this.b.l.add(this.b.h);
                            this.b.h = -1;
                            this.b.O();
                        } else {
                            gz4.E(this.b).onTouchEvent(motionEvent);
                        }
                    }
                }
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends LinkedList<Integer> {
        @DexIgnore
        public boolean add(int i) {
            if (contains((Object) Integer.valueOf(i))) {
                return false;
            }
            return super.add((f) Integer.valueOf(i));
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, java.util.AbstractList, java.util.Queue, java.util.LinkedList, java.util.Deque
        public /* bridge */ /* synthetic */ boolean add(Integer num) {
            return add(num.intValue());
        }

        @DexIgnore
        public /* bridge */ boolean contains(Integer num) {
            return super.contains((Object) num);
        }

        @DexIgnore
        public final /* bridge */ boolean contains(Object obj) {
            if (obj instanceof Integer) {
                return contains((Integer) obj);
            }
            return false;
        }

        @DexIgnore
        public /* bridge */ int getSize() {
            return super.size();
        }

        @DexIgnore
        public /* bridge */ int indexOf(Integer num) {
            return super.indexOf((Object) num);
        }

        @DexIgnore
        public final /* bridge */ int indexOf(Object obj) {
            if (obj instanceof Integer) {
                return indexOf((Integer) obj);
            }
            return -1;
        }

        @DexIgnore
        public /* bridge */ int lastIndexOf(Integer num) {
            return super.lastIndexOf((Object) num);
        }

        @DexIgnore
        public final /* bridge */ int lastIndexOf(Object obj) {
            if (obj instanceof Integer) {
                return lastIndexOf((Integer) obj);
            }
            return -1;
        }

        @DexIgnore
        @Override // java.util.List, java.util.AbstractList, java.util.LinkedList, java.util.AbstractSequentialList
        public final /* bridge */ Integer remove(int i) {
            return removeAt(i);
        }

        @DexIgnore
        public /* bridge */ boolean remove(Integer num) {
            return super.remove((Object) num);
        }

        @DexIgnore
        @Override // java.util.List, java.util.LinkedList
        public final /* bridge */ boolean remove(Object obj) {
            if (obj instanceof Integer) {
                return remove((Integer) obj);
            }
            return false;
        }

        @DexIgnore
        public /* bridge */ Integer removeAt(int i) {
            return (Integer) super.remove(i);
        }

        @DexIgnore
        public final /* bridge */ int size() {
            return getSize();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gz4(RecyclerView recyclerView) {
        super(0, 4);
        pq7.c(recyclerView, "recyclerView");
        this.o = recyclerView;
        this.o.setOnTouchListener(this.n);
        L();
    }

    @DexIgnore
    public static final /* synthetic */ GestureDetector E(gz4 gz4) {
        GestureDetector gestureDetector = gz4.k;
        if (gestureDetector != null) {
            return gestureDetector;
        }
        pq7.n("gestureDetector");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cv0.f
    public void B(RecyclerView.ViewHolder viewHolder, int i2) {
        List<c> arrayList;
        pq7.c(viewHolder, "viewHolder");
        int adapterPosition = viewHolder.getAdapterPosition();
        int i3 = this.h;
        if (i3 != adapterPosition) {
            this.l.add(i3);
        }
        this.h = adapterPosition;
        if (this.j.containsKey(Integer.valueOf(adapterPosition))) {
            List<c> list = this.j.get(Integer.valueOf(this.h));
            if (list == null || (arrayList = pm7.j0(list)) == null) {
                arrayList = new ArrayList<>();
            }
            this.g = arrayList;
        }
        this.j.clear();
        View view = viewHolder.itemView;
        pq7.b(view, "viewHolder.itemView");
        this.i = ((float) this.g.size()) * 0.5f * ((float) (view.getWidth() + this.f));
        O();
    }

    @DexIgnore
    public final void L() {
        new cv0(this).g(this.o);
    }

    @DexIgnore
    public final void M(Canvas canvas, View view, List<c> list, int i2, float f2) {
        float size = (((float) -1) * f2) / ((float) list.size());
        float right = (float) view.getRight();
        for (c cVar : list) {
            float f3 = right - size;
            cVar.b(canvas, new RectF(f3, (float) view.getTop(), right, (float) view.getBottom()), i2);
            right = f3;
        }
    }

    @DexIgnore
    public abstract void N(RecyclerView.ViewHolder viewHolder, List<c> list);

    @DexIgnore
    public final void O() {
        synchronized (this) {
            while (true) {
                f fVar = this.l;
                if (!(fVar == null || fVar.isEmpty())) {
                    Integer num = (Integer) this.l.poll();
                    if (num != null && pq7.d(num.intValue(), -1) > 0) {
                        RecyclerView.g adapter = this.o.getAdapter();
                        if (adapter != null) {
                            adapter.notifyItemChanged(num.intValue());
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.cv0.f
    public float l(float f2) {
        return 0.1f * f2;
    }

    @DexIgnore
    @Override // com.fossil.cv0.f
    public float m(RecyclerView.ViewHolder viewHolder) {
        pq7.c(viewHolder, "viewHolder");
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.cv0.f
    public float n(float f2) {
        return 5.0f * f2;
    }

    @DexIgnore
    @Override // com.fossil.cv0.f
    public void u(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f2, float f3, int i2, boolean z) {
        float f4;
        pq7.c(canvas, "c");
        pq7.c(recyclerView, "recyclerView");
        pq7.c(viewHolder, "viewHolder");
        int adapterPosition = viewHolder.getAdapterPosition();
        View view = viewHolder.itemView;
        pq7.b(view, "viewHolder.itemView");
        if (adapterPosition < 0) {
            this.h = adapterPosition;
            return;
        }
        if (i2 == 1) {
            if (f2 < ((float) 0)) {
                ArrayList arrayList = new ArrayList();
                N(viewHolder, arrayList);
                this.j.put(Integer.valueOf(adapterPosition), arrayList);
                float size = ((((float) arrayList.size()) * f2) * ((float) (view.getHeight() + this.f))) / ((float) view.getWidth());
                M(canvas, view, arrayList, adapterPosition, size);
                f4 = size;
                super.u(canvas, recyclerView, viewHolder, f4, f3, i2, z);
            }
            this.h = -1;
        }
        f4 = f2;
        super.u(canvas, recyclerView, viewHolder, f4, f3, i2, z);
    }

    @DexIgnore
    @Override // com.fossil.cv0.f
    public boolean y(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
        pq7.c(recyclerView, "recyclerView");
        pq7.c(viewHolder, "viewHolder");
        pq7.c(viewHolder2, "target");
        return false;
    }
}
