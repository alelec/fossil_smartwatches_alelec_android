package com.fossil;

import android.text.TextUtils;
import com.portfolio.platform.PortfolioApp;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m47 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f2302a;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b;

        /*
        static {
            int[] iArr = new int[c.values().length];
            b = iArr;
            try {
                iArr[c.CHAT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                b[c.PRIVACY.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                b[c.SUPPORT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                b[c.STORE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                b[c.TERMS.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                b[c.REPAIR_CENTER.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                b[c.CALL.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                b[c.FAQ.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                b[c.DEVICE_FEATURE.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                b[c.SUPPORT_MAIL.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                b[c.SOURCE_LICENSES.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                b[c.FEATURES.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            int[] iArr2 = new int[b.values().length];
            f2302a = iArr2;
            try {
                iArr2[b.USING_YOUR_WATCH.ordinal()] = 1;
            } catch (NoSuchFieldError e13) {
            }
            try {
                f2302a[b.LOW_BATTERY.ordinal()] = 2;
            } catch (NoSuchFieldError e14) {
            }
            try {
                f2302a[b.SHOP_BATTERY.ordinal()] = 3;
            } catch (NoSuchFieldError e15) {
            }
            try {
                f2302a[b.HOUR_TIME_24.ordinal()] = 4;
            } catch (NoSuchFieldError e16) {
            }
            try {
                f2302a[b.ACTIVITY.ordinal()] = 5;
            } catch (NoSuchFieldError e17) {
            }
            try {
                f2302a[b.ALARM.ordinal()] = 6;
            } catch (NoSuchFieldError e18) {
            }
            try {
                f2302a[b.DATE.ordinal()] = 7;
            } catch (NoSuchFieldError e19) {
            }
            try {
                f2302a[b.NOTIFICATIONS.ordinal()] = 8;
            } catch (NoSuchFieldError e20) {
            }
            try {
                f2302a[b.Q_LINK.ordinal()] = 9;
            } catch (NoSuchFieldError e21) {
            }
            try {
                f2302a[b.SECOND_TIME_ZONE.ordinal()] = 10;
            } catch (NoSuchFieldError e22) {
            }
            try {
                f2302a[b.GOAL_TRACKING.ordinal()] = 11;
            } catch (NoSuchFieldError e23) {
            }
            try {
                f2302a[b.CUSTOMIZE_DEVICE.ordinal()] = 12;
            } catch (NoSuchFieldError e24) {
            }
            try {
                f2302a[b.STOP_WATCH.ordinal()] = 13;
            } catch (NoSuchFieldError e25) {
            }
        }
        */
    }

    @DexIgnore
    public enum b {
        USING_YOUR_WATCH,
        LOW_BATTERY,
        SHOP_BATTERY,
        HOUR_TIME_24,
        ACTIVITY,
        ALARM,
        DATE,
        NOTIFICATIONS,
        Q_LINK,
        SECOND_TIME_ZONE,
        GOAL_TRACKING,
        CUSTOMIZE_DEVICE,
        STOP_WATCH
    }

    @DexIgnore
    public enum c {
        PRIVACY,
        SUPPORT,
        STORE,
        TERMS,
        CALL,
        FEATURES,
        REPAIR_CENTER,
        DEVICE_FEATURE,
        CHAT,
        SUPPORT_MAIL,
        FAQ,
        SOURCE_LICENSES
    }

    @DexIgnore
    public static String a(c cVar, b bVar) {
        return b(cVar, bVar, null);
    }

    @DexIgnore
    public static String b(c cVar, b bVar, String str) {
        StringBuilder sb = new StringBuilder(h37.b.a(1));
        if (TextUtils.isEmpty(str)) {
            str = PortfolioApp.d0.J();
        }
        switch (a.b[cVar.ordinal()]) {
            case 1:
                return "http://chat.fossil.com/chat/";
            case 2:
                sb.append("/privacy");
                break;
            case 3:
                sb.append("/support");
                break;
            case 4:
                sb.append("/store");
                break;
            case 5:
                sb.append("/terms");
                break;
            case 6:
                sb.append("/service_centers");
                break;
            case 7:
                sb.append("/call");
                break;
            case 8:
                sb.append("/faq");
                break;
            case 9:
                sb.append("/device_features/customize_device");
                break;
            case 10:
                sb.append("/support-emails");
                break;
            case 11:
                sb.append("/open-source-licenses");
                break;
            case 12:
                sb.append(nk5.o.A(str) ? "/tracker" : "/hybrid");
                if (bVar == null) {
                    sb.append("/using_your_watch");
                    break;
                } else {
                    switch (a.f2302a[bVar.ordinal()]) {
                        case 1:
                            sb.append("/using_your_watch");
                            break;
                        case 2:
                            sb.append("/low_battery");
                            break;
                        case 3:
                            sb.append("/shop_battery");
                            break;
                        case 4:
                            sb.append("/24_hour_time");
                            break;
                        case 5:
                            sb.append("/activity");
                            break;
                        case 6:
                            sb.append("/alarm");
                            break;
                        case 7:
                            sb.append("/date");
                            break;
                        case 8:
                            sb.append("/notifications");
                            break;
                        case 9:
                            sb.append("/link");
                            break;
                        case 10:
                            sb.append("/second_time_zone");
                            break;
                        case 11:
                            sb.append("/service_centers");
                            break;
                        case 12:
                            sb.append("/customize_device");
                            break;
                        case 13:
                            sb.append("/stopwatch");
                            break;
                        default:
                            sb.append("/using_your_watch");
                            break;
                    }
                }
        }
        sb.append(String.format("?locale=%s&platform=android&serialNumber=%s&version=%s", c(), str, "2"));
        return sb.toString();
    }

    @DexIgnore
    public static String c() {
        Locale locale = Locale.getDefault();
        String language = locale.getLanguage();
        String country = locale.getCountry();
        if (TextUtils.isEmpty(language)) {
            return "";
        }
        if (TextUtils.isEmpty(country)) {
            return language;
        }
        return String.format(Locale.US, "%s_%s", language, country);
    }
}
