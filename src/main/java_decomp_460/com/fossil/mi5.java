package com.fossil;

import com.fossil.fitness.WorkoutType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum mi5 {
    UNKNOWN("unknown"),
    RUNNING("running"),
    CYCLING("cycling"),
    TREADMILL("treadmill"),
    ELLIPTICAL("elliptical"),
    WEIGHTS("weights"),
    WORKOUT("workout"),
    WALKING("walking"),
    ROWING("rowing"),
    HIKING("hiking"),
    YOGA("yoga"),
    SWIMMING("swimming"),
    AEROBIC("aerobic"),
    SPINNING("spinning");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final mi5 a(String str) {
            mi5 mi5;
            pq7.c(str, "value");
            mi5[] values = mi5.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    mi5 = null;
                    break;
                }
                mi5 = values[i];
                String mValue = mi5.getMValue();
                String lowerCase = str.toLowerCase();
                pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                if (pq7.a(mValue, lowerCase)) {
                    break;
                }
                i++;
            }
            return mi5 != null ? mi5 : mi5.UNKNOWN;
        }

        @DexIgnore
        public final mi5 b(int i) {
            return i == WorkoutType.RUNNING.ordinal() ? mi5.RUNNING : i == WorkoutType.CYCLING.ordinal() ? mi5.CYCLING : i == WorkoutType.TREADMILL.ordinal() ? mi5.TREADMILL : i == WorkoutType.ELLIPTICAL.ordinal() ? mi5.ELLIPTICAL : i == WorkoutType.WEIGHTS.ordinal() ? mi5.WEIGHTS : i == WorkoutType.WORKOUT.ordinal() ? mi5.WORKOUT : i == WorkoutType.WALKING.ordinal() ? mi5.WALKING : i == WorkoutType.ROWING.ordinal() ? mi5.ROWING : i == WorkoutType.HIKING.ordinal() ? mi5.HIKING : i == WorkoutType.YOGA.ordinal() ? mi5.YOGA : i == WorkoutType.SWIMMING.ordinal() ? mi5.SWIMMING : i == WorkoutType.AEROBIC.ordinal() ? mi5.AEROBIC : i == WorkoutType.SPINNING.ordinal() ? mi5.SPINNING : mi5.UNKNOWN;
        }

        @DexIgnore
        public final WorkoutType c(mi5 mi5) {
            if (mi5 != null) {
                switch (li5.f2201a[mi5.ordinal()]) {
                    case 1:
                        return WorkoutType.RUNNING;
                    case 2:
                        return WorkoutType.CYCLING;
                    case 3:
                        return WorkoutType.TREADMILL;
                    case 4:
                        return WorkoutType.ELLIPTICAL;
                    case 5:
                        return WorkoutType.WEIGHTS;
                    case 6:
                        return WorkoutType.WORKOUT;
                    case 7:
                        return WorkoutType.WALKING;
                    case 8:
                        return WorkoutType.ROWING;
                    case 9:
                        return WorkoutType.HIKING;
                    case 10:
                        return WorkoutType.YOGA;
                    case 11:
                        return WorkoutType.SWIMMING;
                    case 12:
                        return WorkoutType.AEROBIC;
                    case 13:
                        return WorkoutType.SPINNING;
                }
            }
            return WorkoutType.UNKNOWN;
        }
    }

    @DexIgnore
    public mi5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        pq7.c(str, "<set-?>");
        this.mValue = str;
    }
}
