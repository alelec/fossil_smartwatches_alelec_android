package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ph6 implements MembersInjector<GoalTrackingOverviewFragment> {
    @DexIgnore
    public static void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment, mh6 mh6) {
        goalTrackingOverviewFragment.h = mh6;
    }

    @DexIgnore
    public static void b(GoalTrackingOverviewFragment goalTrackingOverviewFragment, xh6 xh6) {
        goalTrackingOverviewFragment.j = xh6;
    }

    @DexIgnore
    public static void c(GoalTrackingOverviewFragment goalTrackingOverviewFragment, di6 di6) {
        goalTrackingOverviewFragment.i = di6;
    }
}
