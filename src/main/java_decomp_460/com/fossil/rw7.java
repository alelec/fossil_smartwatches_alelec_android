package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rw7 implements sw7 {
    @DexIgnore
    public /* final */ kx7 b;

    @DexIgnore
    public rw7(kx7 kx7) {
        this.b = kx7;
    }

    @DexIgnore
    @Override // com.fossil.sw7
    public kx7 b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.sw7
    public boolean isActive() {
        return false;
    }

    @DexIgnore
    public String toString() {
        return nv7.c() ? b().x("New") : super.toString();
    }
}
