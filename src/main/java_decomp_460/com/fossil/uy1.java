package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uy1<T> {
    @DexIgnore
    public static <T> uy1<T> d(T t) {
        return new sy1(null, t, vy1.DEFAULT);
    }

    @DexIgnore
    public static <T> uy1<T> e(T t) {
        return new sy1(null, t, vy1.VERY_LOW);
    }

    @DexIgnore
    public static <T> uy1<T> f(T t) {
        return new sy1(null, t, vy1.HIGHEST);
    }

    @DexIgnore
    public abstract Integer a();

    @DexIgnore
    public abstract T b();

    @DexIgnore
    public abstract vy1 c();
}
