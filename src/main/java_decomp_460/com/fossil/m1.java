package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.tk1;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m1 extends BroadcastReceiver {
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            Serializable serializableExtra = intent.getSerializableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_STATE");
            if (serializableExtra != null) {
                tk1.c cVar = (tk1.c) serializableExtra;
                Serializable serializableExtra2 = intent.getSerializableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_STATE");
                if (serializableExtra2 != null) {
                    zw.i.e((tk1.c) serializableExtra2);
                    return;
                }
                throw new il7("null cannot be cast to non-null type com.fossil.blesdk.adapter.BluetoothLeAdapter.State");
            }
            throw new il7("null cannot be cast to non-null type com.fossil.blesdk.adapter.BluetoothLeAdapter.State");
        }
    }
}
