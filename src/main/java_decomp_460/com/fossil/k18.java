package com.fossil;

import com.fossil.u18;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k18 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f1854a; // = 64;
    @DexIgnore
    public int b; // = 5;
    @DexIgnore
    public Runnable c;
    @DexIgnore
    public ExecutorService d;
    @DexIgnore
    public /* final */ Deque<u18.b> e; // = new ArrayDeque();
    @DexIgnore
    public /* final */ Deque<u18.b> f; // = new ArrayDeque();
    @DexIgnore
    public /* final */ Deque<u18> g; // = new ArrayDeque();

    @DexIgnore
    public k18() {
    }

    @DexIgnore
    public k18(ExecutorService executorService) {
        this.d = executorService;
    }

    @DexIgnore
    public void a(u18.b bVar) {
        synchronized (this) {
            this.e.add(bVar);
        }
        g();
    }

    @DexIgnore
    public void b(u18 u18) {
        synchronized (this) {
            this.g.add(u18);
        }
    }

    @DexIgnore
    public ExecutorService c() {
        ExecutorService executorService;
        synchronized (this) {
            if (this.d == null) {
                this.d = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), b28.G("OkHttp Dispatcher", false));
            }
            executorService = this.d;
        }
        return executorService;
    }

    @DexIgnore
    public final <T> void d(Deque<T> deque, T t) {
        Runnable runnable;
        synchronized (this) {
            if (deque.remove(t)) {
                runnable = this.c;
            } else {
                throw new AssertionError("Call wasn't in-flight!");
            }
        }
        if (!g() && runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void e(u18.b bVar) {
        d(this.f, bVar);
    }

    @DexIgnore
    public void f(u18 u18) {
        d(this.g, u18);
    }

    @DexIgnore
    public final boolean g() {
        boolean z;
        ArrayList arrayList = new ArrayList();
        synchronized (this) {
            Iterator<u18.b> it = this.e.iterator();
            while (it.hasNext()) {
                u18.b next = it.next();
                if (this.f.size() >= this.f1854a) {
                    break;
                } else if (i(next) < this.b) {
                    it.remove();
                    arrayList.add(next);
                    this.f.add(next);
                }
            }
            z = h() > 0;
        }
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            ((u18.b) arrayList.get(i)).l(c());
        }
        return z;
    }

    @DexIgnore
    public int h() {
        int size;
        int size2;
        synchronized (this) {
            size = this.f.size();
            size2 = this.g.size();
        }
        return size + size2;
    }

    @DexIgnore
    public final int i(u18.b bVar) {
        int i = 0;
        for (u18.b bVar2 : this.f) {
            if (!bVar2.m().g && bVar2.n().equals(bVar.n())) {
                i++;
            }
        }
        return i;
    }

    @DexIgnore
    public void j(int i) {
        if (i >= 1) {
            synchronized (this) {
                this.f1854a = i;
            }
            g();
            return;
        }
        throw new IllegalArgumentException("max < 1: " + i);
    }

    @DexIgnore
    public void k(int i) {
        if (i >= 1) {
            synchronized (this) {
                this.b = i;
            }
            g();
            return;
        }
        throw new IllegalArgumentException("max < 1: " + i);
    }
}
