package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gf3 implements Parcelable.Creator<qe3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ qe3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i = 0;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        int i2 = 0;
        ArrayList arrayList = null;
        ce3 ce3 = null;
        ce3 ce32 = null;
        ArrayList arrayList2 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 2:
                    arrayList2 = ad2.j(parcel, t, LatLng.CREATOR);
                    break;
                case 3:
                    f = ad2.r(parcel, t);
                    break;
                case 4:
                    i = ad2.v(parcel, t);
                    break;
                case 5:
                    f2 = ad2.r(parcel, t);
                    break;
                case 6:
                    z = ad2.m(parcel, t);
                    break;
                case 7:
                    z2 = ad2.m(parcel, t);
                    break;
                case 8:
                    z3 = ad2.m(parcel, t);
                    break;
                case 9:
                    ce32 = (ce3) ad2.e(parcel, t, ce3.CREATOR);
                    break;
                case 10:
                    ce3 = (ce3) ad2.e(parcel, t, ce3.CREATOR);
                    break;
                case 11:
                    i2 = ad2.v(parcel, t);
                    break;
                case 12:
                    arrayList = ad2.j(parcel, t, me3.CREATOR);
                    break;
                default:
                    ad2.B(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new qe3(arrayList2, f, i, f2, z, z2, z3, ce32, ce3, i2, arrayList);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ qe3[] newArray(int i) {
        return new qe3[i];
    }
}
