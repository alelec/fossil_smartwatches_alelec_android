package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ot3<TResult> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ lu3<TResult> f2720a; // = new lu3<>();

    @DexIgnore
    public ot3() {
    }

    @DexIgnore
    public ot3(dt3 dt3) {
        dt3.b(new ju3(this));
    }

    @DexIgnore
    public nt3<TResult> a() {
        return this.f2720a;
    }

    @DexIgnore
    public void b(Exception exc) {
        this.f2720a.t(exc);
    }

    @DexIgnore
    public void c(TResult tresult) {
        this.f2720a.u(tresult);
    }

    @DexIgnore
    public boolean d(Exception exc) {
        return this.f2720a.x(exc);
    }

    @DexIgnore
    public boolean e(TResult tresult) {
        return this.f2720a.y(tresult);
    }
}
