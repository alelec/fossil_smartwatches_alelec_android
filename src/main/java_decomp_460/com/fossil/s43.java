package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum s43 {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)),
    DOUBLE(Double.valueOf(0.0d)),
    BOOLEAN(Boolean.FALSE),
    STRING(""),
    BYTE_STRING(xz2.zza),
    ENUM(null),
    MESSAGE(null);
    
    @DexIgnore
    public /* final */ Object zzj;

    @DexIgnore
    public s43(Object obj) {
        this.zzj = obj;
    }
}
