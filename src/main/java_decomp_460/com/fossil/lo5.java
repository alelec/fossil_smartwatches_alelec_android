package com.fossil;

import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.source.FileRepository;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lo5 {
    @DexIgnore
    public static final mo5 a(mo5 mo5) {
        pq7.c(mo5, "$this$clone");
        mo5 mo52 = new mo5(mo5.e(), mo5.a(), mo5.b(), mo5.d(), mo5.i(), mo5.m(), mo5.f(), mo5.j(), mo5.k(), mo5.g(), mo5.n());
        mo52.q(mo5.c());
        mo52.t(mo5.l());
        mo52.s(mo5.h());
        return mo52;
    }

    @DexIgnore
    public static final mo5 b(no5 no5, String str, String str2, String str3) {
        pq7.c(no5, "$this$toDianaPreset");
        pq7.c(str, "presetId");
        pq7.c(str2, "serial");
        pq7.c(str3, ButtonService.USER_ID);
        List j0 = pm7.j0(no5.a());
        String b = no5.b();
        if (b == null) {
            b = "";
        }
        return new mo5(str, j0, b, "", null, no5.i(), no5.f(), str2, str3, null, true);
    }

    @DexIgnore
    public static final jo5 c(mo5 mo5, FileRepository fileRepository) {
        String str;
        byte[] b;
        pq7.c(mo5, "$this$toDianaPresetDraft");
        pq7.c(fileRepository, "fileRepository");
        File fileByFileName = fileRepository.getFileByFileName(mo5.e());
        if (fileByFileName == null || (b = j37.b(fileByFileName)) == null || (str = j37.a(b)) == null) {
            str = "";
        }
        return d(mo5, str);
    }

    @DexIgnore
    public static final jo5 d(mo5 mo5, String str) {
        pq7.c(mo5, "$this$toDianaPresetDraft");
        pq7.c(str, "base64Data");
        return new jo5(mo5.e(), mo5.a(), mo5.f(), mo5.i(), str, mo5.j(), mo5.m(), mo5.g(), mo5.c(), mo5.l());
    }

    @DexIgnore
    public static final jo5 e(no5 no5, String str, String str2, String str3) {
        String str4;
        pq7.c(no5, "$this$toDianaPresetDraft");
        pq7.c(str, "presetId");
        pq7.c(str2, "base64Data");
        pq7.c(str3, "serial");
        SimpleDateFormat simpleDateFormat = lk5.p.get();
        if (simpleDateFormat == null || (str4 = simpleDateFormat.format(new Date())) == null) {
            str4 = "2016-01-01T01:01:01.001Z";
        }
        return new jo5(str, no5.a(), no5.f(), null, str2, str3, no5.i(), null, str4, str4);
    }

    @DexIgnore
    public static final List<jo5> f(List<mo5> list, FileRepository fileRepository) {
        pq7.c(list, "$this$toDianaPresetDrafts");
        pq7.c(fileRepository, "fileRepository");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(c(it.next(), fileRepository));
        }
        return arrayList;
    }

    @DexIgnore
    public static final gj4 g(List<mo5> list) {
        pq7.c(list, "$this$toJsonDeletedIds");
        gj4 gj4 = new gj4();
        bj4 bj4 = new bj4();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            bj4.l(it.next().e());
        }
        gj4.k("_ids", bj4);
        return gj4;
    }

    @DexIgnore
    public static final gj4 h(jo5 jo5) {
        pq7.c(jo5, "$this$toJsonObject");
        gj4 gj4 = new gj4();
        bj4 bj4 = new bj4();
        List<oo5> a2 = jo5.a();
        if (a2 != null) {
            for (T t : a2) {
                gj4 gj42 = new gj4();
                gj42.n("appId", t.a());
                gj42.n("buttonPosition", t.b());
                gj42.n("localUpdatedAt", t.c());
                bj4.k(gj42);
            }
        }
        gj4.k("buttons", bj4);
        gj4.l("isActive", Boolean.valueOf(jo5.i()));
        gj4.n("dianaFaceBase64Data", jo5.c());
        gj4.n("name", jo5.e());
        gj4.n("previewFaceUrl", jo5.g());
        gj4.n("serialNumber", jo5.h());
        gj4.n("id", jo5.d());
        if (!TextUtils.isEmpty(jo5.f())) {
            gj4.n("originalItemIdInStore", jo5.f());
        }
        return gj4;
    }

    @DexIgnore
    public static final gj4 i(List<jo5> list) {
        pq7.c(list, "$this$toJsonObject");
        gj4 gj4 = new gj4();
        bj4 bj4 = new bj4();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            bj4.k(h(it.next()));
        }
        gj4.k(CloudLogWriter.ITEMS_PARAM, bj4);
        return gj4;
    }

    @DexIgnore
    public static final Object j(mo5 mo5, boolean z, boolean z2, ob7 ob7, List<fb7> list, List<fb7> list2, List<fb7> list3, String str, qn7<? super dp5> qn7) {
        T t;
        T t2;
        T t3;
        byte[] a2;
        ArrayList arrayList = new ArrayList();
        if (ob7 == null) {
            return new dp5(mo5.e(), z, z2, mo5.f(), mo5.a(), null, str, mo5.d());
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            String b = next.b();
            hb7 b2 = ob7.b();
            if (ao7.a(pq7.a(b, b2 != null ? b2.b() : null)).booleanValue()) {
                t = next;
                break;
            }
        }
        T t4 = t;
        String f = t4 != null ? t4.f() : null;
        hb7 b3 = ob7.b();
        hb7 b4 = ob7.b();
        rb7 rb7 = new rb7(b3, (b4 == null || (a2 = b4.a()) == null) ? null : j37.c(a2), f, null, 8, null);
        for (T t5 : ob7.a()) {
            if (t5 instanceof pb7) {
                Iterator<T> it2 = list2.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t3 = null;
                        break;
                    }
                    T next2 = it2.next();
                    if (ao7.a(pq7.a(next2.d(), t5.d())).booleanValue()) {
                        t3 = next2;
                        break;
                    }
                }
                T t6 = t3;
                T t7 = t5;
                arrayList.add(new vb7(t7, t7.e().length == 0 ? null : j37.c(t7.e()), t6 != null ? t6.f() : null));
            } else if (t5 instanceof ib7) {
                Iterator<T> it3 = list3.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next3 = it3.next();
                    if (ao7.a(pq7.a(next3.d(), t5.f())).booleanValue()) {
                        t2 = next3;
                        break;
                    }
                }
                T t8 = t2;
                T t9 = t5;
                arrayList.add(new sb7(t9, t9.h().length == 0 ? null : j37.c(t9.h()), t8 != null ? t8.f() : null));
            } else if (t5 instanceof mb7) {
                arrayList.add(new tb7(t5));
            }
        }
        return new dp5(mo5.e(), z, z2, mo5.f(), mo5.a(), new ub7(rb7, arrayList), str, mo5.d());
    }
}
