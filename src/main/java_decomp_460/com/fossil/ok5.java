package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ok5 implements MembersInjector<nk5> {
    @DexIgnore
    public static void a(nk5 nk5, DeviceRepository deviceRepository) {
        nk5.b = deviceRepository;
    }

    @DexIgnore
    public static void b(nk5 nk5, on5 on5) {
        nk5.f2539a = on5;
    }
}
