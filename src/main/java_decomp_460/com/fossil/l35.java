package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class l35 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ RTLImageView s;
    @DexIgnore
    public /* final */ View t;
    @DexIgnore
    public /* final */ RecyclerView u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ SwipeRefreshLayout w;
    @DexIgnore
    public /* final */ FlexibleTextView x;

    @DexIgnore
    public l35(Object obj, View view, int i, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView, View view2, RecyclerView recyclerView, ConstraintLayout constraintLayout, SwipeRefreshLayout swipeRefreshLayout, FlexibleTextView flexibleTextView3) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = flexibleTextView2;
        this.s = rTLImageView;
        this.t = view2;
        this.u = recyclerView;
        this.v = constraintLayout;
        this.w = swipeRefreshLayout;
        this.x = flexibleTextView3;
    }
}
