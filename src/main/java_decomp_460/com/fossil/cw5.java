package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cw5 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<AddressWrapper> f674a;
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ TextView f675a;
        @DexIgnore
        public /* final */ TextView b;
        @DexIgnore
        public /* final */ ImageView c;
        @DexIgnore
        public /* final */ /* synthetic */ cw5 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cw5$a$a")
        /* renamed from: com.fossil.cw5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0034a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public View$OnClickListenerC0034a(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b bVar;
                int adapterPosition = this.b.getAdapterPosition();
                if (this.b.getAdapterPosition() != -1 && (bVar = this.b.d.b) != null) {
                    List list = this.b.d.f674a;
                    if (list != null) {
                        bVar.a((AddressWrapper) list.get(adapterPosition));
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(cw5 cw5, View view) {
            super(view);
            pq7.c(view, "view");
            this.d = cw5;
            view.setOnClickListener(new View$OnClickListenerC0034a(this));
            View findViewById = view.findViewById(2131362546);
            if (findViewById != null) {
                this.f675a = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131362386);
                if (findViewById2 != null) {
                    this.b = (TextView) findViewById2;
                    View findViewById3 = view.findViewById(2131362726);
                    if (findViewById3 != null) {
                        this.c = (ImageView) findViewById3;
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.b;
        }

        @DexIgnore
        public final ImageView b() {
            return this.c;
        }

        @DexIgnore
        public final TextView c() {
            return this.f675a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(AddressWrapper addressWrapper);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<AddressWrapper> list = this.f674a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public final AddressWrapper i(int i) {
        if (i == -1) {
            return null;
        }
        List<AddressWrapper> list = this.f674a;
        if (list == null) {
            pq7.i();
            throw null;
        } else if (i > list.size()) {
            return null;
        } else {
            List<AddressWrapper> list2 = this.f674a;
            if (list2 != null) {
                return list2.get(i);
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: j */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        List<AddressWrapper> list = this.f674a;
        if (list != null) {
            AddressWrapper addressWrapper = list.get(i);
            aVar.c().setText(addressWrapper.getName());
            aVar.a().setText(addressWrapper.getAddress());
            int i2 = dw5.f844a[addressWrapper.getType().ordinal()];
            if (i2 == 1) {
                aVar.b().setImageDrawable(gl0.f(PortfolioApp.h0.c(), 2131230987));
                aVar.c().setText(um5.c(PortfolioApp.h0.c(), 2131886354));
            } else if (i2 == 2) {
                aVar.b().setImageDrawable(gl0.f(PortfolioApp.h0.c(), 2131230989));
                aVar.c().setText(um5.c(PortfolioApp.h0.c(), 2131886356));
            } else if (i2 == 3) {
                aVar.b().setImageDrawable(gl0.f(PortfolioApp.h0.c(), 2131230988));
                aVar.c().setText(addressWrapper.getName());
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: k */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558656, viewGroup, false);
        pq7.b(inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void l(List<AddressWrapper> list) {
        pq7.c(list, "addressList");
        this.f674a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void m(b bVar) {
        pq7.c(bVar, "listener");
        this.b = bVar;
    }
}
