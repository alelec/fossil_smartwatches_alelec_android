package com.fossil;

import androidx.fragment.app.FragmentActivity;
import com.fossil.iq4;
import com.fossil.ou5;
import com.fossil.zu5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq6 extends xp6 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public /* final */ yp6 e;
    @DexIgnore
    public /* final */ DeviceRepository f;
    @DexIgnore
    public /* final */ ck5 g;
    @DexIgnore
    public /* final */ ou5 h;
    @DexIgnore
    public /* final */ zu5 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return bq6.j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.e<zu5.d, zu5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ bq6 f485a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(bq6 bq6) {
            this.f485a = bq6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(zu5.c cVar) {
            pq7.c(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(bq6.k.a(), "deleteUser - onError");
            this.f485a.e.k();
            this.f485a.e.o(cVar.a(), "");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(zu5.d dVar) {
            pq7.c(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(bq6.k.a(), "deleteUser - onSuccess");
            this.f485a.e.k();
            if (((kp6) this.f485a.e).isActive()) {
                this.f485a.e.a6();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$1", f = "DeleteAccountPresenter.kt", l = {108}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bq6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$1$skuModel$1", f = "DeleteAccountPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super SKUModel>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super SKUModel> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.f.getSkuModelBySerialPrefix(PortfolioApp.h0.c().J());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(bq6 bq6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bq6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SKUModel sKUModel = (SKUModel) g;
            if (sKUModel != null) {
                HashMap hashMap = new HashMap();
                String sku = sKUModel.getSku();
                if (sku == null) {
                    sku = "";
                }
                hashMap.put("Style_Number", sku);
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                hashMap.put("Device_Name", deviceName);
                this.this$0.g.l("feedback_submit", hashMap);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendOpenFeedbackEvent$1", f = "DeleteAccountPresenter.kt", l = {93}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bq6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendOpenFeedbackEvent$1$skuModel$1", f = "DeleteAccountPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super SKUModel>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super SKUModel> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.f.getSkuModelBySerialPrefix(PortfolioApp.h0.c().J());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(bq6 bq6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bq6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SKUModel sKUModel = (SKUModel) g;
            if (sKUModel != null) {
                HashMap hashMap = new HashMap();
                String sku = sKUModel.getSku();
                if (sku == null) {
                    sku = "";
                }
                hashMap.put("Style_Number", sku);
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                hashMap.put("Device_Name", deviceName);
                hashMap.put("Trigger_Screen", "Support");
                this.this$0.g.l("feedback_open", hashMap);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements iq4.e<ou5.c, ou5.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ bq6 f486a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends BaseZendeskFeedbackConfiguration {
            @DexIgnore
            public /* final */ /* synthetic */ ou5.c $responseValue;

            @DexIgnore
            public a(ou5.c cVar) {
                this.$responseValue = cVar;
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public String getAdditionalInfo() {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = bq6.k.a();
                local.d(a2, "Inside. getAdditionalInfo: \n" + this.$responseValue.a());
                return this.$responseValue.a();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration
            public String getRequestSubject() {
                FLogger.INSTANCE.getLocal().d(bq6.k.a(), "getRequestSubject");
                return this.$responseValue.d();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public List<String> getTags() {
                FLogger.INSTANCE.getLocal().d(bq6.k.a(), "getTags");
                return this.$responseValue.e();
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(bq6 bq6) {
            this.f486a = bq6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(ou5.a aVar) {
            pq7.c(aVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(bq6.k.a(), "sendFeedback onError");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ou5.c cVar) {
            pq7.c(cVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(bq6.k.a(), "sendFeedback onSuccess");
            ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(cVar.f()).withEmailIdentifier(cVar.c()).build());
            ZendeskConfig.INSTANCE.setCustomFields(cVar.b());
            this.f486a.e.j0(new a(cVar));
        }
    }

    /*
    static {
        String simpleName = bq6.class.getSimpleName();
        pq7.b(simpleName, "DeleteAccountPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public bq6(yp6 yp6, DeviceRepository deviceRepository, ck5 ck5, UserRepository userRepository, ou5 ou5, zu5 zu5) {
        pq7.c(yp6, "mView");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(ck5, "mAnalyticsHelper");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(ou5, "mGetZendeskInformation");
        pq7.c(zu5, "mDeleteLogoutUserUseCase");
        this.e = yp6;
        this.f = deviceRepository;
        this.g = ck5;
        this.h = ou5;
        this.i = zu5;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(j, "presenter start");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(j, "presenter stop");
    }

    @DexIgnore
    @Override // com.fossil.xp6
    public void n() {
        FLogger.INSTANCE.getLocal().d(j, "deleteUser");
        this.e.m();
        zu5 zu5 = this.i;
        yp6 yp6 = this.e;
        if (yp6 != null) {
            FragmentActivity activity = ((kp6) yp6).getActivity();
            if (activity != null) {
                zu5.e(new zu5.b(0, new WeakReference(activity)), new b(this));
                return;
            }
            throw new il7("null cannot be cast to non-null type android.app.Activity");
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.help.DeleteAccountFragment");
    }

    @DexIgnore
    @Override // com.fossil.xp6
    public void o() {
        xw7 unused = gu7.d(k(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.xp6
    public void p() {
        xw7 unused = gu7.d(k(), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.xp6
    public void q(String str) {
        pq7.c(str, "subject");
        this.h.e(new ou5.b(str), new e(this));
    }

    @DexIgnore
    public void v() {
        this.e.M5(this);
    }
}
