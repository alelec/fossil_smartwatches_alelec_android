package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class io7 extends zn7 {
    @DexIgnore
    public io7(qn7<Object> qn7) {
        super(qn7);
        if (qn7 != null) {
            if (!(qn7.getContext() == un7.INSTANCE)) {
                throw new IllegalArgumentException("Coroutines with restricted suspension must have EmptyCoroutineContext".toString());
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.zn7, com.fossil.qn7
    public tn7 getContext() {
        return un7.INSTANCE;
    }
}
