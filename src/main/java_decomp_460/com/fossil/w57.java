package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w57 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f3886a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore
    public w57(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f3886a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = i6;
    }

    @DexIgnore
    public final int a() {
        return this.e;
    }

    @DexIgnore
    public final int b() {
        return this.c;
    }

    @DexIgnore
    public final int c() {
        return this.f;
    }

    @DexIgnore
    public final int d() {
        return this.f3886a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof w57) {
                w57 w57 = (w57) obj;
                if (!(this.f3886a == w57.f3886a && this.b == w57.b && this.c == w57.c && this.d == w57.d && this.e == w57.e && this.f == w57.f)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((this.f3886a * 31) + this.b) * 31) + this.c) * 31) + this.d) * 31) + this.e) * 31) + this.f;
    }

    @DexIgnore
    public String toString() {
        return "TodayHeartRateModel(value=" + this.f3886a + ", minValue=" + this.b + ", maxValue=" + this.c + ", startTime=" + this.d + ", endTime=" + this.e + ", midTime=" + this.f + ")";
    }
}
