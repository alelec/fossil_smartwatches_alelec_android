package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ab0 implements Parcelable.Creator<bb0> {
    @DexIgnore
    public /* synthetic */ ab0(kq7 kq7) {
    }

    @DexIgnore
    public bb0 a(Parcel parcel) {
        return new bb0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public bb0 createFromParcel(Parcel parcel) {
        return new bb0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public bb0[] newArray(int i) {
        return new bb0[i];
    }
}
