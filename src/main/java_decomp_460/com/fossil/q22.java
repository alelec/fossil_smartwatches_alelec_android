package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class q22 {
    @DexIgnore
    public static q22 a(long j, h02 h02, c02 c02) {
        return new j22(j, h02, c02);
    }

    @DexIgnore
    public abstract c02 b();

    @DexIgnore
    public abstract long c();

    @DexIgnore
    public abstract h02 d();
}
