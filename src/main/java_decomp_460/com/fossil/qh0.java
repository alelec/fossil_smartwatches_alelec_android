package com.fossil;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Build;
import java.lang.ref.WeakReference;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qh0 extends ContextWrapper {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public static ArrayList<WeakReference<qh0>> d;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Resources f2979a;
    @DexIgnore
    public /* final */ Resources.Theme b;

    @DexIgnore
    public qh0(Context context) {
        super(context);
        if (yh0.b()) {
            yh0 yh0 = new yh0(this, context.getResources());
            this.f2979a = yh0;
            Resources.Theme newTheme = yh0.newTheme();
            this.b = newTheme;
            newTheme.setTo(context.getTheme());
            return;
        }
        this.f2979a = new sh0(this, context.getResources());
        this.b = null;
    }

    @DexIgnore
    public static boolean a(Context context) {
        if ((context instanceof qh0) || (context.getResources() instanceof sh0) || (context.getResources() instanceof yh0)) {
            return false;
        }
        return Build.VERSION.SDK_INT < 21 || yh0.b();
    }

    @DexIgnore
    public static Context b(Context context) {
        if (!a(context)) {
            return context;
        }
        synchronized (c) {
            if (d == null) {
                d = new ArrayList<>();
            } else {
                for (int size = d.size() - 1; size >= 0; size--) {
                    WeakReference<qh0> weakReference = d.get(size);
                    if (weakReference == null || weakReference.get() == null) {
                        d.remove(size);
                    }
                }
                for (int size2 = d.size() - 1; size2 >= 0; size2--) {
                    WeakReference<qh0> weakReference2 = d.get(size2);
                    qh0 qh0 = weakReference2 != null ? weakReference2.get() : null;
                    if (qh0 != null && qh0.getBaseContext() == context) {
                        return qh0;
                    }
                }
            }
            qh0 qh02 = new qh0(context);
            d.add(new WeakReference<>(qh02));
            return qh02;
        }
    }

    @DexIgnore
    public AssetManager getAssets() {
        return this.f2979a.getAssets();
    }

    @DexIgnore
    public Resources getResources() {
        return this.f2979a;
    }

    @DexIgnore
    public Resources.Theme getTheme() {
        Resources.Theme theme = this.b;
        return theme == null ? super.getTheme() : theme;
    }

    @DexIgnore
    public void setTheme(int i) {
        Resources.Theme theme = this.b;
        if (theme == null) {
            super.setTheme(i);
        } else {
            theme.applyStyle(i, true);
        }
    }
}
