package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cm7 extends bm7 {
    @DexIgnore
    public static final <T> boolean c(T[] tArr, T[] tArr2) {
        pq7.c(tArr, "$this$contentDeepEqualsImpl");
        pq7.c(tArr2, FacebookRequestErrorClassification.KEY_OTHER);
        if (tArr == tArr2) {
            return true;
        }
        if (tArr.length != tArr2.length) {
            return false;
        }
        int length = tArr.length;
        for (int i = 0; i < length; i++) {
            T t = tArr[i];
            T t2 = tArr2[i];
            if (t != t2) {
                if (t == null || t2 == null) {
                    return false;
                }
                if (!(t instanceof Object[]) || !(t2 instanceof Object[])) {
                    if (!(t instanceof byte[]) || !(t2 instanceof byte[])) {
                        if (!(t instanceof short[]) || !(t2 instanceof short[])) {
                            if (!(t instanceof int[]) || !(t2 instanceof int[])) {
                                if (!(t instanceof long[]) || !(t2 instanceof long[])) {
                                    if (!(t instanceof float[]) || !(t2 instanceof float[])) {
                                        if (!(t instanceof double[]) || !(t2 instanceof double[])) {
                                            if (!(t instanceof char[]) || !(t2 instanceof char[])) {
                                                if (!(t instanceof boolean[]) || !(t2 instanceof boolean[])) {
                                                    if (!(t instanceof kl7) || !(t2 instanceof kl7)) {
                                                        if (!(t instanceof rl7) || !(t2 instanceof rl7)) {
                                                            if (!(t instanceof ml7) || !(t2 instanceof ml7)) {
                                                                if (!(t instanceof ol7) || !(t2 instanceof ol7)) {
                                                                    if (!pq7.a(t, t2)) {
                                                                        return false;
                                                                    }
                                                                } else if (!ln7.d(t.m(), t2.m())) {
                                                                    return false;
                                                                }
                                                            } else if (!ln7.a(t.m(), t2.m())) {
                                                                return false;
                                                            }
                                                        } else if (!ln7.c(t.m(), t2.m())) {
                                                            return false;
                                                        }
                                                    } else if (!ln7.b(t.m(), t2.m())) {
                                                        return false;
                                                    }
                                                } else if (!Arrays.equals((boolean[]) t, (boolean[]) t2)) {
                                                    return false;
                                                }
                                            } else if (!Arrays.equals((char[]) t, (char[]) t2)) {
                                                return false;
                                            }
                                        } else if (!Arrays.equals((double[]) t, (double[]) t2)) {
                                            return false;
                                        }
                                    } else if (!Arrays.equals((float[]) t, (float[]) t2)) {
                                        return false;
                                    }
                                } else if (!Arrays.equals((long[]) t, (long[]) t2)) {
                                    return false;
                                }
                            } else if (!Arrays.equals((int[]) t, (int[]) t2)) {
                                return false;
                            }
                        } else if (!Arrays.equals((short[]) t, (short[]) t2)) {
                            return false;
                        }
                    } else if (!Arrays.equals((byte[]) t, (byte[]) t2)) {
                        return false;
                    }
                } else if (!c((Object[]) t, (Object[]) t2)) {
                    return false;
                }
            }
        }
        return true;
    }
}
