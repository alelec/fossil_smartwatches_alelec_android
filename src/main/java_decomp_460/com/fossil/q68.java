package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q68 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2930a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public q68(String str, String str2) {
        this.f2930a = str;
        this.b = str2;
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public String b() {
        return this.f2930a;
    }

    @DexIgnore
    public String toString() {
        return this.f2930a + ": " + this.b;
    }
}
