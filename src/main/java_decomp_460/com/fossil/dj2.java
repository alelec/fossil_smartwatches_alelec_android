package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.pc2;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dj2 extends zc2 implements z62 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<dj2> CREATOR; // = new ej2();
    @DexIgnore
    public /* final */ List<uh2> b;
    @DexIgnore
    public /* final */ Status c;

    @DexIgnore
    public dj2(List<uh2> list, Status status) {
        this.b = Collections.unmodifiableList(list);
        this.c = status;
    }

    @DexIgnore
    @Override // com.fossil.z62
    public Status a() {
        return this.c;
    }

    @DexIgnore
    public List<uh2> c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof dj2)) {
                return false;
            }
            dj2 dj2 = (dj2) obj;
            if (!(this.c.equals(dj2.c) && pc2.a(this.b, dj2.b))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return pc2.b(this.c, this.b);
    }

    @DexIgnore
    public String toString() {
        pc2.a c2 = pc2.c(this);
        c2.a("status", this.c);
        c2.a("dataSources", this.b);
        return c2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.y(parcel, 1, c(), false);
        bd2.t(parcel, 2, a(), i, false);
        bd2.b(parcel, a2);
    }
}
