package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zb4 {
    /*
    static {
        a(1, 3);
        a(1, 4);
        a(2, 0);
        a(3, 2);
    }
    */

    @DexIgnore
    public static int a(int i, int i2) {
        return (i << 3) | i2;
    }
}
