package com.fossil;

import android.content.Intent;
import android.os.Binder;
import android.os.Process;
import android.util.Log;
import com.fossil.kg4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hg4 extends Binder {
    @DexIgnore
    public /* final */ a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        nt3<Void> a(Intent intent);
    }

    @DexIgnore
    public hg4(a aVar) {
        this.b = aVar;
    }

    @DexIgnore
    public void b(kg4.a aVar) {
        if (Binder.getCallingUid() == Process.myUid()) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "service received new intent via bind strategy");
            }
            this.b.a(aVar.f1915a).c(se4.a(), new gg4(aVar));
            return;
        }
        throw new SecurityException("Binding only allowed within app");
    }
}
