package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface h97 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    void b(String str);

    @DexIgnore
    long c(g97 g97);

    @DexIgnore
    List<g97> d();

    @DexIgnore
    List<g97> e();

    @DexIgnore
    List<g97> f();

    @DexIgnore
    g97 g(String str);

    @DexIgnore
    void h(String str);

    @DexIgnore
    Long[] insert(List<g97> list);
}
