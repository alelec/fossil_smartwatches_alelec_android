package com.fossil;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewParent;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kz3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements io0 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ c f2121a;
        @DexIgnore
        public /* final */ /* synthetic */ d b;

        @DexIgnore
        public a(c cVar, d dVar) {
            this.f2121a = cVar;
            this.b = dVar;
        }

        @DexIgnore
        @Override // com.fossil.io0
        public vo0 a(View view, vo0 vo0) {
            return this.f2121a.a(view, vo0, new d(this.b));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnAttachStateChangeListener {
        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            view.removeOnAttachStateChangeListener(this);
            mo0.i0(view);
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        vo0 a(View view, vo0 vo0, d dVar);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f2122a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;

        @DexIgnore
        public d(int i, int i2, int i3, int i4) {
            this.f2122a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
        }

        @DexIgnore
        public d(d dVar) {
            this.f2122a = dVar.f2122a;
            this.b = dVar.b;
            this.c = dVar.c;
            this.d = dVar.d;
        }

        @DexIgnore
        public void a(View view) {
            mo0.A0(view, this.f2122a, this.b, this.c, this.d);
        }
    }

    @DexIgnore
    public static void a(View view, c cVar) {
        mo0.z0(view, new a(cVar, new d(mo0.E(view), view.getPaddingTop(), mo0.D(view), view.getPaddingBottom())));
        f(view);
    }

    @DexIgnore
    public static float b(Context context, int i) {
        return TypedValue.applyDimension(1, (float) i, context.getResources().getDisplayMetrics());
    }

    @DexIgnore
    public static float c(View view) {
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        for (ViewParent parent = view.getParent(); parent instanceof View; parent = parent.getParent()) {
            f = mo0.u((View) parent) + f;
        }
        return f;
    }

    @DexIgnore
    public static boolean d(View view) {
        return mo0.z(view) == 1;
    }

    @DexIgnore
    public static PorterDuff.Mode e(int i, PorterDuff.Mode mode) {
        if (i == 3) {
            return PorterDuff.Mode.SRC_OVER;
        }
        if (i == 5) {
            return PorterDuff.Mode.SRC_IN;
        }
        if (i == 9) {
            return PorterDuff.Mode.SRC_ATOP;
        }
        switch (i) {
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            case 16:
                return PorterDuff.Mode.ADD;
            default:
                return mode;
        }
    }

    @DexIgnore
    public static void f(View view) {
        if (mo0.P(view)) {
            mo0.i0(view);
        } else {
            view.addOnAttachStateChangeListener(new b());
        }
    }
}
