package com.fossil;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.NumberPicker;
import java.text.DateFormatSymbols;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sv5 extends u47 implements hx6 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public gx6 k;
    @DexIgnore
    public g37<p25> l;
    @DexIgnore
    public z67 m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return sv5.t;
        }

        @DexIgnore
        public final sv5 b() {
            return new sv5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sv5 f3317a;

        @DexIgnore
        public b(sv5 sv5) {
            this.f3317a = sv5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = sv5.u.a();
            local.d(a2, "Month was changed from " + i + " to " + i2);
            sv5.A6(this.f3317a).p(i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sv5 f3318a;

        @DexIgnore
        public c(sv5 sv5) {
            this.f3318a = sv5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = sv5.u.a();
            local.d(a2, "Day was changed from " + i + " to " + i2);
            sv5.A6(this.f3318a).o(i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sv5 f3319a;

        @DexIgnore
        public d(sv5 sv5) {
            this.f3319a = sv5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = sv5.u.a();
            local.d(a2, "Year was changed from " + i + " to " + i2);
            sv5.A6(this.f3319a).q(i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sv5 b;

        @DexIgnore
        public e(sv5 sv5) {
            this.b = sv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            sv5.A6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sv5 b;

        @DexIgnore
        public f(sv5 sv5) {
            this.b = sv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.dismiss();
        }
    }

    /*
    static {
        String simpleName = sv5.class.getSimpleName();
        pq7.b(simpleName, "BirthdayFragment::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ gx6 A6(sv5 sv5) {
        gx6 gx6 = sv5.k;
        if (gx6 != null) {
            return gx6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.hx6
    public void A4(cl7<Integer, Integer> cl7, cl7<Integer, Integer> cl72, cl7<Integer, Integer> cl73) {
        NumberPicker numberPicker;
        NumberPicker numberPicker2;
        NumberPicker numberPicker3;
        NumberPicker numberPicker4;
        NumberPicker numberPicker5;
        NumberPicker numberPicker6;
        pq7.c(cl7, "dayRange");
        pq7.c(cl72, "monthRange");
        pq7.c(cl73, "yearRange");
        p25 C6 = C6();
        if (!(C6 == null || (numberPicker6 = C6.w) == null)) {
            pq7.b(numberPicker6, "npYear");
            numberPicker6.setMinValue(cl73.getFirst().intValue());
            numberPicker6.setMaxValue(cl73.getSecond().intValue());
        }
        p25 C62 = C6();
        if (!(C62 == null || (numberPicker5 = C62.v) == null)) {
            pq7.b(numberPicker5, "npMonth");
            numberPicker5.setMinValue(cl72.getFirst().intValue());
            numberPicker5.setMaxValue(cl72.getSecond().intValue());
        }
        p25 C63 = C6();
        if (!(C63 == null || (numberPicker4 = C63.u) == null)) {
            pq7.b(numberPicker4, "npDay");
            numberPicker4.setMinValue(cl7.getFirst().intValue());
            numberPicker4.setMaxValue(cl7.getSecond().intValue());
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            int i = arguments.getInt("DAY");
            int i2 = arguments.getInt("MONTH");
            int i3 = arguments.getInt("YEAR");
            p25 C64 = C6();
            if (!(C64 == null || (numberPicker3 = C64.u) == null)) {
                numberPicker3.setValue(i);
            }
            p25 C65 = C6();
            if (!(C65 == null || (numberPicker2 = C65.v) == null)) {
                numberPicker2.setValue(i2);
            }
            p25 C66 = C6();
            if (!(C66 == null || (numberPicker = C66.w) == null)) {
                numberPicker.setValue(i3);
            }
            gx6 gx6 = this.k;
            if (gx6 != null) {
                gx6.o(i);
                gx6 gx62 = this.k;
                if (gx62 != null) {
                    gx62.p(i2);
                    gx6 gx63 = this.k;
                    if (gx63 != null) {
                        gx63.q(i3);
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final p25 C6() {
        g37<p25> g37 = this.l;
        if (g37 != null) {
            return g37.a();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: D6 */
    public void M5(gx6 gx6) {
        pq7.c(gx6, "presenter");
        this.k = gx6;
    }

    @DexIgnore
    @Override // com.fossil.hx6
    public void M2(int i, int i2) {
        NumberPicker numberPicker;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "Update day range: from " + i + " to " + i2);
        p25 C6 = C6();
        if (C6 != null && (numberPicker = C6.u) != null) {
            pq7.b(numberPicker, "npDay");
            numberPicker.setMinValue(i);
            numberPicker.setMaxValue(i2);
        }
    }

    @DexIgnore
    @Override // com.fossil.hx6
    public void o6(Date date) {
        pq7.c(date, Constants.PROFILE_KEY_BIRTHDAY);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProfileSetupFragment", "birthDay: " + date);
        z67 z67 = this.m;
        if (z67 != null) {
            z67.a().l(date);
            dismiss();
            return;
        }
        pq7.n("mUserBirthDayViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.u47, androidx.fragment.app.Fragment, com.fossil.kq0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, 2131951897);
    }

    @DexIgnore
    @Override // com.fossil.ze0, com.fossil.nx3, com.fossil.kq0
    public Dialog onCreateDialog(Bundle bundle) {
        FLogger.INSTANCE.getLocal().d(t, "onCreateDialog");
        View inflate = View.inflate(getContext(), 2131558504, null);
        pq7.b(inflate, "view");
        inflate.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        mx3 mx3 = new mx3(requireContext(), getTheme());
        mx3.setContentView(inflate);
        mx3.setCanceledOnTouchOutside(true);
        return mx3;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        ViewDataBinding e2 = aq0.e(layoutInflater, 2131558504, viewGroup, false);
        pq7.b(e2, "DataBindingUtil.inflate(\u2026rthday, container, false)");
        p25 p25 = (p25) e2;
        this.l = new g37<>(this, p25);
        return p25.n();
    }

    @DexIgnore
    @Override // com.fossil.u47, androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        gx6 gx6 = this.k;
        if (gx6 != null) {
            gx6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public void onStop() {
        super.onStop();
        gx6 gx6 = this.k;
        if (gx6 != null) {
            gx6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        NumberPicker numberPicker;
        NumberPicker numberPicker2;
        NumberPicker numberPicker3;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        ts0 a2 = vs0.e(requireActivity()).a(z67.class);
        pq7.b(a2, "ViewModelProviders.of(re\u2026DayViewModel::class.java)");
        this.m = (z67) a2;
        p25 C6 = C6();
        if (!(C6 == null || (numberPicker3 = C6.v) == null)) {
            DateFormatSymbols instance = DateFormatSymbols.getInstance(Locale.getDefault());
            pq7.b(instance, "DateFormatSymbols.getInstance(Locale.getDefault())");
            numberPicker3.setDisplayedValues(instance.getShortMonths());
            numberPicker3.setOnValueChangedListener(new b(this));
        }
        p25 C62 = C6();
        if (!(C62 == null || (numberPicker2 = C62.u) == null)) {
            numberPicker2.setOnValueChangedListener(new c(this));
        }
        p25 C63 = C6();
        if (!(C63 == null || (numberPicker = C63.w) == null)) {
            numberPicker.setOnValueChangedListener(new d(this));
        }
        p25 C64 = C6();
        if (!(C64 == null || (flexibleButton2 = C64.s) == null)) {
            flexibleButton2.setOnClickListener(new e(this));
        }
        p25 C65 = C6();
        if (C65 != null && (flexibleButton = C65.r) != null) {
            flexibleButton.setOnClickListener(new f(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.u47
    public void z6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
