package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class qg4 implements Callable {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ tg4 f2977a;

    @DexIgnore
    public qg4(tg4 tg4) {
        this.f2977a = tg4;
    }

    @DexIgnore
    public static Callable a(tg4 tg4) {
        return new qg4(tg4);
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public Object call() {
        return this.f2977a.d();
    }
}
