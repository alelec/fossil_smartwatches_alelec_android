package com.fossil;

import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.fossil.tn7;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pn7 implements tn7, Serializable {
    @DexIgnore
    public /* final */ tn7.b element;
    @DexIgnore
    public /* final */ tn7 left;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Serializable {
        @DexIgnore
        public static /* final */ C0192a Companion; // = new C0192a(null);
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ tn7[] elements;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.pn7$a$a")
        /* renamed from: com.fossil.pn7$a$a  reason: collision with other inner class name */
        public static final class C0192a {
            @DexIgnore
            public C0192a() {
            }

            @DexIgnore
            public /* synthetic */ C0192a(kq7 kq7) {
                this();
            }
        }

        @DexIgnore
        public a(tn7[] tn7Arr) {
            pq7.c(tn7Arr, MessengerShareContentUtility.ELEMENTS);
            this.elements = tn7Arr;
        }

        @DexIgnore
        private final Object readResolve() {
            tn7[] tn7Arr = this.elements;
            tn7 tn7 = un7.INSTANCE;
            for (tn7 tn72 : tn7Arr) {
                tn7 = tn7.plus(tn72);
            }
            return tn7;
        }

        @DexIgnore
        public final tn7[] getElements() {
            return this.elements;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements vp7<String, tn7.b, String> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(2);
        }

        @DexIgnore
        public final String invoke(String str, tn7.b bVar) {
            pq7.c(str, "acc");
            pq7.c(bVar, "element");
            if (str.length() == 0) {
                return bVar.toString();
            }
            return str + ", " + bVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qq7 implements vp7<tl7, tn7.b, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ tn7[] $elements;
        @DexIgnore
        public /* final */ /* synthetic */ br7 $index;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(tn7[] tn7Arr, br7 br7) {
            super(2);
            this.$elements = tn7Arr;
            this.$index = br7;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public /* bridge */ /* synthetic */ tl7 invoke(tl7 tl7, tn7.b bVar) {
            invoke(tl7, bVar);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(tl7 tl7, tn7.b bVar) {
            pq7.c(tl7, "<anonymous parameter 0>");
            pq7.c(bVar, "element");
            tn7[] tn7Arr = this.$elements;
            br7 br7 = this.$index;
            int i = br7.element;
            br7.element = i + 1;
            tn7Arr[i] = bVar;
        }
    }

    @DexIgnore
    public pn7(tn7 tn7, tn7.b bVar) {
        pq7.c(tn7, ViewHierarchy.DIMENSION_LEFT_KEY);
        pq7.c(bVar, "element");
        this.left = tn7;
        this.element = bVar;
    }

    @DexIgnore
    private final Object writeReplace() {
        boolean z = false;
        int e = e();
        tn7[] tn7Arr = new tn7[e];
        br7 br7 = new br7();
        br7.element = 0;
        fold(tl7.f3441a, new c(tn7Arr, br7));
        if (br7.element == e) {
            z = true;
        }
        if (z) {
            return new a(tn7Arr);
        }
        throw new IllegalStateException("Check failed.".toString());
    }

    @DexIgnore
    public final boolean b(tn7.b bVar) {
        return pq7.a(get(bVar.getKey()), bVar);
    }

    @DexIgnore
    public final boolean d(pn7 pn7) {
        while (b(pn7.element)) {
            tn7 tn7 = pn7.left;
            if (tn7 instanceof pn7) {
                pn7 = (pn7) tn7;
            } else if (tn7 != null) {
                return b((tn7.b) tn7);
            } else {
                throw new il7("null cannot be cast to non-null type kotlin.coroutines.CoroutineContext.Element");
            }
        }
        return false;
    }

    @DexIgnore
    public final int e() {
        int i = 2;
        while (true) {
            tn7 tn7 = this.left;
            if (!(tn7 instanceof pn7)) {
                tn7 = null;
            }
            pn7 pn7 = (pn7) tn7;
            if (pn7 == null) {
                return i;
            }
            i++;
            this = pn7;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof pn7) {
                pn7 pn7 = (pn7) obj;
                if (pn7.e() != e() || !pn7.d(this)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.tn7
    public <R> R fold(R r, vp7<? super R, ? super tn7.b, ? extends R> vp7) {
        pq7.c(vp7, "operation");
        return (R) vp7.invoke((Object) this.left.fold(r, vp7), this.element);
    }

    @DexIgnore
    @Override // com.fossil.tn7
    public <E extends tn7.b> E get(tn7.c<E> cVar) {
        pq7.c(cVar, "key");
        while (true) {
            E e = (E) this.element.get(cVar);
            if (e != null) {
                return e;
            }
            tn7 tn7 = this.left;
            if (!(tn7 instanceof pn7)) {
                return (E) tn7.get(cVar);
            }
            this = (pn7) tn7;
        }
    }

    @DexIgnore
    public int hashCode() {
        return this.left.hashCode() + this.element.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.tn7
    public tn7 minusKey(tn7.c<?> cVar) {
        pq7.c(cVar, "key");
        if (this.element.get(cVar) != null) {
            return this.left;
        }
        tn7 minusKey = this.left.minusKey(cVar);
        return minusKey != this.left ? minusKey == un7.INSTANCE ? this.element : new pn7(minusKey, this.element) : this;
    }

    @DexIgnore
    @Override // com.fossil.tn7
    public tn7 plus(tn7 tn7) {
        pq7.c(tn7, "context");
        return tn7.a.a(this, tn7);
    }

    @DexIgnore
    public String toString() {
        return "[" + ((String) fold("", b.INSTANCE)) + "]";
    }
}
