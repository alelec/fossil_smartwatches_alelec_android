package com.fossil;

import androidx.media.AudioAttributesCompat;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ft0 implements dt0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f1189a; // = 0;
    @DexIgnore
    public int b; // = 0;
    @DexIgnore
    public int c; // = 0;
    @DexIgnore
    public int d; // = -1;

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        int i = this.c;
        int c2 = c();
        if (c2 == 6) {
            i |= 4;
        } else if (c2 == 7) {
            i |= 1;
        }
        return i & 273;
    }

    @DexIgnore
    public int c() {
        int i = this.d;
        return i != -1 ? i : AudioAttributesCompat.a(false, this.c, this.f1189a);
    }

    @DexIgnore
    public int d() {
        return this.f1189a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof ft0)) {
            return false;
        }
        ft0 ft0 = (ft0) obj;
        return this.b == ft0.a() && this.c == ft0.b() && this.f1189a == ft0.d() && this.d == ft0.d;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.b), Integer.valueOf(this.c), Integer.valueOf(this.f1189a), Integer.valueOf(this.d)});
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("AudioAttributesCompat:");
        if (this.d != -1) {
            sb.append(" stream=");
            sb.append(this.d);
            sb.append(" derived");
        }
        sb.append(" usage=");
        sb.append(AudioAttributesCompat.b(this.f1189a));
        sb.append(" content=");
        sb.append(this.b);
        sb.append(" flags=0x");
        sb.append(Integer.toHexString(this.c).toUpperCase());
        return sb.toString();
    }
}
