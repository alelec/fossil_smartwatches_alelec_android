package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class na0 implements Parcelable.Creator<oa0> {
    @DexIgnore
    public /* synthetic */ na0(kq7 kq7) {
    }

    @DexIgnore
    public oa0 a(Parcel parcel) {
        return new oa0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public oa0 createFromParcel(Parcel parcel) {
        return new oa0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public oa0[] newArray(int i) {
        return new oa0[i];
    }
}
