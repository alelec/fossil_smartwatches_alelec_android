package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class in1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ short f; // = hy1.d(fq7.f1179a);
    @DexIgnore
    public static /* final */ short g; // = hy1.c(fq7.f1179a);
    @DexIgnore
    public /* final */ short b;
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ short d;
    @DexIgnore
    public /* final */ short e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<in1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public in1 createFromParcel(Parcel parcel) {
            in1 in1 = new in1(hy1.p(parcel.readByte()), hy1.p(parcel.readByte()), hy1.p(parcel.readByte()), hy1.p(parcel.readByte()));
            in1.a();
            return in1;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public in1[] newArray(int i) {
            return new in1[i];
        }
    }

    @DexIgnore
    public in1(short s, short s2, short s3, short s4) throws IllegalArgumentException {
        this.b = (short) s;
        this.c = (short) s2;
        this.d = (short) s3;
        this.e = (short) s4;
        a();
    }

    @DexIgnore
    public final void a() throws IllegalArgumentException {
        boolean z = true;
        short s = f;
        short s2 = g;
        short s3 = this.b;
        if (s <= s3 && s2 >= s3) {
            short s4 = f;
            short s5 = g;
            short s6 = this.c;
            if (s4 <= s6 && s5 >= s6) {
                short s7 = f;
                short s8 = g;
                short s9 = this.d;
                if (s7 <= s9 && s8 >= s9) {
                    short s10 = f;
                    short s11 = g;
                    short s12 = this.e;
                    if (s10 > s12 || s11 < s12) {
                        z = false;
                    }
                    if (!z) {
                        StringBuilder e2 = e.e("stop latency (");
                        e2.append((int) this.b);
                        e2.append(") is out of range ");
                        e2.append('[');
                        e2.append((int) f);
                        e2.append(", ");
                        throw new IllegalArgumentException(e.b(e2, g, "]."));
                    }
                    return;
                }
                StringBuilder e3 = e.e("resume latency (");
                e3.append((int) this.b);
                e3.append(") is out of range ");
                e3.append('[');
                e3.append((int) f);
                e3.append(", ");
                throw new IllegalArgumentException(e.b(e3, g, "]."));
            }
            StringBuilder e4 = e.e("pause latency (");
            e4.append((int) this.b);
            e4.append(") is out of range ");
            e4.append('[');
            e4.append((int) f);
            e4.append(", ");
            throw new IllegalArgumentException(e.b(e4, g, "]."));
        }
        StringBuilder e5 = e.e("start latency (");
        e5.append((int) this.b);
        e5.append(") is out of range ");
        e5.append('[');
        e5.append((int) f);
        e5.append(", ");
        throw new IllegalArgumentException(e.b(e5, g, "]."));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(in1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            in1 in1 = (in1) obj;
            if (this.b != in1.b) {
                return false;
            }
            if (this.c != in1.c) {
                return false;
            }
            if (this.d != in1.d) {
                return false;
            }
            return this.e == in1.e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.autoworkoutdectection.ActivityDetectionLatency");
    }

    @DexIgnore
    public final short getPauseLatencyInMinute() {
        return this.c;
    }

    @DexIgnore
    public final short getResumeLatencyInMinute() {
        return this.d;
    }

    @DexIgnore
    public final short getStartLatencyInMinute() {
        return this.b;
    }

    @DexIgnore
    public final short getStopLatencyInMinute() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.b * 31) + this.c) * 31) + this.d) * 31) + this.e;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put("start_latency", Short.valueOf(this.b)).put("pause_latency", Short.valueOf(this.c)).put("resume_latency", Short.valueOf(this.d)).put("stop_latency", Short.valueOf(this.e));
        pq7.b(put, "JSONObject()\n           \u2026cy\", stopLatencyInMinute)");
        return put;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte((byte) this.b);
        }
        if (parcel != null) {
            parcel.writeByte((byte) this.c);
        }
        if (parcel != null) {
            parcel.writeByte((byte) this.d);
        }
        if (parcel != null) {
            parcel.writeByte((byte) this.e);
        }
    }
}
