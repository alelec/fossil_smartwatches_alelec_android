package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h42 {
    @DexIgnore
    public static j42 a(Context context, GoogleSignInOptions googleSignInOptions) {
        rc2.k(googleSignInOptions);
        return new j42(context, googleSignInOptions);
    }

    @DexIgnore
    public static GoogleSignInAccount b(Context context) {
        return b52.c(context).e();
    }

    @DexIgnore
    public static boolean c(GoogleSignInAccount googleSignInAccount, k42 k42) {
        rc2.l(k42, "Please provide a non-null GoogleSignInOptionsExtension");
        return d(googleSignInAccount, h(k42.a()));
    }

    @DexIgnore
    public static boolean d(GoogleSignInAccount googleSignInAccount, Scope... scopeArr) {
        if (googleSignInAccount == null) {
            return false;
        }
        HashSet hashSet = new HashSet();
        Collections.addAll(hashSet, scopeArr);
        return googleSignInAccount.A().containsAll(hashSet);
    }

    @DexIgnore
    public static void e(Fragment fragment, int i, GoogleSignInAccount googleSignInAccount, k42 k42) {
        rc2.l(fragment, "Please provide a non-null Fragment");
        rc2.l(k42, "Please provide a non-null GoogleSignInOptionsExtension");
        f(fragment, i, googleSignInAccount, h(k42.a()));
    }

    @DexIgnore
    public static void f(Fragment fragment, int i, GoogleSignInAccount googleSignInAccount, Scope... scopeArr) {
        rc2.l(fragment, "Please provide a non-null Fragment");
        rc2.l(scopeArr, "Please provide at least one scope");
        fragment.startActivityForResult(g(fragment.getActivity(), googleSignInAccount, scopeArr), i);
    }

    @DexIgnore
    public static Intent g(Activity activity, GoogleSignInAccount googleSignInAccount, Scope... scopeArr) {
        GoogleSignInOptions.a aVar = new GoogleSignInOptions.a();
        if (scopeArr.length > 0) {
            aVar.f(scopeArr[0], scopeArr);
        }
        if (googleSignInAccount != null && !TextUtils.isEmpty(googleSignInAccount.f())) {
            aVar.i(googleSignInAccount.f());
        }
        return new j42(activity, aVar.a()).s();
    }

    @DexIgnore
    public static Scope[] h(List<Scope> list) {
        return list == null ? new Scope[0] : (Scope[]) list.toArray(new Scope[list.size()]);
    }
}
