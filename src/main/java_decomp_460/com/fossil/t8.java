package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum t8 {
    GET((byte) 0),
    SET((byte) 1);
    
    @DexIgnore
    public static /* final */ s8 f; // = new s8(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public t8(byte b2) {
        this.b = (byte) b2;
    }
}
