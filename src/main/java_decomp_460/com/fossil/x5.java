package com.fossil;

import android.bluetooth.BluetoothGatt;
import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x5 extends u5 {
    @DexIgnore
    public /* final */ n5 k; // = n5.DAEMON;
    @DexIgnore
    public /* final */ fd0<h7> l;

    @DexIgnore
    public x5(n4 n4Var) {
        super(v5.b, n4Var);
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void d(k5 k5Var) {
        BluetoothGatt bluetoothGatt = k5Var.c;
        if (bluetoothGatt != null) {
            if (k5Var.y) {
                k5Var.v(bluetoothGatt);
            }
            ky1 ky1 = ky1.DEBUG;
            bluetoothGatt.close();
        }
        k5Var.c = null;
        k5Var.e = false;
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        if (myLooper != null) {
            new Handler(myLooper).postDelayed(new w5(this), 1000);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.u5
    public n5 h() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public boolean i(h7 h7Var) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public fd0<h7> j() {
        return this.l;
    }
}
