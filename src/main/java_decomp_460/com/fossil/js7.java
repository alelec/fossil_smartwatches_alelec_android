package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface js7<T, R> extends ms7<T, R>, Object<R> {

    @DexIgnore
    public interface a<T, R> extends hs7<R>, vp7<T, R, tl7> {
    }

    @DexIgnore
    a<T, R> getSetter();
}
