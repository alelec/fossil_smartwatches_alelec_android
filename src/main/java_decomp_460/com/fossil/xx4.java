package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xx4 implements Factory<wx4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<tt4> f4201a;

    @DexIgnore
    public xx4(Provider<tt4> provider) {
        this.f4201a = provider;
    }

    @DexIgnore
    public static xx4 a(Provider<tt4> provider) {
        return new xx4(provider);
    }

    @DexIgnore
    public static wx4 c(tt4 tt4) {
        return new wx4(tt4);
    }

    @DexIgnore
    /* renamed from: b */
    public wx4 get() {
        return c(this.f4201a.get());
    }
}
