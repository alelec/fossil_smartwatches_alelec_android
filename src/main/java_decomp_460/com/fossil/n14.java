package com.fossil;

import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n14 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Object f2449a;

    /*
    static {
        Object b = b();
        f2449a = b;
        if (b != null) {
            a();
        }
        if (f2449a != null) {
            d();
        }
    }
    */

    @DexIgnore
    public static Method a() {
        return c("getStackTraceElement", Throwable.class, Integer.TYPE);
    }

    @DexIgnore
    public static Object b() {
        try {
            return Class.forName("sun.misc.SharedSecrets", false, null).getMethod("getJavaLangAccess", new Class[0]).invoke(null, new Object[0]);
        } catch (ThreadDeath e) {
            throw e;
        } catch (Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static Method c(String str, Class<?>... clsArr) throws ThreadDeath {
        try {
            return Class.forName("sun.misc.JavaLangAccess", false, null).getMethod(str, clsArr);
        } catch (ThreadDeath e) {
            throw e;
        } catch (Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static Method d() {
        return c("getStackTraceDepth", Throwable.class);
    }

    @DexIgnore
    @Deprecated
    public static <X extends Throwable> void e(Throwable th, Class<X> cls) throws Throwable {
        if (th != null) {
            h(th, cls);
        }
    }

    @DexIgnore
    @Deprecated
    public static void f(Throwable th) {
        if (th != null) {
            i(th);
        }
    }

    @DexIgnore
    public static <X extends Throwable> void g(Throwable th, Class<X> cls) throws Throwable {
        e(th, cls);
        f(th);
    }

    @DexIgnore
    public static <X extends Throwable> void h(Throwable th, Class<X> cls) throws Throwable {
        i14.l(th);
        if (cls.isInstance(th)) {
            throw cls.cast(th);
        }
    }

    @DexIgnore
    public static void i(Throwable th) {
        i14.l(th);
        if (th instanceof RuntimeException) {
            throw ((RuntimeException) th);
        } else if (th instanceof Error) {
            throw ((Error) th);
        }
    }
}
