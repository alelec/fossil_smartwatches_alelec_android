package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mg5 extends lg5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d v; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray w;
    @DexIgnore
    public long u;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        w = sparseIntArray;
        sparseIntArray.put(2131362665, 1);
        w.put(2131362502, 2);
        w.put(2131362406, 3);
    }
    */

    @DexIgnore
    public mg5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 4, v, w));
    }

    @DexIgnore
    public mg5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[3], (FlexibleButton) objArr[2], (RTLImageView) objArr[1]);
        this.u = -1;
        this.q.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.u = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.u != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.u = 1;
        }
        w();
    }
}
