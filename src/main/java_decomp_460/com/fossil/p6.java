package com.fossil;

import android.bluetooth.BluetoothGattCharacteristic;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p6 {
    @DexIgnore
    public static /* final */ o6 b; // = new o6(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ BluetoothGattCharacteristic f2785a;

    @DexIgnore
    public p6(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        this.f2785a = bluetoothGattCharacteristic;
        o6 o6Var = b;
        UUID uuid = bluetoothGattCharacteristic.getUuid();
        pq7.b(uuid, "this.bluetoothGattCharacteristic.uuid");
        o6Var.a(uuid);
    }
}
