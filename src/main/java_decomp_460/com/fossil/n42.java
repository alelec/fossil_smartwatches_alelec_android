package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n42 {
    @DexIgnore
    public static int b; // = 31;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f2465a; // = 1;

    @DexIgnore
    public n42 a(Object obj) {
        this.f2465a = (obj == null ? 0 : obj.hashCode()) + (b * this.f2465a);
        return this;
    }

    @DexIgnore
    public int b() {
        return this.f2465a;
    }

    @DexIgnore
    public final n42 c(boolean z) {
        this.f2465a = (b * this.f2465a) + (z ? 1 : 0);
        return this;
    }
}
