package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.SparseIntArray;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c01 extends b01 {
    @DexIgnore
    public /* final */ SparseIntArray d;
    @DexIgnore
    public /* final */ Parcel e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;

    @DexIgnore
    public c01(Parcel parcel) {
        this(parcel, parcel.dataPosition(), parcel.dataSize(), "", new zi0(), new zi0(), new zi0());
    }

    @DexIgnore
    public c01(Parcel parcel, int i2, int i3, String str, zi0<String, Method> zi0, zi0<String, Method> zi02, zi0<String, Class> zi03) {
        super(zi0, zi02, zi03);
        this.d = new SparseIntArray();
        this.i = -1;
        this.j = 0;
        this.k = -1;
        this.e = parcel;
        this.f = i2;
        this.g = i3;
        this.j = i2;
        this.h = str;
    }

    @DexIgnore
    @Override // com.fossil.b01
    public void A(byte[] bArr) {
        if (bArr != null) {
            this.e.writeInt(bArr.length);
            this.e.writeByteArray(bArr);
            return;
        }
        this.e.writeInt(-1);
    }

    @DexIgnore
    @Override // com.fossil.b01
    public void C(CharSequence charSequence) {
        TextUtils.writeToParcel(charSequence, this.e, 0);
    }

    @DexIgnore
    @Override // com.fossil.b01
    public void E(int i2) {
        this.e.writeInt(i2);
    }

    @DexIgnore
    @Override // com.fossil.b01
    public void G(Parcelable parcelable) {
        this.e.writeParcelable(parcelable, 0);
    }

    @DexIgnore
    @Override // com.fossil.b01
    public void I(String str) {
        this.e.writeString(str);
    }

    @DexIgnore
    @Override // com.fossil.b01
    public void a() {
        int i2 = this.i;
        if (i2 >= 0) {
            int i3 = this.d.get(i2);
            int dataPosition = this.e.dataPosition();
            this.e.setDataPosition(i3);
            this.e.writeInt(dataPosition - i3);
            this.e.setDataPosition(dataPosition);
        }
    }

    @DexIgnore
    @Override // com.fossil.b01
    public b01 b() {
        Parcel parcel = this.e;
        int dataPosition = parcel.dataPosition();
        int i2 = this.j;
        if (i2 == this.f) {
            i2 = this.g;
        }
        return new c01(parcel, dataPosition, i2, this.h + "  ", this.f374a, this.b, this.c);
    }

    @DexIgnore
    @Override // com.fossil.b01
    public boolean g() {
        return this.e.readInt() != 0;
    }

    @DexIgnore
    @Override // com.fossil.b01
    public byte[] i() {
        int readInt = this.e.readInt();
        if (readInt < 0) {
            return null;
        }
        byte[] bArr = new byte[readInt];
        this.e.readByteArray(bArr);
        return bArr;
    }

    @DexIgnore
    @Override // com.fossil.b01
    public CharSequence k() {
        return (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(this.e);
    }

    @DexIgnore
    @Override // com.fossil.b01
    public boolean m(int i2) {
        while (this.j < this.g) {
            int i3 = this.k;
            if (i3 == i2) {
                return true;
            }
            if (String.valueOf(i3).compareTo(String.valueOf(i2)) > 0) {
                return false;
            }
            this.e.setDataPosition(this.j);
            int readInt = this.e.readInt();
            this.k = this.e.readInt();
            this.j = readInt + this.j;
        }
        return this.k == i2;
    }

    @DexIgnore
    @Override // com.fossil.b01
    public int o() {
        return this.e.readInt();
    }

    @DexIgnore
    @Override // com.fossil.b01
    public <T extends Parcelable> T q() {
        return (T) this.e.readParcelable(c01.class.getClassLoader());
    }

    @DexIgnore
    @Override // com.fossil.b01
    public String s() {
        return this.e.readString();
    }

    @DexIgnore
    @Override // com.fossil.b01
    public void w(int i2) {
        a();
        this.i = i2;
        this.d.put(i2, this.e.dataPosition());
        E(0);
        E(i2);
    }

    @DexIgnore
    @Override // com.fossil.b01
    public void y(boolean z) {
        this.e.writeInt(z ? 1 : 0);
    }
}
