package com.fossil;

import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wd1 implements od1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ud1<a, Object> f3919a; // = new ud1<>();
    @DexIgnore
    public /* final */ b b; // = new b();
    @DexIgnore
    public /* final */ Map<Class<?>, NavigableMap<Integer, Integer>> c; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, nd1<?>> d; // = new HashMap();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements zd1 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ b f3920a;
        @DexIgnore
        public int b;
        @DexIgnore
        public Class<?> c;

        @DexIgnore
        public a(b bVar) {
            this.f3920a = bVar;
        }

        @DexIgnore
        @Override // com.fossil.zd1
        public void a() {
            this.f3920a.c(this);
        }

        @DexIgnore
        public void b(int i, Class<?> cls) {
            this.b = i;
            this.c = cls;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.b == aVar.b && this.c == aVar.c;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.b;
            Class<?> cls = this.c;
            return (cls != null ? cls.hashCode() : 0) + (i * 31);
        }

        @DexIgnore
        public String toString() {
            return "Key{size=" + this.b + "array=" + this.c + '}';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qd1<a> {
        @DexIgnore
        /* renamed from: d */
        public a a() {
            return new a(this);
        }

        @DexIgnore
        public a e(int i, Class<?> cls) {
            a aVar = (a) b();
            aVar.b(i, cls);
            return aVar;
        }
    }

    @DexIgnore
    public wd1(int i) {
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.od1
    public void a(int i) {
        synchronized (this) {
            if (i >= 40) {
                d();
            } else if (i >= 20 || i == 15) {
                h(this.e / 2);
            }
        }
    }

    @DexIgnore
    public final void b(int i, Class<?> cls) {
        NavigableMap<Integer, Integer> m = m(cls);
        Integer num = (Integer) m.get(Integer.valueOf(i));
        if (num == null) {
            throw new NullPointerException("Tried to decrement empty size, size: " + i + ", this: " + this);
        } else if (num.intValue() == 1) {
            m.remove(Integer.valueOf(i));
        } else {
            m.put(Integer.valueOf(i), Integer.valueOf(num.intValue() - 1));
        }
    }

    @DexIgnore
    public final void c() {
        h(this.e);
    }

    @DexIgnore
    @Override // com.fossil.od1
    public void d() {
        synchronized (this) {
            h(0);
        }
    }

    @DexIgnore
    @Override // com.fossil.od1
    public <T> T e(int i, Class<T> cls) {
        T t;
        synchronized (this) {
            t = (T) l(this.b.e(i, cls), cls);
        }
        return t;
    }

    @DexIgnore
    @Override // com.fossil.od1
    public <T> void f(T t) {
        synchronized (this) {
            Class<?> cls = t.getClass();
            nd1<T> j = j(cls);
            int b2 = j.b(t);
            int a2 = j.a() * b2;
            if (o(a2)) {
                a e2 = this.b.e(b2, cls);
                this.f3919a.d(e2, t);
                NavigableMap<Integer, Integer> m = m(cls);
                Integer num = (Integer) m.get(Integer.valueOf(e2.b));
                m.put(Integer.valueOf(e2.b), Integer.valueOf(num == null ? 1 : num.intValue() + 1));
                this.f += a2;
                c();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.od1
    public <T> T g(int i, Class<T> cls) {
        T t;
        synchronized (this) {
            Integer ceilingKey = m(cls).ceilingKey(Integer.valueOf(i));
            t = (T) l(p(i, ceilingKey) ? this.b.e(ceilingKey.intValue(), cls) : this.b.e(i, cls), cls);
        }
        return t;
    }

    @DexIgnore
    public final void h(int i) {
        while (this.f > i) {
            Object f2 = this.f3919a.f();
            ik1.d(f2);
            nd1 i2 = i(f2);
            this.f -= i2.b(f2) * i2.a();
            b(i2.b(f2), f2.getClass());
            if (Log.isLoggable(i2.getTag(), 2)) {
                Log.v(i2.getTag(), "evicted: " + i2.b(f2));
            }
        }
    }

    @DexIgnore
    public final <T> nd1<T> i(T t) {
        return j(t.getClass());
    }

    @DexIgnore
    public final <T> nd1<T> j(Class<T> cls) {
        vd1 vd1 = (nd1<T>) this.d.get(cls);
        if (vd1 == null) {
            if (cls.equals(int[].class)) {
                vd1 = new vd1();
            } else if (cls.equals(byte[].class)) {
                vd1 = new td1();
            } else {
                throw new IllegalArgumentException("No array pool found for: " + cls.getSimpleName());
            }
            this.d.put(cls, vd1);
        }
        return vd1;
    }

    @DexIgnore
    public final <T> T k(a aVar) {
        return (T) this.f3919a.a(aVar);
    }

    @DexIgnore
    public final <T> T l(a aVar, Class<T> cls) {
        nd1<T> j = j(cls);
        T t = (T) k(aVar);
        if (t != null) {
            this.f -= j.b(t) * j.a();
            b(j.b(t), cls);
        }
        if (t != null) {
            return t;
        }
        if (Log.isLoggable(j.getTag(), 2)) {
            Log.v(j.getTag(), "Allocated " + aVar.b + " bytes");
        }
        return j.newArray(aVar.b);
    }

    @DexIgnore
    public final NavigableMap<Integer, Integer> m(Class<?> cls) {
        NavigableMap<Integer, Integer> navigableMap = this.c.get(cls);
        if (navigableMap != null) {
            return navigableMap;
        }
        TreeMap treeMap = new TreeMap();
        this.c.put(cls, treeMap);
        return treeMap;
    }

    @DexIgnore
    public final boolean n() {
        int i = this.f;
        return i == 0 || this.e / i >= 2;
    }

    @DexIgnore
    public final boolean o(int i) {
        return i <= this.e / 2;
    }

    @DexIgnore
    public final boolean p(int i, Integer num) {
        return num != null && (n() || num.intValue() <= i * 8);
    }
}
