package com.fossil;

import com.fossil.m62;
import com.fossil.m62.d;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g72<O extends m62.d> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ boolean f1270a; // = true;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ m62<O> c;
    @DexIgnore
    public /* final */ O d;

    @DexIgnore
    public g72(m62<O> m62) {
        this.c = m62;
        this.d = null;
        this.b = System.identityHashCode(this);
    }

    @DexIgnore
    public g72(m62<O> m62, O o) {
        this.c = m62;
        this.d = o;
        this.b = pc2.b(m62, o);
    }

    @DexIgnore
    public static <O extends m62.d> g72<O> b(m62<O> m62, O o) {
        return new g72<>(m62, o);
    }

    @DexIgnore
    public static <O extends m62.d> g72<O> c(m62<O> m62) {
        return new g72<>(m62);
    }

    @DexIgnore
    public final String a() {
        return this.c.b();
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof g72)) {
            return false;
        }
        g72 g72 = (g72) obj;
        return !this.f1270a && !g72.f1270a && pc2.a(this.c, g72.c) && pc2.a(this.d, g72.d);
    }

    @DexIgnore
    public final int hashCode() {
        return this.b;
    }
}
