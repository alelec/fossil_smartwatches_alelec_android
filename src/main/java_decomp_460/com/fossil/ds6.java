package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.fossil.gs6;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ds6 extends pv5 implements x47, t47.g {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static String l;
    @DexIgnore
    public static String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public gs6 h;
    @DexIgnore
    public g37<r45> i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ds6.l;
        }

        @DexIgnore
        public final String b() {
            return ds6.m;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<gs6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ds6 f829a;

        @DexIgnore
        public b(ds6 ds6) {
            this.f829a = ds6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(gs6.a aVar) {
            if (aVar != null) {
                Integer a2 = aVar.a();
                if (a2 != null) {
                    this.f829a.N6(a2.intValue());
                }
                Integer c = aVar.c();
                if (c != null) {
                    this.f829a.P6(c.intValue());
                }
                Integer b = aVar.b();
                if (b != null) {
                    this.f829a.M6(b.intValue());
                }
                Integer d = aVar.d();
                if (d != null) {
                    this.f829a.O6(d.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ds6 b;

        @DexIgnore
        public c(ds6 ds6) {
            this.b = ds6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, 201);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ds6 b;

        @DexIgnore
        public d(ds6 ds6) {
            this.b = ds6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, 203);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ds6 b;

        @DexIgnore
        public e(ds6 ds6) {
            this.b = ds6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.p(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = ds6.class.getSimpleName();
        pq7.b(simpleName, "CustomizeButtonFragment::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.x47
    public void C3(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        hr7 hr7 = hr7.f1520a;
        String format = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(16777215 & i3)}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        gs6 gs6 = this.h;
        if (gs6 != null) {
            gs6.h(i2, Color.parseColor(format));
            if (i2 == 201) {
                l = format;
            } else if (i2 == 203) {
                m = format;
            }
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void M6(int i2) {
        g37<r45> g37 = this.i;
        if (g37 != null) {
            r45 a2 = g37.a();
            if (a2 != null) {
                a2.t.setTextColor(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void N6(int i2) {
        g37<r45> g37 = this.i;
        if (g37 != null) {
            r45 a2 = g37.a();
            if (a2 != null) {
                a2.z.setBackgroundColor(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void O6(int i2) {
        g37<r45> g37 = this.i;
        if (g37 != null) {
            r45 a2 = g37.a();
            if (a2 != null) {
                a2.u.setTextColor(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void P6(int i2) {
        g37<r45> g37 = this.i;
        if (g37 != null) {
            r45 a2 = g37.a();
            if (a2 != null) {
                a2.A.setBackgroundColor(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        FLogger.INSTANCE.getLocal().d(k, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363373) {
            gs6 gs6 = this.h;
            if (gs6 != null) {
                gs6.f(pt6.m.a(), l, m);
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        r45 r45 = (r45) aq0.f(LayoutInflater.from(getContext()), 2131558533, null, false, A6());
        PortfolioApp.h0.c().M().Z0(new fs6()).a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(gs6.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026tonViewModel::class.java)");
            gs6 gs6 = (gs6) a2;
            this.h = gs6;
            if (gs6 != null) {
                gs6.e().h(getViewLifecycleOwner(), new b(this));
                gs6 gs62 = this.h;
                if (gs62 != null) {
                    gs62.g();
                    this.i = new g37<>(this, r45);
                    pq7.b(r45, "binding");
                    return r45.n();
                }
                pq7.n("mViewModel");
                throw null;
            }
            pq7.n("mViewModel");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(k, "onDestroy");
        l = null;
        m = null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(k, "onResume");
        gs6 gs6 = this.h;
        if (gs6 != null) {
            gs6.g();
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<r45> g37 = this.i;
        if (g37 != null) {
            r45 a2 = g37.a();
            if (a2 != null) {
                a2.v.setOnClickListener(new c(this));
                a2.w.setOnClickListener(new d(this));
                a2.s.setOnClickListener(new e(this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.x47
    public void q3(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
