package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uj extends cq {
    @DexIgnore
    public int E;

    @DexIgnore
    public uj(k5 k5Var, i60 i60) {
        super(k5Var, i60, yp.f, new ht(k5Var));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        return g80.k(super.E(), jd0.c, Integer.valueOf(this.E));
    }

    @DexIgnore
    @Override // com.fossil.cq
    public void G(fs fsVar) {
        if (fsVar instanceof ht) {
            this.E = ((ht) fsVar).A;
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        return Integer.valueOf(this.E);
    }
}
