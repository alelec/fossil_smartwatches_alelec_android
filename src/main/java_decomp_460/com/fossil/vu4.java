package com.fossil;

import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.zu4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vu4 extends pv5 implements zu4.a {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public g37<j35> g;
    @DexIgnore
    public xu4 h;
    @DexIgnore
    public ts4 i;
    @DexIgnore
    public po4 j;
    @DexIgnore
    public zu4 k;
    @DexIgnore
    public zu4 l;
    @DexIgnore
    public Parcelable m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return vu4.t;
        }

        @DexIgnore
        public final vu4 b(ts4 ts4) {
            vu4 vu4 = new vu4();
            Bundle bundle = new Bundle();
            bundle.putParcelable("challenge_draft_extra", ts4);
            vu4.setArguments(bundle);
            return vu4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vu4 b;

        @DexIgnore
        public b(vu4 vu4, boolean z) {
            this.b = vu4;
        }

        @DexIgnore
        public final void onClick(View view) {
            vu4.R6(this.b).C();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vu4 b;

        @DexIgnore
        public c(vu4 vu4, boolean z) {
            this.b = vu4;
        }

        @DexIgnore
        public final void onClick(View view) {
            ck5 g = ck5.f.g();
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                g.m("bc_start_type_now", activity);
                vu4.R6(this.b).I();
                return;
            }
            throw new il7("null cannot be cast to non-null type android.app.Activity");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vu4 b;

        @DexIgnore
        public d(vu4 vu4, boolean z) {
            this.b = vu4;
        }

        @DexIgnore
        public final void onClick(View view) {
            ck5 g = ck5.f.g();
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                g.m("bc_start_type_later", activity);
                vu4.R6(this.b).H();
                return;
            }
            throw new il7("null cannot be cast to non-null type android.app.Activity");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vu4 b;

        @DexIgnore
        public e(vu4 vu4, boolean z) {
            this.b = vu4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.B0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vu4 f3824a;

        @DexIgnore
        public f(vu4 vu4, boolean z) {
            this.f3824a = vu4;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            vu4.R6(this.f3824a).D(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements SwipeRefreshLayout.j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ j35 f3825a;
        @DexIgnore
        public /* final */ /* synthetic */ vu4 b;

        @DexIgnore
        public g(j35 j35, vu4 vu4, boolean z) {
            this.f3825a = j35;
            this.b = vu4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.f3825a.x;
            pq7.b(flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            vu4.R6(this.b).F();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ j35 b;
        @DexIgnore
        public /* final */ /* synthetic */ vu4 c;

        @DexIgnore
        public h(j35 j35, vu4 vu4, boolean z) {
            this.b = j35;
            this.c = vu4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (charSequence == null || charSequence.length() == 0) {
                vu4.R6(this.c).B();
                FlexibleTextView flexibleTextView = this.b.D;
                pq7.b(flexibleTextView, "tvEmpty");
                flexibleTextView.setVisibility(8);
                ImageView imageView = this.b.r;
                pq7.b(imageView, "btnSearchClear");
                imageView.setVisibility(8);
                return;
            }
            ImageView imageView2 = this.b.r;
            pq7.b(imageView2, "btnSearchClear");
            imageView2.setVisibility(0);
            FlexibleCheckBox flexibleCheckBox = this.b.v;
            pq7.b(flexibleCheckBox, "cbInviteAll");
            flexibleCheckBox.setVisibility(8);
            FlexibleTextView flexibleTextView2 = this.b.y;
            pq7.b(flexibleTextView2, "ftvInviteAll");
            flexibleTextView2.setVisibility(8);
            if (this.c.m == null) {
                vu4 vu4 = this.c;
                RecyclerView recyclerView = this.b.B;
                pq7.b(recyclerView, "rvFriends");
                RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                vu4.m = layoutManager != null ? layoutManager.e1() : null;
                tl7 tl7 = tl7.f3441a;
            }
            vu4.R6(this.c).G(charSequence.toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ j35 b;

        @DexIgnore
        public i(j35 j35) {
            this.b = j35;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.w.setText("");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnFocusChangeListener {
        @DexIgnore
        public static /* final */ j b; // = new j();

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                xr4.f4164a.g(PortfolioApp.h0.c());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements ls0<cl7<? extends Boolean, ? extends Boolean>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vu4 f3826a;

        @DexIgnore
        public k(vu4 vu4) {
            this.f3826a = vu4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, Boolean> cl7) {
            j35 j35 = (j35) vu4.M6(this.f3826a).a();
            if (j35 != null) {
                Boolean first = cl7.getFirst();
                Boolean second = cl7.getSecond();
                if (first != null) {
                    if (first.booleanValue()) {
                        SwipeRefreshLayout swipeRefreshLayout = j35.C;
                        pq7.b(swipeRefreshLayout, "swipe");
                        swipeRefreshLayout.setEnabled(true);
                        SwipeRefreshLayout swipeRefreshLayout2 = j35.C;
                        pq7.b(swipeRefreshLayout2, "swipe");
                        swipeRefreshLayout2.setRefreshing(true);
                        FlexibleCheckBox flexibleCheckBox = j35.v;
                        pq7.b(flexibleCheckBox, "cbInviteAll");
                        flexibleCheckBox.setVisibility(8);
                        FlexibleTextView flexibleTextView = j35.y;
                        pq7.b(flexibleTextView, "ftvInviteAll");
                        flexibleTextView.setVisibility(8);
                    } else {
                        SwipeRefreshLayout swipeRefreshLayout3 = j35.C;
                        pq7.b(swipeRefreshLayout3, "swipe");
                        swipeRefreshLayout3.setRefreshing(false);
                        SwipeRefreshLayout swipeRefreshLayout4 = j35.C;
                        pq7.b(swipeRefreshLayout4, "swipe");
                        swipeRefreshLayout4.setEnabled(false);
                    }
                }
                if (second == null) {
                    return;
                }
                if (second.booleanValue()) {
                    this.f3826a.b();
                } else {
                    this.f3826a.a();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements ls0<cl7<? extends List<? extends ot4>, ? extends ServerError>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vu4 f3827a;

        @DexIgnore
        public l(vu4 vu4) {
            this.f3827a = vu4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<? extends List<ot4>, ? extends ServerError> cl7) {
            List<ot4> list = (List) cl7.getFirst();
            if (list != null) {
                j35 j35 = (j35) vu4.M6(this.f3827a).a();
                if (j35 == null) {
                    return;
                }
                if (list.isEmpty()) {
                    FlexibleTextView flexibleTextView = j35.D;
                    pq7.b(flexibleTextView, "tvEmpty");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                FlexibleCheckBox flexibleCheckBox = j35.v;
                pq7.b(flexibleCheckBox, "cbInviteAll");
                flexibleCheckBox.setVisibility(0);
                FlexibleTextView flexibleTextView2 = j35.y;
                pq7.b(flexibleTextView2, "ftvInviteAll");
                flexibleTextView2.setVisibility(0);
                RecyclerView recyclerView = j35.B;
                pq7.b(recyclerView, "rvFriends");
                recyclerView.setAdapter(vu4.P6(this.f3827a));
                vu4.P6(this.f3827a).j(list);
                Parcelable parcelable = this.f3827a.m;
                if (parcelable != null) {
                    RecyclerView recyclerView2 = j35.B;
                    pq7.b(recyclerView2, "rvFriends");
                    RecyclerView.m layoutManager = recyclerView2.getLayoutManager();
                    if (layoutManager != null) {
                        layoutManager.d1(parcelable);
                        return;
                    }
                    return;
                }
                return;
            }
            j35 j352 = (j35) vu4.M6(this.f3827a).a();
            if (j352 != null) {
                SwipeRefreshLayout swipeRefreshLayout = j352.C;
                pq7.b(swipeRefreshLayout, "swipe");
                swipeRefreshLayout.setEnabled(true);
                SwipeRefreshLayout swipeRefreshLayout2 = j352.C;
                pq7.b(swipeRefreshLayout2, "swipe");
                swipeRefreshLayout2.setRefreshing(false);
                FlexibleTextView flexibleTextView3 = j352.x;
                pq7.b(flexibleTextView3, "ftvError");
                flexibleTextView3.setVisibility(0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vu4 f3828a;

        @DexIgnore
        public m(vu4 vu4) {
            this.f3828a = vu4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            FlexibleCheckBox flexibleCheckBox;
            j35 j35 = (j35) vu4.M6(this.f3828a).a();
            if (j35 != null && (flexibleCheckBox = j35.v) != null) {
                pq7.b(bool, "it");
                flexibleCheckBox.setChecked(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n<T> implements ls0<cl7<? extends Boolean, ? extends ServerError>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vu4 f3829a;

        @DexIgnore
        public n(vu4 vu4) {
            this.f3829a = vu4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, ? extends ServerError> cl7) {
            if (cl7.getFirst().booleanValue()) {
                this.f3829a.W6();
                return;
            }
            ServerError serverError = (ServerError) cl7.getSecond();
            s37 s37 = s37.c;
            Integer code = serverError != null ? serverError.getCode() : null;
            String message = serverError != null ? serverError.getMessage() : null;
            FragmentManager childFragmentManager = this.f3829a.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.g(code, message, childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o<T> implements ls0<List<? extends Date>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vu4 f3830a;

        @DexIgnore
        public o(vu4 vu4) {
            this.f3830a = vu4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<? extends Date> list) {
            vu4 vu4 = this.f3830a;
            pq7.b(list, "it");
            vu4.Z6(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p<T> implements ls0<List<? extends gs4>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vu4 f3831a;

        @DexIgnore
        public p(vu4 vu4) {
            this.f3831a = vu4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<gs4> list) {
            vu4 vu4 = this.f3831a;
            pq7.b(list, "it");
            vu4.a7(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q<T> implements ls0<cl7<? extends List<? extends ot4>, ? extends String>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vu4 f3832a;

        @DexIgnore
        public q(vu4 vu4) {
            this.f3832a = vu4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<? extends List<ot4>, String> cl7) {
            List<ot4> list = (List) cl7.getFirst();
            String second = cl7.getSecond();
            j35 j35 = (j35) vu4.M6(this.f3832a).a();
            if (j35 != null) {
                RecyclerView recyclerView = j35.B;
                pq7.b(recyclerView, "rvFriends");
                recyclerView.setAdapter(vu4.O6(this.f3832a));
                vu4.O6(this.f3832a).j(list);
                if (list.isEmpty()) {
                    FlexibleTextView flexibleTextView = j35.D;
                    pq7.b(flexibleTextView, "tvEmpty");
                    hr7 hr7 = hr7.f1520a;
                    FlexibleTextView flexibleTextView2 = j35.D;
                    pq7.b(flexibleTextView2, "tvEmpty");
                    String c = um5.c(flexibleTextView2.getContext(), 2131886286);
                    pq7.b(c, "LanguageHelper.getString\u2026or__NothingFoundForInput)");
                    String format = String.format(c, Arrays.copyOf(new Object[]{second}, 1));
                    pq7.b(format, "java.lang.String.format(format, *args)");
                    flexibleTextView.setText(format);
                    FlexibleTextView flexibleTextView3 = j35.D;
                    pq7.b(flexibleTextView3, "tvEmpty");
                    flexibleTextView3.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView4 = j35.D;
                pq7.b(flexibleTextView4, "tvEmpty");
                flexibleTextView4.setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r<T> implements ls0<gl7<? extends ps4, ? extends Long, ? extends String>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ r f3833a; // = new r();

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(gl7<ps4, Long, String> gl7) {
            xr4.f4164a.d(gl7.getFirst(), gl7.getSecond().longValue(), gl7.getThird(), PortfolioApp.h0.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s implements ly5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vu4 f3834a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public s(vu4 vu4) {
            this.f3834a = vu4;
        }

        @DexIgnore
        @Override // com.fossil.ly5
        public void a(Date date) {
            pq7.c(date, "date");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = vu4.u.a();
            local.e(a2, "showDateTimePicker - updateValue - date: " + date);
            vu4.R6(this.f3834a).J(date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t implements my5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vu4 f3835a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public t(vu4 vu4) {
            this.f3835a = vu4;
        }

        @DexIgnore
        @Override // com.fossil.my5
        public void a(gs4 gs4) {
            pq7.c(gs4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = vu4.u.a();
            local.e(a2, "showSoonTime - updateValue - model: " + gs4);
            Object a3 = gs4.a();
            if (!(a3 instanceof Date)) {
                a3 = null;
            }
            Date date = (Date) a3;
            if (date != null) {
                vu4.R6(this.f3835a).J(date);
            }
        }
    }

    /*
    static {
        String simpleName = vu4.class.getSimpleName();
        pq7.b(simpleName, "BCInviteFriendFragment::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 M6(vu4 vu4) {
        g37<j35> g37 = vu4.g;
        if (g37 != null) {
            return g37;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ zu4 O6(vu4 vu4) {
        zu4 zu4 = vu4.l;
        if (zu4 != null) {
            return zu4;
        }
        pq7.n("searchingFriendsAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ zu4 P6(vu4 vu4) {
        zu4 zu4 = vu4.k;
        if (zu4 != null) {
            return zu4;
        }
        pq7.n("suggestedFriendsAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ xu4 R6(vu4 vu4) {
        xu4 xu4 = vu4.h;
        if (xu4 != null) {
            return xu4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void B0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.zu4.a
    public void G5(ot4 ot4) {
        pq7.c(ot4, "friend");
        xu4 xu4 = this.h;
        if (xu4 != null) {
            xu4.E(ot4);
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void V6() {
        g37<j35> g37 = this.g;
        if (g37 != null) {
            j35 a2 = g37.a();
            if (a2 != null) {
                fk0 fk0 = new fk0();
                fk0.c(a2.A);
                RecyclerView recyclerView = a2.B;
                pq7.b(recyclerView, "rvFriends");
                int id = recyclerView.getId();
                FlexibleButton flexibleButton = a2.q;
                pq7.b(flexibleButton, "btnDone");
                fk0.e(id, 4, flexibleButton.getId(), 3);
                fk0.a(a2.A);
                return;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final void W6() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public final void X6(boolean z) {
        g37<j35> g37 = this.g;
        if (g37 != null) {
            j35 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.E;
                pq7.b(flexibleTextView, "tvTitle");
                ts4 ts4 = this.i;
                flexibleTextView.setText(ts4 != null ? ts4.e() : null);
                if (z) {
                    FlexibleButton flexibleButton = a2.t;
                    pq7.b(flexibleButton, "btnStartNow");
                    flexibleButton.setVisibility(8);
                    FlexibleButton flexibleButton2 = a2.s;
                    pq7.b(flexibleButton2, "btnStartLater");
                    flexibleButton2.setVisibility(8);
                    FlexibleButton flexibleButton3 = a2.q;
                    pq7.b(flexibleButton3, "btnDone");
                    flexibleButton3.setVisibility(0);
                    V6();
                    FlexibleTextView flexibleTextView2 = a2.y;
                    pq7.b(flexibleTextView2, "ftvInviteAll");
                    flexibleTextView2.setVisibility(8);
                    FlexibleCheckBox flexibleCheckBox = a2.v;
                    pq7.b(flexibleCheckBox, "cbInviteAll");
                    flexibleCheckBox.setVisibility(8);
                    a2.q.setOnClickListener(new b(this, z));
                } else {
                    a2.t.setOnClickListener(new c(this, z));
                    a2.s.setOnClickListener(new d(this, z));
                }
                a2.z.setOnClickListener(new e(this, z));
                this.k = new zu4(this);
                this.l = new zu4(this);
                RecyclerView recyclerView = a2.B;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                zu4 zu4 = this.k;
                if (zu4 != null) {
                    recyclerView.setAdapter(zu4);
                    recyclerView.setHasFixedSize(true);
                    a2.v.setOnCheckedChangeListener(new f(this, z));
                    a2.C.setOnRefreshListener(new g(a2, this, z));
                    a2.r.setOnClickListener(new i(a2));
                    a2.w.setOnFocusChangeListener(j.b);
                    a2.w.addTextChangedListener(new h(a2, this, z));
                    return;
                }
                pq7.n("suggestedFriendsAdapter");
                throw null;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final void Y6() {
        xu4 xu4 = this.h;
        if (xu4 != null) {
            xu4.t().h(getViewLifecycleOwner(), new k(this));
            xu4 xu42 = this.h;
            if (xu42 != null) {
                xu42.y().h(getViewLifecycleOwner(), new l(this));
                xu4 xu43 = this.h;
                if (xu43 != null) {
                    xu43.r().h(getViewLifecycleOwner(), new m(this));
                    xu4 xu44 = this.h;
                    if (xu44 != null) {
                        xu44.x().h(getViewLifecycleOwner(), new n(this));
                        xu4 xu45 = this.h;
                        if (xu45 != null) {
                            xu45.s().h(getViewLifecycleOwner(), new o(this));
                            xu4 xu46 = this.h;
                            if (xu46 != null) {
                                xu46.w().h(getViewLifecycleOwner(), new p(this));
                                xu4 xu47 = this.h;
                                if (xu47 != null) {
                                    xu47.u().h(getViewLifecycleOwner(), new q(this));
                                    xu4 xu48 = this.h;
                                    if (xu48 != null) {
                                        xu48.q().h(getViewLifecycleOwner(), r.f3833a);
                                    } else {
                                        pq7.n("viewModel");
                                        throw null;
                                    }
                                } else {
                                    pq7.n("viewModel");
                                    throw null;
                                }
                            } else {
                                pq7.n("viewModel");
                                throw null;
                            }
                        } else {
                            pq7.n("viewModel");
                            throw null;
                        }
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void Z6(List<? extends Date> list) {
        ky5 ky5 = new ky5();
        String c2 = um5.c(requireContext(), 2131886341);
        pq7.b(c2, "LanguageHelper.getString\u2026e_Invite_CTA__StartLater)");
        ky5.setTitle(c2);
        ky5.O6(list);
        ky5.Q6(new s(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        ky5.show(childFragmentManager, ky5.D.a());
    }

    @DexIgnore
    public final void a7(List<gs4> list) {
        es4 es4 = new es4();
        String c2 = um5.c(requireContext(), 2131886347);
        pq7.b(c2, "LanguageHelper.getString\u2026artNow_Title__QuickStart)");
        es4.setTitle(c2);
        es4.E6(list);
        es4.F6(true);
        es4.G6(new t(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        es4.show(childFragmentManager, es4.A.a());
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().j0().a(this);
        po4 po4 = this.j;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(xu4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026endViewModel::class.java)");
            this.h = (xu4) a2;
            Bundle arguments = getArguments();
            this.i = arguments != null ? (ts4) arguments.getParcelable("challenge_draft_extra") : null;
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        j35 j35 = (j35) aq0.f(layoutInflater, 2131558514, viewGroup, false, A6());
        this.g = new g37<>(this, j35);
        pq7.b(j35, "bd");
        return j35.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        String str = null;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        xu4 xu4 = this.h;
        if (xu4 != null) {
            xu4.z(this.i);
            ts4 ts4 = this.i;
            if (ts4 != null) {
                str = ts4.c();
            }
            X6(str != null);
            Y6();
            return;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
