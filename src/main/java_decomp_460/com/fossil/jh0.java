package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import androidx.collection.SimpleArrayMap;
import androidx.collection.SparseArrayCompat;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import org.xmlpull.v1.XmlPullParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jh0 {
    @DexIgnore
    public static /* final */ PorterDuff.Mode h; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public static jh0 i;
    @DexIgnore
    public static /* final */ c j; // = new c(6);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public WeakHashMap<Context, SparseArrayCompat<ColorStateList>> f1759a;
    @DexIgnore
    public SimpleArrayMap<String, d> b;
    @DexIgnore
    public SparseArrayCompat<String> c;
    @DexIgnore
    public /* final */ WeakHashMap<Context, dj0<WeakReference<Drawable.ConstantState>>> d; // = new WeakHashMap<>(0);
    @DexIgnore
    public TypedValue e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public e g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements d {
        @DexIgnore
        @Override // com.fossil.jh0.d
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return hf0.m(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("AsldcInflateDelegate", "Exception while inflating <animated-selector>", e);
                return null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements d {
        @DexIgnore
        @Override // com.fossil.jh0.d
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return uz0.a(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("AvdcInflateDelegate", "Exception while inflating <animated-vector>", e);
                return null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends ej0<Integer, PorterDuffColorFilter> {
        @DexIgnore
        public c(int i) {
            super(i);
        }

        @DexIgnore
        public static int l(int i, PorterDuff.Mode mode) {
            return ((i + 31) * 31) + mode.hashCode();
        }

        @DexIgnore
        public PorterDuffColorFilter m(int i, PorterDuff.Mode mode) {
            return (PorterDuffColorFilter) d(Integer.valueOf(l(i, mode)));
        }

        @DexIgnore
        public PorterDuffColorFilter n(int i, PorterDuff.Mode mode, PorterDuffColorFilter porterDuffColorFilter) {
            return (PorterDuffColorFilter) f(Integer.valueOf(l(i, mode)), porterDuffColorFilter);
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme);
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        boolean a(Context context, int i, Drawable drawable);

        @DexIgnore
        PorterDuff.Mode b(int i);

        @DexIgnore
        Drawable c(jh0 jh0, Context context, int i);

        @DexIgnore
        ColorStateList d(Context context, int i);

        @DexIgnore
        boolean e(Context context, int i, Drawable drawable);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements d {
        @DexIgnore
        @Override // com.fossil.jh0.d
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return a01.c(context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("VdcInflateDelegate", "Exception while inflating <vector>", e);
                return null;
            }
        }
    }

    @DexIgnore
    public static long e(TypedValue typedValue) {
        return (((long) typedValue.assetCookie) << 32) | ((long) typedValue.data);
    }

    @DexIgnore
    public static PorterDuffColorFilter g(ColorStateList colorStateList, PorterDuff.Mode mode, int[] iArr) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return l(colorStateList.getColorForState(iArr, 0), mode);
    }

    @DexIgnore
    public static jh0 h() {
        jh0 jh0;
        synchronized (jh0.class) {
            try {
                if (i == null) {
                    jh0 jh02 = new jh0();
                    i = jh02;
                    p(jh02);
                }
                jh0 = i;
            } catch (Throwable th) {
                throw th;
            }
        }
        return jh0;
    }

    @DexIgnore
    public static PorterDuffColorFilter l(int i2, PorterDuff.Mode mode) {
        PorterDuffColorFilter m;
        synchronized (jh0.class) {
            try {
                m = j.m(i2, mode);
                if (m == null) {
                    m = new PorterDuffColorFilter(i2, mode);
                    j.n(i2, mode, m);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return m;
    }

    @DexIgnore
    public static void p(jh0 jh0) {
        if (Build.VERSION.SDK_INT < 24) {
            jh0.a("vector", new f());
            jh0.a("animated-vector", new b());
            jh0.a("animated-selector", new a());
        }
    }

    @DexIgnore
    public static boolean q(Drawable drawable) {
        return (drawable instanceof a01) || "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName());
    }

    @DexIgnore
    public static void w(Drawable drawable, rh0 rh0, int[] iArr) {
        if (!dh0.a(drawable) || drawable.mutate() == drawable) {
            if (rh0.d || rh0.c) {
                drawable.setColorFilter(g(rh0.d ? rh0.f3111a : null, rh0.c ? rh0.b : h, iArr));
            } else {
                drawable.clearColorFilter();
            }
            if (Build.VERSION.SDK_INT <= 23) {
                drawable.invalidateSelf();
                return;
            }
            return;
        }
        Log.d("ResourceManagerInternal", "Mutated drawable is not the same instance as the input.");
    }

    @DexIgnore
    public final void a(String str, d dVar) {
        if (this.b == null) {
            this.b = new SimpleArrayMap<>();
        }
        this.b.put(str, dVar);
    }

    @DexIgnore
    public final boolean b(Context context, long j2, Drawable drawable) {
        synchronized (this) {
            Drawable.ConstantState constantState = drawable.getConstantState();
            if (constantState == null) {
                return false;
            }
            dj0<WeakReference<Drawable.ConstantState>> dj0 = this.d.get(context);
            if (dj0 == null) {
                dj0 = new dj0<>();
                this.d.put(context, dj0);
            }
            dj0.r(j2, new WeakReference<>(constantState));
            return true;
        }
    }

    @DexIgnore
    public final void c(Context context, int i2, ColorStateList colorStateList) {
        if (this.f1759a == null) {
            this.f1759a = new WeakHashMap<>();
        }
        SparseArrayCompat<ColorStateList> sparseArrayCompat = this.f1759a.get(context);
        if (sparseArrayCompat == null) {
            sparseArrayCompat = new SparseArrayCompat<>();
            this.f1759a.put(context, sparseArrayCompat);
        }
        sparseArrayCompat.d(i2, colorStateList);
    }

    @DexIgnore
    public final void d(Context context) {
        if (!this.f) {
            this.f = true;
            Drawable j2 = j(context, lf0.abc_vector_test);
            if (j2 == null || !q(j2)) {
                this.f = false;
                throw new IllegalStateException("This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat.");
            }
        }
    }

    @DexIgnore
    public final Drawable f(Context context, int i2) {
        if (this.e == null) {
            this.e = new TypedValue();
        }
        TypedValue typedValue = this.e;
        context.getResources().getValue(i2, typedValue, true);
        long e2 = e(typedValue);
        Drawable i3 = i(context, e2);
        if (i3 == null) {
            e eVar = this.g;
            i3 = eVar == null ? null : eVar.c(this, context, i2);
            if (i3 != null) {
                i3.setChangingConfigurations(typedValue.changingConfigurations);
                b(context, e2, i3);
            }
        }
        return i3;
    }

    @DexIgnore
    public final Drawable i(Context context, long j2) {
        synchronized (this) {
            dj0<WeakReference<Drawable.ConstantState>> dj0 = this.d.get(context);
            if (dj0 == null) {
                return null;
            }
            WeakReference<Drawable.ConstantState> l = dj0.l(j2);
            if (l != null) {
                Drawable.ConstantState constantState = l.get();
                if (constantState != null) {
                    return constantState.newDrawable(context.getResources());
                }
                dj0.s(j2);
            }
            return null;
        }
    }

    @DexIgnore
    public Drawable j(Context context, int i2) {
        Drawable k;
        synchronized (this) {
            k = k(context, i2, false);
        }
        return k;
    }

    @DexIgnore
    public Drawable k(Context context, int i2, boolean z) {
        Drawable r;
        synchronized (this) {
            d(context);
            r = r(context, i2);
            if (r == null) {
                r = f(context, i2);
            }
            if (r == null) {
                r = gl0.f(context, i2);
            }
            if (r != null) {
                r = v(context, i2, z, r);
            }
            if (r != null) {
                dh0.b(r);
            }
        }
        return r;
    }

    @DexIgnore
    public ColorStateList m(Context context, int i2) {
        ColorStateList n;
        synchronized (this) {
            n = n(context, i2);
            if (n == null) {
                n = this.g == null ? null : this.g.d(context, i2);
                if (n != null) {
                    c(context, i2, n);
                }
            }
        }
        return n;
    }

    @DexIgnore
    public final ColorStateList n(Context context, int i2) {
        SparseArrayCompat<ColorStateList> sparseArrayCompat;
        WeakHashMap<Context, SparseArrayCompat<ColorStateList>> weakHashMap = this.f1759a;
        if (weakHashMap == null || (sparseArrayCompat = weakHashMap.get(context)) == null) {
            return null;
        }
        return sparseArrayCompat.j(i2);
    }

    @DexIgnore
    public PorterDuff.Mode o(int i2) {
        e eVar = this.g;
        if (eVar == null) {
            return null;
        }
        return eVar.b(i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:36:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.drawable.Drawable r(android.content.Context r10, int r11) {
        /*
            r9 = this;
            r1 = 0
            r8 = 2
            r7 = 1
            androidx.collection.SimpleArrayMap<java.lang.String, com.fossil.jh0$d> r0 = r9.b
            if (r0 == 0) goto L_0x00b6
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x00b6
            androidx.collection.SparseArrayCompat<java.lang.String> r0 = r9.c
            if (r0 == 0) goto L_0x002b
            java.lang.Object r0 = r0.j(r11)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r2 = "appcompat_skip_skip"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x0029
            if (r0 == 0) goto L_0x0032
            androidx.collection.SimpleArrayMap<java.lang.String, com.fossil.jh0$d> r2 = r9.b
            java.lang.Object r0 = r2.get(r0)
            if (r0 != 0) goto L_0x0032
        L_0x0029:
            r0 = r1
        L_0x002a:
            return r0
        L_0x002b:
            androidx.collection.SparseArrayCompat r0 = new androidx.collection.SparseArrayCompat
            r0.<init>()
            r9.c = r0
        L_0x0032:
            android.util.TypedValue r0 = r9.e
            if (r0 != 0) goto L_0x003d
            android.util.TypedValue r0 = new android.util.TypedValue
            r0.<init>()
            r9.e = r0
        L_0x003d:
            android.util.TypedValue r2 = r9.e
            android.content.res.Resources r0 = r10.getResources()
            r0.getValue(r11, r2, r7)
            long r4 = e(r2)
            android.graphics.drawable.Drawable r1 = r9.i(r10, r4)
            if (r1 == 0) goto L_0x0052
            r0 = r1
            goto L_0x002a
        L_0x0052:
            java.lang.CharSequence r3 = r2.string
            if (r3 == 0) goto L_0x00b4
            java.lang.String r3 = r3.toString()
            java.lang.String r6 = ".xml"
            boolean r3 = r3.endsWith(r6)
            if (r3 == 0) goto L_0x00b4
            android.content.res.XmlResourceParser r3 = r0.getXml(r11)     // Catch:{ Exception -> 0x00ac }
            android.util.AttributeSet r6 = android.util.Xml.asAttributeSet(r3)     // Catch:{ Exception -> 0x00ac }
        L_0x006a:
            int r0 = r3.next()     // Catch:{ Exception -> 0x00ac }
            if (r0 == r8) goto L_0x0072
            if (r0 != r7) goto L_0x006a
        L_0x0072:
            if (r0 != r8) goto L_0x00a4
            java.lang.String r0 = r3.getName()     // Catch:{ Exception -> 0x00ac }
            androidx.collection.SparseArrayCompat<java.lang.String> r7 = r9.c     // Catch:{ Exception -> 0x00ac }
            r7.d(r11, r0)     // Catch:{ Exception -> 0x00ac }
            androidx.collection.SimpleArrayMap<java.lang.String, com.fossil.jh0$d> r7 = r9.b     // Catch:{ Exception -> 0x00ac }
            java.lang.Object r0 = r7.get(r0)     // Catch:{ Exception -> 0x00ac }
            com.fossil.jh0$d r0 = (com.fossil.jh0.d) r0     // Catch:{ Exception -> 0x00ac }
            if (r0 == 0) goto L_0x008f
            android.content.res.Resources$Theme r7 = r10.getTheme()     // Catch:{ Exception -> 0x00ac }
            android.graphics.drawable.Drawable r1 = r0.a(r10, r3, r6, r7)     // Catch:{ Exception -> 0x00ac }
        L_0x008f:
            if (r1 == 0) goto L_0x00b4
            int r0 = r2.changingConfigurations     // Catch:{ Exception -> 0x00ac }
            r1.setChangingConfigurations(r0)     // Catch:{ Exception -> 0x00ac }
            r9.b(r10, r4, r1)     // Catch:{ Exception -> 0x00ac }
            r0 = r1
        L_0x009a:
            if (r0 != 0) goto L_0x002a
            androidx.collection.SparseArrayCompat<java.lang.String> r1 = r9.c
            java.lang.String r2 = "appcompat_skip_skip"
            r1.d(r11, r2)
            goto L_0x002a
        L_0x00a4:
            org.xmlpull.v1.XmlPullParserException r0 = new org.xmlpull.v1.XmlPullParserException
            java.lang.String r2 = "No start tag found"
            r0.<init>(r2)
            throw r0
        L_0x00ac:
            r0 = move-exception
            java.lang.String r2 = "ResourceManagerInternal"
            java.lang.String r3 = "Exception while inflating drawable"
            android.util.Log.e(r2, r3, r0)
        L_0x00b4:
            r0 = r1
            goto L_0x009a
        L_0x00b6:
            r0 = r1
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jh0.r(android.content.Context, int):android.graphics.drawable.Drawable");
    }

    @DexIgnore
    public void s(Context context) {
        synchronized (this) {
            dj0<WeakReference<Drawable.ConstantState>> dj0 = this.d.get(context);
            if (dj0 != null) {
                dj0.e();
            }
        }
    }

    @DexIgnore
    public Drawable t(Context context, yh0 yh0, int i2) {
        synchronized (this) {
            Drawable r = r(context, i2);
            if (r == null) {
                r = yh0.c(i2);
            }
            if (r == null) {
                return null;
            }
            return v(context, i2, false, r);
        }
    }

    @DexIgnore
    public void u(e eVar) {
        synchronized (this) {
            this.g = eVar;
        }
    }

    @DexIgnore
    public final Drawable v(Context context, int i2, boolean z, Drawable drawable) {
        ColorStateList m = m(context, i2);
        if (m != null) {
            if (dh0.a(drawable)) {
                drawable = drawable.mutate();
            }
            Drawable r = am0.r(drawable);
            am0.o(r, m);
            PorterDuff.Mode o = o(i2);
            if (o == null) {
                return r;
            }
            am0.p(r, o);
            return r;
        }
        e eVar = this.g;
        if ((eVar == null || !eVar.e(context, i2, drawable)) && !x(context, i2, drawable) && z) {
            return null;
        }
        return drawable;
    }

    @DexIgnore
    public boolean x(Context context, int i2, Drawable drawable) {
        e eVar = this.g;
        return eVar != null && eVar.a(context, i2, drawable);
    }
}
