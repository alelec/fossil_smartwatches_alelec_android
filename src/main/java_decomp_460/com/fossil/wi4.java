package com.fossil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wi4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Field f3945a;

    @DexIgnore
    public wi4(Field field) {
        uj4.b(field);
        this.f3945a = field;
    }

    @DexIgnore
    public <T extends Annotation> T a(Class<T> cls) {
        return (T) this.f3945a.getAnnotation(cls);
    }
}
