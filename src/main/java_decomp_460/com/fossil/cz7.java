package com.fossil;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cz7<T> extends rz7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ AtomicReferenceFieldUpdater f703a; // = AtomicReferenceFieldUpdater.newUpdater(cz7.class, Object.class, "_consensus");
    @DexIgnore
    public volatile Object _consensus; // = bz7.f536a;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.cz7<T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.rz7
    public cz7<?> a() {
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.rz7
    public final Object c(Object obj) {
        Object obj2 = this._consensus;
        if (obj2 == bz7.f536a) {
            obj2 = e(g(obj));
        }
        d(obj, obj2);
        return obj2;
    }

    @DexIgnore
    public abstract void d(T t, Object obj);

    @DexIgnore
    public final Object e(Object obj) {
        if (nv7.a()) {
            if (!(obj != bz7.f536a)) {
                throw new AssertionError();
            }
        }
        Object obj2 = this._consensus;
        return obj2 != bz7.f536a ? obj2 : !f703a.compareAndSet(this, bz7.f536a, obj) ? this._consensus : obj;
    }

    @DexIgnore
    public long f() {
        return 0;
    }

    @DexIgnore
    public abstract Object g(T t);
}
