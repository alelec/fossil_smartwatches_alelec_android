package com.fossil;

import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface k22 extends Closeable {
    @DexIgnore
    q22 Y(h02 h02, c02 c02);

    @DexIgnore
    long c0(h02 h02);

    @DexIgnore
    int cleanUp();

    @DexIgnore
    boolean f0(h02 h02);

    @DexIgnore
    void g(Iterable<q22> iterable);

    @DexIgnore
    void h0(Iterable<q22> iterable);

    @DexIgnore
    Iterable<q22> q(h02 h02);

    @DexIgnore
    void s(h02 h02, long j);

    @DexIgnore
    Iterable<h02> w();
}
