package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pq extends lp {
    @DexIgnore
    public /* final */ ArrayList<ow> C; // = by1.a(this.i, hm7.c(ow.AUTHENTICATION));
    @DexIgnore
    public /* final */ long D;

    @DexIgnore
    public pq(k5 k5Var, i60 i60, long j) {
        super(k5Var, i60, yp.B0, null, false, 24);
        this.D = j;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        long j = this.D;
        if (j < 0 || j > hy1.b(oq7.f2710a)) {
            k(zq.INVALID_PARAMETER);
        } else {
            lp.i(this, new ct(this.w, this.D), new en(this), new qn(this), null, co.b, oo.b, 8, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(super.C(), jd0.K4, Long.valueOf(this.D));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public boolean r(fs fsVar) {
        return fsVar instanceof gt;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        return tl7.f3441a;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.C;
    }
}
