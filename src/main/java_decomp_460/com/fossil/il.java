package com.fossil;

import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class il extends lp {
    @DexIgnore
    public ArrayList<ru1> C; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<ac0> D; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<ru1> E; // = new ArrayList<>(em7.d0(this.H));
    @DexIgnore
    public int F; // = -1;
    @DexIgnore
    public float G;
    @DexIgnore
    public /* final */ ru1[] H;

    @DexIgnore
    public il(k5 k5Var, i60 i60, ru1[] ru1Arr) {
        super(k5Var, i60, yp.H0, null, false, 24);
        this.H = ru1Arr;
    }

    @DexIgnore
    public static final /* synthetic */ void I(il ilVar) {
        if (!ilVar.D.isEmpty()) {
            ac0 remove = ilVar.D.remove(0);
            pq7.b(remove, "needToUninstallWatchApps.removeAt(0)");
            lp.h(ilVar, new ur(ilVar.w, ilVar.x, remove), xj.b, jk.b, null, new vk(ilVar), null, 40, null);
        } else if (ilVar.E.isEmpty()) {
            ilVar.d(1.0f);
            ilVar.l(nr.a(ilVar.v, null, zq.SUCCESS, null, null, 13));
        } else {
            ilVar.J();
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        lp.h(this, new oj(this.w, this.x), new ki(this), new yi(this), new kj(this), null, null, 48, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(super.C(), jd0.I5, px1.a(this.H));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        JSONObject E2 = super.E();
        jd0 jd0 = jd0.J5;
        Object[] array = this.C.toArray(new ru1[0]);
        if (array != null) {
            return g80.k(E2, jd0, px1.a((ox1[]) array));
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void J() {
        int i = this.F + 1;
        this.F = i;
        if (i < this.E.size()) {
            ru1 ru1 = this.E.get(this.F);
            pq7.b(ru1, "needToInstallWatchApps[watchAppToInstallIndex]");
            ru1 ru12 = ru1;
            lp.h(this, new pg(this.w, this.x, ru12), new wg(this, ru12), new jh(this), new wh(this), null, null, 48, null);
            return;
        }
        l(nr.a(this.v, null, zq.SUCCESS, null, null, 13));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        T t;
        boolean z;
        ru1[] ru1Arr = this.H;
        ArrayList arrayList = new ArrayList();
        for (ru1 ru1 : ru1Arr) {
            Iterator<T> it = this.C.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                T t2 = t;
                if (!pq7.a(t2.getBundleId(), ru1.getBundleId()) || t2.getPackageCrc() != ru1.getPackageCrc()) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (t != null) {
                arrayList.add(ru1);
            }
        }
        Object[] array = arrayList.toArray(new ru1[0]);
        if (array != null) {
            return (ru1[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
