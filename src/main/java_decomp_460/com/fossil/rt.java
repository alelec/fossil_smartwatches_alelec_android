package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum rt {
    PRE_SHARED_KEY((byte) 0),
    SIXTEEN_BYTES_MSB_ECDH_SHARED_SECRET_KEY((byte) 1),
    SIXTEEN_BYTES_LSB_ECDH_SHARED_SECRET_KEY((byte) 2);
    
    @DexIgnore
    public static /* final */ qt g; // = new qt(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public rt(byte b2) {
        this.b = (byte) b2;
    }
}
