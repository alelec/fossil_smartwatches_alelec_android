package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class qu6 extends xq7 {
    @DexIgnore
    public static /* final */ ms7 INSTANCE; // = new qu6();

    @DexIgnore
    @Override // com.fossil.xq7
    public Object get(Object obj) {
        return ep7.a((ru6) obj);
    }

    @DexIgnore
    @Override // com.fossil.gq7, com.fossil.ds7
    public String getName() {
        return "javaClass";
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public fs7 getOwner() {
        return er7.c(ep7.class, "app_fossilRelease");
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public String getSignature() {
        return "getJavaClass(Ljava/lang/Object;)Ljava/lang/Class;";
    }
}
