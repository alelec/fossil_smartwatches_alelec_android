package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hp1 extends ox1 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ un1 b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ float d;
    @DexIgnore
    public /* final */ tn1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<hp1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public hp1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                un1 valueOf = un1.valueOf(readString);
                float readFloat = parcel.readFloat();
                float readFloat2 = parcel.readFloat();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    pq7.b(readString2, "parcel.readString()!!");
                    return new hp1(valueOf, readFloat, readFloat2, tn1.valueOf(readString2));
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public hp1[] newArray(int i) {
            return new hp1[i];
        }
    }

    @DexIgnore
    public hp1(un1 un1, float f, float f2, tn1 tn1) {
        this.b = un1;
        this.c = hy1.e(f, 2);
        this.d = hy1.e(f2, 2);
        this.e = tn1;
    }

    @DexIgnore
    public final JSONObject a() {
        JSONObject put = new JSONObject().put("day", this.b.a()).put("high", Float.valueOf(this.c)).put("low", Float.valueOf(this.d)).put("cond_id", this.e.a());
        pq7.b(put, "JSONObject()\n           \u2026_ID, weatherCondition.id)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(hp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            hp1 hp1 = (hp1) obj;
            if (this.b != hp1.b) {
                return false;
            }
            if (this.c != hp1.c) {
                return false;
            }
            if (this.d != hp1.d) {
                return false;
            }
            return this.e == hp1.e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherDayForecast");
    }

    @DexIgnore
    public final float getHighTemperature() {
        return this.c;
    }

    @DexIgnore
    public final float getLowTemperature() {
        return this.d;
    }

    @DexIgnore
    public final tn1 getWeatherCondition() {
        return this.e;
    }

    @DexIgnore
    public final un1 getWeekday() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = Float.valueOf(this.c).hashCode();
        return (((((hashCode * 31) + hashCode2) * 31) + Float.valueOf(this.d).hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.b2, ey1.a(this.b)), jd0.Z1, Float.valueOf(this.c)), jd0.a2, Float.valueOf(this.d)), jd0.t, ey1.a(this.e));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeFloat(this.c);
        }
        if (parcel != null) {
            parcel.writeFloat(this.d);
        }
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
    }
}
