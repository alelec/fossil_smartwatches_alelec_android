package com.fossil;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mz7<E> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ AtomicReferenceFieldUpdater f2443a; // = AtomicReferenceFieldUpdater.newUpdater(mz7.class, Object.class, "_cur");
    @DexIgnore
    public volatile Object _cur;

    @DexIgnore
    public mz7(boolean z) {
        this._cur = new nz7(8, z);
    }

    @DexIgnore
    public final boolean a(E e) {
        while (true) {
            nz7 nz7 = (nz7) this._cur;
            int a2 = nz7.a(e);
            if (a2 == 0) {
                return true;
            }
            if (a2 == 1) {
                f2443a.compareAndSet(this, nz7, nz7.i());
            } else if (a2 == 2) {
                return false;
            }
        }
    }

    @DexIgnore
    public final void b() {
        while (true) {
            nz7 nz7 = (nz7) this._cur;
            if (!nz7.d()) {
                f2443a.compareAndSet(this, nz7, nz7.i());
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final int c() {
        return ((nz7) this._cur).f();
    }

    @DexIgnore
    public final E d() {
        while (true) {
            nz7 nz7 = (nz7) this._cur;
            E e = (E) nz7.j();
            if (e != nz7.g) {
                return e;
            }
            f2443a.compareAndSet(this, nz7, nz7.i());
        }
    }
}
