package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p61 implements n61<Integer, Uri> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2786a;

    @DexIgnore
    public p61(Context context) {
        pq7.c(context, "context");
        this.f2786a = context;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.n61
    public /* bridge */ /* synthetic */ boolean a(Integer num) {
        return c(num.intValue());
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.n61
    public /* bridge */ /* synthetic */ Uri b(Integer num) {
        return d(num.intValue());
    }

    @DexIgnore
    public boolean c(int i) {
        try {
            return this.f2786a.getResources().getResourceEntryName(i) != null;
        } catch (Resources.NotFoundException e) {
            return false;
        }
    }

    @DexIgnore
    public Uri d(int i) {
        Uri parse = Uri.parse("android.resource://" + this.f2786a.getPackageName() + '/' + i);
        pq7.b(parse, "Uri.parse(this)");
        return parse;
    }
}
