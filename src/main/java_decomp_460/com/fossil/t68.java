package com.fossil;

import java.io.IOException;
import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface t68 extends u68 {
    @DexIgnore
    String d();

    @DexIgnore
    void writeTo(OutputStream outputStream) throws IOException;
}
