package com.fossil;

import android.app.Activity;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wj7 extends Activity implements dk7 {
    @DexIgnore
    public ck7<Object> b;

    @DexIgnore
    @Override // com.fossil.dk7
    public vj7<Object> c() {
        return this.b;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        uj7.a(this);
        super.onCreate(bundle);
    }
}
