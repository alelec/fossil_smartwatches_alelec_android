package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gs extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ es CREATOR; // = new es(null);
    @DexIgnore
    public /* final */ n6 b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ byte[] d;

    @DexIgnore
    public gs(n6 n6Var, long j, byte[] bArr) {
        this.b = n6Var;
        this.c = j;
        this.d = bArr;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(new JSONObject(), jd0.D2, this.b.b), jd0.Q0, Double.valueOf(hy1.f(this.c))), jd0.Y2, dy1.e(this.d, null, 1, null));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b.ordinal());
        parcel.writeLong(this.c);
        parcel.writeByteArray(this.d);
    }
}
