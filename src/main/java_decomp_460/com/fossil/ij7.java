package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ij7 {
    @DexIgnore
    public static /* final */ int abc_ab_share_pack_mtrl_alpha; // = 2131230726;
    @DexIgnore
    public static /* final */ int abc_action_bar_item_background_material; // = 2131230727;
    @DexIgnore
    public static /* final */ int abc_btn_borderless_material; // = 2131230728;
    @DexIgnore
    public static /* final */ int abc_btn_check_material; // = 2131230729;
    @DexIgnore
    public static /* final */ int abc_btn_check_to_on_mtrl_000; // = 2131230731;
    @DexIgnore
    public static /* final */ int abc_btn_check_to_on_mtrl_015; // = 2131230732;
    @DexIgnore
    public static /* final */ int abc_btn_colored_material; // = 2131230733;
    @DexIgnore
    public static /* final */ int abc_btn_default_mtrl_shape; // = 2131230734;
    @DexIgnore
    public static /* final */ int abc_btn_radio_material; // = 2131230735;
    @DexIgnore
    public static /* final */ int abc_btn_radio_to_on_mtrl_000; // = 2131230737;
    @DexIgnore
    public static /* final */ int abc_btn_radio_to_on_mtrl_015; // = 2131230738;
    @DexIgnore
    public static /* final */ int abc_btn_switch_to_on_mtrl_00001; // = 2131230739;
    @DexIgnore
    public static /* final */ int abc_btn_switch_to_on_mtrl_00012; // = 2131230740;
    @DexIgnore
    public static /* final */ int abc_cab_background_internal_bg; // = 2131230741;
    @DexIgnore
    public static /* final */ int abc_cab_background_top_material; // = 2131230742;
    @DexIgnore
    public static /* final */ int abc_cab_background_top_mtrl_alpha; // = 2131230743;
    @DexIgnore
    public static /* final */ int abc_control_background_material; // = 2131230744;
    @DexIgnore
    public static /* final */ int abc_edit_text_material; // = 2131230746;
    @DexIgnore
    public static /* final */ int abc_ic_commit_search_api_mtrl_alpha; // = 2131230750;
    @DexIgnore
    public static /* final */ int abc_ic_menu_copy_mtrl_am_alpha; // = 2131230752;
    @DexIgnore
    public static /* final */ int abc_ic_menu_cut_mtrl_alpha; // = 2131230753;
    @DexIgnore
    public static /* final */ int abc_ic_menu_paste_mtrl_am_alpha; // = 2131230755;
    @DexIgnore
    public static /* final */ int abc_ic_menu_selectall_mtrl_alpha; // = 2131230756;
    @DexIgnore
    public static /* final */ int abc_ic_menu_share_mtrl_alpha; // = 2131230757;
    @DexIgnore
    public static /* final */ int abc_ic_star_black_16dp; // = 2131230759;
    @DexIgnore
    public static /* final */ int abc_ic_star_black_36dp; // = 2131230760;
    @DexIgnore
    public static /* final */ int abc_ic_star_half_black_16dp; // = 2131230762;
    @DexIgnore
    public static /* final */ int abc_ic_star_half_black_36dp; // = 2131230763;
    @DexIgnore
    public static /* final */ int abc_item_background_holo_dark; // = 2131230766;
    @DexIgnore
    public static /* final */ int abc_item_background_holo_light; // = 2131230767;
    @DexIgnore
    public static /* final */ int abc_list_divider_mtrl_alpha; // = 2131230769;
    @DexIgnore
    public static /* final */ int abc_list_focused_holo; // = 2131230770;
    @DexIgnore
    public static /* final */ int abc_list_longpressed_holo; // = 2131230771;
    @DexIgnore
    public static /* final */ int abc_list_pressed_holo_dark; // = 2131230772;
    @DexIgnore
    public static /* final */ int abc_list_pressed_holo_light; // = 2131230773;
    @DexIgnore
    public static /* final */ int abc_list_selector_background_transition_holo_dark; // = 2131230774;
    @DexIgnore
    public static /* final */ int abc_list_selector_background_transition_holo_light; // = 2131230775;
    @DexIgnore
    public static /* final */ int abc_list_selector_disabled_holo_dark; // = 2131230776;
    @DexIgnore
    public static /* final */ int abc_list_selector_disabled_holo_light; // = 2131230777;
    @DexIgnore
    public static /* final */ int abc_list_selector_holo_dark; // = 2131230778;
    @DexIgnore
    public static /* final */ int abc_list_selector_holo_light; // = 2131230779;
    @DexIgnore
    public static /* final */ int abc_menu_hardkey_panel_mtrl_mult; // = 2131230780;
    @DexIgnore
    public static /* final */ int abc_popup_background_mtrl_mult; // = 2131230781;
    @DexIgnore
    public static /* final */ int abc_ratingbar_indicator_material; // = 2131230782;
    @DexIgnore
    public static /* final */ int abc_ratingbar_small_material; // = 2131230784;
    @DexIgnore
    public static /* final */ int abc_scrubber_control_off_mtrl_alpha; // = 2131230785;
    @DexIgnore
    public static /* final */ int abc_scrubber_control_to_pressed_mtrl_000; // = 2131230786;
    @DexIgnore
    public static /* final */ int abc_scrubber_control_to_pressed_mtrl_005; // = 2131230787;
    @DexIgnore
    public static /* final */ int abc_scrubber_primary_mtrl_alpha; // = 2131230788;
    @DexIgnore
    public static /* final */ int abc_scrubber_track_mtrl_alpha; // = 2131230789;
    @DexIgnore
    public static /* final */ int abc_seekbar_thumb_material; // = 2131230790;
    @DexIgnore
    public static /* final */ int abc_seekbar_track_material; // = 2131230792;
    @DexIgnore
    public static /* final */ int abc_spinner_mtrl_am_alpha; // = 2131230793;
    @DexIgnore
    public static /* final */ int abc_spinner_textfield_background_material; // = 2131230794;
    @DexIgnore
    public static /* final */ int abc_switch_thumb_material; // = 2131230795;
    @DexIgnore
    public static /* final */ int abc_switch_track_mtrl_alpha; // = 2131230796;
    @DexIgnore
    public static /* final */ int abc_tab_indicator_material; // = 2131230797;
    @DexIgnore
    public static /* final */ int abc_tab_indicator_mtrl_alpha; // = 2131230798;
    @DexIgnore
    public static /* final */ int abc_text_cursor_material; // = 2131230799;
    @DexIgnore
    public static /* final */ int abc_textfield_activated_mtrl_alpha; // = 2131230806;
    @DexIgnore
    public static /* final */ int abc_textfield_default_mtrl_alpha; // = 2131230807;
    @DexIgnore
    public static /* final */ int abc_textfield_search_activated_mtrl_alpha; // = 2131230808;
    @DexIgnore
    public static /* final */ int abc_textfield_search_default_mtrl_alpha; // = 2131230809;
    @DexIgnore
    public static /* final */ int abc_textfield_search_material; // = 2131230810;
    @DexIgnore
    public static /* final */ int ic_camera; // = 2131231048;
    @DexIgnore
    public static /* final */ int ic_image; // = 2131231090;
    @DexIgnore
    public static /* final */ int notification_template_icon_bg; // = 2131231282;
}
