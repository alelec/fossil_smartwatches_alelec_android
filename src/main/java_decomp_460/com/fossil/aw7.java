package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aw7 implements Executor {
    @DexIgnore
    public /* final */ dv7 b;

    @DexIgnore
    public void execute(Runnable runnable) {
        this.b.M(un7.INSTANCE, runnable);
    }

    @DexIgnore
    public String toString() {
        return this.b.toString();
    }
}
