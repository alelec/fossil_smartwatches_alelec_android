package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yg1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static volatile boolean f4312a; // = true;

    @DexIgnore
    public static Drawable a(Context context, int i, Resources.Theme theme) {
        return c(context, context, i, theme);
    }

    @DexIgnore
    public static Drawable b(Context context, Context context2, int i) {
        return c(context, context2, i, null);
    }

    @DexIgnore
    public static Drawable c(Context context, Context context2, int i, Resources.Theme theme) {
        try {
            if (f4312a) {
                return e(context2, i, theme);
            }
        } catch (NoClassDefFoundError e) {
            f4312a = false;
        } catch (IllegalStateException e2) {
            if (!context.getPackageName().equals(context2.getPackageName())) {
                return gl0.f(context2, i);
            }
            throw e2;
        } catch (Resources.NotFoundException e3) {
        }
        if (theme == null) {
            theme = context2.getTheme();
        }
        return d(context2, i, theme);
    }

    @DexIgnore
    public static Drawable d(Context context, int i, Resources.Theme theme) {
        return nl0.a(context.getResources(), i, theme);
    }

    @DexIgnore
    public static Drawable e(Context context, int i, Resources.Theme theme) {
        if (theme != null) {
            context = new qf0(context, theme);
        }
        return gf0.d(context, i);
    }
}
