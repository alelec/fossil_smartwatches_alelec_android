package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.f57;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputActivity;
import com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendActivity;
import com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeActivity;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.customview.FriendsInView;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lw4 extends pv5 implements t47.g {
    @DexIgnore
    public static /* final */ int A; // = gl0.d(PortfolioApp.h0.c(), 2131099689);
    @DexIgnore
    public static /* final */ int B; // = gl0.d(PortfolioApp.h0.c(), 2131099677);
    @DexIgnore
    public static /* final */ String C; // = qn5.l.a().d("nonBrandPlaceholderBackground");
    @DexIgnore
    public static /* final */ a D; // = new a(null);
    @DexIgnore
    public static /* final */ String z;
    @DexIgnore
    public po4 g;
    @DexIgnore
    public ow4 h;
    @DexIgnore
    public ps4 i;
    @DexIgnore
    public String j;
    @DexIgnore
    public int k; // = -1;
    @DexIgnore
    public String l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public g37<yb5> s;
    @DexIgnore
    public gp7<tl7> t; // = b.INSTANCE;
    @DexIgnore
    public gp7<tl7> u; // = c.INSTANCE;
    @DexIgnore
    public /* final */ TimerViewObserver v; // = new TimerViewObserver();
    @DexIgnore
    public /* final */ f57.b w;
    @DexIgnore
    public /* final */ uy4 x;
    @DexIgnore
    public HashMap y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return lw4.z;
        }

        @DexIgnore
        public final lw4 b(ps4 ps4, String str, int i, String str2, boolean z) {
            lw4 lw4 = new lw4();
            Bundle bundle = new Bundle();
            bundle.putParcelable("challenge_extra", ps4);
            bundle.putString("category_extra", str);
            bundle.putInt("index_extra", i);
            bundle.putString("challenge_id_extra", str2);
            bundle.putBoolean("about_extra", z);
            lw4.setArguments(bundle);
            return lw4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements gp7<tl7> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qq7 implements gp7<tl7> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<gl7<? extends String, ? extends String, ? extends String>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ d f2259a; // = new d();

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(gl7<String, String, String> gl7) {
            if (pq7.a(gl7.getFirst(), "bc_left_challenge_before_start")) {
                xr4.f4164a.h(gl7.getFirst(), gl7.getSecond(), gl7.getThird(), 0, PortfolioApp.h0.c());
            } else {
                xr4.f4164a.a(gl7.getFirst(), gl7.getSecond(), gl7.getThird(), PortfolioApp.h0.c());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<ps4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lw4 f2260a;

        @DexIgnore
        public e(lw4 lw4) {
            this.f2260a = lw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ps4 ps4) {
            if (ps4 != null) {
                this.f2260a.d7(ps4);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<ps4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lw4 f2261a;

        @DexIgnore
        public f(lw4 lw4) {
            this.f2261a = lw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ps4 ps4) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = lw4.D.a();
            local.e(a2, "pendingChallenge - data: " + ps4);
            if (ps4 != null) {
                this.f2261a.d7(ps4);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<cl7<? extends Boolean, ? extends ServerError>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lw4 f2262a;

        @DexIgnore
        public g(lw4 lw4) {
            this.f2262a = lw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, ? extends ServerError> cl7) {
            yb5 yb5 = (yb5) lw4.N6(this.f2262a).a();
            if (yb5 != null) {
                boolean booleanValue = cl7.getFirst().booleanValue();
                ServerError serverError = (ServerError) cl7.getSecond();
                if (booleanValue) {
                    FlexibleTextView flexibleTextView = yb5.y;
                    pq7.b(flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView2 = yb5.y;
                pq7.b(flexibleTextView2, "ftvError");
                String c = um5.c(flexibleTextView2.getContext(), 2131886231);
                FragmentActivity activity = this.f2262a.getActivity();
                if (activity != null) {
                    Toast.makeText(activity, c, 1).show();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ls0<cl7<? extends Boolean, ? extends Boolean>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lw4 f2263a;

        @DexIgnore
        public h(lw4 lw4) {
            this.f2263a = lw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, Boolean> cl7) {
            yb5 yb5;
            Boolean first = cl7.getFirst();
            Boolean second = cl7.getSecond();
            if (!(first == null || (yb5 = (yb5) lw4.N6(this.f2263a).a()) == null)) {
                SwipeRefreshLayout swipeRefreshLayout = yb5.G;
                pq7.b(swipeRefreshLayout, "swipeRefresh");
                swipeRefreshLayout.setRefreshing(first.booleanValue());
            }
            if (second == null) {
                return;
            }
            if (pq7.a(second, Boolean.TRUE)) {
                this.f2263a.b();
            } else if (pq7.a(second, Boolean.FALSE)) {
                this.f2263a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ls0<cl7<? extends Boolean, ? extends ServerError>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lw4 f2264a;

        @DexIgnore
        public i(lw4 lw4) {
            this.f2264a = lw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, ? extends ServerError> cl7) {
            Intent intent;
            String str = null;
            if (cl7.getFirst().booleanValue()) {
                if (this.f2264a.k != -1) {
                    intent = new Intent();
                    intent.putExtra("index_extra", this.f2264a.k);
                } else {
                    intent = null;
                }
                this.f2264a.b7(intent);
                return;
            }
            ServerError serverError = (ServerError) cl7.getSecond();
            this.f2264a.h7(serverError != null ? serverError.getCode() : null, serverError != null ? serverError.getMessage() : null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = lw4.D.a();
            StringBuilder sb = new StringBuilder();
            sb.append("code: ");
            sb.append(serverError != null ? serverError.getCode() : null);
            sb.append(" - message: ");
            if (serverError != null) {
                str = serverError.getMessage();
            }
            sb.append(str);
            local.e(a2, sb.toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements ls0<gl7<? extends List<? extends at4>, ? extends ServerError, ? extends Integer>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lw4 f2265a;

        @DexIgnore
        public j(lw4 lw4) {
            this.f2265a = lw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(gl7<? extends List<at4>, ? extends ServerError, Integer> gl7) {
            yb5 yb5 = (yb5) lw4.N6(this.f2265a).a();
            if (yb5 != null) {
                List<at4> list = (List) gl7.getFirst();
                if (list != null && (!list.isEmpty())) {
                    FriendsInView friendsInView = yb5.t;
                    pq7.b(friendsInView, "friendsInView");
                    friendsInView.setVisibility(0);
                    yb5.t.setData(list);
                }
                Integer third = gl7.getThird();
                if (third != null) {
                    hr7 hr7 = hr7.f1520a;
                    String c = um5.c(PortfolioApp.h0.c(), 2131886215);
                    pq7.b(c, "LanguageHelper.getString\u2026_Subtitle__NumberMembers)");
                    String format = String.format(c, Arrays.copyOf(new Object[]{third}, 1));
                    pq7.b(format, "java.lang.String.format(format, *args)");
                    FlexibleTextView flexibleTextView = yb5.w;
                    pq7.b(flexibleTextView, "ftvCountMember");
                    flexibleTextView.setText(jl5.b(jl5.b, String.valueOf(third.intValue()), format, 0, 4, null));
                    FlexibleTextView flexibleTextView2 = yb5.w;
                    pq7.b(flexibleTextView2, "ftvCountMember");
                    flexibleTextView2.setTag(third);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements ls0<List<? extends gs4>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lw4 f2266a;

        @DexIgnore
        public k(lw4 lw4) {
            this.f2266a = lw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<gs4> list) {
            lw4 lw4 = this.f2266a;
            pq7.b(list, "it");
            lw4.f7(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements ls0<cl7<? extends vy4, ? extends ps4>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lw4 f2267a;

        @DexIgnore
        public l(lw4 lw4) {
            this.f2267a = lw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<? extends vy4, ps4> cl7) {
            int i = mw4.f2429a[((vy4) cl7.getFirst()).ordinal()];
            if (i == 1) {
                this.f2267a.i7(cl7.getSecond());
            } else if (i == 2) {
                this.f2267a.j7(cl7.getSecond());
            } else if (i != 3) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = lw4.D.a();
                local.e(a2, "optionActionLive - wrong option: " + cl7);
            } else {
                this.f2267a.g7();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements ls0<iz4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lw4 f2268a;

        @DexIgnore
        public m(lw4 lw4) {
            this.f2268a = lw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(iz4 iz4) {
            boolean z = true;
            if (iz4 != null) {
                int i = mw4.b[iz4.ordinal()];
                if (i == 1) {
                    s37 s37 = s37.c;
                    FragmentManager childFragmentManager = this.f2268a.getChildFragmentManager();
                    pq7.b(childFragmentManager, "childFragmentManager");
                    String c = um5.c(PortfolioApp.h0.c(), 2131886235);
                    pq7.b(c, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    String c2 = um5.c(PortfolioApp.h0.c(), 2131886233);
                    pq7.b(c2, "LanguageHelper.getString\u2026uCanOnlyJoinOneChallenge)");
                    s37.E(childFragmentManager, c, c2);
                    return;
                } else if (i == 2) {
                    if (PortfolioApp.h0.c().J().length() <= 0) {
                        z = false;
                    }
                    if (z) {
                        s37 s372 = s37.c;
                        FragmentManager childFragmentManager2 = this.f2268a.getChildFragmentManager();
                        pq7.b(childFragmentManager2, "childFragmentManager");
                        String c3 = um5.c(PortfolioApp.h0.c(), 2131886235);
                        pq7.b(c3, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                        String c4 = um5.c(PortfolioApp.h0.c(), 2131886234);
                        pq7.b(c4, "LanguageHelper.getString\u2026IsDisconnectedPleaseSync)");
                        s372.E(childFragmentManager2, c3, c4);
                        return;
                    }
                    s37 s373 = s37.c;
                    FragmentManager childFragmentManager3 = this.f2268a.getChildFragmentManager();
                    pq7.b(childFragmentManager3, "childFragmentManager");
                    String c5 = um5.c(PortfolioApp.h0.c(), 2131886235);
                    pq7.b(c5, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    String c6 = um5.c(PortfolioApp.h0.c(), 2131886226);
                    pq7.b(c6, "LanguageHelper.getString\u2026ctivateYourDeviceToJoinA)");
                    s373.E(childFragmentManager3, c5, c6);
                    return;
                } else if (i == 3) {
                    s37 s374 = s37.c;
                    FragmentManager childFragmentManager4 = this.f2268a.getChildFragmentManager();
                    pq7.b(childFragmentManager4, "childFragmentManager");
                    s374.C0(childFragmentManager4);
                    return;
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = lw4.D.a();
            local.e(a2, "warningTypeDialogLive - wrong type: " + iz4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ ps4 $challenge$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ String $subject$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ yb5 $this_run$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ lw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(yb5 yb5, String str, lw4 lw4, ps4 ps4) {
            super(0);
            this.$this_run$inlined = yb5;
            this.$subject$inlined = str;
            this.this$0 = lw4;
            this.$challenge$inlined = ps4;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            lw4.S6(this.this$0).a(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ ps4 $challenge$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ String $subject$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ yb5 $this_run$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ lw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(yb5 yb5, String str, lw4 lw4, ps4 ps4) {
            super(0);
            this.$this_run$inlined = yb5;
            this.$subject$inlined = str;
            this.this$0 = lw4;
            this.$challenge$inlined = ps4;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            lw4.S6(this.this$0).a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p extends qq7 implements gp7<tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ ps4 $challenge$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ lw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(lw4 lw4, ps4 ps4) {
            super(0);
            this.this$0 = lw4;
            this.$challenge$inlined = ps4;
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
            Date m = this.$challenge$inlined.m();
            this.this$0.k7(this.$challenge$inlined.f(), (m != null ? m.getTime() : 0) > xy4.f4212a.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q extends qq7 implements gp7<tl7> {
        @DexIgnore
        public static /* final */ q INSTANCE; // = new q();

        @DexIgnore
        public q() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final void invoke() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lw4 b;

        @DexIgnore
        public r(lw4 lw4) {
            this.b = lw4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.B0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s extends qq7 implements rp7<View, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ lw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public s(lw4 lw4) {
            super(1);
            this.this$0 = lw4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(View view) {
            invoke(view);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(View view) {
            this.this$0.t.invoke();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t extends qq7 implements rp7<View, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ lw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public t(lw4 lw4) {
            super(1);
            this.this$0 = lw4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(View view) {
            invoke(view);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(View view) {
            lw4.S6(this.this$0).D();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class u implements SwipeRefreshLayout.j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ yb5 f2269a;
        @DexIgnore
        public /* final */ /* synthetic */ lw4 b;

        @DexIgnore
        public u(yb5 yb5, lw4 lw4) {
            this.f2269a = yb5;
            this.b = lw4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.f2269a.y;
            pq7.b(flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            lw4.S6(this.b).G();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class v extends qq7 implements rp7<View, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ lw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public v(lw4 lw4) {
            super(1);
            this.this$0 = lw4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(View view) {
            invoke(view);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(View view) {
            this.this$0.u.invoke();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class w extends qq7 implements rp7<View, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ lw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public w(lw4 lw4) {
            super(1);
            this.this$0 = lw4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(View view) {
            invoke(view);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(View view) {
            ps4 ps4 = this.this$0.i;
            if (ps4 != null) {
                Date m = ps4.m();
                this.this$0.k7(ps4.f(), (m != null ? m.getTime() : 0) > xy4.f4212a.b());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class x implements my5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lw4 f2270a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public x(lw4 lw4) {
            this.f2270a = lw4;
        }

        @DexIgnore
        @Override // com.fossil.my5
        public void a(gs4 gs4) {
            pq7.c(gs4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            ow4 S6 = lw4.S6(this.f2270a);
            Object a2 = gs4.a();
            if (a2 != null) {
                S6.E((vy4) a2);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.buddy_challenge.util.DetailOptionDialog");
        }
    }

    /*
    static {
        String simpleName = lw4.class.getSimpleName();
        pq7.b(simpleName, "BCWaitingChallengeDetail\u2026nt::class.java.simpleName");
        z = simpleName;
    }
    */

    @DexIgnore
    public lw4() {
        f57.b f2 = f57.a().f();
        pq7.b(f2, "TextDrawable.builder().round()");
        this.w = f2;
        this.x = uy4.d.b();
    }

    @DexIgnore
    public static final /* synthetic */ g37 N6(lw4 lw4) {
        g37<yb5> g37 = lw4.s;
        if (g37 != null) {
            return g37;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ow4 S6(lw4 lw4) {
        ow4 ow4 = lw4.h;
        if (ow4 != null) {
            return ow4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void B0() {
        if (this.l != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finishAffinity();
            }
            Context context = getContext();
            if (context != null) {
                HomeActivity.a aVar = HomeActivity.B;
                pq7.b(context, "it");
                aVar.a(context, 1);
                return;
            }
            return;
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        if (str.hashCode() == 1970588827 && str.equals("LEAVE_CHALLENGE") && i2 == 2131363373) {
            ow4 ow4 = this.h;
            if (ow4 != null) {
                ow4.z();
            } else {
                pq7.n("viewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void a7() {
        ps4 ps4 = this.i;
        if (ps4 != null) {
            cl5.c.d(PortfolioApp.h0.c(), ps4.f().hashCode());
        }
        String str = this.l;
        if (str != null) {
            cl5.c.d(PortfolioApp.h0.c(), str.hashCode());
        }
    }

    @DexIgnore
    public final void b7(Intent intent) {
        if (this.l != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finishAffinity();
            }
            Context context = getContext();
            if (context != null) {
                HomeActivity.a aVar = HomeActivity.B;
                pq7.b(context, "it");
                aVar.a(context, 1);
                return;
            }
            return;
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.setResult(-1, intent);
        }
        FragmentActivity activity3 = getActivity();
        if (activity3 != null) {
            activity3.finish();
        }
    }

    @DexIgnore
    public final void c7() {
        ow4 ow4 = this.h;
        if (ow4 != null) {
            ow4.p().h(getViewLifecycleOwner(), new e(this));
            ow4 ow42 = this.h;
            if (ow42 != null) {
                LiveData<ps4> s2 = ow42.s();
                if (s2 != null) {
                    s2.h(getViewLifecycleOwner(), new f(this));
                }
                ow4 ow43 = this.h;
                if (ow43 != null) {
                    ow43.q().h(getViewLifecycleOwner(), new g(this));
                    ow4 ow44 = this.h;
                    if (ow44 != null) {
                        ow44.t().h(getViewLifecycleOwner(), new h(this));
                        ow4 ow45 = this.h;
                        if (ow45 != null) {
                            ow45.u().h(getViewLifecycleOwner(), new i(this));
                            ow4 ow46 = this.h;
                            if (ow46 != null) {
                                ow46.r().h(getViewLifecycleOwner(), new j(this));
                                ow4 ow47 = this.h;
                                if (ow47 != null) {
                                    ow47.w().h(getViewLifecycleOwner(), new k(this));
                                    ow4 ow48 = this.h;
                                    if (ow48 != null) {
                                        ow48.v().h(getViewLifecycleOwner(), new l(this));
                                        ow4 ow49 = this.h;
                                        if (ow49 != null) {
                                            ow49.x().h(getViewLifecycleOwner(), new m(this));
                                            ow4 ow410 = this.h;
                                            if (ow410 != null) {
                                                ow410.o().h(getViewLifecycleOwner(), d.f2259a);
                                            } else {
                                                pq7.n("viewModel");
                                                throw null;
                                            }
                                        } else {
                                            pq7.n("viewModel");
                                            throw null;
                                        }
                                    } else {
                                        pq7.n("viewModel");
                                        throw null;
                                    }
                                } else {
                                    pq7.n("viewModel");
                                    throw null;
                                }
                            } else {
                                pq7.n("viewModel");
                                throw null;
                            }
                        } else {
                            pq7.n("viewModel");
                            throw null;
                        }
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0534  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void d7(com.fossil.ps4 r12) {
        /*
        // Method dump skipped, instructions count: 1342
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.lw4.d7(com.fossil.ps4):void");
    }

    @DexIgnore
    public final void e7() {
        g37<yb5> g37 = this.s;
        if (g37 != null) {
            yb5 a2 = g37.a();
            if (a2 != null) {
                a2.C.setOnClickListener(new r(this));
                String str = C;
                if (str != null) {
                    a2.w.setBackgroundColor(Color.parseColor(str));
                }
                FlexibleButton flexibleButton = a2.q;
                pq7.b(flexibleButton, "btnAction");
                fz4.a(flexibleButton, new s(this));
                RTLImageView rTLImageView = a2.B;
                pq7.b(rTLImageView, "imgOption");
                fz4.a(rTLImageView, new t(this));
                a2.G.setOnRefreshListener(new u(a2, this));
                FlexibleTextView flexibleTextView = a2.w;
                pq7.b(flexibleTextView, "ftvCountMember");
                fz4.a(flexibleTextView, new v(this));
                FriendsInView friendsInView = a2.t;
                pq7.b(friendsInView, "friendsInView");
                fz4.a(friendsInView, new w(this));
                return;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final void f7(List<gs4> list) {
        es4 b2 = es4.A.b();
        String c2 = um5.c(requireContext(), 2131886322);
        pq7.b(c2, "LanguageHelper.getString\u2026rBoard_Menu_Title__Allow)");
        b2.setTitle(c2);
        b2.E6(list);
        b2.G6(new x(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        b2.show(childFragmentManager, es4.A.a());
    }

    @DexIgnore
    public final void g7() {
        s37 s37 = s37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        s37.h(childFragmentManager);
    }

    @DexIgnore
    public final void h7(Integer num, String str) {
        if (num == null) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.C(childFragmentManager);
            return;
        }
        s37 s372 = s37.c;
        int intValue = num.intValue();
        FragmentManager childFragmentManager2 = getChildFragmentManager();
        pq7.b(childFragmentManager2, "childFragmentManager");
        s372.n0(intValue, str, childFragmentManager2);
    }

    @DexIgnore
    public final void i7(ps4 ps4) {
        BCCreateChallengeInputActivity.A.a(this, py4.g(ps4));
    }

    @DexIgnore
    public final void j7(ps4 ps4) {
        BCInviteFriendActivity.A.a(this, py4.g(ps4));
    }

    @DexIgnore
    public final void k7(String str, boolean z2) {
        BCMemberInChallengeActivity.A.a(this, str, z2);
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        this.i = arguments != null ? (ps4) arguments.getParcelable("challenge_extra") : null;
        Bundle arguments2 = getArguments();
        this.j = arguments2 != null ? arguments2.getString("category_extra") : null;
        Bundle arguments3 = getArguments();
        this.k = arguments3 != null ? arguments3.getInt("index_extra") : -1;
        Bundle arguments4 = getArguments();
        this.l = arguments4 != null ? arguments4.getString("challenge_id_extra") : null;
        Bundle arguments5 = getArguments();
        this.m = arguments5 != null ? arguments5.getBoolean("about_extra") : false;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = z;
        local.e(str, "onCreate - challenge: " + this.i + " - category: " + this.j + " - index: " + this.k + " - visitId: " + this.l + " - about: " + this.m);
        a7();
        PortfolioApp.h0.c().M().Y0().a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(ow4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ailViewModel::class.java)");
            this.h = (ow4) a2;
            getLifecycle().a(this.v);
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        yb5 yb5 = (yb5) aq0.f(layoutInflater, 2131558635, viewGroup, false, A6());
        this.s = new g37<>(this, yb5);
        pq7.b(yb5, "binding");
        return yb5.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        ow4 ow4 = this.h;
        if (ow4 != null) {
            ow4.F();
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ow4 ow4 = this.h;
        if (ow4 != null) {
            ow4.H();
            ck5 g2 = ck5.f.g();
            FragmentActivity activity = getActivity();
            if (activity != null) {
                g2.m("bc_challenge_detail", activity);
                return;
            }
            throw new il7("null cannot be cast to non-null type android.app.Activity");
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        ow4 ow4 = this.h;
        if (ow4 != null) {
            ow4.y(this.i, this.l);
            e7();
            c7();
            ps4 ps4 = this.i;
            if (ps4 != null) {
                d7(ps4);
                return;
            }
            return;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
