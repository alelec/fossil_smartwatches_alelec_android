package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface w74 {
    @DexIgnore
    boolean a(String str);

    @DexIgnore
    z74 b(String str);

    @DexIgnore
    void c(String str, int i, String str2, int i2, long j, long j2, boolean z, int i3, String str3, String str4);

    @DexIgnore
    void d(String str, String str2, long j);

    @DexIgnore
    boolean e(String str);

    @DexIgnore
    void f(String str, String str2, String str3, String str4, String str5, int i, String str6);

    @DexIgnore
    void g(String str, String str2, String str3, boolean z);

    @DexIgnore
    boolean h(String str);
}
