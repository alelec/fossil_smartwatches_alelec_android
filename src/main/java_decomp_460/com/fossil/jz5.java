package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.bv5;
import com.fossil.iq4;
import com.fossil.ou5;
import com.fossil.tq4;
import com.fossil.wq5;
import com.fossil.yt5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jz5 extends yy5 {
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ a y; // = new a(null);
    @DexIgnore
    public int e;
    @DexIgnore
    public volatile boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public LiveData<List<InAppNotification>> h;
    @DexIgnore
    public /* final */ f i; // = new f(this);
    @DexIgnore
    public /* final */ i j; // = new i(this);
    @DexIgnore
    public /* final */ h k; // = new h(this);
    @DexIgnore
    public /* final */ zy5 l;
    @DexIgnore
    public /* final */ on5 m;
    @DexIgnore
    public /* final */ DeviceRepository n;
    @DexIgnore
    public /* final */ PortfolioApp o;
    @DexIgnore
    public /* final */ yt5 p;
    @DexIgnore
    public /* final */ uq4 q;
    @DexIgnore
    public /* final */ mj5 r;
    @DexIgnore
    public /* final */ bv5 s;
    @DexIgnore
    public /* final */ n47 t;
    @DexIgnore
    public /* final */ ServerSettingRepository u;
    @DexIgnore
    public /* final */ ou5 v;
    @DexIgnore
    public /* final */ pr4 w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return jz5.x;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$1", f = "HomePresenter.kt", l = {282}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ jz5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$1$isLatestFw$1", f = "HomePresenter.kt", l = {282}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    p37 a2 = p37.h.a();
                    String str = this.this$0.$activeDeviceSerial;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object j = a2.j(str, null, this);
                    return j == d ? d : j;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(jz5 jz5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = jz5;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$activeDeviceSerial, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                FLogger.INSTANCE.getLocal().d(jz5.y.a(), "checkFirmware() enter checkFirmware");
                dv7 h = this.this$0.h();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(h, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            boolean booleanValue = ((Boolean) g).booleanValue();
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String str = this.$activeDeviceSerial;
            String a2 = jz5.y.a();
            remote.i(component, session, str, a2, "[Sync OK] Is device has latest FW " + booleanValue);
            if (booleanValue) {
                FLogger.INSTANCE.getLocal().d(jz5.y.a(), "checkFirmware() fw is latest, skip OTA after sync success");
            } else {
                this.this$0.S();
            }
            FLogger.INSTANCE.getLocal().d(jz5.y.a(), "checkFirmware() exit checkFirmware");
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements tq4.d<bv5.a.c, bv5.a.C0023a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ jz5 f1841a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(jz5 jz5) {
            this.f1841a = jz5;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(bv5.a.C0023a aVar) {
            FLogger.INSTANCE.getLocal().d(jz5.y.a(), "GetServerSettingUseCase onError");
            this.f1841a.M(false);
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(bv5.a.c cVar) {
            FLogger.INSTANCE.getLocal().d(jz5.y.a(), "GetServerSettingUseCase onSuccess");
            this.f1841a.M(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1", f = "HomePresenter.kt", l = {332}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $hasServerSettings;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ jz5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1$1", f = "HomePresenter.kt", l = {352, 353}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public long J$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.jz5$d$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1$1$1", f = "HomePresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.jz5$d$a$a  reason: collision with other inner class name */
            public static final class C0133a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0133a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0133a aVar = new C0133a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0133a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        this.this$0.this$0.this$0.l.O3();
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:14:0x007a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r14) {
                /*
                // Method dump skipped, instructions count: 429
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.jz5.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(jz5 jz5, boolean z, qn7 qn7) {
            super(2, qn7);
            this.this$0 = jz5;
            this.$hasServerSettings = z;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$hasServerSettings, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(i2, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$1", f = "HomePresenter.kt", l = {253}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeSerial;
        @DexIgnore
        public /* final */ /* synthetic */ String $lastFwTemp;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ jz5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$1$activeDevice$1", f = "HomePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Device>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Device> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.n.getDeviceBySerial(this.this$0.$activeSerial);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(jz5 jz5, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = jz5;
            this.$activeSerial = str;
            this.$lastFwTemp = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, this.$activeSerial, this.$lastFwTemp, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            String str = null;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(i2, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Device device = (Device) g;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = jz5.y.a();
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .checkLastOTASuccess, getDeviceBySerial success currentDeviceFw ");
            if (device != null) {
                str = device.getFirmwareRevision();
            }
            sb.append(str);
            local.d(a2, sb.toString());
            if (device != null && !vt7.j(this.$lastFwTemp, device.getFirmwareRevision(), true)) {
                FLogger.INSTANCE.getLocal().d(jz5.y.a(), "Handle OTA complete on check last OTA success");
                this.this$0.m.B0(this.$activeSerial);
                this.this$0.m.X0(this.this$0.o.J(), -1);
                this.this$0.l.A(true);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements wq5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ jz5 f1842a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(jz5 jz5) {
            this.f1842a = jz5;
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && vt7.j(stringExtra, this.f1842a.o.J(), true)) {
                this.f1842a.l.C1(intent);
                int intExtra = intent.getIntExtra("sync_result", 3);
                int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>();
                }
                int intExtra3 = intent.getIntExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), 10);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = jz5.y.a();
                local.d(a2, "Inside .syncReceiver syncResult=" + intExtra + ", serial=" + stringExtra + ", errorCode=" + intExtra2 + " permissionErrors " + integerArrayListExtra + " originalSyncMode " + intExtra3);
                if (intExtra == 1) {
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.OTHER;
                    pq7.b(stringExtra, "serial");
                    remote.i(component, session, stringExtra, jz5.y.a(), "[Sync OK] Check FW to OTA");
                    this.f1842a.K();
                    this.f1842a.L();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.HomePresenter$onActiveSerialChange$1", f = "HomePresenter.kt", l = {174}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $appMode;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ jz5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(jz5 jz5, String str, int i, qn7 qn7) {
            super(2, qn7);
            this.this$0 = jz5;
            this.$serial = str;
            this.$appMode = i;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, this.$serial, this.$appMode, qn7);
            gVar.p$ = (iv7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00ab  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0057  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r6) {
            /*
                r5 = this;
                r3 = 1
                java.lang.Object r1 = com.fossil.yn7.d()
                int r0 = r5.label
                if (r0 == 0) goto L_0x0073
                if (r0 != r3) goto L_0x006b
                java.lang.Object r0 = r5.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r6)
                r0 = r6
            L_0x0013:
                java.lang.Boolean r0 = (java.lang.Boolean) r0
                boolean r0 = r0.booleanValue()
                com.fossil.jz5 r1 = r5.this$0
                com.fossil.on5 r1 = com.fossil.jz5.D(r1)
                java.lang.Boolean r2 = com.fossil.ao7.a(r0)
                r1.O0(r2)
            L_0x0026:
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.fossil.jz5$a r2 = com.fossil.jz5.y
                java.lang.String r2 = r2.a()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "isBCOn: "
                r3.append(r4)
                r3.append(r0)
                java.lang.String r3 = r3.toString()
                r1.e(r2, r3)
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.h0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                r1.w(r0)
                java.lang.String r1 = r5.$serial
                boolean r1 = android.text.TextUtils.isEmpty(r1)
                if (r1 != 0) goto L_0x00ab
                com.fossil.jz5 r1 = r5.this$0
                com.fossil.zy5 r1 = com.fossil.jz5.F(r1)
                java.lang.String r2 = r5.$serial
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r2 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.getDeviceBySerial(r2)
                int r3 = r5.$appMode
                r1.L0(r2, r3, r0)
            L_0x0068:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x006a:
                return r0
            L_0x006b:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0073:
                com.fossil.el7.b(r6)
                com.fossil.iv7 r0 = r5.p$
                com.fossil.jz5 r2 = r5.this$0
                boolean r2 = com.fossil.jz5.x(r2)
                if (r2 == 0) goto L_0x0094
                com.fossil.jz5 r0 = r5.this$0
                com.fossil.on5 r0 = com.fossil.jz5.D(r0)
                java.lang.Boolean r0 = r0.b()
                java.lang.String r1 = "mSharedPreferencesManager.bcStatus()"
                com.fossil.pq7.b(r0, r1)
                boolean r0 = r0.booleanValue()
                goto L_0x0026
            L_0x0094:
                com.fossil.jz5 r2 = r5.this$0
                com.fossil.jz5.I(r2, r3)
                com.fossil.jz5 r2 = r5.this$0
                com.fossil.pr4 r2 = com.fossil.jz5.w(r2)
                r5.L$0 = r0
                r5.label = r3
                java.lang.Object r0 = r2.g(r5)
                if (r0 != r1) goto L_0x0013
                r0 = r1
                goto L_0x006a
            L_0x00ab:
                com.fossil.jz5 r1 = r5.this$0
                com.fossil.zy5 r1 = com.fossil.jz5.F(r1)
                r2 = 0
                int r3 = r5.$appMode
                r1.L0(r2, r3, r0)
                goto L_0x0068
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.jz5.g.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements wq5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ jz5 f1843a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public h(jz5 jz5) {
            this.f1843a = jz5;
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            this.f1843a.Q(false);
            boolean booleanExtra = intent.getBooleanExtra("OTA_RESULT", false);
            boolean v0 = this.f1843a.o.v0();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = jz5.y.a();
            local.d(a2, "ota complete isSuccess" + booleanExtra + " isDeviceStillOta " + v0);
            if (!v0 || !booleanExtra) {
                this.f1843a.l.A(booleanExtra);
            }
            if (booleanExtra) {
                this.f1843a.m.X0(this.f1843a.o.J(), -1);
                PortfolioApp.h0.c().S1(this.f1843a.r, false, 13);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ jz5 f1844a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(jz5 jz5) {
            this.f1844a = jz5;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            pq7.c(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = jz5.y.a();
            local.d(a2, "---Inside .otaProgressReceiver ota progress " + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
            if (!TextUtils.isEmpty(otaEvent.getSerial())) {
                if (!this.f1844a.O()) {
                    this.f1844a.Q(true);
                }
                if (vt7.j(otaEvent.getSerial(), this.f1844a.o.J(), true)) {
                    this.f1844a.l.o0((int) otaEvent.getProcess());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements iq4.e<ou5.c, ou5.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ jz5 f1845a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends BaseZendeskFeedbackConfiguration {
            @DexIgnore
            public /* final */ /* synthetic */ ou5.c $responseValue;

            @DexIgnore
            public a(ou5.c cVar) {
                this.$responseValue = cVar;
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public String getAdditionalInfo() {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = jz5.y.a();
                local.d(a2, "Inside. getAdditionalInfo: \n" + this.$responseValue.a());
                return this.$responseValue.a();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration
            public String getRequestSubject() {
                return this.$responseValue.d();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public List<String> getTags() {
                return this.$responseValue.e();
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public j(jz5 jz5) {
            this.f1845a = jz5;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(ou5.a aVar) {
            pq7.c(aVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(jz5.y.a(), "startCollectingUserFeedback onError");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ou5.c cVar) {
            pq7.c(cVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(jz5.y.a(), "startCollectingUserFeedback onSuccess");
            ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(cVar.f()).withEmailIdentifier(cVar.c()).build());
            ZendeskConfig.INSTANCE.setCustomFields(cVar.b());
            this.f1845a.l.z3(new a(cVar));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements iq4.e<yt5.d, yt5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ jz5 f1846a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public k(jz5 jz5) {
            this.f1846a = jz5;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(yt5.c cVar) {
            pq7.c(cVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(yt5.d dVar) {
            pq7.c(dVar, "responseValue");
            this.f1846a.Q(true);
            this.f1846a.l.T1();
        }
    }

    /*
    static {
        String simpleName = jz5.class.getSimpleName();
        pq7.b(simpleName, "HomePresenter::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    public jz5(zy5 zy5, on5 on5, DeviceRepository deviceRepository, PortfolioApp portfolioApp, yt5 yt5, uq4 uq4, mj5 mj5, bv5 bv5, n47 n47, ServerSettingRepository serverSettingRepository, ou5 ou5, pr4 pr4) {
        pq7.c(zy5, "mView");
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(portfolioApp, "mApp");
        pq7.c(yt5, "mUpdateFwUseCase");
        pq7.c(uq4, "mUseCaseHandler");
        pq7.c(mj5, "mDeviceSettingFactory");
        pq7.c(bv5, "mGetServerSettingUseCase");
        pq7.c(n47, "mUserUtils");
        pq7.c(serverSettingRepository, "mServerSettingRepository");
        pq7.c(ou5, "mGetZendeskInformation");
        pq7.c(pr4, "flagRepository");
        this.l = zy5;
        this.m = on5;
        this.n = deviceRepository;
        this.o = portfolioApp;
        this.p = yt5;
        this.q = uq4;
        this.r = mj5;
        this.s = bv5;
        this.t = n47;
        this.u = serverSettingRepository;
        this.v = ou5;
        this.w = pr4;
    }

    @DexIgnore
    public final void K() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "checkFirmware(), isSkipOTA=" + this.m.y0());
        if (!PortfolioApp.h0.e() || !this.m.y0()) {
            String J = this.o.J();
            boolean v0 = this.o.v0();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = x;
            local2.d(str2, "checkFirmware() isDeviceOtaing=" + v0);
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String str3 = x;
            remote.i(component, session, J, str3, "[Sync OK] Is device OTAing " + v0);
            if (!v0) {
                xw7 unused = gu7.d(k(), null, null, new b(this, J, null), 3, null);
            }
        }
    }

    @DexIgnore
    public final void L() {
        this.q.a(this.s, new bv5.a.b(), new c(this));
    }

    @DexIgnore
    public final void M(boolean z) {
        xw7 unused = gu7.d(k(), null, null, new d(this, z, null), 3, null);
    }

    @DexIgnore
    public final void N() {
        String J = this.o.J();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "Inside .checkLastOTASuccess, activeSerial=" + J);
        if (!TextUtils.isEmpty(J)) {
            String Q = this.m.Q(J);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = x;
            local2.d(str2, "Inside .checkLastOTASuccess, lastTempFw=" + Q);
            if (!TextUtils.isEmpty(Q)) {
                xw7 unused = gu7.d(k(), null, null, new e(this, J, Q, null), 3, null);
            }
        }
    }

    @DexIgnore
    public final boolean O() {
        return this.f;
    }

    @DexIgnore
    public void P(int i2, int i3, Intent intent) {
        this.l.onActivityResult(i2, i3, intent);
    }

    @DexIgnore
    public final void Q(boolean z) {
        this.f = z;
    }

    @DexIgnore
    public void R() {
        this.l.M5(this);
    }

    @DexIgnore
    public final void S() {
        FLogger.INSTANCE.getLocal().d(x, "Inside .updateFirmware");
        String J = this.o.J();
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, J, x, "[Sync OK] Start update FW");
        if (!TextUtils.isEmpty(J)) {
            this.p.e(new yt5.b(J, false, 2, null), new k(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(x, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        PortfolioApp portfolioApp = this.o;
        i iVar = this.j;
        portfolioApp.registerReceiver(iVar, new IntentFilter(this.o.getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        wq5.d.e(this.k, CommunicateMode.OTA);
        wq5.d.e(this.i, CommunicateMode.SYNC);
        wq5.d.g(CommunicateMode.SYNC, CommunicateMode.OTA);
        N();
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(x, "stop");
        try {
            wq5.d.j(this.k, CommunicateMode.OTA);
            wq5.d.j(this.i, CommunicateMode.SYNC);
            this.o.unregisterReceiver(this.j);
            LiveData<List<InAppNotification>> liveData = this.h;
            if (liveData != null) {
                zy5 zy5 = this.l;
                if (zy5 != null) {
                    liveData.n((ez5) zy5);
                    return;
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeFragment");
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = x;
            local.d(str, "Exception when stop presenter=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.yy5
    public void n() {
        y87.f4259a.a();
    }

    @DexIgnore
    @Override // com.fossil.yy5
    public void o(InAppNotification inAppNotification) {
    }

    @DexIgnore
    @Override // com.fossil.yy5
    public int p() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.yy5
    public boolean q() {
        return this.f || this.o.v0();
    }

    @DexIgnore
    @Override // com.fossil.yy5
    public void r(String str) {
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        local.d(str2, "on active serial change serial " + str);
        xw7 unused = gu7.d(k(), null, null, new g(this, str, TextUtils.isEmpty(this.o.k0().R()) ? 0 : (TextUtils.isEmpty(str) || !nk5.o.v(str)) ? 1 : 2, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.yy5
    public void s() {
    }

    @DexIgnore
    @Override // com.fossil.yy5
    public void t(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "setTab " + i2);
        this.e = i2;
    }

    @DexIgnore
    @Override // com.fossil.yy5
    public void u(String str) {
        pq7.c(str, "subject");
        this.v.e(new ou5.b(str), new j(this));
    }
}
