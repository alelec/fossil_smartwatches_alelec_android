package com.fossil;

import java.util.zip.CRC32;
import java.util.zip.Checksum;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ix1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ix1 f1688a; // = new ix1();

    @DexIgnore
    public enum a {
        CRC32,
        CRC32C
    }

    @DexIgnore
    public final long a(byte[] bArr, int i, int i2, a aVar) {
        pq7.c(bArr, "data");
        pq7.c(aVar, "crcType");
        if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
            return 0;
        }
        Checksum c = c(aVar);
        c.update(bArr, i, i2);
        return c.getValue();
    }

    @DexIgnore
    public final long b(byte[] bArr, a aVar) {
        pq7.c(bArr, "data");
        pq7.c(aVar, "crcType");
        Checksum c = c(aVar);
        c.update(bArr, 0, bArr.length);
        return c.getValue();
    }

    @DexIgnore
    public final Checksum c(a aVar) {
        int i = jx1.f1827a[aVar.ordinal()];
        if (i == 1) {
            return new CRC32();
        }
        if (i == 2) {
            return new hx1();
        }
        throw new al7();
    }
}
