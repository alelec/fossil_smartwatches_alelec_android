package com.fossil;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v04 extends s04 {
    @DexIgnore
    public /* final */ TextWatcher d; // = new a();
    @DexIgnore
    public /* final */ TextInputLayout.f e; // = new b();
    @DexIgnore
    public /* final */ TextInputLayout.g f; // = new c(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements TextWatcher {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            v04 v04 = v04.this;
            v04.c.setChecked(!v04.f());
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements TextInputLayout.f {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.google.android.material.textfield.TextInputLayout.f
        public void a(TextInputLayout textInputLayout) {
            EditText editText = textInputLayout.getEditText();
            textInputLayout.setEndIconVisible(true);
            v04 v04 = v04.this;
            v04.c.setChecked(!v04.f());
            editText.removeTextChangedListener(v04.this.d);
            editText.addTextChangedListener(v04.this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements TextInputLayout.g {
        @DexIgnore
        public c(v04 v04) {
        }

        @DexIgnore
        @Override // com.google.android.material.textfield.TextInputLayout.g
        public void a(TextInputLayout textInputLayout, int i) {
            EditText editText = textInputLayout.getEditText();
            if (editText != null && i == 1) {
                editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements View.OnClickListener {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onClick(View view) {
            EditText editText = v04.this.f3188a.getEditText();
            if (editText != null) {
                int selectionEnd = editText.getSelectionEnd();
                if (v04.this.f()) {
                    editText.setTransformationMethod(null);
                } else {
                    editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
                if (selectionEnd >= 0) {
                    editText.setSelection(selectionEnd);
                }
            }
        }
    }

    @DexIgnore
    public v04(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    @Override // com.fossil.s04
    public void a() {
        this.f3188a.setEndIconDrawable(gf0.d(this.b, mw3.design_password_eye));
        TextInputLayout textInputLayout = this.f3188a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(rw3.password_toggle_content_description));
        this.f3188a.setEndIconOnClickListener(new d());
        this.f3188a.c(this.e);
        this.f3188a.d(this.f);
    }

    @DexIgnore
    public final boolean f() {
        EditText editText = this.f3188a.getEditText();
        return editText != null && (editText.getTransformationMethod() instanceof PasswordTransformationMethod);
    }
}
