package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.StreetViewPanoramaOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nf3 implements Parcelable.Creator<StreetViewPanoramaOptions> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ StreetViewPanoramaOptions createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        byte b = 0;
        byte b2 = 0;
        byte b3 = 0;
        byte b4 = 0;
        byte b5 = 0;
        ve3 ve3 = null;
        Integer num = null;
        LatLng latLng = null;
        String str = null;
        StreetViewPanoramaCamera streetViewPanoramaCamera = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 2:
                    streetViewPanoramaCamera = (StreetViewPanoramaCamera) ad2.e(parcel, t, StreetViewPanoramaCamera.CREATOR);
                    break;
                case 3:
                    str = ad2.f(parcel, t);
                    break;
                case 4:
                    latLng = (LatLng) ad2.e(parcel, t, LatLng.CREATOR);
                    break;
                case 5:
                    num = ad2.w(parcel, t);
                    break;
                case 6:
                    b = ad2.o(parcel, t);
                    break;
                case 7:
                    b2 = ad2.o(parcel, t);
                    break;
                case 8:
                    b3 = ad2.o(parcel, t);
                    break;
                case 9:
                    b4 = ad2.o(parcel, t);
                    break;
                case 10:
                    b5 = ad2.o(parcel, t);
                    break;
                case 11:
                    ve3 = (ve3) ad2.e(parcel, t, ve3.CREATOR);
                    break;
                default:
                    ad2.B(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new StreetViewPanoramaOptions(streetViewPanoramaCamera, str, latLng, num, b, b2, b3, b4, b5, ve3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ StreetViewPanoramaOptions[] newArray(int i) {
        return new StreetViewPanoramaOptions[i];
    }
}
