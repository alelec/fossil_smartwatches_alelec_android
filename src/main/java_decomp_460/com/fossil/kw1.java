package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class kw1 implements nx1 {
    @DexIgnore
    public static /* final */ ry1 f; // = new ry1(3, 2);
    @DexIgnore
    public String b;
    @DexIgnore
    public mw1 c;
    @DexIgnore
    public /* final */ ry1 d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor$buildThemePackage$1", f = "ThemeEditor.kt", l = {68}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public iv7 b;
        @DexIgnore
        public Object c;
        @DexIgnore
        public int d;
        @DexIgnore
        public /* final */ /* synthetic */ kw1 e;
        @DexIgnore
        public /* final */ /* synthetic */ ry1 f;
        @DexIgnore
        public /* final */ /* synthetic */ qy1 g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(kw1 kw1, ry1 ry1, qy1 qy1, qn7 qn7) {
            super(2, qn7);
            this.e = kw1;
            this.f = ry1;
            this.g = qy1;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            a aVar = new a(this.e, this.f, this.g, qn7);
            aVar.b = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object a2;
            Object d2 = yn7.d();
            int i = this.d;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.b;
                nc0 nc0 = nc0.f2498a;
                ec0 g2 = this.e.g();
                ry1 ry1 = this.f;
                if (ry1 == null) {
                    ry1 = kw1.f;
                }
                this.c = iv7;
                this.d = 1;
                a2 = nc0.a(g2, ry1, true, this);
                if (a2 == d2) {
                    return d2;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.c;
                el7.b(obj);
                a2 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            o oVar = (o) a2;
            V v = oVar.f2599a;
            if (v != null) {
                lw1 a3 = this.e.a(v);
                if (a3 != null) {
                    this.g.o(a3);
                } else {
                    qy1 qy1 = this.g;
                    ax1 ax1 = oVar.b;
                    if (ax1 == null) {
                        ax1 = new ax1(bx1.NETWORK_UNAVAILABLE, null);
                    }
                    qy1.n(ax1);
                }
            } else {
                qy1 qy12 = this.g;
                ax1 ax12 = oVar.b;
                if (ax12 == null) {
                    ax12 = new ax1(bx1.NETWORK_UNAVAILABLE, null);
                }
                qy12.n(ax12);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public kw1(ry1 ry1, String str, String str2) {
        this.d = ry1;
        this.e = str;
        this.b = str2;
    }

    @DexIgnore
    public /* synthetic */ kw1(ry1 ry1, String str, String str2, int i) {
        String str3;
        if ((i & 2) != 0) {
            String a2 = e.a("UUID.randomUUID().toString()");
            if (a2 != null) {
                str3 = a2.substring(0, 16);
                pq7.b(str3, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            } else {
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
        } else {
            str3 = str;
        }
        str2 = (i & 4) != 0 ? str3 : str2;
        this.d = ry1;
        this.e = str3;
        this.b = str2;
    }

    @DexIgnore
    public abstract lw1 a(hc0 hc0);

    @DexIgnore
    public final ry1 b() {
        return this.d;
    }

    @DexIgnore
    public final qy1<lw1> c(ry1 ry1) {
        qy1<lw1> qy1 = new qy1<>();
        xw7 unused = gu7.d(id0.i.e(), null, null, new a(this, ry1, qy1, null), 3, null);
        return qy1;
    }

    @DexIgnore
    public final String d() {
        return this.e;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public final mw1 f() {
        return this.c;
    }

    @DexIgnore
    public abstract ec0 g();
}
