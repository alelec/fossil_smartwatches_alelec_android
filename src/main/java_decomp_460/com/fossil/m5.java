package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class m5 extends u5 {
    @DexIgnore
    public boolean k;
    @DexIgnore
    public /* final */ n6 l;

    @DexIgnore
    public m5(v5 v5Var, n6 n6Var, n4 n4Var) {
        super(v5Var, n4Var);
        this.l = n6Var;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public JSONObject b(boolean z) {
        return g80.k(super.b(z), jd0.R0, this.l.b);
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void f(h7 h7Var) {
        super.f(h7Var);
        this.k = false;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public boolean l() {
        return this.e.c == r5.m && this.k;
    }
}
