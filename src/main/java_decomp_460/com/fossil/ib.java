package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ib {
    @DexIgnore
    public /* synthetic */ ib(kq7 kq7) {
    }

    @DexIgnore
    public final String a(short s) {
        String a2;
        if (s == 256) {
            return "legacy_activity_file";
        }
        if (s == 23131) {
            return "legacy_ota";
        }
        switch (s) {
            case 1792:
                return "asset_background_file";
            case 1793:
                return "asset_notification_icon_file";
            case 1794:
                return "asset_localization_file";
            case 1795:
                return "asset_notification_reply_message_icon_file";
            case 1796:
                return "asset_route_image_file";
            case 1797:
                return "asset_elabel_file";
            default:
                ob b = ob.A.b(s);
                return (b == null || (a2 = ey1.a(b)) == null) ? "unknown" : a2;
        }
    }
}
