package com.fossil;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.model.PlaceFields;
import com.fossil.au5;
import com.fossil.du5;
import com.fossil.iq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.network.utils.NetworkUtils;
import com.portfolio.platform.PortfolioApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gv6 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public MutableLiveData<a> f1380a; // = new MutableLiveData<>();
    @DexIgnore
    public Location b;
    @DexIgnore
    public /* final */ Geocoder c; // = new Geocoder(this.d);
    @DexIgnore
    public /* final */ PortfolioApp d;
    @DexIgnore
    public /* final */ du5 e;
    @DexIgnore
    public /* final */ au5 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Integer f1381a;
        @DexIgnore
        public Double b;
        @DexIgnore
        public Double c;
        @DexIgnore
        public String d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public Boolean f;
        @DexIgnore
        public Boolean g;

        @DexIgnore
        public a(Integer num, Double d2, Double d3, String str, Boolean bool, Boolean bool2, Boolean bool3) {
            this.f1381a = num;
            this.b = d2;
            this.c = d3;
            this.d = str;
            this.e = bool;
            this.f = bool2;
            this.g = bool3;
        }

        @DexIgnore
        public final String a() {
            return this.d;
        }

        @DexIgnore
        public final Boolean b() {
            return this.e;
        }

        @DexIgnore
        public final Integer c() {
            return this.f1381a;
        }

        @DexIgnore
        public final Double d() {
            return this.b;
        }

        @DexIgnore
        public final Double e() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!pq7.a(this.f1381a, aVar.f1381a) || !pq7.a(this.b, aVar.b) || !pq7.a(this.c, aVar.c) || !pq7.a(this.d, aVar.d) || !pq7.a(this.e, aVar.e) || !pq7.a(this.f, aVar.f) || !pq7.a(this.g, aVar.g)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final Boolean f() {
            return this.g;
        }

        @DexIgnore
        public final Boolean g() {
            return this.f;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            Integer num = this.f1381a;
            int hashCode = num != null ? num.hashCode() : 0;
            Double d2 = this.b;
            int hashCode2 = d2 != null ? d2.hashCode() : 0;
            Double d3 = this.c;
            int hashCode3 = d3 != null ? d3.hashCode() : 0;
            String str = this.d;
            int hashCode4 = str != null ? str.hashCode() : 0;
            Boolean bool = this.e;
            int hashCode5 = bool != null ? bool.hashCode() : 0;
            Boolean bool2 = this.f;
            int hashCode6 = bool2 != null ? bool2.hashCode() : 0;
            Boolean bool3 = this.g;
            if (bool3 != null) {
                i = bool3.hashCode();
            }
            return (((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "UiDataWrapper(errorNetwork=" + this.f1381a + ", latitude=" + this.b + ", longitude=" + this.c + ", address=" + this.d + ", enableBtnConfirm=" + this.e + ", isShowDialogLoading=" + this.f + ", isPermissionGranted=" + this.g + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.e<au5.c, au5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gv6 f1382a;
        @DexIgnore
        public /* final */ /* synthetic */ double b;
        @DexIgnore
        public /* final */ /* synthetic */ double c;

        @DexIgnore
        public b(gv6 gv6, double d, double d2) {
            this.f1382a = gv6;
            this.b = d;
            this.c = d2;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(au5.b bVar) {
            pq7.c(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetCityName onError");
            gv6.f(this.f1382a, null, null, null, um5.c(PortfolioApp.h0.c(), 2131886553), null, Boolean.FALSE, null, 87, null);
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(au5.c cVar) {
            pq7.c(cVar, "responseValue");
            String a2 = cVar.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MapPickerViewModel", "GetCityName onSuccess - address: " + a2 + " lat " + this.b + " long " + this.c);
            this.f1382a.b = new Location("gps");
            Location location = this.f1382a.b;
            if (location != null) {
                location.setLatitude(this.b);
                Location location2 = this.f1382a.b;
                if (location2 != null) {
                    location2.setLongitude(this.c);
                    gv6.f(this.f1382a, null, null, null, a2, Boolean.TRUE, Boolean.FALSE, null, 71, null);
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.e<du5.c, du5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gv6 f1383a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(gv6 gv6) {
            this.f1383a = gv6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(du5.b bVar) {
            pq7.c(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetLocation onError");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(du5.c cVar) {
            pq7.c(cVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetLocation onSuccess");
            this.f1383a.k(cVar.a(), cVar.b());
        }
    }

    @DexIgnore
    public gv6(PortfolioApp portfolioApp, du5 du5, au5 au5) {
        pq7.c(portfolioApp, "mApp");
        pq7.c(du5, "mGetLocation");
        pq7.c(au5, "mGetAddress");
        this.d = portfolioApp;
        this.e = du5;
        this.f = au5;
    }

    @DexIgnore
    public static /* synthetic */ void f(gv6 gv6, Integer num, Double d2, Double d3, String str, Boolean bool, Boolean bool2, Boolean bool3, int i, Object obj) {
        gv6.e((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : d2, (i & 4) != 0 ? null : d3, (i & 8) != 0 ? null : str, (i & 16) != 0 ? null : bool, (i & 32) != 0 ? null : bool2, (i & 64) != 0 ? null : bool3);
    }

    @DexIgnore
    public final boolean d() {
        if (NetworkUtils.isNetworkAvailable(this.d)) {
            return true;
        }
        FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "No internet connection");
        f(this, 601, null, null, null, null, null, null, 126, null);
        return false;
    }

    @DexIgnore
    public final void e(Integer num, Double d2, Double d3, String str, Boolean bool, Boolean bool2, Boolean bool3) {
        this.f1380a.l(new a(num, d2, d3, str, bool, bool2, bool3));
    }

    @DexIgnore
    public final void g(double d2, double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerViewModel", "GetCityName at lat: " + d2 + ", lng: " + d3);
        this.f.e(new au5.a(d2, d3), new b(this, d2, d3));
    }

    @DexIgnore
    public final void h(String str) {
        pq7.c(str, "address");
        try {
            List<Address> fromLocationName = this.c.getFromLocationName(str, 5);
            pq7.b(fromLocationName, PlaceFields.LOCATION);
            if (!fromLocationName.isEmpty()) {
                Address address = fromLocationName.get(0);
                pq7.b(address, "location[0]");
                double latitude = address.getLatitude();
                Address address2 = fromLocationName.get(0);
                pq7.b(address2, "location[0]");
                f(this, null, Double.valueOf(latitude), Double.valueOf(address2.getLongitude()), str, null, null, null, 113, null);
                return;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MapPickerViewModel", "No lat lng found for address " + str);
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("MapPickerViewModel", "Exception when get lat lng from address " + str + ' ' + e2);
            j();
        }
    }

    @DexIgnore
    public final LiveData<a> i() {
        return this.f1380a;
    }

    @DexIgnore
    public final void j() {
        FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetLocation");
        this.e.e(new du5.a(), new c(this));
    }

    @DexIgnore
    public final void k(double d2, double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerViewModel", "handleLocation latitude=" + d2 + ", longitude=" + d3);
        if (d2 != 0.0d && d3 != 0.0d) {
            f(this, null, Double.valueOf(d2), Double.valueOf(d3), null, null, null, null, 121, null);
            g(d2, d3);
        }
    }

    @DexIgnore
    public final void l(double d2, double d3, String str) {
        pq7.c(str, "address");
        if (d2 != 0.0d && d3 != 0.0d && !TextUtils.isEmpty(str)) {
            f(this, null, Double.valueOf(d2), Double.valueOf(d3), str, null, Boolean.FALSE, null, 81, null);
        } else if (!TextUtils.isEmpty(str)) {
            h(str);
        } else {
            j();
        }
    }

    @DexIgnore
    public final Location m() {
        return this.b;
    }
}
