package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class es3 extends vg2<cs3> {
    @DexIgnore
    public static /* final */ es3 c; // = new es3();

    @DexIgnore
    public es3() {
        super("com.google.android.gms.plus.plusone.PlusOneButtonCreatorImpl");
    }

    @DexIgnore
    public static View c(Context context, int i, int i2, String str, int i3) {
        if (str != null) {
            try {
                return (View) tg2.i(((cs3) c.b(context)).y(tg2.n(context), i, i2, str, i3));
            } catch (Exception e) {
                return new bs3(context, i);
            }
        } else {
            throw new NullPointerException();
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.vg2
    public final /* synthetic */ cs3 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.plus.internal.IPlusOneButtonCreator");
        return queryLocalInterface instanceof cs3 ? (cs3) queryLocalInterface : new ds3(iBinder);
    }
}
