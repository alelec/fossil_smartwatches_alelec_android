package com.fossil;

import com.fossil.fitness.WorkoutState;
import com.fossil.fitness.WorkoutType;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tu extends mu {
    @DexIgnore
    public /* final */ boolean L;
    @DexIgnore
    public lp1 M; // = new lp1(0, WorkoutType.UNKNOWN, WorkoutState.END, 0, 0, 0, 0, 0, 0, 0, 0, false);

    @DexIgnore
    public tu(k5 k5Var) {
        super(fu.p, hs.D, k5Var, 0, 8);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(super.A(), jd0.f0, this.M.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.ps
    public JSONObject F(byte[] bArr) {
        WorkoutState workoutState;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 29) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            WorkoutType d = g80.d(hy1.p(order.get(4)));
            short p = hy1.p(order.get(5));
            WorkoutState[] values = WorkoutState.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    workoutState = null;
                    break;
                }
                workoutState = values[i];
                if (workoutState.getValue() == p) {
                    break;
                }
                i++;
            }
            if (workoutState == null) {
                this.v = mw.a(this.v, null, null, lw.l, null, null, 27);
            } else {
                lp1 lp1 = new lp1(hy1.o(order.getInt(0)), d, workoutState, hy1.o(order.getInt(6)), hy1.o(order.getInt(10)), hy1.o(order.getInt(14)), hy1.o(order.getInt(18)), hy1.o(order.getInt(22)), hy1.p(order.get(26)), hy1.p(order.get(27)), hy1.p(order.get(28)), bArr.length != 29 && ((hy1.p(order.get(29)) >> 0) & 1) == 1);
                this.M = lp1;
                g80.k(jSONObject, jd0.f0, lp1.toJSONObject());
                this.v = mw.a(this.v, null, null, lw.b, null, null, 27);
            }
        } else {
            this.v = mw.a(this.v, null, null, lw.k, null, null, 27);
        }
        this.E = true;
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public boolean O() {
        return this.L;
    }

    @DexIgnore
    public final lp1 Q() {
        return this.M;
    }
}
