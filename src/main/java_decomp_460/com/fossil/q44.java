package com.fossil;

import com.fossil.c44;
import com.fossil.d44;
import com.fossil.h34;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q44<E> extends g34<E> {
    @DexIgnore
    public static /* final */ q44<Object> EMPTY; // = new q44<>(y24.of());
    @DexIgnore
    public /* final */ transient d44.e<E>[] d;
    @DexIgnore
    public /* final */ transient d44.e<E>[] e;
    @DexIgnore
    public /* final */ transient int f;
    @DexIgnore
    public /* final */ transient int g;
    @DexIgnore
    @LazyInit
    public transient h34<E> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends h34.b<E> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean contains(Object obj) {
            return q44.this.contains(obj);
        }

        @DexIgnore
        @Override // com.fossil.h34.b
        public E get(int i) {
            return (E) q44.this.d[i].getElement();
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return q44.this.d.length;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<E> extends d44.e<E> {
        @DexIgnore
        public /* final */ d44.e<E> nextInBucket;

        @DexIgnore
        public c(E e, int i, d44.e<E> eVar) {
            super(e, i);
            this.nextInBucket = eVar;
        }

        @DexIgnore
        @Override // com.fossil.d44.e
        public d44.e<E> nextInBucket() {
            return this.nextInBucket;
        }
    }

    @DexIgnore
    public q44(Collection<? extends c44.a<? extends E>> collection) {
        d44.e<E> cVar;
        int size = collection.size();
        d44.e<E>[] eVarArr = new d44.e[size];
        if (size == 0) {
            this.d = eVarArr;
            this.e = null;
            this.f = 0;
            this.g = 0;
            this.h = h34.of();
            return;
        }
        int a2 = r24.a(size, 1.0d);
        d44.e<E>[] eVarArr2 = new d44.e[a2];
        long j = 0;
        Iterator<? extends c44.a<? extends E>> it = collection.iterator();
        int i = 0;
        int i2 = 0;
        while (it.hasNext()) {
            c44.a aVar = (c44.a) it.next();
            Object element = aVar.getElement();
            i14.l(element);
            int count = aVar.getCount();
            int hashCode = element.hashCode();
            int b2 = (a2 - 1) & r24.b(hashCode);
            d44.e<E> eVar = eVarArr2[b2];
            if (eVar == null) {
                cVar = (aVar instanceof d44.e) && !(aVar instanceof c) ? (d44.e) aVar : new d44.e<>(element, count);
            } else {
                cVar = new c<>(element, count, eVar);
            }
            i2 += hashCode ^ count;
            eVarArr[i] = cVar;
            eVarArr2[b2] = cVar;
            j += (long) count;
            i++;
        }
        this.d = eVarArr;
        this.e = eVarArr2;
        this.f = v54.b(j);
        this.g = i2;
    }

    @DexIgnore
    @Override // com.fossil.c44, com.fossil.g34
    public int count(Object obj) {
        d44.e<E>[] eVarArr = this.e;
        if (!(obj == null || eVarArr == null)) {
            for (d44.e<E> eVar = eVarArr[r24.c(obj) & (eVarArr.length - 1)]; eVar != null; eVar = eVar.nextInBucket()) {
                if (f14.a(obj, eVar.getElement())) {
                    return eVar.getCount();
                }
            }
        }
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.c44, com.fossil.g34
    public h34<E> elementSet() {
        h34<E> h34 = this.h;
        if (h34 != null) {
            return h34;
        }
        b bVar = new b();
        this.h = bVar;
        return bVar;
    }

    @DexIgnore
    @Override // com.fossil.g34
    public c44.a<E> getEntry(int i) {
        return this.d[i];
    }

    @DexIgnore
    @Override // com.fossil.g34
    public int hashCode() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.f;
    }
}
