package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface lo0 {
    @DexIgnore
    ColorStateList getSupportBackgroundTintList();

    @DexIgnore
    PorterDuff.Mode getSupportBackgroundTintMode();

    @DexIgnore
    void setSupportBackgroundTintList(ColorStateList colorStateList);

    @DexIgnore
    void setSupportBackgroundTintMode(PorterDuff.Mode mode);
}
