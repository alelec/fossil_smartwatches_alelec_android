package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.f57;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iy4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ f57.b f1695a;
    @DexIgnore
    public /* final */ uy4 b; // = uy4.d.b();
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ oy5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ sf5 f1696a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.iy4$a$a")
        /* renamed from: com.fossil.iy4$a$a  reason: collision with other inner class name */
        public static final class C0125a extends qq7 implements rp7<View, tl7> {
            @DexIgnore
            public /* final */ /* synthetic */ uy4 $colorGenerator$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ f57.b $drawableBuilder$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ xs4 $friend$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ oy5 $listener$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0125a(a aVar, xs4 xs4, f57.b bVar, uy4 uy4, oy5 oy5) {
                super(1);
                this.this$0 = aVar;
                this.$friend$inlined = xs4;
                this.$drawableBuilder$inlined = bVar;
                this.$colorGenerator$inlined = uy4;
                this.$listener$inlined = oy5;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.rp7
            public /* bridge */ /* synthetic */ tl7 invoke(View view) {
                invoke(view);
                return tl7.f3441a;
            }

            @DexIgnore
            public final void invoke(View view) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("SentViewHolder", "accept: " + this.this$0.getAdapterPosition());
                this.$listener$inlined.e4(this.$friend$inlined);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends qq7 implements rp7<View, tl7> {
            @DexIgnore
            public /* final */ /* synthetic */ uy4 $colorGenerator$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ f57.b $drawableBuilder$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ xs4 $friend$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ oy5 $listener$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(a aVar, xs4 xs4, f57.b bVar, uy4 uy4, oy5 oy5) {
                super(1);
                this.this$0 = aVar;
                this.$friend$inlined = xs4;
                this.$drawableBuilder$inlined = bVar;
                this.$colorGenerator$inlined = uy4;
                this.$listener$inlined = oy5;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.rp7
            public /* bridge */ /* synthetic */ tl7 invoke(View view) {
                invoke(view);
                return tl7.f3441a;
            }

            @DexIgnore
            public final void invoke(View view) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("SentViewHolder", "reject: " + this.this$0.getAdapterPosition());
                this.$listener$inlined.R3(this.$friend$inlined);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(sf5 sf5) {
            super(sf5.n());
            pq7.c(sf5, "binding");
            this.f1696a = sf5;
        }

        @DexIgnore
        public final void a(xs4 xs4, oy5 oy5, f57.b bVar, uy4 uy4) {
            pq7.c(xs4, "friend");
            pq7.c(oy5, "listener");
            pq7.c(bVar, "drawableBuilder");
            pq7.c(uy4, "colorGenerator");
            sf5 sf5 = this.f1696a;
            String a2 = hz4.f1561a.a(xs4.b(), xs4.e(), xs4.i());
            hr7 hr7 = hr7.f1520a;
            FlexibleTextView flexibleTextView = sf5.v;
            pq7.b(flexibleTextView, "tvFriendRequested");
            String c = um5.c(flexibleTextView.getContext(), 2131886296);
            pq7.b(c, "LanguageHelper.getString\u2026rnameWantsToBeYourFriend)");
            String format = String.format(c, Arrays.copyOf(new Object[]{a2}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            FlexibleTextView flexibleTextView2 = sf5.v;
            pq7.b(flexibleTextView2, "tvFriendRequested");
            flexibleTextView2.setText(jl5.b(jl5.b, a2, format, 0, 4, null));
            ImageView imageView = sf5.s;
            pq7.b(imageView, "ivAvatar");
            ty4.b(imageView, xs4.h(), a2, bVar, uy4);
            FlexibleButton flexibleButton = sf5.q;
            pq7.b(flexibleButton, "btnAccept");
            fz4.a(flexibleButton, new C0125a(this, xs4, bVar, uy4, oy5));
            FlexibleButton flexibleButton2 = sf5.r;
            pq7.b(flexibleButton2, "btnDecline");
            fz4.a(flexibleButton2, new b(this, xs4, bVar, uy4, oy5));
        }
    }

    @DexIgnore
    public iy4(int i, oy5 oy5) {
        pq7.c(oy5, "listener");
        this.c = i;
        this.d = oy5;
        f57.b f = f57.a().f();
        pq7.b(f, "TextDrawable.builder().round()");
        this.f1695a = f;
    }

    @DexIgnore
    public final int a() {
        return this.c;
    }

    @DexIgnore
    public boolean b(List<? extends Object> list, int i) {
        pq7.c(list, "items");
        Object obj = list.get(i);
        return (obj instanceof xs4) && ((xs4) obj).c() == 2;
    }

    @DexIgnore
    public void c(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        a aVar = null;
        pq7.c(list, "items");
        pq7.c(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof xs4)) {
            obj = null;
        }
        xs4 xs4 = (xs4) obj;
        if (viewHolder instanceof a) {
            aVar = viewHolder;
        }
        a aVar2 = aVar;
        if (xs4 != null && aVar2 != null) {
            aVar2.a(xs4, this.d, this.f1695a, this.b);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder d(ViewGroup viewGroup) {
        pq7.c(viewGroup, "parent");
        sf5 z = sf5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemSentRequestFriendBin\u2026.context), parent, false)");
        return new a(z);
    }
}
