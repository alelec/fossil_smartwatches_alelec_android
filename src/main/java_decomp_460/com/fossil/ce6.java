package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ce6 implements MembersInjector<ActiveTimeOverviewFragment> {
    @DexIgnore
    public static void a(ActiveTimeOverviewFragment activeTimeOverviewFragment, zd6 zd6) {
        activeTimeOverviewFragment.h = zd6;
    }

    @DexIgnore
    public static void b(ActiveTimeOverviewFragment activeTimeOverviewFragment, ke6 ke6) {
        activeTimeOverviewFragment.j = ke6;
    }

    @DexIgnore
    public static void c(ActiveTimeOverviewFragment activeTimeOverviewFragment, qe6 qe6) {
        activeTimeOverviewFragment.i = qe6;
    }
}
