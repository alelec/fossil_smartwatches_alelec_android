package com.fossil;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ay4 extends RecyclerView.g<RecyclerView.ViewHolder> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<Object> f359a; // = new ArrayList();
    @DexIgnore
    public zy4 b; // = new zy4(1);
    @DexIgnore
    public iy4 c;
    @DexIgnore
    public hy4 d;

    /*
    static {
        pq7.b(ay4.class.getSimpleName(), "AllFriendListAdapter::class.java.simpleName");
    }
    */

    @DexIgnore
    public ay4(oy5 oy5) {
        pq7.c(oy5, "listener");
        this.c = new iy4(2, oy5);
        this.d = new hy4(3);
    }

    @DexIgnore
    public final void g() {
        this.f359a.clear();
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f359a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        if (this.b.b(this.f359a, i)) {
            return this.b.a();
        }
        if (this.c.b(this.f359a, i)) {
            return this.c.a();
        }
        if (this.d.b(this.f359a, i)) {
            return this.d.a();
        }
        throw new IllegalArgumentException("No delegate for this position : " + this.f359a.get(i));
    }

    @DexIgnore
    public final Object h(int i) {
        return this.f359a.get(i);
    }

    @DexIgnore
    public final void i(List<? extends Object> list) {
        pq7.c(list, "newData");
        this.f359a.clear();
        this.f359a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        pq7.c(viewHolder, "holder");
        int itemViewType = getItemViewType(i);
        if (itemViewType == 1) {
            this.b.c(this.f359a, i, viewHolder);
        } else if (itemViewType == 2) {
            this.c.c(this.f359a, i, viewHolder);
        } else if (itemViewType == 3) {
            this.d.c(this.f359a, i, viewHolder);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        if (i == this.b.a()) {
            return this.b.d(viewGroup);
        }
        if (i == this.c.a()) {
            return this.c.d(viewGroup);
        }
        if (i == this.d.a()) {
            return this.d.d(viewGroup);
        }
        throw new IllegalArgumentException("No support for this viewType: " + i);
    }
}
