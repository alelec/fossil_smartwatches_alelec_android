package com.fossil;

import android.os.Build;
import android.util.Base64;
import com.fossil.iq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.security.KeyPair;
import java.security.KeyStore;
import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q27 extends iq4<a, c, b> {
    @DexIgnore
    public /* final */ on5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f2914a;
        @DexIgnore
        public /* final */ wh5 b;

        @DexIgnore
        public a(String str, wh5 wh5) {
            pq7.c(str, "aliasName");
            pq7.c(wh5, "scope");
            this.f2914a = str;
            this.b = wh5;
        }

        @DexIgnore
        public final String a() {
            return this.f2914a;
        }

        @DexIgnore
        public final wh5 b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f2915a;

        @DexIgnore
        public b(int i, String str) {
            pq7.c(str, "errorMessage");
            this.f2915a = i;
        }

        @DexIgnore
        public final int a() {
            return this.f2915a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f2916a;

        @DexIgnore
        public c(String str) {
            pq7.c(str, "decryptedValue");
            this.f2916a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f2916a;
        }
    }

    @DexIgnore
    public q27(on5 on5) {
        pq7.c(on5, "mSharedPreferencesManager");
        this.d = on5;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "DecryptValueKeyStoreUseCase";
    }

    @DexIgnore
    /* renamed from: m */
    public Object k(a aVar, qn7<Object> qn7) {
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                on5 on5 = this.d;
                if (aVar != null) {
                    String n = on5.n(aVar.a(), aVar.b());
                    String o = this.d.o(aVar.a(), aVar.b());
                    KeyStore.SecretKeyEntry f = z37.b.f(aVar.a());
                    Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                    instance.init(2, f.getSecretKey(), new GCMParameterSpec(128, Base64.decode(n, 0)));
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("DecryptValueKeyStoreUseCase", "Start decrypt with iv " + n + " encryptedValue " + o);
                    byte[] doFinal = instance.doFinal(Base64.decode(o, 0));
                    pq7.b(doFinal, "decryptedByteArray");
                    String str = new String(doFinal, et7.f986a);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("DecryptValueKeyStoreUseCase", "Decrypted success " + str);
                    j(new c(str));
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                on5 on52 = this.d;
                if (aVar != null) {
                    String o2 = on52.o(aVar.a(), aVar.b());
                    KeyPair d2 = z37.b.d(aVar.a());
                    if (d2 == null) {
                        d2 = z37.b.b(aVar.a());
                    }
                    Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                    instance2.init(2, d2.getPrivate());
                    byte[] doFinal2 = instance2.doFinal(Base64.decode(o2, 0));
                    pq7.b(doFinal2, "decryptedByte");
                    String str2 = new String(doFinal2, et7.f986a);
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("DecryptValueKeyStoreUseCase", "Decrypted success " + str2);
                    j(new c(str2));
                } else {
                    pq7.i();
                    throw null;
                }
            }
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.e("DecryptValueKeyStoreUseCase", "Exception when decrypt value " + e);
            i(new b(600, ""));
        }
        return new Object();
    }
}
