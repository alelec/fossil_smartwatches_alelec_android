package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ow5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sc6 extends pv5 implements rc6 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public g37<r85> g;
    @DexIgnore
    public ow5 h;
    @DexIgnore
    public qc6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return sc6.k;
        }

        @DexIgnore
        public final sc6 b() {
            return new sc6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sc6 b;

        @DexIgnore
        public b(sc6 sc6) {
            this.b = sc6;
        }

        @DexIgnore
        public final void onClick(View view) {
            r85 a2 = this.b.M6().a();
            if (a2 != null) {
                a2.t.setText("");
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ sc6 b;

        @DexIgnore
        public c(sc6 sc6) {
            this.b = sc6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView;
            FlexibleTextView flexibleTextView;
            ImageView imageView2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = sc6.l.a();
            local.d(a2, "onTextChanged " + charSequence);
            if (TextUtils.isEmpty(charSequence)) {
                r85 a3 = this.b.M6().a();
                if (!(a3 == null || (imageView2 = a3.r) == null)) {
                    imageView2.setVisibility(8);
                }
                this.b.z("");
                this.b.N6().n();
                return;
            }
            r85 a4 = this.b.M6().a();
            if (!(a4 == null || (flexibleTextView = a4.w) == null)) {
                flexibleTextView.setVisibility(8);
            }
            r85 a5 = this.b.M6().a();
            if (!(a5 == null || (imageView = a5.r) == null)) {
                imageView.setVisibility(0);
            }
            this.b.z(String.valueOf(charSequence));
            this.b.N6().p(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sc6 b;

        @DexIgnore
        public d(sc6 sc6) {
            this.b = sc6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ow5.c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sc6 f3239a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(sc6 sc6) {
            this.f3239a = sc6;
        }

        @DexIgnore
        @Override // com.fossil.ow5.c
        public void a(String str) {
            pq7.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
            r85 a2 = this.f3239a.M6().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                pq7.b(flexibleTextView, "it.tvNotFound");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = a2.w;
                pq7.b(flexibleTextView2, "it.tvNotFound");
                hr7 hr7 = hr7.f1520a;
                String c = um5.c(PortfolioApp.h0.c(), 2131886813);
                pq7.b(c, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                String format = String.format(c, Arrays.copyOf(new Object[]{str}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView2.setText(format);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ow5.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sc6 f3240a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(sc6 sc6) {
            this.f3240a = sc6;
        }

        @DexIgnore
        @Override // com.fossil.ow5.d
        public void a(MicroApp microApp) {
            pq7.c(microApp, "item");
            this.f3240a.N6().o(microApp);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Transition.TransitionListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ r85 f3241a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        public g(r85 r85, long j) {
            this.f3241a = r85;
            this.b = j;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FlexibleButton flexibleButton = this.f3241a.q;
            pq7.b(flexibleButton, "binding.btnCancel");
            if (flexibleButton.getAlpha() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                this.f3241a.q.animate().setDuration(this.b).alpha(1.0f);
            } else {
                this.f3241a.q.animate().setDuration(this.b).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        }
    }

    /*
    static {
        String simpleName = sc6.class.getSimpleName();
        pq7.b(simpleName, "SearchMicroAppFragment::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.rc6
    public void B(List<cl7<MicroApp, String>> list) {
        pq7.c(list, "results");
        ow5 ow5 = this.h;
        if (ow5 != null) {
            ow5.m(list);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.rc6
    public void D() {
        ow5 ow5 = this.h;
        if (ow5 != null) {
            ow5.m(null);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return k;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return true;
        }
        activity.supportFinishAfterTransition();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.rc6
    public void K(List<cl7<MicroApp, String>> list) {
        pq7.c(list, "recentSearchResult");
        ow5 ow5 = this.h;
        if (ow5 != null) {
            ow5.l(list);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void K6() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            xk5 xk5 = xk5.f4136a;
            g37<r85> g37 = this.g;
            if (g37 != null) {
                r85 a2 = g37.a();
                FlexibleButton flexibleButton = a2 != null ? a2.q : null;
                if (flexibleButton != null) {
                    pq7.b(activity, "it");
                    xk5.a(flexibleButton, activity);
                    activity.setResult(0);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.view.View");
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final g37<r85> M6() {
        g37<r85> g37 = this.g;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final qc6 N6() {
        qc6 qc6 = this.i;
        if (qc6 != null) {
            return qc6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void O6(FragmentActivity fragmentActivity, long j2) {
        TransitionSet a2 = is5.f1661a.a(j2);
        Window window = fragmentActivity.getWindow();
        pq7.b(window, "context.window");
        window.setEnterTransition(a2);
        g37<r85> g37 = this.g;
        if (g37 != null) {
            r85 a3 = g37.a();
            if (a3 != null) {
                pq7.b(a3, "binding");
                P6(a2, j2, a3);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final TransitionSet P6(TransitionSet transitionSet, long j2, r85 r85) {
        FlexibleButton flexibleButton = r85.q;
        pq7.b(flexibleButton, "binding.btnCancel");
        flexibleButton.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        return transitionSet.addListener((Transition.TransitionListener) new g(r85, j2));
    }

    @DexIgnore
    /* renamed from: Q6 */
    public void M5(qc6 qc6) {
        pq7.c(qc6, "presenter");
        this.i = qc6;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        g37<r85> g37 = new g37<>(this, (r85) aq0.f(layoutInflater, 2131558586, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            r85 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        qc6 qc6 = this.i;
        if (qc6 != null) {
            qc6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        qc6 qc6 = this.i;
        if (qc6 != null) {
            qc6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            pq7.b(activity, "it");
            O6(activity, 550);
        }
        this.h = new ow5();
        g37<r85> g37 = this.g;
        if (g37 != null) {
            r85 a2 = g37.a();
            if (a2 != null) {
                r85 r85 = a2;
                RecyclerViewEmptySupport recyclerViewEmptySupport = r85.v;
                pq7.b(recyclerViewEmptySupport, "this.rvResults");
                ow5 ow5 = this.h;
                if (ow5 != null) {
                    recyclerViewEmptySupport.setAdapter(ow5);
                    RecyclerViewEmptySupport recyclerViewEmptySupport2 = r85.v;
                    pq7.b(recyclerViewEmptySupport2, "this.rvResults");
                    recyclerViewEmptySupport2.setLayoutManager(new LinearLayoutManager(getContext()));
                    RecyclerViewEmptySupport recyclerViewEmptySupport3 = r85.v;
                    FlexibleTextView flexibleTextView = r85.w;
                    pq7.b(flexibleTextView, "tvNotFound");
                    recyclerViewEmptySupport3.setEmptyView(flexibleTextView);
                    ImageView imageView = r85.r;
                    pq7.b(imageView, "this.btnSearchClear");
                    imageView.setVisibility(8);
                    r85.r.setOnClickListener(new b(this));
                    r85.t.addTextChangedListener(new c(this));
                    r85.q.setOnClickListener(new d(this));
                    ow5 ow52 = this.h;
                    if (ow52 != null) {
                        ow52.o(new e(this));
                        ow5 ow53 = this.h;
                        if (ow53 != null) {
                            ow53.p(new f(this));
                        } else {
                            pq7.n("mAdapter");
                            throw null;
                        }
                    } else {
                        pq7.n("mAdapter");
                        throw null;
                    }
                } else {
                    pq7.n("mAdapter");
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.rc6
    public void v1(MicroApp microApp) {
        pq7.c(microApp, "selectedMicroApp");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            xk5 xk5 = xk5.f4136a;
            g37<r85> g37 = this.g;
            if (g37 != null) {
                r85 a2 = g37.a();
                FlexibleButton flexibleButton = a2 != null ? a2.q : null;
                if (flexibleButton != null) {
                    pq7.b(activity, "it");
                    xk5.a(flexibleButton, activity);
                    activity.setResult(-1, new Intent().putExtra("SEARCH_MICRO_APP_RESULT_ID", microApp.getId()));
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.view.View");
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.rc6
    public void z(String str) {
        pq7.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        ow5 ow5 = this.h;
        if (ow5 != null) {
            ow5.n(str);
        } else {
            pq7.n("mAdapter");
            throw null;
        }
    }
}
