package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qo extends qq7 implements vp7<lp, Float, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ rq b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qo(rq rqVar) {
        super(2);
        this.b = rqVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public tl7 invoke(lp lpVar, Float f) {
        float floatValue = f.floatValue();
        if (floatValue == 1.0f) {
            this.b.d(floatValue);
        } else {
            rq rqVar = this.b;
            rqVar.d((floatValue * this.b.J) + rq.G(rqVar));
        }
        return tl7.f3441a;
    }
}
