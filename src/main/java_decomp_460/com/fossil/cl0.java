package com.fossil;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.ud0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cl0 extends Service {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ud0.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.ud0
        public void A1(String str, int i, String str2) throws RemoteException {
            cl0.this.c(Binder.getCallingUid(), str);
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                cl0.this.a(str, i, str2);
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }

        @DexIgnore
        @Override // com.fossil.ud0
        public void I2(String str, int i, String str2, Notification notification) throws RemoteException {
            cl0.this.c(Binder.getCallingUid(), str);
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                cl0.this.d(str, i, str2, notification);
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }

        @DexIgnore
        @Override // com.fossil.ud0
        public void h0(String str) {
            cl0.this.c(Binder.getCallingUid(), str);
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                cl0.this.b(str);
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
    }

    @DexIgnore
    public abstract void a(String str, int i, String str2);

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public void c(int i, String str) {
        for (String str2 : getPackageManager().getPackagesForUid(i)) {
            if (str2.equals(str)) {
                return;
            }
        }
        throw new SecurityException("NotificationSideChannelService: Uid " + i + " is not authorized for package " + str);
    }

    @DexIgnore
    public abstract void d(String str, int i, String str2, Notification notification);

    @DexIgnore
    public IBinder onBind(Intent intent) {
        if (!intent.getAction().equals("android.support.BIND_NOTIFICATION_SIDE_CHANNEL") || Build.VERSION.SDK_INT > 19) {
            return null;
        }
        return new a();
    }
}
