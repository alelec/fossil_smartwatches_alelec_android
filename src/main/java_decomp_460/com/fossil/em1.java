package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class em1 extends hv1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ HashSet<dm1> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<em1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public em1 a(Parcel parcel) {
            return new em1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public em1 createFromParcel(Parcel parcel) {
            return new em1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public em1[] newArray(int i) {
            return new em1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ em1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.b = new HashSet<>();
        ArrayList createTypedArrayList = parcel.createTypedArrayList(dm1.CREATOR);
        if (createTypedArrayList != null) {
            this.b.addAll(createTypedArrayList);
        }
    }

    @DexIgnore
    public em1(dm1 dm1, dm1 dm12, dm1 dm13, dm1 dm14) {
        this.b = new HashSet<>();
        dm1.a(new dt1(0, 62));
        dm12.a(new dt1(90, 62));
        dm13.a(new dt1(180, 62));
        dm14.a(new dt1(270, 62));
        this.b.add(dm1);
        this.b.add(dm12);
        this.b.add(dm13);
        this.b.add(dm14);
    }

    @DexIgnore
    public em1(dm1[] dm1Arr) {
        HashSet<dm1> hashSet = new HashSet<>();
        this.b = hashSet;
        mm7.t(hashSet, dm1Arr);
    }

    @DexIgnore
    @Override // com.fossil.hv1
    public JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        Iterator<T> it = this.b.iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next().toJSONObject());
        }
        JSONObject put = new JSONObject().put("watchFace._.config.comps", jSONArray);
        pq7.b(put, "JSONObject().put(UIScrip\u2026ationAssignmentJsonArray)");
        return put;
    }

    @DexIgnore
    public final HashSet<dm1> c() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(em1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.b, ((em1) obj).b) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.complication.ComplicationConfig");
    }

    @DexIgnore
    public final dm1 getBottomFace() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 180 && t2.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final dm1 getLeftFace() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 270 && t2.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final dm1 getRightFace() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 90 && t2.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final dm1 getTopFace() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 0 && t2.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return a();
    }

    @DexIgnore
    @Override // com.fossil.hv1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedList(pm7.h0(this.b));
        }
    }
}
