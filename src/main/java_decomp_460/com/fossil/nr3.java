package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nr3 implements Parcelable.Creator<or3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ or3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        long j = -2147483648L;
        boolean z = true;
        boolean z2 = false;
        int i = 0;
        boolean z3 = true;
        boolean z4 = true;
        boolean z5 = false;
        String str = null;
        ArrayList<String> arrayList = null;
        Boolean bool = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        long j5 = 0;
        long j6 = 0;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 2:
                    str8 = ad2.f(parcel, t);
                    break;
                case 3:
                    str7 = ad2.f(parcel, t);
                    break;
                case 4:
                    str6 = ad2.f(parcel, t);
                    break;
                case 5:
                    str5 = ad2.f(parcel, t);
                    break;
                case 6:
                    j6 = ad2.y(parcel, t);
                    break;
                case 7:
                    j5 = ad2.y(parcel, t);
                    break;
                case 8:
                    str4 = ad2.f(parcel, t);
                    break;
                case 9:
                    z = ad2.m(parcel, t);
                    break;
                case 10:
                    z2 = ad2.m(parcel, t);
                    break;
                case 11:
                    j = ad2.y(parcel, t);
                    break;
                case 12:
                    str3 = ad2.f(parcel, t);
                    break;
                case 13:
                    j4 = ad2.y(parcel, t);
                    break;
                case 14:
                    j3 = ad2.y(parcel, t);
                    break;
                case 15:
                    i = ad2.v(parcel, t);
                    break;
                case 16:
                    z3 = ad2.m(parcel, t);
                    break;
                case 17:
                    z4 = ad2.m(parcel, t);
                    break;
                case 18:
                    z5 = ad2.m(parcel, t);
                    break;
                case 19:
                    str2 = ad2.f(parcel, t);
                    break;
                case 20:
                default:
                    ad2.B(parcel, t);
                    break;
                case 21:
                    bool = ad2.n(parcel, t);
                    break;
                case 22:
                    j2 = ad2.y(parcel, t);
                    break;
                case 23:
                    arrayList = ad2.h(parcel, t);
                    break;
                case 24:
                    str = ad2.f(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new or3(str8, str7, str6, str5, j6, j5, str4, z, z2, j, str3, j4, j3, i, z3, z4, z5, str2, bool, j2, arrayList, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ or3[] newArray(int i) {
        return new or3[i];
    }
}
