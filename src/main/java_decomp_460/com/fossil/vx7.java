package com.fossil;

import com.fossil.tn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface vx7<S> extends tn7.b {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <S, R> R a(vx7<S> vx7, R r, vp7<? super R, ? super tn7.b, ? extends R> vp7) {
            return (R) tn7.b.a.a(vx7, r, vp7);
        }

        @DexIgnore
        public static <S, E extends tn7.b> E b(vx7<S> vx7, tn7.c<E> cVar) {
            return (E) tn7.b.a.b(vx7, cVar);
        }

        @DexIgnore
        public static <S> tn7 c(vx7<S> vx7, tn7.c<?> cVar) {
            return tn7.b.a.c(vx7, cVar);
        }

        @DexIgnore
        public static <S> tn7 d(vx7<S> vx7, tn7 tn7) {
            return tn7.b.a.d(vx7, tn7);
        }
    }

    @DexIgnore
    void B(tn7 tn7, S s);

    @DexIgnore
    S F(tn7 tn7);
}
