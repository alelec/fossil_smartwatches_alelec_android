package com.fossil;

import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cy1 {
    @DexIgnore
    public static final byte[] a(Bitmap bitmap, Bitmap.CompressFormat compressFormat) {
        pq7.c(bitmap, "$this$toByteArray");
        pq7.c(compressFormat, "compressFormat");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(compressFormat, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        pq7.b(byteArray, "stream.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public static /* synthetic */ byte[] b(Bitmap bitmap, Bitmap.CompressFormat compressFormat, int i, Object obj) {
        if ((i & 1) != 0) {
            compressFormat = Bitmap.CompressFormat.PNG;
        }
        return a(bitmap, compressFormat);
    }
}
