package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gn extends qq7 implements rp7<fs, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ rq b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gn(rq rqVar) {
        super(1);
        this.b = rqVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(fs fsVar) {
        rq rqVar = this.b;
        long j = rqVar.F;
        long j2 = rqVar.G;
        long j3 = j + j2;
        rqVar.H = j3;
        if (j3 != 0) {
            float f = (float) j;
            float f2 = (float) j3;
            rqVar.I = f / f2;
            rqVar.J = ((float) j2) / f2;
        }
        this.b.J();
        return tl7.f3441a;
    }
}
