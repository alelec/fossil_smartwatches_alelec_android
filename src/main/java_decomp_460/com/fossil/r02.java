package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r02 extends w02 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3057a;
    @DexIgnore
    public /* final */ t32 b;
    @DexIgnore
    public /* final */ t32 c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public r02(Context context, t32 t32, t32 t322, String str) {
        if (context != null) {
            this.f3057a = context;
            if (t32 != null) {
                this.b = t32;
                if (t322 != null) {
                    this.c = t322;
                    if (str != null) {
                        this.d = str;
                        return;
                    }
                    throw new NullPointerException("Null backendName");
                }
                throw new NullPointerException("Null monotonicClock");
            }
            throw new NullPointerException("Null wallClock");
        }
        throw new NullPointerException("Null applicationContext");
    }

    @DexIgnore
    @Override // com.fossil.w02
    public Context b() {
        return this.f3057a;
    }

    @DexIgnore
    @Override // com.fossil.w02
    public String c() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.w02
    public t32 d() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.w02
    public t32 e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof w02)) {
            return false;
        }
        w02 w02 = (w02) obj;
        return this.f3057a.equals(w02.b()) && this.b.equals(w02.e()) && this.c.equals(w02.d()) && this.d.equals(w02.c());
    }

    @DexIgnore
    public int hashCode() {
        return ((((((this.f3057a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "CreationContext{applicationContext=" + this.f3057a + ", wallClock=" + this.b + ", monotonicClock=" + this.c + ", backendName=" + this.d + "}";
    }
}
