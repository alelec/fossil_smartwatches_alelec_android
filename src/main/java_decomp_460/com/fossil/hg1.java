package com.fossil;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hg1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ rd1 f1476a; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends sd1 {
        @DexIgnore
        @Override // com.fossil.rd1, com.fossil.sd1
        public void b(Bitmap bitmap) {
        }
    }

    @DexIgnore
    public static id1<Bitmap> a(rd1 rd1, Drawable drawable, int i, int i2) {
        Bitmap bitmap;
        boolean z;
        Drawable current = drawable.getCurrent();
        if (current instanceof BitmapDrawable) {
            bitmap = ((BitmapDrawable) current).getBitmap();
            z = false;
        } else if (!(current instanceof Animatable)) {
            Bitmap b = b(rd1, current, i, i2);
            z = true;
            bitmap = b;
        } else {
            bitmap = null;
            z = false;
        }
        if (!z) {
            rd1 = f1476a;
        }
        return yf1.f(bitmap, rd1);
    }

    @DexIgnore
    public static Bitmap b(rd1 rd1, Drawable drawable, int i, int i2) {
        Bitmap bitmap = null;
        if (i != Integer.MIN_VALUE || drawable.getIntrinsicWidth() > 0) {
            if (i2 != Integer.MIN_VALUE || drawable.getIntrinsicHeight() > 0) {
                if (drawable.getIntrinsicWidth() > 0) {
                    i = drawable.getIntrinsicWidth();
                }
                if (drawable.getIntrinsicHeight() > 0) {
                    i2 = drawable.getIntrinsicHeight();
                }
                Lock f = tg1.f();
                f.lock();
                bitmap = rd1.c(i, i2, Bitmap.Config.ARGB_8888);
                try {
                    Canvas canvas = new Canvas(bitmap);
                    drawable.setBounds(0, 0, i, i2);
                    drawable.draw(canvas);
                    canvas.setBitmap(null);
                } finally {
                    f.unlock();
                }
            } else if (Log.isLoggable("DrawableToBitmap", 5)) {
                Log.w("DrawableToBitmap", "Unable to draw " + drawable + " to Bitmap with Target.SIZE_ORIGINAL because the Drawable has no intrinsic height");
            }
        } else if (Log.isLoggable("DrawableToBitmap", 5)) {
            Log.w("DrawableToBitmap", "Unable to draw " + drawable + " to Bitmap with Target.SIZE_ORIGINAL because the Drawable has no intrinsic width");
        }
        return bitmap;
    }
}
