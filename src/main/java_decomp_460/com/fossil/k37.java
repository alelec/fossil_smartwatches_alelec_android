package com.fossil;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k37 {
    @DexIgnore
    public static final void a(Canvas canvas, float f, float f2, float f3, float f4, float f5, float f6, boolean z, boolean z2, boolean z3, boolean z4, Paint paint) {
        pq7.c(canvas, "$this$drawRoundedPath");
        pq7.c(paint, "paint");
        Path path = new Path();
        float f7 = (float) 0;
        float f8 = f5 < f7 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : f5;
        float f9 = f6 < f7 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : f6;
        float f10 = f3 - f;
        float f11 = (float) 2;
        float f12 = f10 / f11;
        if (f8 > f12) {
            f8 = f12;
        }
        if (f9 <= f12) {
            f12 = f9;
        }
        float f13 = f10 - (f11 * f8);
        float f14 = (f4 - f2) - (f11 * f12);
        path.moveTo(f3, f2 + f12);
        if (z2) {
            float f15 = -f12;
            path.rQuadTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f15, -f8, f15);
        } else {
            path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, -f12);
            path.rLineTo(-f8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        path.rLineTo(-f13, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        if (z) {
            float f16 = -f8;
            path.rQuadTo(f16, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f16, f12);
        } else {
            path.rLineTo(-f8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f12);
        }
        path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f14);
        if (z4) {
            path.rQuadTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f12, f8, f12);
        } else {
            path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f12);
            path.rLineTo(f8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        path.rLineTo(f13, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        if (z3) {
            path.rQuadTo(f8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f8, -f12);
        } else {
            path.rLineTo(f8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, -f12);
        }
        path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, -f14);
        path.close();
        canvas.drawPath(path, paint);
    }

    @DexIgnore
    public static final void b(Canvas canvas, RectF rectF, Paint paint, float f) {
        pq7.c(canvas, "$this$drawTopLeftRoundRect");
        pq7.c(rectF, "rect");
        pq7.c(paint, "paint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CanvasUtils", "drawTopLeftRoundRect, rect: " + rectF + ", radius: " + f);
        canvas.drawRoundRect(rectF, f, f, paint);
        canvas.drawRect((rectF.width() / ((float) 2)) + rectF.left, rectF.top, rectF.right, rectF.bottom, paint);
        canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom, paint);
    }

    @DexIgnore
    public static final void c(Canvas canvas, RectF rectF, Paint paint, float f) {
        pq7.c(canvas, "$this$drawTopRightRoundRect");
        pq7.c(rectF, "rect");
        pq7.c(paint, "paint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CanvasUtils", "drawTopRightRoundRect, rect: " + rectF + ", radius: " + f);
        canvas.drawRoundRect(rectF, f, f, paint);
        canvas.drawRect(rectF.left, rectF.top, rectF.right - (rectF.width() / ((float) 2)), rectF.bottom, paint);
        canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom, paint);
    }

    @DexIgnore
    public static final void d(Canvas canvas, RectF rectF, Paint paint, float f) {
        pq7.c(canvas, "$this$drawTopRoundRect");
        pq7.c(rectF, "rect");
        pq7.c(paint, "paint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CanvasUtils", "drawTopRoundRect, rect: " + rectF + ", radius: " + f);
        if (rectF.height() >= f) {
            canvas.drawRoundRect(rectF, f, f, paint);
            canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom, paint);
        }
    }
}
