package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ya0 implements Parcelable.Creator<za0> {
    @DexIgnore
    public /* synthetic */ ya0(kq7 kq7) {
    }

    @DexIgnore
    public za0 a(Parcel parcel) {
        return new za0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public za0 createFromParcel(Parcel parcel) {
        return new za0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public za0[] newArray(int i) {
        return new za0[i];
    }
}
