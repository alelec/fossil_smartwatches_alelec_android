package com.fossil;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.fossil.m62;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g82 implements d92 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ c92 f1275a;
    @DexIgnore
    public boolean b; // = false;

    @DexIgnore
    public g82(c92 c92) {
        this.f1275a = c92;
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final boolean a() {
        if (this.b) {
            return false;
        }
        if (this.f1275a.t.E()) {
            this.b = true;
            for (da2 da2 : this.f1275a.t.x) {
                da2.d();
            }
            return false;
        }
        this.f1275a.u(null);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final void b() {
        if (this.b) {
            this.b = false;
            this.f1275a.p(new i82(this, this));
        }
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final void d(int i) {
        this.f1275a.u(null);
        this.f1275a.u.c(i, this.b);
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final void e(Bundle bundle) {
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final void f() {
    }

    @DexIgnore
    public final void g() {
        if (this.b) {
            this.b = false;
            this.f1275a.t.y.a();
            a();
        }
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final void i(z52 z52, m62<?> m62, boolean z) {
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: T extends com.fossil.i72<? extends com.fossil.z62, A> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.d92
    public final <A extends m62.b, T extends i72<? extends z62, A>> T j(T t) {
        try {
            this.f1275a.t.y.b(t);
            t82 t82 = this.f1275a.t;
            m62.f fVar = t82.p.get(t.w());
            rc2.l(fVar, "Appropriate Api was not requested.");
            if (fVar.c() || !this.f1275a.h.containsKey(t.w())) {
                boolean z = fVar instanceof wc2;
                m62.b bVar = fVar;
                if (z) {
                    bVar = ((wc2) fVar).t0();
                }
                t.y(bVar == 1 ? 1 : 0);
                return t;
            }
            t.A(new Status(17));
            return t;
        } catch (DeadObjectException e) {
            this.f1275a.p(new f82(this, this));
        }
    }

    @DexIgnore
    @Override // com.fossil.d92
    public final <A extends m62.b, R extends z62, T extends i72<R, A>> T k(T t) {
        j(t);
        return t;
    }
}
