package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wk5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f3964a;
    @DexIgnore
    public static String b;
    @DexIgnore
    public static /* final */ wk5 c; // = new wk5();

    /*
    static {
        String simpleName = wk5.class.getSimpleName();
        pq7.b(simpleName, "InstallationUUID::class.java.simpleName");
        f3964a = simpleName;
    }
    */

    @DexIgnore
    public final String a(Context context) {
        String str;
        synchronized (this) {
            pq7.c(context, "context");
            if (b == null) {
                File file = new File(context.getFilesDir(), "INSTALLATION");
                try {
                    if (!file.exists()) {
                        c(file);
                    }
                    b = b(file);
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = f3964a;
                    local.d(str2, ".id(), error=" + e);
                }
            }
            str = b;
            if (str == null) {
                str = "";
            }
        }
        return str;
    }

    @DexIgnore
    public final String b(File file) throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
        byte[] bArr = new byte[((int) randomAccessFile.length())];
        randomAccessFile.readFully(bArr);
        randomAccessFile.close();
        return new String(bArr, et7.f986a);
    }

    @DexIgnore
    public final void c(File file) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        String uuid = UUID.randomUUID().toString();
        pq7.b(uuid, "UUID.randomUUID().toString()");
        Charset charset = et7.f986a;
        if (uuid != null) {
            byte[] bytes = uuid.getBytes(charset);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            fileOutputStream.write(bytes);
            fileOutputStream.close();
            return;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }
}
