package com.fossil;

import com.misfit.frameworks.common.enums.Action;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dm4 {
    @DexIgnore
    public static /* final */ dm4 g; // = new dm4(4201, 4096, 1);
    @DexIgnore
    public static /* final */ dm4 h; // = new dm4(1033, 1024, 1);
    @DexIgnore
    public static /* final */ dm4 i; // = new dm4(67, 64, 1);
    @DexIgnore
    public static /* final */ dm4 j; // = new dm4(19, 16, 1);
    @DexIgnore
    public static /* final */ dm4 k; // = new dm4(285, 256, 0);
    @DexIgnore
    public static /* final */ dm4 l; // = new dm4(Action.Presenter.NEXT, 256, 1);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int[] f807a;
    @DexIgnore
    public /* final */ int[] b;
    @DexIgnore
    public /* final */ em4 c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore
    public dm4(int i2, int i3, int i4) {
        this.e = i2;
        this.d = i3;
        this.f = i4;
        this.f807a = new int[i3];
        this.b = new int[i3];
        int i5 = 1;
        for (int i6 = 0; i6 < i3; i6++) {
            this.f807a[i6] = i5;
            i5 <<= 1;
            if (i5 >= i3) {
                i5 = (i5 ^ i2) & (i3 - 1);
            }
        }
        for (int i7 = 0; i7 < i3 - 1; i7++) {
            this.b[this.f807a[i7]] = i7;
        }
        this.c = new em4(this, new int[]{0});
        new em4(this, new int[]{1});
    }

    @DexIgnore
    public static int a(int i2, int i3) {
        return i2 ^ i3;
    }

    @DexIgnore
    public em4 b(int i2, int i3) {
        if (i2 < 0) {
            throw new IllegalArgumentException();
        } else if (i3 == 0) {
            return this.c;
        } else {
            int[] iArr = new int[(i2 + 1)];
            iArr[0] = i3;
            return new em4(this, iArr);
        }
    }

    @DexIgnore
    public int c(int i2) {
        return this.f807a[i2];
    }

    @DexIgnore
    public int d() {
        return this.f;
    }

    @DexIgnore
    public em4 e() {
        return this.c;
    }

    @DexIgnore
    public int f(int i2) {
        if (i2 != 0) {
            return this.f807a[(this.d - this.b[i2]) - 1];
        }
        throw new ArithmeticException();
    }

    @DexIgnore
    public int g(int i2) {
        if (i2 != 0) {
            return this.b[i2];
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public int h(int i2, int i3) {
        if (i2 == 0 || i3 == 0) {
            return 0;
        }
        int[] iArr = this.f807a;
        int[] iArr2 = this.b;
        return iArr[(iArr2[i3] + iArr2[i2]) % (this.d - 1)];
    }

    @DexIgnore
    public String toString() {
        return "GF(0x" + Integer.toHexString(this.e) + ',' + this.d + ')';
    }
}
