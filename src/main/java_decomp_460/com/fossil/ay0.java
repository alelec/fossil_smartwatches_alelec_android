package com.fossil;

import android.graphics.Rect;
import android.view.ViewGroup;
import androidx.transition.Transition;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ay0 extends oz0 {
    @DexIgnore
    public float b; // = 3.0f;

    @DexIgnore
    public static float h(float f, float f2, float f3, float f4) {
        float f5 = f3 - f;
        float f6 = f4 - f2;
        return (float) Math.sqrt((double) ((f5 * f5) + (f6 * f6)));
    }

    @DexIgnore
    @Override // com.fossil.uy0
    public long c(ViewGroup viewGroup, Transition transition, wy0 wy0, wy0 wy02) {
        int i;
        int round;
        int round2;
        if (wy0 == null && wy02 == null) {
            return 0;
        }
        if (wy02 == null || e(wy0) == 0) {
            i = -1;
            wy02 = wy0;
        } else {
            i = 1;
        }
        int f = f(wy02);
        int g = g(wy02);
        Rect y = transition.y();
        if (y != null) {
            round = y.centerX();
            round2 = y.centerY();
        } else {
            int[] iArr = new int[2];
            viewGroup.getLocationOnScreen(iArr);
            round = Math.round(((float) (iArr[0] + (viewGroup.getWidth() / 2))) + viewGroup.getTranslationX());
            round2 = Math.round(((float) (iArr[1] + (viewGroup.getHeight() / 2))) + viewGroup.getTranslationY());
        }
        float h = h((float) f, (float) g, (float) round, (float) round2) / h(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) viewGroup.getWidth(), (float) viewGroup.getHeight());
        long x = transition.x();
        if (x < 0) {
            x = 300;
        }
        return (long) Math.round((((float) (x * ((long) i))) / this.b) * h);
    }
}
