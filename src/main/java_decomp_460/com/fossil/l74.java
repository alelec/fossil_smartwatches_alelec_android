package com.fossil;

import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l74 extends m74 {
    @DexIgnore
    public /* final */ List<a74<?>> componentsInCycle;

    @DexIgnore
    public l74(List<a74<?>> list) {
        super("Dependency cycle detected: " + Arrays.toString(list.toArray()));
        this.componentsInCycle = list;
    }

    @DexIgnore
    public List<a74<?>> getComponentsInCycle() {
        return this.componentsInCycle;
    }
}
