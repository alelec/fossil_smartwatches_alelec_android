package com.fossil;

import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class po3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ Uri c;
    @DexIgnore
    public /* final */ /* synthetic */ String d;
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ qo3 f;

    @DexIgnore
    public po3(qo3 qo3, boolean z, Uri uri, String str, String str2) {
        this.f = qo3;
        this.b = z;
        this.c = uri;
        this.d = str;
        this.e = str2;
    }

    @DexIgnore
    public final void run() {
        this.f.b(this.b, this.c, this.d, this.e);
    }
}
