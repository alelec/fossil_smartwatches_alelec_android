package com.fossil;

import com.fossil.qg2;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xg2 implements ug2<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ qg2 f4116a;

    @DexIgnore
    public xg2(qg2 qg2) {
        this.f4116a = qg2;
    }

    @DexIgnore
    @Override // com.fossil.ug2
    public final void a(T t) {
        this.f4116a.f2976a = t;
        Iterator it = this.f4116a.c.iterator();
        while (it.hasNext()) {
            ((qg2.a) it.next()).a(this.f4116a.f2976a);
        }
        this.f4116a.c.clear();
        this.f4116a.b = null;
    }
}
