package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ul6 implements Factory<tl6> {
    @DexIgnore
    public static tl6 a(pl6 pl6, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FileRepository fileRepository, no4 no4, PortfolioApp portfolioApp) {
        return new tl6(pl6, summariesRepository, activitiesRepository, userRepository, workoutSessionRepository, fileRepository, no4, portfolioApp);
    }
}
