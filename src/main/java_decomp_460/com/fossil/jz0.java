package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Matrix;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jz0 extends iz0 {
    @DexIgnore
    public static boolean g; // = true;
    @DexIgnore
    public static boolean h; // = true;
    @DexIgnore
    public static boolean i; // = true;

    @DexIgnore
    @Override // com.fossil.nz0
    @SuppressLint({"NewApi"})
    public void e(View view, Matrix matrix) {
        if (g) {
            try {
                view.setAnimationMatrix(matrix);
            } catch (NoSuchMethodError e) {
                g = false;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.nz0
    @SuppressLint({"NewApi"})
    public void i(View view, Matrix matrix) {
        if (h) {
            try {
                view.transformMatrixToGlobal(matrix);
            } catch (NoSuchMethodError e) {
                h = false;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.nz0
    @SuppressLint({"NewApi"})
    public void j(View view, Matrix matrix) {
        if (i) {
            try {
                view.transformMatrixToLocal(matrix);
            } catch (NoSuchMethodError e) {
                i = false;
            }
        }
    }
}
