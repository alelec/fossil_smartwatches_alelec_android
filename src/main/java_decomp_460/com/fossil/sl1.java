package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sl1 extends hv1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ArrayList<rl1> b; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<sl1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final boolean a(rl1 rl1, rl1 rl12) {
            return pq7.a(rl1.getFileName(), rl12.getFileName()) && !Arrays.equals(rl1.getFileData(), rl12.getFileData());
        }

        @DexIgnore
        public sl1 b(Parcel parcel) {
            return new sl1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public sl1 createFromParcel(Parcel parcel) {
            return new sl1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public sl1[] newArray(int i) {
            return new sl1[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements rp7<rl1, Boolean> {
        @DexIgnore
        public static /* final */ b b; // = new b();

        @DexIgnore
        public b() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public Boolean invoke(rl1 rl1) {
            return Boolean.valueOf(rl1.d());
        }
    }

    @DexIgnore
    public /* synthetic */ sl1(Parcel parcel, kq7 kq7) {
        super(parcel);
        ArrayList createTypedArrayList = parcel.createTypedArrayList(rl1.CREATOR);
        if (createTypedArrayList != null) {
            this.b.addAll(createTypedArrayList);
        }
    }

    @DexIgnore
    public sl1(rl1 rl1, rl1 rl12, rl1 rl13, rl1 rl14, rl1 rl15) throws IllegalArgumentException {
        rl1.a(new vs1(0, 0, 0));
        rl12.a(new vs1(0, 62, 1));
        rl13.a(new vs1(90, 62, 1));
        rl14.a(new vs1(180, 62, 1));
        rl15.a(new vs1(270, 62, 1));
        this.b.addAll(hm7.c(rl1, rl12, rl13, rl14, rl15));
        d();
    }

    @DexIgnore
    @Override // com.fossil.hv1
    public JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        Iterator<T> it = this.b.iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next().e());
        }
        JSONObject put = new JSONObject().put("watchFace._.config.backgrounds", jSONArray);
        pq7.b(put, "JSONObject().put(UIScrip\u2026oundsAssignmentJsonArray)");
        return put;
    }

    @DexIgnore
    public final ArrayList<rl1> c() {
        return this.b;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        int size = this.b.size();
        for (int i = 0; i < size - 1; i++) {
            int size2 = this.b.size();
            for (int i2 = i; i2 < size2; i2++) {
                a aVar = CREATOR;
                rl1 rl1 = this.b.get(i);
                pq7.b(rl1, "backgroundImageList[i]");
                rl1 rl12 = this.b.get(i2);
                pq7.b(rl12, "backgroundImageList[j]");
                if (aVar.a(rl1, rl12)) {
                    StringBuilder e = e.e("Background images ");
                    e.append(this.b.get(i).getFileName());
                    e.append(" and  ");
                    e.append(this.b.get(i2).getFileName());
                    e.append(' ');
                    e.append("have same names but different data.");
                    throw new IllegalArgumentException(e.toString());
                }
            }
        }
        mm7.w(this.b, b.b);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(sl1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.b, ((sl1) obj).b) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.background.BackgroundImageConfig");
    }

    @DexIgnore
    public final rl1 getBottomBackground() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 180 && t2.getPositionConfig().getDistanceFromCenter() == 62 && t2.getPositionConfig().getZIndex() == 1) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final rl1 getLeftBackground() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 270 && t2.getPositionConfig().getDistanceFromCenter() == 62 && t2.getPositionConfig().getZIndex() == 1) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final rl1 getMainBackground() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 0 && t2.getPositionConfig().getDistanceFromCenter() == 0 && t2.getPositionConfig().getZIndex() == 0) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final rl1 getRightBackground() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 90 && t2.getPositionConfig().getDistanceFromCenter() == 62 && t2.getPositionConfig().getZIndex() == 1) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final rl1 getTopBackground() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 0 && t2.getPositionConfig().getDistanceFromCenter() == 62 && t2.getPositionConfig().getZIndex() == 1) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            jd0 jd0 = jd0.G;
            Object[] array = this.b.toArray(new rl1[0]);
            if (array != null) {
                g80.k(jSONObject, jd0, px1.a((ox1[]) array));
                return jSONObject;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        } catch (JSONException e) {
            d90.i.i(e);
        }
    }

    @DexIgnore
    @Override // com.fossil.hv1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedList(this.b);
        }
    }
}
