package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tu4 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<vs4> f3470a; // = new ArrayList();
    @DexIgnore
    public a b;
    @DexIgnore
    public /* final */ int c; // = gl0.d(PortfolioApp.h0.c(), 2131099689);
    @DexIgnore
    public /* final */ int d; // = gl0.d(PortfolioApp.h0.c(), 2131099677);

    @DexIgnore
    public interface a {
        @DexIgnore
        void k6(vs4 vs4);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ xd5 f3471a;
        @DexIgnore
        public /* final */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ tu4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends qq7 implements rp7<View, tl7> {
            @DexIgnore
            public /* final */ /* synthetic */ vs4 $item$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, vs4 vs4) {
                super(1);
                this.this$0 = bVar;
                this.$item$inlined = vs4;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.rp7
            public /* bridge */ /* synthetic */ tl7 invoke(View view) {
                invoke(view);
                return tl7.f3441a;
            }

            @DexIgnore
            public final void invoke(View view) {
                a aVar;
                if (this.this$0.getAdapterPosition() != -1 && (aVar = this.this$0.c.b) != null) {
                    aVar.k6(this.$item$inlined);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(tu4 tu4, xd5 xd5, View view) {
            super(view);
            pq7.c(xd5, "binding");
            pq7.c(view, "root");
            this.c = tu4;
            this.f3471a = xd5;
            this.b = view;
        }

        @DexIgnore
        public void a(vs4 vs4) {
            int i = 8;
            pq7.c(vs4, "item");
            xd5 xd5 = this.f3471a;
            View view = xd5.v;
            pq7.b(view, "topDivider");
            view.setVisibility(getAdapterPosition() == 0 ? 8 : 0);
            View view2 = xd5.q;
            pq7.b(view2, "bottomDivider");
            if (getAdapterPosition() != this.c.f3470a.size() - 1) {
                i = 0;
            }
            view2.setVisibility(i);
            FlexibleTextView flexibleTextView = xd5.x;
            pq7.b(flexibleTextView, "tvName");
            flexibleTextView.setText(vs4.c());
            FlexibleTextView flexibleTextView2 = xd5.w;
            pq7.b(flexibleTextView2, "tvDesc");
            flexibleTextView2.setText(vs4.a());
            ImageView imageView = xd5.u;
            pq7.b(imageView, "ivThumbnail");
            imageView.setImageDrawable(gl0.f(imageView.getContext(), vs4.f()));
            String g = vs4.g();
            int hashCode = g.hashCode();
            if (hashCode != -1348781656) {
                if (hashCode == -637042289 && g.equals("activity_reach_goal")) {
                    xd5.x.setTextColor(this.c.c);
                }
            } else if (g.equals("activity_best_result")) {
                xd5.x.setTextColor(this.c.d);
            }
            FlexibleButton flexibleButton = xd5.t;
            pq7.b(flexibleButton, "fbCreate");
            fz4.a(flexibleButton, new a(this, vs4));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f3470a.size();
    }

    @DexIgnore
    /* renamed from: k */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        bVar.a(this.f3470a.get(i));
    }

    @DexIgnore
    /* renamed from: l */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        xd5 z = xd5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemChallengeTemplateLis\u2026tInflater, parent, false)");
        View n = z.n();
        pq7.b(n, "itemTemplateListBinding.root");
        return new b(this, z, n);
    }

    @DexIgnore
    public final void m(a aVar) {
        pq7.c(aVar, "listener");
        this.b = aVar;
    }

    @DexIgnore
    public final void n(List<vs4> list) {
        pq7.c(list, "newList");
        this.f3470a.clear();
        this.f3470a.addAll(list);
        notifyDataSetChanged();
    }
}
