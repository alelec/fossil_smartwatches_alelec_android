package com.fossil;

import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.nk5;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yy6 extends iq4<ty6, uy6, sy6> {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ HybridPresetRepository e;
    @DexIgnore
    public /* final */ MicroAppRepository f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ NotificationsRepository h;
    @DexIgnore
    public /* final */ CategoryRepository i;
    @DexIgnore
    public /* final */ AlarmsRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$1", f = "GetHybridDeviceSettingUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ SparseArray $data;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isMovemberModel;
        @DexIgnore
        public /* final */ /* synthetic */ String $serialNumber;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(SparseArray sparseArray, boolean z, String str, qn7 qn7) {
            super(2, qn7);
            this.$data = sparseArray;
            this.$isMovemberModel = z;
            this.$serialNumber = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.$data, this.$isMovemberModel, this.$serialNumber, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                AppNotificationFilterSettings b = e47.b.b(this.$data, this.$isMovemberModel);
                PortfolioApp.h0.c().s1(b, this.$serialNumber);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = yy6.k;
                local.d(str, "saveNotificationSettingToDevice, total: " + b.getNotificationFilters().size() + " items");
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$start$1", f = "GetHybridDeviceSettingUseCase.kt", l = {60, 61, 62, 63, 64, 76, 84, 100}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ yy6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(yy6 yy6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = yy6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:116:0x03dd  */
        /* JADX WARNING: Removed duplicated region for block: B:118:0x03e1  */
        /* JADX WARNING: Removed duplicated region for block: B:122:0x03e9  */
        /* JADX WARNING: Removed duplicated region for block: B:124:0x03ed  */
        /* JADX WARNING: Removed duplicated region for block: B:126:0x03f1  */
        /* JADX WARNING: Removed duplicated region for block: B:128:0x03f5  */
        /* JADX WARNING: Removed duplicated region for block: B:141:0x02d8 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00f0  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0153  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x01ac  */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x022e  */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x0246  */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x0269  */
        /* JADX WARNING: Removed duplicated region for block: B:73:0x028c  */
        /* JADX WARNING: Removed duplicated region for block: B:87:0x02dd  */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x005c  */
        /* JADX WARNING: Removed duplicated region for block: B:92:0x032b  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 1048
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.yy6.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = yy6.class.getSimpleName();
        pq7.b(simpleName, "GetHybridDeviceSettingUs\u2026se::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public yy6(HybridPresetRepository hybridPresetRepository, MicroAppRepository microAppRepository, DeviceRepository deviceRepository, NotificationsRepository notificationsRepository, CategoryRepository categoryRepository, AlarmsRepository alarmsRepository) {
        pq7.c(hybridPresetRepository, "mPresetRepository");
        pq7.c(microAppRepository, "mMicroAppRepository");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(notificationsRepository, "mNotificationsRepository");
        pq7.c(categoryRepository, "mCategoryRepository");
        pq7.c(alarmsRepository, "mAlarmsRepository");
        this.e = hybridPresetRepository;
        this.f = microAppRepository;
        this.g = deviceRepository;
        this.h = notificationsRepository;
        this.i = categoryRepository;
        this.j = alarmsRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return k;
    }

    @DexIgnore
    /* renamed from: v */
    public Object k(ty6 ty6, qn7<? super tl7> qn7) {
        if (ty6 == null) {
            i(new sy6(600, ""));
            return tl7.f3441a;
        }
        this.d = ty6.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        StringBuilder sb = new StringBuilder();
        sb.append("Inside .GetHybridDeviceSettingUseCase executing!!! with serial=");
        String str2 = this.d;
        if (str2 != null) {
            sb.append(str2);
            local.d(str, sb.toString());
            if (!o37.b(PortfolioApp.h0.c())) {
                i(new sy6(601, ""));
                return tl7.f3441a;
            }
            if (!TextUtils.isEmpty(this.d)) {
                nk5.a aVar = nk5.o;
                String str3 = this.d;
                if (str3 == null) {
                    pq7.i();
                    throw null;
                } else if (aVar.v(str3)) {
                    x();
                    return tl7.f3441a;
                }
            }
            i(new sy6(600, ""));
            return tl7.f3441a;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final xw7 w(SparseArray<List<BaseFeatureModel>> sparseArray, String str, boolean z) {
        return gu7.d(jv7.a(bw7.a()), null, null, new a(sparseArray, z, str, null), 3, null);
    }

    @DexIgnore
    public final xw7 x() {
        return gu7.d(g(), null, null, new b(this, null), 3, null);
    }
}
