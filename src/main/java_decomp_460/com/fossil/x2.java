package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x2 implements Parcelable.Creator<z1> {
    @DexIgnore
    public /* synthetic */ x2(kq7 kq7) {
    }

    @DexIgnore
    public z1 a(Parcel parcel) {
        return new z1(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public z1 createFromParcel(Parcel parcel) {
        return new z1(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public z1[] newArray(int i) {
        return new z1[i];
    }
}
