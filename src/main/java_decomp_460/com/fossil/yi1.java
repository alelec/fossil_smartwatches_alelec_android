package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.yi1;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yi1<T extends yi1<T>> implements Cloneable {
    @DexIgnore
    public Resources.Theme A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public boolean C;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public boolean E; // = true;
    @DexIgnore
    public boolean F;
    @DexIgnore
    public int b;
    @DexIgnore
    public float c; // = 1.0f;
    @DexIgnore
    public wc1 d; // = wc1.d;
    @DexIgnore
    public sa1 e; // = sa1.NORMAL;
    @DexIgnore
    public Drawable f;
    @DexIgnore
    public int g;
    @DexIgnore
    public Drawable h;
    @DexIgnore
    public int i;
    @DexIgnore
    public boolean j; // = true;
    @DexIgnore
    public int k; // = -1;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public mb1 m; // = xj1.c();
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t; // = true;
    @DexIgnore
    public Drawable u;
    @DexIgnore
    public int v;
    @DexIgnore
    public ob1 w; // = new ob1();
    @DexIgnore
    public Map<Class<?>, sb1<?>> x; // = new ak1();
    @DexIgnore
    public Class<?> y; // = Object.class;
    @DexIgnore
    public boolean z;

    @DexIgnore
    public static boolean Q(int i2, int i3) {
        return (i2 & i3) != 0;
    }

    @DexIgnore
    public final sa1 B() {
        return this.e;
    }

    @DexIgnore
    public final Class<?> C() {
        return this.y;
    }

    @DexIgnore
    public final mb1 E() {
        return this.m;
    }

    @DexIgnore
    public final float G() {
        return this.c;
    }

    @DexIgnore
    public final Resources.Theme H() {
        return this.A;
    }

    @DexIgnore
    public final Map<Class<?>, sb1<?>> I() {
        return this.x;
    }

    @DexIgnore
    public final boolean J() {
        return this.F;
    }

    @DexIgnore
    public final boolean K() {
        return this.C;
    }

    @DexIgnore
    public final boolean M() {
        return this.j;
    }

    @DexIgnore
    public final boolean N() {
        return P(8);
    }

    @DexIgnore
    public boolean O() {
        return this.E;
    }

    @DexIgnore
    public final boolean P(int i2) {
        return Q(this.b, i2);
    }

    @DexIgnore
    public final boolean R() {
        return this.t;
    }

    @DexIgnore
    public final boolean S() {
        return this.s;
    }

    @DexIgnore
    public final boolean T() {
        return P(2048);
    }

    @DexIgnore
    public final boolean U() {
        return jk1.s(this.l, this.k);
    }

    @DexIgnore
    public T V() {
        this.z = true;
        h0();
        return this;
    }

    @DexIgnore
    public T W() {
        return a0(fg1.c, new cg1());
    }

    @DexIgnore
    public T X() {
        return Z(fg1.b, new dg1());
    }

    @DexIgnore
    public T Y() {
        return Z(fg1.f1123a, new kg1());
    }

    @DexIgnore
    public final T Z(fg1 fg1, sb1<Bitmap> sb1) {
        return g0(fg1, sb1, false);
    }

    @DexIgnore
    public final T a0(fg1 fg1, sb1<Bitmap> sb1) {
        if (this.B) {
            return (T) clone().a0(fg1, sb1);
        }
        o(fg1);
        return p0(sb1, false);
    }

    @DexIgnore
    public T b0(int i2, int i3) {
        if (this.B) {
            return (T) clone().b0(i2, i3);
        }
        this.l = i2;
        this.k = i3;
        this.b |= 512;
        i0();
        return this;
    }

    @DexIgnore
    public T c0(int i2) {
        if (this.B) {
            return (T) clone().c0(i2);
        }
        this.i = i2;
        int i3 = this.b | 128;
        this.b = i3;
        this.h = null;
        this.b = i3 & -65;
        i0();
        return this;
    }

    @DexIgnore
    public T d(yi1<?> yi1) {
        if (this.B) {
            return (T) clone().d(yi1);
        }
        if (Q(yi1.b, 2)) {
            this.c = yi1.c;
        }
        if (Q(yi1.b, 262144)) {
            this.C = yi1.C;
        }
        if (Q(yi1.b, 1048576)) {
            this.F = yi1.F;
        }
        if (Q(yi1.b, 4)) {
            this.d = yi1.d;
        }
        if (Q(yi1.b, 8)) {
            this.e = yi1.e;
        }
        if (Q(yi1.b, 16)) {
            this.f = yi1.f;
            this.g = 0;
            this.b &= -33;
        }
        if (Q(yi1.b, 32)) {
            this.g = yi1.g;
            this.f = null;
            this.b &= -17;
        }
        if (Q(yi1.b, 64)) {
            this.h = yi1.h;
            this.i = 0;
            this.b &= -129;
        }
        if (Q(yi1.b, 128)) {
            this.i = yi1.i;
            this.h = null;
            this.b &= -65;
        }
        if (Q(yi1.b, 256)) {
            this.j = yi1.j;
        }
        if (Q(yi1.b, 512)) {
            this.l = yi1.l;
            this.k = yi1.k;
        }
        if (Q(yi1.b, 1024)) {
            this.m = yi1.m;
        }
        if (Q(yi1.b, 4096)) {
            this.y = yi1.y;
        }
        if (Q(yi1.b, 8192)) {
            this.u = yi1.u;
            this.v = 0;
            this.b &= -16385;
        }
        if (Q(yi1.b, 16384)) {
            this.v = yi1.v;
            this.u = null;
            this.b &= -8193;
        }
        if (Q(yi1.b, 32768)) {
            this.A = yi1.A;
        }
        if (Q(yi1.b, 65536)) {
            this.t = yi1.t;
        }
        if (Q(yi1.b, 131072)) {
            this.s = yi1.s;
        }
        if (Q(yi1.b, 2048)) {
            this.x.putAll(yi1.x);
            this.E = yi1.E;
        }
        if (Q(yi1.b, 524288)) {
            this.D = yi1.D;
        }
        if (!this.t) {
            this.x.clear();
            int i2 = this.b & -2049;
            this.b = i2;
            this.s = false;
            this.b = i2 & -131073;
            this.E = true;
        }
        this.b |= yi1.b;
        this.w.d(yi1.w);
        i0();
        return this;
    }

    @DexIgnore
    public T d0(Drawable drawable) {
        if (this.B) {
            return (T) clone().d0(drawable);
        }
        this.h = drawable;
        int i2 = this.b | 64;
        this.b = i2;
        this.i = 0;
        this.b = i2 & -129;
        i0();
        return this;
    }

    @DexIgnore
    public T e() {
        if (!this.z || this.B) {
            this.B = true;
            return V();
        }
        throw new IllegalStateException("You cannot auto lock an already locked options object, try clone() first");
    }

    @DexIgnore
    public T e0(sa1 sa1) {
        if (this.B) {
            return (T) clone().e0(sa1);
        }
        ik1.d(sa1);
        this.e = sa1;
        this.b |= 8;
        i0();
        return this;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof yi1)) {
            return false;
        }
        yi1 yi1 = (yi1) obj;
        return Float.compare(yi1.c, this.c) == 0 && this.g == yi1.g && jk1.d(this.f, yi1.f) && this.i == yi1.i && jk1.d(this.h, yi1.h) && this.v == yi1.v && jk1.d(this.u, yi1.u) && this.j == yi1.j && this.k == yi1.k && this.l == yi1.l && this.s == yi1.s && this.t == yi1.t && this.C == yi1.C && this.D == yi1.D && this.d.equals(yi1.d) && this.e == yi1.e && this.w.equals(yi1.w) && this.x.equals(yi1.x) && this.y.equals(yi1.y) && jk1.d(this.m, yi1.m) && jk1.d(this.A, yi1.A);
    }

    @DexIgnore
    public final T f0(fg1 fg1, sb1<Bitmap> sb1) {
        return g0(fg1, sb1, true);
    }

    @DexIgnore
    public T g() {
        return f0(fg1.b, new dg1());
    }

    @DexIgnore
    public final T g0(fg1 fg1, sb1<Bitmap> sb1, boolean z2) {
        T q0 = z2 ? q0(fg1, sb1) : a0(fg1, sb1);
        q0.E = true;
        return q0;
    }

    @DexIgnore
    public final T h0() {
        return this;
    }

    @DexIgnore
    public int hashCode() {
        return jk1.n(this.A, jk1.n(this.m, jk1.n(this.y, jk1.n(this.x, jk1.n(this.w, jk1.n(this.e, jk1.n(this.d, jk1.o(this.D, jk1.o(this.C, jk1.o(this.t, jk1.o(this.s, jk1.m(this.l, jk1.m(this.k, jk1.o(this.j, jk1.n(this.u, jk1.m(this.v, jk1.n(this.h, jk1.m(this.i, jk1.n(this.f, jk1.m(this.g, jk1.k(this.c)))))))))))))))))))));
    }

    @DexIgnore
    /* renamed from: i */
    public T clone() {
        try {
            T t2 = (T) ((yi1) super.clone());
            ob1 ob1 = new ob1();
            t2.w = ob1;
            ob1.d(this.w);
            ak1 ak1 = new ak1();
            t2.x = ak1;
            ak1.putAll(this.x);
            t2.z = false;
            t2.B = false;
            return t2;
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public final T i0() {
        if (!this.z) {
            h0();
            return this;
        }
        throw new IllegalStateException("You cannot modify locked T, consider clone()");
    }

    @DexIgnore
    public T j(Class<?> cls) {
        if (this.B) {
            return (T) clone().j(cls);
        }
        ik1.d(cls);
        this.y = cls;
        this.b |= 4096;
        i0();
        return this;
    }

    @DexIgnore
    public <Y> T j0(nb1<Y> nb1, Y y2) {
        if (this.B) {
            return (T) clone().j0(nb1, y2);
        }
        ik1.d(nb1);
        ik1.d(y2);
        this.w.e(nb1, y2);
        i0();
        return this;
    }

    @DexIgnore
    public T k0(mb1 mb1) {
        if (this.B) {
            return (T) clone().k0(mb1);
        }
        ik1.d(mb1);
        this.m = mb1;
        this.b |= 1024;
        i0();
        return this;
    }

    @DexIgnore
    public T l(wc1 wc1) {
        if (this.B) {
            return (T) clone().l(wc1);
        }
        ik1.d(wc1);
        this.d = wc1;
        this.b |= 4;
        i0();
        return this;
    }

    @DexIgnore
    public T l0(float f2) {
        if (this.B) {
            return (T) clone().l0(f2);
        }
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f2 > 1.0f) {
            throw new IllegalArgumentException("sizeMultiplier must be between 0 and 1");
        }
        this.c = f2;
        this.b |= 2;
        i0();
        return this;
    }

    @DexIgnore
    public T m0(boolean z2) {
        if (this.B) {
            return (T) clone().m0(true);
        }
        this.j = !z2;
        this.b |= 256;
        i0();
        return this;
    }

    @DexIgnore
    public T n() {
        return j0((nb1<Y>) nh1.b, Boolean.TRUE);
    }

    @DexIgnore
    public T n0(int i2) {
        return j0((nb1<Y>) lf1.b, Integer.valueOf(i2));
    }

    @DexIgnore
    public T o(fg1 fg1) {
        ik1.d(fg1);
        return j0((nb1<Y>) fg1.f, fg1);
    }

    @DexIgnore
    public T o0(sb1<Bitmap> sb1) {
        return p0(sb1, true);
    }

    @DexIgnore
    public final wc1 p() {
        return this.d;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.sb1<android.graphics.Bitmap> */
    /* JADX WARN: Multi-variable type inference failed */
    public T p0(sb1<Bitmap> sb1, boolean z2) {
        if (this.B) {
            return (T) clone().p0(sb1, z2);
        }
        ig1 ig1 = new ig1(sb1, z2);
        r0(Bitmap.class, sb1, z2);
        r0(Drawable.class, ig1, z2);
        ig1.c();
        r0(BitmapDrawable.class, ig1, z2);
        r0(hh1.class, new kh1(sb1), z2);
        i0();
        return this;
    }

    @DexIgnore
    public final int q() {
        return this.g;
    }

    @DexIgnore
    public final T q0(fg1 fg1, sb1<Bitmap> sb1) {
        if (this.B) {
            return (T) clone().q0(fg1, sb1);
        }
        o(fg1);
        return o0(sb1);
    }

    @DexIgnore
    public final Drawable r() {
        return this.f;
    }

    @DexIgnore
    public <Y> T r0(Class<Y> cls, sb1<Y> sb1, boolean z2) {
        if (this.B) {
            return (T) clone().r0(cls, sb1, z2);
        }
        ik1.d(cls);
        ik1.d(sb1);
        this.x.put(cls, sb1);
        int i2 = this.b | 2048;
        this.b = i2;
        this.t = true;
        int i3 = i2 | 65536;
        this.b = i3;
        this.E = false;
        if (z2) {
            this.b = i3 | 131072;
            this.s = true;
        }
        i0();
        return this;
    }

    @DexIgnore
    public final Drawable s() {
        return this.u;
    }

    @DexIgnore
    public T s0(boolean z2) {
        if (this.B) {
            return (T) clone().s0(z2);
        }
        this.F = z2;
        this.b |= 1048576;
        i0();
        return this;
    }

    @DexIgnore
    public final int t() {
        return this.v;
    }

    @DexIgnore
    public final boolean u() {
        return this.D;
    }

    @DexIgnore
    public final ob1 v() {
        return this.w;
    }

    @DexIgnore
    public final int w() {
        return this.k;
    }

    @DexIgnore
    public final int x() {
        return this.l;
    }

    @DexIgnore
    public final Drawable y() {
        return this.h;
    }

    @DexIgnore
    public final int z() {
        return this.i;
    }
}
