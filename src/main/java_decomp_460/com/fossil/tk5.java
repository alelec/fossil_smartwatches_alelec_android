package com.fossil;

import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tk5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f3431a; // = "tk5";

    @DexIgnore
    public static int a(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = f3431a;
        local.d(str3, "compareVersionNames oldVersionName=" + str + "newVersionName=" + str2);
        if (TextUtils.isEmpty(str)) {
            return 1;
        }
        if (TextUtils.isEmpty(str2)) {
            return -1;
        }
        return new z68(str).compareTo(new z68(str2));
    }

    @DexIgnore
    public static int b(int i) {
        if (i != 25) {
            return (i == 50 || i != 100) ? 2 : 3;
        }
        return 1;
    }

    @DexIgnore
    public static int c(int i) {
        if (i != 1) {
            return (i == 2 || i != 3) ? 50 : 100;
        }
        return 25;
    }

    @DexIgnore
    public static boolean d() {
        return true;
    }
}
