package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface y08 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static final y08 f4224a = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements y08 {
        @DexIgnore
        @Override // com.fossil.y08
        public v18 authenticate(x18 x18, Response response) {
            return null;
        }
    }

    @DexIgnore
    v18 authenticate(x18 x18, Response response) throws IOException;
}
