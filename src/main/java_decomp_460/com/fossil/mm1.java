package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mm1 extends ym1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ cu1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<mm1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final mm1 a(byte[] bArr) throws IllegalArgumentException {
            cu1 cu1;
            if (bArr.length >= 3) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                short s = order.getShort(0);
                byte b = order.get(2);
                if (bArr.length > 3) {
                    cu1 = cu1.d.a(order.get(3));
                    if (cu1 == null) {
                        StringBuilder e = e.e("Invalid state: ");
                        e.append((int) order.get(3));
                        throw new IllegalArgumentException(e.toString());
                    }
                } else {
                    cu1 = cu1.NORMAL;
                }
                return new mm1(s, b, cu1);
            }
            throw new IllegalArgumentException(e.c(e.e("Invalid data size: "), bArr.length, ", require as ", "least: 3"));
        }

        @DexIgnore
        public mm1 b(Parcel parcel) {
            return new mm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public mm1 createFromParcel(Parcel parcel) {
            return new mm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public mm1[] newArray(int i) {
            return new mm1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ mm1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = (short) ((short) parcel.readInt());
        this.d = parcel.readByte();
        cu1 a2 = cu1.d.a(parcel.readByte());
        if (a2 != null) {
            this.e = a2;
            d();
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public mm1(short s, byte b, cu1 cu1) throws IllegalArgumentException {
        super(zm1.BATTERY);
        this.c = (short) s;
        this.d = (byte) b;
        this.e = cu1;
        d();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).putShort(this.c).put(this.d).put(this.e.a()).array();
        pq7.b(array, "ByteBuffer.allocate(MIN_\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            g80.k(jSONObject, jd0.c5, Short.valueOf(this.c));
            g80.k(jSONObject, jd0.b5, Byte.valueOf(this.d));
            g80.k(jSONObject, jd0.n1, this.e);
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        boolean z = true;
        short s = this.c;
        if (s >= 0 && 4400 >= s) {
            byte b = this.d;
            if (b < 0 || 100 < b) {
                z = false;
            }
            if (!z) {
                throw new IllegalArgumentException(e.c(e.e("percentage("), this.d, ") is out of range ", "[0, 100]."));
            }
            return;
        }
        throw new IllegalArgumentException(e.c(e.e("voltage("), this.c, ") is out of range ", "[0, 4400]."));
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(mm1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            mm1 mm1 = (mm1) obj;
            if (this.c != mm1.c) {
                return false;
            }
            if (this.d != mm1.d) {
                return false;
            }
            return this.e == mm1.e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BatteryConfig");
    }

    @DexIgnore
    public final byte getPercentage() {
        return this.d;
    }

    @DexIgnore
    public final cu1 getState() {
        return this.e;
    }

    @DexIgnore
    public final short getVoltage() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public int hashCode() {
        short s = this.c;
        return (((s * 31) + Byte.valueOf(this.d).hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(hy1.n(this.c));
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
    }
}
