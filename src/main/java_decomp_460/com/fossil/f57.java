package com.fossil;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.RoundRectShape;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f57 extends ShapeDrawable {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Paint f1054a;
    @DexIgnore
    public /* final */ Paint b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ RectShape e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ float i;
    @DexIgnore
    public /* final */ int j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements c, d, b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f1055a; // = "";
        @DexIgnore
        public int b; // = -7829368;
        @DexIgnore
        public int c; // = 0;
        @DexIgnore
        public int d; // = -1;
        @DexIgnore
        public int e; // = -1;
        @DexIgnore
        public Typeface f; // = Typeface.create("sans-serif-light", 0);
        @DexIgnore
        public RectShape g; // = new RectShape();
        @DexIgnore
        public int h; // = -1;
        @DexIgnore
        public int i; // = -1;
        @DexIgnore
        public boolean j; // = false;
        @DexIgnore
        public boolean k; // = false;
        @DexIgnore
        public float l;

        @DexIgnore
        @Override // com.fossil.f57.d
        public f57 a(String str, int i2) {
            k();
            return e(str, i2);
        }

        @DexIgnore
        @Override // com.fossil.f57.c
        public d b() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.f57.c
        public c c(int i2) {
            this.d = i2;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.f57.c
        public c d(int i2) {
            this.i = i2;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.f57.b
        public f57 e(String str, int i2) {
            this.b = i2;
            this.f1055a = str;
            return new f57(this);
        }

        @DexIgnore
        @Override // com.fossil.f57.d
        public b f() {
            this.g = new OvalShape();
            return this;
        }

        @DexIgnore
        @Override // com.fossil.f57.d
        public c g() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.f57.c
        public c h(int i2) {
            this.e = i2;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.f57.c
        public c i(int i2) {
            this.h = i2;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.f57.c
        public c j(Typeface typeface) {
            this.f = typeface;
            return this;
        }

        @DexIgnore
        public b k() {
            this.g = new RectShape();
            return this;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        f57 e(String str, int i);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        d b();

        @DexIgnore
        c c(int i);

        @DexIgnore
        c d(int i);

        @DexIgnore
        c h(int i);

        @DexIgnore
        c i(int i);

        @DexIgnore
        c j(Typeface typeface);
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        f57 a(String str, int i);

        @DexIgnore
        b f();

        @DexIgnore
        c g();
    }

    @DexIgnore
    public f57(a aVar) {
        super(aVar.g);
        this.e = aVar.g;
        this.f = aVar.e;
        this.g = aVar.d;
        this.i = aVar.l;
        this.c = aVar.k ? aVar.f1055a.toUpperCase() : aVar.f1055a;
        this.d = aVar.b;
        this.h = aVar.i;
        Paint paint = new Paint();
        this.f1054a = paint;
        paint.setColor(aVar.h);
        this.f1054a.setAntiAlias(true);
        this.f1054a.setFakeBoldText(aVar.j);
        this.f1054a.setStyle(Paint.Style.FILL);
        this.f1054a.setTypeface(aVar.f);
        this.f1054a.setTextAlign(Paint.Align.CENTER);
        this.f1054a.setStrokeWidth((float) aVar.c);
        this.j = aVar.c;
        Paint paint2 = new Paint();
        this.b = paint2;
        paint2.setAntiAlias(true);
        this.b.setColor(c(this.d));
        this.b.setStyle(Paint.Style.STROKE);
        this.b.setStrokeWidth((float) this.j);
        Paint paint3 = getPaint();
        paint3.setAntiAlias(true);
        paint3.setColor(this.d);
    }

    @DexIgnore
    public static d a() {
        return new a();
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        RectF rectF = new RectF(getBounds());
        int i2 = this.j;
        rectF.inset((float) (i2 / 2), (float) (i2 / 2));
        RectShape rectShape = this.e;
        if (rectShape instanceof OvalShape) {
            canvas.drawOval(rectF, this.b);
        } else if (rectShape instanceof RoundRectShape) {
            float f2 = this.i;
            canvas.drawRoundRect(rectF, f2, f2, this.b);
        } else {
            canvas.drawRect(rectF, this.b);
        }
    }

    @DexIgnore
    public final int c(int i2) {
        return Color.rgb((int) (((float) Color.red(i2)) * 0.9f), (int) (((float) Color.green(i2)) * 0.9f), (int) (((float) Color.blue(i2)) * 0.9f));
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        Rect bounds = getBounds();
        if (this.j > 0) {
            b(canvas);
        }
        int save = canvas.save();
        canvas.translate((float) bounds.left, (float) bounds.top);
        int i2 = this.g;
        if (i2 < 0) {
            i2 = bounds.width();
        }
        int i3 = this.f;
        if (i3 < 0) {
            i3 = bounds.height();
        }
        int i4 = this.h;
        if (i4 < 0) {
            i4 = Math.min(i2, i3) / 2;
        }
        this.f1054a.setTextSize((float) i4);
        canvas.drawText(this.c, (float) (i2 / 2), ((float) (i3 / 2)) - ((this.f1054a.descent() + (this.f1054a.ascent() / 2.0f)) + (this.f1054a.ascent() / 6.0f)), this.f1054a);
        canvas.restoreToCount(save);
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.f;
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.g;
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        this.f1054a.setAlpha(i2);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.f1054a.setColorFilter(colorFilter);
    }
}
