package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rt5 implements Factory<qt5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<UserRepository> f3165a;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> b;
    @DexIgnore
    public /* final */ Provider<mj5> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;
    @DexIgnore
    public /* final */ Provider<on5> e;

    @DexIgnore
    public rt5(Provider<UserRepository> provider, Provider<DeviceRepository> provider2, Provider<mj5> provider3, Provider<PortfolioApp> provider4, Provider<on5> provider5) {
        this.f3165a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static rt5 a(Provider<UserRepository> provider, Provider<DeviceRepository> provider2, Provider<mj5> provider3, Provider<PortfolioApp> provider4, Provider<on5> provider5) {
        return new rt5(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public static qt5 c(UserRepository userRepository, DeviceRepository deviceRepository, mj5 mj5, PortfolioApp portfolioApp, on5 on5) {
        return new qt5(userRepository, deviceRepository, mj5, portfolioApp, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public qt5 get() {
        return c(this.f3165a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }
}
