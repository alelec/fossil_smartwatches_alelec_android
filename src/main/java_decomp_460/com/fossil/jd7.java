package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum jd7 {
    NO_CACHE(1),
    NO_STORE(2);
    
    @DexIgnore
    public /* final */ int index;

    @DexIgnore
    public jd7(int i) {
        this.index = i;
    }

    @DexIgnore
    public static boolean shouldReadFromMemoryCache(int i) {
        return (NO_CACHE.index & i) == 0;
    }

    @DexIgnore
    public static boolean shouldWriteToMemoryCache(int i) {
        return (NO_STORE.index & i) == 0;
    }
}
