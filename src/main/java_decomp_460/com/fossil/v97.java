package com.fossil;

import com.portfolio.platform.data.source.FileRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v97 implements Factory<u97> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<s77> f3738a;
    @DexIgnore
    public /* final */ Provider<FileRepository> b;

    @DexIgnore
    public v97(Provider<s77> provider, Provider<FileRepository> provider2) {
        this.f3738a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static v97 a(Provider<s77> provider, Provider<FileRepository> provider2) {
        return new v97(provider, provider2);
    }

    @DexIgnore
    public static u97 c(s77 s77, FileRepository fileRepository) {
        return new u97(s77, fileRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public u97 get() {
        return c(this.f3738a.get(), this.b.get());
    }
}
