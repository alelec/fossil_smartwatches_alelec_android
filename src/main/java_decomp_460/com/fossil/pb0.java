package com.fossil;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.fossil.blesdk.model.network.AuthMaintainer", f = "AuthMaintainer.kt", l = {64, 73}, m = "getAccessToken")
public final class pb0 extends co7 {
    @DexIgnore
    public /* synthetic */ Object b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ /* synthetic */ rb0 d;
    @DexIgnore
    public Object e;
    @DexIgnore
    public Object f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pb0(rb0 rb0, qn7 qn7) {
        super(qn7);
        this.d = rb0;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        this.b = obj;
        this.c |= RecyclerView.UNDEFINED_DURATION;
        return this.d.a(this);
    }
}
