package com.fossil;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.bo6;
import com.fossil.iw5;
import com.fossil.jn5;
import com.fossil.nk5;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.BaseWebViewActivity;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardActivity;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsActivity;
import com.portfolio.platform.uirenew.home.profile.about.AboutActivity;
import com.portfolio.platform.uirenew.home.profile.battery.ReplaceBatteryActivity;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditActivity;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInActivity;
import com.portfolio.platform.uirenew.home.profile.password.ProfileChangePasswordActivity;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesActivity;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitActivity;
import com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.view.AutoResizeTextView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mz5 extends qv5 implements ao6, iw5.b, t47.g, aw5 {
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public zn6 h;
    @DexIgnore
    public g37<b85> i;
    @DexIgnore
    public wj5 j;
    @DexIgnore
    public iw5 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final mz5 a() {
            return new mz5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public b(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                HelpActivity.a aVar = HelpActivity.B;
                pq7.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public c(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                AboutActivity.a aVar = AboutActivity.B;
                pq7.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public d(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ConnectedAppsActivity.a aVar = ConnectedAppsActivity.C;
                pq7.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public e(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ProfileOptInActivity.a aVar = ProfileOptInActivity.B;
                pq7.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public f(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ReplaceBatteryActivity.a aVar = ReplaceBatteryActivity.B;
                pq7.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public g(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ThemesActivity.a aVar = ThemesActivity.A;
                pq7.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public h(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                String a2 = h37.b.a(6);
                BaseWebViewActivity.a aVar = BaseWebViewActivity.D;
                pq7.b(activity, "it");
                aVar.b(activity, "", a2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public i(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.doCameraTask();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public j(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.M6().n(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public k(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ProfileEditActivity.a aVar = ProfileEditActivity.A;
                pq7.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public l(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.M6().n(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public m(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
                pq7.b(activity, "it");
                PairingInstructionsActivity.a.b(aVar, activity, false, false, 4, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public n(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ProfileGoalEditActivity.a aVar = ProfileGoalEditActivity.B;
                pq7.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public o(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                WorkoutSettingActivity.a aVar = WorkoutSettingActivity.A;
                pq7.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public p(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ProfileChangePasswordActivity.a aVar = ProfileChangePasswordActivity.B;
                pq7.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 b;

        @DexIgnore
        public q(mz5 mz5) {
            this.b = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                PreferredUnitActivity.a aVar = PreferredUnitActivity.B;
                pq7.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r implements ej1<Drawable> {
        @DexIgnore
        public /* final */ /* synthetic */ FossilCircleImageView b;
        @DexIgnore
        public /* final */ /* synthetic */ Handler c;
        @DexIgnore
        public /* final */ /* synthetic */ mz5 d;
        @DexIgnore
        public /* final */ /* synthetic */ String e;
        @DexIgnore
        public /* final */ /* synthetic */ String f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ r this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mz5$r$a$a")
            /* renamed from: com.fossil.mz5$r$a$a  reason: collision with other inner class name */
            public static final class C0162a extends ko7 implements vp7<iv7, qn7<? super Object>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Bitmap $bitmap;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mz5$r$a$a$a")
                /* renamed from: com.fossil.mz5$r$a$a$a  reason: collision with other inner class name */
                public static final class RunnableC0163a implements Runnable {
                    @DexIgnore
                    public /* final */ /* synthetic */ C0162a b;

                    @DexIgnore
                    public RunnableC0163a(C0162a aVar) {
                        this.b = aVar;
                    }

                    @DexIgnore
                    public final void run() {
                        r rVar = this.b.this$0.this$0;
                        rVar.d.O6(rVar.e);
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0162a(a aVar, Bitmap bitmap, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$bitmap = bitmap;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0162a aVar = new C0162a(this.this$0, this.$bitmap, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super Object> qn7) {
                    return ((C0162a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        Bitmap bitmap = this.$bitmap;
                        if (bitmap == null) {
                            return ao7.a(this.this$0.this$0.c.post(new RunnableC0163a(this)));
                        }
                        this.this$0.this$0.b.setImageBitmap(bitmap);
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(r rVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = rVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    Bitmap e = i37.e(this.this$0.f);
                    jx7 c = bw7.c();
                    C0162a aVar = new C0162a(this, e, null);
                    this.L$0 = iv7;
                    this.L$1 = e;
                    this.label = 1;
                    if (eu7.g(c, aVar, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Bitmap bitmap = (Bitmap) this.L$1;
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ r b;

            @DexIgnore
            public b(r rVar) {
                this.b = rVar;
            }

            @DexIgnore
            public final void run() {
                r rVar = this.b;
                rVar.d.O6(rVar.e);
            }
        }

        @DexIgnore
        public r(FossilCircleImageView fossilCircleImageView, Handler handler, mz5 mz5, String str, String str2, MFUser mFUser, String str3) {
            this.b = fossilCircleImageView;
            this.c = handler;
            this.d = mz5;
            this.e = str2;
            this.f = str3;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean g(Drawable drawable, Object obj, qj1<Drawable> qj1, gb1 gb1, boolean z) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.ej1
        public boolean e(dd1 dd1, Object obj, qj1<Drawable> qj1, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onLoadFailed, error = ");
            sb.append(dd1 != null ? dd1.getMessage() : null);
            local.d("HomeProfileFragment", sb.toString());
            String str = this.f;
            if (!(str == null || vt7.l(str))) {
                xw7 unused = gu7.d(ds0.a(this.d), bw7.a(), null, new a(this, null), 2, null);
            } else {
                this.c.post(new b(this));
            }
            return true;
        }
    }

    @DexIgnore
    @t78(122)
    public final void doCameraTask() {
        if (jn5.c(jn5.b, getActivity(), jn5.a.EDIT_AVATAR, false, false, false, null, 60, null)) {
            zn6 zn6 = this.h;
            if (zn6 != null) {
                zn6.s();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "HomeProfileFragment";
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void E0() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.X(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void G4(ArrayList<bo6.b> arrayList) {
        pq7.c(arrayList, Constants.DEVICES);
        iw5 iw5 = this.k;
        if (iw5 != null) {
            iw5.o(arrayList);
        }
        g37<b85> g37 = this.i;
        if (g37 != null) {
            b85 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (arrayList.isEmpty()) {
                FlexibleTextView flexibleTextView = a2.u0;
                pq7.b(flexibleTextView, "it.tvDevice");
                flexibleTextView.setText(um5.c(PortfolioApp.h0.c(), 2131887074));
                FlexibleButton flexibleButton = a2.H;
                pq7.b(flexibleButton, "it.cvPairFirstWatch");
                flexibleButton.setVisibility(0);
                FlexibleButton flexibleButton2 = a2.I;
                pq7.b(flexibleButton2, "it.fbtAddDevice");
                flexibleButton2.setVisibility(8);
                return;
            }
            FlexibleTextView flexibleTextView2 = a2.u0;
            pq7.b(flexibleTextView2, "it.tvDevice");
            flexibleTextView2.setText(um5.c(PortfolioApp.h0.c(), 2131887176));
            FlexibleButton flexibleButton3 = a2.H;
            pq7.b(flexibleButton3, "it.cvPairFirstWatch");
            flexibleButton3.setVisibility(8);
            FlexibleButton flexibleButton4 = a2.I;
            pq7.b(flexibleButton4, "it.fbtAddDevice");
            flexibleButton4.setVisibility(0);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void H3(boolean z, ps4 ps4, boolean z2) {
        if (z) {
            String c2 = z2 ? um5.c(PortfolioApp.h0.c(), 2131886218) : um5.c(PortfolioApp.h0.c(), 2131886246);
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            pq7.b(c2, "title");
            String c3 = um5.c(PortfolioApp.h0.c(), 2131886242);
            pq7.b(c3, "LanguageHelper.getString\u2026ourCurrentChallengeFirst)");
            s37.o(childFragmentManager, c2, c3, ps4);
        } else if (z2) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
                pq7.b(activity, "it");
                PairingInstructionsActivity.a.b(aVar, activity, false, false, 4, null);
            }
        } else {
            s37 s372 = s37.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            pq7.b(childFragmentManager2, "childFragmentManager");
            s372.P(childFragmentManager2);
        }
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void J4(yn6 yn6) {
        pq7.c(yn6, "activityDailyBest");
        g37<b85> g37 = this.i;
        if (g37 != null) {
            b85 a2 = g37.a();
            if (a2 != null) {
                AutoResizeTextView autoResizeTextView = a2.l0;
                pq7.b(autoResizeTextView, "it.tvAvgActivity");
                autoResizeTextView.setText(dl5.d((int) yn6.b()));
                FlexibleTextView flexibleTextView = a2.m0;
                pq7.b(flexibleTextView, "it.tvAvgActivityDate");
                flexibleTextView.setText(lk5.e(yn6.a()));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.iw5.b
    public void M0(String str) {
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeProfileFragment", "user select " + str);
        WatchSettingActivity.a aVar = WatchSettingActivity.B;
        FragmentActivity requireActivity = requireActivity();
        pq7.b(requireActivity, "requireActivity()");
        aVar.b(requireActivity, str);
    }

    @DexIgnore
    public final zn6 M6() {
        zn6 zn6 = this.h;
        if (zn6 != null) {
            return zn6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    /* renamed from: N6 */
    public void M5(zn6 zn6) {
        pq7.c(zn6, "presenter");
        this.h = zn6;
    }

    @DexIgnore
    public final void O6(String str) {
        FossilCircleImageView fossilCircleImageView;
        g37<b85> g37 = this.i;
        if (g37 != null) {
            b85 a2 = g37.a();
            if (a2 != null && (fossilCircleImageView = a2.c0) != null) {
                wj5 wj5 = this.j;
                if (wj5 != null) {
                    wj5.I(new sj5("", str)).u0(new fj1().o0(new hk5())).F0(fossilCircleImageView);
                    pq7.b(fossilCircleImageView, "it");
                    fossilCircleImageView.setBorderColor(gl0.d(requireContext(), 2131099830));
                    fossilCircleImageView.setBorderWidth(3);
                    fossilCircleImageView.setBackground(gl0.f(requireContext(), 2131231287));
                    return;
                }
                pq7.n("mGlideRequests");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void P6() {
        g37<b85> g37 = this.i;
        if (g37 != null) {
            b85 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.C;
                pq7.b(constraintLayout, "clActiveTime");
                constraintLayout.setVisibility(0);
                fk0 fk0 = new fk0();
                fk0.c(a2.B);
                fk0.e(2131362038, 6, 0, 6);
                fk0.e(2131362038, 7, 2131362033, 6);
                fk0.e(2131362038, 3, 0, 3);
                fk0.e(2131362038, 4, 2131362037, 3);
                fk0.e(2131362033, 6, 2131362038, 7);
                fk0.e(2131362033, 7, 0, 7);
                fk0.e(2131362033, 3, 2131362038, 3);
                fk0.e(2131362033, 4, 2131362038, 4);
                fk0.e(2131362048, 6, 2131362038, 6);
                fk0.e(2131362048, 7, 2131362038, 7);
                fk0.e(2131362048, 3, 2131362038, 4);
                fk0.e(2131362048, 4, 0, 4);
                fk0.e(2131362037, 6, 2131362033, 6);
                fk0.e(2131362037, 7, 2131362033, 7);
                fk0.e(2131362037, 3, 2131362048, 3);
                fk0.e(2131362037, 4, 2131362048, 4);
                fk0.a(a2.B);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void Q2(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeProfileFragment", "hideWorkoutSettings, value = " + z);
        g37<b85> g37 = this.i;
        if (g37 != null) {
            b85 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                RelativeLayout relativeLayout = a2.s;
                pq7.b(relativeLayout, "it.btAutoWorkout");
                relativeLayout.setVisibility(8);
                View view = a2.C0;
                pq7.b(view, "it.vAutoWorkoutSeparatorLine");
                view.setVisibility(8);
                return;
            }
            RelativeLayout relativeLayout2 = a2.s;
            pq7.b(relativeLayout2, "it.btAutoWorkout");
            relativeLayout2.setVisibility(0);
            View view2 = a2.C0;
            pq7.b(view2, "it.vAutoWorkoutSeparatorLine");
            view2.setVisibility(0);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        g37<b85> g37 = this.i;
        if (g37 != null) {
            b85 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.C;
                pq7.b(constraintLayout, "clActiveTime");
                constraintLayout.setVisibility(8);
                fk0 fk0 = new fk0();
                fk0.c(a2.B);
                fk0.e(2131362038, 6, 0, 6);
                fk0.e(2131362038, 7, 2131362048, 6);
                fk0.e(2131362038, 3, 0, 3);
                fk0.e(2131362038, 4, 2131362037, 3);
                fk0.e(2131362048, 6, 2131362038, 7);
                fk0.e(2131362048, 7, 0, 7);
                fk0.e(2131362048, 3, 2131362038, 3);
                fk0.e(2131362048, 4, 2131362038, 4);
                fk0.e(2131362037, 6, 2131362038, 6);
                fk0.e(2131362037, 7, 2131362038, 7);
                fk0.e(2131362037, 3, 2131362038, 4);
                fk0.e(2131362037, 4, 0, 4);
                fk0.a(a2.B);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void R4(MFUser mFUser, String str) {
        FossilCircleImageView fossilCircleImageView;
        pq7.c(mFUser, "user");
        String profilePicture = mFUser.getProfilePicture();
        String str2 = mFUser.getFirstName() + " " + mFUser.getLastName();
        g37<b85> g37 = this.i;
        if (g37 != null) {
            b85 a2 = g37.a();
            if (a2 != null && (fossilCircleImageView = a2.c0) != null) {
                if (TextUtils.isEmpty(profilePicture) || (!URLUtil.isHttpUrl(profilePicture) && !URLUtil.isHttpsUrl(profilePicture))) {
                    O6(str2);
                    return;
                }
                wj5 wj5 = this.j;
                if (wj5 != null) {
                    fossilCircleImageView.j(wj5, profilePicture, str2);
                    Handler handler = new Handler();
                    wj5 wj52 = this.j;
                    if (wj52 != null) {
                        wj52.t(mFUser.getProfilePicture()).b1(new r(fossilCircleImageView, handler, this, profilePicture, str2, mFUser, str)).u0(new fj1().o0(new hk5())).F0(fossilCircleImageView);
                        pq7.b(fossilCircleImageView, "it");
                        fossilCircleImageView.setBorderColor(gl0.d(requireContext(), R.color.transparent));
                        return;
                    }
                    pq7.n("mGlideRequests");
                    throw null;
                }
                pq7.n("mGlideRequests");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        ps4 ps4 = null;
        pq7.c(str, "tag");
        if (!(str.length() == 0) && getActivity() != null) {
            int hashCode = str.hashCode();
            if (hashCode != -292748329) {
                if (hashCode == 1970588827 && str.equals("LEAVE_CHALLENGE")) {
                    if (i2 == 2131363373) {
                        if (intent != null) {
                            ps4 = (ps4) intent.getParcelableExtra("CHALLENGE");
                        }
                        if (ps4 != null) {
                            R6(ps4);
                            return;
                        }
                        return;
                    }
                    return;
                }
            } else if (str.equals("CONFIRM_LOGOUT_ACCOUNT")) {
                if (i2 == 2131363373) {
                    zn6 zn6 = this.h;
                    if (zn6 != null) {
                        zn6.q();
                        return;
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                } else {
                    return;
                }
            }
            super.R5(str, i2, intent);
        }
    }

    @DexIgnore
    public final void R6(ps4 ps4) {
        long b2 = xy4.f4212a.b();
        Date m2 = ps4.m();
        if (b2 > (m2 != null ? m2.getTime() : 0)) {
            BCOverviewLeaderBoardActivity.A.a(this, ps4, null);
        } else {
            BCWaitingChallengeDetailActivity.B.a(this, ps4, "joined_challenge", -1, false);
        }
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void U2(SleepStatistic sleepStatistic) {
        SleepStatistic.SleepDailyBest sleepTimeBestDay;
        g37<b85> g37 = this.i;
        if (g37 != null) {
            b85 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (sleepStatistic == null || (sleepTimeBestDay = sleepStatistic.getSleepTimeBestDay()) == null) {
                FlexibleTextView flexibleTextView = a2.r0;
                pq7.b(flexibleTextView, "it.tvAvgSleep");
                flexibleTextView.setText(um5.c(getContext(), 2131887329));
                FlexibleTextView flexibleTextView2 = a2.s0;
                pq7.b(flexibleTextView2, "it.tvAvgSleepDate");
                flexibleTextView2.setText("");
                return;
            }
            FlexibleTextView flexibleTextView3 = a2.r0;
            pq7.b(flexibleTextView3, "it.tvAvgSleep");
            flexibleTextView3.setText(jl5.b.o(sleepTimeBestDay.getValue()));
            FlexibleTextView flexibleTextView4 = a2.s0;
            pq7.b(flexibleTextView4, "it.tvAvgSleepDate");
            flexibleTextView4.setText(lk5.e(sleepTimeBestDay.getDate()));
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void X3(ActivityStatistic activityStatistic) {
        g37<b85> g37 = this.i;
        if (g37 != null) {
            b85 a2 = g37.a();
            if (a2 != null) {
                if (activityStatistic != null) {
                    ActivityStatistic.ActivityDailyBest stepsBestDay = activityStatistic.getStepsBestDay();
                    ActivityStatistic.ActivityDailyBest activeTimeBestDay = activityStatistic.getActiveTimeBestDay();
                    ActivityStatistic.CaloriesBestDay caloriesBestDay = activityStatistic.getCaloriesBestDay();
                    String c2 = um5.c(getContext(), 2131887328);
                    if (stepsBestDay != null) {
                        AutoResizeTextView autoResizeTextView = a2.l0;
                        pq7.b(autoResizeTextView, "it.tvAvgActivity");
                        autoResizeTextView.setText(dl5.d(stepsBestDay.getValue()));
                        FlexibleTextView flexibleTextView = a2.m0;
                        pq7.b(flexibleTextView, "it.tvAvgActivityDate");
                        flexibleTextView.setText(lk5.e(stepsBestDay.getDate()));
                    } else {
                        AutoResizeTextView autoResizeTextView2 = a2.l0;
                        pq7.b(autoResizeTextView2, "it.tvAvgActivity");
                        autoResizeTextView2.setText(c2);
                        FlexibleTextView flexibleTextView2 = a2.m0;
                        pq7.b(flexibleTextView2, "it.tvAvgActivityDate");
                        flexibleTextView2.setText("");
                    }
                    if (activeTimeBestDay != null) {
                        AutoResizeTextView autoResizeTextView3 = a2.i0;
                        pq7.b(autoResizeTextView3, "it.tvAvgActiveTime");
                        autoResizeTextView3.setText(dl5.d(activeTimeBestDay.getValue()));
                        FlexibleTextView flexibleTextView3 = a2.j0;
                        pq7.b(flexibleTextView3, "it.tvAvgActiveTimeDate");
                        flexibleTextView3.setText(lk5.e(activeTimeBestDay.getDate()));
                    } else {
                        AutoResizeTextView autoResizeTextView4 = a2.i0;
                        pq7.b(autoResizeTextView4, "it.tvAvgActiveTime");
                        autoResizeTextView4.setText(c2);
                        FlexibleTextView flexibleTextView4 = a2.j0;
                        pq7.b(flexibleTextView4, "it.tvAvgActiveTimeDate");
                        flexibleTextView4.setText("");
                    }
                    if (caloriesBestDay != null) {
                        AutoResizeTextView autoResizeTextView5 = a2.o0;
                        pq7.b(autoResizeTextView5, "it.tvAvgCalories");
                        autoResizeTextView5.setText(dl5.d(lr7.a(caloriesBestDay.getValue())));
                        FlexibleTextView flexibleTextView5 = a2.p0;
                        pq7.b(flexibleTextView5, "it.tvAvgCaloriesDate");
                        flexibleTextView5.setText(lk5.e(caloriesBestDay.getDate()));
                    } else {
                        AutoResizeTextView autoResizeTextView6 = a2.o0;
                        pq7.b(autoResizeTextView6, "it.tvAvgCalories");
                        autoResizeTextView6.setText(c2);
                        FlexibleTextView flexibleTextView6 = a2.p0;
                        pq7.b(flexibleTextView6, "it.tvAvgCaloriesDate");
                        flexibleTextView6.setText("");
                    }
                } else {
                    String c3 = um5.c(getContext(), 2131887328);
                    AutoResizeTextView autoResizeTextView7 = a2.l0;
                    pq7.b(autoResizeTextView7, "it.tvAvgActivity");
                    autoResizeTextView7.setText(c3);
                    AutoResizeTextView autoResizeTextView8 = a2.o0;
                    pq7.b(autoResizeTextView8, "it.tvAvgCalories");
                    autoResizeTextView8.setText(c3);
                    AutoResizeTextView autoResizeTextView9 = a2.i0;
                    pq7.b(autoResizeTextView9, "it.tvAvgActiveTime");
                    autoResizeTextView9.setText(c3);
                    FlexibleTextView flexibleTextView7 = a2.m0;
                    pq7.b(flexibleTextView7, "it.tvAvgActivityDate");
                    flexibleTextView7.setText("");
                    FlexibleTextView flexibleTextView8 = a2.p0;
                    pq7.b(flexibleTextView8, "it.tvAvgCaloriesDate");
                    flexibleTextView8.setText("");
                    FlexibleTextView flexibleTextView9 = a2.j0;
                    pq7.b(flexibleTextView9, "it.tvAvgActiveTimeDate");
                    flexibleTextView9.setText("");
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeProfileFragment", "active serial =" + PortfolioApp.h0.c().J());
            if (DeviceIdentityUtils.isDianaDevice(PortfolioApp.h0.c().J())) {
                P6();
            } else {
                Q6();
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void Z3(boolean z, boolean z2) {
        g37<b85> g37 = this.i;
        if (g37 != null) {
            b85 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                if (z2) {
                    FlexibleTextView flexibleTextView = a2.L;
                    pq7.b(flexibleTextView, "it.ftvTitleLowBattery");
                    hr7 hr7 = hr7.f1520a;
                    String c2 = um5.c(PortfolioApp.h0.c(), 2131886837);
                    pq7.b(c2, "LanguageHelper.getString\u2026YourBatteryIsBelowNumber)");
                    String format = String.format(c2, Arrays.copyOf(new Object[]{"10%"}, 1));
                    pq7.b(format, "java.lang.String.format(format, *args)");
                    flexibleTextView.setText(format);
                    FlexibleTextView flexibleTextView2 = a2.K;
                    pq7.b(flexibleTextView2, "it.ftvDescriptionLowBattery");
                    flexibleTextView2.setText(um5.c(PortfolioApp.h0.c(), 2131886836));
                    FlexibleButton flexibleButton = a2.x;
                    pq7.b(flexibleButton, "it.btReplaceBattery");
                    flexibleButton.setVisibility(8);
                } else {
                    FlexibleTextView flexibleTextView3 = a2.L;
                    pq7.b(flexibleTextView3, "it.ftvTitleLowBattery");
                    hr7 hr72 = hr7.f1520a;
                    String c3 = um5.c(PortfolioApp.h0.c(), 2131886847);
                    pq7.b(c3, "LanguageHelper.getString\u2026YourBatteryIsBelowNumber)");
                    String format2 = String.format(c3, Arrays.copyOf(new Object[]{"25%"}, 1));
                    pq7.b(format2, "java.lang.String.format(format, *args)");
                    flexibleTextView3.setText(format2);
                    FlexibleTextView flexibleTextView4 = a2.K;
                    pq7.b(flexibleTextView4, "it.ftvDescriptionLowBattery");
                    flexibleTextView4.setText(um5.c(PortfolioApp.h0.c(), 2131886846));
                    FlexibleButton flexibleButton2 = a2.x;
                    pq7.b(flexibleButton2, "it.btReplaceBattery");
                    flexibleButton2.setVisibility(0);
                }
                if (wr4.f3989a.a().g()) {
                    FlexibleButton flexibleButton3 = a2.x;
                    pq7.b(flexibleButton3, "it.btReplaceBattery");
                    flexibleButton3.setVisibility(8);
                }
                ConstraintLayout constraintLayout = a2.G;
                pq7.b(constraintLayout, "it.clLowBattery");
                constraintLayout.setVisibility(0);
                return;
            }
            ConstraintLayout constraintLayout2 = a2.G;
            pq7.b(constraintLayout2, "it.clLowBattery");
            constraintLayout2.setVisibility(8);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.aw5
    public void b2(boolean z) {
        if (z) {
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
            }
            if (this.i != null) {
                zn6 zn6 = this.h;
                if (zn6 != null) {
                    zn6.p();
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        } else {
            vl5 C62 = C6();
            if (C62 != null) {
                C62.c("");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void i3() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WelcomeActivity.a aVar = WelcomeActivity.B;
            pq7.b(activity, "it");
            aVar.b(activity);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void i5(String str) {
        g37<b85> g37 = this.i;
        if (g37 != null) {
            b85 a2 = g37.a();
            if (a2 != null) {
                if (str == null || str.length() == 0) {
                    FlexibleTextView flexibleTextView = a2.A0;
                    pq7.b(flexibleTextView, "tvSocialId");
                    flexibleTextView.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView2 = a2.A0;
                pq7.b(flexibleTextView2, "tvSocialId");
                flexibleTextView2.setVisibility(0);
                FlexibleTextView flexibleTextView3 = a2.A0;
                pq7.b(flexibleTextView3, "tvSocialId");
                hr7 hr7 = hr7.f1520a;
                String string = getString(2131887311);
                pq7.b(string, "getString(R.string.buddy_challenge_profile_social)");
                String format = String.format(string, Arrays.copyOf(new Object[]{str}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView3.setText(format);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void k() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void m() {
        b();
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void o(int i2, String str) {
        pq7.c(str, "message");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && i2 == 1234) {
            zn6 zn6 = this.h;
            if (zn6 != null) {
                zn6.r(intent);
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        b85 b85 = (b85) aq0.f(layoutInflater, 2131558577, viewGroup, false, A6());
        wj5 c2 = tj5.c(this);
        pq7.b(c2, "GlideApp.with(this)");
        this.j = c2;
        ArrayList arrayList = new ArrayList();
        wj5 wj5 = this.j;
        if (wj5 != null) {
            this.k = new iw5(arrayList, wj5, this, PortfolioApp.h0.c());
            g37<b85> g37 = new g37<>(this, b85);
            this.i = g37;
            if (g37 != null) {
                b85 a2 = g37.a();
                if (a2 != null) {
                    pq7.b(a2, "mBinding.get()!!");
                    return a2.n();
                }
                pq7.i();
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mGlideRequests");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        zn6 zn6 = this.h;
        if (zn6 != null) {
            if (zn6 != null) {
                zn6.m();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
        vl5 C6 = C6();
        if (C6 != null) {
            C6.c("");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        zn6 zn6 = this.h;
        if (zn6 != null) {
            if (zn6 != null) {
                zn6.l();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
        vl5 C6 = C6();
        if (C6 != null) {
            C6.i();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        String d2;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<b85> g37 = this.i;
        if (g37 != null) {
            b85 a2 = g37.a();
            if (a2 != null) {
                String d3 = qn5.l.a().d("onPrimaryButton");
                if (!TextUtils.isEmpty(d3)) {
                    int parseColor = Color.parseColor(d3);
                    Drawable drawable = PortfolioApp.h0.c().getDrawable(2131230985);
                    if (drawable != null) {
                        drawable.setTint(parseColor);
                        a2.I.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
                FossilCircleImageView fossilCircleImageView = a2.c0;
                pq7.b(fossilCircleImageView, "binding.ivUserAvatar");
                nl5.a(fossilCircleImageView, new i(this));
                a2.w0.setOnClickListener(new j(this));
                a2.W.setOnClickListener(new k(this));
                a2.I.setOnClickListener(new l(this));
                a2.H.setOnClickListener(new m(this));
                a2.y.setOnClickListener(new n(this));
                a2.s.setOnClickListener(new o(this));
                a2.t.setOnClickListener(new p(this));
                a2.A.setOnClickListener(new q(this));
                a2.v.setOnClickListener(new b(this));
                a2.r.setOnClickListener(new c(this));
                a2.u.setOnClickListener(new d(this));
                a2.w.setOnClickListener(new e(this));
                a2.x.setOnClickListener(new f(this));
                a2.z.setOnClickListener(new g(this));
                ConstraintLayout constraintLayout = a2.B;
                if (constraintLayout != null) {
                    String d4 = qn5.l.a().d("nonBrandSurface");
                    if (!TextUtils.isEmpty(d4)) {
                        constraintLayout.setBackgroundColor(Color.parseColor(d4));
                    }
                }
                ConstraintLayout constraintLayout2 = a2.G;
                if (!(constraintLayout2 == null || (d2 = qn5.l.a().d("nonBrandSurface")) == null)) {
                    constraintLayout2.setBackgroundColor(Color.parseColor(d2));
                }
                a2.J.setOnClickListener(new h(this));
                zn6 zn6 = this.h;
                if (zn6 != null) {
                    nk5.a aVar = nk5.o;
                    if (zn6 == null) {
                        pq7.n("mPresenter");
                        throw null;
                    } else if (aVar.w(zn6.o())) {
                        String d5 = qn5.l.a().d("dianaActiveMinutesTab");
                        String d6 = qn5.l.a().d("dianaStepsTab");
                        String d7 = qn5.l.a().d("dianaActiveCaloriesTab");
                        String d8 = qn5.l.a().d("dianaSleepTab");
                        if (!TextUtils.isEmpty(d5)) {
                            a2.O.setColorFilter(Color.parseColor(d5));
                        }
                        if (!TextUtils.isEmpty(d8)) {
                            a2.P.setColorFilter(Color.parseColor(d8));
                        }
                        if (!TextUtils.isEmpty(d6)) {
                            a2.Q.setColorFilter(Color.parseColor(d6));
                        }
                        if (!TextUtils.isEmpty(d7)) {
                            a2.R.setColorFilter(Color.parseColor(d7));
                        }
                        RecyclerView recyclerView = a2.h0;
                        pq7.b(recyclerView, "binding.rvDevices");
                        recyclerView.setAdapter(this.k);
                        RecyclerView recyclerView2 = a2.h0;
                        pq7.b(recyclerView2, "binding.rvDevices");
                        recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                        RecyclerView recyclerView3 = a2.h0;
                        pq7.b(recyclerView3, "binding.rvDevices");
                        recyclerView3.setNestedScrollingEnabled(false);
                    }
                }
                String d9 = qn5.l.a().d("hybridStepsTab");
                String d10 = qn5.l.a().d("hybridActiveCaloriesTab");
                String d11 = qn5.l.a().d("hybridSleepTab");
                if (!TextUtils.isEmpty(d11)) {
                    a2.P.setColorFilter(Color.parseColor(d11));
                }
                if (!TextUtils.isEmpty(d9)) {
                    a2.Q.setColorFilter(Color.parseColor(d9));
                }
                if (!TextUtils.isEmpty(d10)) {
                    a2.R.setColorFilter(Color.parseColor(d10));
                }
                RecyclerView recyclerView4 = a2.h0;
                pq7.b(recyclerView4, "binding.rvDevices");
                recyclerView4.setAdapter(this.k);
                RecyclerView recyclerView22 = a2.h0;
                pq7.b(recyclerView22, "binding.rvDevices");
                recyclerView22.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView32 = a2.h0;
                pq7.b(recyclerView32, "binding.rvDevices");
                recyclerView32.setNestedScrollingEnabled(false);
            }
            E6("profile_view");
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void updateUser(MFUser mFUser) {
        String str;
        String format;
        String str2 = null;
        pq7.c(mFUser, "user");
        FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "updateUser");
        if (isActive()) {
            g37<b85> g37 = this.i;
            if (g37 != null) {
                b85 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.B0;
                    pq7.b(flexibleTextView, "it.tvUserName");
                    flexibleTextView.setText(mFUser.getFirstName() + " " + mFUser.getLastName());
                    if (!TextUtils.isEmpty(mFUser.getRegisterDate())) {
                        try {
                            String s = lk5.s(lk5.M(mFUser.getRegisterDate()).toDate());
                            FlexibleTextView flexibleTextView2 = a2.x0;
                            pq7.b(flexibleTextView2, "it.tvMemberSince");
                            hr7 hr7 = hr7.f1520a;
                            String c2 = um5.c(PortfolioApp.h0.c(), 2131887174);
                            pq7.b(c2, "LanguageHelper.getString\u2026_Text__JoinedInMonthYear)");
                            String format2 = String.format(c2, Arrays.copyOf(new Object[]{s}, 1));
                            pq7.b(format2, "java.lang.String.format(format, *args)");
                            flexibleTextView2.setText(format2);
                        } catch (Exception e2) {
                            FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "Exception when parse register date. " + e2);
                        }
                    }
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "useDefaultBiometric = " + mFUser.getUseDefaultBiometric());
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "weightInGrams = " + mFUser.getWeightInGrams());
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "heightInCentimeters = " + mFUser.getHeightInCentimeters());
                    String string = PortfolioApp.h0.c().getString(2131887328);
                    pq7.b(string, "PortfolioApp.instance.ge\u2026ng.character_dash_double)");
                    MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
                    if (pq7.a(unitGroup != null ? unitGroup.getWeight() : null, ai5.IMPERIAL.getValue())) {
                        if (mFUser.getWeightInGrams() == 0 || mFUser.getUseDefaultBiometric()) {
                            hr7 hr72 = hr7.f1520a;
                            String c3 = um5.c(PortfolioApp.h0.c(), 2131886956);
                            pq7.b(c3, "LanguageHelper.getString\u2026alInformation_Label__Lbs)");
                            if (c3 != null) {
                                String lowerCase = c3.toLowerCase();
                                pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                                String format3 = String.format("%s %s", Arrays.copyOf(new Object[]{string, lowerCase}, 2));
                                pq7.b(format3, "java.lang.String.format(format, *args)");
                                str = format3;
                            } else {
                                throw new il7("null cannot be cast to non-null type java.lang.String");
                            }
                        } else {
                            hr7 hr73 = hr7.f1520a;
                            String str3 = dl5.b(jk5.h((float) (mFUser.getWeightInGrams() + 1)), 1).toString();
                            String c4 = um5.c(PortfolioApp.h0.c(), 2131886956);
                            pq7.b(c4, "LanguageHelper.getString\u2026alInformation_Label__Lbs)");
                            if (c4 != null) {
                                String lowerCase2 = c4.toLowerCase();
                                pq7.b(lowerCase2, "(this as java.lang.String).toLowerCase()");
                                String format4 = String.format("%s %s", Arrays.copyOf(new Object[]{str3, lowerCase2}, 2));
                                pq7.b(format4, "java.lang.String.format(format, *args)");
                                str = format4;
                            } else {
                                throw new il7("null cannot be cast to non-null type java.lang.String");
                            }
                        }
                    } else if (mFUser.getWeightInGrams() == 0 || mFUser.getUseDefaultBiometric()) {
                        hr7 hr74 = hr7.f1520a;
                        String c5 = um5.c(PortfolioApp.h0.c(), 2131886955);
                        pq7.b(c5, "LanguageHelper.getString\u2026nalInformation_Label__Kg)");
                        if (c5 != null) {
                            String lowerCase3 = c5.toLowerCase();
                            pq7.b(lowerCase3, "(this as java.lang.String).toLowerCase()");
                            String format5 = String.format("%s %s", Arrays.copyOf(new Object[]{string, lowerCase3}, 2));
                            pq7.b(format5, "java.lang.String.format(format, *args)");
                            str = format5;
                        } else {
                            throw new il7("null cannot be cast to non-null type java.lang.String");
                        }
                    } else {
                        hr7 hr75 = hr7.f1520a;
                        String str4 = dl5.b(jk5.g((float) (mFUser.getWeightInGrams() + 1)), 1).toString();
                        String c6 = um5.c(PortfolioApp.h0.c(), 2131886955);
                        pq7.b(c6, "LanguageHelper.getString\u2026nalInformation_Label__Kg)");
                        if (c6 != null) {
                            String lowerCase4 = c6.toLowerCase();
                            pq7.b(lowerCase4, "(this as java.lang.String).toLowerCase()");
                            String format6 = String.format("%s %s", Arrays.copyOf(new Object[]{str4, lowerCase4}, 2));
                            pq7.b(format6, "java.lang.String.format(format, *args)");
                            str = format6;
                        } else {
                            throw new il7("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    String string2 = PortfolioApp.h0.c().getString(2131887328);
                    pq7.b(string2, "PortfolioApp.instance.ge\u2026ng.character_dash_double)");
                    MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                    if (unitGroup2 != null) {
                        str2 = unitGroup2.getHeight();
                    }
                    if (pq7.a(str2, ai5.IMPERIAL.getValue())) {
                        if (mFUser.getHeightInCentimeters() == 0 || mFUser.getUseDefaultBiometric()) {
                            hr7 hr76 = hr7.f1520a;
                            String c7 = um5.c(PortfolioApp.h0.c(), 2131887571);
                            pq7.b(c7, "LanguageHelper.getString\u2026_height_single_character)");
                            format = String.format(c7, Arrays.copyOf(new Object[]{string2}, 1));
                            pq7.b(format, "java.lang.String.format(format, *args)");
                        } else {
                            cl7<Integer, Integer> b2 = jk5.b((float) mFUser.getHeightInCentimeters());
                            hr7 hr77 = hr7.f1520a;
                            String c8 = um5.c(PortfolioApp.h0.c(), 2131887570);
                            pq7.b(c8, "LanguageHelper.getString\u2026_height_double_character)");
                            format = String.format(c8, Arrays.copyOf(new Object[]{String.valueOf(b2.getFirst().intValue()), String.valueOf(b2.getSecond().intValue())}, 2));
                            pq7.b(format, "java.lang.String.format(format, *args)");
                        }
                    } else if (mFUser.getHeightInCentimeters() == 0 || mFUser.getUseDefaultBiometric()) {
                        hr7 hr78 = hr7.f1520a;
                        String c9 = um5.c(PortfolioApp.h0.c(), 2131886953);
                        pq7.b(c9, "LanguageHelper.getString\u2026nalInformation_Label__Cm)");
                        if (c9 != null) {
                            String lowerCase5 = c9.toLowerCase();
                            pq7.b(lowerCase5, "(this as java.lang.String).toLowerCase()");
                            format = String.format("%s %s", Arrays.copyOf(new Object[]{string2, lowerCase5}, 2));
                            pq7.b(format, "java.lang.String.format(format, *args)");
                        } else {
                            throw new il7("null cannot be cast to non-null type java.lang.String");
                        }
                    } else {
                        hr7 hr79 = hr7.f1520a;
                        int heightInCentimeters = mFUser.getHeightInCentimeters();
                        String c10 = um5.c(PortfolioApp.h0.c(), 2131886953);
                        pq7.b(c10, "LanguageHelper.getString\u2026nalInformation_Label__Cm)");
                        if (c10 != null) {
                            String lowerCase6 = c10.toLowerCase();
                            pq7.b(lowerCase6, "(this as java.lang.String).toLowerCase()");
                            format = String.format("%s %s", Arrays.copyOf(new Object[]{String.valueOf(heightInCentimeters), lowerCase6}, 2));
                            pq7.b(format, "java.lang.String.format(format, *args)");
                        } else {
                            throw new il7("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    FlexibleTextView flexibleTextView3 = a2.v0;
                    pq7.b(flexibleTextView3, "it.tvHeightWeight");
                    flexibleTextView3.setText(str + ", " + format);
                    if (pq7.a(mFUser.getAuthType(), kh5.EMAIL.getValue())) {
                        RelativeLayout relativeLayout = a2.t;
                        pq7.b(relativeLayout, "it.btChangePassword");
                        relativeLayout.setVisibility(0);
                        View view = a2.D0;
                        pq7.b(view, "it.vChangePasswordSeparatorLine");
                        view.setVisibility(0);
                        return;
                    }
                    RelativeLayout relativeLayout2 = a2.t;
                    pq7.b(relativeLayout2, "it.btChangePassword");
                    relativeLayout2.setVisibility(8);
                    View view2 = a2.D0;
                    pq7.b(view2, "it.vChangePasswordSeparatorLine");
                    view2.setVisibility(8);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.ao6
    public void z2() {
        iw5 iw5 = this.k;
        if (iw5 != null) {
            iw5.p();
        }
    }
}
