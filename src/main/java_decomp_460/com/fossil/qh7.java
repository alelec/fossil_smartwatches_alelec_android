package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qh7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public long f2983a;
    @DexIgnore
    public String b;

    @DexIgnore
    public qh7(long j, String str, int i, int i2) {
        this.f2983a = j;
        this.b = str;
    }

    @DexIgnore
    public String toString() {
        return this.b;
    }
}
