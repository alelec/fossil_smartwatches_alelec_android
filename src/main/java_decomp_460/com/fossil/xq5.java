package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.FossilNotificationListenerService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xq5 implements MembersInjector<FossilNotificationListenerService> {
    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, no4 no4) {
        fossilNotificationListenerService.l = no4;
    }

    @DexIgnore
    public static void b(FossilNotificationListenerService fossilNotificationListenerService, vr5 vr5) {
        fossilNotificationListenerService.j = vr5;
    }

    @DexIgnore
    public static void c(FossilNotificationListenerService fossilNotificationListenerService, yr5 yr5) {
        fossilNotificationListenerService.k = yr5;
    }

    @DexIgnore
    public static void d(FossilNotificationListenerService fossilNotificationListenerService, or5 or5) {
        fossilNotificationListenerService.i = or5;
    }

    @DexIgnore
    public static void e(FossilNotificationListenerService fossilNotificationListenerService, on5 on5) {
        fossilNotificationListenerService.m = on5;
    }

    @DexIgnore
    public static void f(FossilNotificationListenerService fossilNotificationListenerService, UserRepository userRepository) {
        fossilNotificationListenerService.s = userRepository;
    }
}
