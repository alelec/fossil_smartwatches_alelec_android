package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i4 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ k5 b;
    @DexIgnore
    public /* final */ /* synthetic */ g7 c;
    @DexIgnore
    public /* final */ /* synthetic */ b5 d;
    @DexIgnore
    public /* final */ /* synthetic */ b5 e;

    @DexIgnore
    public i4(k5 k5Var, g7 g7Var, b5 b5Var, b5 b5Var2) {
        this.b = k5Var;
        this.c = g7Var;
        this.d = b5Var;
        this.e = b5Var2;
    }

    @DexIgnore
    public final void run() {
        this.b.z.k.f();
        this.b.z.k.c(new y6(this.c, this.d, this.e));
    }
}
