package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pt3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Executor f2867a; // = new a();
    @DexIgnore
    public static /* final */ Executor b; // = new ku3();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Executor {
        @DexIgnore
        public /* final */ Handler b; // = new aa3(Looper.getMainLooper());

        @DexIgnore
        public final void execute(Runnable runnable) {
            this.b.post(runnable);
        }
    }
}
