package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface k48 extends c58, ReadableByteChannel {
    @DexIgnore
    boolean H(long j, l48 l48) throws IOException;

    @DexIgnore
    String I(Charset charset) throws IOException;

    @DexIgnore
    boolean R(long j) throws IOException;

    @DexIgnore
    String U() throws IOException;

    @DexIgnore
    byte[] W(long j) throws IOException;

    @DexIgnore
    i48 d();

    @DexIgnore
    long e0(a58 a58) throws IOException;

    @DexIgnore
    l48 i(long j) throws IOException;

    @DexIgnore
    void j0(long j) throws IOException;

    @DexIgnore
    long m0() throws IOException;

    @DexIgnore
    InputStream n0();

    @DexIgnore
    k48 peek();

    @DexIgnore
    byte[] r() throws IOException;

    @DexIgnore
    byte readByte() throws IOException;

    @DexIgnore
    void readFully(byte[] bArr) throws IOException;

    @DexIgnore
    int readInt() throws IOException;

    @DexIgnore
    short readShort() throws IOException;

    @DexIgnore
    void skip(long j) throws IOException;

    @DexIgnore
    i48 t();

    @DexIgnore
    boolean u() throws IOException;

    @DexIgnore
    long y() throws IOException;

    @DexIgnore
    String z(long j) throws IOException;
}
