package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class at6 implements Factory<zs6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ThemeRepository> f323a;

    @DexIgnore
    public at6(Provider<ThemeRepository> provider) {
        this.f323a = provider;
    }

    @DexIgnore
    public static at6 a(Provider<ThemeRepository> provider) {
        return new at6(provider);
    }

    @DexIgnore
    public static zs6 c(ThemeRepository themeRepository) {
        return new zs6(themeRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public zs6 get() {
        return c(this.f323a.get());
    }
}
