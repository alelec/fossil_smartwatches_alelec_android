package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ne3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ os2 f2508a;

    @DexIgnore
    public ne3(os2 os2) {
        rc2.k(os2);
        this.f2508a = os2;
    }

    @DexIgnore
    public final String a() {
        try {
            return this.f2508a.getId();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void b() {
        try {
            this.f2508a.remove();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void c(boolean z) {
        try {
            this.f2508a.b(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void d(int i) {
        try {
            this.f2508a.setFillColor(i);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void e(boolean z) {
        try {
            this.f2508a.setGeodesic(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof ne3)) {
            return false;
        }
        try {
            return this.f2508a.L2(((ne3) obj).f2508a);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void f(List<LatLng> list) {
        try {
            this.f2508a.setPoints(list);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void g(int i) {
        try {
            this.f2508a.setStrokeColor(i);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void h(float f) {
        try {
            this.f2508a.setStrokeWidth(f);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return this.f2508a.a();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void i(boolean z) {
        try {
            this.f2508a.setVisible(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void j(float f) {
        try {
            this.f2508a.setZIndex(f);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }
}
