package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleButton;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class br4 extends RecyclerView.g<RecyclerView.ViewHolder> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f488a; // = qn5.l.a().d("onPrimaryButton");
    @DexIgnore
    public ArrayList<m66> b;
    @DexIgnore
    public c c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public TextView b;
        @DexIgnore
        public CustomizeWidget c;
        @DexIgnore
        public CustomizeWidget d;
        @DexIgnore
        public CustomizeWidget e;
        @DexIgnore
        public ImageView f;
        @DexIgnore
        public TextView g;
        @DexIgnore
        public View h;
        @DexIgnore
        public FlexibleButton i;
        @DexIgnore
        public ImageView j;
        @DexIgnore
        public m66 k;
        @DexIgnore
        public View l;
        @DexIgnore
        public View m;
        @DexIgnore
        public View s;
        @DexIgnore
        public View t;
        @DexIgnore
        public /* final */ /* synthetic */ br4 u;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(br4 br4, View view) {
            super(view);
            pq7.c(view, "view");
            this.u = br4;
            View findViewById = view.findViewById(2131363376);
            pq7.b(findViewById, "view.findViewById(R.id.tv_preset_name)");
            TextView textView = (TextView) findViewById;
            this.b = textView;
            textView.setOnClickListener(this);
            View findViewById2 = view.findViewById(2131363531);
            pq7.b(findViewById2, "view.findViewById(R.id.wa_top)");
            this.c = (CustomizeWidget) findViewById2;
            View findViewById3 = view.findViewById(2131363530);
            pq7.b(findViewById3, "view.findViewById(R.id.wa_middle)");
            this.d = (CustomizeWidget) findViewById3;
            View findViewById4 = view.findViewById(2131363529);
            pq7.b(findViewById4, "view.findViewById(R.id.wa_bottom)");
            this.e = (CustomizeWidget) findViewById4;
            View findViewById5 = view.findViewById(2131362770);
            pq7.b(findViewById5, "view.findViewById(R.id.iv_watch_theme_background)");
            this.j = (ImageView) findViewById5;
            View findViewById6 = view.findViewById(2131363471);
            pq7.b(findViewById6, "view.findViewById(R.id.v_underline)");
            this.t = findViewById6;
            View findViewById7 = view.findViewById(2131362789);
            pq7.b(findViewById7, "view.findViewById(R.id.line_bottom)");
            this.s = findViewById7;
            View findViewById8 = view.findViewById(2131362790);
            pq7.b(findViewById8, "view.findViewById(R.id.line_center)");
            this.m = findViewById8;
            View findViewById9 = view.findViewById(2131362791);
            pq7.b(findViewById9, "view.findViewById(R.id.line_top)");
            this.l = findViewById9;
            String d2 = qn5.l.a().d("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(d2)) {
                int parseColor = Color.parseColor(d2);
                this.t.setBackgroundColor(parseColor);
                this.s.setBackgroundColor(parseColor);
                this.m.setBackgroundColor(parseColor);
                this.l.setBackgroundColor(parseColor);
            }
            View findViewById10 = view.findViewById(2131362767);
            pq7.b(findViewById10, "view.findViewById(R.id.iv_warning)");
            this.f = (ImageView) findViewById10;
            View findViewById11 = view.findViewById(2131362780);
            pq7.b(findViewById11, "view.findViewById(R.id.layout_right)");
            this.h = findViewById11;
            View findViewById12 = view.findViewById(2131361978);
            pq7.b(findViewById12, "view.findViewById(R.id.btn_right)");
            this.i = (FlexibleButton) findViewById12;
            View findViewById13 = view.findViewById(2131363348);
            pq7.b(findViewById13, "view.findViewById(R.id.tv_left)");
            this.g = (TextView) findViewById13;
            this.h.setOnClickListener(this);
            this.g.setOnClickListener(this);
            this.i.setOnClickListener(this);
            this.c.setOnClickListener(this);
            this.d.setOnClickListener(this);
            this.e.setOnClickListener(this);
            this.f.setOnClickListener(this);
            if (FossilDeviceSerialPatternUtil.isSamDevice(PortfolioApp.h0.c().J())) {
                this.j.setImageResource(2131230975);
            }
        }

        @DexIgnore
        public final void a(m66 m66, boolean z) {
            pq7.c(m66, "preset");
            if (z) {
                jn5 jn5 = jn5.b;
                c cVar = this.u.c;
                if (cVar != null) {
                    if (jn5.g(jn5, ((gz5) cVar).getContext(), m66.b(), false, false, false, null, 56, null)) {
                        FLogger.INSTANCE.getLocal().d("HybridCustomizePresetDetailAdapter", "warning gone");
                        this.f.setVisibility(8);
                    } else {
                        FLogger.INSTANCE.getLocal().d("HybridCustomizePresetDetailAdapter", "warning visible");
                        this.f.setVisibility(0);
                    }
                    this.h.setSelected(true);
                    this.i.setText(um5.c(PortfolioApp.h0.c(), 2131886550));
                    this.i.setClickable(false);
                    this.i.d("flexible_button_right_applied");
                    if (!TextUtils.isEmpty(this.u.i())) {
                        int parseColor = Color.parseColor(this.u.i());
                        Drawable drawable = PortfolioApp.h0.c().getDrawable(2131231055);
                        if (drawable != null) {
                            drawable.setTint(parseColor);
                            this.i.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                        }
                    }
                    this.h.setClickable(false);
                    if (this.u.b.size() == 2) {
                        this.g.setVisibility(4);
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
                }
            } else {
                this.h.setSelected(false);
                this.i.setText(um5.c(PortfolioApp.h0.c(), 2131886542));
                this.i.setClickable(true);
                this.i.d("flexible_button_right_apply");
                this.i.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                this.h.setClickable(true);
                this.g.setVisibility(0);
            }
            this.k = m66;
            this.b.setText(m66.d());
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(m66.a());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizePresetDetailAdapter", "build with microApps=" + arrayList);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                n66 n66 = (n66) it.next();
                String c2 = n66.c();
                if (c2 != null) {
                    int hashCode = c2.hashCode();
                    if (hashCode != -1383228885) {
                        if (hashCode != -1074341483) {
                            if (hashCode == 115029 && c2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                this.c.S(n66.a());
                                this.c.T();
                            }
                        } else if (c2.equals("middle")) {
                            this.d.S(n66.a());
                            this.d.T();
                        }
                    } else if (c2.equals("bottom")) {
                        this.e.S(n66.a());
                        this.e.T();
                    }
                }
            }
        }

        @DexIgnore
        public final List<ln0<View, String>> b() {
            TextView textView = this.b;
            ln0 ln0 = new ln0(textView, textView.getTransitionName());
            View view = this.t;
            ln0 ln02 = new ln0(view, view.getTransitionName());
            ImageView imageView = this.j;
            if (imageView != null) {
                ln0 ln03 = new ln0(imageView, imageView.getTransitionName());
                View view2 = this.s;
                ln0 ln04 = new ln0(view2, view2.getTransitionName());
                View view3 = this.m;
                ln0 ln05 = new ln0(view3, view3.getTransitionName());
                View view4 = this.l;
                return hm7.h(ln0, ln02, ln03, ln04, ln05, new ln0(view4, view4.getTransitionName()));
            }
            throw new il7("null cannot be cast to non-null type android.view.View");
        }

        @DexIgnore
        public final List<ln0<CustomizeWidget, String>> c() {
            CustomizeWidget customizeWidget = this.c;
            ln0 ln0 = new ln0(customizeWidget, customizeWidget.getTransitionName());
            CustomizeWidget customizeWidget2 = this.d;
            ln0 ln02 = new ln0(customizeWidget2, customizeWidget2.getTransitionName());
            CustomizeWidget customizeWidget3 = this.e;
            return hm7.h(ln0, ln02, new ln0(customizeWidget3, customizeWidget3.getTransitionName()));
        }

        @DexIgnore
        @SuppressLint({"UseSparseArrays"})
        public void onClick(View view) {
            Object obj;
            String str;
            String str2;
            FLogger.INSTANCE.getLocal().d("HybridCustomizePresetDetailAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if ((getAdapterPosition() != -1 || this.k == null) && view != null) {
                switch (view.getId()) {
                    case 2131361978:
                        this.u.c.Y3();
                        return;
                    case 2131362767:
                        Iterator it = this.u.b.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                Object next = it.next();
                                if (((m66) next).e()) {
                                    obj = next;
                                }
                            } else {
                                obj = null;
                            }
                        }
                        m66 m66 = (m66) obj;
                        if (m66 != null) {
                            jn5 jn5 = jn5.b;
                            c cVar = this.u.c;
                            if (cVar != null) {
                                jn5.g(jn5, ((gz5) cVar).getContext(), m66.b(), false, false, false, null, 60, null);
                                return;
                            }
                            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
                        }
                        return;
                    case 2131363348:
                        if (this.u.b.size() > 2) {
                            Object obj2 = this.u.b.get(1);
                            pq7.b(obj2, "mData[1]");
                            m66 m662 = (m66) obj2;
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            StringBuilder sb = new StringBuilder();
                            sb.append("current ");
                            m66 m663 = this.k;
                            sb.append(m663 != null ? m663.d() : null);
                            sb.append(" nextPreset ");
                            sb.append(m662.d());
                            local.d("HybridCustomizePresetDetailAdapter", sb.toString());
                            c cVar2 = this.u.c;
                            m66 m664 = this.k;
                            if (m664 != null) {
                                boolean e2 = m664.e();
                                m66 m665 = this.k;
                                if (m665 != null) {
                                    cVar2.r0(e2, m665.d(), m662.d(), m662.c());
                                    return;
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            return;
                        }
                    case 2131363376:
                        c cVar3 = this.u.c;
                        m66 m666 = this.k;
                        if (m666 == null || (str = m666.d()) == null) {
                            str = "";
                        }
                        m66 m667 = this.k;
                        if (m667 == null || (str2 = m667.c()) == null) {
                            str2 = "";
                        }
                        cVar3.N(str, str2);
                        return;
                    case 2131363529:
                        this.u.c.r4(this.k, b(), c(), "bottom", getAdapterPosition());
                        return;
                    case 2131363530:
                        this.u.c.r4(this.k, b(), c(), "middle", getAdapterPosition());
                        return;
                    case 2131363531:
                        this.u.c.r4(this.k, b(), c(), ViewHierarchy.DIMENSION_TOP_KEY, getAdapterPosition());
                        return;
                    default:
                        return;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public FlexibleButton b;
        @DexIgnore
        public TextView c;
        @DexIgnore
        public /* final */ /* synthetic */ br4 d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(br4 br4, View view) {
            super(view);
            pq7.c(view, "view");
            this.d = br4;
            View findViewById = view.findViewById(2131361966);
            pq7.b(findViewById, "view.findViewById(R.id.btn_add)");
            this.b = (FlexibleButton) findViewById;
            View findViewById2 = view.findViewById(2131363301);
            pq7.b(findViewById2, "view.findViewById(R.id.tv_create_new_preset)");
            this.c = (TextView) findViewById2;
            if (!TextUtils.isEmpty(br4.i())) {
                int parseColor = Color.parseColor(br4.i());
                Drawable drawable = PortfolioApp.h0.c().getDrawable(2131230985);
                if (drawable != null) {
                    drawable.setTint(parseColor);
                    this.b.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                }
            }
            nl5.a(this.b, this);
        }

        @DexIgnore
        public final void a(boolean z) {
            if (z) {
                this.b.d("flexible_button_disabled");
                this.b.setClickable(false);
                this.c.setText(PortfolioApp.h0.c().getString(2131886554));
                return;
            }
            this.b.d("flexible_button_primary");
            this.b.setClickable(true);
            this.c.setText(PortfolioApp.h0.c().getString(2131886541));
        }

        @DexIgnore
        public void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizePresetDetailAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if (view != null && view.getId() == 2131361966) {
                this.d.c.c4();
            }
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void N(String str, String str2);

        @DexIgnore
        Object Y3();  // void declaration

        @DexIgnore
        Object c4();  // void declaration

        @DexIgnore
        void r0(boolean z, String str, String str2, String str3);

        @DexIgnore
        void r4(m66 m66, List<? extends ln0<View, String>> list, List<? extends ln0<CustomizeWidget, String>> list2, String str, int i);
    }

    @DexIgnore
    public br4(ArrayList<m66> arrayList, c cVar) {
        pq7.c(arrayList, "mData");
        pq7.c(cVar, "mListener");
        this.b = arrayList;
        this.c = cVar;
        setHasStableIds(true);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        return (long) this.b.get(i).hashCode();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        if (i == this.b.size() - 1) {
            return 0;
        }
        return i == 0 ? 2 : 1;
    }

    @DexIgnore
    public final String i() {
        return this.f488a;
    }

    @DexIgnore
    public final void j(ArrayList<m66> arrayList) {
        pq7.c(arrayList, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HybridCustomizePresetDetailAdapter", "setData - data=" + arrayList);
        this.b.clear();
        this.b.addAll(arrayList);
        this.b.add(new m66("", "", new ArrayList(), new ArrayList(), false));
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        boolean z = true;
        pq7.c(viewHolder, "holder");
        FLogger.INSTANCE.getLocal().d("HybridCustomizePresetDetailAdapter", "onBindViewHolder pos=" + i);
        if (viewHolder instanceof a) {
            a aVar = (a) viewHolder;
            m66 m66 = this.b.get(i);
            pq7.b(m66, "mData[position]");
            m66 m662 = m66;
            if (i != 0) {
                z = false;
            }
            aVar.a(m662, z);
        } else if (viewHolder instanceof b) {
            b bVar = (b) viewHolder;
            if (this.b.size() <= 10) {
                z = false;
            }
            bVar.a(z);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        if (i == 1 || i == 2) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558700, viewGroup, false);
            pq7.b(inflate, "LayoutInflater.from(pare\u2026et_detail, parent, false)");
            return new a(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558674, viewGroup, false);
        pq7.b(inflate2, "LayoutInflater.from(pare\u2026reset_add, parent, false)");
        return new b(this, inflate2);
    }
}
