package com.fossil;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vv7<T> extends yv7<T> implements do7, qn7<T> {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater j; // = AtomicReferenceFieldUpdater.newUpdater(vv7.class, Object.class, "_reusableCancellableContinuation");
    @DexIgnore
    public volatile Object _reusableCancellableContinuation;
    @DexIgnore
    public Object e; // = wv7.f4003a;
    @DexIgnore
    public /* final */ do7 f;
    @DexIgnore
    public /* final */ Object g;
    @DexIgnore
    public /* final */ dv7 h;
    @DexIgnore
    public /* final */ qn7<T> i;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.qn7<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public vv7(dv7 dv7, qn7<? super T> qn7) {
        super(0);
        this.h = dv7;
        this.i = qn7;
        qn7<T> qn72 = this.i;
        this.f = (do7) (!(qn72 instanceof do7) ? null : qn72);
        this.g = zz7.b(getContext());
        this._reusableCancellableContinuation = null;
    }

    @DexIgnore
    @Override // com.fossil.yv7
    public qn7<T> c() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.do7
    public do7 getCallerFrame() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public tn7 getContext() {
        return this.i.getContext();
    }

    @DexIgnore
    @Override // com.fossil.do7
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.yv7
    public Object j() {
        Object obj = this.e;
        if (nv7.a()) {
            if (!(obj != wv7.f4003a)) {
                throw new AssertionError();
            }
        }
        this.e = wv7.f4003a;
        return obj;
    }

    @DexIgnore
    public final Throwable k(ku7<?> ku7) {
        vz7 vz7;
        do {
            Object obj = this._reusableCancellableContinuation;
            vz7 = wv7.b;
            if (obj != vz7) {
                if (obj == null) {
                    return null;
                }
                if (!(obj instanceof Throwable)) {
                    throw new IllegalStateException(("Inconsistent state " + obj).toString());
                } else if (j.compareAndSet(this, obj, null)) {
                    return (Throwable) obj;
                } else {
                    throw new IllegalArgumentException("Failed requirement.".toString());
                }
            }
        } while (!j.compareAndSet(this, vz7, ku7));
        return null;
    }

    @DexIgnore
    public final lu7<T> l() {
        Object obj;
        do {
            obj = this._reusableCancellableContinuation;
            if (obj == null) {
                this._reusableCancellableContinuation = wv7.b;
                return null;
            } else if (!(obj instanceof lu7)) {
                throw new IllegalStateException(("Inconsistent state " + obj).toString());
            }
        } while (!j.compareAndSet(this, obj, wv7.b));
        return (lu7) obj;
    }

    @DexIgnore
    public final void m(tn7 tn7, T t) {
        this.e = t;
        this.d = 1;
        this.h.P(tn7, this);
    }

    @DexIgnore
    public final lu7<?> n() {
        Object obj = this._reusableCancellableContinuation;
        if (!(obj instanceof lu7)) {
            obj = null;
        }
        return (lu7) obj;
    }

    @DexIgnore
    public final boolean o() {
        return this._reusableCancellableContinuation != null;
    }

    @DexIgnore
    public final boolean p(Throwable th) {
        while (true) {
            Object obj = this._reusableCancellableContinuation;
            if (pq7.a(obj, wv7.b)) {
                if (j.compareAndSet(this, wv7.b, th)) {
                    return true;
                }
            } else if (obj instanceof Throwable) {
                return true;
            } else {
                if (j.compareAndSet(this, obj, null)) {
                    return false;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public void resumeWith(Object obj) {
        tn7 context = this.i.getContext();
        Object b = wu7.b(obj);
        if (this.h.Q(context)) {
            this.e = b;
            this.d = 0;
            this.h.M(context, this);
            return;
        }
        hw7 b2 = wx7.b.b();
        if (b2.o0()) {
            this.e = b;
            this.d = 0;
            b2.X(this);
            return;
        }
        b2.g0(true);
        try {
            tn7 context2 = getContext();
            Object c = zz7.c(context2, this.g);
            try {
                this.i.resumeWith(obj);
                tl7 tl7 = tl7.f3441a;
                do {
                } while (b2.r0());
            } finally {
                zz7.a(context2, c);
            }
        } catch (Throwable th) {
            b2.S(true);
            throw th;
        }
        b2.S(true);
    }

    @DexIgnore
    public String toString() {
        return "DispatchedContinuation[" + this.h + ", " + ov7.c(this.i) + ']';
    }
}
