package com.fossil;

import android.os.Build;
import android.util.Base64;
import com.fossil.iq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.nio.charset.Charset;
import java.security.KeyPair;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s27 extends iq4<a, c, b> {
    @DexIgnore
    public /* final */ on5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f3200a;
        @DexIgnore
        public /* final */ wh5 b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public a(String str, wh5 wh5, String str2) {
            pq7.c(str, "aliasName");
            pq7.c(wh5, "scope");
            pq7.c(str2, "value");
            this.f3200a = str;
            this.b = wh5;
            this.c = str2;
        }

        @DexIgnore
        public final String a() {
            return this.f3200a;
        }

        @DexIgnore
        public final wh5 b() {
            return this.b;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {
        @DexIgnore
        public b(int i, String str) {
            pq7.c(str, "errorMessage");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
    }

    @DexIgnore
    public s27(on5 on5) {
        pq7.c(on5, "mSharedPreferencesManager");
        this.d = on5;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "EncryptValueKeyStoreUseCase";
    }

    @DexIgnore
    /* renamed from: m */
    public Object k(a aVar, qn7<Object> qn7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("Start encrypt ");
        sb.append(aVar != null ? aVar.c() : null);
        sb.append(" with alias ");
        sb.append(aVar != null ? aVar.a() : null);
        local.d("EncryptValueKeyStoreUseCase", sb.toString());
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                z37 z37 = z37.b;
                if (aVar != null) {
                    SecretKey e = z37.e(aVar.a());
                    Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                    instance.init(1, e);
                    String c2 = aVar.c();
                    Charset charset = et7.f986a;
                    if (c2 != null) {
                        byte[] bytes = c2.getBytes(charset);
                        pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
                        byte[] doFinal = instance.doFinal(bytes);
                        pq7.b(instance, "cipher");
                        String encodeToString = Base64.encodeToString(instance.getIV(), 0);
                        String encodeToString2 = Base64.encodeToString(doFinal, 0);
                        this.d.c1(aVar.a(), aVar.b(), encodeToString);
                        this.d.d1(aVar.a(), aVar.b(), encodeToString2);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("EncryptValueKeyStoreUseCase", "Encryption done iv " + encodeToString + " value " + encodeToString2);
                    } else {
                        throw new il7("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                z37 z372 = z37.b;
                if (aVar != null) {
                    KeyPair d2 = z372.d(aVar.a());
                    if (d2 == null) {
                        d2 = z37.b.b(aVar.a());
                    }
                    Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                    instance2.init(1, d2.getPublic());
                    String c3 = aVar.c();
                    Charset charset2 = et7.f986a;
                    if (c3 != null) {
                        byte[] bytes2 = c3.getBytes(charset2);
                        pq7.b(bytes2, "(this as java.lang.String).getBytes(charset)");
                        String encodeToString3 = Base64.encodeToString(instance2.doFinal(bytes2), 0);
                        this.d.d1(aVar.a(), aVar.b(), encodeToString3);
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        local3.d("EncryptValueKeyStoreUseCase", "Encryption done value " + encodeToString3);
                    } else {
                        throw new il7("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            j(new c());
        } catch (Exception e2) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.e("EncryptValueKeyStoreUseCase", "Exception when encrypt values " + e2);
            i(new b(600, ""));
        }
        return new Object();
    }
}
