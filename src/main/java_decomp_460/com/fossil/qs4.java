package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface qs4 {
    @DexIgnore
    List<ms4> a();

    @DexIgnore
    int b(String str);

    @DexIgnore
    LiveData<ps4> c(String[] strArr, Date date);

    @DexIgnore
    ks4 d(String str);

    @DexIgnore
    bt4 e(String str);

    @DexIgnore
    ps4 f(String[] strArr, Date date);

    @DexIgnore
    Object g();  // void declaration

    @DexIgnore
    long h(ls4 ls4);

    @DexIgnore
    LiveData<List<bt4>> i();

    @DexIgnore
    Long[] insert(List<ps4> list);

    @DexIgnore
    List<ls4> j();

    @DexIgnore
    long k(ps4 ps4);

    @DexIgnore
    LiveData<bt4> l(String str);

    @DexIgnore
    ps4 m(String str);

    @DexIgnore
    int n(String str);

    @DexIgnore
    Object o();  // void declaration

    @DexIgnore
    Object p();  // void declaration

    @DexIgnore
    Object q();  // void declaration

    @DexIgnore
    int r(bt4 bt4);

    @DexIgnore
    LiveData<ps4> s(String str);

    @DexIgnore
    Long[] t(List<bt4> list);

    @DexIgnore
    void u(String[] strArr);

    @DexIgnore
    Long[] v(List<ms4> list);

    @DexIgnore
    Object w();  // void declaration

    @DexIgnore
    Object x();  // void declaration

    @DexIgnore
    Long[] y(List<ks4> list);
}
