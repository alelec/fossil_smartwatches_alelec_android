package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c86 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ s76 f582a;

    @DexIgnore
    public c86(s76 s76) {
        pq7.c(s76, "mCommuteTimeSettingsContractView");
        this.f582a = s76;
    }

    @DexIgnore
    public final s76 a() {
        return this.f582a;
    }
}
