package com.fossil;

import com.fossil.tj1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sj1<R> implements tj1<R> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ sj1<?> f3268a; // = new sj1<>();
    @DexIgnore
    public static /* final */ uj1<?> b; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<R> implements uj1<R> {
        @DexIgnore
        @Override // com.fossil.uj1
        public tj1<R> a(gb1 gb1, boolean z) {
            return sj1.f3268a;
        }
    }

    @DexIgnore
    public static <R> uj1<R> b() {
        return (uj1<R>) b;
    }

    @DexIgnore
    @Override // com.fossil.tj1
    public boolean a(Object obj, tj1.a aVar) {
        return false;
    }
}
