package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ho extends qq7 implements vp7<fs, Float, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ af b;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] c;
    @DexIgnore
    public /* final */ /* synthetic */ long d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ho(af afVar, byte[] bArr, long j) {
        super(2);
        this.b = afVar;
        this.c = bArr;
        this.d = j;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public tl7 invoke(fs fsVar, Float f) {
        af afVar = this.b;
        long floatValue = ((long) (f.floatValue() * ((float) this.c.length))) + this.d;
        afVar.E = floatValue;
        float f2 = (((float) floatValue) * 1.0f) / ((float) afVar.D);
        if (Math.abs(f2 - afVar.F) > this.b.Q || f2 == 1.0f) {
            af afVar2 = this.b;
            afVar2.F = f2;
            afVar2.d(f2);
        }
        return tl7.f3441a;
    }
}
