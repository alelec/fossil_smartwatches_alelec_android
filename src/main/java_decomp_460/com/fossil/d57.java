package com.fossil;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d57 extends kq0 {
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public HashMap c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final d57 a(String str) {
            pq7.c(str, "title");
            d57 d57 = new d57();
            Bundle bundle = new Bundle();
            bundle.putString("DIALOG_TITLE", str);
            d57.setArguments(bundle);
            return d57;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProgressDialogFragment", "create with arguments " + getArguments());
        Bundle arguments = getArguments();
        if (arguments != null) {
            String string = arguments.getString("DIALOG_TITLE");
            if (string == null) {
                string = "";
            }
            this.b = string;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        return layoutInflater.inflate(2131558582, viewGroup, false);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Window window;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(2131363410);
        ViewGroup viewGroup = (ViewGroup) view.findViewById(2131363010);
        String d2 = qn5.l.a().d("nonBrandSurface");
        String d3 = qn5.l.a().d("primaryColor");
        FlexibleProgressBar flexibleProgressBar = (FlexibleProgressBar) view.findViewById(2131362970);
        if (d3 != null) {
            pq7.b(flexibleProgressBar, "progressBar");
            flexibleProgressBar.getIndeterminateDrawable().setTint(Color.parseColor(d3));
        }
        if (d2 != null) {
            viewGroup.setBackgroundColor(Color.parseColor(d2));
        }
        if (TextUtils.isEmpty(this.b)) {
            pq7.b(flexibleTextView, "mTvTitle");
            flexibleTextView.setVisibility(8);
        } else {
            pq7.b(flexibleTextView, "mTvTitle");
            flexibleTextView.setVisibility(0);
            flexibleTextView.setText(this.b);
        }
        Dialog dialog = getDialog();
        if (dialog != null && (window = dialog.getWindow()) != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
    }

    @DexIgnore
    public void v6() {
        HashMap hashMap = this.c;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
