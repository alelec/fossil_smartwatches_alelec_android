package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f86 implements Factory<e86> {
    @DexIgnore
    public static e86 a(s76 s76, on5 on5, UserRepository userRepository) {
        return new e86(s76, on5, userRepository);
    }
}
