package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f21 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public boolean f1038a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public f21(boolean z, boolean z2, boolean z3, boolean z4) {
        this.f1038a = z;
        this.b = z2;
        this.c = z3;
        this.d = z4;
    }

    @DexIgnore
    public boolean a() {
        return this.f1038a;
    }

    @DexIgnore
    public boolean b() {
        return this.c;
    }

    @DexIgnore
    public boolean c() {
        return this.d;
    }

    @DexIgnore
    public boolean d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f21)) {
            return false;
        }
        f21 f21 = (f21) obj;
        return this.f1038a == f21.f1038a && this.b == f21.b && this.c == f21.c && this.d == f21.d;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.f1038a ? 1 : 0;
        if (this.b) {
            i += 16;
        }
        if (this.c) {
            i += 256;
        }
        return this.d ? i + 4096 : i;
    }

    @DexIgnore
    public String toString() {
        return String.format("[ Connected=%b Validated=%b Metered=%b NotRoaming=%b ]", Boolean.valueOf(this.f1038a), Boolean.valueOf(this.b), Boolean.valueOf(this.c), Boolean.valueOf(this.d));
    }
}
