package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uv extends tv {
    @DexIgnore
    public byte[] L; // = new byte[0];
    @DexIgnore
    public byte M; // = ((byte) -1);
    @DexIgnore
    public boolean N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public n6 P; // = n6.FTD;

    @DexIgnore
    public uv(sv svVar, short s, hs hsVar, k5 k5Var, int i) {
        super(svVar, s, hsVar, k5Var, i);
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final JSONObject F(byte[] bArr) {
        JSONObject jSONObject = new JSONObject();
        if (!this.N) {
            this.N = true;
            this.E = this.v.d != lw.b;
            byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put((byte) 8).array();
            pq7.b(array, "ByteBuffer.allocate(1)\n \u2026                 .array()");
            Q(array);
        } else {
            this.E = true;
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final boolean G(o7 o7Var) {
        return o7Var.f2638a == this.P;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final void I(o7 o7Var) {
        if (!this.N) {
            super.I(o7Var);
            return;
        }
        byte[] bArr = o7Var.b;
        this.E = true;
        this.g.add(new hw(0, o7Var.f2638a, bArr, new JSONObject(), 1));
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final void J(o7 o7Var) {
        byte[] b = this.s ? jx.b.b(this.y.x, this.P, o7Var.b) : o7Var.b;
        int n = hy1.n(hy1.p((byte) (b[0] & 63)));
        int n2 = hy1.n(hy1.p((byte) (((byte) (this.M + 1)) & 63)));
        if (n == n2) {
            n(this.p);
            this.M = (byte) ((byte) n2);
            this.L = dy1.a(this.L, dm7.k(b, 1, b.length));
            boolean z = ((byte) (b[0] & Byte.MIN_VALUE)) != ((byte) 0);
            this.O = z;
            if (z) {
                S(this.L);
                T();
                return;
            }
            return;
        }
        this.v = mw.a(this.v, null, null, lw.h, null, null, 27);
        this.E = true;
    }

    @DexIgnore
    public abstract void S(byte[] bArr);

    @DexIgnore
    public void T() {
    }
}
