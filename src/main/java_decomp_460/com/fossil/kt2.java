package com.fossil;

import android.os.RemoteException;
import com.fossil.zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kt2 extends zs2.a {
    @DexIgnore
    public /* final */ /* synthetic */ r93 f;
    @DexIgnore
    public /* final */ /* synthetic */ zs2 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public kt2(zs2 zs2, r93 r93) {
        super(zs2);
        this.g = zs2;
        this.f = r93;
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void a() throws RemoteException {
        this.g.h.getGmpAppId(this.f);
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void b() {
        this.f.c(null);
    }
}
