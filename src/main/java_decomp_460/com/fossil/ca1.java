package com.fossil;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ca1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f587a;
    @DexIgnore
    public /* final */ List<f91> b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ InputStream d;

    @DexIgnore
    public ca1(int i, List<f91> list) {
        this(i, list, -1, null);
    }

    @DexIgnore
    public ca1(int i, List<f91> list, int i2, InputStream inputStream) {
        this.f587a = i;
        this.b = list;
        this.c = i2;
        this.d = inputStream;
    }

    @DexIgnore
    public final InputStream a() {
        return this.d;
    }

    @DexIgnore
    public final int b() {
        return this.c;
    }

    @DexIgnore
    public final List<f91> c() {
        return Collections.unmodifiableList(this.b);
    }

    @DexIgnore
    public final int d() {
        return this.f587a;
    }
}
