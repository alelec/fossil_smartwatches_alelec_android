package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pg extends zj {
    @DexIgnore
    public /* final */ iw1 T;

    @DexIgnore
    public pg(k5 k5Var, i60 i60, iw1 iw1) {
        super(k5Var, i60, yp.y0, true, ke.b.b(k5Var.x, ob.UI_PACKAGE_FILE), iw1.getData(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.T = iw1;
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.ro, com.fossil.mj
    public JSONObject C() {
        return g80.k(super.C(), jd0.J4, this.T.toJSONObject());
    }
}
