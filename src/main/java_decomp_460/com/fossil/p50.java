package com.fossil;

import com.fossil.yk1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p50 implements d5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ e60 f2778a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public p50(e60 e60) {
        this.f2778a = e60;
    }

    @DexIgnore
    @Override // com.fossil.d5
    public void a(k5 k5Var, b5 b5Var, b5 b5Var2) {
        if (pq7.a(k5Var, this.f2778a.d)) {
            this.f2778a.r0(yk1.d.g.a(b5Var), yk1.d.g.a(b5Var2));
        }
    }

    @DexIgnore
    @Override // com.fossil.d5
    public void b(k5 k5Var, f5 f5Var) {
        lp lpVar;
        if (l10.f2132a[f5Var.ordinal()] == 1) {
            this.f2778a.v0(false);
        }
        if (e60.w0(this.f2778a)) {
            this.f2778a.E0();
        }
        lp[] f = this.f2778a.g.f();
        int length = f.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                lpVar = null;
                break;
            }
            lpVar = f[i];
            yp ypVar = lpVar.y;
            if (ypVar == yp.h || ypVar == yp.c || ypVar == yp.i) {
                break;
            }
            i++;
        }
        if (lpVar == null) {
            int i2 = l10.b[f5Var.ordinal()];
            if (i2 == 1) {
                this.f2778a.q0(yk1.c.DISCONNECTED);
            } else if (i2 == 2) {
                this.f2778a.q0(yk1.c.CONNECTING);
            } else if (i2 == 4) {
                this.f2778a.q0(yk1.c.DISCONNECTING);
            }
        }
    }
}
