package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tz7<T> extends au7<T> implements do7 {
    @DexIgnore
    public /* final */ qn7<T> e;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.qn7<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public tz7(tn7 tn7, qn7<? super T> qn7) {
        super(tn7, true);
        this.e = qn7;
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public final boolean V() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.do7
    public final do7 getCallerFrame() {
        return (do7) this.e;
    }

    @DexIgnore
    @Override // com.fossil.do7
    public final StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public void p(Object obj) {
        wv7.b(xn7.c(this.e), wu7.a(obj, this.e));
    }

    @DexIgnore
    @Override // com.fossil.au7
    public void u0(Object obj) {
        qn7<T> qn7 = this.e;
        qn7.resumeWith(wu7.a(obj, qn7));
    }
}
