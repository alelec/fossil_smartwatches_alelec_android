package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ei2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ double f944a;
    @DexIgnore
    public /* final */ double b;

    @DexIgnore
    public ei2(double d, double d2) {
        this.f944a = d;
        this.b = d2;
    }

    @DexIgnore
    public final boolean a(double d) {
        return d >= this.f944a && d <= this.b;
    }
}
