package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b98 implements e88<w18, Double> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ b98 f407a; // = new b98();

    @DexIgnore
    /* renamed from: b */
    public Double a(w18 w18) throws IOException {
        return Double.valueOf(w18.string());
    }
}
