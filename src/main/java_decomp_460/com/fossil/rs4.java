package com.fossil;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.ShareConstants;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rs4 implements qs4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f3151a;
    @DexIgnore
    public /* final */ jw0<ps4> b;
    @DexIgnore
    public /* final */ oz4 c; // = new oz4();
    @DexIgnore
    public /* final */ jw0<ls4> d;
    @DexIgnore
    public /* final */ jw0<ks4> e;
    @DexIgnore
    public /* final */ ns4 f; // = new ns4();
    @DexIgnore
    public /* final */ jw0<bt4> g;
    @DexIgnore
    public /* final */ ct4 h; // = new ct4();
    @DexIgnore
    public /* final */ jw0<ms4> i;
    @DexIgnore
    public /* final */ iw0<bt4> j;
    @DexIgnore
    public /* final */ xw0 k;
    @DexIgnore
    public /* final */ xw0 l;
    @DexIgnore
    public /* final */ xw0 m;
    @DexIgnore
    public /* final */ xw0 n;
    @DexIgnore
    public /* final */ xw0 o;
    @DexIgnore
    public /* final */ xw0 p;
    @DexIgnore
    public /* final */ xw0 q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends xw0 {
        @DexIgnore
        public a(rs4 rs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM fitness";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends xw0 {
        @DexIgnore
        public b(rs4 rs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM display_player";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends xw0 {
        @DexIgnore
        public c(rs4 rs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM challenge_player";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends xw0 {
        @DexIgnore
        public d(rs4 rs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM history_challenge";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends xw0 {
        @DexIgnore
        public e(rs4 rs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM players";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements Callable<ps4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tw0 f3152a;

        @DexIgnore
        public f(tw0 tw0) {
            this.f3152a = tw0;
        }

        @DexIgnore
        /* renamed from: a */
        public ps4 call() throws Exception {
            ps4 ps4;
            Cursor b2 = ex0.b(rs4.this.f3151a, this.f3152a, false, null);
            try {
                int c = dx0.c(b2, "id");
                int c2 = dx0.c(b2, "type");
                int c3 = dx0.c(b2, "name");
                int c4 = dx0.c(b2, "des");
                int c5 = dx0.c(b2, "owner");
                int c6 = dx0.c(b2, "numberOfPlayers");
                int c7 = dx0.c(b2, SampleRaw.COLUMN_START_TIME);
                int c8 = dx0.c(b2, SampleRaw.COLUMN_END_TIME);
                int c9 = dx0.c(b2, "target");
                int c10 = dx0.c(b2, "duration");
                int c11 = dx0.c(b2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
                int c12 = dx0.c(b2, "version");
                int c13 = dx0.c(b2, "status");
                int c14 = dx0.c(b2, "syncData");
                int c15 = dx0.c(b2, "createdAt");
                int c16 = dx0.c(b2, "updatedAt");
                int c17 = dx0.c(b2, "category");
                if (b2.moveToFirst()) {
                    ps4 = new ps4(b2.getString(c), b2.getString(c2), b2.getString(c3), b2.getString(c4), rs4.this.c.c(b2.getString(c5)), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), rs4.this.c.d(b2.getString(c7)), rs4.this.c.d(b2.getString(c8)), b2.isNull(c9) ? null : Integer.valueOf(b2.getInt(c9)), b2.isNull(c10) ? null : Integer.valueOf(b2.getInt(c10)), b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getString(c14), rs4.this.c.d(b2.getString(c15)), rs4.this.c.d(b2.getString(c16)), b2.getString(c17));
                } else {
                    ps4 = null;
                }
                return ps4;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.f3152a.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements Callable<ps4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tw0 f3153a;

        @DexIgnore
        public g(tw0 tw0) {
            this.f3153a = tw0;
        }

        @DexIgnore
        /* renamed from: a */
        public ps4 call() throws Exception {
            ps4 ps4;
            Cursor b2 = ex0.b(rs4.this.f3151a, this.f3153a, false, null);
            try {
                int c = dx0.c(b2, "id");
                int c2 = dx0.c(b2, "type");
                int c3 = dx0.c(b2, "name");
                int c4 = dx0.c(b2, "des");
                int c5 = dx0.c(b2, "owner");
                int c6 = dx0.c(b2, "numberOfPlayers");
                int c7 = dx0.c(b2, SampleRaw.COLUMN_START_TIME);
                int c8 = dx0.c(b2, SampleRaw.COLUMN_END_TIME);
                int c9 = dx0.c(b2, "target");
                int c10 = dx0.c(b2, "duration");
                int c11 = dx0.c(b2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
                int c12 = dx0.c(b2, "version");
                int c13 = dx0.c(b2, "status");
                int c14 = dx0.c(b2, "syncData");
                int c15 = dx0.c(b2, "createdAt");
                int c16 = dx0.c(b2, "updatedAt");
                int c17 = dx0.c(b2, "category");
                if (b2.moveToFirst()) {
                    ps4 = new ps4(b2.getString(c), b2.getString(c2), b2.getString(c3), b2.getString(c4), rs4.this.c.c(b2.getString(c5)), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), rs4.this.c.d(b2.getString(c7)), rs4.this.c.d(b2.getString(c8)), b2.isNull(c9) ? null : Integer.valueOf(b2.getInt(c9)), b2.isNull(c10) ? null : Integer.valueOf(b2.getInt(c10)), b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getString(c14), rs4.this.c.d(b2.getString(c15)), rs4.this.c.d(b2.getString(c16)), b2.getString(c17));
                } else {
                    ps4 = null;
                }
                return ps4;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.f3153a.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends jw0<ps4> {
        @DexIgnore
        public h(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, ps4 ps4) {
            if (ps4.f() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, ps4.f());
            }
            if (ps4.r() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, ps4.r());
            }
            if (ps4.g() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, ps4.g());
            }
            if (ps4.c() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, ps4.c());
            }
            String b = rs4.this.c.b(ps4.i());
            if (b == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, b);
            }
            if (ps4.h() == null) {
                px0.bindNull(6);
            } else {
                px0.bindLong(6, (long) ps4.h().intValue());
            }
            String a2 = rs4.this.c.a(ps4.m());
            if (a2 == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, a2);
            }
            String a3 = rs4.this.c.a(ps4.e());
            if (a3 == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, a3);
            }
            if (ps4.q() == null) {
                px0.bindNull(9);
            } else {
                px0.bindLong(9, (long) ps4.q().intValue());
            }
            if (ps4.d() == null) {
                px0.bindNull(10);
            } else {
                px0.bindLong(10, (long) ps4.d().intValue());
            }
            if (ps4.k() == null) {
                px0.bindNull(11);
            } else {
                px0.bindString(11, ps4.k());
            }
            if (ps4.t() == null) {
                px0.bindNull(12);
            } else {
                px0.bindString(12, ps4.t());
            }
            if (ps4.n() == null) {
                px0.bindNull(13);
            } else {
                px0.bindString(13, ps4.n());
            }
            if (ps4.p() == null) {
                px0.bindNull(14);
            } else {
                px0.bindString(14, ps4.p());
            }
            String a4 = rs4.this.c.a(ps4.b());
            if (a4 == null) {
                px0.bindNull(15);
            } else {
                px0.bindString(15, a4);
            }
            String a5 = rs4.this.c.a(ps4.s());
            if (a5 == null) {
                px0.bindNull(16);
            } else {
                px0.bindString(16, a5);
            }
            if (ps4.a() == null) {
                px0.bindNull(17);
            } else {
                px0.bindString(17, ps4.a());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `challenge` (`id`,`type`,`name`,`des`,`owner`,`numberOfPlayers`,`startTime`,`endTime`,`target`,`duration`,`privacy`,`version`,`status`,`syncData`,`createdAt`,`updatedAt`,`category`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements Callable<List<bt4>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tw0 f3155a;

        @DexIgnore
        public i(tw0 tw0) {
            this.f3155a = tw0;
        }

        @DexIgnore
        /* renamed from: a */
        public List<bt4> call() throws Exception {
            Cursor b2 = ex0.b(rs4.this.f3151a, this.f3155a, false, null);
            try {
                int c = dx0.c(b2, "id");
                int c2 = dx0.c(b2, "type");
                int c3 = dx0.c(b2, "name");
                int c4 = dx0.c(b2, "des");
                int c5 = dx0.c(b2, "owner");
                int c6 = dx0.c(b2, "numberOfPlayers");
                int c7 = dx0.c(b2, SampleRaw.COLUMN_START_TIME);
                int c8 = dx0.c(b2, SampleRaw.COLUMN_END_TIME);
                int c9 = dx0.c(b2, "target");
                int c10 = dx0.c(b2, "duration");
                int c11 = dx0.c(b2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
                int c12 = dx0.c(b2, "version");
                int c13 = dx0.c(b2, "status");
                int c14 = dx0.c(b2, "players");
                int c15 = dx0.c(b2, "topPlayer");
                int c16 = dx0.c(b2, "currentPlayer");
                int c17 = dx0.c(b2, "createdAt");
                int c18 = dx0.c(b2, "updatedAt");
                int c19 = dx0.c(b2, "completedAt");
                int c20 = dx0.c(b2, "isFirst");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    arrayList.add(new bt4(b2.getString(c), b2.getString(c2), b2.getString(c3), b2.getString(c4), rs4.this.h.d(b2.getString(c5)), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), rs4.this.c.d(b2.getString(c7)), rs4.this.c.d(b2.getString(c8)), b2.isNull(c9) ? null : Integer.valueOf(b2.getInt(c9)), b2.isNull(c10) ? null : Integer.valueOf(b2.getInt(c10)), b2.getString(c11), b2.getString(c12), b2.getString(c13), rs4.this.h.f(b2.getString(c14)), rs4.this.h.e(b2.getString(c15)), rs4.this.h.e(b2.getString(c16)), rs4.this.c.d(b2.getString(c17)), rs4.this.c.d(b2.getString(c18)), rs4.this.c.d(b2.getString(c19)), b2.getInt(c20) != 0));
                }
                return arrayList;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.f3155a.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements Callable<bt4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tw0 f3156a;

        @DexIgnore
        public j(tw0 tw0) {
            this.f3156a = tw0;
        }

        @DexIgnore
        /* renamed from: a */
        public bt4 call() throws Exception {
            bt4 bt4;
            Cursor b2 = ex0.b(rs4.this.f3151a, this.f3156a, false, null);
            try {
                int c = dx0.c(b2, "id");
                int c2 = dx0.c(b2, "type");
                int c3 = dx0.c(b2, "name");
                int c4 = dx0.c(b2, "des");
                int c5 = dx0.c(b2, "owner");
                int c6 = dx0.c(b2, "numberOfPlayers");
                int c7 = dx0.c(b2, SampleRaw.COLUMN_START_TIME);
                int c8 = dx0.c(b2, SampleRaw.COLUMN_END_TIME);
                int c9 = dx0.c(b2, "target");
                int c10 = dx0.c(b2, "duration");
                int c11 = dx0.c(b2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
                int c12 = dx0.c(b2, "version");
                int c13 = dx0.c(b2, "status");
                int c14 = dx0.c(b2, "players");
                int c15 = dx0.c(b2, "topPlayer");
                int c16 = dx0.c(b2, "currentPlayer");
                int c17 = dx0.c(b2, "createdAt");
                int c18 = dx0.c(b2, "updatedAt");
                int c19 = dx0.c(b2, "completedAt");
                int c20 = dx0.c(b2, "isFirst");
                if (b2.moveToFirst()) {
                    bt4 = new bt4(b2.getString(c), b2.getString(c2), b2.getString(c3), b2.getString(c4), rs4.this.h.d(b2.getString(c5)), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), rs4.this.c.d(b2.getString(c7)), rs4.this.c.d(b2.getString(c8)), b2.isNull(c9) ? null : Integer.valueOf(b2.getInt(c9)), b2.isNull(c10) ? null : Integer.valueOf(b2.getInt(c10)), b2.getString(c11), b2.getString(c12), b2.getString(c13), rs4.this.h.f(b2.getString(c14)), rs4.this.h.e(b2.getString(c15)), rs4.this.h.e(b2.getString(c16)), rs4.this.c.d(b2.getString(c17)), rs4.this.c.d(b2.getString(c18)), rs4.this.c.d(b2.getString(c19)), b2.getInt(c20) != 0);
                } else {
                    bt4 = null;
                }
                return bt4;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.f3156a.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends jw0<ls4> {
        @DexIgnore
        public k(rs4 rs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, ls4 ls4) {
            px0.bindLong(1, (long) ls4.e());
            px0.bindLong(2, (long) ls4.c());
            px0.bindLong(3, (long) ls4.a());
            if (ls4.b() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, ls4.b());
            }
            px0.bindLong(5, (long) ls4.d());
            if (ls4.f() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, ls4.f());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `fitness` (`pinType`,`id`,`encryptMethod`,`encryptedData`,`keyType`,`sequence`) VALUES (?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l extends jw0<ks4> {
        @DexIgnore
        public l(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, ks4 ks4) {
            if (ks4.a() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, ks4.a());
            }
            if (ks4.c() == null) {
                px0.bindNull(2);
            } else {
                px0.bindLong(2, (long) ks4.c().intValue());
            }
            String a2 = rs4.this.f.a(ks4.d());
            if (a2 == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, a2);
            }
            String a3 = rs4.this.f.a(ks4.b());
            if (a3 == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, a3);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `display_player` (`challengeId`,`numberOfPlayers`,`topPlayers`,`nearPlayers`) VALUES (?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class m extends jw0<js4> {
        @DexIgnore
        public m(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, js4 js4) {
            if (js4.a() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, js4.a());
            }
            if (js4.b() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, js4.b());
            }
            String a2 = rs4.this.f.a(js4.c());
            if (a2 == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, a2);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `challenge_player` (`challengeId`,`challengeName`,`players`) VALUES (?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class n extends jw0<bt4> {
        @DexIgnore
        public n(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, bt4 bt4) {
            if (bt4.g() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, bt4.g());
            }
            if (bt4.q() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, bt4.q());
            }
            if (bt4.h() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, bt4.h());
            }
            if (bt4.d() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, bt4.d());
            }
            String a2 = rs4.this.h.a(bt4.j());
            if (a2 == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, a2);
            }
            if (bt4.i() == null) {
                px0.bindNull(6);
            } else {
                px0.bindLong(6, (long) bt4.i().intValue());
            }
            String a3 = rs4.this.c.a(bt4.m());
            if (a3 == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, a3);
            }
            String a4 = rs4.this.c.a(bt4.f());
            if (a4 == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, a4);
            }
            if (bt4.o() == null) {
                px0.bindNull(9);
            } else {
                px0.bindLong(9, (long) bt4.o().intValue());
            }
            if (bt4.e() == null) {
                px0.bindNull(10);
            } else {
                px0.bindLong(10, (long) bt4.e().intValue());
            }
            if (bt4.l() == null) {
                px0.bindNull(11);
            } else {
                px0.bindString(11, bt4.l());
            }
            if (bt4.s() == null) {
                px0.bindNull(12);
            } else {
                px0.bindString(12, bt4.s());
            }
            if (bt4.n() == null) {
                px0.bindNull(13);
            } else {
                px0.bindString(13, bt4.n());
            }
            String c = rs4.this.h.c(bt4.k());
            if (c == null) {
                px0.bindNull(14);
            } else {
                px0.bindString(14, c);
            }
            String b = rs4.this.h.b(bt4.p());
            if (b == null) {
                px0.bindNull(15);
            } else {
                px0.bindString(15, b);
            }
            String b2 = rs4.this.h.b(bt4.c());
            if (b2 == null) {
                px0.bindNull(16);
            } else {
                px0.bindString(16, b2);
            }
            String a5 = rs4.this.c.a(bt4.b());
            if (a5 == null) {
                px0.bindNull(17);
            } else {
                px0.bindString(17, a5);
            }
            String a6 = rs4.this.c.a(bt4.r());
            if (a6 == null) {
                px0.bindNull(18);
            } else {
                px0.bindString(18, a6);
            }
            String a7 = rs4.this.c.a(bt4.a());
            if (a7 == null) {
                px0.bindNull(19);
            } else {
                px0.bindString(19, a7);
            }
            px0.bindLong(20, bt4.t() ? 1 : 0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `history_challenge` (`id`,`type`,`name`,`des`,`owner`,`numberOfPlayers`,`startTime`,`endTime`,`target`,`duration`,`privacy`,`version`,`status`,`players`,`topPlayer`,`currentPlayer`,`createdAt`,`updatedAt`,`completedAt`,`isFirst`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class o extends jw0<ms4> {
        @DexIgnore
        public o(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, ms4 ms4) {
            if (ms4.d() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, ms4.d());
            }
            if (ms4.i() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, ms4.i());
            }
            if (ms4.c() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, ms4.c());
            }
            if (ms4.e() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, ms4.e());
            }
            Integer valueOf = ms4.p() == null ? null : Integer.valueOf(ms4.p().booleanValue() ? 1 : 0);
            if (valueOf == null) {
                px0.bindNull(5);
            } else {
                px0.bindLong(5, (long) valueOf.intValue());
            }
            if (ms4.h() == null) {
                px0.bindNull(6);
            } else {
                px0.bindLong(6, (long) ms4.h().intValue());
            }
            if (ms4.k() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, ms4.k());
            }
            if (ms4.m() == null) {
                px0.bindNull(8);
            } else {
                px0.bindLong(8, (long) ms4.m().intValue());
            }
            if (ms4.n() == null) {
                px0.bindNull(9);
            } else {
                px0.bindLong(9, (long) ms4.n().intValue());
            }
            if (ms4.a() == null) {
                px0.bindNull(10);
            } else {
                px0.bindLong(10, (long) ms4.a().intValue());
            }
            if (ms4.b() == null) {
                px0.bindNull(11);
            } else {
                px0.bindLong(11, (long) ms4.b().intValue());
            }
            if (ms4.g() == null) {
                px0.bindNull(12);
            } else {
                px0.bindString(12, ms4.g());
            }
            String a2 = rs4.this.c.a(ms4.f());
            if (a2 == null) {
                px0.bindNull(13);
            } else {
                px0.bindString(13, a2);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `players` (`id`,`socialId`,`firstName`,`lastName`,`isDeleted`,`ranking`,`status`,`stepsBase`,`stepsOffset`,`caloriesBase`,`caloriesOffset`,`profilePicture`,`lastSync`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class p extends iw0<bt4> {
        @DexIgnore
        public p(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, bt4 bt4) {
            if (bt4.g() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, bt4.g());
            }
            if (bt4.q() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, bt4.q());
            }
            if (bt4.h() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, bt4.h());
            }
            if (bt4.d() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, bt4.d());
            }
            String a2 = rs4.this.h.a(bt4.j());
            if (a2 == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, a2);
            }
            if (bt4.i() == null) {
                px0.bindNull(6);
            } else {
                px0.bindLong(6, (long) bt4.i().intValue());
            }
            String a3 = rs4.this.c.a(bt4.m());
            if (a3 == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, a3);
            }
            String a4 = rs4.this.c.a(bt4.f());
            if (a4 == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, a4);
            }
            if (bt4.o() == null) {
                px0.bindNull(9);
            } else {
                px0.bindLong(9, (long) bt4.o().intValue());
            }
            if (bt4.e() == null) {
                px0.bindNull(10);
            } else {
                px0.bindLong(10, (long) bt4.e().intValue());
            }
            if (bt4.l() == null) {
                px0.bindNull(11);
            } else {
                px0.bindString(11, bt4.l());
            }
            if (bt4.s() == null) {
                px0.bindNull(12);
            } else {
                px0.bindString(12, bt4.s());
            }
            if (bt4.n() == null) {
                px0.bindNull(13);
            } else {
                px0.bindString(13, bt4.n());
            }
            String c = rs4.this.h.c(bt4.k());
            if (c == null) {
                px0.bindNull(14);
            } else {
                px0.bindString(14, c);
            }
            String b = rs4.this.h.b(bt4.p());
            if (b == null) {
                px0.bindNull(15);
            } else {
                px0.bindString(15, b);
            }
            String b2 = rs4.this.h.b(bt4.c());
            if (b2 == null) {
                px0.bindNull(16);
            } else {
                px0.bindString(16, b2);
            }
            String a5 = rs4.this.c.a(bt4.b());
            if (a5 == null) {
                px0.bindNull(17);
            } else {
                px0.bindString(17, a5);
            }
            String a6 = rs4.this.c.a(bt4.r());
            if (a6 == null) {
                px0.bindNull(18);
            } else {
                px0.bindString(18, a6);
            }
            String a7 = rs4.this.c.a(bt4.a());
            if (a7 == null) {
                px0.bindNull(19);
            } else {
                px0.bindString(19, a7);
            }
            px0.bindLong(20, bt4.t() ? 1 : 0);
            if (bt4.g() == null) {
                px0.bindNull(21);
            } else {
                px0.bindString(21, bt4.g());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0, com.fossil.iw0
        public String createQuery() {
            return "UPDATE OR ABORT `history_challenge` SET `id` = ?,`type` = ?,`name` = ?,`des` = ?,`owner` = ?,`numberOfPlayers` = ?,`startTime` = ?,`endTime` = ?,`target` = ?,`duration` = ?,`privacy` = ?,`version` = ?,`status` = ?,`players` = ?,`topPlayer` = ?,`currentPlayer` = ?,`createdAt` = ?,`updatedAt` = ?,`completedAt` = ?,`isFirst` = ? WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class q extends xw0 {
        @DexIgnore
        public q(rs4 rs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM challenge WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class r extends xw0 {
        @DexIgnore
        public r(rs4 rs4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM challenge";
        }
    }

    @DexIgnore
    public rs4(qw0 qw0) {
        this.f3151a = qw0;
        this.b = new h(qw0);
        this.d = new k(this, qw0);
        this.e = new l(qw0);
        new m(qw0);
        this.g = new n(qw0);
        this.i = new o(qw0);
        this.j = new p(qw0);
        this.k = new q(this, qw0);
        this.l = new r(this, qw0);
        this.m = new a(this, qw0);
        this.n = new b(this, qw0);
        this.o = new c(this, qw0);
        this.p = new d(this, qw0);
        this.q = new e(this, qw0);
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public List<ms4> a() {
        Boolean valueOf;
        tw0 f2 = tw0.f("SELECT * FROM players", 0);
        this.f3151a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3151a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, "socialId");
            int c4 = dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = dx0.c(b2, "isDeleted");
            int c7 = dx0.c(b2, "ranking");
            int c8 = dx0.c(b2, "status");
            int c9 = dx0.c(b2, "stepsBase");
            int c10 = dx0.c(b2, "stepsOffset");
            int c11 = dx0.c(b2, "caloriesBase");
            int c12 = dx0.c(b2, "caloriesOffset");
            int c13 = dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c14 = dx0.c(b2, "lastSync");
            try {
                try {
                    ArrayList arrayList = new ArrayList(b2.getCount());
                    while (b2.moveToNext()) {
                        String string = b2.getString(c2);
                        String string2 = b2.getString(c3);
                        String string3 = b2.getString(c4);
                        String string4 = b2.getString(c5);
                        Integer valueOf2 = b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6));
                        if (valueOf2 == null) {
                            valueOf = null;
                        } else {
                            valueOf = Boolean.valueOf(valueOf2.intValue() != 0);
                        }
                        try {
                            arrayList.add(new ms4(string, string2, string3, string4, valueOf, b2.isNull(c7) ? null : Integer.valueOf(b2.getInt(c7)), b2.getString(c8), b2.isNull(c9) ? null : Integer.valueOf(b2.getInt(c9)), b2.isNull(c10) ? null : Integer.valueOf(b2.getInt(c10)), b2.isNull(c11) ? null : Integer.valueOf(b2.getInt(c11)), b2.isNull(c12) ? null : Integer.valueOf(b2.getInt(c12)), b2.getString(c13), this.c.d(b2.getString(c14))));
                        } catch (Throwable th) {
                            th = th;
                        }
                    }
                    b2.close();
                    f2.m();
                    return arrayList;
                } catch (Throwable th2) {
                    th = th2;
                    b2.close();
                    f2.m();
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public int b(String str) {
        this.f3151a.assertNotSuspendingTransaction();
        px0 acquire = this.k.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.f3151a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.f3151a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.f3151a.endTransaction();
            this.k.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public LiveData<ps4> c(String[] strArr, Date date) {
        StringBuilder b2 = hx0.b();
        b2.append("SELECT ");
        b2.append(g78.ANY_MARKER);
        b2.append(" FROM challenge WHERE status IN (");
        int length = strArr.length;
        hx0.a(b2, length);
        b2.append(") AND endTime > ");
        b2.append("?");
        b2.append(" LIMIT 1");
        int i2 = length + 1;
        tw0 f2 = tw0.f(b2.toString(), i2);
        int i3 = 1;
        for (String str : strArr) {
            if (str == null) {
                f2.bindNull(i3);
            } else {
                f2.bindString(i3, str);
            }
            i3++;
        }
        String a2 = this.c.a(date);
        if (a2 == null) {
            f2.bindNull(i2);
        } else {
            f2.bindString(i2, a2);
        }
        return this.f3151a.getInvalidationTracker().d(new String[]{"challenge"}, false, new g(f2));
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public ks4 d(String str) {
        ks4 ks4 = null;
        tw0 f2 = tw0.f("SELECT * FROM display_player WHERE challengeId = ?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f3151a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3151a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "challengeId");
            int c3 = dx0.c(b2, "numberOfPlayers");
            int c4 = dx0.c(b2, "topPlayers");
            int c5 = dx0.c(b2, "nearPlayers");
            if (b2.moveToFirst()) {
                ks4 = new ks4(b2.getString(c2), b2.isNull(c3) ? null : Integer.valueOf(b2.getInt(c3)), this.f.b(b2.getString(c4)), this.f.b(b2.getString(c5)));
            }
            return ks4;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public bt4 e(String str) {
        Throwable th;
        bt4 bt4;
        tw0 f2 = tw0.f("SELECT * FROM history_challenge WHERE id = ?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f3151a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3151a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, "type");
            int c4 = dx0.c(b2, "name");
            int c5 = dx0.c(b2, "des");
            int c6 = dx0.c(b2, "owner");
            int c7 = dx0.c(b2, "numberOfPlayers");
            int c8 = dx0.c(b2, SampleRaw.COLUMN_START_TIME);
            int c9 = dx0.c(b2, SampleRaw.COLUMN_END_TIME);
            int c10 = dx0.c(b2, "target");
            int c11 = dx0.c(b2, "duration");
            int c12 = dx0.c(b2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
            int c13 = dx0.c(b2, "version");
            int c14 = dx0.c(b2, "status");
            try {
                int c15 = dx0.c(b2, "players");
                int c16 = dx0.c(b2, "topPlayer");
                int c17 = dx0.c(b2, "currentPlayer");
                int c18 = dx0.c(b2, "createdAt");
                int c19 = dx0.c(b2, "updatedAt");
                int c20 = dx0.c(b2, "completedAt");
                int c21 = dx0.c(b2, "isFirst");
                if (b2.moveToFirst()) {
                    bt4 = new bt4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), this.h.d(b2.getString(c6)), b2.isNull(c7) ? null : Integer.valueOf(b2.getInt(c7)), this.c.d(b2.getString(c8)), this.c.d(b2.getString(c9)), b2.isNull(c10) ? null : Integer.valueOf(b2.getInt(c10)), b2.isNull(c11) ? null : Integer.valueOf(b2.getInt(c11)), b2.getString(c12), b2.getString(c13), b2.getString(c14), this.h.f(b2.getString(c15)), this.h.e(b2.getString(c16)), this.h.e(b2.getString(c17)), this.c.d(b2.getString(c18)), this.c.d(b2.getString(c19)), this.c.d(b2.getString(c20)), b2.getInt(c21) != 0);
                } else {
                    bt4 = null;
                }
                b2.close();
                f2.m();
                return bt4;
            } catch (Throwable th2) {
                th = th2;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public ps4 f(String[] strArr, Date date) {
        Throwable th;
        StringBuilder b2 = hx0.b();
        b2.append("SELECT ");
        b2.append(g78.ANY_MARKER);
        b2.append(" FROM challenge WHERE status IN (");
        int length = strArr.length;
        hx0.a(b2, length);
        b2.append(") AND endTime > ");
        b2.append("?");
        b2.append(" LIMIT 1");
        String sb = b2.toString();
        int i2 = 1;
        int i3 = length + 1;
        tw0 f2 = tw0.f(sb, i3);
        for (String str : strArr) {
            if (str == null) {
                f2.bindNull(i2);
            } else {
                f2.bindString(i2, str);
            }
            i2++;
        }
        String a2 = this.c.a(date);
        if (a2 == null) {
            f2.bindNull(i3);
        } else {
            f2.bindString(i3, a2);
        }
        this.f3151a.assertNotSuspendingTransaction();
        Cursor b3 = ex0.b(this.f3151a, f2, false, null);
        try {
            int c2 = dx0.c(b3, "id");
            int c3 = dx0.c(b3, "type");
            int c4 = dx0.c(b3, "name");
            int c5 = dx0.c(b3, "des");
            int c6 = dx0.c(b3, "owner");
            int c7 = dx0.c(b3, "numberOfPlayers");
            int c8 = dx0.c(b3, SampleRaw.COLUMN_START_TIME);
            int c9 = dx0.c(b3, SampleRaw.COLUMN_END_TIME);
            int c10 = dx0.c(b3, "target");
            int c11 = dx0.c(b3, "duration");
            try {
                ps4 ps4 = b3.moveToFirst() ? new ps4(b3.getString(c2), b3.getString(c3), b3.getString(c4), b3.getString(c5), this.c.c(b3.getString(c6)), b3.isNull(c7) ? null : Integer.valueOf(b3.getInt(c7)), this.c.d(b3.getString(c8)), this.c.d(b3.getString(c9)), b3.isNull(c10) ? null : Integer.valueOf(b3.getInt(c10)), b3.isNull(c11) ? null : Integer.valueOf(b3.getInt(c11)), b3.getString(dx0.c(b3, ShareConstants.WEB_DIALOG_PARAM_PRIVACY)), b3.getString(dx0.c(b3, "version")), b3.getString(dx0.c(b3, "status")), b3.getString(dx0.c(b3, "syncData")), this.c.d(b3.getString(dx0.c(b3, "createdAt"))), this.c.d(b3.getString(dx0.c(b3, "updatedAt"))), b3.getString(dx0.c(b3, "category"))) : null;
                b3.close();
                f2.m();
                return ps4;
            } catch (Throwable th2) {
                th = th2;
                b3.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b3.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public void g() {
        this.f3151a.assertNotSuspendingTransaction();
        px0 acquire = this.l.acquire();
        this.f3151a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f3151a.setTransactionSuccessful();
        } finally {
            this.f3151a.endTransaction();
            this.l.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public long h(ls4 ls4) {
        this.f3151a.assertNotSuspendingTransaction();
        this.f3151a.beginTransaction();
        try {
            long insertAndReturnId = this.d.insertAndReturnId(ls4);
            this.f3151a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.f3151a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public LiveData<List<bt4>> i() {
        tw0 f2 = tw0.f("SELECT * FROM history_challenge ORDER BY completedAt DESC LIMIT 15", 0);
        nw0 invalidationTracker = this.f3151a.getInvalidationTracker();
        i iVar = new i(f2);
        return invalidationTracker.d(new String[]{"history_challenge"}, false, iVar);
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public Long[] insert(List<ps4> list) {
        this.f3151a.assertNotSuspendingTransaction();
        this.f3151a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.f3151a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.f3151a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public List<ls4> j() {
        tw0 f2 = tw0.f("SELECT * FROM fitness", 0);
        this.f3151a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3151a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "pinType");
            int c3 = dx0.c(b2, "id");
            int c4 = dx0.c(b2, "encryptMethod");
            int c5 = dx0.c(b2, "encryptedData");
            int c6 = dx0.c(b2, "keyType");
            int c7 = dx0.c(b2, "sequence");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                ls4 ls4 = new ls4(b2.getInt(c3), b2.getInt(c4), b2.getString(c5), b2.getInt(c6), b2.getString(c7));
                ls4.g(b2.getInt(c2));
                arrayList.add(ls4);
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public long k(ps4 ps4) {
        this.f3151a.assertNotSuspendingTransaction();
        this.f3151a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(ps4);
            this.f3151a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.f3151a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public LiveData<bt4> l(String str) {
        tw0 f2 = tw0.f("SELECT * FROM history_challenge WHERE id = ? limit 1", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        nw0 invalidationTracker = this.f3151a.getInvalidationTracker();
        j jVar = new j(f2);
        return invalidationTracker.d(new String[]{"history_challenge"}, false, jVar);
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public ps4 m(String str) {
        Throwable th;
        ps4 ps4;
        tw0 f2 = tw0.f("SELECT * FROM challenge WHERE id = ?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f3151a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3151a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, "type");
            int c4 = dx0.c(b2, "name");
            int c5 = dx0.c(b2, "des");
            int c6 = dx0.c(b2, "owner");
            int c7 = dx0.c(b2, "numberOfPlayers");
            int c8 = dx0.c(b2, SampleRaw.COLUMN_START_TIME);
            int c9 = dx0.c(b2, SampleRaw.COLUMN_END_TIME);
            int c10 = dx0.c(b2, "target");
            int c11 = dx0.c(b2, "duration");
            int c12 = dx0.c(b2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
            int c13 = dx0.c(b2, "version");
            int c14 = dx0.c(b2, "status");
            try {
                int c15 = dx0.c(b2, "syncData");
                int c16 = dx0.c(b2, "createdAt");
                int c17 = dx0.c(b2, "updatedAt");
                int c18 = dx0.c(b2, "category");
                if (b2.moveToFirst()) {
                    ps4 = new ps4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), this.c.c(b2.getString(c6)), b2.isNull(c7) ? null : Integer.valueOf(b2.getInt(c7)), this.c.d(b2.getString(c8)), this.c.d(b2.getString(c9)), b2.isNull(c10) ? null : Integer.valueOf(b2.getInt(c10)), b2.isNull(c11) ? null : Integer.valueOf(b2.getInt(c11)), b2.getString(c12), b2.getString(c13), b2.getString(c14), b2.getString(c15), this.c.d(b2.getString(c16)), this.c.d(b2.getString(c17)), b2.getString(c18));
                } else {
                    ps4 = null;
                }
                b2.close();
                f2.m();
                return ps4;
            } catch (Throwable th2) {
                th = th2;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public int n(String str) {
        int i2 = 0;
        tw0 f2 = tw0.f("SELECT COUNT(*) FROM challenge WHERE id =?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f3151a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3151a, f2, false, null);
        try {
            if (b2.moveToFirst()) {
                i2 = b2.getInt(0);
            }
            return i2;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public void o() {
        this.f3151a.assertNotSuspendingTransaction();
        px0 acquire = this.p.acquire();
        this.f3151a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f3151a.setTransactionSuccessful();
        } finally {
            this.f3151a.endTransaction();
            this.p.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public void p() {
        this.f3151a.assertNotSuspendingTransaction();
        px0 acquire = this.q.acquire();
        this.f3151a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f3151a.setTransactionSuccessful();
        } finally {
            this.f3151a.endTransaction();
            this.q.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public void q() {
        this.f3151a.assertNotSuspendingTransaction();
        px0 acquire = this.o.acquire();
        this.f3151a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f3151a.setTransactionSuccessful();
        } finally {
            this.f3151a.endTransaction();
            this.o.release(acquire);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    @Override // com.fossil.qs4
    public int r(bt4 bt4) {
        this.f3151a.assertNotSuspendingTransaction();
        this.f3151a.beginTransaction();
        try {
            int handle = this.j.handle(bt4);
            this.f3151a.setTransactionSuccessful();
            this.f3151a.endTransaction();
            return handle + 0;
        } catch (Throwable th) {
            this.f3151a.endTransaction();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public LiveData<ps4> s(String str) {
        tw0 f2 = tw0.f("SELECT * FROM challenge WHERE id = ?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        nw0 invalidationTracker = this.f3151a.getInvalidationTracker();
        f fVar = new f(f2);
        return invalidationTracker.d(new String[]{"challenge"}, false, fVar);
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public Long[] t(List<bt4> list) {
        this.f3151a.assertNotSuspendingTransaction();
        this.f3151a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.g.insertAndReturnIdsArrayBox(list);
            this.f3151a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.f3151a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public void u(String[] strArr) {
        this.f3151a.assertNotSuspendingTransaction();
        StringBuilder b2 = hx0.b();
        b2.append("DELETE FROM challenge WHERE status IN (");
        hx0.a(b2, strArr.length);
        b2.append(")");
        px0 compileStatement = this.f3151a.compileStatement(b2.toString());
        int i2 = 1;
        for (String str : strArr) {
            if (str == null) {
                compileStatement.bindNull(i2);
            } else {
                compileStatement.bindString(i2, str);
            }
            i2++;
        }
        this.f3151a.beginTransaction();
        try {
            compileStatement.executeUpdateDelete();
            this.f3151a.setTransactionSuccessful();
        } finally {
            this.f3151a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public Long[] v(List<ms4> list) {
        this.f3151a.assertNotSuspendingTransaction();
        this.f3151a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.i.insertAndReturnIdsArrayBox(list);
            this.f3151a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.f3151a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public void w() {
        this.f3151a.assertNotSuspendingTransaction();
        px0 acquire = this.m.acquire();
        this.f3151a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f3151a.setTransactionSuccessful();
        } finally {
            this.f3151a.endTransaction();
            this.m.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public void x() {
        this.f3151a.assertNotSuspendingTransaction();
        px0 acquire = this.n.acquire();
        this.f3151a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f3151a.setTransactionSuccessful();
        } finally {
            this.f3151a.endTransaction();
            this.n.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.qs4
    public Long[] y(List<ks4> list) {
        this.f3151a.assertNotSuspendingTransaction();
        this.f3151a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.e.insertAndReturnIdsArrayBox(list);
            this.f3151a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.f3151a.endTransaction();
        }
    }
}
