package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cr3 implements Callable<String> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ or3 f640a;
    @DexIgnore
    public /* final */ /* synthetic */ yq3 b;

    @DexIgnore
    public cr3(yq3 yq3, or3 or3) {
        this.b = yq3;
        this.f640a = or3;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public final /* synthetic */ String call() throws Exception {
        ll3 P = this.b.P(this.f640a);
        if (P != null) {
            return P.x();
        }
        this.b.d().I().a("App info was null when attempting to get app instance id");
        return null;
    }
}
