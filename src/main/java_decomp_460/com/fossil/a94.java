package com.fossil;

import java.lang.Thread;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a94 implements Thread.UncaughtExceptionHandler {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ a f228a;
    @DexIgnore
    public /* final */ rc4 b;
    @DexIgnore
    public /* final */ Thread.UncaughtExceptionHandler c;
    @DexIgnore
    public /* final */ AtomicBoolean d; // = new AtomicBoolean(false);

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(rc4 rc4, Thread thread, Throwable th);
    }

    @DexIgnore
    public a94(a aVar, rc4 rc4, Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.f228a = aVar;
        this.b = rc4;
        this.c = uncaughtExceptionHandler;
    }

    @DexIgnore
    public boolean a() {
        return this.d.get();
    }

    @DexIgnore
    public void uncaughtException(Thread thread, Throwable th) {
        this.d.set(true);
        if (thread == null) {
            try {
                x74.f().d("Could not handle uncaught exception; null thread");
            } catch (Exception e) {
                x74.f().e("An error occurred in the uncaught exception handler", e);
            } catch (Throwable th2) {
                x74.f().b("Crashlytics completed exception processing. Invoking default exception handler.");
                this.c.uncaughtException(thread, th);
                this.d.set(false);
                throw th2;
            }
        } else if (th == null) {
            x74.f().d("Could not handle uncaught exception; null throwable");
        } else {
            this.f228a.a(this.b, thread, th);
        }
        x74.f().b("Crashlytics completed exception processing. Invoking default exception handler.");
        this.c.uncaughtException(thread, th);
        this.d.set(false);
    }
}
