package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.bo6;
import com.fossil.nk5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iw5 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ on5 f1680a;
    @DexIgnore
    public /* final */ ArrayList<bo6.b> b;
    @DexIgnore
    public /* final */ wa1 c;
    @DexIgnore
    public b d;
    @DexIgnore
    public /* final */ PortfolioApp e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ImageView f1681a;
        @DexIgnore
        public /* final */ ImageView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ FlexibleTextView d;
        @DexIgnore
        public /* final */ ConstraintLayout e;
        @DexIgnore
        public /* final */ ImageView f;
        @DexIgnore
        public /* final */ FlexibleTextView g;
        @DexIgnore
        public /* final */ CardView h;
        @DexIgnore
        public /* final */ /* synthetic */ iw5 i;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.iw5$a$a")
        /* renamed from: com.fossil.iw5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0124a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public View$OnClickListenerC0124a(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b bVar;
                if (this.b.i.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && (bVar = this.b.i.d) != null) {
                    bVar.M0(((bo6.b) this.b.i.b.get(this.b.getAdapterPosition())).c());
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements CloudImageHelper.OnImageCallbackListener {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ a f1682a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public b(a aVar) {
                this.f1682a = aVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
            public void onImageCallback(String str, String str2) {
                pq7.c(str, "serial");
                pq7.c(str2, "filePath");
                this.f1682a.i.c.t(str2).F0(this.f1682a.f1681a);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(iw5 iw5, View view) {
            super(view);
            pq7.c(view, "view");
            this.i = iw5;
            this.f1681a = (ImageView) view.findViewById(2131362691);
            this.b = (ImageView) view.findViewById(2131362750);
            this.c = (FlexibleTextView) view.findViewById(2131363319);
            this.d = (FlexibleTextView) view.findViewById(2131363347);
            this.e = (ConstraintLayout) view.findViewById(2131362056);
            this.f = (ImageView) view.findViewById(2131362752);
            this.g = (FlexibleTextView) view.findViewById(2131363405);
            this.h = (CardView) view.findViewById(2131361994);
            this.f.setOnClickListener(new View$OnClickListenerC0124a(this));
        }

        @DexIgnore
        /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0076: INVOKE  (r2v13 int) = (r8v0 com.fossil.bo6$b) type: VIRTUAL call: com.fossil.bo6.b.a():int), ('%' char)] */
        @SuppressLint({"SetTextI18n"})
        public final void b(bo6.b bVar) {
            pq7.c(bVar, "device");
            FlexibleTextView flexibleTextView = this.c;
            pq7.b(flexibleTextView, "mTvDeviceName");
            flexibleTextView.setText(l47.d(bVar.b()));
            if (bVar.d()) {
                CardView cardView = this.h;
                pq7.b(cardView, "mCardView");
                cardView.setRadius(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                CardView cardView2 = this.h;
                pq7.b(cardView2, "mCardView");
                cardView2.setCardElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.b.setImageResource(2131230960);
                ConstraintLayout constraintLayout = this.e;
                pq7.b(constraintLayout, "mClContainer");
                constraintLayout.setAlpha(1.0f);
                ConstraintLayout constraintLayout2 = this.e;
                pq7.b(constraintLayout2, "mClContainer");
                constraintLayout2.setBackground(gl0.f(this.i.e, 2131230833));
                if (bVar.a() >= 0) {
                    FlexibleTextView flexibleTextView2 = this.g;
                    pq7.b(flexibleTextView2, "mTvDeviceStatus");
                    StringBuilder sb = new StringBuilder();
                    sb.append(bVar.a());
                    sb.append('%');
                    flexibleTextView2.setText(sb.toString());
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, gl0.f(this.i.e, nk5.o.c(bVar.a())), (Drawable) null);
                } else {
                    FlexibleTextView flexibleTextView3 = this.g;
                    pq7.b(flexibleTextView3, "mTvDeviceStatus");
                    flexibleTextView3.setText("");
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                }
                fk0 fk0 = new fk0();
                ImageView imageView = this.f;
                pq7.b(imageView, "mIvSetting");
                imageView.setVisibility(0);
                fk0.c(this.e);
                FlexibleTextView flexibleTextView4 = this.d;
                pq7.b(flexibleTextView4, "mTvLastSyncedTime");
                int id = flexibleTextView4.getId();
                ImageView imageView2 = this.f;
                pq7.b(imageView2, "mIvSetting");
                fk0.e(id, 7, imageView2.getId(), 6);
                fk0.a(this.e);
                if (!bVar.e()) {
                    FlexibleTextView flexibleTextView5 = this.g;
                    pq7.b(flexibleTextView5, "mTvDeviceStatus");
                    flexibleTextView5.setText(um5.c(this.i.e, 2131887173));
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                }
                FlexibleTextView flexibleTextView6 = this.d;
                pq7.b(flexibleTextView6, "mTvLastSyncedTime");
                flexibleTextView6.setText(this.i.l(bVar.c(), true));
            } else {
                CardView cardView3 = this.h;
                pq7.b(cardView3, "mCardView");
                cardView3.setRadius(2.0f);
                CardView cardView4 = this.h;
                pq7.b(cardView4, "mCardView");
                cardView4.setCardElevation(2.0f);
                this.b.setImageResource(2131230959);
                ConstraintLayout constraintLayout3 = this.e;
                pq7.b(constraintLayout3, "mClContainer");
                constraintLayout3.setAlpha(0.7f);
                ConstraintLayout constraintLayout4 = this.e;
                pq7.b(constraintLayout4, "mClContainer");
                constraintLayout4.setBackground(gl0.f(this.i.e, 2131230858));
                FlexibleTextView flexibleTextView7 = this.g;
                pq7.b(flexibleTextView7, "mTvDeviceStatus");
                flexibleTextView7.setText(um5.c(this.i.e, 2131887173));
                this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.c.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                FlexibleTextView flexibleTextView8 = this.d;
                pq7.b(flexibleTextView8, "mTvLastSyncedTime");
                flexibleTextView8.setText(this.i.l(bVar.c(), false));
            }
            String d2 = qn5.l.a().d("nonBrandSurface");
            if (!TextUtils.isEmpty(d2)) {
                this.h.setBackgroundColor(Color.parseColor(d2));
            }
            CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(bVar.c()).setSerialPrefix(nk5.o.m(bVar.c())).setType(Constants.DeviceType.TYPE_LARGE);
            ImageView imageView3 = this.f1681a;
            pq7.b(imageView3, "mIvDevice");
            type.setPlaceHolder(imageView3, nk5.o.i(bVar.c(), nk5.b.SMALL)).setImageCallback(new b(this)).download();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void M0(String str);
    }

    @DexIgnore
    public iw5(ArrayList<bo6.b> arrayList, wa1 wa1, b bVar, PortfolioApp portfolioApp) {
        pq7.c(arrayList, "mData");
        pq7.c(wa1, "mRequestManager");
        pq7.c(portfolioApp, "mApp");
        this.b = arrayList;
        this.c = wa1;
        this.d = bVar;
        this.e = portfolioApp;
        this.f1680a = portfolioApp.k0();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public final String l(String str, boolean z) {
        String h;
        if (!z || !this.e.A0(str)) {
            long C = this.f1680a.C(str);
            if (C == 0) {
                h = "";
            } else if (System.currentTimeMillis() - C < 60000) {
                hr7 hr7 = hr7.f1520a;
                String c2 = um5.c(PortfolioApp.h0.c(), 2131887191);
                pq7.b(c2, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                h = String.format(c2, Arrays.copyOf(new Object[]{1}, 1));
                pq7.b(h, "java.lang.String.format(format, *args)");
            } else {
                h = lk5.h(C);
            }
            String string = this.e.getString(2131887175, new Object[]{h});
            pq7.b(string, "mApp.getString(R.string.\u2026ayTime, lastSyncTimeText)");
            return string;
        }
        String string2 = this.e.getString(2131886784);
        pq7.b(string2, "mApp.getString(R.string.\u2026ded_Text__SyncInProgress)");
        return string2;
    }

    @DexIgnore
    /* renamed from: m */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            bo6.b bVar = this.b.get(i);
            pq7.b(bVar, "mData[position]");
            aVar.b(bVar);
        }
    }

    @DexIgnore
    /* renamed from: n */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558699, viewGroup, false);
        pq7.b(inflate, "LayoutInflater.from(pare\u2026le_device, parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void o(ArrayList<bo6.b> arrayList) {
        pq7.c(arrayList, "data");
        this.b.clear();
        this.b.addAll(arrayList);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void p() {
        if (getItemCount() > 0) {
            notifyItemChanged(0);
        }
    }
}
