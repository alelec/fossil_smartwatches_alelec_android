package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import androidx.appcompat.view.menu.ExpandedMenuView;
import com.fossil.ig0;
import com.fossil.jg0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ag0 implements ig0, AdapterView.OnItemClickListener {
    @DexIgnore
    public Context b;
    @DexIgnore
    public LayoutInflater c;
    @DexIgnore
    public cg0 d;
    @DexIgnore
    public ExpandedMenuView e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public ig0.a i;
    @DexIgnore
    public a j;
    @DexIgnore
    public int k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BaseAdapter {
        @DexIgnore
        public int b; // = -1;

        @DexIgnore
        public a() {
            a();
        }

        @DexIgnore
        public void a() {
            eg0 x = ag0.this.d.x();
            if (x != null) {
                ArrayList<eg0> B = ag0.this.d.B();
                int size = B.size();
                for (int i = 0; i < size; i++) {
                    if (B.get(i) == x) {
                        this.b = i;
                        return;
                    }
                }
            }
            this.b = -1;
        }

        @DexIgnore
        /* renamed from: b */
        public eg0 getItem(int i) {
            ArrayList<eg0> B = ag0.this.d.B();
            int i2 = ag0.this.f + i;
            int i3 = this.b;
            if (i3 >= 0 && i2 >= i3) {
                i2++;
            }
            return B.get(i2);
        }

        @DexIgnore
        public int getCount() {
            int size = ag0.this.d.B().size() - ag0.this.f;
            return this.b < 0 ? size : size - 1;
        }

        @DexIgnore
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view2;
            if (view == null) {
                ag0 ag0 = ag0.this;
                view2 = ag0.c.inflate(ag0.h, viewGroup, false);
            } else {
                view2 = view;
            }
            ((jg0.a) view2).f(getItem(i), 0);
            return view2;
        }

        @DexIgnore
        public void notifyDataSetChanged() {
            a();
            super.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public ag0(int i2, int i3) {
        this.h = i2;
        this.g = i3;
    }

    @DexIgnore
    public ag0(Context context, int i2) {
        this(i2, 0);
        this.b = context;
        this.c = LayoutInflater.from(context);
    }

    @DexIgnore
    public ListAdapter a() {
        if (this.j == null) {
            this.j = new a();
        }
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void b(cg0 cg0, boolean z) {
        ig0.a aVar = this.i;
        if (aVar != null) {
            aVar.b(cg0, z);
        }
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void c(boolean z) {
        a aVar = this.j;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean d() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean e(cg0 cg0, eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean f(cg0 cg0, eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void g(ig0.a aVar) {
        this.i = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public int getId() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void h(Context context, cg0 cg0) {
        if (this.g != 0) {
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, this.g);
            this.b = contextThemeWrapper;
            this.c = LayoutInflater.from(contextThemeWrapper);
        } else if (this.b != null) {
            this.b = context;
            if (this.c == null) {
                this.c = LayoutInflater.from(context);
            }
        }
        this.d = cg0;
        a aVar = this.j;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void i(Parcelable parcelable) {
        m((Bundle) parcelable);
    }

    @DexIgnore
    public jg0 j(ViewGroup viewGroup) {
        if (this.e == null) {
            this.e = (ExpandedMenuView) this.c.inflate(re0.abc_expanded_menu_layout, viewGroup, false);
            if (this.j == null) {
                this.j = new a();
            }
            this.e.setAdapter((ListAdapter) this.j);
            this.e.setOnItemClickListener(this);
        }
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean k(ng0 ng0) {
        if (!ng0.hasVisibleItems()) {
            return false;
        }
        new dg0(ng0).d(null);
        ig0.a aVar = this.i;
        if (aVar != null) {
            aVar.c(ng0);
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public Parcelable l() {
        if (this.e == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        n(bundle);
        return bundle;
    }

    @DexIgnore
    public void m(Bundle bundle) {
        SparseArray<Parcelable> sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.e.restoreHierarchyState(sparseParcelableArray);
        }
    }

    @DexIgnore
    public void n(Bundle bundle) {
        SparseArray<Parcelable> sparseArray = new SparseArray<>();
        ExpandedMenuView expandedMenuView = this.e;
        if (expandedMenuView != null) {
            expandedMenuView.saveHierarchyState(sparseArray);
        }
        bundle.putSparseParcelableArray("android:menu:list", sparseArray);
    }

    @DexIgnore
    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        this.d.O(this.j.getItem(i2), this, 0);
    }
}
