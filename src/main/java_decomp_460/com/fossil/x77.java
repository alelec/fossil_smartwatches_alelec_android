package com.fossil;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.a87;
import com.fossil.hq4;
import com.fossil.ja7;
import com.fossil.jn5;
import com.fossil.s87;
import com.fossil.t47;
import com.fossil.w67;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.watchface.WatchFaceEditorView;
import com.portfolio.platform.watchface.edit.WatchFaceEditActivity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x77 extends qv5 {
    @DexIgnore
    public static /* final */ String C;
    @DexIgnore
    public static /* final */ a D; // = new a(null);
    @DexIgnore
    public WatchFaceEditorView A;
    @DexIgnore
    public HashMap B;
    @DexIgnore
    public po4 h;
    @DexIgnore
    public /* final */ ArrayList<Fragment> i; // = new ArrayList<>();
    @DexIgnore
    public a87 j;
    @DexIgnore
    public rg5 k;
    @DexIgnore
    public int l; // = 2;
    @DexIgnore
    public View m;
    @DexIgnore
    public ea7 s;
    @DexIgnore
    public s97 t;
    @DexIgnore
    public z97 u;
    @DexIgnore
    public d87 v;
    @DexIgnore
    public n97 w;
    @DexIgnore
    public String x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return x77.C;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditFragment$setPresetToWatch$1", f = "WatchFaceEditFragment.kt", l = {464}, m = "invokeSuspend")
    public static final class a0 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ x77 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a0(x77 x77, qn7 qn7) {
            super(2, qn7);
            this.this$0 = x77;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a0 a0Var = new a0(this.this$0, qn7);
            a0Var.p$ = (iv7) obj;
            return a0Var;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a0) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object h0;
            Boolean a2;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                a87 Q6 = x77.Q6(this.this$0);
                this.L$0 = iv7;
                this.label = 1;
                h0 = Q6.h0(this);
                if (h0 == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                h0 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((Boolean) h0).booleanValue()) {
                return tl7.f3441a;
            }
            mo5 a0 = x77.Q6(this.this$0).a0();
            boolean booleanValue = (a0 == null || (a2 = ao7.a(a0.m())) == null) ? false : a2.booleanValue();
            List<s87> g0 = x77.R6(this.this$0).g0();
            if (booleanValue && !jn5.c(jn5.b, this.this$0.requireActivity(), jn5.a.SET_BLE_COMMAND, false, false, false, null, 44, null)) {
                return tl7.f3441a;
            }
            if (!jn5.f(jn5.b, this.this$0.requireActivity(), x77.Q6(this.this$0).c0(g0), x77.Q6(this.this$0).Z(g0), false, true, true, null, 72, null)) {
                return tl7.f3441a;
            }
            if (booleanValue) {
                x77.Q6(this.this$0).P();
            } else {
                x77.Q6(this.this$0).T();
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements BottomNavigationView.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4049a;
        @DexIgnore
        public /* final */ /* synthetic */ br7 b;

        @DexIgnore
        public b(x77 x77, br7 br7) {
            this.f4049a = x77;
            this.b = br7;
        }

        @DexIgnore
        @Override // com.google.android.material.bottomnavigation.BottomNavigationView.d
        public final boolean a(MenuItem menuItem) {
            Drawable newDrawable;
            Drawable newDrawable2;
            Drawable newDrawable3;
            Drawable newDrawable4;
            Drawable newDrawable5;
            Drawable newDrawable6;
            Drawable drawable = null;
            pq7.c(menuItem, "item");
            BottomNavigationView bottomNavigationView = x77.M6(this.f4049a).z;
            pq7.b(bottomNavigationView, "mBinding.tlIndicator");
            Menu menu = bottomNavigationView.getMenu();
            pq7.b(menu, "mBinding.tlIndicator.menu");
            menu.findItem(2131363192).setIcon(2131231208);
            menu.findItem(2131363149).setIcon(2131231198);
            menu.findItem(2131363186).setIcon(2131231207);
            menu.findItem(2131361907).setIcon(2131231203);
            menu.findItem(2131362148).setIcon(2131231199);
            int i = 2;
            switch (menuItem.getItemId()) {
                case 2131361907:
                    Drawable f = gl0.f(this.f4049a.requireContext(), 2131231203);
                    if (f != null) {
                        Drawable.ConstantState constantState = f.getConstantState();
                        if (!(constantState == null || (newDrawable = constantState.newDrawable()) == null)) {
                            drawable = newDrawable.mutate();
                        }
                        if (drawable != null) {
                            drawable.setColorFilter(this.b.element, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(drawable);
                    }
                    x77 x77 = this.f4049a;
                    View view = x77.M6(x77).E;
                    pq7.b(view, "mBinding.viewIndicatorPhoto");
                    x77.c7(view);
                    i = 3;
                    break;
                case 2131362148:
                    Drawable f2 = gl0.f(this.f4049a.requireContext(), 2131231199);
                    if (f2 != null) {
                        Drawable.ConstantState constantState2 = f2.getConstantState();
                        if (!(constantState2 == null || (newDrawable2 = constantState2.newDrawable()) == null)) {
                            drawable = newDrawable2.mutate();
                        }
                        if (drawable != null) {
                            drawable.setColorFilter(this.b.element, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(drawable);
                    }
                    x77 x772 = this.f4049a;
                    View view2 = x77.M6(x772).D;
                    pq7.b(view2, "mBinding.viewIndicatorComplication");
                    x772.c7(view2);
                    i = 4;
                    break;
                case 2131363149:
                    Drawable f3 = gl0.f(this.f4049a.requireContext(), 2131231198);
                    if (f3 != null) {
                        Drawable.ConstantState constantState3 = f3.getConstantState();
                        if (!(constantState3 == null || (newDrawable3 = constantState3.newDrawable()) == null)) {
                            drawable = newDrawable3.mutate();
                        }
                        if (drawable != null) {
                            drawable.setColorFilter(this.b.element, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(drawable);
                    }
                    x77 x773 = this.f4049a;
                    View view3 = x77.M6(x773).F;
                    pq7.b(view3, "mBinding.viewIndicatorSticker");
                    x773.c7(view3);
                    i = 1;
                    break;
                case 2131363186:
                    Drawable f4 = gl0.f(this.f4049a.requireContext(), 2131231207);
                    if (f4 != null) {
                        Drawable.ConstantState constantState4 = f4.getConstantState();
                        if (!(constantState4 == null || (newDrawable4 = constantState4.newDrawable()) == null)) {
                            drawable = newDrawable4.mutate();
                        }
                        if (drawable != null) {
                            drawable.setColorFilter(this.b.element, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(drawable);
                    }
                    x77 x774 = this.f4049a;
                    View view4 = x77.M6(x774).G;
                    pq7.b(view4, "mBinding.viewIndicatorTemplate");
                    x774.c7(view4);
                    break;
                case 2131363192:
                    Drawable f5 = gl0.f(this.f4049a.requireContext(), 2131231208);
                    if (f5 != null) {
                        Drawable.ConstantState constantState5 = f5.getConstantState();
                        if (!(constantState5 == null || (newDrawable5 = constantState5.newDrawable()) == null)) {
                            drawable = newDrawable5.mutate();
                        }
                        if (drawable != null) {
                            drawable.setColorFilter(this.b.element, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(drawable);
                    }
                    x77 x775 = this.f4049a;
                    View view5 = x77.M6(x775).H;
                    pq7.b(view5, "mBinding.viewIndicatorText");
                    x775.c7(view5);
                    i = 0;
                    break;
                default:
                    Drawable f6 = gl0.f(this.f4049a.requireContext(), 2131231207);
                    if (f6 != null) {
                        Drawable.ConstantState constantState6 = f6.getConstantState();
                        if (!(constantState6 == null || (newDrawable6 = constantState6.newDrawable()) == null)) {
                            drawable = newDrawable6.mutate();
                        }
                        if (drawable != null) {
                            drawable.setColorFilter(this.b.element, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(drawable);
                    }
                    x77 x776 = this.f4049a;
                    View view6 = x77.M6(x776).G;
                    pq7.b(view6, "mBinding.viewIndicatorTemplate");
                    x776.c7(view6);
                    break;
            }
            this.f4049a.l = i;
            x77.M6(this.f4049a).I.j(i, false);
            b77.f401a.b(this.f4049a.l, this.f4049a.getActivity());
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b0 implements ja7.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4050a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b0(x77 x77) {
            this.f4050a = x77;
        }

        @DexIgnore
        @Override // com.fossil.ja7.b
        public void a(String str) {
            pq7.c(str, "text");
            WatchFaceEditorView.u0(x77.R6(this.f4050a), str, null, null, 6, null);
        }

        @DexIgnore
        @Override // com.fossil.ja7.b
        public void onCancel() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ x77 b;

        @DexIgnore
        public c(x77 x77) {
            this.b = x77;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.l7();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ x77 b;

        @DexIgnore
        public d(x77 x77) {
            this.b = x77;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(x77.D.a(), "onclick preview elementConfigs");
            if (jn5.c(jn5.b, this.b.requireContext(), jn5.a.SET_BLE_COMMAND, false, false, false, null, 60, null)) {
                x77.Q6(this.b).l0();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ x77 b;

        @DexIgnore
        public e(x77 x77) {
            this.b = x77;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.k7();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnDragListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4051a;

        @DexIgnore
        public f(x77 x77) {
            this.f4051a = x77;
        }

        @DexIgnore
        public final boolean onDrag(View view, DragEvent dragEvent) {
            gc7 f;
            pq7.b(dragEvent, Constants.EVENT);
            if (dragEvent.getAction() == 3) {
                ClipData clipData = dragEvent.getClipData();
                if (clipData == null || clipData.getItemCount() == 0) {
                    return false;
                }
                ClipData.Item itemAt = clipData.getItemAt(0);
                pq7.b(itemAt, "clipData.getItemAt(0)");
                Intent intent = itemAt.getIntent();
                String stringExtra = intent.getStringExtra("complication_id");
                if (stringExtra != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = x77.D.a();
                    local.d(a2, "clipData[EXTRA_COMPLICATION_ID] = " + stringExtra);
                    ClipDescription description = clipData.getDescription();
                    if (pq7.a(description != null ? description.getLabel() : null, "complication_list")) {
                        int intExtra = intent.getIntExtra("complication_radius", 0);
                        Point point = (Point) intent.getParcelableExtra("complication_point");
                        if (point == null) {
                            point = new Point();
                        }
                        za7 za7 = new za7(dragEvent.getX() - ((((float) point.x) + x77.R6(this.f4051a).getOffset()) + x77.R6(this.f4051a).getX()), dragEvent.getY() - ((((float) point.y) + x77.R6(this.f4051a).getOffset()) + x77.R6(this.f4051a).getY()), intExtra);
                        if (!x77.R6(this.f4051a).p0(za7)) {
                            return false;
                        }
                        if (x77.R6(this.f4051a).o0(stringExtra)) {
                            return false;
                        }
                        bb7 bb7 = new bb7(stringExtra, "", null, za7);
                        x77.Q6(this.f4051a).V(ab7.ADDED, bb7.a(), bb7.c());
                    }
                }
            } else if (dragEvent.getAction() == 4 && (f = hc7.c.f(this.f4051a)) != null) {
                f.s();
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements WatchFaceEditorView.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4052a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditFragment$initWatchFaceEditor$editorEventListener$1$initEditorDone$1", f = "WatchFaceEditFragment.kt", l = {188}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    a87 Q6 = x77.Q6(this.this$0.f4052a);
                    String str = this.this$0.f4052a.x;
                    this.L$0 = iv7;
                    this.label = 1;
                    if (Q6.i0(str, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (x77.Q6(this.this$0.f4052a).a0() != null) {
                    FlexibleTextView flexibleTextView = x77.M6(this.this$0.f4052a).C;
                    pq7.b(flexibleTextView, "mBinding.tvPresetName");
                    mo5 a0 = x77.Q6(this.this$0.f4052a).a0();
                    if (a0 != null) {
                        flexibleTextView.setText(a0.f());
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g(x77 x77) {
            this.f4052a = x77;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void a() {
            this.f4052a.n7();
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void b() {
            this.f4052a.d7();
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public boolean c(zb7 zb7) {
            pq7.c(zb7, "component");
            boolean i7 = this.f4052a.i7(zb7);
            if (i7) {
                x77.R6(this.f4052a).r0(zb7);
            }
            this.f4052a.d7();
            return i7;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void d(zb7 zb7) {
            pq7.c(zb7, "component");
            this.f4052a.n7();
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void e(w67.b bVar, s87 s87) {
            pq7.c(bVar, "eventType");
            pq7.c(s87, "config");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = x77.D.a();
            local.d(a2, "onConfigChanged eventType=" + bVar + " config=" + s87);
            x77.Q6(this.f4052a).k0(s87, bVar);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void f(s87.c cVar) {
            pq7.c(cVar, "textConfig");
            this.f4052a.m7(cVar);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void g(eb7 eb7) {
            pq7.c(eb7, Constants.EVENT);
            gc7 f = hc7.c.f(this.f4052a);
            if (f != null) {
                f.E(eb7);
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void h(s87 s87) {
            gc7 f = hc7.c.f(this.f4052a);
            if (f != null) {
                f.v(s87);
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void i(Rect rect) {
            pq7.c(rect, "rect");
            xw7 unused = gu7.d(ds0.a(this.f4052a), null, null, new a(this, null), 3, null);
            gc7 f = hc7.c.f(this.f4052a);
            if (f != null) {
                f.u(rect);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4053a;

        @DexIgnore
        public h(x77 x77) {
            this.f4053a = x77;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            x77.R6(this.f4053a).j0(t);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4054a;

        @DexIgnore
        public i(x77 x77) {
            this.f4054a = x77;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            s87.c cVar = (s87.c) t.a();
            if (cVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = x77.D.a();
                local.d(a2, "Received text: " + cVar);
                if (!x77.R6(this.f4054a).Y(cVar)) {
                    String c = um5.c(PortfolioApp.h0.c(), 2131886595);
                    s37 s37 = s37.c;
                    FragmentManager childFragmentManager = this.f4054a.getChildFragmentManager();
                    pq7.b(childFragmentManager, "childFragmentManager");
                    pq7.b(c, "errorDescription");
                    s37.K(childFragmentManager, c);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4055a;

        @DexIgnore
        public j(x77 x77) {
            this.f4055a = x77;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            s87.b bVar = (s87.b) t.a();
            if (bVar != null && !x77.R6(this.f4055a).X(bVar)) {
                hr7 hr7 = hr7.f1520a;
                String string = PortfolioApp.h0.c().getString(2131886583);
                pq7.b(string, "PortfolioApp.instance.ge\u2026erStickersAppliedToWatch)");
                String format = String.format(string, Arrays.copyOf(new Object[]{"5"}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = this.f4055a.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                s37.K(childFragmentManager, format);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4056a;

        @DexIgnore
        public k(x77 x77) {
            this.f4056a = x77;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            WatchFaceEditorView.u0(x77.R6(this.f4056a), null, t, null, 5, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4057a;

        @DexIgnore
        public l(x77 x77) {
            this.f4057a = x77;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            WatchFaceEditorView.u0(x77.R6(this.f4057a), null, null, t, 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4058a;

        @DexIgnore
        public m(x77 x77) {
            this.f4058a = x77;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            boolean booleanValue = t.booleanValue();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = x77.D.a();
            local.d(a2, "goalRingLive, value = " + booleanValue);
            x77.R6(this.f4058a).v0(booleanValue);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4059a;

        @DexIgnore
        public n(x77 x77) {
            this.f4059a = x77;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            T t2 = t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = x77.D.a();
            local.d(a2, "listComplicationLive, value = " + ((String) t2));
            if (x77.R6(this.f4059a).getCurrentComplication() != null) {
                x77.Q6(this.f4059a).V(ab7.SELECTED, t2, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4060a;

        @DexIgnore
        public o(x77 x77) {
            this.f4060a = x77;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            T t2 = t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = x77.D.a();
            local.d(a2, "ringLive, value = " + ((Object) t2));
            x77.R6(this.f4060a).w0(t2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4061a;

        @DexIgnore
        public p(x77 x77) {
            this.f4061a = x77;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            T t2 = t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = x77.D.a();
            local.d(a2, "dianaAppSetingLive, value = " + ((Object) t2));
            x77.Q6(this.f4061a).q0(t2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4062a;

        @DexIgnore
        public q(x77 x77) {
            this.f4062a = x77;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            Boolean bool = (Boolean) t.a();
            if (bool != null) {
                bool.booleanValue();
                FLogger.INSTANCE.getLocal().d(x77.D.a(), "refresh watch face editor");
                x77.R6(this.f4062a).q0();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r<T> implements ls0<a87.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4063a;

        @DexIgnore
        public r(x77 x77) {
            this.f4063a = x77;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(a87.b bVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = x77.D.a();
            local.d(a2, "complicationDataLive, value = " + bVar);
            int i = y77.f4254a[bVar.b().ordinal()];
            if (i != 1) {
                if (i == 2) {
                    x77.R6(this.f4063a).x0(bVar.a());
                }
            } else if (!x77.R6(this.f4063a).W(bVar.a())) {
                hr7 hr7 = hr7.f1520a;
                String string = PortfolioApp.h0.c().getString(2131886572);
                pq7.b(string, "PortfolioApp.instance.ge\u2026erFeaturesAppliedToWatch)");
                String format = String.format(string, Arrays.copyOf(new Object[]{"4"}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = this.f4063a.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                s37.K(childFragmentManager, format);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s<T> implements ls0<ub7> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4064a;

        @DexIgnore
        public s(x77 x77) {
            this.f4064a = x77;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ub7 ub7) {
            hb7 a2;
            String str = null;
            if (ub7 == null) {
                gc7 f = hc7.c.f(this.f4064a);
                if (f != null) {
                    f.x("-metadata.priority");
                    return;
                }
                return;
            }
            WatchFaceEditorView R6 = x77.R6(this.f4064a);
            rb7 a3 = ub7.a();
            String c = a3 != null ? a3.c() : null;
            rb7 a4 = ub7.a();
            R6.n0(c, a4 != null ? a4.b() : null);
            gc7 f2 = hc7.c.f(this.f4064a);
            if (f2 != null) {
                rb7 a5 = ub7.a();
                if (!(a5 == null || (a2 = a5.a()) == null)) {
                    str = a2.b();
                }
                f2.x(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t<T> implements ls0<Object> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4065a;

        @DexIgnore
        public t(x77 x77) {
            this.f4065a = x77;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(Object obj) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.f4065a.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.z(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class u<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4066a;

        @DexIgnore
        public u(x77 x77) {
            this.f4066a = x77;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            pq7.b(bool, "isChanged");
            if (bool.booleanValue()) {
                FlexibleButton flexibleButton = x77.M6(this.f4066a).r;
                pq7.b(flexibleButton, "mBinding.fbApply");
                flexibleButton.setEnabled(true);
                x77.M6(this.f4066a).r.setBackgroundResource(2131231291);
                return;
            }
            FlexibleButton flexibleButton2 = x77.M6(this.f4066a).r;
            pq7.b(flexibleButton2, "mBinding.fbApply");
            flexibleButton2.setEnabled(false);
            x77.M6(this.f4066a).r.setBackgroundResource(2131231292);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class v<T> implements ls0<hq4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4067a;

        @DexIgnore
        public v(x77 x77) {
            this.f4067a = x77;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.a aVar) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.f4067a.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.O(childFragmentManager, aVar.a(), aVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class w<T> implements ls0<hq4.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4068a;

        @DexIgnore
        public w(x77 x77) {
            this.f4068a = x77;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.b bVar) {
            if (bVar.a()) {
                this.f4068a.b();
            } else {
                this.f4068a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class x<T> implements ls0<gl7<? extends Boolean, ? extends Boolean, ? extends Integer>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4069a;

        @DexIgnore
        public x(x77 x77) {
            this.f4069a = x77;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(gl7<Boolean, Boolean, Integer> gl7) {
            boolean booleanValue = gl7.getFirst().booleanValue();
            if (gl7.getSecond().booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = x77.D.a();
                StringBuilder sb = new StringBuilder();
                sb.append("isStartFromMyFaces ");
                sb.append(this.f4069a.y);
                sb.append(" presetId ");
                mo5 a0 = x77.Q6(this.f4069a).a0();
                sb.append(a0 != null ? a0.e() : null);
                local.d(a2, sb.toString());
                if (this.f4069a.y) {
                    mo5 a02 = x77.Q6(this.f4069a).a0();
                    if (!TextUtils.isEmpty(a02 != null ? a02.e() : null)) {
                        HomeActivity.a aVar = HomeActivity.B;
                        FragmentActivity requireActivity = this.f4069a.requireActivity();
                        pq7.b(requireActivity, "requireActivity()");
                        mo5 a03 = x77.Q6(this.f4069a).a0();
                        if (a03 != null) {
                            aVar.c(requireActivity, a03.e(), this.f4069a.z);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                }
                this.f4069a.requireActivity().finish();
            } else if (booleanValue) {
            } else {
                if (gl7.getThird().intValue() == 600 || gl7.getThird().intValue() == 601) {
                    s37 s37 = s37.c;
                    FragmentManager childFragmentManager = this.f4069a.getChildFragmentManager();
                    pq7.b(childFragmentManager, "childFragmentManager");
                    s37.O(childFragmentManager, gl7.getThird().intValue(), "");
                    return;
                }
                TroubleshootingActivity.a aVar2 = TroubleshootingActivity.B;
                FragmentActivity requireActivity2 = this.f4069a.requireActivity();
                pq7.b(requireActivity2, "requireActivity()");
                aVar2.a(requireActivity2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class y<T> implements ls0<fb7> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4070a;

        @DexIgnore
        public y(x77 x77) {
            this.f4070a = x77;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(fb7 fb7) {
            x77.Q6(this.f4070a).j0(fb7);
            this.f4070a.b7(fb7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class z<T> implements ls0<fb7> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ x77 f4071a;

        @DexIgnore
        public z(x77 x77) {
            this.f4071a = x77;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(fb7 fb7) {
            x77.Q6(this.f4071a).j0(fb7);
            this.f4071a.b7(fb7);
        }
    }

    /*
    static {
        String simpleName = x77.class.getSimpleName();
        pq7.b(simpleName, "WatchFaceEditFragment::class.java.simpleName");
        C = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ rg5 M6(x77 x77) {
        rg5 rg5 = x77.k;
        if (rg5 != null) {
            return rg5;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ a87 Q6(x77 x77) {
        a87 a87 = x77.j;
        if (a87 != null) {
            return a87;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ WatchFaceEditorView R6(x77 x77) {
        WatchFaceEditorView watchFaceEditorView = x77.A;
        if (watchFaceEditorView != null) {
            return watchFaceEditorView;
        }
        pq7.n("watchFaceEditor");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return C;
    }

    @DexIgnore
    public final void L() {
        t47.f fVar = new t47.f(2131558482);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886536));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886534));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886535));
        fVar.b(2131363291);
        fVar.b(2131363373);
        fVar.k(getChildFragmentManager(), "DIALOG_SET_TO_WATCH");
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        pq7.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -523101473) {
            if (hashCode == 1810891314 && str.equals("FAILED_LOADING_WATCH_FACE") && i2 == 2131363373 && (activity = getActivity()) != null) {
                activity.finish();
            }
        } else if (!str.equals("DIALOG_SET_TO_WATCH")) {
        } else {
            if (i2 != 2131363291) {
                if (i2 == 2131363373 && isActive()) {
                    l7();
                }
            } else if (isActive()) {
                requireActivity().finish();
            }
        }
    }

    @DexIgnore
    public final void b7(fb7 fb7) {
        if (this.k == null) {
            pq7.n("mBinding");
            throw null;
        } else if (fb7 != null) {
            WatchFaceEditorView watchFaceEditorView = this.A;
            if (watchFaceEditorView != null) {
                watchFaceEditorView.s0(fb7.f(), fb7.e());
            } else {
                pq7.n("watchFaceEditor");
                throw null;
            }
        } else {
            WatchFaceEditorView watchFaceEditorView2 = this.A;
            if (watchFaceEditorView2 != null) {
                watchFaceEditorView2.setWatchFaceBackground(null);
            } else {
                pq7.n("watchFaceEditor");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void c7(View view) {
        View view2;
        Drawable f2 = gl0.f(PortfolioApp.h0.c(), 2131230957);
        Drawable f3 = gl0.f(PortfolioApp.h0.c(), 2131230956);
        if (!(f2 == null || (view2 = this.m) == null)) {
            view2.setBackground(f2);
        }
        if (f3 != null) {
            view.setBackground(f3);
        }
        this.m = view;
    }

    @DexIgnore
    public final void d7() {
        rg5 rg5 = this.k;
        if (rg5 != null) {
            LinearLayout linearLayout = rg5.w;
            pq7.b(linearLayout, "mBinding.llTab");
            linearLayout.setVisibility(0);
            rg5 rg52 = this.k;
            if (rg52 != null) {
                ConstraintLayout constraintLayout = rg52.q;
                pq7.b(constraintLayout, "mBinding.clTrashBin");
                constraintLayout.setVisibility(8);
                rg5 rg53 = this.k;
                if (rg53 != null) {
                    FrameLayout frameLayout = rg53.t;
                    pq7.b(frameLayout, "mBinding.flBottom");
                    frameLayout.setVisibility(0);
                    return;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void e7() {
        String d2 = qn5.l.a().d("nonBrandSurface");
        if (!TextUtils.isEmpty(d2)) {
            rg5 rg5 = this.k;
            if (rg5 != null) {
                rg5.x.setBackgroundColor(Color.parseColor(d2));
            } else {
                pq7.n("mBinding");
                throw null;
            }
        }
        br7 br7 = new br7();
        br7.element = gl0.d(requireContext(), 2131099967);
        String d3 = qn5.l.a().d("primaryIconColor");
        if (d3 != null) {
            br7.element = Color.parseColor(d3);
        }
        rg5 rg52 = this.k;
        if (rg52 != null) {
            BottomNavigationView bottomNavigationView = rg52.z;
            pq7.b(bottomNavigationView, "mBinding.tlIndicator");
            bottomNavigationView.setItemIconTintList(null);
            rg5 rg53 = this.k;
            if (rg53 != null) {
                rg53.z.setOnNavigationItemSelectedListener(new b(this, br7));
                rg5 rg54 = this.k;
                if (rg54 != null) {
                    BottomNavigationView bottomNavigationView2 = rg54.z;
                    pq7.b(bottomNavigationView2, "mBinding.tlIndicator");
                    bottomNavigationView2.setSelectedItemId(2131363186);
                    return;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void f7() {
        this.i.clear();
        this.s = (ea7) getChildFragmentManager().Z("WatchFaceTextFragment");
        this.t = (s97) getChildFragmentManager().Z("WatchFaceStickerFragment");
        this.u = (z97) getChildFragmentManager().Z("WatchFaceTemplateFragment");
        this.w = (n97) getChildFragmentManager().Z("WatchFacePhotoBackgroundFragment");
        this.v = (d87) getChildFragmentManager().Z("WatchFaceComplicationContainerFragment");
        if (this.s == null) {
            fc7 fc7 = fc7.f1105a;
            String str = this.x;
            if (str == null) {
                str = "";
            }
            this.s = (ea7) fc7.b(fc7, ea7.class, str, 0, null, false, false, 60, null);
        }
        if (this.t == null) {
            fc7 fc72 = fc7.f1105a;
            String str2 = this.x;
            if (str2 == null) {
                str2 = "";
            }
            this.t = (s97) fc7.b(fc72, s97.class, str2, 0, null, false, false, 60, null);
        }
        if (this.u == null) {
            fc7 fc73 = fc7.f1105a;
            String str3 = this.x;
            if (str3 == null) {
                str3 = "";
            }
            this.u = (z97) fc7.b(fc73, z97.class, str3, 0, null, false, false, 60, null);
        }
        if (this.w == null) {
            fc7 fc74 = fc7.f1105a;
            String str4 = this.x;
            if (str4 == null) {
                str4 = "";
            }
            this.w = (n97) fc7.b(fc74, n97.class, str4, 0, null, false, false, 60, null);
        }
        if (this.v == null) {
            fc7 fc75 = fc7.f1105a;
            String str5 = this.x;
            if (str5 == null) {
                str5 = "";
            }
            this.v = (d87) fc7.b(fc75, d87.class, str5, 0, null, false, false, 60, null);
        }
        ea7 ea7 = this.s;
        if (ea7 != null) {
            this.i.add(ea7);
        }
        s97 s97 = this.t;
        if (s97 != null) {
            this.i.add(s97);
        }
        z97 z97 = this.u;
        if (z97 != null) {
            this.i.add(z97);
        }
        n97 n97 = this.w;
        if (n97 != null) {
            this.i.add(n97);
        }
        d87 d87 = this.v;
        if (d87 != null) {
            this.i.add(d87);
        }
        hc7 hc7 = hc7.c;
        ea7 ea72 = this.s;
        if (ea72 != null) {
            hc7.b(this, ea72);
            hc7 hc72 = hc7.c;
            s97 s972 = this.t;
            if (s972 != null) {
                hc72.b(this, s972);
                hc7 hc73 = hc7.c;
                z97 z972 = this.u;
                if (z972 != null) {
                    hc73.b(this, z972);
                    hc7 hc74 = hc7.c;
                    n97 n972 = this.w;
                    if (n972 != null) {
                        hc74.b(this, n972);
                        hc7 hc75 = hc7.c;
                        d87 d872 = this.v;
                        if (d872 != null) {
                            hc75.b(this, d872);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void g7() {
        rg5 rg5 = this.k;
        if (rg5 != null) {
            rg5.r.setOnClickListener(new c(this));
            rg5 rg52 = this.k;
            if (rg52 != null) {
                rg52.s.setOnClickListener(new d(this));
                rg5 rg53 = this.k;
                if (rg53 != null) {
                    rg53.B.setOnClickListener(new e(this));
                    rg5 rg54 = this.k;
                    if (rg54 != null) {
                        rg54.n().setOnDragListener(new f(this));
                    } else {
                        pq7.n("mBinding");
                        throw null;
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            } else {
                pq7.n("mBinding");
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void h7() {
        rg5 rg5 = this.k;
        if (rg5 != null) {
            WatchFaceEditorView watchFaceEditorView = rg5.L;
            pq7.b(watchFaceEditorView, "mBinding.watchFaceEditorView");
            this.A = watchFaceEditorView;
            g gVar = new g(this);
            WatchFaceEditorView watchFaceEditorView2 = this.A;
            if (watchFaceEditorView2 != null) {
                rg5 rg52 = this.k;
                if (rg52 != null) {
                    ConstraintLayout constraintLayout = rg52.q;
                    pq7.b(constraintLayout, "mBinding.clTrashBin");
                    watchFaceEditorView2.m0(constraintLayout, gVar);
                    return;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("watchFaceEditor");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final boolean i7(zb7 zb7) {
        View view = zb7.getView();
        int[] iArr = new int[2];
        int[] iArr2 = new int[2];
        view.getLocationOnScreen(iArr);
        rg5 rg5 = this.k;
        if (rg5 != null) {
            rg5.q.getLocationOnScreen(iArr2);
            return view.getHeight() + iArr[1] > iArr2[1];
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void j7() {
        LiveData<u37<Boolean>> m2;
        LiveData<List<DianaAppSetting>> g2;
        LiveData<cb7> n2;
        LiveData<String> k2;
        LiveData<Boolean> j2;
        LiveData<o87> o2;
        LiveData<Typeface> p2;
        MutableLiveData<u37<s87.b>> c2;
        LiveData<u37<s87.c>> d2;
        LiveData<fb7> l2;
        LiveData<fb7> e2;
        a87 a87 = this.j;
        if (a87 != null) {
            a87.e0().h(getViewLifecycleOwner(), new u(this));
            a87 a872 = this.j;
            if (a872 != null) {
                a872.h().h(getViewLifecycleOwner(), new v(this));
                a87 a873 = this.j;
                if (a873 != null) {
                    a873.j().h(getViewLifecycleOwner(), new w(this));
                    a87 a874 = this.j;
                    if (a874 != null) {
                        a874.U().h(getViewLifecycleOwner(), new x(this));
                        gc7 f2 = hc7.c.f(this);
                        if (!(f2 == null || (e2 = f2.e()) == null)) {
                            e2.h(getViewLifecycleOwner(), new y(this));
                        }
                        gc7 f3 = hc7.c.f(this);
                        if (!(f3 == null || (l2 = f3.l()) == null)) {
                            l2.h(getViewLifecycleOwner(), new z(this));
                        }
                        gc7 f4 = hc7.c.f(this);
                        if (!(f4 == null || (d2 = f4.d()) == null)) {
                            LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
                            pq7.b(viewLifecycleOwner, "viewLifecycleOwner");
                            d2.h(viewLifecycleOwner, new i(this));
                        }
                        gc7 f5 = hc7.c.f(this);
                        if (!(f5 == null || (c2 = f5.c()) == null)) {
                            LifecycleOwner viewLifecycleOwner2 = getViewLifecycleOwner();
                            pq7.b(viewLifecycleOwner2, "viewLifecycleOwner");
                            c2.h(viewLifecycleOwner2, new j(this));
                        }
                        gc7 f6 = hc7.c.f(this);
                        if (!(f6 == null || (p2 = f6.p()) == null)) {
                            LifecycleOwner viewLifecycleOwner3 = getViewLifecycleOwner();
                            pq7.b(viewLifecycleOwner3, "viewLifecycleOwner");
                            p2.h(viewLifecycleOwner3, new k(this));
                        }
                        gc7 f7 = hc7.c.f(this);
                        if (!(f7 == null || (o2 = f7.o()) == null)) {
                            LifecycleOwner viewLifecycleOwner4 = getViewLifecycleOwner();
                            pq7.b(viewLifecycleOwner4, "viewLifecycleOwner");
                            o2.h(viewLifecycleOwner4, new l(this));
                        }
                        gc7 f8 = hc7.c.f(this);
                        if (!(f8 == null || (j2 = f8.j()) == null)) {
                            LifecycleOwner viewLifecycleOwner5 = getViewLifecycleOwner();
                            pq7.b(viewLifecycleOwner5, "viewLifecycleOwner");
                            j2.h(viewLifecycleOwner5, new m(this));
                        }
                        gc7 f9 = hc7.c.f(this);
                        if (!(f9 == null || (k2 = f9.k()) == null)) {
                            LifecycleOwner viewLifecycleOwner6 = getViewLifecycleOwner();
                            pq7.b(viewLifecycleOwner6, "viewLifecycleOwner");
                            k2.h(viewLifecycleOwner6, new n(this));
                        }
                        gc7 f10 = hc7.c.f(this);
                        if (!(f10 == null || (n2 = f10.n()) == null)) {
                            LifecycleOwner viewLifecycleOwner7 = getViewLifecycleOwner();
                            pq7.b(viewLifecycleOwner7, "viewLifecycleOwner");
                            n2.h(viewLifecycleOwner7, new o(this));
                        }
                        gc7 f11 = hc7.c.f(this);
                        if (!(f11 == null || (g2 = f11.g()) == null)) {
                            LifecycleOwner viewLifecycleOwner8 = getViewLifecycleOwner();
                            pq7.b(viewLifecycleOwner8, "viewLifecycleOwner");
                            g2.h(viewLifecycleOwner8, new p(this));
                        }
                        gc7 f12 = hc7.c.f(this);
                        if (!(f12 == null || (m2 = f12.m()) == null)) {
                            LifecycleOwner viewLifecycleOwner9 = getViewLifecycleOwner();
                            pq7.b(viewLifecycleOwner9, "viewLifecycleOwner");
                            m2.h(viewLifecycleOwner9, new q(this));
                        }
                        a87 a875 = this.j;
                        if (a875 != null) {
                            a875.W().h(getViewLifecycleOwner(), new r(this));
                            a87 a876 = this.j;
                            if (a876 != null) {
                                a876.d0().h(getViewLifecycleOwner(), new s(this));
                                a87 a877 = this.j;
                                if (a877 != null) {
                                    LiveData<List<s87>> Y = a877.Y();
                                    LifecycleOwner viewLifecycleOwner10 = getViewLifecycleOwner();
                                    pq7.b(viewLifecycleOwner10, "viewLifecycleOwner");
                                    Y.h(viewLifecycleOwner10, new h(this));
                                    a87 a878 = this.j;
                                    if (a878 != null) {
                                        a878.b0().h(getViewLifecycleOwner(), new t(this));
                                    } else {
                                        pq7.n("viewModel");
                                        throw null;
                                    }
                                } else {
                                    pq7.n("viewModel");
                                    throw null;
                                }
                            } else {
                                pq7.n("viewModel");
                                throw null;
                            }
                        } else {
                            pq7.n("viewModel");
                            throw null;
                        }
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void k7() {
        a87 a87 = this.j;
        if (a87 != null) {
            Boolean e2 = a87.e0().e();
            if (e2 == null) {
                e2 = Boolean.FALSE;
            }
            pq7.b(e2, "viewModel.wfChangedLive.value ?: false");
            if (e2.booleanValue()) {
                L();
                return;
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
                return;
            }
            return;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void l7() {
        FLogger.INSTANCE.getLocal().d(C, "setPresetToWatch");
        xw7 unused = gu7.d(ds0.a(this), null, null, new a0(this, null), 3, null);
    }

    @DexIgnore
    public final void m7(s87.c cVar) {
        if (getChildFragmentManager().Z(ja7.h.a()) == null) {
            ja7 b2 = ja7.h.b(cVar.i(), new b0(this));
            if (isActive()) {
                b2.show(getChildFragmentManager(), ja7.h.a());
            }
        }
    }

    @DexIgnore
    public final void n7() {
        rg5 rg5 = this.k;
        if (rg5 != null) {
            LinearLayout linearLayout = rg5.w;
            pq7.b(linearLayout, "mBinding.llTab");
            linearLayout.setVisibility(8);
            rg5 rg52 = this.k;
            if (rg52 != null) {
                ConstraintLayout constraintLayout = rg52.q;
                pq7.b(constraintLayout, "mBinding.clTrashBin");
                constraintLayout.setVisibility(0);
                rg5 rg53 = this.k;
                if (rg53 != null) {
                    FrameLayout frameLayout = rg53.t;
                    pq7.b(frameLayout, "mBinding.flBottom");
                    frameLayout.setVisibility(8);
                    return;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        int i2;
        super.onActivityCreated(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.x = arguments.getString("preset_id");
            arguments.getString("order_id");
            this.y = arguments.getBoolean("from_faces");
            this.z = arguments.getBoolean("sharing_flow_extra");
            i2 = arguments.getInt("TAB_EXTRA");
        } else {
            i2 = 2;
        }
        FLogger.INSTANCE.getLocal().d(C, "onActivityCreated isStartFromMyFace " + this.y + " isSharingFlow " + this.z);
        PortfolioApp.h0.c().M().q1().a(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WatchFaceEditActivity watchFaceEditActivity = (WatchFaceEditActivity) activity;
            po4 po4 = this.h;
            if (po4 != null) {
                ts0 a2 = vs0.f(watchFaceEditActivity, po4).a(a87.class);
                pq7.b(a2, "ViewModelProviders.of(ac\u2026ditViewModel::class.java)");
                this.j = (a87) a2;
                hc7.c.a(this);
                h7();
                f7();
                e7();
                rg5 rg5 = this.k;
                if (rg5 != null) {
                    ViewPager2 viewPager2 = rg5.I;
                    pq7.b(viewPager2, "mBinding.vpTab");
                    viewPager2.setAdapter(new g67(getChildFragmentManager(), this.i));
                    rg5 rg52 = this.k;
                    if (rg52 != null) {
                        ViewPager2 viewPager22 = rg52.I;
                        pq7.b(viewPager22, "mBinding.vpTab");
                        viewPager22.setUserInputEnabled(false);
                        rg5 rg53 = this.k;
                        if (rg53 != null) {
                            ViewPager2 viewPager23 = rg53.I;
                            pq7.b(viewPager23, "mBinding.vpTab");
                            viewPager23.setOffscreenPageLimit(4);
                            rg5 rg54 = this.k;
                            if (rg54 != null) {
                                rg54.I.j(i2, false);
                                this.l = i2;
                                rg5 rg55 = this.k;
                                if (rg55 != null) {
                                    BottomNavigationView bottomNavigationView = rg55.z;
                                    pq7.b(bottomNavigationView, "mBinding.tlIndicator");
                                    bottomNavigationView.setSelectedItemId(this.l != 4 ? 2131363186 : 2131362148);
                                    g7();
                                    j7();
                                    return;
                                }
                                pq7.n("mBinding");
                                throw null;
                            }
                            pq7.n("mBinding");
                            throw null;
                        }
                        pq7.n("mBinding");
                        throw null;
                    }
                    pq7.n("mBinding");
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("viewModelFactory");
            throw null;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.watchface.edit.WatchFaceEditActivity");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 111 && i3 == 1 && jn5.b.m(getContext(), jn5.c.BLUETOOTH_CONNECTION)) {
            FLogger.INSTANCE.getLocal().d(C, "onclick apply elements elementConfigs, user skip setting & permission");
            a87 a87 = this.j;
            if (a87 != null) {
                mo5 a02 = a87.a0();
                if (a02 != null ? a02.m() : false) {
                    a87 a872 = this.j;
                    if (a872 != null) {
                        a872.P();
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else {
                    a87 a873 = this.j;
                    if (a873 != null) {
                        a873.T();
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final boolean onBackPressed() {
        k7();
        return false;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        rg5 z2 = rg5.z(layoutInflater);
        pq7.b(z2, "WatchFaceCustomizeFragme\u2026Binding.inflate(inflater)");
        this.k = z2;
        String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
        if (d2 != null) {
            rg5 rg5 = this.k;
            if (rg5 != null) {
                rg5.n().setBackgroundColor(Color.parseColor(d2));
                rg5 rg52 = this.k;
                if (rg52 != null) {
                    rg52.J.setBackgroundColor(Color.parseColor(d2));
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            } else {
                pq7.n("mBinding");
                throw null;
            }
        }
        rg5 rg53 = this.k;
        if (rg53 != null) {
            this.m = rg53.E;
            if (rg53 != null) {
                View n2 = rg53.n();
                pq7.b(n2, "mBinding.root");
                return n2;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        hc7.c.c(this);
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.B;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
