package com.fossil;

import android.content.Context;
import android.os.Bundle;
import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.m64;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n64 implements m64 {
    @DexIgnore
    public static volatile m64 c;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ fg3 f2473a;
    @DexIgnore
    public /* final */ Map<String, Object> b; // = new ConcurrentHashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements m64.a {
        @DexIgnore
        public a(n64 n64, String str) {
        }
    }

    @DexIgnore
    public n64(fg3 fg3) {
        rc2.k(fg3);
        this.f2473a = fg3;
    }

    @DexIgnore
    public static m64 d(j64 j64, Context context, ge4 ge4) {
        rc2.k(j64);
        rc2.k(context);
        rc2.k(ge4);
        rc2.k(context.getApplicationContext());
        if (c == null) {
            synchronized (n64.class) {
                try {
                    if (c == null) {
                        Bundle bundle = new Bundle(1);
                        if (j64.q()) {
                            ge4.b(h64.class, u64.b, v64.f3724a);
                            bundle.putBoolean("dataCollectionDefaultEnabled", j64.p());
                        }
                        c = new n64(zs2.b(context, null, null, null, bundle).e());
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return c;
    }

    @DexIgnore
    public static final /* synthetic */ void e(de4 de4) {
        boolean z = ((h64) de4.a()).f1432a;
        synchronized (n64.class) {
            try {
                ((n64) c).f2473a.d(z);
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.m64
    public void a(String str, String str2, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        if (o64.a(str) && o64.b(str2, bundle) && o64.d(str, str2, bundle)) {
            o64.e(str, str2, bundle);
            this.f2473a.a(str, str2, bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.m64
    public void b(String str, String str2, Object obj) {
        if (o64.a(str) && o64.c(str, str2)) {
            this.f2473a.c(str, str2, obj);
        }
    }

    @DexIgnore
    @Override // com.fossil.m64
    public m64.a c(String str, m64.b bVar) {
        rc2.k(bVar);
        if (!o64.a(str) || f(str)) {
            return null;
        }
        fg3 fg3 = this.f2473a;
        Object r64 = "fiam".equals(str) ? new r64(fg3, bVar) : (CrashDumperPlugin.NAME.equals(str) || "clx".equals(str)) ? new t64(fg3, bVar) : null;
        if (r64 == null) {
            return null;
        }
        this.b.put(str, r64);
        return new a(this, str);
    }

    @DexIgnore
    public final boolean f(String str) {
        return !str.isEmpty() && this.b.containsKey(str) && this.b.get(str) != null;
    }
}
