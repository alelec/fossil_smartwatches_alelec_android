package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u8 implements Parcelable.Creator<v8> {
    @DexIgnore
    public /* synthetic */ u8(kq7 kq7) {
    }

    @DexIgnore
    public final v8 a(byte[] bArr) throws IllegalArgumentException {
        if (bArr.length == 3) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            byte b = order.get(0);
            t8 a2 = t8.f.a(b);
            if (a2 != null) {
                return new v8(a2, order.getShort(1));
            }
            throw new IllegalArgumentException("Invalid action: " + ((int) b) + '.');
        }
        throw new IllegalArgumentException(e.c(e.e("Invalid data length ("), bArr.length, "), ", "require 3."));
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public v8 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            pq7.b(readString, "parcel.readString()!!");
            return new v8(t8.valueOf(readString), (short) parcel.readInt());
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public v8[] newArray(int i) {
        return new v8[i];
    }
}
