package com.fossil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class a44<K0, V0> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e<Object> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ int f193a;

        @DexIgnore
        public a(int i) {
            this.f193a = i;
        }

        @DexIgnore
        @Override // com.fossil.a44.e
        public <K, V> Map<K, Collection<V>> c() {
            return x34.k(this.f193a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<V> implements m14<List<V>>, Serializable {
        @DexIgnore
        public /* final */ int expectedValuesPerKey;

        @DexIgnore
        public b(int i) {
            a24.b(i, "expectedValuesPerKey");
            this.expectedValuesPerKey = i;
        }

        @DexIgnore
        @Override // com.fossil.m14
        public List<V> get() {
            return new ArrayList(this.expectedValuesPerKey);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<V> implements m14<Set<V>>, Serializable {
        @DexIgnore
        public /* final */ int expectedValuesPerKey;

        @DexIgnore
        public c(int i) {
            a24.b(i, "expectedValuesPerKey");
            this.expectedValuesPerKey = i;
        }

        @DexIgnore
        @Override // com.fossil.m14
        public Set<V> get() {
            return x44.e(this.expectedValuesPerKey);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<K0, V0> extends a44<K0, V0> {
        @DexIgnore
        public d() {
            super(null);
        }

        @DexIgnore
        public abstract <K extends K0, V extends V0> s34<K, V> c();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<K0> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends d<K0, Object> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ int f194a;

            @DexIgnore
            public a(int i) {
                this.f194a = i;
            }

            @DexIgnore
            @Override // com.fossil.a44.d
            public <K extends K0, V> s34<K, V> c() {
                return b44.b(e.this.c(), new b(this.f194a));
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b extends f<K0, Object> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ int f195a;

            @DexIgnore
            public b(int i) {
                this.f195a = i;
            }

            @DexIgnore
            @Override // com.fossil.a44.f
            public <K extends K0, V> w44<K, V> c() {
                return b44.c(e.this.c(), new c(this.f195a));
            }
        }

        @DexIgnore
        public d<K0, Object> a() {
            return b(2);
        }

        @DexIgnore
        public d<K0, Object> b(int i) {
            a24.b(i, "expectedValuesPerKey");
            return new a(i);
        }

        @DexIgnore
        public abstract <K extends K0, V> Map<K, Collection<V>> c();

        @DexIgnore
        public f<K0, Object> d() {
            return e(2);
        }

        @DexIgnore
        public f<K0, Object> e(int i) {
            a24.b(i, "expectedValuesPerKey");
            return new b(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f<K0, V0> extends a44<K0, V0> {
        @DexIgnore
        public f() {
            super(null);
        }

        @DexIgnore
        public abstract <K extends K0, V extends V0> w44<K, V> c();
    }

    @DexIgnore
    public a44() {
    }

    @DexIgnore
    public /* synthetic */ a44(z34 z34) {
        this();
    }

    @DexIgnore
    public static e<Object> a() {
        return b(8);
    }

    @DexIgnore
    public static e<Object> b(int i) {
        a24.b(i, "expectedKeys");
        return new a(i);
    }
}
