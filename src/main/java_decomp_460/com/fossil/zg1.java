package com.fossil;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zg1<T extends Drawable> implements id1<T>, ed1 {
    @DexIgnore
    public /* final */ T b;

    @DexIgnore
    public zg1(T t) {
        ik1.d(t);
        this.b = t;
    }

    @DexIgnore
    @Override // com.fossil.ed1
    public void a() {
        T t = this.b;
        if (t instanceof BitmapDrawable) {
            ((BitmapDrawable) t).getBitmap().prepareToDraw();
        } else if (t instanceof hh1) {
            ((hh1) t).e().prepareToDraw();
        }
    }

    @DexIgnore
    /* renamed from: e */
    public final T get() {
        Drawable.ConstantState constantState = this.b.getConstantState();
        return constantState == null ? this.b : (T) constantState.newDrawable();
    }
}
