package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sg0 extends DataSetObservable {
    @DexIgnore
    public static /* final */ String n; // = sg0.class.getSimpleName();
    @DexIgnore
    public static /* final */ Object o; // = new Object();
    @DexIgnore
    public static /* final */ Map<String, sg0> p; // = new HashMap();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f3253a; // = new Object();
    @DexIgnore
    public /* final */ List<a> b; // = new ArrayList();
    @DexIgnore
    public /* final */ List<d> c; // = new ArrayList();
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public Intent f;
    @DexIgnore
    public b g; // = new c();
    @DexIgnore
    public int h; // = 50;
    @DexIgnore
    public boolean i; // = true;
    @DexIgnore
    public boolean j; // = false;
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public boolean l; // = false;
    @DexIgnore
    public e m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Comparable<a> {
        @DexIgnore
        public /* final */ ResolveInfo b;
        @DexIgnore
        public float c;

        @DexIgnore
        public a(ResolveInfo resolveInfo) {
            this.b = resolveInfo;
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(a aVar) {
            return Float.floatToIntBits(aVar.c) - Float.floatToIntBits(this.c);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (a.class != obj.getClass()) {
                return false;
            }
            return Float.floatToIntBits(this.c) == Float.floatToIntBits(((a) obj).c);
        }

        @DexIgnore
        public int hashCode() {
            return Float.floatToIntBits(this.c) + 31;
        }

        @DexIgnore
        public String toString() {
            return "[resolveInfo:" + this.b.toString() + "; weight:" + new BigDecimal((double) this.c) + "]";
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Intent intent, List<a> list, List<d> list2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Map<ComponentName, a> f3254a; // = new HashMap();

        @DexIgnore
        @Override // com.fossil.sg0.b
        public void a(Intent intent, List<a> list, List<d> list2) {
            float f;
            Map<ComponentName, a> map = this.f3254a;
            map.clear();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                a aVar = list.get(i);
                aVar.c = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                ActivityInfo activityInfo = aVar.b.activityInfo;
                map.put(new ComponentName(activityInfo.packageName, activityInfo.name), aVar);
            }
            float f2 = 1.0f;
            for (int size2 = list2.size() - 1; size2 >= 0; size2--) {
                d dVar = list2.get(size2);
                a aVar2 = map.get(dVar.f3255a);
                if (aVar2 != null) {
                    aVar2.c = (dVar.c * f2) + aVar2.c;
                    f = 0.95f * f2;
                } else {
                    f = f2;
                }
                f2 = f;
            }
            Collections.sort(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ComponentName f3255a;
        @DexIgnore
        public /* final */ long b;
        @DexIgnore
        public /* final */ float c;

        @DexIgnore
        public d(ComponentName componentName, long j, float f) {
            this.f3255a = componentName;
            this.b = j;
            this.c = f;
        }

        @DexIgnore
        public d(String str, long j, float f) {
            this(ComponentName.unflattenFromString(str), j, f);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (d.class != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            ComponentName componentName = this.f3255a;
            if (componentName == null) {
                if (dVar.f3255a != null) {
                    return false;
                }
            } else if (!componentName.equals(dVar.f3255a)) {
                return false;
            }
            if (this.b != dVar.b) {
                return false;
            }
            return Float.floatToIntBits(this.c) == Float.floatToIntBits(dVar.c);
        }

        @DexIgnore
        public int hashCode() {
            ComponentName componentName = this.f3255a;
            int hashCode = componentName == null ? 0 : componentName.hashCode();
            long j = this.b;
            return ((((hashCode + 31) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + Float.floatToIntBits(this.c);
        }

        @DexIgnore
        public String toString() {
            return "[; activity:" + this.f3255a + "; time:" + this.b + "; weight:" + new BigDecimal((double) this.c) + "]";
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        boolean a(sg0 sg0, Intent intent);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f extends AsyncTask<Object, Void, Void> {
        @DexIgnore
        public f() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0077, code lost:
            if (r3 != null) goto L_0x0079;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
            r3.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x009c, code lost:
            if (r3 == null) goto L_0x007c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x00be, code lost:
            if (r3 == null) goto L_0x007c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x00e0, code lost:
            if (r3 == null) goto L_0x007c;
         */
        @DexIgnore
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Void doInBackground(java.lang.Object... r13) {
            /*
            // Method dump skipped, instructions count: 268
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.sg0.f.doInBackground(java.lang.Object[]):java.lang.Void");
        }
    }

    @DexIgnore
    public sg0(Context context, String str) {
        this.d = context.getApplicationContext();
        if (TextUtils.isEmpty(str) || str.endsWith(".xml")) {
            this.e = str;
            return;
        }
        this.e = str + ".xml";
    }

    @DexIgnore
    public static sg0 d(Context context, String str) {
        sg0 sg0;
        synchronized (o) {
            sg0 = p.get(str);
            if (sg0 == null) {
                sg0 = new sg0(context, str);
                p.put(str, sg0);
            }
        }
        return sg0;
    }

    @DexIgnore
    public final boolean a(d dVar) {
        boolean add = this.c.add(dVar);
        if (add) {
            this.k = true;
            l();
            k();
            r();
            notifyChanged();
        }
        return add;
    }

    @DexIgnore
    public Intent b(int i2) {
        synchronized (this.f3253a) {
            if (this.f == null) {
                return null;
            }
            c();
            a aVar = this.b.get(i2);
            ComponentName componentName = new ComponentName(aVar.b.activityInfo.packageName, aVar.b.activityInfo.name);
            Intent intent = new Intent(this.f);
            intent.setComponent(componentName);
            if (this.m != null) {
                if (this.m.a(this, new Intent(intent))) {
                    return null;
                }
            }
            a(new d(componentName, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }

    @DexIgnore
    public final void c() {
        boolean j2 = j();
        boolean m2 = m();
        l();
        if (j2 || m2) {
            r();
            notifyChanged();
        }
    }

    @DexIgnore
    public ResolveInfo e(int i2) {
        ResolveInfo resolveInfo;
        synchronized (this.f3253a) {
            c();
            resolveInfo = this.b.get(i2).b;
        }
        return resolveInfo;
    }

    @DexIgnore
    public int f() {
        int size;
        synchronized (this.f3253a) {
            c();
            size = this.b.size();
        }
        return size;
    }

    @DexIgnore
    public int g(ResolveInfo resolveInfo) {
        synchronized (this.f3253a) {
            c();
            List<a> list = this.b;
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (list.get(i2).b == resolveInfo) {
                    return i2;
                }
            }
            return -1;
        }
    }

    @DexIgnore
    public ResolveInfo h() {
        synchronized (this.f3253a) {
            c();
            if (this.b.isEmpty()) {
                return null;
            }
            return this.b.get(0).b;
        }
    }

    @DexIgnore
    public int i() {
        int size;
        synchronized (this.f3253a) {
            c();
            size = this.c.size();
        }
        return size;
    }

    @DexIgnore
    public final boolean j() {
        if (!this.l || this.f == null) {
            return false;
        }
        this.l = false;
        this.b.clear();
        List<ResolveInfo> queryIntentActivities = this.d.getPackageManager().queryIntentActivities(this.f, 0);
        int size = queryIntentActivities.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.b.add(new a(queryIntentActivities.get(i2)));
        }
        return true;
    }

    @DexIgnore
    public final void k() {
        if (!this.j) {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        } else if (this.k) {
            this.k = false;
            if (!TextUtils.isEmpty(this.e)) {
                new f().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new ArrayList(this.c), this.e);
            }
        }
    }

    @DexIgnore
    public final void l() {
        int size = this.c.size() - this.h;
        if (size > 0) {
            this.k = true;
            for (int i2 = 0; i2 < size; i2++) {
                this.c.remove(0);
            }
        }
    }

    @DexIgnore
    public final boolean m() {
        if (!this.i || !this.k || TextUtils.isEmpty(this.e)) {
            return false;
        }
        this.i = false;
        this.j = true;
        n();
        return true;
    }

    @DexIgnore
    public final void n() {
        try {
            FileInputStream openFileInput = this.d.openFileInput(this.e);
            try {
                XmlPullParser newPullParser = Xml.newPullParser();
                newPullParser.setInput(openFileInput, "UTF-8");
                int i2 = 0;
                while (i2 != 1 && i2 != 2) {
                    i2 = newPullParser.next();
                }
                if ("historical-records".equals(newPullParser.getName())) {
                    List<d> list = this.c;
                    list.clear();
                    while (true) {
                        int next = newPullParser.next();
                        if (next == 1) {
                            if (openFileInput == null) {
                                return;
                            }
                        } else if (!(next == 3 || next == 4)) {
                            if ("historical-record".equals(newPullParser.getName())) {
                                list.add(new d(newPullParser.getAttributeValue(null, Constants.ACTIVITY), Long.parseLong(newPullParser.getAttributeValue(null, LogBuilder.KEY_TIME)), Float.parseFloat(newPullParser.getAttributeValue(null, Constants.PROFILE_KEY_UNITS_WEIGHT))));
                            } else {
                                throw new XmlPullParserException("Share records file not well-formed.");
                            }
                        }
                    }
                    try {
                        openFileInput.close();
                    } catch (IOException e2) {
                    }
                } else {
                    throw new XmlPullParserException("Share records file does not start with historical-records tag.");
                }
            } catch (XmlPullParserException e3) {
                String str = n;
                Log.e(str, "Error reading historical recrod file: " + this.e, e3);
                if (openFileInput == null) {
                }
            } catch (IOException e4) {
                String str2 = n;
                Log.e(str2, "Error reading historical recrod file: " + this.e, e4);
                if (openFileInput == null) {
                }
            } catch (Throwable th) {
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException e5) {
                    }
                }
                throw th;
            }
        } catch (FileNotFoundException e6) {
        }
    }

    @DexIgnore
    public void o(int i2) {
        synchronized (this.f3253a) {
            c();
            a aVar = this.b.get(i2);
            a aVar2 = this.b.get(0);
            a(new d(new ComponentName(aVar.b.activityInfo.packageName, aVar.b.activityInfo.name), System.currentTimeMillis(), aVar2 != null ? (aVar2.c - aVar.c) + 5.0f : 1.0f));
        }
    }

    @DexIgnore
    public void p(Intent intent) {
        synchronized (this.f3253a) {
            if (this.f != intent) {
                this.f = intent;
                this.l = true;
                c();
            }
        }
    }

    @DexIgnore
    public void q(e eVar) {
        synchronized (this.f3253a) {
            this.m = eVar;
        }
    }

    @DexIgnore
    public final boolean r() {
        if (this.g == null || this.f == null || this.b.isEmpty() || this.c.isEmpty()) {
            return false;
        }
        this.g.a(this.f, this.b, Collections.unmodifiableList(this.c));
        return true;
    }
}
