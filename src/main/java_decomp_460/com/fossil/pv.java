package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pv extends ps {
    @DexIgnore
    public /* final */ n6 G;
    @DexIgnore
    public /* final */ n6 H;
    @DexIgnore
    public /* final */ byte[] I;
    @DexIgnore
    public byte[] J;
    @DexIgnore
    public /* final */ boolean K;
    @DexIgnore
    public n6 L;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ pv(short s, k5 k5Var, int i, int i2) {
        super(hs.X, k5Var, (i2 & 4) != 0 ? 3 : i);
        n6 n6Var = n6.FTC;
        this.G = n6Var;
        this.H = n6Var;
        byte[] array = ByteBuffer.allocate(11).order(ByteOrder.LITTLE_ENDIAN).put(sv.LEGACY_GET_FILE.b).putShort(s).putInt(0).putInt(1).array();
        pq7.b(array, "ByteBuffer.allocate(11)\n\u2026ILE)\n            .array()");
        this.I = array;
        byte[] array2 = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).put(sv.LEGACY_GET_FILE.a()).put((byte) 0).putShort(s).array();
        pq7.b(array2, "ByteBuffer.allocate(4)\n \u2026dle)\n            .array()");
        this.J = array2;
        this.L = n6.FTD;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public mt E(byte b) {
        return kt.k.a(b);
    }

    @DexIgnore
    @Override // com.fossil.ps
    public JSONObject F(byte[] bArr) {
        JSONObject jSONObject = new JSONObject();
        this.E = this.v.d != lw.b;
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public boolean G(o7 o7Var) {
        return o7Var.f2638a == this.L;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public void J(o7 o7Var) {
        if ((this.s ? jx.b.b(this.y.x, this.L, o7Var.b) : o7Var.b).length == 0) {
            this.v = mw.a(this.v, null, null, lw.e, null, null, 27);
        }
        this.E = true;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public n6 K() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] M() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public n6 N() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public boolean O() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] P() {
        return this.J;
    }
}
