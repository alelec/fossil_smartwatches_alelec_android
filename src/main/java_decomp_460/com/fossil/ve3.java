package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.MessengerShareContentUtility;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ve3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ve3> CREATOR; // = new jf3();
    @DexIgnore
    public static /* final */ ve3 c; // = new ve3(0);
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public ve3(int i) {
        this.b = i;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ve3)) {
            return false;
        }
        return this.b == ((ve3) obj).b;
    }

    @DexIgnore
    public final int hashCode() {
        return pc2.b(Integer.valueOf(this.b));
    }

    @DexIgnore
    public final String toString() {
        String str;
        int i = this.b;
        if (i == 0) {
            str = MessengerShareContentUtility.PREVIEW_DEFAULT;
        } else if (i != 1) {
            str = String.format("UNKNOWN(%s)", Integer.valueOf(i));
        } else {
            str = "OUTDOOR";
        }
        return String.format("StreetViewSource:%s", str);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.n(parcel, 2, this.b);
        bd2.b(parcel, a2);
    }
}
