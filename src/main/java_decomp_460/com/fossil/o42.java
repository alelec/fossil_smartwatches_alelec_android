package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o42 {
    @DexIgnore
    public static /* final */ Lock c; // = new ReentrantLock();
    @DexIgnore
    public static o42 d;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Lock f2630a; // = new ReentrantLock();
    @DexIgnore
    public /* final */ SharedPreferences b;

    @DexIgnore
    public o42(Context context) {
        this.b = context.getSharedPreferences("com.google.android.gms.signin", 0);
    }

    @DexIgnore
    public static o42 b(Context context) {
        rc2.k(context);
        c.lock();
        try {
            if (d == null) {
                d = new o42(context.getApplicationContext());
            }
            return d;
        } finally {
            c.unlock();
        }
    }

    @DexIgnore
    public static String h(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 1 + String.valueOf(str2).length());
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        return sb.toString();
    }

    @DexIgnore
    public void a() {
        this.f2630a.lock();
        try {
            this.b.edit().clear().apply();
        } finally {
            this.f2630a.unlock();
        }
    }

    @DexIgnore
    public GoogleSignInAccount c() {
        return i(k("defaultGoogleSignInAccount"));
    }

    @DexIgnore
    public GoogleSignInOptions d() {
        return j(k("defaultGoogleSignInAccount"));
    }

    @DexIgnore
    public String e() {
        return k(Constants.PROFILE_KEY_REFRESH_TOKEN);
    }

    @DexIgnore
    public void f(GoogleSignInAccount googleSignInAccount, GoogleSignInOptions googleSignInOptions) {
        rc2.k(googleSignInAccount);
        rc2.k(googleSignInOptions);
        g("defaultGoogleSignInAccount", googleSignInAccount.t0());
        rc2.k(googleSignInAccount);
        rc2.k(googleSignInOptions);
        String t0 = googleSignInAccount.t0();
        g(h("googleSignInAccount", t0), googleSignInAccount.u0());
        g(h("googleSignInOptions", t0), googleSignInOptions.v0());
    }

    @DexIgnore
    public final void g(String str, String str2) {
        this.f2630a.lock();
        try {
            this.b.edit().putString(str, str2).apply();
        } finally {
            this.f2630a.unlock();
        }
    }

    @DexIgnore
    public final GoogleSignInAccount i(String str) {
        String k;
        if (TextUtils.isEmpty(str) || (k = k(h("googleSignInAccount", str))) == null) {
            return null;
        }
        try {
            return GoogleSignInAccount.r0(k);
        } catch (JSONException e) {
            return null;
        }
    }

    @DexIgnore
    public final GoogleSignInOptions j(String str) {
        String k;
        if (TextUtils.isEmpty(str) || (k = k(h("googleSignInOptions", str))) == null) {
            return null;
        }
        try {
            return GoogleSignInOptions.p0(k);
        } catch (JSONException e) {
            return null;
        }
    }

    @DexIgnore
    public final String k(String str) {
        this.f2630a.lock();
        try {
            return this.b.getString(str, null);
        } finally {
            this.f2630a.unlock();
        }
    }

    @DexIgnore
    public final void l() {
        String k = k("defaultGoogleSignInAccount");
        m("defaultGoogleSignInAccount");
        if (!TextUtils.isEmpty(k)) {
            m(h("googleSignInAccount", k));
            m(h("googleSignInOptions", k));
        }
    }

    @DexIgnore
    public final void m(String str) {
        this.f2630a.lock();
        try {
            this.b.edit().remove(str).apply();
        } finally {
            this.f2630a.unlock();
        }
    }
}
