package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yi0 extends Drawable {
    @DexIgnore
    public static /* final */ double q; // = Math.cos(Math.toRadians(45.0d));
    @DexIgnore
    public static a r;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f4320a;
    @DexIgnore
    public Paint b;
    @DexIgnore
    public Paint c;
    @DexIgnore
    public Paint d;
    @DexIgnore
    public /* final */ RectF e;
    @DexIgnore
    public float f;
    @DexIgnore
    public Path g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public boolean l; // = true;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public /* final */ int n;
    @DexIgnore
    public boolean o; // = true;
    @DexIgnore
    public boolean p; // = false;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Canvas canvas, RectF rectF, float f, Paint paint);
    }

    @DexIgnore
    public yi0(Resources resources, ColorStateList colorStateList, float f2, float f3, float f4) {
        this.m = resources.getColor(oi0.cardview_shadow_start_color);
        this.n = resources.getColor(oi0.cardview_shadow_end_color);
        this.f4320a = resources.getDimensionPixelSize(pi0.cardview_compat_inset_shadow);
        this.b = new Paint(5);
        n(colorStateList);
        Paint paint = new Paint(5);
        this.c = paint;
        paint.setStyle(Paint.Style.FILL);
        this.f = (float) ((int) (0.5f + f2));
        this.e = new RectF();
        Paint paint2 = new Paint(this.c);
        this.d = paint2;
        paint2.setAntiAlias(false);
        s(f3, f4);
    }

    @DexIgnore
    public static float c(float f2, float f3, boolean z) {
        return z ? (float) (((double) f2) + ((1.0d - q) * ((double) f3))) : f2;
    }

    @DexIgnore
    public static float d(float f2, float f3, boolean z) {
        return z ? (float) (((double) (1.5f * f2)) + ((1.0d - q) * ((double) f3))) : 1.5f * f2;
    }

    @DexIgnore
    public final void a(Rect rect) {
        float f2 = this.h;
        float f3 = 1.5f * f2;
        this.e.set(((float) rect.left) + f2, ((float) rect.top) + f3, ((float) rect.right) - f2, ((float) rect.bottom) - f3);
        b();
    }

    @DexIgnore
    public final void b() {
        float f2 = this.f;
        RectF rectF = new RectF(-f2, -f2, f2, f2);
        RectF rectF2 = new RectF(rectF);
        float f3 = this.i;
        rectF2.inset(-f3, -f3);
        Path path = this.g;
        if (path == null) {
            this.g = new Path();
        } else {
            path.reset();
        }
        this.g.setFillType(Path.FillType.EVEN_ODD);
        this.g.moveTo(-this.f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.g.rLineTo(-this.i, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.g.arcTo(rectF2, 180.0f, 90.0f, false);
        this.g.arcTo(rectF, 270.0f, -90.0f, false);
        this.g.close();
        float f4 = this.f;
        Paint paint = this.c;
        float f5 = this.f;
        float f6 = this.i;
        int i2 = this.m;
        int i3 = this.n;
        Shader.TileMode tileMode = Shader.TileMode.CLAMP;
        paint.setShader(new RadialGradient((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f5 + f6, new int[]{i2, i2, i3}, new float[]{0.0f, f4 / (this.i + f4), 1.0f}, tileMode));
        Paint paint2 = this.d;
        float f7 = this.f;
        float f8 = this.i;
        int i4 = this.m;
        int i5 = this.n;
        int[] iArr = {i4, i4, i5};
        paint2.setShader(new LinearGradient((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (-f7) + f8, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (-f7) - f8, iArr, new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0.5f, 1.0f}, Shader.TileMode.CLAMP));
        this.d.setAntiAlias(false);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        if (this.l) {
            a(getBounds());
            this.l = false;
        }
        canvas.translate(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.j / 2.0f);
        e(canvas);
        canvas.translate(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (-this.j) / 2.0f);
        r.a(canvas, this.e, this.f, this.b);
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        float f2 = this.f;
        float f3 = (-f2) - this.i;
        float f4 = f2 + ((float) this.f4320a) + (this.j / 2.0f);
        float f5 = f4 * 2.0f;
        boolean z = this.e.width() - f5 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z2 = this.e.height() - f5 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int save = canvas.save();
        RectF rectF = this.e;
        canvas.translate(rectF.left + f4, rectF.top + f4);
        canvas.drawPath(this.g, this.c);
        if (z) {
            canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3, this.e.width() - f5, -this.f, this.d);
        }
        canvas.restoreToCount(save);
        int save2 = canvas.save();
        RectF rectF2 = this.e;
        canvas.translate(rectF2.right - f4, rectF2.bottom - f4);
        canvas.rotate(180.0f);
        canvas.drawPath(this.g, this.c);
        if (z) {
            canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3, this.e.width() - f5, this.i + (-this.f), this.d);
        }
        canvas.restoreToCount(save2);
        int save3 = canvas.save();
        RectF rectF3 = this.e;
        canvas.translate(rectF3.left + f4, rectF3.bottom - f4);
        canvas.rotate(270.0f);
        canvas.drawPath(this.g, this.c);
        if (z2) {
            canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3, this.e.height() - f5, -this.f, this.d);
        }
        canvas.restoreToCount(save3);
        int save4 = canvas.save();
        RectF rectF4 = this.e;
        canvas.translate(rectF4.right - f4, rectF4.top + f4);
        canvas.rotate(90.0f);
        canvas.drawPath(this.g, this.c);
        if (z2) {
            canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3, this.e.height() - f5, -this.f, this.d);
        }
        canvas.restoreToCount(save4);
    }

    @DexIgnore
    public ColorStateList f() {
        return this.k;
    }

    @DexIgnore
    public float g() {
        return this.f;
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        int ceil = (int) Math.ceil((double) d(this.h, this.f, this.o));
        int ceil2 = (int) Math.ceil((double) c(this.h, this.f, this.o));
        rect.set(ceil2, ceil, ceil2, ceil);
        return true;
    }

    @DexIgnore
    public void h(Rect rect) {
        getPadding(rect);
    }

    @DexIgnore
    public float i() {
        return this.h;
    }

    @DexIgnore
    public boolean isStateful() {
        ColorStateList colorStateList = this.k;
        return (colorStateList != null && colorStateList.isStateful()) || super.isStateful();
    }

    @DexIgnore
    public float j() {
        float f2 = this.h;
        return (Math.max(f2, this.f + ((float) this.f4320a) + ((f2 * 1.5f) / 2.0f)) * 2.0f) + (((this.h * 1.5f) + ((float) this.f4320a)) * 2.0f);
    }

    @DexIgnore
    public float k() {
        float f2 = this.h;
        return (Math.max(f2, this.f + ((float) this.f4320a) + (f2 / 2.0f)) * 2.0f) + ((this.h + ((float) this.f4320a)) * 2.0f);
    }

    @DexIgnore
    public float l() {
        return this.j;
    }

    @DexIgnore
    public void m(boolean z) {
        this.o = z;
        invalidateSelf();
    }

    @DexIgnore
    public final void n(ColorStateList colorStateList) {
        if (colorStateList == null) {
            colorStateList = ColorStateList.valueOf(0);
        }
        this.k = colorStateList;
        this.b.setColor(colorStateList.getColorForState(getState(), this.k.getDefaultColor()));
    }

    @DexIgnore
    public void o(ColorStateList colorStateList) {
        n(colorStateList);
        invalidateSelf();
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.l = true;
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        ColorStateList colorStateList = this.k;
        int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
        if (this.b.getColor() == colorForState) {
            return false;
        }
        this.b.setColor(colorForState);
        this.l = true;
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public void p(float f2) {
        if (f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float f3 = (float) ((int) (0.5f + f2));
            if (this.f != f3) {
                this.f = f3;
                this.l = true;
                invalidateSelf();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Invalid radius " + f2 + ". Must be >= 0");
    }

    @DexIgnore
    public void q(float f2) {
        s(this.j, f2);
    }

    @DexIgnore
    public void r(float f2) {
        s(f2, this.h);
    }

    @DexIgnore
    public final void s(float f2, float f3) {
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            throw new IllegalArgumentException("Invalid shadow size " + f2 + ". Must be >= 0");
        } else if (f3 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float t = (float) t(f2);
            float t2 = (float) t(f3);
            if (t > t2) {
                if (!this.p) {
                    this.p = true;
                }
                t = t2;
            }
            if (this.j != t || this.h != t2) {
                this.j = t;
                this.h = t2;
                this.i = (float) ((int) ((t * 1.5f) + ((float) this.f4320a) + 0.5f));
                this.l = true;
                invalidateSelf();
            }
        } else {
            throw new IllegalArgumentException("Invalid max shadow size " + f3 + ". Must be >= 0");
        }
    }

    @DexIgnore
    public void setAlpha(int i2) {
        this.b.setAlpha(i2);
        this.c.setAlpha(i2);
        this.d.setAlpha(i2);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.b.setColorFilter(colorFilter);
    }

    @DexIgnore
    public final int t(float f2) {
        int i2 = (int) (0.5f + f2);
        return i2 % 2 == 1 ? i2 - 1 : i2;
    }
}
