package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xs2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<xs2> CREATOR; // = new at2();
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ Bundle h;

    @DexIgnore
    public xs2(long j, long j2, boolean z, String str, String str2, String str3, Bundle bundle) {
        this.b = j;
        this.c = j2;
        this.d = z;
        this.e = str;
        this.f = str2;
        this.g = str3;
        this.h = bundle;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.r(parcel, 1, this.b);
        bd2.r(parcel, 2, this.c);
        bd2.c(parcel, 3, this.d);
        bd2.u(parcel, 4, this.e, false);
        bd2.u(parcel, 5, this.f, false);
        bd2.u(parcel, 6, this.g, false);
        bd2.e(parcel, 7, this.h, false);
        bd2.b(parcel, a2);
    }
}
