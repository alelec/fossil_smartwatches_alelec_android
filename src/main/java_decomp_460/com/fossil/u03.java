package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u03 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ s03<?> f3500a; // = new r03();
    @DexIgnore
    public static /* final */ s03<?> b; // = c();

    @DexIgnore
    public static s03<?> a() {
        return f3500a;
    }

    @DexIgnore
    public static s03<?> b() {
        s03<?> s03 = b;
        if (s03 != null) {
            return s03;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }

    @DexIgnore
    public static s03<?> c() {
        try {
            return (s03) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e) {
            return null;
        }
    }
}
