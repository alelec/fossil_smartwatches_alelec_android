package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ua7 extends qv5 {
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public vg5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ua7 a() {
            return new ua7();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ua7 b;

        @DexIgnore
        public b(ua7 ua7) {
            this.b = ua7;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchFaceGalleryActivity.a aVar = WatchFaceGalleryActivity.B;
            Context requireContext = this.b.requireContext();
            pq7.b(requireContext, "requireContext()");
            aVar.a(requireContext);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        vg5 c = vg5.c(layoutInflater, viewGroup, false);
        this.h = c;
        if (c != null) {
            return c.b();
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        vg5 vg5 = this.h;
        if (vg5 != null) {
            vg5.b.setOnClickListener(new b(this));
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
