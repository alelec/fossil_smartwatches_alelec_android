package com.fossil;

import android.graphics.Bitmap;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cg1 extends zf1 {
    @DexIgnore
    public static /* final */ byte[] b; // = "com.bumptech.glide.load.resource.bitmap.CenterCrop".getBytes(mb1.f2349a);

    @DexIgnore
    @Override // com.fossil.mb1
    public void a(MessageDigest messageDigest) {
        messageDigest.update(b);
    }

    @DexIgnore
    @Override // com.fossil.zf1
    public Bitmap c(rd1 rd1, Bitmap bitmap, int i, int i2) {
        return tg1.b(rd1, bitmap, i, i2);
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public boolean equals(Object obj) {
        return obj instanceof cg1;
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public int hashCode() {
        return -599754482;
    }
}
