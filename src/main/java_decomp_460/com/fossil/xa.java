package com.fossil;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xa extends vx1<nu1[], byte[]> {
    @DexIgnore
    public static /* final */ qx1<nu1[]>[] b; // = {new na(), new qa()};
    @DexIgnore
    public static /* final */ rx1<byte[]>[] c; // = {new ta(ob.ASSET.c), new va(ob.ASSET.c)};
    @DexIgnore
    public static /* final */ xa d; // = new xa();

    @DexIgnore
    @Override // com.fossil.vx1
    public qx1<nu1[]>[] b() {
        return b;
    }

    @DexIgnore
    @Override // com.fossil.vx1
    public rx1<byte[]>[] c() {
        return c;
    }

    @DexIgnore
    public final byte[] g(nu1[] nu1Arr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ArrayList arrayList = new ArrayList();
        for (nu1 nu1 : nu1Arr) {
            byte[] a2 = nu1.a();
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        for (byte[] bArr : pm7.C(arrayList)) {
            byteArrayOutputStream.write(bArr);
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        pq7.b(byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public final byte[] h(qu1[] qu1Arr, short s, ry1 ry1) {
        if (qu1Arr.length == 0) {
            return new byte[0];
        }
        try {
            return a(s, ry1, qu1Arr);
        } catch (sx1 e) {
            return new byte[0];
        }
    }
}
