package com.fossil;

import java.net.URL;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class to3 implements Runnable {
    @DexIgnore
    public /* final */ URL b;
    @DexIgnore
    public /* final */ uo3 c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ Map<String, String> e; // = null;
    @DexIgnore
    public /* final */ /* synthetic */ ro3 f;

    @DexIgnore
    public to3(ro3 ro3, String str, URL url, byte[] bArr, Map<String, String> map, uo3 uo3) {
        this.f = ro3;
        rc2.g(str);
        rc2.k(url);
        rc2.k(uo3);
        this.b = url;
        this.c = uo3;
        this.d = str;
    }

    @DexIgnore
    public final /* synthetic */ void a(int i, Exception exc, byte[] bArr, Map map) {
        this.c.a(this.d, i, exc, bArr, map);
    }

    @DexIgnore
    public final void b(int i, Exception exc, byte[] bArr, Map<String, List<String>> map) {
        this.f.c().y(new wo3(this, i, exc, bArr, map));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0060  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r6 = this;
            r2 = 0
            r4 = 0
            com.fossil.ro3 r0 = r6.f
            r0.g()
            com.fossil.ro3 r0 = r6.f     // Catch:{ IOException -> 0x0073, all -> 0x006e }
            java.net.URL r1 = r6.b     // Catch:{ IOException -> 0x0073, all -> 0x006e }
            java.net.HttpURLConnection r5 = r0.t(r1)     // Catch:{ IOException -> 0x0073, all -> 0x006e }
            java.util.Map<java.lang.String, java.lang.String> r0 = r6.e     // Catch:{ IOException -> 0x0039, all -> 0x006a }
            if (r0 == 0) goto L_0x0045
            java.util.Map<java.lang.String, java.lang.String> r0 = r6.e     // Catch:{ IOException -> 0x0039, all -> 0x006a }
            java.util.Set r0 = r0.entrySet()     // Catch:{ IOException -> 0x0039, all -> 0x006a }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ IOException -> 0x0039, all -> 0x006a }
        L_0x001d:
            boolean r0 = r3.hasNext()     // Catch:{ IOException -> 0x0039, all -> 0x006a }
            if (r0 == 0) goto L_0x0045
            java.lang.Object r0 = r3.next()     // Catch:{ IOException -> 0x0039, all -> 0x006a }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ IOException -> 0x0039, all -> 0x006a }
            java.lang.Object r1 = r0.getKey()     // Catch:{ IOException -> 0x0039, all -> 0x006a }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ IOException -> 0x0039, all -> 0x006a }
            java.lang.Object r0 = r0.getValue()     // Catch:{ IOException -> 0x0039, all -> 0x006a }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x0039, all -> 0x006a }
            r5.addRequestProperty(r1, r0)     // Catch:{ IOException -> 0x0039, all -> 0x006a }
            goto L_0x001d
        L_0x0039:
            r0 = move-exception
        L_0x003a:
            r3 = r4
            r1 = r2
        L_0x003c:
            if (r5 == 0) goto L_0x0041
            r5.disconnect()
        L_0x0041:
            r6.b(r1, r0, r4, r3)
        L_0x0044:
            return
        L_0x0045:
            int r1 = r5.getResponseCode()
            java.util.Map r2 = r5.getHeaderFields()     // Catch:{ IOException -> 0x0078, all -> 0x007b }
            com.fossil.ro3 r0 = r6.f     // Catch:{ IOException -> 0x0067, all -> 0x005c }
            byte[] r0 = com.fossil.ro3.u(r0, r5)     // Catch:{ IOException -> 0x0067, all -> 0x005c }
            if (r5 == 0) goto L_0x0058
            r5.disconnect()
        L_0x0058:
            r6.b(r1, r4, r0, r2)
            goto L_0x0044
        L_0x005c:
            r0 = move-exception
            r3 = r2
        L_0x005e:
            if (r5 == 0) goto L_0x0063
            r5.disconnect()
        L_0x0063:
            r6.b(r1, r4, r4, r3)
            throw r0
        L_0x0067:
            r0 = move-exception
            r3 = r2
            goto L_0x003c
        L_0x006a:
            r0 = move-exception
            r1 = r2
        L_0x006c:
            r3 = r4
            goto L_0x005e
        L_0x006e:
            r0 = move-exception
            r3 = r4
            r5 = r4
            r1 = r2
            goto L_0x005e
        L_0x0073:
            r0 = move-exception
            r3 = r4
            r5 = r4
            r1 = r2
            goto L_0x003c
        L_0x0078:
            r0 = move-exception
            r2 = r1
            goto L_0x003a
        L_0x007b:
            r0 = move-exception
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.to3.run():void");
    }
}
