package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ke3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ls2 f1904a;

    @DexIgnore
    public ke3(ls2 ls2) {
        rc2.k(ls2);
        this.f1904a = ls2;
    }

    @DexIgnore
    public final String a() {
        try {
            return this.f1904a.getId();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final LatLng b() {
        try {
            return this.f1904a.getPosition();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void c() {
        try {
            this.f1904a.K0();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final boolean d() {
        try {
            return this.f1904a.y1();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void e() {
        try {
            this.f1904a.remove();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof ke3)) {
            return false;
        }
        try {
            return this.f1904a.I1(((ke3) obj).f1904a);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void f(float f) {
        try {
            this.f1904a.setAlpha(f);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void g(float f, float f2) {
        try {
            this.f1904a.setAnchor(f, f2);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void h(boolean z) {
        try {
            this.f1904a.setDraggable(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return this.f1904a.a();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void i(boolean z) {
        try {
            this.f1904a.setFlat(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void j(zd3 zd3) {
        if (zd3 == null) {
            try {
                this.f1904a.X0(null);
            } catch (RemoteException e) {
                throw new se3(e);
            }
        } else {
            this.f1904a.X0(zd3.a());
        }
    }

    @DexIgnore
    public final void k(float f, float f2) {
        try {
            this.f1904a.setInfoWindowAnchor(f, f2);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void l(LatLng latLng) {
        if (latLng != null) {
            try {
                this.f1904a.setPosition(latLng);
            } catch (RemoteException e) {
                throw new se3(e);
            }
        } else {
            throw new IllegalArgumentException("latlng cannot be null - a position is required.");
        }
    }

    @DexIgnore
    public final void m(float f) {
        try {
            this.f1904a.setRotation(f);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void n(String str) {
        try {
            this.f1904a.e2(str);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void o(String str) {
        try {
            this.f1904a.setTitle(str);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void p(boolean z) {
        try {
            this.f1904a.setVisible(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void q(float f) {
        try {
            this.f1904a.setZIndex(f);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void r() {
        try {
            this.f1904a.t();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }
}
