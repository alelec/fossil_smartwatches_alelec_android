package com.fossil;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class js3 extends zc2 implements z62 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<js3> CREATOR; // = new is3();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public Intent d;

    @DexIgnore
    public js3() {
        this(0, null);
    }

    @DexIgnore
    public js3(int i, int i2, Intent intent) {
        this.b = i;
        this.c = i2;
        this.d = intent;
    }

    @DexIgnore
    public js3(int i, Intent intent) {
        this(2, 0, null);
    }

    @DexIgnore
    @Override // com.fossil.z62
    public final Status a() {
        return this.c == 0 ? Status.f : Status.j;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.n(parcel, 1, this.b);
        bd2.n(parcel, 2, this.c);
        bd2.t(parcel, 3, this.d, i, false);
        bd2.b(parcel, a2);
    }
}
