package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.pc2;
import com.google.android.gms.fitness.data.DataType;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yo2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<yo2> CREATOR; // = new ap2();
    @DexIgnore
    public /* final */ List<DataType> b;

    @DexIgnore
    public yo2(List<DataType> list) {
        this.b = list;
    }

    @DexIgnore
    public final List<DataType> c() {
        return Collections.unmodifiableList(this.b);
    }

    @DexIgnore
    public final String toString() {
        pc2.a c = pc2.c(this);
        c.a("dataTypes", this.b);
        return c.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.y(parcel, 1, Collections.unmodifiableList(this.b), false);
        bd2.b(parcel, a2);
    }
}
