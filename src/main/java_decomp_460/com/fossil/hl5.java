package com.fossil;

import com.fossil.jn5;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hl5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ hl5 f1493a; // = new hl5();

    @DexIgnore
    public final List<jn5.a> a(List<oo5> list) {
        pq7.c(list, "buttons");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            jn5.a e = f1493a.e(it.next().a());
            if (e != null) {
                arrayList2.add(e);
            }
        }
        arrayList.addAll(arrayList2);
        return arrayList;
    }

    @DexIgnore
    public final jn5.a b(String str) {
        pq7.c(str, "complicationId");
        int hashCode = str.hashCode();
        if (hashCode != -48173007) {
            if (hashCode == 1223440372 && str.equals("weather")) {
                return jn5.a.SET_COMPLICATION_WATCH_APP_WEATHER;
            }
        } else if (str.equals("chance-of-rain")) {
            return jn5.a.SET_COMPLICATION_CHANCE_OF_RAIN;
        }
        return null;
    }

    @DexIgnore
    public final List<jn5.a> c(HybridPreset hybridPreset) {
        pq7.c(hybridPreset, "hybridPreset");
        ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = buttons.iterator();
        while (it.hasNext()) {
            jn5.a d = f1493a.d(it.next().getAppId());
            if (d != null) {
                arrayList.add(d);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final jn5.a d(String str) {
        pq7.c(str, "microAppid");
        if (pq7.a(str, MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
            return jn5.a.SET_MICRO_APP_MUSIC;
        }
        if (pq7.a(str, MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
            return jn5.a.SET_MICRO_APP_COMMUTE_TIME;
        }
        return null;
    }

    @DexIgnore
    public final jn5.a e(String str) {
        pq7.c(str, "watchAppId");
        int hashCode = str.hashCode();
        if (hashCode != -829740640) {
            if (hashCode != 104263205) {
                if (hashCode == 1223440372 && str.equals("weather")) {
                    return jn5.a.SET_COMPLICATION_WATCH_APP_WEATHER;
                }
            } else if (str.equals(Constants.MUSIC)) {
                return jn5.a.SET_WATCH_APP_MUSIC;
            }
        } else if (str.equals("commute-time")) {
            return jn5.a.SET_WATCH_APP_COMMUTE_TIME;
        }
        return null;
    }
}
