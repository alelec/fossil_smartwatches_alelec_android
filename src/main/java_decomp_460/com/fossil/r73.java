package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r73 implements s73 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f3087a;
    @DexIgnore
    public static /* final */ xv2<Boolean> b;
    @DexIgnore
    public static /* final */ xv2<Boolean> c;
    @DexIgnore
    public static /* final */ xv2<Boolean> d;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f3087a = hw2.d("measurement.sdk.collection.enable_extend_user_property_size", true);
        b = hw2.d("measurement.sdk.collection.last_deep_link_referrer2", true);
        c = hw2.d("measurement.sdk.collection.last_deep_link_referrer_campaign2", false);
        d = hw2.d("measurement.sdk.collection.last_gclid_from_referrer2", false);
        hw2.b("measurement.id.sdk.collection.last_deep_link_referrer2", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.s73
    public final boolean zza() {
        return f3087a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.s73
    public final boolean zzb() {
        return b.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.s73
    public final boolean zzc() {
        return c.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.s73
    public final boolean zzd() {
        return d.o().booleanValue();
    }
}
