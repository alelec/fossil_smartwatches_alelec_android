package com.fossil;

import com.fossil.tn7;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface xw7 extends tn7.b {
    @DexIgnore
    public static final b r = b.f4199a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* synthetic */ void a(xw7 xw7, CancellationException cancellationException, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    cancellationException = null;
                }
                xw7.D(cancellationException);
                return;
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: cancel");
        }

        @DexIgnore
        public static <R> R b(xw7 xw7, R r, vp7<? super R, ? super tn7.b, ? extends R> vp7) {
            return (R) tn7.b.a.a(xw7, r, vp7);
        }

        @DexIgnore
        public static <E extends tn7.b> E c(xw7 xw7, tn7.c<E> cVar) {
            return (E) tn7.b.a.b(xw7, cVar);
        }

        @DexIgnore
        public static /* synthetic */ dw7 d(xw7 xw7, boolean z, boolean z2, rp7 rp7, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    z = false;
                }
                if ((i & 2) != 0) {
                    z2 = true;
                }
                return xw7.j(z, z2, rp7);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: invokeOnCompletion");
        }

        @DexIgnore
        public static tn7 e(xw7 xw7, tn7.c<?> cVar) {
            return tn7.b.a.c(xw7, cVar);
        }

        @DexIgnore
        public static tn7 f(xw7 xw7, tn7 tn7) {
            return tn7.b.a.d(xw7, tn7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tn7.c<xw7> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ b f4199a; // = new b();

        /*
        static {
            CoroutineExceptionHandler.a aVar = CoroutineExceptionHandler.q;
        }
        */
    }

    @DexIgnore
    dw7 A(rp7<? super Throwable, tl7> rp7);

    @DexIgnore
    void D(CancellationException cancellationException);

    @DexIgnore
    qu7 L(su7 su7);

    @DexIgnore
    boolean isActive();

    @DexIgnore
    dw7 j(boolean z, boolean z2, rp7<? super Throwable, tl7> rp7);

    @DexIgnore
    CancellationException k();

    @DexIgnore
    boolean start();
}
