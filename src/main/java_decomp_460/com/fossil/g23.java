package com.fossil;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g23<K, V> extends LinkedHashMap<K, V> {
    @DexIgnore
    public static /* final */ g23 b;
    @DexIgnore
    public boolean zza; // = true;

    /*
    static {
        g23 g23 = new g23();
        b = g23;
        g23.zza = false;
    }
    */

    @DexIgnore
    public g23() {
    }

    @DexIgnore
    public g23(Map<K, V> map) {
        super(map);
    }

    @DexIgnore
    public static int e(Object obj) {
        if (obj instanceof byte[]) {
            return h13.j((byte[]) obj);
        }
        if (!(obj instanceof g13)) {
            return obj.hashCode();
        }
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public static <K, V> g23<K, V> zza() {
        return b;
    }

    @DexIgnore
    public final void clear() {
        g();
        super.clear();
    }

    @DexIgnore
    @Override // java.util.LinkedHashMap, java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final Set<Map.Entry<K, V>> entrySet() {
        return isEmpty() ? Collections.emptySet() : super.entrySet();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0017  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean equals(java.lang.Object r7) {
        /*
            r6 = this;
            r3 = 1
            r4 = 0
            boolean r0 = r7 instanceof java.util.Map
            if (r0 == 0) goto L_0x005f
            java.util.Map r7 = (java.util.Map) r7
            if (r6 == r7) goto L_0x005d
            int r0 = r6.size()
            int r1 = r7.size()
            if (r0 == r1) goto L_0x0019
        L_0x0014:
            r0 = r4
        L_0x0015:
            if (r0 == 0) goto L_0x005f
            r0 = r3
        L_0x0018:
            return r0
        L_0x0019:
            java.util.Set r0 = r6.entrySet()
            java.util.Iterator r5 = r0.iterator()
        L_0x0021:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x005d
            java.lang.Object r0 = r5.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getKey()
            boolean r1 = r7.containsKey(r1)
            if (r1 == 0) goto L_0x0014
            java.lang.Object r1 = r0.getValue()
            java.lang.Object r0 = r0.getKey()
            java.lang.Object r2 = r7.get(r0)
            boolean r0 = r1 instanceof byte[]
            if (r0 == 0) goto L_0x0058
            boolean r0 = r2 instanceof byte[]
            if (r0 == 0) goto L_0x0058
            r0 = r1
            byte[] r0 = (byte[]) r0
            r1 = r2
            byte[] r1 = (byte[]) r1
            boolean r0 = java.util.Arrays.equals(r0, r1)
        L_0x0055:
            if (r0 != 0) goto L_0x0021
            goto L_0x0014
        L_0x0058:
            boolean r0 = r1.equals(r2)
            goto L_0x0055
        L_0x005d:
            r0 = r3
            goto L_0x0015
        L_0x005f:
            r0 = r4
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.g23.equals(java.lang.Object):boolean");
    }

    @DexIgnore
    public final void g() {
        if (!this.zza) {
            throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final int hashCode() {
        int i = 0;
        for (Map.Entry<K, V> entry : entrySet()) {
            i = (e(entry.getValue()) ^ e(entry.getKey())) + i;
        }
        return i;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final V put(K k, V v) {
        g();
        h13.d(k);
        h13.d(v);
        return (V) super.put(k, v);
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final void putAll(Map<? extends K, ? extends V> map) {
        g();
        for (Object obj : map.keySet()) {
            h13.d(obj);
            h13.d(map.get(obj));
        }
        super.putAll(map);
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final V remove(Object obj) {
        g();
        return (V) super.remove(obj);
    }

    @DexIgnore
    public final void zza(g23<K, V> g23) {
        g();
        if (!g23.isEmpty()) {
            putAll(g23);
        }
    }

    @DexIgnore
    public final g23<K, V> zzb() {
        return isEmpty() ? new g23<>() : new g23<>(this);
    }

    @DexIgnore
    public final void zzc() {
        this.zza = false;
    }

    @DexIgnore
    public final boolean zzd() {
        return this.zza;
    }
}
