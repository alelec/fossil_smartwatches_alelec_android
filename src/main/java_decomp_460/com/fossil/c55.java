package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepDayChart;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c55 extends b55 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d G; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray H;
    @DexIgnore
    public /* final */ NestedScrollView E;
    @DexIgnore
    public long F;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        H = sparseIntArray;
        sparseIntArray.put(2131362040, 1);
        H.put(2131363277, 2);
        H.put(2131363478, 3);
        H.put(2131362696, 4);
        H.put(2131362078, 5);
        H.put(2131363350, 6);
        H.put(2131363492, 7);
        H.put(2131362704, 8);
        H.put(2131362062, 9);
        H.put(2131363310, 10);
        H.put(2131363485, 11);
        H.put(2131362703, 12);
        H.put(2131363122, 13);
        H.put(2131362262, 14);
    }
    */

    @DexIgnore
    public c55(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 15, G, H));
    }

    @DexIgnore
    public c55(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[9], (ConstraintLayout) objArr[5], (FlexibleButton) objArr[14], (ImageView) objArr[4], (ImageView) objArr[12], (ImageView) objArr[8], (OverviewSleepDayChart) objArr[13], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[6], (View) objArr[3], (View) objArr[11], (View) objArr[7]);
        this.F = -1;
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.E = nestedScrollView;
        nestedScrollView.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.F = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.F != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.F = 1;
        }
        w();
    }
}
