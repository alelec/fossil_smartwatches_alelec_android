package com.fossil;

import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l14<T> extends g14<T> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ T reference;

    @DexIgnore
    public l14(T t) {
        this.reference = t;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public Set<T> asSet() {
        return Collections.singleton(this.reference);
    }

    @DexIgnore
    @Override // com.fossil.g14
    public boolean equals(Object obj) {
        if (obj instanceof l14) {
            return this.reference.equals(((l14) obj).reference);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public T get() {
        return this.reference;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public int hashCode() {
        return this.reference.hashCode() + 1502476572;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public boolean isPresent() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public g14<T> or(g14<? extends T> g14) {
        i14.l(g14);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public T or(m14<? extends T> m14) {
        i14.l(m14);
        return this.reference;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public T or(T t) {
        i14.m(t, "use Optional.orNull() instead of Optional.or(null)");
        return this.reference;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public T orNull() {
        return this.reference;
    }

    @DexIgnore
    @Override // com.fossil.g14
    public String toString() {
        return "Optional.of(" + ((Object) this.reference) + ")";
    }

    @DexIgnore
    @Override // com.fossil.g14
    public <V> g14<V> transform(b14<? super T, V> b14) {
        V apply = b14.apply(this.reference);
        i14.m(apply, "the Function passed to Optional.transform() must not return null.");
        return new l14(apply);
    }
}
