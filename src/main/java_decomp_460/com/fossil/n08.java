package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n08 implements Runnable {
    @DexIgnore
    public long b;
    @DexIgnore
    public o08 c;

    @DexIgnore
    public n08() {
        this(0, m08.c);
    }

    @DexIgnore
    public n08(long j, o08 o08) {
        this.b = j;
        this.c = o08;
    }
}
