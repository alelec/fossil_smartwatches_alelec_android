package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xp4 implements Factory<n47> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f4156a;

    @DexIgnore
    public xp4(uo4 uo4) {
        this.f4156a = uo4;
    }

    @DexIgnore
    public static xp4 a(uo4 uo4) {
        return new xp4(uo4);
    }

    @DexIgnore
    public static n47 c(uo4 uo4) {
        n47 E = uo4.E();
        lk7.c(E, "Cannot return null from a non-@Nullable @Provides method");
        return E;
    }

    @DexIgnore
    /* renamed from: b */
    public n47 get() {
        return c(this.f4156a);
    }
}
