package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gv1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ hv1[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<gv1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public gv1 createFromParcel(Parcel parcel) {
            Parcelable[] readParcelableArray = parcel.readParcelableArray(hv1.class.getClassLoader());
            if (readParcelableArray != null) {
                return new gv1((hv1[]) readParcelableArray);
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.model.preset.DevicePresetItem>");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public gv1[] newArray(int i) {
            return new gv1[i];
        }
    }

    @DexIgnore
    public gv1(hv1[] hv1Arr) {
        this.b = hv1Arr;
        a();
    }

    @DexIgnore
    public final void a() {
        ArrayList arrayList = new ArrayList();
        for (em1 em1 : dm7.n(this.b, em1.class)) {
            arrayList.addAll(em1.c());
        }
        hv1[] hv1Arr = this.b;
        for (hv1 hv1 : hv1Arr) {
            if (hv1 instanceof sl1) {
                mm7.w(((sl1) hv1).c(), new tb0(arrayList));
            }
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final hv1[] getPresetItems() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONArray jSONArray = new JSONArray();
        for (hv1 hv1 : this.b) {
            jSONArray.put(hv1.toJSONObject());
        }
        return g80.k(new JSONObject(), jd0.g3, jSONArray);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelableArray(this.b, i);
        }
    }
}
