package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cv1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ qu1 b;
    @DexIgnore
    public /* final */ Hashtable<mo1, qu1> c; // = new Hashtable<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<cv1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public cv1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(qu1.class.getClassLoader());
            if (readParcelable != null) {
                pq7.b(readParcelable, "parcel.readParcelable<No\u2026class.java.classLoader)!!");
                cv1 cv1 = new cv1((qu1) readParcelable);
                Hashtable<mo1, qu1> hashtable = cv1.c;
                Serializable readSerializable = parcel.readSerializable();
                if (readSerializable != null) {
                    hashtable.putAll((Hashtable) readSerializable);
                    return cv1;
                }
                throw new il7("null cannot be cast to non-null type java.util.Hashtable<com.fossil.blesdk.device.data.notification.NotificationType, com.fossil.blesdk.model.file.NotificationIcon>");
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public cv1[] newArray(int i) {
            return new cv1[i];
        }
    }

    @DexIgnore
    public cv1(qu1 qu1) {
        this.b = qu1;
    }

    @DexIgnore
    public final qu1[] a() {
        Collection<qu1> values = this.c.values();
        pq7.b(values, "typeIcons.values");
        Object[] array = pm7.C(pm7.W(values, this.b)).toArray(new qu1[0]);
        if (array != null) {
            return (qu1[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final byte[] b() {
        int i;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        mo1[] values = mo1.values();
        int length = values.length;
        int i2 = 0;
        int i3 = 0;
        while (i3 < length) {
            mo1 mo1 = values[i3];
            qu1 qu1 = this.c.get(mo1);
            int ordinal = 1 << mo1.ordinal();
            if (qu1 == null || qu1.d()) {
                i = i2 | ordinal;
            } else {
                String fileName = qu1.getFileName();
                Charset c2 = hd0.y.c();
                if (fileName != null) {
                    byte[] bytes = fileName.getBytes(c2);
                    pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
                    byte[] p = dm7.p(bytes, (byte) 0);
                    ByteBuffer order = ByteBuffer.allocate(p.length + 3).order(ByteOrder.LITTLE_ENDIAN);
                    pq7.b(order, "ByteBuffer.allocate(TYPE\u2026(ByteOrder.LITTLE_ENDIAN)");
                    order.putShort((short) ordinal).put((byte) p.length).put(p);
                    byteArrayOutputStream.write(order.array());
                    i = i2;
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            }
            i3++;
            i2 = i;
        }
        if (i2 != 0) {
            String fileName2 = this.b.getFileName();
            Charset c3 = hd0.y.c();
            if (fileName2 != null) {
                byte[] bytes2 = fileName2.getBytes(c3);
                pq7.b(bytes2, "(this as java.lang.String).getBytes(charset)");
                byte[] p2 = dm7.p(bytes2, (byte) 0);
                ByteBuffer order2 = ByteBuffer.allocate(p2.length + 3).order(ByteOrder.LITTLE_ENDIAN);
                pq7.b(order2, "ByteBuffer.allocate(TYPE\u2026(ByteOrder.LITTLE_ENDIAN)");
                order2.putShort((short) i2).put((byte) p2.length).put(p2);
                byteArrayOutputStream.write(order2.array());
            } else {
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        pq7.b(byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(cv1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            cv1 cv1 = (cv1) obj;
            return pq7.a(this.b, cv1.b) && pq7.a(this.c, cv1.c);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.notification.filter.NotificationIconConfig");
    }

    @DexIgnore
    public final qu1 getDefaultIcon() {
        return this.b;
    }

    @DexIgnore
    public final qu1 getIconForType(mo1 mo1) {
        return this.c.get(mo1);
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    public final cv1 setIconForType(mo1 mo1, qu1 qu1) {
        if (qu1 != null) {
            this.c.put(mo1, qu1);
        } else {
            this.c.remove(mo1);
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject k = g80.k(new JSONObject(), jd0.a3, this.b.toJSONObject());
        for (Map.Entry<mo1, qu1> entry : this.c.entrySet()) {
            mo1 key = entry.getKey();
            pq7.b(key, "icon.key");
            k.put(ey1.a(key), entry.getValue().toJSONObject());
        }
        return k;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
        if (parcel != null) {
            parcel.writeSerializable(this.c);
        }
    }
}
