package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yh2 {
    @DexIgnore
    public static /* final */ wh2 A; // = wh2.A("body_temperature_measurement_location");
    @DexIgnore
    public static /* final */ wh2 B; // = wh2.A("cervical_mucus_texture");
    @DexIgnore
    public static /* final */ wh2 C; // = wh2.A("cervical_mucus_amount");
    @DexIgnore
    public static /* final */ wh2 D; // = wh2.A("cervical_position");
    @DexIgnore
    public static /* final */ wh2 E; // = wh2.A("cervical_dilation");
    @DexIgnore
    public static /* final */ wh2 F; // = wh2.A("cervical_firmness");
    @DexIgnore
    public static /* final */ wh2 G; // = wh2.A("menstrual_flow");
    @DexIgnore
    public static /* final */ wh2 H; // = wh2.A("ovulation_test_result");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ wh2 f4316a; // = wh2.D("blood_pressure_systolic");
    @DexIgnore
    public static /* final */ wh2 b; // = wh2.D("blood_pressure_systolic_average");
    @DexIgnore
    public static /* final */ wh2 c; // = wh2.D("blood_pressure_systolic_min");
    @DexIgnore
    public static /* final */ wh2 d; // = wh2.D("blood_pressure_systolic_max");
    @DexIgnore
    public static /* final */ wh2 e; // = wh2.D("blood_pressure_diastolic");
    @DexIgnore
    public static /* final */ wh2 f; // = wh2.D("blood_pressure_diastolic_average");
    @DexIgnore
    public static /* final */ wh2 g; // = wh2.D("blood_pressure_diastolic_min");
    @DexIgnore
    public static /* final */ wh2 h; // = wh2.D("blood_pressure_diastolic_max");
    @DexIgnore
    public static /* final */ wh2 i; // = wh2.A("body_position");
    @DexIgnore
    public static /* final */ wh2 j; // = wh2.A("blood_pressure_measurement_location");
    @DexIgnore
    public static /* final */ wh2 k; // = wh2.D("blood_glucose_level");
    @DexIgnore
    public static /* final */ wh2 l; // = wh2.A("temporal_relation_to_meal");
    @DexIgnore
    public static /* final */ wh2 m; // = wh2.A("temporal_relation_to_sleep");
    @DexIgnore
    public static /* final */ wh2 n; // = wh2.A("blood_glucose_specimen_source");
    @DexIgnore
    public static /* final */ wh2 o; // = wh2.D("oxygen_saturation");
    @DexIgnore
    public static /* final */ wh2 p; // = wh2.D("oxygen_saturation_average");
    @DexIgnore
    public static /* final */ wh2 q; // = wh2.D("oxygen_saturation_min");
    @DexIgnore
    public static /* final */ wh2 r; // = wh2.D("oxygen_saturation_max");
    @DexIgnore
    public static /* final */ wh2 s; // = wh2.D("supplemental_oxygen_flow_rate");
    @DexIgnore
    public static /* final */ wh2 t; // = wh2.D("supplemental_oxygen_flow_rate_average");
    @DexIgnore
    public static /* final */ wh2 u; // = wh2.D("supplemental_oxygen_flow_rate_min");
    @DexIgnore
    public static /* final */ wh2 v; // = wh2.D("supplemental_oxygen_flow_rate_max");
    @DexIgnore
    public static /* final */ wh2 w; // = wh2.A("oxygen_therapy_administration_mode");
    @DexIgnore
    public static /* final */ wh2 x; // = wh2.A("oxygen_saturation_system");
    @DexIgnore
    public static /* final */ wh2 y; // = wh2.A("oxygen_saturation_measurement_method");
    @DexIgnore
    public static /* final */ wh2 z; // = wh2.D("body_temperature");
}
