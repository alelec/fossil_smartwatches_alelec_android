package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n57 {
    @DexIgnore
    public static /* final */ ExecutorService f; // = Executors.newCachedThreadPool();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Resources f2471a;
    @DexIgnore
    public /* final */ WeakReference<Context> b;
    @DexIgnore
    public /* final */ m57 c;
    @DexIgnore
    public /* final */ Bitmap d;
    @DexIgnore
    public /* final */ b e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.n57$a$a")
        /* renamed from: com.fossil.n57$a$a  reason: collision with other inner class name */
        public class RunnableC0166a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ BitmapDrawable b;

            @DexIgnore
            public RunnableC0166a(BitmapDrawable bitmapDrawable) {
                this.b = bitmapDrawable;
            }

            @DexIgnore
            public void run() {
                n57.this.e.a(this.b);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            n57 n57 = n57.this;
            BitmapDrawable bitmapDrawable = new BitmapDrawable(n57.f2471a, i57.a(n57.this.b.get(), n57.d, n57.c));
            if (n57.this.e != null) {
                new Handler(Looper.getMainLooper()).post(new RunnableC0166a(bitmapDrawable));
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(BitmapDrawable bitmapDrawable);
    }

    @DexIgnore
    public n57(Context context, Bitmap bitmap, m57 m57, b bVar) {
        this.f2471a = context.getResources();
        this.c = m57;
        this.e = bVar;
        this.b = new WeakReference<>(context);
        this.d = bitmap;
    }

    @DexIgnore
    public void a() {
        f.execute(new a());
    }
}
