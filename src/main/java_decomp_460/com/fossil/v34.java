package com.fossil;

import com.fossil.v34.i;
import com.fossil.v34.n;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.j2objc.annotations.Weak;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v34<K, V, E extends i<K, V, E>, S extends n<K, V, E, S>> extends AbstractMap<K, V> implements ConcurrentMap<K, V>, Serializable {
    @DexIgnore
    public static /* final */ long CLEANUP_EXECUTOR_DELAY_SECS; // = 60;
    @DexIgnore
    public static /* final */ int CONTAINS_VALUE_RETRIES; // = 3;
    @DexIgnore
    public static /* final */ int DRAIN_MAX; // = 16;
    @DexIgnore
    public static /* final */ int DRAIN_THRESHOLD; // = 63;
    @DexIgnore
    public static /* final */ int MAXIMUM_CAPACITY; // = 1073741824;
    @DexIgnore
    public static /* final */ int MAX_SEGMENTS; // = 65536;
    @DexIgnore
    public static /* final */ b0<Object, Object, e> UNSET_WEAK_VALUE_REFERENCE; // = new a();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 5;
    @DexIgnore
    public /* final */ int concurrencyLevel;
    @DexIgnore
    public /* final */ transient j<K, V, E, S> entryHelper;
    @DexIgnore
    public transient Set<Map.Entry<K, V>> entrySet;
    @DexIgnore
    public /* final */ z04<Object> keyEquivalence;
    @DexIgnore
    public transient Set<K> keySet;
    @DexIgnore
    public /* final */ transient int segmentMask;
    @DexIgnore
    public /* final */ transient int segmentShift;
    @DexIgnore
    public /* final */ transient n<K, V, E, S>[] segments;
    @DexIgnore
    public transient Collection<V> values;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements b0<Object, Object, e> {
        @DexIgnore
        /* Return type fixed from 'com.fossil.v34$b0' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.ref.ReferenceQueue, com.fossil.v34$i] */
        @Override // com.fossil.v34.b0
        public /* bridge */ /* synthetic */ b0<Object, Object, e> b(ReferenceQueue<Object> referenceQueue, e eVar) {
            c(referenceQueue, eVar);
            return this;
        }

        @DexIgnore
        public b0<Object, Object, e> c(ReferenceQueue<Object> referenceQueue, e eVar) {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v34.b0
        public void clear() {
        }

        @DexIgnore
        /* renamed from: d */
        public e a() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.v34.b0
        public Object get() {
            return null;
        }
    }

    @DexIgnore
    public interface a0<K, V, E extends i<K, V, E>> extends i<K, V, E> {
        @DexIgnore
        b0<K, V, E> b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<K, V> extends m24<K, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 3;
        @DexIgnore
        public /* final */ int concurrencyLevel;
        @DexIgnore
        public transient ConcurrentMap<K, V> delegate;
        @DexIgnore
        public /* final */ z04<Object> keyEquivalence;
        @DexIgnore
        public /* final */ p keyStrength;
        @DexIgnore
        public /* final */ z04<Object> valueEquivalence;
        @DexIgnore
        public /* final */ p valueStrength;

        @DexIgnore
        public b(p pVar, p pVar2, z04<Object> z04, z04<Object> z042, int i, ConcurrentMap<K, V> concurrentMap) {
            this.keyStrength = pVar;
            this.valueStrength = pVar2;
            this.keyEquivalence = z04;
            this.valueEquivalence = z042;
            this.concurrencyLevel = i;
            this.delegate = concurrentMap;
        }

        @DexIgnore
        @Override // com.fossil.m24, com.fossil.m24, com.fossil.m24, com.fossil.n24, com.fossil.n24, com.fossil.o24
        public ConcurrentMap<K, V> delegate() {
            return this.delegate;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.concurrent.ConcurrentMap<K, V> */
        /* JADX WARN: Multi-variable type inference failed */
        public void readEntries(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            while (true) {
                Object readObject = objectInputStream.readObject();
                if (readObject != null) {
                    this.delegate.put(readObject, objectInputStream.readObject());
                } else {
                    return;
                }
            }
        }

        @DexIgnore
        public u34 readMapMaker(ObjectInputStream objectInputStream) throws IOException {
            int readInt = objectInputStream.readInt();
            u34 u34 = new u34();
            u34.g(readInt);
            u34.j(this.keyStrength);
            u34.k(this.valueStrength);
            u34.h(this.keyEquivalence);
            u34.a(this.concurrencyLevel);
            return u34;
        }

        @DexIgnore
        public void writeMapTo(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.writeInt(this.delegate.size());
            for (Map.Entry<K, V> entry : this.delegate.entrySet()) {
                objectOutputStream.writeObject(entry.getKey());
                objectOutputStream.writeObject(entry.getValue());
            }
            objectOutputStream.writeObject(null);
        }
    }

    @DexIgnore
    public interface b0<K, V, E extends i<K, V, E>> {
        @DexIgnore
        E a();

        @DexIgnore
        b0<K, V, E> b(ReferenceQueue<V> referenceQueue, E e);

        @DexIgnore
        Object clear();  // void declaration

        @DexIgnore
        V get();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<K, V, E extends i<K, V, E>> implements i<K, V, E> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ K f3702a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ E c;

        @DexIgnore
        public c(K k, int i, E e) {
            this.f3702a = k;
            this.b = i;
            this.c = e;
        }

        @DexIgnore
        @Override // com.fossil.v34.i
        public E a() {
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.v34.i
        public int c() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.v34.i
        public K getKey() {
            return this.f3702a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c0<K, V, E extends i<K, V, E>> extends WeakReference<V> implements b0<K, V, E> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ E f3703a;

        @DexIgnore
        public c0(ReferenceQueue<V> referenceQueue, V v, E e) {
            super(v, referenceQueue);
            this.f3703a = e;
        }

        @DexIgnore
        @Override // com.fossil.v34.b0
        public E a() {
            return this.f3703a;
        }

        @DexIgnore
        @Override // com.fossil.v34.b0
        public b0<K, V, E> b(ReferenceQueue<V> referenceQueue, E e) {
            return new c0(referenceQueue, get(), e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<K, V, E extends i<K, V, E>> extends WeakReference<K> implements i<K, V, E> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f3704a;
        @DexIgnore
        public /* final */ E b;

        @DexIgnore
        public d(ReferenceQueue<K> referenceQueue, K k, int i, E e) {
            super(k, referenceQueue);
            this.f3704a = i;
            this.b = e;
        }

        @DexIgnore
        @Override // com.fossil.v34.i
        public E a() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.v34.i
        public int c() {
            return this.f3704a;
        }

        @DexIgnore
        @Override // com.fossil.v34.i
        public K getKey() {
            return (K) get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d0 extends t14<K, V> {
        @DexIgnore
        public /* final */ K b;
        @DexIgnore
        public V c;

        @DexIgnore
        public d0(K k, V v) {
            this.b = k;
            this.c = v;
        }

        @DexIgnore
        @Override // com.fossil.t14
        public boolean equals(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return this.b.equals(entry.getKey()) && this.c.equals(entry.getValue());
        }

        @DexIgnore
        @Override // java.util.Map.Entry, com.fossil.t14
        public K getKey() {
            return this.b;
        }

        @DexIgnore
        @Override // java.util.Map.Entry, com.fossil.t14
        public V getValue() {
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.t14
        public int hashCode() {
            return this.b.hashCode() ^ this.c.hashCode();
        }

        @DexIgnore
        @Override // java.util.Map.Entry, com.fossil.t14
        public V setValue(V v) {
            V v2 = (V) v34.this.put(this.b, v);
            this.c = v;
            return v2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements i<Object, Object, e> {
        @DexIgnore
        public e() {
            throw new AssertionError();
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.v34$i' to match base method */
        @Override // com.fossil.v34.i
        public /* bridge */ /* synthetic */ e a() {
            d();
            throw null;
        }

        @DexIgnore
        @Override // com.fossil.v34.i
        public int c() {
            throw new AssertionError();
        }

        @DexIgnore
        public e d() {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // com.fossil.v34.i
        public Object getKey() {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // com.fossil.v34.i
        public Object getValue() {
            throw new AssertionError();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f extends v34<K, V, E, S>.h {
        @DexIgnore
        public f(v34 v34) {
            super();
        }

        @DexIgnore
        /* renamed from: f */
        public Map.Entry<K, V> next() {
            return c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g extends m<Map.Entry<K, V>> {
        @DexIgnore
        public g() {
            super(null);
        }

        @DexIgnore
        public void clear() {
            v34.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            Map.Entry entry;
            Object key;
            Object obj2;
            return (obj instanceof Map.Entry) && (key = (entry = (Map.Entry) obj).getKey()) != null && (obj2 = v34.this.get(key)) != null && v34.this.valueEquivalence().equivalent(entry.getValue(), obj2);
        }

        @DexIgnore
        public boolean isEmpty() {
            return v34.this.isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<Map.Entry<K, V>> iterator() {
            return new f(v34.this);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            Map.Entry entry;
            Object key;
            return (obj instanceof Map.Entry) && (key = (entry = (Map.Entry) obj).getKey()) != null && v34.this.remove(key, entry.getValue());
        }

        @DexIgnore
        public int size() {
            return v34.this.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class h<T> implements Iterator<T> {
        @DexIgnore
        public int b;
        @DexIgnore
        public int c; // = -1;
        @DexIgnore
        public n<K, V, E, S> d;
        @DexIgnore
        public AtomicReferenceArray<E> e;
        @DexIgnore
        public E f;
        @DexIgnore
        public v34<K, V, E, S>.d0 g;
        @DexIgnore
        public v34<K, V, E, S>.d0 h;

        @DexIgnore
        public h() {
            this.b = v34.this.segments.length - 1;
            a();
        }

        @DexIgnore
        public final void a() {
            this.g = null;
            if (!d() && !e()) {
                while (true) {
                    int i2 = this.b;
                    if (i2 >= 0) {
                        n<K, V, E, S>[] nVarArr = v34.this.segments;
                        this.b = i2 - 1;
                        n<K, V, E, S> nVar = nVarArr[i2];
                        this.d = nVar;
                        if (nVar.count != 0) {
                            AtomicReferenceArray<E> atomicReferenceArray = this.d.table;
                            this.e = atomicReferenceArray;
                            this.c = atomicReferenceArray.length() - 1;
                            if (e()) {
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }

        @DexIgnore
        public boolean b(E e2) {
            boolean z;
            try {
                Object key = e2.getKey();
                Object liveValue = v34.this.getLiveValue(e2);
                if (liveValue != null) {
                    this.g = new d0(key, liveValue);
                    z = true;
                } else {
                    z = false;
                }
                return z;
            } finally {
                this.d.postReadCleanup();
            }
        }

        @DexIgnore
        public v34<K, V, E, S>.d0 c() {
            v34<K, V, E, S>.d0 d0Var = this.g;
            if (d0Var != null) {
                this.h = d0Var;
                a();
                return this.h;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public boolean d() {
            E e2 = this.f;
            if (e2 != null) {
                while (true) {
                    this.f = (E) e2.a();
                    E e3 = this.f;
                    if (e3 == null) {
                        break;
                    } else if (b(e3)) {
                        return true;
                    } else {
                        e2 = this.f;
                    }
                }
            }
            return false;
        }

        @DexIgnore
        public boolean e() {
            while (true) {
                int i2 = this.c;
                if (i2 < 0) {
                    return false;
                }
                AtomicReferenceArray<E> atomicReferenceArray = this.e;
                this.c = i2 - 1;
                E e2 = atomicReferenceArray.get(i2);
                this.f = e2;
                if (e2 == null || (!b(e2) && !d())) {
                }
            }
            return true;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.g != null;
        }

        @DexIgnore
        public void remove() {
            a24.c(this.h != null);
            v34.this.remove(this.h.getKey());
            this.h = null;
        }
    }

    @DexIgnore
    public interface i<K, V, E extends i<K, V, E>> {
        @DexIgnore
        E a();

        @DexIgnore
        int c();

        @DexIgnore
        K getKey();

        @DexIgnore
        V getValue();
    }

    @DexIgnore
    public interface j<K, V, E extends i<K, V, E>, S extends n<K, V, E, S>> {
        @DexIgnore
        E a(S s, E e, E e2);

        @DexIgnore
        p b();

        @DexIgnore
        p c();

        @DexIgnore
        void d(S s, E e, V v);

        @DexIgnore
        S e(v34<K, V, E, S> v34, int i, int i2);

        @DexIgnore
        E f(S s, K k, int i, E e);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k extends v34<K, V, E, S>.h {
        @DexIgnore
        public k(v34 v34) {
            super();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public K next() {
            return (K) c().getKey();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l extends m<K> {
        @DexIgnore
        public l() {
            super(null);
        }

        @DexIgnore
        public void clear() {
            v34.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return v34.this.containsKey(obj);
        }

        @DexIgnore
        public boolean isEmpty() {
            return v34.this.isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<K> iterator() {
            return new k(v34.this);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            return v34.this.remove(obj) != null;
        }

        @DexIgnore
        public int size() {
            return v34.this.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class m<E> extends AbstractSet<E> {
        @DexIgnore
        public m() {
        }

        @DexIgnore
        public /* synthetic */ m(a aVar) {
            this();
        }

        @DexIgnore
        public Object[] toArray() {
            return v34.a(this).toArray();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public <E> E[] toArray(E[] eArr) {
            return (E[]) v34.a(this).toArray(eArr);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class n<K, V, E extends i<K, V, E>, S extends n<K, V, E, S>> extends ReentrantLock {
        @DexIgnore
        public volatile int count;
        @DexIgnore
        @Weak
        public /* final */ v34<K, V, E, S> map;
        @DexIgnore
        public /* final */ int maxSegmentSize;
        @DexIgnore
        public int modCount;
        @DexIgnore
        public /* final */ AtomicInteger readCount; // = new AtomicInteger();
        @DexIgnore
        public volatile AtomicReferenceArray<E> table;
        @DexIgnore
        public int threshold;

        @DexIgnore
        public n(v34<K, V, E, S> v34, int i, int i2) {
            this.map = v34;
            this.maxSegmentSize = i2;
            initTable(newEntryArray(i));
        }

        @DexIgnore
        public static <K, V, E extends i<K, V, E>> boolean isCollected(E e) {
            return e.getValue() == null;
        }

        @DexIgnore
        public abstract E castForTesting(i<K, V, ?> iVar);

        @DexIgnore
        public void clear() {
            if (this.count != 0) {
                lock();
                try {
                    AtomicReferenceArray<E> atomicReferenceArray = this.table;
                    for (int i = 0; i < atomicReferenceArray.length(); i++) {
                        atomicReferenceArray.set(i, null);
                    }
                    maybeClearReferenceQueues();
                    this.readCount.set(0);
                    this.modCount++;
                    this.count = 0;
                } finally {
                    unlock();
                }
            }
        }

        @DexIgnore
        public <T> void clearReferenceQueue(ReferenceQueue<T> referenceQueue) {
            do {
            } while (referenceQueue.poll() != null);
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: com.fossil.v34$n<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public boolean clearValueForTesting(K k, int i, b0<K, V, ? extends i<K, V, ?>> b0Var) {
            lock();
            try {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (i iVar = e; iVar != null; iVar = iVar.a()) {
                    Object key = iVar.getKey();
                    if (iVar.c() == i && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        if (((a0) iVar).b() == b0Var) {
                            atomicReferenceArray.set(length, removeFromChain(e, iVar));
                            unlock();
                            return true;
                        } else {
                            unlock();
                            return false;
                        }
                    }
                }
                unlock();
                return false;
            } catch (Throwable th) {
                unlock();
                throw th;
            }
        }

        @DexIgnore
        public boolean containsKey(Object obj, int i) {
            boolean z = false;
            try {
                if (this.count != 0) {
                    E liveEntry = getLiveEntry(obj, i);
                    if (!(liveEntry == null || liveEntry.getValue() == null)) {
                        z = true;
                    }
                } else {
                    postReadCleanup();
                }
                return z;
            } finally {
                postReadCleanup();
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.fossil.v34$n<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public boolean containsValue(Object obj) {
            try {
                if (this.count != 0) {
                    AtomicReferenceArray<E> atomicReferenceArray = this.table;
                    int length = atomicReferenceArray.length();
                    for (int i = 0; i < length; i++) {
                        for (E e = atomicReferenceArray.get(i); e != null; e = e.a()) {
                            Object liveValue = getLiveValue(e);
                            if (liveValue != null && this.map.valueEquivalence().equivalent(obj, liveValue)) {
                                postReadCleanup();
                                return true;
                            }
                        }
                    }
                }
                postReadCleanup();
                return false;
            } catch (Throwable th) {
                postReadCleanup();
                throw th;
            }
        }

        @DexIgnore
        public E copyEntry(E e, E e2) {
            return this.map.entryHelper.a(self(), e, e2);
        }

        @DexIgnore
        public E copyForTesting(i<K, V, ?> iVar, i<K, V, ?> iVar2) {
            return this.map.entryHelper.a(self(), castForTesting(iVar), castForTesting(iVar2));
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.v34<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public void drainKeyReferenceQueue(ReferenceQueue<K> referenceQueue) {
            int i = 0;
            while (true) {
                Reference<? extends K> poll = referenceQueue.poll();
                if (poll != null) {
                    this.map.reclaimKey((i) poll);
                    i++;
                    if (i == 16) {
                        return;
                    }
                } else {
                    return;
                }
            }
        }

        @DexIgnore
        public void drainValueReferenceQueue(ReferenceQueue<V> referenceQueue) {
            int i = 0;
            while (true) {
                Reference<? extends V> poll = referenceQueue.poll();
                if (poll != null) {
                    this.map.reclaimValue((b0) poll);
                    i++;
                    if (i == 16) {
                        return;
                    }
                } else {
                    return;
                }
            }
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX DEBUG: Type inference failed for r9v0. Raw type applied. Possible types: java.util.concurrent.atomic.AtomicReferenceArray<E extends com.fossil.v34$i<K, V, E>>, java.util.concurrent.atomic.AtomicReferenceArray */
        /* JADX WARN: Type inference failed for: r11v0, types: [com.fossil.v34$n<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>>, com.fossil.v34$n] */
        /* JADX WARN: Type inference failed for: r4v0, types: [com.fossil.v34$i] */
        /* JADX WARN: Type inference failed for: r4v3, types: [com.fossil.v34$i] */
        /* JADX WARNING: Unknown variable types count: 2 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void expand() {
            /*
                r11 = this;
                java.util.concurrent.atomic.AtomicReferenceArray<E extends com.fossil.v34$i<K, V, E>> r7 = r11.table
                int r8 = r7.length()
                r0 = 1073741824(0x40000000, float:2.0)
                if (r8 < r0) goto L_0x000b
            L_0x000a:
                return
            L_0x000b:
                int r5 = r11.count
                int r0 = r8 << 1
                java.util.concurrent.atomic.AtomicReferenceArray r9 = r11.newEntryArray(r0)
                int r0 = r9.length()
                int r0 = r0 * 3
                int r0 = r0 / 4
                r11.threshold = r0
                int r0 = r9.length()
                int r10 = r0 + -1
                r0 = 0
                r6 = r0
            L_0x0025:
                if (r6 >= r8) goto L_0x007b
                java.lang.Object r0 = r7.get(r6)
                com.fossil.v34$i r0 = (com.fossil.v34.i) r0
                if (r0 == 0) goto L_0x0082
                com.fossil.v34$i r4 = r0.a()
                int r1 = r0.c()
                r2 = r1 & r10
                if (r4 != 0) goto L_0x0044
                r9.set(r2, r0)
                r1 = r5
            L_0x003f:
                int r0 = r6 + 1
                r6 = r0
                r5 = r1
                goto L_0x0025
            L_0x0044:
                r3 = r0
            L_0x0045:
                if (r4 == 0) goto L_0x0055
                int r1 = r4.c()
                r1 = r1 & r10
                if (r1 == r2) goto L_0x0080
                r3 = r4
            L_0x004f:
                com.fossil.v34$i r4 = r4.a()
                r2 = r1
                goto L_0x0045
            L_0x0055:
                r9.set(r2, r3)
                r2 = r0
                r1 = r5
            L_0x005a:
                if (r2 == r3) goto L_0x003f
                int r0 = r2.c()
                r4 = r0 & r10
                java.lang.Object r0 = r9.get(r4)
                com.fossil.v34$i r0 = (com.fossil.v34.i) r0
                com.fossil.v34$i r0 = r11.copyEntry(r2, r0)
                if (r0 == 0) goto L_0x0078
                r9.set(r4, r0)
                r0 = r1
            L_0x0072:
                com.fossil.v34$i r2 = r2.a()
                r1 = r0
                goto L_0x005a
            L_0x0078:
                int r0 = r1 + -1
                goto L_0x0072
            L_0x007b:
                r11.table = r9
                r11.count = r5
                goto L_0x000a
            L_0x0080:
                r1 = r2
                goto L_0x004f
            L_0x0082:
                r1 = r5
                goto L_0x003f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.v34.n.expand():void");
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public V get(Object obj, int i) {
            try {
                E liveEntry = getLiveEntry(obj, i);
                if (liveEntry == null) {
                    postReadCleanup();
                    return null;
                }
                V v = (V) liveEntry.getValue();
                if (v == null) {
                    tryDrainReferenceQueues();
                }
                postReadCleanup();
                return v;
            } catch (Throwable th) {
                postReadCleanup();
                throw th;
            }
        }

        @DexIgnore
        public E getEntry(Object obj, int i) {
            if (this.count != 0) {
                for (E first = getFirst(i); first != null; first = (E) first.a()) {
                    if (first.c() == i) {
                        Object key = first.getKey();
                        if (key == null) {
                            tryDrainReferenceQueues();
                        } else if (this.map.keyEquivalence.equivalent(obj, key)) {
                            return first;
                        }
                    }
                }
            }
            return null;
        }

        @DexIgnore
        public E getFirst(int i) {
            AtomicReferenceArray<E> atomicReferenceArray = this.table;
            return atomicReferenceArray.get((atomicReferenceArray.length() - 1) & i);
        }

        @DexIgnore
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            throw new AssertionError();
        }

        @DexIgnore
        public E getLiveEntry(Object obj, int i) {
            return getEntry(obj, i);
        }

        @DexIgnore
        public V getLiveValue(E e) {
            if (e.getKey() == null) {
                tryDrainReferenceQueues();
                return null;
            }
            V v = (V) e.getValue();
            if (v != null) {
                return v;
            }
            tryDrainReferenceQueues();
            return null;
        }

        @DexIgnore
        public V getLiveValueForTesting(i<K, V, ?> iVar) {
            return getLiveValue(castForTesting(iVar));
        }

        @DexIgnore
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            throw new AssertionError();
        }

        @DexIgnore
        public b0<K, V, E> getWeakValueReferenceForTesting(i<K, V, ?> iVar) {
            throw new AssertionError();
        }

        @DexIgnore
        public void initTable(AtomicReferenceArray<E> atomicReferenceArray) {
            int length = (atomicReferenceArray.length() * 3) / 4;
            this.threshold = length;
            if (length == this.maxSegmentSize) {
                this.threshold = length + 1;
            }
            this.table = atomicReferenceArray;
        }

        @DexIgnore
        public void maybeClearReferenceQueues() {
        }

        @DexIgnore
        public void maybeDrainReferenceQueues() {
        }

        @DexIgnore
        public AtomicReferenceArray<E> newEntryArray(int i) {
            return new AtomicReferenceArray<>(i);
        }

        @DexIgnore
        public E newEntryForTesting(K k, int i, i<K, V, ?> iVar) {
            return this.map.entryHelper.f(self(), k, i, castForTesting(iVar));
        }

        @DexIgnore
        public b0<K, V, E> newWeakValueReferenceForTesting(i<K, V, ?> iVar, V v) {
            throw new AssertionError();
        }

        @DexIgnore
        public void postReadCleanup() {
            if ((this.readCount.incrementAndGet() & 63) == 0) {
                runCleanup();
            }
        }

        @DexIgnore
        public void preWriteCleanup() {
            runLockedCleanup();
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: com.fossil.v34$n<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public V put(K k, int i, V v, boolean z) {
            int i2;
            lock();
            try {
                preWriteCleanup();
                int i3 = this.count + 1;
                if (i3 > this.threshold) {
                    expand();
                    i2 = this.count + 1;
                } else {
                    i2 = i3;
                }
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (i iVar = e; iVar != null; iVar = iVar.a()) {
                    Object key = iVar.getKey();
                    if (iVar.c() == i && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        V v2 = (V) iVar.getValue();
                        if (v2 == null) {
                            this.modCount++;
                            setValue(iVar, v);
                            this.count = this.count;
                            return null;
                        } else if (z) {
                            unlock();
                            return v2;
                        } else {
                            this.modCount++;
                            setValue(iVar, v);
                            unlock();
                            return v2;
                        }
                    }
                }
                this.modCount++;
                E f = this.map.entryHelper.f(self(), k, i, e);
                setValue(f, v);
                atomicReferenceArray.set(length, f);
                this.count = i2;
                unlock();
                return null;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.v34$n<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public boolean reclaimKey(E e, int i) {
            lock();
            try {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = i & (atomicReferenceArray.length() - 1);
                E e2 = atomicReferenceArray.get(length);
                for (i iVar = e2; iVar != null; iVar = iVar.a()) {
                    if (iVar == e) {
                        this.modCount++;
                        E removeFromChain = removeFromChain(e2, iVar);
                        int i2 = this.count;
                        atomicReferenceArray.set(length, removeFromChain);
                        this.count = i2 - 1;
                        unlock();
                        return true;
                    }
                }
                unlock();
                return false;
            } catch (Throwable th) {
                unlock();
                throw th;
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: com.fossil.v34$n<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public boolean reclaimValue(K k, int i, b0<K, V, E> b0Var) {
            lock();
            try {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (i iVar = e; iVar != null; iVar = iVar.a()) {
                    Object key = iVar.getKey();
                    if (iVar.c() == i && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        if (((a0) iVar).b() == b0Var) {
                            this.modCount++;
                            E removeFromChain = removeFromChain(e, iVar);
                            int i2 = this.count;
                            atomicReferenceArray.set(length, removeFromChain);
                            this.count = i2 - 1;
                            unlock();
                            return true;
                        } else {
                            unlock();
                            return false;
                        }
                    }
                }
                unlock();
                return false;
            } catch (Throwable th) {
                unlock();
                throw th;
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.fossil.v34$n<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public V remove(Object obj, int i) {
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (i iVar = e; iVar != null; iVar = iVar.a()) {
                    Object key = iVar.getKey();
                    if (iVar.c() == i && key != null && this.map.keyEquivalence.equivalent(obj, key)) {
                        V v = (V) iVar.getValue();
                        if (v == null && !isCollected(iVar)) {
                            unlock();
                            return null;
                        }
                        this.modCount++;
                        E removeFromChain = removeFromChain(e, iVar);
                        int i2 = this.count;
                        atomicReferenceArray.set(length, removeFromChain);
                        this.count = i2 - 1;
                        return v;
                    }
                }
                unlock();
                return null;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.fossil.v34$n<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public boolean remove(Object obj, int i, Object obj2) {
            boolean z = false;
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (i iVar = e; iVar != null; iVar = iVar.a()) {
                    Object key = iVar.getKey();
                    if (iVar.c() == i && key != null && this.map.keyEquivalence.equivalent(obj, key)) {
                        if (this.map.valueEquivalence().equivalent(obj2, iVar.getValue())) {
                            z = true;
                        } else if (!isCollected(iVar)) {
                            unlock();
                            return false;
                        }
                        this.modCount++;
                        E removeFromChain = removeFromChain(e, iVar);
                        int i2 = this.count;
                        atomicReferenceArray.set(length, removeFromChain);
                        this.count = i2 - 1;
                        return z;
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.v34$n<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public boolean removeEntryForTesting(E e) {
            int c = e.c();
            AtomicReferenceArray<E> atomicReferenceArray = this.table;
            int length = c & (atomicReferenceArray.length() - 1);
            E e2 = atomicReferenceArray.get(length);
            for (i iVar = e2; iVar != null; iVar = iVar.a()) {
                if (iVar == e) {
                    this.modCount++;
                    E removeFromChain = removeFromChain(e2, iVar);
                    int i = this.count;
                    atomicReferenceArray.set(length, removeFromChain);
                    this.count = i - 1;
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public E removeFromChain(E e, E e2) {
            int i = this.count;
            E e3 = (E) e2.a();
            while (e != e2) {
                E copyEntry = copyEntry(e, e3);
                if (copyEntry == null) {
                    i--;
                    copyEntry = e3;
                }
                e = (E) e.a();
                e3 = copyEntry;
            }
            this.count = i;
            return e3;
        }

        @DexIgnore
        public E removeFromChainForTesting(i<K, V, ?> iVar, i<K, V, ?> iVar2) {
            return removeFromChain(castForTesting(iVar), castForTesting(iVar2));
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public boolean removeTableEntryForTesting(i<K, V, ?> iVar) {
            return removeEntryForTesting(castForTesting(iVar));
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.fossil.v34$n<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public V replace(K k, int i, V v) {
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (i iVar = e; iVar != null; iVar = iVar.a()) {
                    Object key = iVar.getKey();
                    if (iVar.c() == i && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        V v2 = (V) iVar.getValue();
                        if (v2 == null) {
                            if (isCollected(iVar)) {
                                this.modCount++;
                                E removeFromChain = removeFromChain(e, iVar);
                                int i2 = this.count;
                                atomicReferenceArray.set(length, removeFromChain);
                                this.count = i2 - 1;
                            }
                            return null;
                        }
                        this.modCount++;
                        setValue(iVar, v);
                        unlock();
                        return v2;
                    }
                }
                unlock();
                return null;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.fossil.v34$n<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public boolean replace(K k, int i, V v, V v2) {
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (i iVar = e; iVar != null; iVar = iVar.a()) {
                    Object key = iVar.getKey();
                    if (iVar.c() == i && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        Object value = iVar.getValue();
                        if (value == null) {
                            if (isCollected(iVar)) {
                                this.modCount++;
                                E removeFromChain = removeFromChain(e, iVar);
                                int i2 = this.count;
                                atomicReferenceArray.set(length, removeFromChain);
                                this.count = i2 - 1;
                            }
                            return false;
                        } else if (this.map.valueEquivalence().equivalent(v, value)) {
                            this.modCount++;
                            setValue(iVar, v2);
                            unlock();
                            return true;
                        } else {
                            unlock();
                            return false;
                        }
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        public void runCleanup() {
            runLockedCleanup();
        }

        @DexIgnore
        public void runLockedCleanup() {
            if (tryLock()) {
                try {
                    maybeDrainReferenceQueues();
                    this.readCount.set(0);
                } finally {
                    unlock();
                }
            }
        }

        @DexIgnore
        public abstract S self();

        @DexIgnore
        public void setTableEntryForTesting(int i, i<K, V, ?> iVar) {
            this.table.set(i, castForTesting(iVar));
        }

        @DexIgnore
        public void setValue(E e, V v) {
            this.map.entryHelper.d(self(), e, v);
        }

        @DexIgnore
        public void setValueForTesting(i<K, V, ?> iVar, V v) {
            this.map.entryHelper.d(self(), castForTesting(iVar), v);
        }

        @DexIgnore
        public void setWeakValueReferenceForTesting(i<K, V, ?> iVar, b0<K, V, ? extends i<K, V, ?>> b0Var) {
            throw new AssertionError();
        }

        @DexIgnore
        public void tryDrainReferenceQueues() {
            if (tryLock()) {
                try {
                    maybeDrainReferenceQueues();
                } finally {
                    unlock();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o<K, V> extends b<K, V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 3;

        @DexIgnore
        public o(p pVar, p pVar2, z04<Object> z04, z04<Object> z042, int i, ConcurrentMap<K, V> concurrentMap) {
            super(pVar, pVar2, z04, z042, i, concurrentMap);
        }

        @DexIgnore
        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.delegate = readMapMaker(objectInputStream).i();
            readEntries(objectInputStream);
        }

        @DexIgnore
        private Object readResolve() {
            return this.delegate;
        }

        @DexIgnore
        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            writeMapTo(objectOutputStream);
        }
    }

    @DexIgnore
    public enum p {
        STRONG {
            @DexIgnore
            @Override // com.fossil.v34.p
            public z04<Object> defaultEquivalence() {
                return z04.equals();
            }
        },
        WEAK {
            @DexIgnore
            @Override // com.fossil.v34.p
            public z04<Object> defaultEquivalence() {
                return z04.identity();
            }
        };

        @DexIgnore
        public /* synthetic */ p(a aVar) {
            this();
        }

        @DexIgnore
        public abstract z04<Object> defaultEquivalence();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q<K, V> extends c<K, V, q<K, V>> implements Object<K, V, q<K, V>> {
        @DexIgnore
        public volatile V d; // = null;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<K, V> implements j<K, V, q<K, V>, r<K, V>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public static /* final */ a<?, ?> f3705a; // = new a<>();

            @DexIgnore
            public static <K, V> a<K, V> h() {
                return (a<K, V>) f3705a;
            }

            @DexIgnore
            @Override // com.fossil.v34.j
            public p b() {
                return p.STRONG;
            }

            @DexIgnore
            @Override // com.fossil.v34.j
            public p c() {
                return p.STRONG;
            }

            @DexIgnore
            /* renamed from: g */
            public q<K, V> a(r<K, V> rVar, q<K, V> qVar, q<K, V> qVar2) {
                return qVar.d(qVar2);
            }

            @DexIgnore
            /* renamed from: i */
            public q<K, V> f(r<K, V> rVar, K k, int i, q<K, V> qVar) {
                return new q<>(k, i, qVar);
            }

            @DexIgnore
            /* renamed from: j */
            public r<K, V> e(v34<K, V, q<K, V>, r<K, V>> v34, int i, int i2) {
                return new r<>(v34, i, i2);
            }

            @DexIgnore
            /* renamed from: k */
            public void d(r<K, V> rVar, q<K, V> qVar, V v) {
                qVar.e(v);
            }
        }

        @DexIgnore
        public q(K k, int i, q<K, V> qVar) {
            super(k, i, qVar);
        }

        @DexIgnore
        public q<K, V> d(q<K, V> qVar) {
            q<K, V> qVar2 = new q<>(this.f3702a, this.b, qVar);
            qVar2.d = this.d;
            return qVar2;
        }

        @DexIgnore
        public void e(V v) {
            this.d = v;
        }

        @DexIgnore
        @Override // com.fossil.v34.i
        public V getValue() {
            return this.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r<K, V> extends n<K, V, q<K, V>, r<K, V>> {
        @DexIgnore
        public r(v34<K, V, q<K, V>, r<K, V>> v34, int i, int i2) {
            super(v34, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public q<K, V> castForTesting(i<K, V, ?> iVar) {
            return (q) iVar;
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public r<K, V> self() {
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s<K, V> extends c<K, V, s<K, V>> implements a0<K, V, s<K, V>> {
        @DexIgnore
        public volatile b0<K, V, s<K, V>> d; // = v34.unsetWeakValueReference();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<K, V> implements j<K, V, s<K, V>, t<K, V>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public static /* final */ a<?, ?> f3706a; // = new a<>();

            @DexIgnore
            public static <K, V> a<K, V> h() {
                return (a<K, V>) f3706a;
            }

            @DexIgnore
            @Override // com.fossil.v34.j
            public p b() {
                return p.STRONG;
            }

            @DexIgnore
            @Override // com.fossil.v34.j
            public p c() {
                return p.WEAK;
            }

            @DexIgnore
            /* renamed from: g */
            public s<K, V> a(t<K, V> tVar, s<K, V> sVar, s<K, V> sVar2) {
                if (n.isCollected(sVar)) {
                    return null;
                }
                return sVar.f(tVar.queueForValues, sVar2);
            }

            @DexIgnore
            /* renamed from: i */
            public s<K, V> f(t<K, V> tVar, K k, int i, s<K, V> sVar) {
                return new s<>(k, i, sVar);
            }

            @DexIgnore
            /* renamed from: j */
            public t<K, V> e(v34<K, V, s<K, V>, t<K, V>> v34, int i, int i2) {
                return new t<>(v34, i, i2);
            }

            @DexIgnore
            /* renamed from: k */
            public void d(t<K, V> tVar, s<K, V> sVar, V v) {
                sVar.g(v, tVar.queueForValues);
            }
        }

        @DexIgnore
        public s(K k, int i, s<K, V> sVar) {
            super(k, i, sVar);
        }

        @DexIgnore
        @Override // com.fossil.v34.a0
        public b0<K, V, s<K, V>> b() {
            return this.d;
        }

        @DexIgnore
        public s<K, V> f(ReferenceQueue<V> referenceQueue, s<K, V> sVar) {
            s<K, V> sVar2 = new s<>(this.f3702a, this.b, sVar);
            sVar2.d = this.d.b(referenceQueue, sVar2);
            return sVar2;
        }

        @DexIgnore
        public void g(V v, ReferenceQueue<V> referenceQueue) {
            b0<K, V, s<K, V>> b0Var = this.d;
            this.d = new c0(referenceQueue, v, this);
            b0Var.clear();
        }

        @DexIgnore
        @Override // com.fossil.v34.i
        public V getValue() {
            return this.d.get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t<K, V> extends n<K, V, s<K, V>, t<K, V>> {
        @DexIgnore
        public /* final */ ReferenceQueue<V> queueForValues; // = new ReferenceQueue<>();

        @DexIgnore
        public t(v34<K, V, s<K, V>, t<K, V>> v34, int i, int i2) {
            super(v34, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public s<K, V> castForTesting(i<K, V, ?> iVar) {
            return (s) iVar;
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            return this.queueForValues;
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public b0<K, V, s<K, V>> getWeakValueReferenceForTesting(i<K, V, ?> iVar) {
            return castForTesting((i) iVar).b();
        }

        @DexIgnore
        /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.lang.ref.ReferenceQueue<V>, java.lang.ref.ReferenceQueue<T> */
        @Override // com.fossil.v34.n
        public void maybeClearReferenceQueues() {
            clearReferenceQueue((ReferenceQueue<V>) this.queueForValues);
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public void maybeDrainReferenceQueues() {
            drainValueReferenceQueue(this.queueForValues);
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public b0<K, V, s<K, V>> newWeakValueReferenceForTesting(i<K, V, ?> iVar, V v) {
            return new c0(this.queueForValues, v, castForTesting((i) iVar));
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public t<K, V> self() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public void setWeakValueReferenceForTesting(i<K, V, ?> iVar, b0<K, V, ? extends i<K, V, ?>> b0Var) {
            s<K, V> castForTesting = castForTesting((i) iVar);
            b0 b0Var2 = castForTesting.d;
            castForTesting.d = b0Var;
            b0Var2.clear();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u extends v34<K, V, E, S>.h {
        @DexIgnore
        public u(v34 v34) {
            super();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public V next() {
            return (V) c().getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v extends AbstractCollection<V> {
        @DexIgnore
        public v() {
        }

        @DexIgnore
        public void clear() {
            v34.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return v34.this.containsValue(obj);
        }

        @DexIgnore
        public boolean isEmpty() {
            return v34.this.isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            return new u(v34.this);
        }

        @DexIgnore
        public int size() {
            return v34.this.size();
        }

        @DexIgnore
        public Object[] toArray() {
            return v34.a(this).toArray();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public <E> E[] toArray(E[] eArr) {
            return (E[]) v34.a(this).toArray(eArr);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class w<K, V> extends d<K, V, w<K, V>> implements Object<K, V, w<K, V>> {
        @DexIgnore
        public volatile V c; // = null;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<K, V> implements j<K, V, w<K, V>, x<K, V>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public static /* final */ a<?, ?> f3707a; // = new a<>();

            @DexIgnore
            public static <K, V> a<K, V> h() {
                return (a<K, V>) f3707a;
            }

            @DexIgnore
            @Override // com.fossil.v34.j
            public p b() {
                return p.WEAK;
            }

            @DexIgnore
            @Override // com.fossil.v34.j
            public p c() {
                return p.STRONG;
            }

            @DexIgnore
            /* renamed from: g */
            public w<K, V> a(x<K, V> xVar, w<K, V> wVar, w<K, V> wVar2) {
                if (wVar.getKey() == null) {
                    return null;
                }
                return wVar.d(xVar.queueForKeys, wVar2);
            }

            @DexIgnore
            /* renamed from: i */
            public w<K, V> f(x<K, V> xVar, K k, int i, w<K, V> wVar) {
                return new w<>(xVar.queueForKeys, k, i, wVar);
            }

            @DexIgnore
            /* renamed from: j */
            public x<K, V> e(v34<K, V, w<K, V>, x<K, V>> v34, int i, int i2) {
                return new x<>(v34, i, i2);
            }

            @DexIgnore
            /* renamed from: k */
            public void d(x<K, V> xVar, w<K, V> wVar, V v) {
                wVar.e(v);
            }
        }

        @DexIgnore
        public w(ReferenceQueue<K> referenceQueue, K k, int i, w<K, V> wVar) {
            super(referenceQueue, k, i, wVar);
        }

        @DexIgnore
        public w<K, V> d(ReferenceQueue<K> referenceQueue, w<K, V> wVar) {
            w<K, V> wVar2 = new w<>(referenceQueue, getKey(), this.f3704a, wVar);
            wVar2.e(this.c);
            return wVar2;
        }

        @DexIgnore
        public void e(V v) {
            this.c = v;
        }

        @DexIgnore
        @Override // com.fossil.v34.i
        public V getValue() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class x<K, V> extends n<K, V, w<K, V>, x<K, V>> {
        @DexIgnore
        public /* final */ ReferenceQueue<K> queueForKeys; // = new ReferenceQueue<>();

        @DexIgnore
        public x(v34<K, V, w<K, V>, x<K, V>> v34, int i, int i2) {
            super(v34, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public w<K, V> castForTesting(i<K, V, ?> iVar) {
            return (w) iVar;
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            return this.queueForKeys;
        }

        @DexIgnore
        /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.lang.ref.ReferenceQueue<K>, java.lang.ref.ReferenceQueue<T> */
        @Override // com.fossil.v34.n
        public void maybeClearReferenceQueues() {
            clearReferenceQueue((ReferenceQueue<K>) this.queueForKeys);
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public void maybeDrainReferenceQueues() {
            drainKeyReferenceQueue(this.queueForKeys);
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public x<K, V> self() {
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class y<K, V> extends d<K, V, y<K, V>> implements a0<K, V, y<K, V>> {
        @DexIgnore
        public volatile b0<K, V, y<K, V>> c; // = v34.unsetWeakValueReference();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<K, V> implements j<K, V, y<K, V>, z<K, V>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public static /* final */ a<?, ?> f3708a; // = new a<>();

            @DexIgnore
            public static <K, V> a<K, V> h() {
                return (a<K, V>) f3708a;
            }

            @DexIgnore
            @Override // com.fossil.v34.j
            public p b() {
                return p.WEAK;
            }

            @DexIgnore
            @Override // com.fossil.v34.j
            public p c() {
                return p.WEAK;
            }

            @DexIgnore
            /* renamed from: g */
            public y<K, V> a(z<K, V> zVar, y<K, V> yVar, y<K, V> yVar2) {
                if (yVar.getKey() != null && !n.isCollected(yVar)) {
                    return yVar.f(zVar.queueForKeys, zVar.queueForValues, yVar2);
                }
                return null;
            }

            @DexIgnore
            /* renamed from: i */
            public y<K, V> f(z<K, V> zVar, K k, int i, y<K, V> yVar) {
                return new y<>(zVar.queueForKeys, k, i, yVar);
            }

            @DexIgnore
            /* renamed from: j */
            public z<K, V> e(v34<K, V, y<K, V>, z<K, V>> v34, int i, int i2) {
                return new z<>(v34, i, i2);
            }

            @DexIgnore
            /* renamed from: k */
            public void d(z<K, V> zVar, y<K, V> yVar, V v) {
                yVar.g(v, zVar.queueForValues);
            }
        }

        @DexIgnore
        public y(ReferenceQueue<K> referenceQueue, K k, int i, y<K, V> yVar) {
            super(referenceQueue, k, i, yVar);
        }

        @DexIgnore
        @Override // com.fossil.v34.a0
        public b0<K, V, y<K, V>> b() {
            return this.c;
        }

        @DexIgnore
        public y<K, V> f(ReferenceQueue<K> referenceQueue, ReferenceQueue<V> referenceQueue2, y<K, V> yVar) {
            y<K, V> yVar2 = new y<>(referenceQueue, getKey(), this.f3704a, yVar);
            yVar2.c = this.c.b(referenceQueue2, yVar2);
            return yVar2;
        }

        @DexIgnore
        public void g(V v, ReferenceQueue<V> referenceQueue) {
            b0<K, V, y<K, V>> b0Var = this.c;
            this.c = new c0(referenceQueue, v, this);
            b0Var.clear();
        }

        @DexIgnore
        @Override // com.fossil.v34.i
        public V getValue() {
            return this.c.get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class z<K, V> extends n<K, V, y<K, V>, z<K, V>> {
        @DexIgnore
        public /* final */ ReferenceQueue<K> queueForKeys; // = new ReferenceQueue<>();
        @DexIgnore
        public /* final */ ReferenceQueue<V> queueForValues; // = new ReferenceQueue<>();

        @DexIgnore
        public z(v34<K, V, y<K, V>, z<K, V>> v34, int i, int i2) {
            super(v34, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public y<K, V> castForTesting(i<K, V, ?> iVar) {
            return (y) iVar;
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            return this.queueForKeys;
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            return this.queueForValues;
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public b0<K, V, y<K, V>> getWeakValueReferenceForTesting(i<K, V, ?> iVar) {
            return castForTesting((i) iVar).b();
        }

        @DexIgnore
        /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.lang.ref.ReferenceQueue<K>, java.lang.ref.ReferenceQueue<T> */
        @Override // com.fossil.v34.n
        public void maybeClearReferenceQueues() {
            clearReferenceQueue((ReferenceQueue<K>) this.queueForKeys);
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public void maybeDrainReferenceQueues() {
            drainKeyReferenceQueue(this.queueForKeys);
            drainValueReferenceQueue(this.queueForValues);
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public b0<K, V, y<K, V>> newWeakValueReferenceForTesting(i<K, V, ?> iVar, V v) {
            return new c0(this.queueForValues, v, castForTesting((i) iVar));
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public z<K, V> self() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v34.n
        public void setWeakValueReferenceForTesting(i<K, V, ?> iVar, b0<K, V, ? extends i<K, V, ?>> b0Var) {
            y<K, V> castForTesting = castForTesting((i) iVar);
            b0 b0Var2 = castForTesting.c;
            castForTesting.c = b0Var;
            b0Var2.clear();
        }
    }

    @DexIgnore
    public v34(u34 u34, j<K, V, E, S> jVar) {
        int i2 = 1;
        int i3 = 0;
        this.concurrencyLevel = Math.min(u34.b(), 65536);
        this.keyEquivalence = u34.d();
        this.entryHelper = jVar;
        int min = Math.min(u34.c(), 1073741824);
        int i4 = 0;
        int i5 = 1;
        while (i5 < this.concurrencyLevel) {
            i4++;
            i5 <<= 1;
        }
        this.segmentShift = 32 - i4;
        this.segmentMask = i5 - 1;
        this.segments = newSegmentArray(i5);
        int i6 = min / i5;
        while (i2 < (i5 * i6 < min ? i6 + 1 : i6)) {
            i2 <<= 1;
        }
        while (true) {
            n<K, V, E, S>[] nVarArr = this.segments;
            if (i3 < nVarArr.length) {
                nVarArr[i3] = createSegment(i2, -1);
                i3++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public static <E> ArrayList<E> a(Collection<E> collection) {
        ArrayList<E> arrayList = new ArrayList<>(collection.size());
        p34.a(arrayList, collection.iterator());
        return arrayList;
    }

    @DexIgnore
    public static <K, V> v34<K, V, ? extends i<K, V, ?>, ?> create(u34 u34) {
        if (u34.e() == p.STRONG && u34.f() == p.STRONG) {
            return new v34<>(u34, q.a.h());
        }
        if (u34.e() == p.STRONG && u34.f() == p.WEAK) {
            return new v34<>(u34, s.a.h());
        }
        if (u34.e() == p.WEAK && u34.f() == p.STRONG) {
            return new v34<>(u34, w.a.h());
        }
        if (u34.e() == p.WEAK && u34.f() == p.WEAK) {
            return new v34<>(u34, y.a.h());
        }
        throw new AssertionError();
    }

    @DexIgnore
    public static int rehash(int i2) {
        int i3 = ((i2 << 15) ^ -12931) + i2;
        int i4 = i3 ^ (i3 >>> 10);
        int i5 = i4 + (i4 << 3);
        int i6 = i5 ^ (i5 >>> 6);
        int i7 = i6 + (i6 << 2) + (i6 << 14);
        return i7 ^ (i7 >>> 16);
    }

    @DexIgnore
    public static <K, V, E extends i<K, V, E>> b0<K, V, E> unsetWeakValueReference() {
        return (b0<K, V, E>) UNSET_WEAK_VALUE_REFERENCE;
    }

    @DexIgnore
    public void clear() {
        for (n<K, V, E, S> nVar : this.segments) {
            nVar.clear();
        }
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        if (obj == null) {
            return false;
        }
        int hash = hash(obj);
        return segmentFor(hash).containsKey(obj, hash);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: com.fossil.v34$n<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>>[] */
    /* JADX DEBUG: Multi-variable search result rejected for r10v0, resolved type: com.fossil.v34$z */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean containsValue(Object obj) {
        if (obj == null) {
            return false;
        }
        n<K, V, E, S>[] nVarArr = this.segments;
        long j2 = -1;
        for (int i2 = 0; i2 < 3; i2++) {
            j2 = 0;
            for (z zVar : nVarArr) {
                int i3 = zVar.count;
                AtomicReferenceArray<E> atomicReferenceArray = zVar.table;
                for (int i4 = 0; i4 < atomicReferenceArray.length(); i4++) {
                    for (E e2 = atomicReferenceArray.get(i4); e2 != null; e2 = e2.a()) {
                        Object liveValue = zVar.getLiveValue(e2);
                        if (liveValue != null && valueEquivalence().equivalent(obj, liveValue)) {
                            return true;
                        }
                    }
                }
                j2 += (long) zVar.modCount;
            }
            if (j2 == j2) {
                break;
            }
        }
        return false;
    }

    @DexIgnore
    public E copyEntry(E e2, E e3) {
        return segmentFor(e2.c()).copyEntry(e2, e3);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v1. Raw type applied. Possible types: S extends com.fossil.v34$n<K, V, E, S>, com.fossil.v34$n<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>> */
    public n<K, V, E, S> createSegment(int i2, int i3) {
        return (S) this.entryHelper.e(this, i2, i3);
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> set = this.entrySet;
        if (set != null) {
            return set;
        }
        g gVar = new g();
        this.entrySet = gVar;
        return gVar;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        if (obj == null) {
            return null;
        }
        int hash = hash(obj);
        return segmentFor(hash).get(obj, hash);
    }

    @DexIgnore
    public E getEntry(Object obj) {
        if (obj == null) {
            return null;
        }
        int hash = hash(obj);
        return segmentFor(hash).getEntry(obj, hash);
    }

    @DexIgnore
    public V getLiveValue(E e2) {
        V v2;
        if (e2.getKey() == null || (v2 = (V) e2.getValue()) == null) {
            return null;
        }
        return v2;
    }

    @DexIgnore
    public int hash(Object obj) {
        return rehash(this.keyEquivalence.hash(obj));
    }

    @DexIgnore
    public boolean isEmpty() {
        n<K, V, E, S>[] nVarArr = this.segments;
        long j2 = 0;
        for (int i2 = 0; i2 < nVarArr.length; i2++) {
            if (nVarArr[i2].count != 0) {
                return false;
            }
            j2 += (long) nVarArr[i2].modCount;
        }
        if (j2 != 0) {
            for (int i3 = 0; i3 < nVarArr.length; i3++) {
                if (nVarArr[i3].count != 0) {
                    return false;
                }
                j2 -= (long) nVarArr[i3].modCount;
            }
            return j2 == 0;
        }
    }

    @DexIgnore
    public boolean isLiveForTesting(i<K, V, ?> iVar) {
        return segmentFor(iVar.c()).getLiveValueForTesting(iVar) != null;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Set<K> keySet() {
        Set<K> set = this.keySet;
        if (set != null) {
            return set;
        }
        l lVar = new l();
        this.keySet = lVar;
        return lVar;
    }

    @DexIgnore
    public p keyStrength() {
        return this.entryHelper.b();
    }

    @DexIgnore
    public final n<K, V, E, S>[] newSegmentArray(int i2) {
        return new n[i2];
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    @CanIgnoreReturnValue
    public V put(K k2, V v2) {
        i14.l(k2);
        i14.l(v2);
        int hash = hash(k2);
        return segmentFor(hash).put(k2, hash, v2, false);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.v34<K, V, E extends com.fossil.v34$i<K, V, E>, S extends com.fossil.v34$n<K, V, E, S>> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public void putAll(Map<? extends K, ? extends V> map) {
        for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @DexIgnore
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public V putIfAbsent(K k2, V v2) {
        i14.l(k2);
        i14.l(v2);
        int hash = hash(k2);
        return segmentFor(hash).put(k2, hash, v2, true);
    }

    @DexIgnore
    public void reclaimKey(E e2) {
        int c2 = e2.c();
        segmentFor(c2).reclaimKey(e2, c2);
    }

    @DexIgnore
    public void reclaimValue(b0<K, V, E> b0Var) {
        E a2 = b0Var.a();
        int c2 = a2.c();
        segmentFor(c2).reclaimValue((K) a2.getKey(), c2, b0Var);
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    @CanIgnoreReturnValue
    public V remove(Object obj) {
        if (obj == null) {
            return null;
        }
        int hash = hash(obj);
        return segmentFor(hash).remove(obj, hash);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public boolean remove(Object obj, Object obj2) {
        if (obj == null || obj2 == null) {
            return false;
        }
        int hash = hash(obj);
        return segmentFor(hash).remove(obj, hash, obj2);
    }

    @DexIgnore
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public V replace(K k2, V v2) {
        i14.l(k2);
        i14.l(v2);
        int hash = hash(k2);
        return segmentFor(hash).replace(k2, hash, v2);
    }

    @DexIgnore
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public boolean replace(K k2, V v2, V v3) {
        i14.l(k2);
        i14.l(v3);
        if (v2 == null) {
            return false;
        }
        int hash = hash(k2);
        return segmentFor(hash).replace(k2, hash, v2, v3);
    }

    @DexIgnore
    public n<K, V, E, S> segmentFor(int i2) {
        return this.segments[(i2 >>> this.segmentShift) & this.segmentMask];
    }

    @DexIgnore
    public int size() {
        n<K, V, E, S>[] nVarArr;
        long j2 = 0;
        for (n<K, V, E, S> nVar : this.segments) {
            j2 += (long) nVar.count;
        }
        return v54.b(j2);
    }

    @DexIgnore
    public z04<Object> valueEquivalence() {
        return this.entryHelper.c().defaultEquivalence();
    }

    @DexIgnore
    public p valueStrength() {
        return this.entryHelper.c();
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Collection<V> values() {
        Collection<V> collection = this.values;
        if (collection != null) {
            return collection;
        }
        v vVar = new v();
        this.values = vVar;
        return vVar;
    }

    @DexIgnore
    public Object writeReplace() {
        return new o(this.entryHelper.b(), this.entryHelper.c(), this.keyEquivalence, this.entryHelper.c().defaultEquivalence(), this.concurrencyLevel, this);
    }
}
