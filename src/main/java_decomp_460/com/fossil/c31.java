package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c31 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f550a;
    @DexIgnore
    public Long b;

    @DexIgnore
    public c31(String str, long j) {
        this.f550a = str;
        this.b = Long.valueOf(j);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public c31(String str, boolean z) {
        this(str, z ? 1 : 0);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c31)) {
            return false;
        }
        c31 c31 = (c31) obj;
        if (!this.f550a.equals(c31.f550a)) {
            return false;
        }
        Long l = this.b;
        Long l2 = c31.b;
        return l != null ? l.equals(l2) : l2 == null;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.f550a.hashCode();
        Long l = this.b;
        return (l != null ? l.hashCode() : 0) + (hashCode * 31);
    }
}
