package com.fossil;

import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h78 implements e78 {
    @DexIgnore
    public String b;
    @DexIgnore
    public p78 c;
    @DexIgnore
    public Queue<k78> d;

    @DexIgnore
    public h78(p78 p78, Queue<k78> queue) {
        this.c = p78;
        this.b = p78.c();
        this.d = queue;
    }

    @DexIgnore
    public final void a(i78 i78, String str, Object[] objArr, Throwable th) {
        b(i78, null, str, objArr, th);
    }

    @DexIgnore
    public final void b(i78 i78, g78 g78, String str, Object[] objArr, Throwable th) {
        k78 k78 = new k78();
        k78.j(System.currentTimeMillis());
        k78.c(i78);
        k78.d(this.c);
        k78.e(this.b);
        k78.f(g78);
        k78.g(str);
        k78.b(objArr);
        k78.i(th);
        k78.h(Thread.currentThread().getName());
        this.d.add(k78);
    }

    @DexIgnore
    @Override // com.fossil.e78
    public void debug(String str) {
        a(i78.TRACE, str, null, null);
    }

    @DexIgnore
    @Override // com.fossil.e78
    public void debug(String str, Throwable th) {
        a(i78.DEBUG, str, null, th);
    }

    @DexIgnore
    @Override // com.fossil.e78
    public void error(String str) {
        a(i78.ERROR, str, null, null);
    }

    @DexIgnore
    @Override // com.fossil.e78
    public void error(String str, Throwable th) {
        a(i78.ERROR, str, null, th);
    }

    @DexIgnore
    @Override // com.fossil.e78
    public void error(String str, Object... objArr) {
        a(i78.ERROR, str, objArr, null);
    }

    @DexIgnore
    @Override // com.fossil.e78
    public void info(String str) {
        a(i78.INFO, str, null, null);
    }

    @DexIgnore
    @Override // com.fossil.e78
    public void info(String str, Object obj) {
        a(i78.INFO, str, new Object[]{obj}, null);
    }

    @DexIgnore
    @Override // com.fossil.e78
    public void info(String str, Throwable th) {
        a(i78.INFO, str, null, th);
    }

    @DexIgnore
    @Override // com.fossil.e78
    public boolean isDebugEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.e78
    public boolean isErrorEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.e78
    public boolean isInfoEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.e78
    public boolean isTraceEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.e78
    public boolean isWarnEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.e78
    public void trace(String str) {
        a(i78.TRACE, str, null, null);
    }

    @DexIgnore
    @Override // com.fossil.e78
    public void trace(String str, Throwable th) {
        a(i78.TRACE, str, null, th);
    }

    @DexIgnore
    @Override // com.fossil.e78
    public void warn(String str) {
        a(i78.WARN, str, null, null);
    }

    @DexIgnore
    @Override // com.fossil.e78
    public void warn(String str, Object obj, Object obj2) {
        a(i78.WARN, str, new Object[]{obj, obj2}, null);
    }

    @DexIgnore
    @Override // com.fossil.e78
    public void warn(String str, Throwable th) {
        a(i78.WARN, str, null, th);
    }
}
