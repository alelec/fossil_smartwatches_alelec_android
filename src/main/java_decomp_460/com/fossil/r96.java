package com.fossil;

import com.fossil.imagefilters.FilterType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class r96 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f3090a;

    /*
    static {
        int[] iArr = new int[FilterType.values().length];
        f3090a = iArr;
        iArr[FilterType.ATKINSON_DITHERING.ordinal()] = 1;
        f3090a[FilterType.BURKES_DITHERING.ordinal()] = 2;
        f3090a[FilterType.DIRECT_MAPPING.ordinal()] = 3;
        f3090a[FilterType.JAJUNI_DITHERING.ordinal()] = 4;
        f3090a[FilterType.SIERRA_DITHERING.ordinal()] = 5;
        f3090a[FilterType.ORDERED_DITHERING.ordinal()] = 6;
    }
    */
}
