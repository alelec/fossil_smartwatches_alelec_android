package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rt4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qs4 f3164a;

    @DexIgnore
    public rt4(qs4 qs4) {
        pq7.c(qs4, "dao");
        this.f3164a = qs4;
    }

    @DexIgnore
    public final ps4 a(String str) {
        pq7.c(str, "id");
        return this.f3164a.m(str);
    }

    @DexIgnore
    public final LiveData<ps4> b(String str) {
        pq7.c(str, "id");
        return this.f3164a.s(str);
    }

    @DexIgnore
    public final int c(String str) {
        pq7.c(str, "id");
        return this.f3164a.n(str);
    }

    @DexIgnore
    public final void d() {
        this.f3164a.g();
        this.f3164a.w();
        this.f3164a.x();
        this.f3164a.q();
        this.f3164a.o();
    }

    @DexIgnore
    public final void e() {
        this.f3164a.w();
    }

    @DexIgnore
    public final void f() {
        this.f3164a.p();
    }

    @DexIgnore
    public final ps4 g(String[] strArr, Date date) {
        pq7.c(strArr, "status");
        pq7.c(date, "date");
        return this.f3164a.f(strArr, date);
    }

    @DexIgnore
    public final LiveData<ps4> h(String[] strArr, Date date) {
        pq7.c(strArr, "status");
        pq7.c(date, "date");
        return this.f3164a.c(strArr, date);
    }

    @DexIgnore
    public final int i(String str) {
        pq7.c(str, "id");
        return this.f3164a.b(str);
    }

    @DexIgnore
    public final void j(String[] strArr) {
        pq7.c(strArr, "status");
        this.f3164a.u(strArr);
    }

    @DexIgnore
    public final ks4 k(String str) {
        pq7.c(str, "id");
        return this.f3164a.d(str);
    }

    @DexIgnore
    public final List<ls4> l() {
        return this.f3164a.j();
    }

    @DexIgnore
    public final List<ms4> m() {
        return this.f3164a.a();
    }

    @DexIgnore
    public final LiveData<bt4> n(String str) {
        pq7.c(str, "challengeId");
        return this.f3164a.l(str);
    }

    @DexIgnore
    public final bt4 o(String str) {
        pq7.c(str, "id");
        return this.f3164a.e(str);
    }

    @DexIgnore
    public final LiveData<List<bt4>> p() {
        return this.f3164a.i();
    }

    @DexIgnore
    public final long q(ls4 ls4) {
        pq7.c(ls4, "fitnessData");
        return this.f3164a.h(ls4);
    }

    @DexIgnore
    public final long r(ps4 ps4) {
        pq7.c(ps4, "challenge");
        return this.f3164a.k(ps4);
    }

    @DexIgnore
    public final Long[] s(List<ps4> list) {
        pq7.c(list, "challenges");
        return this.f3164a.insert(list);
    }

    @DexIgnore
    public final Long[] t(List<ks4> list) {
        pq7.c(list, "displayPlayer");
        return this.f3164a.y(list);
    }

    @DexIgnore
    public final Long[] u(List<bt4> list) {
        pq7.c(list, "histories");
        return this.f3164a.t(list);
    }

    @DexIgnore
    public final Long[] v(List<ms4> list) {
        pq7.c(list, "players");
        return this.f3164a.v(list);
    }

    @DexIgnore
    public final int w(bt4 bt4) {
        pq7.c(bt4, "historyChallenge");
        return this.f3164a.r(bt4);
    }
}
