package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cs2 extends as2 implements rs2 {
    @DexIgnore
    public cs2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IPolylineDelegate");
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final boolean B0(rs2 rs2) throws RemoteException {
        Parcel d = d();
        es2.c(d, rs2);
        Parcel e = e(15, d);
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final int a() throws RemoteException {
        Parcel e = e(16, d());
        int readInt = e.readInt();
        e.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final void b(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(17, d);
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final String getId() throws RemoteException {
        Parcel e = e(2, d());
        String readString = e.readString();
        e.recycle();
        return readString;
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final void remove() throws RemoteException {
        i(1, d());
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final void setColor(int i) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        i(7, d);
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final void setEndCap(ce3 ce3) throws RemoteException {
        Parcel d = d();
        es2.d(d, ce3);
        i(21, d);
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final void setGeodesic(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(13, d);
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final void setJointType(int i) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        i(23, d);
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final void setPattern(List<me3> list) throws RemoteException {
        Parcel d = d();
        d.writeTypedList(list);
        i(25, d);
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final void setPoints(List<LatLng> list) throws RemoteException {
        Parcel d = d();
        d.writeTypedList(list);
        i(3, d);
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final void setStartCap(ce3 ce3) throws RemoteException {
        Parcel d = d();
        es2.d(d, ce3);
        i(19, d);
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final void setVisible(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(11, d);
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final void setWidth(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        i(5, d);
    }

    @DexIgnore
    @Override // com.fossil.rs2
    public final void setZIndex(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        i(9, d);
    }
}
