package com.fossil;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class re7 {
    @DexIgnore
    public static re7 c;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Map<Integer, qe7> f3103a; // = null;
    @DexIgnore
    public Context b; // = null;

    @DexIgnore
    public re7(Context context) {
        this.b = context.getApplicationContext();
        HashMap hashMap = new HashMap(3);
        this.f3103a = hashMap;
        hashMap.put(1, new pe7(context));
        this.f3103a.put(2, new me7(context));
        this.f3103a.put(4, new oe7(context));
    }

    @DexIgnore
    public static re7 a(Context context) {
        re7 re7;
        synchronized (re7.class) {
            try {
                if (c == null) {
                    c = new re7(context);
                }
                re7 = c;
            } catch (Throwable th) {
                throw th;
            }
        }
        return re7;
    }

    @DexIgnore
    public final void b(String str) {
        ne7 d = d();
        d.c = str;
        if (!se7.f(d.f2510a)) {
            d.f2510a = se7.a(this.b);
        }
        if (!se7.f(d.b)) {
            d.b = se7.e(this.b);
        }
        d.d = System.currentTimeMillis();
        for (Map.Entry<Integer, qe7> entry : this.f3103a.entrySet()) {
            entry.getValue().a(d);
        }
    }

    @DexIgnore
    public final ne7 c(List<Integer> list) {
        ne7 e;
        if (list.size() >= 0) {
            for (Integer num : list) {
                qe7 qe7 = this.f3103a.get(num);
                if (!(qe7 == null || (e = qe7.e()) == null || !se7.g(e.c))) {
                    return e;
                }
            }
        }
        return new ne7();
    }

    @DexIgnore
    public final ne7 d() {
        return c(new ArrayList(Arrays.asList(1, 2, 4)));
    }
}
