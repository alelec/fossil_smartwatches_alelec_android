package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class i44<T> implements Comparator<T> {
    @DexIgnore
    public static /* final */ int LEFT_IS_GREATER; // = 1;
    @DexIgnore
    public static /* final */ int RIGHT_IS_GREATER; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends i44<Object> {
        @DexIgnore
        public /* final */ AtomicInteger b; // = new AtomicInteger(0);
        @DexIgnore
        public /* final */ ConcurrentMap<Object, Integer> c;

        @DexIgnore
        public a() {
            u34 u34 = new u34();
            k44.b(u34);
            this.c = u34.i();
        }

        @DexIgnore
        public final Integer a(Object obj) {
            Integer num = this.c.get(obj);
            if (num != null) {
                return num;
            }
            Integer valueOf = Integer.valueOf(this.b.getAndIncrement());
            Integer putIfAbsent = this.c.putIfAbsent(obj, valueOf);
            return putIfAbsent != null ? putIfAbsent : valueOf;
        }

        @DexIgnore
        public int b(Object obj) {
            return System.identityHashCode(obj);
        }

        @DexIgnore
        @Override // com.fossil.i44, java.util.Comparator
        public int compare(Object obj, Object obj2) {
            if (obj == obj2) {
                return 0;
            }
            if (obj == null) {
                return -1;
            }
            if (obj2 == null) {
                return 1;
            }
            int b2 = b(obj);
            int b3 = b(obj2);
            if (b2 != b3) {
                return b2 >= b3 ? 1 : -1;
            }
            int compareTo = a(obj).compareTo(a(obj2));
            if (compareTo != 0) {
                return compareTo;
            }
            throw new AssertionError();
        }

        @DexIgnore
        public String toString() {
            return "Ordering.arbitrary()";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ i44<Object> f1580a; // = new a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends ClassCastException {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Object value;

        @DexIgnore
        public c(Object obj) {
            super("Cannot compare value: " + obj);
            this.value = obj;
        }
    }

    @DexIgnore
    public static i44<Object> allEqual() {
        return x14.INSTANCE;
    }

    @DexIgnore
    public static i44<Object> arbitrary() {
        return b.f1580a;
    }

    @DexIgnore
    public static <T> i44<T> compound(Iterable<? extends Comparator<? super T>> iterable) {
        return new d24(iterable);
    }

    @DexIgnore
    public static <T> i44<T> explicit(T t, T... tArr) {
        return explicit(t34.a(t, tArr));
    }

    @DexIgnore
    public static <T> i44<T> explicit(List<T> list) {
        return new j24(list);
    }

    @DexIgnore
    @Deprecated
    public static <T> i44<T> from(i44<T> i44) {
        i14.l(i44);
        return i44;
    }

    @DexIgnore
    public static <T> i44<T> from(Comparator<T> comparator) {
        return comparator instanceof i44 ? (i44) comparator : new c24(comparator);
    }

    @DexIgnore
    public static <C extends Comparable> i44<C> natural() {
        return e44.INSTANCE;
    }

    @DexIgnore
    public static i44<Object> usingToString() {
        return j54.INSTANCE;
    }

    @DexIgnore
    @Deprecated
    public int binarySearch(List<? extends T> list, T t) {
        return Collections.binarySearch(list, t, this);
    }

    @DexIgnore
    @Override // java.util.Comparator
    @CanIgnoreReturnValue
    public abstract int compare(T t, T t2);

    @DexIgnore
    public <U extends T> i44<U> compound(Comparator<? super U> comparator) {
        i14.l(comparator);
        return new d24(this, comparator);
    }

    @DexIgnore
    public <E extends T> List<E> greatestOf(Iterable<E> iterable, int i) {
        return reverse().leastOf(iterable, i);
    }

    @DexIgnore
    public <E extends T> List<E> greatestOf(Iterator<E> it, int i) {
        return reverse().leastOf(it, i);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> y24<E> immutableSortedCopy(Iterable<E> iterable) {
        Object[] h = o34.h(iterable);
        for (Object obj : h) {
            i14.l(obj);
        }
        Arrays.sort(h, this);
        return y24.asImmutableList(h);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.i44<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean isOrdered(Iterable<? extends T> iterable) {
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            Object next = it.next();
            while (it.hasNext()) {
                Object next2 = it.next();
                if (compare(next, next2) > 0) {
                    return false;
                }
                next = next2;
            }
        }
        return true;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.i44<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean isStrictlyOrdered(Iterable<? extends T> iterable) {
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            Object next = it.next();
            while (it.hasNext()) {
                Object next2 = it.next();
                if (compare(next, next2) >= 0) {
                    return false;
                }
                next = next2;
            }
        }
        return true;
    }

    @DexIgnore
    public <E extends T> List<E> leastOf(Iterable<E> iterable, int i) {
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (((long) collection.size()) <= ((long) i) * 2) {
                Object[] array = collection.toArray();
                Arrays.sort(array, this);
                if (array.length > i) {
                    array = h44.a(array, i);
                }
                return Collections.unmodifiableList(Arrays.asList(array));
            }
        }
        return leastOf(iterable.iterator(), i);
    }

    @DexIgnore
    public <E extends T> List<E> leastOf(Iterator<E> it, int i) {
        i14.l(it);
        a24.b(i, "k");
        if (i == 0 || !it.hasNext()) {
            return y24.of();
        }
        if (i >= 1073741823) {
            ArrayList i2 = t34.i(it);
            Collections.sort(i2, this);
            if (i2.size() > i) {
                i2.subList(i, i2.size()).clear();
            }
            i2.trimToSize();
            return Collections.unmodifiableList(i2);
        }
        f54 a2 = f54.a(i, this);
        a2.c(it);
        return a2.f();
    }

    @DexIgnore
    public <S extends T> i44<Iterable<S>> lexicographical() {
        return new q34(this);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> E max(Iterable<E> iterable) {
        return (E) max(iterable.iterator());
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: E extends T */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: E extends T */
    /* JADX WARN: Multi-variable type inference failed */
    @CanIgnoreReturnValue
    public <E extends T> E max(E e, E e2) {
        return compare(e, e2) >= 0 ? e : e2;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> E max(E e, E e2, E e3, E... eArr) {
        E e4 = (E) max(max(e, e2), e3);
        for (E e5 : eArr) {
            e4 = (E) max(e4, e5);
        }
        return e4;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> E max(Iterator<E> it) {
        E next = it.next();
        while (it.hasNext()) {
            next = (E) max(next, it.next());
        }
        return next;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> E min(Iterable<E> iterable) {
        return (E) min(iterable.iterator());
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: E extends T */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: E extends T */
    /* JADX WARN: Multi-variable type inference failed */
    @CanIgnoreReturnValue
    public <E extends T> E min(E e, E e2) {
        return compare(e, e2) <= 0 ? e : e2;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> E min(E e, E e2, E e3, E... eArr) {
        E e4 = (E) min(min(e, e2), e3);
        for (E e5 : eArr) {
            e4 = (E) min(e4, e5);
        }
        return e4;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> E min(Iterator<E> it) {
        E next = it.next();
        while (it.hasNext()) {
            next = (E) min(next, it.next());
        }
        return next;
    }

    @DexIgnore
    public <S extends T> i44<S> nullsFirst() {
        return new f44(this);
    }

    @DexIgnore
    public <S extends T> i44<S> nullsLast() {
        return new g44(this);
    }

    @DexIgnore
    public <T2 extends T> i44<Map.Entry<T2, ?>> onKeys() {
        return onResultOf(x34.g());
    }

    @DexIgnore
    public <F> i44<F> onResultOf(b14<F, ? extends T> b14) {
        return new z14(b14, this);
    }

    @DexIgnore
    public <S extends T> i44<S> reverse() {
        return new u44(this);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> List<E> sortedCopy(Iterable<E> iterable) {
        Object[] h = o34.h(iterable);
        Arrays.sort(h, this);
        return t34.h(Arrays.asList(h));
    }
}
