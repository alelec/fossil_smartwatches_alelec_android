package com.fossil;

import android.content.Context;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.lang.Thread;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ig7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static yh7 f1626a;
    @DexIgnore
    public static volatile Map<String, Properties> b; // = new ConcurrentHashMap();
    @DexIgnore
    public static volatile Map<Integer, Integer> c; // = new ConcurrentHashMap(10);
    @DexIgnore
    public static volatile long d; // = 0;
    @DexIgnore
    public static volatile long e; // = 0;
    @DexIgnore
    public static volatile long f; // = 0;
    @DexIgnore
    public static String g; // = "";
    @DexIgnore
    public static volatile int h; // = 0;
    @DexIgnore
    public static volatile String i; // = "";
    @DexIgnore
    public static volatile String j; // = "";
    @DexIgnore
    public static Map<String, Long> k; // = new ConcurrentHashMap();
    @DexIgnore
    public static Map<String, Long> l; // = new ConcurrentHashMap();
    @DexIgnore
    public static th7 m; // = ei7.p();
    @DexIgnore
    public static Thread.UncaughtExceptionHandler n; // = null;
    @DexIgnore
    public static volatile boolean o; // = true;
    @DexIgnore
    public static volatile int p; // = 0;
    @DexIgnore
    public static volatile long q; // = 0;
    @DexIgnore
    public static Context r; // = null;
    @DexIgnore
    public static volatile long s; // = 0;

    /*
    static {
        new ConcurrentHashMap();
    }
    */

    @DexIgnore
    public static void D(Context context, jg7 jg7) {
        if (fg7.M() && l(context) != null) {
            f1626a.a(new ui7(context, jg7));
        }
    }

    @DexIgnore
    public static void E(Context context, jg7 jg7) {
        if (fg7.M() && l(context) != null) {
            f1626a.a(new yg7(context, jg7));
        }
    }

    @DexIgnore
    public static boolean F(Context context, String str, String str2, jg7 jg7) {
        try {
            if (!fg7.M()) {
                m.f("MTA StatService is disable.");
                return false;
            }
            if (fg7.K()) {
                m.b("MTA SDK version, current: 2.0.3 ,required: " + str2);
            }
            if (context == null || str2 == null) {
                m.f("Context or mtaSdkVersion in StatService.startStatService() is null, please check it!");
                fg7.P(false);
                return false;
            } else if (ei7.o("2.0.3") < ei7.o(str2)) {
                m.f(("MTA SDK version conflicted, current: 2.0.3,required: " + str2) + ". please delete the current SDK and download the latest one. official website: http://mta.qq.com/ or http://mta.oa.com/");
                fg7.P(false);
                return false;
            } else {
                String v = fg7.v(context);
                if (v == null || v.length() == 0) {
                    fg7.R(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                }
                if (str != null) {
                    fg7.N(context, str);
                }
                if (l(context) != null) {
                    f1626a.a(new zg7(context, jg7));
                }
                return true;
            }
        } catch (Throwable th) {
            m.e(th);
            return false;
        }
    }

    @DexIgnore
    public static void G(Context context) {
        if (fg7.M()) {
            Context x = x(context);
            if (x == null) {
                m.f("The Context of StatService.testSpeed() can not be null!");
            } else if (l(x) != null) {
                f1626a.a(new vg7(x));
            }
        }
    }

    @DexIgnore
    public static void H(Context context, String str, jg7 jg7) {
        if (fg7.M()) {
            Context x = x(context);
            if (x == null || str == null || str.length() == 0) {
                m.f("The Context or pageName of StatService.trackBeginPage() can not be null or empty!");
                return;
            }
            String str2 = new String(str);
            if (l(x) != null) {
                f1626a.a(new yi7(str2, x, jg7));
            }
        }
    }

    @DexIgnore
    public static void I(Context context, String str, Properties properties, jg7 jg7) {
        th7 th7;
        String str2;
        if (fg7.M()) {
            Context x = x(context);
            if (x == null) {
                th7 = m;
                str2 = "The Context of StatService.trackCustomEvent() can not be null!";
            } else if (h(str)) {
                th7 = m;
                str2 = "The event_id of StatService.trackCustomEvent() can not be null or empty.";
            } else {
                lg7 lg7 = new lg7(str, null, properties);
                if (l(x) != null) {
                    f1626a.a(new xi7(x, jg7, lg7));
                    return;
                }
                return;
            }
            th7.f(str2);
        }
    }

    @DexIgnore
    public static void J(Context context, String str, jg7 jg7) {
        if (fg7.M()) {
            Context x = x(context);
            if (x == null || str == null || str.length() == 0) {
                m.f("The Context or pageName of StatService.trackEndPage() can not be null or empty!");
                return;
            }
            String str2 = new String(str);
            if (l(x) != null) {
                f1626a.a(new xg7(x, str2, jg7));
            }
        }
    }

    @DexIgnore
    public static int a(Context context, boolean z, jg7 jg7) {
        boolean z2 = true;
        long currentTimeMillis = System.currentTimeMillis();
        boolean z3 = z && currentTimeMillis - e >= ((long) fg7.G());
        e = currentTimeMillis;
        if (f == 0) {
            f = ei7.r();
        }
        if (currentTimeMillis >= f) {
            f = ei7.r();
            if (gh7.b(context).v(context).e() != 1) {
                gh7.b(context).v(context).b(1);
            }
            fg7.o(0);
            p = 0;
            g = ei7.g(0);
            z3 = true;
        }
        String str = g;
        if (ei7.m(jg7)) {
            str = jg7.a() + g;
        }
        if (l.containsKey(str)) {
            z2 = z3;
        }
        if (z2) {
            if (ei7.m(jg7)) {
                e(context, jg7);
            } else if (fg7.r() < fg7.y()) {
                ei7.T(context);
                e(context, null);
            } else {
                m.d("Exceed StatConfig.getMaxDaySessionNumbers().");
            }
            l.put(str, 1L);
        }
        if (o) {
            G(context);
            o = false;
        }
        return h;
    }

    @DexIgnore
    public static void d(Context context) {
        synchronized (ig7.class) {
            if (context != null) {
                try {
                    if (f1626a == null) {
                        if (k(context)) {
                            Context applicationContext = context.getApplicationContext();
                            r = applicationContext;
                            f1626a = new yh7();
                            g = ei7.g(0);
                            d = System.currentTimeMillis() + fg7.x;
                            f1626a.a(new ti7(applicationContext));
                        }
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
    }

    @DexIgnore
    public static void e(Context context, jg7 jg7) {
        if (l(context) != null) {
            if (fg7.K()) {
                m.b("start new session.");
            }
            if (jg7 == null || h == 0) {
                h = ei7.d();
            }
            fg7.d(0);
            fg7.n();
            new ch7(new sg7(context, h, j(), jg7)).b();
        }
    }

    @DexIgnore
    public static void f(Context context, Throwable th) {
        if (fg7.M()) {
            Context x = x(context);
            if (x == null) {
                m.f("The Context of StatService.reportSdkSelfException() can not be null!");
            } else if (l(x) != null) {
                f1626a.a(new vi7(x, th));
            }
        }
    }

    @DexIgnore
    public static boolean g() {
        if (p < 2) {
            return false;
        }
        q = System.currentTimeMillis();
        return true;
    }

    @DexIgnore
    public static boolean h(String str) {
        return str == null || str.length() == 0;
    }

    @DexIgnore
    public static JSONObject j() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (fg7.c.d != 0) {
                jSONObject2.put("v", fg7.c.d);
            }
            jSONObject.put(Integer.toString(fg7.c.f2529a), jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            if (fg7.b.d != 0) {
                jSONObject3.put("v", fg7.b.d);
            }
            jSONObject.put(Integer.toString(fg7.b.f2529a), jSONObject3);
        } catch (JSONException e2) {
            m.e(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public static boolean k(Context context) {
        boolean z;
        boolean z2 = false;
        long b2 = ii7.b(context, fg7.n, 0);
        long o2 = ei7.o("2.0.3");
        if (o2 <= b2) {
            th7 th7 = m;
            th7.f("MTA is disable for current version:" + o2 + ",wakeup version:" + b2);
            z = false;
        } else {
            z = true;
        }
        long b3 = ii7.b(context, fg7.o, 0);
        if (b3 > System.currentTimeMillis()) {
            th7 th72 = m;
            th72.f("MTA is disable for current time:" + System.currentTimeMillis() + ",wakeup time:" + b3);
        } else {
            z2 = z;
        }
        fg7.P(z2);
        return z2;
    }

    @DexIgnore
    public static yh7 l(Context context) {
        if (f1626a == null) {
            synchronized (ig7.class) {
                try {
                    if (f1626a == null) {
                        try {
                            d(context);
                        } catch (Throwable th) {
                            m.g(th);
                            fg7.P(false);
                        }
                    }
                } catch (Throwable th2) {
                    throw th2;
                }
            }
        }
        return f1626a;
    }

    @DexIgnore
    public static void n() {
        p = 0;
        q = 0;
    }

    @DexIgnore
    public static void o(Context context, int i2) {
        th7 th7;
        String str;
        if (fg7.M()) {
            if (fg7.K()) {
                th7 th72 = m;
                th72.h("commitEvents, maxNumber=" + i2);
            }
            Context x = x(context);
            if (x == null) {
                th7 = m;
                str = "The Context of StatService.commitEvents() can not be null!";
            } else if (i2 < -1 || i2 == 0) {
                th7 = m;
                str = "The maxNumber of StatService.commitEvents() should be -1 or bigger than 0.";
            } else if (tg7.a(r).j() && l(x) != null) {
                f1626a.a(new ug7(x, i2));
                return;
            } else {
                return;
            }
            th7.f(str);
        }
    }

    @DexIgnore
    public static void p() {
        p++;
        q = System.currentTimeMillis();
        u(r);
    }

    @DexIgnore
    public static void q(Context context) {
        if (fg7.M()) {
            Context x = x(context);
            if (x == null) {
                m.f("The Context of StatService.sendNetworkDetector() can not be null!");
                return;
            }
            try {
                qi7.f(x).c(new pg7(x), new wi7());
            } catch (Throwable th) {
                m.e(th);
            }
        }
    }

    @DexIgnore
    public static void s(Context context) {
        s = System.currentTimeMillis() + ((long) (fg7.F() * 60000));
        ii7.f(context, "last_period_ts", s);
        o(context, -1);
    }

    @DexIgnore
    public static void u(Context context) {
        if (fg7.M() && fg7.J > 0) {
            Context x = x(context);
            if (x == null) {
                m.f("The Context of StatService.testSpeed() can not be null!");
            } else {
                gh7.b(x).B();
            }
        }
    }

    @DexIgnore
    public static Properties w(String str) {
        return b.get(str);
    }

    @DexIgnore
    public static Context x(Context context) {
        return context != null ? context : r;
    }
}
