package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class du5 extends iq4<a, c, b> {
    @DexIgnore
    public LocationSource d;
    @DexIgnore
    public PortfolioApp e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ double f835a;
        @DexIgnore
        public /* final */ double b;

        @DexIgnore
        public c(double d, double d2) {
            this.f835a = d;
            this.b = d2;
        }

        @DexIgnore
        public final double a() {
            return this.f835a;
        }

        @DexIgnore
        public final double b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.locate.map.usecase.GetUserLocation", f = "GetUserLocation.kt", l = {16}, m = "run")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ du5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(du5 du5, qn7 qn7) {
            super(qn7);
            this.this$0 = du5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore
    public du5(LocationSource locationSource, PortfolioApp portfolioApp) {
        pq7.c(locationSource, "mLocationSource");
        pq7.c(portfolioApp, "mPortfolioApp");
        this.d = locationSource;
        this.e = portfolioApp;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "GetUserLocation";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.du5.a r11, com.fossil.qn7<java.lang.Object> r12) {
        /*
            r10 = this;
            r8 = 0
            r7 = 1
            r2 = 0
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            boolean r0 = r12 instanceof com.fossil.du5.d
            if (r0 == 0) goto L_0x004d
            r0 = r12
            com.fossil.du5$d r0 = (com.fossil.du5.d) r0
            int r1 = r0.label
            r4 = r1 & r5
            if (r4 == 0) goto L_0x004d
            int r1 = r1 + r5
            r0.label = r1
            r6 = r0
        L_0x0017:
            java.lang.Object r1 = r6.result
            java.lang.Object r9 = com.fossil.yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x005b
            if (r0 != r7) goto L_0x0053
            java.lang.Object r0 = r6.L$1
            com.fossil.du5$a r0 = (com.fossil.du5.a) r0
            java.lang.Object r0 = r6.L$0
            com.fossil.du5 r0 = (com.fossil.du5) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x002f:
            com.portfolio.platform.data.LocationSource$Result r0 = (com.portfolio.platform.data.LocationSource.Result) r0
            com.portfolio.platform.data.LocationSource$ErrorState r1 = r0.getErrorState()
            com.portfolio.platform.data.LocationSource$ErrorState r2 = com.portfolio.platform.data.LocationSource.ErrorState.SUCCESS
            if (r1 != r2) goto L_0x0081
            android.location.Location r0 = r0.getLocation()
            if (r0 == 0) goto L_0x004c
            com.fossil.du5$c r8 = new com.fossil.du5$c
            double r2 = r0.getLatitude()
            double r0 = r0.getLongitude()
            r8.<init>(r2, r0)
        L_0x004c:
            return r8
        L_0x004d:
            com.fossil.du5$d r6 = new com.fossil.du5$d
            r6.<init>(r10, r12)
            goto L_0x0017
        L_0x0053:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005b:
            com.fossil.el7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = "GetUserLocation"
            java.lang.String r4 = "running UseCase"
            r0.d(r1, r4)
            com.portfolio.platform.data.LocationSource r0 = r10.d
            com.portfolio.platform.PortfolioApp r1 = r10.e
            r6.L$0 = r10
            r6.L$1 = r11
            r6.label = r7
            r7 = 28
            r4 = r3
            r5 = r2
            java.lang.Object r0 = com.portfolio.platform.data.LocationSource.getLocation$default(r0, r1, r2, r3, r4, r5, r6, r7, r8)
            if (r0 != r9) goto L_0x002f
            r8 = r9
            goto L_0x004c
        L_0x0081:
            com.fossil.du5$b r8 = new com.fossil.du5$b
            r8.<init>()
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.du5.k(com.fossil.du5$a, com.fossil.qn7):java.lang.Object");
    }
}
