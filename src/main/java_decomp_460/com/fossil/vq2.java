package com.fossil;

import android.location.Location;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vq2 extends mq2 implements uq2 {
    @DexIgnore
    public vq2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IGoogleLocationManagerService");
    }

    @DexIgnore
    @Override // com.fossil.uq2
    public final void O2(ia3 ia3, wq2 wq2, String str) throws RemoteException {
        Parcel d = d();
        qr2.c(d, ia3);
        qr2.b(d, wq2);
        d.writeString(str);
        i(63, d);
    }

    @DexIgnore
    @Override // com.fossil.uq2
    public final void X1(boolean z) throws RemoteException {
        Parcel d = d();
        qr2.d(d, z);
        i(12, d);
    }

    @DexIgnore
    @Override // com.fossil.uq2
    public final void c1(vr2 vr2) throws RemoteException {
        Parcel d = d();
        qr2.c(d, vr2);
        i(75, d);
    }

    @DexIgnore
    @Override // com.fossil.uq2
    public final void g2(kr2 kr2) throws RemoteException {
        Parcel d = d();
        qr2.c(d, kr2);
        i(59, d);
    }

    @DexIgnore
    @Override // com.fossil.uq2
    public final Location zza(String str) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        Parcel e = e(21, d);
        Location location = (Location) qr2.a(e, Location.CREATOR);
        e.recycle();
        return location;
    }
}
