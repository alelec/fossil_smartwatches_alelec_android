package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface m64 {

    @DexIgnore
    public interface a {
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, Bundle bundle);
    }

    @DexIgnore
    void a(String str, String str2, Bundle bundle);

    @DexIgnore
    void b(String str, String str2, Object obj);

    @DexIgnore
    a c(String str, b bVar);
}
