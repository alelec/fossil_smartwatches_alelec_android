package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.cw1;
import java.nio.charset.Charset;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ew1 extends mv1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ vb0 e;
    @DexIgnore
    public String f;
    @DexIgnore
    public float g;
    @DexIgnore
    public String h;
    @DexIgnore
    public cw1 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ew1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ew1 createFromParcel(Parcel parcel) {
            return new ew1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ew1[] newArray(int i) {
            return new ew1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ew1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.e = vb0.TEXT;
        String readString = parcel.readString();
        if (readString != null) {
            this.f = readString;
            this.g = parcel.readFloat();
            String readString2 = parcel.readString();
            if (readString2 != null) {
                this.h = readString2;
                this.i = cw1.values()[parcel.readInt()];
                return;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public ew1(jv1 jv1, kv1 kv1, String str, float f2, String str2, cw1 cw1) {
        this(g80.e(0, 1), jv1, kv1, str, f2, str2, cw1);
    }

    @DexIgnore
    public ew1(String str, jv1 jv1, kv1 kv1, String str2, float f2, String str3, cw1 cw1) {
        super(str, jv1, kv1);
        this.e = vb0.TEXT;
        this.f = str2;
        this.g = f2;
        this.h = str3;
        this.i = cw1;
    }

    @DexIgnore
    public ew1(JSONObject jSONObject, String str) throws IllegalArgumentException {
        super(jSONObject, str);
        this.e = vb0.TEXT;
        try {
            String string = jSONObject.getString("text");
            pq7.b(string, "jsonObject.getString(UIScriptConstant.TEXT)");
            String string2 = jSONObject.getString("font_name");
            pq7.b(string2, "jsonObject.getString(UIScriptConstant.FONT_NAME)");
            int i2 = jSONObject.getInt(ViewHierarchy.TEXT_SIZE);
            cw1.a aVar = cw1.d;
            String string3 = jSONObject.getString("font_color");
            pq7.b(string3, "jsonObject.getString(UIScriptConstant.FONT_COLOR)");
            cw1 a2 = aVar.a(string3);
            if (a2 != null) {
                this.f = string;
                this.g = (((float) i2) * 1.0f) / ((float) 240);
                this.h = string2;
                this.i = a2;
                return;
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    @DexIgnore
    public final cc0 a(String str) {
        String jSONObject = d().toString();
        pq7.b(jSONObject, "getUIScript().toString()");
        String a2 = iy1.a(jSONObject);
        Charset c = hd0.y.c();
        if (a2 != null) {
            byte[] bytes = a2.getBytes(c);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return new cc0(str, bytes);
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public vb0 a() {
        return this.e;
    }

    @DexIgnore
    @Override // java.lang.Object, com.fossil.mv1, com.fossil.mv1
    public ew1 clone() {
        return new ew1(getName(), b().clone(), c().clone(), this.f, this.g, this.h, this.i);
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public JSONObject d() {
        JSONObject put = super.d().put("text", this.f).put("font_name", this.h).put(ViewHierarchy.TEXT_SIZE, lr7.b(this.g * ((float) 240))).put("font_color", ey1.a(this.i));
        pq7.b(put, "super.getUIScript()\n    \u2026 textColor.lowerCaseName)");
        return put;
    }

    @DexIgnore
    public final String getFontName() {
        return this.h;
    }

    @DexIgnore
    public final float getFontScaledSize() {
        return this.g;
    }

    @DexIgnore
    public final String getText() {
        return this.f;
    }

    @DexIgnore
    public final cw1 getTextColor() {
        return this.i;
    }

    @DexIgnore
    public final ew1 setFontName(String str) {
        this.h = str;
        return this;
    }

    @DexIgnore
    public final ew1 setFontScaledSize(float f2) {
        this.g = f2;
        return this;
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public ew1 setScaledHeight(float f2) {
        mv1 scaledHeight = super.setScaledHeight(f2);
        if (scaledHeight != null) {
            return (ew1) scaledHeight;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.text.TextElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public ew1 setScaledPosition(jv1 jv1) {
        mv1 scaledPosition = super.setScaledPosition(jv1);
        if (scaledPosition != null) {
            return (ew1) scaledPosition;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.text.TextElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public ew1 setScaledSize(kv1 kv1) {
        mv1 scaledSize = super.setScaledSize(kv1);
        if (scaledSize != null) {
            return (ew1) scaledSize;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.text.TextElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public ew1 setScaledWidth(float f2) {
        mv1 scaledWidth = super.setScaledWidth(f2);
        if (scaledWidth != null) {
            return (ew1) scaledWidth;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.text.TextElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public ew1 setScaledX(float f2) {
        mv1 scaledX = super.setScaledX(f2);
        if (scaledX != null) {
            return (ew1) scaledX;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.text.TextElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public ew1 setScaledY(float f2) {
        mv1 scaledY = super.setScaledY(f2);
        if (scaledY != null) {
            return (ew1) scaledY;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.text.TextElement");
    }

    @DexIgnore
    public final ew1 setText(String str) {
        this.f = str;
        return this;
    }

    @DexIgnore
    public final ew1 setTextColor(cw1 cw1) {
        this.i = cw1;
        return this;
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeString(this.f);
        }
        if (parcel != null) {
            parcel.writeFloat(this.g);
        }
        if (parcel != null) {
            parcel.writeString(this.h);
        }
        if (parcel != null) {
            parcel.writeInt(this.i.ordinal());
        }
    }
}
