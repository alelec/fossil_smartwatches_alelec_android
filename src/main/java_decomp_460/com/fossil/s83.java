package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s83 implements xw2<r83> {
    @DexIgnore
    public static s83 c; // = new s83();
    @DexIgnore
    public /* final */ xw2<r83> b;

    @DexIgnore
    public s83() {
        this(ww2.b(new u83()));
    }

    @DexIgnore
    public s83(xw2<r83> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((r83) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ r83 zza() {
        return this.b.zza();
    }
}
