package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.text.format.DateUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.ht5;
import com.fossil.iq4;
import com.fossil.ot5;
import com.fossil.qt5;
import com.fossil.vt5;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f17 extends hq4 {
    @DexIgnore
    public static /* final */ String B;
    @DexIgnore
    public static /* final */ b C; // = new b(null);
    @DexIgnore
    public /* final */ PortfolioApp A;
    @DexIgnore
    public MutableLiveData<c> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Handler i; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public MutableLiveData<String> j;
    @DexIgnore
    public /* final */ LiveData<Device> k;
    @DexIgnore
    public d l;
    @DexIgnore
    public c m;
    @DexIgnore
    public String n;
    @DexIgnore
    public /* final */ Runnable o;
    @DexIgnore
    public ls0<Device> p;
    @DexIgnore
    public /* final */ MutableLiveData<a> q;
    @DexIgnore
    public /* final */ i r;
    @DexIgnore
    public /* final */ DeviceRepository s;
    @DexIgnore
    public /* final */ ot5 t;
    @DexIgnore
    public /* final */ vt5 u;
    @DexIgnore
    public /* final */ mj5 v;
    @DexIgnore
    public /* final */ on5 w;
    @DexIgnore
    public /* final */ qt5 x;
    @DexIgnore
    public /* final */ ht5 y;
    @DexIgnore
    public /* final */ tt4 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f1025a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public ps4 c;

        @DexIgnore
        public a(boolean z, boolean z2, ps4 ps4) {
            this.f1025a = z;
            this.b = z2;
            this.c = ps4;
        }

        @DexIgnore
        public final ps4 a() {
            return this.c;
        }

        @DexIgnore
        public final boolean b() {
            return this.f1025a;
        }

        @DexIgnore
        public final boolean c() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!(this.f1025a == aVar.f1025a && this.b == aVar.b && pq7.a(this.c, aVar.c))) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 1;
            boolean z = this.f1025a;
            if (z) {
                z = true;
            }
            boolean z2 = this.b;
            if (!z2) {
                i = z2 ? 1 : 0;
            }
            ps4 ps4 = this.c;
            int hashCode = ps4 != null ? ps4.hashCode() : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            return (((i2 * 31) + i) * 31) + hashCode;
        }

        @DexIgnore
        public String toString() {
            return "CheckingChallengeWrapper(needToLeave=" + this.f1025a + ", isSwitch=" + this.b + ", challenge=" + this.c + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return f17.B;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f1026a;
        @DexIgnore
        public d b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public cl7<Integer, String> e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;
        @DexIgnore
        public String i;
        @DexIgnore
        public String j;
        @DexIgnore
        public String k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public boolean m;

        @DexIgnore
        public c() {
            this(false, null, null, null, null, false, null, null, null, null, null, false, false, 8191, null);
        }

        @DexIgnore
        public c(boolean z, d dVar, String str, Integer num, cl7<Integer, String> cl7, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4) {
            this.f1026a = z;
            this.b = dVar;
            this.c = str;
            this.d = num;
            this.e = cl7;
            this.f = z2;
            this.g = str2;
            this.h = str3;
            this.i = str4;
            this.j = str5;
            this.k = str6;
            this.l = z3;
            this.m = z4;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(boolean z, d dVar, String str, Integer num, cl7 cl7, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4, int i2, kq7 kq7) {
            this((i2 & 1) != 0 ? false : z, (i2 & 2) != 0 ? null : dVar, (i2 & 4) != 0 ? null : str, (i2 & 8) != 0 ? null : num, (i2 & 16) != 0 ? null : cl7, (i2 & 32) != 0 ? false : z2, (i2 & 64) != 0 ? null : str2, (i2 & 128) != 0 ? null : str3, (i2 & 256) != 0 ? null : str4, (i2 & 512) != 0 ? null : str5, (i2 & 1024) == 0 ? str6 : null, (i2 & 2048) != 0 ? false : z3, (i2 & 4096) == 0 ? z4 : false);
        }

        @DexIgnore
        public static /* synthetic */ void q(c cVar, boolean z, d dVar, String str, Integer num, cl7 cl7, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4, int i2, Object obj) {
            boolean z5 = false;
            boolean z6 = (i2 & 1) != 0 ? false : z;
            String str7 = null;
            d dVar2 = (i2 & 2) != 0 ? null : dVar;
            String str8 = (i2 & 4) != 0 ? null : str;
            Integer num2 = (i2 & 8) != 0 ? null : num;
            cl7 cl72 = (i2 & 16) != 0 ? null : cl7;
            boolean z7 = (i2 & 32) != 0 ? false : z2;
            String str9 = (i2 & 64) != 0 ? null : str2;
            String str10 = (i2 & 128) != 0 ? null : str3;
            String str11 = (i2 & 256) != 0 ? null : str4;
            String str12 = (i2 & 512) != 0 ? null : str5;
            if ((i2 & 1024) == 0) {
                str7 = str6;
            }
            boolean z8 = (i2 & 2048) != 0 ? false : z3;
            if ((i2 & 4096) == 0) {
                z5 = z4;
            }
            cVar.p(z6, dVar2, str8, num2, cl72, z7, str9, str10, str11, str12, str7, z8, z5);
        }

        @DexIgnore
        public final boolean a() {
            return this.f1026a;
        }

        @DexIgnore
        public final String b() {
            return this.j;
        }

        @DexIgnore
        public final String c() {
            return this.g;
        }

        @DexIgnore
        public final boolean d() {
            return this.f;
        }

        @DexIgnore
        public final String e() {
            return this.i;
        }

        @DexIgnore
        public final cl7<Integer, String> f() {
            return this.e;
        }

        @DexIgnore
        public final boolean g() {
            return this.l;
        }

        @DexIgnore
        public final String h() {
            return this.k;
        }

        @DexIgnore
        public final String i() {
            return this.h;
        }

        @DexIgnore
        public final boolean j() {
            return this.m;
        }

        @DexIgnore
        public final d k() {
            return this.b;
        }

        @DexIgnore
        public final String l() {
            return this.c;
        }

        @DexIgnore
        public final Integer m() {
            return this.d;
        }

        @DexIgnore
        public final void n(d dVar) {
            this.b = dVar;
        }

        @DexIgnore
        public final void o(String str) {
            this.c = str;
        }

        @DexIgnore
        public final void p(boolean z, d dVar, String str, Integer num, cl7<Integer, String> cl7, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4) {
            synchronized (this) {
                this.f1026a = z;
                this.b = dVar;
                this.c = str;
                this.d = num;
                this.e = cl7;
                this.f = z2;
                this.g = str2;
                this.h = str3;
                this.i = str4;
                this.j = str5;
                this.k = str6;
                this.l = z3;
                this.m = z4;
            }
        }

        @DexIgnore
        public String toString() {
            String t = new Gson().t(this);
            pq7.b(t, "Gson().toJson(this)");
            return t;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Device f1027a;
        @DexIgnore
        public String b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public Boolean e;

        @DexIgnore
        public d(Device device, String str, boolean z, boolean z2, Boolean bool) {
            pq7.c(device, "deviceModel");
            pq7.c(str, "deviceName");
            this.f1027a = device;
            this.b = str;
            this.c = z;
            this.d = z2;
            this.e = bool;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(Device device, String str, boolean z, boolean z2, Boolean bool, int i, kq7 kq7) {
            this(device, str, z, z2, (i & 16) != 0 ? null : bool);
        }

        @DexIgnore
        public final Device a() {
            return this.f1027a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final Boolean c() {
            return this.e;
        }

        @DexIgnore
        public final boolean d() {
            return this.d;
        }

        @DexIgnore
        public final boolean e() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof d) {
                    d dVar = (d) obj;
                    if (!pq7.a(this.f1027a, dVar.f1027a) || !pq7.a(this.b, dVar.b) || this.c != dVar.c || this.d != dVar.d || !pq7.a(this.e, dVar.e)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 1;
            int i2 = 0;
            Device device = this.f1027a;
            int hashCode = device != null ? device.hashCode() : 0;
            String str = this.b;
            int hashCode2 = str != null ? str.hashCode() : 0;
            boolean z = this.c;
            if (z) {
                z = true;
            }
            boolean z2 = this.d;
            if (!z2) {
                i = z2 ? 1 : 0;
            }
            Boolean bool = this.e;
            if (bool != null) {
                i2 = bool.hashCode();
            }
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = z ? 1 : 0;
            return (((((((hashCode * 31) + hashCode2) * 31) + i3) * 31) + i) * 31) + i2;
        }

        @DexIgnore
        public String toString() {
            return "WatchSettingWrapper(deviceModel=" + this.f1027a + ", deviceName=" + this.b + ", isConnected=" + this.c + ", isActive=" + this.d + ", shouldShowVibrationUI=" + this.e + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$checkToReconnectOrSwitchActiveDevice$1", f = "WatchSettingViewModel.kt", l = {MFNetworkReturnCode.BAD_REQUEST}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $currentActiveSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ f17 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$checkToReconnectOrSwitchActiveDevice$1$challenge$1", f = "WatchSettingViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super ps4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ps4> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.z.g(new String[]{"running", "waiting"}, xy4.f4212a.a());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(f17 f17, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = f17;
            this.$currentActiveSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, this.$currentActiveSerial, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                Boolean b = this.this$0.w.b();
                pq7.b(b, "mSharedPreferencesManager.bcStatus()");
                if (b.booleanValue()) {
                    dv7 b2 = bw7.b();
                    a aVar = new a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    g = eu7.g(b2, aVar, this);
                    if (g == d) {
                        return d;
                    }
                } else {
                    if (!vt7.l(this.$currentActiveSerial)) {
                        this.this$0.d0(this.$currentActiveSerial, 0);
                    } else {
                        f17 f17 = this.this$0;
                        Object e = f17.j.e();
                        if (e != null) {
                            pq7.b(e, "mSerialLiveData.value!!");
                            f17.I((String) e, 1);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                    return tl7.f3441a;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ps4 ps4 = (ps4) g;
            if (ps4 != null) {
                hq4.d(this.this$0, false, false, null, 6, null);
                this.this$0.q.l(new a(true, true, ps4));
            } else {
                this.this$0.w.m0(ao7.a(false));
                if (!vt7.l(this.$currentActiveSerial)) {
                    this.this$0.d0(this.$currentActiveSerial, 0);
                } else {
                    f17 f172 = this.this$0;
                    Object e2 = f172.j.e();
                    if (e2 != null) {
                        pq7.b(e2, "mSerialLiveData.value!!");
                        f172.I((String) e2, 1);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$checkingBeforeRemoveDevice$1", f = "WatchSettingViewModel.kt", l = {545}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isSwitch;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ f17 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$checkingBeforeRemoveDevice$1$challenge$1", f = "WatchSettingViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super ps4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ps4> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.z.g(new String[]{"running", "waiting"}, xy4.f4212a.a());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(f17 f17, boolean z, qn7 qn7) {
            super(2, qn7);
            this.this$0 = f17;
            this.$isSwitch = z;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, this.$isSwitch, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                Boolean b = this.this$0.w.b();
                pq7.b(b, "mSharedPreferencesManager.bcStatus()");
                if (b.booleanValue()) {
                    String J = PortfolioApp.h0.c().J();
                    Device device = (Device) this.this$0.k.e();
                    if (!pq7.a(J, device != null ? device.getDeviceId() : null)) {
                        this.this$0.q.l(new a(false, this.$isSwitch, null));
                    } else {
                        dv7 b2 = bw7.b();
                        a aVar = new a(this, null);
                        this.L$0 = iv7;
                        this.L$1 = J;
                        this.label = 1;
                        g = eu7.g(b2, aVar, this);
                        if (g == d) {
                            return d;
                        }
                    }
                } else {
                    this.this$0.q.l(new a(false, this.$isSwitch, null));
                }
                hq4.d(this.this$0, false, false, null, 6, null);
                return tl7.f3441a;
            } else if (i == 1) {
                String str = (String) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ps4 ps4 = (ps4) g;
            if (ps4 == null) {
                this.this$0.w.m0(ao7.a(false));
                this.this$0.q.l(new a(false, this.$isSwitch, null));
            } else {
                this.this$0.q.l(new a(true, this.$isSwitch, ps4));
            }
            hq4.d(this.this$0, false, false, null, 6, null);
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements iq4.e<vt5.d, vt5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f17 f1028a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public g(f17 f17, String str) {
            this.f1028a = f17;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(vt5.c cVar) {
            pq7.c(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = f17.C.a();
            local.d(a2, "remove device " + this.f1028a.j + ".value fail due to " + cVar.b());
            hq4.d(this.f1028a, false, true, null, 5, null);
            String b2 = cVar.b();
            switch (b2.hashCode()) {
                case -1767173543:
                    if (!b2.equals("UNLINK_FAIL_DUE_TO_STOP_WORKOUT_FAIL")) {
                        return;
                    }
                    break;
                case -1697024179:
                    if (b2.equals("UNLINK_FAIL_DUE_TO_LACK_PERMISSION")) {
                        if (cVar.c() != null) {
                            List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(new ArrayList(cVar.c()));
                            pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                            f17 f17 = this.f1028a;
                            Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                            if (array != null) {
                                uh5[] uh5Arr = (uh5[]) array;
                                f17.e((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                            } else {
                                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                            }
                        }
                        this.f1028a.J();
                        return;
                    }
                    return;
                case -643272338:
                    if (b2.equals("UNLINK_FAIL_ON_SERVER")) {
                        Integer num = (cVar.c() == null || !(cVar.c().isEmpty() ^ true)) ? 600 : cVar.c().get(0);
                        pq7.b(num, "if (errorValue.subErrorC\u2026                        }");
                        int intValue = num.intValue();
                        c cVar2 = this.f1028a.m;
                        String a3 = cVar.a();
                        c.q(cVar2, false, null, null, null, new cl7(Integer.valueOf(intValue), a3 != null ? a3 : ""), false, null, null, null, null, null, false, false, 8175, null);
                        this.f1028a.J();
                        return;
                    }
                    return;
                case 1447890910:
                    if (!b2.equals("UNLINK_FAIL_DUE_TO_SYNC_FAIL")) {
                        return;
                    }
                    break;
                case 1768665169:
                    if (b2.equals("UNLINK_FAIL_DUE_TO_PENDING_WORKOUT")) {
                        c.q(this.f1028a.m, false, null, null, null, null, false, null, null, null, this.b, null, false, false, 7679, null);
                        this.f1028a.J();
                        return;
                    }
                    return;
                default:
                    return;
            }
            c.q(this.f1028a.m, false, null, null, null, null, false, null, null, null, null, this.b, false, false, 7167, null);
            this.f1028a.J();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(vt5.d dVar) {
            pq7.c(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = f17.C.a();
            local.d(a2, "removeDevice success serial=" + this.f1028a.j + ".value");
            hq4.d(this.f1028a, false, true, null, 5, null);
            c.q(this.f1028a.m, true, null, null, null, null, false, null, null, null, null, null, false, false, 8190, null);
            this.f1028a.J();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements iq4.e<qt5.d, qt5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f17 f1029a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public h(f17 f17, String str) {
            this.f1029a = f17;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(qt5.c cVar) {
            pq7.c(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = f17.C.a();
            local.d(a2, "removeDevice switch to " + this.b + " fail due to " + cVar.b());
            hq4.d(this.f1029a, false, true, null, 5, null);
            int b2 = cVar.b();
            if (b2 == 113) {
                if (cVar.c() != null) {
                    List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(new ArrayList(cVar.c()));
                    pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                    f17 f17 = this.f1029a;
                    Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                    if (array != null) {
                        uh5[] uh5Arr = (uh5[]) array;
                        f17.e((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    } else {
                        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                this.f1029a.J();
            } else if (b2 == 114) {
                Integer num = (cVar.c() == null || !(cVar.c().isEmpty() ^ true)) ? 600 : cVar.c().get(0);
                pq7.b(num, "if (errorValue.subErrorC\u2026                        }");
                int intValue = num.intValue();
                c cVar2 = this.f1029a.m;
                String a3 = cVar.a();
                c.q(cVar2, false, null, null, null, new cl7(Integer.valueOf(intValue), a3 != null ? a3 : ""), false, null, null, null, null, null, false, false, 8175, null);
                this.f1029a.J();
            } else if (b2 == 117) {
                c.q(this.f1029a.m, false, null, null, null, null, false, null, null, this.b, null, null, false, false, 7935, null);
                this.f1029a.J();
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(qt5.d dVar) {
            pq7.c(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = f17.C.a();
            local.d(a2, "removeDevice(), switch to newDevice=" + this.b + " success");
            hq4.d(this.f1029a, false, true, null, 5, null);
            this.f1029a.a0(this.b);
            this.f1029a.Q(dVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f17 f1030a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(f17 f17) {
            this.f1030a = f17;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Device device;
            pq7.c(context, "context");
            pq7.c(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = f17.C.a();
            local.d(a2, "onConnectionStateChangeReceiver, serial=" + stringExtra);
            if (pq7.a(stringExtra, (String) this.f1030a.j.e()) && vt7.j(stringExtra, this.f1030a.A.J(), true)) {
                FLogger.INSTANCE.getLocal().d(f17.C.a(), "onConnectionStateChanged");
                LiveData liveData = this.f1030a.k;
                if (!(liveData == null || (device = (Device) liveData.e()) == null)) {
                    this.f1030a.Q(device);
                }
                hq4.d(this.f1030a, false, true, null, 5, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f17 f1031a;

        @DexIgnore
        public j(f17 f17) {
            this.f1031a = f17;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<Device> apply(String str) {
            DeviceRepository deviceRepository = this.f1031a.s;
            pq7.b(str, "serial");
            return deviceRepository.getDeviceBySerialAsLiveData(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ f17 b;

        @DexIgnore
        public k(f17 f17) {
            this.b = f17;
        }

        @DexIgnore
        public final void run() {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements ls0<Device> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f17 f1032a;

        @DexIgnore
        public l(f17 f17) {
            this.f1032a = f17;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Device device) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = f17.C.a();
            local.d(a2, "on device changed " + device);
            this.f1032a.Q(device);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Device $device$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Device $this_run;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ f17 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ m this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(m mVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = mVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.s.getDeviceNameBySerial(this.this$0.$this_run.getDeviceId());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(Device device, qn7 qn7, f17 f17, Device device2) {
            super(2, qn7);
            this.$this_run = device;
            this.this$0 = f17;
            this.$device$inlined = device2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            m mVar = new m(this.$this_run, qn7, this.this$0, this.$device$inlined);
            mVar.p$ = (iv7) obj;
            return mVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((m) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            String str;
            boolean z;
            iv7 iv7;
            boolean z2;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv72 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = f17.C.a();
                local.d(a2, "onDeviceChanged - device: " + this.$device$inlined);
                String J = PortfolioApp.h0.c().J();
                boolean z3 = !FossilDeviceSerialPatternUtil.isSamSlimDevice(this.$this_run.getDeviceId()) && !FossilDeviceSerialPatternUtil.isDianaDevice(this.$this_run.getDeviceId());
                if (this.$this_run.getVibrationStrength() == null) {
                    this.$this_run.setVibrationStrength(ao7.e(50));
                    tl7 tl7 = tl7.f3441a;
                }
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv72;
                this.L$1 = J;
                this.Z$0 = z3;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
                str = J;
                z = z3;
                iv7 = iv72;
            } else if (i == 1) {
                boolean z4 = this.Z$0;
                el7.b(obj);
                g = obj;
                str = (String) this.L$1;
                z = z4;
                iv7 = (iv7) this.L$0;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str2 = (String) g;
            if (this.$this_run.getBatteryLevel() > 100) {
                this.$this_run.setBatteryLevel(100);
            }
            if (pq7.a(this.$this_run.getDeviceId(), str)) {
                try {
                    IButtonConnectivity b2 = PortfolioApp.h0.b();
                    z2 = b2 != null && b2.getGattState(this.$this_run.getDeviceId()) == 2;
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = f17.C.a();
                    local2.e(a3, "exception when get gatt state of " + iv7);
                    z2 = false;
                }
                this.this$0.l = new d(this.$device$inlined, str2, z2, true, ao7.a(z));
            } else {
                this.this$0.l = new d(this.$device$inlined, str2, false, false, ao7.a(z));
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String a4 = f17.C.a();
            local3.d(a4, "onDeviceChanged, mDeviceWrapper=" + this.this$0.l);
            c cVar = this.this$0.m;
            d dVar = this.this$0.l;
            if (dVar != null) {
                c.q(cVar, false, dVar, null, null, null, false, null, null, null, null, null, false, false, 8189, null);
                this.this$0.J();
                this.this$0.i.removeCallbacksAndMessages(null);
                this.this$0.e0();
                return tl7.f3441a;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements iq4.e<qt5.d, qt5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f17 f1033a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public n(f17 f17, String str) {
            this.f1033a = f17;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(qt5.c cVar) {
            pq7.c(cVar, "errorValue");
            Integer num = (cVar.c() == null || !(cVar.c().isEmpty() ^ true)) ? 600 : cVar.c().get(0);
            pq7.b(num, "if (errorValue.subErrorC\u2026                        }");
            int intValue = num.intValue();
            hq4.d(this.f1033a, false, true, null, 5, null);
            c cVar2 = this.f1033a.m;
            String a2 = cVar.a();
            c.q(cVar2, false, null, null, null, new cl7(Integer.valueOf(intValue), a2 != null ? a2 : ""), false, null, null, null, null, null, false, false, 8175, null);
            this.f1033a.J();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(qt5.d dVar) {
            pq7.c(dVar, "responseValue");
            hq4.d(this.f1033a, false, true, null, 5, null);
            this.f1033a.a0(this.b);
            this.f1033a.Q(dVar.a());
            this.f1033a.Y();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements iq4.e<ht5.e, ht5.d> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f17 f1034a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public o(f17 f17) {
            this.f1034a = f17;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(ht5.d dVar) {
            pq7.c(dVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = f17.C.a();
            local.d(a2, "reconnectActiveDevice fail!! errorValue=" + dVar.a());
            hq4.d(this.f1034a, false, true, null, 5, null);
            int c = dVar.c();
            if (c == 1101 || c == 1112 || c == 1113) {
                List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(dVar.b());
                pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                f17 f17 = this.f1034a;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    f17.e((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    this.f1034a.J();
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
            c.q(this.f1034a.m, false, null, null, null, null, true, null, null, null, null, null, false, false, 8159, null);
            this.f1034a.J();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ht5.e eVar) {
            pq7.c(eVar, "responseValue");
            hq4.d(this.f1034a, false, true, null, 5, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements iq4.e<ut5, st5> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f17 f1035a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public p(f17 f17, String str, int i) {
            this.f1035a = f17;
            this.b = str;
            this.c = i;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(st5 st5) {
            pq7.c(st5, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = f17.C.a();
            local.d(a2, "syncDevice fail - serial=" + this.b + " - errorCode=" + st5.a());
            int i = g17.f1246a[st5.a().ordinal()];
            if (i == 1) {
                FLogger.INSTANCE.getLocal().d(f17.C.a(), "Sync of this device is in progress, keep waiting for result");
            } else if (i == 2) {
                if (st5.b() != null) {
                    List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(new ArrayList(st5.b()));
                    pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                    f17 f17 = this.f1035a;
                    Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                    if (array != null) {
                        uh5[] uh5Arr = (uh5[]) array;
                        f17.e((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                        this.f1035a.J();
                    } else {
                        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                hq4.d(this.f1035a, false, true, null, 5, null);
            } else if (i == 3) {
                int i2 = this.c;
                if (i2 == 0) {
                    c.q(this.f1035a.m, false, null, null, null, null, false, this.b, null, null, null, null, false, false, 8127, null);
                    this.f1035a.J();
                } else if (i2 == 1) {
                    c.q(this.f1035a.m, false, null, null, null, null, false, null, null, null, this.b, null, false, false, 7679, null);
                    this.f1035a.J();
                }
                hq4.d(this.f1035a, false, true, null, 5, null);
            } else if (i == 4) {
                int i3 = this.c;
                if (i3 == 0) {
                    c cVar = this.f1035a.m;
                    Object e = this.f1035a.j.e();
                    if (e != null) {
                        c.q(cVar, false, null, null, null, null, false, null, (String) e, null, null, null, false, false, 8063, null);
                        this.f1035a.J();
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else if (i3 == 1) {
                    c cVar2 = this.f1035a.m;
                    Object e2 = this.f1035a.j.e();
                    if (e2 != null) {
                        c.q(cVar2, false, null, null, null, null, false, null, null, null, null, (String) e2, false, false, 7167, null);
                        this.f1035a.J();
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                hq4.d(this.f1035a, false, true, null, 5, null);
            } else if (i != 5) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = f17.C.a();
                local2.d(a3, "errorValue.lastErrorCode=" + st5.a());
                c.q(this.f1035a.m, false, null, null, null, null, true, null, null, null, null, null, false, false, 8159, null);
                this.f1035a.J();
                hq4.d(this.f1035a, false, true, null, 5, null);
            } else {
                FLogger.INSTANCE.getLocal().d(f17.C.a(), "User deny stopping workout");
                String build = ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SYNC_CURRENT_DEVICE, ErrorCodeBuilder.Component.APP, ErrorCodeBuilder.AppError.USER_CANCELLED);
                int i4 = this.c;
                if (i4 == 0) {
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.SWITCH_DEVICE;
                    Object e3 = this.f1035a.j.e();
                    if (e3 != null) {
                        pq7.b(e3, "mSerialLiveData.value!!");
                        remote.e(component, session, (String) e3, f17.C.a(), build, ErrorCodeBuilder.Step.SYNC_CURRENT_DEVICE, "User deny stopping workout");
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else if (i4 == 1) {
                    FLogger.INSTANCE.getRemote().e(FLogger.Component.APP, FLogger.Session.REMOVE_DEVICE, this.b, f17.C.a(), build, ErrorCodeBuilder.Step.SYNC_CURRENT_DEVICE, "User deny stopping workout");
                }
                hq4.d(this.f1035a, false, true, null, 5, null);
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ut5 ut5) {
            pq7.c(ut5, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = f17.C.a();
            local.d(a2, "syncDevice success - serial=" + this.b);
            int i = this.c;
            if (i == 0) {
                f17 f17 = this.f1035a;
                Object e = f17.j.e();
                if (e != null) {
                    pq7.b(e, "mSerialLiveData.value!!");
                    f17.I((String) e, 1);
                    return;
                }
                pq7.i();
                throw null;
            } else if (i == 1) {
                f17 f172 = this.f1035a;
                Object e2 = f172.j.e();
                if (e2 != null) {
                    f172.H((String) e2);
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements iq4.e<ot5.d, ot5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Device f1036a;
        @DexIgnore
        public /* final */ /* synthetic */ f17 b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public q(Device device, String str, f17 f17, int i) {
            this.f1036a = device;
            this.b = f17;
            this.c = i;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(ot5.c cVar) {
            pq7.c(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = f17.C.a();
            local.d(a2, "updateVibrationLevel fail!! errorValue=" + cVar.a());
            hq4.d(this.b, false, true, null, 5, null);
            int c2 = cVar.c();
            if (c2 != 1101) {
                if (c2 == 8888) {
                    c.q(this.b.m, false, null, null, null, null, false, null, null, null, null, null, true, false, 6143, null);
                    this.b.J();
                    return;
                } else if (!(c2 == 1112 || c2 == 1113)) {
                    c.q(this.b.m, false, null, null, null, null, true, null, null, null, null, null, false, false, 8159, null);
                    this.b.J();
                    return;
                }
            }
            List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(cVar.b());
            pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            f17 f17 = this.b;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
            if (array != null) {
                uh5[] uh5Arr = (uh5[]) array;
                f17.e((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                this.b.J();
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ot5.d dVar) {
            Device a2;
            pq7.c(dVar, "responseValue");
            Integer vibrationStrength = this.f1036a.getVibrationStrength();
            hq4.d(this.b, false, true, null, 5, null);
            c.q(this.b.m, false, null, null, Integer.valueOf(this.c), null, false, null, null, null, null, null, false, false, 8183, null);
            this.b.J();
            d dVar2 = this.b.l;
            if (!(dVar2 == null || (a2 = dVar2.a()) == null)) {
                a2.setVibrationStrength(vibrationStrength);
            }
            FLogger.INSTANCE.getLocal().d(f17.C.a(), "updateVibrationLevel success");
        }
    }

    /*
    static {
        String simpleName = f17.class.getSimpleName();
        pq7.b(simpleName, "WatchSettingViewModel::class.java.simpleName");
        B = simpleName;
    }
    */

    @DexIgnore
    public f17(DeviceRepository deviceRepository, ot5 ot5, vt5 vt5, mj5 mj5, on5 on5, qt5 qt5, ht5 ht5, tt4 tt4, PortfolioApp portfolioApp) {
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(ot5, "mSetVibrationStrengthUseCase");
        pq7.c(vt5, "mUnlinkDeviceUseCase");
        pq7.c(mj5, "mDeviceSettingFactory");
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(qt5, "mSwitchActiveDeviceUseCase");
        pq7.c(ht5, "mReconnectDeviceUseCase");
        pq7.c(tt4, "challengeRepository");
        pq7.c(portfolioApp, "mApp");
        this.s = deviceRepository;
        this.t = ot5;
        this.u = vt5;
        this.v = mj5;
        this.w = on5;
        this.x = qt5;
        this.y = ht5;
        this.z = tt4;
        this.A = portfolioApp;
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        this.j = mutableLiveData;
        LiveData<Device> c2 = ss0.c(mutableLiveData, new j(this));
        pq7.b(c2, "Transformations.switchMa\u2026lAsLiveData(serial)\n    }");
        this.k = c2;
        this.m = new c(false, null, null, null, null, false, null, null, null, null, null, false, false, 8191, null);
        this.o = new k(this);
        this.p = new l(this);
        this.q = new MutableLiveData<>();
        this.r = new i(this);
    }

    @DexIgnore
    public final void F() {
        FLogger.INSTANCE.getLocal().d(B, "checkToReconnectOrSwitchActiveDevice");
        String J = this.A.J();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = B;
        local.d(str, "activeSerial=" + J + ", mSerialLiveData=" + this.j.e());
        if (pq7.a(this.j.e(), J)) {
            Y();
        } else if (!o37.b(PortfolioApp.h0.c())) {
            c.q(this.m, false, null, null, null, new cl7(601, ""), false, null, null, null, null, null, false, false, 8175, null);
            J();
        } else {
            hq4.d(this, true, false, null, 6, null);
            xw7 unused = gu7.d(us0.a(this), null, null, new e(this, J, null), 3, null);
        }
    }

    @DexIgnore
    public final void G(boolean z2) {
        xw7 unused = gu7.d(us0.a(this), null, null, new f(this, z2, null), 3, null);
    }

    @DexIgnore
    public final void H(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = B;
        local.d(str2, "removeDevice " + str);
        this.u.e(str != null ? new vt5.b(str) : null, new g(this, str));
    }

    @DexIgnore
    public final void I(String str, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = B;
        local.d(str2, "switch to device " + str + " mode " + i2);
        this.x.e(new qt5.b(str, i2), new h(this, str));
    }

    @DexIgnore
    public final void J() {
        this.m.n(M());
        this.m.o(N());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = B;
        local.d(str, ".emitUIState(), mUiModelWrapper=" + this.m);
        this.h.l(this.m);
    }

    @DexIgnore
    public final LiveData<a> K() {
        return this.q;
    }

    @DexIgnore
    public final String L() {
        String b2;
        d dVar = this.l;
        return (dVar == null || (b2 = dVar.b()) == null) ? "" : b2;
    }

    @DexIgnore
    public final d M() {
        return this.l;
    }

    @DexIgnore
    public final String N() {
        return this.n;
    }

    @DexIgnore
    public final MutableLiveData<c> O() {
        return this.h;
    }

    @DexIgnore
    public final String P() {
        return this.j.e();
    }

    @DexIgnore
    public final void Q(Device device) {
        if (device != null) {
            xw7 unused = gu7.d(us0.a(this), null, null, new m(device, null, this, device), 3, null);
        }
    }

    @DexIgnore
    public final void R(String str) {
        pq7.c(str, "serial");
        hq4.d(this, true, false, null, 6, null);
        H(str);
    }

    @DexIgnore
    public final void S(String str) {
        pq7.c(str, "serial");
        hq4.d(this, true, false, null, 6, null);
        this.A.x(str, true);
    }

    @DexIgnore
    public final void T(String str) {
        pq7.c(str, "serial");
        hq4.d(this, true, false, null, 6, null);
        this.A.x(str, true);
    }

    @DexIgnore
    public final void U(String str) {
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = B;
        local.d(str2, "force switch to device " + str);
        hq4.d(this, true, false, null, 6, null);
        this.x.e(new qt5.b(str, 4), new n(this, str));
    }

    @DexIgnore
    public final void V(String str) {
        pq7.c(str, "serial");
        hq4.d(this, true, false, null, 6, null);
        I(str, 2);
    }

    @DexIgnore
    public final void W(String str) {
        pq7.c(str, "serial");
        hq4.d(this, true, false, null, 6, null);
        this.A.x(str, false);
    }

    @DexIgnore
    public final void X(String str) {
        pq7.c(str, "serial");
        hq4.d(this, true, false, null, 6, null);
        this.A.x(str, false);
    }

    @DexIgnore
    public final void Y() {
        FLogger.INSTANCE.getLocal().d(B, "reconnectActiveDevice");
        hq4.d(this, true, false, null, 6, null);
        ht5 ht5 = this.y;
        String e2 = this.j.e();
        if (e2 != null) {
            pq7.b(e2, "mSerialLiveData.value!!");
            ht5.e(new ht5.c(e2), new o(this));
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void Z() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = B;
        local.d(str, "removeDevice serial=" + this.j + ".value");
        hq4.d(this, true, false, null, 6, null);
        String J = this.A.J();
        if (!pq7.a(this.j.e(), J) || vt7.l(J)) {
            H(this.j.e());
        } else {
            d0(J, 1);
        }
    }

    @DexIgnore
    public final void a0(String str) {
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = B;
        local.d(str2, "setWatchSerial, serial=" + str);
        this.j.o(str);
    }

    @DexIgnore
    public final void b0() {
        c.q(this.m, false, null, null, null, null, false, null, null, null, null, null, false, false, 8191, null);
        this.t.s();
        this.y.o();
        this.x.y();
        wq5.d.g(CommunicateMode.FORCE_CONNECT, CommunicateMode.SET_VIBRATION_STRENGTH, CommunicateMode.SWITCH_DEVICE);
        PortfolioApp portfolioApp = this.A;
        i iVar = this.r;
        portfolioApp.registerReceiver(iVar, new IntentFilter(this.A.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        LiveData<Device> liveData = this.k;
        if (liveData != null) {
            liveData.i(this.p);
        }
    }

    @DexIgnore
    public final void c0() {
        try {
            this.x.C();
            LiveData<Device> liveData = this.k;
            if (liveData != null) {
                liveData.m(this.p);
            }
            this.y.r();
            this.t.v();
            this.A.unregisterReceiver(this.r);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = B;
            local.e(str, "stop with " + e2);
        }
        this.i.removeCallbacksAndMessages(null);
    }

    @DexIgnore
    public final void d0(String str, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = B;
        local.d(str2, "syncDevice - serial=" + str + ", userAction=" + i2);
        this.v.b(str).e(new tt5(!nk5.o.w(FossilDeviceSerialPatternUtil.getDeviceBySerial(str)) ? 10 : 15, str, false), new p(this, str, i2));
    }

    @DexIgnore
    public final void e0() {
        String relativeTimeSpanString;
        String e2 = this.j.e();
        if (e2 != null) {
            if (TextUtils.equals(PortfolioApp.h0.c().J(), e2)) {
                PortfolioApp portfolioApp = this.A;
                pq7.b(e2, "it");
                if (portfolioApp.A0(e2)) {
                    relativeTimeSpanString = this.A.getString(2131886784);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = B;
                    local.d(str, "updateSyncTime " + relativeTimeSpanString);
                    c.q(this.m, false, null, relativeTimeSpanString.toString(), null, null, false, null, null, null, null, null, false, false, 8187, null);
                    this.n = relativeTimeSpanString.toString();
                    J();
                    this.i.postDelayed(this.o, 60000);
                }
            }
            long C2 = this.w.C(e2);
            if (((int) C2) == 0) {
                relativeTimeSpanString = "";
            } else if (System.currentTimeMillis() - C2 < 60000) {
                hr7 hr7 = hr7.f1520a;
                String c2 = um5.c(PortfolioApp.h0.c(), 2131887191);
                pq7.b(c2, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                String format = String.format(c2, Arrays.copyOf(new Object[]{1}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                relativeTimeSpanString = format;
            } else {
                relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(C2, System.currentTimeMillis(), TimeUnit.SECONDS.toMillis(1));
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = B;
            local2.d(str2, "updateSyncTime " + relativeTimeSpanString);
            c.q(this.m, false, null, relativeTimeSpanString.toString(), null, null, false, null, null, null, null, null, false, false, 8187, null);
            this.n = relativeTimeSpanString.toString();
            J();
            this.i.postDelayed(this.o, 60000);
        }
    }

    @DexIgnore
    public final void f0(int i2) {
        d dVar;
        Device a2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = B;
        local.d(str, "updateVibrationLevel " + i2 + " of " + this.j.e());
        String e2 = this.j.e();
        if (e2 != null && (dVar = this.l) != null && (a2 = dVar.a()) != null) {
            Integer vibrationStrength = a2.getVibrationStrength();
            if ((vibrationStrength == null || vibrationStrength.intValue() != i2) && !FossilDeviceSerialPatternUtil.isSamSlimDevice(e2) && !FossilDeviceSerialPatternUtil.isDianaDevice(e2)) {
                hq4.d(this, true, false, null, 6, null);
                ot5 ot5 = this.t;
                pq7.b(e2, "it");
                ot5.e(new ot5.b(e2, i2), new q(a2, e2, this, i2));
            }
        }
    }

    @DexIgnore
    public final boolean g0() {
        String e2 = this.j.e();
        if (e2 != null) {
            return !FossilDeviceSerialPatternUtil.isSamSlimDevice(e2) && !FossilDeviceSerialPatternUtil.isDianaDevice(e2);
        }
        return true;
    }
}
