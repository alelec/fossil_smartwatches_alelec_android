package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import androidx.appcompat.view.menu.ListMenuItemView;
import com.fossil.jg0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bg0 extends BaseAdapter {
    @DexIgnore
    public cg0 b;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ LayoutInflater f;
    @DexIgnore
    public /* final */ int g;

    @DexIgnore
    public bg0(cg0 cg0, LayoutInflater layoutInflater, boolean z, int i) {
        this.e = z;
        this.f = layoutInflater;
        this.b = cg0;
        this.g = i;
        a();
    }

    @DexIgnore
    public void a() {
        eg0 x = this.b.x();
        if (x != null) {
            ArrayList<eg0> B = this.b.B();
            int size = B.size();
            for (int i = 0; i < size; i++) {
                if (B.get(i) == x) {
                    this.c = i;
                    return;
                }
            }
        }
        this.c = -1;
    }

    @DexIgnore
    public cg0 b() {
        return this.b;
    }

    @DexIgnore
    /* renamed from: c */
    public eg0 getItem(int i) {
        ArrayList<eg0> B = this.e ? this.b.B() : this.b.G();
        int i2 = this.c;
        if (i2 >= 0 && i >= i2) {
            i++;
        }
        return B.get(i);
    }

    @DexIgnore
    public void d(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public int getCount() {
        ArrayList<eg0> B = this.e ? this.b.B() : this.b.G();
        return this.c < 0 ? B.size() : B.size() - 1;
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.f.inflate(this.g, viewGroup, false) : view;
        int groupId = getItem(i).getGroupId();
        int i2 = i - 1;
        int groupId2 = i2 >= 0 ? getItem(i2).getGroupId() : groupId;
        ListMenuItemView listMenuItemView = (ListMenuItemView) inflate;
        listMenuItemView.setGroupDividerEnabled(this.b.H() && groupId != groupId2);
        jg0.a aVar = (jg0.a) inflate;
        if (this.d) {
            listMenuItemView.setForceShowIcon(true);
        }
        aVar.f(getItem(i), 0);
        return inflate;
    }

    @DexIgnore
    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }
}
