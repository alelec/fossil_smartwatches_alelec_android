package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jg7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f1758a; // = null;
    @DexIgnore
    public String b; // = null;
    @DexIgnore
    public String c; // = null;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    public String a() {
        return this.f1758a;
    }

    @DexIgnore
    public String b() {
        return this.b;
    }

    @DexIgnore
    public String c() {
        return this.c;
    }

    @DexIgnore
    public boolean d() {
        return this.e;
    }

    @DexIgnore
    public boolean e() {
        return this.d;
    }

    @DexIgnore
    public void f(String str) {
        this.f1758a = str;
    }

    @DexIgnore
    public String toString() {
        return "StatSpecifyReportedInfo [appKey=" + this.f1758a + ", installChannel=" + this.b + ", version=" + this.c + ", sendImmediately=" + this.d + ", isImportant=" + this.e + "]";
    }
}
