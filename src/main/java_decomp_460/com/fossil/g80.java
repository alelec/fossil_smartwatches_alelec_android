package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import com.fossil.fitness.ActiveMinute;
import com.fossil.fitness.Calorie;
import com.fossil.fitness.Distance;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.GoalTracking;
import com.fossil.fitness.HeartRate;
import com.fossil.fitness.Resting;
import com.fossil.fitness.SleepSession;
import com.fossil.fitness.Step;
import com.fossil.fitness.WorkoutSession;
import com.fossil.fitness.WorkoutState;
import com.fossil.fitness.WorkoutType;
import com.fossil.imagefilters.Format;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class g80 {
    @DexIgnore
    public static final byte a(Set<? extends un1> set) {
        byte b = (byte) 0;
        Iterator<T> it = set.iterator();
        while (it.hasNext()) {
            b = (byte) (it.next().b() | b);
        }
        return b;
    }

    @DexIgnore
    public static final SharedPreferences b(ld0 ld0) {
        String name = ld0.name();
        Context a2 = id0.i.a();
        if (a2 != null) {
            return a2.getSharedPreferences(name, 0);
        }
        return null;
    }

    @DexIgnore
    public static final WorkoutState c(String str) {
        if (vt7.j(str, "started", true)) {
            return WorkoutState.START;
        }
        if (vt7.j(str, "paused", true)) {
            return WorkoutState.PAUSE;
        }
        if (vt7.j(str, "resumed", true)) {
            return WorkoutState.RESUME;
        }
        if (vt7.j(str, "end", true)) {
            return WorkoutState.END;
        }
        return null;
    }

    @DexIgnore
    public static final WorkoutType d(int i) {
        WorkoutType workoutType;
        WorkoutType[] values = WorkoutType.values();
        int length = values.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                workoutType = null;
                break;
            }
            workoutType = values[i2];
            if (workoutType.getValue() == i) {
                break;
            }
            i2++;
        }
        return workoutType != null ? workoutType : WorkoutType.UNKNOWN;
    }

    @DexIgnore
    public static /* synthetic */ String e(int i, int i2) {
        if ((i2 & 1) != 0) {
            i = 16;
        }
        String a2 = e.a("UUID.randomUUID().toString()");
        String substring = a2.substring(0, Math.min(i, a2.length()));
        pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final String f(Format format) {
        int i = h80.f1445a[format.ordinal()];
        if (i == 1) {
            return OrmLiteConfigUtil.RAW_DIR_NAME;
        }
        if (i == 2) {
            return "rle";
        }
        throw new al7();
    }

    @DexIgnore
    public static final JSONArray g(n6[] n6VarArr) {
        JSONArray jSONArray = new JSONArray();
        for (n6 n6Var : n6VarArr) {
            jSONArray.put(n6Var.b);
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONArray h(zm1[] zm1Arr) {
        JSONArray jSONArray = new JSONArray();
        for (zm1 zm1 : zm1Arr) {
            jSONArray.put(zm1.b());
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONArray i(FitnessData[] fitnessDataArr) {
        ArrayList<Short> values;
        JSONArray jSONArray = new JSONArray();
        for (FitnessData fitnessData : fitnessDataArr) {
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray2 = new JSONArray();
            ArrayList<WorkoutSession> workouts = fitnessData.getWorkouts();
            if (workouts != null) {
                for (T t : workouts) {
                    JSONObject jSONObject2 = new JSONObject();
                    jd0 jd0 = jd0.I4;
                    pq7.b(t, "item");
                    k(jSONObject2, jd0, Long.valueOf(t.getId()));
                    k(jSONObject2, jd0.X, Integer.valueOf(t.getStartTime()));
                    k(jSONObject2, jd0.Y, Integer.valueOf(t.getEndTime()));
                    k(jSONObject2, jd0.S5, Integer.valueOf(t.getGpsDataPoints().size()));
                    jSONArray2.put(jSONObject2);
                }
            }
            JSONArray jSONArray3 = new JSONArray();
            ArrayList<SleepSession> sleeps = fitnessData.getSleeps();
            if (sleeps != null) {
                for (T t2 : sleeps) {
                    JSONObject jSONObject3 = new JSONObject();
                    jd0 jd02 = jd0.X;
                    pq7.b(t2, "item");
                    k(jSONObject3, jd02, Integer.valueOf(t2.getStartTime()));
                    k(jSONObject3, jd0.Y, Integer.valueOf(t2.getEndTime()));
                    jSONArray3.put(jSONObject3);
                }
            }
            k(jSONObject, jd0.f0, jSONArray2);
            k(jSONObject, jd0.e0, jSONArray3);
            k(jSONObject, jd0.Z, Integer.valueOf(fitnessData.getTimezoneOffsetInSecond()));
            jd0 jd03 = jd0.d0;
            ActiveMinute activeMinute = fitnessData.getActiveMinute();
            k(jSONObject, jd03, Integer.valueOf((activeMinute != null ? Integer.valueOf(activeMinute.getTotal()) : null).intValue()));
            jd0 jd04 = jd0.a0;
            Distance distance = fitnessData.getDistance();
            k(jSONObject, jd04, Double.valueOf((distance != null ? Double.valueOf(distance.getTotal()) : null).doubleValue()));
            k(jSONObject, jd0.Y, Integer.valueOf(fitnessData.getEndTime()));
            k(jSONObject, jd0.X, Integer.valueOf(fitnessData.getStartTime()));
            jd0 jd05 = jd0.c0;
            Step step = fitnessData.getStep();
            k(jSONObject, jd05, Integer.valueOf((step != null ? Integer.valueOf(step.getTotal()) : null).intValue()));
            jd0 jd06 = jd0.b0;
            Calorie calorie = fitnessData.getCalorie();
            k(jSONObject, jd06, Integer.valueOf((calorie != null ? Integer.valueOf(calorie.getTotal()) : null).intValue()));
            jd0 jd07 = jd0.g0;
            HeartRate heartrate = fitnessData.getHeartrate();
            k(jSONObject, jd07, (heartrate == null || (values = heartrate.getValues()) == null) ? JSONObject.NULL : Integer.valueOf(values.size()));
            jd0 jd08 = jd0.h0;
            ArrayList<Resting> resting = fitnessData.getResting();
            k(jSONObject, jd08, Integer.valueOf((resting != null ? Integer.valueOf(resting.size()) : null).intValue()));
            jd0 jd09 = jd0.i0;
            ArrayList<GoalTracking> goals = fitnessData.getGoals();
            k(jSONObject, jd09, Integer.valueOf((goals != null ? Integer.valueOf(goals.size()) : null).intValue()));
            jSONArray.put(jSONObject);
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONArray j(UUID[] uuidArr) {
        JSONArray jSONArray = new JSONArray();
        for (UUID uuid : uuidArr) {
            jSONArray.put(uuid.toString());
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONObject k(JSONObject jSONObject, jd0 jd0, Object obj) {
        JSONObject put = jSONObject.put(ey1.a(jd0), obj);
        pq7.b(put, "this.put(key.lowerCaseName, value)");
        return put;
    }
}
