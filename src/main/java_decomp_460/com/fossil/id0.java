package com.fossil;

import android.content.Context;
import android.os.Build;
import com.facebook.internal.AnalyticsEvents;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class id0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static WeakReference<Context> f1609a;
    @DexIgnore
    public static String b; // = new String();
    @DexIgnore
    public static zw1 c; // = new zw1("", "", "");
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static Object f; // = new Object();
    @DexIgnore
    public static /* final */ iv7 g;
    @DexIgnore
    public static rp7<? super v18, ? extends HashMap<String, String>> h;
    @DexIgnore
    public static /* final */ id0 i; // = new id0();

    /*
    static {
        String str = Build.VERSION.RELEASE;
        if (str == null) {
            str = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        d = str;
        String str2 = Build.MODEL;
        if (str2 == null) {
            str2 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        e = str2;
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
        pq7.b(newSingleThreadExecutor, "Executors.newSingleThreadExecutor()");
        g = jv7.a(pw7.b(newSingleThreadExecutor));
    }
    */

    @DexIgnore
    public final Context a() {
        WeakReference<Context> weakReference = f1609a;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    @DexIgnore
    public final void b(Context context) {
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null) {
            f1609a = new WeakReference<>(applicationContext);
        }
    }

    @DexIgnore
    public final void c(Object obj) {
        f = obj;
    }

    @DexIgnore
    public final void d(String str) {
        b = str;
    }

    @DexIgnore
    public final iv7 e() {
        return g;
    }

    @DexIgnore
    public final void f(zw1 zw1) {
        c = zw1;
        d90 d90 = d90.i;
        d90.d.c(new zw1(c.c() + "/sdk_log", c.a(), c.b()));
        y80 y80 = y80.i;
        y80.d.c(new zw1(c.c() + "/raw_minute_data", c.a(), c.b()));
        x80 x80 = x80.i;
        x80.d.c(new zw1(c.c() + "/raw_hardware_log", c.a(), c.b()));
        w80 w80 = w80.i;
        w80.d.c(new zw1(c.c() + "/gps_data", c.a(), c.b()));
    }

    @DexIgnore
    public final String g() {
        return d;
    }

    @DexIgnore
    public final String h() {
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String i() {
        /*
            r5 = this;
            r2 = 0
            int r3 = android.os.Process.myPid()
            android.content.Context r0 = r5.a()
            if (r0 == 0) goto L_0x0069
            java.lang.String r1 = "activity"
            java.lang.Object r0 = r0.getSystemService(r1)
        L_0x0011:
            boolean r1 = r0 instanceof android.app.ActivityManager
            if (r1 != 0) goto L_0x0016
            r0 = r2
        L_0x0016:
            android.app.ActivityManager r0 = (android.app.ActivityManager) r0
            if (r0 == 0) goto L_0x006f
            java.util.List r0 = r0.getRunningAppProcesses()
            if (r0 == 0) goto L_0x006f
            java.util.Iterator r4 = r0.iterator()
        L_0x0024:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x006d
            java.lang.Object r1 = r4.next()
            r0 = r1
            android.app.ActivityManager$RunningAppProcessInfo r0 = (android.app.ActivityManager.RunningAppProcessInfo) r0
            int r0 = r0.pid
            if (r0 != r3) goto L_0x006b
            r0 = 1
        L_0x0036:
            if (r0 == 0) goto L_0x0024
            r0 = r1
        L_0x0039:
            android.app.ActivityManager$RunningAppProcessInfo r0 = (android.app.ActivityManager.RunningAppProcessInfo) r0
            if (r0 == 0) goto L_0x006f
            java.lang.String r0 = r0.processName
        L_0x003f:
            if (r0 == 0) goto L_0x0068
            java.lang.String r1 = "SHA-1"
            java.security.MessageDigest r1 = java.security.MessageDigest.getInstance(r1)
            java.lang.String r2 = "MessageDigest.getInstance(\"SHA-1\")"
            com.fossil.pq7.b(r1, r2)
            com.fossil.hd0 r2 = com.fossil.hd0.y
            java.nio.charset.Charset r2 = r2.c()
            byte[] r0 = r0.getBytes(r2)
            java.lang.String r2 = "(this as java.lang.String).getBytes(charset)"
            com.fossil.pq7.b(r0, r2)
            r1.update(r0)
            byte[] r0 = r1.digest()
            r1 = 8
            java.lang.String r2 = android.util.Base64.encodeToString(r0, r1)
        L_0x0068:
            return r2
        L_0x0069:
            r0 = r2
            goto L_0x0011
        L_0x006b:
            r0 = 0
            goto L_0x0036
        L_0x006d:
            r0 = r2
            goto L_0x0039
        L_0x006f:
            r0 = r2
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.id0.i():java.lang.String");
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: com.fossil.rp7<? super com.fossil.v18, ? extends java.util.HashMap<java.lang.String, java.lang.String>>, com.fossil.rp7<com.fossil.v18, java.util.HashMap<java.lang.String, java.lang.String>> */
    public final rp7<v18, HashMap<String, String>> j() {
        return h;
    }

    @DexIgnore
    public final int k() {
        return (TimeZone.getDefault().getOffset(System.currentTimeMillis()) / 1000) / 60;
    }

    @DexIgnore
    public final String l() {
        return b;
    }

    @DexIgnore
    public final Object m() {
        return f;
    }
}
