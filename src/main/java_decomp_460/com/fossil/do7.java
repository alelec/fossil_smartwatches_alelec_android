package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface do7 {
    @DexIgnore
    do7 getCallerFrame();

    @DexIgnore
    StackTraceElement getStackTraceElement();
}
