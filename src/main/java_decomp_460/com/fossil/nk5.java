package com.fossil;

import android.os.Build;
import android.text.TextUtils;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nk5 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static float[] e;
    @DexIgnore
    public static /* final */ String[] f; // = {"HW.0.0", "HL.0.0", "HM.0.0", "DN.0.0", "DN.1.0"};
    @DexIgnore
    public static nk5 g;
    @DexIgnore
    public static /* final */ MFDeviceFamily[] h; // = {MFDeviceFamily.DEVICE_FAMILY_SAM, MFDeviceFamily.DEVICE_FAMILY_SAM_SLIM, MFDeviceFamily.DEVICE_FAMILY_SAM_MINI, MFDeviceFamily.DEVICE_FAMILY_RMM};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] i; // = {FossilDeviceSerialPatternUtil.DEVICE.SAM, FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION, FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI, FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM, FossilDeviceSerialPatternUtil.DEVICE.DIANA, FossilDeviceSerialPatternUtil.DEVICE.IVY};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] j; // = {FossilDeviceSerialPatternUtil.DEVICE.SAM, FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI, FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION, FossilDeviceSerialPatternUtil.DEVICE.DIANA, FossilDeviceSerialPatternUtil.DEVICE.IVY};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] k; // = {FossilDeviceSerialPatternUtil.DEVICE.RMM, FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] l; // = {FossilDeviceSerialPatternUtil.DEVICE.DIANA, FossilDeviceSerialPatternUtil.DEVICE.IVY};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] m; // = {FossilDeviceSerialPatternUtil.DEVICE.SAM, FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM, FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] n; // = {FossilDeviceSerialPatternUtil.DEVICE.SAM, FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM, FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI, FossilDeviceSerialPatternUtil.DEVICE.DIANA, FossilDeviceSerialPatternUtil.DEVICE.IVY};
    @DexIgnore
    public static /* final */ a o; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public on5 f2539a;
    @DexIgnore
    public DeviceRepository b;
    @DexIgnore
    public List<String> c; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final boolean A(String str) {
            pq7.c(str, "serial");
            return t(str, q());
        }

        @DexIgnore
        public final boolean B(String str) {
            pq7.c(str, "serialNumber");
            return vt7.s(str, "D0F102", false, 2, null) || vt7.s(str, "D0F104", false, 2, null) || vt7.s(str, "D0F105", false, 2, null) || vt7.s(str, "V0F002", false, 2, null) || vt7.s(str, "V0F004", false, 2, null) || vt7.s(str, "V0F005", false, 2, null);
        }

        @DexIgnore
        public final void C(nk5 nk5) {
            nk5.g = nk5;
        }

        @DexIgnore
        public final void D(float[] fArr) {
            nk5.e = fArr;
        }

        @DexIgnore
        public final MFDeviceFamily[] a() {
            return nk5.h;
        }

        @DexIgnore
        public final String[] b() {
            return nk5.f;
        }

        @DexIgnore
        public final int c(int i) {
            if (i < 25) {
                return 2131231008;
            }
            if (i < 50) {
                return 2131231010;
            }
            return i < 75 ? 2131231012 : 2131231006;
        }

        @DexIgnore
        public final int d(String str, b bVar) {
            if (!v(str)) {
                return 2131231334;
            }
            switch (mk5.f2397a[bVar.ordinal()]) {
                case 1:
                    return 2131231336;
                case 2:
                    return 2131231335;
                case 3:
                case 4:
                    return 2131231334;
                case 5:
                    return 2131230972;
                case 6:
                    return 2131230973;
                case 7:
                    return 2131230974;
                case 8:
                    return 2131230953;
                case 9:
                    return 2131230954;
                default:
                    throw new al7();
            }
        }

        @DexIgnore
        public final MFDeviceFamily e(String str) {
            pq7.c(str, "serial");
            MFDeviceFamily deviceFamily = DeviceIdentityUtils.getDeviceFamily(str);
            pq7.b(deviceFamily, "DeviceIdentityUtils.getDeviceFamily(serial)");
            return deviceFamily;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] f() {
            return nk5.l;
        }

        @DexIgnore
        public final float g(int i) {
            if (l() == null) {
                r();
            }
            int i2 = (-i) - 30;
            if (i2 >= 0) {
                float[] l = l();
                if (l == null) {
                    pq7.i();
                    throw null;
                } else if (i2 < l.length) {
                    float[] l2 = l();
                    if (l2 != null) {
                        return l2[i2];
                    }
                    pq7.i();
                    throw null;
                }
            }
            if (i2 < 0) {
                return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
            return 16.4f;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] h() {
            return nk5.m;
        }

        @DexIgnore
        public final int i(String str, b bVar) {
            pq7.c(str, "serial");
            pq7.c(bVar, AnalyticsEvents.PARAMETER_LIKE_VIEW_STYLE);
            return d(str, bVar);
        }

        @DexIgnore
        public final nk5 j() {
            nk5 k;
            synchronized (this) {
                if (nk5.o.k() == null) {
                    nk5.o.C(new nk5());
                }
                k = nk5.o.k();
                if (k == null) {
                    pq7.i();
                    throw null;
                }
            }
            return k;
        }

        @DexIgnore
        public final nk5 k() {
            return nk5.g;
        }

        @DexIgnore
        public final float[] l() {
            return nk5.e;
        }

        @DexIgnore
        public final String m(String str) {
            if (!TextUtils.isEmpty(str)) {
                if (str == null) {
                    pq7.i();
                    throw null;
                } else if (str.length() >= 5) {
                    if (FossilDeviceSerialPatternUtil.isQMotion(str)) {
                        String substring = str.substring(0, 5);
                        pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                        return substring;
                    }
                    String substring2 = str.substring(0, 6);
                    pq7.b(substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    return substring2;
                }
            }
            return "";
        }

        @DexIgnore
        public final List<String> n(String str) {
            pq7.c(str, "serial");
            ArrayList arrayList = new ArrayList();
            FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
            if (deviceBySerial != null) {
                int i = mk5.b[deviceBySerial.ordinal()];
                if (i == 1) {
                    arrayList.add("HW.0.0");
                } else if (i == 2) {
                    arrayList.add("HM.0.0");
                } else if (i == 3) {
                    arrayList.add("HL.0.0");
                } else if (i == 4) {
                    arrayList.add("DN.0.0");
                    arrayList.add("DN.1.0");
                } else if (i == 5) {
                    arrayList.add("IV.0.0");
                }
            }
            return arrayList;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] o() {
            return nk5.n;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] p() {
            return nk5.i;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] q() {
            return nk5.k;
        }

        @DexIgnore
        public final void r() {
            D(new float[71]);
            int i = 0;
            while (i <= 20) {
                float[] l = l();
                if (l != null) {
                    l[i] = 0.005f * ((float) i) * 3.28f;
                    i++;
                } else {
                    pq7.i();
                    throw null;
                }
            }
            while (i <= 35) {
                float[] l2 = l();
                if (l2 != null) {
                    l2[i] = ((((float) (i - 20)) * 0.06f) + 0.1f) * 3.28f;
                    i++;
                } else {
                    pq7.i();
                    throw null;
                }
            }
            while (i <= 50) {
                float[] l3 = l();
                if (l3 != null) {
                    l3[i] = ((((float) (i - 35)) * 0.06666667f) + 1.0f) * 3.28f;
                    i++;
                } else {
                    pq7.i();
                    throw null;
                }
            }
            while (i <= 60) {
                float[] l4 = l();
                if (l4 != null) {
                    l4[i] = ((((float) (i - 50)) * 0.1f) + 2.0f) * 3.28f;
                    i++;
                } else {
                    pq7.i();
                    throw null;
                }
            }
            while (i <= 63) {
                float[] l5 = l();
                if (l5 != null) {
                    l5[i] = ((((float) (i - 60)) * 0.33333334f) + 3.0f) * 3.28f;
                    i++;
                } else {
                    pq7.i();
                    throw null;
                }
            }
            while (i <= 70) {
                float[] l6 = l();
                if (l6 != null) {
                    l6[i] = ((((float) (i - 60)) * 0.14285715f) + 4.0f) * 3.28f;
                    i++;
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        public final boolean s() {
            return Build.VERSION.SDK_INT >= 29;
        }

        @DexIgnore
        public final boolean t(String str, FossilDeviceSerialPatternUtil.DEVICE[] deviceArr) {
            pq7.c(str, "serial");
            pq7.c(deviceArr, "supportedDevices");
            FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
            for (FossilDeviceSerialPatternUtil.DEVICE device : deviceArr) {
                if (device == deviceBySerial) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public final boolean u(String str) {
            pq7.c(str, "serial");
            return t(str, p());
        }

        @DexIgnore
        public final boolean v(String str) {
            pq7.c(str, "serial");
            return t(str, o());
        }

        @DexIgnore
        public final boolean w(FossilDeviceSerialPatternUtil.DEVICE device) {
            return device == FossilDeviceSerialPatternUtil.DEVICE.DIANA || device == FossilDeviceSerialPatternUtil.DEVICE.IVY;
        }

        @DexIgnore
        public final boolean x(String str) {
            pq7.c(str, "serial");
            return t(str, f());
        }

        @DexIgnore
        public final boolean y(String str) {
            pq7.c(str, "serial");
            return t(str, h());
        }

        @DexIgnore
        public final boolean z() {
            return Build.VERSION.SDK_INT >= 26;
        }
    }

    @DexIgnore
    public enum b {
        SMALL(0),
        NORMAL(1),
        LARGE(2),
        HYBRID_WATCH_HOUR(3),
        HYBRID_WATCH_MINUTE(4),
        HYBRID_WATCH_SUBEYE(5),
        DIANA_WATCH_HOUR(6),
        DIANA_WATCH_MINUTE(7),
        WATCH_COMPLETED(8);
        
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public b(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    /*
    static {
        String simpleName = nk5.class.getSimpleName();
        pq7.b(simpleName, "DeviceHelper::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public nk5() {
        PortfolioApp.h0.c().M().n(this);
        l();
    }

    @DexIgnore
    public final void l() {
        this.c.clear();
        if (!vt7.j("release", "release", true)) {
            on5 on5 = this.f2539a;
            if (on5 == null) {
                pq7.n("sharedPreferencesManager");
                throw null;
            } else if (on5.x0() || pq7.a(PortfolioApp.h0.c().Q(), ph5.PORTFOLIO.getName())) {
                this.c.add(DeviceIdentityUtils.RAY_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.FLASH_SERIAL_NUMBER_PREFIX);
                List<String> list = this.c;
                String[] strArr = DeviceIdentityUtils.Q_MOTION_PREFIX;
                List asList = Arrays.asList((String[]) Arrays.copyOf(strArr, strArr.length));
                pq7.b(asList, "Arrays.asList(*DeviceIde\u2026ityUtils.Q_MOTION_PREFIX)");
                list.addAll(asList);
                this.c.add(DeviceIdentityUtils.RMM_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.FAKE_SAM_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.SAM_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.SAM_SLIM_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.SAM_DIANA_SERIAL_NUMBER_PREFIX);
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        StringBuilder sb = new StringBuilder();
        sb.append("debug=");
        sb.append(PortfolioApp.h0.e());
        sb.append(", BUILD_TYPE=");
        sb.append("release");
        sb.append(", filterList=");
        Object[] array = this.c.toArray(new String[0]);
        if (array != null) {
            sb.append(Arrays.toString(array));
            local.d(str, sb.toString());
            return;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final MisfitDeviceProfile m(String str) {
        pq7.c(str, "serial");
        IButtonConnectivity b2 = PortfolioApp.h0.b();
        MisfitDeviceProfile misfitDeviceProfile = null;
        if (b2 == null) {
            return null;
        }
        try {
            MisfitDeviceProfile deviceProfile = b2.getDeviceProfile(str);
            if (deviceProfile == null) {
                try {
                    DeviceRepository deviceRepository = this.b;
                    if (deviceRepository != null) {
                        Device deviceBySerial = deviceRepository.getDeviceBySerial(str);
                        if (deviceBySerial != null) {
                            String macAddress = deviceBySerial.getMacAddress();
                            if (macAddress != null) {
                                String productDisplayName = deviceBySerial.getProductDisplayName();
                                if (productDisplayName != null) {
                                    String deviceId = deviceBySerial.getDeviceId();
                                    String sku = deviceBySerial.getSku();
                                    if (sku != null) {
                                        String firmwareRevision = deviceBySerial.getFirmwareRevision();
                                        if (firmwareRevision != null) {
                                            return new MisfitDeviceProfile(macAddress, productDisplayName, deviceId, sku, firmwareRevision, deviceBySerial.getBatteryLevel(), "", 0, 0, (short) deviceBySerial.getMajor(), (short) deviceBySerial.getMinor(), "", null);
                                        }
                                        pq7.i();
                                        throw null;
                                    }
                                    pq7.i();
                                    throw null;
                                }
                                pq7.i();
                                throw null;
                            }
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.n("mDeviceRepository");
                        throw null;
                    }
                } catch (Exception e2) {
                    e = e2;
                    misfitDeviceProfile = deviceProfile;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = d;
                    local.e(str2, "getDeviceProfileFromSerial exception=" + e);
                    return misfitDeviceProfile;
                }
            }
            return deviceProfile;
        } catch (Exception e3) {
            e = e3;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str22 = d;
            local2.e(str22, "getDeviceProfileFromSerial exception=" + e);
            return misfitDeviceProfile;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0047, code lost:
        if (r0.x0() == false) goto L_0x0049;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean n(java.lang.String r11, java.util.List<com.portfolio.platform.data.model.SKUModel> r12) {
        /*
            r10 = this;
            r2 = 0
            r3 = 1
            r4 = 0
            java.lang.String r0 = "serial"
            com.fossil.pq7.c(r11, r0)
            java.lang.String r0 = "allSkuModel"
            com.fossil.pq7.c(r12, r0)
            com.fossil.nk5$a r0 = com.fossil.nk5.o
            boolean r0 = r0.v(r11)
            if (r0 != 0) goto L_0x0037
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.nk5.d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "device "
            r2.append(r3)
            r2.append(r11)
            java.lang.String r3 = " is not supported"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
        L_0x0036:
            return r4
        L_0x0037:
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x0049
            com.fossil.on5 r0 = r10.f2539a
            if (r0 == 0) goto L_0x0061
            boolean r0 = r0.x0()
            if (r0 != 0) goto L_0x005f
        L_0x0049:
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            java.lang.String r0 = r0.Q()
            com.fossil.ph5 r1 = com.fossil.ph5.PORTFOLIO
            java.lang.String r1 = r1.getName()
            boolean r0 = com.fossil.pq7.a(r0, r1)
            if (r0 == 0) goto L_0x0067
        L_0x005f:
            r4 = r3
            goto L_0x0036
        L_0x0061:
            java.lang.String r0 = "sharedPreferencesManager"
            com.fossil.pq7.n(r0)
            throw r2
        L_0x0067:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.nk5.d
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "prefix "
            r5.append(r6)
            com.fossil.nk5$a r6 = com.fossil.nk5.o
            java.lang.String r6 = r6.m(r11)
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r0.d(r1, r5)
            java.util.Iterator r5 = r12.iterator()
        L_0x008d:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x00d9
            java.lang.Object r1 = r5.next()
            r0 = r1
            com.portfolio.platform.data.model.SKUModel r0 = (com.portfolio.platform.data.model.SKUModel) r0
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = com.fossil.nk5.d
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "compare sku "
            r8.append(r9)
            r8.append(r0)
            java.lang.String r9 = " to "
            r8.append(r9)
            r8.append(r11)
            java.lang.String r8 = r8.toString()
            r6.d(r7, r8)
            com.fossil.nk5$a r6 = com.fossil.nk5.o
            java.lang.String r6 = r6.m(r11)
            java.lang.String r0 = r0.getSerialNumberPrefix()
            boolean r0 = com.fossil.pq7.a(r6, r0)
            if (r0 == 0) goto L_0x008d
            r0 = r1
        L_0x00cf:
            com.portfolio.platform.data.model.SKUModel r0 = (com.portfolio.platform.data.model.SKUModel) r0
            if (r0 == 0) goto L_0x00d7
            r0 = r3
        L_0x00d4:
            r4 = r0
            goto L_0x0036
        L_0x00d7:
            r0 = r4
            goto L_0x00d4
        L_0x00d9:
            r0 = r2
            goto L_0x00cf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nk5.n(java.lang.String, java.util.List):boolean");
    }

    @DexIgnore
    public final boolean o(String str) {
        pq7.c(str, "serial");
        return o.t(str, j);
    }
}
