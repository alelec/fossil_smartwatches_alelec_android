package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class w02 {
    @DexIgnore
    public static w02 a(Context context, t32 t32, t32 t322, String str) {
        return new r02(context, t32, t322, str);
    }

    @DexIgnore
    public abstract Context b();

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public abstract t32 d();

    @DexIgnore
    public abstract t32 e();
}
