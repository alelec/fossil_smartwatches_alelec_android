package com.fossil;

import android.net.Uri;
import com.fossil.n61;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r61 implements n61<String, Uri> {
    @DexIgnore
    /* renamed from: c */
    public boolean a(String str) {
        pq7.c(str, "data");
        return n61.a.a(this, str);
    }

    @DexIgnore
    /* renamed from: d */
    public Uri b(String str) {
        pq7.c(str, "data");
        Uri parse = Uri.parse(str);
        pq7.b(parse, "Uri.parse(this)");
        return parse;
    }
}
