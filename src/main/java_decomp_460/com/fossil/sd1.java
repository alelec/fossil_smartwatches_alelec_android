package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sd1 implements rd1 {
    @DexIgnore
    @Override // com.fossil.rd1
    public void a(int i) {
    }

    @DexIgnore
    @Override // com.fossil.rd1
    public void b(Bitmap bitmap) {
        bitmap.recycle();
    }

    @DexIgnore
    @Override // com.fossil.rd1
    public Bitmap c(int i, int i2, Bitmap.Config config) {
        return Bitmap.createBitmap(i, i2, config);
    }

    @DexIgnore
    @Override // com.fossil.rd1
    public void d() {
    }

    @DexIgnore
    @Override // com.fossil.rd1
    public Bitmap e(int i, int i2, Bitmap.Config config) {
        return c(i, i2, config);
    }
}
