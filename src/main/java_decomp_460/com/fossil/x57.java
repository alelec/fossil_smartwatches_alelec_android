package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x57 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ boolean f4042a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public x57(int i, int i2, int i3, int i4) {
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.f4042a = i3 == i4;
    }

    @DexIgnore
    public final int a() {
        return this.e;
    }

    @DexIgnore
    public final int b() {
        return this.d;
    }

    @DexIgnore
    public final int c() {
        return this.b;
    }

    @DexIgnore
    public final int d() {
        return this.c;
    }

    @DexIgnore
    public final boolean e() {
        return this.f4042a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof x57) {
                x57 x57 = (x57) obj;
                if (!(this.b == x57.b && this.c == x57.c && this.d == x57.d && this.e == x57.e)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.b * 31) + this.c) * 31) + this.d) * 31) + this.e;
    }

    @DexIgnore
    public String toString() {
        return "DashBarInformation(length=" + this.b + ", progress=" + this.c + ", animStart=" + this.d + ", animEnd=" + this.e + ")";
    }
}
