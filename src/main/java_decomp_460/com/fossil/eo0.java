package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface eo0 extends go0 {
    @DexIgnore
    void k(View view, int i, int i2, int i3, int i4, int i5);

    @DexIgnore
    boolean l(View view, View view2, int i, int i2);

    @DexIgnore
    void m(View view, View view2, int i, int i2);

    @DexIgnore
    void n(View view, int i);

    @DexIgnore
    void o(View view, int i, int i2, int[] iArr, int i3);
}
