package com.fossil;

import com.portfolio.platform.PortfolioApp;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kl5 {
    /*
    static {
        TimeUnit.DAYS.toMillis(1);
    }
    */

    @DexIgnore
    public static String a(int i) {
        int i2 = i / 3600;
        int i3 = (i % 3600) / 60;
        if (i2 > 0 && i3 > 0) {
            return String.format(um5.c(PortfolioApp.d0, 2131886204), String.valueOf(i2), String.valueOf(i3));
        } else if (i2 <= 0 || i3 != 0) {
            return String.format(um5.c(PortfolioApp.d0, 2131886205), String.valueOf(i3));
        } else {
            return String.format(um5.c(PortfolioApp.d0, 2131886203), String.valueOf(i2));
        }
    }

    @DexIgnore
    public static Date b(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.set(11, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        return instance.getTime();
    }

    @DexIgnore
    public static String c(int i) {
        int i2 = i == 0 ? 12 : i % 12;
        String str = i >= 12 ? "p" : "a";
        return i2 + str;
    }

    @DexIgnore
    public static int d() {
        return TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()) / 1000;
    }
}
