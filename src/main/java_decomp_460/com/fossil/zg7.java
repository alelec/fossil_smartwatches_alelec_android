package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zg7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;
    @DexIgnore
    public /* final */ /* synthetic */ jg7 c;

    @DexIgnore
    public zg7(Context context, jg7 jg7) {
        this.b = context;
        this.c = jg7;
    }

    @DexIgnore
    public final void run() {
        try {
            ig7.a(this.b, false, this.c);
        } catch (Throwable th) {
            ig7.m.e(th);
        }
    }
}
