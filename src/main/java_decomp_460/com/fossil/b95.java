package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class b95 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleEditText r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ RTLImageView u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ AlphabetFastScrollRecyclerView w;

    @DexIgnore
    public b95(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, ImageView imageView, RTLImageView rTLImageView, ConstraintLayout constraintLayout, AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleEditText;
        this.s = flexibleTextView;
        this.t = imageView;
        this.u = rTLImageView;
        this.v = constraintLayout;
        this.w = alphabetFastScrollRecyclerView;
    }
}
