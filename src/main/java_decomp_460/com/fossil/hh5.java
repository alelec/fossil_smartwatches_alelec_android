package com.fossil;

import android.net.Uri;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hh5 extends hq4 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public /* final */ yk7 h; // = zk7.a(d.INSTANCE);
    @DexIgnore
    public /* final */ yk7 i; // = zk7.a(c.INSTANCE);
    @DexIgnore
    public /* final */ UserRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f1480a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public a() {
            this(false, false, false, false, 15, null);
        }

        @DexIgnore
        public a(boolean z, boolean z2, boolean z3, boolean z4) {
            this.f1480a = z;
            this.b = z2;
            this.c = z3;
            this.d = z4;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(boolean z, boolean z2, boolean z3, boolean z4, int i, kq7 kq7) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? false : z2, (i & 4) != 0 ? false : z3, (i & 8) != 0 ? false : z4);
        }

        @DexIgnore
        public final boolean a() {
            return this.d;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public final boolean c() {
            return this.f1480a;
        }

        @DexIgnore
        public final boolean d() {
            return this.c;
        }

        @DexIgnore
        public final void e(boolean z) {
            this.d = z;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!(this.f1480a == aVar.f1480a && this.b == aVar.b && this.c == aVar.c && this.d == aVar.d)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final void f(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final void g(boolean z) {
            this.f1480a = z;
        }

        @DexIgnore
        public final void h(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public int hashCode() {
            int i = 1;
            boolean z = this.f1480a;
            if (z) {
                z = true;
            }
            boolean z2 = this.b;
            if (z2) {
                z2 = true;
            }
            boolean z3 = this.c;
            if (z3) {
                z3 = true;
            }
            boolean z4 = this.d;
            if (!z4) {
                i = z4 ? 1 : 0;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            int i6 = z2 ? 1 : 0;
            int i7 = z2 ? 1 : 0;
            int i8 = z3 ? 1 : 0;
            int i9 = z3 ? 1 : 0;
            int i10 = z3 ? 1 : 0;
            return (((((i2 * 31) + i5) * 31) + i8) * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "NavigationState(mNextToWatchFaceList=" + this.f1480a + ", mNextToWatchFaceGallery=" + this.b + ", mNextToWelcome=" + this.c + ", mNextToDashboard=" + this.d + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Uri $it;
        @DexIgnore
        public /* final */ /* synthetic */ Uri $uri$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ hh5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.j;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Uri uri, qn7 qn7, hh5 hh5, Uri uri2) {
            super(2, qn7);
            this.$it = uri;
            this.this$0 = hh5;
            this.$uri$inlined = uri2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.$it, qn7, this.this$0, this.$uri$inlined);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((MFUser) g) == null) {
                hh5.r(this.this$0, false, false, true, false, 11, null);
            } else if (!nk5.o.x(PortfolioApp.h0.c().J())) {
                hh5.r(this.this$0, false, false, false, true, 7, null);
                hq4.d(this.this$0, false, true, null, 5, null);
                return tl7.f3441a;
            } else {
                boolean contains = this.$it.getPathSegments().contains("customized");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = hh5.k;
                local.d(str, "checkToGoToNextStep open my faces: " + contains);
                if (contains) {
                    hh5.r(this.this$0, true, false, false, false, 14, null);
                } else {
                    hh5.r(this.this$0, false, true, false, false, 13, null);
                }
            }
            hq4.d(this.this$0, false, true, null, 5, null);
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qq7 implements gp7<a> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final a invoke() {
            return new a(false, false, false, false, 8, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends qq7 implements gp7<MutableLiveData<a>> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final MutableLiveData<a> invoke() {
            return new MutableLiveData<>();
        }
    }

    /*
    static {
        String simpleName = hh5.class.getSimpleName();
        pq7.b(simpleName, "DeepLinkViewModel::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public hh5(UserRepository userRepository) {
        pq7.c(userRepository, "mUserRepository");
        this.j = userRepository;
    }

    @DexIgnore
    public static /* synthetic */ void r(hh5 hh5, boolean z, boolean z2, boolean z3, boolean z4, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            z = false;
        }
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        if ((i2 & 4) != 0) {
            z3 = false;
        }
        if ((i2 & 8) != 0) {
            z4 = false;
        }
        hh5.q(z, z2, z3, z4);
    }

    @DexIgnore
    public final void p(Uri uri) {
        if (uri != null) {
            hq4.d(this, true, false, null, 6, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = k;
            local.d(str, "checkToGoToNextStep uri " + uri);
            xw7 unused = gu7.d(us0.a(this), null, null, new b(uri, null, this, uri), 3, null);
        }
    }

    @DexIgnore
    public final void q(boolean z, boolean z2, boolean z3, boolean z4) {
        s().f(z2);
        s().g(z);
        s().h(z3);
        s().e(z4);
        t().l(s());
    }

    @DexIgnore
    public final a s() {
        return (a) this.i.getValue();
    }

    @DexIgnore
    public final MutableLiveData<a> t() {
        return (MutableLiveData) this.h.getValue();
    }
}
