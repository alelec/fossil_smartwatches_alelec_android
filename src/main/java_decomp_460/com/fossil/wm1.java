package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wm1 extends ym1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ long d; // = hy1.b(oq7.f2710a);
    @DexIgnore
    public /* final */ long c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<wm1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final wm1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 4) {
                return new wm1(hy1.o(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0)));
            }
            throw new IllegalArgumentException(e.b(e.e("Invalid data size: "), bArr.length, ", require: 4"));
        }

        @DexIgnore
        public wm1 b(Parcel parcel) {
            return new wm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public wm1 createFromParcel(Parcel parcel) {
            return new wm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public wm1[] newArray(int i) {
            return new wm1[i];
        }
    }

    @DexIgnore
    public wm1(long j) throws IllegalArgumentException {
        super(zm1.DAILY_STEP_GOAL);
        this.c = j;
        d();
    }

    @DexIgnore
    public /* synthetic */ wm1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = parcel.readLong();
        d();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.c).array();
        pq7.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public Long c() {
        return Long.valueOf(this.c);
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        long j = d;
        long j2 = this.c;
        if (!(0 <= j2 && j >= j2)) {
            StringBuilder e = e.e("step(");
            e.append(this.c);
            e.append(") is out of range ");
            e.append("[0, ");
            e.append(d);
            e.append("].");
            throw new IllegalArgumentException(e.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(wm1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((wm1) obj).c;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepGoalConfig");
    }

    @DexIgnore
    public final long getStep() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public int hashCode() {
        return Long.valueOf(this.c).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.c);
        }
    }
}
