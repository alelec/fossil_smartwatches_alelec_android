package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i77 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f1591a;
    @DexIgnore
    public static /* final */ a b; // = new a();
    @DexIgnore
    public static /* final */ i77 c; // = new i77();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements wq5.b {
        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            boolean z = true;
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            ServiceActionResult serviceActionResult = ServiceActionResult.values()[intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), ServiceActionResult.UNALLOWED_ACTION.ordinal())];
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
            if (integerArrayListExtra == null) {
                integerArrayListExtra = new ArrayList<>(intExtra);
            }
            Bundle extras = intent.getExtras();
            byte[] byteArray = extras != null ? extras.getByteArray(ButtonService.THEME_BINARY_EXTRA) : null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            i77 i77 = i77.c;
            local.e(i77.f1591a, "errorCode " + intExtra + " permissionErrorCodes " + integerArrayListExtra + " resultCode: " + serviceActionResult + " - themeBinary: " + byteArray);
            int i = h77.f1444a[communicateMode.ordinal()];
            if (i == 1) {
                i77 i772 = i77.c;
                if (serviceActionResult != ServiceActionResult.SUCCEEDED) {
                    z = false;
                }
                i77.e(i772, "preview_theme_action", z, intExtra, null, 8, null);
            } else if (i == 2) {
                i77 i773 = i77.c;
                if (serviceActionResult != ServiceActionResult.SUCCEEDED) {
                    z = false;
                }
                i773.d("apply_theme_action", z, intExtra, byteArray);
            } else if (i == 3) {
                i77.c.d("parse_theme_data_to_bin_action", serviceActionResult == ServiceActionResult.SUCCEEDED, intExtra, byteArray);
            }
        }
    }

    /*
    static {
        String simpleName = i77.class.getSimpleName();
        pq7.b(simpleName, "WFBleConnection::class.java.simpleName");
        f1591a = simpleName;
    }
    */

    @DexIgnore
    public static /* synthetic */ void e(i77 i77, String str, boolean z, int i, byte[] bArr, int i2, Object obj) {
        if ((i2 & 8) != 0) {
            bArr = null;
        }
        i77.d(str, z, i, bArr);
    }

    @DexIgnore
    public final void c() {
        wq5.d.e(b, CommunicateMode.PREVIEW_THEME_SESSION, CommunicateMode.APPLY_THEME_SESSION, CommunicateMode.PARSE_THEME_DATA_TO_BINARY);
    }

    @DexIgnore
    public final void d(String str, boolean z, int i, byte[] bArr) {
        Intent intent = new Intent();
        intent.setAction(str);
        intent.putExtra("theme_result_extra", z);
        if (bArr != null) {
            intent.putExtra(ButtonService.THEME_BINARY_EXTRA, bArr);
        }
        intent.putExtra("error_code_extra", i);
        ct0.b(PortfolioApp.h0.c()).d(intent);
    }

    @DexIgnore
    public final void f() {
        wq5.d.j(b, CommunicateMode.PREVIEW_THEME_SESSION, CommunicateMode.APPLY_THEME_SESSION, CommunicateMode.PARSE_THEME_DATA_TO_BINARY);
    }
}
