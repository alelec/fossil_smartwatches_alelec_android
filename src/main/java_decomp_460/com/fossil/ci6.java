package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.mv5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ci6 extends pv5 implements bi6 {
    @DexIgnore
    public y67 g;
    @DexIgnore
    public g37<x65> h;
    @DexIgnore
    public ai6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ci6 b;

        @DexIgnore
        public a(ci6 ci6, x65 x65) {
            this.b = ci6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ci6.K6(this.b).a().l(2);
        }
    }

    @DexIgnore
    public static final /* synthetic */ y67 K6(ci6 ci6) {
        y67 y67 = ci6.g;
        if (y67 != null) {
            return y67;
        }
        pq7.n("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "GoalTrackingOverviewWeekFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void L6() {
        x65 a2;
        OverviewWeekChart overviewWeekChart;
        g37<x65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null && (overviewWeekChart = a2.v) != null) {
            overviewWeekChart.D("hybridGoalTrackingTab", "nonBrandNonReachGoal");
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(ai6 ai6) {
        pq7.c(ai6, "presenter");
        this.i = ai6;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        x65 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onCreateView");
        x65 x65 = (x65) aq0.f(layoutInflater, 2131558562, viewGroup, false, A6());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ts0 a3 = vs0.e(activity).a(y67.class);
            pq7.b(a3, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.g = (y67) a3;
            x65.r.setOnClickListener(new a(this, x65));
        }
        this.h = new g37<>(this, x65);
        L6();
        g37<x65> g37 = this.h;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onResume");
        L6();
        ai6 ai6 = this.i;
        if (ai6 != null) {
            ai6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onStop");
        ai6 ai6 = this.i;
        if (ai6 != null) {
            ai6.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.bi6
    public void p(mv5 mv5) {
        x65 a2;
        OverviewWeekChart overviewWeekChart;
        pq7.c(mv5, "baseModel");
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "showWeekDetails");
        g37<x65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null && (overviewWeekChart = a2.v) != null) {
            new ArrayList();
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(mv5.f2426a.b(cVar.d()));
            mv5.a aVar = mv5.f2426a;
            pq7.b(overviewWeekChart, "it");
            Context context = overviewWeekChart.getContext();
            pq7.b(context, "it.context");
            BarChart.H(overviewWeekChart, aVar.a(context, cVar), false, 2, null);
            overviewWeekChart.r(mv5);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.bi6
    public void x(boolean z) {
        x65 a2;
        g37<x65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null) {
            if (z) {
                OverviewWeekChart overviewWeekChart = a2.v;
                pq7.b(overviewWeekChart, "binding.weekChart");
                overviewWeekChart.setVisibility(4);
                ConstraintLayout constraintLayout = a2.q;
                pq7.b(constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            OverviewWeekChart overviewWeekChart2 = a2.v;
            pq7.b(overviewWeekChart2, "binding.weekChart");
            overviewWeekChart2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.q;
            pq7.b(constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }
}
