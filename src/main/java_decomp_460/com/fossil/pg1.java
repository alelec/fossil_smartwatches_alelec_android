package com.fossil;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pg1 implements qb1<ParcelFileDescriptor, Bitmap> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ gg1 f2828a;

    @DexIgnore
    public pg1(gg1 gg1) {
        this.f2828a = gg1;
    }

    @DexIgnore
    /* renamed from: c */
    public id1<Bitmap> b(ParcelFileDescriptor parcelFileDescriptor, int i, int i2, ob1 ob1) throws IOException {
        return this.f2828a.d(parcelFileDescriptor, i, i2, ob1);
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(ParcelFileDescriptor parcelFileDescriptor, ob1 ob1) {
        return this.f2828a.o(parcelFileDescriptor);
    }
}
