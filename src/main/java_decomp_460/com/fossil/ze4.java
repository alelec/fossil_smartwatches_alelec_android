package com.fossil;

import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ze4 implements mt3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ FirebaseInstanceId f4461a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public ze4(FirebaseInstanceId firebaseInstanceId, String str, String str2, String str3) {
        this.f4461a = firebaseInstanceId;
        this.b = str;
        this.c = str2;
        this.d = str3;
    }

    @DexIgnore
    @Override // com.fossil.mt3
    public final nt3 then(Object obj) {
        return this.f4461a.A(this.b, this.c, this.d, (String) obj);
    }
}
