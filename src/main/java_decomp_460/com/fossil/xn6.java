package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentManager;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xn6 extends pv5 implements cy5 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public g37<z35> g;
    @DexIgnore
    public by5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return xn6.j;
        }

        @DexIgnore
        public final xn6 b() {
            return new xn6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ xn6 f4148a;

        @DexIgnore
        public b(xn6 xn6) {
            this.f4148a = xn6;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            pq7.c(compoundButton, "button");
            if (compoundButton.isPressed()) {
                xn6.K6(this.f4148a).n();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xn6 b;

        @DexIgnore
        public c(xn6 xn6) {
            this.b = xn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.getActivity() != null) {
                this.b.requireActivity().finish();
            }
        }
    }

    /*
    static {
        String simpleName = xn6.class.getSimpleName();
        if (simpleName != null) {
            pq7.b(simpleName, "ConnectedAppsFragment::class.java.simpleName!!");
            j = simpleName;
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ by5 K6(xn6 xn6) {
        by5 by5 = xn6.h;
        if (by5 != null) {
            return by5;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return j;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.cy5
    public void I5(int i2) {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, "", childFragmentManager);
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(by5 by5) {
        pq7.c(by5, "presenter");
        this.h = by5;
    }

    @DexIgnore
    @Override // com.fossil.cy5
    public void N2(boolean z) {
        if (isActive()) {
            g37<z35> g37 = this.g;
            if (g37 != null) {
                z35 a2 = g37.a();
                if (a2 != null) {
                    FlexibleSwitchCompat flexibleSwitchCompat = a2.q;
                    pq7.b(flexibleSwitchCompat, "it.cbGooglefit");
                    flexibleSwitchCompat.setChecked(z);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.cy5
    public void f1(z52 z52) {
        pq7.c(z52, "connectionResult");
        z52.D(getActivity(), 1);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1 && i3 == -1) {
            by5 by5 = this.h;
            if (by5 != null) {
                by5.o();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        g37<z35> g37 = new g37<>(this, (z35) aq0.f(layoutInflater, 2131558523, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            z35 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        by5 by5 = this.h;
        if (by5 != null) {
            by5.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        by5 by5 = this.h;
        if (by5 != null) {
            by5.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<z35> g37 = this.g;
        if (g37 != null) {
            z35 a2 = g37.a();
            if (a2 != null) {
                a2.q.setOnCheckedChangeListener(new b(this));
                a2.w.setOnClickListener(new c(this));
            }
            E6("connected_app_view");
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
