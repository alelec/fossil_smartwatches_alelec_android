package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gd3 extends as2 implements ec3 {
    @DexIgnore
    public gd3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IStreetViewPanoramaViewDelegate");
    }

    @DexIgnore
    @Override // com.fossil.ec3
    public final void I(ad3 ad3) throws RemoteException {
        Parcel d = d();
        es2.c(d, ad3);
        i(9, d);
    }

    @DexIgnore
    @Override // com.fossil.ec3
    public final rg2 getView() throws RemoteException {
        Parcel e = e(8, d());
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.ec3
    public final void onCreate(Bundle bundle) throws RemoteException {
        Parcel d = d();
        es2.d(d, bundle);
        i(2, d);
    }

    @DexIgnore
    @Override // com.fossil.ec3
    public final void onDestroy() throws RemoteException {
        i(5, d());
    }

    @DexIgnore
    @Override // com.fossil.ec3
    public final void onLowMemory() throws RemoteException {
        i(6, d());
    }

    @DexIgnore
    @Override // com.fossil.ec3
    public final void onPause() throws RemoteException {
        i(4, d());
    }

    @DexIgnore
    @Override // com.fossil.ec3
    public final void onResume() throws RemoteException {
        i(3, d());
    }

    @DexIgnore
    @Override // com.fossil.ec3
    public final void onSaveInstanceState(Bundle bundle) throws RemoteException {
        Parcel d = d();
        es2.d(d, bundle);
        Parcel e = e(7, d);
        if (e.readInt() != 0) {
            bundle.readFromParcel(e);
        }
        e.recycle();
    }

    @DexIgnore
    @Override // com.fossil.ec3
    public final void onStart() throws RemoteException {
        i(10, d());
    }

    @DexIgnore
    @Override // com.fossil.ec3
    public final void onStop() throws RemoteException {
        i(11, d());
    }
}
