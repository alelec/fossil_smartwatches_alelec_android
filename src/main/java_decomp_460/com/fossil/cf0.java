package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import com.fossil.cg0;
import com.fossil.ig0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cf0 extends ActionBar {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ch0 f603a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public Window.Callback c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public ArrayList<ActionBar.a> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Runnable g; // = new a();
    @DexIgnore
    public /* final */ Toolbar.e h; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            cf0.this.A();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Toolbar.e {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // androidx.appcompat.widget.Toolbar.e
        public boolean onMenuItemClick(MenuItem menuItem) {
            return cf0.this.c.onMenuItemSelected(0, menuItem);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements ig0.a {
        @DexIgnore
        public boolean b;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.ig0.a
        public void b(cg0 cg0, boolean z) {
            if (!this.b) {
                this.b = true;
                cf0.this.f603a.h();
                Window.Callback callback = cf0.this.c;
                if (callback != null) {
                    callback.onPanelClosed(108, cg0);
                }
                this.b = false;
            }
        }

        @DexIgnore
        @Override // com.fossil.ig0.a
        public boolean c(cg0 cg0) {
            Window.Callback callback = cf0.this.c;
            if (callback == null) {
                return false;
            }
            callback.onMenuOpened(108, cg0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements cg0.a {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.cg0.a
        public boolean a(cg0 cg0, MenuItem menuItem) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.cg0.a
        public void b(cg0 cg0) {
            cf0 cf0 = cf0.this;
            if (cf0.c == null) {
                return;
            }
            if (cf0.f603a.b()) {
                cf0.this.c.onPanelClosed(108, cg0);
            } else if (cf0.this.c.onPreparePanel(0, null, cg0)) {
                cf0.this.c.onMenuOpened(108, cg0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends vf0 {
        @DexIgnore
        public e(Window.Callback callback) {
            super(callback);
        }

        @DexIgnore
        @Override // com.fossil.vf0
        public View onCreatePanelView(int i) {
            return i == 0 ? new View(cf0.this.f603a.getContext()) : super.onCreatePanelView(i);
        }

        @DexIgnore
        @Override // com.fossil.vf0
        public boolean onPreparePanel(int i, View view, Menu menu) {
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (onPreparePanel) {
                cf0 cf0 = cf0.this;
                if (!cf0.b) {
                    cf0.f603a.c();
                    cf0.this.b = true;
                }
            }
            return onPreparePanel;
        }
    }

    @DexIgnore
    public cf0(Toolbar toolbar, CharSequence charSequence, Window.Callback callback) {
        this.f603a = new uh0(toolbar, false);
        e eVar = new e(callback);
        this.c = eVar;
        this.f603a.setWindowCallback(eVar);
        toolbar.setOnMenuItemClickListener(this.h);
        this.f603a.setWindowTitle(charSequence);
    }

    @DexIgnore
    public void A() {
        Menu y = y();
        cg0 cg0 = y instanceof cg0 ? (cg0) y : null;
        if (cg0 != null) {
            cg0.h0();
        }
        try {
            y.clear();
            if (!this.c.onCreatePanelMenu(0, y) || !this.c.onPreparePanel(0, null, y)) {
                y.clear();
            }
        } finally {
            if (cg0 != null) {
                cg0.g0();
            }
        }
    }

    @DexIgnore
    public void B(int i, int i2) {
        this.f603a.k((this.f603a.s() & i2) | (i & i2));
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean g() {
        return this.f603a.f();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean h() {
        if (!this.f603a.j()) {
            return false;
        }
        this.f603a.collapseActionView();
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void i(boolean z) {
        if (z != this.e) {
            this.e = z;
            int size = this.f.size();
            for (int i = 0; i < size; i++) {
                this.f.get(i).a(z);
            }
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public int j() {
        return this.f603a.s();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public Context k() {
        return this.f603a.getContext();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean l() {
        this.f603a.q().removeCallbacks(this.g);
        mo0.d0(this.f603a.q(), this.g);
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void m(Configuration configuration) {
        super.m(configuration);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void n() {
        this.f603a.q().removeCallbacks(this.g);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean o(int i, KeyEvent keyEvent) {
        Menu y = y();
        if (y == null) {
            return false;
        }
        y.setQwertyMode(KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1);
        return y.performShortcut(i, keyEvent, 0);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean p(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            q();
        }
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean q() {
        return this.f603a.g();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void r(boolean z) {
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void s(boolean z) {
        B(z ? 4 : 0, 4);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void t(float f2) {
        mo0.s0(this.f603a.q(), f2);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void u(boolean z) {
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void v(CharSequence charSequence) {
        this.f603a.setTitle(charSequence);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void w(CharSequence charSequence) {
        this.f603a.setWindowTitle(charSequence);
    }

    @DexIgnore
    public final Menu y() {
        if (!this.d) {
            this.f603a.p(new c(), new d());
            this.d = true;
        }
        return this.f603a.l();
    }

    @DexIgnore
    public Window.Callback z() {
        return this.c;
    }
}
