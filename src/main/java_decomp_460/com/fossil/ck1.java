package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ck1 extends InputStream {
    @DexIgnore
    public static /* final */ Queue<ck1> d; // = jk1.f(0);
    @DexIgnore
    public InputStream b;
    @DexIgnore
    public IOException c;

    @DexIgnore
    public static ck1 b(InputStream inputStream) {
        ck1 poll;
        synchronized (d) {
            poll = d.poll();
        }
        if (poll == null) {
            poll = new ck1();
        }
        poll.f(inputStream);
        return poll;
    }

    @DexIgnore
    public IOException a() {
        return this.c;
    }

    @DexIgnore
    @Override // java.io.InputStream
    public int available() throws IOException {
        return this.b.available();
    }

    @DexIgnore
    public void c() {
        this.c = null;
        this.b = null;
        synchronized (d) {
            d.offer(this);
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
    public void close() throws IOException {
        this.b.close();
    }

    @DexIgnore
    public void f(InputStream inputStream) {
        this.b = inputStream;
    }

    @DexIgnore
    public void mark(int i) {
        this.b.mark(i);
    }

    @DexIgnore
    public boolean markSupported() {
        return this.b.markSupported();
    }

    @DexIgnore
    @Override // java.io.InputStream
    public int read() {
        try {
            return this.b.read();
        } catch (IOException e) {
            this.c = e;
            return -1;
        }
    }

    @DexIgnore
    @Override // java.io.InputStream
    public int read(byte[] bArr) {
        try {
            return this.b.read(bArr);
        } catch (IOException e) {
            this.c = e;
            return -1;
        }
    }

    @DexIgnore
    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        try {
            return this.b.read(bArr, i, i2);
        } catch (IOException e) {
            this.c = e;
            return -1;
        }
    }

    @DexIgnore
    @Override // java.io.InputStream
    public void reset() throws IOException {
        synchronized (this) {
            this.b.reset();
        }
    }

    @DexIgnore
    @Override // java.io.InputStream
    public long skip(long j) {
        try {
            return this.b.skip(j);
        } catch (IOException e) {
            this.c = e;
            return 0;
        }
    }
}
