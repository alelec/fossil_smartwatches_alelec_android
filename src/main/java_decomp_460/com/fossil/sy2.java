package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sy2<K, V> extends ay2<Map.Entry<K, V>> {
    @DexIgnore
    public /* final */ transient wx2<K, V> d;
    @DexIgnore
    public /* final */ transient Object[] e;
    @DexIgnore
    public /* final */ transient int f;

    @DexIgnore
    public sy2(wx2<K, V> wx2, Object[] objArr, int i, int i2) {
        this.d = wx2;
        this.e = objArr;
        this.f = i2;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final boolean contains(Object obj) {
        if (obj instanceof Map.Entry) {
            Map.Entry entry = (Map.Entry) obj;
            Object key = entry.getKey();
            Object value = entry.getValue();
            return value != null && value.equals(this.d.get(key));
        }
    }

    @DexIgnore
    public final int size() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final int zzb(Object[] objArr, int i) {
        return zzc().zzb(objArr, i);
    }

    @DexIgnore
    @Override // com.fossil.tx2
    /* renamed from: zzb */
    public final cz2<Map.Entry<K, V>> iterator() {
        return (cz2) zzc().iterator();
    }

    @DexIgnore
    @Override // com.fossil.ay2
    public final sx2<Map.Entry<K, V>> zzd() {
        return new ry2(this);
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final boolean zzh() {
        return true;
    }
}
