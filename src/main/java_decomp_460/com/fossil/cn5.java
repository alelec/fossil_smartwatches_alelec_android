package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.LocalFile;
import com.portfolio.platform.data.source.remote.DownloadServiceApi;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cn5 {
    @DexIgnore
    public static /* final */ dv7 i; // = bw7.b();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f630a;
    @DexIgnore
    public u08 b; // = w08.b(false, 1, null);
    @DexIgnore
    public DownloadServiceApi c;
    @DexIgnore
    public /* final */ LinkedBlockingDeque<a> d; // = new LinkedBlockingDeque<>();
    @DexIgnore
    public /* final */ List<String> e; // = new ArrayList();
    @DexIgnore
    public /* final */ uu7 f;
    @DexIgnore
    public /* final */ iv7 g;
    @DexIgnore
    public /* final */ iv7 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f631a;
        @DexIgnore
        public /* final */ LocalFile b;
        @DexIgnore
        public /* final */ b c;

        @DexIgnore
        public a(LocalFile localFile, b bVar) {
            pq7.c(localFile, "localFile");
            this.b = localFile;
            this.c = bVar;
        }

        @DexIgnore
        public final b a() {
            return this.c;
        }

        @DexIgnore
        public final LocalFile b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.f631a;
        }

        @DexIgnore
        public final void d(int i) {
            this.f631a = i;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof a)) {
                return false;
            }
            return pq7.a(this.b.getRemoteUrl(), ((a) obj).b.getRemoteUrl());
        }

        @DexIgnore
        public int hashCode() {
            int hashCode = this.b.hashCode();
            b bVar = this.c;
            return (bVar != null ? bVar.hashCode() : 0) + (hashCode * 31);
        }

        @DexIgnore
        public String toString() {
            return "DownloadFileJobInfo(localFile=" + this.b + ", callback=" + this.c + ")";
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void onComplete(boolean z, LocalFile localFile);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.FileDownloadManager$downloadFile$1", f = "FileDownloadManager.kt", l = {100}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ a $job;
        @DexIgnore
        public /* final */ /* synthetic */ LocalFile $localFile;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ cn5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.manager.FileDownloadManager$downloadFile$1$repoResponse$1", f = "FileDownloadManager.kt", l = {100}, m = "invokeSuspend")
        public static final class a extends ko7 implements rp7<qn7<? super q88<w18>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(1, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(qn7<?> qn7) {
                pq7.c(qn7, "completion");
                return new a(this.this$0, qn7);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.rp7
            public final Object invoke(qn7<? super q88<w18>> qn7) {
                return ((a) create(qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    DownloadServiceApi j = this.this$0.this$0.j();
                    String remoteUrl = this.this$0.$localFile.getRemoteUrl();
                    this.label = 1;
                    Object downloadFile = j.downloadFile(remoteUrl, this);
                    return downloadFile == d ? d : downloadFile;
                } else if (i == 1) {
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(cn5 cn5, LocalFile localFile, a aVar, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cn5;
            this.$localFile = localFile;
            this.$job = aVar;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$localFile, this.$job, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00c9  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0188  */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x0326  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 990
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.cn5.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.FileDownloadManager$enqueue$1", f = "FileDownloadManager.kt", l = {213}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ cn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(cn5 cn5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            u08 u08;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                u08 = this.this$0.b;
                this.L$0 = iv7;
                this.L$1 = u08;
                this.label = 1;
                if (u08.a(null, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                u08 = (u08) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            try {
                this.this$0.g();
                tl7 tl7 = tl7.f3441a;
                u08.b(null);
                return tl7.f3441a;
            } catch (Throwable th) {
                u08.b(null);
                throw th;
            }
        }
    }

    @DexIgnore
    public cn5() {
        uu7 b2 = ux7.b(null, 1, null);
        this.f = b2;
        this.g = jv7.a(b2.plus(i));
        this.h = jv7.a(this.f.plus(bw7.a()));
        PortfolioApp.h0.c().M().i1(this);
    }

    @DexIgnore
    public final void g() {
        synchronized (this) {
            if (this.d.size() > 0) {
                a pop = this.d.pop();
                pq7.b(pop, "jobToRun");
                h(pop);
            }
        }
    }

    @DexIgnore
    public final void h(a aVar) {
        LocalFile b2 = aVar.b();
        this.f630a++;
        this.e.add(b2.getRemoteUrl());
        xw7 unused = gu7.d(this.g, null, null, new c(this, b2, aVar, null), 3, null);
    }

    @DexIgnore
    public final void i(a aVar) {
        synchronized (this) {
            pq7.c(aVar, "job");
            if (this.e.contains(aVar.b().getRemoteUrl()) || this.d.contains(aVar)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FileDownloadManager", aVar.b().getRemoteUrl() + " is already added");
                return;
            }
            this.d.add(aVar);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("FileDownloadManager", aVar.b().getRemoteUrl() + " added to mQueue at times: " + aVar.c() + ", having " + this.f630a + " download running");
            xw7 unused = gu7.d(this.h, null, null, new d(this, null), 3, null);
        }
    }

    @DexIgnore
    public final DownloadServiceApi j() {
        DownloadServiceApi downloadServiceApi = this.c;
        if (downloadServiceApi != null) {
            return downloadServiceApi;
        }
        pq7.n("mApiService");
        throw null;
    }
}
