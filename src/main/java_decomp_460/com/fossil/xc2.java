package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import com.facebook.LegacyTokenHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xc2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Resources f4096a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public xc2(Context context) {
        rc2.k(context);
        Resources resources = context.getResources();
        this.f4096a = resources;
        this.b = resources.getResourcePackageName(j62.common_google_play_services_unknown_issue);
    }

    @DexIgnore
    public String a(String str) {
        int identifier = this.f4096a.getIdentifier(str, LegacyTokenHelper.TYPE_STRING, this.b);
        if (identifier == 0) {
            return null;
        }
        return this.f4096a.getString(identifier);
    }
}
