package com.fossil;

import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ay1 {
    @DexIgnore
    public static final JSONArray a(int[] iArr) {
        pq7.c(iArr, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (int i : iArr) {
            jSONArray.put(i);
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONArray b(String[] strArr) {
        pq7.c(strArr, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (String str : strArr) {
            jSONArray.put(str);
        }
        return jSONArray;
    }
}
