package com.fossil;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.fossil.kq3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gq3<T extends Context & kq3> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ T f1344a;

    @DexIgnore
    public gq3(T t) {
        rc2.k(t);
        this.f1344a = t;
    }

    @DexIgnore
    public final int a(Intent intent, int i, int i2) {
        pm3 a2 = pm3.a(this.f1344a, null, null);
        kl3 d = a2.d();
        if (intent == null) {
            d.I().a("AppMeasurementService started with null intent");
        } else {
            String action = intent.getAction();
            a2.b();
            d.N().c("Local AppMeasurementService called. startId, action", Integer.valueOf(i2), action);
            if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
                f(new fq3(this, i2, d, intent));
            }
        }
        return 2;
    }

    @DexIgnore
    public final IBinder b(Intent intent) {
        if (intent == null) {
            j().F().a("onBind called with null intent");
            return null;
        }
        String action = intent.getAction();
        if ("com.google.android.gms.measurement.START".equals(action)) {
            return new qm3(yq3.g(this.f1344a));
        }
        j().I().b("onBind received unknown action", action);
        return null;
    }

    @DexIgnore
    public final void c() {
        pm3 a2 = pm3.a(this.f1344a, null, null);
        kl3 d = a2.d();
        a2.b();
        d.N().a("Local AppMeasurementService is starting up");
    }

    @DexIgnore
    public final /* synthetic */ void d(int i, kl3 kl3, Intent intent) {
        if (this.f1344a.zza(i)) {
            kl3.N().b("Local AppMeasurementService processed last upload request. StartId", Integer.valueOf(i));
            j().N().a("Completed wakeful intent.");
            this.f1344a.a(intent);
        }
    }

    @DexIgnore
    public final /* synthetic */ void e(kl3 kl3, JobParameters jobParameters) {
        kl3.N().a("AppMeasurementJobService processed last upload request.");
        this.f1344a.b(jobParameters, false);
    }

    @DexIgnore
    public final void f(Runnable runnable) {
        yq3 g = yq3.g(this.f1344a);
        g.c().y(new hq3(this, g, runnable));
    }

    @DexIgnore
    @TargetApi(24)
    public final boolean g(JobParameters jobParameters) {
        pm3 a2 = pm3.a(this.f1344a, null, null);
        kl3 d = a2.d();
        String string = jobParameters.getExtras().getString("action");
        a2.b();
        d.N().b("Local AppMeasurementJobService called. action", string);
        if (!"com.google.android.gms.measurement.UPLOAD".equals(string)) {
            return true;
        }
        f(new iq3(this, d, jobParameters));
        return true;
    }

    @DexIgnore
    public final void h() {
        pm3 a2 = pm3.a(this.f1344a, null, null);
        kl3 d = a2.d();
        a2.b();
        d.N().a("Local AppMeasurementService is shutting down");
    }

    @DexIgnore
    public final boolean i(Intent intent) {
        if (intent == null) {
            j().F().a("onUnbind called with null intent");
        } else {
            j().N().b("onUnbind called for intent. action", intent.getAction());
        }
        return true;
    }

    @DexIgnore
    public final kl3 j() {
        return pm3.a(this.f1344a, null, null).d();
    }

    @DexIgnore
    public final void k(Intent intent) {
        if (intent == null) {
            j().F().a("onRebind called with null intent");
            return;
        }
        j().N().b("onRebind called. action", intent.getAction());
    }
}
