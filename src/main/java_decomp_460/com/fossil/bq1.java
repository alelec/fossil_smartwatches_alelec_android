package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.EnumMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq1 extends vp1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ EnumMap<mu1, ap1[]> e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<bq1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public bq1 createFromParcel(Parcel parcel) {
            return new bq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public bq1[] newArray(int i) {
            return new bq1[i];
        }
    }

    @DexIgnore
    public bq1(byte b, int i, EnumMap<mu1, ap1[]> enumMap) {
        super(np1.WATCH_APP_LIFE_CYCLE, b);
        this.d = i;
        this.e = enumMap;
    }

    @DexIgnore
    public /* synthetic */ bq1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.d = parcel.readInt();
        Serializable readSerializable = parcel.readSerializable();
        if (readSerializable != null) {
            this.e = (EnumMap) readSerializable;
            return;
        }
        throw new il7("null cannot be cast to non-null type java.util.EnumMap<com.fossil.blesdk.model.enumerate.WatchAppStatus, kotlin.Array<com.fossil.blesdk.device.data.watchapp.WatchAppId>>");
    }

    @DexIgnore
    public final int b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(bq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            bq1 bq1 = (bq1) obj;
            if (this.d != bq1.d) {
                return false;
            }
            return !(pq7.a(this.e, bq1.e) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.WatchAppLifeCycleNotification");
    }

    @DexIgnore
    public final EnumMap<mu1, ap1[]> getWatchAppStatus() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public int hashCode() {
        int hashCode = super.hashCode();
        return (((hashCode * 31) + Integer.valueOf(this.d).hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject k = g80.k(super.toJSONObject(), jd0.q, Integer.valueOf(this.d));
        for (Map.Entry<K, ap1[]> entry : this.e.entrySet()) {
            JSONArray jSONArray = new JSONArray();
            ap1[] value = entry.getValue();
            pq7.b(value, "entry.value");
            for (ap1 ap1 : value) {
                jSONArray.put(ap1.a());
            }
            if (jSONArray.length() > 0) {
                k.put(entry.getKey().a(), jSONArray);
            }
        }
        return k;
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeSerializable(this.e);
        }
    }
}
