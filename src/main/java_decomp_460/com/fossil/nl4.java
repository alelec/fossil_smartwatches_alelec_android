package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nl4 extends pl4 {
    @DexIgnore
    public static /* final */ nl4 b;

    /*
    static {
        nl4 nl4 = new nl4();
        b = nl4;
        nl4.setStackTrace(pl4.NO_TRACE);
    }
    */

    @DexIgnore
    public nl4() {
    }

    @DexIgnore
    public nl4(Throwable th) {
        super(th);
    }

    @DexIgnore
    public static nl4 getFormatInstance() {
        return pl4.isStackTrace ? new nl4() : b;
    }

    @DexIgnore
    public static nl4 getFormatInstance(Throwable th) {
        return pl4.isStackTrace ? new nl4(th) : b;
    }
}
