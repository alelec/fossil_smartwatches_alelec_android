package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hn7 implements Iterator<ll7>, jr7 {
    @DexIgnore
    /* renamed from: a */
    public final ll7 next() {
        return ll7.a(b());
    }

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
