package com.fossil;

import android.content.ContentResolver;
import android.net.Uri;
import android.util.Log;
import com.fossil.wb1;
import java.io.FileNotFoundException;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ec1<T> implements wb1<T> {
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ ContentResolver c;
    @DexIgnore
    public T d;

    @DexIgnore
    public ec1(ContentResolver contentResolver, Uri uri) {
        this.c = contentResolver;
        this.b = uri;
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public void a() {
        T t = this.d;
        if (t != null) {
            try {
                b(t);
            } catch (IOException e) {
            }
        }
    }

    @DexIgnore
    public abstract void b(T t) throws IOException;

    @DexIgnore
    @Override // com.fossil.wb1
    public gb1 c() {
        return gb1.LOCAL;
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public void cancel() {
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public final void d(sa1 sa1, wb1.a<? super T> aVar) {
        try {
            T e = e(this.b, this.c);
            this.d = e;
            aVar.e(e);
        } catch (FileNotFoundException e2) {
            if (Log.isLoggable("LocalUriFetcher", 3)) {
                Log.d("LocalUriFetcher", "Failed to open Uri", e2);
            }
            aVar.b(e2);
        }
    }

    @DexIgnore
    public abstract T e(Uri uri, ContentResolver contentResolver) throws FileNotFoundException;
}
