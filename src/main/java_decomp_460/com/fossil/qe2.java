package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qe2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static Object f2966a; // = new Object();
    @DexIgnore
    public static boolean b;
    @DexIgnore
    public static String c;
    @DexIgnore
    public static int d;

    @DexIgnore
    public static String a(Context context) {
        c(context);
        return c;
    }

    @DexIgnore
    public static int b(Context context) {
        c(context);
        return d;
    }

    @DexIgnore
    public static void c(Context context) {
        synchronized (f2966a) {
            if (!b) {
                b = true;
                try {
                    Bundle bundle = ag2.a(context).c(context.getPackageName(), 128).metaData;
                    if (bundle != null) {
                        c = bundle.getString("com.google.app.id");
                        d = bundle.getInt("com.google.android.gms.version");
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    Log.wtf("MetadataValueReader", "This should never happen.", e);
                }
            }
        }
    }
}
