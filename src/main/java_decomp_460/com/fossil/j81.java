package com.fossil;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface j81 {
    @DexIgnore
    void a(Drawable drawable);

    @DexIgnore
    void c(Drawable drawable);

    @DexIgnore
    void d(Drawable drawable);
}
