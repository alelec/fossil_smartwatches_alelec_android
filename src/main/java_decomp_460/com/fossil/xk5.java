package com.fossil;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xk5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xk5 f4136a; // = new xk5();

    @DexIgnore
    public final void a(View view, Context context) {
        pq7.c(view, "view");
        pq7.c(context, "context");
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @DexIgnore
    public final void b(View view, Context context) {
        pq7.c(view, "view");
        pq7.c(context, "context");
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.showSoftInput(view, 2);
        }
    }
}
