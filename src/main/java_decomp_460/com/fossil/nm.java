package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nm extends lp {
    @DexIgnore
    public /* final */ ArrayList<ow> C;
    @DexIgnore
    public short D;
    @DexIgnore
    public long E;
    @DexIgnore
    public int F;
    @DexIgnore
    public /* final */ ArrayList<j0> G;
    @DexIgnore
    public /* final */ ve H;
    @DexIgnore
    public /* final */ short I;
    @DexIgnore
    public float J;
    @DexIgnore
    public /* final */ nm1 K;
    @DexIgnore
    public /* final */ boolean L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ float N;

    @DexIgnore
    public nm(k5 k5Var, i60 i60, nm1 nm1, boolean z, boolean z2, float f, String str) {
        super(k5Var, i60, yp.m0, str, false, 16);
        this.K = nm1;
        this.L = z;
        this.M = z2;
        this.N = f;
        this.C = by1.a(this.i, hm7.c(ow.FILE_CONFIG, ow.TRANSFER_DATA));
        this.G = new ArrayList<>();
        this.H = hd0.y.m();
        this.I = (short) 256;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ nm(k5 k5Var, i60 i60, nm1 nm1, boolean z, boolean z2, float f, String str, int i) {
        this(k5Var, i60, nm1, z, z2, (i & 32) != 0 ? 0.001f : f, (i & 64) != 0 ? e.a("UUID.randomUUID().toString()") : str);
    }

    @DexIgnore
    public static final /* synthetic */ void J(nm nmVar) {
        int i = nmVar.F + 1;
        nmVar.F = i;
        if (i < nmVar.D) {
            nmVar.M();
        } else if (nmVar.M) {
            nmVar.l(nr.a(nmVar.v, null, zq.SUCCESS, null, null, 13));
        } else {
            nmVar.L();
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        lp.h(this, new tl(this.w, this.x, this.H, this.z), new ol(this), new am(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        JSONObject put = super.C().put(zm1.BIOMETRIC_PROFILE.b(), this.K.c());
        pq7.b(put, "super.optionDescription(\u2026ofile.valueDescription())");
        return g80.k(g80.k(g80.k(put, jd0.j3, Boolean.valueOf(this.L)), jd0.k3, Boolean.valueOf(this.M)), jd0.A0, hy1.l(this.I, null, 1, null));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        JSONObject E2 = super.E();
        jd0 jd0 = jd0.P2;
        Object[] array = this.G.toArray(new j0[0]);
        if (array != null) {
            return g80.k(E2, jd0, px1.a((ox1[]) array));
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void L() {
        lp.i(this, new qv(this.I, this.w, 0, 4), new di(this), new ri(this), null, null, null, 56, null);
    }

    @DexIgnore
    public final void M() {
        short s = (short) (this.I + this.F);
        lp.i(this, new vv(s, this.E, this.w, 0, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 24), new ej(this, s), new qj(this), new ck(this), null, null, 48, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.C;
    }
}
