package com.fossil;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class og3 {
    @DexIgnore
    public static Set<String> a(SQLiteDatabase sQLiteDatabase, String str) {
        HashSet hashSet = new HashSet();
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 22);
        sb.append("SELECT * FROM ");
        sb.append(str);
        sb.append(" LIMIT 0");
        Cursor rawQuery = sQLiteDatabase.rawQuery(sb.toString(), null);
        try {
            Collections.addAll(hashSet, rawQuery.getColumnNames());
            return hashSet;
        } finally {
            rawQuery.close();
        }
    }

    @DexIgnore
    public static void b(kl3 kl3, SQLiteDatabase sQLiteDatabase) {
        if (kl3 != null) {
            File file = new File(sQLiteDatabase.getPath());
            if (!file.setReadable(false, false)) {
                kl3.I().a("Failed to turn off database read permission");
            }
            if (!file.setWritable(false, false)) {
                kl3.I().a("Failed to turn off database write permission");
            }
            if (!file.setReadable(true, true)) {
                kl3.I().a("Failed to turn on database read permission for owner");
            }
            if (!file.setWritable(true, true)) {
                kl3.I().a("Failed to turn on database write permission for owner");
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Monitor must not be null");
    }

    @DexIgnore
    public static void c(kl3 kl3, SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, String[] strArr) throws SQLiteException {
        if (kl3 != null) {
            if (!d(kl3, sQLiteDatabase, str)) {
                sQLiteDatabase.execSQL(str2);
            }
            if (kl3 != null) {
                try {
                    Set<String> a2 = a(sQLiteDatabase, str);
                    String[] split = str3.split(",");
                    for (String str4 : split) {
                        if (!a2.remove(str4)) {
                            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 35 + String.valueOf(str4).length());
                            sb.append("Table ");
                            sb.append(str);
                            sb.append(" is missing required column: ");
                            sb.append(str4);
                            throw new SQLiteException(sb.toString());
                        }
                    }
                    if (strArr != null) {
                        for (int i = 0; i < strArr.length; i += 2) {
                            if (!a2.remove(strArr[i])) {
                                sQLiteDatabase.execSQL(strArr[i + 1]);
                            }
                        }
                    }
                    if (!a2.isEmpty()) {
                        kl3.I().c("Table has extra columns. table, columns", str, TextUtils.join(", ", a2));
                    }
                } catch (SQLiteException e) {
                    kl3.F().b("Failed to verify columns on table that was just created", str);
                    throw e;
                }
            } else {
                throw new IllegalArgumentException("Monitor must not be null");
            }
        } else {
            throw new IllegalArgumentException("Monitor must not be null");
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean d(com.fossil.kl3 r10, android.database.sqlite.SQLiteDatabase r11, java.lang.String r12) {
        /*
            r8 = 0
            r9 = 0
            if (r10 == 0) goto L_0x0042
            java.lang.String r1 = "SQLITE_MASTER"
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0028, all -> 0x003a }
            r0 = 0
            java.lang.String r3 = "name"
            r2[r0] = r3     // Catch:{ SQLiteException -> 0x0028, all -> 0x003a }
            java.lang.String r3 = "name=?"
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0028, all -> 0x003a }
            r0 = 0
            r4[r0] = r12     // Catch:{ SQLiteException -> 0x0028, all -> 0x003a }
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r11
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0028, all -> 0x003a }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x004a }
            if (r1 == 0) goto L_0x0027
            r1.close()
        L_0x0027:
            return r0
        L_0x0028:
            r0 = move-exception
            r1 = r9
        L_0x002a:
            com.fossil.nl3 r2 = r10.I()     // Catch:{ all -> 0x004c }
            java.lang.String r3 = "Error querying for table"
            r2.c(r3, r12, r0)     // Catch:{ all -> 0x004c }
            if (r1 == 0) goto L_0x0038
            r1.close()
        L_0x0038:
            r0 = r8
            goto L_0x0027
        L_0x003a:
            r0 = move-exception
            r1 = r9
        L_0x003c:
            if (r1 == 0) goto L_0x0041
            r1.close()
        L_0x0041:
            throw r0
        L_0x0042:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Monitor must not be null"
            r0.<init>(r1)
            throw r0
        L_0x004a:
            r0 = move-exception
            goto L_0x002a
        L_0x004c:
            r0 = move-exception
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.og3.d(com.fossil.kl3, android.database.sqlite.SQLiteDatabase, java.lang.String):boolean");
    }
}
