package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yt4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ApiServiceV2 f4368a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {114}, m = "block")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(yt4 yt4, qn7 qn7) {
            super(qn7);
            this.this$0 = yt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$block$response$1", f = "FriendRemoteDataSource.kt", l = {114}, m = "invokeSuspend")
    public static final class b extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(yt4 yt4, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = yt4;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new b(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((b) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f4368a;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object block = apiServiceV2.block(gj4, this);
                return block == d ? d : block;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {99}, m = "cancelFriendRequest")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(yt4 yt4, qn7 qn7) {
            super(qn7);
            this.this$0 = yt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$cancelFriendRequest$response$1", f = "FriendRemoteDataSource.kt", l = {99}, m = "invokeSuspend")
    public static final class d extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(yt4 yt4, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = yt4;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new d(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((d) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f4368a;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object cancelFriendRequest = apiServiceV2.cancelFriendRequest(gj4, this);
                return cancelFriendRequest == d ? d : cancelFriendRequest;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {53}, m = "fetchFriends")
    public static final class e extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(yt4 yt4, qn7 qn7) {
            super(qn7);
            this.this$0 = yt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$fetchFriends$response$1", f = "FriendRemoteDataSource.kt", l = {53}, m = "invokeSuspend")
    public static final class f extends ko7 implements rp7<qn7<? super q88<ApiResponse<xs4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $status;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(yt4 yt4, String str, qn7 qn7) {
            super(1, qn7);
            this.this$0 = yt4;
            this.$status = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new f(this.this$0, this.$status, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<xs4>>> qn7) {
            return ((f) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f4368a;
                String str = this.$status;
                this.label = 1;
                Object friends = apiServiceV2.getFriends(str, this);
                return friends == d ? d : friends;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {17}, m = Constants.FIND)
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(yt4 yt4, qn7 qn7) {
            super(qn7);
            this.this$0 = yt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$find$response$1", f = "FriendRemoteDataSource.kt", l = {17}, m = "invokeSuspend")
    public static final class h extends ko7 implements rp7<qn7<? super q88<ApiResponse<xs4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $key;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(yt4 yt4, String str, qn7 qn7) {
            super(1, qn7);
            this.this$0 = yt4;
            this.$key = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new h(this.this$0, this.$key, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<xs4>>> qn7) {
            return ((h) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f4368a;
                String str = this.$key;
                this.label = 1;
                Object findFriend = apiServiceV2.findFriend(str, this);
                return findFriend == d ? d : findFriend;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {145}, m = "respondFriendRequest")
    public static final class i extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(yt4 yt4, qn7 qn7) {
            super(qn7);
            this.this$0 = yt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f(false, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$respondFriendRequest$response$1", f = "FriendRemoteDataSource.kt", l = {145}, m = "invokeSuspend")
    public static final class j extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(yt4 yt4, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = yt4;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new j(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((j) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f4368a;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object respondFriendRequest = apiServiceV2.respondFriendRequest(gj4, this);
                return respondFriendRequest == d ? d : respondFriendRequest;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {69}, m = "sendRequest")
    public static final class k extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(yt4 yt4, qn7 qn7) {
            super(qn7);
            this.this$0 = yt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$sendRequest$response$1", f = "FriendRemoteDataSource.kt", l = {69}, m = "invokeSuspend")
    public static final class l extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(yt4 yt4, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = yt4;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new l(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((l) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f4368a;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object sendRequest = apiServiceV2.sendRequest(gj4, this);
                return sendRequest == d ? d : sendRequest;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {84}, m = "unFriend")
    public static final class m extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(yt4 yt4, qn7 qn7) {
            super(qn7);
            this.this$0 = yt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$unFriend$response$1", f = "FriendRemoteDataSource.kt", l = {84}, m = "invokeSuspend")
    public static final class n extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(yt4 yt4, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = yt4;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new n(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((n) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f4368a;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object unFriend = apiServiceV2.unFriend(gj4, this);
                return unFriend == d ? d : unFriend;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {129}, m = "unblock")
    public static final class o extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(yt4 yt4, qn7 qn7) {
            super(qn7);
            this.this$0 = yt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$unblock$response$1", f = "FriendRemoteDataSource.kt", l = {129}, m = "invokeSuspend")
    public static final class p extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ yt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(yt4 yt4, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = yt4;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new p(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((p) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f4368a;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object unblock = apiServiceV2.unblock(gj4, this);
                return unblock == d ? d : unblock;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public yt4(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        this.f4368a = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(java.lang.String r9, com.fossil.qn7<? super com.fossil.iq5<java.lang.Void>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.fossil.yt4.a
            if (r0 == 0) goto L_0x003f
            r0 = r10
            com.fossil.yt4$a r0 = (com.fossil.yt4.a) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003f
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x004e
            if (r0 != r5) goto L_0x0046
            java.lang.Object r0 = r1.L$2
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.yt4 r0 = (com.fossil.yt4) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x0031:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0070
            com.fossil.kq5 r0 = new com.fossil.kq5
            r1 = 0
            r2 = 2
            r0.<init>(r4, r1, r2, r4)
        L_0x003e:
            return r0
        L_0x003f:
            com.fossil.yt4$a r0 = new com.fossil.yt4$a
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0046:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004e:
            com.fossil.el7.b(r2)
            com.fossil.gj4 r0 = new com.fossil.gj4
            r0.<init>()
            java.lang.String r2 = "profileID"
            r0.n(r2, r9)
            com.fossil.yt4$b r2 = new com.fossil.yt4$b
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r0
            r1.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r2, r1)
            if (r0 != r3) goto L_0x0031
            r0 = r3
            goto L_0x003e
        L_0x0070:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x009e
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.portfolio.platform.data.model.ServerError r0 = r3.c()
            if (r0 == 0) goto L_0x0099
            java.lang.Integer r0 = r0.getCode()
            if (r0 == 0) goto L_0x0099
            int r1 = r0.intValue()
        L_0x0087:
            com.fossil.hq5 r0 = new com.fossil.hq5
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x003e
        L_0x0099:
            int r1 = r3.a()
            goto L_0x0087
        L_0x009e:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yt4.b(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(java.lang.String r9, com.fossil.qn7<? super com.fossil.iq5<java.lang.Void>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.fossil.yt4.c
            if (r0 == 0) goto L_0x003f
            r0 = r10
            com.fossil.yt4$c r0 = (com.fossil.yt4.c) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003f
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x004e
            if (r0 != r5) goto L_0x0046
            java.lang.Object r0 = r1.L$2
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.yt4 r0 = (com.fossil.yt4) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x0031:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0070
            com.fossil.kq5 r0 = new com.fossil.kq5
            r1 = 0
            r2 = 2
            r0.<init>(r4, r1, r2, r4)
        L_0x003e:
            return r0
        L_0x003f:
            com.fossil.yt4$c r0 = new com.fossil.yt4$c
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0046:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004e:
            com.fossil.el7.b(r2)
            com.fossil.gj4 r0 = new com.fossil.gj4
            r0.<init>()
            java.lang.String r2 = "friendId"
            r0.n(r2, r9)
            com.fossil.yt4$d r2 = new com.fossil.yt4$d
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r0
            r1.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r2, r1)
            if (r0 != r3) goto L_0x0031
            r0 = r3
            goto L_0x003e
        L_0x0070:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x008d
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x003e
        L_0x008d:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yt4.c(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(java.lang.String r9, com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.xs4>>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.fossil.yt4.e
            if (r0 == 0) goto L_0x0044
            r0 = r10
            com.fossil.yt4$e r0 = (com.fossil.yt4.e) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0044
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0053
            if (r0 != r5) goto L_0x004b
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.yt4 r0 = (com.fossil.yt4) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x002d:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0069
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            com.fossil.kq5 r1 = new com.fossil.kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x0043:
            return r0
        L_0x0044:
            com.fossil.yt4$e r0 = new com.fossil.yt4$e
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x004b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0053:
            com.fossil.el7.b(r2)
            com.fossil.yt4$f r0 = new com.fossil.yt4$f
            r0.<init>(r8, r9, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x0043
        L_0x0069:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x0086
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0043
        L_0x0086:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yt4.d(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(java.lang.String r9, com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.xs4>>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.fossil.yt4.g
            if (r0 == 0) goto L_0x0044
            r0 = r10
            com.fossil.yt4$g r0 = (com.fossil.yt4.g) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0044
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0053
            if (r0 != r5) goto L_0x004b
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.yt4 r0 = (com.fossil.yt4) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x002d:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0069
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            com.fossil.kq5 r1 = new com.fossil.kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x0043:
            return r0
        L_0x0044:
            com.fossil.yt4$g r0 = new com.fossil.yt4$g
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x004b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0053:
            com.fossil.el7.b(r2)
            com.fossil.yt4$h r0 = new com.fossil.yt4$h
            r0.<init>(r8, r9, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x0043
        L_0x0069:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x0086
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0043
        L_0x0086:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yt4.e(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object f(boolean r9, java.lang.String r10, com.fossil.qn7<? super com.fossil.iq5<com.fossil.xs4>> r11) {
        /*
        // Method dump skipped, instructions count: 219
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yt4.f(boolean, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g(java.lang.String r9, java.lang.String r10, com.fossil.qn7<? super com.fossil.iq5<java.lang.Void>> r11) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r11 instanceof com.fossil.yt4.k
            if (r0 == 0) goto L_0x0043
            r0 = r11
            com.fossil.yt4$k r0 = (com.fossil.yt4.k) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0043
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0052
            if (r0 != r5) goto L_0x004a
            java.lang.Object r0 = r1.L$3
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            java.lang.Object r0 = r1.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.yt4 r0 = (com.fossil.yt4) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x0035:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x007b
            com.fossil.kq5 r0 = new com.fossil.kq5
            r1 = 0
            r2 = 2
            r0.<init>(r4, r1, r2, r4)
        L_0x0042:
            return r0
        L_0x0043:
            com.fossil.yt4$k r0 = new com.fossil.yt4$k
            r0.<init>(r8, r11)
            r1 = r0
            goto L_0x0015
        L_0x004a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0052:
            com.fossil.el7.b(r2)
            com.fossil.gj4 r0 = new com.fossil.gj4
            r0.<init>()
            java.lang.String r2 = "friendId"
            r0.n(r2, r9)
            java.lang.String r2 = "socialId"
            r0.n(r2, r10)
            com.fossil.yt4$l r2 = new com.fossil.yt4$l
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r10
            r1.L$3 = r0
            r1.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r2, r1)
            if (r0 != r3) goto L_0x0035
            r0 = r3
            goto L_0x0042
        L_0x007b:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00a9
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.portfolio.platform.data.model.ServerError r0 = r3.c()
            if (r0 == 0) goto L_0x00a4
            java.lang.Integer r0 = r0.getCode()
            if (r0 == 0) goto L_0x00a4
            int r1 = r0.intValue()
        L_0x0092:
            com.fossil.hq5 r0 = new com.fossil.hq5
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0042
        L_0x00a4:
            int r1 = r3.a()
            goto L_0x0092
        L_0x00a9:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yt4.g(java.lang.String, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object h(java.lang.String r9, com.fossil.qn7<? super com.fossil.iq5<java.lang.Void>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.fossil.yt4.m
            if (r0 == 0) goto L_0x003f
            r0 = r10
            com.fossil.yt4$m r0 = (com.fossil.yt4.m) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003f
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x004e
            if (r0 != r5) goto L_0x0046
            java.lang.Object r0 = r1.L$2
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.yt4 r0 = (com.fossil.yt4) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x0031:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0070
            com.fossil.kq5 r0 = new com.fossil.kq5
            r1 = 0
            r2 = 2
            r0.<init>(r4, r1, r2, r4)
        L_0x003e:
            return r0
        L_0x003f:
            com.fossil.yt4$m r0 = new com.fossil.yt4$m
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0046:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004e:
            com.fossil.el7.b(r2)
            com.fossil.gj4 r0 = new com.fossil.gj4
            r0.<init>()
            java.lang.String r2 = "friendId"
            r0.n(r2, r9)
            com.fossil.yt4$n r2 = new com.fossil.yt4$n
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r0
            r1.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r2, r1)
            if (r0 != r3) goto L_0x0031
            r0 = r3
            goto L_0x003e
        L_0x0070:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x008d
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x003e
        L_0x008d:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yt4.h(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object i(java.lang.String r9, com.fossil.qn7<? super com.fossil.iq5<com.fossil.os4>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.fossil.yt4.o
            if (r0 == 0) goto L_0x004e
            r0 = r10
            com.fossil.yt4$o r0 = (com.fossil.yt4.o) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x004e
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x005d
            if (r0 != r5) goto L_0x0055
            java.lang.Object r0 = r1.L$2
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.yt4 r0 = (com.fossil.yt4) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x0031:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0090
            com.fossil.hz4 r1 = com.fossil.hz4.f1561a
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r1 = r0.a()
            com.fossil.gj4 r1 = (com.fossil.gj4) r1
            if (r1 != 0) goto L_0x007f
        L_0x0043:
            com.fossil.kq5 r1 = new com.fossil.kq5
            boolean r0 = r0.b()
            r1.<init>(r4, r0)
            r0 = r1
        L_0x004d:
            return r0
        L_0x004e:
            com.fossil.yt4$o r0 = new com.fossil.yt4$o
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0055:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005d:
            com.fossil.el7.b(r2)
            com.fossil.gj4 r0 = new com.fossil.gj4
            r0.<init>()
            java.lang.String r2 = "profileID"
            r0.n(r2, r9)
            com.fossil.yt4$p r2 = new com.fossil.yt4$p
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r0
            r1.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r2, r1)
            if (r0 != r3) goto L_0x0031
            r0 = r3
            goto L_0x004d
        L_0x007f:
            com.google.gson.Gson r2 = new com.google.gson.Gson     // Catch:{ mj4 -> 0x008b }
            r2.<init>()     // Catch:{ mj4 -> 0x008b }
            java.lang.Class<com.fossil.os4> r3 = com.fossil.os4.class
            java.lang.Object r4 = r2.g(r1, r3)     // Catch:{ mj4 -> 0x008b }
            goto L_0x0043
        L_0x008b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0043
        L_0x0090:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00be
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.portfolio.platform.data.model.ServerError r0 = r3.c()
            if (r0 == 0) goto L_0x00b9
            java.lang.Integer r0 = r0.getCode()
            if (r0 == 0) goto L_0x00b9
            int r1 = r0.intValue()
        L_0x00a7:
            com.fossil.hq5 r0 = new com.fossil.hq5
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x004d
        L_0x00b9:
            int r1 = r3.a()
            goto L_0x00a7
        L_0x00be:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yt4.i(java.lang.String, com.fossil.qn7):java.lang.Object");
    }
}
