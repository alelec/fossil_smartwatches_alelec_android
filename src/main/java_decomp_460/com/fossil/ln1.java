package com.fossil;

import com.fossil.cn1;
import com.fossil.en1;
import com.fossil.hn1;
import com.fossil.nm1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ln1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ArrayList<ym1> f2222a; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<ym1, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ ym1 b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ym1 ym1) {
            super(1);
            this.b = ym1;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public Boolean invoke(ym1 ym1) {
            return Boolean.valueOf(ym1.getKey() == this.b.getKey());
        }
    }

    @DexIgnore
    public final void a(ym1 ym1) {
        mm7.w(this.f2222a, new a(ym1));
        this.f2222a.add(ym1);
    }

    @DexIgnore
    public final ym1[] b() {
        Object[] array = this.f2222a.toArray(new ym1[0]);
        if (array != null) {
            return (ym1[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final ln1 c(kn1[] kn1Arr) throws IllegalArgumentException {
        a(new jn1(kn1Arr));
        return this;
    }

    @DexIgnore
    public final ln1 d(byte b, nm1.a aVar, short s, short s2, nm1.b bVar) throws IllegalArgumentException {
        a(new nm1(b, aVar, s, s2, bVar));
        return this;
    }

    @DexIgnore
    public final ln1 e(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        a(new pm1(i, i2, i3, i4));
        return this;
    }

    @DexIgnore
    public final ln1 f(int i) throws IllegalArgumentException {
        a(new qm1(i));
        return this;
    }

    @DexIgnore
    public final ln1 g(long j) throws IllegalArgumentException {
        a(new rm1(j));
        return this;
    }

    @DexIgnore
    public final ln1 h(long j) throws IllegalArgumentException {
        a(new sm1(j));
        return this;
    }

    @DexIgnore
    public final ln1 i(long j) throws IllegalArgumentException {
        a(new tm1(j));
        return this;
    }

    @DexIgnore
    public final ln1 j(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        a(new um1(i, i2, i3, i4));
        return this;
    }

    @DexIgnore
    public final ln1 k(long j) throws IllegalArgumentException {
        a(new vm1(j));
        return this;
    }

    @DexIgnore
    public final ln1 l(long j) throws IllegalArgumentException {
        a(new wm1(j));
        return this;
    }

    @DexIgnore
    public final ln1 m(int i) throws IllegalArgumentException {
        a(new xm1(i));
        return this;
    }

    @DexIgnore
    public final ln1 n(rn1 rn1, mn1 mn1, pn1 pn1, sn1 sn1, on1 on1) {
        a(new an1(rn1, mn1, pn1, sn1, on1));
        return this;
    }

    @DexIgnore
    public final ln1 o(cn1.a aVar) {
        a(new cn1(aVar));
        return this;
    }

    @DexIgnore
    public final ln1 p(byte b, byte b2, byte b3, byte b4, short s, en1.a aVar) throws IllegalArgumentException {
        a(new en1(b, b2, b3, b4, s, aVar));
        return this;
    }

    @DexIgnore
    public final ln1 q(short s) throws IllegalArgumentException {
        a(new fn1(s));
        return this;
    }

    @DexIgnore
    public final ln1 r(long j, short s, short s2) throws IllegalArgumentException {
        a(new gn1(j, s, s2));
        return this;
    }

    @DexIgnore
    public final ln1 s(hn1.a aVar) {
        a(new hn1(aVar));
        return this;
    }
}
