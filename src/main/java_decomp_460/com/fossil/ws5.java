package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ws5 extends us5 {
    @DexIgnore
    public boolean d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ws5(String str, String str2, boolean z) {
        super(str, str2);
        pq7.c(str, "tagName");
        pq7.c(str2, "title");
        this.d = z;
    }

    @DexIgnore
    public final boolean f() {
        return this.d;
    }

    @DexIgnore
    public final void g(boolean z) {
        this.d = z;
    }
}
