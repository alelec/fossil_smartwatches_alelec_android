package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f07 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ls5 f1023a;
    @DexIgnore
    public /* final */ zz6 b;

    @DexIgnore
    public f07(ls5 ls5, zz6 zz6) {
        pq7.c(ls5, "mContext");
        pq7.c(zz6, "mView");
        this.f1023a = ls5;
        this.b = zz6;
    }

    @DexIgnore
    public final zz6 a() {
        return this.b;
    }
}
