package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qq3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public long f3006a;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ ng3 c; // = new tq3(this, this.d.f1780a);
    @DexIgnore
    public /* final */ /* synthetic */ jq3 d;

    @DexIgnore
    public qq3(jq3 jq3) {
        this.d = jq3;
        long c2 = jq3.zzm().c();
        this.f3006a = c2;
        this.b = c2;
    }

    @DexIgnore
    public final void a() {
        this.c.e();
        this.f3006a = 0;
        this.b = 0;
    }

    @DexIgnore
    public final void b(long j) {
        this.d.h();
        this.c.e();
        this.f3006a = j;
        this.b = j;
    }

    @DexIgnore
    public final boolean d(boolean z, boolean z2, long j) {
        this.d.h();
        this.d.x();
        if (!l63.a() || !this.d.m().s(xg3.A0)) {
            j = this.d.zzm().c();
        }
        if (!w63.a() || !this.d.m().s(xg3.w0) || this.d.f1780a.o()) {
            this.d.l().u.b(this.d.zzm().b());
        }
        long j2 = j - this.f3006a;
        if (z || j2 >= 1000) {
            if (this.d.m().s(xg3.U) && !z2) {
                j2 = (!x63.a() || !this.d.m().s(xg3.W) || !l63.a() || !this.d.m().s(xg3.A0)) ? e() : g(j);
            }
            this.d.d().N().b("Recording user engagement, ms", Long.valueOf(j2));
            Bundle bundle = new Bundle();
            bundle.putLong("_et", j2);
            ap3.L(this.d.s().D(!this.d.m().K().booleanValue()), bundle, true);
            if (this.d.m().s(xg3.U) && !this.d.m().s(xg3.V) && z2) {
                bundle.putLong("_fr", 1);
            }
            if (!this.d.m().s(xg3.V) || !z2) {
                this.d.p().Q("auto", "_e", bundle);
            }
            this.f3006a = j;
            this.c.e();
            this.c.c(3600000);
            return true;
        }
        this.d.d().N().b("Screen exposed for less than 1000 ms. Event not sent. time", Long.valueOf(j2));
        return false;
    }

    @DexIgnore
    public final long e() {
        long c2 = this.d.zzm().c();
        long j = this.b;
        this.b = c2;
        return c2 - j;
    }

    @DexIgnore
    public final void f(long j) {
        this.c.e();
    }

    @DexIgnore
    public final long g(long j) {
        long j2 = this.b;
        this.b = j;
        return j - j2;
    }

    @DexIgnore
    public final void h() {
        this.d.h();
        d(false, false, this.d.zzm().c());
        this.d.o().v(this.d.zzm().c());
    }
}
