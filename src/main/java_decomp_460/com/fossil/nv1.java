package com.fossil;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.imagefilters.EInkImageFactory;
import com.fossil.imagefilters.Format;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nv1 extends mv1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ vb0 e;
    @DexIgnore
    public hw1 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<nv1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public nv1 createFromParcel(Parcel parcel) {
            return new nv1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public nv1[] newArray(int i) {
            return new nv1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ nv1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.e = vb0.IMAGE;
        Parcelable readParcelable = parcel.readParcelable(hw1.class.getClassLoader());
        if (readParcelable != null) {
            this.f = (hw1) readParcelable;
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public nv1(String str, byte[] bArr) {
        super(str, new jv1(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES), new kv1(1.0f, 1.0f));
        this.e = vb0.IMAGE;
        this.f = new hw1(str, bArr);
    }

    @DexIgnore
    public nv1(JSONObject jSONObject, cc0[] cc0Arr, String str) throws IllegalArgumentException {
        super(jSONObject, str);
        cc0 cc0;
        this.e = vb0.IMAGE;
        String optString = jSONObject.optString("name");
        int length = cc0Arr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                cc0 = null;
                break;
            }
            cc0 = cc0Arr[i];
            if (pq7.a(optString, cc0.b)) {
                break;
            }
            i++;
        }
        byte[] bArr = cc0 != null ? cc0.c : null;
        if (bArr != null) {
            Bitmap decode = EInkImageFactory.decode(bArr, Format.RAW);
            pq7.b(decode, "EInkImageFactory.decode(\u2026iceImageData, Format.RAW)");
            this.f = new hw1(getName(), cy1.b(decode, null, 1, null));
            decode.recycle();
            return;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    @DexIgnore
    public nv1(byte[] bArr) {
        this(g80.e(0, 1), bArr);
    }

    @DexIgnore
    public final cc0 a(String str) {
        return this.f.a(new lv1(240, 240), str);
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public vb0 a() {
        return this.e;
    }

    @DexIgnore
    public final JSONObject b(String str) {
        JSONObject put = d().put("name", str);
        pq7.b(put, "getUIScript()\n          \u2026onstant.NAME, customName)");
        return put;
    }

    @DexIgnore
    @Override // java.lang.Object, com.fossil.mv1, com.fossil.mv1
    public nv1 clone() {
        String name = getName();
        byte[] bitmapImageData = this.f.getBitmapImageData();
        byte[] copyOf = Arrays.copyOf(bitmapImageData, bitmapImageData.length);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new nv1(name, copyOf);
    }

    @DexIgnore
    public final hw1 e() {
        return this.f;
    }

    @DexIgnore
    public nv1 f() {
        return this;
    }

    @DexIgnore
    public nv1 g() {
        return this;
    }

    @DexIgnore
    public final byte[] getBitmapImageData() {
        return this.f.getBitmapImageData();
    }

    @DexIgnore
    public nv1 h() {
        return this;
    }

    @DexIgnore
    public nv1 i() {
        return this;
    }

    @DexIgnore
    public final nv1 setImageData(byte[] bArr) {
        this.f = new hw1(getName(), bArr);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public /* bridge */ /* synthetic */ mv1 setScaledHeight(float f2) {
        return f();
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public nv1 setScaledPosition(jv1 jv1) {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public nv1 setScaledSize(kv1 kv1) {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public /* bridge */ /* synthetic */ mv1 setScaledWidth(float f2) {
        return g();
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public /* bridge */ /* synthetic */ mv1 setScaledX(float f2) {
        return h();
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public /* bridge */ /* synthetic */ mv1 setScaledY(float f2) {
        return i();
    }
}
