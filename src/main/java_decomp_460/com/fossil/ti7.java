package com.fossil;

import android.content.Context;
import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ti7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;

    @DexIgnore
    public ti7(Context context) {
        this.b = context;
    }

    @DexIgnore
    public final void run() {
        tg7.a(ig7.r).l();
        ei7.e(this.b, true);
        gh7.b(this.b);
        qi7.f(this.b);
        Thread.UncaughtExceptionHandler unused = ig7.n = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new ah7());
        if (fg7.I() == gg7.APP_LAUNCH) {
            ig7.o(this.b, -1);
        }
        if (fg7.K()) {
            ig7.m.b("Init MTA StatService success.");
        }
    }
}
