package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wc1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ wc1 f3916a; // = new a();
    @DexIgnore
    public static /* final */ wc1 b; // = new b();
    @DexIgnore
    public static /* final */ wc1 c; // = new c();
    @DexIgnore
    public static /* final */ wc1 d; // = new d();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends wc1 {
        @DexIgnore
        @Override // com.fossil.wc1
        public boolean a() {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.wc1
        public boolean b() {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.wc1
        public boolean c(gb1 gb1) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.wc1
        public boolean d(boolean z, gb1 gb1, ib1 ib1) {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends wc1 {
        @DexIgnore
        @Override // com.fossil.wc1
        public boolean a() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.wc1
        public boolean b() {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.wc1
        public boolean c(gb1 gb1) {
            return (gb1 == gb1.DATA_DISK_CACHE || gb1 == gb1.MEMORY_CACHE) ? false : true;
        }

        @DexIgnore
        @Override // com.fossil.wc1
        public boolean d(boolean z, gb1 gb1, ib1 ib1) {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends wc1 {
        @DexIgnore
        @Override // com.fossil.wc1
        public boolean a() {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.wc1
        public boolean b() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.wc1
        public boolean c(gb1 gb1) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.wc1
        public boolean d(boolean z, gb1 gb1, ib1 ib1) {
            return (gb1 == gb1.RESOURCE_DISK_CACHE || gb1 == gb1.MEMORY_CACHE) ? false : true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends wc1 {
        @DexIgnore
        @Override // com.fossil.wc1
        public boolean a() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.wc1
        public boolean b() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.wc1
        public boolean c(gb1 gb1) {
            return gb1 == gb1.REMOTE;
        }

        @DexIgnore
        @Override // com.fossil.wc1
        public boolean d(boolean z, gb1 gb1, ib1 ib1) {
            return ((z && gb1 == gb1.DATA_DISK_CACHE) || gb1 == gb1.LOCAL) && ib1 == ib1.TRANSFORMED;
        }
    }

    @DexIgnore
    public abstract boolean a();

    @DexIgnore
    public abstract boolean b();

    @DexIgnore
    public abstract boolean c(gb1 gb1);

    @DexIgnore
    public abstract boolean d(boolean z, gb1 gb1, ib1 ib1);
}
