package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ix1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mb0 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ lb0 CREATOR; // = new lb0(null);
    @DexIgnore
    public /* final */ bv1 b;
    @DexIgnore
    public /* final */ ry1 c;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public mb0(android.os.Parcel r4) {
        /*
            r3 = this;
            r2 = 0
            java.lang.Class<com.fossil.bv1> r0 = com.fossil.bv1.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r4.readParcelable(r0)
            if (r0 == 0) goto L_0x0025
            com.fossil.bv1 r0 = (com.fossil.bv1) r0
            java.lang.Class<com.fossil.ry1> r1 = com.fossil.ry1.class
            java.lang.ClassLoader r1 = r1.getClassLoader()
            android.os.Parcelable r1 = r4.readParcelable(r1)
            if (r1 == 0) goto L_0x0021
            com.fossil.ry1 r1 = (com.fossil.ry1) r1
            r3.<init>(r0, r1)
            return
        L_0x0021:
            com.fossil.pq7.i()
            throw r2
        L_0x0025:
            com.fossil.pq7.i()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mb0.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public mb0(bv1 bv1, ry1 ry1) {
        this.b = bv1;
        this.c = ry1;
    }

    @DexIgnore
    public final byte[] a() {
        byte[] k = dm7.k(this.b.a(), 0, 8);
        byte[] bArr = new byte[0];
        for (T t : b()) {
            ByteBuffer order = ByteBuffer.allocate(t.a().length + 2).order(ByteOrder.LITTLE_ENDIAN);
            order.putShort((short) ((t.a().length << 7) | t.b.b));
            order.put(t.a());
            byte[] array = order.array();
            pq7.b(array, "dataBuffer.array()");
            bArr = dy1.a(bArr, array);
        }
        ByteBuffer order2 = ByteBuffer.allocate(bArr.length + 3).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order2, "ByteBuffer.allocate(3 + \u2026(ByteOrder.LITTLE_ENDIAN)");
        order2.put((byte) 255);
        order2.putShort((short) (((short) bArr.length) + 3));
        order2.put(bArr);
        byte[] array2 = order2.array();
        pq7.b(array2, "byteBuffer.array()");
        byte[] a2 = dy1.a(dy1.a(dy1.a(new byte[0], new byte[]{(byte) this.c.getMajor(), (byte) this.c.getMinor(), x90.d.b}), k), array2);
        int b2 = (int) ix1.f1688a.b(a2, ix1.a.CRC32);
        ByteBuffer order3 = ByteBuffer.allocate(a2.length + 4).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order3, "ByteBuffer.allocate(resp\u2026(ByteOrder.LITTLE_ENDIAN)");
        order3.put(a2);
        order3.putInt(b2);
        byte[] array3 = order3.array();
        pq7.b(array3, "byteBuffer.array()");
        return array3;
    }

    @DexIgnore
    public abstract List<va0> b();

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            mb0 mb0 = (mb0) obj;
            if (!pq7.a(this.b, mb0.b)) {
                return false;
            }
            return !(pq7.a(this.c, mb0.c) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppResponse");
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(new JSONObject(), jd0.v3, this.b.toJSONObject()), jd0.Z3, this.c.toJSONObject());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(getClass().getName());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }
}
