package com.fossil;

import java.util.AbstractMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ry2 extends sx2<Map.Entry<K, V>> {
    @DexIgnore
    public /* final */ /* synthetic */ sy2 zza;

    @DexIgnore
    public ry2(sy2 sy2) {
        this.zza = sy2;
    }

    @DexIgnore
    @Override // java.util.List
    public final /* synthetic */ Object get(int i) {
        sw2.a(i, this.zza.f);
        int i2 = i * 2;
        return new AbstractMap.SimpleImmutableEntry(this.zza.e[i2], this.zza.e[i2 + 1]);
    }

    @DexIgnore
    public final int size() {
        return this.zza.f;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final boolean zzh() {
        return true;
    }
}
