package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z26 extends pv5 implements y26 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public x26 g;
    @DexIgnore
    public g37<n95> h;
    @DexIgnore
    public r26 i;
    @DexIgnore
    public i36 j;
    @DexIgnore
    public s26 k;
    @DexIgnore
    public j36 l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return z26.s;
        }

        @DexIgnore
        public final z26 b() {
            return new z26();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z26 b;

        @DexIgnore
        public b(z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            z26.L6(this.b).r();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z26 b;

        @DexIgnore
        public c(z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            z26.L6(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z26 b;

        @DexIgnore
        public d(z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            z26.L6(this.b).p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z26 b;

        @DexIgnore
        public e(z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            z26.L6(this.b).q();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z26 b;

        @DexIgnore
        public f(z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            z26.L6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z26 b;

        @DexIgnore
        public g(z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            p47.l(view);
            r26 r26 = this.b.i;
            if (r26 != null) {
                r26.C6(0);
            }
            r26 r262 = this.b.i;
            if (r262 != null) {
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                r262.show(childFragmentManager, r26.u.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z26 b;

        @DexIgnore
        public h(z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            p47.l(view);
            r26 r26 = this.b.i;
            if (r26 != null) {
                r26.C6(1);
            }
            r26 r262 = this.b.i;
            if (r262 != null) {
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                r262.show(childFragmentManager, r26.u.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z26 b;

        @DexIgnore
        public i(z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            p47.l(view);
            i36 i36 = this.b.j;
            if (i36 != null) {
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                i36.show(childFragmentManager, i36.u.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z26 b;

        @DexIgnore
        public j(z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            z26.L6(this.b).r();
        }
    }

    /*
    static {
        String simpleName = z26.class.getSimpleName();
        pq7.b(simpleName, "NotificationWatchReminde\u2026nt::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ x26 L6(z26 z26) {
        x26 x26 = z26.g;
        if (x26 != null) {
            return x26;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        x26 x26 = this.g;
        if (x26 != null) {
            x26.r();
            return true;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    /* renamed from: O6 */
    public void M5(x26 x26) {
        pq7.c(x26, "presenter");
        this.g = x26;
    }

    @DexIgnore
    @Override // com.fossil.y26
    public void S1(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        g37<n95> g37 = this.h;
        if (g37 != null) {
            n95 a2 = g37.a();
            if (a2 != null && (flexibleSwitchCompat = a2.T) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y26
    public void Y(boolean z) {
        g37<n95> g37 = this.h;
        if (g37 != null) {
            n95 a2 = g37.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.v;
                if (flexibleButton != null) {
                    flexibleButton.setEnabled(z);
                }
                if (z) {
                    FlexibleButton flexibleButton2 = a2.v;
                    if (flexibleButton2 != null) {
                        flexibleButton2.d("flexible_button_primary");
                        return;
                    }
                    return;
                }
                FlexibleButton flexibleButton3 = a2.v;
                if (flexibleButton3 != null) {
                    flexibleButton3.d("flexible_button_disabled");
                    return;
                }
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y26
    public void Y5(SpannableString spannableString) {
        FlexibleTextView flexibleTextView;
        pq7.c(spannableString, LogBuilder.KEY_TIME);
        g37<n95> g37 = this.h;
        if (g37 != null) {
            n95 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.A) != null) {
                flexibleTextView.setText(spannableString);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y26
    public void b6(String str) {
        FlexibleTextView flexibleTextView;
        pq7.c(str, LogBuilder.KEY_TIME);
        g37<n95> g37 = this.h;
        if (g37 != null) {
            n95 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.H) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y26
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.y26
    public void n4(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        g37<n95> g37 = this.h;
        if (g37 != null) {
            n95 a2 = g37.a();
            if (a2 != null && (flexibleSwitchCompat = a2.U) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        n95 n95 = (n95) aq0.f(layoutInflater, 2131558597, viewGroup, false, A6());
        r26 r26 = (r26) getChildFragmentManager().Z(r26.u.a());
        this.i = r26;
        if (r26 == null) {
            this.i = r26.u.b();
        }
        i36 i36 = (i36) getChildFragmentManager().Z(i36.u.a());
        this.j = i36;
        if (i36 == null) {
            this.j = i36.u.b();
        }
        String d2 = qn5.l.a().d("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(d2)) {
            int parseColor = Color.parseColor(d2);
            n95.V.setBackgroundColor(parseColor);
            n95.W.setBackgroundColor(parseColor);
            n95.X.setBackgroundColor(parseColor);
        }
        FlexibleTextView flexibleTextView = n95.K;
        pq7.b(flexibleTextView, "binding.ftvTitle");
        flexibleTextView.setSelected(true);
        n95.L.setOnClickListener(new b(this));
        n95.S.setOnClickListener(new c(this));
        n95.T.setOnClickListener(new d(this));
        n95.U.setOnClickListener(new e(this));
        n95.R.setOnClickListener(new f(this));
        n95.N.setOnClickListener(new g(this));
        n95.M.setOnClickListener(new h(this));
        n95.O.setOnClickListener(new i(this));
        n95.v.setOnClickListener(new j(this));
        this.h = new g37<>(this, n95);
        ro4 M = PortfolioApp.h0.c().M();
        r26 r262 = this.i;
        if (r262 != null) {
            i36 i362 = this.j;
            if (i362 != null) {
                M.j1(new n36(r262, i362)).a(this);
                E6("reminder_view");
                pq7.b(n95, "binding");
                return n95.n();
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimeContract.View");
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimeContract.View");
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        x26 x26 = this.g;
        if (x26 != null) {
            x26.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
            }
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        x26 x26 = this.g;
        if (x26 != null) {
            x26.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<n95> g37 = this.h;
        if (g37 != null) {
            n95 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.t;
                pq7.b(constraintLayout, "clRemindersContainer");
                constraintLayout.setVisibility(8);
                FlexibleButton flexibleButton = a2.v;
                pq7.b(flexibleButton, "fbSave");
                flexibleButton.setVisibility(8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y26
    public void s1(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        g37<n95> g37 = this.h;
        if (g37 != null) {
            n95 a2 = g37.a();
            if (a2 != null && (flexibleSwitchCompat = a2.R) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y26
    public void t5(SpannableString spannableString) {
        FlexibleTextView flexibleTextView;
        pq7.c(spannableString, LogBuilder.KEY_TIME);
        g37<n95> g37 = this.h;
        if (g37 != null) {
            n95 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.C) != null) {
                flexibleTextView.setText(spannableString);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.y26
    public void w4(boolean z) {
        g37<n95> g37 = this.h;
        if (g37 != null) {
            n95 a2 = g37.a();
            if (a2 != null) {
                FlexibleSwitchCompat flexibleSwitchCompat = a2.S;
                if (flexibleSwitchCompat != null) {
                    flexibleSwitchCompat.setChecked(z);
                }
                ConstraintLayout constraintLayout = a2.r;
                if (constraintLayout != null) {
                    constraintLayout.setAlpha(z ? 1.0f : 0.5f);
                }
                ConstraintLayout constraintLayout2 = a2.r;
                pq7.b(constraintLayout2, "clInactivityNudgeContainer");
                int childCount = constraintLayout2.getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    View childAt = a2.r.getChildAt(i2);
                    pq7.b(childAt, "child");
                    childAt.setEnabled(z);
                }
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }
}
