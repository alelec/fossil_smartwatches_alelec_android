package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class fg2 implements Callable {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ boolean f1124a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ eg2 c;

    @DexIgnore
    public fg2(boolean z, String str, eg2 eg2) {
        this.f1124a = z;
        this.b = str;
        this.c = eg2;
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return dg2.b(this.f1124a, this.b, this.c);
    }
}
