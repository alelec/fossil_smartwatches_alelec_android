package com.fossil;

import android.graphics.Bitmap;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ag1 implements qb1<ByteBuffer, Bitmap> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ gg1 f263a;

    @DexIgnore
    public ag1(gg1 gg1) {
        this.f263a = gg1;
    }

    @DexIgnore
    /* renamed from: c */
    public id1<Bitmap> b(ByteBuffer byteBuffer, int i, int i2, ob1 ob1) throws IOException {
        return this.f263a.f(zj1.f(byteBuffer), i, i2, ob1);
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(ByteBuffer byteBuffer, ob1 ob1) {
        return this.f263a.q(byteBuffer);
    }
}
