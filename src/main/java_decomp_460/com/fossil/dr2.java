package com.fossil;

import android.location.Location;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dr2 extends mb3 {
    @DexIgnore
    public /* final */ p72<ga3> b;

    @DexIgnore
    public dr2(p72<ga3> p72) {
        this.b = p72;
    }

    @DexIgnore
    public final void i() {
        synchronized (this) {
            this.b.a();
        }
    }

    @DexIgnore
    @Override // com.fossil.lb3
    public final void onLocationChanged(Location location) {
        synchronized (this) {
            this.b.c(new er2(this, location));
        }
    }
}
