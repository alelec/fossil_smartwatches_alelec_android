package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p53 implements q53 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f2781a;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f2781a = hw2.d("measurement.sdk.dynamite.allow_remote_dynamite2", false);
        hw2.d("measurement.collection.init_params_control_enabled", true);
        hw2.d("measurement.sdk.dynamite.use_dynamite3", true);
        hw2.b("measurement.id.sdk.dynamite.use_dynamite", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.q53
    public final boolean zza() {
        return f2781a.o().booleanValue();
    }
}
