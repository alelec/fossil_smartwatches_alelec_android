package com.fossil;

import com.fossil.fitness.WorkoutMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum gi5 {
    UNKNOWN("UNKNOWN"),
    INDOOR("INDOOR"),
    OUTDOOR("OUTDOOR");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final gi5 a(int i) {
            return i == WorkoutMode.OUTDOOR.ordinal() ? gi5.OUTDOOR : i == WorkoutMode.INDOOR.ordinal() ? gi5.INDOOR : gi5.INDOOR;
        }

        @DexIgnore
        public final WorkoutMode b(gi5 gi5) {
            if (gi5 != null) {
                int i = fi5.f1135a[gi5.ordinal()];
                if (i == 1) {
                    return WorkoutMode.OUTDOOR;
                }
                if (i == 2) {
                    return WorkoutMode.INDOOR;
                }
            }
            return WorkoutMode.INDOOR;
        }
    }

    @DexIgnore
    public gi5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        pq7.c(str, "<set-?>");
        this.mValue = str;
    }
}
