package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lw7 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ vz7 f2274a; // = new vz7("REMOVED_TASK");
    @DexIgnore
    public static /* final */ vz7 b; // = new vz7("CLOSED_EMPTY");

    @DexIgnore
    public static final long c(long j) {
        if (j <= 0) {
            return 0;
        }
        if (j >= 9223372036854L) {
            return Long.MAX_VALUE;
        }
        return 1000000 * j;
    }
}
