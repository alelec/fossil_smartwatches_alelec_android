package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class tm3 implements Runnable {
    @DexIgnore
    public /* final */ qm3 b;
    @DexIgnore
    public /* final */ or3 c;
    @DexIgnore
    public /* final */ Bundle d;

    @DexIgnore
    public tm3(qm3 qm3, or3 or3, Bundle bundle) {
        this.b = qm3;
        this.c = or3;
        this.d = bundle;
    }

    @DexIgnore
    public final void run() {
        this.b.i(this.c, this.d);
    }
}
