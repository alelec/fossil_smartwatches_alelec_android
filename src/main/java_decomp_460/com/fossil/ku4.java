package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.mu4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendActivity;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ku4 extends pv5 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public g37<b45> h;
    @DexIgnore
    public mu4 i;
    @DexIgnore
    public vs4 j;
    @DexIgnore
    public ts4 k;
    @DexIgnore
    public List<gs4> l; // = new ArrayList();
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ku4.s;
        }

        @DexIgnore
        public final ku4 b(vs4 vs4, ts4 ts4) {
            ku4 ku4 = new ku4();
            Bundle bundle = new Bundle();
            bundle.putParcelable("challenge_template_extra", vs4);
            bundle.putParcelable("challenge_draft_extra", ts4);
            ku4.setArguments(bundle);
            return ku4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ku4 b;

        @DexIgnore
        public b(ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.onBackPressed();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ku4 b;

        @DexIgnore
        public c(ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public final void onClick(View view) {
            ku4.Q6(this.b).J();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ku4 b;

        @DexIgnore
        public d(ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public final void onClick(View view) {
            ku4.Q6(this.b).F();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ku4 b;

        @DexIgnore
        public e(ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.Z6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ku4 b;

        @DexIgnore
        public f(ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            mu4 Q6 = ku4.Q6(this.b);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                Q6.I(wt7.u0(valueOf).toString());
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ku4 b;

        @DexIgnore
        public g(ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            mu4 Q6 = ku4.Q6(this.b);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                Q6.E(wt7.u0(valueOf).toString());
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ku4 b;

        @DexIgnore
        public h(ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ku4.Q6(this.b).G(charSequence);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ku4 b;

        @DexIgnore
        public i(ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ku4.Q6(this.b).H(charSequence);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ku4 b;

        @DexIgnore
        public j(ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ku4.Q6(this.b).L(charSequence);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements ls0<mu4.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku4 f2083a;

        @DexIgnore
        public k(ku4 ku4) {
            this.f2083a = ku4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(mu4.b bVar) {
            this.f2083a.W6(bVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements ls0<gl7<? extends Boolean, ? extends ServerError, ? extends ts4>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku4 f2084a;

        @DexIgnore
        public l(ku4 ku4) {
            this.f2084a = ku4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(gl7<Boolean, ? extends ServerError, ts4> gl7) {
            if (gl7.getFirst().booleanValue()) {
                ts4 third = gl7.getThird();
                if (third == null) {
                    FragmentActivity activity = this.f2084a.getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                }
                ku4 ku4 = this.f2084a;
                ku4.U6(ku4.V6(third));
                return;
            }
            ServerError serverError = (ServerError) gl7.getSecond();
            s37 s37 = s37.c;
            Integer code = serverError != null ? serverError.getCode() : null;
            String message = serverError != null ? serverError.getMessage() : null;
            FragmentManager childFragmentManager = this.f2084a.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.g(code, message, childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku4 f2085a;

        @DexIgnore
        public m(ku4 ku4) {
            this.f2085a = ku4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            pq7.b(bool, "it");
            if (bool.booleanValue()) {
                this.f2085a.b();
            } else {
                this.f2085a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku4 f2086a;

        @DexIgnore
        public n(ku4 ku4) {
            this.f2086a = ku4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            b45 b45 = (b45) ku4.N6(this.f2086a).a();
            if (b45 != null) {
                FlexibleTextInputLayout flexibleTextInputLayout = b45.B;
                pq7.b(flexibleTextInputLayout, "inputDes");
                flexibleTextInputLayout.setErrorEnabled(!bool.booleanValue());
                if (!bool.booleanValue()) {
                    FlexibleTextInputLayout flexibleTextInputLayout2 = b45.B;
                    pq7.b(flexibleTextInputLayout2, "inputDes");
                    FlexibleTextInputLayout flexibleTextInputLayout3 = b45.B;
                    pq7.b(flexibleTextInputLayout3, "inputDes");
                    flexibleTextInputLayout2.setError(um5.c(flexibleTextInputLayout3.getContext(), 2131886219));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o<T> implements ls0<ts4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku4 f2087a;

        @DexIgnore
        public o(ku4 ku4) {
            this.f2087a = ku4;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0049  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0067  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x013c  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x013f  */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x0046  */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void onChanged(com.fossil.ts4 r9) {
            /*
            // Method dump skipped, instructions count: 440
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ku4.o.onChanged(com.fossil.ts4):void");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p<T> implements ls0<List<gs4>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku4 f2088a;

        @DexIgnore
        public p(ku4 ku4) {
            this.f2088a = ku4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<gs4> list) {
            T t;
            FlexibleTextInputEditText flexibleTextInputEditText;
            this.f2088a.l.clear();
            List list2 = this.f2088a.l;
            pq7.b(list, "models");
            list2.addAll(list);
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (next.c()) {
                    t = next;
                    break;
                }
            }
            T t2 = t;
            if (t2 != null) {
                Object a2 = t2.a();
                String c = pq7.a(a2, "private") ? um5.c(PortfolioApp.h0.c(), 2131886279) : pq7.a(a2, "public_with_friend") ? um5.c(PortfolioApp.h0.c(), 2131886278) : um5.c(PortfolioApp.h0.c(), 2131886278);
                b45 b45 = (b45) ku4.N6(this.f2088a).a();
                if (b45 != null && (flexibleTextInputEditText = b45.y) != null) {
                    flexibleTextInputEditText.setText(c);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q<T> implements ls0<Integer> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku4 f2089a;

        @DexIgnore
        public q(ku4 ku4) {
            this.f2089a = ku4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Integer num) {
            FlexibleEditText flexibleEditText;
            b45 b45 = (b45) ku4.N6(this.f2089a).a();
            if (b45 != null && (flexibleEditText = b45.v) != null) {
                flexibleEditText.setText(String.valueOf(num.intValue()));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r<T> implements ls0<Integer> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku4 f2090a;

        @DexIgnore
        public r(ku4 ku4) {
            this.f2090a = ku4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Integer num) {
            FlexibleEditText flexibleEditText;
            b45 b45 = (b45) ku4.N6(this.f2090a).a();
            if (b45 != null && (flexibleEditText = b45.w) != null) {
                flexibleEditText.setText(String.valueOf(num.intValue()));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s<T> implements ls0<Integer> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku4 f2091a;

        @DexIgnore
        public s(ku4 ku4) {
            this.f2091a = ku4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Integer num) {
            FlexibleEditText flexibleEditText;
            b45 b45 = (b45) ku4.N6(this.f2091a).a();
            if (b45 != null && (flexibleEditText = b45.z) != null) {
                flexibleEditText.setText(String.valueOf(num.intValue()));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku4 f2092a;

        @DexIgnore
        public t(ku4 ku4) {
            this.f2092a = ku4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            b45 b45 = (b45) ku4.N6(this.f2092a).a();
            if (b45 != null) {
                FlexibleButton flexibleButton = b45.r;
                pq7.b(flexibleButton, "btnNext");
                pq7.b(bool, "it");
                flexibleButton.setEnabled(bool.booleanValue());
                FlexibleButton flexibleButton2 = b45.q;
                pq7.b(flexibleButton2, "btnDone");
                flexibleButton2.setEnabled(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class u<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku4 f2093a;

        @DexIgnore
        public u(ku4 ku4) {
            this.f2093a = ku4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            int i;
            b45 b45 = (b45) ku4.N6(this.f2093a).a();
            if (b45 != null) {
                FlexibleTextView flexibleTextView = b45.A;
                pq7.b(flexibleTextView, "ftvInputErrorDurationStep");
                pq7.b(bool, "isValid");
                if (bool.booleanValue()) {
                    i = 8;
                } else {
                    FlexibleTextView flexibleTextView2 = b45.A;
                    pq7.b(flexibleTextView2, "ftvInputErrorDurationStep");
                    FlexibleTextView flexibleTextView3 = b45.A;
                    pq7.b(flexibleTextView3, "ftvInputErrorDurationStep");
                    flexibleTextView2.setText(um5.c(flexibleTextView3.getContext(), 2131886268));
                    i = 0;
                }
                flexibleTextView.setVisibility(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class v<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku4 f2094a;

        @DexIgnore
        public v(ku4 ku4) {
            this.f2094a = ku4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            int i;
            b45 b45 = (b45) ku4.N6(this.f2094a).a();
            if (b45 != null) {
                FlexibleTextView flexibleTextView = b45.A;
                pq7.b(flexibleTextView, "ftvInputErrorDurationStep");
                pq7.b(bool, "isValid");
                if (bool.booleanValue()) {
                    i = 8;
                } else {
                    FlexibleTextView flexibleTextView2 = b45.A;
                    pq7.b(flexibleTextView2, "ftvInputErrorDurationStep");
                    FlexibleTextView flexibleTextView3 = b45.A;
                    pq7.b(flexibleTextView3, "ftvInputErrorDurationStep");
                    flexibleTextView2.setText(um5.c(flexibleTextView3.getContext(), 2131886275));
                    i = 0;
                }
                flexibleTextView.setVisibility(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class w<T> implements ls0<mu4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku4 f2095a;

        @DexIgnore
        public w(ku4 ku4) {
            this.f2095a = ku4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(mu4.a aVar) {
            b45 b45 = (b45) ku4.N6(this.f2095a).a();
            if (b45 == null) {
                return;
            }
            if (aVar.b()) {
                FlexibleTextInputLayout flexibleTextInputLayout = b45.C;
                pq7.b(flexibleTextInputLayout, "inputName");
                flexibleTextInputLayout.setErrorEnabled(false);
                return;
            }
            FlexibleTextInputLayout flexibleTextInputLayout2 = b45.C;
            pq7.b(flexibleTextInputLayout2, "inputName");
            flexibleTextInputLayout2.setErrorEnabled(true);
            if (aVar.a() < 1) {
                FlexibleTextInputLayout flexibleTextInputLayout3 = b45.C;
                pq7.b(flexibleTextInputLayout3, "inputName");
                FlexibleTextView flexibleTextView = b45.A;
                pq7.b(flexibleTextView, "ftvInputErrorDurationStep");
                flexibleTextInputLayout3.setError(um5.c(flexibleTextView.getContext(), 2131886247));
            } else if (aVar.a() > 32) {
                FlexibleTextInputLayout flexibleTextInputLayout4 = b45.C;
                pq7.b(flexibleTextInputLayout4, "inputName");
                FlexibleTextView flexibleTextView2 = b45.A;
                pq7.b(flexibleTextView2, "ftvInputErrorDurationStep");
                flexibleTextInputLayout4.setError(um5.c(flexibleTextView2.getContext(), 2131886258));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class x implements my5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ku4 f2096a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public x(ku4 ku4) {
            this.f2096a = ku4;
        }

        @DexIgnore
        @Override // com.fossil.my5
        public void a(gs4 gs4) {
            pq7.c(gs4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            ku4.Q6(this.f2096a).K(gs4);
        }
    }

    /*
    static {
        String simpleName = ku4.class.getSimpleName();
        pq7.b(simpleName, "BCCreateChallengeInputFr\u2026nt::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 N6(ku4 ku4) {
        g37<b45> g37 = ku4.h;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ mu4 Q6(ku4 ku4) {
        mu4 mu4 = ku4.i;
        if (mu4 != null) {
            return mu4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void T6() {
        String k2;
        vs4 vs4 = this.j;
        if (vs4 == null || (k2 = vs4.g()) == null) {
            ts4 ts4 = this.k;
            k2 = ts4 != null ? ts4.k() : null;
        }
        String str = pq7.a(k2, "activity_best_result") ? "bc_abr_input" : "bc_arg_input";
        ck5 g2 = ck5.f.g();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            g2.m(str, activity);
            return;
        }
        throw new il7("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    public final void U6(Intent intent) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public final Intent V6(ts4 ts4) {
        Intent intent = new Intent();
        intent.putExtra("challenge_name_extra", ts4.e());
        intent.putExtra("challenge_des_extra", ts4.a());
        intent.putExtra("challenge_target_extra", ts4.i());
        intent.putExtra("challenge_duration_extra", ts4.b());
        return intent;
    }

    @DexIgnore
    public final void W6(ts4 ts4) {
        BCInviteFriendActivity.A.b(this, ts4);
    }

    @DexIgnore
    public final void X6() {
        g37<b45> g37 = this.h;
        if (g37 != null) {
            b45 a2 = g37.a();
            if (a2 != null) {
                a2.E.setOnClickListener(new b(this));
                a2.r.setOnClickListener(new c(this));
                a2.q.setOnClickListener(new d(this));
                a2.y.setOnClickListener(new e(this));
                a2.x.addTextChangedListener(new f(this));
                a2.u.addTextChangedListener(new g(this));
                a2.v.addTextChangedListener(new h(this));
                a2.w.addTextChangedListener(new i(this));
                a2.z.addTextChangedListener(new j(this));
                ImageView imageView = a2.F;
                pq7.b(imageView, "ivThumbnail");
                Context context = imageView.getContext();
                vs4 vs4 = this.j;
                imageView.setImageDrawable(gl0.f(context, vs4 != null ? vs4.f() : 2131231014));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Y6() {
        mu4 mu4 = this.i;
        if (mu4 != null) {
            mu4.p().h(getViewLifecycleOwner(), new o(this));
            mu4 mu42 = this.i;
            if (mu42 != null) {
                mu42.y().h(getViewLifecycleOwner(), new p(this));
                mu4 mu43 = this.i;
                if (mu43 != null) {
                    mu43.r().h(getViewLifecycleOwner(), new q(this));
                    mu4 mu44 = this.i;
                    if (mu44 != null) {
                        mu44.u().h(getViewLifecycleOwner(), new r(this));
                        mu4 mu45 = this.i;
                        if (mu45 != null) {
                            mu45.z().h(getViewLifecycleOwner(), new s(this));
                            mu4 mu46 = this.i;
                            if (mu46 != null) {
                                mu46.x().h(getViewLifecycleOwner(), new t(this));
                                mu4 mu47 = this.i;
                                if (mu47 != null) {
                                    mu47.B().h(getViewLifecycleOwner(), new u(this));
                                    mu4 mu48 = this.i;
                                    if (mu48 != null) {
                                        mu48.A().h(getViewLifecycleOwner(), new v(this));
                                        mu4 mu49 = this.i;
                                        if (mu49 != null) {
                                            mu49.v().h(getViewLifecycleOwner(), new w(this));
                                            mu4 mu410 = this.i;
                                            if (mu410 != null) {
                                                mu410.w().h(getViewLifecycleOwner(), new k(this));
                                                mu4 mu411 = this.i;
                                                if (mu411 != null) {
                                                    mu411.q().h(getViewLifecycleOwner(), new l(this));
                                                    mu4 mu412 = this.i;
                                                    if (mu412 != null) {
                                                        mu412.s().h(getViewLifecycleOwner(), new m(this));
                                                        mu4 mu413 = this.i;
                                                        if (mu413 != null) {
                                                            mu413.o().h(getViewLifecycleOwner(), new n(this));
                                                        } else {
                                                            pq7.n("viewModel");
                                                            throw null;
                                                        }
                                                    } else {
                                                        pq7.n("viewModel");
                                                        throw null;
                                                    }
                                                } else {
                                                    pq7.n("viewModel");
                                                    throw null;
                                                }
                                            } else {
                                                pq7.n("viewModel");
                                                throw null;
                                            }
                                        } else {
                                            pq7.n("viewModel");
                                            throw null;
                                        }
                                    } else {
                                        pq7.n("viewModel");
                                        throw null;
                                    }
                                } else {
                                    pq7.n("viewModel");
                                    throw null;
                                }
                            } else {
                                pq7.n("viewModel");
                                throw null;
                            }
                        } else {
                            pq7.n("viewModel");
                            throw null;
                        }
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void Z6() {
        es4 b2 = es4.A.b();
        String c2 = um5.c(requireContext(), 2131886213);
        pq7.b(c2, "LanguageHelper.getString\u2026nge_List__PrivacySetting)");
        b2.setTitle(c2);
        b2.E6(this.l);
        b2.G6(new x(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        b2.show(childFragmentManager, es4.A.a());
    }

    @DexIgnore
    public final void a7(boolean z) {
        g37<b45> g37 = this.h;
        if (g37 != null) {
            b45 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                FlexibleButton flexibleButton = a2.r;
                pq7.b(flexibleButton, "btnNext");
                flexibleButton.setVisibility(0);
                FlexibleButton flexibleButton2 = a2.q;
                pq7.b(flexibleButton2, "btnDone");
                flexibleButton2.setVisibility(8);
                FlexibleTextInputLayout flexibleTextInputLayout = a2.D;
                pq7.b(flexibleTextInputLayout, "inputPrivacy");
                flexibleTextInputLayout.setEnabled(true);
                return;
            }
            FlexibleButton flexibleButton3 = a2.r;
            pq7.b(flexibleButton3, "btnNext");
            flexibleButton3.setVisibility(8);
            FlexibleButton flexibleButton4 = a2.q;
            pq7.b(flexibleButton4, "btnDone");
            flexibleButton4.setVisibility(0);
            FlexibleTextInputLayout flexibleTextInputLayout2 = a2.D;
            pq7.b(flexibleTextInputLayout2, "inputPrivacy");
            flexibleTextInputLayout2.setEnabled(false);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 11 && i3 == -1) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.setResult(i3);
            }
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                activity2.finish();
            }
        }
        FragmentActivity activity3 = getActivity();
        if (activity3 != null) {
            activity3.setResult(i3, intent);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().d0().a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(mu4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026putViewModel::class.java)");
            this.i = (mu4) a2;
            Bundle arguments = getArguments();
            this.j = arguments != null ? (vs4) arguments.getParcelable("challenge_template_extra") : null;
            Bundle arguments2 = getArguments();
            this.k = arguments2 != null ? (ts4) arguments2.getParcelable("challenge_draft_extra") : null;
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        b45 b45 = (b45) aq0.f(layoutInflater, 2131558525, viewGroup, false, A6());
        this.h = new g37<>(this, b45);
        pq7.b(b45, "binding");
        return b45.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        T6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        ts4 ts4 = this.k;
        if (ts4 != null) {
            mu4 mu4 = this.i;
            if (mu4 == null) {
                pq7.n("viewModel");
                throw null;
            } else if (ts4 != null) {
                mu4.D(ts4);
                a7(false);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            a7(true);
            mu4 mu42 = this.i;
            if (mu42 != null) {
                mu42.C(this.j);
            } else {
                pq7.n("viewModel");
                throw null;
            }
        }
        X6();
        Y6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
