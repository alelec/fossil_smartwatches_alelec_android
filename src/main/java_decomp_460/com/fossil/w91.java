package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class w91 implements da1 {
    @DexIgnore
    @Override // com.fossil.da1
    @Deprecated
    public final HttpResponse a(m91<?> m91, Map<String, String> map) throws IOException, z81 {
        ca1 b = b(m91, map);
        BasicHttpResponse basicHttpResponse = new BasicHttpResponse(new BasicStatusLine(new ProtocolVersion("HTTP", 1, 1), b.d(), ""));
        ArrayList arrayList = new ArrayList();
        for (f91 f91 : b.c()) {
            arrayList.add(new BasicHeader(f91.a(), f91.b()));
        }
        basicHttpResponse.setHeaders((Header[]) arrayList.toArray(new Header[arrayList.size()]));
        InputStream a2 = b.a();
        if (a2 != null) {
            BasicHttpEntity basicHttpEntity = new BasicHttpEntity();
            basicHttpEntity.setContent(a2);
            basicHttpEntity.setContentLength((long) b.b());
            basicHttpResponse.setEntity(basicHttpEntity);
        }
        return basicHttpResponse;
    }

    @DexIgnore
    public abstract ca1 b(m91<?> m91, Map<String, String> map) throws IOException, z81;
}
