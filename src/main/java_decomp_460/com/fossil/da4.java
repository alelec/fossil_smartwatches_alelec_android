package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class da4 extends ta4.d {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f756a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ Long d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ ta4.d.a f;
    @DexIgnore
    public /* final */ ta4.d.f g;
    @DexIgnore
    public /* final */ ta4.d.e h;
    @DexIgnore
    public /* final */ ta4.d.c i;
    @DexIgnore
    public /* final */ ua4<ta4.d.AbstractC0224d> j;
    @DexIgnore
    public /* final */ int k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.d.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f757a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Long c;
        @DexIgnore
        public Long d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public ta4.d.a f;
        @DexIgnore
        public ta4.d.f g;
        @DexIgnore
        public ta4.d.e h;
        @DexIgnore
        public ta4.d.c i;
        @DexIgnore
        public ua4<ta4.d.AbstractC0224d> j;
        @DexIgnore
        public Integer k;

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public b(ta4.d dVar) {
            this.f757a = dVar.f();
            this.b = dVar.h();
            this.c = Long.valueOf(dVar.k());
            this.d = dVar.d();
            this.e = Boolean.valueOf(dVar.m());
            this.f = dVar.b();
            this.g = dVar.l();
            this.h = dVar.j();
            this.i = dVar.c();
            this.j = dVar.e();
            this.k = Integer.valueOf(dVar.g());
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.b
        public ta4.d a() {
            String str = "";
            if (this.f757a == null) {
                str = " generator";
            }
            if (this.b == null) {
                str = str + " identifier";
            }
            if (this.c == null) {
                str = str + " startedAt";
            }
            if (this.e == null) {
                str = str + " crashed";
            }
            if (this.f == null) {
                str = str + " app";
            }
            if (this.k == null) {
                str = str + " generatorType";
            }
            if (str.isEmpty()) {
                return new da4(this.f757a, this.b, this.c.longValue(), this.d, this.e.booleanValue(), this.f, this.g, this.h, this.i, this.j, this.k.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.b
        public ta4.d.b b(ta4.d.a aVar) {
            if (aVar != null) {
                this.f = aVar;
                return this;
            }
            throw new NullPointerException("Null app");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.b
        public ta4.d.b c(boolean z) {
            this.e = Boolean.valueOf(z);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.b
        public ta4.d.b d(ta4.d.c cVar) {
            this.i = cVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.b
        public ta4.d.b e(Long l) {
            this.d = l;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.b
        public ta4.d.b f(ua4<ta4.d.AbstractC0224d> ua4) {
            this.j = ua4;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.b
        public ta4.d.b g(String str) {
            if (str != null) {
                this.f757a = str;
                return this;
            }
            throw new NullPointerException("Null generator");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.b
        public ta4.d.b h(int i2) {
            this.k = Integer.valueOf(i2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.b
        public ta4.d.b i(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null identifier");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.b
        public ta4.d.b k(ta4.d.e eVar) {
            this.h = eVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.b
        public ta4.d.b l(long j2) {
            this.c = Long.valueOf(j2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.b
        public ta4.d.b m(ta4.d.f fVar) {
            this.g = fVar;
            return this;
        }
    }

    @DexIgnore
    public da4(String str, String str2, long j2, Long l, boolean z, ta4.d.a aVar, ta4.d.f fVar, ta4.d.e eVar, ta4.d.c cVar, ua4<ta4.d.AbstractC0224d> ua4, int i2) {
        this.f756a = str;
        this.b = str2;
        this.c = j2;
        this.d = l;
        this.e = z;
        this.f = aVar;
        this.g = fVar;
        this.h = eVar;
        this.i = cVar;
        this.j = ua4;
        this.k = i2;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d
    public ta4.d.a b() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d
    public ta4.d.c c() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d
    public Long d() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d
    public ua4<ta4.d.AbstractC0224d> e() {
        return this.j;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Long l;
        ta4.d.f fVar;
        ta4.d.e eVar;
        ta4.d.c cVar;
        ua4<ta4.d.AbstractC0224d> ua4;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.d)) {
            return false;
        }
        ta4.d dVar = (ta4.d) obj;
        return this.f756a.equals(dVar.f()) && this.b.equals(dVar.h()) && this.c == dVar.k() && ((l = this.d) != null ? l.equals(dVar.d()) : dVar.d() == null) && this.e == dVar.m() && this.f.equals(dVar.b()) && ((fVar = this.g) != null ? fVar.equals(dVar.l()) : dVar.l() == null) && ((eVar = this.h) != null ? eVar.equals(dVar.j()) : dVar.j() == null) && ((cVar = this.i) != null ? cVar.equals(dVar.c()) : dVar.c() == null) && ((ua4 = this.j) != null ? ua4.equals(dVar.e()) : dVar.e() == null) && this.k == dVar.g();
    }

    @DexIgnore
    @Override // com.fossil.ta4.d
    public String f() {
        return this.f756a;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d
    public int g() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d
    public String h() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        int hashCode = this.f756a.hashCode();
        int hashCode2 = this.b.hashCode();
        long j2 = this.c;
        int i3 = (int) (j2 ^ (j2 >>> 32));
        Long l = this.d;
        int hashCode3 = l == null ? 0 : l.hashCode();
        int i4 = this.e ? 1231 : 1237;
        int hashCode4 = this.f.hashCode();
        ta4.d.f fVar = this.g;
        int hashCode5 = fVar == null ? 0 : fVar.hashCode();
        ta4.d.e eVar = this.h;
        int hashCode6 = eVar == null ? 0 : eVar.hashCode();
        ta4.d.c cVar = this.i;
        int hashCode7 = cVar == null ? 0 : cVar.hashCode();
        ua4<ta4.d.AbstractC0224d> ua4 = this.j;
        if (ua4 != null) {
            i2 = ua4.hashCode();
        }
        return ((((((((((((((hashCode3 ^ ((((((hashCode ^ 1000003) * 1000003) ^ hashCode2) * 1000003) ^ i3) * 1000003)) * 1000003) ^ i4) * 1000003) ^ hashCode4) * 1000003) ^ hashCode5) * 1000003) ^ hashCode6) * 1000003) ^ hashCode7) * 1000003) ^ i2) * 1000003) ^ this.k;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d
    public ta4.d.e j() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d
    public long k() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d
    public ta4.d.f l() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d
    public boolean m() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d
    public ta4.d.b n() {
        return new b(this);
    }

    @DexIgnore
    public String toString() {
        return "Session{generator=" + this.f756a + ", identifier=" + this.b + ", startedAt=" + this.c + ", endedAt=" + this.d + ", crashed=" + this.e + ", app=" + this.f + ", user=" + this.g + ", os=" + this.h + ", device=" + this.i + ", events=" + this.j + ", generatorType=" + this.k + "}";
    }
}
