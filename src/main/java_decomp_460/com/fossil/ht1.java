package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ht1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ xs1[] d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ht1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ht1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(dq1.class.getClassLoader());
            if (readParcelable != null) {
                return new ht1((dq1) readParcelable, (nt1) parcel.readParcelable(nt1.class.getClassLoader()), (xs1[]) parcel.createTypedArray(xs1.CREATOR));
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ht1[] newArray(int i) {
            return new ht1[i];
        }
    }

    @DexIgnore
    public ht1(dq1 dq1, nt1 nt1, xs1[] xs1Arr) {
        super(dq1, nt1);
        this.d = xs1Arr;
    }

    @DexIgnore
    public ht1(dq1 dq1, xs1[] xs1Arr) {
        super(dq1, null);
        this.d = xs1Arr;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public JSONObject a() {
        Object obj;
        JSONObject a2 = super.a();
        jd0 jd0 = jd0.e5;
        xs1[] xs1Arr = this.d;
        if (xs1Arr == null || (obj = px1.a(xs1Arr)) == null) {
            obj = JSONObject.NULL;
        }
        return g80.k(a2, jd0, obj);
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        jq1 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.d != null) {
                JSONArray jSONArray = new JSONArray();
                for (xs1 xs1 : this.d) {
                    jSONArray.put(xs1.a());
                }
                jSONObject.put("buddyChallengeApp._.config.challenge_list", jSONArray);
            } else {
                getDeviceMessage();
            }
        } catch (JSONException e) {
            d90.i.i(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        pq7.b(jSONObject4, "deviceResponseJSONObject.toString()");
        Charset c = hd0.y.c();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(c);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public boolean equals(Object obj) {
        xs1[] xs1Arr;
        if (this == obj) {
            return true;
        }
        if (!pq7.a(ht1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ht1 ht1 = (ht1) obj;
            xs1[] xs1Arr2 = this.d;
            if (xs1Arr2 == null || (xs1Arr = ht1.d) == null) {
                if (this.d == null || ht1.d == null) {
                    return false;
                }
            } else if (!Arrays.equals(xs1Arr2, xs1Arr)) {
                return false;
            }
            return true;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.BuddyChallengeWatchAppListChallengesData");
    }

    @DexIgnore
    public final xs1[] getBuddyChallenges() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public int hashCode() {
        int hashCode = super.hashCode();
        xs1[] xs1Arr = this.d;
        return (xs1Arr != null ? xs1Arr.hashCode() : 0) + (hashCode * 31);
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.d, i);
        }
    }
}
