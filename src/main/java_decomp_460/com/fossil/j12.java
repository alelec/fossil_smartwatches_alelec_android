package com.fossil;

import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j12 implements Factory<i12> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<Executor> f1703a;
    @DexIgnore
    public /* final */ Provider<t02> b;
    @DexIgnore
    public /* final */ Provider<h22> c;
    @DexIgnore
    public /* final */ Provider<k22> d;
    @DexIgnore
    public /* final */ Provider<s32> e;

    @DexIgnore
    public j12(Provider<Executor> provider, Provider<t02> provider2, Provider<h22> provider3, Provider<k22> provider4, Provider<s32> provider5) {
        this.f1703a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static j12 a(Provider<Executor> provider, Provider<t02> provider2, Provider<h22> provider3, Provider<k22> provider4, Provider<s32> provider5) {
        return new j12(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    /* renamed from: b */
    public i12 get() {
        return new i12(this.f1703a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }
}
