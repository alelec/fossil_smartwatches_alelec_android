package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;
import androidx.collection.SimpleArrayMap;
import com.facebook.LegacyTokenHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bc2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ SimpleArrayMap<String, String> f412a; // = new SimpleArrayMap<>();

    @DexIgnore
    public static String a(Context context) {
        String packageName = context.getPackageName();
        try {
            return ag2.a(context).d(packageName).toString();
        } catch (PackageManager.NameNotFoundException | NullPointerException e) {
            String str = context.getApplicationInfo().name;
            return !TextUtils.isEmpty(str) ? str : packageName;
        }
    }

    @DexIgnore
    public static String b(Context context) {
        return context.getResources().getString(w52.common_google_play_services_notification_channel_name);
    }

    @DexIgnore
    public static String c(Context context, int i) {
        Resources resources = context.getResources();
        return i != 1 ? i != 2 ? i != 3 ? resources.getString(17039370) : resources.getString(w52.common_google_play_services_enable_button) : resources.getString(w52.common_google_play_services_update_button) : resources.getString(w52.common_google_play_services_install_button);
    }

    @DexIgnore
    public static String d(Context context, int i) {
        Resources resources = context.getResources();
        String a2 = a(context);
        if (i == 1) {
            return resources.getString(w52.common_google_play_services_install_text, a2);
        } else if (i != 2) {
            if (i == 3) {
                return resources.getString(w52.common_google_play_services_enable_text, a2);
            } else if (i == 5) {
                return i(context, "common_google_play_services_invalid_account_text", a2);
            } else {
                if (i == 7) {
                    return i(context, "common_google_play_services_network_error_text", a2);
                }
                if (i == 9) {
                    return resources.getString(w52.common_google_play_services_unsupported_text, a2);
                } else if (i == 20) {
                    return i(context, "common_google_play_services_restricted_profile_text", a2);
                } else {
                    switch (i) {
                        case 16:
                            return i(context, "common_google_play_services_api_unavailable_text", a2);
                        case 17:
                            return i(context, "common_google_play_services_sign_in_failed_text", a2);
                        case 18:
                            return resources.getString(w52.common_google_play_services_updating_text, a2);
                        default:
                            return resources.getString(j62.common_google_play_services_unknown_issue, a2);
                    }
                }
            }
        } else if (if2.d(context)) {
            return resources.getString(w52.common_google_play_services_wear_update_text);
        } else {
            return resources.getString(w52.common_google_play_services_update_text, a2);
        }
    }

    @DexIgnore
    public static String e(Context context, int i) {
        return (i == 6 || i == 19) ? i(context, "common_google_play_services_resolution_required_text", a(context)) : d(context, i);
    }

    @DexIgnore
    public static String f(Context context, int i) {
        String h = i == 6 ? h(context, "common_google_play_services_resolution_required_title") : g(context, i);
        return h == null ? context.getResources().getString(w52.common_google_play_services_notification_ticker) : h;
    }

    @DexIgnore
    public static String g(Context context, int i) {
        Resources resources = context.getResources();
        switch (i) {
            case 1:
                return resources.getString(w52.common_google_play_services_install_title);
            case 2:
                return resources.getString(w52.common_google_play_services_update_title);
            case 3:
                return resources.getString(w52.common_google_play_services_enable_title);
            case 4:
            case 6:
            case 18:
                return null;
            case 5:
                Log.e("GoogleApiAvailability", "An invalid account was specified when connecting. Please provide a valid account.");
                return h(context, "common_google_play_services_invalid_account_title");
            case 7:
                Log.e("GoogleApiAvailability", "Network error occurred. Please retry request later.");
                return h(context, "common_google_play_services_network_error_title");
            case 8:
                Log.e("GoogleApiAvailability", "Internal error occurred. Please see logs for detailed information");
                return null;
            case 9:
                Log.e("GoogleApiAvailability", "Google Play services is invalid. Cannot recover.");
                return null;
            case 10:
                Log.e("GoogleApiAvailability", "Developer error occurred. Please see logs for detailed information");
                return null;
            case 11:
                Log.e("GoogleApiAvailability", "The application is not licensed to the user.");
                return null;
            case 12:
            case 13:
            case 14:
            case 15:
            case 19:
            default:
                StringBuilder sb = new StringBuilder(33);
                sb.append("Unexpected error code ");
                sb.append(i);
                Log.e("GoogleApiAvailability", sb.toString());
                return null;
            case 16:
                Log.e("GoogleApiAvailability", "One of the API components you attempted to connect to is not available.");
                return null;
            case 17:
                Log.e("GoogleApiAvailability", "The specified account could not be signed in.");
                return h(context, "common_google_play_services_sign_in_failed_title");
            case 20:
                Log.e("GoogleApiAvailability", "The current user profile is restricted and could not use authenticated features.");
                return h(context, "common_google_play_services_restricted_profile_title");
        }
    }

    @DexIgnore
    public static String h(Context context, String str) {
        synchronized (f412a) {
            String str2 = f412a.get(str);
            if (str2 != null) {
                return str2;
            }
            Resources e = g62.e(context);
            if (e == null) {
                return null;
            }
            int identifier = e.getIdentifier(str, LegacyTokenHelper.TYPE_STRING, "com.google.android.gms");
            if (identifier == 0) {
                String valueOf = String.valueOf(str);
                Log.w("GoogleApiAvailability", valueOf.length() != 0 ? "Missing resource: ".concat(valueOf) : new String("Missing resource: "));
                return null;
            }
            String string = e.getString(identifier);
            if (TextUtils.isEmpty(string)) {
                String valueOf2 = String.valueOf(str);
                Log.w("GoogleApiAvailability", valueOf2.length() != 0 ? "Got empty resource: ".concat(valueOf2) : new String("Got empty resource: "));
                return null;
            }
            f412a.put(str, string);
            return string;
        }
    }

    @DexIgnore
    public static String i(Context context, String str, String str2) {
        Resources resources = context.getResources();
        String h = h(context, str);
        if (h == null) {
            h = resources.getString(j62.common_google_play_services_unknown_issue);
        }
        return String.format(resources.getConfiguration().locale, h, str2);
    }
}
