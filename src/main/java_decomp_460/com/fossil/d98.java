package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d98 implements e88<w18, Integer> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ d98 f754a; // = new d98();

    @DexIgnore
    /* renamed from: b */
    public Integer a(w18 w18) throws IOException {
        return Integer.valueOf(w18.string());
    }
}
