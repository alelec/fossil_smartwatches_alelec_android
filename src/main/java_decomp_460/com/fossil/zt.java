package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum zt {
    WORKOUT_SESSION_PRIMARY_ID((byte) 3),
    CONNECTION_PARAMETERS_REQUEST((byte) 9),
    CURRENT_CONNECTION_PARAMETERS((byte) 10),
    STREAMING_CONFIG((byte) 12),
    CALIBRATION_SETTING(DateTimeFieldType.SECOND_OF_MINUTE),
    ADVANCE_BLE_REQUEST(DateTimeFieldType.MILLIS_OF_SECOND),
    DIAGNOSTIC_FUNCTIONS((byte) 241),
    HARDWARE_TEST((byte) 242);
    
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public zt(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
