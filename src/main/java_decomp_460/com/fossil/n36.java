package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n36 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ q26 f2460a;
    @DexIgnore
    public /* final */ h36 b;

    @DexIgnore
    public n36(q26 q26, h36 h36) {
        pq7.c(q26, "mInActivityNudgeTimeContractView");
        pq7.c(h36, "mRemindTimeContractView");
        this.f2460a = q26;
        this.b = h36;
    }

    @DexIgnore
    public final q26 a() {
        return this.f2460a;
    }

    @DexIgnore
    public final h36 b() {
        return this.b;
    }
}
