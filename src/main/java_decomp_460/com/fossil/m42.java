package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m42 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<m42> CREATOR; // = new p42();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public Bundle d;

    @DexIgnore
    public m42(int i, int i2, Bundle bundle) {
        this.b = i;
        this.c = i2;
        this.d = bundle;
    }

    @DexIgnore
    public int c() {
        return this.c;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.n(parcel, 1, this.b);
        bd2.n(parcel, 2, c());
        bd2.e(parcel, 3, this.d, false);
        bd2.b(parcel, a2);
    }
}
