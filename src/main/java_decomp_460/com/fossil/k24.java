package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class k24<E> implements Iterable<E> {
    @DexIgnore
    public /* final */ g14<Iterable<E>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends k24<E> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Iterable iterable, Iterable iterable2) {
            super(iterable);
            this.c = iterable2;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<E> iterator() {
            return this.c.iterator();
        }
    }

    @DexIgnore
    public k24() {
        this.b = g14.absent();
    }

    @DexIgnore
    public k24(Iterable<E> iterable) {
        i14.l(iterable);
        this.b = g14.fromNullable(this == iterable ? null : iterable);
    }

    @DexIgnore
    public static <E> k24<E> b(Iterable<E> iterable) {
        return iterable instanceof k24 ? (k24) iterable : new a(iterable, iterable);
    }

    @DexIgnore
    public final k24<E> a(j14<? super E> j14) {
        return b(o34.d(c(), j14));
    }

    @DexIgnore
    public final Iterable<E> c() {
        return this.b.or((g14<Iterable<E>>) this);
    }

    @DexIgnore
    public final h34<E> d() {
        return h34.copyOf(c());
    }

    @DexIgnore
    public String toString() {
        return o34.j(c());
    }
}
