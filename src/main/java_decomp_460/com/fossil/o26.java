package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o26 implements Factory<n26> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<QuickResponseRepository> f2620a;
    @DexIgnore
    public /* final */ Provider<mt5> b;

    @DexIgnore
    public o26(Provider<QuickResponseRepository> provider, Provider<mt5> provider2) {
        this.f2620a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static o26 a(Provider<QuickResponseRepository> provider, Provider<mt5> provider2) {
        return new o26(provider, provider2);
    }

    @DexIgnore
    public static n26 c(QuickResponseRepository quickResponseRepository, mt5 mt5) {
        return new n26(quickResponseRepository, mt5);
    }

    @DexIgnore
    /* renamed from: b */
    public n26 get() {
        return c(this.f2620a.get(), this.b.get());
    }
}
