package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mf5 extends lf5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B;
    @DexIgnore
    public /* final */ CardView y;
    @DexIgnore
    public long z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        B = sparseIntArray;
        sparseIntArray.put(2131362624, 1);
        B.put(2131362544, 2);
        B.put(2131362382, 3);
        B.put(2131362488, 4);
        B.put(2131362418, 5);
        B.put(2131362534, 6);
        B.put(2131362391, 7);
        B.put(2131362380, 8);
    }
    */

    @DexIgnore
    public mf5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 9, A, B));
    }

    @DexIgnore
    public mf5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[6], (TimerTextView) objArr[2], (ImageView) objArr[1]);
        this.z = -1;
        CardView cardView = (CardView) objArr[0];
        this.y = cardView;
        cardView.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.z != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.z = 1;
        }
        w();
    }
}
