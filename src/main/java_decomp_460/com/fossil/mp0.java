package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface mp0 {
    @DexIgnore
    void setSupportCompoundDrawablesTintList(ColorStateList colorStateList);

    @DexIgnore
    void setSupportCompoundDrawablesTintMode(PorterDuff.Mode mode);
}
