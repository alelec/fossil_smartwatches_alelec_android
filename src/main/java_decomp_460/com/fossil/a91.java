package com.fossil;

import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface a91 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public byte[] f225a;
        @DexIgnore
        public String b;
        @DexIgnore
        public long c;
        @DexIgnore
        public long d;
        @DexIgnore
        public long e;
        @DexIgnore
        public long f;
        @DexIgnore
        public Map<String, String> g; // = Collections.emptyMap();
        @DexIgnore
        public List<f91> h;

        @DexIgnore
        public boolean a() {
            return this.e < System.currentTimeMillis();
        }

        @DexIgnore
        public boolean b() {
            return this.f < System.currentTimeMillis();
        }
    }

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    a b(String str);

    @DexIgnore
    void c(String str, a aVar);
}
