package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ww5 extends du0<GoalTrackingSummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar c; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ PortfolioApp k;
    @DexIgnore
    public /* final */ zw5 l;
    @DexIgnore
    public /* final */ FragmentManager m;
    @DexIgnore
    public /* final */ pv5 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Date f4007a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;

        @DexIgnore
        public a(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4) {
            pq7.c(str, "mDayOfWeek");
            pq7.c(str2, "mDayOfMonth");
            pq7.c(str3, "mDailyValue");
            pq7.c(str4, "mDailyUnit");
            this.f4007a = date;
            this.b = z;
            this.c = z2;
            this.d = str;
            this.e = str2;
            this.f = str3;
            this.g = str4;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4, int i, kq7 kq7) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? false : z, (i & 4) == 0 ? z2 : false, (i & 8) != 0 ? "" : str, (i & 16) != 0 ? "" : str2, (i & 32) != 0 ? "" : str3, (i & 64) != 0 ? "" : str4);
        }

        @DexIgnore
        public final String a() {
            return this.g;
        }

        @DexIgnore
        public final String b() {
            return this.f;
        }

        @DexIgnore
        public final Date c() {
            return this.f4007a;
        }

        @DexIgnore
        public final String d() {
            return this.e;
        }

        @DexIgnore
        public final String e() {
            return this.d;
        }

        @DexIgnore
        public final boolean f() {
            return this.c;
        }

        @DexIgnore
        public final boolean g() {
            return this.b;
        }

        @DexIgnore
        public final void h(String str) {
            pq7.c(str, "<set-?>");
            this.g = str;
        }

        @DexIgnore
        public final void i(String str) {
            pq7.c(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final void j(Date date) {
            this.f4007a = date;
        }

        @DexIgnore
        public final void k(String str) {
            pq7.c(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final void l(String str) {
            pq7.c(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void m(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public final void n(boolean z) {
            this.b = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Date f4008a;
        @DexIgnore
        public /* final */ re5 b;
        @DexIgnore
        public /* final */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ ww5 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date date = this.b.f4008a;
                if (date != null) {
                    this.b.d.l.Q(date);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ww5 ww5, re5 re5, View view) {
            super(view);
            pq7.c(re5, "binding");
            pq7.c(view, "root");
            this.d = ww5;
            this.b = re5;
            this.c = view;
            re5.n().setOnClickListener(new a(this));
            this.b.r.setTextColor(ww5.f);
        }

        @DexIgnore
        public void b(GoalTrackingSummary goalTrackingSummary) {
            a v = this.d.v(goalTrackingSummary);
            this.f4008a = v.c();
            FlexibleTextView flexibleTextView = this.b.u;
            pq7.b(flexibleTextView, "binding.ftvDayOfWeek");
            flexibleTextView.setText(v.e());
            FlexibleTextView flexibleTextView2 = this.b.t;
            pq7.b(flexibleTextView2, "binding.ftvDayOfMonth");
            flexibleTextView2.setText(v.d());
            FlexibleTextView flexibleTextView3 = this.b.s;
            pq7.b(flexibleTextView3, "binding.ftvDailyValue");
            flexibleTextView3.setText(v.b());
            FlexibleTextView flexibleTextView4 = this.b.r;
            pq7.b(flexibleTextView4, "binding.ftvDailyUnit");
            flexibleTextView4.setText(v.a());
            ConstraintLayout constraintLayout = this.b.q;
            pq7.b(constraintLayout, "binding.container");
            constraintLayout.setSelected(!v.f());
            FlexibleTextView flexibleTextView5 = this.b.u;
            pq7.b(flexibleTextView5, "binding.ftvDayOfWeek");
            flexibleTextView5.setSelected(v.g());
            FlexibleTextView flexibleTextView6 = this.b.t;
            pq7.b(flexibleTextView6, "binding.ftvDayOfMonth");
            flexibleTextView6.setSelected(v.g());
            if (v.g()) {
                this.b.q.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.g);
                this.b.t.setBackgroundColor(this.d.g);
                this.b.u.setTextColor(this.d.j);
                this.b.t.setTextColor(this.d.j);
            } else if (v.f()) {
                this.b.q.setBackgroundColor(this.d.e);
                this.b.u.setBackgroundColor(this.d.e);
                this.b.t.setBackgroundColor(this.d.e);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            } else {
                this.b.q.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.i);
                this.b.t.setBackgroundColor(this.d.i);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Date f4009a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public c(Date date, Date date2, String str, String str2) {
            pq7.c(str, "mWeekly");
            pq7.c(str2, "mWeeklyValue");
            this.f4009a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(Date date, Date date2, String str, String str2, int i, kq7 kq7) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.f4009a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        public final void e(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void f(Date date) {
            this.f4009a = date;
        }

        @DexIgnore
        public final void g(String str) {
            pq7.c(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void h(String str) {
            pq7.c(str, "<set-?>");
            this.d = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends b {
        @DexIgnore
        public Date e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public /* final */ te5 g;
        @DexIgnore
        public /* final */ /* synthetic */ ww5 h;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d b;

            @DexIgnore
            public a(d dVar) {
                this.b = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.e != null && this.b.f != null) {
                    zw5 zw5 = this.b.h.l;
                    Date date = this.b.e;
                    if (date != null) {
                        Date date2 = this.b.f;
                        if (date2 != null) {
                            zw5.q0(date, date2);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public d(com.fossil.ww5 r4, com.fossil.te5 r5) {
            /*
                r3 = this;
                java.lang.String r0 = "binding"
                com.fossil.pq7.c(r5, r0)
                r3.h = r4
                com.fossil.re5 r0 = r5.r
                if (r0 == 0) goto L_0x0029
                java.lang.String r1 = "binding.dailyItem!!"
                com.fossil.pq7.b(r0, r1)
                android.view.View r1 = r5.n()
                java.lang.String r2 = "binding.root"
                com.fossil.pq7.b(r1, r2)
                r3.<init>(r4, r0, r1)
                r3.g = r5
                androidx.constraintlayout.widget.ConstraintLayout r0 = r5.q
                com.fossil.ww5$d$a r1 = new com.fossil.ww5$d$a
                r1.<init>(r3)
                r0.setOnClickListener(r1)
                return
            L_0x0029:
                com.fossil.pq7.i()
                r0 = 0
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ww5.d.<init>(com.fossil.ww5, com.fossil.te5):void");
        }

        @DexIgnore
        @Override // com.fossil.ww5.b
        public void b(GoalTrackingSummary goalTrackingSummary) {
            c w = this.h.w(goalTrackingSummary);
            this.f = w.a();
            this.e = w.b();
            FlexibleTextView flexibleTextView = this.g.s;
            pq7.b(flexibleTextView, "binding.ftvWeekly");
            flexibleTextView.setText(w.c());
            FlexibleTextView flexibleTextView2 = this.g.t;
            pq7.b(flexibleTextView2, "binding.ftvWeeklyValue");
            flexibleTextView2.setText(w.d());
            super.b(goalTrackingSummary);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ww5 b;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        public e(ww5 ww5, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.b = ww5;
            this.c = viewHolder;
            this.d = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            pq7.c(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - mFragment.id=" + this.b.n.getId() + ", isAdded=" + this.b.n.isAdded());
            this.c.itemView.removeOnAttachStateChangeListener(this);
            Fragment Z = this.b.m.Z(this.b.n.D6());
            if (Z == null) {
                FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                xq0 j = this.b.m.j();
                j.b(view.getId(), this.b.n, this.b.n.D6());
                j.k();
            } else if (this.d) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - oldFragment.id=" + Z.getId() + ", isAdded=" + Z.isAdded());
                xq0 j2 = this.b.m.j();
                j2.q(Z);
                j2.k();
                xq0 j3 = this.b.m.j();
                j3.b(view.getId(), this.b.n, this.b.n.D6());
                j3.k();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - oldFragment.id=" + Z.getId() + ", isAdded=" + Z.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.b.n.getId() + ", isAdded2=" + this.b.n.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            pq7.c(view, "v");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ FrameLayout f4010a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(FrameLayout frameLayout, View view) {
            super(view);
            this.f4010a = frameLayout;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ww5(yw5 yw5, PortfolioApp portfolioApp, zw5 zw5, FragmentManager fragmentManager, pv5 pv5) {
        super(yw5);
        pq7.c(yw5, "difference");
        pq7.c(portfolioApp, "mApp");
        pq7.c(zw5, "mOnItemClick");
        pq7.c(fragmentManager, "mFragmentManager");
        pq7.c(pv5, "mFragment");
        this.k = portfolioApp;
        this.l = zw5;
        this.m = fragmentManager;
        this.n = pv5;
        String d2 = qn5.l.a().d("primaryText");
        this.d = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = qn5.l.a().d("nonBrandSurface");
        this.e = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = qn5.l.a().d("nonBrandNonReachGoal");
        this.f = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = qn5.l.a().d("hybridGoalTrackingTab");
        this.g = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
        String d6 = qn5.l.a().d("secondaryText");
        this.h = Color.parseColor(d6 == null ? "#FFFFFF" : d6);
        String d7 = qn5.l.a().d("nonBrandActivityDetailBackground");
        this.i = Color.parseColor(d7 == null ? "#FFFFFF" : d7);
        String d8 = qn5.l.a().d("onHybridGoalTrackingTab");
        this.j = Color.parseColor(d8 == null ? "#FFFFFF" : d8);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i2) {
        if (getItemViewType(i2) != 0) {
            return super.getItemId(i2);
        }
        if (this.n.getId() == 0) {
            return 1010101;
        }
        return (long) this.n.getId();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) getItem(i2);
        if (goalTrackingSummary != null) {
            Calendar calendar = this.c;
            pq7.b(calendar, "mCalendar");
            calendar.setTime(goalTrackingSummary.getDate());
            Calendar calendar2 = this.c;
            pq7.b(calendar2, "mCalendar");
            Boolean p0 = lk5.p0(calendar2.getTime());
            pq7.b(p0, "DateHelper.isToday(mCalendar.time)");
            if (p0.booleanValue() || this.c.get(7) == 7) {
                return 2;
            }
        }
        return 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        boolean z = true;
        pq7.c(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardGoalTrackingAdapter", "onBindViewHolder - position=" + i2);
        int itemViewType = getItemViewType(i2);
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            pq7.b(view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            pq7.b(view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardGoalTrackingAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            pq7.b(view3, "holder.itemView");
            view3.setId((int) getItemId(i2));
            viewHolder.itemView.addOnAttachStateChangeListener(new e(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((b) viewHolder).b((GoalTrackingSummary) getItem(i2));
        } else if (itemViewType != 2) {
            ((b) viewHolder).b((GoalTrackingSummary) getItem(i2));
        } else {
            ((d) viewHolder).b((GoalTrackingSummary) getItem(i2));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        pq7.c(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i2 == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new f(frameLayout, frameLayout);
        } else if (i2 == 1) {
            re5 z = re5.z(from, viewGroup, false);
            pq7.b(z, "ItemGoalTrackingDayBindi\u2026tInflater, parent, false)");
            View n2 = z.n();
            pq7.b(n2, "itemGoalTrackingDayBinding.root");
            return new b(this, z, n2);
        } else if (i2 != 2) {
            re5 z2 = re5.z(from, viewGroup, false);
            pq7.b(z2, "ItemGoalTrackingDayBindi\u2026tInflater, parent, false)");
            View n3 = z2.n();
            pq7.b(n3, "itemGoalTrackingDayBinding.root");
            return new b(this, z2, n3);
        } else {
            te5 z3 = te5.z(from, viewGroup, false);
            pq7.b(z3, "ItemGoalTrackingWeekBind\u2026tInflater, parent, false)");
            return new d(this, z3);
        }
    }

    @DexIgnore
    public final a v(GoalTrackingSummary goalTrackingSummary) {
        boolean z = false;
        a aVar = new a(null, false, false, null, null, null, null, 127, null);
        if (goalTrackingSummary != null) {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "calendar");
            instance.setTime(goalTrackingSummary.getDate());
            int i2 = instance.get(7);
            aVar.j(instance.getTime());
            aVar.k(String.valueOf(instance.get(5)));
            Boolean p0 = lk5.p0(instance.getTime());
            pq7.b(p0, "DateHelper.isToday(calendar.time)");
            if (p0.booleanValue()) {
                String c2 = um5.c(this.k, 2131886758);
                pq7.b(c2, "LanguageHelper.getString\u2026rackingToday_Text__Today)");
                aVar.l(c2);
            } else {
                aVar.l(jl5.b.i(i2));
            }
            aVar.i(String.valueOf(goalTrackingSummary.getTotalTracked()));
            aVar.h("/ " + goalTrackingSummary.getGoalTarget() + " " + um5.c(this.k, 2131886757));
            if (goalTrackingSummary.getGoalTarget() > 0) {
                if (goalTrackingSummary.getTotalTracked() >= goalTrackingSummary.getGoalTarget()) {
                    z = true;
                }
                aVar.n(z);
            } else {
                aVar.n(false);
            }
            if (goalTrackingSummary.getTotalTracked() <= 0) {
                aVar.m(true);
            }
        }
        return aVar;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r1v17 int), ('%' char)] */
    public final c w(GoalTrackingSummary goalTrackingSummary) {
        String str;
        c cVar = new c(null, null, null, null, 15, null);
        if (goalTrackingSummary != null) {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "calendar");
            instance.setTime(goalTrackingSummary.getDate());
            Boolean p0 = lk5.p0(instance.getTime());
            int i2 = instance.get(5);
            int i3 = instance.get(2);
            String N = lk5.N(i3);
            int i4 = instance.get(1);
            cVar.e(instance.getTime());
            instance.add(5, -6);
            int i5 = instance.get(5);
            int i6 = instance.get(2);
            String N2 = lk5.N(i6);
            int i7 = instance.get(1);
            cVar.f(instance.getTime());
            pq7.b(p0, "isToday");
            if (p0.booleanValue()) {
                str = um5.c(this.k, 2131886760);
                pq7.b(str, "LanguageHelper.getString\u2026ingToday_Title__ThisWeek)");
            } else if (i3 == i6) {
                str = N2 + ' ' + i5 + " - " + N2 + ' ' + i2;
            } else if (i7 == i4) {
                str = N2 + ' ' + i5 + " - " + N + ' ' + i2;
            } else {
                str = N2 + ' ' + i5 + ", " + i7 + " - " + N + ' ' + i2 + ", " + i4;
            }
            cVar.g(str);
            if (goalTrackingSummary.getTotalTargetOfWeek() > 0) {
                int b2 = lr7.b((((float) goalTrackingSummary.getTotalValueOfWeek()) / ((float) goalTrackingSummary.getTotalTargetOfWeek())) * ((float) 100));
                StringBuilder sb = new StringBuilder();
                sb.append(b2);
                sb.append('%');
                cVar.h(sb.toString());
            }
        }
        return cVar;
    }

    @DexIgnore
    public final void x(cu0<GoalTrackingSummary> cu0) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList - size=");
        sb.append(cu0 != null ? Integer.valueOf(cu0.size()) : null);
        local.d("DashboardGoalTrackingAdapter", sb.toString());
        super.i(cu0);
    }
}
