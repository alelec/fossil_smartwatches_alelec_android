package com.fossil;

import android.bluetooth.BluetoothGatt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b6 extends u5 {
    @DexIgnore
    public /* final */ n5 k; // = n5.VERY_HIGH;
    @DexIgnore
    public f5 l; // = f5.DISCONNECTED;

    @DexIgnore
    public b6(n4 n4Var) {
        super(v5.d, n4Var);
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void d(k5 k5Var) {
        int i = h5.b[k5Var.w.ordinal()];
        if (i == 1) {
            k5Var.b.post(new o4(k5Var, new g7(f7.SUCCESS, 0, 2), f5.DISCONNECTED));
        } else if (i == 2) {
            k5Var.c(3, 0);
            BluetoothGatt bluetoothGatt = k5Var.c;
            if (bluetoothGatt != null) {
                ky1 ky1 = ky1.DEBUG;
                bluetoothGatt.disconnect();
            }
            k5Var.c(0, 0);
        } else if (i == 3) {
            k5Var.c(3, 0);
            BluetoothGatt bluetoothGatt2 = k5Var.c;
            if (bluetoothGatt2 != null) {
                ky1 ky12 = ky1.DEBUG;
                bluetoothGatt2.disconnect();
            } else {
                ky1 ky13 = ky1.DEBUG;
            }
            k5Var.c(0, 0);
        }
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void f(h7 h7Var) {
        s5 a2;
        k(h7Var);
        g7 g7Var = h7Var.f1435a;
        if (g7Var.b == f7.SUCCESS) {
            a2 = this.l == f5.DISCONNECTED ? s5.a(this.e, null, r5.b, null, 5) : s5.a(this.e, null, r5.e, null, 5);
        } else {
            s5 a3 = s5.e.a(g7Var);
            a2 = s5.a(this.e, null, a3.c, a3.d, 1);
        }
        this.e = a2;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public n5 h() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public boolean i(h7 h7Var) {
        return (h7Var instanceof z6) && ((z6) h7Var).b == f5.DISCONNECTED;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public fd0<h7> j() {
        return this.j.f;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void k(h7 h7Var) {
        this.l = ((z6) h7Var).b;
    }
}
