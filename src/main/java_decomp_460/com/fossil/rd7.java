package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.NetworkInfo;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rd7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Picasso.LoadedFrom f3100a;
        @DexIgnore
        public /* final */ Bitmap b;
        @DexIgnore
        public /* final */ InputStream c;
        @DexIgnore
        public /* final */ int d;

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public a(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
            this(bitmap, null, loadedFrom, 0);
            xd7.d(bitmap, "bitmap == null");
        }

        @DexIgnore
        public a(Bitmap bitmap, InputStream inputStream, Picasso.LoadedFrom loadedFrom, int i) {
            boolean z = true;
            if ((inputStream == null ? false : z) ^ (bitmap != null)) {
                this.b = bitmap;
                this.c = inputStream;
                xd7.d(loadedFrom, "loadedFrom == null");
                this.f3100a = loadedFrom;
                this.d = i;
                return;
            }
            throw new AssertionError();
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public a(InputStream inputStream, Picasso.LoadedFrom loadedFrom) {
            this(null, inputStream, loadedFrom, 0);
            xd7.d(inputStream, "stream == null");
        }

        @DexIgnore
        public Bitmap a() {
            return this.b;
        }

        @DexIgnore
        public int b() {
            return this.d;
        }

        @DexIgnore
        public Picasso.LoadedFrom c() {
            return this.f3100a;
        }

        @DexIgnore
        public InputStream d() {
            return this.c;
        }
    }

    @DexIgnore
    public static void a(int i, int i2, int i3, int i4, BitmapFactory.Options options, pd7 pd7) {
        int i5;
        double floor;
        if (i4 > i2 || i3 > i) {
            if (i2 == 0) {
                floor = Math.floor((double) (((float) i3) / ((float) i)));
            } else if (i == 0) {
                floor = Math.floor((double) (((float) i4) / ((float) i2)));
            } else {
                int floor2 = (int) Math.floor((double) (((float) i4) / ((float) i2)));
                int floor3 = (int) Math.floor((double) (((float) i3) / ((float) i)));
                i5 = pd7.k ? Math.max(floor2, floor3) : Math.min(floor2, floor3);
            }
            i5 = (int) floor;
        } else {
            i5 = 1;
        }
        options.inSampleSize = i5;
        options.inJustDecodeBounds = false;
    }

    @DexIgnore
    public static void b(int i, int i2, BitmapFactory.Options options, pd7 pd7) {
        a(i, i2, options.outWidth, options.outHeight, options, pd7);
    }

    @DexIgnore
    public static BitmapFactory.Options d(pd7 pd7) {
        boolean c = pd7.c();
        boolean z = pd7.q != null;
        BitmapFactory.Options options = null;
        if (c || z) {
            options = new BitmapFactory.Options();
            options.inJustDecodeBounds = c;
            if (z) {
                options.inPreferredConfig = pd7.q;
            }
        }
        return options;
    }

    @DexIgnore
    public static boolean g(BitmapFactory.Options options) {
        return options != null && options.inJustDecodeBounds;
    }

    @DexIgnore
    public abstract boolean c(pd7 pd7);

    @DexIgnore
    public int e() {
        return 0;
    }

    @DexIgnore
    public abstract a f(pd7 pd7, int i) throws IOException;

    @DexIgnore
    public boolean h(boolean z, NetworkInfo networkInfo) {
        return false;
    }

    @DexIgnore
    public boolean i() {
        return false;
    }
}
