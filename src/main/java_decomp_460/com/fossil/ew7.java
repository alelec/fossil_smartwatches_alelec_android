package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ew7 extends iu7 {
    @DexIgnore
    public /* final */ dw7 b;

    @DexIgnore
    public ew7(dw7 dw7) {
        this.b = dw7;
    }

    @DexIgnore
    @Override // com.fossil.ju7
    public void a(Throwable th) {
        this.b.dispose();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
        a(th);
        return tl7.f3441a;
    }

    @DexIgnore
    public String toString() {
        return "DisposeOnCancel[" + this.b + ']';
    }
}
