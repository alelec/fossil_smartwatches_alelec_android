package com.fossil;

import android.database.Cursor;
import com.portfolio.platform.data.model.Firmware;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i97 implements h97 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f1596a;
    @DexIgnore
    public /* final */ jw0<g97> b;
    @DexIgnore
    public /* final */ xw0 c;
    @DexIgnore
    public /* final */ xw0 d;
    @DexIgnore
    public /* final */ xw0 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jw0<g97> {
        @DexIgnore
        public a(i97 i97, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, g97 g97) {
            if (g97.d() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, g97.d());
            }
            if (g97.c() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, g97.c());
            }
            if (g97.a() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, g97.a());
            }
            if (g97.g() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, g97.g());
            }
            if (g97.b() == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, g97.b());
            }
            if (g97.h() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, g97.h());
            }
            if (g97.e() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, g97.e());
            }
            px0.bindLong(8, (long) g97.f());
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `wf_background_photo` (`id`,`downloadUrl`,`checkSum`,`uid`,`createdAt`,`updatedAt`,`localPath`,`pinType`) VALUES (?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends xw0 {
        @DexIgnore
        public b(i97 i97, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM wf_background_photo WHERE id =?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends xw0 {
        @DexIgnore
        public c(i97 i97, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "UPDATE wf_background_photo SET pinType = 3 WHERE id =?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends xw0 {
        @DexIgnore
        public d(i97 i97, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM wf_background_photo";
        }
    }

    @DexIgnore
    public i97(qw0 qw0) {
        this.f1596a = qw0;
        this.b = new a(this, qw0);
        this.c = new b(this, qw0);
        this.d = new c(this, qw0);
        this.e = new d(this, qw0);
    }

    @DexIgnore
    @Override // com.fossil.h97
    public void a() {
        this.f1596a.assertNotSuspendingTransaction();
        px0 acquire = this.e.acquire();
        this.f1596a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f1596a.setTransactionSuccessful();
        } finally {
            this.f1596a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.h97
    public void b(String str) {
        this.f1596a.assertNotSuspendingTransaction();
        px0 acquire = this.c.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.f1596a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f1596a.setTransactionSuccessful();
        } finally {
            this.f1596a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.h97
    public long c(g97 g97) {
        this.f1596a.assertNotSuspendingTransaction();
        this.f1596a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(g97);
            this.f1596a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.f1596a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.h97
    public List<g97> d() {
        tw0 f = tw0.f("SELECT * FROM wf_background_photo WHERE pinType = 3", 0);
        this.f1596a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f1596a, f, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, Firmware.COLUMN_DOWNLOAD_URL);
            int c4 = dx0.c(b2, "checkSum");
            int c5 = dx0.c(b2, "uid");
            int c6 = dx0.c(b2, "createdAt");
            int c7 = dx0.c(b2, "updatedAt");
            int c8 = dx0.c(b2, "localPath");
            int c9 = dx0.c(b2, "pinType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new g97(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.getString(c6), b2.getString(c7), b2.getString(c8), b2.getInt(c9)));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.h97
    public List<g97> e() {
        tw0 f = tw0.f("SELECT * FROM wf_background_photo WHERE pinType = 1", 0);
        this.f1596a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f1596a, f, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, Firmware.COLUMN_DOWNLOAD_URL);
            int c4 = dx0.c(b2, "checkSum");
            int c5 = dx0.c(b2, "uid");
            int c6 = dx0.c(b2, "createdAt");
            int c7 = dx0.c(b2, "updatedAt");
            int c8 = dx0.c(b2, "localPath");
            int c9 = dx0.c(b2, "pinType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new g97(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.getString(c6), b2.getString(c7), b2.getString(c8), b2.getInt(c9)));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.h97
    public List<g97> f() {
        tw0 f = tw0.f("SELECT * FROM wf_background_photo WHERE pinType <> 3", 0);
        this.f1596a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f1596a, f, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, Firmware.COLUMN_DOWNLOAD_URL);
            int c4 = dx0.c(b2, "checkSum");
            int c5 = dx0.c(b2, "uid");
            int c6 = dx0.c(b2, "createdAt");
            int c7 = dx0.c(b2, "updatedAt");
            int c8 = dx0.c(b2, "localPath");
            int c9 = dx0.c(b2, "pinType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new g97(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.getString(c6), b2.getString(c7), b2.getString(c8), b2.getInt(c9)));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.h97
    public g97 g(String str) {
        g97 g97 = null;
        tw0 f = tw0.f("SELECT * FROM wf_background_photo WHERE id = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.f1596a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f1596a, f, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, Firmware.COLUMN_DOWNLOAD_URL);
            int c4 = dx0.c(b2, "checkSum");
            int c5 = dx0.c(b2, "uid");
            int c6 = dx0.c(b2, "createdAt");
            int c7 = dx0.c(b2, "updatedAt");
            int c8 = dx0.c(b2, "localPath");
            int c9 = dx0.c(b2, "pinType");
            if (b2.moveToFirst()) {
                g97 = new g97(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.getString(c6), b2.getString(c7), b2.getString(c8), b2.getInt(c9));
            }
            return g97;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.h97
    public void h(String str) {
        this.f1596a.assertNotSuspendingTransaction();
        px0 acquire = this.d.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.f1596a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f1596a.setTransactionSuccessful();
        } finally {
            this.f1596a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.h97
    public Long[] insert(List<g97> list) {
        this.f1596a.assertNotSuspendingTransaction();
        this.f1596a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.f1596a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.f1596a.endTransaction();
        }
    }
}
