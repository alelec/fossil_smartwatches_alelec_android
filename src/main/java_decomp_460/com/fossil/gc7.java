package com.fossil;

import android.graphics.Rect;
import android.graphics.Typeface;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.s87;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gc7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MutableLiveData<fb7> f1292a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Rect> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<fb7> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<u37<s87.c>> d;
    @DexIgnore
    public /* final */ LiveData<u37<s87.c>> e;
    @DexIgnore
    public /* final */ MutableLiveData<u37<s87.b>> f;
    @DexIgnore
    public /* final */ MutableLiveData<u37<s87.b>> g;
    @DexIgnore
    public /* final */ MutableLiveData<s87> h;
    @DexIgnore
    public /* final */ LiveData<s87> i;
    @DexIgnore
    public /* final */ MutableLiveData<Typeface> j;
    @DexIgnore
    public /* final */ LiveData<Typeface> k;
    @DexIgnore
    public /* final */ MutableLiveData<o87> l;
    @DexIgnore
    public /* final */ LiveData<o87> m;
    @DexIgnore
    public /* final */ MutableLiveData<Object> n;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> o;
    @DexIgnore
    public /* final */ MutableLiveData<String> p;
    @DexIgnore
    public /* final */ MutableLiveData<eb7> q;
    @DexIgnore
    public /* final */ MutableLiveData<List<DianaAppSetting>> r;
    @DexIgnore
    public /* final */ MutableLiveData<cb7> s;
    @DexIgnore
    public /* final */ MutableLiveData<u37<Boolean>> t;
    @DexIgnore
    public /* final */ LiveData<u37<Boolean>> u;
    @DexIgnore
    public MutableLiveData<String> v;

    @DexIgnore
    public gc7() {
        MutableLiveData<u37<s87.c>> mutableLiveData = new MutableLiveData<>();
        this.d = mutableLiveData;
        this.e = mutableLiveData;
        MutableLiveData<u37<s87.b>> mutableLiveData2 = new MutableLiveData<>();
        this.f = mutableLiveData2;
        this.g = mutableLiveData2;
        MutableLiveData<s87> mutableLiveData3 = new MutableLiveData<>();
        this.h = mutableLiveData3;
        this.i = mutableLiveData3;
        MutableLiveData<Typeface> mutableLiveData4 = new MutableLiveData<>();
        this.j = mutableLiveData4;
        this.k = mutableLiveData4;
        MutableLiveData<o87> mutableLiveData5 = new MutableLiveData<>();
        this.l = mutableLiveData5;
        this.m = mutableLiveData5;
        this.n = new MutableLiveData<>();
        this.o = new MutableLiveData<>();
        this.p = new MutableLiveData<>();
        this.q = new MutableLiveData<>();
        this.r = new MutableLiveData<>();
        this.s = new MutableLiveData<>();
        MutableLiveData<u37<Boolean>> mutableLiveData6 = new MutableLiveData<>();
        this.t = mutableLiveData6;
        this.u = mutableLiveData6;
        this.v = new MutableLiveData<>();
    }

    @DexIgnore
    public final void A(cb7 cb7) {
        this.s.l(cb7);
    }

    @DexIgnore
    public final void B(fb7 fb7) {
        pq7.c(fb7, "wfWrapper");
        this.f1292a.l(fb7);
    }

    @DexIgnore
    public final void C(o87 o87) {
        pq7.c(o87, "colorSpace");
        this.l.l(o87);
    }

    @DexIgnore
    public final void D(Typeface typeface) {
        pq7.c(typeface, "typeface");
        this.j.l(typeface);
    }

    @DexIgnore
    public final void E(eb7 eb7) {
        pq7.c(eb7, Constants.EVENT);
        this.q.l(eb7);
    }

    @DexIgnore
    public final void F() {
        this.t.l(new u37<>(Boolean.TRUE));
    }

    @DexIgnore
    public final void a(s87.b bVar) {
        pq7.c(bVar, "stickerConfig");
        this.f.l(new u37<>(bVar));
    }

    @DexIgnore
    public final void b(s87.c cVar) {
        pq7.c(cVar, "textConfig");
        this.d.l(new u37<>(cVar));
    }

    @DexIgnore
    public final MutableLiveData<u37<s87.b>> c() {
        return this.g;
    }

    @DexIgnore
    public final LiveData<u37<s87.c>> d() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<fb7> e() {
        return this.f1292a;
    }

    @DexIgnore
    public final LiveData<Object> f() {
        return this.n;
    }

    @DexIgnore
    public final LiveData<List<DianaAppSetting>> g() {
        return this.r;
    }

    @DexIgnore
    public final LiveData<Rect> h() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<s87> i() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<Boolean> j() {
        return this.o;
    }

    @DexIgnore
    public final LiveData<String> k() {
        return this.p;
    }

    @DexIgnore
    public final LiveData<fb7> l() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<u37<Boolean>> m() {
        return this.u;
    }

    @DexIgnore
    public final LiveData<cb7> n() {
        return this.s;
    }

    @DexIgnore
    public final LiveData<o87> o() {
        return this.m;
    }

    @DexIgnore
    public final LiveData<Typeface> p() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<eb7> q() {
        return this.q;
    }

    @DexIgnore
    public final MutableLiveData<String> r() {
        return this.v;
    }

    @DexIgnore
    public final void s() {
        this.n.l(new Object());
    }

    @DexIgnore
    public final void t(List<DianaAppSetting> list) {
        pq7.c(list, Constants.USER_SETTING);
        this.r.l(list);
    }

    @DexIgnore
    public final void u(Rect rect) {
        pq7.c(rect, "rect");
        this.b.l(rect);
    }

    @DexIgnore
    public final void v(s87 s87) {
        this.h.l(s87);
    }

    @DexIgnore
    public final void w(boolean z) {
        this.o.l(Boolean.valueOf(z));
    }

    @DexIgnore
    public final void x(String str) {
        this.v.l(str);
    }

    @DexIgnore
    public final void y(String str) {
        pq7.c(str, "complicationId");
        this.p.l(str);
    }

    @DexIgnore
    public final void z(fb7 fb7) {
        this.c.l(fb7);
    }
}
