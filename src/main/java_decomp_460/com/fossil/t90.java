package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t90 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ s90 CREATOR; // = new s90(null);
    @DexIgnore
    public /* final */ tu1[] b;
    @DexIgnore
    public /* final */ ry1 c;
    @DexIgnore
    public /* final */ List<byte[]> d; // = new ArrayList();
    @DexIgnore
    public /* final */ List<byte[]> e; // = new ArrayList();
    @DexIgnore
    public /* final */ List<byte[]> f; // = new ArrayList();

    @DexIgnore
    public t90(tu1[] tu1Arr, ry1 ry1) {
        this.b = tu1Arr;
        this.c = ry1;
        for (tu1 tu1 : tu1Arr) {
            ArrayList<u90> arrayList = new ArrayList();
            wu1[] microAppDeclarations = tu1.getMicroAppDeclarations();
            for (wu1 wu1 : microAppDeclarations) {
                o90 o90 = new o90(wu1.a(), wu1.getMicroAppVersion(), wu1.getVariationNumber());
                arrayList.add(new u90(o90));
                this.d.add(wu1.getData());
                vu1 customization = wu1.getCustomization();
                if (customization != null) {
                    customization.a(o90);
                    this.e.add(customization.a());
                }
            }
            xu1 microAppButton = tu1.getMicroAppButton();
            byte[] bArr = new byte[0];
            for (u90 u90 : arrayList) {
                bArr = dy1.a(bArr, u90.f3552a);
            }
            ByteBuffer order = ByteBuffer.allocate(bArr.length + 2).order(ByteOrder.LITTLE_ENDIAN);
            pq7.b(order, "ByteBuffer.allocate(2 + \u2026(ByteOrder.LITTLE_ENDIAN)");
            order.put(microAppButton.a());
            order.put((byte) arrayList.size());
            order.put(bArr);
            byte[] array = order.array();
            pq7.b(array, "byteBuffer.array()");
            this.f.add(array);
        }
    }

    @DexIgnore
    public final byte[] a(List<byte[]> list) {
        byte[] bArr = new byte[0];
        for (byte[] bArr2 : list) {
            bArr = dy1.a(bArr, bArr2);
        }
        ByteBuffer order = ByteBuffer.allocate(bArr.length + 1).order(ByteOrder.LITTLE_ENDIAN);
        order.put((byte) list.size());
        order.put(bArr);
        byte[] array = order.array();
        pq7.b(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(t90.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            t90 t90 = (t90) obj;
            if (!pq7.a(this.c, t90.c)) {
                return false;
            }
            return Arrays.equals(this.b, t90.b);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.configuration.MicroAppConfiguration");
    }

    @DexIgnore
    public int hashCode() {
        return (this.c.hashCode() * 31) + this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONArray jSONArray = new JSONArray();
        for (tu1 tu1 : this.b) {
            jSONArray.put(tu1.toJSONObject());
        }
        return g80.k(g80.k(g80.k(new JSONObject(), jd0.Z3, this.c.toJSONObject()), jd0.a4, jSONArray), jd0.b4, Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeTypedArray(this.b, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }
}
