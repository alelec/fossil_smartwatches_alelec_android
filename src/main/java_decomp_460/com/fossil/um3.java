package com.fossil;

import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class um3 implements Callable<List<hr3>> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ or3 f3614a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ qm3 d;

    @DexIgnore
    public um3(qm3 qm3, or3 or3, String str, String str2) {
        this.d = qm3;
        this.f3614a = or3;
        this.b = str;
        this.c = str2;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public final /* synthetic */ List<hr3> call() throws Exception {
        this.d.b.d0();
        return this.d.b.U().I(this.f3614a.b, this.b, this.c);
    }
}
