package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hk0 {
    @DexIgnore
    public static /* final */ int[] ConstraintLayout_Layout; // = {16842948, 16843039, 16843040, 16843071, 16843072, 2130968766, 2130968767, 2130968884, 2130968973, 2130968974, 2130969336, 2130969337, 2130969338, 2130969339, 2130969340, 2130969341, 2130969342, 2130969343, 2130969344, 2130969345, 2130969346, 2130969347, 2130969348, 2130969349, 2130969350, 2130969351, 2130969352, 2130969353, 2130969354, 2130969355, 2130969356, 2130969357, 2130969358, 2130969359, 2130969360, 2130969361, 2130969362, 2130969363, 2130969364, 2130969365, 2130969366, 2130969367, 2130969368, 2130969369, 2130969370, 2130969371, 2130969372, 2130969373, 2130969374, 2130969375, 2130969376, 2130969378, 2130969379, 2130969380, 2130969381, 2130969382, 2130969383, 2130969384, 2130969385, 2130969396};
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_maxHeight; // = 2;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_maxWidth; // = 1;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_minHeight; // = 4;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_minWidth; // = 3;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_barrierAllowsGoneWidgets; // = 5;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_barrierDirection; // = 6;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_chainUseRtl; // = 7;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_constraintSet; // = 8;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_constraint_referenced_ids; // = 9;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constrainedHeight; // = 10;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constrainedWidth; // = 11;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBaseline_creator; // = 12;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf; // = 13;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBottom_creator; // = 14;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBottom_toBottomOf; // = 15;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBottom_toTopOf; // = 16;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintCircle; // = 17;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintCircleAngle; // = 18;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintCircleRadius; // = 19;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintDimensionRatio; // = 20;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintEnd_toEndOf; // = 21;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintEnd_toStartOf; // = 22;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintGuide_begin; // = 23;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintGuide_end; // = 24;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintGuide_percent; // = 25;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_default; // = 26;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_max; // = 27;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_min; // = 28;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_percent; // = 29;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHorizontal_bias; // = 30;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle; // = 31;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHorizontal_weight; // = 32;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintLeft_creator; // = 33;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintLeft_toLeftOf; // = 34;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintLeft_toRightOf; // = 35;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintRight_creator; // = 36;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintRight_toLeftOf; // = 37;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintRight_toRightOf; // = 38;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintStart_toEndOf; // = 39;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintStart_toStartOf; // = 40;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintTop_creator; // = 41;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintTop_toBottomOf; // = 42;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintTop_toTopOf; // = 43;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintVertical_bias; // = 44;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintVertical_chainStyle; // = 45;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintVertical_weight; // = 46;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_default; // = 47;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_max; // = 48;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_min; // = 49;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_percent; // = 50;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_editor_absoluteX; // = 51;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_editor_absoluteY; // = 52;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginBottom; // = 53;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginEnd; // = 54;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginLeft; // = 55;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginRight; // = 56;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginStart; // = 57;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginTop; // = 58;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_optimizationLevel; // = 59;
    @DexIgnore
    public static /* final */ int[] ConstraintLayout_placeholder; // = {2130968977, 2130969141};
    @DexIgnore
    public static /* final */ int ConstraintLayout_placeholder_content; // = 0;
    @DexIgnore
    public static /* final */ int ConstraintLayout_placeholder_emptyVisibility; // = 1;
    @DexIgnore
    public static /* final */ int[] ConstraintSet; // = {16842948, 16842960, 16842972, 16842996, 16842997, 16842999, 16843000, 16843001, 16843002, 16843039, 16843040, 16843071, 16843072, 16843551, 16843552, 16843553, 16843554, 16843555, 16843556, 16843557, 16843558, 16843559, 16843560, 16843701, 16843702, 16843770, 16843840, 2130968766, 2130968767, 2130968884, 2130968974, 2130969336, 2130969337, 2130969338, 2130969339, 2130969340, 2130969341, 2130969342, 2130969343, 2130969344, 2130969345, 2130969346, 2130969347, 2130969348, 2130969349, 2130969350, 2130969351, 2130969352, 2130969353, 2130969354, 2130969355, 2130969356, 2130969357, 2130969358, 2130969359, 2130969360, 2130969361, 2130969362, 2130969363, 2130969364, 2130969365, 2130969366, 2130969367, 2130969368, 2130969369, 2130969370, 2130969371, 2130969372, 2130969373, 2130969374, 2130969375, 2130969376, 2130969378, 2130969379, 2130969380, 2130969381, 2130969382, 2130969383, 2130969384, 2130969385};
    @DexIgnore
    public static /* final */ int ConstraintSet_android_alpha; // = 13;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_elevation; // = 26;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_id; // = 1;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_height; // = 4;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginBottom; // = 8;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginEnd; // = 24;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginLeft; // = 5;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginRight; // = 7;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginStart; // = 23;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginTop; // = 6;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_width; // = 3;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_maxHeight; // = 10;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_maxWidth; // = 9;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_minHeight; // = 12;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_minWidth; // = 11;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_rotation; // = 20;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_rotationX; // = 21;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_rotationY; // = 22;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_scaleX; // = 18;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_scaleY; // = 19;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_transformPivotX; // = 14;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_transformPivotY; // = 15;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_translationX; // = 16;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_translationY; // = 17;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_translationZ; // = 25;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_visibility; // = 2;
    @DexIgnore
    public static /* final */ int ConstraintSet_barrierAllowsGoneWidgets; // = 27;
    @DexIgnore
    public static /* final */ int ConstraintSet_barrierDirection; // = 28;
    @DexIgnore
    public static /* final */ int ConstraintSet_chainUseRtl; // = 29;
    @DexIgnore
    public static /* final */ int ConstraintSet_constraint_referenced_ids; // = 30;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constrainedHeight; // = 31;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constrainedWidth; // = 32;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBaseline_creator; // = 33;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBaseline_toBaselineOf; // = 34;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBottom_creator; // = 35;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBottom_toBottomOf; // = 36;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBottom_toTopOf; // = 37;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintCircle; // = 38;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintCircleAngle; // = 39;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintCircleRadius; // = 40;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintDimensionRatio; // = 41;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintEnd_toEndOf; // = 42;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintEnd_toStartOf; // = 43;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintGuide_begin; // = 44;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintGuide_end; // = 45;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintGuide_percent; // = 46;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_default; // = 47;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_max; // = 48;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_min; // = 49;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_percent; // = 50;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHorizontal_bias; // = 51;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHorizontal_chainStyle; // = 52;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHorizontal_weight; // = 53;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintLeft_creator; // = 54;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintLeft_toLeftOf; // = 55;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintLeft_toRightOf; // = 56;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintRight_creator; // = 57;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintRight_toLeftOf; // = 58;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintRight_toRightOf; // = 59;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintStart_toEndOf; // = 60;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintStart_toStartOf; // = 61;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintTop_creator; // = 62;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintTop_toBottomOf; // = 63;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintTop_toTopOf; // = 64;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintVertical_bias; // = 65;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintVertical_chainStyle; // = 66;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintVertical_weight; // = 67;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_default; // = 68;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_max; // = 69;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_min; // = 70;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_percent; // = 71;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_editor_absoluteX; // = 72;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_editor_absoluteY; // = 73;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginBottom; // = 74;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginEnd; // = 75;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginLeft; // = 76;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginRight; // = 77;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginStart; // = 78;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginTop; // = 79;
    @DexIgnore
    public static /* final */ int[] LinearConstraintLayout; // = {16842948};
    @DexIgnore
    public static /* final */ int LinearConstraintLayout_android_orientation; // = 0;
}
