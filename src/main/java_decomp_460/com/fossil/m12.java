package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m12 implements Factory<v12> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<t32> f2287a;

    @DexIgnore
    public m12(Provider<t32> provider) {
        this.f2287a = provider;
    }

    @DexIgnore
    public static v12 a(t32 t32) {
        v12 a2 = l12.a(t32);
        lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public static m12 b(Provider<t32> provider) {
        return new m12(provider);
    }

    @DexIgnore
    /* renamed from: c */
    public v12 get() {
        return a(this.f2287a.get());
    }
}
