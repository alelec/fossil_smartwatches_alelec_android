package com.fossil;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ra2 extends LifecycleCallback implements DialogInterface.OnCancelListener {
    @DexIgnore
    public volatile boolean c;
    @DexIgnore
    public /* final */ AtomicReference<qa2> d;
    @DexIgnore
    public /* final */ Handler e;
    @DexIgnore
    public /* final */ c62 f;

    @DexIgnore
    public ra2(o72 o72) {
        this(o72, c62.q());
    }

    @DexIgnore
    public ra2(o72 o72, c62 c62) {
        super(o72);
        this.d = new AtomicReference<>(null);
        this.e = new ol2(Looper.getMainLooper());
        this.f = c62;
    }

    @DexIgnore
    public static int l(qa2 qa2) {
        if (qa2 == null) {
            return -1;
        }
        return qa2.b();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0016  */
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void e(int r8, int r9, android.content.Intent r10) {
        /*
            r7 = this;
            r5 = 18
            r1 = 13
            r3 = 0
            r2 = 1
            java.util.concurrent.atomic.AtomicReference<com.fossil.qa2> r0 = r7.d
            java.lang.Object r0 = r0.get()
            com.fossil.qa2 r0 = (com.fossil.qa2) r0
            if (r8 == r2) goto L_0x0038
            r1 = 2
            if (r8 == r1) goto L_0x001a
        L_0x0013:
            r1 = r3
        L_0x0014:
            if (r1 == 0) goto L_0x0067
            r7.p()
        L_0x0019:
            return
        L_0x001a:
            com.fossil.c62 r1 = r7.f
            android.app.Activity r4 = r7.b()
            int r4 = r1.i(r4)
            if (r4 != 0) goto L_0x0036
            r1 = r2
        L_0x0027:
            if (r0 == 0) goto L_0x0019
            com.fossil.z52 r2 = r0.a()
            int r2 = r2.c()
            if (r2 != r5) goto L_0x0014
            if (r4 != r5) goto L_0x0014
            goto L_0x0019
        L_0x0036:
            r1 = r3
            goto L_0x0027
        L_0x0038:
            r4 = -1
            if (r9 != r4) goto L_0x003d
            r1 = r2
            goto L_0x0014
        L_0x003d:
            if (r9 != 0) goto L_0x0013
            if (r0 == 0) goto L_0x0019
            if (r10 == 0) goto L_0x0049
            java.lang.String r2 = "<<ResolutionFailureErrorDetail>>"
            int r1 = r10.getIntExtra(r2, r1)
        L_0x0049:
            com.fossil.qa2 r2 = new com.fossil.qa2
            com.fossil.z52 r4 = new com.fossil.z52
            r5 = 0
            com.fossil.z52 r6 = r0.a()
            java.lang.String r6 = r6.toString()
            r4.<init>(r1, r5, r6)
            int r0 = l(r0)
            r2.<init>(r4, r0)
            java.util.concurrent.atomic.AtomicReference<com.fossil.qa2> r0 = r7.d
            r0.set(r2)
            r0 = r2
            goto L_0x0013
        L_0x0067:
            if (r0 == 0) goto L_0x0019
            com.fossil.z52 r1 = r0.a()
            int r0 = r0.b()
            r7.m(r1, r0)
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ra2.e(int, int, android.content.Intent):void");
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void f(Bundle bundle) {
        super.f(bundle);
        if (bundle != null) {
            this.d.set(bundle.getBoolean("resolving_error", false) ? new qa2(new z52(bundle.getInt("failed_status"), (PendingIntent) bundle.getParcelable("failed_resolution")), bundle.getInt("failed_client_id", -1)) : null);
        }
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void i(Bundle bundle) {
        super.i(bundle);
        qa2 qa2 = this.d.get();
        if (qa2 != null) {
            bundle.putBoolean("resolving_error", true);
            bundle.putInt("failed_client_id", qa2.b());
            bundle.putInt("failed_status", qa2.a().c());
            bundle.putParcelable("failed_resolution", qa2.a().h());
        }
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void j() {
        super.j();
        this.c = true;
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void k() {
        super.k();
        this.c = false;
    }

    @DexIgnore
    public abstract void m(z52 z52, int i);

    @DexIgnore
    public final void n(z52 z52, int i) {
        qa2 qa2 = new qa2(z52, i);
        if (this.d.compareAndSet(null, qa2)) {
            this.e.post(new ta2(this, qa2));
        }
    }

    @DexIgnore
    public abstract void o();

    @DexIgnore
    public void onCancel(DialogInterface dialogInterface) {
        m(new z52(13, null), l(this.d.get()));
        p();
    }

    @DexIgnore
    public final void p() {
        this.d.set(null);
        o();
    }
}
