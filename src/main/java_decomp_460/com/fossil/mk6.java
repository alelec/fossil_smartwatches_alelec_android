package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mk6 implements Factory<lk6> {
    @DexIgnore
    public static lk6 a(jk6 jk6, UserRepository userRepository, SleepSummariesRepository sleepSummariesRepository, PortfolioApp portfolioApp) {
        return new lk6(jk6, userRepository, sleepSummariesRepository, portfolioApp);
    }
}
