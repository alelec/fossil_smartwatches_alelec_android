package com.fossil;

import com.facebook.internal.Utility;
import com.facebook.places.model.PlaceFields;
import com.misfit.frameworks.common.constants.Constants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f38 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ e38[] f1048a; // = {new e38(e38.i, ""), new e38(e38.f, "GET"), new e38(e38.f, "POST"), new e38(e38.g, "/"), new e38(e38.g, "/index.html"), new e38(e38.h, "http"), new e38(e38.h, Utility.URL_SCHEME), new e38(e38.e, "200"), new e38(e38.e, "204"), new e38(e38.e, "206"), new e38(e38.e, "304"), new e38(e38.e, "400"), new e38(e38.e, "404"), new e38(e38.e, "500"), new e38("accept-charset", ""), new e38("accept-encoding", "gzip, deflate"), new e38("accept-language", ""), new e38("accept-ranges", ""), new e38("accept", ""), new e38("access-control-allow-origin", ""), new e38("age", ""), new e38("allow", ""), new e38(Constants.IF_AUTHORIZATION, ""), new e38("cache-control", ""), new e38("content-disposition", ""), new e38("content-encoding", ""), new e38("content-language", ""), new e38("content-length", ""), new e38("content-location", ""), new e38("content-range", ""), new e38("content-type", ""), new e38("cookie", ""), new e38("date", ""), new e38(Constants.JSON_KEY_ETAG, ""), new e38("expect", ""), new e38("expires", ""), new e38("from", ""), new e38("host", ""), new e38("if-match", ""), new e38("if-modified-since", ""), new e38("if-none-match", ""), new e38("if-range", ""), new e38("if-unmodified-since", ""), new e38("last-modified", ""), new e38("link", ""), new e38(PlaceFields.LOCATION, ""), new e38("max-forwards", ""), new e38("proxy-authenticate", ""), new e38("proxy-authorization", ""), new e38("range", ""), new e38("referer", ""), new e38("refresh", ""), new e38("retry-after", ""), new e38("server", ""), new e38("set-cookie", ""), new e38("strict-transport-security", ""), new e38("transfer-encoding", ""), new e38("user-agent", ""), new e38("vary", ""), new e38("via", ""), new e38("www-authenticate", "")};
    @DexIgnore
    public static /* final */ Map<l48, Integer> b; // = b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<e38> f1049a;
        @DexIgnore
        public /* final */ k48 b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public e38[] e;
        @DexIgnore
        public int f;
        @DexIgnore
        public int g;
        @DexIgnore
        public int h;

        @DexIgnore
        public a(int i, int i2, c58 c58) {
            this.f1049a = new ArrayList();
            e38[] e38Arr = new e38[8];
            this.e = e38Arr;
            this.f = e38Arr.length - 1;
            this.g = 0;
            this.h = 0;
            this.c = i;
            this.d = i2;
            this.b = s48.d(c58);
        }

        @DexIgnore
        public a(int i, c58 c58) {
            this(i, i, c58);
        }

        @DexIgnore
        public final void a() {
            int i = this.d;
            int i2 = this.h;
            if (i >= i2) {
                return;
            }
            if (i == 0) {
                b();
            } else {
                d(i2 - i);
            }
        }

        @DexIgnore
        public final void b() {
            Arrays.fill(this.e, (Object) null);
            this.f = this.e.length - 1;
            this.g = 0;
            this.h = 0;
        }

        @DexIgnore
        public final int c(int i) {
            return this.f + 1 + i;
        }

        @DexIgnore
        public final int d(int i) {
            int i2 = 0;
            if (i > 0) {
                int length = this.e.length;
                while (true) {
                    length--;
                    if (length < this.f || i <= 0) {
                        e38[] e38Arr = this.e;
                        int i3 = this.f;
                        System.arraycopy(e38Arr, i3 + 1, e38Arr, i3 + 1 + i2, this.g);
                        this.f += i2;
                    } else {
                        e38[] e38Arr2 = this.e;
                        i -= e38Arr2[length].c;
                        this.h -= e38Arr2[length].c;
                        this.g--;
                        i2++;
                    }
                }
                e38[] e38Arr3 = this.e;
                int i32 = this.f;
                System.arraycopy(e38Arr3, i32 + 1, e38Arr3, i32 + 1 + i2, this.g);
                this.f += i2;
            }
            return i2;
        }

        @DexIgnore
        public List<e38> e() {
            ArrayList arrayList = new ArrayList(this.f1049a);
            this.f1049a.clear();
            return arrayList;
        }

        @DexIgnore
        public final l48 f(int i) throws IOException {
            if (h(i)) {
                return f38.f1048a[i].f876a;
            }
            int c2 = c(i - f38.f1048a.length);
            if (c2 >= 0) {
                e38[] e38Arr = this.e;
                if (c2 < e38Arr.length) {
                    return e38Arr[c2].f876a;
                }
            }
            throw new IOException("Header index too large " + (i + 1));
        }

        @DexIgnore
        public final void g(int i, e38 e38) {
            this.f1049a.add(e38);
            int i2 = e38.c;
            if (i != -1) {
                i2 -= this.e[c(i)].c;
            }
            int i3 = this.d;
            if (i2 > i3) {
                b();
                return;
            }
            int d2 = d((this.h + i2) - i3);
            if (i == -1) {
                int i4 = this.g;
                e38[] e38Arr = this.e;
                if (i4 + 1 > e38Arr.length) {
                    e38[] e38Arr2 = new e38[(e38Arr.length * 2)];
                    System.arraycopy(e38Arr, 0, e38Arr2, e38Arr.length, e38Arr.length);
                    this.f = this.e.length - 1;
                    this.e = e38Arr2;
                }
                int i5 = this.f;
                this.f = i5 - 1;
                this.e[i5] = e38;
                this.g++;
            } else {
                this.e[d2 + c(i) + i] = e38;
            }
            this.h = i2 + this.h;
        }

        @DexIgnore
        public final boolean h(int i) {
            return i >= 0 && i <= f38.f1048a.length + -1;
        }

        @DexIgnore
        public final int i() throws IOException {
            return this.b.readByte() & 255;
        }

        @DexIgnore
        public l48 j() throws IOException {
            int i = i();
            boolean z = (i & 128) == 128;
            int m = m(i, 127);
            return z ? l48.of(m38.f().c(this.b.W((long) m))) : this.b.i((long) m);
        }

        @DexIgnore
        public void k() throws IOException {
            while (!this.b.u()) {
                int readByte = this.b.readByte() & 255;
                if (readByte == 128) {
                    throw new IOException("index == 0");
                } else if ((readByte & 128) == 128) {
                    l(m(readByte, 127) - 1);
                } else if (readByte == 64) {
                    o();
                } else if ((readByte & 64) == 64) {
                    n(m(readByte, 63) - 1);
                } else if ((readByte & 32) == 32) {
                    int m = m(readByte, 31);
                    this.d = m;
                    if (m < 0 || m > this.c) {
                        throw new IOException("Invalid dynamic table size update " + this.d);
                    }
                    a();
                } else if (readByte == 16 || readByte == 0) {
                    q();
                } else {
                    p(m(readByte, 15) - 1);
                }
            }
        }

        @DexIgnore
        public final void l(int i) throws IOException {
            if (h(i)) {
                this.f1049a.add(f38.f1048a[i]);
                return;
            }
            int c2 = c(i - f38.f1048a.length);
            if (c2 >= 0) {
                e38[] e38Arr = this.e;
                if (c2 < e38Arr.length) {
                    this.f1049a.add(e38Arr[c2]);
                    return;
                }
            }
            throw new IOException("Header index too large " + (i + 1));
        }

        @DexIgnore
        public int m(int i, int i2) throws IOException {
            int i3 = i & i2;
            if (i3 < i2) {
                return i3;
            }
            int i4 = 0;
            while (true) {
                int i5 = i();
                if ((i5 & 128) == 0) {
                    return (i5 << i4) + i2;
                }
                i2 += (i5 & 127) << i4;
                i4 += 7;
            }
        }

        @DexIgnore
        public final void n(int i) throws IOException {
            g(-1, new e38(f(i), j()));
        }

        @DexIgnore
        public final void o() throws IOException {
            l48 j = j();
            f38.a(j);
            g(-1, new e38(j, j()));
        }

        @DexIgnore
        public final void p(int i) throws IOException {
            this.f1049a.add(new e38(f(i), j()));
        }

        @DexIgnore
        public final void q() throws IOException {
            l48 j = j();
            f38.a(j);
            this.f1049a.add(new e38(j, j()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ i48 f1050a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public int e;
        @DexIgnore
        public e38[] f;
        @DexIgnore
        public int g;
        @DexIgnore
        public int h;
        @DexIgnore
        public int i;

        @DexIgnore
        public b(int i2, boolean z, i48 i48) {
            this.c = Integer.MAX_VALUE;
            e38[] e38Arr = new e38[8];
            this.f = e38Arr;
            this.g = e38Arr.length - 1;
            this.h = 0;
            this.i = 0;
            this.e = i2;
            this.b = z;
            this.f1050a = i48;
        }

        @DexIgnore
        public b(i48 i48) {
            this(4096, true, i48);
        }

        @DexIgnore
        public final void a() {
            int i2 = this.e;
            int i3 = this.i;
            if (i2 >= i3) {
                return;
            }
            if (i2 == 0) {
                b();
            } else {
                c(i3 - i2);
            }
        }

        @DexIgnore
        public final void b() {
            Arrays.fill(this.f, (Object) null);
            this.g = this.f.length - 1;
            this.h = 0;
            this.i = 0;
        }

        @DexIgnore
        public final int c(int i2) {
            int i3 = 0;
            if (i2 > 0) {
                int length = this.f.length;
                while (true) {
                    length--;
                    if (length < this.g || i2 <= 0) {
                        e38[] e38Arr = this.f;
                        int i4 = this.g;
                        System.arraycopy(e38Arr, i4 + 1, e38Arr, i4 + 1 + i3, this.h);
                        e38[] e38Arr2 = this.f;
                        int i5 = this.g;
                        Arrays.fill(e38Arr2, i5 + 1, i5 + 1 + i3, (Object) null);
                        this.g += i3;
                    } else {
                        e38[] e38Arr3 = this.f;
                        i2 -= e38Arr3[length].c;
                        this.i -= e38Arr3[length].c;
                        this.h--;
                        i3++;
                    }
                }
                e38[] e38Arr4 = this.f;
                int i42 = this.g;
                System.arraycopy(e38Arr4, i42 + 1, e38Arr4, i42 + 1 + i3, this.h);
                e38[] e38Arr22 = this.f;
                int i52 = this.g;
                Arrays.fill(e38Arr22, i52 + 1, i52 + 1 + i3, (Object) null);
                this.g += i3;
            }
            return i3;
        }

        @DexIgnore
        public final void d(e38 e38) {
            int i2 = e38.c;
            int i3 = this.e;
            if (i2 > i3) {
                b();
                return;
            }
            c((this.i + i2) - i3);
            int i4 = this.h;
            e38[] e38Arr = this.f;
            if (i4 + 1 > e38Arr.length) {
                e38[] e38Arr2 = new e38[(e38Arr.length * 2)];
                System.arraycopy(e38Arr, 0, e38Arr2, e38Arr.length, e38Arr.length);
                this.g = this.f.length - 1;
                this.f = e38Arr2;
            }
            int i5 = this.g;
            this.g = i5 - 1;
            this.f[i5] = e38;
            this.h++;
            this.i = i2 + this.i;
        }

        @DexIgnore
        public void e(int i2) {
            int min = Math.min(i2, 16384);
            int i3 = this.e;
            if (i3 != min) {
                if (min < i3) {
                    this.c = Math.min(this.c, min);
                }
                this.d = true;
                this.e = min;
                a();
            }
        }

        @DexIgnore
        public void f(l48 l48) throws IOException {
            if (!this.b || m38.f().e(l48) >= l48.size()) {
                h(l48.size(), 127, 0);
                this.f1050a.t0(l48);
                return;
            }
            i48 i48 = new i48();
            m38.f().d(l48, i48);
            l48 S = i48.S();
            h(S.size(), 127, 128);
            this.f1050a.t0(S);
        }

        @DexIgnore
        public void g(List<e38> list) throws IOException {
            int i2;
            int i3;
            if (this.d) {
                int i4 = this.c;
                if (i4 < this.e) {
                    h(i4, 31, 32);
                }
                this.d = false;
                this.c = Integer.MAX_VALUE;
                h(this.e, 31, 32);
            }
            int size = list.size();
            for (int i5 = 0; i5 < size; i5++) {
                e38 e38 = list.get(i5);
                l48 asciiLowercase = e38.f876a.toAsciiLowercase();
                l48 l48 = e38.b;
                Integer num = f38.b.get(asciiLowercase);
                if (num != null) {
                    int intValue = num.intValue() + 1;
                    if (intValue > 1 && intValue < 8) {
                        if (b28.q(f38.f1048a[intValue - 1].b, l48)) {
                            i2 = intValue;
                            i3 = intValue;
                        } else if (b28.q(f38.f1048a[intValue].b, l48)) {
                            i2 = intValue + 1;
                            i3 = intValue;
                        }
                    }
                    i2 = -1;
                    i3 = intValue;
                } else {
                    i2 = -1;
                    i3 = -1;
                }
                if (i2 == -1) {
                    int i6 = this.g + 1;
                    int length = this.f.length;
                    while (true) {
                        if (i6 >= length) {
                            break;
                        }
                        if (b28.q(this.f[i6].f876a, asciiLowercase)) {
                            if (b28.q(this.f[i6].b, l48)) {
                                i2 = (i6 - this.g) + f38.f1048a.length;
                                break;
                            } else if (i3 == -1) {
                                i3 = (i6 - this.g) + f38.f1048a.length;
                            }
                        }
                        i6++;
                    }
                }
                if (i2 != -1) {
                    h(i2, 127, 128);
                } else if (i3 == -1) {
                    this.f1050a.w0(64);
                    f(asciiLowercase);
                    f(l48);
                    d(e38);
                } else if (!asciiLowercase.startsWith(e38.d) || e38.i.equals(asciiLowercase)) {
                    h(i3, 63, 64);
                    f(l48);
                    d(e38);
                } else {
                    h(i3, 15, 0);
                    f(l48);
                }
            }
        }

        @DexIgnore
        public void h(int i2, int i3, int i4) {
            if (i2 < i3) {
                this.f1050a.w0(i2 | i4);
                return;
            }
            this.f1050a.w0(i4 | i3);
            int i5 = i2 - i3;
            while (i5 >= 128) {
                this.f1050a.w0((i5 & 127) | 128);
                i5 >>>= 7;
            }
            this.f1050a.w0(i5);
        }
    }

    @DexIgnore
    public static l48 a(l48 l48) throws IOException {
        int size = l48.size();
        for (int i = 0; i < size; i++) {
            byte b2 = l48.getByte(i);
            if (b2 >= 65 && b2 <= 90) {
                throw new IOException("PROTOCOL_ERROR response malformed: mixed case name: " + l48.utf8());
            }
        }
        return l48;
    }

    @DexIgnore
    public static Map<l48, Integer> b() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(f1048a.length);
        int i = 0;
        while (true) {
            e38[] e38Arr = f1048a;
            if (i >= e38Arr.length) {
                return Collections.unmodifiableMap(linkedHashMap);
            }
            if (!linkedHashMap.containsKey(e38Arr[i].f876a)) {
                linkedHashMap.put(f1048a[i].f876a, Integer.valueOf(i));
            }
            i++;
        }
    }
}
