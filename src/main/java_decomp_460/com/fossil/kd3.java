package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface kd3 extends IInterface {
    @DexIgnore
    void onCancel() throws RemoteException;

    @DexIgnore
    void onFinish() throws RemoteException;
}
