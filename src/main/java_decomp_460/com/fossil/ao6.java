package com.fossil;

import com.fossil.bo6;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.MFUser;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ao6 extends rv5<zn6> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* synthetic */ void a(ao6 ao6, MFUser mFUser, String str, int i, Object obj) {
            if (obj == null) {
                if ((i & 2) != 0) {
                    str = null;
                }
                ao6.R4(mFUser, str);
                return;
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: updatePhoto");
        }
    }

    @DexIgnore
    Object E0();  // void declaration

    @DexIgnore
    void G4(ArrayList<bo6.b> arrayList);

    @DexIgnore
    void H3(boolean z, ps4 ps4, boolean z2);

    @DexIgnore
    void J4(yn6 yn6);

    @DexIgnore
    void Q2(boolean z);

    @DexIgnore
    void R4(MFUser mFUser, String str);

    @DexIgnore
    void U2(SleepStatistic sleepStatistic);

    @DexIgnore
    void X3(ActivityStatistic activityStatistic);

    @DexIgnore
    void Z3(boolean z, boolean z2);

    @DexIgnore
    Object i3();  // void declaration

    @DexIgnore
    void i5(String str);

    @DexIgnore
    Object k();  // void declaration

    @DexIgnore
    Object m();  // void declaration

    @DexIgnore
    void o(int i, String str);

    @DexIgnore
    void updateUser(MFUser mFUser);

    @DexIgnore
    Object z2();  // void declaration
}
