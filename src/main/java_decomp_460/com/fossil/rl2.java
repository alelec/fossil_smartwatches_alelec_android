package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rl2 implements IInterface {
    @DexIgnore
    public /* final */ IBinder b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public rl2(IBinder iBinder, String str) {
        this.b = iBinder;
        this.c = str;
    }

    @DexIgnore
    public IBinder asBinder() {
        return this.b;
    }

    @DexIgnore
    public final Parcel d() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.c);
        return obtain;
    }

    @DexIgnore
    public final Parcel e(int i, Parcel parcel) throws RemoteException {
        parcel = Parcel.obtain();
        try {
            this.b.transact(i, parcel, parcel, 0);
            parcel.readException();
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }
}
