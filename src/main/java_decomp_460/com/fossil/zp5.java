package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zp5 implements MembersInjector<NetworkChangedReceiver> {
    @DexIgnore
    public static void a(NetworkChangedReceiver networkChangedReceiver, UserRepository userRepository) {
        networkChangedReceiver.f4701a = userRepository;
    }
}
