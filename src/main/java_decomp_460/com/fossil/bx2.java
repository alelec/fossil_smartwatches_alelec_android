package com.fossil;

import java.io.Serializable;
import java.util.Arrays;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bx2<T> implements xw2<T>, Serializable {
    @DexIgnore
    @NullableDecl
    public /* final */ T zza;

    @DexIgnore
    public bx2(@NullableDecl T t) {
        this.zza = t;
    }

    @DexIgnore
    public final boolean equals(@NullableDecl Object obj) {
        if (obj instanceof bx2) {
            return qw2.a(this.zza, ((bx2) obj).zza);
        }
        return false;
    }

    @DexIgnore
    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.zza});
    }

    @DexIgnore
    public final String toString() {
        String valueOf = String.valueOf(this.zza);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 22);
        sb.append("Suppliers.ofInstance(");
        sb.append(valueOf);
        sb.append(")");
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.xw2
    public final T zza() {
        return this.zza;
    }
}
