package com.fossil;

import com.bumptech.glide.load.ImageHeaderParser;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ti1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<ImageHeaderParser> f3418a; // = new ArrayList();

    @DexIgnore
    public void a(ImageHeaderParser imageHeaderParser) {
        synchronized (this) {
            this.f3418a.add(imageHeaderParser);
        }
    }

    @DexIgnore
    public List<ImageHeaderParser> b() {
        List<ImageHeaderParser> list;
        synchronized (this) {
            list = this.f3418a;
        }
        return list;
    }
}
