package com.fossil;

import android.os.Bundle;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface so3 {
    @DexIgnore
    List<Bundle> a(String str, String str2);

    @DexIgnore
    int b(String str);

    @DexIgnore
    void c(Bundle bundle);

    @DexIgnore
    void d(String str);

    @DexIgnore
    Map<String, Object> e(String str, String str2, boolean z);

    @DexIgnore
    void f(String str, String str2, Bundle bundle);

    @DexIgnore
    void g(String str, String str2, Bundle bundle);

    @DexIgnore
    String zza();

    @DexIgnore
    void zza(String str);

    @DexIgnore
    String zzb();

    @DexIgnore
    String zzc();

    @DexIgnore
    String zzd();

    @DexIgnore
    long zze();
}
