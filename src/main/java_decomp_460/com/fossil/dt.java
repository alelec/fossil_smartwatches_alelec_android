package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dt extends bt {
    @DexIgnore
    public byte[] K; // = new byte[0];
    @DexIgnore
    public /* final */ byte[] L;

    @DexIgnore
    public dt(k5 k5Var, byte[] bArr) {
        super(k5Var, ut.EXCHANGE_PUBLIC_KEYS, hs.J, 0, 8);
        this.L = bArr;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(super.A(), jd0.s2, dy1.e(this.K, null, 1, null));
    }

    @DexIgnore
    @Override // com.fossil.ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 32) {
            byte[] k = dm7.k(bArr, 0, 32);
            this.K = k;
            g80.k(jSONObject, jd0.s2, dy1.e(k, null, 1, null));
            this.v = mw.a(this.v, null, null, lw.b, null, null, 27);
        } else {
            this.v = mw.a(this.v, null, null, lw.k, null, null, 27);
        }
        this.E = true;
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(this.L.length).order(ByteOrder.LITTLE_ENDIAN).put(this.L).array();
        pq7.b(array, "ByteBuffer.allocate(phon\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(super.z(), jd0.r2, dy1.e(this.L, null, 1, null));
    }
}
