package com.fossil;

import androidx.activity.OnBackPressedDispatcher;
import androidx.lifecycle.LifecycleOwner;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ke0 extends LifecycleOwner {
    @DexIgnore
    OnBackPressedDispatcher getOnBackPressedDispatcher();
}
