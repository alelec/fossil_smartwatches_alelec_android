package com.fossil;

import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ey1 {
    @DexIgnore
    public static final String a(Enum<?> r2) {
        pq7.c(r2, "$this$lowerCaseName");
        String name = r2.name();
        Locale b = dx1.b();
        if (name != null) {
            String lowerCase = name.toLowerCase(b);
            pq7.b(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            return lowerCase;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }
}
