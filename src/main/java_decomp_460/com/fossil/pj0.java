package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.tj0;
import com.fossil.uj0;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pj0 {
    @DexIgnore
    public static void a(vj0 vj0) {
        if ((vj0.R0() & 32) != 32) {
            j(vj0);
            return;
        }
        vj0.D0 = true;
        vj0.x0 = false;
        vj0.y0 = false;
        vj0.z0 = false;
        ArrayList<uj0> arrayList = vj0.k0;
        List<wj0> list = vj0.w0;
        boolean z = vj0.s() == uj0.b.WRAP_CONTENT;
        boolean z2 = vj0.B() == uj0.b.WRAP_CONTENT;
        boolean z3 = z || z2;
        list.clear();
        for (uj0 uj0 : arrayList) {
            uj0.p = null;
            uj0.d0 = false;
            uj0.S();
        }
        for (uj0 uj02 : arrayList) {
            if (uj02.p == null && !b(uj02, list, z3)) {
                j(vj0);
                vj0.D0 = false;
                return;
            }
        }
        int i = 0;
        int i2 = 0;
        for (wj0 wj0 : list) {
            i2 = Math.max(i2, c(wj0, 0));
            i = Math.max(i, c(wj0, 1));
        }
        if (z) {
            vj0.g0(uj0.b.FIXED);
            vj0.y0(i2);
            vj0.x0 = true;
            vj0.y0 = true;
            vj0.A0 = i2;
        }
        if (z2) {
            vj0.u0(uj0.b.FIXED);
            vj0.b0(i);
            vj0.x0 = true;
            vj0.z0 = true;
            vj0.B0 = i;
        }
        i(list, 0, vj0.D());
        i(list, 1, vj0.r());
    }

    @DexIgnore
    public static boolean b(uj0 uj0, List<wj0> list, boolean z) {
        wj0 wj0 = new wj0(new ArrayList(), true);
        list.add(wj0);
        return k(uj0, wj0, list, z);
    }

    @DexIgnore
    public static int c(wj0 wj0, int i) {
        int i2 = i * 2;
        List<uj0> b = wj0.b(i);
        int size = b.size();
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            uj0 uj0 = b.get(i4);
            tj0[] tj0Arr = uj0.A;
            int i5 = i2 + 1;
            i3 = Math.max(i3, d(uj0, i, tj0Arr[i5].d == null || !(tj0Arr[i2].d == null || tj0Arr[i5].d == null), 0));
        }
        wj0.e[i] = i3;
        return i3;
    }

    @DexIgnore
    public static int d(uj0 uj0, int i, boolean z, int i2) {
        int r;
        int j;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int D;
        int i8;
        int i9;
        int i10;
        if (!uj0.b0) {
            return 0;
        }
        boolean z2 = uj0.w.d != null && i == 1;
        if (z) {
            r = uj0.j();
            j = uj0.r() - uj0.j();
            i4 = i * 2;
            i3 = i4 + 1;
        } else {
            r = uj0.r() - uj0.j();
            j = uj0.j();
            i3 = i * 2;
            i4 = i3 + 1;
        }
        tj0[] tj0Arr = uj0.A;
        if (tj0Arr[i3].d == null || tj0Arr[i4].d != null) {
            i5 = 1;
            i6 = i3;
            i7 = i4;
        } else {
            i5 = -1;
            i6 = i4;
            i7 = i3;
        }
        if (z2) {
            i2 -= r;
        }
        int d = (uj0.A[i7].d() * i5) + e(uj0, i);
        int i11 = i2 + d;
        int D2 = (i == 0 ? uj0.D() : uj0.r()) * i5;
        Iterator<ck0> it = uj0.A[i7].f().f623a.iterator();
        int i12 = 0;
        while (it.hasNext()) {
            i12 = Math.max(i12, d(((ak0) it.next()).c.b, i, z, i11));
        }
        Iterator<ck0> it2 = uj0.A[i6].f().f623a.iterator();
        int i13 = 0;
        while (it2.hasNext()) {
            i13 = Math.max(i13, d(((ak0) it2.next()).c.b, i, z, D2 + i11));
        }
        if (z2) {
            i12 -= r;
            D = i13 + j;
        } else {
            D = ((i == 0 ? uj0.D() : uj0.r()) * i5) + i13;
        }
        if (i == 1) {
            Iterator<ck0> it3 = uj0.w.f().f623a.iterator();
            int i14 = 0;
            while (true) {
                i8 = i14;
                if (!it3.hasNext()) {
                    break;
                }
                ak0 ak0 = (ak0) it3.next();
                i14 = i5 == 1 ? Math.max(i8, d(ak0.c.b, i, z, r + i11)) : Math.max(i8, d(ak0.c.b, i, z, (j * i5) + i11));
            }
            if (uj0.w.f().f623a.size() > 0 && !z2) {
                i8 = i5 == 1 ? i8 + r : i8 - j;
            }
        } else {
            i8 = 0;
        }
        int max = Math.max(i12, Math.max(D, i8));
        int i15 = D2 + i11;
        if (i5 == -1) {
            i9 = i15;
            i10 = i11;
        } else {
            i9 = i11;
            i10 = i15;
        }
        if (z) {
            zj0.e(uj0, i, i9);
            uj0.Z(i9, i10, i);
        } else {
            uj0.p.a(uj0, i);
            uj0.q0(i9, i);
        }
        if (uj0.o(i) == uj0.b.MATCH_CONSTRAINT && uj0.G != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            uj0.p.a(uj0, i);
        }
        tj0[] tj0Arr2 = uj0.A;
        if (!(tj0Arr2[i7].d == null || tj0Arr2[i6].d == null)) {
            uj0 u = uj0.u();
            tj0[] tj0Arr3 = uj0.A;
            if (tj0Arr3[i7].d.b == u && tj0Arr3[i6].d.b == u) {
                uj0.p.a(uj0, i);
            }
        }
        return d + max;
    }

    @DexIgnore
    public static int e(uj0 uj0, int i) {
        uj0 uj02;
        tj0 tj0;
        int i2 = i * 2;
        tj0[] tj0Arr = uj0.A;
        tj0 tj02 = tj0Arr[i2];
        tj0 tj03 = tj0Arr[i2 + 1];
        tj0 tj04 = tj02.d;
        if (tj04 == null || tj04.b != (uj02 = uj0.D) || (tj0 = tj03.d) == null || tj0.b != uj02) {
            return 0;
        }
        return (int) ((i == 0 ? uj0.V : uj0.W) * ((float) (((uj02.t(i) - tj02.d()) - tj03.d()) - uj0.t(i))));
    }

    @DexIgnore
    public static void f(vj0 vj0, uj0 uj0, wj0 wj0) {
        wj0.d = false;
        vj0.D0 = false;
        uj0.b0 = false;
    }

    @DexIgnore
    public static int g(uj0 uj0) {
        if (uj0.s() == uj0.b.MATCH_CONSTRAINT) {
            int r = (int) (uj0.H == 0 ? ((float) uj0.r()) * uj0.G : ((float) uj0.r()) / uj0.G);
            uj0.y0(r);
            return r;
        } else if (uj0.B() != uj0.b.MATCH_CONSTRAINT) {
            return -1;
        } else {
            int D = (int) (uj0.H == 1 ? ((float) uj0.D()) * uj0.G : ((float) uj0.D()) / uj0.G);
            uj0.b0(D);
            return D;
        }
    }

    @DexIgnore
    public static void h(tj0 tj0) {
        ak0 f = tj0.f();
        tj0 tj02 = tj0.d;
        if (tj02 != null && tj02.d != tj0) {
            tj02.f().a(f);
        }
    }

    @DexIgnore
    public static void i(List<wj0> list, int i, int i2) {
        int size = list.size();
        for (int i3 = 0; i3 < size; i3++) {
            for (uj0 uj0 : list.get(i3).c(i)) {
                if (uj0.b0) {
                    l(uj0, i, i2);
                }
            }
        }
    }

    @DexIgnore
    public static void j(vj0 vj0) {
        vj0.w0.clear();
        vj0.w0.add(0, new wj0(vj0.k0));
    }

    @DexIgnore
    public static boolean k(uj0 uj0, wj0 wj0, List<wj0> list, boolean z) {
        tj0 tj0;
        tj0 tj02;
        tj0 tj03;
        uj0 uj02;
        tj0 tj04;
        tj0 tj05;
        tj0 tj06;
        tj0 tj07;
        uj0 uj03;
        tj0 tj08;
        if (uj0 == null) {
            return true;
        }
        uj0.c0 = false;
        vj0 vj0 = (vj0) uj0.u();
        wj0 wj02 = uj0.p;
        if (wj02 == null) {
            uj0.b0 = true;
            wj0.f3947a.add(uj0);
            uj0.p = wj0;
            if (uj0.s.d == null && uj0.u.d == null && uj0.t.d == null && uj0.v.d == null && uj0.w.d == null && uj0.z.d == null) {
                f(vj0, uj0, wj0);
                if (z) {
                    return false;
                }
            }
            if (!(uj0.t.d == null || uj0.v.d == null)) {
                vj0.B();
                uj0.b bVar = uj0.b.WRAP_CONTENT;
                if (z) {
                    f(vj0, uj0, wj0);
                    return false;
                } else if (!(uj0.t.d.b == uj0.u() && uj0.v.d.b == uj0.u())) {
                    f(vj0, uj0, wj0);
                }
            }
            if (!(uj0.s.d == null || uj0.u.d == null)) {
                vj0.s();
                uj0.b bVar2 = uj0.b.WRAP_CONTENT;
                if (z) {
                    f(vj0, uj0, wj0);
                    return false;
                } else if (!(uj0.s.d.b == uj0.u() && uj0.u.d.b == uj0.u())) {
                    f(vj0, uj0, wj0);
                }
            }
            if (((uj0.s() == uj0.b.MATCH_CONSTRAINT) ^ (uj0.B() == uj0.b.MATCH_CONSTRAINT)) && uj0.G != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                g(uj0);
            } else if (uj0.s() == uj0.b.MATCH_CONSTRAINT || uj0.B() == uj0.b.MATCH_CONSTRAINT) {
                f(vj0, uj0, wj0);
                if (z) {
                    return false;
                }
            }
            if (((uj0.s.d == null && uj0.u.d == null) || (((tj05 = uj0.s.d) != null && tj05.b == uj0.D && uj0.u.d == null) || (((tj06 = uj0.u.d) != null && tj06.b == uj0.D && uj0.s.d == null) || ((tj07 = uj0.s.d) != null && tj07.b == (uj03 = uj0.D) && (tj08 = uj0.u.d) != null && tj08.b == uj03)))) && uj0.z.d == null && !(uj0 instanceof xj0) && !(uj0 instanceof yj0)) {
                wj0.f.add(uj0);
            }
            if (((uj0.t.d == null && uj0.v.d == null) || (((tj0 = uj0.t.d) != null && tj0.b == uj0.D && uj0.v.d == null) || (((tj02 = uj0.v.d) != null && tj02.b == uj0.D && uj0.t.d == null) || ((tj03 = uj0.t.d) != null && tj03.b == (uj02 = uj0.D) && (tj04 = uj0.v.d) != null && tj04.b == uj02)))) && uj0.z.d == null && uj0.w.d == null && !(uj0 instanceof xj0) && !(uj0 instanceof yj0)) {
                wj0.g.add(uj0);
            }
            if (uj0 instanceof yj0) {
                f(vj0, uj0, wj0);
                if (z) {
                    return false;
                }
                yj0 yj0 = (yj0) uj0;
                for (int i = 0; i < yj0.l0; i++) {
                    if (!k(yj0.k0[i], wj0, list, z)) {
                        return false;
                    }
                }
            }
            int length = uj0.A.length;
            for (int i2 = 0; i2 < length; i2++) {
                tj0 tj09 = uj0.A[i2];
                tj0 tj010 = tj09.d;
                if (!(tj010 == null || tj010.b == uj0.u())) {
                    if (tj09.c == tj0.d.CENTER) {
                        f(vj0, uj0, wj0);
                        if (z) {
                            return false;
                        }
                    } else {
                        h(tj09);
                    }
                    if (!k(tj09.d.b, wj0, list, z)) {
                        return false;
                    }
                }
            }
            return true;
        }
        if (wj02 != wj0) {
            wj0.f3947a.addAll(wj02.f3947a);
            wj0.f.addAll(uj0.p.f);
            wj0.g.addAll(uj0.p.g);
            if (!uj0.p.d) {
                wj0.d = false;
            }
            list.remove(uj0.p);
            for (uj0 uj04 : uj0.p.f3947a) {
                uj04.p = wj0;
            }
        }
        return true;
    }

    @DexIgnore
    public static void l(uj0 uj0, int i, int i2) {
        int i3 = i * 2;
        tj0[] tj0Arr = uj0.A;
        tj0 tj0 = tj0Arr[i3];
        tj0 tj02 = tj0Arr[i3 + 1];
        if ((tj0.d == null || tj02.d == null) ? false : true) {
            zj0.e(uj0, i, e(uj0, i) + tj0.d());
        } else if (uj0.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || uj0.o(i) != uj0.b.MATCH_CONSTRAINT) {
            int v = i2 - uj0.v(i);
            int t = v - uj0.t(i);
            uj0.Z(t, v, i);
            zj0.e(uj0, i, t);
        } else {
            int g = g(uj0);
            int i4 = (int) uj0.A[i3].f().g;
            tj02.f().f = tj0.f();
            tj02.f().g = (float) g;
            tj02.f().b = 1;
            uj0.Z(i4, g + i4, i);
        }
    }
}
