package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gs4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1351a;
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public gs4(String str, Object obj, boolean z) {
        pq7.c(str, "name");
        pq7.c(obj, "data");
        this.f1351a = str;
        this.b = obj;
        this.c = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ gs4(String str, Object obj, boolean z, int i, kq7 kq7) {
        this(str, obj, (i & 4) != 0 ? false : z);
    }

    @DexIgnore
    public final Object a() {
        return this.b;
    }

    @DexIgnore
    public final String b() {
        return this.f1351a;
    }

    @DexIgnore
    public final boolean c() {
        return this.c;
    }

    @DexIgnore
    public final void d(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof gs4) {
                gs4 gs4 = (gs4) obj;
                if (!pq7.a(this.f1351a, gs4.f1351a) || !pq7.a(this.b, gs4.b) || this.c != gs4.c) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f1351a;
        int hashCode = str != null ? str.hashCode() : 0;
        Object obj = this.b;
        if (obj != null) {
            i = obj.hashCode();
        }
        boolean z = this.c;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((hashCode * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "BottomDialogModel(name=" + this.f1351a + ", data=" + this.b + ", isChecked=" + this.c + ")";
    }
}
