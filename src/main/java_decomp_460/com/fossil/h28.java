package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h28 extends m48 {
    @DexIgnore
    public boolean c;

    @DexIgnore
    public h28(a58 a58) {
        super(a58);
    }

    @DexIgnore
    @Override // com.fossil.m48, com.fossil.a58
    public void K(i48 i48, long j) throws IOException {
        if (this.c) {
            i48.skip(j);
            return;
        }
        try {
            super.K(i48, j);
        } catch (IOException e) {
            this.c = true;
            a(e);
        }
    }

    @DexIgnore
    public abstract void a(IOException iOException);

    @DexIgnore
    @Override // com.fossil.m48, com.fossil.a58, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (!this.c) {
            try {
                super.close();
            } catch (IOException e) {
                this.c = true;
                a(e);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.m48, com.fossil.a58, java.io.Flushable
    public void flush() throws IOException {
        if (!this.c) {
            try {
                super.flush();
            } catch (IOException e) {
                this.c = true;
                a(e);
            }
        }
    }
}
