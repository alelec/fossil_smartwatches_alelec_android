package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oq1 extends jq1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ju1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<oq1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public oq1 createFromParcel(Parcel parcel) {
            return new oq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public oq1[] newArray(int i) {
            return new oq1[i];
        }
    }

    @DexIgnore
    public oq1(byte b, int i, ju1 ju1) {
        super(np1.RING_PHONE, b, i);
        this.e = ju1;
    }

    @DexIgnore
    public /* synthetic */ oq1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.e = ju1.values()[parcel.readInt()];
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(oq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.e == ((oq1) obj).e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.RingPhoneRequest");
    }

    @DexIgnore
    public final ju1 getAction() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public int hashCode() {
        return (super.hashCode() * 31) + this.e.hashCode();
    }
}
