package com.fossil;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.ContactProviderImpl;
import com.fossil.wearables.fsl.contact.EmailAddress;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.table.TableUtils;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fp5 extends ContactProviderImpl {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f1163a; // = "fp5";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements UpgradeCommand {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
        public void execute(SQLiteDatabase sQLiteDatabase) {
            FLogger.INSTANCE.getLocal().d(fp5.f1163a, " ---- UPGRADE DB ENTOURAGE, table CONTACTGROUP");
            sQLiteDatabase.execSQL("ALTER TABLE contactgroup ADD COLUMN deviceFamily int");
            FLogger.INSTANCE.getLocal().d(fp5.f1163a, " ---- UPGRADE DB ENTOURAGE, table CONTACTGROUP SUCCESS");
            StringBuilder sb = new StringBuilder();
            String J = PortfolioApp.d0.J();
            if (!TextUtils.isEmpty(J)) {
                sb.append("UPDATE ");
                sb.append("contactgroup");
                sb.append(" SET deviceFamily = ");
                sb.append(DeviceIdentityUtils.getDeviceFamily(J).ordinal());
                sQLiteDatabase.execSQL(sb.toString());
                return;
            }
            fp5.this.removeAllContactGroups();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements UpgradeCommand {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
        public void execute(SQLiteDatabase sQLiteDatabase) {
            int C;
            try {
                FLogger.INSTANCE.getLocal().d(fp5.f1163a, "Inside .doInBackground upgrade contact");
                List<ln0<Integer, Integer>> B = fp5.this.B(sQLiteDatabase);
                Cursor query = sQLiteDatabase.query(true, "contactgroup", new String[]{"id", BaseFeatureModel.COLUMN_COLOR, "name", BaseFeatureModel.COLUMN_HAPTIC, "timestamp", "enabled", "deviceFamily"}, null, null, null, null, null, null);
                ArrayList<ContactGroup> arrayList = new ArrayList();
                int i = -1;
                String J = PortfolioApp.d0.J();
                if (!TextUtils.isEmpty(J)) {
                    i = nk5.o.e(J).getValue();
                }
                String str = "deviceFamily";
                String str2 = "enabled";
                String str3 = "timestamp";
                String str4 = "name";
                if (query == null || i <= 0 || !nk5.o.y(J)) {
                    str = "deviceFamily";
                    str2 = "enabled";
                    str3 = "timestamp";
                    str4 = "name";
                } else {
                    query.moveToFirst();
                    while (!query.isAfterLast()) {
                        String string = query.getString(query.getColumnIndex(BaseFeatureModel.COLUMN_COLOR));
                        String string2 = query.getString(query.getColumnIndex(str4));
                        String string3 = query.getString(query.getColumnIndex(BaseFeatureModel.COLUMN_HAPTIC));
                        int i2 = query.getInt(query.getColumnIndex(str3));
                        int i3 = query.getInt(query.getColumnIndex(str2));
                        int i4 = query.getInt(query.getColumnIndex(str));
                        int i5 = query.getInt(query.getColumnIndex("id"));
                        if (i4 == i) {
                            ContactGroup contactGroup = new ContactGroup();
                            contactGroup.setColor(string);
                            contactGroup.setName(string2);
                            contactGroup.setHaptic(string3);
                            contactGroup.setTimestamp((long) i2);
                            boolean z = true;
                            if (i3 != 1) {
                                z = false;
                            }
                            contactGroup.setEnabled(z);
                            contactGroup.setDeviceFamily(i4);
                            contactGroup.setDbRowId(i5);
                            if (!B.isEmpty() && (C = fp5.this.C(B, contactGroup.getDbRowId())) > 0) {
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String str5 = fp5.f1163a;
                                local.d(str5, "Find contact, contactId=" + C);
                                gp5 d = mn5.p.a().h().d(String.valueOf(C), MFDeviceFamily.fromInt(i4).toString());
                                if (d != null) {
                                    contactGroup.setHour(d.d());
                                    contactGroup.setVibrationOnly(d.f());
                                }
                            }
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str6 = fp5.f1163a;
                            local2.d(str6, "Add contact=" + contactGroup);
                            if (!contactGroup.isVibrationOnly()) {
                                arrayList.add(contactGroup);
                            }
                        }
                        query.moveToNext();
                    }
                    query.close();
                }
                sQLiteDatabase.execSQL("CREATE TABLE contactgroup_copy (id INTEGER PRIMARY KEY AUTOINCREMENT, color VARCHAR, haptic VARCHAR, timestamp BIGINT, enabled INTEGER, name VARCHAR, deviceFamily INTEGER, hour INTEGER, isVibrationOnly INTEGER);");
                if (!arrayList.isEmpty()) {
                    for (ContactGroup contactGroup2 : arrayList) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(BaseFeatureModel.COLUMN_COLOR, contactGroup2.getColor());
                        contentValues.put(BaseFeatureModel.COLUMN_HAPTIC, contactGroup2.getHaptic());
                        contentValues.put(str3, Long.valueOf(contactGroup2.getTimestamp()));
                        contentValues.put(str4, contactGroup2.getName());
                        contentValues.put(str2, Boolean.valueOf(contactGroup2.isEnabled()));
                        contentValues.put(str, Integer.valueOf(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()));
                        contentValues.put(AppFilter.COLUMN_HOUR, Integer.valueOf(contactGroup2.getHour()));
                        contentValues.put(AppFilter.COLUMN_IS_VIBRATION_ONLY, Boolean.valueOf(contactGroup2.isVibrationOnly()));
                        contentValues.put("id", Integer.valueOf(contactGroup2.getDbRowId()));
                        sQLiteDatabase.insert("contactgroup_copy", null, contentValues);
                    }
                }
                sQLiteDatabase.execSQL("DROP TABLE contactgroup;");
                sQLiteDatabase.execSQL("ALTER TABLE contactgroup_copy RENAME TO contactgroup;");
                FLogger.INSTANCE.getLocal().d(fp5.f1163a, "Migration complete");
            } catch (Exception e) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str7 = fp5.f1163a;
                local3.e(str7, "Error inside " + fp5.f1163a + ".upgrade - e=" + e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements UpgradeCommand {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Callable<Boolean> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ SQLiteDatabase f1167a;
            @DexIgnore
            public /* final */ /* synthetic */ List b;

            @DexIgnore
            public a(c cVar, SQLiteDatabase sQLiteDatabase, List list) {
                this.f1167a = sQLiteDatabase;
                this.b = list;
            }

            @DexIgnore
            /* renamed from: a */
            public Boolean call() {
                try {
                    this.f1167a.execSQL("CREATE TABLE unique_contact (id INTEGER PRIMARY KEY AUTOINCREMENT, firstName VARCHAR, lastName VARCHAR, contactId BIGINT, companyName VARCHAR, photoThumbUri VARCHAR, useCall INTEGER, useEmail INTEGER, useSms INTEGER, contact_group_id INTEGER);");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = fp5.f1163a;
                    local.d(str, "Create new contact unique table complete, uniqueContactListSize=" + this.b.size());
                    for (Contact contact : this.b) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("id", Integer.valueOf(contact.getDbRowId()));
                        contentValues.put("firstname", contact.getFirstName());
                        contentValues.put(Constants.PROFILE_KEY_LAST_NAME, contact.getLastName());
                        contentValues.put("contactId", Integer.valueOf(contact.getContactId()));
                        contentValues.put("companyName", contact.getCompanyName());
                        contentValues.put("photoThumbUri", contact.getPhotoThumbUri());
                        contentValues.put("useCall", Boolean.valueOf(contact.isUseCall()));
                        contentValues.put("useEmail", Boolean.valueOf(contact.isUseEmail()));
                        contentValues.put("useSms", Boolean.valueOf(contact.isUseSms()));
                        contentValues.put("contact_group_id", Integer.valueOf(contact.getContactGroup().getDbRowId()));
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = fp5.f1163a;
                        local2.d(str2, "Put contact with contact id=" + contact.getDbRowId() + ", contactGroupId=" + contact.getContactGroup().getDbRowId());
                        this.f1167a.insert("unique_contact", null, contentValues);
                    }
                    this.f1167a.execSQL("DROP TABLE contact;");
                    this.f1167a.execSQL("ALTER TABLE unique_contact RENAME TO contact;");
                    FLogger.INSTANCE.getLocal().d(fp5.f1163a, "Done remove duplicate for contact table");
                    return Boolean.TRUE;
                } catch (Exception e) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = fp5.f1163a;
                    local3.e(str3, "Exception when execute transaction put contact e=" + e);
                    return Boolean.FALSE;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements Callable<Boolean> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ SQLiteDatabase f1168a;
            @DexIgnore
            public /* final */ /* synthetic */ List b;

            @DexIgnore
            public b(c cVar, SQLiteDatabase sQLiteDatabase, List list) {
                this.f1168a = sQLiteDatabase;
                this.b = list;
            }

            @DexIgnore
            /* renamed from: a */
            public Boolean call() {
                try {
                    this.f1168a.execSQL("CREATE TABLE unique_contactgroup (id INTEGER PRIMARY KEY AUTOINCREMENT, color VARCHAR, haptic VARCHAR, timestamp BIGINT, enabled INTEGER, name VARCHAR, deviceFamily INTEGER, hour INTEGER, isVibrationOnly INTEGER);");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = fp5.f1163a;
                    local.d(str, "Create new contact group unique table complete, uniqueContactGroupListSize=" + this.b.size());
                    for (ContactGroup contactGroup : this.b) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(BaseFeatureModel.COLUMN_COLOR, contactGroup.getColor());
                        contentValues.put(BaseFeatureModel.COLUMN_HAPTIC, contactGroup.getHaptic());
                        contentValues.put("timestamp", Long.valueOf(contactGroup.getTimestamp()));
                        contentValues.put("name", contactGroup.getName());
                        contentValues.put("enabled", Boolean.valueOf(contactGroup.isEnabled()));
                        contentValues.put("deviceFamily", Integer.valueOf(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()));
                        contentValues.put(AppFilter.COLUMN_HOUR, Integer.valueOf(contactGroup.getHour()));
                        contentValues.put(AppFilter.COLUMN_IS_VIBRATION_ONLY, Boolean.valueOf(contactGroup.isVibrationOnly()));
                        contentValues.put("id", Integer.valueOf(contactGroup.getDbRowId()));
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = fp5.f1163a;
                        local2.d(str2, "Put contact group with contact group id=" + contactGroup.getDbRowId());
                        this.f1168a.insert("unique_contactgroup", null, contentValues);
                    }
                    this.f1168a.execSQL("DROP TABLE contactgroup;");
                    this.f1168a.execSQL("ALTER TABLE unique_contactgroup RENAME TO contactgroup;");
                    return Boolean.TRUE;
                } catch (Exception e) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = fp5.f1163a;
                    local3.e(str3, "Exception when execute put unique contact group, e=" + e);
                    return Boolean.FALSE;
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.fp5$c$c")
        /* renamed from: com.fossil.fp5$c$c  reason: collision with other inner class name */
        public class CallableC0088c implements Callable<Boolean> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ SQLiteDatabase f1169a;
            @DexIgnore
            public /* final */ /* synthetic */ List b;

            @DexIgnore
            public CallableC0088c(c cVar, SQLiteDatabase sQLiteDatabase, List list) {
                this.f1169a = sQLiteDatabase;
                this.b = list;
            }

            @DexIgnore
            /* renamed from: a */
            public Boolean call() {
                try {
                    this.f1169a.execSQL("CREATE TABLE unique_phone_number (id INTEGER PRIMARY KEY AUTOINCREMENT, phone_number_id INTEGER, number VARCHAR);");
                    for (PhoneNumber phoneNumber : this.b) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("id", Integer.valueOf(phoneNumber.getDbRowId()));
                        contentValues.put("phone_number_id", Integer.valueOf(phoneNumber.getContact().getDbRowId()));
                        contentValues.put("number", phoneNumber.getNumber());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = fp5.f1163a;
                        local.d(str, "Put phone number with contact id=" + phoneNumber.getContact().getDbRowId() + ", id=" + phoneNumber.getDbRowId());
                        this.f1169a.insert("unique_phone_number", null, contentValues);
                    }
                    this.f1169a.execSQL("DROP TABLE phonenumber;");
                    this.f1169a.execSQL("ALTER TABLE unique_phone_number RENAME TO phonenumber;");
                    return Boolean.TRUE;
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = fp5.f1163a;
                    local2.e(str2, "Exception when execute insert phone number, e=" + e);
                    return Boolean.FALSE;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class d implements Callable<Boolean> {
            @DexIgnore
            public d() {
            }

            @DexIgnore
            /* renamed from: a */
            public Boolean call() {
                try {
                    TableUtils.clearTable(fp5.this.databaseHelper.getConnectionSource(), EmailAddress.class);
                    return Boolean.TRUE;
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = fp5.f1163a;
                    local.e(str, "Exception when drop email address table, e=" + e);
                    return Boolean.FALSE;
                }
            }
        }

        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
        public void execute(SQLiteDatabase sQLiteDatabase) {
            FLogger.INSTANCE.getLocal().d(fp5.f1163a, "------- Enter upgrade database version 5 --------");
            try {
                FLogger.INSTANCE.getLocal().d(fp5.f1163a, "------- Start upgrade database version 5 --------");
                Cursor query = sQLiteDatabase.query(true, "contact", new String[]{"id", Constants.PROFILE_KEY_FIRST_NAME, Constants.PROFILE_KEY_LAST_NAME, "contactId", "companyName", "photoThumbUri", "useCall", "useEmail", "useSms", "contact_group_id"}, null, null, "contactId", null, null, null);
                FLogger.INSTANCE.getLocal().d(fp5.f1163a, "Query cursor for contact success");
                ArrayList arrayList = new ArrayList();
                if (query != null) {
                    query.moveToFirst();
                    while (!query.isAfterLast()) {
                        try {
                            int i = query.getInt(query.getColumnIndex("id"));
                            String string = query.getString(query.getColumnIndex(Constants.PROFILE_KEY_FIRST_NAME));
                            String string2 = query.getString(query.getColumnIndex(Constants.PROFILE_KEY_LAST_NAME));
                            int i2 = query.getInt(query.getColumnIndex("contactId"));
                            String string3 = query.getString(query.getColumnIndex("companyName"));
                            String string4 = query.getString(query.getColumnIndex("photoThumbUri"));
                            int i3 = query.getInt(query.getColumnIndex("useCall"));
                            int i4 = query.getInt(query.getColumnIndex("useEmail"));
                            int i5 = query.getInt(query.getColumnIndex("useSms"));
                            int i6 = query.getInt(query.getColumnIndex("contact_group_id"));
                            Contact contact = new Contact();
                            contact.setContactId(i2);
                            contact.setLastName(string2);
                            contact.setFirstName(string);
                            contact.setCompanyName(string3);
                            contact.setPhotoThumbUri(string4);
                            contact.setUseCall(i3 == 1);
                            contact.setUseEmail(i4 == 1);
                            contact.setUseSms(i5 == 1);
                            ContactGroup contactGroup = new ContactGroup();
                            contactGroup.setDbRowId(i6);
                            contact.setContactGroup(contactGroup);
                            contact.setDbRowId(i);
                            arrayList.add(contact);
                            query.moveToNext();
                        } catch (Exception e) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String str = fp5.f1163a;
                            local.e(str, "Exception when query cursor contact, e=" + e);
                            fp5.this.D();
                            return;
                        } finally {
                            query.close();
                        }
                    }
                }
                if (!arrayList.isEmpty()) {
                    try {
                        TransactionManager.callInTransaction(fp5.this.databaseHelper.getConnectionSource(), new a(this, sQLiteDatabase, arrayList));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        fp5.this.D();
                        return;
                    }
                }
                Cursor rawQuery = sQLiteDatabase.rawQuery("select contactgroup.id, contactgroup.color, contactgroup.deviceFamily, contactgroup.enabled, contactgroup.haptic, contactgroup.hour, contactgroup.isVibrationOnly, contactgroup.name, contactgroup.timestamp from contactgroup,contact where contact.contact_group_id = contactgroup.id", new String[0]);
                FLogger.INSTANCE.getLocal().d(fp5.f1163a, "Query cursor for contact group success");
                ArrayList arrayList2 = new ArrayList();
                if (rawQuery != null) {
                    rawQuery.moveToFirst();
                    while (!rawQuery.isAfterLast()) {
                        try {
                            String string5 = rawQuery.getString(rawQuery.getColumnIndex(BaseFeatureModel.COLUMN_COLOR));
                            String string6 = rawQuery.getString(rawQuery.getColumnIndex("name"));
                            String string7 = rawQuery.getString(rawQuery.getColumnIndex(BaseFeatureModel.COLUMN_HAPTIC));
                            int i7 = rawQuery.getInt(rawQuery.getColumnIndex("timestamp"));
                            int i8 = rawQuery.getInt(rawQuery.getColumnIndex("enabled"));
                            int i9 = rawQuery.getInt(rawQuery.getColumnIndex("deviceFamily"));
                            int i10 = rawQuery.getInt(rawQuery.getColumnIndex("id"));
                            int i11 = rawQuery.getInt(rawQuery.getColumnIndex(AppFilter.COLUMN_HOUR));
                            int i12 = rawQuery.getInt(rawQuery.getColumnIndex(AppFilter.COLUMN_IS_VIBRATION_ONLY));
                            ContactGroup contactGroup2 = new ContactGroup();
                            contactGroup2.setColor(string5);
                            contactGroup2.setName(string6);
                            contactGroup2.setHaptic(string7);
                            contactGroup2.setTimestamp((long) i7);
                            contactGroup2.setEnabled(i8 == 1);
                            contactGroup2.setDeviceFamily(i9);
                            contactGroup2.setDbRowId(i10);
                            contactGroup2.setHour(i11);
                            contactGroup2.setVibrationOnly(i12 == 1);
                            arrayList2.add(contactGroup2);
                            rawQuery.moveToNext();
                        } catch (Exception e3) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str2 = fp5.f1163a;
                            local2.e(str2, "Exception when query cursor contact group, e=" + e3);
                            fp5.this.D();
                            return;
                        } finally {
                            rawQuery.close();
                        }
                    }
                }
                if (!arrayList2.isEmpty()) {
                    try {
                        TransactionManager.callInTransaction(fp5.this.databaseHelper.getConnectionSource(), new b(this, sQLiteDatabase, arrayList2));
                    } catch (Exception e4) {
                        e4.printStackTrace();
                        fp5.this.D();
                        return;
                    }
                }
                Cursor rawQuery2 = sQLiteDatabase.rawQuery("select phonenumber.id, phonenumber.number, phonenumber.phone_number_id from phonenumber,contact where contact.id = phonenumber.phone_number_id ", new String[0]);
                ArrayList arrayList3 = new ArrayList();
                FLogger.INSTANCE.getLocal().d(fp5.f1163a, "Query cursor for phone number success");
                if (rawQuery2 != null) {
                    rawQuery2.moveToFirst();
                    while (!rawQuery2.isAfterLast()) {
                        try {
                            int i13 = rawQuery2.getInt(rawQuery2.getColumnIndex("phone_number_id"));
                            String string8 = rawQuery2.getString(rawQuery2.getColumnIndex("number"));
                            int i14 = rawQuery2.getInt(rawQuery2.getColumnIndex("id"));
                            Contact contact2 = new Contact();
                            contact2.setDbRowId(i13);
                            PhoneNumber phoneNumber = new PhoneNumber();
                            phoneNumber.setDbRowId(i14);
                            phoneNumber.setNumber(string8);
                            phoneNumber.setContact(contact2);
                            arrayList3.add(phoneNumber);
                            rawQuery2.moveToNext();
                        } catch (Exception e5) {
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String str3 = fp5.f1163a;
                            local3.e(str3, "Exception when query cursor phone number, e=" + e5);
                            fp5.this.D();
                            return;
                        } finally {
                            rawQuery2.close();
                        }
                    }
                }
                if (!arrayList3.isEmpty()) {
                    try {
                        TransactionManager.callInTransaction(fp5.this.databaseHelper.getConnectionSource(), new CallableC0088c(this, sQLiteDatabase, arrayList3));
                    } catch (Exception e6) {
                        e6.printStackTrace();
                        fp5.this.D();
                        return;
                    }
                }
                try {
                    TransactionManager.callInTransaction(fp5.this.databaseHelper.getConnectionSource(), new d());
                } catch (Exception e7) {
                    e7.printStackTrace();
                }
                FLogger.INSTANCE.getLocal().d(fp5.f1163a, "------- Done upgrade database version 5 --------");
            } catch (Exception e8) {
                e8.printStackTrace();
                fp5.this.D();
            }
            FLogger.INSTANCE.getLocal().d(fp5.f1163a, "------- Exit upgrade database version 5 --------");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Callable<Boolean> {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean call() {
            try {
                TableUtils.clearTable(fp5.this.databaseHelper.getConnectionSource(), ContactGroup.class);
                TableUtils.clearTable(fp5.this.databaseHelper.getConnectionSource(), Contact.class);
                TableUtils.clearTable(fp5.this.databaseHelper.getConnectionSource(), PhoneNumber.class);
                TableUtils.clearTable(fp5.this.databaseHelper.getConnectionSource(), EmailAddress.class);
                return Boolean.TRUE;
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = fp5.f1163a;
                local.e(str, "Inside .forceClearAllTables exception when drop all table, e=" + e);
                return Boolean.FALSE;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Callable<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ StringBuilder f1172a;

        @DexIgnore
        public e(StringBuilder sb) {
            this.f1172a = sb;
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean call() {
            try {
                Dao contactDao = fp5.this.getContactDao();
                contactDao.queryRaw("DELETE FROM contact WHERE contactId NOT IN " + ((Object) this.f1172a), new String[0]);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = fp5.f1163a;
                local.d(str, ".Inside step 4 - remove redundant contact, executed sql, IN condition=DELETE FROM contact WHERE contactId NOT IN " + ((Object) this.f1172a));
                return Boolean.TRUE;
            } catch (Exception e) {
                e.printStackTrace();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = fp5.f1163a;
                local2.d(str2, ".Inside step 4 - remove redundant contact, exception=" + e);
                return Boolean.FALSE;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements Callable<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ StringBuilder f1173a;

        @DexIgnore
        public f(StringBuilder sb) {
            this.f1173a = sb;
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean call() {
            try {
                Dao contactGroupDao = fp5.this.getContactGroupDao();
                contactGroupDao.queryRaw("DELETE FROM contactgroup WHERE id NOT IN " + ((Object) this.f1173a), new String[0]);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = fp5.f1163a;
                local.d(str, ".Inside step 5 - remove redundant contact group, executed sql=DELETE FROM contactgroup WHERE id NOT IN " + ((Object) this.f1173a));
                return Boolean.TRUE;
            } catch (Exception e) {
                e.printStackTrace();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = fp5.f1163a;
                local2.d(str2, ".Inside step 5 - remove redundant contact group, Exception outside, exception=" + e);
                return Boolean.FALSE;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements Callable<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ StringBuilder f1174a;

        @DexIgnore
        public g(StringBuilder sb) {
            this.f1174a = sb;
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean call() {
            try {
                Dao phoneNumberDao = fp5.this.getPhoneNumberDao();
                phoneNumberDao.queryRaw("DELETE FROM phonenumber WHERE phone_number_id NOT IN " + ((Object) this.f1174a), new String[0]);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = fp5.f1163a;
                local.d(str, ".Inside step 6 - remove phone number, executed sql=DELETE FROM phonenumber WHERE phone_number_id NOT IN " + ((Object) this.f1174a));
                return Boolean.TRUE;
            } catch (Exception e) {
                e.printStackTrace();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = fp5.f1163a;
                local2.d(str2, ".Inside step 6 - remove phone number, exception=" + e);
                return Boolean.FALSE;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements Callable<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ int f1175a;

        @DexIgnore
        public h(int i) {
            this.f1175a = i;
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean call() {
            try {
                Dao phoneNumberDao = fp5.this.getPhoneNumberDao();
                phoneNumberDao.queryRaw("DELETE FROM phonenumber WHERE phone_number_id = '" + this.f1175a + '\'', new String[0]);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = fp5.f1163a;
                local.d(str, ".Inside removePhoneNumberByContactGroupId, executed sql=DELETE FROM phonenumber WHERE phone_number_id = '" + this.f1175a + '\'');
                return Boolean.TRUE;
            } catch (Exception e) {
                e.printStackTrace();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = fp5.f1163a;
                local2.d(str2, ".Inside removePhoneNumberByContactGroupId, exception=" + e);
                return Boolean.FALSE;
            }
        }
    }

    @DexIgnore
    public fp5(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    public List<ln0<Integer, Integer>> B(SQLiteDatabase sQLiteDatabase) {
        Cursor query = sQLiteDatabase.query(true, "contact", new String[]{"contactId", "contact_group_id"}, null, null, null, null, null, null);
        ArrayList arrayList = new ArrayList();
        if (query != null) {
            query.moveToFirst();
            while (!query.isAfterLast()) {
                int i = query.getInt(query.getColumnIndex("contactId"));
                int i2 = query.getInt(query.getColumnIndex("contact_group_id"));
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = f1163a;
                local.d(str, "Add pair contactGroupid=" + i2 + ", contacId=" + i);
                arrayList.add(new ln0(Integer.valueOf(i2), Integer.valueOf(i)));
                query.moveToNext();
            }
            query.close();
        }
        return arrayList;
    }

    @DexIgnore
    public int C(List<ln0<Integer, Integer>> list, int i) {
        for (ln0<Integer, Integer> ln0 : list) {
            if (ln0.f2221a.intValue() == i) {
                return ln0.b.intValue();
            }
        }
        return -1;
    }

    @DexIgnore
    public void D() {
        try {
            TransactionManager.callInTransaction(this.databaseHelper.getConnectionSource(), new d());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public Contact E(int i) {
        try {
            return getContactDao().queryForEq("contactId", Integer.valueOf(i)).get(0);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f1163a;
            local.d(str, "getContactById, ex=" + e2);
            e2.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public List<Integer> F(List<Integer> list) {
        ArrayList arrayList = new ArrayList();
        try {
            StringBuilder L = L(list);
            Dao<Contact, Integer> contactDao = getContactDao();
            for (String[] strArr : contactDao.queryRaw("SELECT contact_group_id FROM contact WHERE contactId IN " + ((Object) L), new String[0]).getResults()) {
                int parseInt = Integer.parseInt(strArr[0]);
                arrayList.add(Integer.valueOf(parseInt));
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = f1163a;
                local.d(str, ".Inside step 3 - get contact group Id, contactGroupId=" + parseInt);
            }
        } catch (SQLiteException | SQLException e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = f1163a;
            local2.d(str2, ".Inside step 3 , SQLiteException, ex=" + e2);
            e2.printStackTrace();
        }
        return arrayList;
    }

    @DexIgnore
    public List<Integer> G() {
        ArrayList arrayList = new ArrayList();
        try {
            for (String[] strArr : getContactDao().queryRaw("SELECT contactId FROM contact", new String[0]).getResults()) {
                int parseInt = Integer.parseInt(strArr[0]);
                arrayList.add(Integer.valueOf(parseInt));
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = f1163a;
                local.d(str, ".Inside step 1 - get Id on local DB, contactId=" + parseInt);
            }
        } catch (SQLiteException | SQLException e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = f1163a;
            local2.d(str2, ".Inside step 1 , SQLiteException, ex=" + e2);
            e2.printStackTrace();
        }
        return arrayList;
    }

    @DexIgnore
    public void H(List<Integer> list) {
        try {
            TransactionManager.callInTransaction(this.databaseHelper.getConnectionSource(), new e(L(list)));
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f1163a;
            local.d(str, ".Inside step 4 - remove redundant contact, Exception outside, exception=" + e2);
        }
    }

    @DexIgnore
    public void I(List<Integer> list) {
        try {
            TransactionManager.callInTransaction(this.databaseHelper.getConnectionSource(), new g(L(list)));
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f1163a;
            local.d(str, ".Inside step 6 - remove phone number, exception=" + e2);
        }
    }

    @DexIgnore
    public void J(int i) {
        try {
            TransactionManager.callInTransaction(this.databaseHelper.getConnectionSource(), new h(i));
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f1163a;
            local.d(str, ".Inside removePhoneNumberByContactGroupId, exception=" + e2);
        }
    }

    @DexIgnore
    public void K(List<Integer> list) {
        try {
            TransactionManager.callInTransaction(this.databaseHelper.getConnectionSource(), new f(L(list)));
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f1163a;
            local.d(str, ".Inside step 5 - remove redundant contact group, Exception, exception=" + e2);
        }
    }

    @DexIgnore
    public final StringBuilder L(List<Integer> list) {
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        for (Integer num : list) {
            int intValue = num.intValue();
            if (sb.length() > 1) {
                sb.append(',');
            }
            sb.append(intValue);
        }
        sb.append(')');
        return sb;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider, com.fossil.wearables.fsl.contact.ContactProvider, com.fossil.wearables.fsl.contact.ContactProviderImpl
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.contact.ContactProviderImpl, com.fossil.wearables.fsl.shared.BaseDbProvider
    @SuppressLint({"UseSparseArrays"})
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        Map<Integer, UpgradeCommand> dbUpgrades = super.getDbUpgrades();
        if (dbUpgrades == null) {
            dbUpgrades = new HashMap<>();
        }
        dbUpgrades.put(2, new a());
        dbUpgrades.put(4, new b());
        dbUpgrades.put(5, new c());
        return dbUpgrades;
    }
}
