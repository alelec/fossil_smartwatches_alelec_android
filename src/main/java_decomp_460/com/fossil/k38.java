package com.fossil;

import com.fossil.e38;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k38 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public long f1861a; // = 0;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ i38 d;
    @DexIgnore
    public /* final */ Deque<p18> e; // = new ArrayDeque();
    @DexIgnore
    public e38.a f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ b h;
    @DexIgnore
    public /* final */ a i;
    @DexIgnore
    public /* final */ c j; // = new c();
    @DexIgnore
    public /* final */ c k; // = new c();
    @DexIgnore
    public d38 l; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements a58 {
        @DexIgnore
        public /* final */ i48 b; // = new i48();
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.a58
        public void K(i48 i48, long j) throws IOException {
            this.b.K(i48, j);
            while (this.b.p0() >= 16384) {
                a(false);
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public final void a(boolean z) throws IOException {
            long min;
            synchronized (k38.this) {
                k38.this.k.r();
                while (k38.this.b <= 0 && !this.d && !this.c && k38.this.l == null) {
                    try {
                        k38.this.t();
                    } catch (Throwable th) {
                        k38.this.k.y();
                        throw th;
                    }
                }
                k38.this.k.y();
                k38.this.e();
                min = Math.min(k38.this.b, this.b.p0());
                k38.this.b -= min;
            }
            k38.this.k.r();
            try {
                k38.this.d.r0(k38.this.c, z && min == this.b.p0(), this.b, min);
            } finally {
                k38.this.k.y();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
            if (r6.b.p0() <= 0) goto L_0x002d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
            if (r6.b.p0() <= 0) goto L_0x0037;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0029, code lost:
            a(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x002d, code lost:
            r1 = r6.e;
            r1.d.r0(r1.c, true, null, 0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0037, code lost:
            r1 = r6.e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0039, code lost:
            monitor-enter(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            r6.c = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x003d, code lost:
            monitor-exit(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x003e, code lost:
            r6.e.d.flush();
            r6.e.d();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
            if (r6.e.i.d != false) goto L_0x0037;
         */
        @DexIgnore
        @Override // com.fossil.a58, java.io.Closeable, java.lang.AutoCloseable
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() throws java.io.IOException {
            /*
                r6 = this;
                r4 = 0
                r2 = 1
                com.fossil.k38 r1 = com.fossil.k38.this
                monitor-enter(r1)
                boolean r0 = r6.c     // Catch:{ all -> 0x004e }
                if (r0 == 0) goto L_0x000c
                monitor-exit(r1)     // Catch:{ all -> 0x004e }
            L_0x000b:
                return
            L_0x000c:
                monitor-exit(r1)     // Catch:{ all -> 0x004e }
                com.fossil.k38 r0 = com.fossil.k38.this
                com.fossil.k38$a r0 = r0.i
                boolean r0 = r0.d
                if (r0 != 0) goto L_0x0037
                com.fossil.i48 r0 = r6.b
                long r0 = r0.p0()
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x002d
            L_0x001f:
                com.fossil.i48 r0 = r6.b
                long r0 = r0.p0()
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x0037
                r6.a(r2)
                goto L_0x001f
            L_0x002d:
                com.fossil.k38 r1 = com.fossil.k38.this
                com.fossil.i38 r0 = r1.d
                int r1 = r1.c
                r3 = 0
                r0.r0(r1, r2, r3, r4)
            L_0x0037:
                com.fossil.k38 r1 = com.fossil.k38.this
                monitor-enter(r1)
                r0 = 1
                r6.c = r0     // Catch:{ all -> 0x004b }
                monitor-exit(r1)     // Catch:{ all -> 0x004b }
                com.fossil.k38 r0 = com.fossil.k38.this
                com.fossil.i38 r0 = r0.d
                r0.flush()
                com.fossil.k38 r0 = com.fossil.k38.this
                r0.d()
                goto L_0x000b
            L_0x004b:
                r0 = move-exception
                monitor-exit(r1)
                throw r0
            L_0x004e:
                r0 = move-exception
                monitor-exit(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.k38.a.close():void");
        }

        @DexIgnore
        @Override // com.fossil.a58
        public d58 e() {
            return k38.this.k;
        }

        @DexIgnore
        @Override // com.fossil.a58, java.io.Flushable
        public void flush() throws IOException {
            synchronized (k38.this) {
                k38.this.e();
            }
            while (this.b.p0() > 0) {
                a(false);
                k38.this.d.flush();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements c58 {
        @DexIgnore
        public /* final */ i48 b; // = new i48();
        @DexIgnore
        public /* final */ i48 c; // = new i48();
        @DexIgnore
        public /* final */ long d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public b(long j) {
            this.d = j;
        }

        @DexIgnore
        public void a(k48 k48, long j) throws IOException {
            boolean z;
            boolean z2;
            long j2;
            while (j > 0) {
                synchronized (k38.this) {
                    z = this.f;
                    z2 = this.c.p0() + j > this.d;
                }
                if (z2) {
                    k48.skip(j);
                    k38.this.h(d38.FLOW_CONTROL_ERROR);
                    return;
                } else if (z) {
                    k48.skip(j);
                    return;
                } else {
                    long d0 = k48.d0(this.b, j);
                    if (d0 != -1) {
                        j -= d0;
                        synchronized (k38.this) {
                            if (this.e) {
                                j2 = this.b.p0();
                                this.b.j();
                            } else {
                                boolean z3 = this.c.p0() == 0;
                                this.c.N(this.b);
                                if (z3) {
                                    k38.this.notifyAll();
                                }
                                j2 = 0;
                            }
                        }
                        if (j2 > 0) {
                            b(j2);
                        }
                    } else {
                        throw new EOFException();
                    }
                }
            }
        }

        @DexIgnore
        public final void b(long j) {
            k38.this.d.q0(j);
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.c58, java.lang.AutoCloseable
        public void close() throws IOException {
            long p0;
            ArrayList<p18> arrayList;
            e38.a aVar;
            synchronized (k38.this) {
                this.e = true;
                p0 = this.c.p0();
                this.c.j();
                if (k38.this.e.isEmpty() || k38.this.f == null) {
                    arrayList = null;
                    aVar = null;
                } else {
                    arrayList = new ArrayList(k38.this.e);
                    k38.this.e.clear();
                    aVar = k38.this.f;
                }
                k38.this.notifyAll();
            }
            if (p0 > 0) {
                b(p0);
            }
            k38.this.d();
            if (aVar != null) {
                for (p18 p18 : arrayList) {
                    aVar.a(p18);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.c58
        public long d0(i48 i48, long j) throws IOException {
            d38 d38;
            long j2;
            p18 p18;
            e38.a aVar;
            long j3;
            if (j >= 0) {
                while (true) {
                    synchronized (k38.this) {
                        k38.this.j.r();
                        try {
                            d38 = k38.this.l != null ? k38.this.l : null;
                            if (!this.e) {
                                if (k38.this.e.isEmpty() || k38.this.f == null) {
                                    if (this.c.p0() > 0) {
                                        j2 = this.c.d0(i48, Math.min(j, this.c.p0()));
                                        k38.this.f1861a += j2;
                                        if (d38 == null && k38.this.f1861a >= ((long) (k38.this.d.y.d() / 2))) {
                                            k38.this.d.v0(k38.this.c, k38.this.f1861a);
                                            k38.this.f1861a = 0;
                                        }
                                    } else if (this.f || d38 != null) {
                                        j2 = -1;
                                    } else {
                                        k38.this.t();
                                        k38.this.j.y();
                                    }
                                    p18 = null;
                                    aVar = null;
                                    j3 = j2;
                                } else {
                                    p18 p182 = (p18) k38.this.e.removeFirst();
                                    j3 = -1;
                                    aVar = k38.this.f;
                                    p18 = p182;
                                }
                                if (p18 != null && aVar != null) {
                                    aVar.a(p18);
                                }
                            } else {
                                throw new IOException("stream closed");
                            }
                        } finally {
                            k38.this.j.y();
                        }
                    }
                }
                if (j3 != -1) {
                    b(j3);
                    return j3;
                } else if (d38 == null) {
                    return -1;
                } else {
                    throw new p38(d38);
                }
            } else {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            }
        }

        @DexIgnore
        @Override // com.fossil.c58
        public d58 e() {
            return k38.this.j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends g48 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.g48
        public IOException t(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        @DexIgnore
        @Override // com.fossil.g48
        public void x() {
            k38.this.h(d38.CANCEL);
            k38.this.d.g0();
        }

        @DexIgnore
        public void y() throws IOException {
            if (s()) {
                throw t(null);
            }
        }
    }

    @DexIgnore
    public k38(int i2, i38 i38, boolean z, boolean z2, p18 p18) {
        if (i38 != null) {
            this.c = i2;
            this.d = i38;
            this.b = (long) i38.z.d();
            this.h = new b((long) i38.y.d());
            a aVar = new a();
            this.i = aVar;
            this.h.f = z2;
            aVar.d = z;
            if (p18 != null) {
                this.e.add(p18);
            }
            if (l() && p18 != null) {
                throw new IllegalStateException("locally-initiated streams shouldn't have headers yet");
            } else if (!l() && p18 == null) {
                throw new IllegalStateException("remotely-initiated streams should have headers");
            }
        } else {
            throw new NullPointerException("connection == null");
        }
    }

    @DexIgnore
    public void c(long j2) {
        this.b += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }

    @DexIgnore
    public void d() throws IOException {
        boolean z;
        boolean m;
        synchronized (this) {
            z = !this.h.f && this.h.e && (this.i.d || this.i.c);
            m = m();
        }
        if (z) {
            f(d38.CANCEL);
        } else if (!m) {
            this.d.b0(this.c);
        }
    }

    @DexIgnore
    public void e() throws IOException {
        a aVar = this.i;
        if (aVar.c) {
            throw new IOException("stream closed");
        } else if (aVar.d) {
            throw new IOException("stream finished");
        } else if (this.l != null) {
            throw new p38(this.l);
        }
    }

    @DexIgnore
    public void f(d38 d38) throws IOException {
        if (g(d38)) {
            this.d.t0(this.c, d38);
        }
    }

    @DexIgnore
    public final boolean g(d38 d38) {
        synchronized (this) {
            if (this.l != null) {
                return false;
            }
            if (this.h.f && this.i.d) {
                return false;
            }
            this.l = d38;
            notifyAll();
            this.d.b0(this.c);
            return true;
        }
    }

    @DexIgnore
    public void h(d38 d38) {
        if (g(d38)) {
            this.d.u0(this.c, d38);
        }
    }

    @DexIgnore
    public int i() {
        return this.c;
    }

    @DexIgnore
    public a58 j() {
        synchronized (this) {
            if (!this.g && !l()) {
                throw new IllegalStateException("reply before requesting the sink");
            }
        }
        return this.i;
    }

    @DexIgnore
    public c58 k() {
        return this.h;
    }

    @DexIgnore
    public boolean l() {
        return this.d.b == ((this.c & 1) == 1);
    }

    @DexIgnore
    public boolean m() {
        synchronized (this) {
            if (this.l != null) {
                return false;
            }
            return (!this.h.f && !this.h.e) || (!this.i.d && !this.i.c) || !this.g;
        }
    }

    @DexIgnore
    public d58 n() {
        return this.j;
    }

    @DexIgnore
    public void o(k48 k48, int i2) throws IOException {
        this.h.a(k48, (long) i2);
    }

    @DexIgnore
    public void p() {
        boolean m;
        synchronized (this) {
            this.h.f = true;
            m = m();
            notifyAll();
        }
        if (!m) {
            this.d.b0(this.c);
        }
    }

    @DexIgnore
    public void q(List<e38> list) {
        boolean m;
        synchronized (this) {
            this.g = true;
            this.e.add(b28.H(list));
            m = m();
            notifyAll();
        }
        if (!m) {
            this.d.b0(this.c);
        }
    }

    @DexIgnore
    public void r(d38 d38) {
        synchronized (this) {
            if (this.l == null) {
                this.l = d38;
                notifyAll();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public p18 s() throws IOException {
        p18 removeFirst;
        synchronized (this) {
            this.j.r();
            while (this.e.isEmpty() && this.l == null) {
                try {
                    t();
                } catch (Throwable th) {
                    this.j.y();
                    throw th;
                }
            }
            this.j.y();
            if (!this.e.isEmpty()) {
                removeFirst = this.e.removeFirst();
            } else {
                throw new p38(this.l);
            }
        }
        return removeFirst;
    }

    @DexIgnore
    public void t() throws InterruptedIOException {
        try {
            wait();
        } catch (InterruptedException e2) {
            Thread.currentThread().interrupt();
            throw new InterruptedIOException();
        }
    }

    @DexIgnore
    public d58 u() {
        return this.k;
    }
}
