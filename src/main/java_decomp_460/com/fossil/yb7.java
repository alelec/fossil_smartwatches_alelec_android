package com.fossil;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.fossil.wq5;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.FileRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yb7 extends iq4<c, d, b> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ a e; // = new a();
    @DexIgnore
    public /* final */ e f; // = new e();
    @DexIgnore
    public mo5 g;
    @DexIgnore
    public /* final */ uo5 h;
    @DexIgnore
    public /* final */ FileRepository i;
    @DexIgnore
    public /* final */ on5 j;
    @DexIgnore
    public /* final */ DianaAppSettingRepository k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements wq5.b {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.yb7$a$a")
        @eo7(c = "com.portfolio.platform.watchface.usecase.SetPresetUseCase$ApplyThemeReceiver$receive$1", f = "SetPresetUseCase.kt", l = {57}, m = "invokeSuspend")
        /* renamed from: com.fossil.yb7$a$a  reason: collision with other inner class name */
        public static final class C0294a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0294a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0294a aVar = new C0294a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((C0294a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                List<oo5> a2;
                Object allDianaAppSettings;
                Gson gson;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    mo5 mo5 = yb7.this.g;
                    if (mo5 != null) {
                        a2 = mo5.a();
                        if (a2 != null) {
                            Gson gson2 = new Gson();
                            DianaAppSettingRepository dianaAppSettingRepository = yb7.this.k;
                            this.L$0 = iv7;
                            this.L$1 = a2;
                            this.L$2 = gson2;
                            this.label = 1;
                            allDianaAppSettings = dianaAppSettingRepository.getAllDianaAppSettings("watch_app", this);
                            if (allDianaAppSettings == d) {
                                return d;
                            }
                            gson = gson2;
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else if (i == 1) {
                    a2 = (List) this.L$1;
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    gson = (Gson) this.L$2;
                    allDianaAppSettings = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                PortfolioApp.h0.c().M1(kj5.l(a2, gson, (List) allDianaAppSettings), PortfolioApp.h0.c().J());
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            if (yb7.this.s() && communicateMode == CommunicateMode.APPLY_THEME_SESSION) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetPresetUseCase", "onReceive - ApplyThemeReceiver - isExecuted " + yb7.this.s() + " communicateMode=" + communicateMode);
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("SetPresetUseCase", "apply theme success,start set watch apps preset " + yb7.this.g);
                    if (yb7.this.g != null) {
                        xw7 unused = gu7.d(yb7.this.g(), null, null, new C0294a(this, null), 3, null);
                        return;
                    }
                    yb7.this.v(false);
                    yb7.this.w();
                    yb7.this.i(new b(-1, new ArrayList()));
                    return;
                }
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("SetPresetUseCase", "onReceive - failed erorCode " + intExtra + " permissionErrorCodes " + integerArrayListExtra);
                yb7.this.w();
                yb7.this.i(new b(intExtra, integerArrayListExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f4299a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public b(int i, ArrayList<Integer> arrayList) {
            pq7.c(arrayList, "mBLEErrorCodes");
            this.f4299a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.f4299a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f4300a;

        @DexIgnore
        public c(String str) {
            pq7.c(str, "presetId");
            this.f4300a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f4300a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements wq5.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ mo5 $currentPreset;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(mo5 mo5, qn7 qn7, e eVar) {
                super(2, qn7);
                this.$currentPreset = mo5;
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$currentPreset, qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r0v16, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r1v10, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r0v25, types: [java.util.List] */
            /* JADX WARNING: Removed duplicated region for block: B:13:0x0062  */
            /* JADX WARNING: Removed duplicated region for block: B:19:0x0088  */
            /* JADX WARNING: Removed duplicated region for block: B:22:0x00d9  */
            /* JADX WARNING: Removed duplicated region for block: B:26:0x0116  */
            /* JADX WARNING: Unknown variable types count: 3 */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                /*
                // Method dump skipped, instructions count: 285
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.yb7.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            if (yb7.this.s() && communicateMode == CommunicateMode.SET_WATCH_APPS) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetPresetUseCase", "onReceive - SetWatchAppReceiver - isExecuted " + yb7.this.s() + " communicateMode=" + communicateMode);
                mo5 mo5 = yb7.this.g;
                if (mo5 != null) {
                    xw7 unused = gu7.d(yb7.this.g(), null, null, new a(mo5, null, this), 3, null);
                }
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.FAILED.ordinal()) {
                    yb7.this.j.Z0(PortfolioApp.h0.c().J(), -1, false);
                }
                yb7.this.w();
                yb7.this.v(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.usecase.SetPresetUseCase", f = "SetPresetUseCase.kt", l = {119, 126, 133}, m = "run")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yb7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(yb7 yb7, qn7 qn7) {
            super(qn7);
            this.this$0 = yb7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore
    public yb7(uo5 uo5, FileRepository fileRepository, on5 on5, s77 s77, DianaAppSettingRepository dianaAppSettingRepository) {
        pq7.c(uo5, "mDianaPresetRepository");
        pq7.c(fileRepository, "mFileRepository");
        pq7.c(on5, "mSharedPrefs");
        pq7.c(s77, "wfAssetRepository");
        pq7.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        this.h = uo5;
        this.i = fileRepository;
        this.j = on5;
        this.k = dianaAppSettingRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "SetPresetUseCase";
    }

    @DexIgnore
    public final boolean s() {
        return this.d;
    }

    @DexIgnore
    public final void t() {
        wq5.d.e(this.e, CommunicateMode.APPLY_THEME_SESSION);
        wq5.d.e(this.f, CommunicateMode.SET_WATCH_APPS);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0142  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0193  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* renamed from: u */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.yb7.c r14, com.fossil.qn7<java.lang.Object> r15) {
        /*
        // Method dump skipped, instructions count: 406
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yb7.k(com.fossil.yb7$c, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void v(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final void w() {
        wq5.d.j(this.e, CommunicateMode.APPLY_THEME_SESSION);
        wq5.d.j(this.f, CommunicateMode.SET_WATCH_APPS);
    }
}
