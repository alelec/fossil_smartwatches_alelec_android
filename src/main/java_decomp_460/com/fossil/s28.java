package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface s28 {
    @DexIgnore
    void a() throws IOException;

    @DexIgnore
    void b(v18 v18) throws IOException;

    @DexIgnore
    w18 c(Response response) throws IOException;

    @DexIgnore
    Object cancel();  // void declaration

    @DexIgnore
    Response.a d(boolean z) throws IOException;

    @DexIgnore
    void e() throws IOException;

    @DexIgnore
    a58 f(v18 v18, long j);
}
