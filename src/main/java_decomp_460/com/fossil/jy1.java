package com.fossil;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jy1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ jy1 f1832a; // = new jy1();

    @DexIgnore
    public final boolean a(String str, File file) {
        FileWriter fileWriter;
        pq7.c(str, "text");
        pq7.c(file, "file");
        try {
            FileWriter fileWriter2 = new FileWriter(file, true);
            try {
                fileWriter2.append((CharSequence) str);
                fileWriter2.flush();
                fileWriter2.close();
                return true;
            } catch (IOException e) {
                fileWriter = fileWriter2;
            }
        } catch (IOException e2) {
            fileWriter = null;
            if (fileWriter != null) {
                try {
                    fileWriter.flush();
                } catch (IOException e3) {
                    return false;
                }
            }
            if (fileWriter != null) {
                fileWriter.close();
            }
            return false;
        }
    }

    @DexIgnore
    public final byte[] b(File file) {
        pq7.c(file, "file");
        try {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file));
            byte[] bArr = new byte[((int) file.length())];
            try {
                dataInputStream.readFully(bArr);
                try {
                    dataInputStream.close();
                    return bArr;
                } catch (IOException e) {
                    return bArr;
                }
            } catch (IOException e2) {
                try {
                    dataInputStream.close();
                } catch (IOException e3) {
                }
                return null;
            } catch (Throwable th) {
                try {
                    dataInputStream.close();
                } catch (IOException e4) {
                }
                throw th;
            }
        } catch (FileNotFoundException e5) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0049, code lost:
        if (r1 != null) goto L_0x0034;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003f A[SYNTHETIC, Splitter:B:18:0x003f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String c(java.io.File r7) {
        /*
            r6 = this;
            r0 = 0
            java.lang.String r1 = "file"
            com.fossil.pq7.c(r7, r1)
            boolean r1 = r7.exists()
            if (r1 != 0) goto L_0x000d
        L_0x000c:
            return r0
        L_0x000d:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0047, all -> 0x0043 }
            r3.<init>()     // Catch:{ IOException -> 0x0047, all -> 0x0043 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0047, all -> 0x0043 }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0047, all -> 0x0043 }
            r2.<init>(r7)     // Catch:{ IOException -> 0x0047, all -> 0x0043 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0047, all -> 0x0043 }
            r2 = 1024(0x400, float:1.435E-42)
            char[] r4 = new char[r2]     // Catch:{ IOException -> 0x004e, all -> 0x003a }
            int r2 = r1.read(r4)     // Catch:{ IOException -> 0x004e, all -> 0x003a }
        L_0x0024:
            r5 = -1
            if (r2 == r5) goto L_0x0030
            r5 = 0
            r3.append(r4, r5, r2)     // Catch:{ IOException -> 0x004e, all -> 0x003a }
            int r2 = r1.read(r4)     // Catch:{ IOException -> 0x004e, all -> 0x003a }
            goto L_0x0024
        L_0x0030:
            java.lang.String r0 = r3.toString()     // Catch:{ IOException -> 0x004e, all -> 0x003a }
        L_0x0034:
            r1.close()     // Catch:{ IOException -> 0x0038 }
            goto L_0x000c
        L_0x0038:
            r1 = move-exception
            goto L_0x000c
        L_0x003a:
            r0 = move-exception
            r2 = r1
            r3 = r0
        L_0x003d:
            if (r2 == 0) goto L_0x0042
            r2.close()     // Catch:{ IOException -> 0x004c }
        L_0x0042:
            throw r3
        L_0x0043:
            r1 = move-exception
            r2 = r0
            r3 = r1
            goto L_0x003d
        L_0x0047:
            r1 = move-exception
            r1 = r0
        L_0x0049:
            if (r1 == 0) goto L_0x000c
            goto L_0x0034
        L_0x004c:
            r0 = move-exception
            goto L_0x0042
        L_0x004e:
            r2 = move-exception
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jy1.c(java.io.File):java.lang.String");
    }
}
