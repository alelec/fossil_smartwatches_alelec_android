package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lh6 extends pv5 implements kh6 {
    @DexIgnore
    public y67 g;
    @DexIgnore
    public g37<t65> h;
    @DexIgnore
    public jh6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lh6 b;

        @DexIgnore
        public a(lh6 lh6, t65 t65) {
            this.b = lh6;
        }

        @DexIgnore
        public final void onClick(View view) {
            lh6.K6(this.b).a().l(2);
        }
    }

    @DexIgnore
    public static final /* synthetic */ y67 K6(lh6 lh6) {
        y67 y67 = lh6.g;
        if (y67 != null) {
            return y67;
        }
        pq7.n("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "GoalTrackingOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void L6() {
        t65 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        g37<t65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayGoalChart = a2.r) != null) {
            overviewDayGoalChart.D("hybridGoalTrackingTab", "nonBrandNonReachGoal");
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(jh6 jh6) {
        pq7.c(jh6, "presenter");
        this.i = jh6;
    }

    @DexIgnore
    @Override // com.fossil.kh6
    public void n(mv5 mv5, ArrayList<String> arrayList) {
        t65 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        pq7.c(mv5, "baseModel");
        pq7.c(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewDayFragment", "showDayDetailChart - baseModel=" + mv5);
        g37<t65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayGoalChart = a2.r) != null) {
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(mv5.f2426a.b(cVar.d()));
            if (!arrayList.isEmpty()) {
                BarChart.H(overviewDayGoalChart, arrayList, false, 2, null);
            } else {
                BarChart.H(overviewDayGoalChart, jl5.b.f(), false, 2, null);
            }
            overviewDayGoalChart.r(mv5);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        t65 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onCreateView");
        t65 t65 = (t65) aq0.f(layoutInflater, 2131558560, viewGroup, false, A6());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ts0 a3 = vs0.e(activity).a(y67.class);
            pq7.b(a3, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.g = (y67) a3;
            t65.s.setOnClickListener(new a(this, t65));
        }
        this.h = new g37<>(this, t65);
        L6();
        g37<t65> g37 = this.h;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onResume");
        L6();
        jh6 jh6 = this.i;
        if (jh6 != null) {
            jh6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onStop");
        jh6 jh6 = this.i;
        if (jh6 != null) {
            jh6.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.kh6
    public void x(boolean z) {
        t65 a2;
        g37<t65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null) {
            if (z) {
                OverviewDayGoalChart overviewDayGoalChart = a2.r;
                pq7.b(overviewDayGoalChart, "binding.dayChart");
                overviewDayGoalChart.setVisibility(4);
                ConstraintLayout constraintLayout = a2.q;
                pq7.b(constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            OverviewDayGoalChart overviewDayGoalChart2 = a2.r;
            pq7.b(overviewDayGoalChart2, "binding.dayChart");
            overviewDayGoalChart2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.q;
            pq7.b(constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }
}
