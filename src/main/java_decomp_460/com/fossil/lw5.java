package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.BaseWebViewActivity;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.PermissionData;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleImageButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lw5 extends RecyclerView.g<RecyclerView.ViewHolder> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<PermissionData> f2271a;
    @DexIgnore
    public a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(PermissionData permissionData);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ hf5 f2272a;
        @DexIgnore
        public /* final */ /* synthetic */ lw5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    BaseWebViewActivity.a aVar = BaseWebViewActivity.D;
                    FlexibleImageButton flexibleImageButton = this.b.f2272a.s;
                    pq7.b(flexibleImageButton, "binding.ibInfo");
                    Context context = flexibleImageButton.getContext();
                    pq7.b(context, "binding.ibInfo.context");
                    List list = this.b.b.f2271a;
                    if (list != null) {
                        aVar.b(context, "", ((PermissionData) list.get(adapterPosition)).getExternalLink());
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lw5$b$b")
        /* renamed from: com.fossil.lw5$b$b  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0145b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public View$OnClickListenerC0145b(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List list = this.b.b.f2271a;
                    if (list != null) {
                        PermissionData permissionData = (PermissionData) list.get(adapterPosition);
                        a aVar = this.b.b.b;
                        if (aVar != null) {
                            aVar.a(permissionData);
                            return;
                        }
                        return;
                    }
                    pq7.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(lw5 lw5, hf5 hf5) {
            super(hf5.n());
            pq7.c(hf5, "binding");
            this.b = lw5;
            this.f2272a = hf5;
            hf5.s.setOnClickListener(new a(this));
            String d = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            if (d != null) {
                this.f2272a.t.setBackgroundColor(Color.parseColor(d));
                this.f2272a.u.setColorFilter(Color.parseColor(d), PorterDuff.Mode.SRC_ATOP);
            }
            this.f2272a.q.setOnClickListener(new View$OnClickListenerC0145b(this));
        }

        @DexIgnore
        public final void b(PermissionData permissionData) {
            int i = 0;
            pq7.c(permissionData, "permissionData");
            FlexibleTextView flexibleTextView = this.f2272a.r;
            pq7.b(flexibleTextView, "binding.ftvDescription");
            flexibleTextView.setText(permissionData.getShortDescription());
            String type = permissionData.getType();
            int hashCode = type.hashCode();
            if (hashCode != -1168251815) {
                if (hashCode == -381825158 && type.equals("PERMISSION_REQUEST_TYPE")) {
                    FlexibleButton flexibleButton = this.f2272a.q;
                    pq7.b(flexibleButton, "binding.fbGrantPermission");
                    flexibleButton.setText(um5.c(PortfolioApp.h0.c().getApplicationContext(), 2131886925));
                    FlexibleButton flexibleButton2 = this.f2272a.q;
                    pq7.b(flexibleButton2, "binding.fbGrantPermission");
                    flexibleButton2.setEnabled(!permissionData.isGranted());
                    ImageView imageView = this.f2272a.u;
                    pq7.b(imageView, "binding.ivCheck");
                    imageView.setVisibility(permissionData.isGranted() ? 0 : 4);
                }
            } else if (type.equals("PERMISSION_SETTING_TYPE")) {
                FlexibleButton flexibleButton3 = this.f2272a.q;
                pq7.b(flexibleButton3, "binding.fbGrantPermission");
                flexibleButton3.setText(um5.c(PortfolioApp.h0.c().getApplicationContext(), 2131886538));
                FlexibleButton flexibleButton4 = this.f2272a.q;
                pq7.b(flexibleButton4, "binding.fbGrantPermission");
                flexibleButton4.setEnabled(true);
                ImageView imageView2 = this.f2272a.u;
                pq7.b(imageView2, "binding.ivCheck");
                imageView2.setVisibility(8);
            }
            FlexibleImageButton flexibleImageButton = this.f2272a.s;
            pq7.b(flexibleImageButton, "binding.ibInfo");
            if (TextUtils.isEmpty(permissionData.getExternalLink())) {
                i = 4;
            }
            flexibleImageButton.setVisibility(i);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<PermissionData> list = this.f2271a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public final void i(a aVar) {
        pq7.c(aVar, "listener");
        this.b = aVar;
    }

    @DexIgnore
    public final void j(List<PermissionData> list) {
        pq7.c(list, "permissionList");
        this.f2271a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        pq7.c(viewHolder, "holder");
        b bVar = (b) viewHolder;
        List<PermissionData> list = this.f2271a;
        if (list != null) {
            bVar.b(list.get(i));
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        hf5 z = hf5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemPermissionBinding.in\u2026.context), parent, false)");
        return new b(this, z);
    }
}
