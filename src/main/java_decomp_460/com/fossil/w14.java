package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class w14<K, V> extends r14<K, V> implements w44<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 7431625294878419160L;

    @DexIgnore
    public w14(Map<K, Collection<V>> map) {
        super(map);
    }

    @DexIgnore
    @Override // com.fossil.u14, com.fossil.y34
    public Map<K, Collection<V>> asMap() {
        return super.asMap();
    }

    @DexIgnore
    @Override // com.fossil.r14
    public abstract Set<V> createCollection();

    @DexIgnore
    @Override // com.fossil.r14
    public Set<V> createUnmodifiableEmptyCollection() {
        return h34.of();
    }

    @DexIgnore
    @Override // com.fossil.r14, com.fossil.u14, com.fossil.y34
    public Set<Map.Entry<K, V>> entries() {
        return (Set) super.entries();
    }

    @DexIgnore
    @Override // com.fossil.u14
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.r14, com.fossil.y34
    public Set<V> get(K k) {
        return (Set) super.get((w14<K, V>) k);
    }

    @DexIgnore
    @Override // com.fossil.r14, com.fossil.u14, com.fossil.y34
    @CanIgnoreReturnValue
    public boolean put(K k, V v) {
        return super.put(k, v);
    }

    @DexIgnore
    @Override // com.fossil.r14
    @CanIgnoreReturnValue
    public Set<V> removeAll(Object obj) {
        return (Set) super.removeAll(obj);
    }

    @DexIgnore
    @Override // com.fossil.r14, com.fossil.u14
    @CanIgnoreReturnValue
    public Set<V> replaceValues(K k, Iterable<? extends V> iterable) {
        return (Set) super.replaceValues((w14<K, V>) k, (Iterable) iterable);
    }
}
