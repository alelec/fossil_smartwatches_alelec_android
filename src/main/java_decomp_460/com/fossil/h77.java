package com.fossil;

import com.misfit.frameworks.buttonservice.communite.CommunicateMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class h77 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f1444a;

    /*
    static {
        int[] iArr = new int[CommunicateMode.values().length];
        f1444a = iArr;
        iArr[CommunicateMode.PREVIEW_THEME_SESSION.ordinal()] = 1;
        f1444a[CommunicateMode.APPLY_THEME_SESSION.ordinal()] = 2;
        f1444a[CommunicateMode.PARSE_THEME_DATA_TO_BINARY.ordinal()] = 3;
    }
    */
}
