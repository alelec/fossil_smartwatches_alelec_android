package com.fossil;

import com.fossil.a91;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o91<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ T f2651a;
    @DexIgnore
    public /* final */ a91.a b;
    @DexIgnore
    public /* final */ t91 c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void onErrorResponse(t91 t91);
    }

    @DexIgnore
    public interface b<T> {
        @DexIgnore
        void onResponse(T t);
    }

    @DexIgnore
    public o91(t91 t91) {
        this.d = false;
        this.f2651a = null;
        this.b = null;
        this.c = t91;
    }

    @DexIgnore
    public o91(T t, a91.a aVar) {
        this.d = false;
        this.f2651a = t;
        this.b = aVar;
        this.c = null;
    }

    @DexIgnore
    public static <T> o91<T> a(t91 t91) {
        return new o91<>(t91);
    }

    @DexIgnore
    public static <T> o91<T> c(T t, a91.a aVar) {
        return new o91<>(t, aVar);
    }

    @DexIgnore
    public boolean b() {
        return this.c == null;
    }
}
