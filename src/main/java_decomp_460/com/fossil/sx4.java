package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.f57;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sx4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public TimerViewObserver f3329a;
    @DexIgnore
    public /* final */ f57.b b;
    @DexIgnore
    public /* final */ uy4 c; // = uy4.d.b();
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ lf5 f3330a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(lf5 lf5, View view) {
            super(view);
            pq7.c(lf5, "binding");
            pq7.c(view, "root");
            this.f3330a = lf5;
        }

        @DexIgnore
        public final void a(mt4 mt4, TimerViewObserver timerViewObserver, f57.b bVar, uy4 uy4) {
            String str;
            String str2 = null;
            pq7.c(mt4, "recommended");
            pq7.c(bVar, "drawableBuilder");
            pq7.c(uy4, "colorGenerator");
            lf5 lf5 = this.f3330a;
            ps4 b = mt4.b();
            lf5.w.setEndingText(um5.c(PortfolioApp.h0.c(), 2131886306));
            TimerTextView timerTextView = lf5.w;
            Date m = b.m();
            timerTextView.setTime(m != null ? m.getTime() : 0);
            lf5.w.w(timerViewObserver, getAdapterPosition());
            FlexibleTextView flexibleTextView = lf5.r;
            pq7.b(flexibleTextView, "ftvChallengeName");
            flexibleTextView.setText(b.g());
            ht4 i = b.i();
            if (i == null || (str = i.c()) == null) {
                str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
            }
            ImageView imageView = lf5.x;
            pq7.b(imageView, "imgAvatar");
            ht4 i2 = b.i();
            ty4.b(imageView, i2 != null ? i2.e() : null, str, bVar, uy4);
            String c = b.c();
            if (c != null) {
                FlexibleTextView flexibleTextView2 = lf5.q;
                pq7.b(flexibleTextView2, "ftvChallengeDes");
                flexibleTextView2.setText(c);
            } else {
                FlexibleTextView flexibleTextView3 = lf5.q;
                pq7.b(flexibleTextView3, "ftvChallengeDes");
                flexibleTextView3.setText("");
            }
            FlexibleTextView flexibleTextView4 = lf5.t;
            pq7.b(flexibleTextView4, "ftvDuration");
            Integer d = b.d();
            flexibleTextView4.setText(d != null ? kl5.a(d.intValue()) : null);
            Integer h = b.h();
            int intValue = h != null ? h.intValue() : 0;
            FlexibleTextView flexibleTextView5 = lf5.s;
            pq7.b(flexibleTextView5, "ftvCountPlayer");
            flexibleTextView5.setText(String.valueOf(intValue));
            int d2 = gl0.d(PortfolioApp.h0.c(), 2131099689);
            int d3 = gl0.d(PortfolioApp.h0.c(), 2131099677);
            if (pq7.a(b.r(), "activity_best_result")) {
                lf5.r.setTextColor(d3);
                FlexibleTextView flexibleTextView6 = lf5.v;
                pq7.b(flexibleTextView6, "ftvSteps");
                flexibleTextView6.setVisibility(8);
            } else {
                lf5.r.setTextColor(d2);
                FlexibleTextView flexibleTextView7 = lf5.v;
                pq7.b(flexibleTextView7, "ftvSteps");
                flexibleTextView7.setVisibility(0);
                FlexibleTextView flexibleTextView8 = lf5.v;
                pq7.b(flexibleTextView8, "ftvSteps");
                Integer q = b.q();
                if (q != null) {
                    str2 = String.valueOf(q.intValue());
                }
                flexibleTextView8.setText(str2);
            }
            FlexibleTextView flexibleTextView9 = lf5.u;
            pq7.b(flexibleTextView9, "ftvNew");
            flexibleTextView9.setVisibility(mt4.c() ? 0 : 8);
            String k = b.k();
            if (k != null && k.hashCode() == -314497661 && k.equals("private")) {
                lf5.s.setCompoundDrawablesWithIntrinsicBounds(2131231028, 0, 0, 0);
            } else {
                lf5.s.setCompoundDrawablesWithIntrinsicBounds(2131231029, 0, 0, 0);
            }
        }
    }

    @DexIgnore
    public sx4(int i) {
        this.d = i;
        f57.b f = f57.a().f();
        pq7.b(f, "TextDrawable.builder().round()");
        this.b = f;
    }

    @DexIgnore
    public final int a() {
        return this.d;
    }

    @DexIgnore
    public boolean b(List<? extends Object> list, int i) {
        pq7.c(list, "items");
        Object obj = list.get(i);
        return (obj instanceof mt4) && pq7.a(((mt4) obj).a(), "available-challenge");
    }

    @DexIgnore
    public void c(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        a aVar = null;
        pq7.c(list, "items");
        pq7.c(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof mt4)) {
            obj = null;
        }
        mt4 mt4 = (mt4) obj;
        if (viewHolder instanceof a) {
            aVar = viewHolder;
        }
        a aVar2 = aVar;
        if (mt4 != null && aVar2 != null) {
            aVar2.a(mt4, this.f3329a, this.b, this.c);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder d(ViewGroup viewGroup) {
        pq7.c(viewGroup, "parent");
        lf5 z = lf5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemRecommendationChalle\u2026(inflater, parent, false)");
        View n = z.n();
        pq7.b(n, "binding.root");
        return new a(z, n);
    }

    @DexIgnore
    public final void e(TimerViewObserver timerViewObserver) {
        this.f3329a = timerViewObserver;
    }
}
