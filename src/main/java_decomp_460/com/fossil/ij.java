package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ij extends bi {
    @DexIgnore
    public zk1 V;

    @DexIgnore
    public ij(k5 k5Var, i60 i60, short s, String str) {
        super(k5Var, i60, yp.d, s, false, zm7.i(hl7.a(hu1.SKIP_ERASE, Boolean.TRUE), hl7.a(hu1.NUMBER_OF_FILE_REQUIRED, 1), hl7.a(hu1.ERASE_CACHE_FILE_BEFORE_GET, Boolean.TRUE)), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 80);
        this.V = new zk1(k5Var.C(), k5Var.x, "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
    }

    @DexIgnore
    @Override // com.fossil.bi, com.fossil.lp
    public JSONObject E() {
        return g80.k(super.E(), jd0.b, this.V.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.bi
    public void M(ArrayList<j0> arrayList) {
        l(this.v);
    }

    @DexIgnore
    @Override // com.fossil.bi
    public void Q(j0 j0Var) {
        zq zqVar;
        super.Q(j0Var);
        try {
            this.V = zk1.a((zk1) eb.d.f(j0Var.f), this.V.getName(), this.V.getMacAddress(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262140);
            zqVar = zq.SUCCESS;
        } catch (sx1 e) {
            d90.i.i(e);
            zqVar = zq.UNSUPPORTED_FORMAT;
        }
        this.v = nr.a(this.v, null, zqVar, null, null, 13);
    }

    @DexIgnore
    @Override // com.fossil.bi, com.fossil.lp
    public Object x() {
        return this.V;
    }
}
