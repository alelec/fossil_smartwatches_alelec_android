package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x43 implements y43 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f4039a;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f4039a = hw2.d("measurement.androidId.delete_feature", true);
        hw2.d("measurement.log_androidId_enabled", false);
    }
    */

    @DexIgnore
    @Override // com.fossil.y43
    public final boolean zza() {
        return f4039a.o().booleanValue();
    }
}
