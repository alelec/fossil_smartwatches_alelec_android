package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class x05 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ PresetRepository.Anon17 b;
    @DexIgnore
    public /* final */ /* synthetic */ List c;
    @DexIgnore
    public /* final */ /* synthetic */ PresetDataSource.DeleteMappingSetCallback d;

    @DexIgnore
    public /* synthetic */ x05(PresetRepository.Anon17 anon17, List list, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        this.b = anon17;
        this.c = list;
        this.d = deleteMappingSetCallback;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c, this.d);
    }
}
