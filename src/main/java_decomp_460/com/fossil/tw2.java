package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tw2<T> implements Serializable {
    @DexIgnore
    public static <T> tw2<T> zza(T t) {
        sw2.b(t);
        return new vw2(t);
    }

    @DexIgnore
    public static <T> tw2<T> zzc() {
        return pw2.zza;
    }

    @DexIgnore
    public abstract boolean zza();

    @DexIgnore
    public abstract T zzb();
}
