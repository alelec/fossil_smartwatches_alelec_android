package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ig0;
import com.fossil.yo0;
import com.google.android.material.internal.NavigationMenuItemView;
import com.google.android.material.internal.NavigationMenuView;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dz3 implements ig0 {
    @DexIgnore
    public int A; // = -1;
    @DexIgnore
    public /* final */ View.OnClickListener B; // = new a();
    @DexIgnore
    public NavigationMenuView b;
    @DexIgnore
    public LinearLayout c;
    @DexIgnore
    public ig0.a d;
    @DexIgnore
    public cg0 e;
    @DexIgnore
    public int f;
    @DexIgnore
    public c g;
    @DexIgnore
    public LayoutInflater h;
    @DexIgnore
    public int i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public ColorStateList l;
    @DexIgnore
    public Drawable m;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w; // = true;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            boolean z = true;
            dz3.this.J(true);
            eg0 itemData = ((NavigationMenuItemView) view).getItemData();
            dz3 dz3 = dz3.this;
            boolean O = dz3.e.O(itemData, dz3, 0);
            if (itemData == null || !itemData.isCheckable() || !O) {
                z = false;
            } else {
                dz3.this.g.p(itemData);
            }
            dz3.this.J(false);
            if (z) {
                dz3.this.c(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends l {
        @DexIgnore
        public b(View view) {
            super(view);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.g<l> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ArrayList<e> f855a; // = new ArrayList<>();
        @DexIgnore
        public eg0 b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public c() {
            n();
        }

        @DexIgnore
        public final void g(int i, int i2) {
            while (i < i2) {
                ((g) this.f855a.get(i)).b = true;
                i++;
            }
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public int getItemCount() {
            return this.f855a.size();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public int getItemViewType(int i) {
            e eVar = this.f855a.get(i);
            if (eVar instanceof f) {
                return 2;
            }
            if (eVar instanceof d) {
                return 3;
            }
            if (eVar instanceof g) {
                return ((g) eVar).a().hasSubMenu() ? 1 : 0;
            }
            throw new RuntimeException("Unknown item type.");
        }

        @DexIgnore
        public Bundle h() {
            Bundle bundle = new Bundle();
            eg0 eg0 = this.b;
            if (eg0 != null) {
                bundle.putInt("android:menu:checked", eg0.getItemId());
            }
            SparseArray<? extends Parcelable> sparseArray = new SparseArray<>();
            int size = this.f855a.size();
            for (int i = 0; i < size; i++) {
                e eVar = this.f855a.get(i);
                if (eVar instanceof g) {
                    eg0 a2 = ((g) eVar).a();
                    View actionView = a2 != null ? a2.getActionView() : null;
                    if (actionView != null) {
                        fz3 fz3 = new fz3();
                        actionView.saveHierarchyState(fz3);
                        sparseArray.put(a2.getItemId(), fz3);
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:action_views", sparseArray);
            return bundle;
        }

        @DexIgnore
        public eg0 i() {
            return this.b;
        }

        @DexIgnore
        public int j() {
            int i;
            int i2;
            if (dz3.this.c.getChildCount() == 0) {
                i2 = 0;
                i = 0;
            } else {
                i = 1;
                i2 = 0;
            }
            while (i2 < dz3.this.g.getItemCount()) {
                int i3 = dz3.this.g.getItemViewType(i2) == 0 ? i + 1 : i;
                i2++;
                i = i3;
            }
            return i;
        }

        @DexIgnore
        /* renamed from: k */
        public void onBindViewHolder(l lVar, int i) {
            int itemViewType = getItemViewType(i);
            if (itemViewType == 0) {
                NavigationMenuItemView navigationMenuItemView = (NavigationMenuItemView) lVar.itemView;
                navigationMenuItemView.setIconTintList(dz3.this.l);
                dz3 dz3 = dz3.this;
                if (dz3.j) {
                    navigationMenuItemView.setTextAppearance(dz3.i);
                }
                ColorStateList colorStateList = dz3.this.k;
                if (colorStateList != null) {
                    navigationMenuItemView.setTextColor(colorStateList);
                }
                Drawable drawable = dz3.this.m;
                mo0.o0(navigationMenuItemView, drawable != null ? drawable.getConstantState().newDrawable() : null);
                g gVar = (g) this.f855a.get(i);
                navigationMenuItemView.setNeedsEmptyIcon(gVar.b);
                navigationMenuItemView.setHorizontalPadding(dz3.this.s);
                navigationMenuItemView.setIconPadding(dz3.this.t);
                dz3 dz32 = dz3.this;
                if (dz32.v) {
                    navigationMenuItemView.setIconSize(dz32.u);
                }
                navigationMenuItemView.setMaxLines(dz3.this.x);
                navigationMenuItemView.f(gVar.a(), 0);
            } else if (itemViewType == 1) {
                ((TextView) lVar.itemView).setText(((g) this.f855a.get(i)).a().getTitle());
            } else if (itemViewType == 2) {
                f fVar = (f) this.f855a.get(i);
                lVar.itemView.setPadding(0, fVar.b(), 0, fVar.a());
            }
        }

        @DexIgnore
        /* renamed from: l */
        public l onCreateViewHolder(ViewGroup viewGroup, int i) {
            if (i == 0) {
                dz3 dz3 = dz3.this;
                return new i(dz3.h, viewGroup, dz3.B);
            } else if (i == 1) {
                return new k(dz3.this.h, viewGroup);
            } else {
                if (i == 2) {
                    return new j(dz3.this.h, viewGroup);
                }
                if (i != 3) {
                    return null;
                }
                return new b(dz3.this.c);
            }
        }

        @DexIgnore
        /* renamed from: m */
        public void onViewRecycled(l lVar) {
            if (lVar instanceof i) {
                ((NavigationMenuItemView) lVar.itemView).g();
            }
        }

        @DexIgnore
        public final void n() {
            int i;
            boolean z;
            int i2;
            if (!this.c) {
                this.c = true;
                this.f855a.clear();
                this.f855a.add(new d());
                int i3 = -1;
                int size = dz3.this.e.G().size();
                boolean z2 = false;
                int i4 = 0;
                int i5 = 0;
                while (i5 < size) {
                    eg0 eg0 = dz3.this.e.G().get(i5);
                    if (eg0.isChecked()) {
                        p(eg0);
                    }
                    if (eg0.isCheckable()) {
                        eg0.t(false);
                    }
                    if (eg0.hasSubMenu()) {
                        SubMenu subMenu = eg0.getSubMenu();
                        if (subMenu.hasVisibleItems()) {
                            if (i5 != 0) {
                                this.f855a.add(new f(dz3.this.z, 0));
                            }
                            this.f855a.add(new g(eg0));
                            int size2 = this.f855a.size();
                            int size3 = subMenu.size();
                            boolean z3 = false;
                            for (int i6 = 0; i6 < size3; i6++) {
                                eg0 eg02 = (eg0) subMenu.getItem(i6);
                                if (eg02.isVisible()) {
                                    if (!z3 && eg02.getIcon() != null) {
                                        z3 = true;
                                    }
                                    if (eg02.isCheckable()) {
                                        eg02.t(false);
                                    }
                                    if (eg0.isChecked()) {
                                        p(eg0);
                                    }
                                    this.f855a.add(new g(eg02));
                                }
                            }
                            if (z3) {
                                g(size2, this.f855a.size());
                                i2 = i3;
                            }
                        }
                        i2 = i3;
                    } else {
                        int groupId = eg0.getGroupId();
                        if (groupId != i3) {
                            i = this.f855a.size();
                            z = eg0.getIcon() != null;
                            if (i5 != 0) {
                                i++;
                                ArrayList<e> arrayList = this.f855a;
                                int i7 = dz3.this.z;
                                arrayList.add(new f(i7, i7));
                            }
                        } else if (z2 || eg0.getIcon() == null) {
                            i = i4;
                            z = z2;
                        } else {
                            g(i4, this.f855a.size());
                            z = true;
                            i = i4;
                        }
                        g gVar = new g(eg0);
                        gVar.b = z;
                        this.f855a.add(gVar);
                        i4 = i;
                        z2 = z;
                        i2 = groupId;
                    }
                    i5++;
                    i3 = i2;
                }
                this.c = false;
            }
        }

        @DexIgnore
        public void o(Bundle bundle) {
            eg0 a2;
            View actionView;
            fz3 fz3;
            eg0 a3;
            int i = bundle.getInt("android:menu:checked", 0);
            if (i != 0) {
                this.c = true;
                int size = this.f855a.size();
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    }
                    e eVar = this.f855a.get(i2);
                    if ((eVar instanceof g) && (a3 = ((g) eVar).a()) != null && a3.getItemId() == i) {
                        p(a3);
                        break;
                    }
                    i2++;
                }
                this.c = false;
                n();
            }
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:action_views");
            if (sparseParcelableArray != null) {
                int size2 = this.f855a.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    e eVar2 = this.f855a.get(i3);
                    if (!(!(eVar2 instanceof g) || (a2 = ((g) eVar2).a()) == null || (actionView = a2.getActionView()) == null || (fz3 = (fz3) sparseParcelableArray.get(a2.getItemId())) == null)) {
                        actionView.restoreHierarchyState(fz3);
                    }
                }
            }
        }

        @DexIgnore
        public void p(eg0 eg0) {
            if (this.b != eg0 && eg0.isCheckable()) {
                eg0 eg02 = this.b;
                if (eg02 != null) {
                    eg02.setChecked(false);
                }
                this.b = eg0;
                eg0.setChecked(true);
            }
        }

        @DexIgnore
        public void q(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public void r() {
            n();
            notifyDataSetChanged();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements e {
    }

    @DexIgnore
    public interface e {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f856a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public f(int i, int i2) {
            this.f856a = i;
            this.b = i2;
        }

        @DexIgnore
        public int a() {
            return this.b;
        }

        @DexIgnore
        public int b() {
            return this.f856a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g implements e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ eg0 f857a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public g(eg0 eg0) {
            this.f857a = eg0;
        }

        @DexIgnore
        public eg0 a() {
            return this.f857a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends nv0 {
        @DexIgnore
        public h(RecyclerView recyclerView) {
            super(recyclerView);
        }

        @DexIgnore
        @Override // com.fossil.rn0, com.fossil.nv0
        public void g(View view, yo0 yo0) {
            super.g(view, yo0);
            yo0.e0(yo0.b.a(dz3.this.g.j(), 0, false));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i extends l {
        @DexIgnore
        public i(LayoutInflater layoutInflater, ViewGroup viewGroup, View.OnClickListener onClickListener) {
            super(layoutInflater.inflate(pw3.design_navigation_item, viewGroup, false));
            this.itemView.setOnClickListener(onClickListener);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class j extends l {
        @DexIgnore
        public j(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(pw3.design_navigation_item_separator, viewGroup, false));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class k extends l {
        @DexIgnore
        public k(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(pw3.design_navigation_item_subheader, viewGroup, false));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class l extends RecyclerView.ViewHolder {
        @DexIgnore
        public l(View view) {
            super(view);
        }
    }

    @DexIgnore
    public void A(Drawable drawable) {
        this.m = drawable;
        c(false);
    }

    @DexIgnore
    public void B(int i2) {
        this.s = i2;
        c(false);
    }

    @DexIgnore
    public void C(int i2) {
        this.t = i2;
        c(false);
    }

    @DexIgnore
    public void D(int i2) {
        if (this.u != i2) {
            this.u = i2;
            this.v = true;
            c(false);
        }
    }

    @DexIgnore
    public void E(ColorStateList colorStateList) {
        this.l = colorStateList;
        c(false);
    }

    @DexIgnore
    public void F(int i2) {
        this.x = i2;
        c(false);
    }

    @DexIgnore
    public void G(int i2) {
        this.i = i2;
        this.j = true;
        c(false);
    }

    @DexIgnore
    public void H(ColorStateList colorStateList) {
        this.k = colorStateList;
        c(false);
    }

    @DexIgnore
    public void I(int i2) {
        this.A = i2;
        NavigationMenuView navigationMenuView = this.b;
        if (navigationMenuView != null) {
            navigationMenuView.setOverScrollMode(i2);
        }
    }

    @DexIgnore
    public void J(boolean z2) {
        c cVar = this.g;
        if (cVar != null) {
            cVar.q(z2);
        }
    }

    @DexIgnore
    public final void K() {
        int i2 = (this.c.getChildCount() != 0 || !this.w) ? 0 : this.y;
        NavigationMenuView navigationMenuView = this.b;
        navigationMenuView.setPadding(0, i2, 0, navigationMenuView.getPaddingBottom());
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void b(cg0 cg0, boolean z2) {
        ig0.a aVar = this.d;
        if (aVar != null) {
            aVar.b(cg0, z2);
        }
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void c(boolean z2) {
        c cVar = this.g;
        if (cVar != null) {
            cVar.r();
        }
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean d() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean e(cg0 cg0, eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean f(cg0 cg0, eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void g(ig0.a aVar) {
        this.d = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public int getId() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void h(Context context, cg0 cg0) {
        this.h = LayoutInflater.from(context);
        this.e = cg0;
        this.z = context.getResources().getDimensionPixelOffset(lw3.design_navigation_separator_vertical_padding);
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void i(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            SparseArray<Parcelable> sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
            if (sparseParcelableArray != null) {
                this.b.restoreHierarchyState(sparseParcelableArray);
            }
            Bundle bundle2 = bundle.getBundle("android:menu:adapter");
            if (bundle2 != null) {
                this.g.o(bundle2);
            }
            SparseArray sparseParcelableArray2 = bundle.getSparseParcelableArray("android:menu:header");
            if (sparseParcelableArray2 != null) {
                this.c.restoreHierarchyState(sparseParcelableArray2);
            }
        }
    }

    @DexIgnore
    public void j(View view) {
        this.c.addView(view);
        NavigationMenuView navigationMenuView = this.b;
        navigationMenuView.setPadding(0, 0, 0, navigationMenuView.getPaddingBottom());
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean k(ng0 ng0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public Parcelable l() {
        Bundle bundle = new Bundle();
        if (this.b != null) {
            SparseArray<Parcelable> sparseArray = new SparseArray<>();
            this.b.saveHierarchyState(sparseArray);
            bundle.putSparseParcelableArray("android:menu:list", sparseArray);
        }
        c cVar = this.g;
        if (cVar != null) {
            bundle.putBundle("android:menu:adapter", cVar.h());
        }
        if (this.c != null) {
            SparseArray<? extends Parcelable> sparseArray2 = new SparseArray<>();
            this.c.saveHierarchyState(sparseArray2);
            bundle.putSparseParcelableArray("android:menu:header", sparseArray2);
        }
        return bundle;
    }

    @DexIgnore
    public void m(vo0 vo0) {
        int g2 = vo0.g();
        if (this.y != g2) {
            this.y = g2;
            K();
        }
        NavigationMenuView navigationMenuView = this.b;
        navigationMenuView.setPadding(0, navigationMenuView.getPaddingTop(), 0, vo0.d());
        mo0.g(this.c, vo0);
    }

    @DexIgnore
    public eg0 n() {
        return this.g.i();
    }

    @DexIgnore
    public int o() {
        return this.c.getChildCount();
    }

    @DexIgnore
    public Drawable p() {
        return this.m;
    }

    @DexIgnore
    public int q() {
        return this.s;
    }

    @DexIgnore
    public int r() {
        return this.t;
    }

    @DexIgnore
    public int s() {
        return this.x;
    }

    @DexIgnore
    public ColorStateList t() {
        return this.k;
    }

    @DexIgnore
    public ColorStateList u() {
        return this.l;
    }

    @DexIgnore
    public jg0 v(ViewGroup viewGroup) {
        if (this.b == null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) this.h.inflate(pw3.design_navigation_menu, viewGroup, false);
            this.b = navigationMenuView;
            navigationMenuView.setAccessibilityDelegateCompat(new h(this.b));
            if (this.g == null) {
                this.g = new c();
            }
            int i2 = this.A;
            if (i2 != -1) {
                this.b.setOverScrollMode(i2);
            }
            this.c = (LinearLayout) this.h.inflate(pw3.design_navigation_item_header, (ViewGroup) this.b, false);
            this.b.setAdapter(this.g);
        }
        return this.b;
    }

    @DexIgnore
    public View w(int i2) {
        View inflate = this.h.inflate(i2, (ViewGroup) this.c, false);
        j(inflate);
        return inflate;
    }

    @DexIgnore
    public void x(boolean z2) {
        if (this.w != z2) {
            this.w = z2;
            K();
        }
    }

    @DexIgnore
    public void y(eg0 eg0) {
        this.g.p(eg0);
    }

    @DexIgnore
    public void z(int i2) {
        this.f = i2;
    }
}
