package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.jn5;
import com.fossil.s87;
import com.fossil.w67;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a87 extends hq4 {
    @DexIgnore
    public /* final */ FileRepository A;
    @DexIgnore
    public /* final */ s77 B;
    @DexIgnore
    public /* final */ DianaAppSettingRepository C;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository D;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<gl7<Boolean, Boolean, Integer>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<ub7> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Object> k; // = new MutableLiveData<>();
    @DexIgnore
    public mo5 l;
    @DexIgnore
    public a m;
    @DexIgnore
    public ob7 n; // = new ob7(null, null, 2, null);
    @DexIgnore
    public ob7 o; // = new ob7(null, null, 2, null);
    @DexIgnore
    public /* final */ List<DianaAppSetting> p; // = new ArrayList();
    @DexIgnore
    public /* final */ MutableLiveData<b> q; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<s87>> r; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Set<String> s; // = new LinkedHashSet();
    @DexIgnore
    public /* final */ Map<Integer, Object> t; // = new LinkedHashMap();
    @DexIgnore
    public /* final */ u08 u; // = w08.b(false, 1, null);
    @DexIgnore
    public /* final */ q v; // = new q(this);
    @DexIgnore
    public /* final */ zm5 w;
    @DexIgnore
    public /* final */ UserRepository x;
    @DexIgnore
    public /* final */ CustomizeRealDataRepository y;
    @DexIgnore
    public /* final */ uo5 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ l77 f215a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public a(l77 l77, String str) {
            pq7.c(l77, "type");
            pq7.c(str, "name");
            this.f215a = l77;
            this.b = str;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final l77 b() {
            return this.f215a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!pq7.a(this.f215a, aVar.f215a) || !pq7.a(this.b, aVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            l77 l77 = this.f215a;
            int hashCode = l77 != null ? l77.hashCode() : 0;
            String str = this.b;
            if (str != null) {
                i = str.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "BackgroundData(type=" + this.f215a + ", name=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ab7 f216a;
        @DexIgnore
        public /* final */ bb7 b;

        @DexIgnore
        public b(ab7 ab7, bb7 bb7) {
            pq7.c(ab7, Constants.EVENT);
            pq7.c(bb7, "complication");
            this.f216a = ab7;
            this.b = bb7;
        }

        @DexIgnore
        public final bb7 a() {
            return this.b;
        }

        @DexIgnore
        public final ab7 b() {
            return this.f216a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (!pq7.a(this.f216a, bVar.f216a) || !pq7.a(this.b, bVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            ab7 ab7 = this.f216a;
            int hashCode = ab7 != null ? ab7.hashCode() : 0;
            bb7 bb7 = this.b;
            if (bb7 != null) {
                i = bb7.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "ComplicationWrapperEvent(event=" + this.f216a + ", complication=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$applyTheme$2", f = "WatchFaceEditViewModel.kt", l = {241}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ a87 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$applyTheme$2$1", f = "WatchFaceEditViewModel.kt", l = {242, 245}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0049  */
            /* JADX WARNING: Removed duplicated region for block: B:18:0x0086  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r3 = 0
                    r6 = 2
                    r4 = 1
                    r1 = 0
                    java.lang.Object r5 = com.fossil.yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x0071
                    if (r0 == r4) goto L_0x0037
                    if (r0 != r6) goto L_0x002f
                    java.lang.Object r0 = r7.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r8)
                L_0x0017:
                    com.fossil.b77 r0 = com.fossil.b77.f401a
                    com.fossil.a87$c r1 = r7.this$0
                    com.fossil.a87 r1 = r1.this$0
                    com.fossil.ob7 r1 = com.fossil.a87.p(r1)
                    com.fossil.a87$c r2 = r7.this$0
                    com.fossil.a87 r2 = r2.this$0
                    com.fossil.a87$a r2 = com.fossil.a87.q(r2)
                    r0.g(r1, r2)
                L_0x002c:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x002e:
                    return r0
                L_0x002f:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0037:
                    java.lang.Object r0 = r7.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r8)
                    r4 = r0
                    r2 = r8
                L_0x0040:
                    r0 = r2
                    java.lang.Boolean r0 = (java.lang.Boolean) r0
                    boolean r0 = r0.booleanValue()
                    if (r0 == 0) goto L_0x0086
                    com.fossil.a87$c r0 = r7.this$0
                    com.fossil.a87 r0 = r0.this$0
                    com.fossil.a87.I(r0)
                    com.fossil.a87$c r0 = r7.this$0
                    com.fossil.a87 r0 = r0.this$0
                    com.fossil.a87.n(r0)
                    com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0
                    com.portfolio.platform.PortfolioApp r0 = r0.c()
                    com.fossil.a87$c r1 = r7.this$0
                    com.fossil.a87 r1 = r1.this$0
                    com.fossil.ob7 r1 = com.fossil.a87.p(r1)
                    r7.L$0 = r4
                    r7.label = r6
                    java.lang.Object r0 = r0.q(r1, r7)
                    if (r0 != r5) goto L_0x0017
                    r0 = r5
                    goto L_0x002e
                L_0x0071:
                    com.fossil.el7.b(r8)
                    com.fossil.iv7 r0 = r7.p$
                    com.fossil.a87$c r2 = r7.this$0
                    com.fossil.a87 r2 = r2.this$0
                    r7.L$0 = r0
                    r7.label = r4
                    java.lang.Object r2 = r2.Q(r7)
                    if (r2 != r5) goto L_0x009b
                    r0 = r5
                    goto L_0x002e
                L_0x0086:
                    com.fossil.a87$c r0 = r7.this$0
                    com.fossil.a87 r0 = r0.this$0
                    r4 = 6
                    r2 = r1
                    r5 = r3
                    com.fossil.hq4.d(r0, r1, r2, r3, r4, r5)
                    com.fossil.a87$c r0 = r7.this$0
                    com.fossil.a87 r0 = r0.this$0
                    r1 = -1
                    java.lang.String r2 = "Fail to download data required for this theme"
                    com.fossil.a87.o(r0, r1, r2)
                    goto L_0x002c
                L_0x009b:
                    r4 = r0
                    goto L_0x0040
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.a87.c.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(a87 a87, qn7 qn7) {
            super(2, qn7);
            this.this$0 = a87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                hq4.d(this.this$0, true, false, null, 6, null);
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(b, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel", f = "WatchFaceEditViewModel.kt", l = {530}, m = "checkAndDownloadThemeData")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ a87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(a87 a87, qn7 qn7) {
            super(qn7);
            this.this$0 = a87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.Q(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$generateThemeBinary$1", f = "WatchFaceEditViewModel.kt", l = {217, 220, 223}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ a87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(a87 a87, qn7 qn7) {
            super(2, qn7);
            this.this$0 = a87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x004d  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0076  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00d7  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00ef  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 257
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.a87.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$getComplicationData$1", f = "WatchFaceEditViewModel.kt", l = {453}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $complicationId;
        @DexIgnore
        public /* final */ /* synthetic */ za7 $dimension;
        @DexIgnore
        public /* final */ /* synthetic */ ab7 $event;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ a87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(a87 a87, String str, za7 za7, ab7 ab7, qn7 qn7) {
            super(2, qn7);
            this.this$0 = a87;
            this.$complicationId = str;
            this.$dimension = za7;
            this.$event = ab7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, this.$complicationId, this.$dimension, this.$event, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object X;
            Object obj2;
            String str = null;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                a87 a87 = this.this$0;
                String str2 = this.$complicationId;
                this.L$0 = iv7;
                this.label = 1;
                X = a87.X(str2, this);
                if (X == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                X = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str3 = (String) X;
            if (pq7.a(this.$complicationId, "second-timezone")) {
                Iterator it = this.this$0.p.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    Object next = it.next();
                    if (ao7.a(pq7.a(((DianaAppSetting) next).getAppId(), this.$complicationId)).booleanValue()) {
                        obj2 = next;
                        break;
                    }
                }
                DianaAppSetting dianaAppSetting = (DianaAppSetting) obj2;
                if (dianaAppSetting != null) {
                    str = dianaAppSetting.getSetting();
                }
            }
            this.this$0.q.l(new b(this.$event, new bb7(this.$complicationId, str3, str, this.$dimension)));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel", f = "WatchFaceEditViewModel.kt", l = {479, 480}, m = "getComplicationSettingData")
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ a87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(a87 a87, qn7 qn7) {
            super(qn7);
            this.this$0 = a87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.X(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$handleResultFromThemeAction$1", f = "WatchFaceEditViewModel.kt", l = {272, 290}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $action;
        @DexIgnore
        public /* final */ /* synthetic */ int $errorCode;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isSucceeded;
        @DexIgnore
        public /* final */ /* synthetic */ byte[] $themeBinary;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ a87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(a87 a87, String str, boolean z, byte[] bArr, int i, qn7 qn7) {
            super(2, qn7);
            this.this$0 = a87;
            this.$action = str;
            this.$isSucceeded = z;
            this.$themeBinary = bArr;
            this.$errorCode = i;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, this.$action, this.$isSucceeded, this.$themeBinary, this.$errorCode, qn7);
            hVar.p$ = (iv7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                String str = this.$action;
                int hashCode = str.hashCode();
                if (hashCode != -1937539297) {
                    if (hashCode != -442528637) {
                        if (hashCode == 1718094493 && str.equals("apply_theme_action")) {
                            if (this.$isSucceeded) {
                                byte[] bArr = this.$themeBinary;
                                if (bArr != null) {
                                    a87 a87 = this.this$0;
                                    this.L$0 = iv7;
                                    this.label = 1;
                                    if (a87.o0(bArr, this) == d) {
                                        return d;
                                    }
                                } else {
                                    hq4.d(this.this$0, false, false, null, 6, null);
                                    this.this$0.a(-1, "apply them succeed but no data received");
                                }
                            } else {
                                this.this$0.i.l(new gl7(ao7.a(false), ao7.a(false), ao7.e(this.$errorCode)));
                                hq4.d(this.this$0, false, false, null, 6, null);
                            }
                        }
                    } else if (str.equals("preview_theme_action")) {
                        this.this$0.i.l(new gl7(ao7.a(this.$isSucceeded), ao7.a(false), ao7.e(this.$errorCode)));
                        hq4.d(this.this$0, false, false, null, 6, null);
                    }
                } else if (str.equals("parse_theme_data_to_bin_action")) {
                    this.this$0.i.l(new gl7(ao7.a(this.$isSucceeded), ao7.a(false), ao7.e(this.$errorCode)));
                    a87 a872 = this.this$0;
                    byte[] bArr2 = this.$themeBinary;
                    this.L$0 = iv7;
                    this.label = 2;
                    if (a872.g0(bArr2, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1 || i == 2) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel", f = "WatchFaceEditViewModel.kt", l = {261}, m = "handleThemeBinaryOfInactivePreset")
    public static final class i extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ a87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(a87 a87, qn7 qn7) {
            super(qn7);
            this.this$0 = a87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g0(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel", f = "WatchFaceEditViewModel.kt", l = {465}, m = "isReachedMaxPreset")
    public static final class j extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ a87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(a87 a87, qn7 qn7) {
            super(qn7);
            this.this$0 = a87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$isReachedMaxPreset$totalPreset$1", f = "WatchFaceEditViewModel.kt", l = {465}, m = "invokeSuspend")
    public static final class k extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ a87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(a87 a87, qn7 qn7) {
            super(2, qn7);
            this.this$0 = a87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            k kVar = new k(this.this$0, qn7);
            kVar.p$ = (iv7) obj;
            return kVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
            return ((k) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                uo5 uo5 = this.this$0.z;
                String J = PortfolioApp.h0.c().J();
                this.L$0 = iv7;
                this.label = 1;
                Object i2 = uo5.i(J, this);
                return i2 == d ? d : i2;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$loadWatchFace$2", f = "WatchFaceEditViewModel.kt", l = {389, MFNetworkReturnCode.WRONG_PASSWORD, 414, MFNetworkReturnCode.CONTENT_TYPE_ERROR, 416, 434}, m = "invokeSuspend")
    public static final class l extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ a87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(a87 a87, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = a87;
            this.$presetId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            l lVar = new l(this.this$0, this.$presetId, qn7);
            lVar.p$ = (iv7) obj;
            return lVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((l) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0113  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0133  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x01b2  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0240  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x02ca  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x02f1  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0387  */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x0457  */
        /* JADX WARNING: Removed duplicated region for block: B:65:0x0465  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x047b  */
        /* JADX WARNING: Removed duplicated region for block: B:69:0x0484  */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x0477 A[SYNTHETIC] */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r25) {
            /*
            // Method dump skipped, instructions count: 1196
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.a87.l.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$onBackgroundChanged$1", f = "WatchFaceEditViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class m extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ fb7 $newBackground;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ a87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(a87 a87, fb7 fb7, qn7 qn7) {
            super(2, qn7);
            this.this$0 = a87;
            this.$newBackground = fb7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            m mVar = new m(this.this$0, this.$newBackground, qn7);
            mVar.p$ = (iv7) obj;
            return mVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((m) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            File file;
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                fb7 fb7 = this.$newBackground;
                if (fb7 != null) {
                    this.this$0.m = new a(fb7.a(), this.$newBackground.d());
                    if (this.$newBackground.c() == null) {
                        file = this.this$0.A.getFileByRemoteUrl(this.$newBackground.g());
                    } else {
                        String c = this.$newBackground.c();
                        if (c != null) {
                            file = new File(c);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                    byte[] b = file != null ? j37.b(file) : null;
                    if (b == null) {
                        String g = this.$newBackground.g();
                        if (!(g == null || g.length() == 0)) {
                            Set set = this.this$0.s;
                            String g2 = this.$newBackground.g();
                            if (g2 != null) {
                                set.add(g2);
                                FileRepository fileRepository = this.this$0.A;
                                String g3 = this.$newBackground.g();
                                if (g3 != null) {
                                    FileRepository.asyncDownloadFromUrl$default(fileRepository, g3, FileType.WATCH_FACE, null, 4, null);
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                    }
                    this.this$0.n.d(new hb7(b, this.$newBackground.b()));
                } else {
                    this.this$0.n.d(null);
                }
                this.this$0.h.l(ao7.a(!pq7.a(this.this$0.n, this.this$0.o)));
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$onMovingObjectChanged$1", f = "WatchFaceEditViewModel.kt", l = {689, 486}, m = "invokeSuspend")
    public static final class n extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ s87 $elementConfig;
        @DexIgnore
        public /* final */ /* synthetic */ w67.b $type;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ a87 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ n this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(qn7 qn7, n nVar) {
                super(2, qn7);
                this.this$0 = nVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    List<gb7> a2 = this.this$0.this$0.n.a();
                    int i2 = b87.f402a[this.this$0.$type.ordinal()];
                    if (i2 == 1) {
                        n nVar = this.this$0;
                        a2.add(nVar.this$0.p0(nVar.$elementConfig));
                    } else if (i2 == 2) {
                        n nVar2 = this.this$0;
                        a2.add(nVar2.this$0.p0(nVar2.$elementConfig));
                        n nVar3 = this.this$0;
                        nVar3.this$0.n0(nVar3.$elementConfig.c(), a2);
                    } else if (i2 == 3) {
                        n nVar4 = this.this$0;
                        nVar4.this$0.n0(nVar4.$elementConfig.a(), a2);
                    }
                    this.this$0.this$0.n.c(a2);
                    this.this$0.this$0.h.l(ao7.a(!pq7.a(this.this$0.this$0.n, this.this$0.this$0.o)));
                    n nVar5 = this.this$0;
                    a87 a87 = nVar5.this$0;
                    s87 s87 = nVar5.$elementConfig;
                    this.L$0 = iv7;
                    this.L$1 = a2;
                    this.label = 1;
                    if (a87.R(s87, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    List list = (List) this.L$1;
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(a87 a87, w67.b bVar, s87 s87, qn7 qn7) {
            super(2, qn7);
            this.this$0 = a87;
            this.$type = bVar;
            this.$elementConfig = s87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            n nVar = new n(this.this$0, this.$type, this.$elementConfig, qn7);
            nVar.p$ = (iv7) obj;
            return nVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((n) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x004c  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r1 = 2
                r3 = 1
                r6 = 0
                java.lang.Object r2 = com.fossil.yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x004e
                if (r0 == r3) goto L_0x002a
                if (r0 != r1) goto L_0x0022
                java.lang.Object r0 = r7.L$1
                com.fossil.u08 r0 = (com.fossil.u08) r0
                java.lang.Object r1 = r7.L$0
                com.fossil.iv7 r1 = (com.fossil.iv7) r1
                com.fossil.el7.b(r8)     // Catch:{ all -> 0x006c }
            L_0x001a:
                com.fossil.tl7 r1 = com.fossil.tl7.f3441a     // Catch:{ all -> 0x006c }
                r0.b(r6)
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0021:
                return r0
            L_0x0022:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x002a:
                java.lang.Object r0 = r7.L$1
                com.fossil.u08 r0 = (com.fossil.u08) r0
                java.lang.Object r1 = r7.L$0
                com.fossil.iv7 r1 = (com.fossil.iv7) r1
                com.fossil.el7.b(r8)
            L_0x0035:
                com.fossil.dv7 r3 = com.fossil.bw7.a()     // Catch:{ all -> 0x0067 }
                com.fossil.a87$n$a r4 = new com.fossil.a87$n$a     // Catch:{ all -> 0x0067 }
                r5 = 0
                r4.<init>(r5, r7)     // Catch:{ all -> 0x0067 }
                r7.L$0 = r1     // Catch:{ all -> 0x0067 }
                r7.L$1 = r0     // Catch:{ all -> 0x0067 }
                r1 = 2
                r7.label = r1     // Catch:{ all -> 0x0067 }
                java.lang.Object r1 = com.fossil.eu7.g(r3, r4, r7)     // Catch:{ all -> 0x0067 }
                if (r1 != r2) goto L_0x001a
                r0 = r2
                goto L_0x0021
            L_0x004e:
                com.fossil.el7.b(r8)
                com.fossil.iv7 r1 = r7.p$
                com.fossil.a87 r0 = r7.this$0
                com.fossil.u08 r0 = com.fossil.a87.v(r0)
                r7.L$0 = r1
                r7.L$1 = r0
                r7.label = r3
                java.lang.Object r3 = r0.a(r6, r7)
                if (r3 != r2) goto L_0x0035
                r0 = r2
                goto L_0x0021
            L_0x0067:
                r1 = move-exception
            L_0x0068:
                r0.b(r6)
                throw r1
            L_0x006c:
                r1 = move-exception
                goto L_0x0068
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.a87.n.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$previewTheme$1", f = "WatchFaceEditViewModel.kt", l = {200, 203}, m = "invokeSuspend")
    public static final class o extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ a87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(a87 a87, qn7 qn7) {
            super(2, qn7);
            this.this$0 = a87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            o oVar = new o(this.this$0, qn7);
            oVar.p$ = (iv7) obj;
            return oVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((o) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0070  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                r4 = 6
                r8 = 2
                r1 = 1
                r3 = 0
                r2 = 0
                java.lang.Object r7 = com.fossil.yn7.d()
                int r0 = r9.label
                if (r0 == 0) goto L_0x0057
                if (r0 == r1) goto L_0x0023
                if (r0 != r8) goto L_0x001b
                java.lang.Object r0 = r9.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r10)
            L_0x0018:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x001a:
                return r0
            L_0x001b:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0023:
                java.lang.Object r0 = r9.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r10)
                r5 = r0
                r1 = r10
            L_0x002c:
                r0 = r1
                java.lang.Boolean r0 = (java.lang.Boolean) r0
                boolean r0 = r0.booleanValue()
                if (r0 == 0) goto L_0x0070
                com.fossil.a87 r0 = r9.this$0
                com.fossil.a87.I(r0)
                com.fossil.a87 r0 = r9.this$0
                com.fossil.a87.n(r0)
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                com.fossil.a87 r1 = r9.this$0
                com.fossil.ob7 r1 = com.fossil.a87.p(r1)
                r9.L$0 = r5
                r9.label = r8
                java.lang.Object r0 = r0.Q0(r1, r9)
                if (r0 != r7) goto L_0x0018
                r0 = r7
                goto L_0x001a
            L_0x0057:
                com.fossil.el7.b(r10)
                com.fossil.iv7 r6 = r9.p$
                com.fossil.a87 r0 = r9.this$0
                r5 = r3
                com.fossil.hq4.d(r0, r1, r2, r3, r4, r5)
                com.fossil.a87 r0 = r9.this$0
                r9.L$0 = r6
                r9.label = r1
                java.lang.Object r1 = r0.Q(r9)
                if (r1 != r7) goto L_0x009d
                r0 = r7
                goto L_0x001a
            L_0x0070:
                com.fossil.a87 r0 = r9.this$0
                r1 = r2
                r5 = r3
                com.fossil.hq4.d(r0, r1, r2, r3, r4, r5)
                com.fossil.a87 r0 = r9.this$0
                r1 = -1
                java.lang.String r3 = "Fail to download data required for this theme"
                com.fossil.a87.o(r0, r1, r3)
                com.fossil.a87 r0 = r9.this$0
                androidx.lifecycle.MutableLiveData r0 = com.fossil.a87.B(r0)
                com.fossil.gl7 r1 = new com.fossil.gl7
                java.lang.Boolean r3 = com.fossil.ao7.a(r2)
                java.lang.Boolean r2 = com.fossil.ao7.a(r2)
                r4 = 600(0x258, float:8.41E-43)
                java.lang.Integer r4 = com.fossil.ao7.e(r4)
                r1.<init>(r3, r2, r4)
                r0.l(r1)
                goto L_0x0018
            L_0x009d:
                r5 = r6
                goto L_0x002c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.a87.o.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel", f = "WatchFaceEditViewModel.kt", l = {298, Action.Presenter.PREVIOUS, 311, 324, 339, 346}, m = "savePresetWithThemeBinary")
    public static final class p extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ a87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(a87 a87, qn7 qn7) {
            super(qn7);
            this.this$0 = a87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o0(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ a87 f217a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public q(a87 a87) {
            this.f217a = a87;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceEditViewModel", "wfBleConnection onReceive " + intent);
            if (intent != null) {
                String action = intent.getAction();
                if (action == null) {
                    action = "";
                }
                pq7.b(action, "it.action ?: \"\"");
                this.f217a.f0(action, intent.getBooleanExtra("theme_result_extra", true), intent.getByteArrayExtra(ButtonService.THEME_BINARY_EXTRA), intent.getIntExtra("error_code_extra", -1));
            }
        }
    }

    @DexIgnore
    public a87(zm5 zm5, UserRepository userRepository, CustomizeRealDataRepository customizeRealDataRepository, uo5 uo5, FileRepository fileRepository, s77 s77, DianaAppSettingRepository dianaAppSettingRepository, DianaWatchFaceRepository dianaWatchFaceRepository) {
        pq7.c(zm5, "customizeRealDataManager");
        pq7.c(userRepository, "userRepository");
        pq7.c(customizeRealDataRepository, "customizeRealDataRepository");
        pq7.c(uo5, "presetRepository");
        pq7.c(fileRepository, "fileRepository");
        pq7.c(s77, "wfAssetRepository");
        pq7.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        pq7.c(dianaWatchFaceRepository, "mWatchFaceRepository");
        this.w = zm5;
        this.x = userRepository;
        this.y = customizeRealDataRepository;
        this.z = uo5;
        this.A = fileRepository;
        this.B = s77;
        this.C = dianaAppSettingRepository;
        this.D = dianaWatchFaceRepository;
        i77.c.c();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("preview_theme_action");
        intentFilter.addAction("apply_theme_action");
        intentFilter.addAction("parse_theme_data_to_bin_action");
        ct0.b(PortfolioApp.h0.c()).c(this.v, intentFilter);
    }

    @DexIgnore
    public final void P() {
        byte[] h2;
        byte[] a2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("applyTheme - backgroud size: ");
        hb7 b2 = this.n.b();
        sb.append((b2 == null || (a2 = b2.a()) == null) ? null : Integer.valueOf(a2.length));
        sb.append(' ');
        sb.append("- complication counted: ");
        sb.append(this.n.a().size());
        sb.append(" - each size : ");
        List<gb7> a3 = this.n.a();
        ArrayList arrayList = new ArrayList(im7.m(a3, 10));
        for (T t2 : a3) {
            if (!(t2 instanceof ib7)) {
                t2 = null;
            }
            T t3 = t2;
            arrayList.add((t3 == null || (h2 = t3.h()) == null) ? null : Integer.valueOf(h2.length));
        }
        sb.append(arrayList);
        local.d("1MBDebugTag", sb.toString());
        xw7 unused = gu7.d(us0.a(this), null, null, new c(this, null), 3, null);
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0092 A[SYNTHETIC] */
    public final /* synthetic */ java.lang.Object Q(com.fossil.qn7<? super java.lang.Boolean> r11) {
        /*
            r10 = this;
            r3 = 0
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 1
            boolean r0 = r11 instanceof com.fossil.a87.d
            if (r0 == 0) goto L_0x006f
            r0 = r11
            com.fossil.a87$d r0 = (com.fossil.a87.d) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x006f
            int r1 = r1 + r4
            r0.label = r1
            r4 = r0
        L_0x0015:
            java.lang.Object r2 = r4.result
            java.lang.Object r9 = com.fossil.yn7.d()
            int r0 = r4.label
            if (r0 == 0) goto L_0x007e
            if (r0 != r7) goto L_0x0076
            java.lang.Object r0 = r4.L$3
            java.io.File r0 = (java.io.File) r0
            java.lang.Object r0 = r4.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r4.L$1
            java.util.Iterator r0 = (java.util.Iterator) r0
            java.lang.Object r1 = r4.L$0
            com.fossil.a87 r1 = (com.fossil.a87) r1
            com.fossil.el7.b(r2)
            r5 = r0
            r10 = r1
        L_0x0036:
            r0 = r2
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x008a
            r0 = r7
            r8 = r5
        L_0x0041:
            boolean r1 = r8.hasNext()
            if (r1 == 0) goto L_0x0092
            java.lang.Object r1 = r8.next()
            java.lang.String r1 = (java.lang.String) r1
            com.portfolio.platform.data.source.FileRepository r2 = r10.A
            java.io.File r5 = r2.getFileByRemoteUrl(r1)
            if (r5 != 0) goto L_0x008e
            if (r0 == 0) goto L_0x0099
            com.portfolio.platform.data.source.FileRepository r0 = r10.A
            com.misfit.frameworks.buttonservice.model.FileType r2 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            r4.L$0 = r10
            r4.L$1 = r8
            r4.L$2 = r1
            r4.L$3 = r5
            r4.label = r7
            r5 = 4
            r6 = r3
            java.lang.Object r2 = com.portfolio.platform.data.source.FileRepository.downloadAndSaveWithRemoteUrl$default(r0, r1, r2, r3, r4, r5, r6)
            if (r2 != r9) goto L_0x0097
            r0 = r9
        L_0x006e:
            return r0
        L_0x006f:
            com.fossil.a87$d r0 = new com.fossil.a87$d
            r0.<init>(r10, r11)
            r4 = r0
            goto L_0x0015
        L_0x0076:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x007e:
            com.fossil.el7.b(r2)
            java.util.Set<java.lang.String> r0 = r10.s
            java.util.Iterator r1 = r0.iterator()
            r0 = r7
            r8 = r1
            goto L_0x0041
        L_0x008a:
            r1 = r5
        L_0x008b:
            r0 = 0
            r8 = r1
            goto L_0x0041
        L_0x008e:
            r8.remove()
            goto L_0x0041
        L_0x0092:
            java.lang.Boolean r0 = com.fossil.ao7.a(r0)
            goto L_0x006e
        L_0x0097:
            r5 = r8
            goto L_0x0036
        L_0x0099:
            r1 = r8
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a87.Q(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object R(s87 s87, qn7<? super tl7> qn7) {
        if (s87 instanceof s87.a) {
            s87.a aVar = (s87.a) s87;
            if (aVar.g() == null) {
                cb7 k2 = aVar.k();
                if ((k2 != null ? k2.c() : null) != null) {
                    if (!vt7.l(aVar.k().c())) {
                        FileRepository.asyncDownloadFromUrl$default(this.A, aVar.k().c(), FileType.WATCH_FACE, null, 4, null);
                        this.s.add(aVar.k().c());
                        this.t.put(ao7.e(s87.a()), aVar.k().c());
                    } else {
                        Bitmap c2 = a97.b.c(aVar.k().b());
                        if (c2 != null) {
                            this.t.put(ao7.e(s87.a()), c2);
                        }
                    }
                }
            }
        } else if (s87 instanceof s87.b) {
            s87.b bVar = (s87.b) s87;
            if (bVar.i() == null && (!bVar.m().isEmpty())) {
                if (bVar.h() == o87.BLACK) {
                    FileRepository.asyncDownloadFromUrl$default(this.A, bVar.m().get(0).c(), FileType.WATCH_FACE, null, 4, null);
                    this.s.add(bVar.m().get(0).c());
                    this.t.put(ao7.e(s87.a()), bVar.m().get(0).c());
                } else {
                    FileRepository.asyncDownloadFromUrl$default(this.A, bVar.m().get(1).c(), FileType.WATCH_FACE, null, 4, null);
                    this.s.add(bVar.m().get(1).c());
                    this.t.put(ao7.e(s87.a()), bVar.m().get(1).c());
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("WatchFaceEditViewModel", "needDownloadingUrl: " + this.s);
        return tl7.f3441a;
    }

    @DexIgnore
    public final void S() {
        T t2;
        for (T t3 : this.n.a()) {
            if (t3.getType() == lb7.SECOND_TIME) {
                Iterator<T> it = this.p.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next = it.next();
                    if (pq7.a(next.getAppId(), "second-timezone")) {
                        t2 = next;
                        break;
                    }
                }
                T t4 = t2;
                String setting = t4 != null ? t4.getSetting() : null;
                FLogger.INSTANCE.getLocal().d("WatchFaceEditViewModel", "checkingSecondTimeZoneComplication, setting = " + setting);
                if (!(setting == null || vt7.l(setting))) {
                    SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) new Gson().k(setting, SecondTimezoneSetting.class);
                    if (t3 != null) {
                        T t5 = t3;
                        t5.j(Integer.valueOf(ConversionUtils.INSTANCE.getTimezoneRawOffsetById(secondTimezoneSetting.getTimeZoneId())));
                        t5.i(secondTimezoneSetting.getCityCode());
                    } else {
                        throw new il7("null cannot be cast to non-null type com.portfolio.platform.watchface.model.theme.communication.WFComplicationData");
                    }
                } else {
                    continue;
                }
            }
        }
    }

    @DexIgnore
    public final void T() {
        xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new e(this, null), 2, null);
    }

    @DexIgnore
    public final LiveData<gl7<Boolean, Boolean, Integer>> U() {
        return this.i;
    }

    @DexIgnore
    public final void V(ab7 ab7, String str, za7 za7) {
        pq7.c(ab7, Constants.EVENT);
        pq7.c(str, "complicationId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceEditViewModel", "getComplicationData, complicationId = " + str);
        xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new f(this, str, za7, ab7, null), 2, null);
    }

    @DexIgnore
    public final LiveData<b> W() {
        return this.q;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object X(java.lang.String r9, com.fossil.qn7<? super java.lang.String> r10) {
        /*
            r8 = this;
            r7 = 2
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r10 instanceof com.fossil.a87.g
            if (r0 == 0) goto L_0x0037
            r0 = r10
            com.fossil.a87$g r0 = (com.fossil.a87.g) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0037
            int r1 = r1 + r3
            r0.label = r1
            r4 = r0
        L_0x0015:
            java.lang.Object r3 = r4.result
            java.lang.Object r6 = com.fossil.yn7.d()
            int r0 = r4.label
            if (r0 == 0) goto L_0x006d
            if (r0 == r5) goto L_0x0046
            if (r0 != r7) goto L_0x003e
            java.lang.Object r0 = r4.L$3
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            java.lang.Object r0 = r4.L$2
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r4.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r4.L$0
            com.fossil.a87 r0 = (com.fossil.a87) r0
            com.fossil.el7.b(r3)
        L_0x0036:
            return r3
        L_0x0037:
            com.fossil.a87$g r0 = new com.fossil.a87$g
            r0.<init>(r8, r10)
            r4 = r0
            goto L_0x0015
        L_0x003e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0046:
            java.lang.Object r0 = r4.L$2
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r1 = r4.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r4.L$0
            com.fossil.a87 r2 = (com.fossil.a87) r2
            com.fossil.el7.b(r3)
            r5 = r0
        L_0x0056:
            r0 = r3
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            com.fossil.zm5 r3 = r2.w
            r4.L$0 = r2
            r4.L$1 = r1
            r4.L$2 = r5
            r4.L$3 = r0
            r4.label = r7
            java.lang.Object r3 = r3.d(r0, r5, r1, r4)
            if (r3 != r6) goto L_0x0036
            r3 = r6
            goto L_0x0036
        L_0x006d:
            com.fossil.el7.b(r3)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "complicationId = "
            r1.append(r2)
            r1.append(r9)
            java.lang.String r2 = "WatchFaceEditViewModel"
            java.lang.String r1 = r1.toString()
            r0.d(r2, r1)
            com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository r0 = r8.y
            java.util.List r0 = r0.getAllRealDataRaw()
            com.portfolio.platform.data.source.UserRepository r1 = r8.x
            r4.L$0 = r8
            r4.L$1 = r9
            r4.L$2 = r0
            r4.label = r5
            java.lang.Object r3 = r1.getCurrentUser(r4)
            if (r3 != r6) goto L_0x00a4
            r3 = r6
            goto L_0x0036
        L_0x00a4:
            r2 = r8
            r1 = r9
            r5 = r0
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a87.X(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final LiveData<List<s87>> Y() {
        return this.r;
    }

    @DexIgnore
    public final List<jn5.a> Z(List<? extends s87> list) {
        pq7.c(list, "elementConfigs");
        ArrayList arrayList = new ArrayList();
        for (T t2 : list) {
            if (t2 instanceof s87.a) {
                arrayList.add(t2);
            }
        }
        ArrayList<s87.a> arrayList2 = new ArrayList();
        for (Object obj : arrayList) {
            if (ik5.d.c(((s87.a) obj).i())) {
                arrayList2.add(obj);
            }
        }
        ArrayList arrayList3 = new ArrayList();
        for (s87.a aVar : arrayList2) {
            jn5.a b2 = hl5.f1493a.b(aVar.i());
            if (b2 != null) {
                arrayList3.add(b2);
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceEditViewModel", "missing features " + arrayList3);
        return arrayList3;
    }

    @DexIgnore
    public final mo5 a0() {
        return this.l;
    }

    @DexIgnore
    public final LiveData<Object> b0() {
        return this.k;
    }

    @DexIgnore
    public final List<String> c0(List<? extends s87> list) {
        T t2;
        pq7.c(list, "elementConfigs");
        ArrayList arrayList = new ArrayList();
        for (T t3 : list) {
            if (t3 instanceof s87.a) {
                arrayList.add(t3);
            }
        }
        ArrayList<s87.a> arrayList2 = new ArrayList();
        for (Object obj : arrayList) {
            if (ik5.d.d(((s87.a) obj).i())) {
                arrayList2.add(obj);
            }
        }
        ArrayList arrayList3 = new ArrayList();
        for (s87.a aVar : arrayList2) {
            Iterator<T> it = this.p.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                T next = it.next();
                if (pq7.a(next.getAppId(), aVar.i())) {
                    t2 = next;
                    break;
                }
            }
            if (t2 == null) {
                String i2 = aVar.i();
                int hashCode = i2.hashCode();
                if (hashCode != -829740640) {
                    if (hashCode == 134170930 && i2.equals("second-timezone")) {
                        arrayList3.add("Please choose a city for second timezone complication");
                    }
                } else if (i2.equals("commute-time")) {
                    String c2 = um5.c(PortfolioApp.h0.c(), 2131886368);
                    pq7.b(c2, "LanguageHelper.getString\u2026ayCommuteTimePleaseEnter)");
                    arrayList3.add(c2);
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceEditViewModel", "missing setting " + arrayList3);
        return arrayList3;
    }

    @DexIgnore
    public final LiveData<ub7> d0() {
        return this.j;
    }

    @DexIgnore
    public final LiveData<Boolean> e0() {
        return this.h;
    }

    @DexIgnore
    public final void f0(String str, boolean z2, byte[] bArr, int i2) {
        pq7.c(str, "action");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceEditViewModel", "handleResultFromThemeAction action " + str + " isSucceed " + z2 + " errorCode " + i2);
        xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new h(this, str, z2, bArr, i2, null), 2, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object g0(byte[] r9, com.fossil.qn7<? super com.fossil.tl7> r10) {
        /*
            r8 = this;
            r3 = 0
            r7 = 1
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = 0
            boolean r0 = r10 instanceof com.fossil.a87.i
            if (r0 == 0) goto L_0x0030
            r0 = r10
            com.fossil.a87$i r0 = (com.fossil.a87.i) r0
            int r2 = r0.label
            r4 = r2 & r5
            if (r4 == 0) goto L_0x0030
            int r2 = r2 + r5
            r0.label = r2
            r2 = r0
        L_0x0016:
            java.lang.Object r4 = r2.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r5 = r2.label
            if (r5 == 0) goto L_0x003f
            if (r5 != r7) goto L_0x0037
            java.lang.Object r0 = r2.L$1
            byte[] r0 = (byte[]) r0
            java.lang.Object r0 = r2.L$0
            com.fossil.a87 r0 = (com.fossil.a87) r0
            com.fossil.el7.b(r4)
        L_0x002d:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x002f:
            return r0
        L_0x0030:
            com.fossil.a87$i r0 = new com.fossil.a87$i
            r0.<init>(r8, r10)
            r2 = r0
            goto L_0x0016
        L_0x0037:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003f:
            com.fossil.el7.b(r4)
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "handleResultFromThemeAction data "
            r5.append(r6)
            r5.append(r9)
            java.lang.String r6 = "WatchFaceEditViewModel"
            java.lang.String r5 = r5.toString()
            r4.d(r6, r5)
            if (r9 != 0) goto L_0x0080
            r4 = 6
            r0 = r8
            r2 = r1
            r5 = r3
            com.fossil.hq4.d(r0, r1, r2, r3, r4, r5)
            androidx.lifecycle.MutableLiveData<com.fossil.gl7<java.lang.Boolean, java.lang.Boolean, java.lang.Integer>> r0 = r8.i
            com.fossil.gl7 r2 = new com.fossil.gl7
            java.lang.Boolean r3 = com.fossil.ao7.a(r1)
            java.lang.Boolean r1 = com.fossil.ao7.a(r1)
            r4 = 600(0x258, float:8.41E-43)
            java.lang.Integer r4 = com.fossil.ao7.e(r4)
            r2.<init>(r3, r1, r4)
            r0.l(r2)
            goto L_0x002d
        L_0x0080:
            r2.L$0 = r8
            r2.L$1 = r9
            r2.label = r7
            java.lang.Object r1 = r8.o0(r9, r2)
            if (r1 != r0) goto L_0x002d
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a87.g0(byte[], com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object h0(com.fossil.qn7<? super java.lang.Boolean> r7) {
        /*
            r6 = this;
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 1
            boolean r0 = r7 instanceof com.fossil.a87.j
            if (r0 == 0) goto L_0x0058
            r0 = r7
            com.fossil.a87$j r0 = (com.fossil.a87.j) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0058
            int r1 = r1 + r4
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0066
            if (r4 != r2) goto L_0x005e
            java.lang.Object r0 = r0.L$0
            com.fossil.a87 r0 = (com.fossil.a87) r0
            com.fossil.el7.b(r1)
            r6 = r0
        L_0x0027:
            r0 = r1
            java.lang.Number r0 = (java.lang.Number) r0
            int r0 = r0.intValue()
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "isReachedMaxPreset total "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r4 = "WatchFaceEditViewModel"
            java.lang.String r3 = r3.toString()
            r1.d(r4, r3)
            r1 = 20
            if (r0 < r1) goto L_0x0052
            com.fossil.mo5 r0 = r6.l
            if (r0 == 0) goto L_0x007f
        L_0x0052:
            r0 = 0
        L_0x0053:
            java.lang.Boolean r0 = com.fossil.ao7.a(r0)
        L_0x0057:
            return r0
        L_0x0058:
            com.fossil.a87$j r0 = new com.fossil.a87$j
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x005e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0066:
            com.fossil.el7.b(r1)
            com.fossil.dv7 r1 = com.fossil.bw7.b()
            com.fossil.a87$k r4 = new com.fossil.a87$k
            r5 = 0
            r4.<init>(r6, r5)
            r0.L$0 = r6
            r0.label = r2
            java.lang.Object r1 = com.fossil.eu7.g(r1, r4, r0)
            if (r1 != r3) goto L_0x0027
            r0 = r3
            goto L_0x0057
        L_0x007f:
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            r1 = 2131886573(0x7f1201ed, float:1.9407729E38)
            java.lang.String r0 = com.fossil.um5.c(r0, r1)
            java.lang.String r1 = "LanguageHelper.getString\u2026t__MaximumPresetsCreated)"
            com.fossil.pq7.b(r0, r1)
            r1 = -1
            r6.a(r1, r0)
            r0 = r2
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a87.h0(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object i0(String str, qn7<? super tl7> qn7) {
        Object g2 = eu7.g(bw7.b(), new l(this, str, null), qn7);
        return g2 == yn7.d() ? g2 : tl7.f3441a;
    }

    @DexIgnore
    public final void j0(fb7 fb7) {
        xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new m(this, fb7, null), 2, null);
    }

    @DexIgnore
    public final void k0(s87 s87, w67.b bVar) {
        pq7.c(s87, "elementConfig");
        pq7.c(bVar, "type");
        xw7 unused = gu7.d(us0.a(this), null, null, new n(this, bVar, s87, null), 3, null);
    }

    @DexIgnore
    public final void l0() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceEditViewModel", "previewTheme - data: " + this.n);
        xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new o(this, null), 2, null);
    }

    @DexIgnore
    public final void m0() {
        int i2;
        byte[] bArr;
        byte[] bArr2;
        for (Map.Entry<Integer, Object> entry : this.t.entrySet()) {
            int intValue = entry.getKey().intValue();
            Object value = entry.getValue();
            Iterator<gb7> it = this.n.a().iterator();
            int i3 = 0;
            while (true) {
                if (!it.hasNext()) {
                    i2 = -1;
                    break;
                }
                if (it.next().a() == intValue) {
                    i2 = i3;
                    break;
                }
                i3++;
            }
            if (i2 != -1) {
                gb7 gb7 = this.n.a().get(i2);
                this.n.a().remove(i2);
                if (gb7 instanceof pb7) {
                    if (value instanceof String) {
                        pb7 pb7 = (pb7) gb7;
                        File fileByRemoteUrl = this.A.getFileByRemoteUrl((String) value);
                        if (fileByRemoteUrl == null || (bArr2 = j37.b(fileByRemoteUrl)) == null) {
                            bArr2 = new byte[0];
                        }
                        pb7.f(bArr2);
                    }
                } else if (gb7 instanceof ib7) {
                    if (value instanceof String) {
                        ib7 ib7 = (ib7) gb7;
                        File fileByRemoteUrl2 = this.A.getFileByRemoteUrl((String) value);
                        if (fileByRemoteUrl2 == null || (bArr = j37.b(fileByRemoteUrl2)) == null) {
                            bArr = new byte[0];
                        }
                        ib7.k(bArr);
                    } else if (value instanceof Bitmap) {
                        ((ib7) gb7).k(cy1.b((Bitmap) value, null, 1, null));
                    }
                }
                this.n.a().add(i2, gb7);
            }
        }
        this.t.clear();
    }

    @DexIgnore
    public final void n0(int i2, List<gb7> list) {
        T t2;
        boolean z2;
        this.t.remove(Integer.valueOf(i2));
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (next.a() == i2) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                t2 = next;
                break;
            }
        }
        T t3 = t2;
        if (t3 != null) {
            list.remove(t3);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0500  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0505  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00d1  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x01db  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0201  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x022c  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0261  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x027f  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x02c0  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0325  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0379  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x037d  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x03a4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x03c2  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0485  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x048a  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0499  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object o0(byte[] r25, com.fossil.qn7<? super com.fossil.tl7> r26) {
        /*
        // Method dump skipped, instructions count: 1322
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a87.o0(byte[], com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.ts0
    public void onCleared() {
        i77.c.f();
        ct0.b(PortfolioApp.h0.c()).e(this.v);
        super.onCleared();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0135  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.gb7 p0(com.fossil.s87 r12) {
        /*
        // Method dump skipped, instructions count: 322
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a87.p0(com.fossil.s87):com.fossil.gb7");
    }

    @DexIgnore
    public final void q0(List<DianaAppSetting> list) {
        pq7.c(list, "updatedSettings");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceEditViewModel", "updateComplicationSettings " + list);
        this.p.clear();
        this.p.addAll(list);
    }
}
