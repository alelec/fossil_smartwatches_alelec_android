package com.fossil;

import com.fossil.ju2;
import com.fossil.lu2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class sr3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f3289a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[ju2.b.values().length];
        b = iArr;
        try {
            iArr[ju2.b.LESS_THAN.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            b[ju2.b.GREATER_THAN.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            b[ju2.b.EQUAL.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            b[ju2.b.BETWEEN.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        int[] iArr2 = new int[lu2.a.values().length];
        f3289a = iArr2;
        try {
            iArr2[lu2.a.REGEXP.ordinal()] = 1;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f3289a[lu2.a.BEGINS_WITH.ordinal()] = 2;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f3289a[lu2.a.ENDS_WITH.ordinal()] = 3;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f3289a[lu2.a.PARTIAL.ordinal()] = 4;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f3289a[lu2.a.EXACT.ordinal()] = 5;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f3289a[lu2.a.IN_LIST.ordinal()] = 6;
        } catch (NoSuchFieldError e10) {
        }
    }
    */
}
