package com.fossil;

import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ph2 implements k42 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set<Scope> f2831a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Set<Scope> f2832a;

        @DexIgnore
        public a() {
            this.f2832a = new HashSet();
        }

        @DexIgnore
        public final a a(DataType dataType, int i) {
            rc2.b(i == 0 || i == 1, "valid access types are FitnessOptions.ACCESS_READ or FitnessOptions.ACCESS_WRITE");
            if (i == 0 && dataType.k() != null) {
                this.f2832a.add(new Scope(dataType.k()));
            } else if (i == 1 && dataType.A() != null) {
                this.f2832a.add(new Scope(dataType.A()));
            }
            return this;
        }

        @DexIgnore
        public final ph2 b() {
            return new ph2(this);
        }
    }

    @DexIgnore
    public ph2(a aVar) {
        this.f2831a = lj2.a(aVar.f2832a);
    }

    @DexIgnore
    public static a b() {
        return new a();
    }

    @DexIgnore
    @Override // com.fossil.k42
    public final List<Scope> a() {
        return new ArrayList(this.f2831a);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ph2)) {
            return false;
        }
        return this.f2831a.equals(((ph2) obj).f2831a);
    }

    @DexIgnore
    public final int hashCode() {
        return pc2.b(this.f2831a);
    }
}
