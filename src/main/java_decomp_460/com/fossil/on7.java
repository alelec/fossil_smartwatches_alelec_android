package com.fossil;

import com.fossil.tn7;
import com.fossil.tn7.b;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class on7<B extends tn7.b, E extends B> implements tn7.c<E> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ tn7.c<?> f2701a;
    @DexIgnore
    public /* final */ rp7<tn7.b, E> b;

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX DEBUG: Type inference failed for r2v1. Raw type applied. Possible types: com.fossil.tn7$c<?> */
    /* JADX WARN: Type inference failed for: r3v0, types: [com.fossil.rp7<? super com.fossil.tn7$b, ? extends E extends B>, com.fossil.rp7<com.fossil.tn7$b, E extends B>, java.lang.Object] */
    public on7(tn7.c<B> cVar, rp7<? super tn7.b, ? extends E> rp7) {
        pq7.c(cVar, "baseKey");
        pq7.c(rp7, "safeCast");
        this.b = rp7;
        this.f2701a = cVar instanceof on7 ? (tn7.c<B>) ((on7) cVar).f2701a : cVar;
    }

    @DexIgnore
    public final boolean a(tn7.c<?> cVar) {
        pq7.c(cVar, "key");
        return cVar == this || this.f2701a == cVar;
    }

    @DexIgnore
    public final E b(tn7.b bVar) {
        pq7.c(bVar, "element");
        return this.b.invoke(bVar);
    }
}
