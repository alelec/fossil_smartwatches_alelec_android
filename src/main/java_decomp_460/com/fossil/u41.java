package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u41<TResult> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ t41<TResult> f3513a; // = new t41<>();

    @DexIgnore
    public t41<TResult> a() {
        return this.f3513a;
    }

    @DexIgnore
    public void b() {
        if (!e()) {
            throw new IllegalStateException("Cannot cancel a completed task.");
        }
    }

    @DexIgnore
    public void c(Exception exc) {
        if (!f(exc)) {
            throw new IllegalStateException("Cannot set the error on a completed task.");
        }
    }

    @DexIgnore
    public void d(TResult tresult) {
        if (!g(tresult)) {
            throw new IllegalStateException("Cannot set the result of a completed task.");
        }
    }

    @DexIgnore
    public boolean e() {
        return this.f3513a.v();
    }

    @DexIgnore
    public boolean f(Exception exc) {
        return this.f3513a.w(exc);
    }

    @DexIgnore
    public boolean g(TResult tresult) {
        return this.f3513a.x(tresult);
    }
}
