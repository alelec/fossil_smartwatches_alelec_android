package com.fossil;

import com.fossil.lz7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class py7<E> extends xy7 implements wy7<E> {
    @DexIgnore
    public /* final */ Throwable e;

    @DexIgnore
    public py7(Throwable th) {
        this.e = th;
    }

    @DexIgnore
    @Override // com.fossil.wy7
    public /* bridge */ /* synthetic */ Object a() {
        w();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.wy7
    public void d(E e2) {
    }

    @DexIgnore
    @Override // com.fossil.wy7
    public vz7 e(E e2, lz7.c cVar) {
        vz7 vz7 = mu7.f2424a;
        if (cVar == null) {
            return vz7;
        }
        cVar.d();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.lz7
    public String toString() {
        return "Closed@" + ov7.b(this) + '[' + this.e + ']';
    }

    @DexIgnore
    public py7<E> w() {
        return this;
    }

    @DexIgnore
    public final Throwable x() {
        Throwable th = this.e;
        return th != null ? th : new qy7("Channel was closed");
    }
}
