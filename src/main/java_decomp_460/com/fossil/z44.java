package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z44<E> extends y24<E> {
    @DexIgnore
    public /* final */ transient E element;

    @DexIgnore
    public z44(E e) {
        i14.l(e);
        this.element = e;
    }

    @DexIgnore
    @Override // java.util.List
    public E get(int i) {
        i14.j(i, 1);
        return this.element;
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, com.fossil.u24, com.fossil.u24, com.fossil.y24, com.fossil.y24, java.lang.Iterable
    public h54<E> iterator() {
        return p34.u(this.element);
    }

    @DexIgnore
    public int size() {
        return 1;
    }

    @DexIgnore
    @Override // java.util.List, com.fossil.y24, com.fossil.y24
    public y24<E> subList(int i, int i2) {
        i14.r(i, i2, 1);
        return i == i2 ? y24.of() : this;
    }

    @DexIgnore
    public String toString() {
        return '[' + this.element.toString() + ']';
    }
}
