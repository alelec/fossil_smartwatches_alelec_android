package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jd6 implements Factory<lj6> {
    @DexIgnore
    public static lj6 a(dd6 dd6) {
        lj6 f = dd6.f();
        lk7.c(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }
}
