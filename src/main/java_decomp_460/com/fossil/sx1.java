package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sx1 extends IllegalArgumentException {
    @DexIgnore
    public /* final */ a errorCode;

    @DexIgnore
    public enum a {
        UNSUPPORTED_VERSION(1),
        INVALID_FILE_DATA(2);
        
        @DexIgnore
        public /* final */ int id;

        @DexIgnore
        public a(int i) {
            this.id = i;
        }

        @DexIgnore
        public final int getId() {
            return this.id;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sx1(a aVar, String str, Throwable th) {
        super(str, th);
        pq7.c(aVar, "errorCode");
        this.errorCode = aVar;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ sx1(a aVar, String str, Throwable th, int i, kq7 kq7) {
        this(aVar, (i & 2) != 0 ? null : str, (i & 4) != 0 ? null : th);
    }

    @DexIgnore
    public final a getErrorCode() {
        return this.errorCode;
    }
}
