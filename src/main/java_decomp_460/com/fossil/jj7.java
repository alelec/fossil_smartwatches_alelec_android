package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jj7 {
    @DexIgnore
    public static /* final */ int action0; // = 2131361856;
    @DexIgnore
    public static /* final */ int action_bar; // = 2131361857;
    @DexIgnore
    public static /* final */ int action_bar_activity_content; // = 2131361858;
    @DexIgnore
    public static /* final */ int action_bar_container; // = 2131361859;
    @DexIgnore
    public static /* final */ int action_bar_root; // = 2131361860;
    @DexIgnore
    public static /* final */ int action_bar_spinner; // = 2131361861;
    @DexIgnore
    public static /* final */ int action_bar_subtitle; // = 2131361862;
    @DexIgnore
    public static /* final */ int action_bar_title; // = 2131361863;
    @DexIgnore
    public static /* final */ int action_context_bar; // = 2131361865;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361866;
    @DexIgnore
    public static /* final */ int action_menu_divider; // = 2131361868;
    @DexIgnore
    public static /* final */ int action_menu_presenter; // = 2131361869;
    @DexIgnore
    public static /* final */ int action_mode_bar; // = 2131361870;
    @DexIgnore
    public static /* final */ int action_mode_bar_stub; // = 2131361871;
    @DexIgnore
    public static /* final */ int action_mode_close_button; // = 2131361872;
    @DexIgnore
    public static /* final */ int activity_chooser_view_content; // = 2131361875;
    @DexIgnore
    public static /* final */ int alertTitle; // = 2131361885;
    @DexIgnore
    public static /* final */ int always; // = 2131361888;
    @DexIgnore
    public static /* final */ int beginning; // = 2131361912;
    @DexIgnore
    public static /* final */ int belvedere_dialog_listview; // = 2131361915;
    @DexIgnore
    public static /* final */ int belvedere_dialog_row_image; // = 2131361916;
    @DexIgnore
    public static /* final */ int belvedere_dialog_row_text; // = 2131361917;
    @DexIgnore
    public static /* final */ int buttonPanel; // = 2131361985;
    @DexIgnore
    public static /* final */ int cancel_action; // = 2131361989;
    @DexIgnore
    public static /* final */ int checkbox; // = 2131362022;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131362026;
    @DexIgnore
    public static /* final */ int collapseActionView; // = 2131362136;
    @DexIgnore
    public static /* final */ int contentPanel; // = 2131362159;
    @DexIgnore
    public static /* final */ int custom; // = 2131362170;
    @DexIgnore
    public static /* final */ int customPanel; // = 2131362171;
    @DexIgnore
    public static /* final */ int decor_content_parent; // = 2131362198;
    @DexIgnore
    public static /* final */ int default_activity_button; // = 2131362199;
    @DexIgnore
    public static /* final */ int disableHome; // = 2131362208;
    @DexIgnore
    public static /* final */ int edit_query; // = 2131362217;
    @DexIgnore
    public static /* final */ int end; // = 2131362225;
    @DexIgnore
    public static /* final */ int end_padder; // = 2131362226;
    @DexIgnore
    public static /* final */ int expand_activities_button; // = 2131362249;
    @DexIgnore
    public static /* final */ int expanded_menu; // = 2131362250;
    @DexIgnore
    public static /* final */ int home; // = 2131362584;
    @DexIgnore
    public static /* final */ int homeAsUp; // = 2131362585;
    @DexIgnore
    public static /* final */ int icon; // = 2131362608;
    @DexIgnore
    public static /* final */ int ifRoom; // = 2131362615;
    @DexIgnore
    public static /* final */ int image; // = 2131362616;
    @DexIgnore
    public static /* final */ int info; // = 2131362633;
    @DexIgnore
    public static /* final */ int line1; // = 2131362784;
    @DexIgnore
    public static /* final */ int line3; // = 2131362786;
    @DexIgnore
    public static /* final */ int listMode; // = 2131362793;
    @DexIgnore
    public static /* final */ int list_item; // = 2131362794;
    @DexIgnore
    public static /* final */ int media_actions; // = 2131362843;
    @DexIgnore
    public static /* final */ int middle; // = 2131362846;
    @DexIgnore
    public static /* final */ int multiply; // = 2131362876;
    @DexIgnore
    public static /* final */ int never; // = 2131362878;
    @DexIgnore
    public static /* final */ int none; // = 2131362882;
    @DexIgnore
    public static /* final */ int normal; // = 2131362883;
    @DexIgnore
    public static /* final */ int parentPanel; // = 2131362919;
    @DexIgnore
    public static /* final */ int progress_circular; // = 2131362971;
    @DexIgnore
    public static /* final */ int progress_horizontal; // = 2131362972;
    @DexIgnore
    public static /* final */ int radio; // = 2131362975;
    @DexIgnore
    public static /* final */ int screen; // = 2131363080;
    @DexIgnore
    public static /* final */ int scrollIndicatorDown; // = 2131363082;
    @DexIgnore
    public static /* final */ int scrollIndicatorUp; // = 2131363083;
    @DexIgnore
    public static /* final */ int scrollView; // = 2131363084;
    @DexIgnore
    public static /* final */ int search_badge; // = 2131363088;
    @DexIgnore
    public static /* final */ int search_bar; // = 2131363089;
    @DexIgnore
    public static /* final */ int search_button; // = 2131363090;
    @DexIgnore
    public static /* final */ int search_close_btn; // = 2131363091;
    @DexIgnore
    public static /* final */ int search_edit_frame; // = 2131363092;
    @DexIgnore
    public static /* final */ int search_go_btn; // = 2131363093;
    @DexIgnore
    public static /* final */ int search_mag_icon; // = 2131363094;
    @DexIgnore
    public static /* final */ int search_plate; // = 2131363095;
    @DexIgnore
    public static /* final */ int search_src_text; // = 2131363096;
    @DexIgnore
    public static /* final */ int search_voice_btn; // = 2131363098;
    @DexIgnore
    public static /* final */ int select_dialog_listview; // = 2131363102;
    @DexIgnore
    public static /* final */ int shortcut; // = 2131363117;
    @DexIgnore
    public static /* final */ int showCustom; // = 2131363118;
    @DexIgnore
    public static /* final */ int showHome; // = 2131363119;
    @DexIgnore
    public static /* final */ int showTitle; // = 2131363120;
    @DexIgnore
    public static /* final */ int spacer; // = 2131363134;
    @DexIgnore
    public static /* final */ int split_action_bar; // = 2131363136;
    @DexIgnore
    public static /* final */ int src_atop; // = 2131363140;
    @DexIgnore
    public static /* final */ int src_in; // = 2131363141;
    @DexIgnore
    public static /* final */ int src_over; // = 2131363142;
    @DexIgnore
    public static /* final */ int status_bar_latest_event_content; // = 2131363147;
    @DexIgnore
    public static /* final */ int submit_area; // = 2131363154;
    @DexIgnore
    public static /* final */ int tabMode; // = 2131363174;
    @DexIgnore
    public static /* final */ int text; // = 2131363190;
    @DexIgnore
    public static /* final */ int text2; // = 2131363191;
    @DexIgnore
    public static /* final */ int textSpacerNoButtons; // = 2131363194;
    @DexIgnore
    public static /* final */ int time; // = 2131363213;
    @DexIgnore
    public static /* final */ int title; // = 2131363215;
    @DexIgnore
    public static /* final */ int title_template; // = 2131363218;
    @DexIgnore
    public static /* final */ int topPanel; // = 2131363226;
    @DexIgnore
    public static /* final */ int up; // = 2131363437;
    @DexIgnore
    public static /* final */ int useLogo; // = 2131363444;
    @DexIgnore
    public static /* final */ int withText; // = 2131363558;
    @DexIgnore
    public static /* final */ int wrap_content; // = 2131363560;
}
