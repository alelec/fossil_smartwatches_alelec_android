package com.fossil;

import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lt4 {
    @rj4("id")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2242a;
    @DexIgnore
    @rj4("socialId")
    public String b;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_FIRST_NAME)
    public String c;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_LAST_NAME)
    public String d;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_PROFILE_PIC)
    public String e;

    @DexIgnore
    public final String a() {
        return this.e;
    }

    @DexIgnore
    public final String b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.f2242a;
    }

    @DexIgnore
    public final String d() {
        return this.d;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof lt4) {
                lt4 lt4 = (lt4) obj;
                if (!pq7.a(this.f2242a, lt4.f2242a) || !pq7.a(this.b, lt4.b) || !pq7.a(this.c, lt4.c) || !pq7.a(this.d, lt4.d) || !pq7.a(this.e, lt4.e)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f2242a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.d;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.e;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "ProfileData(id=" + this.f2242a + ", socialId=" + this.b + ", firstName=" + this.c + ", lastName=" + this.d + ", avatar=" + this.e + ")";
    }
}
