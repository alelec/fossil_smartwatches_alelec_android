package com.fossil;

import android.content.Context;
import android.content.Intent;
import com.facebook.share.internal.VideoUploader;
import com.fossil.iq4;
import com.fossil.jn5;
import com.fossil.tq4;
import com.fossil.u06;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r06 extends m06 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public boolean e; // = this.p.W();
    @DexIgnore
    public List<InstalledApp> f; // = new ArrayList();
    @DexIgnore
    public List<i06> g; // = new ArrayList();
    @DexIgnore
    public List<AppNotificationFilter> h; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> i; // = new ArrayList();
    @DexIgnore
    public /* final */ b j; // = new b();
    @DexIgnore
    public /* final */ n06 k;
    @DexIgnore
    public /* final */ uq4 l;
    @DexIgnore
    public /* final */ v36 m;
    @DexIgnore
    public /* final */ d26 n;
    @DexIgnore
    public /* final */ u06 o;
    @DexIgnore
    public /* final */ on5 p;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return r06.r;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements wq5.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements tq4.d<u06.b, tq4.a> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ b f3063a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public a(b bVar) {
                this.f3063a = bVar;
            }

            @DexIgnore
            /* renamed from: b */
            public void a(tq4.a aVar) {
                FLogger.INSTANCE.getLocal().d(r06.s.a(), ".Inside mSaveAppsNotification onError");
                r06.this.k.a();
                r06.this.k.close();
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(u06.b bVar) {
                FLogger.INSTANCE.getLocal().d(r06.s.a(), ".Inside mSaveAppsNotification onSuccess");
                r06.this.k.a();
                r06.this.k.close();
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            FLogger.INSTANCE.getLocal().d(r06.s.a(), "SetNotificationFilterReceiver");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = r06.s.a();
            local.d(a2, "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
            if (communicateMode != CommunicateMode.SET_NOTIFICATION_FILTERS) {
                return;
            }
            if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                FLogger.INSTANCE.getLocal().d(r06.s.a(), "onReceive - success");
                r06.this.p.J0(r06.this.H());
                r06.this.l.a(r06.this.o, new u06.a(r06.this.I()), new a(this));
                return;
            }
            FLogger.INSTANCE.getLocal().d(r06.s.a(), "onReceive - failed");
            int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
            ArrayList<Integer> arrayList = integerArrayListExtra != null ? integerArrayListExtra : new ArrayList<>(intExtra2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = r06.s.a();
            local2.d(a3, "permissionErrorCodes=" + arrayList + " , size=" + arrayList.size());
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = r06.s.a();
                local3.d(a4, "error code " + i + " =" + arrayList.get(i));
            }
            if (intExtra2 != 1101) {
                if (intExtra2 == 1924) {
                    r06.this.k.u();
                    return;
                } else if (intExtra2 == 8888) {
                    r06.this.k.c();
                    return;
                } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                    return;
                }
            }
            List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(arrayList);
            pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
            n06 n06 = r06.this.k;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
            if (array != null) {
                uh5[] uh5Arr = (uh5[]) array;
                n06.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1", f = "NotificationAppsPresenter.kt", l = {130, 135, 184, 192, 201, 210}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ r06 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$1", f = "NotificationAppsPresenter.kt", l = {131}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    PortfolioApp c = PortfolioApp.h0.c();
                    this.L$0 = iv7;
                    this.label = 1;
                    if (c.V0(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$6", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $notificationSettings;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ b b;

                @DexIgnore
                public a(b bVar) {
                    this.b = bVar;
                }

                @DexIgnore
                public final void run() {
                    this.b.this$0.this$0.q.getNotificationSettingsDao().insertListNotificationSettings(this.b.$notificationSettings);
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(c cVar, List list, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$notificationSettings = list;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, this.$notificationSettings, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.q.runInTransaction(new a(this));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.r06$c$c")
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$7", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.r06$c$c  reason: collision with other inner class name */
        public static final class C0204c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $listNotificationSettings;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0204c(c cVar, List list, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$listNotificationSettings = list;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0204c cVar = new C0204c(this.this$0, this.$listNotificationSettings, qn7);
                cVar.p$ = (iv7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((C0204c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    for (NotificationSettingsModel notificationSettingsModel : this.$listNotificationSettings) {
                        int component2 = notificationSettingsModel.component2();
                        if (notificationSettingsModel.component3()) {
                            String J = this.this$0.this$0.J(component2);
                            FLogger.INSTANCE.getLocal().d(r06.s.a(), "CALL settingsTypeName=" + J);
                            if (component2 == 0) {
                                FLogger.INSTANCE.getLocal().d(r06.s.a(), "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL());
                                DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                this.this$0.this$0.h.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
                            } else if (component2 == 1) {
                                int size = this.this$0.this$0.i.size();
                                for (int i = 0; i < size; i++) {
                                    ContactGroup contactGroup = (ContactGroup) this.this$0.this$0.i.get(i);
                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                    String a2 = r06.s.a();
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("mListAppNotificationFilter add PHONE item - ");
                                    sb.append(i);
                                    sb.append(" name = ");
                                    Contact contact = contactGroup.getContacts().get(0);
                                    pq7.b(contact, "item.contacts[0]");
                                    sb.append(contact.getDisplayName());
                                    local.d(a2, sb.toString());
                                    DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType()));
                                    Contact contact2 = contactGroup.getContacts().get(0);
                                    pq7.b(contact2, "item.contacts[0]");
                                    appNotificationFilter.setSender(contact2.getDisplayName());
                                    this.this$0.this$0.h.add(appNotificationFilter);
                                }
                            }
                        } else {
                            String J2 = this.this$0.this$0.J(component2);
                            FLogger.INSTANCE.getLocal().d(r06.s.a(), "MESSAGE settingsTypeName=" + J2);
                            if (component2 == 0) {
                                FLogger.INSTANCE.getLocal().d(r06.s.a(), "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getMESSAGES());
                                DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                this.this$0.this$0.h.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
                            } else if (component2 == 1) {
                                int size2 = this.this$0.this$0.i.size();
                                for (int i2 = 0; i2 < size2; i2++) {
                                    ContactGroup contactGroup2 = (ContactGroup) this.this$0.this$0.i.get(i2);
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String a3 = r06.s.a();
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append("mListAppNotificationFilter add MESSAGE item - ");
                                    sb2.append(i2);
                                    sb2.append(" name = ");
                                    Contact contact3 = contactGroup2.getContacts().get(0);
                                    pq7.b(contact3, "item.contacts[0]");
                                    sb2.append(contact3.getDisplayName());
                                    local2.d(a3, sb2.toString());
                                    DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType()));
                                    Contact contact4 = contactGroup2.getContacts().get(0);
                                    pq7.b(contact4, "item.contacts[0]");
                                    appNotificationFilter2.setSender(contact4.getDisplayName());
                                    this.this$0.this$0.h.add(appNotificationFilter2);
                                }
                            }
                        }
                    }
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$getAllContactGroupsResponse$1", f = "NotificationAppsPresenter.kt", l = {186}, m = "invokeSuspend")
        public static final class d extends ko7 implements vp7<iv7, qn7<? super iq4.c>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                d dVar = new d(this.this$0, qn7);
                dVar.p$ = (iv7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super iq4.c> qn7) {
                return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    d26 d26 = this.this$0.this$0.n;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object a2 = jq4.a(d26, null, this);
                    return a2 == d ? d : a2;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$getAppResponse$1", f = "NotificationAppsPresenter.kt", l = {136}, m = "invokeSuspend")
        public static final class e extends ko7 implements vp7<iv7, qn7<? super iq4.c>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public e(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                e eVar = new e(this.this$0, qn7);
                eVar.p$ = (iv7) obj;
                return eVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super iq4.c> qn7) {
                return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    v36 v36 = this.this$0.this$0.m;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object a2 = jq4.a(v36, null, this);
                    return a2 == d ? d : a2;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$listNotificationSettings$1", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class f extends ko7 implements vp7<iv7, qn7<? super List<? extends NotificationSettingsModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public f(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                f fVar = new f(this.this$0, qn7);
                fVar.p$ = (iv7) obj;
                return fVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends NotificationSettingsModel>> qn7) {
                return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.q.getNotificationSettingsDao().getListNotificationSettingsNoLiveData();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(r06 r06, qn7 qn7) {
            super(2, qn7);
            this.this$0 = r06;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:104:0x030d, code lost:
            if (r0 == false) goto L_0x0331;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0054  */
        /* JADX WARNING: Removed duplicated region for block: B:113:0x0357  */
        /* JADX WARNING: Removed duplicated region for block: B:126:0x038e  */
        /* JADX WARNING: Removed duplicated region for block: B:129:0x03ad  */
        /* JADX WARNING: Removed duplicated region for block: B:132:0x03f8  */
        /* JADX WARNING: Removed duplicated region for block: B:136:0x0413  */
        /* JADX WARNING: Removed duplicated region for block: B:137:0x0416  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00a4  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x0111  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x01b5  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 1068
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.r06.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = r06.class.getSimpleName();
        pq7.b(simpleName, "NotificationAppsPresenter::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public r06(n06 n06, uq4 uq4, v36 v36, d26 d26, u06 u06, on5 on5, NotificationSettingsDatabase notificationSettingsDatabase) {
        pq7.c(n06, "mView");
        pq7.c(uq4, "mUseCaseHandler");
        pq7.c(v36, "mGetApps");
        pq7.c(d26, "mGetAllContactGroup");
        pq7.c(u06, "mSaveAppsNotification");
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.k = n06;
        this.l = uq4;
        this.m = v36;
        this.n = d26;
        this.o = u06;
        this.p = on5;
        this.q = notificationSettingsDatabase;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "init mIsAllAppToggleEnable " + this.e);
    }

    @DexIgnore
    public final boolean H() {
        return this.e;
    }

    @DexIgnore
    public final List<i06> I() {
        return this.g;
    }

    @DexIgnore
    public final String J(int i2) {
        if (i2 == 0) {
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886089);
            pq7.b(c2, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return c2;
        } else if (i2 != 1) {
            String c3 = um5.c(PortfolioApp.h0.c(), 2131886091);
            pq7.b(c3, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return c3;
        } else {
            String c4 = um5.c(PortfolioApp.h0.c(), 2131886090);
            pq7.b(c4, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return c4;
        }
    }

    @DexIgnore
    public final boolean K(boolean z) {
        int i2;
        int i3;
        List<i06> list = this.g;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            i2 = 0;
            for (T t : list) {
                InstalledApp installedApp = t.getInstalledApp();
                Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                if (isSelected == null) {
                    pq7.i();
                    throw null;
                } else if (isSelected.booleanValue() == z && t.getUri() != null) {
                    int i4 = i2 + 1;
                    if (i4 >= 0) {
                        i2 = i4;
                    } else {
                        hm7.k();
                        throw null;
                    }
                }
            }
        } else {
            i2 = 0;
        }
        List<i06> list2 = this.g;
        if (!(list2 instanceof Collection) || !list2.isEmpty()) {
            Iterator<T> it = list2.iterator();
            i3 = 0;
            while (it.hasNext()) {
                if (it.next().getUri() != null) {
                    int i5 = i3 + 1;
                    if (i5 >= 0) {
                        i3 = i5;
                    } else {
                        hm7.k();
                        throw null;
                    }
                }
            }
        } else {
            i3 = 0;
        }
        return i2 == i3;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0049  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean L() {
        /*
            r7 = this;
            r3 = 1
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.List<com.fossil.i06> r0 = r7.g
            java.util.Iterator r2 = r0.iterator()
        L_0x000c:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0031
            java.lang.Object r0 = r2.next()
            com.fossil.i06 r0 = (com.fossil.i06) r0
            com.portfolio.platform.data.model.InstalledApp r0 = r0.getInstalledApp()
            if (r0 == 0) goto L_0x000c
            java.lang.Boolean r4 = r0.isSelected()
            java.lang.String r5 = "it.isSelected"
            com.fossil.pq7.b(r4, r5)
            boolean r4 = r4.booleanValue()
            if (r4 == 0) goto L_0x000c
            r1.add(r0)
            goto L_0x000c
        L_0x0031:
            int r0 = r1.size()
            java.util.List<com.portfolio.platform.data.model.InstalledApp> r2 = r7.f
            int r2 = r2.size()
            if (r0 == r2) goto L_0x003f
            r0 = r3
        L_0x003e:
            return r0
        L_0x003f:
            java.util.Iterator r4 = r1.iterator()
        L_0x0043:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x008a
            java.lang.Object r0 = r4.next()
            r2 = r0
            com.portfolio.platform.data.model.InstalledApp r2 = (com.portfolio.platform.data.model.InstalledApp) r2
            java.util.List<com.portfolio.platform.data.model.InstalledApp> r0 = r7.f
            java.util.Iterator r5 = r0.iterator()
        L_0x0056:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0088
            java.lang.Object r1 = r5.next()
            r0 = r1
            com.portfolio.platform.data.model.InstalledApp r0 = (com.portfolio.platform.data.model.InstalledApp) r0
            java.lang.String r0 = r0.getIdentifier()
            java.lang.String r6 = r2.getIdentifier()
            boolean r0 = com.fossil.pq7.a(r0, r6)
            if (r0 == 0) goto L_0x0056
            r0 = r1
        L_0x0072:
            com.portfolio.platform.data.model.InstalledApp r0 = (com.portfolio.platform.data.model.InstalledApp) r0
            if (r0 == 0) goto L_0x0086
            java.lang.Boolean r0 = r0.isSelected()
            java.lang.Boolean r1 = r2.isSelected()
            boolean r0 = com.fossil.pq7.a(r0, r1)
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0043
        L_0x0086:
            r0 = r3
            goto L_0x003e
        L_0x0088:
            r0 = 0
            goto L_0x0072
        L_0x008a:
            r0 = 0
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.r06.L():boolean");
    }

    @DexIgnore
    public final void M() {
        FLogger.INSTANCE.getLocal().d(r, "registerBroadcastReceiver");
        wq5.d.e(this.j, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public final void N(boolean z) {
        InstalledApp installedApp;
        for (i06 i06 : this.g) {
            if (!(i06.getUri() == null || (installedApp = i06.getInstalledApp()) == null)) {
                installedApp.setSelected(z);
            }
        }
    }

    @DexIgnore
    public final void O(List<i06> list) {
        pq7.c(list, "<set-?>");
        this.g = list;
    }

    @DexIgnore
    public void P() {
        this.k.M5(this);
    }

    @DexIgnore
    public final void Q() {
        FLogger.INSTANCE.getLocal().d(r, "unregisterBroadcastReceiver");
        wq5.d.j(this.j, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(r, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        M();
        this.k.b();
        xw7 unused = gu7.d(k(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(r, "stop");
        Q();
        this.k.a();
    }

    @DexIgnore
    @Override // com.fossil.m06
    public void n() {
        if (!L()) {
            this.p.J0(this.e);
            FLogger.INSTANCE.getLocal().d(r, "closeAndSave, nothing changed");
            this.k.close();
            return;
        }
        jn5 jn5 = jn5.b;
        n06 n06 = this.k;
        if (n06 == null) {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
        } else if (jn5.c(jn5, ((o06) n06).getContext(), jn5.a.SET_BLE_COMMAND, false, false, false, null, 60, null)) {
            FLogger.INSTANCE.getLocal().d(r, "setRuleToDevice, showProgressDialog");
            this.k.b();
            long currentTimeMillis = System.currentTimeMillis();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = r;
            local.d(str, "filter notification, time start=" + currentTimeMillis);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = r;
            local2.d(str2, "mListAppWrapper.size = " + this.g.size());
            this.h.addAll(d47.c(this.g, false, 1, null));
            long E1 = PortfolioApp.h0.c().E1(new AppNotificationFilterSettings(this.h, System.currentTimeMillis()), PortfolioApp.h0.c().J());
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = r;
            local3.d(str3, "filter notification, time end= " + E1);
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = r;
            local4.d(str4, "delayTime =" + (E1 - currentTimeMillis) + " mili seconds");
        }
    }

    @DexIgnore
    @Override // com.fossil.m06
    public void o() {
        this.k.a();
        this.k.close();
    }

    @DexIgnore
    @Override // com.fossil.m06
    public void p(i06 i06, boolean z) {
        T t;
        boolean z2;
        InstalledApp installedApp;
        pq7.c(i06, "appWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        StringBuilder sb = new StringBuilder();
        sb.append("setAppState: appName = ");
        InstalledApp installedApp2 = i06.getInstalledApp();
        sb.append(installedApp2 != null ? installedApp2.getTitle() : null);
        sb.append(", selected = ");
        sb.append(z);
        local.d(str, sb.toString());
        Iterator<T> it = this.g.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (pq7.a(next.getUri(), i06.getUri())) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (!(t2 == null || (installedApp = t2.getInstalledApp()) == null)) {
            installedApp.setSelected(z);
        }
        if (K(z) && this.e != z) {
            this.e = z;
            this.k.Q0(z);
        } else if (!z && this.e) {
            this.e = false;
            this.k.Q0(false);
        }
        if (!this.e) {
            List<i06> list = this.g;
            if (!(list instanceof Collection) || !list.isEmpty()) {
                Iterator<T> it2 = list.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        z2 = false;
                        break;
                    }
                    InstalledApp installedApp3 = it2.next().getInstalledApp();
                    if (installedApp3 != null) {
                        Boolean isSelected = installedApp3.isSelected();
                        pq7.b(isSelected, "it.installedApp!!.isSelected");
                        if (isSelected.booleanValue()) {
                            z2 = true;
                            break;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            } else {
                z2 = false;
            }
            if (!z2) {
                return;
            }
        }
        n06 n06 = this.k;
        if (n06 != null) {
            Context context = ((o06) n06).getContext();
            if (context != null) {
                jn5.c(jn5.b, context, jn5.a.NOTIFICATION_APPS, false, false, false, null, 60, null);
                return;
            }
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
    }

    @DexIgnore
    @Override // com.fossil.m06
    public void q(boolean z) {
        boolean z2 = true;
        FLogger.INSTANCE.getLocal().d(r, "setEnableAllAppsToggle: enable = " + z);
        this.e = z;
        if (!z && K(true)) {
            N(false);
        } else if (z) {
            N(true);
        } else {
            z2 = false;
        }
        if (z2) {
            this.k.s2(this.g);
        }
        if (this.e) {
            jn5 jn5 = jn5.b;
            n06 n06 = this.k;
            if (n06 != null) {
                jn5.c(jn5, ((o06) n06).getContext(), jn5.a.NOTIFICATION_APPS, false, false, false, null, 60, null);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
        }
    }
}
