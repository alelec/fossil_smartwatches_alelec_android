package com.fossil;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x44 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<E> extends AbstractSet<E> {
        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.AbstractSet, java.util.Set
        public boolean removeAll(Collection<?> collection) {
            return x44.f(this, collection);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean retainAll(Collection<?> collection) {
            i14.l(collection);
            return super.retainAll(collection);
        }
    }

    @DexIgnore
    public static boolean a(Set<?> set, Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set2 = (Set) obj;
            try {
                return set.size() == set2.size() && set.containsAll(set2);
            } catch (ClassCastException | NullPointerException e) {
            }
        }
        return false;
    }

    @DexIgnore
    public static int b(Set<?> set) {
        Iterator<?> it = set.iterator();
        int i = 0;
        while (it.hasNext()) {
            Object next = it.next();
            i = (next != null ? next.hashCode() : 0) + i;
        }
        return i;
    }

    @DexIgnore
    public static <E> HashSet<E> c() {
        return new HashSet<>();
    }

    @DexIgnore
    public static <E> HashSet<E> d(int i) {
        return new HashSet<>(x34.a(i));
    }

    @DexIgnore
    public static <E> LinkedHashSet<E> e(int i) {
        return new LinkedHashSet<>(x34.a(i));
    }

    @DexIgnore
    public static boolean f(Set<?> set, Collection<?> collection) {
        i14.l(collection);
        if (collection instanceof c44) {
            collection = ((c44) collection).elementSet();
        }
        return (!(collection instanceof Set) || collection.size() <= set.size()) ? g(set, collection.iterator()) : p34.r(set.iterator(), collection);
    }

    @DexIgnore
    public static boolean g(Set<?> set, Iterator<?> it) {
        boolean z = false;
        while (it.hasNext()) {
            z |= set.remove(it.next());
        }
        return z;
    }
}
