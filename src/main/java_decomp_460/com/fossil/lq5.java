package com.fossil;

import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Range;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lq5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<MicroAppSetting> f2233a; // = new ArrayList();
    @DexIgnore
    public Range b;

    @DexIgnore
    public List<MicroAppSetting> a() {
        return this.f2233a;
    }

    @DexIgnore
    public Range b() {
        return this.b;
    }

    @DexIgnore
    public void c(gj4 gj4) {
        this.f2233a = new ArrayList();
        if (gj4.s(CloudLogWriter.ITEMS_PARAM)) {
            try {
                bj4 q = gj4.q(CloudLogWriter.ITEMS_PARAM);
                if (q.size() > 0) {
                    for (int i = 0; i < q.size(); i++) {
                        gj4 d = q.m(i).d();
                        MicroAppSetting microAppSetting = new MicroAppSetting();
                        if (d.s("appId")) {
                            microAppSetting.setMicroAppId(d.p("appId").f());
                        }
                        if (d.s(MicroAppSetting.LIKE)) {
                            microAppSetting.setLike(d.p(MicroAppSetting.LIKE).a());
                        }
                        if (d.s(MicroAppSetting.SETTING)) {
                            if (d.r(MicroAppSetting.SETTING).size() == 0) {
                                microAppSetting.setSetting("");
                            } else {
                                microAppSetting.setSetting(d.r(MicroAppSetting.SETTING).toString());
                            }
                        }
                        this.f2233a.add(microAppSetting);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (gj4.s("_range")) {
            try {
                this.b = (Range) new Gson().k(gj4.r("_range").toString(), Range.class);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
