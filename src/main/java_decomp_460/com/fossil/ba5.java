package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.WaveView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ba5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ RTLImageView s;
    @DexIgnore
    public /* final */ WaveView t;
    @DexIgnore
    public /* final */ DashBar u;
    @DexIgnore
    public /* final */ ConstraintLayout v;

    @DexIgnore
    public ba5(Object obj, View view, int i, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView, WaveView waveView, DashBar dashBar, ConstraintLayout constraintLayout) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = flexibleTextView2;
        this.s = rTLImageView;
        this.t = waveView;
        this.u = dashBar;
        this.v = constraintLayout;
    }
}
