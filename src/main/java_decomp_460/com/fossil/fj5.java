package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fj5 {
    @DexIgnore
    public static final boolean a(String str, Gson gson, String str2) {
        CommuteTimeSetting commuteTimeSetting;
        CommuteTimeSetting commuteTimeSetting2;
        pq7.c(gson, "gson");
        try {
            commuteTimeSetting = (CommuteTimeSetting) gson.k(str2, CommuteTimeSetting.class);
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse commute time");
            commuteTimeSetting = new CommuteTimeSetting(null, null, false, null, 15, null);
        }
        CommuteTimeSetting commuteTimeSetting3 = commuteTimeSetting == null ? new CommuteTimeSetting(null, null, false, null, 15, null) : commuteTimeSetting;
        try {
            commuteTimeSetting2 = (CommuteTimeSetting) gson.k(str, CommuteTimeSetting.class);
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse other commute time");
            commuteTimeSetting2 = new CommuteTimeSetting(null, null, false, null, 15, null);
        }
        if (commuteTimeSetting2 == null) {
            commuteTimeSetting2 = new CommuteTimeSetting(null, null, false, null, 15, null);
        }
        return pq7.a(commuteTimeSetting3.getAddress(), commuteTimeSetting2.getAddress()) && commuteTimeSetting3.getAvoidTolls() == commuteTimeSetting2.getAvoidTolls() && pq7.a(commuteTimeSetting3.getFormat(), commuteTimeSetting2.getFormat());
    }

    @DexIgnore
    public static final boolean b(String str, Gson gson, String str2) {
        Ringtone ringtone;
        Ringtone ringtone2;
        pq7.c(gson, "gson");
        try {
            ringtone = (Ringtone) gson.k(str, Ringtone.class);
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse ringPhoneSetting");
            ringtone = new Ringtone(null, null, 3, null);
        }
        Ringtone ringtone3 = ringtone == null ? new Ringtone(null, null, 3, null) : ringtone;
        try {
            ringtone2 = (Ringtone) gson.k(str2, Ringtone.class);
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse otherRingPhoneSetting");
            ringtone2 = new Ringtone(null, null, 3, null);
        }
        if (ringtone2 == null) {
            ringtone2 = new Ringtone(null, null, 3, null);
        }
        return pq7.a(ringtone2.getRingtoneName(), ringtone3.getRingtoneName());
    }

    @DexIgnore
    public static final boolean c(String str, Gson gson, String str2) {
        SecondTimezoneSetting secondTimezoneSetting;
        SecondTimezoneSetting secondTimezoneSetting2;
        pq7.c(gson, "gson");
        try {
            secondTimezoneSetting = (SecondTimezoneSetting) gson.k(str2, SecondTimezoneSetting.class);
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse otherSecondTzSetting");
            secondTimezoneSetting = new SecondTimezoneSetting(null, null, 0, null, 15, null);
        }
        SecondTimezoneSetting secondTimezoneSetting3 = secondTimezoneSetting == null ? new SecondTimezoneSetting(null, null, 0, null, 15, null) : secondTimezoneSetting;
        try {
            secondTimezoneSetting2 = (SecondTimezoneSetting) gson.k(str, SecondTimezoneSetting.class);
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse secondTzSetting");
            secondTimezoneSetting2 = new SecondTimezoneSetting(null, null, 0, null, 15, null);
        }
        if (secondTimezoneSetting2 == null) {
            secondTimezoneSetting2 = new SecondTimezoneSetting(null, null, 0, null, 15, null);
        }
        return pq7.a(secondTimezoneSetting3, secondTimezoneSetting2);
    }

    @DexIgnore
    public static final boolean d(String str) {
        return TextUtils.isEmpty(str) || pq7.a(str, "{}");
    }
}
