package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qs2 extends as2 implements os2 {
    @DexIgnore
    public qs2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IPolygonDelegate");
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final boolean L2(os2 os2) throws RemoteException {
        Parcel d = d();
        es2.c(d, os2);
        Parcel e = e(19, d);
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final int a() throws RemoteException {
        Parcel e = e(20, d());
        int readInt = e.readInt();
        e.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final void b(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(21, d);
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final String getId() throws RemoteException {
        Parcel e = e(2, d());
        String readString = e.readString();
        e.recycle();
        return readString;
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final void remove() throws RemoteException {
        i(1, d());
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final void setFillColor(int i) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        i(11, d);
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final void setGeodesic(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(17, d);
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final void setPoints(List<LatLng> list) throws RemoteException {
        Parcel d = d();
        d.writeTypedList(list);
        i(3, d);
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final void setStrokeColor(int i) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        i(9, d);
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final void setStrokeWidth(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        i(7, d);
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final void setVisible(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(15, d);
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final void setZIndex(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        i(13, d);
    }
}
