package com.fossil;

import android.text.TextUtils;
import com.facebook.share.internal.VideoUploader;
import com.fossil.tq4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.y56;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.AppType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h66 extends b66 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public /* final */ List<j06> e; // = new ArrayList();
    @DexIgnore
    public /* final */ c66 f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ ArrayList<j06> h;
    @DexIgnore
    public /* final */ uq4 i;
    @DexIgnore
    public /* final */ y56 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return h66.k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$registerContactObserver$1", f = "NotificationHybridEveryonePresenter.kt", l = {135}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ h66 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$registerContactObserver$1$1", f = "NotificationHybridEveryonePresenter.kt", l = {136}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    PortfolioApp c = PortfolioApp.h0.c();
                    this.L$0 = iv7;
                    this.label = 1;
                    if (c.V0(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(h66 h66, qn7 qn7) {
            super(2, qn7);
            this.this$0 = h66;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 h = this.this$0.h();
                a aVar = new a(null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(h, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1", f = "NotificationHybridEveryonePresenter.kt", l = {40}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ h66 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1$1", f = "NotificationHybridEveryonePresenter.kt", l = {41}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    PortfolioApp c = PortfolioApp.h0.c();
                    this.L$0 = iv7;
                    this.label = 1;
                    if (c.V0(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements tq4.d<y56.c, y56.a> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ c f1433a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public b(c cVar) {
                this.f1433a = cVar;
            }

            @DexIgnore
            /* renamed from: b */
            public void a(y56.a aVar) {
                pq7.c(aVar, "errorResponse");
                FLogger.INSTANCE.getLocal().d(h66.l.a(), "GetAllContactGroup onError");
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(y56.c cVar) {
                pq7.c(cVar, "successResponse");
                FLogger.INSTANCE.getLocal().d(h66.l.a(), "GetAllContactGroup onSuccess");
                for (T t : cVar.a()) {
                    for (Contact contact : t.getContacts()) {
                        pq7.b(contact, "contact");
                        if (contact.getContactId() == -100 || contact.getContactId() == -200) {
                            j06 j06 = new j06(contact, null, 2, null);
                            j06.setAdded(true);
                            Contact contact2 = j06.getContact();
                            if (contact2 != null) {
                                contact2.setDbRowId(contact.getDbRowId());
                                contact2.setUseSms(contact.isUseSms());
                                contact2.setUseCall(contact.isUseCall());
                            }
                            j06.setCurrentHandGroup(t.getHour());
                            List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                            pq7.b(phoneNumbers, "contact.phoneNumbers");
                            if (!phoneNumbers.isEmpty()) {
                                PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                                pq7.b(phoneNumber, "contact.phoneNumbers[0]");
                                String number = phoneNumber.getNumber();
                                if (!TextUtils.isEmpty(number)) {
                                    j06.setHasPhoneNumber(true);
                                    j06.setPhoneNumber(number);
                                    FLogger.INSTANCE.getLocal().d(h66.l.a(), " filter selected contact, phoneNumber=" + number);
                                }
                            }
                            Iterator it = this.f1433a.this$0.h.iterator();
                            int i = 0;
                            while (true) {
                                if (!it.hasNext()) {
                                    i = -1;
                                    break;
                                }
                                Contact contact3 = ((j06) it.next()).getContact();
                                if (contact3 != null && contact3.getContactId() == contact.getContactId()) {
                                    break;
                                }
                                i++;
                            }
                            if (i != -1) {
                                j06.setCurrentHandGroup(this.f1433a.this$0.g);
                                this.f1433a.this$0.h.remove(i);
                            }
                            FLogger.INSTANCE.getLocal().d(h66.l.a(), ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                            this.f1433a.this$0.B().add(j06);
                        }
                    }
                }
                if (!this.f1433a.this$0.h.isEmpty()) {
                    for (j06 j062 : this.f1433a.this$0.h) {
                        this.f1433a.this$0.B().add(j062);
                    }
                }
                this.f1433a.this$0.f.V3(this.f1433a.this$0.B(), this.f1433a.this$0.g);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(h66 h66, qn7 qn7) {
            super(2, qn7);
            this.this$0 = h66;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (!PortfolioApp.h0.c().k0().s0()) {
                    dv7 h = this.this$0.h();
                    a aVar = new a(null);
                    this.L$0 = iv7;
                    this.label = 1;
                    if (eu7.g(h, aVar, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.B().isEmpty()) {
                this.this$0.i.a(this.this$0.j, null, new b(this));
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = h66.class.getSimpleName();
        pq7.b(simpleName, "NotificationHybridEveryo\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public h66(c66 c66, int i2, ArrayList<j06> arrayList, uq4 uq4, y56 y56) {
        pq7.c(c66, "mView");
        pq7.c(arrayList, "mContactWrappersSelected");
        pq7.c(uq4, "mUseCaseHandler");
        pq7.c(y56, "mGetAllHybridContactGroups");
        this.f = c66;
        this.g = i2;
        this.h = arrayList;
        this.i = uq4;
        this.j = y56;
    }

    @DexIgnore
    public final j06 A(int i2, String str) {
        List<PhoneNumber> phoneNumbers;
        List<PhoneNumber> phoneNumbers2;
        Contact contact = new Contact();
        contact.setContactId(i2);
        contact.setFirstName(str);
        j06 j06 = new j06(contact, null, 2, null);
        j06.setHasPhoneNumber(true);
        j06.setAdded(true);
        j06.setCurrentHandGroup(this.g);
        if (i2 == -100) {
            Contact contact2 = j06.getContact();
            if (contact2 != null) {
                contact2.setUseCall(true);
            }
            Contact contact3 = j06.getContact();
            if (contact3 != null) {
                contact3.setUseSms(false);
            }
            PhoneNumber phoneNumber = new PhoneNumber();
            phoneNumber.setContact(j06.getContact());
            phoneNumber.setNumber("-1234");
            Contact contact4 = j06.getContact();
            if (!(contact4 == null || (phoneNumbers2 = contact4.getPhoneNumbers()) == null)) {
                phoneNumbers2.add(phoneNumber);
            }
        } else {
            Contact contact5 = j06.getContact();
            if (contact5 != null) {
                contact5.setUseCall(false);
            }
            Contact contact6 = j06.getContact();
            if (contact6 != null) {
                contact6.setUseSms(true);
            }
            PhoneNumber phoneNumber2 = new PhoneNumber();
            phoneNumber2.setContact(j06.getContact());
            phoneNumber2.setNumber("-5678");
            Contact contact7 = j06.getContact();
            if (!(contact7 == null || (phoneNumbers = contact7.getPhoneNumbers()) == null)) {
                phoneNumbers.add(phoneNumber2);
            }
        }
        return j06;
    }

    @DexIgnore
    public final List<j06> B() {
        return this.e;
    }

    @DexIgnore
    public void C() {
        this.f.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(k, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        xw7 unused = gu7.d(k(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    @Override // com.fossil.b66
    public int n() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.b66
    public void o() {
        boolean z;
        if (!this.e.isEmpty()) {
            int size = this.e.size() - 1;
            while (true) {
                if (size < 0) {
                    z = false;
                    break;
                }
                Contact contact = this.e.get(size).getContact();
                if (contact == null || contact.getContactId() != -100) {
                    size--;
                } else if (this.e.get(size).getCurrentHandGroup() != this.g) {
                    this.f.N0(this.e.get(size));
                    z = true;
                } else {
                    this.e.remove(size);
                    z = true;
                }
            }
            if (!z) {
                String str = AppType.ALL_CALLS.packageName;
                pq7.b(str, "AppType.ALL_CALLS.packageName");
                this.e.add(A(-100, str));
            }
        } else {
            String str2 = AppType.ALL_CALLS.packageName;
            pq7.b(str2, "AppType.ALL_CALLS.packageName");
            this.e.add(A(-100, str2));
        }
        this.f.V3(this.e, this.g);
    }

    @DexIgnore
    @Override // com.fossil.b66
    public void p() {
        boolean z;
        if (!this.e.isEmpty()) {
            int size = this.e.size() - 1;
            while (true) {
                if (size < 0) {
                    z = false;
                    break;
                }
                Contact contact = this.e.get(size).getContact();
                if (contact == null || contact.getContactId() != -200) {
                    size--;
                } else if (this.e.get(size).getCurrentHandGroup() != this.g) {
                    this.f.N0(this.e.get(size));
                    z = true;
                } else {
                    this.e.remove(size);
                    z = true;
                }
            }
            if (!z) {
                String str = AppType.ALL_SMS.packageName;
                pq7.b(str, "AppType.ALL_SMS.packageName");
                this.e.add(A(-200, str));
            }
        } else {
            String str2 = AppType.ALL_SMS.packageName;
            pq7.b(str2, "AppType.ALL_SMS.packageName");
            this.e.add(A(-200, str2));
        }
        this.f.V3(this.e, this.g);
    }

    @DexIgnore
    @Override // com.fossil.b66
    public void q() {
        ArrayList<j06> arrayList = new ArrayList<>();
        for (T t : this.e) {
            if (t.isAdded() && t.getCurrentHandGroup() == this.g) {
                arrayList.add(t);
            }
        }
        this.f.I(arrayList);
    }

    @DexIgnore
    @Override // com.fossil.b66
    public void r(j06 j06) {
        pq7.c(j06, "contactWrapper");
        for (T t : this.e) {
            Contact contact = t.getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Contact contact2 = j06.getContact();
            if (pq7.a(valueOf, contact2 != null ? Integer.valueOf(contact2.getContactId()) : null)) {
                t.setCurrentHandGroup(this.g);
                t.setAdded(true);
            }
        }
        this.f.V3(this.e, this.g);
    }

    @DexIgnore
    @Override // com.fossil.b66
    public void s() {
        if (!PortfolioApp.h0.c().k0().s0()) {
            xw7 unused = gu7.d(k(), null, null, new b(this, null), 3, null);
        }
    }
}
