package com.fossil;

import android.graphics.Bitmap;
import android.os.RemoteException;
import com.fossil.qb3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xf3 extends ed3 {
    @DexIgnore
    public /* final */ /* synthetic */ qb3.n b;

    @DexIgnore
    public xf3(qb3 qb3, qb3.n nVar) {
        this.b = nVar;
    }

    @DexIgnore
    @Override // com.fossil.dd3
    public final void onSnapshotReady(Bitmap bitmap) throws RemoteException {
        this.b.onSnapshotReady(bitmap);
    }

    @DexIgnore
    @Override // com.fossil.dd3
    public final void q1(rg2 rg2) throws RemoteException {
        this.b.onSnapshotReady((Bitmap) tg2.i(rg2));
    }
}
