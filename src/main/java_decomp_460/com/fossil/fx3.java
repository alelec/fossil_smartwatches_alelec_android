package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fx3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ View f1234a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public boolean f; // = true;
    @DexIgnore
    public boolean g; // = true;

    @DexIgnore
    public fx3(View view) {
        this.f1234a = view;
    }

    @DexIgnore
    public void a() {
        View view = this.f1234a;
        mo0.W(view, this.d - (view.getTop() - this.b));
        View view2 = this.f1234a;
        mo0.V(view2, this.e - (view2.getLeft() - this.c));
    }

    @DexIgnore
    public int b() {
        return this.b;
    }

    @DexIgnore
    public int c() {
        return this.e;
    }

    @DexIgnore
    public int d() {
        return this.d;
    }

    @DexIgnore
    public boolean e() {
        return this.g;
    }

    @DexIgnore
    public boolean f() {
        return this.f;
    }

    @DexIgnore
    public void g() {
        this.b = this.f1234a.getTop();
        this.c = this.f1234a.getLeft();
    }

    @DexIgnore
    public void h(boolean z) {
        this.g = z;
    }

    @DexIgnore
    public boolean i(int i) {
        if (!this.g || this.e == i) {
            return false;
        }
        this.e = i;
        a();
        return true;
    }

    @DexIgnore
    public boolean j(int i) {
        if (!this.f || this.d == i) {
            return false;
        }
        this.d = i;
        a();
        return true;
    }

    @DexIgnore
    public void k(boolean z) {
        this.f = z;
    }
}
