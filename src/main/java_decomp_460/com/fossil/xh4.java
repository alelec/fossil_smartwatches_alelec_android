package com.fossil;

import android.content.Intent;
import com.misfit.frameworks.common.constants.Constants;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xh4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4123a;
    @DexIgnore
    public /* final */ Intent b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements sd4<xh4> {
        @DexIgnore
        /* renamed from: b */
        public void a(xh4 xh4, td4 td4) throws rd4, IOException {
            Intent b = xh4.b();
            td4.c("ttl", ai4.q(b));
            td4.f(Constants.EVENT, xh4.a());
            td4.f("instanceId", ai4.e());
            td4.c("priority", ai4.n(b));
            td4.f("packageName", ai4.m());
            td4.f("sdkPlatform", "ANDROID");
            td4.f("messageType", ai4.k(b));
            String g = ai4.g(b);
            if (g != null) {
                td4.f("messageId", g);
            }
            String p = ai4.p(b);
            if (p != null) {
                td4.f("topic", p);
            }
            String b2 = ai4.b(b);
            if (b2 != null) {
                td4.f("collapseKey", b2);
            }
            if (ai4.h(b) != null) {
                td4.f("analyticsLabel", ai4.h(b));
            }
            if (ai4.d(b) != null) {
                td4.f("composerLabel", ai4.d(b));
            }
            String o = ai4.o();
            if (o != null) {
                td4.f("projectNumber", o);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ xh4 f4124a;

        @DexIgnore
        public b(xh4 xh4) {
            rc2.k(xh4);
            this.f4124a = xh4;
        }

        @DexIgnore
        public final xh4 a() {
            return this.f4124a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements sd4<b> {
        @DexIgnore
        /* renamed from: b */
        public final void a(b bVar, td4 td4) throws rd4, IOException {
            td4.f("messaging_client_event", bVar.a());
        }
    }

    @DexIgnore
    public xh4(String str, Intent intent) {
        rc2.h(str, "evenType must be non-null");
        this.f4123a = str;
        rc2.l(intent, "intent must be non-null");
        this.b = intent;
    }

    @DexIgnore
    public final String a() {
        return this.f4123a;
    }

    @DexIgnore
    public final Intent b() {
        return this.b;
    }
}
