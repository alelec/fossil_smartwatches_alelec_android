package com.fossil;

import com.fossil.e13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vu2 extends e13<vu2, a> implements o23 {
    @DexIgnore
    public static /* final */ vu2 zzf;
    @DexIgnore
    public static volatile z23<vu2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public long zze;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<vu2, a> implements o23 {
        @DexIgnore
        public a() {
            super(vu2.zzf);
        }

        @DexIgnore
        public /* synthetic */ a(tu2 tu2) {
            this();
        }

        @DexIgnore
        public final a x(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((vu2) this.c).C(i);
            return this;
        }

        @DexIgnore
        public final a y(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((vu2) this.c).D(j);
            return this;
        }
    }

    /*
    static {
        vu2 vu2 = new vu2();
        zzf = vu2;
        e13.u(vu2.class, vu2);
    }
    */

    @DexIgnore
    public static a L() {
        return (a) zzf.w();
    }

    @DexIgnore
    public final void C(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    @DexIgnore
    public final void D(long j) {
        this.zzc |= 2;
        this.zze = j;
    }

    @DexIgnore
    public final boolean H() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int I() {
        return this.zzd;
    }

    @DexIgnore
    public final boolean J() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final long K() {
        return this.zze;
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (tu2.f3469a[i - 1]) {
            case 1:
                return new vu2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u1004\u0000\u0002\u1002\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                z23<vu2> z232 = zzg;
                if (z232 != null) {
                    return z232;
                }
                synchronized (vu2.class) {
                    try {
                        z23 = zzg;
                        if (z23 == null) {
                            z23 = new e13.c(zzf);
                            zzg = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
