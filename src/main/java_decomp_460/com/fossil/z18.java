package com.fossil;

import com.fossil.p18;
import java.io.IOException;
import java.net.Socket;
import javax.net.ssl.SSLSocket;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class z18 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static z18 f4406a;

    @DexIgnore
    public abstract void a(p18.a aVar, String str);

    @DexIgnore
    public abstract void b(p18.a aVar, String str, String str2);

    @DexIgnore
    public abstract void c(g18 g18, SSLSocket sSLSocket, boolean z);

    @DexIgnore
    public abstract int d(Response.a aVar);

    @DexIgnore
    public abstract boolean e(f18 f18, l28 l28);

    @DexIgnore
    public abstract Socket f(f18 f18, x08 x08, p28 p28);

    @DexIgnore
    public abstract boolean g(x08 x08, x08 x082);

    @DexIgnore
    public abstract l28 h(f18 f18, x08 x08, p28 p28, x18 x18);

    @DexIgnore
    public abstract void i(f18 f18, l28 l28);

    @DexIgnore
    public abstract m28 j(f18 f18);

    @DexIgnore
    public abstract IOException k(a18 a18, IOException iOException);
}
