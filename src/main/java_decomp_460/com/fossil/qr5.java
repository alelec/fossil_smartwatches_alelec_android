package com.fossil;

import android.view.KeyEvent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qr5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3010a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public qr5(String str, String str2) {
        pq7.c(str, "appName");
        pq7.c(str2, "packageName");
        this.f3010a = str;
        this.b = str2;
    }

    @DexIgnore
    public abstract boolean a(KeyEvent keyEvent);

    @DexIgnore
    public final String b() {
        return this.f3010a;
    }

    @DexIgnore
    public abstract rr5 c();

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public abstract int e();

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof qr5)) {
            return false;
        }
        return pq7.a(this.b, ((qr5) obj).b);
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }
}
