package com.fossil;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.br4;
import com.fossil.n04;
import com.fossil.t47;
import com.fossil.wz5;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gz5 extends qv5 implements lb6, View.OnClickListener, br4.c, t47.g, aw5 {
    @DexIgnore
    public static /* final */ a x; // = new a(null);
    @DexIgnore
    public kb6 h;
    @DexIgnore
    public ConstraintLayout i;
    @DexIgnore
    public ViewPager2 j;
    @DexIgnore
    public int k;
    @DexIgnore
    public br4 l;
    @DexIgnore
    public g37<z75> m;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public HashMap w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final gz5 a() {
            return new gz5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ViewPager2.i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gz5 f1400a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(gz5 gz5) {
            this.f1400a = gz5;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            TabLayout tabLayout;
            TabLayout.g v;
            Drawable e;
            TabLayout tabLayout2;
            TabLayout.g v2;
            Drawable e2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "onPageScrolled position " + i + " mCurrentPosition " + this.f1400a.N6());
            super.b(i, f, i2);
            if (!TextUtils.isEmpty(this.f1400a.t)) {
                int parseColor = Color.parseColor(this.f1400a.t);
                z75 z75 = (z75) gz5.L6(this.f1400a).a();
                if (!(z75 == null || (tabLayout2 = z75.z) == null || (v2 = tabLayout2.v(i)) == null || (e2 = v2.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.f1400a.s) && this.f1400a.N6() != i) {
                int parseColor2 = Color.parseColor(this.f1400a.s);
                z75 z752 = (z75) gz5.L6(this.f1400a).a();
                if (!(z752 == null || (tabLayout = z752.z) == null || (v = tabLayout.v(this.f1400a.N6())) == null || (e = v.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.f1400a.Q6(i);
            this.f1400a.O6().n(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements wz5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gz5 f1401a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public c(gz5 gz5, String str) {
            this.f1401a = gz5;
            this.b = str;
        }

        @DexIgnore
        @Override // com.fossil.wz5.b
        public void a(String str) {
            pq7.c(str, "presetName");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "showRenamePresetDialog - presetName=" + str);
            if (!TextUtils.isEmpty(str)) {
                this.f1401a.O6().q(str, this.b);
            }
        }

        @DexIgnore
        @Override // com.fossil.wz5.b
        public void onCancel() {
            FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "showRenamePresetDialog - onCancel");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements n04.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gz5 f1402a;

        @DexIgnore
        public d(gz5 gz5) {
            this.f1402a = gz5;
        }

        @DexIgnore
        @Override // com.fossil.n04.b
        public final void a(TabLayout.g gVar, int i) {
            pq7.c(gVar, "tab");
            int itemCount = this.f1402a.getItemCount();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "tabLayoutConfig dataSize=" + itemCount);
            if (!TextUtils.isEmpty(this.f1402a.s)) {
                int parseColor = Color.parseColor(this.f1402a.s);
                if (i == 0) {
                    gVar.o(2131230943);
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor);
                    }
                } else if (i == itemCount - 1) {
                    gVar.o(2131230817);
                    Drawable e2 = gVar.e();
                    if (e2 != null) {
                        e2.setTint(parseColor);
                    }
                } else {
                    gVar.o(2131230966);
                    Drawable e3 = gVar.e();
                    if (e3 != null) {
                        e3.setTint(parseColor);
                    }
                }
                if (!TextUtils.isEmpty(this.f1402a.t) && i == this.f1402a.N6()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("HomeHybridCustomizeFragment", "tabLayoutConfig primary mCurrentPosition " + this.f1402a.N6());
                    Drawable e4 = gVar.e();
                    if (e4 != null) {
                        e4.setTint(Color.parseColor(this.f1402a.t));
                    }
                }
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ g37 L6(gz5 gz5) {
        g37<z75> g37 = gz5.m;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.lb6
    public void C5(String str) {
        pq7.c(str, "microAppId");
        if (isActive()) {
            jn5.c(jn5.b, requireContext(), hl5.f1493a.d(str), false, false, false, null, 60, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "HomeHybridCustomizeFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.lb6
    public void J2(List<m66> list) {
        pq7.c(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizeFragment", "showPresets - data=" + list.size());
        br4 br4 = this.l;
        if (br4 != null) {
            br4.j(new ArrayList<>(list));
            if (!this.v && this.k == 0) {
                this.v = false;
            }
            if (!this.v) {
                ViewPager2 viewPager2 = this.j;
                if (viewPager2 != null) {
                    viewPager2.setCurrentItem(this.k);
                } else {
                    pq7.n("rvCustomize");
                    throw null;
                }
            }
        } else {
            pq7.n("mHybridPresetDetailAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.br4.c
    public void N(String str, String str2) {
        pq7.c(str, "presetName");
        pq7.c(str2, "presetId");
        if (getChildFragmentManager().Z("RenamePresetDialogFragment") == null) {
            wz5 a2 = wz5.i.a(str, new c(this, str2));
            if (isActive()) {
                a2.show(getChildFragmentManager(), "RenamePresetDialogFragment");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.lb6
    public void N3() {
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "showCreateNewSuccessfully");
        br4 br4 = this.l;
        if (br4 != null) {
            int itemCount = br4.getItemCount();
            int i2 = this.k;
            if (itemCount > i2) {
                ViewPager2 viewPager2 = this.j;
                if (viewPager2 != null) {
                    viewPager2.setCurrentItem(i2);
                } else {
                    pq7.n("rvCustomize");
                    throw null;
                }
            }
        } else {
            pq7.n("mHybridPresetDetailAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final int N6() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.lb6
    public void O(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizeFragment", "showDeleteSuccessfully - position=" + i2 + " mCurrentPosition=" + this.k);
        br4 br4 = this.l;
        if (br4 == null) {
            pq7.n("mHybridPresetDetailAdapter");
            throw null;
        } else if (br4.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.j;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(this.k);
            } else {
                pq7.n("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    public final kb6 O6() {
        kb6 kb6 = this.h;
        if (kb6 != null) {
            return kb6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void P6(z75 z75) {
        this.s = qn5.l.a().d("nonBrandSwitchDisabledGray");
        this.t = qn5.l.a().d("primaryColor");
        this.u = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
        this.l = new br4(new ArrayList(), this);
        ConstraintLayout constraintLayout = z75.q;
        pq7.b(constraintLayout, "binding.clNoDevice");
        this.i = constraintLayout;
        z75.u.setOnClickListener(this);
        z75.v.setImageResource(2131230945);
        ViewPager2 viewPager2 = z75.y;
        pq7.b(viewPager2, "binding.rvPreset");
        this.j = viewPager2;
        if (viewPager2 != null) {
            if (viewPager2.getChildAt(0) != null) {
                ViewPager2 viewPager22 = this.j;
                if (viewPager22 != null) {
                    View childAt = viewPager22.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setOverScrollMode(2);
                    } else {
                        throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                } else {
                    pq7.n("rvCustomize");
                    throw null;
                }
            }
            ViewPager2 viewPager23 = this.j;
            if (viewPager23 != null) {
                br4 br4 = this.l;
                if (br4 != null) {
                    viewPager23.setAdapter(br4);
                    if (!TextUtils.isEmpty(this.u)) {
                        TabLayout tabLayout = z75.z;
                        pq7.b(tabLayout, "binding.tab");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.u)));
                    }
                    ViewPager2 viewPager24 = this.j;
                    if (viewPager24 != null) {
                        viewPager24.g(new b(this));
                        ViewPager2 viewPager25 = this.j;
                        if (viewPager25 != null) {
                            viewPager25.setCurrentItem(this.k);
                        } else {
                            pq7.n("rvCustomize");
                            throw null;
                        }
                    } else {
                        pq7.n("rvCustomize");
                        throw null;
                    }
                } else {
                    pq7.n("mHybridPresetDetailAdapter");
                    throw null;
                }
            } else {
                pq7.n("rvCustomize");
                throw null;
            }
        } else {
            pq7.n("rvCustomize");
            throw null;
        }
    }

    @DexIgnore
    public final void Q6(int i2) {
        this.k = i2;
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        String str2;
        pq7.c(str, "tag");
        if (str.hashCode() == -1353443012 && str.equals("DIALOG_DELETE_PRESET") && i2 == 2131363373) {
            if (intent != null) {
                str2 = intent.getStringExtra("NEXT_ACTIVE_PRESET_ID");
                pq7.b(str2, "it.getStringExtra(NEXT_ACTIVE_PRESET_ID)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeHybridCustomizeFragment", "onDialogFragmentResult - nextActivePreset " + str2);
            } else {
                str2 = "";
            }
            kb6 kb6 = this.h;
            if (kb6 != null) {
                kb6.p(str2);
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    /* renamed from: R6 */
    public void M5(kb6 kb6) {
        pq7.c(kb6, "presenter");
        this.h = kb6;
    }

    @DexIgnore
    public final void S6() {
        g37<z75> g37 = this.m;
        if (g37 != null) {
            z75 a2 = g37.a();
            TabLayout tabLayout = a2 != null ? a2.z : null;
            if (tabLayout != null) {
                ViewPager2 viewPager2 = this.j;
                if (viewPager2 != null) {
                    new n04(tabLayout, viewPager2, new d(this)).a();
                } else {
                    pq7.n("rvCustomize");
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.br4.c
    public void Y3() {
        kb6 kb6 = this.h;
        if (kb6 != null) {
            kb6.r();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.aw5
    public void b2(boolean z) {
        if (z) {
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        vl5 C62 = C6();
        if (C62 != null) {
            C62.c("");
        }
    }

    @DexIgnore
    @Override // com.fossil.lb6
    public void c0(int i2) {
        br4 br4 = this.l;
        if (br4 == null) {
            return;
        }
        if (br4 == null) {
            pq7.n("mHybridPresetDetailAdapter");
            throw null;
        } else if (br4.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.j;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(this.k);
            } else {
                pq7.n("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.br4.c
    public void c4() {
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "onAddPresetClick");
        kb6 kb6 = this.h;
        if (kb6 != null) {
            kb6.o();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.lb6
    public int getItemCount() {
        ViewPager2 viewPager2 = this.j;
        if (viewPager2 != null) {
            RecyclerView.g adapter = viewPager2.getAdapter();
            if (adapter != null) {
                return adapter.getItemCount();
            }
            return 0;
        }
        pq7.n("rvCustomize");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.lb6
    public void j2(boolean z) {
        if (z) {
            g37<z75> g37 = this.m;
            if (g37 != null) {
                z75 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.B;
                    pq7.b(flexibleTextView, "tvTapIconToCustomize");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        g37<z75> g372 = this.m;
        if (g372 != null) {
            z75 a3 = g372.a();
            if (a3 != null) {
                FlexibleTextView flexibleTextView2 = a3.B;
                pq7.b(flexibleTextView2, "tvTapIconToCustomize");
                flexibleTextView2.setVisibility(4);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.lb6
    public void m0(int i2) {
        this.v = true;
        br4 br4 = this.l;
        if (br4 == null) {
            pq7.n("mHybridPresetDetailAdapter");
            throw null;
        } else if (br4.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.j;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(i2);
            } else {
                pq7.n("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        FragmentActivity activity;
        pq7.c(view, "v");
        if (view.getId() == 2131362501 && (activity = getActivity()) != null) {
            PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
            pq7.b(activity, "it");
            PairingInstructionsActivity.a.b(aVar, activity, false, false, 6, null);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        z75 z75 = (z75) aq0.f(layoutInflater, 2131558576, viewGroup, false, A6());
        pq7.b(z75, "binding");
        P6(z75);
        g37<z75> g37 = new g37<>(this, z75);
        this.m = g37;
        if (g37 != null) {
            z75 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "onPause");
        kb6 kb6 = this.h;
        if (kb6 == null) {
            return;
        }
        if (kb6 != null) {
            kb6.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "onResume");
        kb6 kb6 = this.h;
        if (kb6 == null) {
            return;
        }
        if (kb6 != null) {
            kb6.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        S6();
        if (this.h != null) {
            E6("customize_view");
        }
    }

    @DexIgnore
    @Override // com.fossil.lb6
    public void r(boolean z) {
        if (z) {
            String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d2)) {
                ConstraintLayout constraintLayout = this.i;
                if (constraintLayout != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(d2));
                } else {
                    pq7.n("clNoDevice");
                    throw null;
                }
            }
            ConstraintLayout constraintLayout2 = this.i;
            if (constraintLayout2 != null) {
                constraintLayout2.setVisibility(0);
            } else {
                pq7.n("clNoDevice");
                throw null;
            }
        } else {
            ConstraintLayout constraintLayout3 = this.i;
            if (constraintLayout3 != null) {
                constraintLayout3.setVisibility(8);
            } else {
                pq7.n("clNoDevice");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.br4.c
    public void r0(boolean z, String str, String str2, String str3) {
        pq7.c(str, "currentPresetName");
        pq7.c(str2, "nextPresetName");
        pq7.c(str3, "nextPresetId");
        if (isActive()) {
            String string = requireActivity().getString(2131886547);
            pq7.b(string, "requireActivity().getStr\u2026ingAPresetIsPermanentAnd)");
            if (z) {
                String string2 = requireActivity().getString(2131886548);
                pq7.b(string2, "requireActivity().getStr\u2026ingAPresetIsPermanentAnd)");
                hr7 hr7 = hr7.f1520a;
                string = String.format(string2, Arrays.copyOf(new Object[]{vt7.g(str2)}, 1));
                pq7.b(string, "java.lang.String.format(format, *args)");
            }
            Bundle bundle = new Bundle();
            bundle.putString("NEXT_ACTIVE_PRESET_ID", str3);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "nextPresetId " + str3);
            t47.f fVar = new t47.f(2131558484);
            hr7 hr72 = hr7.f1520a;
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886549);
            pq7.b(c2, "LanguageHelper.getString\u2026_Title__DeletePresetName)");
            String format = String.format(c2, Arrays.copyOf(new Object[]{str}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            fVar.e(2131363410, format);
            fVar.e(2131363317, string);
            fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886546));
            fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886545));
            fVar.b(2131363373);
            fVar.b(2131363291);
            fVar.m(getChildFragmentManager(), "DIALOG_DELETE_PRESET", bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.br4.c
    public void r4(m66 m66, List<? extends ln0<View, String>> list, List<? extends ln0<CustomizeWidget, String>> list2, String str, int i2) {
        pq7.c(list, "views");
        pq7.c(list2, "customizeWidgetViews");
        pq7.c(str, "microAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onPresetWatchClick preset=");
        sb.append(m66 != null ? m66.a() : null);
        sb.append(" microAppPos=");
        sb.append(str);
        sb.append(" position=");
        sb.append(i2);
        local.d("HomeHybridCustomizeFragment", sb.toString());
        if (m66 != null) {
            HybridCustomizeEditActivity.a aVar = HybridCustomizeEditActivity.D;
            FragmentActivity requireActivity = requireActivity();
            pq7.b(requireActivity, "requireActivity()");
            aVar.b(requireActivity, m66.c(), new ArrayList<>(list), list2, str);
        }
    }

    @DexIgnore
    @Override // com.fossil.lb6
    public void u() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            pq7.b(activity, "it");
            TroubleshootingActivity.a.c(aVar, activity, PortfolioApp.h0.c().J(), false, false, 12, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.w;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.lb6
    public void w() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.lb6
    public void y() {
        String string = getString(2131886814);
        pq7.b(string, "getString(R.string.Desig\u2026on_Text__ApplyingToWatch)");
        H6(string);
    }
}
