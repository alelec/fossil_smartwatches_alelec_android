package com.fossil;

import android.graphics.drawable.ColorDrawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w51 implements u51 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ r51 f3884a; // = new r51(new ColorDrawable(), false);
    @DexIgnore
    public static /* final */ a58 b; // = s48.b();
    @DexIgnore
    public static /* final */ w51 c; // = new w51();

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        com.fossil.so7.a(r4, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0016, code lost:
        throw r1;
     */
    @DexIgnore
    @Override // com.fossil.u51
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.g51 r3, com.fossil.k48 r4, com.fossil.f81 r5, com.fossil.x51 r6, com.fossil.qn7<? super com.fossil.r51> r7) {
        /*
            r2 = this;
            com.fossil.a58 r0 = com.fossil.w51.b     // Catch:{ all -> 0x0010 }
            long r0 = r4.e0(r0)     // Catch:{ all -> 0x0010 }
            com.fossil.ao7.f(r0)     // Catch:{ all -> 0x0010 }
            r0 = 0
            com.fossil.so7.a(r4, r0)
            com.fossil.r51 r0 = com.fossil.w51.f3884a
            return r0
        L_0x0010:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0012 }
        L_0x0012:
            r1 = move-exception
            com.fossil.so7.a(r4, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.w51.a(com.fossil.g51, com.fossil.k48, com.fossil.f81, com.fossil.x51, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.u51
    public boolean b(k48 k48, String str) {
        pq7.c(k48, "source");
        return false;
    }
}
