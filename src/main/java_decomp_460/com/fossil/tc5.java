package com.fossil;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tc5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FrameLayout s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ ProgressBar u;

    @DexIgnore
    public tc5(Object obj, View view, int i, FlexibleButton flexibleButton, ConstraintLayout constraintLayout, FrameLayout frameLayout, FlexibleTextView flexibleTextView, ProgressBar progressBar) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = constraintLayout;
        this.s = frameLayout;
        this.t = flexibleTextView;
        this.u = progressBar;
    }
}
