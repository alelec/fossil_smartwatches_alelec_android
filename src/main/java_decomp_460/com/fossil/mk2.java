package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mk2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ jk2 b;
    @DexIgnore
    public /* final */ /* synthetic */ lk2 c;

    @DexIgnore
    public mk2(lk2 lk2, jk2 jk2) {
        this.c = lk2;
        this.b = jk2;
    }

    @DexIgnore
    public final void run() {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "bg processing of the intent starting now");
        }
        this.c.b.handleIntent(this.b.f1770a);
        this.b.a();
    }
}
