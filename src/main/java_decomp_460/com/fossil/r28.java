package com.fossil;

import com.facebook.stetho.websocket.WebSocketHandler;
import java.io.IOException;
import java.net.ProtocolException;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r28 implements Interceptor {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ boolean f3075a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends m48 {
        @DexIgnore
        public long c;

        @DexIgnore
        public a(a58 a58) {
            super(a58);
        }

        @DexIgnore
        @Override // com.fossil.m48, com.fossil.a58
        public void K(i48 i48, long j) throws IOException {
            super.K(i48, j);
            this.c += j;
        }
    }

    @DexIgnore
    public r28(boolean z) {
        this.f3075a = z;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response.a aVar;
        Response c;
        Response.a aVar2 = null;
        w28 w28 = (w28) chain;
        s28 i = w28.i();
        p28 k = w28.k();
        l28 l28 = (l28) w28.e();
        v18 c2 = w28.c();
        long currentTimeMillis = System.currentTimeMillis();
        w28.h().o(w28.g());
        i.b(c2);
        w28.h().n(w28.g(), c2);
        if (!v28.b(c2.g()) || c2.a() == null) {
            aVar = null;
        } else {
            if ("100-continue".equalsIgnoreCase(c2.c("Expect"))) {
                i.e();
                w28.h().s(w28.g());
                aVar2 = i.d(true);
            }
            if (aVar2 == null) {
                w28.h().m(w28.g());
                a aVar3 = new a(i.f(c2, c2.a().a()));
                j48 c3 = s48.c(aVar3);
                c2.a().h(c3);
                c3.close();
                w28.h().l(w28.g(), aVar3.c);
                aVar = aVar2;
            } else if (!l28.o()) {
                k.j();
                aVar = aVar2;
            } else {
                aVar = aVar2;
            }
        }
        i.a();
        if (aVar == null) {
            w28.h().s(w28.g());
            aVar = i.d(false);
        }
        aVar.p(c2);
        aVar.h(k.d().l());
        aVar.q(currentTimeMillis);
        aVar.o(System.currentTimeMillis());
        Response c4 = aVar.c();
        int f = c4.f();
        if (f == 100) {
            Response.a d = i.d(false);
            d.p(c2);
            d.h(k.d().l());
            d.q(currentTimeMillis);
            d.o(System.currentTimeMillis());
            c4 = d.c();
            f = c4.f();
        }
        w28.h().r(w28.g(), c4);
        if (!this.f3075a || f != 101) {
            Response.a B = c4.B();
            B.b(i.c(c4));
            c = B.c();
        } else {
            Response.a B2 = c4.B();
            B2.b(b28.c);
            c = B2.c();
        }
        if ("close".equalsIgnoreCase(c.G().c(WebSocketHandler.HEADER_CONNECTION)) || "close".equalsIgnoreCase(c.j(WebSocketHandler.HEADER_CONNECTION))) {
            k.j();
        }
        if ((f != 204 && f != 205) || c.a().contentLength() <= 0) {
            return c;
        }
        throw new ProtocolException("HTTP " + f + " had non-zero Content-Length: " + c.a().contentLength());
    }
}
