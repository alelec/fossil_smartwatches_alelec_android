package com.fossil;

import android.os.Bundle;
import com.facebook.internal.NativeProtocol;
import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.fg3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s64 implements fg3.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ t64 f3209a;

    @DexIgnore
    public s64(t64 t64) {
        this.f3209a = t64;
    }

    @DexIgnore
    @Override // com.fossil.sn3
    public final void a(String str, String str2, Bundle bundle, long j) {
        if (str != null && !str.equals(CrashDumperPlugin.NAME) && o64.f(str2)) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("name", str2);
            bundle2.putLong("timestampInMillis", j);
            bundle2.putBundle(NativeProtocol.WEB_DIALOG_PARAMS, bundle);
            this.f3209a.f3372a.a(3, bundle2);
        }
    }
}
