package com.fossil;

import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class lj5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2205a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[FossilDeviceSerialPatternUtil.DEVICE.values().length];
        f2205a = iArr;
        iArr[FossilDeviceSerialPatternUtil.DEVICE.SAM.ordinal()] = 1;
        f2205a[FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI.ordinal()] = 2;
        f2205a[FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM.ordinal()] = 3;
        f2205a[FossilDeviceSerialPatternUtil.DEVICE.DIANA.ordinal()] = 4;
        f2205a[FossilDeviceSerialPatternUtil.DEVICE.IVY.ordinal()] = 5;
        int[] iArr2 = new int[FossilDeviceSerialPatternUtil.DEVICE.values().length];
        b = iArr2;
        iArr2[FossilDeviceSerialPatternUtil.DEVICE.SAM.ordinal()] = 1;
        b[FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI.ordinal()] = 2;
        b[FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM.ordinal()] = 3;
        b[FossilDeviceSerialPatternUtil.DEVICE.DIANA.ordinal()] = 4;
        b[FossilDeviceSerialPatternUtil.DEVICE.IVY.ordinal()] = 5;
    }
    */
}
