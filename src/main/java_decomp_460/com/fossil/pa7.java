package com.fossil;

import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.watchface.faces.WatchFaceListViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pa7 implements Factory<WatchFaceListViewModel> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<DianaWatchFaceRepository> f2810a;
    @DexIgnore
    public /* final */ Provider<FileRepository> b;
    @DexIgnore
    public /* final */ Provider<uo5> c;
    @DexIgnore
    public /* final */ Provider<wb7> d;

    @DexIgnore
    public pa7(Provider<DianaWatchFaceRepository> provider, Provider<FileRepository> provider2, Provider<uo5> provider3, Provider<wb7> provider4) {
        this.f2810a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static pa7 a(Provider<DianaWatchFaceRepository> provider, Provider<FileRepository> provider2, Provider<uo5> provider3, Provider<wb7> provider4) {
        return new pa7(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static WatchFaceListViewModel c(DianaWatchFaceRepository dianaWatchFaceRepository, FileRepository fileRepository, uo5 uo5, wb7 wb7) {
        return new WatchFaceListViewModel(dianaWatchFaceRepository, fileRepository, uo5, wb7);
    }

    @DexIgnore
    /* renamed from: b */
    public WatchFaceListViewModel get() {
        return c(this.f2810a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
