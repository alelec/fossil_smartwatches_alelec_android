package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pe3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ rs2 f2821a;

    @DexIgnore
    public pe3(rs2 rs2) {
        rc2.k(rs2);
        this.f2821a = rs2;
    }

    @DexIgnore
    public final String a() {
        try {
            return this.f2821a.getId();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void b() {
        try {
            this.f2821a.remove();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void c(boolean z) {
        try {
            this.f2821a.b(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void d(int i) {
        try {
            this.f2821a.setColor(i);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void e(ce3 ce3) {
        rc2.l(ce3, "endCap must not be null");
        try {
            this.f2821a.setEndCap(ce3);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof pe3)) {
            return false;
        }
        try {
            return this.f2821a.B0(((pe3) obj).f2821a);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void f(boolean z) {
        try {
            this.f2821a.setGeodesic(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void g(int i) {
        try {
            this.f2821a.setJointType(i);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void h(List<me3> list) {
        try {
            this.f2821a.setPattern(list);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return this.f2821a.a();
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void i(List<LatLng> list) {
        try {
            this.f2821a.setPoints(list);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void j(ce3 ce3) {
        rc2.l(ce3, "startCap must not be null");
        try {
            this.f2821a.setStartCap(ce3);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void k(boolean z) {
        try {
            this.f2821a.setVisible(z);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void l(float f) {
        try {
            this.f2821a.setWidth(f);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public final void m(float f) {
        try {
            this.f2821a.setZIndex(f);
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }
}
