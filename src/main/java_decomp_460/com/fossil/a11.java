package com.fossil;

import android.annotation.SuppressLint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface a11 {
    @SuppressLint({"SyntheticAccessor"})

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static final b.c f178a = new b.c();
    @DexIgnore
    @SuppressLint({"SyntheticAccessor"})
    public static final b.C0006b b = new b.C0006b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends b {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ Throwable f179a;

            @DexIgnore
            public a(Throwable th) {
                this.f179a = th;
            }

            @DexIgnore
            public Throwable a() {
                return this.f179a;
            }

            @DexIgnore
            public String toString() {
                return String.format("FAILURE (%s)", this.f179a.getMessage());
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.a11$b$b")
        /* renamed from: com.fossil.a11$b$b  reason: collision with other inner class name */
        public static final class C0006b extends b {
            @DexIgnore
            public C0006b() {
            }

            @DexIgnore
            public String toString() {
                return "IN_PROGRESS";
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c extends b {
            @DexIgnore
            public c() {
            }

            @DexIgnore
            public String toString() {
                return "SUCCESS";
            }
        }
    }
}
