package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.er4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.Ringtone;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l86 extends pv5 implements k86, er4.a {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public g37<e15> g;
    @DexIgnore
    public er4 h;
    @DexIgnore
    public j86 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return l86.k;
        }

        @DexIgnore
        public final l86 b() {
            return new l86();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ l86 b;

        @DexIgnore
        public b(l86 l86) {
            this.b = l86;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    /*
    static {
        String simpleName = l86.class.getSimpleName();
        pq7.b(simpleName, "SearchRingPhoneFragment::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return k;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return false;
        }
        j86 j86 = this.i;
        if (j86 != null) {
            j86.p();
            Intent intent = new Intent();
            j86 j862 = this.i;
            if (j862 != null) {
                intent.putExtra("KEY_SELECTED_RINGPHONE", j862.n());
                activity.setResult(-1, intent);
                activity.finish();
                return false;
            }
            pq7.n("mPresenter");
            throw null;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.k86
    public void J3(List<Ringtone> list) {
        pq7.c(list, "data");
        er4 er4 = this.h;
        if (er4 != null) {
            j86 j86 = this.i;
            if (j86 != null) {
                er4.k(list, j86.n());
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        } else {
            pq7.n("mSearchRingPhoneAdapter");
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: L6 */
    public void M5(j86 j86) {
        pq7.c(j86, "presenter");
        this.i = j86;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        g37<e15> g37 = new g37<>(this, (e15) aq0.f(layoutInflater, 2131558434, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            e15 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        j86 j86 = this.i;
        if (j86 != null) {
            j86.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        j86 j86 = this.i;
        if (j86 != null) {
            j86.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<e15> g37 = this.g;
        if (g37 != null) {
            e15 a2 = g37.a();
            if (a2 != null) {
                this.h = new er4(this);
                RecyclerView recyclerView = a2.s;
                pq7.b(recyclerView, "it.rvRingphones");
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView2 = a2.s;
                pq7.b(recyclerView2, "it.rvRingphones");
                er4 er4 = this.h;
                if (er4 != null) {
                    recyclerView2.setAdapter(er4);
                    a2.q.setOnClickListener(new b(this));
                    return;
                }
                pq7.n("mSearchRingPhoneAdapter");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.er4.a
    public void p2(Ringtone ringtone) {
        pq7.c(ringtone, Constants.RINGTONE);
        j86 j86 = this.i;
        if (j86 != null) {
            j86.o(ringtone);
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
