package com.fossil;

import android.content.ComponentName;
import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eq3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ aq3 b;

    @DexIgnore
    public eq3(aq3 aq3) {
        this.b = aq3;
    }

    @DexIgnore
    public final void run() {
        fp3 fp3 = this.b.c;
        Context e = this.b.c.e();
        this.b.c.b();
        fp3.E(new ComponentName(e, "com.google.android.gms.measurement.AppMeasurementService"));
    }
}
