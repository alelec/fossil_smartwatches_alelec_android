package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class am4 implements Cloneable {
    @DexIgnore
    public int[] b;
    @DexIgnore
    public int c;

    @DexIgnore
    public am4() {
        this.c = 0;
        this.b = new int[1];
    }

    @DexIgnore
    public am4(int[] iArr, int i) {
        this.b = iArr;
        this.c = i;
    }

    @DexIgnore
    public static int[] p(int i) {
        return new int[((i + 31) / 32)];
    }

    @DexIgnore
    public void d(boolean z) {
        j(this.c + 1);
        if (z) {
            int[] iArr = this.b;
            int i = this.c;
            int i2 = i / 32;
            iArr[i2] = (1 << (i & 31)) | iArr[i2];
        }
        this.c++;
    }

    @DexIgnore
    public void e(am4 am4) {
        int i = am4.c;
        j(this.c + i);
        for (int i2 = 0; i2 < i; i2++) {
            d(am4.l(i2));
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof am4)) {
            return false;
        }
        am4 am4 = (am4) obj;
        return this.c == am4.c && Arrays.equals(this.b, am4.b);
    }

    @DexIgnore
    public void g(int i, int i2) {
        if (i2 < 0 || i2 > 32) {
            throw new IllegalArgumentException("Num bits must be between 0 and 32");
        }
        j(this.c + i2);
        while (i2 > 0) {
            d(((i >> (i2 + -1)) & 1) == 1);
            i2--;
        }
    }

    @DexIgnore
    public int hashCode() {
        return (this.c * 31) + Arrays.hashCode(this.b);
    }

    @DexIgnore
    /* renamed from: i */
    public am4 clone() {
        return new am4((int[]) this.b.clone(), this.c);
    }

    @DexIgnore
    public final void j(int i) {
        if (i > (this.b.length << 5)) {
            int[] p = p(i);
            int[] iArr = this.b;
            System.arraycopy(iArr, 0, p, 0, iArr.length);
            this.b = p;
        }
    }

    @DexIgnore
    public boolean l(int i) {
        return ((1 << (i & 31)) & this.b[i / 32]) != 0;
    }

    @DexIgnore
    public int n() {
        return this.c;
    }

    @DexIgnore
    public int o() {
        return (this.c + 7) / 8;
    }

    @DexIgnore
    public void q(int i, byte[] bArr, int i2, int i3) {
        int i4 = i;
        for (int i5 = 0; i5 < i3; i5++) {
            int i6 = 0;
            for (int i7 = 0; i7 < 8; i7++) {
                if (l(i4)) {
                    i6 |= 1 << (7 - i7);
                }
                i4++;
            }
            bArr[i2 + i5] = (byte) ((byte) i6);
        }
    }

    @DexIgnore
    public void r(am4 am4) {
        if (this.c == am4.c) {
            int i = 0;
            while (true) {
                int[] iArr = this.b;
                if (i < iArr.length) {
                    iArr[i] = iArr[i] ^ am4.b[i];
                    i++;
                } else {
                    return;
                }
            }
        } else {
            throw new IllegalArgumentException("Sizes don't match");
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(this.c);
        for (int i = 0; i < this.c; i++) {
            if ((i & 7) == 0) {
                sb.append(' ');
            }
            sb.append(l(i) ? 'X' : '.');
        }
        return sb.toString();
    }
}
