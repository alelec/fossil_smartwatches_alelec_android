package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.receiver.AlarmReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tp5 implements MembersInjector<AlarmReceiver> {
    @DexIgnore
    public static void a(AlarmReceiver alarmReceiver, bk5 bk5) {
        alarmReceiver.d = bk5;
    }

    @DexIgnore
    public static void b(AlarmReceiver alarmReceiver, AlarmsRepository alarmsRepository) {
        alarmReceiver.e = alarmsRepository;
    }

    @DexIgnore
    public static void c(AlarmReceiver alarmReceiver, DeviceRepository deviceRepository) {
        alarmReceiver.c = deviceRepository;
    }

    @DexIgnore
    public static void d(AlarmReceiver alarmReceiver, on5 on5) {
        alarmReceiver.b = on5;
    }

    @DexIgnore
    public static void e(AlarmReceiver alarmReceiver, UserRepository userRepository) {
        alarmReceiver.f4697a = userRepository;
    }
}
