package com.fossil;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.internal.ServerProtocol;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class un3 extends lm3 {
    @DexIgnore
    public qo3 c;
    @DexIgnore
    public pn3 d;
    @DexIgnore
    public /* final */ Set<sn3> e; // = new CopyOnWriteArraySet();
    @DexIgnore
    public boolean f;
    @DexIgnore
    public /* final */ AtomicReference<String> g; // = new AtomicReference<>();
    @DexIgnore
    public /* final */ qr3 h;
    @DexIgnore
    public boolean i; // = true;

    @DexIgnore
    public un3(pm3 pm3) {
        super(pm3);
        this.h = new qr3(pm3);
    }

    @DexIgnore
    @Override // com.fossil.lm3
    public final boolean A() {
        return false;
    }

    @DexIgnore
    public final ArrayList<Bundle> B(String str, String str2) {
        f();
        return j0(null, str, str2);
    }

    @DexIgnore
    public final ArrayList<Bundle> C(String str, String str2, String str3) {
        rc2.g(str);
        n();
        throw null;
    }

    @DexIgnore
    public final Map<String, Object> D(String str, String str2, String str3, boolean z) {
        rc2.g(str);
        n();
        throw null;
    }

    @DexIgnore
    public final Map<String, Object> E(String str, String str2, boolean z) {
        f();
        return k0(null, str, str2, z);
    }

    @DexIgnore
    public final void F(Bundle bundle) {
        G(bundle, zzm().b());
    }

    @DexIgnore
    public final void G(Bundle bundle, long j) {
        rc2.k(bundle);
        f();
        Bundle bundle2 = new Bundle(bundle);
        if (!TextUtils.isEmpty(bundle2.getString("app_id"))) {
            d().I().a("Package name should be null when calling setConditionalUserProperty");
        }
        bundle2.remove("app_id");
        m0(bundle2, j);
    }

    @DexIgnore
    public final void H(pn3 pn3) {
        pn3 pn32;
        h();
        f();
        x();
        if (!(pn3 == null || pn3 == (pn32 = this.d))) {
            rc2.o(pn32 == null, "EventInterceptor already set.");
        }
        this.d = pn3;
    }

    @DexIgnore
    public final void I(sn3 sn3) {
        f();
        x();
        rc2.k(sn3);
        if (!this.e.add(sn3)) {
            d().I().a("OnEventListener already registered");
        }
    }

    @DexIgnore
    public final void M(String str) {
        this.g.set(str);
    }

    @DexIgnore
    public final void N(String str, String str2, long j, Bundle bundle) {
        f();
        h();
        O(str, str2, j, bundle, true, this.d == null || kr3.B0(str2), false, null);
    }

    @DexIgnore
    public final void O(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        String str4;
        String str5;
        String str6;
        xo3 xo3;
        Bundle bundle2;
        int i2;
        String str7;
        boolean z4;
        List<String> I;
        rc2.g(str);
        rc2.k(bundle);
        h();
        x();
        if (!this.f1780a.o()) {
            d().M().a("Event not sent since app measurement is disabled");
        } else if (!m().s(xg3.c0) || (I = q().I()) == null || I.contains(str2)) {
            if (!this.f) {
                this.f = true;
                try {
                    try {
                        (!this.f1780a.M() ? Class.forName("com.google.android.gms.tagmanager.TagManagerService", true, e().getClassLoader()) : Class.forName("com.google.android.gms.tagmanager.TagManagerService")).getDeclaredMethod("initialize", Context.class).invoke(null, e());
                    } catch (Exception e2) {
                        d().I().b("Failed to invoke Tag Manager's initialize() method", e2);
                    }
                } catch (ClassNotFoundException e3) {
                    d().L().a("Tag Manager is not found and thus will not be used");
                }
            }
            if (m().s(xg3.i0) && "_cmp".equals(str2) && bundle.containsKey("gclid")) {
                S("auto", "_lgclid", bundle.getString("gclid"), zzm().b());
            }
            if (j73.a() && m().s(xg3.N0)) {
                b();
                if (z && kr3.E0(str2)) {
                    k().L(bundle, l().C.a());
                }
            }
            if (z3) {
                b();
                if (!"_iap".equals(str2)) {
                    kr3 F = this.f1780a.F();
                    int i3 = !F.f0(Constants.EVENT, str2) ? 2 : !F.k0(Constants.EVENT, on3.f2699a, str2) ? 13 : !F.e0(Constants.EVENT, 40, str2) ? 2 : 0;
                    if (i3 != 0) {
                        d().H().b("Invalid public event name. Event will not be logged (FE)", j().v(str2));
                        this.f1780a.F();
                        this.f1780a.F().I(i3, "_ev", kr3.G(str2, 40, true), str2 != null ? str2.length() : 0);
                        return;
                    }
                }
            }
            b();
            xo3 D = s().D(false);
            if (D != null && !bundle.containsKey("_sc")) {
                D.d = true;
            }
            ap3.L(D, bundle, z && z3);
            boolean equals = "am".equals(str);
            boolean B0 = kr3.B0(str2);
            if (z && this.d != null && !B0 && !equals) {
                d().M().c("Passing event to registered event handler (FE)", j().v(str2), j().t(bundle));
                this.d.a(str, str2, bundle, j);
            } else if (this.f1780a.t()) {
                int n0 = k().n0(str2);
                if (n0 != 0) {
                    d().H().b("Invalid event name. Event will not be logged (FE)", j().v(str2));
                    k();
                    this.f1780a.F().V(str3, n0, "_ev", kr3.G(str2, 40, true), str2 != null ? str2.length() : 0);
                    return;
                }
                List<String> d2 = ff2.d("_o", "_sn", "_sc", "_si");
                Bundle B = k().B(str3, str2, bundle, d2, z3, true);
                xo3 xo32 = (B == null || !B.containsKey("_sc") || !B.containsKey("_si")) ? null : new xo3(B.getString("_sn"), B.getString("_sc"), Long.valueOf(B.getLong("_si")).longValue());
                xo3 xo33 = xo32 == null ? D : xo32;
                if (m().s(xg3.U)) {
                    b();
                    if (s().D(false) != null && "_ae".equals(str2)) {
                        long e4 = u().e.e();
                        if (e4 > 0) {
                            k().K(B, e4);
                        }
                    }
                }
                if (z53.a() && m().s(xg3.v0)) {
                    if (!"auto".equals(str) && "_ssr".equals(str2)) {
                        kr3 k = k();
                        String string = B.getString("_ffr");
                        String trim = of2.a(string) ? null : string.trim();
                        if (kr3.z0(trim, k.l().z.a())) {
                            k.d().M().a("Not logging duplicate session_start_with_rollout event");
                            z4 = false;
                        } else {
                            k.l().z.b(trim);
                            z4 = true;
                        }
                        if (!z4) {
                            return;
                        }
                    } else if ("_ae".equals(str2)) {
                        String a2 = k().l().z.a();
                        if (!TextUtils.isEmpty(a2)) {
                            B.putString("_ffr", a2);
                        }
                    }
                }
                ArrayList arrayList = new ArrayList();
                arrayList.add(B);
                long nextLong = k().G0().nextLong();
                if (l().u.a() > 0 && l().v(j) && l().w.b()) {
                    d().N().a("Current session is expired, remove the session number, ID, and engagement time");
                    S("auto", "_sid", null, zzm().b());
                    S("auto", "_sno", null, zzm().b());
                    S("auto", "_se", null, zzm().b());
                }
                if (B.getLong("extend_session", 0) == 1) {
                    d().N().a("EXTEND_SESSION param attached: initiate a new session or extend the current active session");
                    this.f1780a.B().d.b(j, true);
                }
                String[] strArr = (String[]) B.keySet().toArray(new String[B.size()]);
                Arrays.sort(strArr);
                if (!s53.a() || !m().s(xg3.H0) || !m().s(xg3.G0)) {
                    int length = strArr.length;
                    int i4 = 0;
                    Bundle bundle3 = B;
                    xo3 xo34 = xo33;
                    int i5 = 0;
                    String str8 = str2;
                    str4 = "_ae";
                    while (i5 < length) {
                        String str9 = strArr[i5];
                        Object obj = bundle3.get(str9);
                        k();
                        Bundle[] v0 = kr3.v0(obj);
                        if (v0 != null) {
                            bundle3.putInt(str9, v0.length);
                            int i6 = 0;
                            str7 = str8;
                            while (i6 < v0.length) {
                                Bundle bundle4 = v0[i6];
                                ap3.L(xo34, bundle4, true);
                                Bundle B2 = k().B(str3, "_ep", bundle4, d2, z3, false);
                                B2.putString("_en", str2);
                                B2.putLong("_eid", nextLong);
                                B2.putString("_gn", str9);
                                B2.putInt("_ll", v0.length);
                                B2.putInt("_i", i6);
                                arrayList.add(B2);
                                i6++;
                                str7 = str2;
                            }
                            str6 = str4;
                            xo3 = xo34;
                            bundle2 = bundle3;
                            i2 = i4 + v0.length;
                        } else {
                            str6 = str4;
                            xo3 = xo34;
                            bundle2 = bundle3;
                            i2 = i4;
                            str7 = str8;
                        }
                        i5++;
                        arrayList = arrayList;
                        bundle3 = bundle2;
                        xo34 = xo3;
                        i4 = i2;
                        str8 = str7;
                        str4 = str6;
                    }
                    if (i4 != 0) {
                        bundle3.putLong("_eid", nextLong);
                        bundle3.putInt("_epc", i4);
                        str5 = str8;
                    } else {
                        str5 = str8;
                    }
                } else {
                    for (String str10 : strArr) {
                        k();
                        Bundle[] v02 = kr3.v0(B.get(str10));
                        if (v02 != null) {
                            B.putParcelableArray(str10, v02);
                        }
                    }
                    str4 = "_ae";
                    str5 = str2;
                }
                int i7 = 0;
                while (i7 < arrayList.size()) {
                    Bundle bundle5 = (Bundle) arrayList.get(i7);
                    String str11 = i7 != 0 ? "_ep" : str5;
                    bundle5.putString("_o", str);
                    Bundle A = z2 ? k().A(bundle5) : bundle5;
                    r().K(new vg3(str11, new ug3(A), str, j), str3);
                    if (!equals) {
                        for (sn3 sn3 : this.e) {
                            sn3.a(str, str2, new Bundle(A), j);
                        }
                    }
                    i7++;
                }
                b();
                if (s().D(false) != null && str4.equals(str5)) {
                    u().E(true, true, zzm().c());
                }
            }
        } else {
            d().M().c("Dropping non-safelisted event. event name, origin", str2, str);
        }
    }

    @DexIgnore
    public final void P(String str, String str2, long j, Object obj) {
        c().y(new zn3(this, str, str2, obj, j));
    }

    @DexIgnore
    public final void Q(String str, String str2, Bundle bundle) {
        R(str, str2, bundle, true, true, zzm().b());
    }

    @DexIgnore
    public final void R(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) {
        f();
        String str3 = str == null ? "app" : str;
        Bundle bundle2 = bundle == null ? new Bundle() : bundle;
        if (!m().s(xg3.D0) || !kr3.z0(str2, "screen_view")) {
            p0(str3, str2, j, bundle2, z2, !z2 || this.d == null || kr3.B0(str2), !z, null);
        } else {
            s().J(bundle2, j);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void S(java.lang.String r9, java.lang.String r10, java.lang.Object r11, long r12) {
        /*
            r8 = this;
            r2 = 1
            com.fossil.rc2.g(r9)
            com.fossil.rc2.g(r10)
            r8.h()
            r8.f()
            r8.x()
            java.lang.String r0 = "allow_personalized_ads"
            boolean r0 = r0.equals(r10)
            if (r0 == 0) goto L_0x0078
            boolean r0 = r11 instanceof java.lang.String
            if (r0 == 0) goto L_0x006a
            r0 = r11
            java.lang.String r0 = (java.lang.String) r0
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x006a
            java.util.Locale r1 = java.util.Locale.ENGLISH
            java.lang.String r0 = r0.toLowerCase(r1)
            java.lang.String r4 = "false"
            java.lang.String r1 = "false"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0067
            r0 = r2
        L_0x0037:
            java.lang.Long r11 = java.lang.Long.valueOf(r0)
            com.fossil.xl3 r0 = r8.l()
            com.fossil.dm3 r1 = r0.s
            long r6 = r11.longValue()
            int r0 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x0092
            java.lang.String r0 = "true"
        L_0x004b:
            r1.b(r0)
        L_0x004e:
            java.lang.String r1 = "_npa"
            r4 = r11
        L_0x0051:
            com.fossil.pm3 r0 = r8.f1780a
            boolean r0 = r0.o()
            if (r0 != 0) goto L_0x007b
            com.fossil.kl3 r0 = r8.d()
            com.fossil.nl3 r0 = r0.N()
            java.lang.String r1 = "User property not set since app measurement is disabled"
            r0.a(r1)
        L_0x0066:
            return
        L_0x0067:
            r0 = 0
            goto L_0x0037
        L_0x006a:
            if (r11 != 0) goto L_0x0078
            com.fossil.xl3 r0 = r8.l()
            com.fossil.dm3 r0 = r0.s
            java.lang.String r1 = "unset"
            r0.b(r1)
            goto L_0x004e
        L_0x0078:
            r1 = r10
            r4 = r11
            goto L_0x0051
        L_0x007b:
            com.fossil.pm3 r0 = r8.f1780a
            boolean r0 = r0.t()
            if (r0 == 0) goto L_0x0066
            com.fossil.fr3 r0 = new com.fossil.fr3
            r2 = r12
            r5 = r9
            r0.<init>(r1, r2, r4, r5)
            com.fossil.fp3 r1 = r8.r()
            r1.P(r0)
            goto L_0x0066
        L_0x0092:
            r0 = r4
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.un3.S(java.lang.String, java.lang.String, java.lang.Object, long):void");
    }

    @DexIgnore
    public final void T(String str, String str2, Object obj, boolean z) {
        U(str, str2, obj, true, zzm().b());
    }

    @DexIgnore
    public final void U(String str, String str2, Object obj, boolean z, long j) {
        int i2 = 0;
        String str3 = str == null ? "app" : str;
        int i3 = 6;
        if (z) {
            i3 = k().w0(str2);
        } else {
            kr3 k = k();
            if (k.f0("user property", str2)) {
                if (!k.k0("user property", qn3.f2999a, str2)) {
                    i3 = 15;
                } else if (k.e0("user property", 24, str2)) {
                    i3 = 0;
                }
            }
        }
        if (i3 != 0) {
            k();
            String G = kr3.G(str2, 24, true);
            if (str2 != null) {
                i2 = str2.length();
            }
            this.f1780a.F().I(i3, "_ev", G, i2);
        } else if (obj != null) {
            int o0 = k().o0(str2, obj);
            if (o0 != 0) {
                k();
                String G2 = kr3.G(str2, 24, true);
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    i2 = String.valueOf(obj).length();
                }
                this.f1780a.F().I(o0, "_ev", G2, i2);
                return;
            }
            Object x0 = k().x0(str2, obj);
            if (x0 != null) {
                P(str3, str2, j, x0);
            }
        } else {
            P(str3, str2, j, null);
        }
    }

    @DexIgnore
    public final void V(String str, String str2, String str3, Bundle bundle) {
        rc2.g(str);
        n();
        throw null;
    }

    @DexIgnore
    public final void W(boolean z) {
        x();
        f();
        c().y(new lo3(this, z));
    }

    @DexIgnore
    public final void X() {
        if (e().getApplicationContext() instanceof Application) {
            ((Application) e().getApplicationContext()).unregisterActivityLifecycleCallbacks(this.c);
        }
    }

    @DexIgnore
    public final Boolean Y() {
        AtomicReference atomicReference = new AtomicReference();
        return (Boolean) c().u(atomicReference, 15000, "boolean test flag value", new vn3(this, atomicReference));
    }

    @DexIgnore
    public final String Z() {
        AtomicReference atomicReference = new AtomicReference();
        return (String) c().u(atomicReference, 15000, "String test flag value", new fo3(this, atomicReference));
    }

    @DexIgnore
    public final Long a0() {
        AtomicReference atomicReference = new AtomicReference();
        return (Long) c().u(atomicReference, 15000, "long test flag value", new go3(this, atomicReference));
    }

    @DexIgnore
    public final Integer b0() {
        AtomicReference atomicReference = new AtomicReference();
        return (Integer) c().u(atomicReference, 15000, "int test flag value", new jo3(this, atomicReference));
    }

    @DexIgnore
    public final Double c0() {
        AtomicReference atomicReference = new AtomicReference();
        return (Double) c().u(atomicReference, 15000, "double test flag value", new io3(this, atomicReference));
    }

    @DexIgnore
    public final String d0() {
        f();
        return this.g.get();
    }

    @DexIgnore
    public final void e0() {
        h();
        f();
        x();
        if (this.f1780a.t()) {
            if (m().s(xg3.h0)) {
                zr3 m = m();
                m.b();
                Boolean A = m.A("google_analytics_deferred_deep_link_enabled");
                if (A != null && A.booleanValue()) {
                    d().M().a("Deferred Deep Link feature enabled.");
                    c().y(new wn3(this));
                }
            }
            r().Y();
            this.i = false;
            String H = l().H();
            if (!TextUtils.isEmpty(H)) {
                i().o();
                if (!H.equals(Build.VERSION.RELEASE)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("_po", H);
                    Q("auto", "_ou", bundle);
                }
            }
        }
    }

    @DexIgnore
    public final String f0() {
        xo3 S = this.f1780a.N().S();
        if (S != null) {
            return S.f4149a;
        }
        return null;
    }

    @DexIgnore
    public final String g0() {
        xo3 S = this.f1780a.N().S();
        if (S != null) {
            return S.b;
        }
        return null;
    }

    @DexIgnore
    public final String h0() {
        if (this.f1780a.J() != null) {
            return this.f1780a.J();
        }
        try {
            return yo3.a(e(), "google_app_id");
        } catch (IllegalStateException e2) {
            this.f1780a.d().F().b("getGoogleAppId failed with exception", e2);
            return null;
        }
    }

    @DexIgnore
    public final void i0() {
        h();
        String a2 = l().s.a();
        if (a2 != null) {
            if ("unset".equals(a2)) {
                S("app", "_npa", null, zzm().b());
            } else {
                S("app", "_npa", Long.valueOf(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equals(a2) ? 1 : 0), zzm().b());
            }
        }
        if (!this.f1780a.o() || !this.i) {
            d().M().a("Updating Scion state (FE)");
            r().W();
            return;
        }
        d().M().a("Recording app launch after enabling measurement for the first time (FE)");
        e0();
        if (w63.a() && m().s(xg3.w0)) {
            u().d.a();
        }
        if (k63.a() && m().s(xg3.B0)) {
            if (!(this.f1780a.C().f629a.z().k.a() > 0)) {
                cm3 C = this.f1780a.C();
                C.f629a.q();
                C.b(C.f629a.e().getPackageName());
            }
        }
        if (m().s(xg3.R0)) {
            c().y(new oo3(this));
        }
    }

    @DexIgnore
    public final ArrayList<Bundle> j0(String str, String str2, String str3) {
        if (c().G()) {
            d().F().a("Cannot get conditional user properties from analytics worker thread");
            return new ArrayList<>(0);
        } else if (yr3.a()) {
            d().F().a("Cannot get conditional user properties from main thread");
            return new ArrayList<>(0);
        } else {
            AtomicReference atomicReference = new AtomicReference();
            this.f1780a.c().u(atomicReference, 5000, "get conditional user properties", new eo3(this, atomicReference, str, str2, str3));
            List list = (List) atomicReference.get();
            if (list != null) {
                return kr3.q0(list);
            }
            d().F().b("Timed out waiting for get conditional user properties", str);
            return new ArrayList<>();
        }
    }

    @DexIgnore
    public final Map<String, Object> k0(String str, String str2, String str3, boolean z) {
        if (c().G()) {
            d().F().a("Cannot get user properties from analytics worker thread");
            return Collections.emptyMap();
        } else if (yr3.a()) {
            d().F().a("Cannot get user properties from main thread");
            return Collections.emptyMap();
        } else {
            AtomicReference atomicReference = new AtomicReference();
            this.f1780a.c().u(atomicReference, 5000, "get user properties", new ho3(this, atomicReference, str, str2, str3, z));
            List<fr3> list = (List) atomicReference.get();
            if (list == null) {
                d().F().b("Timed out waiting for handle get user properties, includeInternal", Boolean.valueOf(z));
                return Collections.emptyMap();
            }
            zi0 zi0 = new zi0(list.size());
            for (fr3 fr3 : list) {
                zi0.put(fr3.c, fr3.c());
            }
            return zi0;
        }
    }

    @DexIgnore
    public final void l0(Bundle bundle) {
        rc2.k(bundle);
        rc2.g(bundle.getString("app_id"));
        n();
        throw null;
    }

    @DexIgnore
    public final void m0(Bundle bundle, long j) {
        rc2.k(bundle);
        kn3.a(bundle, "app_id", String.class, null);
        kn3.a(bundle, "origin", String.class, null);
        kn3.a(bundle, "name", String.class, null);
        kn3.a(bundle, "value", Object.class, null);
        kn3.a(bundle, "trigger_event_name", String.class, null);
        kn3.a(bundle, "trigger_timeout", Long.class, 0L);
        kn3.a(bundle, "timed_out_event_name", String.class, null);
        kn3.a(bundle, "timed_out_event_params", Bundle.class, null);
        kn3.a(bundle, "triggered_event_name", String.class, null);
        kn3.a(bundle, "triggered_event_params", Bundle.class, null);
        kn3.a(bundle, "time_to_live", Long.class, 0L);
        kn3.a(bundle, "expired_event_name", String.class, null);
        kn3.a(bundle, "expired_event_params", Bundle.class, null);
        rc2.g(bundle.getString("name"));
        rc2.g(bundle.getString("origin"));
        rc2.k(bundle.get("value"));
        bundle.putLong("creation_timestamp", j);
        String string = bundle.getString("name");
        Object obj = bundle.get("value");
        if (k().w0(string) != 0) {
            d().F().b("Invalid conditional user property name", j().z(string));
        } else if (k().o0(string, obj) != 0) {
            d().F().c("Invalid conditional user property value", j().z(string), obj);
        } else {
            Object x0 = k().x0(string, obj);
            if (x0 == null) {
                d().F().c("Unable to normalize conditional user property value", j().z(string), obj);
                return;
            }
            kn3.b(bundle, x0);
            long j2 = bundle.getLong("trigger_timeout");
            if (TextUtils.isEmpty(bundle.getString("trigger_event_name")) || (j2 <= 15552000000L && j2 >= 1)) {
                long j3 = bundle.getLong("time_to_live");
                if (j3 > 15552000000L || j3 < 1) {
                    d().F().c("Invalid conditional user property time to live", j().z(string), Long.valueOf(j3));
                } else {
                    c().y(new do3(this, bundle));
                }
            } else {
                d().F().c("Invalid conditional user property timeout", j().z(string), Long.valueOf(j2));
            }
        }
    }

    @DexIgnore
    public final void n0(sn3 sn3) {
        f();
        x();
        rc2.k(sn3);
        if (!this.e.remove(sn3)) {
            d().I().a("OnEventListener had not been registered");
        }
    }

    @DexIgnore
    public final void p0(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        c().y(new xn3(this, str, str2, j, kr3.p0(bundle), z, z2, z3, str3));
    }

    @DexIgnore
    public final void q0(String str, String str2, Bundle bundle) {
        f();
        h();
        N(str, str2, zzm().b(), bundle);
    }

    @DexIgnore
    public final void r0(String str, String str2, String str3, Bundle bundle) {
        long b = zzm().b();
        rc2.g(str2);
        Bundle bundle2 = new Bundle();
        if (str != null) {
            bundle2.putString("app_id", str);
        }
        bundle2.putString("name", str2);
        bundle2.putLong("creation_timestamp", b);
        if (str3 != null) {
            bundle2.putString("expired_event_name", str3);
            bundle2.putBundle("expired_event_params", bundle);
        }
        c().y(new co3(this, bundle2));
    }

    @DexIgnore
    public final void s0(Bundle bundle) {
        h();
        x();
        rc2.k(bundle);
        rc2.g(bundle.getString("name"));
        rc2.g(bundle.getString("origin"));
        rc2.k(bundle.get("value"));
        if (!this.f1780a.o()) {
            d().N().a("Conditional property not set since app measurement is disabled");
            return;
        }
        fr3 fr3 = new fr3(bundle.getString("name"), bundle.getLong("triggered_timestamp"), bundle.get("value"), bundle.getString("origin"));
        try {
            vg3 D = k().D(bundle.getString("app_id"), bundle.getString("triggered_event_name"), bundle.getBundle("triggered_event_params"), bundle.getString("origin"), 0, true, false);
            r().Q(new xr3(bundle.getString("app_id"), bundle.getString("origin"), fr3, bundle.getLong("creation_timestamp"), false, bundle.getString("trigger_event_name"), k().D(bundle.getString("app_id"), bundle.getString("timed_out_event_name"), bundle.getBundle("timed_out_event_params"), bundle.getString("origin"), 0, true, false), bundle.getLong("trigger_timeout"), D, bundle.getLong("time_to_live"), k().D(bundle.getString("app_id"), bundle.getString("expired_event_name"), bundle.getBundle("expired_event_params"), bundle.getString("origin"), 0, true, false)));
        } catch (IllegalArgumentException e2) {
        }
    }

    @DexIgnore
    public final void t0(String str, String str2, Bundle bundle) {
        f();
        r0(null, str, str2, bundle);
    }

    @DexIgnore
    public final void u0(boolean z) {
        h();
        f();
        x();
        d().M().b("Setting app measurement enabled (FE)", Boolean.valueOf(z));
        l().x(z);
        i0();
    }

    @DexIgnore
    public final void v0(Bundle bundle) {
        h();
        x();
        rc2.k(bundle);
        rc2.g(bundle.getString("name"));
        if (!this.f1780a.o()) {
            d().N().a("Conditional property not cleared since app measurement is disabled");
            return;
        }
        try {
            r().Q(new xr3(bundle.getString("app_id"), bundle.getString("origin"), new fr3(bundle.getString("name"), 0, null, null), bundle.getLong("creation_timestamp"), bundle.getBoolean("active"), bundle.getString("trigger_event_name"), null, bundle.getLong("trigger_timeout"), null, bundle.getLong("time_to_live"), k().D(bundle.getString("app_id"), bundle.getString("expired_event_name"), bundle.getBundle("expired_event_params"), bundle.getString("origin"), bundle.getLong("creation_timestamp"), true, false)));
        } catch (IllegalArgumentException e2) {
        }
    }
}
