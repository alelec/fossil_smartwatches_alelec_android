package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import com.fossil.ve0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u78 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<u78> CREATOR; // = new a();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public Object i;
    @DexIgnore
    public Context j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<u78> {
        @DexIgnore
        /* renamed from: a */
        public u78 createFromParcel(Parcel parcel) {
            return new u78(parcel, null);
        }

        @DexIgnore
        /* renamed from: b */
        public u78[] newArray(int i) {
            return new u78[i];
        }
    }

    @DexIgnore
    public u78(Parcel parcel) {
        this.b = parcel.readInt();
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = parcel.readString();
        this.g = parcel.readInt();
        this.h = parcel.readInt();
    }

    @DexIgnore
    public /* synthetic */ u78(Parcel parcel, a aVar) {
        this(parcel);
    }

    @DexIgnore
    public static u78 a(Intent intent, Activity activity) {
        u78 u78 = (u78) intent.getParcelableExtra("extra_app_settings");
        u78.c(activity);
        return u78;
    }

    @DexIgnore
    public int b() {
        return this.h;
    }

    @DexIgnore
    public final void c(Object obj) {
        this.i = obj;
        if (obj instanceof Activity) {
            this.j = (Activity) obj;
        } else if (obj instanceof Fragment) {
            this.j = ((Fragment) obj).getContext();
        } else {
            throw new IllegalStateException("Unknown object: " + obj);
        }
    }

    @DexIgnore
    public ve0 d(DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        int i2 = this.b;
        ve0.a aVar = i2 > 0 ? new ve0.a(this.j, i2) : new ve0.a(this.j);
        aVar.d(false);
        aVar.o(this.d);
        aVar.g(this.c);
        aVar.l(this.e, onClickListener);
        aVar.h(this.f, onClickListener2);
        return aVar.q();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeInt(this.g);
        parcel.writeInt(this.h);
    }
}
