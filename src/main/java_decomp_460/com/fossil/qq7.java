package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qq7<R> implements mq7<R>, Serializable {
    @DexIgnore
    public /* final */ int arity;

    @DexIgnore
    public qq7(int i) {
        this.arity = i;
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public int getArity() {
        return this.arity;
    }

    @DexIgnore
    public String toString() {
        String i = er7.i(this);
        pq7.b(i, "Reflection.renderLambdaToString(this)");
        return i;
    }
}
