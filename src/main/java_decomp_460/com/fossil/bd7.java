package com.fossil;

import android.content.Context;
import com.fossil.rd7;
import com.squareup.picasso.Picasso;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bd7 extends rd7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f417a;

    @DexIgnore
    public bd7(Context context) {
        this.f417a = context;
    }

    @DexIgnore
    @Override // com.fossil.rd7
    public boolean c(pd7 pd7) {
        return "content".equals(pd7.d.getScheme());
    }

    @DexIgnore
    @Override // com.fossil.rd7
    public rd7.a f(pd7 pd7, int i) throws IOException {
        return new rd7.a(j(pd7), Picasso.LoadedFrom.DISK);
    }

    @DexIgnore
    public InputStream j(pd7 pd7) throws FileNotFoundException {
        return this.f417a.getContentResolver().openInputStream(pd7.d);
    }
}
