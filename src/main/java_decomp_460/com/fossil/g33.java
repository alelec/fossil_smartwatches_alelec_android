package com.fossil;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g33<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public List<p33> c;
    @DexIgnore
    public Map<K, V> d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public volatile r33 f;
    @DexIgnore
    public Map<K, V> g;
    @DexIgnore
    public volatile l33 h;

    @DexIgnore
    public g33(int i) {
        this.b = i;
        this.c = Collections.emptyList();
        this.d = Collections.emptyMap();
        this.g = Collections.emptyMap();
    }

    @DexIgnore
    public /* synthetic */ g33(int i, j33 j33) {
        this(i);
    }

    @DexIgnore
    public static <FieldDescriptorType extends v03<FieldDescriptorType>> g33<FieldDescriptorType, Object> b(int i) {
        return new j33(i);
    }

    @DexIgnore
    public final int a(K k) {
        int size = this.c.size() - 1;
        if (size >= 0) {
            int compareTo = k.compareTo((Comparable) this.c.get(size).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i = 0;
        int i2 = size;
        while (i <= i2) {
            int i3 = (i + i2) / 2;
            int compareTo2 = k.compareTo((Comparable) this.c.get(i3).getKey());
            if (compareTo2 < 0) {
                i2 = i3 - 1;
            } else if (compareTo2 <= 0) {
                return i3;
            } else {
                i = i3 + 1;
            }
        }
        return -(i + 1);
    }

    @DexIgnore
    public void clear() {
        q();
        if (!this.c.isEmpty()) {
            this.c.clear();
        }
        if (!this.d.isEmpty()) {
            this.d.clear();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.g33<K extends java.lang.Comparable<K>, V> */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return a(comparable) >= 0 || this.d.containsKey(comparable);
    }

    @DexIgnore
    /* renamed from: e */
    public final V put(K k, V v) {
        q();
        int a2 = a(k);
        if (a2 >= 0) {
            return (V) this.c.get(a2).setValue(v);
        }
        q();
        if (this.c.isEmpty() && !(this.c instanceof ArrayList)) {
            this.c = new ArrayList(this.b);
        }
        int i = -(a2 + 1);
        if (i >= this.b) {
            return r().put(k, v);
        }
        int size = this.c.size();
        int i2 = this.b;
        if (size == i2) {
            p33 remove = this.c.remove(i2 - 1);
            r().put((K) ((Comparable) remove.getKey()), (V) remove.getValue());
        }
        this.c.add(i, new p33(this, k, v));
        return null;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        if (this.f == null) {
            this.f = new r33(this, null);
        }
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof g33)) {
            return super.equals(obj);
        }
        g33 g33 = (g33) obj;
        int size = size();
        if (size != g33.size()) {
            return false;
        }
        int k = k();
        if (k != g33.k()) {
            return entrySet().equals(g33.entrySet());
        }
        for (int i = 0; i < k; i++) {
            if (!i(i).equals(g33.i(i))) {
                return false;
            }
        }
        if (k != size) {
            return this.d.equals(g33.d);
        }
        return true;
    }

    @DexIgnore
    public void f() {
        if (!this.e) {
            this.d = this.d.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(this.d);
            this.g = this.g.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(this.g);
            this.e = true;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.g33<K extends java.lang.Comparable<K>, V> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int a2 = a(comparable);
        return a2 >= 0 ? (V) this.c.get(a2).getValue() : this.d.get(comparable);
    }

    @DexIgnore
    public int hashCode() {
        int k = k();
        int i = 0;
        for (int i2 = 0; i2 < k; i2++) {
            i += this.c.get(i2).hashCode();
        }
        return this.d.size() > 0 ? this.d.hashCode() + i : i;
    }

    @DexIgnore
    public final Map.Entry<K, V> i(int i) {
        return this.c.get(i);
    }

    @DexIgnore
    public final boolean j() {
        return this.e;
    }

    @DexIgnore
    public final int k() {
        return this.c.size();
    }

    @DexIgnore
    public final V l(int i) {
        q();
        V v = (V) this.c.remove(i).getValue();
        if (!this.d.isEmpty()) {
            Iterator<Map.Entry<K, V>> it = r().entrySet().iterator();
            this.c.add(new p33(this, it.next()));
            it.remove();
        }
        return v;
    }

    @DexIgnore
    public final Iterable<Map.Entry<K, V>> n() {
        return this.d.isEmpty() ? k33.a() : this.d.entrySet();
    }

    @DexIgnore
    public final Set<Map.Entry<K, V>> p() {
        if (this.h == null) {
            this.h = new l33(this, null);
        }
        return this.h;
    }

    @DexIgnore
    public final void q() {
        if (this.e) {
            throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final SortedMap<K, V> r() {
        q();
        if (this.d.isEmpty() && !(this.d instanceof TreeMap)) {
            TreeMap treeMap = new TreeMap();
            this.d = treeMap;
            this.g = treeMap.descendingMap();
        }
        return (SortedMap) this.d;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.g33<K extends java.lang.Comparable<K>, V> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V remove(Object obj) {
        q();
        Comparable comparable = (Comparable) obj;
        int a2 = a(comparable);
        if (a2 >= 0) {
            return (V) l(a2);
        }
        if (this.d.isEmpty()) {
            return null;
        }
        return this.d.remove(comparable);
    }

    @DexIgnore
    public int size() {
        return this.c.size() + this.d.size();
    }
}
