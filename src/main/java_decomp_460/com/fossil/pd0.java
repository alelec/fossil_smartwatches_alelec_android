package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pd0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f2816a;
    @DexIgnore
    public long b;

    @DexIgnore
    public pd0(int i, long j) {
        this.f2816a = i;
        this.b = j;
    }

    @DexIgnore
    public final pd0 a(int i, long j) {
        return new pd0(i, j);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof pd0) {
                pd0 pd0 = (pd0) obj;
                if (!(this.f2816a == pd0.f2816a && this.b == pd0.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.f2816a;
        long j = this.b;
        return (i * 31) + ((int) (j ^ (j >>> 32)));
    }

    @DexIgnore
    public String toString() {
        StringBuilder e = e.e("ExponentBackOffInfo(currentExponent=");
        e.append(this.f2816a);
        e.append(", maxRate=");
        e.append(this.b);
        e.append(")");
        return e.toString();
    }
}
