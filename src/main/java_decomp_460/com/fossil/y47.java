package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class y47 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f4241a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[rh5.values().length];
        f4241a = iArr;
        iArr[rh5.CALORIES.ordinal()] = 1;
        f4241a[rh5.ACTIVE_TIME.ordinal()] = 2;
        f4241a[rh5.TOTAL_STEPS.ordinal()] = 3;
        f4241a[rh5.TOTAL_SLEEP.ordinal()] = 4;
        f4241a[rh5.GOAL_TRACKING.ordinal()] = 5;
        int[] iArr2 = new int[rh5.values().length];
        b = iArr2;
        iArr2[rh5.TOTAL_STEPS.ordinal()] = 1;
        b[rh5.CALORIES.ordinal()] = 2;
        b[rh5.ACTIVE_TIME.ordinal()] = 3;
        b[rh5.GOAL_TRACKING.ordinal()] = 4;
        b[rh5.TOTAL_SLEEP.ordinal()] = 5;
    }
    */
}
