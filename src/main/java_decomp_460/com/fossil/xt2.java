package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import com.fossil.zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xt2 extends zs2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Long f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ String h;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle i;
    @DexIgnore
    public /* final */ /* synthetic */ boolean j;
    @DexIgnore
    public /* final */ /* synthetic */ boolean k;
    @DexIgnore
    public /* final */ /* synthetic */ zs2 l;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xt2(zs2 zs2, Long l2, String str, String str2, Bundle bundle, boolean z, boolean z2) {
        super(zs2);
        this.l = zs2;
        this.f = l2;
        this.g = str;
        this.h = str2;
        this.i = bundle;
        this.j = z;
        this.k = z2;
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void a() throws RemoteException {
        Long l2 = this.f;
        this.l.h.logEvent(this.g, this.h, this.i, this.j, this.k, l2 == null ? this.b : l2.longValue());
    }
}
