package com.fossil;

import android.util.Log;
import java.io.Writer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class jn0 extends Writer {
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public StringBuilder c; // = new StringBuilder(128);

    @DexIgnore
    public jn0(String str) {
        this.b = str;
    }

    @DexIgnore
    public final void a() {
        if (this.c.length() > 0) {
            Log.d(this.b, this.c.toString());
            StringBuilder sb = this.c;
            sb.delete(0, sb.length());
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.io.Writer, java.lang.AutoCloseable
    public void close() {
        a();
    }

    @DexIgnore
    @Override // java.io.Writer, java.io.Flushable
    public void flush() {
        a();
    }

    @DexIgnore
    @Override // java.io.Writer
    public void write(char[] cArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            char c2 = cArr[i + i3];
            if (c2 == '\n') {
                a();
            } else {
                this.c.append(c2);
            }
        }
    }
}
