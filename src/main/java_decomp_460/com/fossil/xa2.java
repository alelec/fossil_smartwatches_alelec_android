package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xa2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ya2 b;

    @DexIgnore
    public xa2(ya2 ya2) {
        this.b = ya2;
    }

    @DexIgnore
    public final void run() {
        ya2.n(this.b).lock();
        try {
            ya2.w(this.b);
        } finally {
            ya2.n(this.b).unlock();
        }
    }
}
