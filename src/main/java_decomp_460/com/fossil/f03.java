package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f03 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ l03 f1014a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public f03(int i) {
        byte[] bArr = new byte[i];
        this.b = bArr;
        this.f1014a = l03.f(bArr);
    }

    @DexIgnore
    public /* synthetic */ f03(int i, wz2 wz2) {
        this(i);
    }

    @DexIgnore
    public final xz2 a() {
        this.f1014a.N();
        return new h03(this.b);
    }

    @DexIgnore
    public final l03 b() {
        return this.f1014a;
    }
}
