package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jv4 implements Factory<iv4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<tt4> f1814a;

    @DexIgnore
    public jv4(Provider<tt4> provider) {
        this.f1814a = provider;
    }

    @DexIgnore
    public static jv4 a(Provider<tt4> provider) {
        return new jv4(provider);
    }

    @DexIgnore
    public static iv4 c(tt4 tt4) {
        return new iv4(tt4);
    }

    @DexIgnore
    /* renamed from: b */
    public iv4 get() {
        return c(this.f1814a.get());
    }
}
