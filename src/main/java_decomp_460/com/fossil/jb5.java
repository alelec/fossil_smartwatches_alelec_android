package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jb5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ TabLayout r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ ViewPager2 u;
    @DexIgnore
    public /* final */ FlexibleTextView v;

    @DexIgnore
    public jb5(Object obj, View view, int i, ConstraintLayout constraintLayout, TabLayout tabLayout, FlexibleTextView flexibleTextView, ImageView imageView, ViewPager2 viewPager2, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = tabLayout;
        this.s = flexibleTextView;
        this.t = imageView;
        this.u = viewPager2;
        this.v = flexibleTextView2;
    }
}
