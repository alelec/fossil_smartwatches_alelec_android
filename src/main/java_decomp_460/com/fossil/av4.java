package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.fossil.cv4;
import com.fossil.m47;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class av4 extends pv5 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ int m; // = 110;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public /* final */ String g; // = qn5.l.a().d("primaryText");
    @DexIgnore
    public g37<d85> h;
    @DexIgnore
    public cv4 i;
    @DexIgnore
    public po4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final int a() {
            return av4.m;
        }

        @DexIgnore
        public final String b() {
            return av4.l;
        }

        @DexIgnore
        public final av4 c() {
            return new av4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d85 b;
        @DexIgnore
        public /* final */ /* synthetic */ av4 c;

        @DexIgnore
        public b(d85 d85, av4 av4) {
            this.b = d85;
            this.c = av4;
        }

        @DexIgnore
        public final void onClick(View view) {
            cv4 N6 = av4.N6(this.c);
            FlexibleTextInputEditText flexibleTextInputEditText = this.b.t;
            pq7.b(flexibleTextInputEditText, "etSocialId");
            String valueOf = String.valueOf(flexibleTextInputEditText.getText());
            if (valueOf != null) {
                N6.h(wt7.u0(valueOf).toString());
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ d85 b;
        @DexIgnore
        public /* final */ /* synthetic */ av4 c;

        @DexIgnore
        public c(d85 d85, av4 av4) {
            this.b = d85;
            this.c = av4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            FlexibleTextInputLayout flexibleTextInputLayout = this.b.z;
            pq7.b(flexibleTextInputLayout, "inputSocialId");
            flexibleTextInputLayout.setErrorEnabled(false);
            cv4 N6 = av4.N6(this.c);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                N6.q(wt7.u0(valueOf).toString());
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ av4 f339a;

        @DexIgnore
        public d(av4 av4) {
            this.f339a = av4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            pq7.b(bool, "loading");
            if (bool.booleanValue()) {
                av4 av4 = this.f339a;
                String c = um5.c(PortfolioApp.h0.c(), 2131886259);
                pq7.b(c, "LanguageHelper.getString\u2026reating_Text__PleaseWait)");
                av4.H6(c);
                return;
            }
            this.f339a.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<cv4.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ av4 f340a;

        @DexIgnore
        public e(av4 av4) {
            this.f340a = av4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cv4.b bVar) {
            d85 d85 = (d85) av4.K6(this.f340a).a();
            if (d85 != null) {
                if (bVar.b()) {
                    d85.B.setCompoundDrawablesWithIntrinsicBounds(gl0.f(PortfolioApp.h0.c(), 2131231114), (Drawable) null, (Drawable) null, (Drawable) null);
                } else {
                    d85.B.setCompoundDrawablesWithIntrinsicBounds(gl0.f(PortfolioApp.h0.c(), 2131230967), (Drawable) null, (Drawable) null, (Drawable) null);
                }
                if (bVar.a()) {
                    d85.C.setCompoundDrawablesWithIntrinsicBounds(gl0.f(PortfolioApp.h0.c(), 2131231114), (Drawable) null, (Drawable) null, (Drawable) null);
                } else {
                    d85.C.setCompoundDrawablesWithIntrinsicBounds(gl0.f(PortfolioApp.h0.c(), 2131230967), (Drawable) null, (Drawable) null, (Drawable) null);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ av4 f341a;

        @DexIgnore
        public f(av4 av4) {
            this.f341a = av4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            pq7.b(bool, "it");
            if (bool.booleanValue()) {
                xr4.f4164a.e(PortfolioApp.h0.c().l0(), PortfolioApp.h0.c());
                this.f341a.Q6();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<cv4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ av4 f342a;

        @DexIgnore
        public g(av4 av4) {
            this.f342a = av4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cv4.a aVar) {
            if (aVar.a() != 409) {
                this.f342a.o(aVar.a(), aVar.b());
                return;
            }
            String c = um5.c(PortfolioApp.h0.c(), 2131886262);
            d85 d85 = (d85) av4.K6(this.f342a).a();
            if (d85 != null) {
                FlexibleButton flexibleButton = d85.q;
                pq7.b(flexibleButton, "btnCreateSocialProfile");
                flexibleButton.setEnabled(false);
                FlexibleTextInputLayout flexibleTextInputLayout = d85.z;
                pq7.b(flexibleTextInputLayout, "inputSocialId");
                flexibleTextInputLayout.setErrorEnabled(true);
                FlexibleTextInputLayout flexibleTextInputLayout2 = d85.z;
                pq7.b(flexibleTextInputLayout2, "inputSocialId");
                flexibleTextInputLayout2.setError(c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ av4 f343a;

        @DexIgnore
        public h(av4 av4) {
            this.f343a = av4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            FlexibleButton flexibleButton;
            d85 d85 = (d85) av4.K6(this.f343a).a();
            if (d85 != null && (flexibleButton = d85.q) != null) {
                pq7.b(bool, "it");
                flexibleButton.setEnabled(bool.booleanValue());
            }
        }
    }

    /*
    static {
        String simpleName = av4.class.getSimpleName();
        pq7.b(simpleName, "BCCreateSocialProfileFra\u2026nt::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 K6(av4 av4) {
        g37<d85> g37 = av4.h;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ cv4 N6(av4 av4) {
        cv4 cv4 = av4.i;
        if (cv4 != null) {
            return cv4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final String P6() {
        String a2 = m47.a(m47.c.TERMS, null);
        String a3 = m47.a(m47.c.PRIVACY, null);
        String str = um5.c(PortfolioApp.h0.c(), 2131886977).toString();
        pq7.b(a2, "termOfUseUrl");
        String q = vt7.q(str, "term_of_use_url", a2, false, 4, null);
        pq7.b(a3, "privacyPolicyUrl");
        return vt7.q(q, "privacy_policy", a3, false, 4, null);
    }

    @DexIgnore
    public final void Q6() {
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            int i2 = m;
            parentFragment.onActivityResult(i2, i2, null);
        }
    }

    @DexIgnore
    public final void R6() {
        g37<d85> g37 = this.h;
        if (g37 != null) {
            d85 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.x;
                pq7.b(flexibleTextView, "ftvTermsService");
                flexibleTextView.setMovementMethod(new LinkMovementMethod());
                String str = this.g;
                if (str != null) {
                    a2.x.setLinkTextColor(Color.parseColor(str));
                }
                a2.q.setOnClickListener(new b(a2, this));
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886266);
                FlexibleTextView flexibleTextView2 = a2.w;
                pq7.b(flexibleTextView2, "ftvDescriptionBold");
                jl5 jl5 = jl5.b;
                pq7.b(c2, "strBold");
                flexibleTextView2.setText(jl5.b(jl5, c2, c2, 0, 4, null));
                FlexibleTextView flexibleTextView3 = a2.x;
                pq7.b(flexibleTextView3, "ftvTermsService");
                flexibleTextView3.setText(cn0.a(P6(), 0));
                a2.t.addTextChangedListener(new c(a2, this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void S6() {
        cv4 cv4 = this.i;
        if (cv4 != null) {
            cv4.n().h(getViewLifecycleOwner(), new d(this));
            cv4 cv42 = this.i;
            if (cv42 != null) {
                cv42.p().h(getViewLifecycleOwner(), new e(this));
                cv4 cv43 = this.i;
                if (cv43 != null) {
                    cv43.o().h(getViewLifecycleOwner(), new f(this));
                    cv4 cv44 = this.i;
                    if (cv44 != null) {
                        cv44.m().h(getViewLifecycleOwner(), new g(this));
                        cv4 cv45 = this.i;
                        if (cv45 != null) {
                            cv45.l().h(getViewLifecycleOwner(), new h(this));
                        } else {
                            pq7.n("viewModel");
                            throw null;
                        }
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void o(int i2, String str) {
        s37 s37 = s37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        s37.n0(i2, str, childFragmentManager);
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().o0().a(this);
        po4 po4 = this.j;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(cv4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ileViewModel::class.java)");
            this.i = (cv4) a2;
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        d85 d85 = (d85) aq0.f(layoutInflater, 2131558578, viewGroup, false, A6());
        this.h = new g37<>(this, d85);
        pq7.b(d85, "binding");
        return d85.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        R6();
        S6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
