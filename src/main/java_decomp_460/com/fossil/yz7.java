package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class yz7 {
    @DexIgnore
    public static final int a(String str, int i, int i2, int i3) {
        return (int) wz7.c(str, (long) i, (long) i2, (long) i3);
    }

    @DexIgnore
    public static final long b(String str, long j, long j2, long j3) {
        String d = wz7.d(str);
        if (d != null) {
            Long e = ut7.e(d);
            if (e != null) {
                j = e.longValue();
                if (j2 > j || j3 < j) {
                    throw new IllegalStateException(("System property '" + str + "' should be in range " + j2 + ".." + j3 + ", but is '" + j + '\'').toString());
                }
            } else {
                throw new IllegalStateException(("System property '" + str + "' has unrecognized value '" + d + '\'').toString());
            }
        }
        return j;
    }

    @DexIgnore
    public static final boolean c(String str, boolean z) {
        String d = wz7.d(str);
        return d != null ? Boolean.parseBoolean(d) : z;
    }

    @DexIgnore
    public static /* synthetic */ int d(String str, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 4) != 0) {
            i2 = 1;
        }
        if ((i4 & 8) != 0) {
            i3 = Integer.MAX_VALUE;
        }
        return wz7.b(str, i, i2, i3);
    }

    @DexIgnore
    public static /* synthetic */ long e(String str, long j, long j2, long j3, int i, Object obj) {
        return wz7.c(str, j, (i & 4) != 0 ? 1 : j2, (i & 8) != 0 ? Long.MAX_VALUE : j3);
    }
}
