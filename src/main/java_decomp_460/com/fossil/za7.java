package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class za7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ float f4445a;
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public za7(float f, float f2, int i) {
        this.f4445a = f;
        this.b = f2;
        this.c = i;
    }

    @DexIgnore
    public final float a() {
        return this.f4445a;
    }

    @DexIgnore
    public final float b() {
        return this.b;
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof za7) {
                za7 za7 = (za7) obj;
                if (!(Float.compare(this.f4445a, za7.f4445a) == 0 && Float.compare(this.b, za7.b) == 0 && this.c == za7.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (((Float.floatToIntBits(this.f4445a) * 31) + Float.floatToIntBits(this.b)) * 31) + this.c;
    }

    @DexIgnore
    public String toString() {
        return "ComplicationDimension(posX=" + this.f4445a + ", posY=" + this.b + ", radius=" + this.c + ")";
    }
}
