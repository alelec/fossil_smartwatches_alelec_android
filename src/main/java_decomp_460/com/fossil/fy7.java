package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fy7 {
    @DexIgnore
    public static final void a(tn7 tn7) {
        xw7 xw7 = (xw7) tn7.get(xw7.r);
        if (xw7 != null && !xw7.isActive()) {
            throw xw7.k();
        }
    }

    @DexIgnore
    public static final Object b(qn7<? super tl7> qn7) {
        Object obj;
        tn7 context = qn7.getContext();
        a(context);
        qn7 c = xn7.c(qn7);
        if (!(c instanceof vv7)) {
            c = null;
        }
        vv7 vv7 = (vv7) c;
        if (vv7 != null) {
            if (vv7.h.Q(context)) {
                vv7.m(context, tl7.f3441a);
            } else {
                ey7 ey7 = new ey7();
                vv7.m(context.plus(ey7), tl7.f3441a);
                if (ey7.b) {
                    obj = wv7.c(vv7) ? yn7.d() : tl7.f3441a;
                }
            }
            obj = yn7.d();
        } else {
            obj = tl7.f3441a;
        }
        if (obj == yn7.d()) {
            go7.c(qn7);
        }
        return obj == yn7.d() ? obj : tl7.f3441a;
    }
}
