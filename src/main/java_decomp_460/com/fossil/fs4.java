package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.share.internal.ShareConstants;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fs4 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<gs4> f1184a;
    @DexIgnore
    public /* final */ View.OnClickListener b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ m15 f1185a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(m15 m15, View view, View.OnClickListener onClickListener) {
            super(view);
            pq7.c(m15, "binding");
            pq7.c(view, "root");
            pq7.c(onClickListener, "listener");
            this.f1185a = m15;
            view.setTag(this);
            view.setOnClickListener(onClickListener);
        }

        @DexIgnore
        public final void a(gs4 gs4) {
            pq7.c(gs4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            m15 m15 = this.f1185a;
            FlexibleTextView flexibleTextView = m15.q;
            pq7.b(flexibleTextView, "ftvName");
            flexibleTextView.setText(gs4.b());
            RTLImageView rTLImageView = m15.r;
            pq7.b(rTLImageView, "ivCheck");
            rTLImageView.setVisibility(gs4.c() ? 0 : 8);
        }
    }

    @DexIgnore
    public fs4(List<gs4> list, View.OnClickListener onClickListener) {
        pq7.c(list, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
        pq7.c(onClickListener, "listener");
        this.f1184a = list;
        this.b = onClickListener;
    }

    @DexIgnore
    /* renamed from: g */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        aVar.a(this.f1184a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f1184a.size();
    }

    @DexIgnore
    /* renamed from: h */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        m15 z = m15.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "DialogItemPrivacyLayoutB\u2026tInflater, parent, false)");
        View n = z.n();
        pq7.b(n, "binding.root");
        return new a(z, n, this.b);
    }
}
