package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum hu implements mt {
    SUCCESS((byte) 0),
    UNKNOWN((byte) 1);
    
    @DexIgnore
    public static /* final */ gu g; // = new gu(null);
    @DexIgnore
    public /* final */ String b; // = ey1.a(this);
    @DexIgnore
    public /* final */ byte c;

    @DexIgnore
    public hu(byte b2) {
        this.c = (byte) b2;
    }

    @DexIgnore
    @Override // com.fossil.mt
    public boolean a() {
        return this == SUCCESS;
    }

    @DexIgnore
    @Override // com.fossil.mt
    public String getLogName() {
        return this.b;
    }
}
