package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fv3 implements Parcelable.Creator<ev3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ev3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        gv3 gv3 = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 2) {
                gv3 = (gv3) ad2.e(parcel, t, gv3.CREATOR);
            } else if (l == 3) {
                i3 = ad2.v(parcel, t);
            } else if (l == 4) {
                i2 = ad2.v(parcel, t);
            } else if (l != 5) {
                ad2.B(parcel, t);
            } else {
                i = ad2.v(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new ev3(gv3, i3, i2, i);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ev3[] newArray(int i) {
        return new ev3[i];
    }
}
