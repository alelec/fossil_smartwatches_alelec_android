package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b7 extends h7 {
    @DexIgnore
    public /* final */ n6 b;
    @DexIgnore
    public /* final */ u6 c;
    @DexIgnore
    public /* final */ byte[] d;

    @DexIgnore
    public b7(g7 g7Var, n6 n6Var, u6 u6Var, byte[] bArr) {
        super(g7Var);
        this.b = n6Var;
        this.c = u6Var;
        this.d = bArr;
    }
}
