package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class za3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<za3> CREATOR; // = new ab3();
    @DexIgnore
    public boolean b;
    @DexIgnore
    public long c;
    @DexIgnore
    public float d;
    @DexIgnore
    public long e;
    @DexIgnore
    public int f;

    @DexIgnore
    public za3() {
        this(true, 50, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, Long.MAX_VALUE, Integer.MAX_VALUE);
    }

    @DexIgnore
    public za3(boolean z, long j, float f2, long j2, int i) {
        this.b = z;
        this.c = j;
        this.d = f2;
        this.e = j2;
        this.f = i;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof za3)) {
            return false;
        }
        za3 za3 = (za3) obj;
        return this.b == za3.b && this.c == za3.c && Float.compare(this.d, za3.d) == 0 && this.e == za3.e && this.f == za3.f;
    }

    @DexIgnore
    public final int hashCode() {
        return pc2.b(Boolean.valueOf(this.b), Long.valueOf(this.c), Float.valueOf(this.d), Long.valueOf(this.e), Integer.valueOf(this.f));
    }

    @DexIgnore
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceOrientationRequest[mShouldUseMag=");
        sb.append(this.b);
        sb.append(" mMinimumSamplingPeriodMs=");
        sb.append(this.c);
        sb.append(" mSmallestAngleChangeRadians=");
        sb.append(this.d);
        long j = this.e;
        if (j != Long.MAX_VALUE) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            sb.append(" expireIn=");
            sb.append(j - elapsedRealtime);
            sb.append("ms");
        }
        if (this.f != Integer.MAX_VALUE) {
            sb.append(" num=");
            sb.append(this.f);
        }
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.c(parcel, 1, this.b);
        bd2.r(parcel, 2, this.c);
        bd2.j(parcel, 3, this.d);
        bd2.r(parcel, 4, this.e);
        bd2.n(parcel, 5, this.f);
        bd2.b(parcel, a2);
    }
}
