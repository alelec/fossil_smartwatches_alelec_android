package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tg5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ConstraintLayout f3410a;
    @DexIgnore
    public /* final */ FlexibleButton b;
    @DexIgnore
    public /* final */ Group c;
    @DexIgnore
    public /* final */ FlexibleButton d;
    @DexIgnore
    public /* final */ RTLImageView e;
    @DexIgnore
    public /* final */ FlexibleTextView f;
    @DexIgnore
    public /* final */ RTLImageView g;
    @DexIgnore
    public /* final */ ConstraintLayout h;
    @DexIgnore
    public /* final */ FlexibleTextView i;
    @DexIgnore
    public /* final */ ViewPager2 j;

    @DexIgnore
    public tg5(ConstraintLayout constraintLayout, FlexibleButton flexibleButton, Group group, FlexibleButton flexibleButton2, RTLImageView rTLImageView, FlexibleTextView flexibleTextView, RTLImageView rTLImageView2, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView2, ViewPager2 viewPager2) {
        this.f3410a = constraintLayout;
        this.b = flexibleButton;
        this.c = group;
        this.d = flexibleButton2;
        this.e = rTLImageView;
        this.f = flexibleTextView;
        this.g = rTLImageView2;
        this.h = constraintLayout2;
        this.i = flexibleTextView2;
        this.j = viewPager2;
    }

    @DexIgnore
    public static tg5 a(View view) {
        int i2 = 2131362426;
        FlexibleButton flexibleButton = (FlexibleButton) view.findViewById(2131361973);
        if (flexibleButton != null) {
            Group group = (Group) view.findViewById(2131362224);
            if (group != null) {
                FlexibleButton flexibleButton2 = (FlexibleButton) view.findViewById(2131362260);
                if (flexibleButton2 != null) {
                    RTLImageView rTLImageView = (RTLImageView) view.findViewById(2131362281);
                    if (rTLImageView != null) {
                        FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(2131362426);
                        if (flexibleTextView != null) {
                            i2 = 2131362666;
                            RTLImageView rTLImageView2 = (RTLImageView) view.findViewById(2131362666);
                            if (rTLImageView2 != null) {
                                ConstraintLayout constraintLayout = (ConstraintLayout) view;
                                i2 = 2131363376;
                                FlexibleTextView flexibleTextView2 = (FlexibleTextView) view.findViewById(2131363376);
                                if (flexibleTextView2 != null) {
                                    i2 = 2131363497;
                                    ViewPager2 viewPager2 = (ViewPager2) view.findViewById(2131363497);
                                    if (viewPager2 != null) {
                                        return new tg5(constraintLayout, flexibleButton, group, flexibleButton2, rTLImageView, flexibleTextView, rTLImageView2, constraintLayout, flexibleTextView2, viewPager2);
                                    }
                                }
                            }
                        }
                    } else {
                        i2 = 2131362281;
                    }
                } else {
                    i2 = 2131362260;
                }
            } else {
                i2 = 2131362224;
            }
        } else {
            i2 = 2131361973;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
    }

    @DexIgnore
    public static tg5 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    @DexIgnore
    public static tg5 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(2131558856, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    @DexIgnore
    public ConstraintLayout b() {
        return this.f3410a;
    }
}
