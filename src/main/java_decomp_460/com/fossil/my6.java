package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class my6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2439a;

    /*
    static {
        int[] iArr = new int[zh5.values().length];
        f2439a = iArr;
        iArr[zh5.FAIL_DUE_TO_LACK_PERMISSION.ordinal()] = 1;
        f2439a[zh5.FAIL_DUE_TO_PENDING_WORKOUT.ordinal()] = 2;
        f2439a[zh5.FAIL_DUE_TO_SYNC_FAIL.ordinal()] = 3;
        f2439a[zh5.FAIL_DUE_TO_USER_DENY_STOP_WORKOUT.ordinal()] = 4;
    }
    */
}
