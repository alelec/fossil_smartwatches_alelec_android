package com.fossil;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class r55 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RecyclerView q;

    @DexIgnore
    public r55(Object obj, View view, int i, RecyclerView recyclerView) {
        super(obj, view, i);
        this.q = recyclerView;
    }
}
