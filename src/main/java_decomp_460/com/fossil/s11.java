package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Build;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.utils.ForceStopRunnable;
import com.fossil.o01;
import com.fossil.x01;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s11 extends g11 {
    @DexIgnore
    public static s11 j;
    @DexIgnore
    public static s11 k;
    @DexIgnore
    public static /* final */ Object l; // = new Object();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Context f3190a;
    @DexIgnore
    public o01 b;
    @DexIgnore
    public WorkDatabase c;
    @DexIgnore
    public k41 d;
    @DexIgnore
    public List<n11> e;
    @DexIgnore
    public m11 f;
    @DexIgnore
    public z31 g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public BroadcastReceiver.PendingResult i;

    @DexIgnore
    public s11(Context context, o01 o01, k41 k41) {
        this(context, o01, k41, context.getResources().getBoolean(c11.workmanager_test_configuration));
    }

    @DexIgnore
    public s11(Context context, o01 o01, k41 k41, WorkDatabase workDatabase) {
        Context applicationContext = context.getApplicationContext();
        x01.e(new x01.a(o01.g()));
        List<n11> h2 = h(applicationContext, o01, k41);
        r(context, o01, k41, workDatabase, h2, new m11(context, o01, k41, workDatabase, h2));
    }

    @DexIgnore
    public s11(Context context, o01 o01, k41 k41, boolean z) {
        this(context, o01, k41, WorkDatabase.a(context.getApplicationContext(), k41.c(), z));
    }

    @DexIgnore
    public static void f(Context context, o01 o01) {
        synchronized (l) {
            if (j != null && k != null) {
                throw new IllegalStateException("WorkManager is already initialized.  Did you try to initialize it manually without disabling WorkManagerInitializer? See WorkManager#initialize(Context, Configuration) or the class level Javadoc for more information.");
            } else if (j == null) {
                Context applicationContext = context.getApplicationContext();
                if (k == null) {
                    k = new s11(applicationContext, o01, new l41(o01.i()));
                }
                j = k;
            }
        }
    }

    @DexIgnore
    @Deprecated
    public static s11 k() {
        s11 s11;
        synchronized (l) {
            s11 = j != null ? j : k;
        }
        return s11;
    }

    @DexIgnore
    public static s11 l(Context context) {
        s11 k2;
        synchronized (l) {
            k2 = k();
            if (k2 == null) {
                Context applicationContext = context.getApplicationContext();
                if (applicationContext instanceof o01.b) {
                    f(applicationContext, ((o01.b) applicationContext).a());
                    k2 = l(applicationContext);
                } else {
                    throw new IllegalStateException("WorkManager is not initialized properly.  You have explicitly disabled WorkManagerInitializer in your manifest, have not manually called WorkManager#initialize at this point, and your Application does not implement Configuration.Provider.");
                }
            }
        }
        return k2;
    }

    @DexIgnore
    @Override // com.fossil.g11
    public a11 b(List<? extends h11> list) {
        if (!list.isEmpty()) {
            return new p11(this, list).a();
        }
        throw new IllegalArgumentException("enqueue needs at least one WorkRequest.");
    }

    @DexIgnore
    @Override // com.fossil.g11
    public a11 d(String str, s01 s01, List<z01> list) {
        return new p11(this, str, s01, list).a();
    }

    @DexIgnore
    public a11 g(UUID uuid) {
        v31 b2 = v31.b(uuid, this);
        this.d.b(b2);
        return b2.d();
    }

    @DexIgnore
    public List<n11> h(Context context, o01 o01, k41 k41) {
        return Arrays.asList(o11.a(context, this), new v11(context, o01, k41, this));
    }

    @DexIgnore
    public Context i() {
        return this.f3190a;
    }

    @DexIgnore
    public o01 j() {
        return this.b;
    }

    @DexIgnore
    public z31 m() {
        return this.g;
    }

    @DexIgnore
    public m11 n() {
        return this.f;
    }

    @DexIgnore
    public List<n11> o() {
        return this.e;
    }

    @DexIgnore
    public WorkDatabase p() {
        return this.c;
    }

    @DexIgnore
    public k41 q() {
        return this.d;
    }

    @DexIgnore
    public final void r(Context context, o01 o01, k41 k41, WorkDatabase workDatabase, List<n11> list, m11 m11) {
        Context applicationContext = context.getApplicationContext();
        this.f3190a = applicationContext;
        this.b = o01;
        this.d = k41;
        this.c = workDatabase;
        this.e = list;
        this.f = m11;
        this.g = new z31(workDatabase);
        this.h = false;
        if (Build.VERSION.SDK_INT < 24 || !applicationContext.isDeviceProtectedStorage()) {
            this.d.b(new ForceStopRunnable(applicationContext, this));
            return;
        }
        throw new IllegalStateException("Cannot initialize WorkManager in direct boot mode");
    }

    @DexIgnore
    public void s() {
        synchronized (l) {
            this.h = true;
            if (this.i != null) {
                this.i.finish();
                this.i = null;
            }
        }
    }

    @DexIgnore
    public void t() {
        if (Build.VERSION.SDK_INT >= 23) {
            d21.b(i());
        }
        p().j().t();
        o11.b(j(), p(), o());
    }

    @DexIgnore
    public void u(BroadcastReceiver.PendingResult pendingResult) {
        synchronized (l) {
            this.i = pendingResult;
            if (this.h) {
                pendingResult.finish();
                this.i = null;
            }
        }
    }

    @DexIgnore
    public void v(String str) {
        w(str, null);
    }

    @DexIgnore
    public void w(String str, WorkerParameters.a aVar) {
        this.d.b(new b41(this, str, aVar));
    }

    @DexIgnore
    public void x(String str) {
        this.d.b(new c41(this, str, true));
    }

    @DexIgnore
    public void y(String str) {
        this.d.b(new c41(this, str, false));
    }
}
