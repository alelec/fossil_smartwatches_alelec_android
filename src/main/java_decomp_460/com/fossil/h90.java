package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h90 implements Parcelable.Creator<i90> {
    @DexIgnore
    public /* synthetic */ h90(kq7 kq7) {
    }

    @DexIgnore
    public i90 a(Parcel parcel) {
        return new i90(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public i90 createFromParcel(Parcel parcel) {
        return new i90(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public i90[] newArray(int i) {
        return new i90[i];
    }
}
