package com.fossil;

import androidx.lifecycle.Lifecycle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l61 extends Lifecycle {
    @DexIgnore
    public static /* final */ l61 b; // = new l61();

    @DexIgnore
    @Override // androidx.lifecycle.Lifecycle
    public void a(cs0 cs0) {
        pq7.c(cs0, "observer");
    }

    @DexIgnore
    @Override // androidx.lifecycle.Lifecycle
    public Lifecycle.State b() {
        return Lifecycle.State.RESUMED;
    }

    @DexIgnore
    @Override // androidx.lifecycle.Lifecycle
    public void c(cs0 cs0) {
        pq7.c(cs0, "observer");
    }
}
