package com.fossil;

import com.fossil.cv2;
import com.fossil.e13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uu2 extends e13<uu2, a> implements o23 {
    @DexIgnore
    public static /* final */ uu2 zzh;
    @DexIgnore
    public static volatile z23<uu2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public cv2 zze;
    @DexIgnore
    public cv2 zzf;
    @DexIgnore
    public boolean zzg;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<uu2, a> implements o23 {
        @DexIgnore
        public a() {
            super(uu2.zzh);
        }

        @DexIgnore
        public /* synthetic */ a(tu2 tu2) {
            this();
        }

        @DexIgnore
        public final a B(boolean z) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((uu2) this.c).I(z);
            return this;
        }

        @DexIgnore
        public final a x(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((uu2) this.c).C(i);
            return this;
        }

        @DexIgnore
        public final a y(cv2.a aVar) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((uu2) this.c).H((cv2) ((e13) aVar.h()));
            return this;
        }

        @DexIgnore
        public final a z(cv2 cv2) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((uu2) this.c).M(cv2);
            return this;
        }
    }

    /*
    static {
        uu2 uu2 = new uu2();
        zzh = uu2;
        e13.u(uu2.class, uu2);
    }
    */

    @DexIgnore
    public static a S() {
        return (a) zzh.w();
    }

    @DexIgnore
    public final void C(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    @DexIgnore
    public final void H(cv2 cv2) {
        cv2.getClass();
        this.zze = cv2;
        this.zzc |= 2;
    }

    @DexIgnore
    public final void I(boolean z) {
        this.zzc |= 8;
        this.zzg = z;
    }

    @DexIgnore
    public final boolean J() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int K() {
        return this.zzd;
    }

    @DexIgnore
    public final void M(cv2 cv2) {
        cv2.getClass();
        this.zzf = cv2;
        this.zzc |= 4;
    }

    @DexIgnore
    public final cv2 N() {
        cv2 cv2 = this.zze;
        return cv2 == null ? cv2.c0() : cv2;
    }

    @DexIgnore
    public final boolean O() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final cv2 P() {
        cv2 cv2 = this.zzf;
        return cv2 == null ? cv2.c0() : cv2;
    }

    @DexIgnore
    public final boolean Q() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final boolean R() {
        return this.zzg;
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (tu2.f3469a[i - 1]) {
            case 1:
                return new uu2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u1004\u0000\u0002\u1009\u0001\u0003\u1009\u0002\u0004\u1007\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                z23<uu2> z232 = zzi;
                if (z232 != null) {
                    return z232;
                }
                synchronized (uu2.class) {
                    try {
                        z23 = zzi;
                        if (z23 == null) {
                            z23 = new e13.c(zzh);
                            zzi = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
