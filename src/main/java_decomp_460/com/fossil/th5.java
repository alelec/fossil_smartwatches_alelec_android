package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum th5 {
    DISCONNECTED(0),
    CONNECTED(1),
    RECONNECTED(2),
    UNKNOWN(3);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public th5(int i) {
        this.value = i;
    }

    @DexIgnore
    public final int getValue() {
        return this.value;
    }
}
