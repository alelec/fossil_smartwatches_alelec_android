package com.fossil;

import android.util.Log;
import com.fossil.af1;
import com.fossil.sc1;
import com.fossil.wb1;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class md1 implements sc1, sc1.a {
    @DexIgnore
    public /* final */ tc1<?> b;
    @DexIgnore
    public /* final */ sc1.a c;
    @DexIgnore
    public int d;
    @DexIgnore
    public pc1 e;
    @DexIgnore
    public Object f;
    @DexIgnore
    public volatile af1.a<?> g;
    @DexIgnore
    public qc1 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements wb1.a<Object> {
        @DexIgnore
        public /* final */ /* synthetic */ af1.a b;

        @DexIgnore
        public a(af1.a aVar) {
            this.b = aVar;
        }

        @DexIgnore
        @Override // com.fossil.wb1.a
        public void b(Exception exc) {
            if (md1.this.g(this.b)) {
                md1.this.i(this.b, exc);
            }
        }

        @DexIgnore
        @Override // com.fossil.wb1.a
        public void e(Object obj) {
            if (md1.this.g(this.b)) {
                md1.this.h(this.b, obj);
            }
        }
    }

    @DexIgnore
    public md1(tc1<?> tc1, sc1.a aVar) {
        this.b = tc1;
        this.c = aVar;
    }

    @DexIgnore
    @Override // com.fossil.sc1.a
    public void a(mb1 mb1, Exception exc, wb1<?> wb1, gb1 gb1) {
        this.c.a(mb1, exc, wb1, this.g.c.c());
    }

    @DexIgnore
    @Override // com.fossil.sc1.a
    public void b() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.sc1
    public boolean c() {
        Object obj = this.f;
        if (obj != null) {
            this.f = null;
            d(obj);
        }
        pc1 pc1 = this.e;
        if (pc1 != null && pc1.c()) {
            return true;
        }
        this.e = null;
        this.g = null;
        boolean z = false;
        while (!z && f()) {
            List<af1.a<?>> g2 = this.b.g();
            int i = this.d;
            this.d = i + 1;
            this.g = g2.get(i);
            if (this.g != null && (this.b.e().c(this.g.c.c()) || this.b.t(this.g.c.getDataClass()))) {
                j(this.g);
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.sc1
    public void cancel() {
        af1.a<?> aVar = this.g;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void d(Object obj) {
        long b2 = ek1.b();
        try {
            jb1<X> p = this.b.p(obj);
            rc1 rc1 = new rc1(p, obj, this.b.k());
            this.h = new qc1(this.g.f261a, this.b.o());
            this.b.d().a(this.h, rc1);
            if (Log.isLoggable("SourceGenerator", 2)) {
                Log.v("SourceGenerator", "Finished encoding source to cache, key: " + this.h + ", data: " + obj + ", encoder: " + p + ", duration: " + ek1.a(b2));
            }
            this.g.c.a();
            this.e = new pc1(Collections.singletonList(this.g.f261a), this.b, this);
        } catch (Throwable th) {
            this.g.c.a();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.sc1.a
    public void e(mb1 mb1, Object obj, wb1<?> wb1, gb1 gb1, mb1 mb12) {
        this.c.e(mb1, obj, wb1, this.g.c.c(), mb1);
    }

    @DexIgnore
    public final boolean f() {
        return this.d < this.b.g().size();
    }

    @DexIgnore
    public boolean g(af1.a<?> aVar) {
        af1.a<?> aVar2 = this.g;
        return aVar2 != null && aVar2 == aVar;
    }

    @DexIgnore
    public void h(af1.a<?> aVar, Object obj) {
        wc1 e2 = this.b.e();
        if (obj == null || !e2.c(aVar.c.c())) {
            sc1.a aVar2 = this.c;
            mb1 mb1 = aVar.f261a;
            wb1<Data> wb1 = aVar.c;
            aVar2.e(mb1, obj, wb1, wb1.c(), this.h);
            return;
        }
        this.f = obj;
        this.c.b();
    }

    @DexIgnore
    public void i(af1.a<?> aVar, Exception exc) {
        sc1.a aVar2 = this.c;
        qc1 qc1 = this.h;
        wb1<Data> wb1 = aVar.c;
        aVar2.a(qc1, exc, wb1, wb1.c());
    }

    @DexIgnore
    public final void j(af1.a<?> aVar) {
        this.g.c.d(this.b.l(), new a(aVar));
    }
}
