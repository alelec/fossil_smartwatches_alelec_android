package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mk1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends mk1 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public volatile boolean f2395a;

        @DexIgnore
        public b() {
            super();
        }

        @DexIgnore
        @Override // com.fossil.mk1
        public void b(boolean z) {
            this.f2395a = z;
        }

        @DexIgnore
        @Override // com.fossil.mk1
        public void c() {
            if (this.f2395a) {
                throw new IllegalStateException("Already released");
            }
        }
    }

    @DexIgnore
    public mk1() {
    }

    @DexIgnore
    public static mk1 a() {
        return new b();
    }

    @DexIgnore
    public abstract void b(boolean z);

    @DexIgnore
    public abstract void c();
}
