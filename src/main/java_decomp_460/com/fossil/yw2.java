package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yw2<T> implements xw2<T> {
    @DexIgnore
    public volatile xw2<T> b;
    @DexIgnore
    public volatile boolean c;
    @DexIgnore
    @NullableDecl
    public T d;

    @DexIgnore
    public yw2(xw2<T> xw2) {
        sw2.b(xw2);
        this.b = xw2;
    }

    @DexIgnore
    public final String toString() {
        Object obj = this.b;
        if (obj == null) {
            String valueOf = String.valueOf(this.d);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 25);
            sb.append("<supplier that returned ");
            sb.append(valueOf);
            sb.append(SimpleComparison.GREATER_THAN_OPERATION);
            obj = sb.toString();
        }
        String valueOf2 = String.valueOf(obj);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 19);
        sb2.append("Suppliers.memoize(");
        sb2.append(valueOf2);
        sb2.append(")");
        return sb2.toString();
    }

    @DexIgnore
    @Override // com.fossil.xw2
    public final T zza() {
        if (!this.c) {
            synchronized (this) {
                if (!this.c) {
                    T zza = this.b.zza();
                    this.d = zza;
                    this.c = true;
                    this.b = null;
                    return zza;
                }
            }
        }
        return this.d;
    }
}
