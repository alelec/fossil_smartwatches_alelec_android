package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rl4 extends Exception {
    @DexIgnore
    public rl4() {
    }

    @DexIgnore
    public rl4(String str) {
        super(str);
    }

    @DexIgnore
    public rl4(Throwable th) {
        super(th);
    }
}
