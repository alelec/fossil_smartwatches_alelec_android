package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.PixelCopy;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ar5;
import com.fossil.g47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.debug.DebugActivity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class br5 {
    @DexIgnore
    public static /* final */ String j;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public WeakReference<Context> f489a;
    @DexIgnore
    public ar5 b;
    @DexIgnore
    public mx3 c;
    @DexIgnore
    public mx3 d;
    @DexIgnore
    public String e;
    @DexIgnore
    public int f; // = -1;
    @DexIgnore
    public int g; // = -1;
    @DexIgnore
    public /* final */ iv7 h; // = jv7.a(ux7.b(null, 1, null).plus(bw7.b()));
    @DexIgnore
    public /* final */ UserRepository i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ar5.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ br5 f490a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.br5$a$a")
        /* renamed from: com.fossil.br5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0021a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public View$OnClickListenerC0021a(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.f490a.f = 1;
                if (this.b.f490a.f489a != null) {
                    WeakReference weakReference = this.b.f490a.f489a;
                    if (weakReference == null) {
                        pq7.i();
                        throw null;
                    } else if (weakReference.get() != null) {
                        g47.a aVar = g47.f1261a;
                        WeakReference weakReference2 = this.b.f490a.f489a;
                        if (weakReference2 != null) {
                            Object obj = weakReference2.get();
                            if (obj == null) {
                                throw new il7("null cannot be cast to non-null type android.app.Activity");
                            } else if (aVar.s((Activity) obj, 123)) {
                                this.b.f490a.u();
                                mx3 mx3 = this.b.f490a.c;
                                if (mx3 != null) {
                                    mx3.dismiss();
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public b(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.f490a.k();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public c(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.f490a.f = 0;
                this.b.f490a.k();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class d implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public d(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.f490a.f = 1;
                this.b.f490a.k();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class e implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public e(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.f490a.f = 3;
                this.b.f490a.k();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class f implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public f(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.f490a.f = 2;
                this.b.f490a.k();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class g implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.br5$a$g$a")
            @eo7(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$7$1", f = "ShakeFeedbackService.kt", l = {144}, m = "invokeSuspend")
            /* renamed from: com.fossil.br5$a$g$a  reason: collision with other inner class name */
            public static final class C0022a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ g this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0022a(g gVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = gVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0022a aVar = new C0022a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0022a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        br5 br5 = this.this$0.b.f490a;
                        this.L$0 = iv7;
                        this.label = 1;
                        if (br5.s(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            public g(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.f490a.f = 1;
                if (this.b.f490a.f489a != null) {
                    WeakReference weakReference = this.b.f490a.f489a;
                    if (weakReference == null) {
                        pq7.i();
                        throw null;
                    } else if (weakReference.get() != null) {
                        g47.a aVar = g47.f1261a;
                        WeakReference weakReference2 = this.b.f490a.f489a;
                        if (weakReference2 != null) {
                            Object obj = weakReference2.get();
                            if (obj == null) {
                                throw new il7("null cannot be cast to non-null type android.app.Activity");
                            } else if (aVar.s((Activity) obj, 123)) {
                                xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new C0022a(this, null), 3, null);
                                mx3 mx3 = this.b.f490a.c;
                                if (mx3 != null) {
                                    mx3.dismiss();
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class h implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public h(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.f490a.f489a != null) {
                    WeakReference weakReference = this.b.f490a.f489a;
                    if (weakReference == null) {
                        pq7.i();
                        throw null;
                    } else if (weakReference.get() != null) {
                        DebugActivity.a aVar = DebugActivity.O;
                        WeakReference weakReference2 = this.b.f490a.f489a;
                        if (weakReference2 != null) {
                            Object obj = weakReference2.get();
                            if (obj != null) {
                                pq7.b(obj, "contextWeakReference!!.get()!!");
                                aVar.a((Context) obj);
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                }
                mx3 mx3 = this.b.f490a.c;
                if (mx3 != null) {
                    mx3.dismiss();
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        public a(br5 br5) {
            this.f490a = br5;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:38:0x013b  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0151  */
        @Override // com.fossil.ar5.a
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a() {
            /*
            // Method dump skipped, instructions count: 381
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.br5.a.a():void");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.ShakeFeedbackService", f = "ShakeFeedbackService.kt", l = {401}, m = "sendFeedbackEmail")
    public static final class b extends co7 {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$13;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ br5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(br5 br5, qn7 qn7) {
            super(qn7);
            this.this$0 = br5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.r(null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Context b;

        @DexIgnore
        public c(Context context) {
            this.b = context;
        }

        @DexIgnore
        public final void run() {
            Toast.makeText(this.b, "Can't zip file!", 1).show();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.ShakeFeedbackService", f = "ShakeFeedbackService.kt", l = {261}, m = "sendHardwareLog")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ br5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(br5 br5, qn7 qn7) {
            super(qn7);
            this.this$0 = br5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ br5 b;

        @DexIgnore
        public e(br5 br5) {
            this.b = br5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.g = 0;
            this.b.k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ br5 b;

        @DexIgnore
        public f(br5 br5) {
            this.b = br5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements PixelCopy.OnPixelCopyFinishedListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ br5 f491a;
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.service.ShakeFeedbackService$startScreenShot$1$1", f = "ShakeFeedbackService.kt", l = {492}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    g gVar = this.this$0;
                    br5 br5 = gVar.f491a;
                    Bitmap bitmap = gVar.b;
                    pq7.b(bitmap, "bitmap");
                    this.L$0 = iv7;
                    this.label = 1;
                    if (br5.x(bitmap, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public g(br5 br5, Bitmap bitmap) {
            this.f491a = br5;
            this.b = bitmap;
        }

        @DexIgnore
        public final void onPixelCopyFinished(int i) {
            if (i == 0) {
                xw7 unused = gu7.d(this.f491a.h, null, null, new a(this, null), 3, null);
            } else {
                FLogger.INSTANCE.getLocal().d(br5.j, "prepareScreenShot - Can not extract image from current view");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.ShakeFeedbackService$startScreenShot$2", f = "ShakeFeedbackService.kt", l = {504}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ br5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(br5 br5, Bitmap bitmap, qn7 qn7) {
            super(2, qn7);
            this.this$0 = br5;
            this.$bitmap = bitmap;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, this.$bitmap, qn7);
            hVar.p$ = (iv7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                br5 br5 = this.this$0;
                Bitmap bitmap = this.$bitmap;
                pq7.b(bitmap, "bitmap");
                this.L$0 = iv7;
                this.label = 1;
                if (br5.x(bitmap, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.ShakeFeedbackService", f = "ShakeFeedbackService.kt", l = {540, 552, 553}, m = "takeScreenShot")
    public static final class i extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ br5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(br5 br5, qn7 qn7) {
            super(qn7);
            this.this$0 = br5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.x(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.ShakeFeedbackService$takeScreenShot$2", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
    public static final class j extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ br5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(br5 br5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = br5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            j jVar = new j(this.this$0, qn7);
            jVar.p$ = (iv7) obj;
            return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((j) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                mx3 mx3 = this.this$0.c;
                if (mx3 == null) {
                    return null;
                }
                mx3.dismiss();
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = br5.class.getSimpleName();
        pq7.b(simpleName, "ShakeFeedbackService::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public br5(UserRepository userRepository) {
        String file;
        pq7.c(userRepository, "mUserRepository");
        this.i = userRepository;
        if (o()) {
            file = Environment.getExternalStorageDirectory().toString();
        } else {
            Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
            pq7.b(applicationContext, "PortfolioApp.instance.applicationContext");
            file = applicationContext.getFilesDir().toString();
        }
        this.e = file;
        this.e = pq7.h(file, "/com.fossil.wearables.fossil/");
    }

    @DexIgnore
    public final void k() {
        WeakReference<Context> weakReference = this.f489a;
        if (weakReference == null) {
            return;
        }
        if (weakReference == null) {
            pq7.i();
            throw null;
        } else if (weakReference.get() != null) {
            g47.a aVar = g47.f1261a;
            WeakReference<Context> weakReference2 = this.f489a;
            if (weakReference2 != null) {
                Context context = weakReference2.get();
                if (context == null) {
                    throw new il7("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.s((Activity) context, 123)) {
                    v();
                }
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final List<File> l() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(FLogger.INSTANCE.getLocal().exportAppLogs());
        arrayList2.addAll(FLogger.INSTANCE.getRemote().exportAppLogs());
        arrayList2.addAll(MicroAppEventLogger.exportLogFiles());
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            File file = (File) it.next();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = j;
            StringBuilder sb = new StringBuilder();
            sb.append("Exporting ");
            pq7.b(file, "file");
            sb.append(file.getName());
            sb.append(", size=");
            sb.append(file.length());
            local.d(str, sb.toString());
            try {
                File file2 = new File(this.e, file.getName());
                if (file2.exists()) {
                    file2.delete();
                } else {
                    file2.createNewFile();
                }
                FileChannel channel = new FileInputStream(file).getChannel();
                pq7.b(channel, "FileInputStream(file).channel");
                FileChannel channel2 = new FileOutputStream(file2).getChannel();
                pq7.b(channel2, "FileOutputStream(exportFile).channel");
                channel2.transferFrom(channel, 0, channel.size());
                channel.close();
                channel2.close();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = j;
                local2.d(str2, "Done exporting " + file2.getName() + ", size=" + file2.length());
                arrayList.add(file2);
            } catch (Exception e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = j;
                local3.e(str3, "Error while exporting log files - e=" + e2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final Uri m(String str) {
        if (Build.VERSION.SDK_INT < 24) {
            return Uri.parse("file://" + str);
        } else if (this.f489a == null || TextUtils.isEmpty(str)) {
            return null;
        } else {
            WeakReference<Context> weakReference = this.f489a;
            if (weakReference != null) {
                Context context = weakReference.get();
                if (context != null) {
                    return FileProvider.getUriForFile(context, "com.fossil.wearables.fossil.provider", new File(str));
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void n(Context context) {
        synchronized (this) {
            pq7.c(context, Constants.ACTIVITY);
            this.f489a = new WeakReference<>(context);
            Object systemService = context.getSystemService("sensor");
            if (systemService != null) {
                SensorManager sensorManager = (SensorManager) systemService;
                ar5 ar5 = new ar5(new a(this));
                this.b = ar5;
                if (ar5 != null) {
                    ar5.b(sensorManager);
                }
            } else {
                throw new il7("null cannot be cast to non-null type android.hardware.SensorManager");
            }
        }
    }

    @DexIgnore
    public final boolean o() {
        return pq7.a("mounted", Environment.getExternalStorageState());
    }

    @DexIgnore
    public final boolean p() {
        mx3 mx3;
        mx3 mx32 = this.c;
        if (mx32 != null) {
            if (mx32 == null) {
                pq7.i();
                throw null;
            } else if (mx32.isShowing() && (mx3 = this.d) != null) {
                if (mx3 == null) {
                    pq7.i();
                    throw null;
                } else if (mx3.isShowing()) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final File q(String str, File file) {
        File[] listFiles;
        File file2;
        if (file == null || (listFiles = file.listFiles()) == null) {
            return null;
        }
        if (!(!(listFiles.length == 0))) {
            return null;
        }
        FLogger.INSTANCE.getLocal().e(j, ".sendFeedbackEmail - files.length=" + listFiles.length);
        try {
            file2 = new File(Environment.getExternalStorageDirectory(), str + ".zip");
            try {
                ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(file2));
                for (File file3 : listFiles) {
                    byte[] bArr = new byte[1024];
                    FileInputStream fileInputStream = new FileInputStream(file3);
                    pq7.b(file3, "file");
                    zipOutputStream.putNextEntry(new ZipEntry(file3.getName()));
                    br7 br7 = new br7();
                    while (true) {
                        int read = fileInputStream.read(bArr);
                        br7.element = read;
                        if (!(read > 0)) {
                            break;
                        }
                        zipOutputStream.write(bArr, 0, br7.element);
                    }
                    fileInputStream.close();
                }
                zipOutputStream.close();
                return file2;
            } catch (IOException e2) {
                e = e2;
                FLogger.INSTANCE.getLocal().e(j, ".sendFeedbackEmail - read sdk log ex=" + e);
                return file2;
            }
        } catch (IOException e3) {
            e = e3;
            file2 = null;
            FLogger.INSTANCE.getLocal().e(j, ".sendFeedbackEmail - read sdk log ex=" + e);
            return file2;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object r(java.lang.String r25, java.util.List<java.lang.String> r26, java.util.List<? extends java.io.File> r27, com.fossil.qn7<? super com.fossil.tl7> r28) {
        /*
        // Method dump skipped, instructions count: 1526
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.br5.r(java.lang.String, java.util.List, java.util.List, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004c A[Catch:{ Exception -> 0x0177 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object s(com.fossil.qn7<? super com.fossil.tl7> r15) {
        /*
        // Method dump skipped, instructions count: 417
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.br5.s(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object t(Context context, qn7<? super tl7> qn7) {
        v();
        return tl7.f3441a;
    }

    @DexIgnore
    public final void u() {
        WeakReference<Context> weakReference = this.f489a;
        if (weakReference == null) {
            pq7.i();
            throw null;
        } else if (pk5.a(weakReference.get()) && !p()) {
            mx3 mx3 = this.d;
            if (mx3 != null) {
                if (mx3 != null) {
                    mx3.dismiss();
                } else {
                    pq7.i();
                    throw null;
                }
            }
            WeakReference<Context> weakReference2 = this.f489a;
            if (weakReference2 != null) {
                Context context = weakReference2.get();
                if (context != null) {
                    Object systemService = context.getSystemService("layout_inflater");
                    if (systemService != null) {
                        View inflate = ((LayoutInflater) systemService).inflate(2131558462, (ViewGroup) null);
                        WeakReference<Context> weakReference3 = this.f489a;
                        if (weakReference3 != null) {
                            Context context2 = weakReference3.get();
                            if (context2 != null) {
                                mx3 mx32 = new mx3(context2);
                                this.d = mx32;
                                mx32.setContentView(inflate);
                                View findViewById = inflate.findViewById(2131363419);
                                if (findViewById != null) {
                                    ((TextView) findViewById).setText("4.6.0");
                                    inflate.findViewById(2131361947).setOnClickListener(new e(this));
                                    inflate.findViewById(2131361949).setOnClickListener(new f(this));
                                    mx3 mx33 = this.d;
                                    if (mx33 != null) {
                                        mx33.show();
                                    } else {
                                        pq7.i();
                                        throw null;
                                    }
                                } else {
                                    throw new il7("null cannot be cast to non-null type android.widget.TextView");
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        throw new il7("null cannot be cast to non-null type android.view.LayoutInflater");
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void v() {
        WeakReference<Context> weakReference = this.f489a;
        if (weakReference != null) {
            Context context = weakReference.get();
            if (context != null) {
                Window window = ((Activity) context).getWindow();
                pq7.b(window, "(contextWeakReference!!.get() as Activity).window");
                View decorView = window.getDecorView();
                pq7.b(decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                View rootView = decorView.getRootView();
                if (Build.VERSION.SDK_INT >= 26) {
                    pq7.b(rootView, "view");
                    Bitmap createBitmap = Bitmap.createBitmap(rootView.getWidth(), rootView.getHeight(), Bitmap.Config.ARGB_8888);
                    int[] iArr = new int[2];
                    rootView.getLocationInWindow(iArr);
                    WeakReference<Context> weakReference2 = this.f489a;
                    if (weakReference2 != null) {
                        Context context2 = weakReference2.get();
                        if (context2 != null) {
                            PixelCopy.request(((Activity) context2).getWindow(), new Rect(iArr[0], iArr[1], iArr[0] + rootView.getWidth(), rootView.getHeight() + iArr[1]), createBitmap, new g(this, createBitmap), new Handler(Looper.getMainLooper()));
                            return;
                        }
                        throw new il7("null cannot be cast to non-null type android.app.Activity");
                    }
                    pq7.i();
                    throw null;
                }
                pq7.b(rootView, "view");
                rootView.setDrawingCacheEnabled(true);
                if (rootView.getDrawingCache() != null) {
                    Bitmap createBitmap2 = Bitmap.createBitmap(rootView.getDrawingCache());
                    rootView.setDrawingCacheEnabled(false);
                    xw7 unused = gu7.d(this.h, null, null, new h(this, createBitmap2, null), 3, null);
                    return;
                }
                return;
            }
            throw new il7("null cannot be cast to non-null type android.app.Activity");
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void w() {
        synchronized (this) {
            ar5 ar5 = this.b;
            if (ar5 != null) {
                ar5.c();
            }
            if (this.c != null) {
                mx3 mx3 = this.c;
                if (mx3 != null) {
                    mx3.dismiss();
                    this.c = null;
                } else {
                    pq7.i();
                    throw null;
                }
            }
            if (this.d != null) {
                mx3 mx32 = this.d;
                if (mx32 != null) {
                    mx32.dismiss();
                    this.d = null;
                } else {
                    pq7.i();
                    throw null;
                }
            }
            this.f489a = null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x01b8  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x026a  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0296  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x02be  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object x(android.graphics.Bitmap r26, com.fossil.qn7<? super com.fossil.tl7> r27) {
        /*
        // Method dump skipped, instructions count: 722
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.br5.x(android.graphics.Bitmap, com.fossil.qn7):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0114, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0115, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0118, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0119, code lost:
        r0 = null;
        r4 = null;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0114 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0027] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.io.File y(java.util.ArrayList<java.lang.String> r15) {
        /*
        // Method dump skipped, instructions count: 289
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.br5.y(java.util.ArrayList):java.io.File");
    }
}
