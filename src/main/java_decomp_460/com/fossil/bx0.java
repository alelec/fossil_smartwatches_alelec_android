package com.fossil;

import android.database.Cursor;
import com.fossil.gu0;
import com.fossil.nw0;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bx0<T> extends gu0<T> {
    @DexIgnore
    public /* final */ String mCountQuery;
    @DexIgnore
    public /* final */ qw0 mDb;
    @DexIgnore
    public /* final */ boolean mInTransaction;
    @DexIgnore
    public /* final */ String mLimitOffsetQuery;
    @DexIgnore
    public /* final */ nw0.c mObserver;
    @DexIgnore
    public /* final */ tw0 mSourceQuery;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends nw0.c {
        @DexIgnore
        public a(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        @Override // com.fossil.nw0.c
        public void onInvalidated(Set<String> set) {
            bx0.this.invalidate();
        }
    }

    @DexIgnore
    public bx0(qw0 qw0, ox0 ox0, boolean z, String... strArr) {
        this(qw0, tw0.j(ox0), z, strArr);
    }

    @DexIgnore
    public bx0(qw0 qw0, tw0 tw0, boolean z, String... strArr) {
        this.mDb = qw0;
        this.mSourceQuery = tw0;
        this.mInTransaction = z;
        this.mCountQuery = "SELECT COUNT(*) FROM ( " + this.mSourceQuery.b() + " )";
        this.mLimitOffsetQuery = "SELECT * FROM ( " + this.mSourceQuery.b() + " ) LIMIT ? OFFSET ?";
        this.mObserver = new a(strArr);
        qw0.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private tw0 getSQLiteQuery(int i, int i2) {
        tw0 f = tw0.f(this.mLimitOffsetQuery, this.mSourceQuery.a() + 2);
        f.h(this.mSourceQuery);
        f.bindLong(f.a() - 1, (long) i2);
        f.bindLong(f.a(), (long) i);
        return f;
    }

    @DexIgnore
    public abstract List<T> convertRows(Cursor cursor);

    @DexIgnore
    public int countItems() {
        int i = 0;
        tw0 f = tw0.f(this.mCountQuery, this.mSourceQuery.a());
        f.h(this.mSourceQuery);
        Cursor query = this.mDb.query(f);
        try {
            if (query.moveToFirst()) {
                i = query.getInt(0);
            } else {
                query.close();
                f.m();
            }
            return i;
        } finally {
            query.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public boolean isInvalid() {
        this.mDb.getInvalidationTracker().i();
        return super.isInvalid();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004d  */
    @Override // com.fossil.gu0
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadInitial(com.fossil.gu0.d r7, com.fossil.gu0.b<T> r8) {
        /*
            r6 = this;
            r1 = 0
            java.util.List r4 = java.util.Collections.emptyList()
            com.fossil.qw0 r0 = r6.mDb
            r0.beginTransaction()
            int r5 = r6.countItems()     // Catch:{ all -> 0x0055 }
            if (r5 == 0) goto L_0x0051
            int r0 = com.fossil.gu0.computeInitialLoadPosition(r7, r5)     // Catch:{ all -> 0x0055 }
            int r2 = com.fossil.gu0.computeInitialLoadSize(r7, r0, r5)     // Catch:{ all -> 0x0055 }
            com.fossil.tw0 r2 = r6.getSQLiteQuery(r0, r2)     // Catch:{ all -> 0x0055 }
            com.fossil.qw0 r3 = r6.mDb     // Catch:{ all -> 0x003f }
            android.database.Cursor r1 = r3.query(r2)     // Catch:{ all -> 0x003f }
            java.util.List r4 = r6.convertRows(r1)     // Catch:{ all -> 0x003f }
            com.fossil.qw0 r3 = r6.mDb     // Catch:{ all -> 0x003f }
            r3.setTransactionSuccessful()     // Catch:{ all -> 0x003f }
            r3 = r1
        L_0x002c:
            if (r3 == 0) goto L_0x0031
            r3.close()
        L_0x0031:
            com.fossil.qw0 r1 = r6.mDb
            r1.endTransaction()
            if (r2 == 0) goto L_0x003b
            r2.m()
        L_0x003b:
            r8.a(r4, r0, r5)
            return
        L_0x003f:
            r0 = move-exception
            r3 = r1
        L_0x0041:
            if (r3 == 0) goto L_0x0046
            r3.close()
        L_0x0046:
            com.fossil.qw0 r1 = r6.mDb
            r1.endTransaction()
            if (r2 == 0) goto L_0x0050
            r2.m()
        L_0x0050:
            throw r0
        L_0x0051:
            r0 = 0
            r2 = r1
            r3 = r1
            goto L_0x002c
        L_0x0055:
            r0 = move-exception
            r2 = r1
            r3 = r1
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bx0.loadInitial(com.fossil.gu0$d, com.fossil.gu0$b):void");
    }

    @DexIgnore
    public List<T> loadRange(int i, int i2) {
        List<T> convertRows;
        tw0 sQLiteQuery = getSQLiteQuery(i, i2);
        if (this.mInTransaction) {
            this.mDb.beginTransaction();
            Cursor cursor = null;
            try {
                cursor = this.mDb.query(sQLiteQuery);
                convertRows = convertRows(cursor);
                this.mDb.setTransactionSuccessful();
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
                this.mDb.endTransaction();
                sQLiteQuery.m();
            }
        } else {
            Cursor query = this.mDb.query(sQLiteQuery);
            try {
                convertRows = convertRows(query);
            } finally {
                query.close();
                sQLiteQuery.m();
            }
        }
        return convertRows;
    }

    @DexIgnore
    @Override // com.fossil.gu0
    public void loadRange(gu0.g gVar, gu0.e<T> eVar) {
        eVar.a(loadRange(gVar.f1364a, gVar.b));
    }
}
