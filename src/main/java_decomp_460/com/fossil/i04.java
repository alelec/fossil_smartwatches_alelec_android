package com.fossil;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i04 {
    @Deprecated

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public float f1567a;
    @DexIgnore
    @Deprecated
    public float b;
    @DexIgnore
    @Deprecated
    public float c;
    @DexIgnore
    @Deprecated
    public float d;
    @DexIgnore
    @Deprecated
    public float e;
    @DexIgnore
    @Deprecated
    public float f;
    @DexIgnore
    public /* final */ List<f> g; // = new ArrayList();
    @DexIgnore
    public /* final */ List<g> h; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends g {
        @DexIgnore
        public /* final */ /* synthetic */ List b;
        @DexIgnore
        public /* final */ /* synthetic */ Matrix c;

        @DexIgnore
        public a(i04 i04, List list, Matrix matrix) {
            this.b = list;
            this.c = matrix;
        }

        @DexIgnore
        @Override // com.fossil.i04.g
        public void a(Matrix matrix, uz3 uz3, int i, Canvas canvas) {
            for (g gVar : this.b) {
                gVar.a(this.c, uz3, i, canvas);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends g {
        @DexIgnore
        public /* final */ d b;

        @DexIgnore
        public b(d dVar) {
            this.b = dVar;
        }

        @DexIgnore
        @Override // com.fossil.i04.g
        public void a(Matrix matrix, uz3 uz3, int i, Canvas canvas) {
            uz3.a(canvas, matrix, new RectF(this.b.k(), this.b.o(), this.b.l(), this.b.j()), i, this.b.m(), this.b.n());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends g {
        @DexIgnore
        public /* final */ e b;
        @DexIgnore
        public /* final */ float c;
        @DexIgnore
        public /* final */ float d;

        @DexIgnore
        public c(e eVar, float f, float f2) {
            this.b = eVar;
            this.c = f;
            this.d = f2;
        }

        @DexIgnore
        @Override // com.fossil.i04.g
        public void a(Matrix matrix, uz3 uz3, int i, Canvas canvas) {
            RectF rectF = new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) Math.hypot((double) (this.b.c - this.d), (double) (this.b.b - this.c)), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            Matrix matrix2 = new Matrix(matrix);
            matrix2.preTranslate(this.c, this.d);
            matrix2.preRotate(c());
            uz3.b(canvas, matrix2, rectF, i);
        }

        @DexIgnore
        public float c() {
            return (float) Math.toDegrees(Math.atan((double) ((this.b.c - this.d) / (this.b.b - this.c))));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends f {
        @DexIgnore
        public static /* final */ RectF h; // = new RectF();
        @DexIgnore
        @Deprecated
        public float b;
        @DexIgnore
        @Deprecated
        public float c;
        @DexIgnore
        @Deprecated
        public float d;
        @DexIgnore
        @Deprecated
        public float e;
        @DexIgnore
        @Deprecated
        public float f;
        @DexIgnore
        @Deprecated
        public float g;

        @DexIgnore
        public d(float f2, float f3, float f4, float f5) {
            q(f2);
            u(f3);
            r(f4);
            p(f5);
        }

        @DexIgnore
        @Override // com.fossil.i04.f
        public void a(Matrix matrix, Path path) {
            Matrix matrix2 = this.f1568a;
            matrix.invert(matrix2);
            path.transform(matrix2);
            h.set(k(), o(), l(), j());
            path.arcTo(h, m(), n(), false);
            path.transform(matrix);
        }

        @DexIgnore
        public final float j() {
            return this.e;
        }

        @DexIgnore
        public final float k() {
            return this.b;
        }

        @DexIgnore
        public final float l() {
            return this.d;
        }

        @DexIgnore
        public final float m() {
            return this.f;
        }

        @DexIgnore
        public final float n() {
            return this.g;
        }

        @DexIgnore
        public final float o() {
            return this.c;
        }

        @DexIgnore
        public final void p(float f2) {
            this.e = f2;
        }

        @DexIgnore
        public final void q(float f2) {
            this.b = f2;
        }

        @DexIgnore
        public final void r(float f2) {
            this.d = f2;
        }

        @DexIgnore
        public final void s(float f2) {
            this.f = f2;
        }

        @DexIgnore
        public final void t(float f2) {
            this.g = f2;
        }

        @DexIgnore
        public final void u(float f2) {
            this.c = f2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends f {
        @DexIgnore
        public float b;
        @DexIgnore
        public float c;

        @DexIgnore
        @Override // com.fossil.i04.f
        public void a(Matrix matrix, Path path) {
            Matrix matrix2 = this.f1568a;
            matrix.invert(matrix2);
            path.transform(matrix2);
            path.lineTo(this.b, this.c);
            path.transform(matrix);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Matrix f1568a; // = new Matrix();

        @DexIgnore
        public abstract void a(Matrix matrix, Path path);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ Matrix f1569a; // = new Matrix();

        @DexIgnore
        public abstract void a(Matrix matrix, uz3 uz3, int i, Canvas canvas);

        @DexIgnore
        public final void b(uz3 uz3, int i, Canvas canvas) {
            a(f1569a, uz3, i, canvas);
        }
    }

    @DexIgnore
    public i04() {
        m(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public void a(float f2, float f3, float f4, float f5, float f6, float f7) {
        d dVar = new d(f2, f3, f4, f5);
        dVar.s(f6);
        dVar.t(f7);
        this.g.add(dVar);
        b bVar = new b(dVar);
        float f8 = f6 + f7;
        boolean z = f7 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (z) {
            f6 = (f6 + 180.0f) % 360.0f;
        }
        c(bVar, f6, z ? (180.0f + f8) % 360.0f : f8);
        double d2 = (double) f8;
        q((((f4 - f2) / 2.0f) * ((float) Math.cos(Math.toRadians(d2)))) + ((f2 + f4) * 0.5f));
        r(((f3 + f5) * 0.5f) + (((f5 - f3) / 2.0f) * ((float) Math.sin(Math.toRadians(d2)))));
    }

    @DexIgnore
    public final void b(float f2) {
        if (f() != f2) {
            float f3 = ((f2 - f()) + 360.0f) % 360.0f;
            if (f3 <= 180.0f) {
                d dVar = new d(h(), i(), h(), i());
                dVar.s(f());
                dVar.t(f3);
                this.h.add(new b(dVar));
                o(f2);
            }
        }
    }

    @DexIgnore
    public final void c(g gVar, float f2, float f3) {
        b(f2);
        this.h.add(gVar);
        o(f3);
    }

    @DexIgnore
    public void d(Matrix matrix, Path path) {
        int size = this.g.size();
        for (int i = 0; i < size; i++) {
            this.g.get(i).a(matrix, path);
        }
    }

    @DexIgnore
    public g e(Matrix matrix) {
        b(g());
        return new a(this, new ArrayList(this.h), matrix);
    }

    @DexIgnore
    public final float f() {
        return this.e;
    }

    @DexIgnore
    public final float g() {
        return this.f;
    }

    @DexIgnore
    public float h() {
        return this.c;
    }

    @DexIgnore
    public float i() {
        return this.d;
    }

    @DexIgnore
    public float j() {
        return this.f1567a;
    }

    @DexIgnore
    public float k() {
        return this.b;
    }

    @DexIgnore
    public void l(float f2, float f3) {
        e eVar = new e();
        eVar.b = f2;
        eVar.c = f3;
        this.g.add(eVar);
        c cVar = new c(eVar, h(), i());
        c(cVar, cVar.c() + 270.0f, cVar.c() + 270.0f);
        q(f2);
        r(f3);
    }

    @DexIgnore
    public void m(float f2, float f3) {
        n(f2, f3, 270.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public void n(float f2, float f3, float f4, float f5) {
        s(f2);
        t(f3);
        q(f2);
        r(f3);
        o(f4);
        p((f4 + f5) % 360.0f);
        this.g.clear();
        this.h.clear();
    }

    @DexIgnore
    public final void o(float f2) {
        this.e = f2;
    }

    @DexIgnore
    public final void p(float f2) {
        this.f = f2;
    }

    @DexIgnore
    public final void q(float f2) {
        this.c = f2;
    }

    @DexIgnore
    public final void r(float f2) {
        this.d = f2;
    }

    @DexIgnore
    public final void s(float f2) {
        this.f1567a = f2;
    }

    @DexIgnore
    public final void t(float f2) {
        this.b = f2;
    }
}
