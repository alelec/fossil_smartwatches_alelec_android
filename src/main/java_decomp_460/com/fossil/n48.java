package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n48 implements c58 {
    @DexIgnore
    public /* final */ c58 b;

    @DexIgnore
    public n48(c58 c58) {
        pq7.c(c58, "delegate");
        this.b = c58;
    }

    @DexIgnore
    public final c58 a() {
        return this.b;
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.c58, java.lang.AutoCloseable
    public void close() throws IOException {
        this.b.close();
    }

    @DexIgnore
    @Override // com.fossil.c58
    public long d0(i48 i48, long j) throws IOException {
        pq7.c(i48, "sink");
        return this.b.d0(i48, j);
    }

    @DexIgnore
    @Override // com.fossil.c58
    public d58 e() {
        return this.b.e();
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + '(' + this.b + ')';
    }
}
