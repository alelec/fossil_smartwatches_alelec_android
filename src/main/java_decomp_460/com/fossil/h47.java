package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h47<T> {
    @DexIgnore
    public static /* final */ a e; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ xh5 f1428a;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ Integer c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final <T> h47<T> a(T t) {
            return new h47<>(xh5.DATABASE_LOADING, t, null, null);
        }

        @DexIgnore
        public final <T> h47<T> b(int i, String str, T t) {
            pq7.c(str, "msg");
            return new h47<>(xh5.ERROR, t, Integer.valueOf(i), str);
        }

        @DexIgnore
        public final <T> h47<T> c(T t) {
            return new h47<>(xh5.NETWORK_LOADING, t, null, null);
        }

        @DexIgnore
        public final <T> h47<T> d(T t) {
            return new h47<>(xh5.SUCCESS, t, null, null);
        }
    }

    @DexIgnore
    public h47(xh5 xh5, T t, Integer num, String str) {
        pq7.c(xh5, "status");
        this.f1428a = xh5;
        this.b = t;
        this.c = num;
        this.d = str;
    }

    @DexIgnore
    public final xh5 a() {
        return this.f1428a;
    }

    @DexIgnore
    public final T b() {
        return this.b;
    }

    @DexIgnore
    public final T c() {
        return this.b;
    }

    @DexIgnore
    public final xh5 d() {
        return this.f1428a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof h47) {
                h47 h47 = (h47) obj;
                if (!pq7.a(this.f1428a, h47.f1428a) || !pq7.a(this.b, h47.b) || !pq7.a(this.c, h47.c) || !pq7.a(this.d, h47.d)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        xh5 xh5 = this.f1428a;
        int hashCode = xh5 != null ? xh5.hashCode() : 0;
        T t = this.b;
        int hashCode2 = t != null ? t.hashCode() : 0;
        Integer num = this.c;
        int hashCode3 = num != null ? num.hashCode() : 0;
        String str = this.d;
        if (str != null) {
            i = str.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Resource(status=" + this.f1428a + ", data=" + ((Object) this.b) + ", code=" + this.c + ", message=" + this.d + ")";
    }
}
