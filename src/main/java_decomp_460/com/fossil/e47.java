package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.d26;
import com.fossil.dk5;
import com.fossil.iq4;
import com.fossil.v36;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.setting.SpecialSkuSetting;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e47 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f880a;
    @DexIgnore
    public static /* final */ e47 b; // = new e47();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2", f = "NotificationAppHelper.kt", l = {150, 151}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super List<AppNotificationFilter>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ d26 $getAllContactGroup;
        @DexIgnore
        public /* final */ /* synthetic */ v36 $getApps;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSettingsDatabase $notificationSettingsDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ on5 $sharedPrefs;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.e47$a$a")
        @eo7(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$contactMessageDefer$1", f = "NotificationAppHelper.kt", l = {99}, m = "invokeSuspend")
        /* renamed from: com.fossil.e47$a$a  reason: collision with other inner class name */
        public static final class C0060a extends ko7 implements vp7<iv7, qn7<? super List<AppNotificationFilter>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ br7 $callSettingsType;
            @DexIgnore
            public /* final */ /* synthetic */ br7 $messageSettingsType;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0060a(a aVar, br7 br7, br7 br72, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
                this.$callSettingsType = br7;
                this.$messageSettingsType = br72;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0060a aVar = new C0060a(this.this$0, this.$callSettingsType, this.$messageSettingsType, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<AppNotificationFilter>> qn7) {
                return ((C0060a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX DEBUG: Failed to insert an additional move for type inference into block B:35:0x007c */
            /* JADX DEBUG: Multi-variable search result rejected for r2v2, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Object] */
            /* JADX WARN: Type inference failed for: r2v3, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r2v4 */
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object a2;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    ArrayList arrayList = new ArrayList();
                    d26 d26 = this.this$0.$getAllContactGroup;
                    this.L$0 = iv7;
                    this.L$1 = arrayList;
                    this.label = 1;
                    a2 = jq4.a(d26, null, this);
                    if (a2 != d) {
                        d = arrayList;
                    }
                    return d;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    d = (List) this.L$1;
                    a2 = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                iq4.c cVar = (iq4.c) a2;
                if (cVar instanceof d26.c) {
                    List j0 = pm7.j0(((d26.c) cVar).a());
                    int i2 = this.$callSettingsType.element;
                    if (i2 == 0) {
                        DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                        d.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
                    } else if (i2 == 1) {
                        int size = j0.size();
                        for (int i3 = 0; i3 < size; i3++) {
                            ContactGroup contactGroup = (ContactGroup) j0.get(i3);
                            DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType()));
                            List<Contact> contacts = contactGroup.getContacts();
                            pq7.b(contacts, "item.contacts");
                            if (!contacts.isEmpty()) {
                                Contact contact = contactGroup.getContacts().get(0);
                                pq7.b(contact, "item.contacts[0]");
                                appNotificationFilter.setSender(contact.getDisplayName());
                                d.add(appNotificationFilter);
                            }
                        }
                    }
                    int i4 = this.$messageSettingsType.element;
                    if (i4 == 0) {
                        DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                        d.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
                    } else if (i4 == 1) {
                        int size2 = j0.size();
                        for (int i5 = 0; i5 < size2; i5++) {
                            ContactGroup contactGroup2 = (ContactGroup) j0.get(i5);
                            DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                            FNotification fNotification = new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType());
                            List<Contact> contacts2 = contactGroup2.getContacts();
                            pq7.b(contacts2, "item.contacts");
                            if (!contacts2.isEmpty()) {
                                AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification);
                                Contact contact2 = contactGroup2.getContacts().get(0);
                                pq7.b(contact2, "item.contacts[0]");
                                appNotificationFilter2.setSender(contact2.getDisplayName());
                                d.add(appNotificationFilter2);
                            }
                        }
                    }
                }
                return d;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$otherAppsDefer$1", f = "NotificationAppHelper.kt", l = {143}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super List<AppNotificationFilter>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isAllAppToggleEnabled;
            @DexIgnore
            public /* final */ /* synthetic */ List $notificationAllFilterList;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(a aVar, List list, boolean z, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
                this.$notificationAllFilterList = list;
                this.$isAllAppToggleEnabled = z;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, this.$notificationAllFilterList, this.$isAllAppToggleEnabled, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<AppNotificationFilter>> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object a2;
                ArrayList d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    ArrayList arrayList = new ArrayList();
                    v36 v36 = this.this$0.$getApps;
                    this.L$0 = iv7;
                    this.L$1 = arrayList;
                    this.label = 1;
                    a2 = jq4.a(v36, null, this);
                    if (a2 != d) {
                        d = arrayList;
                    }
                    return d;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    d = (List) this.L$1;
                    a2 = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                iq4.c cVar = (iq4.c) a2;
                if (cVar instanceof v36.a) {
                    this.$notificationAllFilterList.addAll(d47.b(((v36.a) cVar).a(), this.$isAllAppToggleEnabled));
                }
                return d;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(on5 on5, NotificationSettingsDatabase notificationSettingsDatabase, d26 d26, v36 v36, qn7 qn7) {
            super(2, qn7);
            this.$sharedPrefs = on5;
            this.$notificationSettingsDatabase = notificationSettingsDatabase;
            this.$getAllContactGroup = d26;
            this.$getApps = v36;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.$sharedPrefs, this.$notificationSettingsDatabase, this.$getAllContactGroup, this.$getApps, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super List<AppNotificationFilter>> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r3v8, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:13:0x00e4  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x020f  */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r22) {
            /*
            // Method dump skipped, instructions count: 531
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.e47.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = e47.class.getSimpleName();
        pq7.b(simpleName, "NotificationAppHelper::class.java.simpleName");
        f880a = simpleName;
    }
    */

    @DexIgnore
    public final AppNotificationFilterSettings b(SparseArray<List<BaseFeatureModel>> sparseArray, boolean z) {
        ArrayList arrayList = new ArrayList();
        if (sparseArray != null) {
            SparseArray<List<BaseFeatureModel>> clone = sparseArray.clone();
            pq7.b(clone, "it.clone()");
            short s = -1;
            int size = clone.size();
            for (int i = 0; i < size; i++) {
                List<BaseFeatureModel> valueAt = clone.valueAt(i);
                if (valueAt != null) {
                    for (BaseFeatureModel baseFeatureModel : valueAt) {
                        if (baseFeatureModel.isEnabled()) {
                            short e = (short) ll5.e(baseFeatureModel.getHour());
                            if (baseFeatureModel instanceof ContactGroup) {
                                ContactGroup contactGroup = (ContactGroup) baseFeatureModel;
                                if (contactGroup.getContacts() != null) {
                                    for (Contact contact : contactGroup.getContacts()) {
                                        pq7.b(contact, "contact");
                                        if (contact.isUseCall()) {
                                            if (z) {
                                                s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForCall();
                                            }
                                            lo1 lo1 = new lo1(e, e, s, 10000);
                                            DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), "", phone_incoming_call.getNotificationType()));
                                            appNotificationFilter.setSender(contact.getDisplayName());
                                            appNotificationFilter.setHandMovingConfig(lo1);
                                            appNotificationFilter.setVibePattern(no1.CALL);
                                            arrayList.add(appNotificationFilter);
                                        }
                                        if (contact.isUseSms()) {
                                            if (z) {
                                                s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForSms();
                                            }
                                            lo1 lo12 = new lo1(e, e, s, 10000);
                                            DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                            AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType()));
                                            appNotificationFilter2.setSender(contact.getDisplayName());
                                            appNotificationFilter2.setHandMovingConfig(lo12);
                                            appNotificationFilter2.setVibePattern(no1.TEXT);
                                            arrayList.add(appNotificationFilter2);
                                        }
                                    }
                                }
                            } else if (baseFeatureModel instanceof AppFilter) {
                                AppFilter appFilter = (AppFilter) baseFeatureModel;
                                String type = appFilter.getType();
                                if (!(type == null || vt7.l(type))) {
                                    String name = appFilter.getName() == null ? "" : appFilter.getName();
                                    if (z) {
                                        s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForApp();
                                    }
                                    lo1 lo13 = new lo1(e, e, s, 10000);
                                    pq7.b(name, "appName");
                                    String type2 = appFilter.getType();
                                    pq7.b(type2, "item.type");
                                    AppNotificationFilter appNotificationFilter3 = new AppNotificationFilter(new FNotification(name, type2, "", NotificationBaseObj.ANotificationType.NOTIFICATION));
                                    appNotificationFilter3.setHandMovingConfig(lo13);
                                    appNotificationFilter3.setVibePattern(no1.DEFAULT_OTHER_APPS);
                                    arrayList.add(appNotificationFilter3);
                                }
                            }
                        }
                    }
                }
            }
        }
        FLogger.INSTANCE.getLocal().d(f880a, "buildAppNotificationFilterSettings size = " + arrayList.size());
        return new AppNotificationFilterSettings(arrayList, System.currentTimeMillis());
    }

    @DexIgnore
    public final Object c(v36 v36, d26 d26, NotificationSettingsDatabase notificationSettingsDatabase, on5 on5, qn7<? super List<AppNotificationFilter>> qn7) {
        return eu7.g(bw7.a(), new a(on5, notificationSettingsDatabase, d26, v36, null), qn7);
    }

    @DexIgnore
    public final List<AppNotificationFilter> d(Context context, NotificationSettingsDatabase notificationSettingsDatabase, on5 on5) {
        int i;
        int i2 = 0;
        pq7.c(context, "context");
        pq7.c(notificationSettingsDatabase, "notificationSettingsDatabase");
        pq7.c(on5, "sharedPrefs");
        ArrayList arrayList = new ArrayList();
        boolean W = on5.W();
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(true);
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData2 = notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
        FLogger.INSTANCE.getLocal().d(f880a, "buildNotificationAppFilters(), callSettingsTypeModel = " + notificationSettingsWithIsCallNoLiveData + ", messageSettingsTypeModel = " + notificationSettingsWithIsCallNoLiveData2 + ", isAllAppToggleEnabled = " + W);
        if (notificationSettingsWithIsCallNoLiveData != null) {
            i = notificationSettingsWithIsCallNoLiveData.getSettingsType();
        } else {
            notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowCallsFrom", 0, true));
            i = 0;
        }
        if (notificationSettingsWithIsCallNoLiveData2 != null) {
            i2 = notificationSettingsWithIsCallNoLiveData2.getSettingsType();
        } else {
            notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowMessagesFrom", 0, false));
        }
        List<AppNotificationFilter> h = h(i, i2);
        List<i06> g = g(context);
        arrayList.addAll(h);
        arrayList.addAll(d47.b(g, W));
        return arrayList;
    }

    @DexIgnore
    public final List<AppFilter> e(MFDeviceFamily mFDeviceFamily) {
        List<AppFilter> allAppFilters = mFDeviceFamily != null ? mn5.p.a().c().getAllAppFilters(mFDeviceFamily.ordinal()) : null;
        return allAppFilters == null ? new ArrayList() : allAppFilters;
    }

    @DexIgnore
    public final AppFilter f(String str, MFDeviceFamily mFDeviceFamily) {
        pq7.c(str, "type");
        if (TextUtils.isEmpty(str) || mFDeviceFamily == null) {
            return null;
        }
        return mn5.p.a().c().getAppFilterMatchingType(str, mFDeviceFamily.ordinal());
    }

    @DexIgnore
    public final List<i06> g(Context context) {
        List<AppFilter> allAppFilters = mn5.p.a().c().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<dk5.b> it = dk5.g.g().iterator();
        while (it.hasNext()) {
            dk5.b next = it.next();
            if (TextUtils.isEmpty(next.b()) || !vt7.j(next.b(), context.getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), Boolean.FALSE);
                Iterator<AppFilter> it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter next2 = it2.next();
                    pq7.b(next2, "appFilter");
                    if (pq7.a(next2.getType(), installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(next2.getDbRowId());
                        installedApp.setCurrentHandGroup(next2.getHour());
                        break;
                    }
                }
                i06 i06 = new i06();
                i06.setInstalledApp(installedApp);
                i06.setUri(next.c());
                i06.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(i06);
            }
        }
        return linkedList;
    }

    @DexIgnore
    public final List<AppNotificationFilter> h(int i, int i2) {
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = mn5.p.a().d().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (i == 0) {
            DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
            arrayList.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
        } else if (i == 1) {
            int size = allContactGroups.size();
            for (int i3 = 0; i3 < size; i3++) {
                ContactGroup contactGroup = allContactGroups.get(i3);
                DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                FNotification fNotification = new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType());
                pq7.b(contactGroup, "item");
                List<Contact> contacts = contactGroup.getContacts();
                pq7.b(contacts, "item.contacts");
                if (!contacts.isEmpty()) {
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification);
                    Contact contact = contactGroup.getContacts().get(0);
                    pq7.b(contact, "item.contacts[0]");
                    appNotificationFilter.setSender(contact.getDisplayName());
                    arrayList.add(appNotificationFilter);
                }
            }
        }
        if (i2 == 0) {
            DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
            arrayList.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
        } else if (i2 == 1) {
            int size2 = allContactGroups.size();
            for (int i4 = 0; i4 < size2; i4++) {
                ContactGroup contactGroup2 = allContactGroups.get(i4);
                DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                FNotification fNotification2 = new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType());
                pq7.b(contactGroup2, "item");
                List<Contact> contacts2 = contactGroup2.getContacts();
                pq7.b(contacts2, "item.contacts");
                if (!contacts2.isEmpty()) {
                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification2);
                    Contact contact2 = contactGroup2.getContacts().get(0);
                    pq7.b(contact2, "item.contacts[0]");
                    appNotificationFilter2.setSender(contact2.getDisplayName());
                    arrayList.add(appNotificationFilter2);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final boolean i(String str) {
        pq7.c(str, "type");
        return f(str, MFDeviceFamily.DEVICE_FAMILY_SAM) != null;
    }

    @DexIgnore
    public final boolean j(SKUModel sKUModel, String str) {
        SpecialSkuSetting.SpecialSku fromSerialNumber;
        pq7.c(str, "serial");
        if (sKUModel != null) {
            SpecialSkuSetting.SpecialSku.Companion companion = SpecialSkuSetting.SpecialSku.Companion;
            String sku = sKUModel.getSku();
            if (sku == null) {
                sku = "";
            }
            fromSerialNumber = companion.fromType(sku);
        } else {
            fromSerialNumber = SpecialSkuSetting.SpecialSku.Companion.fromSerialNumber(str);
        }
        return fromSerialNumber == SpecialSkuSetting.SpecialSku.MOVEMBER;
    }
}
