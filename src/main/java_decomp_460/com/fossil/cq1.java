package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cq1 extends jq1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<cq1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public cq1 createFromParcel(Parcel parcel) {
            return new cq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public cq1[] newArray(int i) {
            return new cq1[i];
        }
    }

    @DexIgnore
    public cq1(byte b, int i, String str) {
        super(np1.BUDDY_CHALLENGE_GET_INFO, b, i);
        this.e = str;
    }

    @DexIgnore
    public /* synthetic */ cq1(Parcel parcel, kq7 kq7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.e = readString;
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(cq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.e, ((cq1) obj).e) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.BuddyChallengeGetInfoRequest");
    }

    @DexIgnore
    public final String getChallengeId() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public int hashCode() {
        return (super.hashCode() * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.ox1, com.fossil.jq1
    public JSONObject toJSONObject() {
        return g80.k(super.toJSONObject(), jd0.D4, this.e);
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.e);
        }
    }
}
