package com.fossil;

import com.fossil.h34;
import com.fossil.u24;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class m34<E> extends n34<E> implements NavigableSet<E>, b54<E> {
    @DexIgnore
    public /* final */ transient Comparator<? super E> comparator;
    @DexIgnore
    @LazyInit
    public transient m34<E> descendingSet;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<E> extends h34.a<E> {
        @DexIgnore
        public /* final */ Comparator<? super E> c;

        @DexIgnore
        public a(Comparator<? super E> comparator) {
            i14.l(comparator);
            this.c = comparator;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.u24.b, com.fossil.h34.a
        public /* bridge */ /* synthetic */ u24.b a(Object obj) {
            k(obj);
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.h34.a
        public /* bridge */ /* synthetic */ h34.a g(Object obj) {
            k(obj);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.h34.a
        public /* bridge */ /* synthetic */ h34.a i(Iterator it) {
            m(it);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public a<E> k(E e) {
            super.a(e);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public a<E> l(E... eArr) {
            super.h(eArr);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public a<E> m(Iterator<? extends E> it) {
            super.i(it);
            return this;
        }

        @DexIgnore
        /* renamed from: n */
        public m34<E> j() {
            m34<E> construct = m34.construct(this.c, this.b, this.f3507a);
            this.b = construct.size();
            return construct;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<E> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Comparator<? super E> comparator;
        @DexIgnore
        public /* final */ Object[] elements;

        @DexIgnore
        public b(Comparator<? super E> comparator2, Object[] objArr) {
            this.comparator = comparator2;
            this.elements = objArr;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.m34$a */
        /* JADX WARN: Multi-variable type inference failed */
        public Object readResolve() {
            a aVar = new a(this.comparator);
            aVar.l(this.elements);
            return aVar.j();
        }
    }

    @DexIgnore
    public m34(Comparator<? super E> comparator2) {
        this.comparator = comparator2;
    }

    @DexIgnore
    public static <E> m34<E> construct(Comparator<? super E> comparator2, int i, E... eArr) {
        if (i == 0) {
            return emptySet(comparator2);
        }
        h44.d(eArr, i);
        Arrays.sort(eArr, 0, i, comparator2);
        int i2 = 1;
        for (int i3 = 1; i3 < i; i3++) {
            E e = eArr[i3];
            if (comparator2.compare(e, eArr[i2 - 1]) != 0) {
                eArr[i2] = e;
                i2++;
            }
        }
        Arrays.fill(eArr, i2, i, (Object) null);
        return new s44(y24.asImmutableList(eArr, i2), comparator2);
    }

    @DexIgnore
    public static <E> m34<E> copyOf(Iterable<? extends E> iterable) {
        return copyOf(i44.natural(), iterable);
    }

    @DexIgnore
    public static <E> m34<E> copyOf(Collection<? extends E> collection) {
        return copyOf((Comparator) i44.natural(), (Collection) collection);
    }

    @DexIgnore
    public static <E> m34<E> copyOf(Comparator<? super E> comparator2, Iterable<? extends E> iterable) {
        i14.l(comparator2);
        if (c54.b(comparator2, iterable) && (iterable instanceof m34)) {
            m34<E> m34 = (m34) iterable;
            if (!m34.isPartialView()) {
                return m34;
            }
        }
        Object[] h = o34.h(iterable);
        return construct(comparator2, h.length, h);
    }

    @DexIgnore
    public static <E> m34<E> copyOf(Comparator<? super E> comparator2, Collection<? extends E> collection) {
        return copyOf((Comparator) comparator2, (Iterable) collection);
    }

    @DexIgnore
    public static <E> m34<E> copyOf(Comparator<? super E> comparator2, Iterator<? extends E> it) {
        a aVar = new a(comparator2);
        aVar.m(it);
        return aVar.j();
    }

    @DexIgnore
    public static <E> m34<E> copyOf(Iterator<? extends E> it) {
        return copyOf(i44.natural(), it);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> m34<E> copyOf(E[] eArr) {
        return construct(i44.natural(), eArr.length, (Object[]) eArr.clone());
    }

    @DexIgnore
    public static <E> m34<E> copyOfSorted(SortedSet<E> sortedSet) {
        Comparator a2 = c54.a(sortedSet);
        y24 copyOf = y24.copyOf((Collection) sortedSet);
        return copyOf.isEmpty() ? emptySet(a2) : new s44(copyOf, a2);
    }

    @DexIgnore
    public static <E> s44<E> emptySet(Comparator<? super E> comparator2) {
        return i44.natural().equals(comparator2) ? (s44<E>) s44.NATURAL_EMPTY_SET : new s44<>(y24.of(), comparator2);
    }

    @DexIgnore
    public static <E extends Comparable<?>> a<E> naturalOrder() {
        return new a<>(i44.natural());
    }

    @DexIgnore
    public static <E> m34<E> of() {
        return s44.NATURAL_EMPTY_SET;
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> m34<E> of(E e) {
        return new s44(y24.of((Object) e), i44.natural());
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> m34<E> of(E e, E e2) {
        return construct(i44.natural(), 2, e, e2);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> m34<E> of(E e, E e2, E e3) {
        return construct(i44.natural(), 3, e, e2, e3);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> m34<E> of(E e, E e2, E e3, E e4) {
        return construct(i44.natural(), 4, e, e2, e3, e4);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> m34<E> of(E e, E e2, E e3, E e4, E e5) {
        return construct(i44.natural(), 5, e, e2, e3, e4, e5);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> m34<E> of(E e, E e2, E e3, E e4, E e5, E e6, E... eArr) {
        int length = eArr.length + 6;
        Comparable[] comparableArr = new Comparable[length];
        comparableArr[0] = e;
        comparableArr[1] = e2;
        comparableArr[2] = e3;
        comparableArr[3] = e4;
        comparableArr[4] = e5;
        comparableArr[5] = e6;
        System.arraycopy(eArr, 0, comparableArr, 6, eArr.length);
        return construct(i44.natural(), length, comparableArr);
    }

    @DexIgnore
    public static <E> a<E> orderedBy(Comparator<E> comparator2) {
        return new a<>(comparator2);
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    @DexIgnore
    public static <E extends Comparable<?>> a<E> reverseOrder() {
        return new a<>(i44.natural().reverse());
    }

    @DexIgnore
    public static int unsafeCompare(Comparator<?> comparator2, Object obj, Object obj2) {
        return comparator2.compare(obj, obj2);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E ceiling(E e) {
        return (E) o34.e(tailSet((m34<E>) e, true), null);
    }

    @DexIgnore
    @Override // java.util.SortedSet, com.fossil.b54
    public Comparator<? super E> comparator() {
        return this.comparator;
    }

    @DexIgnore
    public m34<E> createDescendingSet() {
        return new f24(this);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public abstract h54<E> descendingIterator();

    @DexIgnore
    @Override // java.util.NavigableSet
    public m34<E> descendingSet() {
        m34<E> m34 = this.descendingSet;
        if (m34 != null) {
            return m34;
        }
        m34<E> createDescendingSet = createDescendingSet();
        this.descendingSet = createDescendingSet;
        createDescendingSet.descendingSet = this;
        return createDescendingSet;
    }

    @DexIgnore
    @Override // java.util.SortedSet
    public E first() {
        return iterator().next();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E floor(E e) {
        return (E) p34.n(headSet((m34<E>) e, true).descendingIterator(), null);
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public m34<E> headSet(E e) {
        return headSet((m34<E>) e, false);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public m34<E> headSet(E e, boolean z) {
        i14.l(e);
        return headSetImpl(e, z);
    }

    @DexIgnore
    public abstract m34<E> headSetImpl(E e, boolean z);

    @DexIgnore
    @Override // java.util.NavigableSet
    public E higher(E e) {
        return (E) o34.e(tailSet((m34<E>) e, false), null);
    }

    @DexIgnore
    public abstract int indexOf(Object obj);

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.u24, com.fossil.u24, java.util.NavigableSet, com.fossil.h34, com.fossil.h34, java.lang.Iterable
    public abstract h54<E> iterator();

    @DexIgnore
    @Override // java.util.SortedSet
    public E last() {
        return descendingIterator().next();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E lower(E e) {
        return (E) p34.n(headSet((m34<E>) e, false).descendingIterator(), null);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    @CanIgnoreReturnValue
    @Deprecated
    public final E pollFirst() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    @CanIgnoreReturnValue
    @Deprecated
    public final E pollLast() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public m34<E> subSet(E e, E e2) {
        return subSet((boolean) e, true, (boolean) e2, false);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public m34<E> subSet(E e, boolean z, E e2, boolean z2) {
        i14.l(e);
        i14.l(e2);
        i14.d(this.comparator.compare(e, e2) <= 0);
        return subSetImpl(e, z, e2, z2);
    }

    @DexIgnore
    public abstract m34<E> subSetImpl(E e, boolean z, E e2, boolean z2);

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public m34<E> tailSet(E e) {
        return tailSet((m34<E>) e, true);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public m34<E> tailSet(E e, boolean z) {
        i14.l(e);
        return tailSetImpl(e, z);
    }

    @DexIgnore
    public abstract m34<E> tailSetImpl(E e, boolean z);

    @DexIgnore
    public int unsafeCompare(Object obj, Object obj2) {
        return unsafeCompare(this.comparator, obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.u24, com.fossil.h34
    public Object writeReplace() {
        return new b(this.comparator, toArray());
    }
}
