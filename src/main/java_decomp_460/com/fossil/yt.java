package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum yt {
    GET((byte) 1),
    SET((byte) 2),
    RESPONSE((byte) 3);
    
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public yt(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
