package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface qo5 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    mo5 b(String str);

    @DexIgnore
    void c(mo5 mo5);

    @DexIgnore
    List<mo5> d(String str);

    @DexIgnore
    void e(String str);

    @DexIgnore
    List<mo5> f(String str);

    @DexIgnore
    void g(String str);

    @DexIgnore
    mo5 getPresetById(String str);

    @DexIgnore
    int h(String str);

    @DexIgnore
    void i(String str, int i);

    @DexIgnore
    Long[] insert(List<mo5> list);

    @DexIgnore
    void j(List<mo5> list);

    @DexIgnore
    mo5 k();

    @DexIgnore
    long l(mo5 mo5);

    @DexIgnore
    LiveData<List<mo5>> m(String str);
}
