package com.fossil;

import java.util.ListIterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b43 implements ListIterator<String> {
    @DexIgnore
    public ListIterator<String> b; // = this.d.b.listIterator(this.c);
    @DexIgnore
    public /* final */ /* synthetic */ int c;
    @DexIgnore
    public /* final */ /* synthetic */ y33 d;

    @DexIgnore
    public b43(y33 y33, int i) {
        this.d = y33;
        this.c = i;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.ListIterator
    public final /* synthetic */ void add(String str) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.b.hasNext();
    }

    @DexIgnore
    public final boolean hasPrevious() {
        return this.b.hasPrevious();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Iterator, java.util.ListIterator
    public final /* synthetic */ String next() {
        return this.b.next();
    }

    @DexIgnore
    public final int nextIndex() {
        return this.b.nextIndex();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.ListIterator
    public final /* synthetic */ String previous() {
        return this.b.previous();
    }

    @DexIgnore
    public final int previousIndex() {
        return this.b.previousIndex();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.ListIterator
    public final /* synthetic */ void set(String str) {
        throw new UnsupportedOperationException();
    }
}
