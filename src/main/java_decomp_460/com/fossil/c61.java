package com.fossil;

import android.graphics.drawable.Drawable;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c61 extends d61 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Drawable f565a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ q51 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c61(Drawable drawable, boolean z, q51 q51) {
        super(null);
        pq7.c(drawable, ResourceManager.DRAWABLE);
        pq7.c(q51, "dataSource");
        this.f565a = drawable;
        this.b = z;
        this.c = q51;
    }

    @DexIgnore
    public static /* synthetic */ c61 e(c61 c61, Drawable drawable, boolean z, q51 q51, int i, Object obj) {
        if ((i & 1) != 0) {
            drawable = c61.f565a;
        }
        if ((i & 2) != 0) {
            z = c61.b;
        }
        if ((i & 4) != 0) {
            q51 = c61.c;
        }
        return c61.d(drawable, z, q51);
    }

    @DexIgnore
    public final Drawable a() {
        return this.f565a;
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public final q51 c() {
        return this.c;
    }

    @DexIgnore
    public final c61 d(Drawable drawable, boolean z, q51 q51) {
        pq7.c(drawable, ResourceManager.DRAWABLE);
        pq7.c(q51, "dataSource");
        return new c61(drawable, z, q51);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof c61) {
                c61 c61 = (c61) obj;
                if (!pq7.a(this.f565a, c61.f565a) || this.b != c61.b || !pq7.a(this.c, c61.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Drawable f() {
        return this.f565a;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Drawable drawable = this.f565a;
        int hashCode = drawable != null ? drawable.hashCode() : 0;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        q51 q51 = this.c;
        if (q51 != null) {
            i = q51.hashCode();
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((hashCode * 31) + i2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "DrawableResult(drawable=" + this.f565a + ", isSampled=" + this.b + ", dataSource=" + this.c + ")";
    }
}
