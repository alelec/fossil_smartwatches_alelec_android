package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.diana.workout.WorkoutSessionUpdateWrapper;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kn6 extends ts0 {
    @DexIgnore
    public static /* final */ String e;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public WorkoutSession f1937a;
    @DexIgnore
    public WorkoutSession b;
    @DexIgnore
    public MutableLiveData<a> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ WorkoutSessionRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public List<oi5> f1938a;
        @DexIgnore
        public WorkoutSession b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public cl7<Integer, String> d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public a(List<oi5> list, WorkoutSession workoutSession, Boolean bool, cl7<Integer, String> cl7, Boolean bool2, boolean z) {
            this.f1938a = list;
            this.b = workoutSession;
            this.c = bool;
            this.d = cl7;
            this.e = bool2;
            this.f = z;
        }

        @DexIgnore
        public final boolean a() {
            return this.f;
        }

        @DexIgnore
        public final cl7<Integer, String> b() {
            return this.d;
        }

        @DexIgnore
        public final Boolean c() {
            return this.e;
        }

        @DexIgnore
        public final WorkoutSession d() {
            return this.b;
        }

        @DexIgnore
        public final List<oi5> e() {
            return this.f1938a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!pq7.a(this.f1938a, aVar.f1938a) || !pq7.a(this.b, aVar.b) || !pq7.a(this.c, aVar.c) || !pq7.a(this.d, aVar.d) || !pq7.a(this.e, aVar.e) || this.f != aVar.f) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final Boolean f() {
            return this.c;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            List<oi5> list = this.f1938a;
            int hashCode = list != null ? list.hashCode() : 0;
            WorkoutSession workoutSession = this.b;
            int hashCode2 = workoutSession != null ? workoutSession.hashCode() : 0;
            Boolean bool = this.c;
            int hashCode3 = bool != null ? bool.hashCode() : 0;
            cl7<Integer, String> cl7 = this.d;
            int hashCode4 = cl7 != null ? cl7.hashCode() : 0;
            Boolean bool2 = this.e;
            if (bool2 != null) {
                i = bool2.hashCode();
            }
            boolean z = this.f;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            return (((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i) * 31) + i2;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(workoutWrapperTypes=" + this.f1938a + ", workoutSession=" + this.b + ", isWorkoutSessionChanged=" + this.c + ", showServerError=" + this.d + ", showSuccess=" + this.e + ", showLoading=" + this.f + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.workout.WorkoutEditViewModel$start$1", f = "WorkoutEditViewModel.kt", l = {40}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $workoutSessionId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kn6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.workout.WorkoutEditViewModel$start$1$2", f = "WorkoutEditViewModel.kt", l = {41}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object workoutSessionById;
                kn6 kn6;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    kn6 kn62 = this.this$0.this$0;
                    WorkoutSessionRepository workoutSessionRepository = kn62.d;
                    String str = this.this$0.$workoutSessionId;
                    this.L$0 = iv7;
                    this.L$1 = kn62;
                    this.label = 1;
                    workoutSessionById = workoutSessionRepository.getWorkoutSessionById(str, this);
                    if (workoutSessionById == d) {
                        return d;
                    }
                    kn6 = kn62;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    workoutSessionById = obj;
                    kn6 = (kn6) this.L$1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                kn6.f1937a = (WorkoutSession) workoutSessionById;
                kn6 kn63 = this.this$0.this$0;
                WorkoutSession workoutSession = kn63.f1937a;
                kn63.b = workoutSession != null ? WorkoutSession.copy$default(workoutSession, null, null, null, null, null, null, null, null, null, null, null, 0, 0, 0, 0, null, null, null, null, null, null, null, null, null, 16777215, null) : null;
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(kn6 kn6, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = kn6;
            this.$workoutSessionId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$workoutSessionId, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                kn6 kn6 = this.this$0;
                oi5[] values = oi5.values();
                ArrayList arrayList = new ArrayList();
                int length = values.length;
                for (int i2 = 0; i2 < length; i2++) {
                    oi5 oi5 = values[i2];
                    if (ao7.a(oi5 != oi5.UNKNOWN).booleanValue()) {
                        arrayList.add(oi5);
                    }
                }
                kn6.g(kn6, pm7.j0(arrayList), null, null, null, null, true, 30, null);
                dv7 a2 = bw7.a();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(a2, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            kn6 kn62 = this.this$0;
            kn6.g(kn62, null, kn62.b, null, null, null, false, 29, null);
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kn6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object updateWorkoutSession;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    WorkoutSession workoutSession = this.this$0.this$0.b;
                    if (workoutSession != null) {
                        WorkoutSessionUpdateWrapper workoutSessionUpdateWrapper = new WorkoutSessionUpdateWrapper(workoutSession);
                        WorkoutSessionRepository workoutSessionRepository = this.this$0.this$0.d;
                        List<WorkoutSessionUpdateWrapper> j0 = pm7.j0(hm7.c(workoutSessionUpdateWrapper));
                        this.L$0 = iv7;
                        this.L$1 = workoutSessionUpdateWrapper;
                        this.label = 1;
                        updateWorkoutSession = workoutSessionRepository.updateWorkoutSession(j0, this);
                        if (updateWorkoutSession == d) {
                            return d;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else if (i == 1) {
                    WorkoutSessionUpdateWrapper workoutSessionUpdateWrapper2 = (WorkoutSessionUpdateWrapper) this.L$1;
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    updateWorkoutSession = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                kz4 kz4 = (kz4) updateWorkoutSession;
                if (kz4.c() != null) {
                    kn6.g(this.this$0.this$0, null, null, null, null, ao7.a(true), false, 15, null);
                } else {
                    ServerError a2 = kz4.a();
                    if (a2 != null) {
                        kn6.g(this.this$0.this$0, null, null, null, new cl7(a2.getCode(), ""), ao7.a(false), false, 7, null);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(qn7 qn7, kn6 kn6) {
            super(2, qn7);
            this.this$0 = kn6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(qn7, this.this$0);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(b, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = kn6.class.getSimpleName();
        pq7.b(simpleName, "WorkoutEditViewModel::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public kn6(WorkoutSessionRepository workoutSessionRepository) {
        pq7.c(workoutSessionRepository, "mWorkoutSessionRepository");
        this.d = workoutSessionRepository;
    }

    @DexIgnore
    public static /* synthetic */ void g(kn6 kn6, List list, WorkoutSession workoutSession, Boolean bool, cl7 cl7, Boolean bool2, boolean z, int i, Object obj) {
        Boolean bool3 = null;
        List list2 = (i & 1) != 0 ? null : list;
        WorkoutSession workoutSession2 = (i & 2) != 0 ? null : workoutSession;
        Boolean bool4 = (i & 4) != 0 ? null : bool;
        cl7 cl72 = (i & 8) != 0 ? null : cl7;
        if ((i & 16) == 0) {
            bool3 = bool2;
        }
        kn6.f(list2, workoutSession2, bool4, cl72, bool3, (i & 32) != 0 ? false : z);
    }

    @DexIgnore
    public final void f(List<oi5> list, WorkoutSession workoutSession, Boolean bool, cl7<Integer, String> cl7, Boolean bool2, boolean z) {
        this.c.l(new a(list, workoutSession, bool, cl7, bool2, z));
    }

    @DexIgnore
    public final cl7<ii5, ii5> h() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession == null) {
            return null;
        }
        DateTime editedEndTime = workoutSession.getEditedEndTime();
        if (editedEndTime != null) {
            long millis = editedEndTime.getMillis();
            DateTime editedStartTime = workoutSession.getEditedStartTime();
            if (editedStartTime != null) {
                gl7<Integer, Integer, Integer> e0 = lk5.e0((int) ((millis - editedStartTime.getMillis()) / ((long) 1000)));
                pq7.b(e0, "DateHelper.getTimeValues(duration.toInt())");
                Integer first = e0.getFirst();
                pq7.b(first, "timeValues.first");
                ii5 ii5 = new ii5(0, 23, first.intValue(), 1.0f);
                Integer second = e0.getSecond();
                pq7.b(second, "timeValues.second");
                return new cl7<>(ii5, new ii5(0, 59, second.intValue(), 1.0f));
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final cl7<ii5, ii5> i() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession == null) {
            return null;
        }
        Calendar instance = Calendar.getInstance();
        TimeZone timeZone = TimeZone.getDefault();
        pq7.b(timeZone, "timezone");
        timeZone.setRawOffset(workoutSession.getTimezoneOffsetInSecond() * 1000);
        pq7.b(instance, "calendar");
        instance.setTimeZone(timeZone);
        DateTime editedStartTime = workoutSession.getEditedStartTime();
        if (editedStartTime != null) {
            instance.setTimeInMillis(editedStartTime.getMillis());
            return new cl7<>(new ii5(0, 23, instance.get(11), 1.0f), new ii5(0, 59, instance.get(12), 1.0f));
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final MutableLiveData<a> j() {
        return this.c;
    }

    @DexIgnore
    public final Long k() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            return Long.valueOf(workoutSession.getDate().getTime());
        }
        return null;
    }

    @DexIgnore
    public final Long l() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            return Long.valueOf(workoutSession.getEditedEndTime().getMillis() - workoutSession.getEditedStartTime().getMillis());
        }
        return null;
    }

    @DexIgnore
    public final Long m() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            return Long.valueOf(workoutSession.getEditedStartTime().getMillis());
        }
        return null;
    }

    @DexIgnore
    public final boolean n() {
        Long l = null;
        WorkoutSession workoutSession = this.f1937a;
        if (workoutSession == null || this.b == null) {
            return false;
        }
        if (workoutSession != null) {
            mi5 editedType = workoutSession.getEditedType();
            Integer valueOf = editedType != null ? Integer.valueOf(editedType.ordinal()) : null;
            WorkoutSession workoutSession2 = this.b;
            if (workoutSession2 != null) {
                mi5 editedType2 = workoutSession2.getEditedType();
                if (!(!pq7.a(valueOf, editedType2 != null ? Integer.valueOf(editedType2.ordinal()) : null))) {
                    WorkoutSession workoutSession3 = this.f1937a;
                    if (workoutSession3 != null) {
                        gi5 editedMode = workoutSession3.getEditedMode();
                        Integer valueOf2 = editedMode != null ? Integer.valueOf(editedMode.ordinal()) : null;
                        WorkoutSession workoutSession4 = this.b;
                        if (workoutSession4 != null) {
                            gi5 editedMode2 = workoutSession4.getEditedMode();
                            if (!(!pq7.a(valueOf2, editedMode2 != null ? Integer.valueOf(editedMode2.ordinal()) : null))) {
                                WorkoutSession workoutSession5 = this.f1937a;
                                if (workoutSession5 != null) {
                                    DateTime editedStartTime = workoutSession5.getEditedStartTime();
                                    if (editedStartTime != null) {
                                        long millis = editedStartTime.getMillis();
                                        WorkoutSession workoutSession6 = this.b;
                                        if (workoutSession6 != null) {
                                            DateTime editedStartTime2 = workoutSession6.getEditedStartTime();
                                            if (millis == (editedStartTime2 != null ? Long.valueOf(editedStartTime2.getMillis()) : null).longValue()) {
                                                WorkoutSession workoutSession7 = this.f1937a;
                                                if (workoutSession7 != null) {
                                                    DateTime editedEndTime = workoutSession7.getEditedEndTime();
                                                    if (editedEndTime != null) {
                                                        long millis2 = editedEndTime.getMillis();
                                                        WorkoutSession workoutSession8 = this.b;
                                                        if (workoutSession8 != null) {
                                                            DateTime editedEndTime2 = workoutSession8.getEditedEndTime();
                                                            if (editedEndTime2 != null) {
                                                                l = Long.valueOf(editedEndTime2.getMillis());
                                                            }
                                                            if (millis2 == l.longValue()) {
                                                                return false;
                                                            }
                                                        } else {
                                                            pq7.i();
                                                            throw null;
                                                        }
                                                    }
                                                } else {
                                                    pq7.i();
                                                    throw null;
                                                }
                                            }
                                        } else {
                                            pq7.i();
                                            throw null;
                                        }
                                    }
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                return true;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void o(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onCaloriesChanged calories=" + i);
        WorkoutSession workoutSession = this.b;
    }

    @DexIgnore
    public final void p(double d2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onDistanceChanged distance=" + d2);
        WorkoutSession workoutSession = this.b;
    }

    @DexIgnore
    public final void q(int i, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onDurationChanged hour=" + i + " min=" + i2);
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            Calendar instance = Calendar.getInstance();
            TimeZone timeZone = TimeZone.getDefault();
            pq7.b(timeZone, "timezone");
            timeZone.setRawOffset(workoutSession.getTimezoneOffsetInSecond() * 1000);
            pq7.b(instance, "calendar");
            instance.setTimeZone(timeZone);
            DateTime editedStartTime = workoutSession.getEditedStartTime();
            if (editedStartTime != null) {
                instance.setTimeInMillis(editedStartTime.getMillis());
                instance.add(12, (i * 60) + i2);
                workoutSession.setEditedEndTime(new DateTime(instance.getTimeInMillis()));
                g(this, null, workoutSession, Boolean.valueOf(n()), null, null, false, 57, null);
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void r(int i, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onStartTimeChanged hour=" + i + " min=" + i2);
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            DateTime editedEndTime = workoutSession.getEditedEndTime();
            if (editedEndTime != null) {
                long millis = editedEndTime.getMillis();
                DateTime editedStartTime = workoutSession.getEditedStartTime();
                if (editedStartTime != null) {
                    long millis2 = (millis - editedStartTime.getMillis()) / ((long) 1000);
                    Calendar instance = Calendar.getInstance();
                    TimeZone timeZone = TimeZone.getDefault();
                    pq7.b(timeZone, "timezone");
                    timeZone.setRawOffset(workoutSession.getTimezoneOffsetInSecond() * 1000);
                    pq7.b(instance, "calendar");
                    instance.setTimeZone(timeZone);
                    DateTime editedStartTime2 = workoutSession.getEditedStartTime();
                    if (editedStartTime2 != null) {
                        instance.setTimeInMillis(editedStartTime2.getMillis());
                        instance.set(11, i);
                        instance.set(12, i2);
                        workoutSession.setEditedStartTime(new DateTime(instance.getTimeInMillis()));
                        instance.add(13, (int) millis2);
                        workoutSession.setEditedEndTime(new DateTime(instance.getTimeInMillis()));
                        g(this, null, this.b, Boolean.valueOf(n()), null, null, false, 57, null);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void s(oi5 oi5) {
        pq7.c(oi5, "workoutWrapperTypes");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onWorkoutTypeChanged workoutTypes=" + oi5);
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            workoutSession.setEditedType(oi5.Companion.c(oi5));
            workoutSession.setEditedMode(oi5.Companion.b(oi5));
            if (workoutSession.getEditedMode() == null) {
                gi5 mode = workoutSession.getMode();
                if (mode == null) {
                    mode = gi5.INDOOR;
                }
                workoutSession.setEditedMode(mode);
            }
            g(this, null, this.b, Boolean.valueOf(n()), null, null, false, 57, null);
        }
    }

    @DexIgnore
    public final void t(String str) {
        pq7.c(str, "workoutSessionId");
        xw7 unused = gu7.d(us0.a(this), null, null, new b(this, str, null), 3, null);
    }

    @DexIgnore
    public final void u() {
        if (this.b != null) {
            g(this, null, null, null, null, null, true, 31, null);
            xw7 unused = gu7.d(us0.a(this), null, null, new c(null, this), 3, null);
            g(this, null, null, null, null, null, true, 31, null);
        }
    }
}
