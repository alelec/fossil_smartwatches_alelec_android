package com.fossil;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r13<K> implements Iterator<Map.Entry<K, Object>> {
    @DexIgnore
    public Iterator<Map.Entry<K, Object>> b;

    @DexIgnore
    public r13(Iterator<Map.Entry<K, Object>> it) {
        this.b = it;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.b.hasNext();
    }

    @DexIgnore
    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        Map.Entry<K, Object> next = this.b.next();
        return next.getValue() instanceof q13 ? new s13(next) : next;
    }

    @DexIgnore
    public final void remove() {
        this.b.remove();
    }
}
