package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.LinkedHashSet;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class np extends lp {
    @DexIgnore
    public static /* final */ rm D; // = new rm(null);
    @DexIgnore
    public /* final */ gv1 C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ np(k5 k5Var, i60 i60, gv1 gv1, String str, int i) {
        super(k5Var, i60, yp.a0, (i & 8) != 0 ? e.a("UUID.randomUUID().toString()") : str, false, 16);
        this.C = gv1;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        boolean z = false;
        if (!(this.C.getPresetItems().length == 0)) {
            rl1[] H = H();
            if (H.length == 0) {
                z = true;
            }
            if (!z) {
                try {
                    rm rmVar = D;
                    ry1 ry1 = this.x.a().h().get(Short.valueOf(ob.ASSET.b));
                    if (ry1 == null) {
                        ry1 = hd0.y.d();
                    }
                    lp.h(this, new zj(this.w, this.x, yp.F, true, 1792, rmVar.a(H, 1792, ry1), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), new dn(this), new pn(this), new bo(this), null, null, 48, null);
                } catch (sx1 e) {
                    d90.i.i(e);
                    l(nr.a(this.v, null, zq.UNSUPPORTED_FORMAT, null, null, 13));
                }
            } else {
                I();
            }
        } else {
            l(nr.a(this.v, null, zq.SUCCESS, null, null, 13));
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(super.C(), jd0.g3, this.C.toJSONObject());
    }

    @DexIgnore
    public final rl1[] H() {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        hv1[] presetItems = this.C.getPresetItems();
        for (hv1 hv1 : presetItems) {
            if (hv1 instanceof sl1) {
                linkedHashSet.addAll(((sl1) hv1).c());
            }
        }
        Object[] array = pm7.C(linkedHashSet).toArray(new rl1[0]);
        if (array != null) {
            return (rl1[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void I() {
        JSONObject jSONObject;
        k5 k5Var = this.w;
        i60 i60 = this.x;
        yp ypVar = yp.b0;
        try {
            ry1 ry1 = i60.a().h().get(Short.valueOf(ob.ASSET.b));
            ry1 d = ry1 != null ? ry1 : hd0.y.d();
            pq7.b(d, "delegate.deviceInformati\u2026tant.DEFAULT_FILE_VERSION");
            JSONObject jSONObject2 = new JSONObject();
            hv1[] presetItems = this.C.getPresetItems();
            for (hv1 hv1 : presetItems) {
                hv1.a(d);
                jSONObject2 = gy1.c(jSONObject2, hv1.a());
            }
            jSONObject = new JSONObject().put("push", new JSONObject().put("set", jSONObject2));
            pq7.b(jSONObject, "JSONObject().put(UIScrip\u2026T, assignmentJsonObject))");
        } catch (JSONException e) {
            d90.i.i(e);
            jSONObject = new JSONObject();
        }
        lp.h(this, new rp(k5Var, i60, ypVar, jSONObject, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 48), new no(this), new ap(this), null, null, null, 56, null);
    }
}
