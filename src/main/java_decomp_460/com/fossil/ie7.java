package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ie7 extends je7 {
    @DexIgnore
    <T> T a(String str);

    @DexIgnore
    be7 b();

    @DexIgnore
    boolean c();

    @DexIgnore
    Boolean d();
}
