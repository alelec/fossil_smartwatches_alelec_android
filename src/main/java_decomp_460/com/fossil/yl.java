package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yl extends bi {
    @DexIgnore
    public yl(k5 k5Var, i60 i60, yp ypVar, HashMap<hu1, Object> hashMap, String str) {
        super(k5Var, i60, ypVar, ke.b.a(k5Var.x, ob.DATA_COLLECTION_FILE), false, hashMap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 80);
    }

    @DexIgnore
    @Override // com.fossil.bi, com.fossil.lp
    public JSONObject E() {
        JSONArray jSONArray = new JSONArray();
        Iterator<j0> it = this.I.iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next().a(false));
        }
        JSONObject put = super.E().put(ey1.a(hu1.SKIP_ERASE), this.Q);
        pq7.b(put, "super.resultDescription(\u2026lowerCaseName, skipErase)");
        return g80.k(put, jd0.P2, jSONArray);
    }

    @DexIgnore
    /* renamed from: a0 */
    public byte[][] x() {
        ArrayList<j0> arrayList = this.I;
        ArrayList arrayList2 = new ArrayList(im7.m(arrayList, 10));
        Iterator<T> it = arrayList.iterator();
        while (it.hasNext()) {
            arrayList2.add(it.next().f);
        }
        Object[] array = arrayList2.toArray(new byte[0][]);
        if (array != null) {
            return (byte[][]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
