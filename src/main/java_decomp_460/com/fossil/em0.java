package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.util.Log;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class em0 extends dm0 {
    @DexIgnore
    public static Method i;

    @DexIgnore
    public em0(Drawable drawable) {
        super(drawable);
        g();
    }

    @DexIgnore
    public em0(fm0 fm0, Resources resources) {
        super(fm0, resources);
        g();
    }

    @DexIgnore
    @Override // com.fossil.dm0
    public boolean c() {
        if (Build.VERSION.SDK_INT != 21) {
            return false;
        }
        Drawable drawable = this.g;
        return (drawable instanceof GradientDrawable) || (drawable instanceof DrawableContainer) || (drawable instanceof InsetDrawable) || (drawable instanceof RippleDrawable);
    }

    @DexIgnore
    public final void g() {
        if (i == null) {
            try {
                i = Drawable.class.getDeclaredMethod("isProjected", new Class[0]);
            } catch (Exception e) {
                Log.w("WrappedDrawableApi21", "Failed to retrieve Drawable#isProjected() method", e);
            }
        }
    }

    @DexIgnore
    public Rect getDirtyBounds() {
        return this.g.getDirtyBounds();
    }

    @DexIgnore
    public void getOutline(Outline outline) {
        this.g.getOutline(outline);
    }

    @DexIgnore
    public boolean isProjected() {
        Method method;
        Drawable drawable = this.g;
        if (!(drawable == null || (method = i) == null)) {
            try {
                return ((Boolean) method.invoke(drawable, new Object[0])).booleanValue();
            } catch (Exception e) {
                Log.w("WrappedDrawableApi21", "Error calling Drawable#isProjected() method", e);
            }
        }
        return false;
    }

    @DexIgnore
    public void setHotspot(float f, float f2) {
        this.g.setHotspot(f, f2);
    }

    @DexIgnore
    public void setHotspotBounds(int i2, int i3, int i4, int i5) {
        this.g.setHotspotBounds(i2, i3, i4, i5);
    }

    @DexIgnore
    @Override // com.fossil.dm0
    public boolean setState(int[] iArr) {
        if (!super.setState(iArr)) {
            return false;
        }
        invalidateSelf();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.bm0, com.fossil.dm0
    public void setTint(int i2) {
        if (c()) {
            super.setTint(i2);
        } else {
            this.g.setTint(i2);
        }
    }

    @DexIgnore
    @Override // com.fossil.bm0, com.fossil.dm0
    public void setTintList(ColorStateList colorStateList) {
        if (c()) {
            super.setTintList(colorStateList);
        } else {
            this.g.setTintList(colorStateList);
        }
    }

    @DexIgnore
    @Override // com.fossil.bm0, com.fossil.dm0
    public void setTintMode(PorterDuff.Mode mode) {
        if (c()) {
            super.setTintMode(mode);
        } else {
            this.g.setTintMode(mode);
        }
    }
}
