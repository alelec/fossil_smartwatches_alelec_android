package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.bb1;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fh1 implements qb1<ByteBuffer, hh1> {
    @DexIgnore
    public static /* final */ a f; // = new a();
    @DexIgnore
    public static /* final */ b g; // = new b();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f1128a;
    @DexIgnore
    public /* final */ List<ImageHeaderParser> b;
    @DexIgnore
    public /* final */ b c;
    @DexIgnore
    public /* final */ a d;
    @DexIgnore
    public /* final */ gh1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public bb1 a(bb1.a aVar, db1 db1, ByteBuffer byteBuffer, int i) {
            return new fb1(aVar, db1, byteBuffer, i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Queue<eb1> f1129a; // = jk1.f(0);

        @DexIgnore
        public eb1 a(ByteBuffer byteBuffer) {
            eb1 poll;
            synchronized (this) {
                poll = this.f1129a.poll();
                if (poll == null) {
                    poll = new eb1();
                }
                poll.p(byteBuffer);
            }
            return poll;
        }

        @DexIgnore
        public void b(eb1 eb1) {
            synchronized (this) {
                eb1.a();
                this.f1129a.offer(eb1);
            }
        }
    }

    @DexIgnore
    public fh1(Context context, List<ImageHeaderParser> list, rd1 rd1, od1 od1) {
        this(context, list, rd1, od1, g, f);
    }

    @DexIgnore
    public fh1(Context context, List<ImageHeaderParser> list, rd1 rd1, od1 od1, b bVar, a aVar) {
        this.f1128a = context.getApplicationContext();
        this.b = list;
        this.d = aVar;
        this.e = new gh1(rd1, od1);
        this.c = bVar;
    }

    @DexIgnore
    public static int e(db1 db1, int i, int i2) {
        int min = Math.min(db1.a() / i2, db1.d() / i);
        int max = Math.max(1, min == 0 ? 0 : Integer.highestOneBit(min));
        if (Log.isLoggable("BufferGifDecoder", 2) && max > 1) {
            Log.v("BufferGifDecoder", "Downsampling GIF, sampleSize: " + max + ", target dimens: [" + i + "x" + i2 + "], actual dimens: [" + db1.d() + "x" + db1.a() + "]");
        }
        return max;
    }

    @DexIgnore
    public final jh1 c(ByteBuffer byteBuffer, int i, int i2, eb1 eb1, ob1 ob1) {
        long b2 = ek1.b();
        try {
            db1 c2 = eb1.c();
            if (c2.b() <= 0 || c2.c() != 0) {
            }
            Bitmap.Config config = ob1.c(nh1.f2521a) == hb1.PREFER_RGB_565 ? Bitmap.Config.RGB_565 : Bitmap.Config.ARGB_8888;
            bb1 a2 = this.d.a(this.e, c2, byteBuffer, e(c2, i, i2));
            a2.e(config);
            a2.b();
            Bitmap a3 = a2.a();
            if (a3 != null) {
                jh1 jh1 = new jh1(new hh1(this.f1128a, a2, tf1.c(), i, i2, a3));
                if (Log.isLoggable("BufferGifDecoder", 2)) {
                    Log.v("BufferGifDecoder", "Decoded GIF from stream in " + ek1.a(b2));
                }
                return jh1;
            } else if (!Log.isLoggable("BufferGifDecoder", 2)) {
                return null;
            } else {
                Log.v("BufferGifDecoder", "Decoded GIF from stream in " + ek1.a(b2));
                return null;
            }
        } finally {
            if (Log.isLoggable("BufferGifDecoder", 2)) {
                Log.v("BufferGifDecoder", "Decoded GIF from stream in " + ek1.a(b2));
            }
        }
    }

    @DexIgnore
    /* renamed from: d */
    public jh1 b(ByteBuffer byteBuffer, int i, int i2, ob1 ob1) {
        eb1 a2 = this.c.a(byteBuffer);
        try {
            return c(byteBuffer, i, i2, a2, ob1);
        } finally {
            this.c.b(a2);
        }
    }

    @DexIgnore
    /* renamed from: f */
    public boolean a(ByteBuffer byteBuffer, ob1 ob1) throws IOException {
        return !((Boolean) ob1.c(nh1.b)).booleanValue() && lb1.f(this.b, byteBuffer) == ImageHeaderParser.ImageType.GIF;
    }
}
