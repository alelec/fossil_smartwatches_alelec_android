package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.uirenew.customview.FriendsInView;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.uirenew.customview.TitleValueCell;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yb5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView A;
    @DexIgnore
    public /* final */ RTLImageView B;
    @DexIgnore
    public /* final */ RTLImageView C;
    @DexIgnore
    public /* final */ TitleValueCell D;
    @DexIgnore
    public /* final */ NestedScrollView E;
    @DexIgnore
    public /* final */ TitleValueCell F;
    @DexIgnore
    public /* final */ SwipeRefreshLayout G;
    @DexIgnore
    public /* final */ FlexibleTextView H;
    @DexIgnore
    public /* final */ TitleValueCell I;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ TitleValueCell s;
    @DexIgnore
    public /* final */ FriendsInView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ TimerTextView z;

    @DexIgnore
    public yb5(Object obj, View view, int i, FlexibleButton flexibleButton, ConstraintLayout constraintLayout, TitleValueCell titleValueCell, FriendsInView friendsInView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, TimerTextView timerTextView, ImageView imageView, RTLImageView rTLImageView, RTLImageView rTLImageView2, TitleValueCell titleValueCell2, NestedScrollView nestedScrollView, TitleValueCell titleValueCell3, SwipeRefreshLayout swipeRefreshLayout, FlexibleTextView flexibleTextView6, TitleValueCell titleValueCell4) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = constraintLayout;
        this.s = titleValueCell;
        this.t = friendsInView;
        this.u = flexibleTextView;
        this.v = flexibleTextView2;
        this.w = flexibleTextView3;
        this.x = flexibleTextView4;
        this.y = flexibleTextView5;
        this.z = timerTextView;
        this.A = imageView;
        this.B = rTLImageView;
        this.C = rTLImageView2;
        this.D = titleValueCell2;
        this.E = nestedScrollView;
        this.F = titleValueCell3;
        this.G = swipeRefreshLayout;
        this.H = flexibleTextView6;
        this.I = titleValueCell4;
    }
}
