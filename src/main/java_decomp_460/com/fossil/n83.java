package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n83 implements xw2<q83> {
    @DexIgnore
    public static n83 c; // = new n83();
    @DexIgnore
    public /* final */ xw2<q83> b;

    @DexIgnore
    public n83() {
        this(ww2.b(new p83()));
    }

    @DexIgnore
    public n83(xw2<q83> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((q83) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ q83 zza() {
        return this.b.zza();
    }
}
