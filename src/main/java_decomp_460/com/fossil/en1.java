package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class en1 extends ym1 {
    @DexIgnore
    public static /* final */ b CREATOR; // = new b(null);
    @DexIgnore
    public static /* final */ short i; // = hy1.c(fq7.f1179a);
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ byte e;
    @DexIgnore
    public /* final */ byte f;
    @DexIgnore
    public /* final */ short g;
    @DexIgnore
    public /* final */ a h;

    @DexIgnore
    public enum a {
        DISABLE((byte) 0),
        ENABLE((byte) 1);
        
        @DexIgnore
        public static /* final */ C0068a d; // = new C0068a(null);
        @DexIgnore
        public /* final */ byte b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.en1$a$a")
        /* renamed from: com.fossil.en1$a$a  reason: collision with other inner class name */
        public static final class C0068a {
            @DexIgnore
            public /* synthetic */ C0068a(kq7 kq7) {
            }

            @DexIgnore
            public final a a(byte b) throws IllegalArgumentException {
                a aVar;
                a[] values = a.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        aVar = null;
                        break;
                    }
                    aVar = values[i];
                    if (aVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (aVar != null) {
                    return aVar;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public a(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Parcelable.Creator<en1> {
        @DexIgnore
        public /* synthetic */ b(kq7 kq7) {
        }

        @DexIgnore
        public final en1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 6) {
                return new en1(bArr[0], bArr[1], bArr[2], bArr[3], hy1.p(bArr[4]), a.d.a(bArr[5]));
            }
            throw new IllegalArgumentException(e.b(e.e("Invalid data size: "), bArr.length, ", require: 6"));
        }

        @DexIgnore
        public en1 b(Parcel parcel) {
            return new en1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public en1 createFromParcel(Parcel parcel) {
            return new en1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public en1[] newArray(int i) {
            return new en1[i];
        }
    }

    @DexIgnore
    public en1(byte b2, byte b3, byte b4, byte b5, short s, a aVar) throws IllegalArgumentException {
        super(zm1.INACTIVE_NUDGE);
        this.c = (byte) b2;
        this.d = (byte) b3;
        this.e = (byte) b4;
        this.f = (byte) b5;
        this.g = (short) s;
        this.h = aVar;
        d();
    }

    @DexIgnore
    public /* synthetic */ en1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = parcel.readByte();
        this.d = parcel.readByte();
        this.e = parcel.readByte();
        this.f = parcel.readByte();
        this.g = (short) ((short) parcel.readInt());
        String readString = parcel.readString();
        if (readString != null) {
            pq7.b(readString, "parcel.readString()!!");
            this.h = a.valueOf(readString);
            d();
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(6).order(ByteOrder.LITTLE_ENDIAN).put(this.c).put(this.d).put(this.e).put(this.f).put((byte) this.g).put(this.h.a()).array();
        pq7.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("start_hour", Byte.valueOf(this.c));
            jSONObject.put("start_minute", Byte.valueOf(this.d));
            jSONObject.put("stop_hour", Byte.valueOf(this.e));
            jSONObject.put("stop_minute", Byte.valueOf(this.f));
            jSONObject.put("repeat_interval", Short.valueOf(this.g));
            jSONObject.put("state", ey1.a(this.h));
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        boolean z = true;
        byte b2 = this.c;
        if (b2 >= 0 && 23 >= b2) {
            byte b3 = this.d;
            if (b3 >= 0 && 59 >= b3) {
                byte b4 = this.e;
                if (b4 >= 0 && 23 >= b4) {
                    byte b5 = this.f;
                    if (b5 >= 0 && 59 >= b5) {
                        short s = i;
                        short s2 = this.g;
                        if (s2 < 0 || s < s2) {
                            z = false;
                        }
                        if (!z) {
                            StringBuilder e2 = e.e("repeatInterval(");
                            e2.append((int) this.g);
                            e2.append(") is out of range ");
                            e2.append("[0, ");
                            throw new IllegalArgumentException(e.b(e2, i, "] (in minute."));
                        }
                        return;
                    }
                    throw new IllegalArgumentException(e.c(e.e("stopMinute("), this.f, ") is out of range ", "[0, 59]."));
                }
                throw new IllegalArgumentException(e.c(e.e("stopHour("), this.e, ") is out of range ", "[0, 23]."));
            }
            throw new IllegalArgumentException(e.c(e.e("startMinute("), this.d, ") is out of range ", "[0, 59]."));
        }
        throw new IllegalArgumentException(e.c(e.e("startHour("), this.c, ") is out of range ", "[0, 23]."));
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(en1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            en1 en1 = (en1) obj;
            if (this.c != en1.c) {
                return false;
            }
            if (this.d != en1.d) {
                return false;
            }
            if (this.e != en1.e) {
                return false;
            }
            if (this.f != en1.f) {
                return false;
            }
            if (this.g != en1.g) {
                return false;
            }
            return this.h == en1.h;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.InactiveNudgeConfig");
    }

    @DexIgnore
    public final short getRepeatInterval() {
        return this.g;
    }

    @DexIgnore
    public final byte getStartHour() {
        return this.c;
    }

    @DexIgnore
    public final byte getStartMinute() {
        return this.d;
    }

    @DexIgnore
    public final a getState() {
        return this.h;
    }

    @DexIgnore
    public final byte getStopHour() {
        return this.e;
    }

    @DexIgnore
    public final byte getStopMinute() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public int hashCode() {
        int hashCode = super.hashCode();
        byte b2 = this.c;
        byte b3 = this.d;
        byte b4 = this.e;
        byte b5 = this.f;
        return (((((((((((hashCode * 31) + b2) * 31) + b3) * 31) + b4) * 31) + b5) * 31) + this.g) * 31) + this.h.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
        if (parcel != null) {
            parcel.writeByte(this.e);
        }
        if (parcel != null) {
            parcel.writeByte(this.f);
        }
        if (parcel != null) {
            parcel.writeInt(hy1.n(this.g));
        }
        if (parcel != null) {
            parcel.writeString(this.h.name());
        }
    }
}
