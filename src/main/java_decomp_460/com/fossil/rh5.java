package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum rh5 {
    ACTIVE_TIME(1),
    TOTAL_STEPS(2),
    CALORIES(3),
    GOAL_TRACKING(3),
    TOTAL_SLEEP(11),
    RESTFUL(12),
    LIGHT(13),
    AWAKE(14);
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public int mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public rh5(int i) {
        this.mValue = i;
    }

    @DexIgnore
    public final int getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(int i) {
        this.mValue = i;
    }
}
