package com.fossil;

import android.animation.TypeEvaluator;
import android.graphics.Matrix;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ax3 implements TypeEvaluator<Matrix> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ float[] f356a; // = new float[9];
    @DexIgnore
    public /* final */ float[] b; // = new float[9];
    @DexIgnore
    public /* final */ Matrix c; // = new Matrix();

    @DexIgnore
    public Matrix a(float f, Matrix matrix, Matrix matrix2) {
        matrix.getValues(this.f356a);
        matrix2.getValues(this.b);
        for (int i = 0; i < 9; i++) {
            float[] fArr = this.b;
            float f2 = fArr[i];
            float[] fArr2 = this.f356a;
            fArr[i] = ((f2 - fArr2[i]) * f) + fArr2[i];
        }
        this.c.setValues(this.b);
        return this.c;
    }
}
