package com.fossil;

import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i87 implements Factory<h87> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ComplicationRepository> f1593a;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> b;

    @DexIgnore
    public i87(Provider<ComplicationRepository> provider, Provider<DianaAppSettingRepository> provider2) {
        this.f1593a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static i87 a(Provider<ComplicationRepository> provider, Provider<DianaAppSettingRepository> provider2) {
        return new i87(provider, provider2);
    }

    @DexIgnore
    public static h87 c(ComplicationRepository complicationRepository, DianaAppSettingRepository dianaAppSettingRepository) {
        return new h87(complicationRepository, dianaAppSettingRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public h87 get() {
        return c(this.f1593a.get(), this.b.get());
    }
}
