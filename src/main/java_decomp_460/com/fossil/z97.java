package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z97 extends pv5 {
    @DexIgnore
    public po4 g;
    @DexIgnore
    public g37<bh5> h;
    @DexIgnore
    public ba7 i;
    @DexIgnore
    public x97 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements e77 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ z97 f4437a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(z97 z97) {
            this.f4437a = z97;
        }

        @DexIgnore
        @Override // com.fossil.e77
        public void a(fb7 fb7, int i) {
            RecyclerView recyclerView;
            pq7.c(fb7, "wf");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("WatchFaceTemplateFragment", "onTemplateClicked template: " + fb7);
            z97.L6(this.f4437a).g(i);
            bh5 bh5 = (bh5) z97.K6(this.f4437a).a();
            if (!(bh5 == null || (recyclerView = bh5.q) == null)) {
                recyclerView.smoothScrollToPosition(i);
            }
            z97.M6(this.f4437a).s(fb7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<cl7<? extends List<fb7>, ? extends Integer>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ z97 f4438a;

        @DexIgnore
        public b(z97 z97) {
            this.f4438a = z97;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<? extends List<fb7>, Integer> cl7) {
            int intValue = cl7.getSecond().intValue();
            z97.L6(this.f4438a).h((List) cl7.getFirst());
            z97.L6(this.f4438a).g(intValue);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<fb7> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ z97 f4439a;

        @DexIgnore
        public c(z97 z97) {
            this.f4439a = z97;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(fb7 fb7) {
            gc7 e;
            if (fb7 != null && (e = hc7.c.e(this.f4439a)) != null) {
                e.B(fb7);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<fb7> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ z97 f4440a;

        @DexIgnore
        public d(z97 z97) {
            this.f4440a = z97;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(fb7 fb7) {
            String str;
            ba7 M6 = z97.M6(this.f4440a);
            if (fb7 == null || (str = fb7.b()) == null) {
                str = "";
            }
            M6.v(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ z97 f4441a;

        @DexIgnore
        public e(z97 z97) {
            this.f4441a = z97;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ba7 M6 = z97.M6(this.f4441a);
            if (str == null) {
                str = "";
            }
            M6.v(str);
        }
    }

    @DexIgnore
    public static final /* synthetic */ g37 K6(z97 z97) {
        g37<bh5> g37 = z97.h;
        if (g37 != null) {
            return g37;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ x97 L6(z97 z97) {
        x97 x97 = z97.j;
        if (x97 != null) {
            return x97;
        }
        pq7.n("templateAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ba7 M6(z97 z97) {
        ba7 ba7 = z97.i;
        if (ba7 != null) {
            return ba7;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "WatchFaceTemplateFragment";
    }

    @DexIgnore
    public final void N6() {
        this.j = new x97(new a(this));
        g37<bh5> g37 = this.h;
        if (g37 != null) {
            bh5 a2 = g37.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.q;
                recyclerView.setHasFixedSize(true);
                recyclerView.setItemAnimator(null);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                x97 x97 = this.j;
                if (x97 != null) {
                    recyclerView.setAdapter(x97);
                } else {
                    pq7.n("templateAdapter");
                    throw null;
                }
            }
        } else {
            pq7.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public final void O6() {
        MutableLiveData<String> r;
        LiveData<fb7> l;
        ba7 ba7 = this.i;
        if (ba7 != null) {
            ba7.q().h(getViewLifecycleOwner(), new b(this));
            ba7 ba72 = this.i;
            if (ba72 != null) {
                ba72.r().h(getViewLifecycleOwner(), new c(this));
                gc7 e2 = hc7.c.e(this);
                if (!(e2 == null || (l = e2.l()) == null)) {
                    l.h(getViewLifecycleOwner(), new d(this));
                }
                gc7 e3 = hc7.c.e(this);
                if (e3 != null && (r = e3.r()) != null) {
                    r.h(getViewLifecycleOwner(), new e(this));
                    return;
                }
                return;
            }
            pq7.n("viewModel");
            throw null;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        ScrollView scrollView;
        super.onActivityCreated(bundle);
        PortfolioApp.h0.c().M().X0().a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(ba7.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ateViewModel::class.java)");
            this.i = (ba7) a2;
            String d2 = qn5.l.a().d("nonBrandSurface");
            if (!TextUtils.isEmpty(d2)) {
                g37<bh5> g37 = this.h;
                if (g37 != null) {
                    bh5 a3 = g37.a();
                    if (!(a3 == null || (scrollView = a3.r) == null)) {
                        scrollView.setBackgroundColor(Color.parseColor(d2));
                    }
                } else {
                    pq7.n("binding");
                    throw null;
                }
            }
            N6();
            O6();
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        bh5 bh5 = (bh5) aq0.f(layoutInflater, 2131558863, viewGroup, false, A6());
        this.h = new g37<>(this, bh5);
        pq7.b(bh5, "binding");
        View n = bh5.n();
        pq7.b(n, "binding.root");
        return n;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
