package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g06 implements Factory<f06> {
    @DexIgnore
    public static f06 a(a06 a06, uq4 uq4, bk5 bk5, v36 v36, d26 d26, u06 u06, NotificationSettingsDatabase notificationSettingsDatabase, yx5 yx5, AlarmsRepository alarmsRepository, on5 on5, DNDSettingsDatabase dNDSettingsDatabase) {
        return new f06(a06, uq4, bk5, v36, d26, u06, notificationSettingsDatabase, yx5, alarmsRepository, on5, dNDSettingsDatabase);
    }
}
