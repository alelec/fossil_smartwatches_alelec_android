package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m95 extends l95 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362546, 1);
        E.put(2131362430, 2);
        E.put(2131362713, 3);
        E.put(2131363459, 4);
        E.put(2131362435, 5);
        E.put(2131362717, 6);
        E.put(2131363460, 7);
        E.put(2131362491, 8);
        E.put(2131362736, 9);
        E.put(2131363461, 10);
        E.put(2131362686, 11);
    }
    */

    @DexIgnore
    public m95(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 12, D, E));
    }

    @DexIgnore
    public m95(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[1], (RTLImageView) objArr[11], (RTLImageView) objArr[3], (RTLImageView) objArr[6], (RTLImageView) objArr[9], (View) objArr[4], (View) objArr[7], (View) objArr[10]);
        this.C = -1;
        this.q.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.C != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.C = 1;
        }
        w();
    }
}
