package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s53 implements xw2<r53> {
    @DexIgnore
    public static s53 c; // = new s53();
    @DexIgnore
    public /* final */ xw2<r53> b;

    @DexIgnore
    public s53() {
        this(ww2.b(new u53()));
    }

    @DexIgnore
    public s53(xw2<r53> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((r53) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((r53) c.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((r53) c.zza()).zzc();
    }

    @DexIgnore
    public static boolean d() {
        return ((r53) c.zza()).zzd();
    }

    @DexIgnore
    public static boolean e() {
        return ((r53) c.zza()).zze();
    }

    @DexIgnore
    public static boolean f() {
        return ((r53) c.zza()).zzf();
    }

    @DexIgnore
    public static boolean g() {
        return ((r53) c.zza()).zzg();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ r53 zza() {
        return this.b.zza();
    }
}
