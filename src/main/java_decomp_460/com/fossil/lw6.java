package com.fossil;

import com.fossil.iq4;
import com.fossil.u27;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lw6 extends hw6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public MFUser e;
    @DexIgnore
    public /* final */ iw6 f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ u27 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return lw6.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.e<u27.c, u27.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ MFUser f2273a;
        @DexIgnore
        public /* final */ /* synthetic */ lw6 b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public b(MFUser mFUser, lw6 lw6, boolean z) {
            this.f2273a = mFUser;
            this.b = lw6;
            this.c = z;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(u27.a aVar) {
            pq7.c(aVar, "errorValue");
            this.b.y(this.f2273a, this.c);
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(u27.c cVar) {
            pq7.c(cVar, "responseValue");
            this.b.y(this.f2273a, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1", f = "OnboardingHeightWeightPresenter.kt", l = {124}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isDefaultValuesUsed;
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ lw6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1$1", f = "OnboardingHeightWeightPresenter.kt", l = {124}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super iq5<? extends MFUser>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super iq5<? extends MFUser>> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.g;
                    MFUser mFUser = this.this$0.$user;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object updateUser = userRepository.updateUser(mFUser, true, this);
                    return updateUser == d ? d : updateUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(lw6 lw6, MFUser mFUser, boolean z, qn7 qn7) {
            super(2, qn7);
            this.this$0 = lw6;
            this.$user = mFUser;
            this.$isDefaultValuesUsed = z;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$user, this.$isDefaultValuesUsed, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (this.$user.getUseDefaultBiometric()) {
                    this.$user.setUseDefaultBiometric(this.$isDefaultValuesUsed);
                }
                dv7 h = this.this$0.h();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(h, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.f.h();
            this.this$0.f.u2();
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1", f = "OnboardingHeightWeightPresenter.kt", l = {32}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ lw6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1$1", f = "OnboardingHeightWeightPresenter.kt", l = {32}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.g;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(lw6 lw6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = lw6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:7:0x004d  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
            // Method dump skipped, instructions count: 322
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.lw6.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = lw6.class.getSimpleName();
        pq7.b(simpleName, "OnboardingHeightWeightPr\u2026er::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public lw6(iw6 iw6, UserRepository userRepository, u27 u27) {
        pq7.c(iw6, "mView");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(u27, "mGetRecommendedGoalUseCase");
        this.f = iw6;
        this.g = userRepository;
        this.h = u27;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        xw7 unused = gu7.d(k(), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.hw6
    public void n(boolean z) {
        String str = null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local.d(str2, "completeOnboarding currentUser=" + this.e);
        this.f.i();
        MFUser mFUser = this.e;
        if (mFUser != null) {
            String birthday = mFUser.getBirthday();
            if (birthday != null) {
                int age = mFUser.getAge(birthday);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = i;
                StringBuilder sb = new StringBuilder();
                sb.append("completeOnboarding update heightUnit=");
                MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
                sb.append(unitGroup != null ? unitGroup.getHeight() : null);
                sb.append(", weightUnit=");
                MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                if (unitGroup2 != null) {
                    str = unitGroup2.getWeight();
                }
                sb.append(str);
                sb.append(',');
                sb.append(" height=");
                sb.append(mFUser.getHeightInCentimeters());
                sb.append(", weight=");
                sb.append(mFUser.getWeightInGrams());
                local2.d(str3, sb.toString());
                this.h.e(new u27.b(age, mFUser.getHeightInCentimeters(), mFUser.getWeightInGrams(), qh5.Companion.a(mFUser.getGender())), new b(mFUser, this, z));
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hw6
    public void o(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onHeightChanged heightInCentimeters=" + i2);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            mFUser.setHeightInCentimeters(i2);
        }
    }

    @DexIgnore
    @Override // com.fossil.hw6
    public void p(ai5 ai5) {
        pq7.c(ai5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onUnitHeightChanged unit=" + ai5);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ai5.getValue();
                pq7.b(value, "unit.value");
                unitGroup.setHeight(value);
                iw6 iw6 = this.f;
                int heightInCentimeters = mFUser.getHeightInCentimeters();
                MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                if (unitGroup2 != null) {
                    String height = unitGroup2.getHeight();
                    if (height != null) {
                        ai5 fromString = ai5.fromString(height);
                        pq7.b(fromString, "Unit.fromString(it.unitGroup!!.height!!)");
                        iw6.R0(heightInCentimeters, fromString);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hw6
    public void q(ai5 ai5) {
        pq7.c(ai5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onUnitWeightChanged unit=" + ai5);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ai5.getValue();
                pq7.b(value, "unit.value");
                unitGroup.setWeight(value);
                iw6 iw6 = this.f;
                int weightInGrams = mFUser.getWeightInGrams();
                MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                if (unitGroup2 != null) {
                    String weight = unitGroup2.getWeight();
                    if (weight != null) {
                        ai5 fromString = ai5.fromString(weight);
                        pq7.b(fromString, "Unit.fromString(it.unitGroup!!.weight!!)");
                        iw6.H1(weightInGrams, fromString);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hw6
    public void r(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onWeightChanged weightInGram=" + i2);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            mFUser.setWeightInGrams(i2);
        }
    }

    @DexIgnore
    public final void y(MFUser mFUser, boolean z) {
        pq7.c(mFUser, "user");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onSetUpHeightWeightComplete register date: " + mFUser.getRegisterDate());
        xw7 unused = gu7.d(k(), null, null, new c(this, mFUser, z, null), 3, null);
    }

    @DexIgnore
    public void z() {
        this.f.M5(this);
    }
}
