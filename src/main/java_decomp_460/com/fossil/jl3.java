package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ int b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ Object d;
    @DexIgnore
    public /* final */ /* synthetic */ Object e;
    @DexIgnore
    public /* final */ /* synthetic */ Object f;
    @DexIgnore
    public /* final */ /* synthetic */ kl3 g;

    @DexIgnore
    public jl3(kl3 kl3, int i, String str, Object obj, Object obj2, Object obj3) {
        this.g = kl3;
        this.b = i;
        this.c = str;
        this.d = obj;
        this.e = obj2;
        this.f = obj3;
    }

    @DexIgnore
    public final void run() {
        xl3 z = this.g.f1780a.z();
        if (z.s()) {
            if (this.g.c == 0) {
                if (this.g.m().E()) {
                    kl3 kl3 = this.g;
                    kl3.b();
                    kl3.c = 'C';
                } else {
                    kl3 kl32 = this.g;
                    kl32.b();
                    kl32.c = 'c';
                }
            }
            if (this.g.d < 0) {
                kl3 kl33 = this.g;
                kl33.d = kl33.m().C();
            }
            char charAt = "01VDIWEA?".charAt(this.b);
            char c2 = this.g.c;
            long j = this.g.d;
            String y = kl3.y(true, this.c, this.d, this.e, this.f);
            StringBuilder sb = new StringBuilder(String.valueOf(y).length() + 24);
            sb.append("2");
            sb.append(charAt);
            sb.append(c2);
            sb.append(j);
            sb.append(":");
            sb.append(y);
            String sb2 = sb.toString();
            if (sb2.length() > 1024) {
                sb2 = this.c.substring(0, 1024);
            }
            z.d.b(sb2, 1);
            return;
        }
        this.g.z(6, "Persisted config not initialized. Not logging error/warn");
    }
}
