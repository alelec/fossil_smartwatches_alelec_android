package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vi5 extends ri5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f3773a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Bundle c;

    @DexIgnore
    public vi5(String str, int i, Bundle bundle) {
        this.f3773a = i;
        this.b = str;
        this.c = bundle;
    }

    @DexIgnore
    public int a() {
        return this.f3773a;
    }

    @DexIgnore
    public Bundle b() {
        return this.c;
    }

    @DexIgnore
    public String c() {
        return this.b;
    }
}
