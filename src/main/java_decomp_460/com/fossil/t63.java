package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t63 implements u63 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f3371a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.service.use_appinfo_modified", false);

    @DexIgnore
    @Override // com.fossil.u63
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.u63
    public final boolean zzb() {
        return f3371a.o().booleanValue();
    }
}
