package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v02 {

    @DexIgnore
    public enum a {
        OK,
        TRANSIENT_ERROR,
        FATAL_ERROR
    }

    @DexIgnore
    public static v02 a() {
        return new q02(a.FATAL_ERROR, -1);
    }

    @DexIgnore
    public static v02 d(long j) {
        return new q02(a.OK, j);
    }

    @DexIgnore
    public static v02 e() {
        return new q02(a.TRANSIENT_ERROR, -1);
    }

    @DexIgnore
    public abstract long b();

    @DexIgnore
    public abstract a c();
}
