package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bj5 extends ri5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f436a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public bj5(String str, int i) {
        this.f436a = i;
        this.b = str;
    }

    @DexIgnore
    public int a() {
        return this.f436a;
    }

    @DexIgnore
    public String b() {
        return this.b;
    }
}
