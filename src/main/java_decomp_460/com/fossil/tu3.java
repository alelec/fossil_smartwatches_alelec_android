package com.fossil;

import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tu3 extends q62<Object> {

    @DexIgnore
    public interface a extends Parcelable {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public abstract void a(a aVar, int i, int i2);

        @DexIgnore
        public abstract void b(a aVar);

        @DexIgnore
        public abstract void c(a aVar, int i, int i2);

        @DexIgnore
        public abstract void d(a aVar, int i, int i2);
    }
}
