package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zl3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4489a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ xl3 e;

    @DexIgnore
    public zl3(xl3 xl3, String str, boolean z) {
        this.e = xl3;
        rc2.g(str);
        this.f4489a = str;
        this.b = z;
    }

    @DexIgnore
    public final void a(boolean z) {
        SharedPreferences.Editor edit = this.e.B().edit();
        edit.putBoolean(this.f4489a, z);
        edit.apply();
        this.d = z;
    }

    @DexIgnore
    public final boolean b() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.B().getBoolean(this.f4489a, this.b);
        }
        return this.d;
    }
}
