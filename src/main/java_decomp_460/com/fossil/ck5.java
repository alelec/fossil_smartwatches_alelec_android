package com.fossil;

import android.app.Activity;
import android.os.Bundle;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.zendesk.sdk.network.impl.DeviceInfo;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ck5 {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static FirebaseAnalytics c;
    @DexIgnore
    public static ck5 d;
    @DexIgnore
    public static /* final */ HashMap<String, ul5> e; // = new HashMap<>();
    @DexIgnore
    public static /* final */ a f; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public volatile String f626a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(String str, ul5 ul5) {
            pq7.c(str, "tracingKey");
            pq7.c(ul5, "trace");
            i().put(str, ul5);
        }

        @DexIgnore
        public final sl5 b(String str) {
            pq7.c(str, "eventName");
            return new sl5(g(), str);
        }

        @DexIgnore
        public final ul5 c(String str) {
            pq7.c(str, "traceName");
            ck5 g = g();
            return pq7.a("view_appearance", str) ? new vl5(g, str, g.g()) : new ul5(g, str, g.g());
        }

        @DexIgnore
        public final tl5 d() {
            return new tl5(g());
        }

        @DexIgnore
        public final vl5 e() {
            ck5 g = g();
            return new vl5(g, "view_appearance", g.g());
        }

        @DexIgnore
        public final ul5 f(String str) {
            pq7.c(str, "tracingKey");
            return i().get(str);
        }

        @DexIgnore
        public final ck5 g() {
            ck5 j;
            synchronized (this) {
                if (j() == null) {
                    l(new ck5(null));
                }
                j = j();
                if (j == null) {
                    pq7.i();
                    throw null;
                }
            }
            return j;
        }

        @DexIgnore
        public final String h(String str) {
            pq7.c(str, "input");
            return new jt7("[^a-zA-Z0-9]+").replace(str, LocaleConverter.LOCALE_DELIMITER);
        }

        @DexIgnore
        public final HashMap<String, ul5> i() {
            return ck5.e;
        }

        @DexIgnore
        public final ck5 j() {
            return ck5.d;
        }

        @DexIgnore
        public final void k(String str) {
            pq7.c(str, "tracingKey");
            i().remove(str);
        }

        @DexIgnore
        public final void l(ck5 ck5) {
            ck5.d = ck5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.helper.AnalyticsHelper$doLogEvent$1", f = "AnalyticsHelper.kt", l = {}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bundle $data;
        @DexIgnore
        public /* final */ /* synthetic */ String $event;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, Bundle bundle, qn7 qn7) {
            super(2, qn7);
            this.$event = str;
            this.$data = bundle;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.$event, this.$data, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                FirebaseAnalytics firebaseAnalytics = ck5.c;
                if (firebaseAnalytics != null) {
                    firebaseAnalytics.a(this.$event, this.$data);
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = ck5.class.getSimpleName();
        pq7.b(simpleName, "AnalyticsHelper::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public ck5() {
        c = FirebaseAnalytics.getInstance(PortfolioApp.h0.c());
    }

    @DexIgnore
    public /* synthetic */ ck5(kq7 kq7) {
        this();
    }

    @DexIgnore
    public final Map<String, String> e(String str, String str2, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("Style_Number", str);
        hashMap.put("Device_Name", str2);
        hashMap.put("Type", String.valueOf(i));
        return hashMap;
    }

    @DexIgnore
    public final void f(String str, Bundle bundle) {
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new b(str, bundle, null), 3, null);
    }

    @DexIgnore
    public final String g() {
        return this.f626a;
    }

    @DexIgnore
    public final void h(String str, String str2, String str3) {
        pq7.c(str, "serialPrefix");
        pq7.c(str2, "activeDeviceName");
        pq7.c(str3, "firmwareVersion");
        tl5 d2 = f.d();
        d2.a("serial_number_prefix", str);
        d2.a(DeviceInfo.DEVICE_INFO_DEVICE_NAME, str2);
        d2.a(Constants.FIRMWARE_VERSION, str3);
        d2.b();
    }

    @DexIgnore
    public final void i(String str) {
        pq7.c(str, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "Inside .analyticLogEvent event=" + str);
        f(str, null);
    }

    @DexIgnore
    public final void j(String str, String str2) {
        pq7.c(str, Constants.EVENT);
        pq7.c(str2, "value");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b;
        local.d(str3, "Inside .analyticLogEvent event=" + str + ", value=" + str2);
        Bundle bundle = new Bundle();
        bundle.putString(str, ej5.b(str2));
        f(str, bundle);
    }

    @DexIgnore
    public final void k(String str, String str2, String str3) {
        pq7.c(str, Constants.EVENT);
        pq7.c(str2, "paramName");
        pq7.c(str3, "paramValue");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = b;
        local.d(str4, "Inside .analyticLogEvent event=" + str + ", name=" + str2 + ", value=" + str3);
        Bundle bundle = new Bundle();
        bundle.putString(str2, ej5.b(str3));
        f(str, bundle);
    }

    @DexIgnore
    public final void l(String str, Map<String, ? extends Object> map) {
        pq7.c(str, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "Inside .analyticLogEvent event=" + str + ", value=" + map);
        if (map != null && (!map.isEmpty())) {
            Bundle bundle = new Bundle();
            for (Map.Entry<String, ? extends Object> entry : map.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                if (value instanceof Integer) {
                    bundle.putInt(key, ((Number) value).intValue());
                } else if (value instanceof Long) {
                    bundle.putLong(key, ((Number) value).longValue());
                } else if (value instanceof Boolean) {
                    bundle.putBoolean(key, ((Boolean) value).booleanValue());
                } else if (value instanceof Float) {
                    bundle.putFloat(key, ((Number) value).floatValue());
                } else if (value != null) {
                    bundle.putString(key, ej5.b((String) value));
                } else {
                    throw new il7("null cannot be cast to non-null type kotlin.String");
                }
            }
            f(str, bundle);
        }
    }

    @DexIgnore
    public final void m(String str, Activity activity) {
        FirebaseAnalytics firebaseAnalytics;
        if (!(str == null || str.length() == 0) && activity != null && (firebaseAnalytics = c) != null) {
            firebaseAnalytics.setCurrentScreen(activity, str, activity.getClass().getSimpleName() + " - " + System.currentTimeMillis());
        }
    }

    @DexIgnore
    public final void n(String str, String str2, int i) {
        pq7.c(str, "styleNumber");
        pq7.c(str2, "deviceName");
        l("sync_error", e(str, str2, i));
    }

    @DexIgnore
    public final void o(int i, SKUModel sKUModel) {
        if (sKUModel != null) {
            String sku = sKUModel.getSku();
            if (sku != null) {
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                l("sync_start", e(sku, deviceName, i));
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void p(String str, String str2, int i) {
        pq7.c(str, "styleNumber");
        pq7.c(str2, "deviceName");
        l("sync_success", e(str, str2, i));
    }

    @DexIgnore
    public final void q(String str, String str2) {
        pq7.c(str, "propertyName");
        pq7.c(str2, "value");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b;
        local.d(str3, "User property " + str + " with value=" + str2);
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.d(str, ej5.b(str2));
        }
    }

    @DexIgnore
    public final void r(Map<String, String> map) {
        if (map != null && (!map.isEmpty())) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = b;
                local.d(str, "User property " + key + " with value=" + value);
                FirebaseAnalytics firebaseAnalytics = c;
                if (firebaseAnalytics != null) {
                    firebaseAnalytics.d(key, ej5.b(value));
                }
            }
        }
    }

    @DexIgnore
    public final void s(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "setAnalyticEnable: " + z);
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.b(z);
        }
    }

    @DexIgnore
    public final void t() {
        q("Auth", "None");
    }

    @DexIgnore
    public final void u(boolean z) {
        q("Optin_Emails", z ? "Yes" : "No");
    }

    @DexIgnore
    public final void v(boolean z) {
        q("Optin_Usage", z ? "Yes" : "No");
    }

    @DexIgnore
    public final void w(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "set userId: " + str);
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.c(str);
        }
        this.f626a = str;
    }
}
