package com.fossil;

import com.fossil.jh4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gh4 extends jh4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1307a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ kh4 d;
    @DexIgnore
    public /* final */ jh4.b e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends jh4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f1308a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public kh4 d;
        @DexIgnore
        public jh4.b e;

        @DexIgnore
        @Override // com.fossil.jh4.a
        public jh4 a() {
            return new gh4(this.f1308a, this.b, this.c, this.d, this.e);
        }

        @DexIgnore
        @Override // com.fossil.jh4.a
        public jh4.a b(kh4 kh4) {
            this.d = kh4;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.jh4.a
        public jh4.a c(String str) {
            this.b = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.jh4.a
        public jh4.a d(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.jh4.a
        public jh4.a e(jh4.b bVar) {
            this.e = bVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.jh4.a
        public jh4.a f(String str) {
            this.f1308a = str;
            return this;
        }
    }

    @DexIgnore
    public gh4(String str, String str2, String str3, kh4 kh4, jh4.b bVar) {
        this.f1307a = str;
        this.b = str2;
        this.c = str3;
        this.d = kh4;
        this.e = bVar;
    }

    @DexIgnore
    @Override // com.fossil.jh4
    public kh4 b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.jh4
    public String c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.jh4
    public String d() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.jh4
    public jh4.b e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof jh4)) {
            return false;
        }
        jh4 jh4 = (jh4) obj;
        String str = this.f1307a;
        if (str != null ? str.equals(jh4.f()) : jh4.f() == null) {
            String str2 = this.b;
            if (str2 != null ? str2.equals(jh4.c()) : jh4.c() == null) {
                String str3 = this.c;
                if (str3 != null ? str3.equals(jh4.d()) : jh4.d() == null) {
                    kh4 kh4 = this.d;
                    if (kh4 != null ? kh4.equals(jh4.b()) : jh4.b() == null) {
                        jh4.b bVar = this.e;
                        if (bVar == null) {
                            if (jh4.e() == null) {
                                return true;
                            }
                        } else if (bVar.equals(jh4.e())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.jh4
    public String f() {
        return this.f1307a;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f1307a;
        int hashCode = str == null ? 0 : str.hashCode();
        String str2 = this.b;
        int hashCode2 = str2 == null ? 0 : str2.hashCode();
        String str3 = this.c;
        int hashCode3 = str3 == null ? 0 : str3.hashCode();
        kh4 kh4 = this.d;
        int hashCode4 = kh4 == null ? 0 : kh4.hashCode();
        jh4.b bVar = this.e;
        if (bVar != null) {
            i = bVar.hashCode();
        }
        return ((((((((hashCode ^ 1000003) * 1000003) ^ hashCode2) * 1000003) ^ hashCode3) * 1000003) ^ hashCode4) * 1000003) ^ i;
    }

    @DexIgnore
    public String toString() {
        return "InstallationResponse{uri=" + this.f1307a + ", fid=" + this.b + ", refreshToken=" + this.c + ", authToken=" + this.d + ", responseCode=" + this.e + "}";
    }
}
