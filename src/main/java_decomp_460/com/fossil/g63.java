package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g63 implements d63 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f1266a;
    @DexIgnore
    public static /* final */ xv2<Boolean> b;
    @DexIgnore
    public static /* final */ xv2<Boolean> c;
    @DexIgnore
    public static /* final */ xv2<Boolean> d;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f1266a = hw2.d("measurement.service.audience.fix_skip_audience_with_failed_filters", true);
        b = hw2.d("measurement.audience.refresh_event_count_filters_timestamp", false);
        c = hw2.d("measurement.audience.use_bundle_end_timestamp_for_non_sequence_property_filters", false);
        d = hw2.d("measurement.audience.use_bundle_timestamp_for_event_count_filters", false);
    }
    */

    @DexIgnore
    @Override // com.fossil.d63
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.d63
    public final boolean zzb() {
        return f1266a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.d63
    public final boolean zzc() {
        return b.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.d63
    public final boolean zzd() {
        return c.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.d63
    public final boolean zze() {
        return d.o().booleanValue();
    }
}
