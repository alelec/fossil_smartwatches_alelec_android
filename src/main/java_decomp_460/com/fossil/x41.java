package com.fossil;

import coil.util.CoilContentProvider;
import com.fossil.a51;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x41 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static a51 f4038a;
    @DexIgnore
    public static gp7<? extends a51> b;
    @DexIgnore
    public static /* final */ x41 c; // = new x41();

    @DexIgnore
    public static final a51 b() {
        a51 a51 = f4038a;
        return a51 != null ? a51 : c.a();
    }

    @DexIgnore
    public static final void c(a51 a51) {
        pq7.c(a51, "loader");
        a51 a512 = f4038a;
        if (a512 != null) {
            a512.shutdown();
        }
        f4038a = a51;
        b = null;
    }

    @DexIgnore
    public final a51 a() {
        a51 a51;
        synchronized (this) {
            a51 = f4038a;
            if (a51 == null) {
                gp7<? extends a51> gp7 = b;
                if (gp7 == null || (a51 = (a51) gp7.invoke()) == null) {
                    a51.a aVar = a51.o;
                    a51 = new b51(CoilContentProvider.c.a()).b();
                }
                b = null;
                c(a51);
            }
        }
        return a51;
    }
}
