package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class de7 extends ee7 {
    @DexIgnore
    @Override // com.fossil.je7
    public void error(String str, String str2, Object obj) {
        i().error(str, str2, obj);
    }

    @DexIgnore
    public abstract je7 i();

    @DexIgnore
    @Override // com.fossil.je7
    public void success(Object obj) {
        i().success(obj);
    }
}
