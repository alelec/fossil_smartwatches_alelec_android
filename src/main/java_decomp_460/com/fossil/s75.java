package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.bottomnavigation.BottomNavigationView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s75 extends r75 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d u; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray v;
    @DexIgnore
    public long t;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        v = sparseIntArray;
        sparseIntArray.put(2131363058, 1);
        v.put(2131361922, 2);
    }
    */

    @DexIgnore
    public s75(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 3, u, v));
    }

    @DexIgnore
    public s75(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (BottomNavigationView) objArr[2], (ConstraintLayout) objArr[0], (ViewPager2) objArr[1]);
        this.t = -1;
        this.r.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.t = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.t != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.t = 1;
        }
        w();
    }
}
