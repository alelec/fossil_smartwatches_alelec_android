package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class st4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ApiServiceV2 f3302a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {98}, m = "createChallenge")
    public static final class a extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(st4 st4, qn7 qn7) {
            super(qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, 0, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$createChallenge$response$1", f = "ChallengeRemoteDataSource.kt", l = {98}, m = "invokeSuspend")
    public static final class b extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(st4 st4, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = st4;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new b(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((b) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3302a;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object createChallenge = apiServiceV2.createChallenge(gj4, this);
                return createChallenge == d ? d : createChallenge;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {176}, m = "editChallenge")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(st4 st4, qn7 qn7) {
            super(qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(null, null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$editChallenge$response$1", f = "ChallengeRemoteDataSource.kt", l = {176}, m = "invokeSuspend")
    public static final class d extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(st4 st4, String str, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = st4;
            this.$challengeId = str;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new d(this.this$0, this.$challengeId, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((d) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3302a;
                String str = this.$challengeId;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object editChallenge = apiServiceV2.editChallenge(str, gj4, this);
                return editChallenge == d ? d : editChallenge;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {19}, m = "fetchChallengesWithStatus")
    public static final class e extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(st4 st4, qn7 qn7) {
            super(qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(null, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$fetchChallengesWithStatus$response$1", f = "ChallengeRemoteDataSource.kt", l = {19}, m = "invokeSuspend")
    public static final class f extends ko7 implements rp7<qn7<? super q88<ApiResponse<ps4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ String[] $status;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(st4 st4, String[] strArr, int i, qn7 qn7) {
            super(1, qn7);
            this.this$0 = st4;
            this.$status = strArr;
            this.$limit = i;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new f(this.this$0, this.$status, this.$limit, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<ps4>>> qn7) {
            return ((f) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3302a;
                String[] strArr = this.$status;
                Integer e = ao7.e(this.$limit);
                this.label = 1;
                Object fetchChallengesWithStatus = apiServiceV2.fetchChallengesWithStatus(strArr, e, this);
                return fetchChallengesWithStatus == d ? d : fetchChallengesWithStatus;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {306}, m = "fetchHistoryChallenges")
    public static final class g extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(st4 st4, qn7 qn7) {
            super(qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(0, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$fetchHistoryChallenges$response$1", f = "ChallengeRemoteDataSource.kt", l = {306}, m = "invokeSuspend")
    public static final class h extends ko7 implements rp7<qn7<? super q88<ApiResponse<bt4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ int $offset;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(st4 st4, int i, int i2, qn7 qn7) {
            super(1, qn7);
            this.this$0 = st4;
            this.$limit = i;
            this.$offset = i2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new h(this.this$0, this.$limit, this.$offset, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<bt4>>> qn7) {
            return ((h) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3302a;
                int i2 = this.$limit;
                int i3 = this.$offset;
                this.label = 1;
                Object fetchHistoryChallenges = apiServiceV2.fetchHistoryChallenges(i2, i3, this);
                return fetchHistoryChallenges == d ? d : fetchHistoryChallenges;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {200}, m = "fetchPlayersInChallenge")
    public static final class i extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(st4 st4, qn7 qn7) {
            super(qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$fetchPlayersInChallenge$response$1", f = "ChallengeRemoteDataSource.kt", l = {200}, m = "invokeSuspend")
    public static final class j extends ko7 implements rp7<qn7<? super q88<ApiResponse<ms4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ String[] $status;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(st4 st4, String str, String[] strArr, qn7 qn7) {
            super(1, qn7);
            this.this$0 = st4;
            this.$id = str;
            this.$status = strArr;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new j(this.this$0, this.$id, this.$status, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<ms4>>> qn7) {
            return ((j) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3302a;
                String str = this.$id;
                String[] strArr = this.$status;
                this.label = 1;
                Object invitedPlayers = apiServiceV2.invitedPlayers(str, strArr, this);
                return invitedPlayers == d ? d : invitedPlayers;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {295}, m = "fetchRecommendedChallenges")
    public static final class k extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(st4 st4, qn7 qn7) {
            super(qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$fetchRecommendedChallenges$response$1", f = "ChallengeRemoteDataSource.kt", l = {295}, m = "invokeSuspend")
    public static final class l extends ko7 implements rp7<qn7<? super q88<ApiResponse<mt4>>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(st4 st4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new l(this.this$0, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<mt4>>> qn7) {
            return ((l) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3302a;
                this.label = 1;
                Object recommendedChallenges = apiServiceV2.recommendedChallenges(this);
                return recommendedChallenges == d ? d : recommendedChallenges;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {55}, m = "getChallenge")
    public static final class m extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(st4 st4, qn7 qn7) {
            super(qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$getChallenge$response$1", f = "ChallengeRemoteDataSource.kt", l = {55}, m = "invokeSuspend")
    public static final class n extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(st4 st4, String str, qn7 qn7) {
            super(1, qn7);
            this.this$0 = st4;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new n(this.this$0, this.$id, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((n) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3302a;
                String str = this.$id;
                this.label = 1;
                Object challengeById = apiServiceV2.getChallengeById(str, this);
                return challengeById == d ? d : challengeById;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {43}, m = "getFocusedPlayers")
    public static final class o extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(st4 st4, qn7 qn7) {
            super(qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(null, 0, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$getFocusedPlayers$response$1", f = "ChallengeRemoteDataSource.kt", l = {43}, m = "invokeSuspend")
    public static final class p extends ko7 implements rp7<qn7<? super q88<ApiResponse<ks4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $challengeIds;
        @DexIgnore
        public /* final */ /* synthetic */ int $near;
        @DexIgnore
        public /* final */ /* synthetic */ int $top;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(st4 st4, List list, int i, int i2, qn7 qn7) {
            super(1, qn7);
            this.this$0 = st4;
            this.$challengeIds = list;
            this.$top = i;
            this.$near = i2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new p(this.this$0, this.$challengeIds, this.$top, this.$near, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<ks4>>> qn7) {
            return ((p) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3302a;
                List<String> list = this.$challengeIds;
                int i2 = this.$top;
                int i3 = this.$near;
                this.label = 1;
                Object focusedPlayers = apiServiceV2.getFocusedPlayers(list, i2, i3, this);
                return focusedPlayers == d ? d : focusedPlayers;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {268}, m = "getSyncStatusData")
    public static final class q extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(st4 st4, qn7 qn7) {
            super(qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.j(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$getSyncStatusData$response$1", f = "ChallengeRemoteDataSource.kt", l = {268}, m = "invokeSuspend")
    public static final class r extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public r(st4 st4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new r(this.this$0, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((r) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3302a;
                this.label = 1;
                Object syncStatusData = apiServiceV2.getSyncStatusData(this);
                return syncStatusData == d ? d : syncStatusData;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {220}, m = "invitation")
    public static final class s extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public s(st4 st4, qn7 qn7) {
            super(qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$invitation$response$1", f = "ChallengeRemoteDataSource.kt", l = {220}, m = "invokeSuspend")
    public static final class t extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public t(st4 st4, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = st4;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new t(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((t) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3302a;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object sendInvitation = apiServiceV2.sendInvitation(gj4, this);
                return sendInvitation == d ? d : sendInvitation;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {255}, m = "joinChallenge")
    public static final class u extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public u(st4 st4, qn7 qn7) {
            super(qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.l(null, 0, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$joinChallenge$response$1", f = "ChallengeRemoteDataSource.kt", l = {255}, m = "invokeSuspend")
    public static final class v extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public v(st4 st4, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = st4;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new v(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((v) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3302a;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object joinChallenge = apiServiceV2.joinChallenge(gj4, this);
                return joinChallenge == d ? d : joinChallenge;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {141}, m = "leaveChallenge")
    public static final class w extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public w(st4 st4, qn7 qn7) {
            super(qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$leaveChallenge$response$1", f = "ChallengeRemoteDataSource.kt", l = {141}, m = "invokeSuspend")
    public static final class x extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public x(st4 st4, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = st4;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new x(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((x) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3302a;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object leaveChallenge = apiServiceV2.leaveChallenge(gj4, this);
                return leaveChallenge == d ? d : leaveChallenge;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {284}, m = "pushStepData")
    public static final class y extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public y(st4 st4, qn7 qn7) {
            super(qn7);
            this.this$0 = st4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.n(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$pushStepData$response$1", f = "ChallengeRemoteDataSource.kt", l = {284}, m = "invokeSuspend")
    public static final class z extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ st4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public z(st4 st4, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = st4;
            this.$jsonObject = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new z(this.this$0, this.$jsonObject, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            return ((z) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.f3302a;
                gj4 gj4 = this.$jsonObject;
                this.label = 1;
                Object pushStepData = apiServiceV2.pushStepData(gj4, this);
                return pushStepData == d ? d : pushStepData;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public st4(ApiServiceV2 apiServiceV2) {
        pq7.c(apiServiceV2, "api");
        this.f3302a = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0137  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.fossil.ts4 r11, int r12, java.lang.String r13, com.fossil.qn7<? super com.fossil.iq5<com.fossil.ps4>> r14) {
        /*
        // Method dump skipped, instructions count: 364
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.st4.b(com.fossil.ts4, int, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(java.lang.String r9, java.lang.String r10, java.lang.String r11, java.lang.Integer r12, com.fossil.qn7<? super com.fossil.iq5<com.fossil.ps4>> r13) {
        /*
        // Method dump skipped, instructions count: 260
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.st4.c(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(java.lang.String[] r9, int r10, com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.ps4>>> r11) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r11 instanceof com.fossil.st4.e
            if (r0 == 0) goto L_0x0046
            r0 = r11
            com.fossil.st4$e r0 = (com.fossil.st4.e) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0046
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0055
            if (r0 != r5) goto L_0x004d
            int r0 = r1.I$0
            java.lang.Object r0 = r1.L$1
            java.lang.String[] r0 = (java.lang.String[]) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.st4 r0 = (com.fossil.st4) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x002f:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x006d
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            com.fossil.kq5 r1 = new com.fossil.kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x0045:
            return r0
        L_0x0046:
            com.fossil.st4$e r0 = new com.fossil.st4$e
            r0.<init>(r8, r11)
            r1 = r0
            goto L_0x0015
        L_0x004d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0055:
            com.fossil.el7.b(r2)
            com.fossil.st4$f r0 = new com.fossil.st4$f
            r0.<init>(r8, r9, r10, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.I$0 = r10
            r1.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r0, r1)
            if (r0 != r3) goto L_0x002f
            r0 = r3
            goto L_0x0045
        L_0x006d:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x008a
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0045
        L_0x008a:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.st4.d(java.lang.String[], int, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(int r9, int r10, com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.bt4>>> r11) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r11 instanceof com.fossil.st4.g
            if (r0 == 0) goto L_0x0043
            r0 = r11
            com.fossil.st4$g r0 = (com.fossil.st4.g) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0043
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0051
            if (r3 != r5) goto L_0x0049
            int r2 = r0.I$1
            int r2 = r0.I$0
            java.lang.Object r0 = r0.L$0
            com.fossil.st4 r0 = (com.fossil.st4) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x002c:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0069
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            com.fossil.kq5 r1 = new com.fossil.kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x0042:
            return r0
        L_0x0043:
            com.fossil.st4$g r0 = new com.fossil.st4$g
            r0.<init>(r8, r11)
            goto L_0x0014
        L_0x0049:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0051:
            com.fossil.el7.b(r1)
            com.fossil.st4$h r1 = new com.fossil.st4$h
            r1.<init>(r8, r9, r10, r4)
            r0.L$0 = r8
            r0.I$0 = r9
            r0.I$1 = r10
            r0.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r1, r0)
            if (r0 != r2) goto L_0x002c
            r0 = r2
            goto L_0x0042
        L_0x0069:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x0086
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0042
        L_0x0086:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.st4.e(int, int, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object f(java.lang.String r9, java.lang.String[] r10, com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.ms4>>> r11) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r11 instanceof com.fossil.st4.i
            if (r0 == 0) goto L_0x0048
            r0 = r11
            com.fossil.st4$i r0 = (com.fossil.st4.i) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0048
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0057
            if (r0 != r5) goto L_0x004f
            java.lang.Object r0 = r1.L$2
            java.lang.String[] r0 = (java.lang.String[]) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.st4 r0 = (com.fossil.st4) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x0031:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x006f
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            com.fossil.kq5 r1 = new com.fossil.kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x0047:
            return r0
        L_0x0048:
            com.fossil.st4$i r0 = new com.fossil.st4$i
            r0.<init>(r8, r11)
            r1 = r0
            goto L_0x0015
        L_0x004f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0057:
            com.fossil.el7.b(r2)
            com.fossil.st4$j r0 = new com.fossil.st4$j
            r0.<init>(r8, r9, r10, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r10
            r1.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r0, r1)
            if (r0 != r3) goto L_0x0031
            r0 = r3
            goto L_0x0047
        L_0x006f:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x008c
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0047
        L_0x008c:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.st4.f(java.lang.String, java.lang.String[], com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g(com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.mt4>>> r9) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.fossil.st4.k
            if (r0 == 0) goto L_0x003f
            r0 = r9
            com.fossil.st4$k r0 = (com.fossil.st4.k) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003f
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x004d
            if (r3 != r5) goto L_0x0045
            java.lang.Object r0 = r0.L$0
            com.fossil.st4 r0 = (com.fossil.st4) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0028:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0061
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            com.fossil.kq5 r1 = new com.fossil.kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x003e:
            return r0
        L_0x003f:
            com.fossil.st4$k r0 = new com.fossil.st4$k
            r0.<init>(r8, r9)
            goto L_0x0014
        L_0x0045:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004d:
            com.fossil.el7.b(r1)
            com.fossil.st4$l r1 = new com.fossil.st4$l
            r1.<init>(r8, r4)
            r0.L$0 = r8
            r0.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r1, r0)
            if (r0 != r2) goto L_0x0028
            r0 = r2
            goto L_0x003e
        L_0x0061:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x007e
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x003e
        L_0x007e:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.st4.g(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object h(java.lang.String r9, com.fossil.qn7<? super com.fossil.iq5<com.fossil.ps4>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.fossil.st4.m
            if (r0 == 0) goto L_0x004a
            r0 = r10
            com.fossil.st4$m r0 = (com.fossil.st4.m) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x004a
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0059
            if (r0 != r5) goto L_0x0051
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.st4 r0 = (com.fossil.st4) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x002d:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0080
            com.fossil.hz4 r1 = com.fossil.hz4.f1561a
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r1 = r0.a()
            com.fossil.gj4 r1 = (com.fossil.gj4) r1
            if (r1 != 0) goto L_0x006f
        L_0x003f:
            com.fossil.kq5 r1 = new com.fossil.kq5
            boolean r0 = r0.b()
            r1.<init>(r4, r0)
            r0 = r1
        L_0x0049:
            return r0
        L_0x004a:
            com.fossil.st4$m r0 = new com.fossil.st4$m
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0051:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0059:
            com.fossil.el7.b(r2)
            com.fossil.st4$n r0 = new com.fossil.st4$n
            r0.<init>(r8, r9, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x0049
        L_0x006f:
            com.google.gson.Gson r2 = new com.google.gson.Gson     // Catch:{ mj4 -> 0x007b }
            r2.<init>()     // Catch:{ mj4 -> 0x007b }
            java.lang.Class<com.fossil.ps4> r3 = com.fossil.ps4.class
            java.lang.Object r4 = r2.g(r1, r3)     // Catch:{ mj4 -> 0x007b }
            goto L_0x003f
        L_0x007b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003f
        L_0x0080:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x009d
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0049
        L_0x009d:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.st4.h(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object i(java.util.List<java.lang.String> r10, int r11, int r12, com.fossil.qn7<? super com.fossil.iq5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.ks4>>> r13) {
        /*
            r9 = this;
            r8 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 0
            boolean r0 = r13 instanceof com.fossil.st4.o
            if (r0 == 0) goto L_0x0047
            r0 = r13
            com.fossil.st4$o r0 = (com.fossil.st4.o) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0047
            int r1 = r1 + r3
            r0.label = r1
            r6 = r0
        L_0x0015:
            java.lang.Object r1 = r6.result
            java.lang.Object r7 = com.fossil.yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x0056
            if (r0 != r8) goto L_0x004e
            int r0 = r6.I$1
            int r0 = r6.I$0
            java.lang.Object r0 = r6.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r6.L$0
            com.fossil.st4 r0 = (com.fossil.st4) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0031:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0074
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            com.fossil.kq5 r1 = new com.fossil.kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
        L_0x0046:
            return r1
        L_0x0047:
            com.fossil.st4$o r0 = new com.fossil.st4$o
            r0.<init>(r9, r13)
            r6 = r0
            goto L_0x0015
        L_0x004e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0056:
            com.fossil.el7.b(r1)
            com.fossil.st4$p r0 = new com.fossil.st4$p
            r1 = r9
            r2 = r10
            r3 = r11
            r4 = r12
            r0.<init>(r1, r2, r3, r4, r5)
            r6.L$0 = r9
            r6.L$1 = r10
            r6.I$0 = r11
            r6.I$1 = r12
            r6.label = r8
            java.lang.Object r0 = com.fossil.jq5.d(r0, r6)
            if (r0 != r7) goto L_0x0031
            r1 = r7
            goto L_0x0046
        L_0x0074:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x0090
            com.fossil.hq5 r0 = (com.fossil.hq5) r0
            com.fossil.hq5 r1 = new com.fossil.hq5
            int r2 = r0.a()
            com.portfolio.platform.data.model.ServerError r3 = r0.c()
            java.lang.Throwable r4 = r0.d()
            r7 = 24
            r6 = r5
            r8 = r5
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            goto L_0x0046
        L_0x0090:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.st4.i(java.util.List, int, int, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object j(com.fossil.qn7<? super com.fossil.iq5<com.fossil.pt4>> r9) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.fossil.st4.q
            if (r0 == 0) goto L_0x0045
            r0 = r9
            com.fossil.st4$q r0 = (com.fossil.st4.q) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0045
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0053
            if (r3 != r5) goto L_0x004b
            java.lang.Object r0 = r0.L$0
            com.fossil.st4 r0 = (com.fossil.st4) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0028:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0078
            com.fossil.hz4 r1 = com.fossil.hz4.f1561a
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r1 = r0.a()
            com.fossil.gj4 r1 = (com.fossil.gj4) r1
            if (r1 != 0) goto L_0x0067
        L_0x003a:
            com.fossil.kq5 r1 = new com.fossil.kq5
            boolean r0 = r0.b()
            r1.<init>(r4, r0)
            r0 = r1
        L_0x0044:
            return r0
        L_0x0045:
            com.fossil.st4$q r0 = new com.fossil.st4$q
            r0.<init>(r8, r9)
            goto L_0x0014
        L_0x004b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0053:
            com.fossil.el7.b(r1)
            com.fossil.st4$r r1 = new com.fossil.st4$r
            r1.<init>(r8, r4)
            r0.L$0 = r8
            r0.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r1, r0)
            if (r0 != r2) goto L_0x0028
            r0 = r2
            goto L_0x0044
        L_0x0067:
            com.google.gson.Gson r2 = new com.google.gson.Gson     // Catch:{ mj4 -> 0x0073 }
            r2.<init>()     // Catch:{ mj4 -> 0x0073 }
            java.lang.Class<com.fossil.pt4> r3 = com.fossil.pt4.class
            java.lang.Object r4 = r2.g(r1, r3)     // Catch:{ mj4 -> 0x0073 }
            goto L_0x003a
        L_0x0073:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003a
        L_0x0078:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x0095
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0044
        L_0x0095:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.st4.j(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object k(java.lang.String r10, java.lang.String[] r11, com.fossil.qn7<? super com.fossil.iq5<com.fossil.ps4>> r12) {
        /*
        // Method dump skipped, instructions count: 231
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.st4.k(java.lang.String, java.lang.String[], com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object l(java.lang.String r9, int r10, java.lang.String r11, com.fossil.qn7<? super com.fossil.iq5<com.fossil.ps4>> r12) {
        /*
        // Method dump skipped, instructions count: 220
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.st4.l(java.lang.String, int, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object m(java.lang.String r9, com.fossil.qn7<? super com.fossil.iq5<com.fossil.ps4>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.fossil.st4.w
            if (r0 == 0) goto L_0x004e
            r0 = r10
            com.fossil.st4$w r0 = (com.fossil.st4.w) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x004e
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x005d
            if (r0 != r5) goto L_0x0055
            java.lang.Object r0 = r1.L$2
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.st4 r0 = (com.fossil.st4) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x0031:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0090
            com.fossil.hz4 r1 = com.fossil.hz4.f1561a
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r1 = r0.a()
            com.fossil.gj4 r1 = (com.fossil.gj4) r1
            if (r1 != 0) goto L_0x007f
        L_0x0043:
            com.fossil.kq5 r1 = new com.fossil.kq5
            boolean r0 = r0.b()
            r1.<init>(r4, r0)
            r0 = r1
        L_0x004d:
            return r0
        L_0x004e:
            com.fossil.st4$w r0 = new com.fossil.st4$w
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0055:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005d:
            com.fossil.el7.b(r2)
            com.fossil.gj4 r0 = new com.fossil.gj4
            r0.<init>()
            java.lang.String r2 = "challengeId"
            r0.n(r2, r9)
            com.fossil.st4$x r2 = new com.fossil.st4$x
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r0
            r1.label = r5
            java.lang.Object r0 = com.fossil.jq5.d(r2, r1)
            if (r0 != r3) goto L_0x0031
            r0 = r3
            goto L_0x004d
        L_0x007f:
            com.google.gson.Gson r2 = new com.google.gson.Gson     // Catch:{ mj4 -> 0x008b }
            r2.<init>()     // Catch:{ mj4 -> 0x008b }
            java.lang.Class<com.fossil.ps4> r3 = com.fossil.ps4.class
            java.lang.Object r4 = r2.g(r1, r3)     // Catch:{ mj4 -> 0x008b }
            goto L_0x0043
        L_0x008b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0043
        L_0x0090:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00ad
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x004d
        L_0x00ad:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.st4.m(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object n(com.fossil.ls4 r9, com.fossil.qn7<? super com.fossil.iq5<com.fossil.nt4>> r10) {
        /*
            r8 = this;
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.fossil.st4.y
            if (r0 == 0) goto L_0x004e
            r0 = r10
            com.fossil.st4$y r0 = (com.fossil.st4.y) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x004e
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x005d
            if (r0 != r6) goto L_0x0055
            java.lang.Object r0 = r1.L$2
            com.fossil.gj4 r0 = (com.fossil.gj4) r0
            java.lang.Object r0 = r1.L$1
            com.fossil.ls4 r0 = (com.fossil.ls4) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.st4 r0 = (com.fossil.st4) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x0031:
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0094
            com.fossil.hz4 r1 = com.fossil.hz4.f1561a
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r1 = r0.a()
            com.fossil.gj4 r1 = (com.fossil.gj4) r1
            if (r1 != 0) goto L_0x0083
        L_0x0043:
            com.fossil.kq5 r1 = new com.fossil.kq5
            boolean r0 = r0.b()
            r1.<init>(r4, r0)
            r0 = r1
        L_0x004d:
            return r0
        L_0x004e:
            com.fossil.st4$y r0 = new com.fossil.st4$y
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0055:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005d:
            com.fossil.el7.b(r2)
            com.fossil.gj4 r0 = new com.fossil.gj4
            r0.<init>()
            java.lang.String r2 = "encryptedDataBase64"
            java.lang.String r5 = r9.b()
            r0.n(r2, r5)
            com.fossil.st4$z r2 = new com.fossil.st4$z
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r0
            r1.label = r6
            java.lang.Object r0 = com.fossil.jq5.d(r2, r1)
            if (r0 != r3) goto L_0x0031
            r0 = r3
            goto L_0x004d
        L_0x0083:
            com.google.gson.Gson r2 = new com.google.gson.Gson     // Catch:{ mj4 -> 0x008f }
            r2.<init>()     // Catch:{ mj4 -> 0x008f }
            java.lang.Class<com.fossil.nt4> r3 = com.fossil.nt4.class
            java.lang.Object r4 = r2.g(r1, r3)     // Catch:{ mj4 -> 0x008f }
            goto L_0x0043
        L_0x008f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0043
        L_0x0094:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00b1
            r3 = r0
            com.fossil.hq5 r3 = (com.fossil.hq5) r3
            com.fossil.hq5 r0 = new com.fossil.hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x004d
        L_0x00b1:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.st4.n(com.fossil.ls4, com.fossil.qn7):java.lang.Object");
    }
}
