package com.fossil;

import android.text.TextUtils;
import android.util.Log;
import com.fossil.wb1;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cc1 implements wb1<InputStream> {
    @DexIgnore
    public static /* final */ b h; // = new a();
    @DexIgnore
    public /* final */ te1 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ b d;
    @DexIgnore
    public HttpURLConnection e;
    @DexIgnore
    public InputStream f;
    @DexIgnore
    public volatile boolean g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements b {
        @DexIgnore
        @Override // com.fossil.cc1.b
        public HttpURLConnection a(URL url) throws IOException {
            return (HttpURLConnection) url.openConnection();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        HttpURLConnection a(URL url) throws IOException;
    }

    @DexIgnore
    public cc1(te1 te1, int i) {
        this(te1, i, h);
    }

    @DexIgnore
    public cc1(te1 te1, int i, b bVar) {
        this.b = te1;
        this.c = i;
        this.d = bVar;
    }

    @DexIgnore
    public static boolean e(int i) {
        return i / 100 == 2;
    }

    @DexIgnore
    public static boolean f(int i) {
        return i / 100 == 3;
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public void a() {
        InputStream inputStream = this.f;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e2) {
            }
        }
        HttpURLConnection httpURLConnection = this.e;
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
        this.e = null;
    }

    @DexIgnore
    public final InputStream b(HttpURLConnection httpURLConnection) throws IOException {
        if (TextUtils.isEmpty(httpURLConnection.getContentEncoding())) {
            this.f = bk1.b(httpURLConnection.getInputStream(), (long) httpURLConnection.getContentLength());
        } else {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Got non empty content encoding: " + httpURLConnection.getContentEncoding());
            }
            this.f = httpURLConnection.getInputStream();
        }
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public gb1 c() {
        return gb1.REMOTE;
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public void cancel() {
        this.g = true;
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public void d(sa1 sa1, wb1.a<? super InputStream> aVar) {
        StringBuilder sb;
        long b2 = ek1.b();
        try {
            aVar.e(g(this.b.h(), 0, null, this.b.e()));
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new StringBuilder();
                sb.append("Finished http url fetcher fetch in ");
                sb.append(ek1.a(b2));
                Log.v("HttpUrlFetcher", sb.toString());
            }
        } catch (IOException e2) {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Failed to load data for url", e2);
            }
            aVar.b(e2);
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new StringBuilder();
            }
        } catch (Throwable th) {
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                Log.v("HttpUrlFetcher", "Finished http url fetcher fetch in " + ek1.a(b2));
            }
            throw th;
        }
    }

    @DexIgnore
    public final InputStream g(URL url, int i, URL url2, Map<String, String> map) throws IOException {
        if (i < 5) {
            if (url2 != null) {
                try {
                    if (url.toURI().equals(url2.toURI())) {
                        throw new kb1("In re-direct loop");
                    }
                } catch (URISyntaxException e2) {
                }
            }
            this.e = this.d.a(url);
            for (Map.Entry<String, String> entry : map.entrySet()) {
                this.e.addRequestProperty(entry.getKey(), entry.getValue());
            }
            this.e.setConnectTimeout(this.c);
            this.e.setReadTimeout(this.c);
            this.e.setUseCaches(false);
            this.e.setDoInput(true);
            this.e.setInstanceFollowRedirects(false);
            this.e.connect();
            this.f = this.e.getInputStream();
            if (this.g) {
                return null;
            }
            int responseCode = this.e.getResponseCode();
            if (e(responseCode)) {
                return b(this.e);
            }
            if (f(responseCode)) {
                String headerField = this.e.getHeaderField("Location");
                if (!TextUtils.isEmpty(headerField)) {
                    URL url3 = new URL(url, headerField);
                    a();
                    return g(url3, i + 1, url, map);
                }
                throw new kb1("Received empty or null redirect url");
            } else if (responseCode == -1) {
                throw new kb1(responseCode);
            } else {
                throw new kb1(this.e.getResponseMessage(), responseCode);
            }
        } else {
            throw new kb1("Too many (> 5) redirects!");
        }
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }
}
