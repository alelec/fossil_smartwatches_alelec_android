package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g35 extends f35 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D;
    @DexIgnore
    public long B;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        D = sparseIntArray;
        sparseIntArray.put(2131361987, 1);
        D.put(2131362244, 2);
        D.put(2131361979, 3);
        D.put(2131363055, 4);
        D.put(2131362495, 5);
        D.put(2131362964, 6);
        D.put(2131362524, 7);
        D.put(2131363410, 8);
        D.put(2131362666, 9);
        D.put(2131362783, 10);
    }
    */

    @DexIgnore
    public g35(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 11, C, D));
    }

    @DexIgnore
    public g35(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (RTLImageView) objArr[3], (ConstraintLayout) objArr[1], (FlexibleEditText) objArr[2], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[7], (RTLImageView) objArr[9], (View) objArr[10], (ProgressBar) objArr[6], (ConstraintLayout) objArr[0], (RecyclerView) objArr[4], (FlexibleTextView) objArr[8]);
        this.B = -1;
        this.y.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.B != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.B = 1;
        }
        w();
    }
}
