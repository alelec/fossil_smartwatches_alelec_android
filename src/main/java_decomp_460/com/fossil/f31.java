package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f31 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1044a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public f31(String str, int i) {
        this.f1044a = str;
        this.b = i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f31)) {
            return false;
        }
        f31 f31 = (f31) obj;
        if (this.b == f31.b) {
            return this.f1044a.equals(f31.f1044a);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return (this.f1044a.hashCode() * 31) + this.b;
    }
}
