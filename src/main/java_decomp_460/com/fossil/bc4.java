package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bc4 {
    @DexIgnore
    public static /* final */ short[] h; // = {10, 20, 30, 60, 120, 300};

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ hc4 f413a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ d94 d;
    @DexIgnore
    public /* final */ ac4 e;
    @DexIgnore
    public /* final */ a f;
    @DexIgnore
    public Thread g;

    @DexIgnore
    public interface a {
        @DexIgnore
        boolean a();
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        bc4 a(wc4 wc4);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        File[] a();

        @DexIgnore
        File[] b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends n84 {
        @DexIgnore
        public /* final */ List<ec4> b;
        @DexIgnore
        public /* final */ boolean c;
        @DexIgnore
        public /* final */ float d;

        @DexIgnore
        public d(List<ec4> list, boolean z, float f) {
            this.b = list;
            this.c = z;
            this.d = f;
        }

        @DexIgnore
        @Override // com.fossil.n84
        public void a() {
            try {
                b(this.b, this.c);
            } catch (Exception e2) {
                x74.f().e("An unexpected error occurred while attempting to upload crash reports.", e2);
            }
            bc4.this.g = null;
        }

        @DexIgnore
        public final void b(List<ec4> list, boolean z) {
            int i;
            x74 f = x74.f();
            f.b("Starting report processing in " + this.d + " second(s)...");
            float f2 = this.d;
            if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                try {
                    Thread.sleep((long) (f2 * 1000.0f));
                } catch (InterruptedException e2) {
                    Thread.currentThread().interrupt();
                    return;
                }
            }
            if (!bc4.this.f.a()) {
                int i2 = 0;
                while (list.size() > 0 && !bc4.this.f.a()) {
                    x74 f3 = x74.f();
                    f3.b("Attempting to send " + list.size() + " report(s)");
                    ArrayList arrayList = new ArrayList();
                    for (ec4 ec4 : list) {
                        if (!bc4.this.d(ec4, z)) {
                            arrayList.add(ec4);
                        }
                    }
                    if (arrayList.size() > 0) {
                        long j = (long) bc4.h[Math.min(i2, bc4.h.length - 1)];
                        x74 f4 = x74.f();
                        f4.b("Report submission: scheduling delayed retry in " + j + " seconds");
                        try {
                            Thread.sleep(j * 1000);
                            i = i2 + 1;
                        } catch (InterruptedException e3) {
                            Thread.currentThread().interrupt();
                            return;
                        }
                    } else {
                        i = i2;
                    }
                    i2 = i;
                    list = arrayList;
                }
            }
        }
    }

    @DexIgnore
    public bc4(String str, String str2, d94 d94, ac4 ac4, hc4 hc4, a aVar) {
        if (hc4 != null) {
            this.f413a = hc4;
            this.b = str;
            this.c = str2;
            this.d = d94;
            this.e = ac4;
            this.f = aVar;
            return;
        }
        throw new IllegalArgumentException("createReportCall must not be null.");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d A[Catch:{ Exception -> 0x003b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean d(com.fossil.ec4 r7, boolean r8) {
        /*
            r6 = this;
            r0 = 1
            r1 = 0
            com.fossil.cc4 r2 = new com.fossil.cc4     // Catch:{ Exception -> 0x003b }
            java.lang.String r3 = r6.b     // Catch:{ Exception -> 0x003b }
            java.lang.String r4 = r6.c     // Catch:{ Exception -> 0x003b }
            r2.<init>(r3, r4, r7)     // Catch:{ Exception -> 0x003b }
            com.fossil.d94 r3 = r6.d     // Catch:{ Exception -> 0x003b }
            com.fossil.d94 r4 = com.fossil.d94.ALL     // Catch:{ Exception -> 0x003b }
            if (r3 != r4) goto L_0x0023
            com.fossil.x74 r2 = com.fossil.x74.f()     // Catch:{ Exception -> 0x003b }
            java.lang.String r3 = "Send to Reports Endpoint disabled. Removing Reports Endpoint report."
            r2.b(r3)     // Catch:{ Exception -> 0x003b }
        L_0x001a:
            r2 = r0
        L_0x001b:
            if (r2 == 0) goto L_0x0083
            com.fossil.ac4 r2 = r6.e     // Catch:{ Exception -> 0x003b }
            r2.b(r7)     // Catch:{ Exception -> 0x003b }
        L_0x0022:
            return r0
        L_0x0023:
            com.fossil.d94 r3 = r6.d     // Catch:{ Exception -> 0x003b }
            com.fossil.d94 r4 = com.fossil.d94.JAVA_ONLY     // Catch:{ Exception -> 0x003b }
            if (r3 != r4) goto L_0x0056
            com.fossil.ec4$a r3 = r7.getType()     // Catch:{ Exception -> 0x003b }
            com.fossil.ec4$a r4 = com.fossil.ec4.a.JAVA     // Catch:{ Exception -> 0x003b }
            if (r3 != r4) goto L_0x0056
            com.fossil.x74 r2 = com.fossil.x74.f()     // Catch:{ Exception -> 0x003b }
            java.lang.String r3 = "Send to Reports Endpoint for non-native reports disabled. Removing Reports Uploader report."
            r2.b(r3)     // Catch:{ Exception -> 0x003b }
            goto L_0x001a
        L_0x003b:
            r0 = move-exception
            com.fossil.x74 r2 = com.fossil.x74.f()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Error occurred sending report "
            r3.append(r4)
            r3.append(r7)
            java.lang.String r3 = r3.toString()
            r2.e(r3, r0)
            r0 = r1
            goto L_0x0022
        L_0x0056:
            com.fossil.hc4 r3 = r6.f413a
            boolean r2 = r3.b(r2, r8)
            com.fossil.x74 r4 = com.fossil.x74.f()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r3 = "Crashlytics Reports Endpoint upload "
            r5.append(r3)
            if (r2 == 0) goto L_0x0080
            java.lang.String r3 = "complete: "
        L_0x006e:
            r5.append(r3)
            java.lang.String r3 = r7.b()
            r5.append(r3)
            java.lang.String r3 = r5.toString()
            r4.g(r3)
            goto L_0x001b
        L_0x0080:
            java.lang.String r3 = "FAILED: "
            goto L_0x006e
        L_0x0083:
            r0 = r1
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bc4.d(com.fossil.ec4, boolean):boolean");
    }

    @DexIgnore
    public void e(List<ec4> list, boolean z, float f2) {
        synchronized (this) {
            if (this.g != null) {
                x74.f().b("Report upload has already been started.");
                return;
            }
            Thread thread = new Thread(new d(list, z, f2), "Crashlytics Report Uploader");
            this.g = thread;
            thread.start();
        }
    }
}
