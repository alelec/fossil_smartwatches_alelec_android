package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class us3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<us3> CREATOR; // = new ts3();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ z52 c;
    @DexIgnore
    public /* final */ tc2 d;

    @DexIgnore
    public us3(int i) {
        this(new z52(8, null), null);
    }

    @DexIgnore
    public us3(int i, z52 z52, tc2 tc2) {
        this.b = i;
        this.c = z52;
        this.d = tc2;
    }

    @DexIgnore
    public us3(z52 z52, tc2 tc2) {
        this(1, z52, null);
    }

    @DexIgnore
    public final z52 c() {
        return this.c;
    }

    @DexIgnore
    public final tc2 f() {
        return this.d;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.n(parcel, 1, this.b);
        bd2.t(parcel, 2, this.c, i, false);
        bd2.t(parcel, 3, this.d, i, false);
        bd2.b(parcel, a2);
    }
}
