package com.fossil;

import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ar3 implements ql3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ String f307a;
    @DexIgnore
    public /* final */ /* synthetic */ yq3 b;

    @DexIgnore
    public ar3(yq3 yq3, String str) {
        this.b = yq3;
        this.f307a = str;
    }

    @DexIgnore
    @Override // com.fossil.ql3
    public final void a(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        this.b.j(i, th, bArr, this.f307a);
    }
}
