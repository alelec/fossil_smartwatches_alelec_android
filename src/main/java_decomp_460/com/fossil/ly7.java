package com.fossil;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ly7<E> {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater b; // = AtomicReferenceFieldUpdater.newUpdater(ly7.class, Object.class, "onCloseHandler");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ jz7 f2282a; // = new jz7();
    @DexIgnore
    public volatile Object onCloseHandler; // = null;

    @DexIgnore
    public boolean c(Throwable th) {
        boolean z;
        py7<?> py7 = new py7<>(th);
        lz7 lz7 = this.f2282a;
        while (true) {
            lz7 n = lz7.n();
            z = true;
            if (!(n instanceof py7)) {
                if (n.g(py7, lz7)) {
                    break;
                }
            } else {
                z = false;
                break;
            }
        }
        if (!z) {
            lz7 n2 = this.f2282a.n();
            if (n2 != null) {
                py7 = (py7) n2;
            } else {
                throw new il7("null cannot be cast to non-null type kotlinx.coroutines.channels.Closed<*>");
            }
        }
        i(py7);
        if (z) {
            j(th);
        }
        return z;
    }

    @DexIgnore
    public final int d() {
        jz7 jz7 = this.f2282a;
        Object l = jz7.l();
        if (l != null) {
            int i = 0;
            lz7 lz7 = (lz7) l;
            while (!pq7.a(lz7, jz7)) {
                int i2 = lz7 instanceof lz7 ? i + 1 : i;
                lz7 = lz7.m();
                i = i2;
            }
            return i;
        }
        throw new il7("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public String e() {
        return "";
    }

    @DexIgnore
    public final py7<?> f() {
        lz7 n = this.f2282a.n();
        if (!(n instanceof py7)) {
            n = null;
        }
        py7<?> py7 = (py7) n;
        if (py7 == null) {
            return null;
        }
        i(py7);
        return py7;
    }

    @DexIgnore
    public final jz7 g() {
        return this.f2282a;
    }

    @DexIgnore
    public final String h() {
        String str;
        lz7 m = this.f2282a.m();
        if (m == this.f2282a) {
            return "EmptyQueue";
        }
        if (m instanceof py7) {
            str = m.toString();
        } else if (m instanceof uy7) {
            str = "ReceiveQueued";
        } else if (m instanceof xy7) {
            str = "SendQueued";
        } else {
            str = "UNEXPECTED:" + m;
        }
        lz7 n = this.f2282a.n();
        if (n == m) {
            return str;
        }
        String str2 = str + ",queueSize=" + d();
        if (!(n instanceof py7)) {
            return str2;
        }
        return str2 + ",closedForSend=" + n;
    }

    @DexIgnore
    public final void i(py7<?> py7) {
        Object b2 = iz7.b(null, 1, null);
        while (true) {
            lz7 n = py7.n();
            if (!(n instanceof uy7)) {
                n = null;
            }
            uy7 uy7 = (uy7) n;
            if (uy7 == null) {
                break;
            } else if (!uy7.r()) {
                uy7.o();
            } else {
                b2 = iz7.c(b2, uy7);
            }
        }
        if (b2 != null) {
            if (!(b2 instanceof ArrayList)) {
                ((uy7) b2).w(py7);
            } else if (b2 != null) {
                ArrayList arrayList = (ArrayList) b2;
                for (int size = arrayList.size() - 1; size >= 0; size--) {
                    ((uy7) arrayList.get(size)).w(py7);
                }
            } else {
                throw new il7("null cannot be cast to non-null type kotlin.collections.ArrayList<E> /* = java.util.ArrayList<E> */");
            }
        }
        k(py7);
    }

    @DexIgnore
    public final void j(Throwable th) {
        Object obj;
        Object obj2 = this.onCloseHandler;
        if (obj2 != null && obj2 != (obj = ky7.c) && b.compareAndSet(this, obj2, obj)) {
            ir7.d(obj2, 1);
            ((rp7) obj2).invoke(th);
        }
    }

    @DexIgnore
    public void k(lz7 lz7) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        r0 = null;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.wy7<E> l() {
        /*
            r3 = this;
            com.fossil.jz7 r2 = r3.f2282a
        L_0x0002:
            java.lang.Object r0 = r2.l()
            if (r0 == 0) goto L_0x002b
            com.fossil.lz7 r0 = (com.fossil.lz7) r0
            if (r0 != r2) goto L_0x0010
        L_0x000c:
            r0 = 0
        L_0x000d:
            com.fossil.wy7 r0 = (com.fossil.wy7) r0
            return r0
        L_0x0010:
            boolean r1 = r0 instanceof com.fossil.wy7
            if (r1 == 0) goto L_0x000c
            r1 = r0
            com.fossil.wy7 r1 = (com.fossil.wy7) r1
            boolean r1 = r1 instanceof com.fossil.py7
            if (r1 == 0) goto L_0x0021
            boolean r1 = r0.q()
            if (r1 == 0) goto L_0x000d
        L_0x0021:
            com.fossil.lz7 r1 = r0.t()
            if (r1 == 0) goto L_0x000d
            r1.p()
            goto L_0x0002
        L_0x002b:
            com.fossil.il7 r0 = new com.fossil.il7
        */
        //  java.lang.String r1 = "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */"
        /*
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ly7.l():com.fossil.wy7");
    }

    @DexIgnore
    public String toString() {
        return ov7.a(this) + '@' + ov7.b(this) + '{' + h() + '}' + e();
    }
}
