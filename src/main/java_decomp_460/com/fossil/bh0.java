package com.fossil;

import android.view.Menu;
import android.view.Window;
import com.fossil.ig0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface bh0 {
    @DexIgnore
    void a(Menu menu, ig0.a aVar);

    @DexIgnore
    boolean b();

    @DexIgnore
    Object c();  // void declaration

    @DexIgnore
    boolean d();

    @DexIgnore
    boolean e();

    @DexIgnore
    boolean f();

    @DexIgnore
    boolean g();

    @DexIgnore
    void h(int i);

    @DexIgnore
    Object i();  // void declaration

    @DexIgnore
    void setWindowCallback(Window.Callback callback);

    @DexIgnore
    void setWindowTitle(CharSequence charSequence);
}
