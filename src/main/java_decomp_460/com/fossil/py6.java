package com.fossil;

import android.os.CountDownTimer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class py6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public CountDownTimer f2893a; // = new a(this, 30000, 1000);
    @DexIgnore
    public px6 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends CountDownTimer {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ py6 f2894a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(py6 py6, long j, long j2) {
            super(j, j2);
            this.f2894a = py6;
        }

        @DexIgnore
        public void onFinish() {
        }

        @DexIgnore
        public void onTick(long j) {
            px6 px6 = this.f2894a.b;
            if (px6 != null) {
                px6.Y6((int) (j / ((long) 1000)));
            }
        }
    }

    @DexIgnore
    public py6(px6 px6) {
        this.b = px6;
    }

    @DexIgnore
    public final void b() {
        this.f2893a.cancel();
        this.b = null;
    }

    @DexIgnore
    public final void c(boolean z) {
        this.f2893a.cancel();
        if (z) {
            this.f2893a.start();
        }
    }
}
