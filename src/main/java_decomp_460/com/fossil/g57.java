package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g57 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public CharSequence f1264a;
    @DexIgnore
    public CharSequence b;
    @DexIgnore
    public CharSequence c;

    @DexIgnore
    public CharSequence a() {
        return this.b;
    }

    @DexIgnore
    public CharSequence b(boolean z) {
        return z ? this.f1264a : this.c;
    }

    @DexIgnore
    public void c(CharSequence charSequence) {
        this.b = charSequence;
    }

    @DexIgnore
    public void d(CharSequence charSequence) {
        this.f1264a = charSequence;
    }

    @DexIgnore
    public void e(CharSequence charSequence) {
        this.c = charSequence;
    }
}
