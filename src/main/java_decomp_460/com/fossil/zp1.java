package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zp1 extends vp1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<zp1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public zp1 createFromParcel(Parcel parcel) {
            return new zp1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public zp1[] newArray(int i) {
            return new zp1[i];
        }
    }

    @DexIgnore
    public zp1(byte b, long j, int i) {
        super(np1.WORKOUT_PAUSE, b);
        this.d = j;
        this.e = i;
    }

    @DexIgnore
    public /* synthetic */ zp1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.d = parcel.readLong();
        this.e = parcel.readInt();
    }

    @DexIgnore
    public final int b() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(zp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            zp1 zp1 = (zp1) obj;
            if (this.d != zp1.d) {
                return false;
            }
            return this.e == zp1.e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.PauseWorkoutNotification");
    }

    @DexIgnore
    public final long getSessionId() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public int hashCode() {
        int hashCode = super.hashCode();
        return (((hashCode * 31) + Long.valueOf(this.d).hashCode()) * 31) + Integer.valueOf(this.e).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(super.toJSONObject(), jd0.H5, Long.valueOf(this.d)), jd0.q, Integer.valueOf(this.e));
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }
}
