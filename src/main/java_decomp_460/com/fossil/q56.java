package com.fossil;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.kx5;
import com.fossil.t47;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q56 extends pv5 implements p56, t47.g {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public o56 g;
    @DexIgnore
    public g37<h95> h;
    @DexIgnore
    public kx5 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return q56.k;
        }

        @DexIgnore
        public final q56 b() {
            return new q56();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements kx5.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ q56 f2923a;

        @DexIgnore
        public b(q56 q56) {
            this.f2923a = q56;
        }

        @DexIgnore
        @Override // com.fossil.kx5.a
        public void a(j06 j06) {
            pq7.c(j06, "contactWrapper");
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.f2923a.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.b0(childFragmentManager, j06, j06.getCurrentHandGroup(), q56.L6(this.f2923a).n());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q56 b;

        @DexIgnore
        public c(q56 q56) {
            this.b = q56;
        }

        @DexIgnore
        public final void onClick(View view) {
            q56.L6(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ q56 b;
        @DexIgnore
        public /* final */ /* synthetic */ h95 c;

        @DexIgnore
        public d(q56 q56, h95 h95) {
            this.b = q56;
            this.c = h95;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            RTLImageView rTLImageView = this.c.t;
            pq7.b(rTLImageView, "binding.ivClear");
            rTLImageView.setVisibility(i3 > 0 ? 0 : 4);
            kx5 kx5 = this.b.i;
            if (kx5 != null) {
                kx5.w(String.valueOf(charSequence));
            }
            AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = this.c.w;
            pq7.b(alphabetFastScrollRecyclerView, "binding.rvContacts");
            RecyclerView.m layoutManager = alphabetFastScrollRecyclerView.getLayoutManager();
            if (layoutManager != null) {
                ((LinearLayoutManager) layoutManager).D2(0, 0);
                return;
            }
            throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ h95 b;

        @DexIgnore
        public e(h95 h95) {
            this.b = h95;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                h95 h95 = this.b;
                pq7.b(h95, "binding");
                h95.n().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ h95 b;

        @DexIgnore
        public f(h95 h95) {
            this.b = h95;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.q.setText("");
        }
    }

    /*
    static {
        String simpleName = q56.class.getSimpleName();
        pq7.b(simpleName, "NotificationHybridContac\u2026nt::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ o56 L6(q56 q56) {
        o56 o56 = q56.g;
        if (o56 != null) {
            return o56;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.p56
    public void A3() {
        kx5 kx5 = this.i;
        if (kx5 != null) {
            kx5.notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        o56 o56 = this.g;
        if (o56 != null) {
            o56.o();
            return true;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.p56
    public void I(ArrayList<j06> arrayList) {
        pq7.c(arrayList, "contactWrappersSelected");
        Intent intent = new Intent();
        intent.putExtra("CONTACT_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    /* renamed from: N6 */
    public void M5(o56 o56) {
        pq7.c(o56, "presenter");
        this.g = o56;
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        j06 j06;
        pq7.c(str, "tag");
        if (str.hashCode() != 1018078562 || !str.equals("CONFIRM_REASSIGN_CONTACT")) {
            return;
        }
        if (i2 != 2131363373) {
            kx5 kx5 = this.i;
            if (kx5 != null) {
                kx5.notifyDataSetChanged();
            } else {
                pq7.i();
                throw null;
            }
        } else {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (j06 = (j06) extras.getSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER")) != null) {
                o56 o56 = this.g;
                if (o56 != null) {
                    o56.p(j06);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.p56
    public void T(Cursor cursor) {
        kx5 kx5 = this.i;
        if (kx5 != null) {
            kx5.l(cursor);
        }
    }

    @DexIgnore
    @Override // com.fossil.p56
    public void V() {
    }

    @DexIgnore
    @Override // com.fossil.p56
    public void n1(List<j06> list, FilterQueryProvider filterQueryProvider, int i2) {
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView;
        pq7.c(list, "listContactWrapper");
        pq7.c(filterQueryProvider, "filterQueryProvider");
        kx5 kx5 = new kx5(null, list, i2);
        kx5.z(new b(this));
        this.i = kx5;
        if (kx5 != null) {
            kx5.k(filterQueryProvider);
            g37<h95> g37 = this.h;
            if (g37 != null) {
                h95 a2 = g37.a();
                if (a2 != null && (alphabetFastScrollRecyclerView = a2.w) != null) {
                    alphabetFastScrollRecyclerView.setAdapter(this.i);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        h95 h95 = (h95) aq0.f(layoutInflater, 2131558594, viewGroup, false, A6());
        h95.s.setOnClickListener(new c(this));
        h95.q.addTextChangedListener(new d(this, h95));
        h95.q.setOnFocusChangeListener(new e(h95));
        h95.t.setOnClickListener(new f(h95));
        h95.w.setIndexBarVisibility(true);
        h95.w.setIndexBarHighLateTextVisibility(true);
        h95.w.setIndexbarHighLateTextColor(2131099703);
        h95.w.setIndexBarTransparentValue(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        h95.w.setIndexTextSize(9);
        Typeface b2 = nl0.b(requireContext(), 2131296256);
        String d2 = qn5.l.a().d("primaryText");
        Typeface f2 = qn5.l.a().f("nonBrandTextStyle5");
        if (f2 == null) {
            f2 = b2;
        }
        if (!TextUtils.isEmpty(d2)) {
            int parseColor = Color.parseColor(d2);
            h95.w.setIndexBarTextColor(parseColor);
            h95.w.setIndexbarHighLateTextColor(parseColor);
        }
        if (f2 != null) {
            h95.w.setTypeface(f2);
        }
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = h95.w;
        alphabetFastScrollRecyclerView.setLayoutManager(new LinearLayoutManager(alphabetFastScrollRecyclerView.getContext()));
        alphabetFastScrollRecyclerView.setAdapter(this.i);
        this.h = new g37<>(this, h95);
        pq7.b(h95, "binding");
        return h95.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        o56 o56 = this.g;
        if (o56 != null) {
            o56.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        o56 o56 = this.g;
        if (o56 != null) {
            o56.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
