package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p74<T> implements mg4<T> {
    @DexIgnore
    public static /* final */ Object c; // = new Object();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public volatile Object f2795a; // = c;
    @DexIgnore
    public volatile mg4<T> b;

    @DexIgnore
    public p74(mg4<T> mg4) {
        this.b = mg4;
    }

    @DexIgnore
    @Override // com.fossil.mg4
    public T get() {
        T t = (T) this.f2795a;
        if (t == c) {
            synchronized (this) {
                t = this.f2795a;
                if (t == c) {
                    t = this.b.get();
                    this.f2795a = t;
                    this.b = null;
                }
            }
        }
        return t;
    }
}
