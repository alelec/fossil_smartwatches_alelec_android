package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j22 extends q22 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ long f1704a;
    @DexIgnore
    public /* final */ h02 b;
    @DexIgnore
    public /* final */ c02 c;

    @DexIgnore
    public j22(long j, h02 h02, c02 c02) {
        this.f1704a = j;
        if (h02 != null) {
            this.b = h02;
            if (c02 != null) {
                this.c = c02;
                return;
            }
            throw new NullPointerException("Null event");
        }
        throw new NullPointerException("Null transportContext");
    }

    @DexIgnore
    @Override // com.fossil.q22
    public c02 b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.q22
    public long c() {
        return this.f1704a;
    }

    @DexIgnore
    @Override // com.fossil.q22
    public h02 d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof q22)) {
            return false;
        }
        q22 q22 = (q22) obj;
        return this.f1704a == q22.c() && this.b.equals(q22.d()) && this.c.equals(q22.b());
    }

    @DexIgnore
    public int hashCode() {
        long j = this.f1704a;
        return ((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "PersistedEvent{id=" + this.f1704a + ", transportContext=" + this.b + ", event=" + this.c + "}";
    }
}
