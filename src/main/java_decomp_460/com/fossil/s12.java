package com.fossil;

import com.fossil.v12;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s12 extends v12.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ long f3191a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ Set<v12.c> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v12.b.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Long f3192a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public Set<v12.c> c;

        @DexIgnore
        @Override // com.fossil.v12.b.a
        public v12.b a() {
            String str = "";
            if (this.f3192a == null) {
                str = " delta";
            }
            if (this.b == null) {
                str = str + " maxAllowedDelay";
            }
            if (this.c == null) {
                str = str + " flags";
            }
            if (str.isEmpty()) {
                return new s12(this.f3192a.longValue(), this.b.longValue(), this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.v12.b.a
        public v12.b.a b(long j) {
            this.f3192a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v12.b.a
        public v12.b.a c(Set<v12.c> set) {
            if (set != null) {
                this.c = set;
                return this;
            }
            throw new NullPointerException("Null flags");
        }

        @DexIgnore
        @Override // com.fossil.v12.b.a
        public v12.b.a d(long j) {
            this.b = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public s12(long j, long j2, Set<v12.c> set) {
        this.f3191a = j;
        this.b = j2;
        this.c = set;
    }

    @DexIgnore
    @Override // com.fossil.v12.b
    public long b() {
        return this.f3191a;
    }

    @DexIgnore
    @Override // com.fossil.v12.b
    public Set<v12.c> c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v12.b
    public long d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v12.b)) {
            return false;
        }
        v12.b bVar = (v12.b) obj;
        return this.f3191a == bVar.b() && this.b == bVar.d() && this.c.equals(bVar.c());
    }

    @DexIgnore
    public int hashCode() {
        long j = this.f3191a;
        long j2 = this.b;
        return ((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ConfigValue{delta=" + this.f3191a + ", maxAllowedDelay=" + this.b + ", flags=" + this.c + "}";
    }
}
