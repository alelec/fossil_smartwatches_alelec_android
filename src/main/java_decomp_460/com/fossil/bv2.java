package com.fossil;

import com.fossil.e13;
import com.j256.ormlite.stmt.query.SimpleComparison;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bv2 extends e13<bv2, a> implements o23 {
    @DexIgnore
    public static /* final */ bv2 zzf;
    @DexIgnore
    public static volatile z23<bv2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd; // = 1;
    @DexIgnore
    public m13<xu2> zze; // = e13.B();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<bv2, a> implements o23 {
        @DexIgnore
        public a() {
            super(bv2.zzf);
        }

        @DexIgnore
        public /* synthetic */ a(tu2 tu2) {
            this();
        }
    }

    @DexIgnore
    public enum b implements g13 {
        RADS(1),
        PROVISIONING(2);
        
        @DexIgnore
        public /* final */ int zzd;

        @DexIgnore
        public b(int i) {
            this.zzd = i;
        }

        @DexIgnore
        public static b zza(int i) {
            if (i == 1) {
                return RADS;
            }
            if (i != 2) {
                return null;
            }
            return PROVISIONING;
        }

        @DexIgnore
        public static i13 zzb() {
            return fv2.f1215a;
        }

        @DexIgnore
        public final String toString() {
            return SimpleComparison.LESS_THAN_OPERATION + b.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzd + " name=" + name() + '>';
        }

        @DexIgnore
        @Override // com.fossil.g13
        public final int zza() {
            return this.zzd;
        }
    }

    /*
    static {
        bv2 bv2 = new bv2();
        zzf = bv2;
        e13.u(bv2.class, bv2);
    }
    */

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (tu2.f3469a[i - 1]) {
            case 1:
                return new bv2();
            case 2:
                return new a(null);
            case 3:
                i13 zzb = b.zzb();
                return e13.s(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u100c\u0000\u0002\u001b", new Object[]{"zzc", "zzd", zzb, "zze", xu2.class});
            case 4:
                return zzf;
            case 5:
                z23<bv2> z232 = zzg;
                if (z232 != null) {
                    return z232;
                }
                synchronized (bv2.class) {
                    try {
                        z23 = zzg;
                        if (z23 == null) {
                            z23 = new e13.c(zzf);
                            zzg = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
