package com.fossil;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h65 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ TabLayout s;
    @DexIgnore
    public /* final */ DashBar t;
    @DexIgnore
    public /* final */ ViewPager2 u;

    @DexIgnore
    public h65(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, TabLayout tabLayout, DashBar dashBar, ViewPager2 viewPager2) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleTextView;
        this.s = tabLayout;
        this.t = dashBar;
        this.u = viewPager2;
    }
}
