package com.fossil;

import com.fossil.bv2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fv2 implements i13 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ i13 f1215a; // = new fv2();

    @DexIgnore
    @Override // com.fossil.i13
    public final boolean zza(int i) {
        return bv2.b.zza(i) != null;
    }
}
