package com.fossil;

import java.util.Comparator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nd0<T> extends CopyOnWriteArrayList<T> {
    @DexIgnore
    public /* final */ Comparator<T> b;

    @DexIgnore
    public nd0(Comparator<T> comparator) {
        this.b = comparator;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.Comparator<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final int a(T t) {
        int i;
        synchronized (this) {
            int size = super.size();
            i = 0;
            while (true) {
                if (i >= size) {
                    i = super.size();
                    break;
                }
                Object I = pm7.I(this, i);
                if (I != null && this.b.compare(t, I) <= 0) {
                    i++;
                }
            }
        }
        return i;
    }

    @DexIgnore
    public final boolean b(T t) {
        synchronized (this) {
            if (indexOf(t) < 0) {
                try {
                    add(a(t), t);
                } catch (IndexOutOfBoundsException e) {
                    add(t);
                }
            }
        }
        return true;
    }

    @DexIgnore
    @Override // java.util.List, java.util.concurrent.CopyOnWriteArrayList
    public final T remove(int i) {
        return (T) super.remove(i);
    }

    @DexIgnore
    public final int size() {
        return super.size();
    }
}
