package com.fossil;

import android.net.Uri;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.InstalledApp;
import java.io.Serializable;
import java.net.URI;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i06 implements Comparable<Object>, Serializable {
    @DexIgnore
    public int currentHandGroup;
    @DexIgnore
    public int iconResourceId; // = -1;
    @DexIgnore
    public InstalledApp installedApp;
    @DexIgnore
    public URI mURI;

    @DexIgnore
    @Override // java.lang.Comparable
    public int compareTo(Object obj) {
        pq7.c(obj, FacebookRequestErrorClassification.KEY_OTHER);
        if (!(obj instanceof i06)) {
            return 0;
        }
        InstalledApp installedApp2 = this.installedApp;
        if (installedApp2 != null) {
            String title = installedApp2.getTitle();
            pq7.b(title, "installedApp!!.title");
            if (title != null) {
                String lowerCase = title.toLowerCase();
                pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                InstalledApp installedApp3 = ((i06) obj).installedApp;
                if (installedApp3 != null) {
                    String title2 = installedApp3.getTitle();
                    pq7.b(title2, "other.installedApp!!.title");
                    if (title2 != null) {
                        String lowerCase2 = title2.toLowerCase();
                        pq7.b(lowerCase2, "(this as java.lang.String).toLowerCase()");
                        return lowerCase.compareTo(lowerCase2);
                    }
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
                pq7.i();
                throw null;
            }
            throw new il7("null cannot be cast to non-null type java.lang.String");
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final int getCurrentHandGroup() {
        return this.currentHandGroup;
    }

    @DexIgnore
    public final int getIconResourceId() {
        return this.iconResourceId;
    }

    @DexIgnore
    public final InstalledApp getInstalledApp() {
        return this.installedApp;
    }

    @DexIgnore
    public final Uri getUri() {
        URI uri = this.mURI;
        if (uri != null) {
            return Uri.parse(String.valueOf(uri));
        }
        return null;
    }

    @DexIgnore
    public final void setCurrentHandGroup(int i) {
        this.currentHandGroup = i;
    }

    @DexIgnore
    public final void setIconResourceId(int i) {
        this.iconResourceId = i;
    }

    @DexIgnore
    public final void setInstalledApp(InstalledApp installedApp2) {
        this.installedApp = installedApp2;
    }

    @DexIgnore
    public final void setUri(Uri uri) {
        if (uri == null) {
            this.mURI = null;
            return;
        }
        try {
            this.mURI = new URI(uri.toString());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            e.printStackTrace();
            local.e("AppWrapper", String.valueOf(tl7.f3441a));
            this.mURI = null;
        }
    }
}
