package com.fossil;

import android.graphics.Bitmap;
import android.util.Log;
import com.fossil.bb1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fb1 implements bb1 {
    @DexIgnore
    public static /* final */ String u; // = "fb1";

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int[] f1096a;
    @DexIgnore
    public /* final */ int[] b;
    @DexIgnore
    public /* final */ bb1.a c;
    @DexIgnore
    public ByteBuffer d;
    @DexIgnore
    public byte[] e;
    @DexIgnore
    public short[] f;
    @DexIgnore
    public byte[] g;
    @DexIgnore
    public byte[] h;
    @DexIgnore
    public byte[] i;
    @DexIgnore
    public int[] j;
    @DexIgnore
    public int k;
    @DexIgnore
    public db1 l;
    @DexIgnore
    public Bitmap m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public Boolean s;
    @DexIgnore
    public Bitmap.Config t;

    @DexIgnore
    public fb1(bb1.a aVar) {
        this.b = new int[256];
        this.t = Bitmap.Config.ARGB_8888;
        this.c = aVar;
        this.l = new db1();
    }

    @DexIgnore
    public fb1(bb1.a aVar, db1 db1, ByteBuffer byteBuffer, int i2) {
        this(aVar);
        r(db1, byteBuffer, i2);
    }

    @DexIgnore
    @Override // com.fossil.bb1
    public Bitmap a() {
        synchronized (this) {
            if (this.l.c <= 0 || this.k < 0) {
                if (Log.isLoggable(u, 3)) {
                    Log.d(u, "Unable to decode frame, frameCount=" + this.l.c + ", framePointer=" + this.k);
                }
                this.o = 1;
            }
            if (this.o == 1 || this.o == 2) {
                if (Log.isLoggable(u, 3)) {
                    Log.d(u, "Unable to decode frame, status=" + this.o);
                }
                return null;
            }
            this.o = 0;
            if (this.e == null) {
                this.e = this.c.e(255);
            }
            cb1 cb1 = this.l.e.get(this.k);
            int i2 = this.k - 1;
            cb1 cb12 = i2 >= 0 ? this.l.e.get(i2) : null;
            int[] iArr = cb1.k != null ? cb1.k : this.l.f761a;
            this.f1096a = iArr;
            if (iArr == null) {
                if (Log.isLoggable(u, 3)) {
                    Log.d(u, "No valid color table found for frame #" + this.k);
                }
                this.o = 1;
                return null;
            }
            if (cb1.f) {
                System.arraycopy(iArr, 0, this.b, 0, iArr.length);
                int[] iArr2 = this.b;
                this.f1096a = iArr2;
                iArr2[cb1.h] = 0;
                if (cb1.g == 2 && this.k == 0) {
                    this.s = Boolean.TRUE;
                }
            }
            return s(cb1, cb12);
        }
    }

    @DexIgnore
    @Override // com.fossil.bb1
    public void b() {
        this.k = (this.k + 1) % this.l.c;
    }

    @DexIgnore
    @Override // com.fossil.bb1
    public int c() {
        return this.l.c;
    }

    @DexIgnore
    @Override // com.fossil.bb1
    public void clear() {
        this.l = null;
        byte[] bArr = this.i;
        if (bArr != null) {
            this.c.d(bArr);
        }
        int[] iArr = this.j;
        if (iArr != null) {
            this.c.f(iArr);
        }
        Bitmap bitmap = this.m;
        if (bitmap != null) {
            this.c.c(bitmap);
        }
        this.m = null;
        this.d = null;
        this.s = null;
        byte[] bArr2 = this.e;
        if (bArr2 != null) {
            this.c.d(bArr2);
        }
    }

    @DexIgnore
    @Override // com.fossil.bb1
    public int d() {
        int i2;
        if (this.l.c <= 0 || (i2 = this.k) < 0) {
            return 0;
        }
        return n(i2);
    }

    @DexIgnore
    @Override // com.fossil.bb1
    public void e(Bitmap.Config config) {
        if (config == Bitmap.Config.ARGB_8888 || config == Bitmap.Config.RGB_565) {
            this.t = config;
            return;
        }
        throw new IllegalArgumentException("Unsupported format: " + config + ", must be one of " + Bitmap.Config.ARGB_8888 + " or " + Bitmap.Config.RGB_565);
    }

    @DexIgnore
    @Override // com.fossil.bb1
    public ByteBuffer f() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.bb1
    public void g() {
        this.k = -1;
    }

    @DexIgnore
    @Override // com.fossil.bb1
    public int h() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.bb1
    public int i() {
        return this.d.limit() + this.i.length + (this.j.length * 4);
    }

    @DexIgnore
    public final int j(int i2, int i3, int i4) {
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        for (int i10 = i2; i10 < this.p + i2; i10++) {
            byte[] bArr = this.i;
            if (i10 >= bArr.length || i10 >= i3) {
                break;
            }
            int i11 = this.f1096a[bArr[i10] & 255];
            if (i11 != 0) {
                i9 += (i11 >> 24) & 255;
                i8 += (i11 >> 16) & 255;
                i7 += (i11 >> 8) & 255;
                i6 += i11 & 255;
                i5++;
            }
        }
        int i12 = i2 + i4;
        for (int i13 = i12; i13 < this.p + i12; i13++) {
            byte[] bArr2 = this.i;
            if (i13 >= bArr2.length || i13 >= i3) {
                break;
            }
            int i14 = this.f1096a[bArr2[i13] & 255];
            if (i14 != 0) {
                i9 += (i14 >> 24) & 255;
                i8 += (i14 >> 16) & 255;
                i7 += (i14 >> 8) & 255;
                i6 += i14 & 255;
                i5++;
            }
        }
        if (i5 == 0) {
            return 0;
        }
        return ((i9 / i5) << 24) | ((i8 / i5) << 16) | ((i7 / i5) << 8) | (i6 / i5);
    }

    @DexIgnore
    public final void k(cb1 cb1) {
        int i2;
        int i3;
        int i4;
        int i5;
        Boolean bool;
        int i6;
        int[] iArr = this.j;
        int i7 = cb1.d;
        int i8 = this.p;
        int i9 = i7 / i8;
        int i10 = cb1.b / i8;
        int i11 = cb1.c / i8;
        int i12 = cb1.f591a / i8;
        boolean z = this.k == 0;
        int i13 = this.p;
        int i14 = this.r;
        int i15 = this.q;
        byte[] bArr = this.i;
        int[] iArr2 = this.f1096a;
        Boolean bool2 = this.s;
        int i16 = 8;
        int i17 = 0;
        int i18 = 0;
        int i19 = 1;
        while (i17 < i9) {
            if (cb1.e) {
                if (i18 >= i9) {
                    i19++;
                    if (i19 == 2) {
                        i18 = 4;
                    } else if (i19 == 3) {
                        i18 = 2;
                        i16 = 4;
                    } else if (i19 == 4) {
                        i18 = 1;
                        i16 = 2;
                    }
                }
                i2 = i19;
                i3 = i18;
                i4 = i18 + i16;
                i5 = i16;
            } else {
                i2 = i19;
                i3 = i17;
                i4 = i18;
                i5 = i16;
            }
            int i20 = i3 + i10;
            boolean z2 = i13 == 1;
            if (i20 < i15) {
                int i21 = i20 * i14;
                int i22 = i21 + i12;
                int i23 = i22 + i11;
                int i24 = i21 + i14;
                if (i24 >= i23) {
                    i24 = i23;
                }
                int i25 = i17 * i13 * cb1.c;
                if (z2) {
                    bool = bool2;
                    while (i22 < i24) {
                        int i26 = iArr2[bArr[i25] & 255];
                        if (i26 != 0) {
                            iArr[i22] = i26;
                        } else if (z && bool == null) {
                            bool = Boolean.TRUE;
                        }
                        i25 += i13;
                        i22++;
                    }
                    i6 = i10;
                } else {
                    Boolean bool3 = bool2;
                    int i27 = i22;
                    int i28 = i25;
                    while (i27 < i24) {
                        int j2 = j(i28, ((i24 - i22) * i13) + i25, cb1.c);
                        if (j2 != 0) {
                            iArr[i27] = j2;
                        } else if (z && bool3 == null) {
                            bool3 = Boolean.TRUE;
                        }
                        i27++;
                        i28 += i13;
                    }
                    bool2 = bool3;
                    i17++;
                    i18 = i4;
                    i16 = i5;
                    i19 = i2;
                }
            } else {
                bool = bool2;
                i6 = i10;
            }
            bool2 = bool;
            i10 = i6;
            i17++;
            i18 = i4;
            i16 = i5;
            i19 = i2;
        }
        if (this.s == null) {
            this.s = Boolean.valueOf(bool2 == null ? false : bool2.booleanValue());
        }
    }

    @DexIgnore
    public final void l(cb1 cb1) {
        int[] iArr = this.j;
        int i2 = cb1.d;
        int i3 = cb1.b;
        int i4 = cb1.c;
        int i5 = cb1.f591a;
        boolean z = this.k == 0;
        int i6 = this.r;
        byte[] bArr = this.i;
        int[] iArr2 = this.f1096a;
        byte b2 = -1;
        for (int i7 = 0; i7 < i2; i7++) {
            int i8 = (i7 + i3) * i6;
            int i9 = i8 + i5;
            int i10 = i9 + i4;
            int i11 = i8 + i6;
            if (i11 >= i10) {
                i11 = i10;
            }
            int i12 = cb1.c * i7;
            byte b3 = b2;
            while (i9 < i11) {
                byte b4 = bArr[i12];
                int i13 = b4 & 255;
                if (i13 != b3) {
                    int i14 = iArr2[i13];
                    if (i14 != 0) {
                        iArr[i9] = i14;
                    } else {
                        b3 = b4;
                    }
                }
                i9++;
                i12++;
            }
            b2 = b3;
        }
        Boolean bool = this.s;
        this.s = Boolean.valueOf((bool != null && bool.booleanValue()) || (this.s == null && z && b2 != -1));
    }

    @DexIgnore
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:78:0x0111 */
    /* JADX DEBUG: Multi-variable search result rejected for r0v7, resolved type: short[] */
    /* JADX DEBUG: Multi-variable search result rejected for r3v22, resolved type: short */
    /* JADX WARN: Multi-variable type inference failed */
    public final void m(cb1 cb1) {
        int i2;
        int i3;
        short s2;
        int i4;
        int i5;
        int i6;
        if (cb1 != null) {
            this.d.position(cb1.j);
        }
        if (cb1 == null) {
            db1 db1 = this.l;
            i2 = db1.f;
            i3 = db1.g;
        } else {
            i2 = cb1.c;
            i3 = cb1.d;
        }
        int i7 = i2 * i3;
        byte[] bArr = this.i;
        if (bArr == null || bArr.length < i7) {
            this.i = this.c.e(i7);
        }
        byte[] bArr2 = this.i;
        if (this.f == null) {
            this.f = new short[4096];
        }
        short[] sArr = this.f;
        if (this.g == null) {
            this.g = new byte[4096];
        }
        byte[] bArr3 = this.g;
        if (this.h == null) {
            this.h = new byte[4097];
        }
        byte[] bArr4 = this.h;
        int q2 = q();
        int i8 = 1 << q2;
        int i9 = i8 + 2;
        int i10 = q2 + 1;
        int i11 = (1 << i10) - 1;
        int i12 = 0;
        for (int i13 = 0; i13 < i8; i13++) {
            sArr[i13] = (short) 0;
            bArr3[i13] = (byte) ((byte) i13);
        }
        byte[] bArr5 = this.e;
        int i14 = 0;
        int i15 = 0;
        int i16 = 0;
        int i17 = 0;
        int i18 = 0;
        int i19 = -1;
        int i20 = 0;
        int i21 = 0;
        int i22 = i11;
        int i23 = i10;
        int i24 = i9;
        while (true) {
            if (i12 >= i7) {
                break;
            }
            if (i14 == 0) {
                i14 = p();
                if (i14 <= 0) {
                    this.o = 3;
                    break;
                }
                i15 = 0;
            }
            int i25 = i15 + 1;
            int i26 = i14 - 1;
            i17 = ((bArr5[i15] & 255) << i16) + i17;
            i16 += 8;
            while (true) {
                if (i16 < i23) {
                    i15 = i25;
                    i14 = i26;
                    break;
                }
                int i27 = i17 & i22;
                i17 >>= i23;
                i16 -= i23;
                if (i27 == i8) {
                    i24 = i9;
                    i22 = i11;
                    i23 = i10;
                    i19 = -1;
                } else if (i27 == i8 + 1) {
                    i15 = i25;
                    i14 = i26;
                    break;
                } else if (i19 == -1) {
                    bArr2[i18] = (byte) bArr3[i27];
                    i18++;
                    i12++;
                    i20 = i27;
                    i19 = i27;
                } else {
                    if (i27 >= i24) {
                        bArr4[i21] = (byte) ((byte) i20);
                        i4 = i21 + 1;
                        s2 = i19;
                    } else {
                        s2 = i27;
                        i4 = i21;
                    }
                    while (s2 >= i8) {
                        bArr4[i4] = (byte) bArr3[s2];
                        i4++;
                        s2 = sArr[s2];
                    }
                    i20 = bArr3[s2] & 255;
                    byte b2 = (byte) i20;
                    bArr2[i18] = (byte) b2;
                    while (true) {
                        i21 = i4;
                        i18++;
                        i12++;
                        if (i21 <= 0) {
                            break;
                        }
                        i4 = i21 - 1;
                        bArr2[i18] = (byte) bArr4[i4];
                    }
                    if (i24 < 4096) {
                        sArr[i24] = (short) ((short) i19);
                        bArr3[i24] = (byte) b2;
                        i5 = i24 + 1;
                        if ((i5 & i22) != 0 || i5 >= 4096) {
                            i6 = i23;
                        } else {
                            i6 = i23 + 1;
                            i22 += i5;
                        }
                    } else {
                        i5 = i24;
                        i6 = i23;
                    }
                    i24 = i5;
                    i23 = i6;
                    i19 = i27;
                }
            }
        }
        Arrays.fill(bArr2, i18, i7, (byte) 0);
    }

    @DexIgnore
    public int n(int i2) {
        if (i2 >= 0) {
            db1 db1 = this.l;
            if (i2 < db1.c) {
                return db1.e.get(i2).i;
            }
        }
        return -1;
    }

    @DexIgnore
    public final Bitmap o() {
        Boolean bool = this.s;
        Bitmap a2 = this.c.a(this.r, this.q, (bool == null || bool.booleanValue()) ? Bitmap.Config.ARGB_8888 : this.t);
        a2.setHasAlpha(true);
        return a2;
    }

    @DexIgnore
    public final int p() {
        int q2 = q();
        if (q2 > 0) {
            ByteBuffer byteBuffer = this.d;
            byteBuffer.get(this.e, 0, Math.min(q2, byteBuffer.remaining()));
        }
        return q2;
    }

    @DexIgnore
    public final int q() {
        return this.d.get() & 255;
    }

    @DexIgnore
    public void r(db1 db1, ByteBuffer byteBuffer, int i2) {
        synchronized (this) {
            if (i2 > 0) {
                int highestOneBit = Integer.highestOneBit(i2);
                this.o = 0;
                this.l = db1;
                this.k = -1;
                ByteBuffer asReadOnlyBuffer = byteBuffer.asReadOnlyBuffer();
                this.d = asReadOnlyBuffer;
                asReadOnlyBuffer.position(0);
                this.d.order(ByteOrder.LITTLE_ENDIAN);
                this.n = false;
                Iterator<cb1> it = db1.e.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (it.next().g == 3) {
                            this.n = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                this.p = highestOneBit;
                this.r = db1.f / highestOneBit;
                this.q = db1.g / highestOneBit;
                this.i = this.c.e(db1.f * db1.g);
                this.j = this.c.b(this.r * this.q);
            } else {
                throw new IllegalArgumentException("Sample size must be >=0, not: " + i2);
            }
        }
    }

    @DexIgnore
    public final Bitmap s(cb1 cb1, cb1 cb12) {
        int i2;
        int i3;
        Bitmap bitmap;
        int i4;
        int[] iArr = this.j;
        if (cb12 == null) {
            Bitmap bitmap2 = this.m;
            if (bitmap2 != null) {
                this.c.c(bitmap2);
            }
            this.m = null;
            Arrays.fill(iArr, 0);
        }
        if (cb12 != null && cb12.g == 3 && this.m == null) {
            Arrays.fill(iArr, 0);
        }
        if (cb12 != null && (i3 = cb12.g) > 0) {
            if (i3 == 2) {
                if (!cb1.f) {
                    db1 db1 = this.l;
                    i4 = db1.l;
                    if (cb1.k != null && db1.j == cb1.h) {
                        i4 = 0;
                    }
                } else {
                    i4 = 0;
                }
                int i5 = cb12.d;
                int i6 = this.p;
                int i7 = i5 / i6;
                int i8 = cb12.b / i6;
                int i9 = cb12.c / i6;
                int i10 = this.r;
                int i11 = (i8 * i10) + (cb12.f591a / i6);
                int i12 = i11;
                while (i12 < (i7 * i10) + i11) {
                    for (int i13 = i12; i13 < i12 + i9; i13++) {
                        iArr[i13] = i4;
                    }
                    i12 += this.r;
                }
            } else if (i3 == 3 && (bitmap = this.m) != null) {
                int i14 = this.r;
                bitmap.getPixels(iArr, 0, i14, 0, 0, i14, this.q);
            }
        }
        m(cb1);
        if (cb1.e || this.p != 1) {
            k(cb1);
        } else {
            l(cb1);
        }
        if (this.n && ((i2 = cb1.g) == 0 || i2 == 1)) {
            if (this.m == null) {
                this.m = o();
            }
            Bitmap bitmap3 = this.m;
            int i15 = this.r;
            bitmap3.setPixels(iArr, 0, i15, 0, 0, i15, this.q);
        }
        Bitmap o2 = o();
        int i16 = this.r;
        o2.setPixels(iArr, 0, i16, 0, 0, i16, this.q);
        return o2;
    }
}
