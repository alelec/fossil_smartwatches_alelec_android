package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uw5 extends du0<ActivitySummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar c; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public ai5 k;
    @DexIgnore
    public /* final */ PortfolioApp l;
    @DexIgnore
    public /* final */ zw5 m;
    @DexIgnore
    public /* final */ FragmentManager n;
    @DexIgnore
    public /* final */ pv5 o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Date f3652a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;

        @DexIgnore
        public a(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4, String str5) {
            pq7.c(str, "mDayOfWeek");
            pq7.c(str2, "mDayOfMonth");
            pq7.c(str3, "mDailyValue");
            pq7.c(str4, "mDailyUnit");
            pq7.c(str5, "mDailyEst");
            this.f3652a = date;
            this.b = z;
            this.c = z2;
            this.d = str;
            this.e = str2;
            this.f = str3;
            this.g = str4;
            this.h = str5;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4, String str5, int i, kq7 kq7) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? false : z, (i & 4) == 0 ? z2 : false, (i & 8) != 0 ? "" : str, (i & 16) != 0 ? "" : str2, (i & 32) != 0 ? "" : str3, (i & 64) != 0 ? "" : str4, (i & 128) == 0 ? str5 : "");
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final String b() {
            return this.g;
        }

        @DexIgnore
        public final String c() {
            return this.f;
        }

        @DexIgnore
        public final Date d() {
            return this.f3652a;
        }

        @DexIgnore
        public final String e() {
            return this.e;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final boolean g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        public final void i(String str) {
            pq7.c(str, "<set-?>");
            this.h = str;
        }

        @DexIgnore
        public final void j(String str) {
            pq7.c(str, "<set-?>");
            this.g = str;
        }

        @DexIgnore
        public final void k(String str) {
            pq7.c(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final void l(Date date) {
            this.f3652a = date;
        }

        @DexIgnore
        public final void m(String str) {
            pq7.c(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final void n(String str) {
            pq7.c(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void o(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public final void p(boolean z) {
            this.b = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Date f3653a;
        @DexIgnore
        public /* final */ bd5 b;
        @DexIgnore
        public /* final */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ uw5 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date date = this.b.f3653a;
                if (date != null) {
                    this.b.d.m.Q(date);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(uw5 uw5, bd5 bd5, View view) {
            super(view);
            pq7.c(bd5, "binding");
            pq7.c(view, "root");
            this.d = uw5;
            this.b = bd5;
            this.c = view;
            bd5.n().setOnClickListener(new a(this));
            this.b.r.setTextColor(uw5.f);
            this.b.v.setTextColor(uw5.f);
        }

        @DexIgnore
        public void b(ActivitySummary activitySummary) {
            a v = this.d.v(activitySummary);
            this.f3653a = v.d();
            FlexibleTextView flexibleTextView = this.b.u;
            pq7.b(flexibleTextView, "binding.ftvDayOfWeek");
            flexibleTextView.setText(v.f());
            FlexibleTextView flexibleTextView2 = this.b.t;
            pq7.b(flexibleTextView2, "binding.ftvDayOfMonth");
            flexibleTextView2.setText(v.e());
            FlexibleTextView flexibleTextView3 = this.b.s;
            pq7.b(flexibleTextView3, "binding.ftvDailyValue");
            flexibleTextView3.setText(v.c());
            FlexibleTextView flexibleTextView4 = this.b.r;
            pq7.b(flexibleTextView4, "binding.ftvDailyUnit");
            flexibleTextView4.setText(v.b());
            FlexibleTextView flexibleTextView5 = this.b.v;
            pq7.b(flexibleTextView5, "binding.ftvEst");
            flexibleTextView5.setText(v.a());
            if (v.g()) {
                this.b.r.setTextColor(gl0.d(PortfolioApp.h0.c(), 2131099938));
                FlexibleTextView flexibleTextView6 = this.b.r;
                pq7.b(flexibleTextView6, "binding.ftvDailyUnit");
                flexibleTextView6.setAllCaps(true);
            } else {
                this.b.r.setTextColor(gl0.d(PortfolioApp.h0.c(), 2131099940));
                FlexibleTextView flexibleTextView7 = this.b.r;
                pq7.b(flexibleTextView7, "binding.ftvDailyUnit");
                flexibleTextView7.setAllCaps(false);
            }
            ConstraintLayout constraintLayout = this.b.q;
            pq7.b(constraintLayout, "binding.container");
            constraintLayout.setSelected(!v.g());
            FlexibleTextView flexibleTextView8 = this.b.u;
            pq7.b(flexibleTextView8, "binding.ftvDayOfWeek");
            flexibleTextView8.setSelected(v.h());
            FlexibleTextView flexibleTextView9 = this.b.t;
            pq7.b(flexibleTextView9, "binding.ftvDayOfMonth");
            flexibleTextView9.setSelected(v.h());
            if (v.h()) {
                this.b.q.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.g);
                this.b.t.setBackgroundColor(this.d.g);
                this.b.u.setTextColor(this.d.j);
                this.b.t.setTextColor(this.d.j);
            } else if (v.g()) {
                this.b.q.setBackgroundColor(this.d.e);
                this.b.u.setBackgroundColor(this.d.e);
                this.b.t.setBackgroundColor(this.d.e);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            } else {
                this.b.q.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.i);
                this.b.t.setBackgroundColor(this.d.i);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Date f3654a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public c(Date date, Date date2, String str, String str2) {
            pq7.c(str, "mWeekly");
            pq7.c(str2, "mWeeklyValue");
            this.f3654a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(Date date, Date date2, String str, String str2, int i, kq7 kq7) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.f3654a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        public final void e(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void f(Date date) {
            this.f3654a = date;
        }

        @DexIgnore
        public final void g(String str) {
            pq7.c(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void h(String str) {
            pq7.c(str, "<set-?>");
            this.d = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends b {
        @DexIgnore
        public Date e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public /* final */ dd5 g;
        @DexIgnore
        public /* final */ /* synthetic */ uw5 h;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d b;

            @DexIgnore
            public a(d dVar) {
                this.b = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.e != null && this.b.f != null) {
                    zw5 zw5 = this.b.h.m;
                    Date date = this.b.e;
                    if (date != null) {
                        Date date2 = this.b.f;
                        if (date2 != null) {
                            zw5.q0(date, date2);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public d(com.fossil.uw5 r4, com.fossil.dd5 r5) {
            /*
                r3 = this;
                java.lang.String r0 = "binding"
                com.fossil.pq7.c(r5, r0)
                r3.h = r4
                com.fossil.bd5 r0 = r5.r
                if (r0 == 0) goto L_0x0029
                java.lang.String r1 = "binding.dailyItem!!"
                com.fossil.pq7.b(r0, r1)
                android.view.View r1 = r5.n()
                java.lang.String r2 = "binding.root"
                com.fossil.pq7.b(r1, r2)
                r3.<init>(r4, r0, r1)
                r3.g = r5
                androidx.constraintlayout.widget.ConstraintLayout r0 = r5.q
                com.fossil.uw5$d$a r1 = new com.fossil.uw5$d$a
                r1.<init>(r3)
                r0.setOnClickListener(r1)
                return
            L_0x0029:
                com.fossil.pq7.i()
                r0 = 0
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.uw5.d.<init>(com.fossil.uw5, com.fossil.dd5):void");
        }

        @DexIgnore
        @Override // com.fossil.uw5.b
        public void b(ActivitySummary activitySummary) {
            c w = this.h.w(activitySummary);
            this.f = w.a();
            this.e = w.b();
            FlexibleTextView flexibleTextView = this.g.s;
            pq7.b(flexibleTextView, "binding.ftvWeekly");
            flexibleTextView.setText(w.c());
            FlexibleTextView flexibleTextView2 = this.g.t;
            pq7.b(flexibleTextView2, "binding.ftvWeeklyValue");
            flexibleTextView2.setText(w.d());
            super.b(activitySummary);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ uw5 b;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        public e(uw5 uw5, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.b = uw5;
            this.c = viewHolder;
            this.d = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            pq7.c(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardActivitiesAdapter", "onViewAttachedToWindow - mFragment.id=" + this.b.o.getId() + ", isAdded=" + this.b.o.isAdded());
            this.c.itemView.removeOnAttachStateChangeListener(this);
            Fragment Z = this.b.n.Z(this.b.o.D6());
            if (Z == null) {
                FLogger.INSTANCE.getLocal().d("DashboardActivitiesAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                xq0 j = this.b.n.j();
                j.b(view.getId(), this.b.o, this.b.o.D6());
                j.k();
            } else if (this.d) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardActivitiesAdapter", "onViewAttachedToWindow - oldFragment.id=" + Z.getId() + ", isAdded=" + Z.isAdded());
                xq0 j2 = this.b.n.j();
                j2.q(Z);
                j2.k();
                xq0 j3 = this.b.n.j();
                j3.b(view.getId(), this.b.o, this.b.o.D6());
                j3.k();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardActivitiesAdapter", "onViewAttachedToWindow - oldFragment.id=" + Z.getId() + ", isAdded=" + Z.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardActivitiesAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.b.o.getId() + ", isAdded2=" + this.b.o.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            pq7.c(view, "v");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ FrameLayout f3655a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(FrameLayout frameLayout, View view) {
            super(view);
            this.f3655a = frameLayout;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public uw5(sw5 sw5, PortfolioApp portfolioApp, zw5 zw5, FragmentManager fragmentManager, pv5 pv5) {
        super(sw5);
        pq7.c(sw5, "activityDifference");
        pq7.c(portfolioApp, "mApp");
        pq7.c(zw5, "mOnItemClick");
        pq7.c(fragmentManager, "mFragmentManager");
        pq7.c(pv5, "mFragment");
        this.l = portfolioApp;
        this.m = zw5;
        this.n = fragmentManager;
        this.o = pv5;
        String d2 = qn5.l.a().d("primaryText");
        this.d = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = qn5.l.a().d("nonBrandSurface");
        this.e = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = qn5.l.a().d("nonBrandNonReachGoal");
        this.f = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = qn5.l.a().d("dianaStepsTab");
        this.g = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
        String d6 = qn5.l.a().d("secondaryText");
        this.h = Color.parseColor(d6 == null ? "#FFFFFF" : d6);
        String d7 = qn5.l.a().d("nonBrandActivityDetailBackground");
        this.i = Color.parseColor(d7 == null ? "#FFFFFF" : d7);
        String d8 = qn5.l.a().d("onDianaStepsTab");
        this.j = Color.parseColor(d8 == null ? "#FFFFFF" : d8);
        this.k = ai5.METRIC;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i2) {
        if (getItemViewType(i2) != 0) {
            return super.getItemId(i2);
        }
        if (this.o.getId() == 0) {
            return 1010101;
        }
        return (long) this.o.getId();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        ActivitySummary activitySummary = (ActivitySummary) getItem(i2);
        if (activitySummary != null) {
            this.c.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
            Calendar calendar = this.c;
            pq7.b(calendar, "mCalendar");
            Boolean p0 = lk5.p0(calendar.getTime());
            pq7.b(p0, "DateHelper.isToday(mCalendar.time)");
            if (p0.booleanValue() || this.c.get(7) == 7) {
                return 2;
            }
        }
        return 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        boolean z = true;
        pq7.c(viewHolder, "holder");
        int itemViewType = getItemViewType(i2);
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            pq7.b(view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            pq7.b(view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local.d("DashboardActivitiesAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            pq7.b(view3, "holder.itemView");
            view3.setId((int) getItemId(i2));
            viewHolder.itemView.addOnAttachStateChangeListener(new e(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((b) viewHolder).b((ActivitySummary) getItem(i2));
        } else if (itemViewType != 2) {
            ((b) viewHolder).b((ActivitySummary) getItem(i2));
        } else {
            ((d) viewHolder).b((ActivitySummary) getItem(i2));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        pq7.c(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i2 == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new f(frameLayout, frameLayout);
        } else if (i2 == 1) {
            bd5 z = bd5.z(from, viewGroup, false);
            pq7.b(z, "ItemActivityDayBinding.i\u2026tInflater, parent, false)");
            View n2 = z.n();
            pq7.b(n2, "itemActivityDayBinding.root");
            return new b(this, z, n2);
        } else if (i2 != 2) {
            bd5 z2 = bd5.z(from, viewGroup, false);
            pq7.b(z2, "ItemActivityDayBinding.i\u2026tInflater, parent, false)");
            View n3 = z2.n();
            pq7.b(n3, "itemActivityDayBinding.root");
            return new b(this, z2, n3);
        } else {
            dd5 z3 = dd5.z(from, viewGroup, false);
            pq7.b(z3, "ItemActivityWeekBinding.\u2026tInflater, parent, false)");
            return new d(this, z3);
        }
    }

    @DexIgnore
    public final a v(ActivitySummary activitySummary) {
        String sb;
        boolean z = false;
        a aVar = new a(null, false, false, null, null, null, null, null, 255, null);
        if (activitySummary != null) {
            Calendar instance = Calendar.getInstance();
            instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
            int i2 = instance.get(7);
            pq7.b(instance, "calendar");
            Boolean p0 = lk5.p0(instance.getTime());
            pq7.b(p0, "DateHelper.isToday(calendar.time)");
            if (p0.booleanValue()) {
                String c2 = um5.c(this.l, 2131886686);
                pq7.b(c2, "LanguageHelper.getString\u2026n_StepsToday_Text__Today)");
                aVar.n(c2);
            } else {
                aVar.n(jl5.b.i(i2));
            }
            aVar.l(instance.getTime());
            aVar.m(String.valueOf(instance.get(5)));
            if (activitySummary.getSteps() > ((double) 0)) {
                double steps = activitySummary.getSteps();
                aVar.k(ml5.f2399a.d(Integer.valueOf((int) steps)));
                String c3 = um5.c(this.l, 2131886689);
                pq7.b(c3, "LanguageHelper.getString\u2026_StepsToday_Title__Steps)");
                if (c3 != null) {
                    String lowerCase = c3.toLowerCase();
                    pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    aVar.j(lowerCase);
                    if (this.k == ai5.IMPERIAL) {
                        StringBuilder sb2 = new StringBuilder();
                        hr7 hr7 = hr7.f1520a;
                        String c4 = um5.c(this.l, 2131886679);
                        pq7.b(c4, "LanguageHelper.getString\u2026oday_Text__EstNumberUnit)");
                        String format = String.format(c4, Arrays.copyOf(new Object[]{ml5.f2399a.c(Float.valueOf((float) activitySummary.getDistance()), this.k)}, 1));
                        pq7.b(format, "java.lang.String.format(format, *args)");
                        sb2.append(format);
                        sb2.append(" ");
                        sb2.append(PortfolioApp.h0.c().getString(2131886851));
                        sb = sb2.toString();
                    } else {
                        StringBuilder sb3 = new StringBuilder();
                        hr7 hr72 = hr7.f1520a;
                        String c5 = um5.c(this.l, 2131886679);
                        pq7.b(c5, "LanguageHelper.getString\u2026oday_Text__EstNumberUnit)");
                        String format2 = String.format(c5, Arrays.copyOf(new Object[]{ml5.f2399a.c(Float.valueOf((float) activitySummary.getDistance()), this.k)}, 1));
                        pq7.b(format2, "java.lang.String.format(format, *args)");
                        sb3.append(format2);
                        sb3.append(" ");
                        sb3.append(PortfolioApp.h0.c().getString(2131886850));
                        sb = sb3.toString();
                    }
                    aVar.i(sb);
                    if (activitySummary.getStepGoal() > 0) {
                        if (steps >= ((double) activitySummary.getStepGoal())) {
                            z = true;
                        }
                        aVar.p(z);
                    } else {
                        aVar.p(false);
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                String c6 = um5.c(this.l, 2131886717);
                pq7.b(c6, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
                aVar.j(c6);
                aVar.o(true);
            }
        }
        return aVar;
    }

    @DexIgnore
    public final c w(ActivitySummary activitySummary) {
        String str;
        Integer num = null;
        c cVar = new c(null, null, null, null, 15, null);
        if (activitySummary != null) {
            Calendar instance = Calendar.getInstance();
            instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
            pq7.b(instance, "calendar");
            Boolean p0 = lk5.p0(instance.getTime());
            int i2 = instance.get(5);
            int i3 = instance.get(2);
            String N = lk5.N(i3);
            int i4 = instance.get(1);
            cVar.e(instance.getTime());
            instance.add(5, -6);
            int i5 = instance.get(5);
            int i6 = instance.get(2);
            String N2 = lk5.N(i6);
            int i7 = instance.get(1);
            cVar.f(instance.getTime());
            pq7.b(p0, "isToday");
            if (p0.booleanValue()) {
                str = um5.c(this.l, 2131886690);
                pq7.b(str, "LanguageHelper.getString\u2026epsToday_Title__ThisWeek)");
            } else if (i3 == i6) {
                str = N2 + ' ' + i5 + " - " + N2 + ' ' + i2;
            } else if (i7 == i4) {
                str = N2 + ' ' + i5 + " - " + N + ' ' + i2;
            } else {
                str = N2 + ' ' + i5 + ", " + i7 + " - " + N + ' ' + i2 + ", " + i4;
            }
            cVar.g(str);
            hr7 hr7 = hr7.f1520a;
            String c2 = um5.c(this.l, 2131886682);
            pq7.b(c2, "LanguageHelper.getString\u2026sToday_Text__NumberSteps)");
            ml5 ml5 = ml5.f2399a;
            ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = activitySummary.getTotalValuesOfWeek();
            if (totalValuesOfWeek != null) {
                num = Integer.valueOf((int) totalValuesOfWeek.getTotalStepsOfWeek());
            }
            String format = String.format(c2, Arrays.copyOf(new Object[]{ml5.d(num)}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            cVar.h(format);
        }
        return cVar;
    }

    @DexIgnore
    public final void x(cu0<ActivitySummary> cu0) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList - size=");
        sb.append(cu0 != null ? Integer.valueOf(cu0.size()) : null);
        sb.append(" on thread ");
        Thread currentThread = Thread.currentThread();
        pq7.b(currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        local.d("DashboardActivitiesAdapter", sb.toString());
        super.i(cu0);
    }

    @DexIgnore
    public final void y(ai5 ai5, int i2, int i3) {
        pq7.c(ai5, "distanceUnit");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActivitiesAdapter", "updateUserUnits - old=" + this.k + ", new=" + ai5);
        this.k = ai5;
        if (getItemCount() > 0 && i3 >= 0 && i2 >= 0) {
            int max = Math.max(0, i2 - 5);
            notifyItemRangeChanged(max, Math.min(getItemCount(), i3 + 5) - max);
        }
    }
}
