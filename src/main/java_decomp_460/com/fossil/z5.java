package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z5 extends u5 {
    @DexIgnore
    public /* final */ n5 k; // = n5.HIGH;
    @DexIgnore
    public b5 l; // = b5.DISCONNECTED;

    @DexIgnore
    public z5(n4 n4Var) {
        super(v5.m, n4Var);
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void d(k5 k5Var) {
        k5Var.a();
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void f(h7 h7Var) {
        s5 a2;
        k(h7Var);
        g7 g7Var = h7Var.f1435a;
        if (g7Var.b == f7.SUCCESS) {
            a2 = this.l == b5.CONNECTED ? s5.a(this.e, null, r5.b, null, 5) : s5.a(this.e, null, r5.e, null, 5);
        } else {
            s5 a3 = s5.e.a(g7Var);
            a2 = s5.a(this.e, null, a3.c, a3.d, 1);
        }
        this.e = a2;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public n5 h() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public boolean i(h7 h7Var) {
        b5 b5Var;
        return (h7Var instanceof y6) && ((b5Var = ((y6) h7Var).b) == b5.CONNECTED || b5Var == b5.DISCONNECTED);
    }

    @DexIgnore
    @Override // com.fossil.u5
    public fd0<h7> j() {
        return this.j.k;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void k(h7 h7Var) {
        this.l = ((y6) h7Var).b;
    }
}
