package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import com.fossil.tk1;
import com.fossil.yk1;
import java.util.concurrent.CopyOnWriteArraySet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zw {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ CopyOnWriteArraySet<e60> f4548a; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<e60> b; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<e60> c; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<e60> d; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ Handler e;
    @DexIgnore
    public static /* final */ BroadcastReceiver f; // = new m1();
    @DexIgnore
    public static /* final */ BroadcastReceiver g; // = new be();
    @DexIgnore
    public static /* final */ BroadcastReceiver h; // = new hd();
    @DexIgnore
    public static /* final */ zw i; // = new zw();

    /*
    static {
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        if (myLooper != null) {
            e = new Handler(myLooper);
            BroadcastReceiver broadcastReceiver = f;
            Context a2 = id0.i.a();
            if (a2 != null) {
                IntentFilter intentFilter = new IntentFilter();
                for (int i2 = 0; i2 < 1; i2++) {
                    intentFilter.addAction(new String[]{"com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED"}[i2]);
                }
                ct0.b(a2).c(broadcastReceiver, intentFilter);
            }
            BroadcastReceiver broadcastReceiver2 = g;
            Context a3 = id0.i.a();
            if (a3 != null) {
                IntentFilter intentFilter2 = new IntentFilter();
                for (int i3 = 0; i3 < 1; i3++) {
                    intentFilter2.addAction(new String[]{"com.fossil.blesdk.device.DeviceImplementation.action.STATE_CHANGED"}[i3]);
                }
                ct0.b(a3).c(broadcastReceiver2, intentFilter2);
            }
            BroadcastReceiver broadcastReceiver3 = h;
            Context a4 = id0.i.a();
            if (a4 != null) {
                IntentFilter intentFilter3 = new IntentFilter();
                for (int i4 = 0; i4 < 1; i4++) {
                    intentFilter3.addAction(new String[]{"com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED"}[i4]);
                }
                ct0.b(a4).c(broadcastReceiver3, intentFilter3);
                return;
            }
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    public static /* synthetic */ void a(zw zwVar, e60 e60, long j, int i2) {
        if ((i2 & 2) != 0) {
            j = 0;
        }
        zwVar.c(e60, j);
    }

    @DexIgnore
    public final void b(e60 e60) {
        if (f4548a.contains(e60) && e60.v == yk1.c.DISCONNECTED) {
            e.postDelayed(new h3(e60), 200);
        }
    }

    @DexIgnore
    public final void c(e60 e60, long j) {
        e.postDelayed(new hc(e60), j);
    }

    @DexIgnore
    public final void d(e60 e60, yk1.d dVar) {
        int i2 = t0.b[dVar.ordinal()];
        if (i2 == 2) {
            c(e60, 0);
        } else if (i2 == 3) {
            qd0.b.b("HID_EXPONENT_BACK_OFF_TAG");
            c(e60, 0);
        }
    }

    @DexIgnore
    public final void e(tk1.c cVar) {
        if (t0.f3342a[cVar.ordinal()] == 1) {
            for (T t : f4548a) {
                zw zwVar = i;
                pq7.b(t, "it");
                zwVar.b(t);
            }
            for (T t2 : b) {
                zw zwVar2 = i;
                pq7.b(t2, "it");
                a(zwVar2, t2, 0, 2);
            }
        }
    }

    @DexIgnore
    public final boolean f(e60 e60) {
        return f4548a.contains(e60);
    }

    @DexIgnore
    public final boolean g(e60 e60) {
        return b.contains(e60);
    }

    @DexIgnore
    public final void h(e60 e60) {
        b.remove(e60);
    }

    @DexIgnore
    public final boolean i(e60 e60) {
        f4548a.add(e60);
        b(e60);
        return true;
    }

    @DexIgnore
    public final boolean j(e60 e60) {
        f4548a.remove(e60);
        return true;
    }
}
