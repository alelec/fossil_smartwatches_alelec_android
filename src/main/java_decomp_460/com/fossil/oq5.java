package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oq5 {
    @DexIgnore
    public static /* final */ String b; // = "com.fossil.oq5";

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Weather f2709a;

    @DexIgnore
    public Weather a() {
        return this.f2709a;
    }

    @DexIgnore
    public void b(gj4 gj4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "Inside " + b + ".parse - json=" + gj4);
        try {
            zi4 zi4 = new zi4();
            zi4.f(DateTime.class, new GsonConvertDateTime());
            zi4.f(Date.class, new GsonConverterShortDate());
            this.f2709a = (Weather) zi4.d().k(gj4.toString(), Weather.class);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = b;
            local2.d(str2, "parse mWeather=" + this.f2709a);
        } catch (Exception e) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = b;
            local3.e(str3, "parse error=" + e.toString());
        }
    }
}
