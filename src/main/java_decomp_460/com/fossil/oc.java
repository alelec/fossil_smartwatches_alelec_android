package com.fossil;

import com.fossil.fitness.WorkoutSessionManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oc extends qq7 implements rp7<tl7, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ zq7 b;
    @DexIgnore
    public /* final */ /* synthetic */ mp1 c;
    @DexIgnore
    public /* final */ /* synthetic */ zq7 d;
    @DexIgnore
    public /* final */ /* synthetic */ e60 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public oc(zq7 zq7, mp1 mp1, zq7 zq72, e60 e60) {
        super(1);
        this.b = zq7;
        this.c = mp1;
        this.d = zq72;
        this.e = e60;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(tl7 tl7) {
        if (this.b.element) {
            this.e.s0(this.c);
        }
        if (this.d.element) {
            WorkoutSessionManager workoutSessionManager = this.e.t;
            Boolean valueOf = workoutSessionManager != null ? Boolean.valueOf(workoutSessionManager.clearAccumulateDistance()) : null;
            m80 m80 = m80.c;
            m80.a("DeviceEventManager", "clearAccumulateDistance result = " + valueOf + '.', new Object[0]);
        }
        return tl7.f3441a;
    }
}
