package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qo1 extends yo1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<qo1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public qo1 a(Parcel parcel) {
            return new qo1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public qo1 createFromParcel(Parcel parcel) {
            return new qo1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public qo1[] newArray(int i) {
            return new qo1[i];
        }
    }

    @DexIgnore
    public qo1() {
        super(ap1.BUDDY_CHALLENGE, null, null, 6);
    }

    @DexIgnore
    public /* synthetic */ qo1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    public qo1(vw1 vw1) {
        super(ap1.BUDDY_CHALLENGE, vw1, null, 4);
    }
}
