package com.fossil;

import com.fossil.wearables.fsl.BaseProvider;
import com.portfolio.platform.data.model.PhoneFavoritesContact;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface np5 extends BaseProvider {
    @DexIgnore
    void g(PhoneFavoritesContact phoneFavoritesContact);

    @DexIgnore
    Object i();  // void declaration

    @DexIgnore
    void removePhoneFavoritesContact(String str);
}
