package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xn7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends io7 {
        @DexIgnore
        public /* final */ /* synthetic */ qn7 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ rp7 $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(qn7 qn7, qn7 qn72, rp7 rp7) {
            super(qn72);
            this.$completion = qn7;
            this.$this_createCoroutineUnintercepted$inlined = rp7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                el7.b(obj);
                rp7 rp7 = this.$this_createCoroutineUnintercepted$inlined;
                if (rp7 != null) {
                    ir7.d(rp7, 1);
                    return rp7.invoke(this);
                }
                throw new il7("null cannot be cast to non-null type (kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends co7 {
        @DexIgnore
        public /* final */ /* synthetic */ qn7 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ tn7 $context;
        @DexIgnore
        public /* final */ /* synthetic */ rp7 $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(qn7 qn7, tn7 tn7, qn7 qn72, tn7 tn72, rp7 rp7) {
            super(qn72, tn72);
            this.$completion = qn7;
            this.$context = tn7;
            this.$this_createCoroutineUnintercepted$inlined = rp7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                el7.b(obj);
                rp7 rp7 = this.$this_createCoroutineUnintercepted$inlined;
                if (rp7 != null) {
                    ir7.d(rp7, 1);
                    return rp7.invoke(this);
                }
                throw new il7("null cannot be cast to non-null type (kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends io7 {
        @DexIgnore
        public /* final */ /* synthetic */ qn7 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ Object $receiver$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ vp7 $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(qn7 qn7, qn7 qn72, vp7 vp7, Object obj) {
            super(qn72);
            this.$completion = qn7;
            this.$this_createCoroutineUnintercepted$inlined = vp7;
            this.$receiver$inlined = obj;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                el7.b(obj);
                vp7 vp7 = this.$this_createCoroutineUnintercepted$inlined;
                if (vp7 != null) {
                    ir7.d(vp7, 2);
                    return vp7.invoke(this.$receiver$inlined, this);
                }
                throw new il7("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends co7 {
        @DexIgnore
        public /* final */ /* synthetic */ qn7 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ tn7 $context;
        @DexIgnore
        public /* final */ /* synthetic */ Object $receiver$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ vp7 $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(qn7 qn7, tn7 tn7, qn7 qn72, tn7 tn72, vp7 vp7, Object obj) {
            super(qn72, tn72);
            this.$completion = qn7;
            this.$context = tn7;
            this.$this_createCoroutineUnintercepted$inlined = vp7;
            this.$receiver$inlined = obj;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                el7.b(obj);
                vp7 vp7 = this.$this_createCoroutineUnintercepted$inlined;
                if (vp7 != null) {
                    ir7.d(vp7, 2);
                    return vp7.invoke(this.$receiver$inlined, this);
                }
                throw new il7("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore
    public static final <T> qn7<tl7> a(rp7<? super qn7<? super T>, ? extends Object> rp7, qn7<? super T> qn7) {
        pq7.c(rp7, "$this$createCoroutineUnintercepted");
        pq7.c(qn7, "completion");
        go7.a(qn7);
        if (rp7 instanceof zn7) {
            return ((zn7) rp7).create(qn7);
        }
        tn7 context = qn7.getContext();
        if (context == un7.INSTANCE) {
            if (qn7 != null) {
                return new a(qn7, qn7, rp7);
            }
            throw new il7("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        } else if (qn7 != null) {
            return new b(qn7, context, qn7, context, rp7);
        } else {
            throw new il7("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        }
    }

    @DexIgnore
    public static final <R, T> qn7<tl7> b(vp7<? super R, ? super qn7<? super T>, ? extends Object> vp7, R r, qn7<? super T> qn7) {
        pq7.c(vp7, "$this$createCoroutineUnintercepted");
        pq7.c(qn7, "completion");
        go7.a(qn7);
        if (vp7 instanceof zn7) {
            return ((zn7) vp7).create(r, qn7);
        }
        tn7 context = qn7.getContext();
        if (context == un7.INSTANCE) {
            if (qn7 != null) {
                return new c(qn7, qn7, vp7, r);
            }
            throw new il7("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        } else if (qn7 != null) {
            return new d(qn7, context, qn7, context, vp7, r);
        } else {
            throw new il7("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.qn7<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> qn7<T> c(qn7<? super T> qn7) {
        qn7<T> qn72;
        pq7.c(qn7, "$this$intercepted");
        co7 co7 = !(qn7 instanceof co7) ? null : qn7;
        return (co7 == null || (qn72 = (qn7<T>) co7.intercepted()) == null) ? qn7 : qn72;
    }
}
