package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bg5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView q;

    @DexIgnore
    public bg5(Object obj, View view, int i, ImageView imageView) {
        super(obj, view, i);
        this.q = imageView;
    }

    @DexIgnore
    @Deprecated
    public static bg5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (bg5) ViewDataBinding.p(layoutInflater, 2131558722, viewGroup, z, obj);
    }

    @DexIgnore
    public static bg5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, aq0.d());
    }
}
