package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wt3<TResult> implements hu3<TResult> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f3994a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public gt3 c;

    @DexIgnore
    public wt3(Executor executor, gt3 gt3) {
        this.f3994a = executor;
        this.c = gt3;
    }

    @DexIgnore
    @Override // com.fossil.hu3
    public final void a(nt3<TResult> nt3) {
        if (nt3.o()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.f3994a.execute(new yt3(this));
                }
            }
        }
    }
}
