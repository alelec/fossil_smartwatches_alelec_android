package com.fossil;

import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qt2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ClassLoader f3020a; // = qt2.class.getClassLoader();

    @DexIgnore
    public static <T extends Parcelable> T a(Parcel parcel, Parcelable.Creator<T> creator) {
        if (parcel.readInt() == 0) {
            return null;
        }
        return creator.createFromParcel(parcel);
    }

    @DexIgnore
    public static void b(Parcel parcel, IInterface iInterface) {
        if (iInterface == null) {
            parcel.writeStrongBinder(null);
        } else {
            parcel.writeStrongBinder(iInterface.asBinder());
        }
    }

    @DexIgnore
    public static void c(Parcel parcel, Parcelable parcelable) {
        if (parcelable == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        parcelable.writeToParcel(parcel, 0);
    }

    @DexIgnore
    public static void d(Parcel parcel, boolean z) {
        parcel.writeInt(z ? 1 : 0);
    }

    @DexIgnore
    public static boolean e(Parcel parcel) {
        return parcel.readInt() != 0;
    }

    @DexIgnore
    public static HashMap f(Parcel parcel) {
        return parcel.readHashMap(f3020a);
    }
}
