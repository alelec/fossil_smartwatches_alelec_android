package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xa8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static boolean f4085a;

    @DexIgnore
    public static void a(Object obj, Throwable th) {
        if (f4085a) {
            Log.e("PhotoManagerPlugin", obj == null ? "null" : obj.toString(), th);
        }
    }

    @DexIgnore
    public static void b(Object obj) {
        if (f4085a) {
            Log.i("PhotoManagerPlugin", obj == null ? "null" : obj.toString());
        }
    }
}
