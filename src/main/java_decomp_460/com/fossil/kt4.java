package com.fossil;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kt4 implements jt4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f2076a;
    @DexIgnore
    public /* final */ jw0<it4> b;
    @DexIgnore
    public /* final */ xw0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jw0<it4> {
        @DexIgnore
        public a(kt4 kt4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, it4 it4) {
            if (it4.c() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, it4.c());
            }
            if (it4.g() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, it4.g());
            }
            if (it4.b() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, it4.b());
            }
            if (it4.d() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, it4.d());
            }
            if (it4.e() == null) {
                px0.bindNull(5);
            } else {
                px0.bindLong(5, (long) it4.e().intValue());
            }
            if (it4.f() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, it4.f());
            }
            if (it4.a() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, it4.a());
            }
            if (it4.h() == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, it4.h());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `profile` (`id`,`socialId`,`firstName`,`lastName`,`points`,`profilePicture`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends xw0 {
        @DexIgnore
        public b(kt4 kt4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "UPDATE profile SET socialId = ? WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends xw0 {
        @DexIgnore
        public c(kt4 kt4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM profile";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Callable<it4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tw0 f2077a;

        @DexIgnore
        public d(tw0 tw0) {
            this.f2077a = tw0;
        }

        @DexIgnore
        /* renamed from: a */
        public it4 call() throws Exception {
            it4 it4;
            Integer num = null;
            Cursor b2 = ex0.b(kt4.this.f2076a, this.f2077a, false, null);
            try {
                int c = dx0.c(b2, "id");
                int c2 = dx0.c(b2, "socialId");
                int c3 = dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
                int c4 = dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
                int c5 = dx0.c(b2, "points");
                int c6 = dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
                int c7 = dx0.c(b2, "createdAt");
                int c8 = dx0.c(b2, "updatedAt");
                if (b2.moveToFirst()) {
                    String string = b2.getString(c);
                    String string2 = b2.getString(c2);
                    String string3 = b2.getString(c3);
                    String string4 = b2.getString(c4);
                    if (!b2.isNull(c5)) {
                        num = Integer.valueOf(b2.getInt(c5));
                    }
                    it4 = new it4(string, string2, string3, string4, num, b2.getString(c6), b2.getString(c7), b2.getString(c8));
                } else {
                    it4 = null;
                }
                return it4;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.f2077a.m();
        }
    }

    @DexIgnore
    public kt4(qw0 qw0) {
        this.f2076a = qw0;
        this.b = new a(this, qw0);
        new b(this, qw0);
        this.c = new c(this, qw0);
    }

    @DexIgnore
    @Override // com.fossil.jt4
    public void a() {
        this.f2076a.assertNotSuspendingTransaction();
        px0 acquire = this.c.acquire();
        this.f2076a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f2076a.setTransactionSuccessful();
        } finally {
            this.f2076a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.jt4
    public LiveData<it4> b() {
        tw0 f = tw0.f("SELECT*FROM profile  LIMIT 1", 0);
        nw0 invalidationTracker = this.f2076a.getInvalidationTracker();
        d dVar = new d(f);
        return invalidationTracker.d(new String[]{"profile"}, false, dVar);
    }

    @DexIgnore
    @Override // com.fossil.jt4
    public long c(it4 it4) {
        this.f2076a.assertNotSuspendingTransaction();
        this.f2076a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(it4);
            this.f2076a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.f2076a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.jt4
    public it4 d() {
        it4 it4;
        Integer num = null;
        tw0 f = tw0.f("SELECT*FROM profile LIMIT 1", 0);
        this.f2076a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f2076a, f, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, "socialId");
            int c4 = dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = dx0.c(b2, "points");
            int c7 = dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = dx0.c(b2, "createdAt");
            int c9 = dx0.c(b2, "updatedAt");
            if (b2.moveToFirst()) {
                String string = b2.getString(c2);
                String string2 = b2.getString(c3);
                String string3 = b2.getString(c4);
                String string4 = b2.getString(c5);
                if (!b2.isNull(c6)) {
                    num = Integer.valueOf(b2.getInt(c6));
                }
                it4 = new it4(string, string2, string3, string4, num, b2.getString(c7), b2.getString(c8), b2.getString(c9));
            } else {
                it4 = null;
            }
            return it4;
        } finally {
            b2.close();
            f.m();
        }
    }
}
