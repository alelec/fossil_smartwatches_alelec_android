package com.fossil;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.tq4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.y56;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v56 extends o56 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public /* final */ List<j06> e; // = new ArrayList();
    @DexIgnore
    public /* final */ p56 f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ ArrayList<j06> h;
    @DexIgnore
    public /* final */ LoaderManager i;
    @DexIgnore
    public /* final */ uq4 j;
    @DexIgnore
    public /* final */ y56 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return v56.l;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1", f = "NotificationHybridContactPresenter.kt", l = {47}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ v56 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$1", f = "NotificationHybridContactPresenter.kt", l = {48}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    PortfolioApp c = PortfolioApp.h0.c();
                    this.L$0 = iv7;
                    this.label = 1;
                    if (c.V0(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v56$b$b")
        /* renamed from: com.fossil.v56$b$b  reason: collision with other inner class name */
        public static final class C0257b implements tq4.d<y56.c, y56.a> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ b f3719a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v56$b$b$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1", f = "NotificationHybridContactPresenter.kt", l = {99}, m = "invokeSuspend")
            /* renamed from: com.fossil.v56$b$b$a */
            public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ y56.c $successResponse;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0257b this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v56$b$b$a$a")
                @eo7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1$populateContacts$1", f = "NotificationHybridContactPresenter.kt", l = {}, m = "invokeSuspend")
                /* renamed from: com.fossil.v56$b$b$a$a  reason: collision with other inner class name */
                public static final class C0258a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0258a(a aVar, qn7 qn7) {
                        super(2, qn7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        C0258a aVar = new C0258a(this.this$0, qn7);
                        aVar.p$ = (iv7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                        return ((C0258a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final Object invokeSuspend(Object obj) {
                        yn7.d();
                        if (this.label == 0) {
                            el7.b(obj);
                            for (T t : this.this$0.$successResponse.a()) {
                                for (Contact contact : t.getContacts()) {
                                    j06 j06 = new j06(contact, null, 2, null);
                                    j06.setAdded(true);
                                    Contact contact2 = j06.getContact();
                                    if (contact2 != null) {
                                        pq7.b(contact, "contact");
                                        contact2.setDbRowId(contact.getDbRowId());
                                        contact2.setUseSms(contact.isUseSms());
                                        contact2.setUseCall(contact.isUseCall());
                                    }
                                    j06.setCurrentHandGroup(t.getHour());
                                    pq7.b(contact, "contact");
                                    List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                                    pq7.b(phoneNumbers, "contact.phoneNumbers");
                                    if (!phoneNumbers.isEmpty()) {
                                        PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                                        pq7.b(phoneNumber, "contact.phoneNumbers[0]");
                                        String number = phoneNumber.getNumber();
                                        if (!TextUtils.isEmpty(number)) {
                                            j06.setHasPhoneNumber(true);
                                            j06.setPhoneNumber(number);
                                            FLogger.INSTANCE.getLocal().d(v56.m.a(), " filter selected contact, phoneNumber=" + number);
                                        }
                                    }
                                    Iterator it = this.this$0.this$0.f3719a.this$0.h.iterator();
                                    int i = 0;
                                    while (true) {
                                        if (!it.hasNext()) {
                                            i = -1;
                                            break;
                                        }
                                        Contact contact3 = ((j06) it.next()).getContact();
                                        if (ao7.a(contact3 != null && contact3.getContactId() == contact.getContactId()).booleanValue()) {
                                            break;
                                        }
                                        i++;
                                    }
                                    if (i != -1) {
                                        j06.setCurrentHandGroup(this.this$0.this$0.f3719a.this$0.g);
                                        pq7.b(this.this$0.this$0.f3719a.this$0.h.remove(i), "mContactWrappersSelected\u2026moveAt(indexContactFound)");
                                    } else if (j06.getCurrentHandGroup() == this.this$0.this$0.f3719a.this$0.g) {
                                    }
                                    FLogger.INSTANCE.getLocal().d(v56.m.a(), ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                                    this.this$0.this$0.f3719a.this$0.z().add(j06);
                                }
                            }
                            return tl7.f3441a;
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(C0257b bVar, y56.c cVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = bVar;
                    this.$successResponse = cVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    a aVar = new a(this.this$0, this.$successResponse, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        rv7 rv7 = gu7.b(iv7, this.this$0.f3719a.this$0.h(), null, new C0258a(this, null), 2, null);
                        this.L$0 = iv7;
                        this.L$1 = rv7;
                        this.label = 1;
                        if (rv7.l(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        rv7 rv72 = (rv7) this.L$1;
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (!this.this$0.f3719a.this$0.h.isEmpty()) {
                        for (j06 j06 : this.this$0.f3719a.this$0.h) {
                            this.this$0.f3719a.this$0.z().add(j06);
                        }
                    }
                    this.this$0.f3719a.this$0.f.n1(this.this$0.f3719a.this$0.z(), w37.f3877a.a(), this.this$0.f3719a.this$0.g);
                    this.this$0.f3719a.this$0.i.d(0, new Bundle(), this.this$0.f3719a.this$0);
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public C0257b(b bVar) {
                this.f3719a = bVar;
            }

            @DexIgnore
            /* renamed from: b */
            public void a(y56.a aVar) {
                pq7.c(aVar, "errorResponse");
                FLogger.INSTANCE.getLocal().d(v56.m.a(), "GetAllContactGroup onError");
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(y56.c cVar) {
                pq7.c(cVar, "successResponse");
                FLogger.INSTANCE.getLocal().d(v56.m.a(), "GetAllContactGroup onSuccess");
                xw7 unused = gu7.d(this.f3719a.this$0.k(), null, null, new a(this, cVar, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(v56 v56, qn7 qn7) {
            super(2, qn7);
            this.this$0 = v56;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (!PortfolioApp.h0.c().k0().s0()) {
                    dv7 h = this.this$0.h();
                    a aVar = new a(null);
                    this.L$0 = iv7;
                    this.label = 1;
                    if (eu7.g(h, aVar, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.z().isEmpty()) {
                this.this$0.j.a(this.this$0.k, null, new C0257b(this));
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = v56.class.getSimpleName();
        pq7.b(simpleName, "NotificationHybridContac\u2026er::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public v56(p56 p56, int i2, ArrayList<j06> arrayList, LoaderManager loaderManager, uq4 uq4, y56 y56) {
        pq7.c(p56, "mView");
        pq7.c(arrayList, "mContactWrappersSelected");
        pq7.c(loaderManager, "mLoaderManager");
        pq7.c(uq4, "mUseCaseHandler");
        pq7.c(y56, "mGetAllHybridContactGroups");
        this.f = p56;
        this.g = i2;
        this.h = arrayList;
        this.i = loaderManager;
        this.j = uq4;
        this.k = y56;
    }

    @DexIgnore
    /* renamed from: A */
    public void a(at0<Cursor> at0, Cursor cursor) {
        pq7.c(at0, "loader");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onLoadFinished cursor=" + cursor);
        this.f.T(cursor);
    }

    @DexIgnore
    public void B() {
        this.f.M5(this);
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public at0<Cursor> d(int i2, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onCreateLoader, selection = has_phone_number!=0 AND mimetype=?");
        return new zs0(PortfolioApp.h0.c(), ContactsContract.Data.CONTENT_URI, new String[]{"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"}, "has_phone_number!=0 AND mimetype=?", new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public void g(at0<Cursor> at0) {
        pq7.c(at0, "loader");
        FLogger.INSTANCE.getLocal().d(l, ".Inside onLoaderReset");
        this.f.V();
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(l, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        xw7 unused = gu7.d(k(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(l, "stop");
    }

    @DexIgnore
    @Override // com.fossil.o56
    public int n() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.o56
    public void o() {
        ArrayList<j06> arrayList = new ArrayList<>();
        for (T t : this.e) {
            if (t.isAdded() && t.getCurrentHandGroup() == this.g) {
                arrayList.add(t);
            }
        }
        this.f.I(arrayList);
    }

    @DexIgnore
    @Override // com.fossil.o56
    public void p(j06 j06) {
        T t;
        pq7.c(j06, "contactWrapper");
        FLogger.INSTANCE.getLocal().d(l, "reassignContact: contactWrapper=" + j06);
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            Contact contact = next.getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Contact contact2 = j06.getContact();
            if (pq7.a(valueOf, contact2 != null ? Integer.valueOf(contact2.getContactId()) : null)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            t2.setAdded(true);
            t2.setCurrentHandGroup(this.g);
            this.f.A3();
        }
    }

    @DexIgnore
    public final List<j06> z() {
        return this.e;
    }
}
