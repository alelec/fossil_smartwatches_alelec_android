package com.fossil;

import android.os.RemoteException;
import com.fossil.qb3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tf3 extends mc3 {
    @DexIgnore
    public /* final */ /* synthetic */ qb3.h b;

    @DexIgnore
    public tf3(qb3 qb3, qb3.h hVar) {
        this.b = hVar;
    }

    @DexIgnore
    @Override // com.fossil.lc3
    public final void onMapLoaded() throws RemoteException {
        this.b.onMapLoaded();
    }
}
