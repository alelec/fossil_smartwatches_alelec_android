package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum vv1 {
    WEATHER("weatherSSE"),
    HEART_RATE("hrSSE"),
    STEPS("stepsSSE"),
    DATE("dateSSE"),
    CHANCE_OF_RAIN("chanceOfRainSSE"),
    SECOND_TIMEZONE("timeZone2SSE"),
    ACTIVE_MINUTES("activeMinutesSSE"),
    CALORIES("caloriesSSE"),
    BATTERY("batterySSE");
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final vv1 a(String str) {
            vv1[] values = vv1.values();
            for (vv1 vv1 : values) {
                if (pq7.a(vv1.a(), str)) {
                    return vv1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public vv1(String str) {
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }
}
