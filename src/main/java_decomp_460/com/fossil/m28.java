package com.fossil;

import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m28 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set<x18> f2292a; // = new LinkedHashSet();

    @DexIgnore
    public void a(x18 x18) {
        synchronized (this) {
            this.f2292a.remove(x18);
        }
    }

    @DexIgnore
    public void b(x18 x18) {
        synchronized (this) {
            this.f2292a.add(x18);
        }
    }

    @DexIgnore
    public boolean c(x18 x18) {
        boolean contains;
        synchronized (this) {
            contains = this.f2292a.contains(x18);
        }
        return contains;
    }
}
