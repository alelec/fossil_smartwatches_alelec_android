package com.fossil;

import com.facebook.appevents.codeless.CodelessMatcher;
import com.facebook.internal.Utility;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q18 {
    @DexIgnore
    public static /* final */ char[] j; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2911a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ List<String> f;
    @DexIgnore
    public /* final */ List<String> g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f2912a;
        @DexIgnore
        public String b; // = "";
        @DexIgnore
        public String c; // = "";
        @DexIgnore
        public String d;
        @DexIgnore
        public int e; // = -1;
        @DexIgnore
        public /* final */ List<String> f;
        @DexIgnore
        public List<String> g;
        @DexIgnore
        public String h;

        @DexIgnore
        public a() {
            ArrayList arrayList = new ArrayList();
            this.f = arrayList;
            arrayList.add("");
        }

        @DexIgnore
        public static String d(String str, int i, int i2) {
            return b28.d(q18.u(str, i, i2, false));
        }

        @DexIgnore
        public static int k(String str, int i, int i2) {
            try {
                int parseInt = Integer.parseInt(q18.a(str, i, i2, "", false, false, false, true, null));
                if (parseInt <= 0 || parseInt > 65535) {
                    return -1;
                }
                return parseInt;
            } catch (NumberFormatException e2) {
            }
        }

        @DexIgnore
        public static int o(String str, int i, int i2) {
            int i3 = i;
            while (i3 < i2) {
                char charAt = str.charAt(i3);
                if (charAt == ':') {
                    return i3;
                }
                if (charAt == '[') {
                    do {
                        i3++;
                        if (i3 >= i2) {
                            break;
                        }
                    } while (str.charAt(i3) != ']');
                }
                i3++;
            }
            return i2;
        }

        @DexIgnore
        public static int t(String str, int i, int i2) {
            if (i2 - i < 2) {
                return -1;
            }
            char charAt = str.charAt(i);
            if ((charAt < 'a' || charAt > 'z') && (charAt < 'A' || charAt > 'Z')) {
                return -1;
            }
            int i3 = i;
            while (true) {
                i3++;
                if (i3 >= i2) {
                    return -1;
                }
                char charAt2 = str.charAt(i3);
                if ((charAt2 < 'a' || charAt2 > 'z') && ((charAt2 < 'A' || charAt2 > 'Z') && !((charAt2 >= '0' && charAt2 <= '9') || charAt2 == '+' || charAt2 == '-' || charAt2 == '.'))) {
                    if (charAt2 == ':') {
                        return i3;
                    }
                    return -1;
                }
            }
        }

        @DexIgnore
        public static int u(String str, int i, int i2) {
            int i3 = 0;
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt != '\\' && charAt != '/') {
                    break;
                }
                i3++;
                i++;
            }
            return i3;
        }

        @DexIgnore
        public a a(String str, String str2) {
            if (str != null) {
                if (this.g == null) {
                    this.g = new ArrayList();
                }
                this.g.add(q18.b(str, " \"'<>#&=", true, false, true, true));
                this.g.add(str2 != null ? q18.b(str2, " \"'<>#&=", true, false, true, true) : null);
                return this;
            }
            throw new NullPointerException("encodedName == null");
        }

        @DexIgnore
        public a b(String str, String str2) {
            if (str != null) {
                if (this.g == null) {
                    this.g = new ArrayList();
                }
                this.g.add(q18.b(str, " !\"#$&'(),/:;<=>?@[]\\^`{|}~", false, false, true, true));
                this.g.add(str2 != null ? q18.b(str2, " !\"#$&'(),/:;<=>?@[]\\^`{|}~", false, false, true, true) : null);
                return this;
            }
            throw new NullPointerException("name == null");
        }

        @DexIgnore
        public q18 c() {
            if (this.f2912a == null) {
                throw new IllegalStateException("scheme == null");
            } else if (this.d != null) {
                return new q18(this);
            } else {
                throw new IllegalStateException("host == null");
            }
        }

        @DexIgnore
        public int e() {
            int i = this.e;
            return i != -1 ? i : q18.e(this.f2912a);
        }

        @DexIgnore
        public a f(String str) {
            this.g = str != null ? q18.B(q18.b(str, " \"'<>#", true, false, true, true)) : null;
            return this;
        }

        @DexIgnore
        public a g(String str) {
            if (str != null) {
                String d2 = d(str, 0, str.length());
                if (d2 != null) {
                    this.d = d2;
                    return this;
                }
                throw new IllegalArgumentException("unexpected host: " + str);
            }
            throw new NullPointerException("host == null");
        }

        @DexIgnore
        public final boolean h(String str) {
            return str.equals(CodelessMatcher.CURRENT_CLASS_NAME) || str.equalsIgnoreCase("%2e");
        }

        @DexIgnore
        public final boolean i(String str) {
            return str.equals("..") || str.equalsIgnoreCase("%2e.") || str.equalsIgnoreCase(".%2e") || str.equalsIgnoreCase("%2e%2e");
        }

        @DexIgnore
        public a j(q18 q18, String str) {
            int i;
            boolean z;
            boolean z2;
            int i2;
            int E = b28.E(str, 0, str.length());
            int F = b28.F(str, E, str.length());
            int t = t(str, E, F);
            if (t != -1) {
                if (str.regionMatches(true, E, "https:", 0, 6)) {
                    this.f2912a = Utility.URL_SCHEME;
                    E += 6;
                } else if (str.regionMatches(true, E, "http:", 0, 5)) {
                    this.f2912a = "http";
                    E += 5;
                } else {
                    throw new IllegalArgumentException("Expected URL scheme 'http' or 'https' but was '" + str.substring(0, t) + "'");
                }
            } else if (q18 != null) {
                this.f2912a = q18.f2911a;
            } else {
                throw new IllegalArgumentException("Expected URL scheme 'http' or 'https' but no colon was found");
            }
            int u = u(str, E, F);
            if (u >= 2 || q18 == null || !q18.f2911a.equals(this.f2912a)) {
                int i3 = E + u;
                boolean z3 = false;
                boolean z4 = false;
                while (true) {
                    E = b28.o(str, i3, F, "@/\\?#");
                    char charAt = E != F ? str.charAt(E) : '\uffff';
                    if (charAt == '\uffff' || charAt == '#' || charAt == '/' || charAt == '\\' || charAt == '?') {
                        int o = o(str, i3, E);
                        int i4 = o + 1;
                    } else {
                        if (charAt != '@') {
                            i2 = i3;
                        } else {
                            if (!z3) {
                                int n = b28.n(str, i3, E, ':');
                                String a2 = q18.a(str, i3, n, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, null);
                                if (z4) {
                                    a2 = this.b + "%40" + a2;
                                }
                                this.b = a2;
                                if (n != E) {
                                    this.c = q18.a(str, n + 1, E, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, null);
                                    z2 = true;
                                } else {
                                    z2 = z3;
                                }
                                z = true;
                            } else {
                                this.c += "%40" + q18.a(str, i3, E, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, null);
                                z = z4;
                                z2 = z3;
                            }
                            i2 = E + 1;
                            z4 = z;
                            z3 = z2;
                        }
                        i3 = i2;
                    }
                }
                int o2 = o(str, i3, E);
                int i42 = o2 + 1;
                if (i42 < E) {
                    this.d = d(str, i3, o2);
                    int k = k(str, i42, E);
                    this.e = k;
                    if (k == -1) {
                        throw new IllegalArgumentException("Invalid URL port: \"" + str.substring(i42, E) + '\"');
                    }
                } else {
                    this.d = d(str, i3, o2);
                    this.e = q18.e(this.f2912a);
                }
                if (this.d == null) {
                    throw new IllegalArgumentException("Invalid URL host: \"" + str.substring(i3, o2) + '\"');
                }
            } else {
                this.b = q18.k();
                this.c = q18.g();
                this.d = q18.d;
                this.e = q18.e;
                this.f.clear();
                this.f.addAll(q18.i());
                if (E == F || str.charAt(E) == '#') {
                    f(q18.j());
                }
            }
            int o3 = b28.o(str, E, F, "?#");
            r(str, E, o3);
            if (o3 >= F || str.charAt(o3) != '?') {
                i = o3;
            } else {
                i = b28.n(str, o3, F, '#');
                this.g = q18.B(q18.a(str, o3 + 1, i, " \"'<>#", true, false, true, true, null));
            }
            if (i < F && str.charAt(i) == '#') {
                this.h = q18.a(str, i + 1, F, "", true, false, false, false, null);
            }
            return this;
        }

        @DexIgnore
        public a l(String str) {
            if (str != null) {
                this.c = q18.b(str, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
                return this;
            }
            throw new NullPointerException("password == null");
        }

        @DexIgnore
        public final void m() {
            List<String> list = this.f;
            if (!list.remove(list.size() - 1).isEmpty() || this.f.isEmpty()) {
                this.f.add("");
                return;
            }
            List<String> list2 = this.f;
            list2.set(list2.size() - 1, "");
        }

        @DexIgnore
        public a n(int i) {
            if (i <= 0 || i > 65535) {
                throw new IllegalArgumentException("unexpected port: " + i);
            }
            this.e = i;
            return this;
        }

        @DexIgnore
        public final void p(String str, int i, int i2, boolean z, boolean z2) {
            String a2 = q18.a(str, i, i2, " \"<>^`{}|/\\?#", z2, false, false, true, null);
            if (!h(a2)) {
                if (i(a2)) {
                    m();
                    return;
                }
                List<String> list = this.f;
                if (list.get(list.size() - 1).isEmpty()) {
                    List<String> list2 = this.f;
                    list2.set(list2.size() - 1, a2);
                } else {
                    this.f.add(a2);
                }
                if (z) {
                    this.f.add("");
                }
            }
        }

        @DexIgnore
        public a q() {
            int size = this.f.size();
            for (int i = 0; i < size; i++) {
                this.f.set(i, q18.b(this.f.get(i), "[]", true, true, false, true));
            }
            List<String> list = this.g;
            if (list != null) {
                int size2 = list.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    String str = this.g.get(i2);
                    if (str != null) {
                        this.g.set(i2, q18.b(str, "\\^`{|}", true, true, true, true));
                    }
                }
            }
            String str2 = this.h;
            if (str2 != null) {
                this.h = q18.b(str2, " \"#<>\\^`{|}", true, true, false, false);
            }
            return this;
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
            	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
            	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
            */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0003 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x0020  */
        public final void r(java.lang.String r7, int r8, int r9) {
            /*
                r6 = this;
                r5 = 1
                if (r8 != r9) goto L_0x0004
            L_0x0003:
                return
            L_0x0004:
                char r0 = r7.charAt(r8)
                r1 = 47
                if (r0 == r1) goto L_0x0010
                r1 = 92
                if (r0 != r1) goto L_0x0032
            L_0x0010:
                java.util.List<java.lang.String> r0 = r6.f
                r0.clear()
                java.util.List<java.lang.String> r0 = r6.f
                java.lang.String r1 = ""
                r0.add(r1)
            L_0x001c:
                int r2 = r8 + 1
            L_0x001e:
                if (r2 >= r9) goto L_0x0003
                java.lang.String r0 = "/\\"
                int r3 = com.fossil.b28.o(r7, r2, r9, r0)
                if (r3 >= r9) goto L_0x0041
                r4 = r5
            L_0x0029:
                r0 = r6
                r1 = r7
                r0.p(r1, r2, r3, r4, r5)
                if (r4 == 0) goto L_0x0043
                r8 = r3
                goto L_0x001c
            L_0x0032:
                java.util.List<java.lang.String> r0 = r6.f
                int r1 = r0.size()
                int r1 = r1 + -1
                java.lang.String r2 = ""
                r0.set(r1, r2)
                r2 = r8
                goto L_0x001e
            L_0x0041:
                r4 = 0
                goto L_0x0029
            L_0x0043:
                r2 = r3
                goto L_0x001e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.q18.a.r(java.lang.String, int, int):void");
        }

        @DexIgnore
        public a s(String str) {
            if (str != null) {
                if (str.equalsIgnoreCase("http")) {
                    this.f2912a = "http";
                } else if (str.equalsIgnoreCase(Utility.URL_SCHEME)) {
                    this.f2912a = Utility.URL_SCHEME;
                } else {
                    throw new IllegalArgumentException("unexpected scheme: " + str);
                }
                return this;
            }
            throw new NullPointerException("scheme == null");
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder();
            String str = this.f2912a;
            if (str != null) {
                sb.append(str);
                sb.append("://");
            } else {
                sb.append("//");
            }
            if (!this.b.isEmpty() || !this.c.isEmpty()) {
                sb.append(this.b);
                if (!this.c.isEmpty()) {
                    sb.append(':');
                    sb.append(this.c);
                }
                sb.append('@');
            }
            String str2 = this.d;
            if (str2 != null) {
                if (str2.indexOf(58) != -1) {
                    sb.append('[');
                    sb.append(this.d);
                    sb.append(']');
                } else {
                    sb.append(this.d);
                }
            }
            if (!(this.e == -1 && this.f2912a == null)) {
                int e2 = e();
                String str3 = this.f2912a;
                if (str3 == null || e2 != q18.e(str3)) {
                    sb.append(':');
                    sb.append(e2);
                }
            }
            q18.t(sb, this.f);
            if (this.g != null) {
                sb.append('?');
                q18.o(sb, this.g);
            }
            if (this.h != null) {
                sb.append('#');
                sb.append(this.h);
            }
            return sb.toString();
        }

        @DexIgnore
        public a v(String str) {
            if (str != null) {
                this.b = q18.b(str, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
                return this;
            }
            throw new NullPointerException("username == null");
        }
    }

    @DexIgnore
    public q18(a aVar) {
        String str = null;
        this.f2911a = aVar.f2912a;
        this.b = v(aVar.b, false);
        this.c = v(aVar.c, false);
        this.d = aVar.d;
        this.e = aVar.e();
        this.f = w(aVar.f, false);
        List<String> list = aVar.g;
        this.g = list != null ? w(list, true) : null;
        String str2 = aVar.h;
        this.h = str2 != null ? v(str2, false) : str;
        this.i = aVar.toString();
    }

    @DexIgnore
    public static List<String> B(String str) {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (i2 <= str.length()) {
            int indexOf = str.indexOf(38, i2);
            if (indexOf == -1) {
                indexOf = str.length();
            }
            int indexOf2 = str.indexOf(61, i2);
            if (indexOf2 == -1 || indexOf2 > indexOf) {
                arrayList.add(str.substring(i2, indexOf));
                arrayList.add(null);
            } else {
                arrayList.add(str.substring(i2, indexOf2));
                arrayList.add(str.substring(indexOf2 + 1, indexOf));
            }
            i2 = indexOf + 1;
        }
        return arrayList;
    }

    @DexIgnore
    public static String a(String str, int i2, int i3, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset) {
        int i4 = i2;
        while (i4 < i3) {
            int codePointAt = str.codePointAt(i4);
            if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && z4) || str2.indexOf(codePointAt) != -1 || ((codePointAt == 37 && (!z || (z2 && !y(str, i4, i3)))) || (codePointAt == 43 && z3)))) {
                i48 i48 = new i48();
                i48.E0(str, i2, i4);
                d(i48, str, i4, i3, str2, z, z2, z3, z4, charset);
                return i48.b0();
            }
            i4 += Character.charCount(codePointAt);
        }
        return str.substring(i2, i3);
    }

    @DexIgnore
    public static String b(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        return a(str, 0, str.length(), str2, z, z2, z3, z4, null);
    }

    @DexIgnore
    public static String c(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset) {
        return a(str, 0, str.length(), str2, z, z2, z3, z4, charset);
    }

    @DexIgnore
    public static void d(i48 i48, String str, int i2, int i3, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset) {
        i48 i482;
        i48 i483 = null;
        while (i2 < i3) {
            int codePointAt = str.codePointAt(i2);
            if (z) {
                if (codePointAt == 9 || codePointAt == 10 || codePointAt == 12) {
                    i482 = i483;
                    i2 += Character.charCount(codePointAt);
                    i483 = i482;
                } else if (codePointAt == 13) {
                    i482 = i483;
                    i2 += Character.charCount(codePointAt);
                    i483 = i482;
                }
            }
            if (codePointAt == 43 && z3) {
                i48.D0(z ? g78.ANY_NON_NULL_MARKER : "%2B");
                i482 = i483;
                i2 += Character.charCount(codePointAt);
                i483 = i482;
            } else if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && z4) || str2.indexOf(codePointAt) != -1 || (codePointAt == 37 && (!z || (z2 && !y(str, i2, i3)))))) {
                i482 = i483 == null ? new i48() : i483;
                if (charset == null || charset.equals(b28.i)) {
                    i482.F0(codePointAt);
                } else {
                    i482.B0(str, i2, Character.charCount(codePointAt) + i2, charset);
                }
                while (!i482.u()) {
                    int readByte = i482.readByte() & 255;
                    i48.w0(37);
                    i48.w0(j[(readByte >> 4) & 15]);
                    i48.w0(j[readByte & 15]);
                }
                i2 += Character.charCount(codePointAt);
                i483 = i482;
            } else {
                i48.F0(codePointAt);
                i482 = i483;
                i2 += Character.charCount(codePointAt);
                i483 = i482;
            }
        }
    }

    @DexIgnore
    public static int e(String str) {
        if (str.equals("http")) {
            return 80;
        }
        return str.equals(Utility.URL_SCHEME) ? 443 : -1;
    }

    @DexIgnore
    public static q18 l(String str) {
        a aVar = new a();
        aVar.j(null, str);
        return aVar.c();
    }

    @DexIgnore
    public static void o(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2 += 2) {
            String str = list.get(i2);
            String str2 = list.get(i2 + 1);
            if (i2 > 0) {
                sb.append('&');
            }
            sb.append(str);
            if (str2 != null) {
                sb.append('=');
                sb.append(str2);
            }
        }
    }

    @DexIgnore
    public static q18 r(String str) {
        try {
            return l(str);
        } catch (IllegalArgumentException e2) {
            return null;
        }
    }

    @DexIgnore
    public static void t(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            sb.append('/');
            sb.append(list.get(i2));
        }
    }

    @DexIgnore
    public static String u(String str, int i2, int i3, boolean z) {
        for (int i4 = i2; i4 < i3; i4++) {
            char charAt = str.charAt(i4);
            if (charAt == '%' || (charAt == '+' && z)) {
                i48 i48 = new i48();
                i48.E0(str, i2, i4);
                x(i48, str, i4, i3, z);
                return i48.b0();
            }
        }
        return str.substring(i2, i3);
    }

    @DexIgnore
    public static String v(String str, boolean z) {
        return u(str, 0, str.length(), z);
    }

    @DexIgnore
    public static void x(i48 i48, String str, int i2, int i3, boolean z) {
        int i4;
        int i5 = i2;
        while (i5 < i3) {
            int codePointAt = str.codePointAt(i5);
            if (codePointAt != 37 || (i4 = i5 + 2) >= i3) {
                if (codePointAt == 43 && z) {
                    i48.w0(32);
                    i4 = i5;
                }
                i48.F0(codePointAt);
                i4 = i5;
            } else {
                int k = b28.k(str.charAt(i5 + 1));
                int k2 = b28.k(str.charAt(i4));
                if (!(k == -1 || k2 == -1)) {
                    i48.w0((k << 4) + k2);
                }
                i48.F0(codePointAt);
                i4 = i5;
            }
            i5 = Character.charCount(codePointAt) + i4;
        }
    }

    @DexIgnore
    public static boolean y(String str, int i2, int i3) {
        int i4 = i2 + 2;
        return i4 < i3 && str.charAt(i2) == '%' && b28.k(str.charAt(i2 + 1)) != -1 && b28.k(str.charAt(i4)) != -1;
    }

    @DexIgnore
    public String A() {
        if (this.g == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        o(sb, this.g);
        return sb.toString();
    }

    @DexIgnore
    public String C() {
        a q = q("/...");
        q.v("");
        q.l("");
        return q.c().toString();
    }

    @DexIgnore
    public q18 D(String str) {
        a q = q(str);
        if (q != null) {
            return q.c();
        }
        return null;
    }

    @DexIgnore
    public String E() {
        return this.f2911a;
    }

    @DexIgnore
    public URI F() {
        a p = p();
        p.q();
        String aVar = p.toString();
        try {
            return new URI(aVar);
        } catch (URISyntaxException e2) {
            try {
                return URI.create(aVar.replaceAll("[\\u0000-\\u001F\\u007F-\\u009F\\p{javaWhitespace}]", ""));
            } catch (Exception e3) {
                throw new RuntimeException(e2);
            }
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof q18) && ((q18) obj).i.equals(this.i);
    }

    @DexIgnore
    public String f() {
        if (this.h == null) {
            return null;
        }
        return this.i.substring(this.i.indexOf(35) + 1);
    }

    @DexIgnore
    public String g() {
        if (this.c.isEmpty()) {
            return "";
        }
        int indexOf = this.i.indexOf(58, this.f2911a.length() + 3);
        return this.i.substring(indexOf + 1, this.i.indexOf(64));
    }

    @DexIgnore
    public String h() {
        int indexOf = this.i.indexOf(47, this.f2911a.length() + 3);
        String str = this.i;
        return this.i.substring(indexOf, b28.o(str, indexOf, str.length(), "?#"));
    }

    @DexIgnore
    public int hashCode() {
        return this.i.hashCode();
    }

    @DexIgnore
    public List<String> i() {
        int indexOf = this.i.indexOf(47, this.f2911a.length() + 3);
        String str = this.i;
        int o = b28.o(str, indexOf, str.length(), "?#");
        ArrayList arrayList = new ArrayList();
        while (indexOf < o) {
            int i2 = indexOf + 1;
            indexOf = b28.n(this.i, i2, o, '/');
            arrayList.add(this.i.substring(i2, indexOf));
        }
        return arrayList;
    }

    @DexIgnore
    public String j() {
        if (this.g == null) {
            return null;
        }
        int indexOf = this.i.indexOf(63) + 1;
        String str = this.i;
        return this.i.substring(indexOf, b28.n(str, indexOf, str.length(), '#'));
    }

    @DexIgnore
    public String k() {
        if (this.b.isEmpty()) {
            return "";
        }
        int length = this.f2911a.length() + 3;
        String str = this.i;
        return this.i.substring(length, b28.o(str, length, str.length(), ":@"));
    }

    @DexIgnore
    public String m() {
        return this.d;
    }

    @DexIgnore
    public boolean n() {
        return this.f2911a.equals(Utility.URL_SCHEME);
    }

    @DexIgnore
    public a p() {
        a aVar = new a();
        aVar.f2912a = this.f2911a;
        aVar.b = k();
        aVar.c = g();
        aVar.d = this.d;
        aVar.e = this.e != e(this.f2911a) ? this.e : -1;
        aVar.f.clear();
        aVar.f.addAll(i());
        aVar.f(j());
        aVar.h = f();
        return aVar;
    }

    @DexIgnore
    public a q(String str) {
        try {
            a aVar = new a();
            aVar.j(this, str);
            return aVar;
        } catch (IllegalArgumentException e2) {
            return null;
        }
    }

    @DexIgnore
    public List<String> s() {
        return this.f;
    }

    @DexIgnore
    public String toString() {
        return this.i;
    }

    @DexIgnore
    public final List<String> w(List<String> list, boolean z) {
        int size = list.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i2 = 0; i2 < size; i2++) {
            String str = list.get(i2);
            arrayList.add(str != null ? v(str, z) : null);
        }
        return Collections.unmodifiableList(arrayList);
    }

    @DexIgnore
    public int z() {
        return this.e;
    }
}
