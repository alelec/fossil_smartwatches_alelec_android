package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class au4 implements Factory<zt4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<xt4> f328a;
    @DexIgnore
    public /* final */ Provider<yt4> b;
    @DexIgnore
    public /* final */ Provider<on5> c;

    @DexIgnore
    public au4(Provider<xt4> provider, Provider<yt4> provider2, Provider<on5> provider3) {
        this.f328a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static au4 a(Provider<xt4> provider, Provider<yt4> provider2, Provider<on5> provider3) {
        return new au4(provider, provider2, provider3);
    }

    @DexIgnore
    public static zt4 c(xt4 xt4, yt4 yt4, on5 on5) {
        return new zt4(xt4, yt4, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public zt4 get() {
        return c(this.f328a.get(), this.b.get(), this.c.get());
    }
}
