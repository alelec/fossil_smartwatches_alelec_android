package com.fossil;

import android.content.ComponentCallbacks2;
import android.content.res.Configuration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface s81 extends ComponentCallbacks2 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static void a(s81 s81, Configuration configuration) {
        }

        @DexIgnore
        public static void b(s81 s81) {
            s81.onTrimMemory(80);
        }
    }

    @DexIgnore
    void onTrimMemory(int i);
}
