package com.fossil;

import android.content.res.AssetManager;
import android.media.MediaDataSource;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import com.facebook.share.internal.VideoUploader;
import com.facebook.stetho.dumpapp.Framer;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.common.enums.Action;
import com.zendesk.sdk.support.help.HelpSearchRecyclerViewAdapter;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Pattern;
import java.util.zip.CRC32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class eq0 {
    @DexIgnore
    public static /* final */ byte[] A; // = {-119, 80, 78, 71, 13, 10, 26, 10};
    @DexIgnore
    public static /* final */ byte[] B; // = {101, 88, 73, 102};
    @DexIgnore
    public static /* final */ byte[] C; // = {73, 72, 68, 82};
    @DexIgnore
    public static /* final */ byte[] D; // = {73, 69, 78, 68};
    @DexIgnore
    public static /* final */ byte[] E; // = {82, 73, 70, 70};
    @DexIgnore
    public static /* final */ byte[] F; // = {87, 69, 66, 80};
    @DexIgnore
    public static /* final */ byte[] G; // = {69, 88, 73, 70};
    @DexIgnore
    public static SimpleDateFormat H;
    @DexIgnore
    public static /* final */ String[] I; // = {"", "BYTE", "STRING", "USHORT", "ULONG", "URATIONAL", "SBYTE", "UNDEFINED", "SSHORT", "SLONG", "SRATIONAL", "SINGLE", "DOUBLE", "IFD"};
    @DexIgnore
    public static /* final */ int[] J; // = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8, 1};
    @DexIgnore
    public static /* final */ byte[] K; // = {65, 83, 67, 73, 73, 0, 0, 0};
    @DexIgnore
    public static /* final */ d[] L; // = {new d("NewSubfileType", 254, 4), new d("SubfileType", 255, 4), new d("ImageWidth", 256, 3, 4), new d("ImageLength", 257, 3, 4), new d("BitsPerSample", 258, 3), new d("Compression", 259, 3), new d("PhotometricInterpretation", 262, 3), new d("ImageDescription", 270, 2), new d("Make", 271, 2), new d("Model", 272, 2), new d("StripOffsets", 273, 3, 4), new d("Orientation", 274, 3), new d("SamplesPerPixel", 277, 3), new d("RowsPerStrip", 278, 3, 4), new d("StripByteCounts", 279, 3, 4), new d("XResolution", 282, 5), new d("YResolution", 283, 5), new d("PlanarConfiguration", 284, 3), new d("ResolutionUnit", 296, 3), new d("TransferFunction", Action.Presenter.NEXT, 3), new d("Software", 305, 2), new d("DateTime", 306, 2), new d("Artist", 315, 2), new d("WhitePoint", 318, 5), new d("PrimaryChromaticities", 319, 5), new d("SubIFDPointer", 330, 4), new d("JPEGInterchangeFormat", 513, 4), new d("JPEGInterchangeFormatLength", 514, 4), new d("YCbCrCoefficients", 529, 5), new d("YCbCrSubSampling", 530, 3), new d("YCbCrPositioning", HelpSearchRecyclerViewAdapter.TYPE_ARTICLE, 3), new d("ReferenceBlackWhite", 532, 5), new d("Copyright", 33432, 2), new d("ExifIFDPointer", 34665, 4), new d("GPSInfoIFDPointer", 34853, 4), new d("SensorTopBorder", 4, 4), new d("SensorLeftBorder", 5, 4), new d("SensorBottomBorder", 6, 4), new d("SensorRightBorder", 7, 4), new d("ISO", 23, 3), new d("JpgFromRaw", 46, 7), new d("Xmp", 700, 1)};
    @DexIgnore
    public static /* final */ d[] M; // = {new d("ExposureTime", 33434, 5), new d("FNumber", 33437, 5), new d("ExposureProgram", 34850, 3), new d("SpectralSensitivity", 34852, 2), new d("PhotographicSensitivity", 34855, 3), new d("OECF", 34856, 7), new d("SensitivityType", 34864, 3), new d("StandardOutputSensitivity", 34865, 4), new d("RecommendedExposureIndex", 34866, 4), new d("ISOSpeed", 34867, 4), new d("ISOSpeedLatitudeyyy", 34868, 4), new d("ISOSpeedLatitudezzz", 34869, 4), new d("ExifVersion", 36864, 2), new d("DateTimeOriginal", 36867, 2), new d("DateTimeDigitized", 36868, 2), new d("OffsetTime", 36880, 2), new d("OffsetTimeOriginal", 36881, 2), new d("OffsetTimeDigitized", 36882, 2), new d("ComponentsConfiguration", 37121, 7), new d("CompressedBitsPerPixel", 37122, 5), new d("ShutterSpeedValue", 37377, 10), new d("ApertureValue", 37378, 5), new d("BrightnessValue", 37379, 10), new d("ExposureBiasValue", 37380, 10), new d("MaxApertureValue", 37381, 5), new d("SubjectDistance", 37382, 5), new d("MeteringMode", 37383, 3), new d("LightSource", 37384, 3), new d("Flash", 37385, 3), new d("FocalLength", 37386, 5), new d("SubjectArea", 37396, 3), new d("MakerNote", 37500, 7), new d("UserComment", 37510, 7), new d("SubSecTime", 37520, 2), new d("SubSecTimeOriginal", 37521, 2), new d("SubSecTimeDigitized", 37522, 2), new d("FlashpixVersion", 40960, 7), new d("ColorSpace", 40961, 3), new d("PixelXDimension", 40962, 3, 4), new d("PixelYDimension", 40963, 3, 4), new d("RelatedSoundFile", 40964, 2), new d("InteroperabilityIFDPointer", 40965, 4), new d("FlashEnergy", 41483, 5), new d("SpatialFrequencyResponse", 41484, 7), new d("FocalPlaneXResolution", 41486, 5), new d("FocalPlaneYResolution", 41487, 5), new d("FocalPlaneResolutionUnit", 41488, 3), new d("SubjectLocation", 41492, 3), new d("ExposureIndex", 41493, 5), new d("SensingMethod", 41495, 3), new d("FileSource", 41728, 7), new d("SceneType", 41729, 7), new d("CFAPattern", 41730, 7), new d("CustomRendered", 41985, 3), new d("ExposureMode", 41986, 3), new d("WhiteBalance", 41987, 3), new d("DigitalZoomRatio", 41988, 5), new d("FocalLengthIn35mmFilm", 41989, 3), new d("SceneCaptureType", 41990, 3), new d("GainControl", 41991, 3), new d("Contrast", 41992, 3), new d("Saturation", 41993, 3), new d("Sharpness", 41994, 3), new d("DeviceSettingDescription", 41995, 7), new d("SubjectDistanceRange", 41996, 3), new d("ImageUniqueID", 42016, 2), new d("CameraOwnerName", 42032, 2), new d("BodySerialNumber", 42033, 2), new d("LensSpecification", 42034, 5), new d("LensMake", 42035, 2), new d("LensModel", 42036, 2), new d("Gamma", 42240, 5), new d("DNGVersion", 50706, 1), new d("DefaultCropSize", 50720, 3, 4)};
    @DexIgnore
    public static /* final */ d[] N; // = {new d("GPSVersionID", 0, 1), new d("GPSLatitudeRef", 1, 2), new d("GPSLatitude", 2, 5), new d("GPSLongitudeRef", 3, 2), new d("GPSLongitude", 4, 5), new d("GPSAltitudeRef", 5, 1), new d("GPSAltitude", 6, 5), new d("GPSTimeStamp", 7, 5), new d("GPSSatellites", 8, 2), new d("GPSStatus", 9, 2), new d("GPSMeasureMode", 10, 2), new d("GPSDOP", 11, 5), new d("GPSSpeedRef", 12, 2), new d("GPSSpeed", 13, 5), new d("GPSTrackRef", 14, 2), new d("GPSTrack", 15, 5), new d("GPSImgDirectionRef", 16, 2), new d("GPSImgDirection", 17, 5), new d("GPSMapDatum", 18, 2), new d("GPSDestLatitudeRef", 19, 2), new d("GPSDestLatitude", 20, 5), new d("GPSDestLongitudeRef", 21, 2), new d("GPSDestLongitude", 22, 5), new d("GPSDestBearingRef", 23, 2), new d("GPSDestBearing", 24, 5), new d("GPSDestDistanceRef", 25, 2), new d("GPSDestDistance", 26, 5), new d("GPSProcessingMethod", 27, 7), new d("GPSAreaInformation", 28, 7), new d("GPSDateStamp", 29, 2), new d("GPSDifferential", 30, 3), new d("GPSHPositioningError", 31, 5)};
    @DexIgnore
    public static /* final */ d[] O; // = {new d("InteroperabilityIndex", 1, 2)};
    @DexIgnore
    public static /* final */ d[] P; // = {new d("NewSubfileType", 254, 4), new d("SubfileType", 255, 4), new d("ThumbnailImageWidth", 256, 3, 4), new d("ThumbnailImageLength", 257, 3, 4), new d("BitsPerSample", 258, 3), new d("Compression", 259, 3), new d("PhotometricInterpretation", 262, 3), new d("ImageDescription", 270, 2), new d("Make", 271, 2), new d("Model", 272, 2), new d("StripOffsets", 273, 3, 4), new d("ThumbnailOrientation", 274, 3), new d("SamplesPerPixel", 277, 3), new d("RowsPerStrip", 278, 3, 4), new d("StripByteCounts", 279, 3, 4), new d("XResolution", 282, 5), new d("YResolution", 283, 5), new d("PlanarConfiguration", 284, 3), new d("ResolutionUnit", 296, 3), new d("TransferFunction", Action.Presenter.NEXT, 3), new d("Software", 305, 2), new d("DateTime", 306, 2), new d("Artist", 315, 2), new d("WhitePoint", 318, 5), new d("PrimaryChromaticities", 319, 5), new d("SubIFDPointer", 330, 4), new d("JPEGInterchangeFormat", 513, 4), new d("JPEGInterchangeFormatLength", 514, 4), new d("YCbCrCoefficients", 529, 5), new d("YCbCrSubSampling", 530, 3), new d("YCbCrPositioning", HelpSearchRecyclerViewAdapter.TYPE_ARTICLE, 3), new d("ReferenceBlackWhite", 532, 5), new d("Copyright", 33432, 2), new d("ExifIFDPointer", 34665, 4), new d("GPSInfoIFDPointer", 34853, 4), new d("DNGVersion", 50706, 1), new d("DefaultCropSize", 50720, 3, 4)};
    @DexIgnore
    public static /* final */ d Q; // = new d("StripOffsets", 273, 3);
    @DexIgnore
    public static /* final */ d[] R; // = {new d("ThumbnailImage", 256, 7), new d("CameraSettingsIFDPointer", 8224, 4), new d("ImageProcessingIFDPointer", 8256, 4)};
    @DexIgnore
    public static /* final */ d[] S; // = {new d("PreviewImageStart", 257, 4), new d("PreviewImageLength", 258, 4)};
    @DexIgnore
    public static /* final */ d[] T; // = {new d("AspectFrame", 4371, 3)};
    @DexIgnore
    public static /* final */ d[] U;
    @DexIgnore
    public static /* final */ d[][] V;
    @DexIgnore
    public static /* final */ d[] W; // = {new d("SubIFDPointer", 330, 4), new d("ExifIFDPointer", 34665, 4), new d("GPSInfoIFDPointer", 34853, 4), new d("InteroperabilityIFDPointer", 40965, 4), new d("CameraSettingsIFDPointer", 8224, 1), new d("ImageProcessingIFDPointer", 8256, 1)};
    @DexIgnore
    public static /* final */ HashMap<Integer, d>[] X;
    @DexIgnore
    public static /* final */ HashMap<String, d>[] Y;
    @DexIgnore
    public static /* final */ HashSet<String> Z; // = new HashSet<>(Arrays.asList("FNumber", "DigitalZoomRatio", "ExposureTime", "SubjectDistance", "GPSTimeStamp"));
    @DexIgnore
    public static /* final */ HashMap<Integer, Integer> a0; // = new HashMap<>();
    @DexIgnore
    public static /* final */ Charset b0;
    @DexIgnore
    public static /* final */ byte[] c0;
    @DexIgnore
    public static /* final */ byte[] d0; // = "http://ns.adobe.com/xap/1.0/\u0000".getBytes(b0);
    @DexIgnore
    public static /* final */ boolean r; // = Log.isLoggable("ExifInterface", 3);
    @DexIgnore
    public static /* final */ int[] s; // = {8, 8, 8};
    @DexIgnore
    public static /* final */ int[] t; // = {8};
    @DexIgnore
    public static /* final */ byte[] u; // = {-1, -40, -1};
    @DexIgnore
    public static /* final */ byte[] v; // = {102, 116, 121, 112};
    @DexIgnore
    public static /* final */ byte[] w; // = {109, 105, 102, Framer.STDOUT_FRAME_PREFIX};
    @DexIgnore
    public static /* final */ byte[] x; // = {104, 101, 105, 99};
    @DexIgnore
    public static /* final */ byte[] y; // = {79, 76, 89, 77, 80, 0};
    @DexIgnore
    public static /* final */ byte[] z; // = {79, 76, 89, 77, 80, 85, 83, 0, 73, 73};

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f972a;
    @DexIgnore
    public FileDescriptor b;
    @DexIgnore
    public AssetManager.AssetInputStream c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ HashMap<String, c>[] f;
    @DexIgnore
    public Set<Integer> g;
    @DexIgnore
    public ByteOrder h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends MediaDataSource {
        @DexIgnore
        public long b;
        @DexIgnore
        public /* final */ /* synthetic */ b c;

        @DexIgnore
        public a(eq0 eq0, b bVar) {
            this.c = bVar;
        }

        @DexIgnore
        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
        }

        @DexIgnore
        @Override // android.media.MediaDataSource
        public long getSize() throws IOException {
            return -1;
        }

        @DexIgnore
        @Override // android.media.MediaDataSource
        public int readAt(long j, byte[] bArr, int i, int i2) throws IOException {
            if (i2 == 0) {
                return 0;
            }
            if (j < 0) {
                return -1;
            }
            try {
                if (this.b != j) {
                    if (this.b >= 0 && j >= this.b + ((long) this.c.available())) {
                        return -1;
                    }
                    this.c.f(j);
                    this.b = j;
                }
                if (i2 > this.c.available()) {
                    i2 = this.c.available();
                }
                int read = this.c.read(bArr, i, i2);
                if (read >= 0) {
                    this.b += (long) read;
                    return read;
                }
            } catch (IOException e) {
            }
            this.b = -1;
            return -1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends InputStream implements DataInput {
        @DexIgnore
        public static /* final */ ByteOrder f; // = ByteOrder.LITTLE_ENDIAN;
        @DexIgnore
        public static /* final */ ByteOrder g; // = ByteOrder.BIG_ENDIAN;
        @DexIgnore
        public DataInputStream b;
        @DexIgnore
        public ByteOrder c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public int e;

        @DexIgnore
        public b(InputStream inputStream) throws IOException {
            this(inputStream, ByteOrder.BIG_ENDIAN);
        }

        @DexIgnore
        public b(InputStream inputStream, ByteOrder byteOrder) throws IOException {
            this.c = ByteOrder.BIG_ENDIAN;
            DataInputStream dataInputStream = new DataInputStream(inputStream);
            this.b = dataInputStream;
            int available = dataInputStream.available();
            this.d = available;
            this.e = 0;
            this.b.mark(available);
            this.c = byteOrder;
        }

        @DexIgnore
        public b(byte[] bArr) throws IOException {
            this(new ByteArrayInputStream(bArr));
        }

        @DexIgnore
        public int a() {
            return this.d;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int available() throws IOException {
            return this.b.available();
        }

        @DexIgnore
        public int b() {
            return this.e;
        }

        @DexIgnore
        public long c() throws IOException {
            return ((long) readInt()) & 4294967295L;
        }

        @DexIgnore
        public void f(long j) throws IOException {
            int i = this.e;
            if (((long) i) > j) {
                this.e = 0;
                this.b.reset();
                this.b.mark(this.d);
            } else {
                j -= (long) i;
            }
            int i2 = (int) j;
            if (skipBytes(i2) != i2) {
                throw new IOException("Couldn't seek up to the byteCount");
            }
        }

        @DexIgnore
        public void h(ByteOrder byteOrder) {
            this.c = byteOrder;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read() throws IOException {
            this.e++;
            return this.b.read();
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) throws IOException {
            int read = this.b.read(bArr, i, i2);
            this.e += read;
            return read;
        }

        @DexIgnore
        @Override // java.io.DataInput
        public boolean readBoolean() throws IOException {
            this.e++;
            return this.b.readBoolean();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public byte readByte() throws IOException {
            int i = this.e + 1;
            this.e = i;
            if (i <= this.d) {
                int read = this.b.read();
                if (read >= 0) {
                    return (byte) read;
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public char readChar() throws IOException {
            this.e += 2;
            return this.b.readChar();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public double readDouble() throws IOException {
            return Double.longBitsToDouble(readLong());
        }

        @DexIgnore
        @Override // java.io.DataInput
        public float readFloat() throws IOException {
            return Float.intBitsToFloat(readInt());
        }

        @DexIgnore
        @Override // java.io.DataInput
        public void readFully(byte[] bArr) throws IOException {
            int length = this.e + bArr.length;
            this.e = length;
            if (length > this.d) {
                throw new EOFException();
            } else if (this.b.read(bArr, 0, bArr.length) != bArr.length) {
                throw new IOException("Couldn't read up to the length of buffer");
            }
        }

        @DexIgnore
        @Override // java.io.DataInput
        public void readFully(byte[] bArr, int i, int i2) throws IOException {
            int i3 = this.e + i2;
            this.e = i3;
            if (i3 > this.d) {
                throw new EOFException();
            } else if (this.b.read(bArr, i, i2) != i2) {
                throw new IOException("Couldn't read up to the length of buffer");
            }
        }

        @DexIgnore
        @Override // java.io.DataInput
        public int readInt() throws IOException {
            int i = this.e + 4;
            this.e = i;
            if (i <= this.d) {
                int read = this.b.read();
                int read2 = this.b.read();
                int read3 = this.b.read();
                int read4 = this.b.read();
                if ((read | read2 | read3 | read4) >= 0) {
                    ByteOrder byteOrder = this.c;
                    if (byteOrder == f) {
                        return read + (read2 << 8) + (read3 << 16) + (read4 << 24);
                    } else if (byteOrder == g) {
                        return (read << 24) + (read2 << 16) + (read3 << 8) + read4;
                    } else {
                        throw new IOException("Invalid byte order: " + this.c);
                    }
                } else {
                    throw new EOFException();
                }
            } else {
                throw new EOFException();
            }
        }

        @DexIgnore
        @Override // java.io.DataInput
        public String readLine() throws IOException {
            Log.d("ExifInterface", "Currently unsupported");
            return null;
        }

        @DexIgnore
        @Override // java.io.DataInput
        public long readLong() throws IOException {
            int i = this.e + 8;
            this.e = i;
            if (i <= this.d) {
                int read = this.b.read();
                int read2 = this.b.read();
                int read3 = this.b.read();
                int read4 = this.b.read();
                int read5 = this.b.read();
                int read6 = this.b.read();
                int read7 = this.b.read();
                int read8 = this.b.read();
                if ((read | read2 | read3 | read4 | read5 | read6 | read7 | read8) >= 0) {
                    ByteOrder byteOrder = this.c;
                    if (byteOrder == f) {
                        return ((long) read) + (((long) read3) << 16) + (((long) read5) << 32) + (((long) read7) << 48) + (((long) read8) << 56) + (((long) read6) << 40) + (((long) read4) << 24) + (((long) read2) << 8);
                    } else if (byteOrder == g) {
                        return (((long) read2) << 48) + (((long) read) << 56) + (((long) read3) << 40) + (((long) read4) << 32) + (((long) read5) << 24) + (((long) read6) << 16) + (((long) read7) << 8) + ((long) read8);
                    } else {
                        throw new IOException("Invalid byte order: " + this.c);
                    }
                } else {
                    throw new EOFException();
                }
            } else {
                throw new EOFException();
            }
        }

        @DexIgnore
        @Override // java.io.DataInput
        public short readShort() throws IOException {
            int i = this.e + 2;
            this.e = i;
            if (i <= this.d) {
                int read = this.b.read();
                int read2 = this.b.read();
                if ((read | read2) >= 0) {
                    ByteOrder byteOrder = this.c;
                    if (byteOrder == f) {
                        return (short) (read + (read2 << 8));
                    }
                    if (byteOrder == g) {
                        return (short) ((read << 8) + read2);
                    }
                    throw new IOException("Invalid byte order: " + this.c);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public String readUTF() throws IOException {
            this.e += 2;
            return this.b.readUTF();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public int readUnsignedByte() throws IOException {
            this.e++;
            return this.b.readUnsignedByte();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public int readUnsignedShort() throws IOException {
            int i = this.e + 2;
            this.e = i;
            if (i <= this.d) {
                int read = this.b.read();
                int read2 = this.b.read();
                if ((read | read2) >= 0) {
                    ByteOrder byteOrder = this.c;
                    if (byteOrder == f) {
                        return read + (read2 << 8);
                    }
                    if (byteOrder == g) {
                        return (read << 8) + read2;
                    }
                    throw new IOException("Invalid byte order: " + this.c);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public int skipBytes(int i) throws IOException {
            int min = Math.min(i, this.d - this.e);
            int i2 = 0;
            while (i2 < min) {
                i2 += this.b.skipBytes(min - i2);
            }
            this.e += i2;
            return i2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f973a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ byte[] c;

        @DexIgnore
        public c(int i, int i2, long j, byte[] bArr) {
            this.f973a = i;
            this.b = i2;
            this.c = bArr;
        }

        @DexIgnore
        public c(int i, int i2, byte[] bArr) {
            this(i, i2, -1, bArr);
        }

        @DexIgnore
        public static c a(String str) {
            byte[] bytes = (str + (char) 0).getBytes(eq0.b0);
            return new c(2, bytes.length, bytes);
        }

        @DexIgnore
        public static c b(long j, ByteOrder byteOrder) {
            return c(new long[]{j}, byteOrder);
        }

        @DexIgnore
        public static c c(long[] jArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[(eq0.J[4] * jArr.length)]);
            wrap.order(byteOrder);
            for (long j : jArr) {
                wrap.putInt((int) j);
            }
            return new c(4, jArr.length, wrap.array());
        }

        @DexIgnore
        public static c d(e eVar, ByteOrder byteOrder) {
            return e(new e[]{eVar}, byteOrder);
        }

        @DexIgnore
        public static c e(e[] eVarArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[(eq0.J[5] * eVarArr.length)]);
            wrap.order(byteOrder);
            for (e eVar : eVarArr) {
                wrap.putInt((int) eVar.f975a);
                wrap.putInt((int) eVar.b);
            }
            return new c(5, eVarArr.length, wrap.array());
        }

        @DexIgnore
        public static c f(int i, ByteOrder byteOrder) {
            return g(new int[]{i}, byteOrder);
        }

        @DexIgnore
        public static c g(int[] iArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[(eq0.J[3] * iArr.length)]);
            wrap.order(byteOrder);
            for (int i : iArr) {
                wrap.putShort((short) i);
            }
            return new c(3, iArr.length, wrap.array());
        }

        @DexIgnore
        public double h(ByteOrder byteOrder) {
            Object k = k(byteOrder);
            if (k == null) {
                throw new NumberFormatException("NULL can't be converted to a double value");
            } else if (k instanceof String) {
                return Double.parseDouble((String) k);
            } else {
                if (k instanceof long[]) {
                    long[] jArr = (long[]) k;
                    if (jArr.length == 1) {
                        return (double) jArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (k instanceof int[]) {
                    int[] iArr = (int[]) k;
                    if (iArr.length == 1) {
                        return (double) iArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (k instanceof double[]) {
                    double[] dArr = (double[]) k;
                    if (dArr.length == 1) {
                        return dArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (k instanceof e[]) {
                    e[] eVarArr = (e[]) k;
                    if (eVarArr.length == 1) {
                        return eVarArr[0].a();
                    }
                    throw new NumberFormatException("There are more than one component");
                } else {
                    throw new NumberFormatException("Couldn't find a double value");
                }
            }
        }

        @DexIgnore
        public int i(ByteOrder byteOrder) {
            Object k = k(byteOrder);
            if (k == null) {
                throw new NumberFormatException("NULL can't be converted to a integer value");
            } else if (k instanceof String) {
                return Integer.parseInt((String) k);
            } else {
                if (k instanceof long[]) {
                    long[] jArr = (long[]) k;
                    if (jArr.length == 1) {
                        return (int) jArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (k instanceof int[]) {
                    int[] iArr = (int[]) k;
                    if (iArr.length == 1) {
                        return iArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else {
                    throw new NumberFormatException("Couldn't find a integer value");
                }
            }
        }

        @DexIgnore
        public String j(ByteOrder byteOrder) {
            int i = 0;
            Object k = k(byteOrder);
            if (k == null) {
                return null;
            }
            if (k instanceof String) {
                return (String) k;
            }
            StringBuilder sb = new StringBuilder();
            if (k instanceof long[]) {
                long[] jArr = (long[]) k;
                while (i < jArr.length) {
                    sb.append(jArr[i]);
                    i++;
                    if (i != jArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (k instanceof int[]) {
                int[] iArr = (int[]) k;
                while (i < iArr.length) {
                    sb.append(iArr[i]);
                    i++;
                    if (i != iArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (k instanceof double[]) {
                double[] dArr = (double[]) k;
                while (i < dArr.length) {
                    sb.append(dArr[i]);
                    i++;
                    if (i != dArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (!(k instanceof e[])) {
                return null;
            } else {
                e[] eVarArr = (e[]) k;
                while (i < eVarArr.length) {
                    sb.append(eVarArr[i].f975a);
                    sb.append('/');
                    sb.append(eVarArr[i].b);
                    i++;
                    if (i != eVarArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            }
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:112:0x0185 A[SYNTHETIC, Splitter:B:112:0x0185] */
        /* JADX WARNING: Removed duplicated region for block: B:141:0x01f2 A[SYNTHETIC, Splitter:B:141:0x01f2] */
        /* JADX WARNING: Removed duplicated region for block: B:166:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Object k(java.nio.ByteOrder r11) {
            /*
            // Method dump skipped, instructions count: 548
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.eq0.c.k(java.nio.ByteOrder):java.lang.Object");
        }

        @DexIgnore
        public String toString() {
            return "(" + eq0.I[this.f973a] + ", data length:" + this.c.length + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f974a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ int d;

        @DexIgnore
        public d(String str, int i, int i2) {
            this.b = str;
            this.f974a = i;
            this.c = i2;
            this.d = -1;
        }

        @DexIgnore
        public d(String str, int i, int i2, int i3) {
            this.b = str;
            this.f974a = i;
            this.c = i2;
            this.d = i3;
        }

        @DexIgnore
        public boolean a(int i) {
            int i2;
            int i3 = this.c;
            if (i3 == 7 || i == 7 || i3 == i || (i2 = this.d) == i) {
                return true;
            }
            if ((i3 == 4 || i2 == 4) && i == 3) {
                return true;
            }
            if ((this.c == 9 || this.d == 9) && i == 8) {
                return true;
            }
            return (this.c == 12 || this.d == 12) && i == 11;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ long f975a;
        @DexIgnore
        public /* final */ long b;

        @DexIgnore
        public e(long j, long j2) {
            if (j2 == 0) {
                this.f975a = 0;
                this.b = 1;
                return;
            }
            this.f975a = j;
            this.b = j2;
        }

        @DexIgnore
        public double a() {
            return ((double) this.f975a) / ((double) this.b);
        }

        @DexIgnore
        public String toString() {
            return this.f975a + "/" + this.b;
        }
    }

    /*
    static {
        Arrays.asList(1, 6, 3, 8);
        Arrays.asList(2, 7, 4, 5);
        "VP8X".getBytes(Charset.defaultCharset());
        "VP8L".getBytes(Charset.defaultCharset());
        "VP8 ".getBytes(Charset.defaultCharset());
        "ANIM".getBytes(Charset.defaultCharset());
        "ANMF".getBytes(Charset.defaultCharset());
        "XMP ".getBytes(Charset.defaultCharset());
        d[] dVarArr = {new d("ColorSpace", 55, 3)};
        U = dVarArr;
        d[] dVarArr2 = L;
        V = new d[][]{dVarArr2, M, N, O, P, dVarArr2, R, S, T, dVarArr};
        d[][] dVarArr3 = V;
        X = new HashMap[dVarArr3.length];
        Y = new HashMap[dVarArr3.length];
        Charset forName = Charset.forName("US-ASCII");
        b0 = forName;
        c0 = "Exif\u0000\u0000".getBytes(forName);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
        H = simpleDateFormat;
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        for (int i2 = 0; i2 < V.length; i2++) {
            X[i2] = new HashMap<>();
            Y[i2] = new HashMap<>();
            d[] dVarArr4 = V[i2];
            for (d dVar : dVarArr4) {
                X[i2].put(Integer.valueOf(dVar.f974a), dVar);
                Y[i2].put(dVar.b, dVar);
            }
        }
        a0.put(Integer.valueOf(W[0].f974a), 5);
        a0.put(Integer.valueOf(W[1].f974a), 1);
        a0.put(Integer.valueOf(W[2].f974a), 2);
        a0.put(Integer.valueOf(W[3].f974a), 3);
        a0.put(Integer.valueOf(W[4].f974a), 7);
        a0.put(Integer.valueOf(W[5].f974a), 8);
        Pattern.compile(".*[1-9].*");
        Pattern.compile("^([0-9][0-9]):([0-9][0-9]):([0-9][0-9])$");
    }
    */

    @DexIgnore
    public eq0(InputStream inputStream) throws IOException {
        this(inputStream, false);
    }

    @DexIgnore
    public eq0(InputStream inputStream, boolean z2) throws IOException {
        this.f = new HashMap[V.length];
        this.g = new HashSet(V.length);
        this.h = ByteOrder.BIG_ENDIAN;
        if (inputStream != null) {
            this.f972a = null;
            if (z2) {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, VideoUploader.RETRY_DELAY_UNIT_MS);
                if (!x(bufferedInputStream)) {
                    Log.w("ExifInterface", "Given data does not follow the structure of an Exif-only data.");
                    return;
                }
                this.e = true;
                this.c = null;
                this.b = null;
                inputStream = bufferedInputStream;
            } else if (inputStream instanceof AssetManager.AssetInputStream) {
                this.c = (AssetManager.AssetInputStream) inputStream;
                this.b = null;
            } else {
                if (inputStream instanceof FileInputStream) {
                    FileInputStream fileInputStream = (FileInputStream) inputStream;
                    if (F(fileInputStream.getFD())) {
                        this.c = null;
                        this.b = fileInputStream.getFD();
                    }
                }
                this.c = null;
                this.b = null;
            }
            J(inputStream);
            return;
        }
        throw new NullPointerException("inputStream cannot be null");
    }

    @DexIgnore
    public eq0(String str) throws IOException {
        this.f = new HashMap[V.length];
        this.g = new HashSet(V.length);
        this.h = ByteOrder.BIG_ENDIAN;
        if (str != null) {
            w(str);
            return;
        }
        throw new NullPointerException("filename cannot be null");
    }

    @DexIgnore
    public static boolean A(byte[] bArr) throws IOException {
        int i2 = 0;
        while (true) {
            byte[] bArr2 = u;
            if (i2 >= bArr2.length) {
                return true;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    @DexIgnore
    public static boolean F(FileDescriptor fileDescriptor) {
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                Os.lseek(fileDescriptor, 0, OsConstants.SEEK_CUR);
                return true;
            } catch (Exception e2) {
                if (r) {
                    Log.d("ExifInterface", "The file descriptor for the given input is not seekable");
                }
            }
        }
        return false;
    }

    @DexIgnore
    public static boolean R(byte[] bArr, byte[] bArr2) {
        if (bArr == null || bArr2 == null || bArr.length < bArr2.length) {
            return false;
        }
        for (int i2 = 0; i2 < bArr2.length; i2++) {
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static String b(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (int i2 = 0; i2 < bArr.length; i2++) {
            sb.append(String.format("%02x", Byte.valueOf(bArr[i2])));
        }
        return sb.toString();
    }

    @DexIgnore
    public static void c(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception e3) {
            }
        }
    }

    @DexIgnore
    public static double d(String str, String str2) {
        try {
            String[] split = str.split(",", -1);
            String[] split2 = split[0].split("/", -1);
            double parseDouble = Double.parseDouble(split2[0].trim()) / Double.parseDouble(split2[1].trim());
            String[] split3 = split[1].split("/", -1);
            double parseDouble2 = Double.parseDouble(split3[0].trim()) / Double.parseDouble(split3[1].trim());
            String[] split4 = split[2].split("/", -1);
            double parseDouble3 = ((Double.parseDouble(split4[0].trim()) / Double.parseDouble(split4[1].trim())) / 3600.0d) + parseDouble + (parseDouble2 / 60.0d);
            if (str2.equals(DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX) || str2.equals("W")) {
                return -parseDouble3;
            }
            if (str2.equals("N") || str2.equals("E")) {
                return parseDouble3;
            }
            throw new IllegalArgumentException();
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException e2) {
            throw new IllegalArgumentException();
        }
    }

    @DexIgnore
    public static long[] e(Object obj) {
        if (obj instanceof int[]) {
            int[] iArr = (int[]) obj;
            long[] jArr = new long[iArr.length];
            for (int i2 = 0; i2 < iArr.length; i2++) {
                jArr[i2] = (long) iArr[i2];
            }
            return jArr;
        } else if (obj instanceof long[]) {
            return (long[]) obj;
        } else {
            return null;
        }
    }

    @DexIgnore
    public static boolean x(BufferedInputStream bufferedInputStream) throws IOException {
        bufferedInputStream.mark(c0.length);
        byte[] bArr = new byte[c0.length];
        bufferedInputStream.read(bArr);
        bufferedInputStream.reset();
        int i2 = 0;
        while (true) {
            byte[] bArr2 = c0;
            if (i2 >= bArr2.length) {
                return true;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x002d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean B(byte[] r5) throws java.io.IOException {
        /*
            r4 = this;
            r2 = 0
            r0 = 0
            com.fossil.eq0$b r1 = new com.fossil.eq0$b     // Catch:{ Exception -> 0x0029, all -> 0x0021 }
            r1.<init>(r5)     // Catch:{ Exception -> 0x0029, all -> 0x0021 }
            java.nio.ByteOrder r2 = r4.M(r1)     // Catch:{ Exception -> 0x0031, all -> 0x0033 }
            r4.h = r2     // Catch:{ Exception -> 0x0031, all -> 0x0033 }
            r1.h(r2)     // Catch:{ Exception -> 0x0031, all -> 0x0033 }
            short r2 = r1.readShort()     // Catch:{ Exception -> 0x0031, all -> 0x0033 }
            r3 = 20306(0x4f52, float:2.8455E-41)
            if (r2 == r3) goto L_0x001c
            r3 = 21330(0x5352, float:2.989E-41)
            if (r2 != r3) goto L_0x001d
        L_0x001c:
            r0 = 1
        L_0x001d:
            r1.close()
        L_0x0020:
            return r0
        L_0x0021:
            r0 = move-exception
            r1 = r2
        L_0x0023:
            if (r1 == 0) goto L_0x0028
            r1.close()
        L_0x0028:
            throw r0
        L_0x0029:
            r1 = move-exception
            r1 = r2
        L_0x002b:
            if (r1 == 0) goto L_0x0020
            r1.close()
            goto L_0x0020
        L_0x0031:
            r2 = move-exception
            goto L_0x002b
        L_0x0033:
            r0 = move-exception
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.eq0.B(byte[]):boolean");
    }

    @DexIgnore
    public final boolean C(byte[] bArr) throws IOException {
        int i2 = 0;
        while (true) {
            byte[] bArr2 = A;
            if (i2 >= bArr2.length) {
                return true;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    @DexIgnore
    public final boolean D(byte[] bArr) throws IOException {
        byte[] bytes = "FUJIFILMCCD-RAW".getBytes(Charset.defaultCharset());
        for (int i2 = 0; i2 < bytes.length; i2++) {
            if (bArr[i2] != bytes[i2]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean E(byte[] r5) throws java.io.IOException {
        /*
            r4 = this;
            r2 = 0
            r0 = 0
            com.fossil.eq0$b r1 = new com.fossil.eq0$b     // Catch:{ Exception -> 0x0025, all -> 0x001d }
            r1.<init>(r5)     // Catch:{ Exception -> 0x0025, all -> 0x001d }
            java.nio.ByteOrder r2 = r4.M(r1)     // Catch:{ Exception -> 0x002d, all -> 0x002f }
            r4.h = r2     // Catch:{ Exception -> 0x002d, all -> 0x002f }
            r1.h(r2)     // Catch:{ Exception -> 0x002d, all -> 0x002f }
            short r2 = r1.readShort()     // Catch:{ Exception -> 0x002d, all -> 0x002f }
            r3 = 85
            if (r2 != r3) goto L_0x0019
            r0 = 1
        L_0x0019:
            r1.close()
        L_0x001c:
            return r0
        L_0x001d:
            r0 = move-exception
            r1 = r2
        L_0x001f:
            if (r1 == 0) goto L_0x0024
            r1.close()
        L_0x0024:
            throw r0
        L_0x0025:
            r1 = move-exception
            r1 = r2
        L_0x0027:
            if (r1 == 0) goto L_0x001c
            r1.close()
            goto L_0x001c
        L_0x002d:
            r2 = move-exception
            goto L_0x0027
        L_0x002f:
            r0 = move-exception
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.eq0.E(byte[]):boolean");
    }

    @DexIgnore
    public final boolean G(HashMap hashMap) throws IOException {
        c cVar;
        int i2;
        c cVar2 = (c) hashMap.get("BitsPerSample");
        if (cVar2 != null) {
            int[] iArr = (int[]) cVar2.k(this.h);
            if (Arrays.equals(s, iArr)) {
                return true;
            }
            if (this.d == 3 && (cVar = (c) hashMap.get("PhotometricInterpretation")) != null && (((i2 = cVar.i(this.h)) == 1 && Arrays.equals(iArr, t)) || (i2 == 6 && Arrays.equals(iArr, s)))) {
                return true;
            }
        }
        if (r) {
            Log.d("ExifInterface", "Unsupported data type value");
        }
        return false;
    }

    @DexIgnore
    public final boolean H(HashMap hashMap) throws IOException {
        c cVar = (c) hashMap.get("ImageLength");
        c cVar2 = (c) hashMap.get("ImageWidth");
        if (!(cVar == null || cVar2 == null)) {
            return cVar.i(this.h) <= 512 && cVar2.i(this.h) <= 512;
        }
    }

    @DexIgnore
    public final boolean I(byte[] bArr) throws IOException {
        int i2 = 0;
        while (true) {
            byte[] bArr2 = E;
            if (i2 >= bArr2.length) {
                int i3 = 0;
                while (true) {
                    byte[] bArr3 = F;
                    if (i3 >= bArr3.length) {
                        return true;
                    }
                    if (bArr[E.length + i3 + 4] != bArr3[i3]) {
                        return false;
                    }
                    i3++;
                }
            } else if (bArr[i2] != bArr2[i2]) {
                return false;
            } else {
                i2++;
            }
        }
    }

    @DexIgnore
    public final void J(InputStream inputStream) {
        if (inputStream != null) {
            for (int i2 = 0; i2 < V.length; i2++) {
                try {
                    this.f[i2] = new HashMap<>();
                } catch (IOException e2) {
                    if (r) {
                        Log.w("ExifInterface", "Invalid image: ExifInterface got an unsupported image format file(ExifInterface supports JPEG and some RAW image formats only) or a corrupted JPEG file to ExifInterface.", e2);
                    }
                    a();
                    if (!r) {
                        return;
                    }
                } catch (Throwable th) {
                    a();
                    if (r) {
                        L();
                    }
                    throw th;
                }
            }
            if (!this.e) {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, VideoUploader.RETRY_DELAY_UNIT_MS);
                this.d = l(bufferedInputStream);
                inputStream = bufferedInputStream;
            }
            b bVar = new b(inputStream);
            if (!this.e) {
                switch (this.d) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 5:
                    case 6:
                    case 8:
                    case 11:
                        p(bVar);
                        break;
                    case 4:
                        j(bVar, 0, 0);
                        break;
                    case 7:
                        m(bVar);
                        break;
                    case 9:
                        o(bVar);
                        break;
                    case 10:
                        r(bVar);
                        break;
                    case 12:
                        i(bVar);
                        break;
                    case 13:
                        n(bVar);
                        break;
                    case 14:
                        t(bVar);
                        break;
                }
            } else {
                s(bVar);
            }
            Q(bVar);
            a();
            if (!r) {
                return;
            }
            L();
            return;
        }
        throw new NullPointerException("inputstream shouldn't be null");
    }

    @DexIgnore
    public final void K(b bVar, int i2) throws IOException {
        ByteOrder M2 = M(bVar);
        this.h = M2;
        bVar.h(M2);
        int readUnsignedShort = bVar.readUnsignedShort();
        int i3 = this.d;
        if (i3 == 7 || i3 == 10 || readUnsignedShort == 42) {
            int readInt = bVar.readInt();
            if (readInt < 8 || readInt >= i2) {
                throw new IOException("Invalid first Ifd offset: " + readInt);
            }
            int i4 = readInt - 8;
            if (i4 > 0 && bVar.skipBytes(i4) != i4) {
                throw new IOException("Couldn't jump to first Ifd: " + i4);
            }
            return;
        }
        throw new IOException("Invalid start code: " + Integer.toHexString(readUnsignedShort));
    }

    @DexIgnore
    public final void L() {
        for (int i2 = 0; i2 < this.f.length; i2++) {
            Log.d("ExifInterface", "The size of tag group[" + i2 + "]: " + this.f[i2].size());
            for (Map.Entry<String, c> entry : this.f[i2].entrySet()) {
                c value = entry.getValue();
                Log.d("ExifInterface", "tagName: " + entry.getKey() + ", tagType: " + value.toString() + ", tagValue: '" + value.j(this.h) + "'");
            }
        }
    }

    @DexIgnore
    public final ByteOrder M(b bVar) throws IOException {
        short readShort = bVar.readShort();
        if (readShort == 18761) {
            if (r) {
                Log.d("ExifInterface", "readExifSegment: Byte Align II");
            }
            return ByteOrder.LITTLE_ENDIAN;
        } else if (readShort == 19789) {
            if (r) {
                Log.d("ExifInterface", "readExifSegment: Byte Align MM");
            }
            return ByteOrder.BIG_ENDIAN;
        } else {
            throw new IOException("Invalid byte order: " + Integer.toHexString(readShort));
        }
    }

    @DexIgnore
    public final void N(byte[] bArr, int i2) throws IOException {
        b bVar = new b(bArr);
        K(bVar, bArr.length);
        O(bVar, i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x015d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void O(com.fossil.eq0.b r25, int r26) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 1143
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.eq0.O(com.fossil.eq0$b, int):void");
    }

    @DexIgnore
    public final void P(b bVar, int i2) throws IOException {
        c cVar;
        c cVar2 = this.f[i2].get("ImageLength");
        c cVar3 = this.f[i2].get("ImageWidth");
        if ((cVar2 == null || cVar3 == null) && (cVar = this.f[i2].get("JPEGInterchangeFormat")) != null) {
            j(bVar, cVar.i(this.h), i2);
        }
    }

    @DexIgnore
    public final void Q(b bVar) throws IOException {
        HashMap<String, c> hashMap = this.f[4];
        c cVar = hashMap.get("Compression");
        if (cVar != null) {
            int i2 = cVar.i(this.h);
            this.l = i2;
            if (i2 != 1) {
                if (i2 == 6) {
                    u(bVar, hashMap);
                    return;
                } else if (i2 != 7) {
                    return;
                }
            }
            if (G(hashMap)) {
                v(bVar, hashMap);
                return;
            }
            return;
        }
        this.l = 6;
        u(bVar, hashMap);
    }

    @DexIgnore
    public final void S(int i2, int i3) throws IOException {
        if (!this.f[i2].isEmpty() && !this.f[i3].isEmpty()) {
            c cVar = this.f[i2].get("ImageLength");
            c cVar2 = this.f[i2].get("ImageWidth");
            c cVar3 = this.f[i3].get("ImageLength");
            c cVar4 = this.f[i3].get("ImageWidth");
            if (cVar == null || cVar2 == null) {
                if (r) {
                    Log.d("ExifInterface", "First image does not contain valid size information");
                }
            } else if (cVar3 != null && cVar4 != null) {
                int i4 = cVar.i(this.h);
                int i5 = cVar2.i(this.h);
                int i6 = cVar3.i(this.h);
                int i7 = cVar4.i(this.h);
                if (i4 < i6 && i5 < i7) {
                    HashMap<String, c>[] hashMapArr = this.f;
                    HashMap<String, c> hashMap = hashMapArr[i2];
                    hashMapArr[i2] = hashMapArr[i3];
                    hashMapArr[i3] = hashMap;
                }
            } else if (r) {
                Log.d("ExifInterface", "Second image does not contain valid size information");
            }
        } else if (r) {
            Log.d("ExifInterface", "Cannot perform swap since only one image data exists");
        }
    }

    @DexIgnore
    public final void T(b bVar, int i2) throws IOException {
        c f2;
        c f3;
        c cVar = this.f[i2].get("DefaultCropSize");
        c cVar2 = this.f[i2].get("SensorTopBorder");
        c cVar3 = this.f[i2].get("SensorLeftBorder");
        c cVar4 = this.f[i2].get("SensorBottomBorder");
        c cVar5 = this.f[i2].get("SensorRightBorder");
        if (cVar != null) {
            if (cVar.f973a == 5) {
                e[] eVarArr = (e[]) cVar.k(this.h);
                if (eVarArr == null || eVarArr.length != 2) {
                    Log.w("ExifInterface", "Invalid crop size values. cropSize=" + Arrays.toString(eVarArr));
                    return;
                }
                f2 = c.d(eVarArr[0], this.h);
                f3 = c.d(eVarArr[1], this.h);
            } else {
                int[] iArr = (int[]) cVar.k(this.h);
                if (iArr == null || iArr.length != 2) {
                    Log.w("ExifInterface", "Invalid crop size values. cropSize=" + Arrays.toString(iArr));
                    return;
                }
                f2 = c.f(iArr[0], this.h);
                f3 = c.f(iArr[1], this.h);
            }
            this.f[i2].put("ImageWidth", f2);
            this.f[i2].put("ImageLength", f3);
        } else if (cVar2 == null || cVar3 == null || cVar4 == null || cVar5 == null) {
            P(bVar, i2);
        } else {
            int i3 = cVar2.i(this.h);
            int i4 = cVar4.i(this.h);
            int i5 = cVar5.i(this.h);
            int i6 = cVar3.i(this.h);
            if (i4 > i3 && i5 > i6) {
                c f4 = c.f(i4 - i3, this.h);
                c f5 = c.f(i5 - i6, this.h);
                this.f[i2].put("ImageLength", f4);
                this.f[i2].put("ImageWidth", f5);
            }
        }
    }

    @DexIgnore
    public final void U() throws IOException {
        S(0, 5);
        S(0, 4);
        S(5, 4);
        c cVar = this.f[1].get("PixelXDimension");
        c cVar2 = this.f[1].get("PixelYDimension");
        if (!(cVar == null || cVar2 == null)) {
            this.f[0].put("ImageWidth", cVar);
            this.f[0].put("ImageLength", cVar2);
        }
        if (this.f[4].isEmpty() && H(this.f[5])) {
            HashMap<String, c>[] hashMapArr = this.f;
            hashMapArr[4] = hashMapArr[5];
            hashMapArr[5] = new HashMap<>();
        }
        if (!H(this.f[4])) {
            Log.d("ExifInterface", "No image meets the size requirements of a thumbnail image.");
        }
    }

    @DexIgnore
    public final void a() {
        String f2 = f("DateTimeOriginal");
        if (f2 != null && f("DateTime") == null) {
            this.f[0].put("DateTime", c.a(f2));
        }
        if (f("ImageWidth") == null) {
            this.f[0].put("ImageWidth", c.b(0, this.h));
        }
        if (f("ImageLength") == null) {
            this.f[0].put("ImageLength", c.b(0, this.h));
        }
        if (f("Orientation") == null) {
            this.f[0].put("Orientation", c.b(0, this.h));
        }
        if (f("LightSource") == null) {
            this.f[1].put("LightSource", c.b(0, this.h));
        }
    }

    @DexIgnore
    public String f(String str) {
        if (str != null) {
            c h2 = h(str);
            if (h2 != null) {
                if (!Z.contains(str)) {
                    return h2.j(this.h);
                }
                if (str.equals("GPSTimeStamp")) {
                    int i2 = h2.f973a;
                    if (i2 == 5 || i2 == 10) {
                        e[] eVarArr = (e[]) h2.k(this.h);
                        if (eVarArr == null || eVarArr.length != 3) {
                            Log.w("ExifInterface", "Invalid GPS Timestamp array. array=" + Arrays.toString(eVarArr));
                            return null;
                        }
                        return String.format("%02d:%02d:%02d", Integer.valueOf((int) (((float) eVarArr[0].f975a) / ((float) eVarArr[0].b))), Integer.valueOf((int) (((float) eVarArr[1].f975a) / ((float) eVarArr[1].b))), Integer.valueOf((int) (((float) eVarArr[2].f975a) / ((float) eVarArr[2].b))));
                    }
                    Log.w("ExifInterface", "GPS Timestamp format is not rational. format=" + h2.f973a);
                    return null;
                }
                try {
                    return Double.toString(h2.h(this.h));
                } catch (NumberFormatException e2) {
                }
            }
            return null;
        }
        throw new NullPointerException("tag shouldn't be null");
    }

    @DexIgnore
    public int g(String str, int i2) {
        if (str != null) {
            c h2 = h(str);
            if (h2 == null) {
                return i2;
            }
            try {
                return h2.i(this.h);
            } catch (NumberFormatException e2) {
                return i2;
            }
        } else {
            throw new NullPointerException("tag shouldn't be null");
        }
    }

    @DexIgnore
    public final c h(String str) {
        if (str != null) {
            if ("ISOSpeedRatings".equals(str)) {
                if (r) {
                    Log.d("ExifInterface", "getExifAttribute: Replacing TAG_ISO_SPEED_RATINGS with TAG_PHOTOGRAPHIC_SENSITIVITY.");
                }
                str = "PhotographicSensitivity";
            }
            for (int i2 = 0; i2 < V.length; i2++) {
                c cVar = this.f[i2].get(str);
                if (cVar != null) {
                    return cVar;
                }
            }
            return null;
        }
        throw new NullPointerException("tag shouldn't be null");
    }

    @DexIgnore
    public final void i(b bVar) throws IOException {
        String str;
        String str2;
        String str3;
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                mediaMetadataRetriever.setDataSource(new a(this, bVar));
            } else if (this.b != null) {
                mediaMetadataRetriever.setDataSource(this.b);
            } else if (this.f972a != null) {
                mediaMetadataRetriever.setDataSource(this.f972a);
            } else {
                mediaMetadataRetriever.release();
                return;
            }
            String extractMetadata = mediaMetadataRetriever.extractMetadata(33);
            String extractMetadata2 = mediaMetadataRetriever.extractMetadata(34);
            String extractMetadata3 = mediaMetadataRetriever.extractMetadata(26);
            String extractMetadata4 = mediaMetadataRetriever.extractMetadata(17);
            if ("yes".equals(extractMetadata3)) {
                String extractMetadata5 = mediaMetadataRetriever.extractMetadata(29);
                str3 = mediaMetadataRetriever.extractMetadata(30);
                str2 = mediaMetadataRetriever.extractMetadata(31);
                str = extractMetadata5;
            } else if ("yes".equals(extractMetadata4)) {
                String extractMetadata6 = mediaMetadataRetriever.extractMetadata(18);
                str3 = mediaMetadataRetriever.extractMetadata(19);
                str2 = mediaMetadataRetriever.extractMetadata(24);
                str = extractMetadata6;
            } else {
                str = null;
                str2 = null;
                str3 = null;
            }
            if (str != null) {
                this.f[0].put("ImageWidth", c.f(Integer.parseInt(str), this.h));
            }
            if (str3 != null) {
                this.f[0].put("ImageLength", c.f(Integer.parseInt(str3), this.h));
            }
            if (str2 != null) {
                int i2 = 1;
                int parseInt = Integer.parseInt(str2);
                if (parseInt == 90) {
                    i2 = 6;
                } else if (parseInt == 180) {
                    i2 = 3;
                } else if (parseInt == 270) {
                    i2 = 8;
                }
                this.f[0].put("Orientation", c.f(i2, this.h));
            }
            if (!(extractMetadata == null || extractMetadata2 == null)) {
                int parseInt2 = Integer.parseInt(extractMetadata);
                int parseInt3 = Integer.parseInt(extractMetadata2);
                if (parseInt3 > 6) {
                    bVar.f((long) parseInt2);
                    byte[] bArr = new byte[6];
                    if (bVar.read(bArr) == 6) {
                        int i3 = parseInt3 - 6;
                        if (Arrays.equals(bArr, c0)) {
                            byte[] bArr2 = new byte[i3];
                            if (bVar.read(bArr2) == i3) {
                                this.m = parseInt2 + 6;
                                N(bArr2, 0);
                            } else {
                                throw new IOException("Can't read exif");
                            }
                        } else {
                            throw new IOException("Invalid identifier");
                        }
                    } else {
                        throw new IOException("Can't read identifier");
                    }
                } else {
                    throw new IOException("Invalid exif length");
                }
            }
            if (r) {
                Log.d("ExifInterface", "Heif meta: " + str + "x" + str3 + ", rotation " + str2);
            }
        } finally {
            mediaMetadataRetriever.release();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00cc A[FALL_THROUGH] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void j(com.fossil.eq0.b r13, int r14, int r15) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 532
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.eq0.j(com.fossil.eq0$b, int, int):void");
    }

    @DexIgnore
    public double[] k() {
        String f2 = f("GPSLatitude");
        String f3 = f("GPSLatitudeRef");
        String f4 = f("GPSLongitude");
        String f5 = f("GPSLongitudeRef");
        if (!(f2 == null || f3 == null || f4 == null || f5 == null)) {
            try {
                return new double[]{d(f2, f3), d(f4, f5)};
            } catch (IllegalArgumentException e2) {
                Log.w("ExifInterface", "Latitude/longitude values are not parsable. " + String.format("latValue=%s, latRef=%s, lngValue=%s, lngRef=%s", f2, f3, f4, f5));
            }
        }
        return null;
    }

    @DexIgnore
    public final int l(BufferedInputStream bufferedInputStream) throws IOException {
        bufferedInputStream.mark(VideoUploader.RETRY_DELAY_UNIT_MS);
        byte[] bArr = new byte[VideoUploader.RETRY_DELAY_UNIT_MS];
        bufferedInputStream.read(bArr);
        bufferedInputStream.reset();
        if (A(bArr)) {
            return 4;
        }
        if (D(bArr)) {
            return 9;
        }
        if (z(bArr)) {
            return 12;
        }
        if (B(bArr)) {
            return 7;
        }
        if (E(bArr)) {
            return 10;
        }
        if (C(bArr)) {
            return 13;
        }
        return I(bArr) ? 14 : 0;
    }

    @DexIgnore
    public final void m(b bVar) throws IOException {
        p(bVar);
        c cVar = this.f[1].get("MakerNote");
        if (cVar != null) {
            b bVar2 = new b(cVar.c);
            bVar2.h(this.h);
            byte[] bArr = new byte[y.length];
            bVar2.readFully(bArr);
            bVar2.f(0);
            byte[] bArr2 = new byte[z.length];
            bVar2.readFully(bArr2);
            if (Arrays.equals(bArr, y)) {
                bVar2.f(8);
            } else if (Arrays.equals(bArr2, z)) {
                bVar2.f(12);
            }
            O(bVar2, 6);
            c cVar2 = this.f[7].get("PreviewImageStart");
            c cVar3 = this.f[7].get("PreviewImageLength");
            if (!(cVar2 == null || cVar3 == null)) {
                this.f[5].put("JPEGInterchangeFormat", cVar2);
                this.f[5].put("JPEGInterchangeFormatLength", cVar3);
            }
            c cVar4 = this.f[8].get("AspectFrame");
            if (cVar4 != null) {
                int[] iArr = (int[]) cVar4.k(this.h);
                if (iArr == null || iArr.length != 4) {
                    Log.w("ExifInterface", "Invalid aspect frame values. frame=" + Arrays.toString(iArr));
                } else if (iArr[2] > iArr[0] && iArr[3] > iArr[1]) {
                    int i2 = (iArr[2] - iArr[0]) + 1;
                    int i3 = (iArr[3] - iArr[1]) + 1;
                    if (i2 < i3) {
                        int i4 = i2 + i3;
                        i3 = i4 - i3;
                        i2 = i4 - i3;
                    }
                    c f2 = c.f(i2, this.h);
                    c f3 = c.f(i3, this.h);
                    this.f[0].put("ImageWidth", f2);
                    this.f[0].put("ImageLength", f3);
                }
            }
        }
    }

    @DexIgnore
    public final void n(b bVar) throws IOException {
        if (r) {
            Log.d("ExifInterface", "getPngAttributes starting with: " + bVar);
        }
        bVar.h(ByteOrder.BIG_ENDIAN);
        bVar.skipBytes(A.length);
        int length = A.length + 0;
        while (true) {
            try {
                int readInt = bVar.readInt();
                byte[] bArr = new byte[4];
                if (bVar.read(bArr) == 4) {
                    int i2 = length + 4 + 4;
                    if (i2 == 16 && !Arrays.equals(bArr, C)) {
                        throw new IOException("Encountered invalid PNG file--IHDR chunk should appearas the first chunk");
                    } else if (!Arrays.equals(bArr, D)) {
                        if (Arrays.equals(bArr, B)) {
                            byte[] bArr2 = new byte[readInt];
                            if (bVar.read(bArr2) == readInt) {
                                int readInt2 = bVar.readInt();
                                CRC32 crc32 = new CRC32();
                                crc32.update(bArr);
                                crc32.update(bArr2);
                                if (((int) crc32.getValue()) == readInt2) {
                                    this.m = i2;
                                    N(bArr2, 0);
                                    U();
                                    return;
                                }
                                throw new IOException("Encountered invalid CRC value for PNG-EXIF chunk.\n recorded CRC value: " + readInt2 + ", calculated CRC value: " + crc32.getValue());
                            }
                            throw new IOException("Failed to read given length for given PNG chunk type: " + b(bArr));
                        }
                        int i3 = readInt + 4;
                        bVar.skipBytes(i3);
                        length = i2 + i3;
                    } else {
                        return;
                    }
                } else {
                    throw new IOException("Encountered invalid length while parsing PNG chunktype");
                }
            } catch (EOFException e2) {
                throw new IOException("Encountered corrupt PNG file.");
            }
        }
    }

    @DexIgnore
    public final void o(b bVar) throws IOException {
        bVar.skipBytes(84);
        byte[] bArr = new byte[4];
        byte[] bArr2 = new byte[4];
        bVar.read(bArr);
        bVar.skipBytes(4);
        bVar.read(bArr2);
        int i2 = ByteBuffer.wrap(bArr).getInt();
        int i3 = ByteBuffer.wrap(bArr2).getInt();
        j(bVar, i2, 5);
        bVar.f((long) i3);
        bVar.h(ByteOrder.BIG_ENDIAN);
        int readInt = bVar.readInt();
        if (r) {
            Log.d("ExifInterface", "numberOfDirectoryEntry: " + readInt);
        }
        for (int i4 = 0; i4 < readInt; i4++) {
            int readUnsignedShort = bVar.readUnsignedShort();
            int readUnsignedShort2 = bVar.readUnsignedShort();
            if (readUnsignedShort == Q.f974a) {
                short readShort = bVar.readShort();
                short readShort2 = bVar.readShort();
                c f2 = c.f(readShort, this.h);
                c f3 = c.f(readShort2, this.h);
                this.f[0].put("ImageLength", f2);
                this.f[0].put("ImageWidth", f3);
                if (r) {
                    Log.d("ExifInterface", "Updated to length: " + ((int) readShort) + ", width: " + ((int) readShort2));
                    return;
                }
                return;
            }
            bVar.skipBytes(readUnsignedShort2);
        }
    }

    @DexIgnore
    public final void p(b bVar) throws IOException {
        c cVar;
        K(bVar, bVar.available());
        O(bVar, 0);
        T(bVar, 0);
        T(bVar, 5);
        T(bVar, 4);
        U();
        if (this.d == 8 && (cVar = this.f[1].get("MakerNote")) != null) {
            b bVar2 = new b(cVar.c);
            bVar2.h(this.h);
            bVar2.f(6);
            O(bVar2, 9);
            c cVar2 = this.f[9].get("ColorSpace");
            if (cVar2 != null) {
                this.f[1].put("ColorSpace", cVar2);
            }
        }
    }

    @DexIgnore
    public int q() {
        switch (g("Orientation", 1)) {
            case 3:
            case 4:
                return 180;
            case 5:
            case 8:
                return 270;
            case 6:
            case 7:
                return 90;
            default:
                return 0;
        }
    }

    @DexIgnore
    public final void r(b bVar) throws IOException {
        p(bVar);
        if (this.f[0].get("JpgFromRaw") != null) {
            j(bVar, this.q, 5);
        }
        c cVar = this.f[0].get("ISO");
        c cVar2 = this.f[1].get("PhotographicSensitivity");
        if (cVar != null && cVar2 == null) {
            this.f[1].put("PhotographicSensitivity", cVar);
        }
    }

    @DexIgnore
    public final void s(b bVar) throws IOException {
        bVar.skipBytes(c0.length);
        byte[] bArr = new byte[bVar.available()];
        bVar.readFully(bArr);
        this.m = c0.length;
        N(bArr, 0);
    }

    @DexIgnore
    public final void t(b bVar) throws IOException {
        if (r) {
            Log.d("ExifInterface", "getWebpAttributes starting with: " + bVar);
        }
        bVar.h(ByteOrder.LITTLE_ENDIAN);
        bVar.skipBytes(E.length);
        int readInt = bVar.readInt() + 8;
        int skipBytes = bVar.skipBytes(F.length) + 8;
        while (true) {
            try {
                byte[] bArr = new byte[4];
                if (bVar.read(bArr) == 4) {
                    int readInt2 = bVar.readInt();
                    int i2 = skipBytes + 4 + 4;
                    if (Arrays.equals(G, bArr)) {
                        byte[] bArr2 = new byte[readInt2];
                        if (bVar.read(bArr2) == readInt2) {
                            this.m = i2;
                            N(bArr2, 0);
                            this.m = i2;
                            return;
                        }
                        throw new IOException("Failed to read given length for given PNG chunk type: " + b(bArr));
                    }
                    if (readInt2 % 2 == 1) {
                        readInt2++;
                    }
                    int i3 = i2 + readInt2;
                    if (i3 == readInt) {
                        return;
                    }
                    if (i3 <= readInt) {
                        int skipBytes2 = bVar.skipBytes(readInt2);
                        if (skipBytes2 == readInt2) {
                            skipBytes = i2 + skipBytes2;
                        } else {
                            throw new IOException("Encountered WebP file with invalid chunk size");
                        }
                    } else {
                        throw new IOException("Encountered WebP file with invalid chunk size");
                    }
                } else {
                    throw new IOException("Encountered invalid length while parsing WebP chunktype");
                }
            } catch (EOFException e2) {
                throw new IOException("Encountered corrupt WebP file.");
            }
        }
    }

    @DexIgnore
    public final void u(b bVar, HashMap hashMap) throws IOException {
        c cVar = (c) hashMap.get("JPEGInterchangeFormat");
        c cVar2 = (c) hashMap.get("JPEGInterchangeFormatLength");
        if (cVar != null && cVar2 != null) {
            int i2 = cVar.i(this.h);
            int i3 = cVar2.i(this.h);
            if (this.d == 7) {
                i2 += this.n;
            }
            int min = Math.min(i3, bVar.a() - i2);
            if (i2 > 0 && min > 0) {
                int i4 = this.m + i2;
                this.j = i4;
                this.k = min;
                if (this.f972a == null && this.c == null && this.b == null) {
                    bVar.f((long) i4);
                    bVar.readFully(new byte[min]);
                }
            }
            if (r) {
                Log.d("ExifInterface", "Setting thumbnail attributes with offset: " + i2 + ", length: " + min);
            }
        }
    }

    @DexIgnore
    public final void v(b bVar, HashMap hashMap) throws IOException {
        c cVar = (c) hashMap.get("StripOffsets");
        c cVar2 = (c) hashMap.get("StripByteCounts");
        if (cVar != null && cVar2 != null) {
            long[] e2 = e(cVar.k(this.h));
            long[] e3 = e(cVar2.k(this.h));
            if (e2 == null || e2.length == 0) {
                Log.w("ExifInterface", "stripOffsets should not be null or have zero length.");
            } else if (e3 == null || e3.length == 0) {
                Log.w("ExifInterface", "stripByteCounts should not be null or have zero length.");
            } else if (e2.length != e3.length) {
                Log.w("ExifInterface", "stripOffsets and stripByteCounts should have same length.");
            } else {
                long j2 = 0;
                for (long j3 : e3) {
                    j2 += j3;
                }
                int i2 = (int) j2;
                byte[] bArr = new byte[i2];
                this.i = true;
                int i3 = 0;
                int i4 = 0;
                for (int i5 = 0; i5 < e2.length; i5++) {
                    int i6 = (int) e2[i5];
                    int i7 = (int) e3[i5];
                    if (i5 < e2.length - 1 && ((long) (i6 + i7)) != e2[i5 + 1]) {
                        this.i = false;
                    }
                    int i8 = i6 - i3;
                    if (i8 < 0) {
                        Log.d("ExifInterface", "Invalid strip offset value");
                    }
                    bVar.f((long) i8);
                    byte[] bArr2 = new byte[i7];
                    bVar.read(bArr2);
                    i3 = i3 + i8 + i7;
                    System.arraycopy(bArr2, 0, bArr, i4, i7);
                    i4 += i7;
                }
                if (this.i) {
                    this.j = ((int) e2[0]) + this.m;
                    this.k = i2;
                }
            }
        }
    }

    @DexIgnore
    public final void w(String str) throws IOException {
        FileInputStream fileInputStream;
        if (str != null) {
            this.c = null;
            this.f972a = str;
            try {
                fileInputStream = new FileInputStream(str);
                try {
                    if (F(fileInputStream.getFD())) {
                        this.b = fileInputStream.getFD();
                    } else {
                        this.b = null;
                    }
                    J(fileInputStream);
                    c(fileInputStream);
                } catch (Throwable th) {
                    th = th;
                    c(fileInputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileInputStream = null;
                c(fileInputStream);
                throw th;
            }
        } else {
            throw new NullPointerException("filename cannot be null");
        }
    }

    @DexIgnore
    public boolean y() {
        int g2 = g("Orientation", 1);
        return g2 == 2 || g2 == 7 || g2 == 4 || g2 == 5;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0099 A[Catch:{ all -> 0x00b1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00ab  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean z(byte[] r11) throws java.io.IOException {
        /*
            r10 = this;
            r1 = 0
            r2 = 0
            com.fossil.eq0$b r4 = new com.fossil.eq0$b     // Catch:{ Exception -> 0x00af, all -> 0x00a6 }
            r4.<init>(r11)     // Catch:{ Exception -> 0x00af, all -> 0x00a6 }
            int r0 = r4.readInt()     // Catch:{ Exception -> 0x0093, all -> 0x00b3 }
            long r2 = (long) r0     // Catch:{ Exception -> 0x0093, all -> 0x00b3 }
            r0 = 4
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0093, all -> 0x00b3 }
            r4.read(r0)     // Catch:{ Exception -> 0x0093, all -> 0x00b3 }
            byte[] r1 = com.fossil.eq0.v     // Catch:{ Exception -> 0x0093, all -> 0x00b3 }
            boolean r0 = java.util.Arrays.equals(r0, r1)     // Catch:{ Exception -> 0x0093, all -> 0x00b3 }
            if (r0 != 0) goto L_0x001f
            r4.close()
            r0 = 0
        L_0x001e:
            return r0
        L_0x001f:
            r0 = 16
            r6 = 1
            int r5 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r5 != 0) goto L_0x0036
            long r2 = r4.readLong()
            r6 = 16
            int r5 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r5 >= 0) goto L_0x0038
            r4.close()
            r0 = 0
            goto L_0x001e
        L_0x0036:
            r0 = 8
        L_0x0038:
            int r5 = r11.length
            long r6 = (long) r5
            int r5 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r5 <= 0) goto L_0x0040
            int r2 = r11.length
            long r2 = (long) r2
        L_0x0040:
            long r6 = r2 - r0
            r0 = 8
            int r0 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x004d
            r4.close()
            r0 = 0
            goto L_0x001e
        L_0x004d:
            r0 = 4
            byte[] r5 = new byte[r0]
            r2 = 0
            r1 = 0
            r0 = 0
        L_0x0054:
            r8 = 4
            long r8 = r6 / r8
            int r8 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r8 >= 0) goto L_0x008e
            int r8 = r4.read(r5)
            r9 = 4
            if (r8 == r9) goto L_0x0068
            r4.close()
            r0 = 0
            goto L_0x001e
        L_0x0068:
            r8 = 1
            int r8 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r8 != 0) goto L_0x0072
        L_0x006e:
            r8 = 1
            long r2 = r2 + r8
            goto L_0x0054
        L_0x0072:
            byte[] r8 = com.fossil.eq0.w
            boolean r8 = java.util.Arrays.equals(r5, r8)
            if (r8 == 0) goto L_0x0084
            r1 = 1
        L_0x007b:
            if (r1 == 0) goto L_0x006e
            if (r0 == 0) goto L_0x006e
            r4.close()
            r0 = 1
            goto L_0x001e
        L_0x0084:
            byte[] r8 = com.fossil.eq0.x
            boolean r8 = java.util.Arrays.equals(r5, r8)
            if (r8 == 0) goto L_0x007b
            r0 = 1
            goto L_0x007b
        L_0x008e:
            r4.close()
        L_0x0091:
            r0 = 0
            goto L_0x001e
        L_0x0093:
            r0 = move-exception
            r1 = r4
        L_0x0095:
            boolean r2 = com.fossil.eq0.r     // Catch:{ all -> 0x00b1 }
            if (r2 == 0) goto L_0x00a0
            java.lang.String r2 = "ExifInterface"
            java.lang.String r3 = "Exception parsing HEIF file type box."
            android.util.Log.d(r2, r3, r0)     // Catch:{ all -> 0x00b1 }
        L_0x00a0:
            if (r1 == 0) goto L_0x0091
            r1.close()
            goto L_0x0091
        L_0x00a6:
            r0 = move-exception
            r1 = r2
        L_0x00a8:
            r4 = r1
        L_0x00a9:
            if (r4 == 0) goto L_0x00ae
            r4.close()
        L_0x00ae:
            throw r0
        L_0x00af:
            r0 = move-exception
            goto L_0x0095
        L_0x00b1:
            r0 = move-exception
            goto L_0x00a8
        L_0x00b3:
            r0 = move-exception
            goto L_0x00a9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.eq0.z(byte[]):boolean");
    }
}
