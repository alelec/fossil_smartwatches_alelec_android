package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.view.ViewConfiguration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class of0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Context f2673a;

    @DexIgnore
    public of0(Context context) {
        this.f2673a = context;
    }

    @DexIgnore
    public static of0 b(Context context) {
        return new of0(context);
    }

    @DexIgnore
    public boolean a() {
        return this.f2673a.getApplicationInfo().targetSdkVersion < 14;
    }

    @DexIgnore
    public int c() {
        return this.f2673a.getResources().getDisplayMetrics().widthPixels / 2;
    }

    @DexIgnore
    public int d() {
        Configuration configuration = this.f2673a.getResources().getConfiguration();
        int i = configuration.screenWidthDp;
        int i2 = configuration.screenHeightDp;
        if (configuration.smallestScreenWidthDp > 600 || i > 600 || ((i > 960 && i2 > 720) || (i > 720 && i2 > 960))) {
            return 5;
        }
        if (i >= 500 || ((i > 640 && i2 > 480) || (i > 480 && i2 > 640))) {
            return 4;
        }
        return i >= 360 ? 3 : 2;
    }

    @DexIgnore
    public int e() {
        return this.f2673a.getResources().getDimensionPixelSize(oe0.abc_action_bar_stacked_tab_max_width);
    }

    @DexIgnore
    public int f() {
        TypedArray obtainStyledAttributes = this.f2673a.obtainStyledAttributes(null, ue0.ActionBar, le0.actionBarStyle, 0);
        int layoutDimension = obtainStyledAttributes.getLayoutDimension(ue0.ActionBar_height, 0);
        Resources resources = this.f2673a.getResources();
        if (!g()) {
            layoutDimension = Math.min(layoutDimension, resources.getDimensionPixelSize(oe0.abc_action_bar_stacked_max_height));
        }
        obtainStyledAttributes.recycle();
        return layoutDimension;
    }

    @DexIgnore
    public boolean g() {
        return this.f2673a.getResources().getBoolean(me0.abc_action_bar_embed_tabs);
    }

    @DexIgnore
    public boolean h() {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return !ViewConfiguration.get(this.f2673a).hasPermanentMenuKey();
    }
}
