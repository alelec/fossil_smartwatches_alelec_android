package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n00 extends qq7 implements rp7<lp, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ e60 b;
    @DexIgnore
    public /* final */ /* synthetic */ lp c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n00(e60 e60, lp lpVar) {
        super(1);
        this.b = e60;
        this.c = lpVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(lp lpVar) {
        lp lpVar2 = lpVar;
        if (!q3.f.b(this.b.u, this.c)) {
            lpVar2.k(zq.REQUEST_UNSUPPORTED);
        }
        return tl7.f3441a;
    }
}
