package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ia3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ia3> CREATOR; // = new ua3();
    @DexIgnore
    public /* final */ List<LocationRequest> b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public sa3 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ArrayList<LocationRequest> f1597a; // = new ArrayList<>();
        @DexIgnore
        public boolean b; // = false;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public final a a(LocationRequest locationRequest) {
            if (locationRequest != null) {
                this.f1597a.add(locationRequest);
            }
            return this;
        }

        @DexIgnore
        public final ia3 b() {
            return new ia3(this.f1597a, this.b, this.c, null);
        }
    }

    @DexIgnore
    public ia3(List<LocationRequest> list, boolean z, boolean z2, sa3 sa3) {
        this.b = list;
        this.c = z;
        this.d = z2;
        this.e = sa3;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.y(parcel, 1, Collections.unmodifiableList(this.b), false);
        bd2.c(parcel, 2, this.c);
        bd2.c(parcel, 3, this.d);
        bd2.t(parcel, 5, this.e, i, false);
        bd2.b(parcel, a2);
    }
}
