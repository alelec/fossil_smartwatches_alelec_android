package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e72 extends UnsupportedOperationException {
    @DexIgnore
    public /* final */ b62 zzbe;

    @DexIgnore
    public e72(b62 b62) {
        this.zzbe = b62;
    }

    @DexIgnore
    public final String getMessage() {
        String valueOf = String.valueOf(this.zzbe);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 8);
        sb.append("Missing ");
        sb.append(valueOf);
        return sb.toString();
    }
}
