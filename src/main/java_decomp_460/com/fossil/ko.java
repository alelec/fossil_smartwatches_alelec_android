package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ko extends zj {
    @DexIgnore
    public long T;
    @DexIgnore
    public long U;
    @DexIgnore
    public String V;
    @DexIgnore
    public String W;
    @DexIgnore
    public zk1 X;
    @DexIgnore
    public int Y;
    @DexIgnore
    public boolean Z;
    @DexIgnore
    public /* final */ String a0;

    @DexIgnore
    public ko(k5 k5Var, i60 i60, byte[] bArr, String str) {
        super(k5Var, i60, yp.h, false, ke.b.b(k5Var.x, ob.OTA), bArr, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.a0 = str;
        String firmwareVersion = i60.a().getFirmwareVersion();
        this.V = firmwareVersion.length() == 0 ? "unknown" : firmwareVersion;
        this.W = "unknown";
        this.X = new zk1(k5Var.C(), k5Var.x, "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
        this.Z = true;
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.ro
    public void B() {
        String firmwareVersion = this.x.a().getFirmwareVersion();
        this.V = firmwareVersion;
        String str = this.a0;
        if (str == null || !vt7.j(firmwareVersion, str, true)) {
            if (q3.f.a()) {
                this.w.y = true;
                a(new qk(this));
            }
            if (q3.f.f(this.x.a())) {
                lp.h(this, new af(this.w, this.x, this.S, false, 23131, 0.001f, this.z), new an(this), new mn(this), new yn(this), null, null, 48, null);
            } else {
                super.B();
            }
        } else {
            this.W = this.V;
            this.X = this.x.a();
            this.Z = false;
            l(nr.a(this.v, null, zq.SUCCESS, null, null, 13));
        }
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.ro, com.fossil.mj
    public JSONObject C() {
        JSONObject C = super.C();
        jd0 jd0 = jd0.j5;
        Object obj = this.a0;
        if (obj == null) {
            obj = JSONObject.NULL;
        }
        return g80.k(C, jd0, obj);
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.ro
    public JSONObject E() {
        return g80.k(g80.k(g80.k(super.E(), jd0.q4, Long.valueOf(Math.max(this.U - this.T, 0L))), jd0.r4, this.V), jd0.s4, this.W);
    }

    @DexIgnore
    @Override // com.fossil.ro
    public mv O() {
        return new jv(this.D, this.w);
    }

    @DexIgnore
    @Override // com.fossil.ro
    public void P() {
        if (q3.f.a()) {
            lp.i(this, new nv(this.w), dl.b, ql.b, null, new cm(this), pm.b, 8, null);
        } else {
            U();
        }
    }

    @DexIgnore
    public final zk1 T() {
        return this.X;
    }

    @DexIgnore
    public final void U() {
        int i = this.Y;
        if (i < 2) {
            this.Y = i + 1;
            HashMap i2 = zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 55000L));
            if (this.T == 0) {
                this.T = System.currentTimeMillis();
            }
            lp.h(this, new fh(this.w, this.x, i2, this.z), new gj(this), new sj(this), null, null, ek.b, 24, null);
            return;
        }
        this.Z = true;
        l(this.v);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void e(f5 f5Var) {
        if (ti.f3417a[f5Var.ordinal()] == 1) {
            fs fsVar = this.b;
            if (fsVar == null || fsVar.t) {
                lp lpVar = this.n;
                if (lpVar == null || lpVar.t) {
                    k(zq.CONNECTION_DROPPED);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        return this.X.getFirmwareVersion();
    }
}
