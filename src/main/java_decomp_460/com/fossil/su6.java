package com.fossil;

import com.portfolio.platform.uirenew.login.LoginActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class su6 implements MembersInjector<LoginActivity> {
    @DexIgnore
    public static void a(LoginActivity loginActivity, un5 un5) {
        loginActivity.A = un5;
    }

    @DexIgnore
    public static void b(LoginActivity loginActivity, vn5 vn5) {
        loginActivity.B = vn5;
    }

    @DexIgnore
    public static void c(LoginActivity loginActivity, wn5 wn5) {
        loginActivity.D = wn5;
    }

    @DexIgnore
    public static void d(LoginActivity loginActivity, xn5 xn5) {
        loginActivity.C = xn5;
    }

    @DexIgnore
    public static void e(LoginActivity loginActivity, av6 av6) {
        loginActivity.E = av6;
    }
}
