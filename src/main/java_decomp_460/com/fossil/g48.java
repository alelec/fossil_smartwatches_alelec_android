package com.fossil;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g48 extends d58 {
    @DexIgnore
    public static /* final */ long h; // = TimeUnit.SECONDS.toMillis(60);
    @DexIgnore
    public static /* final */ long i; // = TimeUnit.MILLISECONDS.toNanos(h);
    @DexIgnore
    public static g48 j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public boolean e;
    @DexIgnore
    public g48 f;
    @DexIgnore
    public long g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final g48 c() throws InterruptedException {
            g48 g48 = g48.j;
            if (g48 != null) {
                g48 g482 = g48.f;
                if (g482 == null) {
                    long nanoTime = System.nanoTime();
                    g48.class.wait(g48.h);
                    g48 g483 = g48.j;
                    if (g483 == null) {
                        pq7.i();
                        throw null;
                    } else if (g483.f != null || System.nanoTime() - nanoTime < g48.i) {
                        return null;
                    } else {
                        return g48.j;
                    }
                } else {
                    long u = g482.u(System.nanoTime());
                    if (u > 0) {
                        long j = u / 1000000;
                        g48.class.wait(j, (int) (u - (1000000 * j)));
                        return null;
                    }
                    g48 g484 = g48.j;
                    if (g484 != null) {
                        g484.f = g482.f;
                        g482.f = null;
                        return g482;
                    }
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        public final boolean d(g48 g48) {
            synchronized (g48.class) {
                try {
                    for (g48 g482 = g48.j; g482 != null; g482 = g482.f) {
                        if (g482.f == g48) {
                            g482.f = g48.f;
                            g48.f = null;
                            return false;
                        }
                    }
                    return true;
                } catch (Throwable th) {
                    throw th;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public final void e(g48 g48, long j, boolean z) {
            synchronized (g48.class) {
                try {
                    if (g48.j == null) {
                        g48.j = new g48();
                        new b().start();
                    }
                    long nanoTime = System.nanoTime();
                    int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
                    if (i != 0 && z) {
                        g48.g = Math.min(j, g48.c() - nanoTime) + nanoTime;
                    } else if (i != 0) {
                        g48.g = j + nanoTime;
                    } else if (z) {
                        g48.g = g48.c();
                    } else {
                        throw new AssertionError();
                    }
                    long u = g48.u(nanoTime);
                    g48 g482 = g48.j;
                    if (g482 != null) {
                        while (g482.f != null) {
                            g48 g483 = g482.f;
                            if (g483 == null) {
                                pq7.i();
                                throw null;
                            } else if (u < g483.u(nanoTime)) {
                                break;
                            } else {
                                g482 = g482.f;
                                if (g482 == null) {
                                    pq7.i();
                                    throw null;
                                }
                            }
                        }
                        g48.f = g482.f;
                        g482.f = g48;
                        if (g482 == g48.j) {
                            g48.class.notify();
                        }
                        tl7 tl7 = tl7.f3441a;
                    } else {
                        pq7.i();
                        throw null;
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Thread {
        @DexIgnore
        public b() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
            if (r0 == null) goto L_0x0000;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x001e, code lost:
            r0.x();
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r2 = this;
            L_0x0000:
                java.lang.Class<com.fossil.g48> r0 = com.fossil.g48.class
                monitor-enter(r0)     // Catch:{ InterruptedException -> 0x0022 }
                com.fossil.g48$a r0 = com.fossil.g48.k     // Catch:{ all -> 0x0024 }
                com.fossil.g48 r0 = r0.c()     // Catch:{ all -> 0x0024 }
                com.fossil.g48 r1 = com.fossil.g48.i()     // Catch:{ all -> 0x0024 }
                if (r0 != r1) goto L_0x0017
                r0 = 0
                com.fossil.g48.o(r0)     // Catch:{ all -> 0x0024 }
                java.lang.Class<com.fossil.g48> r0 = com.fossil.g48.class
                monitor-exit(r0)
                return
            L_0x0017:
                com.fossil.tl7 r1 = com.fossil.tl7.f3441a
                java.lang.Class<com.fossil.g48> r1 = com.fossil.g48.class
                monitor-exit(r1)
                if (r0 == 0) goto L_0x0000
                r0.x()
                goto L_0x0000
            L_0x0022:
                r0 = move-exception
                goto L_0x0000
            L_0x0024:
                r0 = move-exception
                java.lang.Class<com.fossil.g48> r1 = com.fossil.g48.class
                monitor-exit(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.g48.b.run():void");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements a58 {
        @DexIgnore
        public /* final */ /* synthetic */ g48 b;
        @DexIgnore
        public /* final */ /* synthetic */ a58 c;

        @DexIgnore
        public c(g48 g48, a58 a58) {
            this.b = g48;
            this.c = a58;
        }

        @DexIgnore
        @Override // com.fossil.a58
        public void K(i48 i48, long j) {
            long j2;
            pq7.c(i48, "source");
            f48.b(i48.p0(), 0, j);
            for (long j3 = j; j3 > 0; j3 -= j2) {
                x48 x48 = i48.b;
                if (x48 != null) {
                    j2 = 0;
                    while (true) {
                        if (j2 >= ((long) 65536)) {
                            break;
                        }
                        j2 += (long) (x48.c - x48.b);
                        if (j2 >= j3) {
                            j2 = j3;
                            break;
                        }
                        x48 = x48.f;
                        if (x48 == null) {
                            pq7.i();
                            throw null;
                        }
                    }
                    g48 g48 = this.b;
                    g48.r();
                    try {
                        this.c.K(i48, j2);
                        tl7 tl7 = tl7.f3441a;
                        if (g48.s()) {
                            throw g48.m(null);
                        }
                    } catch (IOException e) {
                        if (!g48.s()) {
                            throw e;
                        }
                        throw g48.m(e);
                    } finally {
                        g48.s();
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public g48 e() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.a58, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            g48 g48 = this.b;
            g48.r();
            try {
                this.c.close();
                tl7 tl7 = tl7.f3441a;
                if (g48.s()) {
                    throw g48.m(null);
                }
            } catch (IOException e) {
                if (!g48.s()) {
                    throw e;
                }
                throw g48.m(e);
            } finally {
                g48.s();
            }
        }

        @DexIgnore
        @Override // com.fossil.a58, java.io.Flushable
        public void flush() {
            g48 g48 = this.b;
            g48.r();
            try {
                this.c.flush();
                tl7 tl7 = tl7.f3441a;
                if (g48.s()) {
                    throw g48.m(null);
                }
            } catch (IOException e) {
                if (!g48.s()) {
                    throw e;
                }
                throw g48.m(e);
            } finally {
                g48.s();
            }
        }

        @DexIgnore
        public String toString() {
            return "AsyncTimeout.sink(" + this.c + ')';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements c58 {
        @DexIgnore
        public /* final */ /* synthetic */ g48 b;
        @DexIgnore
        public /* final */ /* synthetic */ c58 c;

        @DexIgnore
        public d(g48 g48, c58 c58) {
            this.b = g48;
            this.c = c58;
        }

        @DexIgnore
        /* renamed from: a */
        public g48 e() {
            return this.b;
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.c58, java.lang.AutoCloseable
        public void close() {
            g48 g48 = this.b;
            g48.r();
            try {
                this.c.close();
                tl7 tl7 = tl7.f3441a;
                if (g48.s()) {
                    throw g48.m(null);
                }
            } catch (IOException e) {
                if (!g48.s()) {
                    throw e;
                }
                throw g48.m(e);
            } finally {
                g48.s();
            }
        }

        @DexIgnore
        @Override // com.fossil.c58
        public long d0(i48 i48, long j) {
            pq7.c(i48, "sink");
            g48 g48 = this.b;
            g48.r();
            try {
                long d0 = this.c.d0(i48, j);
                if (!g48.s()) {
                    return d0;
                }
                throw g48.m(null);
            } catch (IOException e) {
                if (!g48.s()) {
                    throw e;
                }
                throw g48.m(e);
            } finally {
                g48.s();
            }
        }

        @DexIgnore
        public String toString() {
            return "AsyncTimeout.source(" + this.c + ')';
        }
    }

    @DexIgnore
    public final IOException m(IOException iOException) {
        return t(iOException);
    }

    @DexIgnore
    public final void r() {
        if (!this.e) {
            long h2 = h();
            boolean e2 = e();
            if (h2 != 0 || e2) {
                this.e = true;
                k.e(this, h2, e2);
                return;
            }
            return;
        }
        throw new IllegalStateException("Unbalanced enter/exit".toString());
    }

    @DexIgnore
    public final boolean s() {
        if (!this.e) {
            return false;
        }
        this.e = false;
        return k.d(this);
    }

    @DexIgnore
    public IOException t(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    @DexIgnore
    public final long u(long j2) {
        return this.g - j2;
    }

    @DexIgnore
    public final a58 v(a58 a58) {
        pq7.c(a58, "sink");
        return new c(this, a58);
    }

    @DexIgnore
    public final c58 w(c58 c58) {
        pq7.c(c58, "source");
        return new d(this, c58);
    }

    @DexIgnore
    public void x() {
    }
}
