package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fj0<K, V> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public fj0<K, V>.b f1136a;
    @DexIgnore
    public fj0<K, V>.c b;
    @DexIgnore
    public fj0<K, V>.e c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a<T> implements Iterator<T> {
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public boolean e; // = false;

        @DexIgnore
        public a(int i) {
            this.b = i;
            this.c = fj0.this.d();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.d < this.c;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (hasNext()) {
                T t = (T) fj0.this.b(this.d, this.b);
                this.d++;
                this.e = true;
                return t;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            if (this.e) {
                int i = this.d - 1;
                this.d = i;
                this.c--;
                this.e = false;
                fj0.this.h(i);
                return;
            }
            throw new IllegalStateException();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Set<Map.Entry<K, V>> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean a(Map.Entry<K, V> entry) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public /* bridge */ /* synthetic */ boolean add(Object obj) {
            a((Map.Entry) obj);
            throw null;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.fj0 */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // java.util.Collection, java.util.Set
        public boolean addAll(Collection<? extends Map.Entry<K, V>> collection) {
            int d = fj0.this.d();
            Iterator<? extends Map.Entry<K, V>> it = collection.iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                fj0.this.g(entry.getKey(), entry.getValue());
            }
            return d != fj0.this.d();
        }

        @DexIgnore
        public void clear() {
            fj0.this.a();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            int e = fj0.this.e(entry.getKey());
            if (e >= 0) {
                return cj0.c(fj0.this.b(e, 1), entry.getValue());
            }
            return false;
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean containsAll(Collection<?> collection) {
            Iterator<?> it = collection.iterator();
            while (it.hasNext()) {
                if (!contains(it.next())) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return fj0.k(this, obj);
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            for (int d = fj0.this.d() - 1; d >= 0; d--) {
                Object b2 = fj0.this.b(d, 0);
                Object b3 = fj0.this.b(d, 1);
                i = ((b3 == null ? 0 : b3.hashCode()) ^ (b2 == null ? 0 : b2.hashCode())) + i;
            }
            return i;
        }

        @DexIgnore
        public boolean isEmpty() {
            return fj0.this.d() == 0;
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<Map.Entry<K, V>> iterator() {
            return new d();
        }

        @DexIgnore
        public boolean remove(Object obj) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public int size() {
            return fj0.this.d();
        }

        @DexIgnore
        public Object[] toArray() {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public <T> T[] toArray(T[] tArr) {
            throw new UnsupportedOperationException();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements Set<K> {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean add(K k) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean addAll(Collection<? extends K> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public void clear() {
            fj0.this.a();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return fj0.this.e(obj) >= 0;
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean containsAll(Collection<?> collection) {
            return fj0.j(fj0.this.c(), collection);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return fj0.k(this, obj);
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            int d = fj0.this.d() - 1;
            while (d >= 0) {
                Object b2 = fj0.this.b(d, 0);
                d--;
                i = (b2 == null ? 0 : b2.hashCode()) + i;
            }
            return i;
        }

        @DexIgnore
        public boolean isEmpty() {
            return fj0.this.d() == 0;
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<K> iterator() {
            return new a(0);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            int e = fj0.this.e(obj);
            if (e < 0) {
                return false;
            }
            fj0.this.h(e);
            return true;
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean removeAll(Collection<?> collection) {
            return fj0.o(fj0.this.c(), collection);
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean retainAll(Collection<?> collection) {
            return fj0.p(fj0.this.c(), collection);
        }

        @DexIgnore
        public int size() {
            return fj0.this.d();
        }

        @DexIgnore
        public Object[] toArray() {
            return fj0.this.q(0);
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public <T> T[] toArray(T[] tArr) {
            return (T[]) fj0.this.r(tArr, 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements Iterator<Map.Entry<K, V>>, Map.Entry<K, V> {
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d; // = false;

        @DexIgnore
        public d() {
            this.b = fj0.this.d() - 1;
            this.c = -1;
        }

        @DexIgnore
        public Map.Entry<K, V> a() {
            if (hasNext()) {
                this.c++;
                this.d = true;
                return this;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public boolean equals(Object obj) {
            boolean z = true;
            if (!this.d) {
                throw new IllegalStateException("This container does not support retaining Map.Entry objects");
            } else if (!(obj instanceof Map.Entry)) {
                return false;
            } else {
                Map.Entry entry = (Map.Entry) obj;
                if (!cj0.c(entry.getKey(), fj0.this.b(this.c, 0)) || !cj0.c(entry.getValue(), fj0.this.b(this.c, 1))) {
                    z = false;
                }
                return z;
            }
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public K getKey() {
            if (this.d) {
                return (K) fj0.this.b(this.c, 0);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public V getValue() {
            if (this.d) {
                return (V) fj0.this.b(this.c, 1);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public boolean hasNext() {
            return this.c < this.b;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            if (this.d) {
                Object b2 = fj0.this.b(this.c, 0);
                Object b3 = fj0.this.b(this.c, 1);
                int hashCode = b2 == null ? 0 : b2.hashCode();
                if (b3 != null) {
                    i = b3.hashCode();
                }
                return i ^ hashCode;
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        @Override // java.util.Iterator
        public /* bridge */ /* synthetic */ Object next() {
            a();
            return this;
        }

        @DexIgnore
        public void remove() {
            if (this.d) {
                fj0.this.h(this.c);
                this.c--;
                this.b--;
                this.d = false;
                return;
            }
            throw new IllegalStateException();
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public V setValue(V v) {
            if (this.d) {
                return (V) fj0.this.i(this.c, v);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public String toString() {
            return getKey() + SimpleComparison.EQUAL_TO_OPERATION + getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements Collection<V> {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        @Override // java.util.Collection
        public boolean add(V v) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        @Override // java.util.Collection
        public boolean addAll(Collection<? extends V> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public void clear() {
            fj0.this.a();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return fj0.this.f(obj) >= 0;
        }

        @DexIgnore
        @Override // java.util.Collection
        public boolean containsAll(Collection<?> collection) {
            Iterator<?> it = collection.iterator();
            while (it.hasNext()) {
                if (!contains(it.next())) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public boolean isEmpty() {
            return fj0.this.d() == 0;
        }

        @DexIgnore
        @Override // java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            return new a(1);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            int f = fj0.this.f(obj);
            if (f < 0) {
                return false;
            }
            fj0.this.h(f);
            return true;
        }

        @DexIgnore
        @Override // java.util.Collection
        public boolean removeAll(Collection<?> collection) {
            int d = fj0.this.d();
            boolean z = false;
            int i = 0;
            while (i < d) {
                if (collection.contains(fj0.this.b(i, 1))) {
                    fj0.this.h(i);
                    i--;
                    d--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        @DexIgnore
        @Override // java.util.Collection
        public boolean retainAll(Collection<?> collection) {
            int d = fj0.this.d();
            boolean z = false;
            int i = 0;
            while (i < d) {
                if (!collection.contains(fj0.this.b(i, 1))) {
                    fj0.this.h(i);
                    i--;
                    d--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        @DexIgnore
        public int size() {
            return fj0.this.d();
        }

        @DexIgnore
        public Object[] toArray() {
            return fj0.this.q(1);
        }

        @DexIgnore
        @Override // java.util.Collection
        public <T> T[] toArray(T[] tArr) {
            return (T[]) fj0.this.r(tArr, 1);
        }
    }

    @DexIgnore
    public static <K, V> boolean j(Map<K, V> map, Collection<?> collection) {
        Iterator<?> it = collection.iterator();
        while (it.hasNext()) {
            if (!map.containsKey(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static <T> boolean k(Set<T> set, Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set2 = (Set) obj;
            try {
                return set.size() == set2.size() && set.containsAll(set2);
            } catch (ClassCastException | NullPointerException e2) {
            }
        }
        return false;
    }

    @DexIgnore
    public static <K, V> boolean o(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        Iterator<?> it = collection.iterator();
        while (it.hasNext()) {
            map.remove(it.next());
        }
        return size != map.size();
    }

    @DexIgnore
    public static <K, V> boolean p(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        Iterator<K> it = map.keySet().iterator();
        while (it.hasNext()) {
            if (!collection.contains(it.next())) {
                it.remove();
            }
        }
        return size != map.size();
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public abstract Object b(int i, int i2);

    @DexIgnore
    public abstract Map<K, V> c();

    @DexIgnore
    public abstract int d();

    @DexIgnore
    public abstract int e(Object obj);

    @DexIgnore
    public abstract int f(Object obj);

    @DexIgnore
    public abstract void g(K k, V v);

    @DexIgnore
    public abstract void h(int i);

    @DexIgnore
    public abstract V i(int i, V v);

    @DexIgnore
    public Set<Map.Entry<K, V>> l() {
        if (this.f1136a == null) {
            this.f1136a = new b();
        }
        return this.f1136a;
    }

    @DexIgnore
    public Set<K> m() {
        if (this.b == null) {
            this.b = new c();
        }
        return this.b;
    }

    @DexIgnore
    public Collection<V> n() {
        if (this.c == null) {
            this.c = new e();
        }
        return this.c;
    }

    @DexIgnore
    public Object[] q(int i) {
        int d2 = d();
        Object[] objArr = new Object[d2];
        for (int i2 = 0; i2 < d2; i2++) {
            objArr[i2] = b(i2, i);
        }
        return objArr;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v11, resolved type: T[] */
    /* JADX WARN: Multi-variable type inference failed */
    public <T> T[] r(T[] tArr, int i) {
        int d2 = d();
        T[] tArr2 = tArr.length < d2 ? (T[]) ((Object[]) Array.newInstance(tArr.getClass().getComponentType(), d2)) : tArr;
        for (int i2 = 0; i2 < d2; i2++) {
            tArr2[i2] = b(i2, i);
        }
        if (tArr2.length > d2) {
            tArr2[d2] = null;
        }
        return tArr2;
    }
}
