package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wf6 implements Factory<vf6> {
    @DexIgnore
    public static vf6 a(tf6 tf6, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        return new vf6(tf6, userRepository, summariesRepository, portfolioApp);
    }
}
