package com.fossil;

import com.google.gson.JsonElement;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ek4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Writer {
        @DexIgnore
        public /* final */ Appendable b;
        @DexIgnore
        public /* final */ C0066a c; // = new C0066a();

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ek4$a$a")
        /* renamed from: com.fossil.ek4$a$a  reason: collision with other inner class name */
        public static class C0066a implements CharSequence {
            @DexIgnore
            public char[] b;

            @DexIgnore
            public char charAt(int i) {
                return this.b[i];
            }

            @DexIgnore
            public int length() {
                return this.b.length;
            }

            @DexIgnore
            public CharSequence subSequence(int i, int i2) {
                return new String(this.b, i, i2 - i);
            }
        }

        @DexIgnore
        public a(Appendable appendable) {
            this.b = appendable;
        }

        @DexIgnore
        @Override // java.io.Closeable, java.io.Writer, java.lang.AutoCloseable
        public void close() {
        }

        @DexIgnore
        @Override // java.io.Writer, java.io.Flushable
        public void flush() {
        }

        @DexIgnore
        @Override // java.io.Writer
        public void write(int i) throws IOException {
            this.b.append((char) i);
        }

        @DexIgnore
        @Override // java.io.Writer
        public void write(char[] cArr, int i, int i2) throws IOException {
            C0066a aVar = this.c;
            aVar.b = cArr;
            this.b.append(aVar, i, i2 + i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001a, code lost:
        throw new com.fossil.mj4(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0021, code lost:
        throw new com.fossil.ej4(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0022, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0028, code lost:
        throw new com.fossil.mj4(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0014, code lost:
        r0 = move-exception;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001b A[ExcHandler: IOException (r0v2 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0022 A[ExcHandler: ok4 (r0v1 'e' com.fossil.ok4 A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0014 A[ExcHandler: NumberFormatException (r0v3 'e' java.lang.NumberFormatException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.gson.JsonElement a(com.google.gson.stream.JsonReader r3) throws com.fossil.hj4 {
        /*
            r3.V()     // Catch:{ EOFException -> 0x0029, ok4 -> 0x0022, IOException -> 0x001b, NumberFormatException -> 0x0014 }
            r1 = 0
            com.google.gson.TypeAdapter<com.google.gson.JsonElement> r0 = com.google.gson.internal.bind.TypeAdapters.X     // Catch:{ EOFException -> 0x000d, ok4 -> 0x0022, IOException -> 0x001b, NumberFormatException -> 0x0014 }
            java.lang.Object r0 = r0.read(r3)     // Catch:{ EOFException -> 0x000d, ok4 -> 0x0022, IOException -> 0x001b, NumberFormatException -> 0x0014 }
            com.google.gson.JsonElement r0 = (com.google.gson.JsonElement) r0     // Catch:{ EOFException -> 0x000d, ok4 -> 0x0022, IOException -> 0x001b, NumberFormatException -> 0x0014 }
        L_0x000c:
            return r0
        L_0x000d:
            r0 = move-exception
            r2 = r0
        L_0x000f:
            if (r1 == 0) goto L_0x002d
            com.fossil.fj4 r0 = com.fossil.fj4.f1138a
            goto L_0x000c
        L_0x0014:
            r0 = move-exception
            com.fossil.mj4 r1 = new com.fossil.mj4
            r1.<init>(r0)
            throw r1
        L_0x001b:
            r0 = move-exception
            com.fossil.ej4 r1 = new com.fossil.ej4
            r1.<init>(r0)
            throw r1
        L_0x0022:
            r0 = move-exception
            com.fossil.mj4 r1 = new com.fossil.mj4
            r1.<init>(r0)
            throw r1
        L_0x0029:
            r2 = move-exception
            r0 = 1
            r1 = r0
            goto L_0x000f
        L_0x002d:
            com.fossil.mj4 r0 = new com.fossil.mj4
            r0.<init>(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ek4.a(com.google.gson.stream.JsonReader):com.google.gson.JsonElement");
    }

    @DexIgnore
    public static void b(JsonElement jsonElement, JsonWriter jsonWriter) throws IOException {
        TypeAdapters.X.write(jsonWriter, jsonElement);
    }

    @DexIgnore
    public static Writer c(Appendable appendable) {
        return appendable instanceof Writer ? (Writer) appendable : new a(appendable);
    }
}
