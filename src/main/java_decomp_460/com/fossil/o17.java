package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.nk5;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o17 extends k17 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ long u; // = TimeUnit.SECONDS.toMillis(15);
    @DexIgnore
    public static /* final */ long v; // = TimeUnit.SECONDS.toMillis(1);
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public boolean e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = 3;
    @DexIgnore
    public /* final */ e h; // = new e(this, u, v);
    @DexIgnore
    public /* final */ Handler i; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ Runnable k; // = new f(this);
    @DexIgnore
    public /* final */ g l; // = new g(this);
    @DexIgnore
    public /* final */ d m; // = new d(this);
    @DexIgnore
    public /* final */ c n; // = new c(this);
    @DexIgnore
    public /* final */ PortfolioApp o;
    @DexIgnore
    public /* final */ l17 p;
    @DexIgnore
    public /* final */ UserRepository q;
    @DexIgnore
    public /* final */ ct0 r;
    @DexIgnore
    public /* final */ pl5 s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return o17.t;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationPresenter$completeCalibration$1", f = "CalibrationPresenter.kt", l = {383}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ o17 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(o17 o17, qn7 qn7) {
            super(2, qn7);
            this.this$0 = o17;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            pl5 pl5;
            Object currentUser;
            String str;
            String str2;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.h.cancel();
                this.this$0.p.b();
                pl5 = this.this$0.s;
                String J = this.this$0.o.J();
                UserRepository userRepository = this.this$0.q;
                this.L$0 = iv7;
                this.L$1 = pl5;
                this.L$2 = J;
                this.label = 1;
                currentUser = userRepository.getCurrentUser(this);
                if (currentUser == d) {
                    return d;
                }
                str = J;
            } else if (i == 1) {
                pl5 = (pl5) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                str = (String) this.L$2;
                currentUser = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) currentUser;
            if (mFUser == null || (str2 = mFUser.getUserId()) == null) {
                str2 = "";
            }
            pl5.b(str, str2);
            this.this$0.f = 4;
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ o17 f2614a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(o17 o17) {
            this.f2614a = o17;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            pq7.c(intent, "intent");
            if (pq7.a("android.bluetooth.adapter.action.STATE_CHANGED", intent.getAction())) {
                int intExtra = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", RecyclerView.UNDEFINED_DURATION);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = o17.w.a();
                local.d(a2, "mBluetoothStateChangeReceiver - onReceive() - state = " + intExtra);
                if (intExtra == 10) {
                    FLogger.INSTANCE.getLocal().d(o17.w.a(), "Bluetooth off");
                    this.f2614a.m();
                    this.f2614a.p.a();
                    this.f2614a.p.r1();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ o17 f2615a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(o17 o17) {
            this.f2615a = o17;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            pq7.c(intent, "intent");
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = o17.w.a();
            local.d(a2, "mConnectionStateChangeReceiver onReceive: status = " + intExtra);
            if (intExtra == ConnectionStateChange.GATT_OFF.ordinal()) {
                BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
                pq7.b(defaultAdapter, "BluetoothAdapter.getDefaultAdapter()");
                if (!defaultAdapter.isEnabled()) {
                    this.f2615a.p.a();
                    this.f2615a.p.r1();
                    return;
                }
                this.f2615a.m();
                this.f2615a.H(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends CountDownTimer {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ o17 f2616a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(o17 o17, long j, long j2) {
            super(j, j2);
            this.f2616a = o17;
        }

        @DexIgnore
        public void onFinish() {
            FLogger.INSTANCE.getLocal().d(o17.w.a(), "CountDownTimer onFinish");
            this.f2616a.s.e(true, this.f2616a.o.J(), 0, CalibrationEnums.HandId.HOUR);
            cancel();
            start();
        }

        @DexIgnore
        public void onTick(long j) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ o17 b;

        @DexIgnore
        public f(o17 o17) {
            this.b = o17;
        }

        @DexIgnore
        public final void run() {
            if (this.b.j) {
                this.b.p.a();
                this.b.M();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements wq5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ o17 f2617a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g(o17 o17) {
            this.f2617a = o17;
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            boolean z = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal();
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = o17.w.a();
            local.d(a2, "receiver mode=" + communicateMode + ", isSuccess=" + z);
            int i = p17.f2766a[communicateMode.ordinal()];
            if (i == 1) {
                this.f2617a.J(z);
            } else if (i == 2) {
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                this.f2617a.G(z, integerArrayListExtra, intExtra);
            } else if (i == 3) {
                this.f2617a.F(z, intExtra);
            } else if (i == 4) {
                this.f2617a.I(z);
            } else if (i == 5) {
                this.f2617a.H(z);
            }
        }
    }

    /*
    static {
        String simpleName = o17.class.getSimpleName();
        pq7.b(simpleName, "CalibrationPresenter::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public o17(PortfolioApp portfolioApp, l17 l17, UserRepository userRepository, ct0 ct0, pl5 pl5) {
        pq7.c(portfolioApp, "mApp");
        pq7.c(l17, "mView");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(ct0, "mLocalBroadcastManager");
        pq7.c(pl5, "mWatchHelper");
        this.o = portfolioApp;
        this.p = l17;
        this.q = userRepository;
        this.r = ct0;
        this.s = pl5;
    }

    @DexIgnore
    public final void D(String str, String str2, String str3) {
        ck5.f.g().k(str, str2, str3);
    }

    @DexIgnore
    public final void E() {
        FLogger.INSTANCE.getLocal().d(t, "completeCalibration");
        xw7 unused = gu7.d(k(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    public final void F(boolean z, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onApplyHandsComplete isSuccess=" + z);
        if (z) {
            D("device_calibrate_result", "errorCode", "N/A");
            return;
        }
        D("device_calibrate_result", "errorCode", String.valueOf(i2));
        this.p.a();
        M();
    }

    @DexIgnore
    public final void G(boolean z, ArrayList<Integer> arrayList, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.e(str, "onEnterCalibrationComplete isSuccess=" + z + ", lastErrorCode=" + i2);
        if (z) {
            this.s.i(this.o.J());
            return;
        }
        this.p.a();
        if (i2 != 1101 && i2 != 1112 && i2 != 1113) {
            M();
        } else if (arrayList != null) {
            List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(arrayList);
            pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026onErrorCode(errorCodes!!)");
            l17 l17 = this.p;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
            if (array != null) {
                uh5[] uh5Arr = (uh5[]) array;
                l17.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void H(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onExitCalibrationComplete success=" + z);
        this.p.a();
        if (z) {
            this.p.e0();
        } else {
            M();
        }
    }

    @DexIgnore
    public final void I(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onMoveHandComplete isSuccess=" + z);
        if (!z) {
            this.s.a(this.o.J());
            M();
        }
    }

    @DexIgnore
    public final void J(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.e(str, "onResetHandComplete isSuccess=" + z);
        this.p.a();
        if (z) {
            P();
            this.h.start();
            return;
        }
        M();
    }

    @DexIgnore
    public final void K() {
        FLogger.INSTANCE.getLocal().d(t, "registerBroadcastReceiver()");
        wq5.d.e(this.l, CommunicateMode.ENTER_CALIBRATION, CommunicateMode.RESET_HAND, CommunicateMode.MOVE_HAND, CommunicateMode.APPLY_HAND_POSITION, CommunicateMode.EXIT_CALIBRATION);
        ct0 ct0 = this.r;
        d dVar = this.m;
        ct0.c(dVar, new IntentFilter(this.o.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        this.o.registerReceiver(this.n, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
    }

    @DexIgnore
    public void L() {
        this.p.M5(this);
    }

    @DexIgnore
    public final void M() {
        if (!this.e) {
            this.p.i4();
        }
    }

    @DexIgnore
    public final void N() {
        FLogger.INSTANCE.getLocal().d(t, "startCalibration");
        int i2 = this.f;
        if (i2 == 0) {
            this.p.F1(this.o.J());
        } else if (i2 == 1) {
            this.p.I4(this.o.J());
        } else if (i2 == 2) {
            this.p.x1(this.o.J());
        }
        if (!this.e) {
            this.p.b();
            FLogger.INSTANCE.getLocal().e(t, "mStartCalibration");
            this.s.c(this.o.J());
            O(30);
        }
    }

    @DexIgnore
    public final void O(int i2) {
        P();
        this.j = true;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "startSetConfigTimeOutTimer:  timeout = " + i2);
        this.i.postDelayed(this.k, ((long) i2) * 1000);
    }

    @DexIgnore
    public final void P() {
        FLogger.INSTANCE.getLocal().d(t, "stopSetConfigTimeOutTimer");
        this.j = false;
        this.i.removeCallbacks(this.k);
    }

    @DexIgnore
    public final void Q() {
        FLogger.INSTANCE.getLocal().d(t, "unregisterBroadcastReceiver()");
        try {
            wq5.d.j(this.l, CommunicateMode.ENTER_CALIBRATION, CommunicateMode.RESET_HAND, CommunicateMode.MOVE_HAND, CommunicateMode.APPLY_HAND_POSITION, CommunicateMode.EXIT_CALIBRATION);
            this.r.e(this.m);
            this.o.unregisterReceiver(this.n);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = t;
            local.d(str, "unregisterBroadcastReceiver() - ex = " + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(t, "start()");
        Q();
        K();
        N();
        wq5.d.g(CommunicateMode.ENTER_CALIBRATION);
        int i2 = nk5.o.e(this.o.J()) == MFDeviceFamily.DEVICE_FAMILY_SAM ? 3 : 2;
        this.g = i2;
        this.p.L5(this.f, i2);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(t, "stop");
        if (this.f != 3) {
            this.s.a(this.o.J());
        }
        Q();
        this.h.cancel();
        P();
    }

    @DexIgnore
    @Override // com.fossil.k17
    public void n() {
        FLogger.INSTANCE.getLocal().d(t, "back");
        int i2 = this.f;
        if (i2 == 0) {
            this.p.e0();
        } else if (i2 == 1) {
            this.f = 0;
            this.p.F1(this.o.J());
        } else if (i2 == 2) {
            this.f = 1;
            this.p.I4(this.o.J());
        } else if (i2 == 3) {
            if (nk5.o.e(this.o.J()) == MFDeviceFamily.DEVICE_FAMILY_SAM) {
                this.f = 2;
                this.p.x1(this.o.J());
            } else {
                this.f = 1;
                this.p.I4(this.o.J());
            }
        }
        this.p.L5(this.f, this.g);
    }

    @DexIgnore
    @Override // com.fossil.k17
    public List<nk5.b> o() {
        ArrayList arrayList = new ArrayList();
        MFDeviceFamily e2 = nk5.o.e(this.o.J());
        if (e2 == MFDeviceFamily.DEVICE_FAMILY_SAM || e2 == MFDeviceFamily.DEVICE_FAMILY_SAM_SLIM || e2 == MFDeviceFamily.DEVICE_FAMILY_SAM_MINI) {
            arrayList.add(nk5.b.HYBRID_WATCH_HOUR);
            arrayList.add(nk5.b.HYBRID_WATCH_MINUTE);
            arrayList.add(nk5.b.HYBRID_WATCH_SUBEYE);
        } else {
            arrayList.add(nk5.b.DIANA_WATCH_HOUR);
            arrayList.add(nk5.b.DIANA_WATCH_MINUTE);
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.k17
    public void p() {
        FLogger.INSTANCE.getLocal().d(t, "next");
        int i2 = this.f;
        if (i2 == 0) {
            D("device_calibrate", "Step", AppFilter.COLUMN_HOUR);
            this.f = 1;
            this.p.I4(this.o.J());
        } else if (i2 == 1) {
            D("device_calibrate", "Step", MFSleepGoal.COLUMN_MINUTE);
            if (nk5.o.e(this.o.J()) == MFDeviceFamily.DEVICE_FAMILY_SAM) {
                this.f = 2;
                this.p.x1(this.o.J());
            } else {
                this.f = 3;
                E();
            }
        } else if (i2 == 2) {
            D("device_calibrate", "Step", "subeye");
            this.f = 3;
            E();
        } else if (i2 == 3) {
            E();
        }
        this.p.L5(this.f, this.g);
    }

    @DexIgnore
    @Override // com.fossil.k17
    public void q(boolean z) {
        this.e = z;
    }

    @DexIgnore
    @Override // com.fossil.k17
    public void r(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "startMove: clockwise = " + z);
        int i2 = FossilDeviceSerialPatternUtil.isDianaDevice(this.o.J()) ? 1 : 2;
        int i3 = this.f;
        if (i3 == 0) {
            this.s.e(z, this.o.J(), i2, CalibrationEnums.HandId.HOUR);
        } else if (i3 == 1) {
            this.s.e(z, this.o.J(), i2, CalibrationEnums.HandId.MINUTE);
        } else if (i3 == 2) {
            this.s.e(z, this.o.J(), i2, CalibrationEnums.HandId.SUB_EYE);
        }
    }

    @DexIgnore
    @Override // com.fossil.k17
    public void s(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "startSmartMove: clockwise = " + z + ", mCalibrationStep = " + this.f);
        int i2 = this.f;
        if (i2 == 0) {
            this.s.f(z, this.o.J(), 2, CalibrationEnums.HandId.HOUR);
        } else if (i2 == 1) {
            this.s.f(z, this.o.J(), 2, CalibrationEnums.HandId.MINUTE);
        } else if (i2 == 2) {
            this.s.f(z, this.o.J(), 2, CalibrationEnums.HandId.SUB_EYE);
        }
    }

    @DexIgnore
    @Override // com.fossil.k17
    public void t() {
        FLogger.INSTANCE.getLocal().d(t, "stopSmartMove");
        this.s.g();
    }
}
