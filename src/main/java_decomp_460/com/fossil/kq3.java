package com.fossil;

import android.app.job.JobParameters;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface kq3 {
    @DexIgnore
    void a(Intent intent);

    @DexIgnore
    void b(JobParameters jobParameters, boolean z);

    @DexIgnore
    boolean zza(int i);
}
