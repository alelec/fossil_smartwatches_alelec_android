package com.fossil;

import com.fossil.ix1;
import com.fossil.lx1;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ji extends lp {
    @DexIgnore
    public static /* final */ lx1.b I; // = lx1.b.CBC_NO_PADDING;
    @DexIgnore
    public static /* final */ byte[] J; // = new byte[16];
    @DexIgnore
    public static /* final */ ff K; // = new ff(null);
    @DexIgnore
    public byte[] C; // = new byte[8];
    @DexIgnore
    public byte[] D; // = new byte[8];
    @DexIgnore
    public byte[] E; // = new byte[0];
    @DexIgnore
    public /* final */ ArrayList<ow> F; // = by1.a(this.i, hm7.c(ow.AUTHENTICATION));
    @DexIgnore
    public /* final */ rt G;
    @DexIgnore
    public /* final */ byte[] H;

    @DexIgnore
    public ji(k5 k5Var, i60 i60, rt rtVar, byte[] bArr, String str) {
        super(k5Var, i60, yp.S, str, false, 16);
        this.G = rtVar;
        this.H = bArr;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        new SecureRandom().nextBytes(this.C);
        byte[] bArr = this.H;
        if (bArr == null) {
            k(zq.SECRET_KEY_IS_REQUIRED);
        } else if (bArr.length < 16) {
            k(zq.INVALID_PARAMETER);
        } else {
            byte[] copyOf = Arrays.copyOf(bArr, 16);
            pq7.b(copyOf, "java.util.Arrays.copyOf(this, newSize)");
            this.E = copyOf;
            k5 k5Var = this.w;
            rt rtVar = this.G;
            lp.i(this, new ft(k5Var, rtVar, rt.g.a(rtVar, this.C)), new ih(this), new vh(this), null, null, null, 56, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        JSONObject k = g80.k(super.C(), jd0.u2, ey1.a(this.G));
        jd0 jd0 = jd0.t2;
        byte[] bArr = this.H;
        return g80.k(k, jd0, bArr != null ? Long.valueOf(ix1.f1688a.b(bArr, ix1.a.CRC32)) : JSONObject.NULL);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        return g80.k(g80.k(super.E(), jd0.o2, dy1.e(this.C, null, 1, null)), jd0.p2, dy1.e(this.D, null, 1, null));
    }

    @DexIgnore
    public final void H(byte[] bArr) {
        if (bArr.length != 16) {
            l(nr.a(this.v, null, zq.INVALID_DATA_LENGTH, null, null, 13));
            return;
        }
        byte[] a2 = rt.g.a(this.G, lx1.f2275a.a(I, this.E, J, bArr));
        if (a2.length != 16) {
            l(nr.a(this.v, null, zq.INVALID_DATA_LENGTH, null, null, 13));
            return;
        }
        this.D = dm7.k(a2, 0, 8);
        if (Arrays.equals(this.C, dm7.k(a2, 8, 16))) {
            lp.i(this, new et(this.w, this.G, rt.g.a(this.G, lx1.f2275a.b(I, this.E, J, dm7.q(this.C, this.D)))), tf.b, hg.b, null, new vg(this), null, 40, null);
            return;
        }
        l(nr.a(this.v, null, zq.WRONG_RANDOM_NUMBER, null, null, 13));
    }

    @DexIgnore
    public final byte[] I() {
        return this.D;
    }

    @DexIgnore
    public final byte[] J() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.F;
    }
}
