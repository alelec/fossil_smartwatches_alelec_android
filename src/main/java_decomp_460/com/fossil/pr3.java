package com.fossil;

import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pr3 extends zq3 {
    @DexIgnore
    public String d;
    @DexIgnore
    public Set<Integer> e;
    @DexIgnore
    public Map<Integer, rr3> f;
    @DexIgnore
    public Long g;
    @DexIgnore
    public Long h;

    @DexIgnore
    public pr3(yq3 yq3) {
        super(yq3);
    }

    @DexIgnore
    @Override // com.fossil.zq3
    public final boolean t() {
        return false;
    }

    @DexIgnore
    public final rr3 u(int i) {
        if (this.f.containsKey(Integer.valueOf(i))) {
            return this.f.get(Integer.valueOf(i));
        }
        rr3 rr3 = new rr3(this, this.d, null);
        this.f.put(Integer.valueOf(i), rr3);
        return rr3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:201:0x0661, code lost:
        if (r2.E() == false) goto L_0x06d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x0663, code lost:
        r2 = java.lang.Integer.valueOf(r2.G());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:203:0x066b, code lost:
        r5.c("Invalid property filter ID. appId, id", r10, java.lang.String.valueOf(r2));
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:212:0x06d0, code lost:
        r2 = null;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:112:0x036d  */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x0374 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.uu2> v(java.lang.String r25, java.util.List<com.fossil.wu2> r26, java.util.List<com.fossil.ev2> r27, java.lang.Long r28, java.lang.Long r29) {
        /*
        // Method dump skipped, instructions count: 1934
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pr3.v(java.lang.String, java.util.List, java.util.List, java.lang.Long, java.lang.Long):java.util.List");
    }

    @DexIgnore
    public final boolean w(int i, int i2) {
        if (this.f.get(Integer.valueOf(i)) == null) {
            return false;
        }
        return this.f.get(Integer.valueOf(i)).d.get(i2);
    }
}
