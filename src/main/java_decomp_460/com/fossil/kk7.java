package com.fossil;

import com.fossil.gk7;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kk7<K, V> extends gk7<K, V, Provider<V>> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<K, V> extends gk7.a<K, V, Provider<V>> {
        @DexIgnore
        public b(int i) {
            super(i);
        }

        @DexIgnore
        public kk7<K, V> b() {
            return new kk7<>(this.f1322a);
        }

        @DexIgnore
        public b<K, V> c(K k, Provider<V> provider) {
            super.a(k, provider);
            return this;
        }
    }

    @DexIgnore
    public kk7(Map<K, Provider<V>> map) {
        super(map);
    }

    @DexIgnore
    public static <K, V> b<K, V> b(int i) {
        return new b<>(i);
    }

    @DexIgnore
    /* renamed from: c */
    public Map<K, Provider<V>> get() {
        return a();
    }
}
