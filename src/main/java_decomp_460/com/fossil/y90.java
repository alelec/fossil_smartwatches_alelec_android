package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class y90 extends Enum<y90> {
    @DexIgnore
    public static /* final */ /* synthetic */ y90[] d; // = {new y90("PLAY_PAUSE", 0, 205, z90.MEDIA), new y90("VOLUME_UP", 1, 233, z90.MEDIA), new y90("VOLUME_DOWN", 2, 234, z90.MEDIA), new y90("NEXT", 3, 181, z90.MEDIA), new y90("PREVIOUS", 4, 182, z90.MEDIA), new y90("MUTE", 5, 226, z90.MEDIA), new y90("LOW_WHITE", 6, 5, z90.KEYBOARD), new y90("LOW_BLACK", 7, 26, z90.KEYBOARD), new y90("RIGHT", 8, 79, z90.KEYBOARD), new y90("LEFT", 9, 80, z90.KEYBOARD), new y90("PAGE_UP", 10, 75, z90.KEYBOARD), new y90("PAGE_DOWN", 11, 78, z90.KEYBOARD), new y90("ENTER", 12, 88, z90.KEYBOARD)};
    @DexIgnore
    public /* final */ short b;
    @DexIgnore
    public /* final */ z90 c;

    @DexIgnore
    public y90(String str, int i, short s, z90 z90) {
        this.b = (short) s;
        this.c = z90;
    }

    @DexIgnore
    public static y90 valueOf(String str) {
        return (y90) Enum.valueOf(y90.class, str);
    }

    @DexIgnore
    public static y90[] values() {
        return (y90[]) d.clone();
    }
}
