package com.fossil;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.l72;
import com.fossil.m62;
import com.fossil.m62.d;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bb2<O extends m62.d> extends q62<O> {
    @DexIgnore
    public /* final */ m62.f j;
    @DexIgnore
    public /* final */ wa2 k;
    @DexIgnore
    public /* final */ ac2 l;
    @DexIgnore
    public /* final */ m62.a<? extends ys3, gs3> m;

    @DexIgnore
    public bb2(Context context, m62<O> m62, Looper looper, m62.f fVar, wa2 wa2, ac2 ac2, m62.a<? extends ys3, gs3> aVar) {
        super(context, m62, looper);
        this.j = fVar;
        this.k = wa2;
        this.l = ac2;
        this.m = aVar;
        this.i.i(this);
    }

    @DexIgnore
    @Override // com.fossil.q62
    public final m62.f n(Looper looper, l72.a<O> aVar) {
        this.k.a(aVar);
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.q62
    public final x92 p(Context context, Handler handler) {
        return new x92(context, handler, this.l, this.m);
    }

    @DexIgnore
    public final m62.f s() {
        return this.j;
    }
}
