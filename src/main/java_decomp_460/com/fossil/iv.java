package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iv extends fv {
    @DexIgnore
    public ArrayList<j0> X;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ iv(short s, k5 k5Var, int i, int i2) {
        super(iu.d, s, hs.v, k5Var, (i2 & 4) != 0 ? 3 : i);
        this.X = new ArrayList<>();
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        JSONObject A = super.A();
        jd0 jd0 = jd0.w1;
        Object[] array = this.X.toArray(new j0[0]);
        if (array != null) {
            return g80.k(A, jd0, px1.a((ox1[]) array));
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.fv
    public void W() {
        X(this.N);
    }

    @DexIgnore
    public final void X(byte[] bArr) {
        int i = 0;
        while (true) {
            int i2 = i + 10;
            if (i2 <= bArr.length) {
                ByteBuffer order = ByteBuffer.wrap(dm7.k(bArr, i, i2)).order(ByteOrder.LITTLE_ENDIAN);
                this.X.add(new j0(this.y.x, order.getShort(0), hy1.o(order.getInt(2)), hy1.o(order.getInt(6))));
                i = i2;
            } else {
                return;
            }
        }
    }
}
