package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sq2 extends gr2 implements rq2 {
    @DexIgnore
    public sq2() {
        super("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
    }

    @DexIgnore
    @Override // com.fossil.gr2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        M((pq2) qr2.a(parcel, pq2.CREATOR));
        return true;
    }
}
