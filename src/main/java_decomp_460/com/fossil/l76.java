package com.fossil;

import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l76 implements Factory<k76> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<uo5> f2155a;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> b;
    @DexIgnore
    public /* final */ Provider<WatchAppRepository> c;
    @DexIgnore
    public /* final */ Provider<on5> d;

    @DexIgnore
    public l76(Provider<uo5> provider, Provider<DianaAppSettingRepository> provider2, Provider<WatchAppRepository> provider3, Provider<on5> provider4) {
        this.f2155a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static l76 a(Provider<uo5> provider, Provider<DianaAppSettingRepository> provider2, Provider<WatchAppRepository> provider3, Provider<on5> provider4) {
        return new l76(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static k76 c(uo5 uo5, DianaAppSettingRepository dianaAppSettingRepository, WatchAppRepository watchAppRepository, on5 on5) {
        return new k76(uo5, dianaAppSettingRepository, watchAppRepository, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public k76 get() {
        return c(this.f2155a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
