package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class te0 {
    @DexIgnore
    public static /* final */ int AlertDialog_AppCompat; // = 2131951617;
    @DexIgnore
    public static /* final */ int AlertDialog_AppCompat_Light; // = 2131951618;
    @DexIgnore
    public static /* final */ int Animation_AppCompat_Dialog; // = 2131951622;
    @DexIgnore
    public static /* final */ int Animation_AppCompat_DropDownUp; // = 2131951623;
    @DexIgnore
    public static /* final */ int Animation_AppCompat_Tooltip; // = 2131951624;
    @DexIgnore
    public static /* final */ int Base_AlertDialog_AppCompat; // = 2131951681;
    @DexIgnore
    public static /* final */ int Base_AlertDialog_AppCompat_Light; // = 2131951682;
    @DexIgnore
    public static /* final */ int Base_Animation_AppCompat_Dialog; // = 2131951683;
    @DexIgnore
    public static /* final */ int Base_Animation_AppCompat_DropDownUp; // = 2131951684;
    @DexIgnore
    public static /* final */ int Base_Animation_AppCompat_Tooltip; // = 2131951685;
    @DexIgnore
    public static /* final */ int Base_DialogWindowTitleBackground_AppCompat; // = 2131951688;
    @DexIgnore
    public static /* final */ int Base_DialogWindowTitle_AppCompat; // = 2131951687;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat; // = 2131951692;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Body1; // = 2131951693;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Body2; // = 2131951694;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Button; // = 2131951695;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Caption; // = 2131951696;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display1; // = 2131951697;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display2; // = 2131951698;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display3; // = 2131951699;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display4; // = 2131951700;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Headline; // = 2131951701;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Inverse; // = 2131951702;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Large; // = 2131951703;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Large_Inverse; // = 2131951704;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large; // = 2131951705;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small; // = 2131951706;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Medium; // = 2131951707;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Medium_Inverse; // = 2131951708;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Menu; // = 2131951709;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_SearchResult; // = 2131951710;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_SearchResult_Subtitle; // = 2131951711;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_SearchResult_Title; // = 2131951712;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Small; // = 2131951713;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Small_Inverse; // = 2131951714;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Subhead; // = 2131951715;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Subhead_Inverse; // = 2131951716;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Title; // = 2131951717;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Title_Inverse; // = 2131951718;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Tooltip; // = 2131951719;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Menu; // = 2131951720;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle; // = 2131951721;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse; // = 2131951722;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Title; // = 2131951723;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse; // = 2131951724;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle; // = 2131951725;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionMode_Title; // = 2131951726;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button; // = 2131951727;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button_Borderless_Colored; // = 2131951728;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button_Colored; // = 2131951729;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button_Inverse; // = 2131951730;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_DropDownItem; // = 2131951731;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_PopupMenu_Header; // = 2131951732;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_PopupMenu_Large; // = 2131951733;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_PopupMenu_Small; // = 2131951734;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Switch; // = 2131951735;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_TextView_SpinnerItem; // = 2131951736;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item; // = 2131951741;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle; // = 2131951742;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_Widget_AppCompat_Toolbar_Title; // = 2131951743;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat; // = 2131951777;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_ActionBar; // = 2131951778;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dark; // = 2131951779;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dark_ActionBar; // = 2131951780;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dialog; // = 2131951781;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dialog_Alert; // = 2131951782;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Light; // = 2131951783;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat; // = 2131951744;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_CompactMenu; // = 2131951745;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog; // = 2131951746;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_DialogWhenLarge; // = 2131951750;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog_Alert; // = 2131951747;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog_FixedSize; // = 2131951748;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog_MinWidth; // = 2131951749;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light; // = 2131951751;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_DarkActionBar; // = 2131951752;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog; // = 2131951753;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_DialogWhenLarge; // = 2131951757;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog_Alert; // = 2131951754;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog_FixedSize; // = 2131951755;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog_MinWidth; // = 2131951756;
    @DexIgnore
    public static /* final */ int Base_V21_ThemeOverlay_AppCompat_Dialog; // = 2131951803;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat; // = 2131951799;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat_Dialog; // = 2131951800;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat_Light; // = 2131951801;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat_Light_Dialog; // = 2131951802;
    @DexIgnore
    public static /* final */ int Base_V22_Theme_AppCompat; // = 2131951804;
    @DexIgnore
    public static /* final */ int Base_V22_Theme_AppCompat_Light; // = 2131951805;
    @DexIgnore
    public static /* final */ int Base_V23_Theme_AppCompat; // = 2131951806;
    @DexIgnore
    public static /* final */ int Base_V23_Theme_AppCompat_Light; // = 2131951807;
    @DexIgnore
    public static /* final */ int Base_V26_Theme_AppCompat; // = 2131951808;
    @DexIgnore
    public static /* final */ int Base_V26_Theme_AppCompat_Light; // = 2131951809;
    @DexIgnore
    public static /* final */ int Base_V26_Widget_AppCompat_Toolbar; // = 2131951810;
    @DexIgnore
    public static /* final */ int Base_V28_Theme_AppCompat; // = 2131951811;
    @DexIgnore
    public static /* final */ int Base_V28_Theme_AppCompat_Light; // = 2131951812;
    @DexIgnore
    public static /* final */ int Base_V7_ThemeOverlay_AppCompat_Dialog; // = 2131951817;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat; // = 2131951813;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat_Dialog; // = 2131951814;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat_Light; // = 2131951815;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat_Light_Dialog; // = 2131951816;
    @DexIgnore
    public static /* final */ int Base_V7_Widget_AppCompat_AutoCompleteTextView; // = 2131951818;
    @DexIgnore
    public static /* final */ int Base_V7_Widget_AppCompat_EditText; // = 2131951819;
    @DexIgnore
    public static /* final */ int Base_V7_Widget_AppCompat_Toolbar; // = 2131951820;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar; // = 2131951821;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_Solid; // = 2131951822;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_TabBar; // = 2131951823;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_TabText; // = 2131951824;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_TabView; // = 2131951825;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionButton; // = 2131951826;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionButton_CloseMode; // = 2131951827;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionButton_Overflow; // = 2131951828;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionMode; // = 2131951829;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActivityChooserView; // = 2131951830;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_AutoCompleteTextView; // = 2131951831;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button; // = 2131951832;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ButtonBar; // = 2131951838;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ButtonBar_AlertDialog; // = 2131951839;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Borderless; // = 2131951833;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Borderless_Colored; // = 2131951834;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_ButtonBar_AlertDialog; // = 2131951835;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Colored; // = 2131951836;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Small; // = 2131951837;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_CompoundButton_CheckBox; // = 2131951840;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_CompoundButton_RadioButton; // = 2131951841;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_CompoundButton_Switch; // = 2131951842;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_DrawerArrowToggle; // = 2131951843;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_DrawerArrowToggle_Common; // = 2131951844;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_DropDownItem_Spinner; // = 2131951845;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_EditText; // = 2131951846;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ImageButton; // = 2131951847;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar; // = 2131951848;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_Solid; // = 2131951849;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabBar; // = 2131951850;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabText; // = 2131951851;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse; // = 2131951852;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabView; // = 2131951853;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_PopupMenu; // = 2131951854;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_PopupMenu_Overflow; // = 2131951855;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListMenuView; // = 2131951856;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListPopupWindow; // = 2131951857;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListView; // = 2131951858;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListView_DropDown; // = 2131951859;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListView_Menu; // = 2131951860;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_PopupMenu; // = 2131951861;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_PopupMenu_Overflow; // = 2131951862;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_PopupWindow; // = 2131951863;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ProgressBar; // = 2131951864;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ProgressBar_Horizontal; // = 2131951865;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_RatingBar; // = 2131951866;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_RatingBar_Indicator; // = 2131951867;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_RatingBar_Small; // = 2131951868;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SearchView; // = 2131951869;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SearchView_ActionBar; // = 2131951870;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SeekBar; // = 2131951871;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SeekBar_Discrete; // = 2131951872;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Spinner; // = 2131951873;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Spinner_Underlined; // = 2131951874;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_TextView; // = 2131951875;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_TextView_SpinnerItem; // = 2131951876;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Toolbar; // = 2131951877;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Toolbar_Button_Navigation; // = 2131951878;
    @DexIgnore
    public static /* final */ int Platform_AppCompat; // = 2131951998;
    @DexIgnore
    public static /* final */ int Platform_AppCompat_Light; // = 2131951999;
    @DexIgnore
    public static /* final */ int Platform_ThemeOverlay_AppCompat; // = 2131952004;
    @DexIgnore
    public static /* final */ int Platform_ThemeOverlay_AppCompat_Dark; // = 2131952005;
    @DexIgnore
    public static /* final */ int Platform_ThemeOverlay_AppCompat_Light; // = 2131952006;
    @DexIgnore
    public static /* final */ int Platform_V21_AppCompat; // = 2131952007;
    @DexIgnore
    public static /* final */ int Platform_V21_AppCompat_Light; // = 2131952008;
    @DexIgnore
    public static /* final */ int Platform_V25_AppCompat; // = 2131952009;
    @DexIgnore
    public static /* final */ int Platform_V25_AppCompat_Light; // = 2131952010;
    @DexIgnore
    public static /* final */ int Platform_Widget_AppCompat_Spinner; // = 2131952011;
    @DexIgnore
    public static /* final */ int RtlOverlay_DialogWindowTitle_AppCompat; // = 2131952019;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_ActionBar_TitleItem; // = 2131952020;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_DialogTitle_Icon; // = 2131952021;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem; // = 2131952022;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup; // = 2131952023;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_Shortcut; // = 2131952024;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_SubmenuArrow; // = 2131952025;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_Text; // = 2131952026;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_Title; // = 2131952027;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_SearchView_MagIcon; // = 2131952033;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown; // = 2131952028;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1; // = 2131952029;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2; // = 2131952030;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Query; // = 2131952031;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Text; // = 2131952032;
    @DexIgnore
    public static /* final */ int RtlUnderlay_Widget_AppCompat_ActionButton; // = 2131952034;
    @DexIgnore
    public static /* final */ int RtlUnderlay_Widget_AppCompat_ActionButton_Overflow; // = 2131952035;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat; // = 2131952075;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Body1; // = 2131952076;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Body2; // = 2131952077;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Button; // = 2131952078;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Caption; // = 2131952079;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display1; // = 2131952080;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display2; // = 2131952081;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display3; // = 2131952082;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display4; // = 2131952083;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Headline; // = 2131952084;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Inverse; // = 2131952085;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Large; // = 2131952086;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Large_Inverse; // = 2131952087;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_SearchResult_Subtitle; // = 2131952088;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_SearchResult_Title; // = 2131952089;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_Widget_PopupMenu_Large; // = 2131952090;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_Widget_PopupMenu_Small; // = 2131952091;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Medium; // = 2131952092;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Medium_Inverse; // = 2131952093;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Menu; // = 2131952094;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_SearchResult_Subtitle; // = 2131952095;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_SearchResult_Title; // = 2131952096;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Small; // = 2131952097;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Small_Inverse; // = 2131952098;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Subhead; // = 2131952099;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Subhead_Inverse; // = 2131952100;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Title; // = 2131952101;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Title_Inverse; // = 2131952102;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Tooltip; // = 2131952103;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Menu; // = 2131952104;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Subtitle; // = 2131952105;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse; // = 2131952106;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Title; // = 2131952107;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse; // = 2131952108;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Subtitle; // = 2131952109;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse; // = 2131952110;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Title; // = 2131952111;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse; // = 2131952112;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button; // = 2131952113;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button_Borderless_Colored; // = 2131952114;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button_Colored; // = 2131952115;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button_Inverse; // = 2131952116;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_DropDownItem; // = 2131952117;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_PopupMenu_Header; // = 2131952118;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_PopupMenu_Large; // = 2131952119;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_PopupMenu_Small; // = 2131952120;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Switch; // = 2131952121;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_TextView_SpinnerItem; // = 2131952122;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification; // = 2131952123;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Info; // = 2131952124;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Line2; // = 2131952126;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Time; // = 2131952129;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Title; // = 2131952131;
    @DexIgnore
    public static /* final */ int TextAppearance_Widget_AppCompat_ExpandedMenu_Item; // = 2131952156;
    @DexIgnore
    public static /* final */ int TextAppearance_Widget_AppCompat_Toolbar_Subtitle; // = 2131952157;
    @DexIgnore
    public static /* final */ int TextAppearance_Widget_AppCompat_Toolbar_Title; // = 2131952158;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat; // = 2131952245;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_ActionBar; // = 2131952246;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dark; // = 2131952247;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dark_ActionBar; // = 2131952248;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_DayNight; // = 2131952249;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_DayNight_ActionBar; // = 2131952250;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dialog; // = 2131952251;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dialog_Alert; // = 2131952252;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Light; // = 2131952253;
    @DexIgnore
    public static /* final */ int Theme_AppCompat; // = 2131952168;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_CompactMenu; // = 2131952169;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight; // = 2131952170;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_DarkActionBar; // = 2131952171;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_Dialog; // = 2131952172;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_DialogWhenLarge; // = 2131952175;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_Dialog_Alert; // = 2131952173;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_Dialog_MinWidth; // = 2131952174;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_NoActionBar; // = 2131952176;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Dialog; // = 2131952177;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DialogWhenLarge; // = 2131952180;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Dialog_Alert; // = 2131952178;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Dialog_MinWidth; // = 2131952179;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Empty; // = 2131952181;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light; // = 2131952182;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_DarkActionBar; // = 2131952183;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_Dialog; // = 2131952184;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_DialogWhenLarge; // = 2131952187;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_Dialog_Alert; // = 2131952185;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_Dialog_MinWidth; // = 2131952186;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_NoActionBar; // = 2131952188;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_NoActionBar; // = 2131952189;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar; // = 2131952302;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_Solid; // = 2131952303;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_TabBar; // = 2131952304;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_TabText; // = 2131952305;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_TabView; // = 2131952306;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionButton; // = 2131952307;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionButton_CloseMode; // = 2131952308;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionButton_Overflow; // = 2131952309;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionMode; // = 2131952310;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActivityChooserView; // = 2131952311;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_AutoCompleteTextView; // = 2131952312;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button; // = 2131952313;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ButtonBar; // = 2131952319;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ButtonBar_AlertDialog; // = 2131952320;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Borderless; // = 2131952314;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Borderless_Colored; // = 2131952315;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_ButtonBar_AlertDialog; // = 2131952316;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Colored; // = 2131952317;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Small; // = 2131952318;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_CompoundButton_CheckBox; // = 2131952321;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_CompoundButton_RadioButton; // = 2131952322;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_CompoundButton_Switch; // = 2131952323;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_DrawerArrowToggle; // = 2131952324;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_DropDownItem_Spinner; // = 2131952325;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_EditText; // = 2131952326;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ImageButton; // = 2131952327;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar; // = 2131952328;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_Solid; // = 2131952329;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_Solid_Inverse; // = 2131952330;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabBar; // = 2131952331;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabBar_Inverse; // = 2131952332;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabText; // = 2131952333;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabText_Inverse; // = 2131952334;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabView; // = 2131952335;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabView_Inverse; // = 2131952336;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionButton; // = 2131952337;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionButton_CloseMode; // = 2131952338;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionButton_Overflow; // = 2131952339;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionMode_Inverse; // = 2131952340;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActivityChooserView; // = 2131952341;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_AutoCompleteTextView; // = 2131952342;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_DropDownItem_Spinner; // = 2131952343;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ListPopupWindow; // = 2131952344;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ListView_DropDown; // = 2131952345;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_PopupMenu; // = 2131952346;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_PopupMenu_Overflow; // = 2131952347;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_SearchView; // = 2131952348;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_Spinner_DropDown_ActionBar; // = 2131952349;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListMenuView; // = 2131952350;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListPopupWindow; // = 2131952351;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListView; // = 2131952352;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListView_DropDown; // = 2131952353;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListView_Menu; // = 2131952354;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_PopupMenu; // = 2131952355;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_PopupMenu_Overflow; // = 2131952356;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_PopupWindow; // = 2131952357;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ProgressBar; // = 2131952358;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ProgressBar_Horizontal; // = 2131952359;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_RatingBar; // = 2131952360;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_RatingBar_Indicator; // = 2131952361;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_RatingBar_Small; // = 2131952362;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SearchView; // = 2131952363;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SearchView_ActionBar; // = 2131952364;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SeekBar; // = 2131952365;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SeekBar_Discrete; // = 2131952366;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner; // = 2131952367;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner_DropDown; // = 2131952368;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner_DropDown_ActionBar; // = 2131952369;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner_Underlined; // = 2131952370;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_TextView; // = 2131952371;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_TextView_SpinnerItem; // = 2131952372;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Toolbar; // = 2131952373;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Toolbar_Button_Navigation; // = 2131952374;
    @DexIgnore
    public static /* final */ int Widget_Compat_NotificationActionContainer; // = 2131952375;
    @DexIgnore
    public static /* final */ int Widget_Compat_NotificationActionText; // = 2131952376;
}
