package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sl7 extends RuntimeException {
    @DexIgnore
    public sl7() {
    }

    @DexIgnore
    public sl7(String str) {
        super(str);
    }

    @DexIgnore
    public sl7(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public sl7(Throwable th) {
        super(th);
    }
}
