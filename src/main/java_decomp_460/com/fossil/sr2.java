package com.fossil;

import android.os.DeadObjectException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sr2 implements mr2<uq2> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ rr2 f3288a;

    @DexIgnore
    public sr2(rr2 rr2) {
        this.f3288a = rr2;
    }

    @DexIgnore
    @Override // com.fossil.mr2
    public final void a() {
        this.f3288a.A();
    }

    @DexIgnore
    /* Return type fixed from 'android.os.IInterface' to match base method */
    @Override // com.fossil.mr2
    public final /* synthetic */ uq2 getService() throws DeadObjectException {
        return (uq2) this.f3288a.I();
    }
}
