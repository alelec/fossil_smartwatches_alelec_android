package com.fossil;

import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import android.util.Pair;
import com.fossil.wu2;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ur3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public wu2 f3635a;
    @DexIgnore
    public Long b;
    @DexIgnore
    public long c;
    @DexIgnore
    public /* final */ /* synthetic */ pr3 d;

    @DexIgnore
    public ur3(pr3 pr3) {
        this.d = pr3;
    }

    @DexIgnore
    public /* synthetic */ ur3(pr3 pr3, sr3 sr3) {
        this(pr3);
    }

    @DexIgnore
    public final wu2 a(String str, wu2 wu2) {
        ArrayList arrayList;
        String str2;
        Object obj;
        boolean z = true;
        String V = wu2.V();
        List<yu2> D = wu2.D();
        Long l = (Long) this.d.n().U(wu2, "_eid");
        boolean z2 = l != null;
        if (!z2 || !V.equals("_ep")) {
            z = false;
        }
        if (z) {
            String str3 = (String) this.d.n().U(wu2, "_en");
            if (TextUtils.isEmpty(str3)) {
                this.d.d().G().b("Extra parameter without an event name. eventId", l);
                return null;
            }
            if (this.f3635a == null || this.b == null || l.longValue() != this.b.longValue()) {
                Pair<wu2, Long> z3 = this.d.o().z(str, l);
                if (z3 == null || (obj = z3.first) == null) {
                    this.d.d().G().c("Extra parameter without existing main event. eventName, eventId", str3, l);
                    return null;
                }
                this.f3635a = (wu2) obj;
                this.c = ((Long) z3.second).longValue();
                this.b = (Long) this.d.n().U(this.f3635a, "_eid");
            }
            long j = this.c - 1;
            this.c = j;
            if (j <= 0) {
                kg3 o = this.d.o();
                o.h();
                o.d().N().b("Clearing complex main event info. appId", str);
                try {
                    o.v().execSQL("delete from main_event_params where app_id=?", new String[]{str});
                } catch (SQLiteException e) {
                    o.d().F().b("Error clearing complex main event", e);
                }
            } else {
                this.d.o().W(str, l, this.c, this.f3635a);
            }
            ArrayList arrayList2 = new ArrayList();
            for (yu2 yu2 : this.f3635a.D()) {
                this.d.n();
                if (gr3.y(wu2, yu2.O()) == null) {
                    arrayList2.add(yu2);
                }
            }
            if (!arrayList2.isEmpty()) {
                arrayList2.addAll(D);
                arrayList = arrayList2;
                str2 = str3;
            } else {
                this.d.d().G().b("No unique parameters in main event. eventName", str3);
                arrayList = D;
                str2 = str3;
            }
        } else if (z2) {
            this.b = l;
            this.f3635a = wu2;
            long j2 = 0L;
            Object U = this.d.n().U(wu2, "_epc");
            if (U != null) {
                j2 = U;
            }
            long longValue = j2.longValue();
            this.c = longValue;
            if (longValue <= 0) {
                this.d.d().G().b("Complex event with zero extra param count. eventName", V);
                arrayList = D;
                str2 = V;
            } else {
                this.d.o().W(str, l, this.c, wu2);
                arrayList = D;
                str2 = V;
            }
        } else {
            arrayList = D;
            str2 = V;
        }
        wu2.a aVar = (wu2.a) wu2.x();
        aVar.G(str2);
        aVar.N();
        aVar.E(arrayList);
        return (wu2) ((e13) aVar.h());
    }
}
