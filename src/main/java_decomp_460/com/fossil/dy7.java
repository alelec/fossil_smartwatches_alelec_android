package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dy7<T> extends tz7<T> {
    @DexIgnore
    public dy7(tn7 tn7, qn7<? super T> qn7) {
        super(tn7, qn7);
    }

    @DexIgnore
    @Override // com.fossil.tz7, com.fossil.au7
    public void u0(Object obj) {
        Object a2 = wu7.a(obj, this.e);
        tn7 context = this.e.getContext();
        Object c = zz7.c(context, null);
        try {
            this.e.resumeWith(a2);
            tl7 tl7 = tl7.f3441a;
        } finally {
            zz7.a(context, c);
        }
    }
}
