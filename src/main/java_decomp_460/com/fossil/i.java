package com.fossil;

import android.bluetooth.BluetoothDevice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i extends qq7 implements rp7<zk1, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ BluetoothDevice b;
    @DexIgnore
    public /* final */ /* synthetic */ e60 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(BluetoothDevice bluetoothDevice, e60 e60) {
        super(1);
        this.b = bluetoothDevice;
        this.c = e60;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(zk1 zk1) {
        w00 w00 = w00.c;
        String serialNumber = zk1.getSerialNumber();
        String address = this.b.getAddress();
        pq7.b(address, "bluetoothDevice.address");
        w00.d(serialNumber, address);
        tk1 tk1 = tk1.k;
        tk1.d.remove(this.b.getAddress());
        tk1.l(tk1.k, this.c);
        return tl7.f3441a;
    }
}
