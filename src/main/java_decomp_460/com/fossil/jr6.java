package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jr6 implements Factory<ir6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ThemeRepository> f1797a;

    @DexIgnore
    public jr6(Provider<ThemeRepository> provider) {
        this.f1797a = provider;
    }

    @DexIgnore
    public static jr6 a(Provider<ThemeRepository> provider) {
        return new jr6(provider);
    }

    @DexIgnore
    public static ir6 c(ThemeRepository themeRepository) {
        return new ir6(themeRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public ir6 get() {
        return c(this.f1797a.get());
    }
}
