package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ln7 extends kn7 {
    @DexIgnore
    public static final boolean a(int[] iArr, int[] iArr2) {
        pq7.c(iArr, "$this$contentEquals");
        pq7.c(iArr2, FacebookRequestErrorClassification.KEY_OTHER);
        return Arrays.equals(iArr, iArr2);
    }

    @DexIgnore
    public static final boolean b(byte[] bArr, byte[] bArr2) {
        pq7.c(bArr, "$this$contentEquals");
        pq7.c(bArr2, FacebookRequestErrorClassification.KEY_OTHER);
        return Arrays.equals(bArr, bArr2);
    }

    @DexIgnore
    public static final boolean c(short[] sArr, short[] sArr2) {
        pq7.c(sArr, "$this$contentEquals");
        pq7.c(sArr2, FacebookRequestErrorClassification.KEY_OTHER);
        return Arrays.equals(sArr, sArr2);
    }

    @DexIgnore
    public static final boolean d(long[] jArr, long[] jArr2) {
        pq7.c(jArr, "$this$contentEquals");
        pq7.c(jArr2, FacebookRequestErrorClassification.KEY_OTHER);
        return Arrays.equals(jArr, jArr2);
    }
}
