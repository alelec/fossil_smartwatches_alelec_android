package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface or7<R, T> {
    @DexIgnore
    void a(R r, ks7<?> ks7, T t);

    @DexIgnore
    T b(R r, ks7<?> ks7);
}
