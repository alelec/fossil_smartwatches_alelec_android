package com.fossil;

import android.content.Context;
import android.location.Location;
import android.os.RemoteException;
import com.fossil.p72;
import com.google.android.gms.location.LocationRequest;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yq2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ mr2<uq2> f4351a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public /* final */ Map<p72.a<ga3>, dr2> d; // = new HashMap();
    @DexIgnore
    public /* final */ Map<p72.a<Object>, cr2> e; // = new HashMap();
    @DexIgnore
    public /* final */ Map<p72.a<fa3>, zq2> f; // = new HashMap();

    @DexIgnore
    public yq2(Context context, mr2<uq2> mr2) {
        this.b = context;
        this.f4351a = mr2;
    }

    @DexIgnore
    public final Location a() throws RemoteException {
        this.f4351a.a();
        return this.f4351a.getService().zza(this.b.getPackageName());
    }

    @DexIgnore
    public final void b() throws RemoteException {
        synchronized (this.d) {
            for (dr2 dr2 : this.d.values()) {
                if (dr2 != null) {
                    this.f4351a.getService().g2(kr2.f(dr2, null));
                }
            }
            this.d.clear();
        }
        synchronized (this.f) {
            for (zq2 zq2 : this.f.values()) {
                if (zq2 != null) {
                    this.f4351a.getService().g2(kr2.c(zq2, null));
                }
            }
            this.f.clear();
        }
        synchronized (this.e) {
            for (cr2 cr2 : this.e.values()) {
                if (cr2 != null) {
                    this.f4351a.getService().c1(new vr2(2, null, cr2.asBinder(), null));
                }
            }
            this.e.clear();
        }
    }

    @DexIgnore
    public final dr2 c(p72<ga3> p72) {
        dr2 dr2;
        synchronized (this.d) {
            dr2 = this.d.get(p72.b());
            if (dr2 == null) {
                dr2 = new dr2(p72);
            }
            this.d.put(p72.b(), dr2);
        }
        return dr2;
    }

    @DexIgnore
    public final void d(p72.a<ga3> aVar, rq2 rq2) throws RemoteException {
        this.f4351a.a();
        rc2.l(aVar, "Invalid null listener key");
        synchronized (this.d) {
            dr2 remove = this.d.remove(aVar);
            if (remove != null) {
                remove.i();
                this.f4351a.getService().g2(kr2.f(remove, rq2));
            }
        }
    }

    @DexIgnore
    public final void e(ir2 ir2, p72<fa3> p72, rq2 rq2) throws RemoteException {
        this.f4351a.a();
        this.f4351a.getService().g2(new kr2(1, ir2, null, null, h(p72).asBinder(), rq2 != null ? rq2.asBinder() : null));
    }

    @DexIgnore
    public final void f(LocationRequest locationRequest, p72<ga3> p72, rq2 rq2) throws RemoteException {
        this.f4351a.a();
        this.f4351a.getService().g2(new kr2(1, ir2.c(locationRequest), c(p72).asBinder(), null, null, rq2 != null ? rq2.asBinder() : null));
    }

    @DexIgnore
    public final void g(boolean z) throws RemoteException {
        this.f4351a.a();
        this.f4351a.getService().X1(z);
        this.c = z;
    }

    @DexIgnore
    public final zq2 h(p72<fa3> p72) {
        zq2 zq2;
        synchronized (this.f) {
            zq2 = this.f.get(p72.b());
            if (zq2 == null) {
                zq2 = new zq2(p72);
            }
            this.f.put(p72.b(), zq2);
        }
        return zq2;
    }

    @DexIgnore
    public final void i() throws RemoteException {
        if (this.c) {
            g(false);
        }
    }

    @DexIgnore
    public final void j(p72.a<fa3> aVar, rq2 rq2) throws RemoteException {
        this.f4351a.a();
        rc2.l(aVar, "Invalid null listener key");
        synchronized (this.f) {
            zq2 remove = this.f.remove(aVar);
            if (remove != null) {
                remove.i();
                this.f4351a.getService().g2(kr2.c(remove, rq2));
            }
        }
    }
}
