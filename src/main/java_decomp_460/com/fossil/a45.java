package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a45 extends z35 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d G; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray H;
    @DexIgnore
    public long F;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        H = sparseIntArray;
        sparseIntArray.put(2131362666, 1);
        H.put(2131363410, 2);
        H.put(2131362119, 3);
        H.put(2131362763, 4);
        H.put(2131363414, 5);
        H.put(2131362010, 6);
        H.put(2131362073, 7);
        H.put(2131362729, 8);
        H.put(2131363344, 9);
        H.put(2131362004, 10);
        H.put(2131362069, 11);
        H.put(2131362720, 12);
        H.put(2131363339, 13);
        H.put(2131362001, 14);
    }
    */

    @DexIgnore
    public a45(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 15, G, H));
    }

    @DexIgnore
    public a45(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleSwitchCompat) objArr[14], (FlexibleSwitchCompat) objArr[10], (FlexibleSwitchCompat) objArr[6], (ConstraintLayout) objArr[11], (ConstraintLayout) objArr[7], (ConstraintLayout) objArr[3], (RTLImageView) objArr[1], (RTLImageView) objArr[12], (RTLImageView) objArr[8], (RTLImageView) objArr[4], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[5]);
        this.F = -1;
        this.A.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.F = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.F != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.F = 1;
        }
        w();
    }
}
