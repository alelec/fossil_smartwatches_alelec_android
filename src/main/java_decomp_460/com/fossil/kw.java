package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kw {
    @DexIgnore
    public /* synthetic */ kw(kq7 kq7) {
    }

    @DexIgnore
    public final lw a(s5 s5Var) {
        switch (jw.f1819a[s5Var.c.ordinal()]) {
            case 1:
                return lw.b;
            case 2:
                return lw.t;
            case 3:
                return lw.q;
            case 4:
                return lw.g;
            case 5:
                return lw.o;
            case 6:
                return lw.v;
            case 7:
                return lw.w;
            case 8:
                return lw.x;
            case 9:
                return lw.y;
            case 10:
                return lw.m;
            case 11:
                int i = s5Var.d.c;
                return i == x6.d.b ? lw.n : i == x6.e.b ? lw.m : lw.d;
            default:
                return lw.d;
        }
    }
}
