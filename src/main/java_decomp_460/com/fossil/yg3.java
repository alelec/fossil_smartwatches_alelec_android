package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yg3 implements Parcelable.Creator<vg3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ vg3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        long j = 0;
        String str = null;
        ug3 ug3 = null;
        String str2 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 2) {
                str2 = ad2.f(parcel, t);
            } else if (l == 3) {
                ug3 = (ug3) ad2.e(parcel, t, ug3.CREATOR);
            } else if (l == 4) {
                str = ad2.f(parcel, t);
            } else if (l != 5) {
                ad2.B(parcel, t);
            } else {
                j = ad2.y(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new vg3(str2, ug3, str, j);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ vg3[] newArray(int i) {
        return new vg3[i];
    }
}
