package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ms4 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    @rj4("id")
    public String b;
    @DexIgnore
    @rj4("socialId")
    public String c;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_FIRST_NAME)
    public String d;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_LAST_NAME)
    public String e;
    @DexIgnore
    @rj4("isDeleted")
    public Boolean f;
    @DexIgnore
    @rj4("ranking")
    public Integer g;
    @DexIgnore
    @rj4("status")
    public String h;
    @DexIgnore
    @rj4("stepsBase")
    public Integer i;
    @DexIgnore
    @rj4("stepsOffset")
    public Integer j;
    @DexIgnore
    @rj4("caloriesBase")
    public Integer k;
    @DexIgnore
    @rj4("caloriesOffset")
    public Integer l;
    @DexIgnore
    @rj4(Constants.PROFILE_KEY_PROFILE_PIC)
    public String m;
    @DexIgnore
    @rj4("lastSync")
    public Date s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ms4> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        /* renamed from: a */
        public ms4 createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new ms4(parcel);
        }

        @DexIgnore
        /* renamed from: b */
        public ms4[] newArray(int i) {
            return new ms4[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ms4(android.os.Parcel r19) {
        /*
            r18 = this;
            java.lang.String r2 = "parcel"
            r0 = r19
            com.fossil.pq7.c(r0, r2)
            java.lang.String r3 = r19.readString()
            r13 = 0
            if (r3 == 0) goto L_0x00b3
            java.lang.String r2 = "parcel.readString()!!"
            com.fossil.pq7.b(r3, r2)
            java.lang.String r4 = r19.readString()
            if (r4 == 0) goto L_0x00ae
            java.lang.String r2 = "parcel.readString()!!"
            com.fossil.pq7.b(r4, r2)
            java.lang.String r5 = r19.readString()
            java.lang.String r6 = r19.readString()
            java.lang.Class r2 = java.lang.Boolean.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            r0 = r19
            java.lang.Object r7 = r0.readValue(r2)
            boolean r2 = r7 instanceof java.lang.Boolean
            if (r2 != 0) goto L_0x0037
            r7 = 0
        L_0x0037:
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            java.lang.Class r2 = java.lang.Integer.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            r0 = r19
            java.lang.Object r8 = r0.readValue(r2)
            boolean r2 = r8 instanceof java.lang.Integer
            if (r2 != 0) goto L_0x004a
            r8 = 0
        L_0x004a:
            java.lang.Integer r8 = (java.lang.Integer) r8
            java.lang.String r9 = r19.readString()
            java.lang.Class r2 = java.lang.Integer.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            r0 = r19
            java.lang.Object r10 = r0.readValue(r2)
            boolean r2 = r10 instanceof java.lang.Integer
            if (r2 != 0) goto L_0x0061
            r10 = 0
        L_0x0061:
            java.lang.Integer r10 = (java.lang.Integer) r10
            java.lang.Class r2 = java.lang.Integer.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            r0 = r19
            java.lang.Object r11 = r0.readValue(r2)
            boolean r2 = r11 instanceof java.lang.Integer
            if (r2 != 0) goto L_0x0074
            r11 = 0
        L_0x0074:
            java.lang.Integer r11 = (java.lang.Integer) r11
            java.lang.Class r2 = java.lang.Integer.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            r0 = r19
            java.lang.Object r12 = r0.readValue(r2)
            boolean r2 = r12 instanceof java.lang.Integer
            if (r2 != 0) goto L_0x0087
            r12 = 0
        L_0x0087:
            java.lang.Integer r12 = (java.lang.Integer) r12
            java.lang.Class r2 = java.lang.Integer.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            r0 = r19
            java.lang.Object r2 = r0.readValue(r2)
            boolean r14 = r2 instanceof java.lang.Integer
            if (r14 != 0) goto L_0x00b8
        L_0x0099:
            java.lang.Integer r13 = (java.lang.Integer) r13
            java.lang.String r14 = r19.readString()
            java.util.Date r15 = new java.util.Date
            long r16 = r19.readLong()
            r15.<init>(r16)
            r2 = r18
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            return
        L_0x00ae:
            com.fossil.pq7.i()
            r2 = 0
            throw r2
        L_0x00b3:
            com.fossil.pq7.i()
            r2 = 0
            throw r2
        L_0x00b8:
            r13 = r2
            goto L_0x0099
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ms4.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public ms4(String str, String str2, String str3, String str4, Boolean bool, Integer num, String str5, Integer num2, Integer num3, Integer num4, Integer num5, String str6, Date date) {
        pq7.c(str, "id");
        pq7.c(str2, "socialId");
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = bool;
        this.g = num;
        this.h = str5;
        this.i = num2;
        this.j = num3;
        this.k = num4;
        this.l = num5;
        this.m = str6;
        this.s = date;
    }

    @DexIgnore
    public final Integer a() {
        return this.k;
    }

    @DexIgnore
    public final Integer b() {
        return this.l;
    }

    @DexIgnore
    public final String c() {
        return this.d;
    }

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ms4) {
                ms4 ms4 = (ms4) obj;
                if (!pq7.a(this.b, ms4.b) || !pq7.a(this.c, ms4.c) || !pq7.a(this.d, ms4.d) || !pq7.a(this.e, ms4.e) || !pq7.a(this.f, ms4.f) || !pq7.a(this.g, ms4.g) || !pq7.a(this.h, ms4.h) || !pq7.a(this.i, ms4.i) || !pq7.a(this.j, ms4.j) || !pq7.a(this.k, ms4.k) || !pq7.a(this.l, ms4.l) || !pq7.a(this.m, ms4.m) || !pq7.a(this.s, ms4.s)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Date f() {
        return this.s;
    }

    @DexIgnore
    public final String g() {
        return this.m;
    }

    @DexIgnore
    public final Integer h() {
        return this.g;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        String str = this.b;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.c;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.d;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.e;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        Boolean bool = this.f;
        int hashCode5 = bool != null ? bool.hashCode() : 0;
        Integer num = this.g;
        int hashCode6 = num != null ? num.hashCode() : 0;
        String str5 = this.h;
        int hashCode7 = str5 != null ? str5.hashCode() : 0;
        Integer num2 = this.i;
        int hashCode8 = num2 != null ? num2.hashCode() : 0;
        Integer num3 = this.j;
        int hashCode9 = num3 != null ? num3.hashCode() : 0;
        Integer num4 = this.k;
        int hashCode10 = num4 != null ? num4.hashCode() : 0;
        Integer num5 = this.l;
        int hashCode11 = num5 != null ? num5.hashCode() : 0;
        String str6 = this.m;
        int hashCode12 = str6 != null ? str6.hashCode() : 0;
        Date date = this.s;
        if (date != null) {
            i2 = date.hashCode();
        }
        return (((((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + hashCode10) * 31) + hashCode11) * 31) + hashCode12) * 31) + i2;
    }

    @DexIgnore
    public final String i() {
        return this.c;
    }

    @DexIgnore
    public final String k() {
        return this.h;
    }

    @DexIgnore
    public final Integer m() {
        return this.i;
    }

    @DexIgnore
    public final Integer n() {
        return this.j;
    }

    @DexIgnore
    public final Boolean p() {
        return this.f;
    }

    @DexIgnore
    public String toString() {
        return "BCPlayer(id=" + this.b + ", socialId=" + this.c + ", firstName=" + this.d + ", lastName=" + this.e + ", isDeleted=" + this.f + ", ranking=" + this.g + ", status=" + this.h + ", stepsBase=" + this.i + ", stepsOffset=" + this.j + ", caloriesBase=" + this.k + ", caloriesOffset=" + this.l + ", profilePicture=" + this.m + ", lastSync=" + this.s + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        pq7.c(parcel, "parcel");
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeValue(this.f);
        parcel.writeValue(this.g);
        parcel.writeValue(this.h);
        parcel.writeValue(this.i);
        parcel.writeValue(this.j);
        parcel.writeValue(this.k);
        parcel.writeValue(this.l);
        parcel.writeString(this.m);
        Date date = this.s;
        if (date != null) {
            parcel.writeLong(date.getTime());
        }
    }
}
