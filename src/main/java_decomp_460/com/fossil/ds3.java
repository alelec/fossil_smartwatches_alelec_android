package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ds3 extends y93 implements cs3 {
    @DexIgnore
    public ds3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.plus.internal.IPlusOneButtonCreator");
    }

    @DexIgnore
    @Override // com.fossil.cs3
    public final rg2 y(rg2 rg2, int i, int i2, String str, int i3) throws RemoteException {
        Parcel d = d();
        z93.a(d, rg2);
        d.writeInt(i);
        d.writeInt(i2);
        d.writeString(str);
        d.writeInt(i3);
        Parcel e = e(1, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }
}
