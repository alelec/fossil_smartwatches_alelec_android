package com.fossil;

import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface c88<T> {
    @DexIgnore
    void onFailure(Call<T> call, Throwable th);

    @DexIgnore
    void onResponse(Call<T> call, q88<T> q88);
}
