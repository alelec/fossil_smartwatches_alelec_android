package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iq1 extends jq1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<iq1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public iq1 createFromParcel(Parcel parcel) {
            return new iq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public iq1[] newArray(int i) {
            return new iq1[i];
        }
    }

    @DexIgnore
    public iq1(byte b, int i, String str) {
        super(np1.COMMUTE_TIME_WATCH_APP, b, i);
        this.e = str;
    }

    @DexIgnore
    public /* synthetic */ iq1(Parcel parcel, kq7 kq7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.e = readString;
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(iq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.e, ((iq1) obj).e) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.CommuteTimeWatchAppRequest");
    }

    @DexIgnore
    public final String getDestination() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public int hashCode() {
        return (super.hashCode() * 31) + this.e.hashCode();
    }
}
