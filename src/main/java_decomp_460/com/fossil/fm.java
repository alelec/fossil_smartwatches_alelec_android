package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fm extends ro {
    @DexIgnore
    public /* final */ boolean S;
    @DexIgnore
    public zm1[] T;
    @DexIgnore
    public /* final */ ym1[] U;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ fm(k5 k5Var, i60 i60, ym1[] ym1Arr, short s, String str, int i) {
        super(k5Var, i60, yp.v, true, (i & 8) != 0 ? ke.b.b(k5Var.x, ob.DEVICE_CONFIG) : s, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 16) != 0 ? e.a("UUID.randomUUID().toString()") : str, false, 160);
        this.U = ym1Arr;
        this.S = true;
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.ro, com.fossil.mj
    public JSONObject C() {
        return g80.k(super.C(), jd0.R, px1.a(this.U));
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.ro
    public JSONObject E() {
        JSONObject E = super.E();
        jd0 jd0 = jd0.S;
        zm1[] zm1Arr = this.T;
        return g80.k(E, jd0, zm1Arr != null ? g80.h(zm1Arr) : null);
    }

    @DexIgnore
    @Override // com.fossil.ro
    public byte[] M() {
        try {
            ya yaVar = ya.f;
            short s = this.D;
            ry1 ry1 = this.x.a().h().get(Short.valueOf(ob.DEVICE_CONFIG.b));
            if (ry1 == null) {
                ry1 = hd0.y.d();
            }
            return yaVar.a(s, ry1, this.U);
        } catch (sx1 e) {
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // com.fossil.ro
    public void P() {
        ym1[] ym1Arr = this.U;
        ArrayList arrayList = new ArrayList(ym1Arr.length);
        for (ym1 ym1 : ym1Arr) {
            arrayList.add(ym1.getKey());
        }
        Object[] array = arrayList.toArray(new zm1[0]);
        if (array != null) {
            zm1[] zm1Arr = (zm1[]) array;
            this.T = zm1Arr;
            ky1 ky1 = ky1.DEBUG;
            if (zm1Arr != null) {
                pq7.b(Arrays.toString(zm1Arr), "java.util.Arrays.toString(this)");
            }
            super.P();
            return;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.lp
    public boolean t() {
        return this.S;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        zm1[] zm1Arr = this.T;
        return zm1Arr != null ? zm1Arr : new zm1[0];
    }
}
