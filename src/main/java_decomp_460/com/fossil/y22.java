package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.j32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class y22 implements j32.d {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ SQLiteDatabase f4236a;

    @DexIgnore
    public y22(SQLiteDatabase sQLiteDatabase) {
        this.f4236a = sQLiteDatabase;
    }

    @DexIgnore
    public static j32.d b(SQLiteDatabase sQLiteDatabase) {
        return new y22(sQLiteDatabase);
    }

    @DexIgnore
    @Override // com.fossil.j32.d
    public Object a() {
        return this.f4236a.beginTransaction();
    }
}
