package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.service.MFDeviceService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zq5 implements MembersInjector<MFDeviceService> {
    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, ActivitiesRepository activitiesRepository) {
        mFDeviceService.e = activitiesRepository;
    }

    @DexIgnore
    public static void b(MFDeviceService mFDeviceService, ck5 ck5) {
        mFDeviceService.w = ck5;
    }

    @DexIgnore
    public static void c(MFDeviceService mFDeviceService, PortfolioApp portfolioApp) {
        mFDeviceService.x = portfolioApp;
    }

    @DexIgnore
    public static void d(MFDeviceService mFDeviceService, no4 no4) {
        mFDeviceService.v = no4;
    }

    @DexIgnore
    public static void e(MFDeviceService mFDeviceService, dr5 dr5) {
        mFDeviceService.E = dr5;
    }

    @DexIgnore
    public static void f(MFDeviceService mFDeviceService, DeviceRepository deviceRepository) {
        mFDeviceService.d = deviceRepository;
    }

    @DexIgnore
    public static void g(MFDeviceService mFDeviceService, s27 s27) {
        mFDeviceService.A = s27;
    }

    @DexIgnore
    public static void h(MFDeviceService mFDeviceService, FitnessDataRepository fitnessDataRepository) {
        mFDeviceService.t = fitnessDataRepository;
    }

    @DexIgnore
    public static void i(MFDeviceService mFDeviceService, sk5 sk5) {
        mFDeviceService.C = sk5;
    }

    @DexIgnore
    public static void j(MFDeviceService mFDeviceService, GoalTrackingRepository goalTrackingRepository) {
        mFDeviceService.u = goalTrackingRepository;
    }

    @DexIgnore
    public static void k(MFDeviceService mFDeviceService, HeartRateSampleRepository heartRateSampleRepository) {
        mFDeviceService.l = heartRateSampleRepository;
    }

    @DexIgnore
    public static void l(MFDeviceService mFDeviceService, HeartRateSummaryRepository heartRateSummaryRepository) {
        mFDeviceService.m = heartRateSummaryRepository;
    }

    @DexIgnore
    public static void m(MFDeviceService mFDeviceService, hn5 hn5) {
        mFDeviceService.y = hn5;
    }

    @DexIgnore
    public static void n(MFDeviceService mFDeviceService, MicroAppRepository microAppRepository) {
        mFDeviceService.k = microAppRepository;
    }

    @DexIgnore
    public static void o(MFDeviceService mFDeviceService, or5 or5) {
        mFDeviceService.G = or5;
    }

    @DexIgnore
    public static void p(MFDeviceService mFDeviceService, HybridPresetRepository hybridPresetRepository) {
        mFDeviceService.j = hybridPresetRepository;
    }

    @DexIgnore
    public static void q(MFDeviceService mFDeviceService, on5 on5) {
        mFDeviceService.c = on5;
    }

    @DexIgnore
    public static void r(MFDeviceService mFDeviceService, SleepSessionsRepository sleepSessionsRepository) {
        mFDeviceService.g = sleepSessionsRepository;
    }

    @DexIgnore
    public static void s(MFDeviceService mFDeviceService, SleepSummariesRepository sleepSummariesRepository) {
        mFDeviceService.h = sleepSummariesRepository;
    }

    @DexIgnore
    public static void t(MFDeviceService mFDeviceService, SummariesRepository summariesRepository) {
        mFDeviceService.f = summariesRepository;
    }

    @DexIgnore
    public static void u(MFDeviceService mFDeviceService, ThirdPartyRepository thirdPartyRepository) {
        mFDeviceService.z = thirdPartyRepository;
    }

    @DexIgnore
    public static void v(MFDeviceService mFDeviceService, UserRepository userRepository) {
        mFDeviceService.i = userRepository;
    }

    @DexIgnore
    public static void w(MFDeviceService mFDeviceService, d37 d37) {
        mFDeviceService.B = d37;
    }

    @DexIgnore
    public static void x(MFDeviceService mFDeviceService, rl5 rl5) {
        mFDeviceService.D = rl5;
    }

    @DexIgnore
    public static void y(MFDeviceService mFDeviceService, WorkoutSessionRepository workoutSessionRepository) {
        mFDeviceService.s = workoutSessionRepository;
    }

    @DexIgnore
    public static void z(MFDeviceService mFDeviceService, fs5 fs5) {
        mFDeviceService.F = fs5;
    }
}
