package com.fossil;

import androidx.lifecycle.LiveData;
import com.fossil.fl5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class we6 extends te6 {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<ActivitySummary> f;
    @DexIgnore
    public ai5 g;
    @DexIgnore
    public /* final */ ue6 h;
    @DexIgnore
    public /* final */ SummariesRepository i;
    @DexIgnore
    public /* final */ FitnessDataRepository j;
    @DexIgnore
    public /* final */ UserRepository k;
    @DexIgnore
    public /* final */ no4 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$initDataSource$1", f = "DashboardActivityPresenter.kt", l = {85, 93}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ we6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.we6$a$a")
        /* renamed from: com.fossil.we6$a$a  reason: collision with other inner class name */
        public static final class C0269a implements fl5.a {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ a f3926a;

            @DexIgnore
            public C0269a(a aVar) {
                this.f3926a = aVar;
            }

            @DexIgnore
            @Override // com.fossil.fl5.a
            public final void e(fl5.g gVar) {
                pq7.c(gVar, "report");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DashboardActivityPresenter", "XXX-  onStatusChange status=" + gVar);
                if (gVar.b()) {
                    this.f3926a.this$0.h.d();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements ls0<cu0<ActivitySummary>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ a f3927a;

            @DexIgnore
            public b(MFUser mFUser, a aVar) {
                this.f3927a = aVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(cu0<ActivitySummary> cu0) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("XXX-  getSummariesPaging observer size=");
                sb.append(cu0 != null ? Integer.valueOf(cu0.size()) : null);
                local.d("DashboardActivityPresenter", sb.toString());
                if (cu0 != null) {
                    this.f3927a.this$0.h.q(cu0);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$initDataSource$1$user$1", f = "DashboardActivityPresenter.kt", l = {85}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, qn7);
                cVar.p$ = (iv7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.k;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(we6 we6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = we6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00d3  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 344
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.we6.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$start$1", f = "DashboardActivityPresenter.kt", l = {42}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ we6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$start$1$currentUser$1", f = "DashboardActivityPresenter.kt", l = {42}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.k;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(we6 we6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = we6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(i2, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) g;
            if (mFUser != null) {
                if (this.this$0.g == null) {
                    this.this$0.g = ai5.fromString(mFUser.getUnitGroup().getDistance());
                } else {
                    ai5 fromString = ai5.fromString(mFUser.getUnitGroup().getDistance());
                    pq7.b(fromString, "Unit.fromString(distance)");
                    if (this.this$0.g != fromString) {
                        this.this$0.g = fromString;
                        this.this$0.h.I1(fromString);
                    }
                }
            }
            Boolean p0 = lk5.p0(this.this$0.e);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardActivityPresenter", "start isDateTodayDate " + p0 + " listingPage " + this.this$0.f);
            if (!p0.booleanValue()) {
                this.this$0.e = new Date();
                Listing listing = this.this$0.f;
                if (listing != null) {
                    listing.getRefresh();
                }
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public we6(ue6 ue6, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, no4 no4) {
        pq7.c(ue6, "mView");
        pq7.c(summariesRepository, "mSummariesRepository");
        pq7.c(fitnessDataRepository, "mFitnessDataRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(no4, "mAppExecutors");
        this.h = ue6;
        this.i = summariesRepository;
        this.j = fitnessDataRepository;
        this.k = userRepository;
        this.l = no4;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.h0.c().J());
    }

    @DexIgnore
    public void C() {
        this.h.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        xw7 unused = gu7.d(k(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("DashboardActivityPresenter", "stop");
    }

    @DexIgnore
    @Override // com.fossil.te6
    public void n() {
        xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.te6
    public void o() {
        LiveData<cu0<ActivitySummary>> pagedList;
        try {
            FLogger.INSTANCE.getLocal().d("DashboardActivityPresenter", "remove data source observer");
            Listing<ActivitySummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                ue6 ue6 = this.h;
                if (ue6 != null) {
                    pagedList.n((ve6) ue6);
                } else {
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment");
                }
            }
            this.i.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.e("DashboardActivityPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.te6
    public void p() {
        gp7<tl7> retry;
        FLogger.INSTANCE.getLocal().d("DashboardActivityPresenter", "retry all failed request");
        Listing<ActivitySummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }
}
