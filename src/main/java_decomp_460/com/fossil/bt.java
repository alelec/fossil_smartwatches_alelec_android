package com.fossil;

import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bt extends ps {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ n6 I;
    @DexIgnore
    public /* final */ n6 J;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ bt(k5 k5Var, ut utVar, hs hsVar, int i, int i2) {
        super(hsVar, k5Var, (i2 & 8) != 0 ? 3 : i);
        this.G = utVar.a();
        byte[] array = ByteBuffer.allocate(2).put(utVar.d.a()).put(utVar.e.b).array();
        pq7.b(array, "ByteBuffer.allocate(2)\n \u2026\n                .array()");
        this.H = array;
        n6 n6Var = n6.AUTHENTICATION;
        this.I = n6Var;
        this.J = n6Var;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final mt E(byte b) {
        return wt.g.a(b);
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final n6 K() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final byte[] M() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final n6 N() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final byte[] P() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public final long a(o7 o7Var) {
        return 0;
    }
}
