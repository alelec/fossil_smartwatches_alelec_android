package com.fossil;

import com.facebook.internal.FileLruCache;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w94 implements Closeable {
    @DexIgnore
    public static /* final */ Logger h; // = Logger.getLogger(w94.class.getName());
    @DexIgnore
    public /* final */ RandomAccessFile b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public b e;
    @DexIgnore
    public b f;
    @DexIgnore
    public /* final */ byte[] g; // = new byte[16];

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f3904a; // = true;
        @DexIgnore
        public /* final */ /* synthetic */ StringBuilder b;

        @DexIgnore
        public a(StringBuilder sb) {
            this.b = sb;
        }

        @DexIgnore
        @Override // com.fossil.w94.d
        public void a(InputStream inputStream, int i) throws IOException {
            if (this.f3904a) {
                this.f3904a = false;
            } else {
                this.b.append(", ");
            }
            this.b.append(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static /* final */ b c; // = new b(0, 0);

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f3905a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(int i, int i2) {
            this.f3905a = i;
            this.b = i2;
        }

        @DexIgnore
        public String toString() {
            return b.class.getSimpleName() + "[position = " + this.f3905a + ", length = " + this.b + "]";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends InputStream {
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;

        @DexIgnore
        public c(b bVar) {
            this.b = w94.this.V(bVar.f3905a + 4);
            this.c = bVar.b;
        }

        @DexIgnore
        public /* synthetic */ c(w94 w94, b bVar, a aVar) {
            this(bVar);
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read() throws IOException {
            if (this.c == 0) {
                return -1;
            }
            w94.this.b.seek((long) this.b);
            int read = w94.this.b.read();
            this.b = w94.this.V(this.b + 1);
            this.c--;
            return read;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) throws IOException {
            w94.B(bArr, FileLruCache.BufferFile.FILE_NAME_PREFIX);
            if ((i | i2) < 0 || i2 > bArr.length - i) {
                throw new ArrayIndexOutOfBoundsException();
            }
            int i3 = this.c;
            if (i3 <= 0) {
                return -1;
            }
            if (i2 > i3) {
                i2 = i3;
            }
            w94.this.P(this.b, bArr, i, i2);
            this.b = w94.this.V(this.b + i2);
            this.c -= i2;
            return i2;
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(InputStream inputStream, int i) throws IOException;
    }

    @DexIgnore
    public w94(File file) throws IOException {
        if (!file.exists()) {
            o(file);
        }
        this.b = C(file);
        F();
    }

    @DexIgnore
    public static <T> T B(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    @DexIgnore
    public static RandomAccessFile C(File file) throws FileNotFoundException {
        return new RandomAccessFile(file, "rwd");
    }

    @DexIgnore
    public static int G(byte[] bArr, int i) {
        return ((bArr[i] & 255) << 24) + ((bArr[i + 1] & 255) << 16) + ((bArr[i + 2] & 255) << 8) + (bArr[i + 3] & 255);
    }

    @DexIgnore
    public static void b0(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) ((byte) (i2 >> 24));
        bArr[i + 1] = (byte) ((byte) (i2 >> 16));
        bArr[i + 2] = (byte) ((byte) (i2 >> 8));
        bArr[i + 3] = (byte) ((byte) i2);
    }

    @DexIgnore
    public static void g0(byte[] bArr, int... iArr) {
        int i = 0;
        for (int i2 : iArr) {
            b0(bArr, i, i2);
            i += 4;
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static void o(File file) throws IOException {
        File file2 = new File(file.getPath() + ".tmp");
        RandomAccessFile C = C(file2);
        try {
            C.setLength(4096);
            C.seek(0);
            byte[] bArr = new byte[16];
            g0(bArr, 4096, 0, 0, 0);
            C.write(bArr);
            C.close();
            if (!file2.renameTo(file)) {
                throw new IOException("Rename failed!");
            }
        } catch (Throwable th) {
            C.close();
            throw th;
        }
    }

    @DexIgnore
    public boolean A() {
        boolean z;
        synchronized (this) {
            z = this.d == 0;
        }
        return z;
    }

    @DexIgnore
    public final b D(int i) throws IOException {
        if (i == 0) {
            return b.c;
        }
        this.b.seek((long) i);
        return new b(i, this.b.readInt());
    }

    @DexIgnore
    public final void F() throws IOException {
        this.b.seek(0);
        this.b.readFully(this.g);
        int G = G(this.g, 0);
        this.c = G;
        if (((long) G) <= this.b.length()) {
            this.d = G(this.g, 4);
            int G2 = G(this.g, 8);
            int G3 = G(this.g, 12);
            this.e = D(G2);
            this.f = D(G3);
            return;
        }
        throw new IOException("File is truncated. Expected length: " + this.c + ", Actual length: " + this.b.length());
    }

    @DexIgnore
    public final int L() {
        return this.c - T();
    }

    @DexIgnore
    public void M() throws IOException {
        synchronized (this) {
            if (A()) {
                throw new NoSuchElementException();
            } else if (this.d == 1) {
                k();
            } else {
                int V = V(this.e.f3905a + 4 + this.e.b);
                P(V, this.g, 0, 4);
                int G = G(this.g, 0);
                X(this.c, this.d - 1, V, this.f.f3905a);
                this.d--;
                this.e = new b(V, G);
            }
        }
    }

    @DexIgnore
    public final void P(int i, byte[] bArr, int i2, int i3) throws IOException {
        int V = V(i);
        int i4 = this.c;
        if (V + i3 <= i4) {
            this.b.seek((long) V);
            this.b.readFully(bArr, i2, i3);
            return;
        }
        int i5 = i4 - V;
        this.b.seek((long) V);
        this.b.readFully(bArr, i2, i5);
        this.b.seek(16);
        this.b.readFully(bArr, i2 + i5, i3 - i5);
    }

    @DexIgnore
    public final void Q(int i, byte[] bArr, int i2, int i3) throws IOException {
        int V = V(i);
        int i4 = this.c;
        if (V + i3 <= i4) {
            this.b.seek((long) V);
            this.b.write(bArr, i2, i3);
            return;
        }
        int i5 = i4 - V;
        this.b.seek((long) V);
        this.b.write(bArr, i2, i5);
        this.b.seek(16);
        this.b.write(bArr, i2 + i5, i3 - i5);
    }

    @DexIgnore
    public final void S(int i) throws IOException {
        this.b.setLength((long) i);
        this.b.getChannel().force(true);
    }

    @DexIgnore
    public int T() {
        if (this.d == 0) {
            return 16;
        }
        b bVar = this.f;
        int i = bVar.f3905a;
        int i2 = this.e.f3905a;
        if (i >= i2) {
            return bVar.b + (i - i2) + 4 + 16;
        }
        return ((bVar.b + (i + 4)) + this.c) - i2;
    }

    @DexIgnore
    public final int V(int i) {
        int i2 = this.c;
        return i < i2 ? i : (i + 16) - i2;
    }

    @DexIgnore
    public final void X(int i, int i2, int i3, int i4) throws IOException {
        g0(this.g, i, i2, i3, i4);
        this.b.seek(0);
        this.b.write(this.g);
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        synchronized (this) {
            this.b.close();
        }
    }

    @DexIgnore
    public void h(byte[] bArr) throws IOException {
        j(bArr, 0, bArr.length);
    }

    @DexIgnore
    public void j(byte[] bArr, int i, int i2) throws IOException {
        synchronized (this) {
            B(bArr, FileLruCache.BufferFile.FILE_NAME_PREFIX);
            if ((i | i2) < 0 || i2 > bArr.length - i) {
                throw new IndexOutOfBoundsException();
            }
            l(i2);
            boolean A = A();
            b bVar = new b(A ? 16 : V(this.f.f3905a + 4 + this.f.b), i2);
            b0(this.g, 0, i2);
            Q(bVar.f3905a, this.g, 0, 4);
            Q(bVar.f3905a + 4, bArr, i, i2);
            X(this.c, this.d + 1, A ? bVar.f3905a : this.e.f3905a, bVar.f3905a);
            this.f = bVar;
            this.d++;
            if (A) {
                this.e = bVar;
            }
        }
    }

    @DexIgnore
    public void k() throws IOException {
        synchronized (this) {
            X(4096, 0, 0, 0);
            this.d = 0;
            this.e = b.c;
            this.f = b.c;
            if (this.c > 4096) {
                S(4096);
            }
            this.c = 4096;
        }
    }

    @DexIgnore
    public final void l(int i) throws IOException {
        int i2;
        int i3 = i + 4;
        int L = L();
        if (L < i3) {
            int i4 = this.c;
            while (true) {
                L += i4;
                i2 = i4 << 1;
                if (L >= i3) {
                    break;
                }
                i4 = i2;
            }
            S(i2);
            b bVar = this.f;
            int V = V(bVar.b + bVar.f3905a + 4);
            if (V < this.e.f3905a) {
                FileChannel channel = this.b.getChannel();
                channel.position((long) this.c);
                long j = (long) (V - 4);
                if (channel.transferTo(16, j, channel) != j) {
                    throw new AssertionError("Copied insufficient number of bytes!");
                }
            }
            int i5 = this.f.f3905a;
            int i6 = this.e.f3905a;
            if (i5 < i6) {
                int i7 = (i5 + this.c) - 16;
                X(i2, this.d, i6, i7);
                this.f = new b(i7, this.f.b);
            } else {
                X(i2, this.d, i6, i5);
            }
            this.c = i2;
        }
    }

    @DexIgnore
    public void m(d dVar) throws IOException {
        synchronized (this) {
            int i = this.e.f3905a;
            for (int i2 = 0; i2 < this.d; i2++) {
                b D = D(i);
                dVar.a(new c(this, D, null), D.b);
                i = V(D.b + D.f3905a + 4);
            }
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(w94.class.getSimpleName());
        sb.append('[');
        sb.append("fileLength=");
        sb.append(this.c);
        sb.append(", size=");
        sb.append(this.d);
        sb.append(", first=");
        sb.append(this.e);
        sb.append(", last=");
        sb.append(this.f);
        sb.append(", element lengths=[");
        try {
            m(new a(sb));
        } catch (IOException e2) {
            h.log(Level.WARNING, "read error", (Throwable) e2);
        }
        sb.append("]]");
        return sb.toString();
    }
}
