package com.fossil;

import com.fossil.dl7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ov7 {
    @DexIgnore
    public static final String a(Object obj) {
        return obj.getClass().getSimpleName();
    }

    @DexIgnore
    public static final String b(Object obj) {
        return Integer.toHexString(System.identityHashCode(obj));
    }

    @DexIgnore
    public static final String c(qn7<?> qn7) {
        String r0;
        if (qn7 instanceof vv7) {
            return qn7.toString();
        }
        try {
            dl7.a aVar = dl7.Companion;
            r0 = dl7.m1constructorimpl(qn7 + '@' + b(qn7));
        } catch (Throwable th) {
            dl7.a aVar2 = dl7.Companion;
            r0 = dl7.m1constructorimpl(el7.a(th));
        }
        if (dl7.m4exceptionOrNullimpl(r0) != null) {
            r0 = qn7.getClass().getName() + '@' + b(qn7);
        }
        return (String) r0;
    }
}
