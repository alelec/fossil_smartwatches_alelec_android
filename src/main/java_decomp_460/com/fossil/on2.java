package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.m62;
import com.fossil.r62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class on2 extends ip2<lo2> {
    @DexIgnore
    public static /* final */ ep2 E; // = ep2.FIT_SESSIONS;
    @DexIgnore
    public static /* final */ m62.g<on2> F; // = new m62.g<>();
    @DexIgnore
    public static /* final */ m62<m62.d.C0151d> G; // = new m62<>("Fitness.SESSIONS_API", new pn2(), F);
    @DexIgnore
    public static /* final */ m62<m62.d.b> H; // = new m62<>("Fitness.SESSIONS_CLIENT", new rn2(), F);

    @DexIgnore
    public on2(Context context, Looper looper, ac2 ac2, r62.b bVar, r62.c cVar) {
        super(context, looper, E, bVar, cVar, ac2);
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final String p() {
        return "com.google.android.gms.fitness.internal.IGoogleFitSessionsApi";
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final /* synthetic */ IInterface q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitSessionsApi");
        return queryLocalInterface instanceof lo2 ? (lo2) queryLocalInterface : new ko2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.m62.f, com.fossil.ec2, com.fossil.yb2
    public final int s() {
        return h62.f1430a;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final String x() {
        return "com.google.android.gms.fitness.SessionsApi";
    }
}
