package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e63 implements xw2<d63> {
    @DexIgnore
    public static e63 c; // = new e63();
    @DexIgnore
    public /* final */ xw2<d63> b;

    @DexIgnore
    public e63() {
        this(ww2.b(new g63()));
    }

    @DexIgnore
    public e63(xw2<d63> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((d63) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((d63) c.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((d63) c.zza()).zzc();
    }

    @DexIgnore
    public static boolean d() {
        return ((d63) c.zza()).zzd();
    }

    @DexIgnore
    public static boolean e() {
        return ((d63) c.zza()).zze();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ d63 zza() {
        return this.b.zza();
    }
}
