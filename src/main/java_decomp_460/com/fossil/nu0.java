package com.fossil;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class nu0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ViewGroup f2568a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Deprecated
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public float f2569a; // = -1.0f;
        @DexIgnore
        public float b; // = -1.0f;
        @DexIgnore
        public float c; // = -1.0f;
        @DexIgnore
        public float d; // = -1.0f;
        @DexIgnore
        public float e; // = -1.0f;
        @DexIgnore
        public float f; // = -1.0f;
        @DexIgnore
        public float g; // = -1.0f;
        @DexIgnore
        public float h; // = -1.0f;
        @DexIgnore
        public float i;
        @DexIgnore
        public /* final */ c j; // = new c(0, 0);

        @DexIgnore
        public void a(ViewGroup.LayoutParams layoutParams, int i2, int i3) {
            boolean z = false;
            c cVar = this.j;
            int i4 = layoutParams.width;
            ((ViewGroup.MarginLayoutParams) cVar).width = i4;
            ((ViewGroup.MarginLayoutParams) cVar).height = layoutParams.height;
            boolean z2 = (cVar.b || i4 == 0) && this.f2569a < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            c cVar2 = this.j;
            if ((cVar2.f2570a || ((ViewGroup.MarginLayoutParams) cVar2).height == 0) && this.b < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z = true;
            }
            float f2 = this.f2569a;
            if (f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                layoutParams.width = Math.round(f2 * ((float) i2));
            }
            float f3 = this.b;
            if (f3 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                layoutParams.height = Math.round(f3 * ((float) i3));
            }
            float f4 = this.i;
            if (f4 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                if (z2) {
                    layoutParams.width = Math.round(((float) layoutParams.height) * f4);
                    this.j.b = true;
                }
                if (z) {
                    layoutParams.height = Math.round(((float) layoutParams.width) / this.i);
                    this.j.f2570a = true;
                }
            }
        }

        @DexIgnore
        public void b(View view, ViewGroup.MarginLayoutParams marginLayoutParams, int i2, int i3) {
            boolean z = true;
            a(marginLayoutParams, i2, i3);
            c cVar = this.j;
            ((ViewGroup.MarginLayoutParams) cVar).leftMargin = marginLayoutParams.leftMargin;
            ((ViewGroup.MarginLayoutParams) cVar).topMargin = marginLayoutParams.topMargin;
            ((ViewGroup.MarginLayoutParams) cVar).rightMargin = marginLayoutParams.rightMargin;
            ((ViewGroup.MarginLayoutParams) cVar).bottomMargin = marginLayoutParams.bottomMargin;
            zn0.e(cVar, zn0.b(marginLayoutParams));
            zn0.d(this.j, zn0.a(marginLayoutParams));
            float f2 = this.c;
            if (f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.leftMargin = Math.round(f2 * ((float) i2));
            }
            float f3 = this.d;
            if (f3 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.topMargin = Math.round(f3 * ((float) i3));
            }
            float f4 = this.e;
            if (f4 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.rightMargin = Math.round(f4 * ((float) i2));
            }
            float f5 = this.f;
            if (f5 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.bottomMargin = Math.round(f5 * ((float) i3));
            }
            boolean z2 = false;
            float f6 = this.g;
            if (f6 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                zn0.e(marginLayoutParams, Math.round(((float) i2) * f6));
                z2 = true;
            }
            float f7 = this.h;
            if (f7 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                zn0.d(marginLayoutParams, Math.round(((float) i2) * f7));
            } else {
                z = z2;
            }
            if (z && view != null) {
                zn0.c(marginLayoutParams, mo0.z(view));
            }
        }

        @DexIgnore
        public void c(ViewGroup.LayoutParams layoutParams) {
            c cVar = this.j;
            if (!cVar.b) {
                layoutParams.width = ((ViewGroup.MarginLayoutParams) cVar).width;
            }
            c cVar2 = this.j;
            if (!cVar2.f2570a) {
                layoutParams.height = ((ViewGroup.MarginLayoutParams) cVar2).height;
            }
            c cVar3 = this.j;
            cVar3.b = false;
            cVar3.f2570a = false;
        }

        @DexIgnore
        public void d(ViewGroup.MarginLayoutParams marginLayoutParams) {
            c(marginLayoutParams);
            c cVar = this.j;
            marginLayoutParams.leftMargin = ((ViewGroup.MarginLayoutParams) cVar).leftMargin;
            marginLayoutParams.topMargin = ((ViewGroup.MarginLayoutParams) cVar).topMargin;
            marginLayoutParams.rightMargin = ((ViewGroup.MarginLayoutParams) cVar).rightMargin;
            marginLayoutParams.bottomMargin = ((ViewGroup.MarginLayoutParams) cVar).bottomMargin;
            zn0.e(marginLayoutParams, zn0.b(cVar));
            zn0.d(marginLayoutParams, zn0.a(this.j));
        }

        @DexIgnore
        public String toString() {
            return String.format("PercentLayoutInformation width: %f height %f, margins (%f, %f,  %f, %f, %f, %f)", Float.valueOf(this.f2569a), Float.valueOf(this.b), Float.valueOf(this.c), Float.valueOf(this.d), Float.valueOf(this.e), Float.valueOf(this.f), Float.valueOf(this.g), Float.valueOf(this.h));
        }
    }

    @DexIgnore
    @Deprecated
    public interface b {
        @DexIgnore
        a a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends ViewGroup.MarginLayoutParams {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f2570a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public c(int i, int i2) {
            super(i, i2);
        }
    }

    @DexIgnore
    public nu0(ViewGroup viewGroup) {
        if (viewGroup != null) {
            this.f2568a = viewGroup;
            return;
        }
        throw new IllegalArgumentException("host must be non-null");
    }

    @DexIgnore
    public static void b(ViewGroup.LayoutParams layoutParams, TypedArray typedArray, int i, int i2) {
        layoutParams.width = typedArray.getLayoutDimension(i, 0);
        layoutParams.height = typedArray.getLayoutDimension(i2, 0);
    }

    @DexIgnore
    public static a c(Context context, AttributeSet attributeSet) {
        a aVar;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, mu0.PercentLayout_Layout);
        float fraction = obtainStyledAttributes.getFraction(mu0.PercentLayout_Layout_layout_widthPercent, 1, 1, -1.0f);
        if (fraction != -1.0f) {
            aVar = new a();
            aVar.f2569a = fraction;
        } else {
            aVar = null;
        }
        float fraction2 = obtainStyledAttributes.getFraction(mu0.PercentLayout_Layout_layout_heightPercent, 1, 1, -1.0f);
        if (fraction2 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.b = fraction2;
        }
        float fraction3 = obtainStyledAttributes.getFraction(mu0.PercentLayout_Layout_layout_marginPercent, 1, 1, -1.0f);
        if (fraction3 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.c = fraction3;
            aVar.d = fraction3;
            aVar.e = fraction3;
            aVar.f = fraction3;
        }
        float fraction4 = obtainStyledAttributes.getFraction(mu0.PercentLayout_Layout_layout_marginLeftPercent, 1, 1, -1.0f);
        if (fraction4 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.c = fraction4;
        }
        float fraction5 = obtainStyledAttributes.getFraction(mu0.PercentLayout_Layout_layout_marginTopPercent, 1, 1, -1.0f);
        if (fraction5 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.d = fraction5;
        }
        float fraction6 = obtainStyledAttributes.getFraction(mu0.PercentLayout_Layout_layout_marginRightPercent, 1, 1, -1.0f);
        if (fraction6 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.e = fraction6;
        }
        float fraction7 = obtainStyledAttributes.getFraction(mu0.PercentLayout_Layout_layout_marginBottomPercent, 1, 1, -1.0f);
        if (fraction7 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.f = fraction7;
        }
        float fraction8 = obtainStyledAttributes.getFraction(mu0.PercentLayout_Layout_layout_marginStartPercent, 1, 1, -1.0f);
        if (fraction8 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.g = fraction8;
        }
        float fraction9 = obtainStyledAttributes.getFraction(mu0.PercentLayout_Layout_layout_marginEndPercent, 1, 1, -1.0f);
        if (fraction9 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.h = fraction9;
        }
        float fraction10 = obtainStyledAttributes.getFraction(mu0.PercentLayout_Layout_layout_aspectRatio, 1, 1, -1.0f);
        if (fraction10 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.i = fraction10;
        }
        obtainStyledAttributes.recycle();
        return aVar;
    }

    @DexIgnore
    public static boolean f(View view, a aVar) {
        return (view.getMeasuredHeightAndState() & -16777216) == 16777216 && aVar.b >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && ((ViewGroup.MarginLayoutParams) aVar.j).height == -2;
    }

    @DexIgnore
    public static boolean g(View view, a aVar) {
        return (view.getMeasuredWidthAndState() & -16777216) == 16777216 && aVar.f2569a >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && ((ViewGroup.MarginLayoutParams) aVar.j).width == -2;
    }

    @DexIgnore
    public void a(int i, int i2) {
        a a2;
        int size = (View.MeasureSpec.getSize(i) - this.f2568a.getPaddingLeft()) - this.f2568a.getPaddingRight();
        int size2 = (View.MeasureSpec.getSize(i2) - this.f2568a.getPaddingTop()) - this.f2568a.getPaddingBottom();
        int childCount = this.f2568a.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = this.f2568a.getChildAt(i3);
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if ((layoutParams instanceof b) && (a2 = ((b) layoutParams).a()) != null) {
                if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                    a2.b(childAt, (ViewGroup.MarginLayoutParams) layoutParams, size, size2);
                } else {
                    a2.a(layoutParams, size, size2);
                }
            }
        }
    }

    @DexIgnore
    public boolean d() {
        a a2;
        boolean z;
        int childCount = this.f2568a.getChildCount();
        boolean z2 = false;
        for (int i = 0; i < childCount; i++) {
            View childAt = this.f2568a.getChildAt(i);
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if ((layoutParams instanceof b) && (a2 = ((b) layoutParams).a()) != null) {
                if (g(childAt, a2)) {
                    layoutParams.width = -2;
                    z = true;
                } else {
                    z = z2;
                }
                if (f(childAt, a2)) {
                    layoutParams.height = -2;
                    z2 = true;
                } else {
                    z2 = z;
                }
            }
        }
        return z2;
    }

    @DexIgnore
    public void e() {
        a a2;
        int childCount = this.f2568a.getChildCount();
        for (int i = 0; i < childCount; i++) {
            ViewGroup.LayoutParams layoutParams = this.f2568a.getChildAt(i).getLayoutParams();
            if ((layoutParams instanceof b) && (a2 = ((b) layoutParams).a()) != null) {
                if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                    a2.d((ViewGroup.MarginLayoutParams) layoutParams);
                } else {
                    a2.c(layoutParams);
                }
            }
        }
    }
}
