package com.fossil;

import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.r62;
import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ip2<T extends IInterface> extends ec2<T> {
    @DexIgnore
    public ip2(Context context, Looper looper, ep2 ep2, r62.b bVar, r62.c cVar, ac2 ac2) {
        super(context, looper, ep2.zzc(), ac2, bVar, cVar);
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public boolean Q() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.m62.f, com.fossil.ec2
    public Set<Scope> h() {
        return H();
    }

    @DexIgnore
    @Override // com.fossil.ec2
    public Set<Scope> p0(Set<Scope> set) {
        return lj2.a(set);
    }

    @DexIgnore
    @Override // com.fossil.m62.f, com.fossil.yb2
    public boolean v() {
        return !if2.b(E());
    }
}
