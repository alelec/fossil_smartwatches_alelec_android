package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b81 implements g81 {
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore
    public b81(Context context) {
        pq7.c(context, "context");
        this.b = context;
    }

    @DexIgnore
    @Override // com.fossil.g81
    public Object b(qn7<? super f81> qn7) {
        Resources resources = this.b.getResources();
        pq7.b(resources, "context.resources");
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        return new c81(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }
}
