package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pp4 implements Factory<lo4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f2854a;

    @DexIgnore
    public pp4(uo4 uo4) {
        this.f2854a = uo4;
    }

    @DexIgnore
    public static pp4 a(uo4 uo4) {
        return new pp4(uo4);
    }

    @DexIgnore
    public static lo4 c(uo4 uo4) {
        lo4 w = uo4.w();
        lk7.c(w, "Cannot return null from a non-@Nullable @Provides method");
        return w;
    }

    @DexIgnore
    /* renamed from: b */
    public lo4 get() {
        return c(this.f2854a);
    }
}
