package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.j32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class s22 implements j32.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ j32 f3198a;
    @DexIgnore
    public /* final */ h02 b;

    @DexIgnore
    public s22(j32 j32, h02 h02) {
        this.f3198a = j32;
        this.b = h02;
    }

    @DexIgnore
    public static j32.b a(j32 j32, h02 h02) {
        return new s22(j32, h02);
    }

    @DexIgnore
    @Override // com.fossil.j32.b
    public Object apply(Object obj) {
        return j32.Q(this.f3198a, this.b, (SQLiteDatabase) obj);
    }
}
