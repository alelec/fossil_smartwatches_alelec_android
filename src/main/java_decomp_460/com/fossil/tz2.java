package com.fossil;

import java.io.IOException;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tz2 {
    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3, sz2 sz2) throws l13 {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                return k(bArr, i2, sz2);
            }
            if (i4 == 1) {
                return i2 + 8;
            }
            if (i4 == 2) {
                return i(bArr, i2, sz2) + sz2.f3339a;
            }
            if (i4 == 3) {
                int i5 = (i & -8) | 4;
                int i6 = 0;
                int i7 = i2;
                while (i7 < i3) {
                    i7 = i(bArr, i7, sz2);
                    i6 = sz2.f3339a;
                    if (i6 == i5) {
                        break;
                    }
                    i7 = a(i6, bArr, i7, i3, sz2);
                }
                if (i7 <= i3 && i6 == i5) {
                    return i7;
                }
                throw l13.zzg();
            } else if (i4 == 5) {
                return i2 + 4;
            } else {
                throw l13.zzd();
            }
        } else {
            throw l13.zzd();
        }
    }

    @DexIgnore
    public static int b(int i, byte[] bArr, int i2, int i3, m13<?> m13, sz2 sz2) {
        f13 f13 = (f13) m13;
        int i4 = i(bArr, i2, sz2);
        f13.d(sz2.f3339a);
        while (i4 < i3) {
            int i5 = i(bArr, i4, sz2);
            if (i != sz2.f3339a) {
                break;
            }
            i4 = i(bArr, i5, sz2);
            f13.d(sz2.f3339a);
        }
        return i4;
    }

    @DexIgnore
    public static int c(int i, byte[] bArr, int i2, int i3, w33 w33, sz2 sz2) throws l13 {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                int k = k(bArr, i2, sz2);
                w33.c(i, Long.valueOf(sz2.b));
                return k;
            } else if (i4 == 1) {
                w33.c(i, Long.valueOf(l(bArr, i2)));
                return i2 + 8;
            } else if (i4 == 2) {
                int i5 = i(bArr, i2, sz2);
                int i6 = sz2.f3339a;
                if (i6 < 0) {
                    throw l13.zzb();
                } else if (i6 <= bArr.length - i5) {
                    if (i6 == 0) {
                        w33.c(i, xz2.zza);
                    } else {
                        w33.c(i, xz2.zza(bArr, i5, i6));
                    }
                    return i5 + i6;
                } else {
                    throw l13.zza();
                }
            } else if (i4 == 3) {
                w33 g = w33.g();
                int i7 = (i & -8) | 4;
                int i8 = 0;
                int i9 = i2;
                while (true) {
                    if (i9 >= i3) {
                        break;
                    }
                    int i10 = i(bArr, i9, sz2);
                    i8 = sz2.f3339a;
                    if (i8 == i7) {
                        i9 = i10;
                        break;
                    }
                    i9 = c(i8, bArr, i10, i3, g, sz2);
                }
                if (i9 > i3 || i8 != i7) {
                    throw l13.zzg();
                }
                w33.c(i, g);
                return i9;
            } else if (i4 == 5) {
                w33.c(i, Integer.valueOf(h(bArr, i2)));
                return i2 + 4;
            } else {
                throw l13.zzd();
            }
        } else {
            throw l13.zzd();
        }
    }

    @DexIgnore
    public static int d(int i, byte[] bArr, int i2, sz2 sz2) {
        int i3 = i & 127;
        int i4 = i2 + 1;
        byte b = bArr[i2];
        if (b >= 0) {
            sz2.f3339a = i3 | (b << 7);
            return i4;
        }
        int i5 = ((b & Byte.MAX_VALUE) << 7) | i3;
        int i6 = i4 + 1;
        byte b2 = bArr[i4];
        if (b2 >= 0) {
            sz2.f3339a = (b2 << DateTimeFieldType.HOUR_OF_HALFDAY) | i5;
            return i6;
        }
        int i7 = ((b2 & Byte.MAX_VALUE) << 14) | i5;
        int i8 = i6 + 1;
        byte b3 = bArr[i6];
        if (b3 >= 0) {
            sz2.f3339a = i7 | (b3 << DateTimeFieldType.SECOND_OF_MINUTE);
            return i8;
        }
        int i9 = i7 | ((b3 & Byte.MAX_VALUE) << 21);
        int i10 = i8 + 1;
        byte b4 = bArr[i8];
        if (b4 >= 0) {
            sz2.f3339a = (b4 << 28) | i9;
            return i10;
        }
        while (true) {
            int i11 = i10 + 1;
            if (bArr[i10] >= 0) {
                sz2.f3339a = ((b4 & Byte.MAX_VALUE) << 28) | i9;
                return i11;
            }
            i10 = i11;
        }
    }

    @DexIgnore
    public static int e(f33<?> f33, int i, byte[] bArr, int i2, int i3, m13<?> m13, sz2 sz2) throws IOException {
        int g = g(f33, bArr, i2, i3, sz2);
        m13.add(sz2.c);
        while (g < i3) {
            int i4 = i(bArr, g, sz2);
            if (i != sz2.f3339a) {
                break;
            }
            g = g(f33, bArr, i4, i3, sz2);
            m13.add(sz2.c);
        }
        return g;
    }

    @DexIgnore
    public static int f(f33 f33, byte[] bArr, int i, int i2, int i3, sz2 sz2) throws IOException {
        q23 q23 = (q23) f33;
        Object zza = q23.zza();
        int h = q23.h(zza, bArr, i, i2, i3, sz2);
        q23.zzc(zza);
        sz2.c = zza;
        return h;
    }

    @DexIgnore
    public static int g(f33 f33, byte[] bArr, int i, int i2, sz2 sz2) throws IOException {
        int i3 = i + 1;
        byte b = bArr[i];
        byte b2 = b;
        if (b < 0) {
            i3 = d(b, bArr, i3, sz2);
            b2 = sz2.f3339a;
        }
        if (b2 < 0 || b2 > i2 - i3) {
            throw l13.zza();
        }
        Object zza = f33.zza();
        int i4 = (b2 == 1 ? 1 : 0) + i3;
        f33.a(zza, bArr, i3, i4, sz2);
        f33.zzc(zza);
        sz2.c = zza;
        return i4;
    }

    @DexIgnore
    public static int h(byte[] bArr, int i) {
        return (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24);
    }

    @DexIgnore
    public static int i(byte[] bArr, int i, sz2 sz2) {
        int i2 = i + 1;
        byte b = bArr[i];
        if (b < 0) {
            return d(b, bArr, i2, sz2);
        }
        sz2.f3339a = b;
        return i2;
    }

    @DexIgnore
    public static int j(byte[] bArr, int i, m13<?> m13, sz2 sz2) throws IOException {
        f13 f13 = (f13) m13;
        int i2 = i(bArr, i, sz2);
        int i3 = sz2.f3339a + i2;
        while (i2 < i3) {
            i2 = i(bArr, i2, sz2);
            f13.d(sz2.f3339a);
        }
        if (i2 == i3) {
            return i2;
        }
        throw l13.zza();
    }

    @DexIgnore
    public static int k(byte[] bArr, int i, sz2 sz2) {
        int i2 = 7;
        int i3 = i + 1;
        long j = (long) bArr[i];
        if (j >= 0) {
            sz2.b = j;
        } else {
            byte b = bArr[i3];
            long j2 = (j & 127) | (((long) (b & Byte.MAX_VALUE)) << 7);
            i3++;
            while (b < 0) {
                b = bArr[i3];
                i2 += 7;
                j2 |= ((long) (b & Byte.MAX_VALUE)) << i2;
                i3++;
            }
            sz2.b = j2;
        }
        return i3;
    }

    @DexIgnore
    public static long l(byte[] bArr, int i) {
        return (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((255 & ((long) bArr[i + 2])) << 16) | ((255 & ((long) bArr[i + 3])) << 24) | ((255 & ((long) bArr[i + 4])) << 32) | ((255 & ((long) bArr[i + 5])) << 40) | ((255 & ((long) bArr[i + 6])) << 48) | ((((long) bArr[i + 7]) & 255) << 56);
    }

    @DexIgnore
    public static double m(byte[] bArr, int i) {
        return Double.longBitsToDouble(l(bArr, i));
    }

    @DexIgnore
    public static int n(byte[] bArr, int i, sz2 sz2) throws l13 {
        int i2 = i(bArr, i, sz2);
        int i3 = sz2.f3339a;
        if (i3 < 0) {
            throw l13.zzb();
        } else if (i3 == 0) {
            sz2.c = "";
            return i2;
        } else {
            sz2.c = new String(bArr, i2, i3, h13.f1410a);
            return i2 + i3;
        }
    }

    @DexIgnore
    public static float o(byte[] bArr, int i) {
        return Float.intBitsToFloat(h(bArr, i));
    }

    @DexIgnore
    public static int p(byte[] bArr, int i, sz2 sz2) throws l13 {
        int i2 = i(bArr, i, sz2);
        int i3 = sz2.f3339a;
        if (i3 < 0) {
            throw l13.zzb();
        } else if (i3 == 0) {
            sz2.c = "";
            return i2;
        } else {
            sz2.c = g43.k(bArr, i2, i3);
            return i2 + i3;
        }
    }

    @DexIgnore
    public static int q(byte[] bArr, int i, sz2 sz2) throws l13 {
        int i2 = i(bArr, i, sz2);
        int i3 = sz2.f3339a;
        if (i3 < 0) {
            throw l13.zzb();
        } else if (i3 > bArr.length - i2) {
            throw l13.zza();
        } else if (i3 == 0) {
            sz2.c = xz2.zza;
            return i2;
        } else {
            sz2.c = xz2.zza(bArr, i2, i3);
            return i2 + i3;
        }
    }
}
