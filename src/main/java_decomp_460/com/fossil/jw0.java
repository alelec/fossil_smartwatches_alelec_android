package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jw0<T> extends xw0 {
    @DexIgnore
    public jw0(qw0 qw0) {
        super(qw0);
    }

    @DexIgnore
    public abstract void bind(px0 px0, T t);

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.jw0<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final void insert(Iterable<? extends T> iterable) {
        px0 acquire = acquire();
        try {
            Iterator<? extends T> it = iterable.iterator();
            while (it.hasNext()) {
                bind(acquire, it.next());
                acquire.executeInsert();
            }
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final void insert(T t) {
        px0 acquire = acquire();
        try {
            bind(acquire, t);
            acquire.executeInsert();
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final void insert(T[] tArr) {
        px0 acquire = acquire();
        try {
            for (T t : tArr) {
                bind(acquire, t);
                acquire.executeInsert();
            }
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final long insertAndReturnId(T t) {
        px0 acquire = acquire();
        try {
            bind(acquire, t);
            return acquire.executeInsert();
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: com.fossil.jw0<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final long[] insertAndReturnIdsArray(Collection<? extends T> collection) {
        px0 acquire = acquire();
        try {
            long[] jArr = new long[collection.size()];
            int i = 0;
            Iterator<? extends T> it = collection.iterator();
            while (it.hasNext()) {
                bind(acquire, it.next());
                jArr[i] = acquire.executeInsert();
                i++;
            }
            return jArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final long[] insertAndReturnIdsArray(T[] tArr) {
        px0 acquire = acquire();
        try {
            long[] jArr = new long[tArr.length];
            int i = 0;
            for (T t : tArr) {
                bind(acquire, t);
                jArr[i] = acquire.executeInsert();
                i++;
            }
            return jArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: com.fossil.jw0<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final Long[] insertAndReturnIdsArrayBox(Collection<? extends T> collection) {
        px0 acquire = acquire();
        try {
            Long[] lArr = new Long[collection.size()];
            int i = 0;
            Iterator<? extends T> it = collection.iterator();
            while (it.hasNext()) {
                bind(acquire, it.next());
                lArr[i] = Long.valueOf(acquire.executeInsert());
                i++;
            }
            return lArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final Long[] insertAndReturnIdsArrayBox(T[] tArr) {
        px0 acquire = acquire();
        try {
            Long[] lArr = new Long[tArr.length];
            int i = 0;
            for (T t : tArr) {
                bind(acquire, t);
                lArr[i] = Long.valueOf(acquire.executeInsert());
                i++;
            }
            return lArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: com.fossil.jw0<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final List<Long> insertAndReturnIdsList(Collection<? extends T> collection) {
        px0 acquire = acquire();
        try {
            ArrayList arrayList = new ArrayList(collection.size());
            int i = 0;
            Iterator<? extends T> it = collection.iterator();
            while (it.hasNext()) {
                bind(acquire, it.next());
                arrayList.add(i, Long.valueOf(acquire.executeInsert()));
                i++;
            }
            return arrayList;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final List<Long> insertAndReturnIdsList(T[] tArr) {
        px0 acquire = acquire();
        try {
            ArrayList arrayList = new ArrayList(tArr.length);
            int i = 0;
            for (T t : tArr) {
                bind(acquire, t);
                arrayList.add(i, Long.valueOf(acquire.executeInsert()));
                i++;
            }
            return arrayList;
        } finally {
            release(acquire);
        }
    }
}
