package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qs7<T> implements ts7<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ts7<T> f3014a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ rp7<T, Boolean> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<T>, jr7 {
        @DexIgnore
        public /* final */ Iterator<T> b;
        @DexIgnore
        public int c; // = -1;
        @DexIgnore
        public T d;
        @DexIgnore
        public /* final */ /* synthetic */ qs7 e;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(qs7 qs7) {
            this.e = qs7;
            this.b = qs7.f3014a.iterator();
        }

        @DexIgnore
        public final void a() {
            while (this.b.hasNext()) {
                T next = this.b.next();
                if (((Boolean) this.e.c.invoke(next)).booleanValue() == this.e.b) {
                    this.d = next;
                    this.c = 1;
                    return;
                }
            }
            this.c = 0;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.c == -1) {
                a();
            }
            return this.c == 1;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (this.c == -1) {
                a();
            }
            if (this.c != 0) {
                T t = this.d;
                this.d = null;
                this.c = -1;
                return t;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.ts7<? extends T> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.rp7<? super T, java.lang.Boolean> */
    /* JADX WARN: Multi-variable type inference failed */
    public qs7(ts7<? extends T> ts7, boolean z, rp7<? super T, Boolean> rp7) {
        pq7.c(ts7, "sequence");
        pq7.c(rp7, "predicate");
        this.f3014a = ts7;
        this.b = z;
        this.c = rp7;
    }

    @DexIgnore
    @Override // com.fossil.ts7
    public Iterator<T> iterator() {
        return new a(this);
    }
}
