package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jj5 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<T> {
    }

    @DexIgnore
    public static final <T> String a(T t) {
        String u = new Gson().u(t, new a().getType());
        pq7.b(u, "Gson().toJson(this, type)");
        return u;
    }
}
