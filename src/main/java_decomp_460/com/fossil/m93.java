package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m93 implements xw2<p93> {
    @DexIgnore
    public static m93 c; // = new m93();
    @DexIgnore
    public /* final */ xw2<p93> b;

    @DexIgnore
    public m93() {
        this(ww2.b(new o93()));
    }

    @DexIgnore
    public m93(xw2<p93> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((p93) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ p93 zza() {
        return this.b.zza();
    }
}
