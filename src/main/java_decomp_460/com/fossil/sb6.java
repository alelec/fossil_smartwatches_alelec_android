package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sb6 extends ts0 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public HybridPreset f3232a;
    @DexIgnore
    public MutableLiveData<HybridPreset> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> d; // = new ArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<MicroApp> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Gson h; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<String> i;
    @DexIgnore
    public /* final */ LiveData<MicroApp> j;
    @DexIgnore
    public /* final */ LiveData<Boolean> k;
    @DexIgnore
    public /* final */ ls0<HybridPreset> l;
    @DexIgnore
    public /* final */ HybridPresetRepository m;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository n;
    @DexIgnore
    public /* final */ MicroAppRepository o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return sb6.p;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<HybridPreset> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sb6 f3233a;

        @DexIgnore
        public b(sb6 sb6) {
            this.f3233a = sb6;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v8, resolved type: androidx.lifecycle.MutableLiveData */
        /* JADX WARN: Multi-variable type inference failed */
        /* renamed from: a */
        public final void onChanged(HybridPreset hybridPreset) {
            T t;
            boolean z;
            FLogger.INSTANCE.getLocal().d(sb6.q.a(), "current preset change=" + hybridPreset);
            if (hybridPreset != null) {
                String str = (String) this.f3233a.e.e();
                String str2 = (String) this.f3233a.f.e();
                Iterator<T> it = hybridPreset.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    T t2 = next;
                    if (!pq7.a(t2.getPosition(), str) || vt7.j(t2.getAppId(), str2, true)) {
                        z = false;
                        continue;
                    } else {
                        z = true;
                        continue;
                    }
                    if (z) {
                        t = next;
                        break;
                    }
                }
                T t3 = t;
                if (t3 != null) {
                    FLogger.INSTANCE.getLocal().d(sb6.q.a(), "Update new microapp id=" + t3.getAppId() + " at position=" + str);
                    this.f3233a.f.l(t3.getAppId());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$init$1", f = "HybridCustomizeViewModel.kt", l = {102, 107}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $microAppPos;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sb6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$init$1$1", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.b.i(this.this$0.this$0.l);
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(sb6 sb6, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sb6;
            this.$presetId = str;
            this.$microAppPos = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$presetId, this.$microAppPos, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0046  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                r7 = 2
                r6 = 1
                java.lang.Object r1 = com.fossil.yn7.d()
                int r0 = r8.label
                if (r0 == 0) goto L_0x0048
                if (r0 == r6) goto L_0x0020
                if (r0 != r7) goto L_0x0018
                java.lang.Object r0 = r8.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r9)
            L_0x0015:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r8.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r9)
            L_0x0027:
                com.fossil.sb6 r2 = r8.this$0
                androidx.lifecycle.MutableLiveData r2 = com.fossil.sb6.h(r2)
                java.lang.String r3 = r8.$microAppPos
                r2.l(r3)
                com.fossil.jx7 r2 = com.fossil.bw7.c()
                com.fossil.sb6$c$a r3 = new com.fossil.sb6$c$a
                r4 = 0
                r3.<init>(r8, r4)
                r8.L$0 = r0
                r8.label = r7
                java.lang.Object r0 = com.fossil.eu7.g(r2, r3, r8)
                if (r0 != r1) goto L_0x0015
                r0 = r1
                goto L_0x0017
            L_0x0048:
                com.fossil.el7.b(r9)
                com.fossil.iv7 r0 = r8.p$
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                com.fossil.sb6$a r3 = com.fossil.sb6.q
                java.lang.String r3 = r3.a()
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "init presetId="
                r4.append(r5)
                java.lang.String r5 = r8.$presetId
                r4.append(r5)
                java.lang.String r5 = " originalPreset="
                r4.append(r5)
                com.fossil.sb6 r5 = r8.this$0
                com.portfolio.platform.data.model.room.microapp.HybridPreset r5 = com.fossil.sb6.e(r5)
                r4.append(r5)
                java.lang.String r5 = " microAppPos="
                r4.append(r5)
                java.lang.String r5 = r8.$microAppPos
                r4.append(r5)
                r5 = 32
                r4.append(r5)
                java.lang.String r4 = r4.toString()
                r2.d(r3, r4)
                com.fossil.sb6 r2 = r8.this$0
                com.portfolio.platform.data.model.room.microapp.HybridPreset r2 = com.fossil.sb6.e(r2)
                if (r2 != 0) goto L_0x0027
                com.fossil.sb6 r2 = r8.this$0
                java.lang.String r3 = r8.$presetId
                r8.L$0 = r0
                r8.label = r6
                java.lang.Object r2 = r2.x(r3, r8)
                if (r2 != r1) goto L_0x0027
                r0 = r1
                goto L_0x0017
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.sb6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initFromSaveInstanceState$1", f = "HybridCustomizeViewModel.kt", l = {118, 125}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $microAppPos;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $savedCurrentPreset;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $savedOriginalPreset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sb6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initFromSaveInstanceState$1$1", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.b.i(this.this$0.this$0.l);
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(sb6 sb6, String str, HybridPreset hybridPreset, HybridPreset hybridPreset2, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sb6;
            this.$presetId = str;
            this.$savedOriginalPreset = hybridPreset;
            this.$savedCurrentPreset = hybridPreset2;
            this.$microAppPos = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$presetId, this.$savedOriginalPreset, this.$savedCurrentPreset, this.$microAppPos, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0055  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                r7 = 2
                r6 = 1
                java.lang.Object r1 = com.fossil.yn7.d()
                int r0 = r8.label
                if (r0 == 0) goto L_0x0057
                if (r0 == r6) goto L_0x002f
                if (r0 != r7) goto L_0x0027
                java.lang.Object r0 = r8.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r9)
            L_0x0015:
                com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = r8.$savedCurrentPreset
                if (r0 == 0) goto L_0x0024
                com.fossil.sb6 r0 = r8.this$0
                androidx.lifecycle.MutableLiveData r0 = com.fossil.sb6.a(r0)
                com.portfolio.platform.data.model.room.microapp.HybridPreset r1 = r8.$savedCurrentPreset
                r0.l(r1)
            L_0x0024:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0026:
                return r0
            L_0x0027:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x002f:
                java.lang.Object r0 = r8.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r9)
            L_0x0036:
                com.fossil.sb6 r2 = r8.this$0
                androidx.lifecycle.MutableLiveData r2 = com.fossil.sb6.h(r2)
                java.lang.String r3 = r8.$microAppPos
                r2.l(r3)
                com.fossil.jx7 r2 = com.fossil.bw7.c()
                com.fossil.sb6$d$a r3 = new com.fossil.sb6$d$a
                r4 = 0
                r3.<init>(r8, r4)
                r8.L$0 = r0
                r8.label = r7
                java.lang.Object r0 = com.fossil.eu7.g(r2, r3, r8)
                if (r0 != r1) goto L_0x0015
                r0 = r1
                goto L_0x0026
            L_0x0057:
                com.fossil.el7.b(r9)
                com.fossil.iv7 r0 = r8.p$
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                com.fossil.sb6$a r3 = com.fossil.sb6.q
                java.lang.String r3 = r3.a()
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "initFromSaveInstanceState presetId "
                r4.append(r5)
                java.lang.String r5 = r8.$presetId
                r4.append(r5)
                java.lang.String r5 = " savedOriginalPreset="
                r4.append(r5)
                com.portfolio.platform.data.model.room.microapp.HybridPreset r5 = r8.$savedOriginalPreset
                r4.append(r5)
                r5 = 44
                r4.append(r5)
                java.lang.String r5 = " savedCurrentPreset="
                r4.append(r5)
                com.portfolio.platform.data.model.room.microapp.HybridPreset r5 = r8.$savedCurrentPreset
                r4.append(r5)
                java.lang.String r5 = " microAppPos="
                r4.append(r5)
                java.lang.String r5 = r8.$microAppPos
                r4.append(r5)
                java.lang.String r5 = " currentPreset"
                r4.append(r5)
                java.lang.String r4 = r4.toString()
                r2.d(r3, r4)
                com.portfolio.platform.data.model.room.microapp.HybridPreset r2 = r8.$savedOriginalPreset
                if (r2 != 0) goto L_0x00bb
                com.fossil.sb6 r2 = r8.this$0
                java.lang.String r3 = r8.$presetId
                r8.L$0 = r0
                r8.label = r6
                java.lang.Object r2 = r2.x(r3, r8)
                if (r2 != r1) goto L_0x0036
                r0 = r1
                goto L_0x0026
            L_0x00bb:
                com.fossil.sb6 r2 = r8.this$0
                com.fossil.sb6.j(r2)
                com.fossil.sb6 r2 = r8.this$0
                com.portfolio.platform.data.model.room.microapp.HybridPreset r3 = r8.$savedOriginalPreset
                com.fossil.sb6.l(r2, r3)
                goto L_0x0036
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.sb6.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel", f = "HybridCustomizeViewModel.kt", l = {263, 264}, m = "initializePreset")
    public static final class e extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ sb6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(sb6 sb6, qn7 qn7) {
            super(qn7);
            this.this$0 = sb6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.x(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initializePreset$2", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sb6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(sb6 sb6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sb6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.w();
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initializePreset$preset$1", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super HybridPreset>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sb6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(sb6 sb6, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sb6;
            this.$presetId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, this.$presetId, qn7);
            gVar.p$ = (iv7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super HybridPreset> qn7) {
            return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return this.this$0.m.getPresetById(this.$presetId);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sb6 f3234a;

        @DexIgnore
        public h(sb6 sb6) {
            this.f3234a = sb6;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<MicroApp> apply(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = sb6.q.a();
            local.d(a2, "transformWatchAppIdToModel watchAppId=" + str);
            sb6 sb6 = this.f3234a;
            pq7.b(str, "id");
            this.f3234a.g.l(sb6.n(str));
            return this.f3234a.g;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sb6 f3235a;

        @DexIgnore
        public i(sb6 sb6) {
            this.f3235a = sb6;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(String str) {
            T t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = sb6.q.a();
            local.d(a2, "transformMicroAppPosToId pos=" + str);
            HybridPreset hybridPreset = (HybridPreset) this.f3235a.b.e();
            if (hybridPreset != null) {
                Iterator<T> it = hybridPreset.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (pq7.a(next.getPosition(), str)) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    this.f3235a.f.o(t2.getAppId());
                }
            }
            return this.f3235a.f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sb6 f3236a;

        @DexIgnore
        public j(sb6 sb6) {
            this.f3236a = sb6;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<Boolean> apply(HybridPreset hybridPreset) {
            ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
            Gson gson = this.f3236a.h;
            HybridPreset hybridPreset2 = this.f3236a.f3232a;
            if (hybridPreset2 != null) {
                boolean h = kj5.h(buttons, gson, hybridPreset2.getButtons());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = sb6.q.a();
                local.d(a2, "isTheSameHybridAppSetting " + h);
                if (h) {
                    this.f3236a.c.l(Boolean.FALSE);
                } else {
                    this.f3236a.c.l(Boolean.TRUE);
                }
                return this.f3236a.c;
            }
            pq7.i();
            throw null;
        }
    }

    /*
    static {
        String simpleName = sb6.class.getSimpleName();
        pq7.b(simpleName, "HybridCustomizeViewModel::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public sb6(HybridPresetRepository hybridPresetRepository, MicroAppLastSettingRepository microAppLastSettingRepository, MicroAppRepository microAppRepository) {
        pq7.c(hybridPresetRepository, "mHybridPresetRepository");
        pq7.c(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        pq7.c(microAppRepository, "mMicroAppRepository");
        this.m = hybridPresetRepository;
        this.n = microAppLastSettingRepository;
        this.o = microAppRepository;
        LiveData<String> c2 = ss0.c(this.e, new i(this));
        pq7.b(c2, "Transformations.switchMa\u2026dMicroAppIdLiveData\n    }");
        this.i = c2;
        LiveData<MicroApp> c3 = ss0.c(c2, new h(this));
        pq7.b(c3, "Transformations.switchMa\u2026tedMicroAppLiveData\n    }");
        this.j = c3;
        LiveData<Boolean> c4 = ss0.c(this.b, new j(this));
        pq7.b(c4, "Transformations.switchMa\u2026    isPresetChanged\n    }");
        this.k = c4;
        this.l = new b(this);
    }

    @DexIgnore
    public final void A(String str) {
        pq7.c(str, "watchAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = p;
        local.d(str2, "setSelectedMicroApp watchAppPos=" + str);
        this.e.l(str);
    }

    @DexIgnore
    public final void B(HybridPreset hybridPreset) {
        pq7.c(hybridPreset, "preset");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "savePreset newPreset=" + hybridPreset);
        this.b.l(hybridPreset.clone());
    }

    @DexIgnore
    public final List<MicroApp> m(String str) {
        pq7.c(str, "category");
        ArrayList<MicroApp> arrayList = this.d;
        ArrayList arrayList2 = new ArrayList();
        for (T t : arrayList) {
            if (t.getCategories().contains(str)) {
                arrayList2.add(t);
            }
        }
        return arrayList2;
    }

    @DexIgnore
    public final MicroApp n(String str) {
        T t;
        pq7.c(str, "microAppId");
        Iterator<T> it = this.d.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (pq7.a(str, next.getId())) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final MutableLiveData<HybridPreset> o() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ts0
    public void onCleared() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onCleared originalPreset=" + this.f3232a + " currentPreset=" + this.b.e());
        this.b.m(this.l);
        super.onCleared();
    }

    @DexIgnore
    public final LiveData<Boolean> p() {
        return this.k;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v23, resolved type: com.portfolio.platform.data.model.setting.SecondTimezoneSetting */
    /* JADX DEBUG: Multi-variable search result rejected for r0v27, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeSetting */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0045, code lost:
        if (r1 != null) goto L_0x0047;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.os.Parcelable q(java.lang.String r8) {
        /*
        // Method dump skipped, instructions count: 336
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sb6.q(java.lang.String):android.os.Parcelable");
    }

    @DexIgnore
    public final HybridPreset r() {
        return this.f3232a;
    }

    @DexIgnore
    public final LiveData<MicroApp> s() {
        LiveData<MicroApp> liveData = this.j;
        if (liveData != null) {
            return liveData;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final MutableLiveData<String> t() {
        return this.e;
    }

    @DexIgnore
    public final xw7 u(String str, String str2) {
        pq7.c(str, "presetId");
        return gu7.d(jv7.a(bw7.a()), null, null, new c(this, str, str2, null), 3, null);
    }

    @DexIgnore
    public final xw7 v(String str, HybridPreset hybridPreset, HybridPreset hybridPreset2, String str2) {
        pq7.c(str, "presetId");
        return gu7.d(jv7.a(bw7.a()), null, null, new d(this, str, hybridPreset, hybridPreset2, str2, null), 3, null);
    }

    @DexIgnore
    public final void w() {
        this.d.clear();
        this.d.addAll(this.o.getAllMicroApp(PortfolioApp.h0.c().J()));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object x(java.lang.String r10, com.fossil.qn7<? super com.fossil.tl7> r11) {
        /*
            r9 = this;
            r8 = 0
            r7 = 2
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r11 instanceof com.fossil.sb6.e
            if (r0 == 0) goto L_0x0047
            r0 = r11
            com.fossil.sb6$e r0 = (com.fossil.sb6.e) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0047
            int r1 = r1 + r3
            r0.label = r1
            r3 = r0
        L_0x0016:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.yn7.d()
            int r0 = r3.label
            if (r0 == 0) goto L_0x007e
            if (r0 == r6) goto L_0x0056
            if (r0 != r7) goto L_0x004e
            java.lang.Object r0 = r3.L$2
            com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r0
            java.lang.Object r1 = r3.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r3.L$0
            com.fossil.sb6 r1 = (com.fossil.sb6) r1
            com.fossil.el7.b(r2)
        L_0x0033:
            if (r0 == 0) goto L_0x0044
            com.portfolio.platform.data.model.room.microapp.HybridPreset r2 = r0.clone()
            r1.f3232a = r2
            androidx.lifecycle.MutableLiveData<com.portfolio.platform.data.model.room.microapp.HybridPreset> r1 = r1.b
            com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = r0.clone()
            r1.l(r0)
        L_0x0044:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0046:
            return r0
        L_0x0047:
            com.fossil.sb6$e r0 = new com.fossil.sb6$e
            r0.<init>(r9, r11)
            r3 = r0
            goto L_0x0016
        L_0x004e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0056:
            java.lang.Object r0 = r3.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r3.L$0
            com.fossil.sb6 r1 = (com.fossil.sb6) r1
            com.fossil.el7.b(r2)
            r10 = r0
        L_0x0062:
            r0 = r2
            com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r0
            com.fossil.dv7 r2 = com.fossil.bw7.a()
            com.fossil.sb6$f r5 = new com.fossil.sb6$f
            r5.<init>(r1, r8)
            r3.L$0 = r1
            r3.L$1 = r10
            r3.L$2 = r0
            r3.label = r7
            java.lang.Object r2 = com.fossil.eu7.g(r2, r5, r3)
            if (r2 != r4) goto L_0x0033
            r0 = r4
            goto L_0x0046
        L_0x007e:
            com.fossil.el7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.sb6.p
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r5 = "initializePreset presetId="
            r2.append(r5)
            r2.append(r10)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            com.fossil.dv7 r0 = com.fossil.bw7.b()
            com.fossil.sb6$g r1 = new com.fossil.sb6$g
            r1.<init>(r9, r10, r8)
            r3.L$0 = r9
            r3.L$1 = r10
            r3.label = r6
            java.lang.Object r2 = com.fossil.eu7.g(r0, r1, r3)
            if (r2 != r4) goto L_0x00b4
            r0 = r4
            goto L_0x0046
        L_0x00b4:
            r1 = r9
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sb6.x(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final boolean y(String str) {
        pq7.c(str, "microAppId");
        HybridPreset e2 = this.b.e();
        if (e2 != null) {
            Iterator<HybridPresetAppSetting> it = e2.getButtons().iterator();
            while (it.hasNext()) {
                if (pq7.a(it.next().getAppId(), str)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean z() {
        Boolean e2 = this.c.e();
        if (e2 != null) {
            return e2.booleanValue();
        }
        return false;
    }
}
