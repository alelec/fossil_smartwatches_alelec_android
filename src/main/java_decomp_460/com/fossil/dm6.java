package com.fossil;

import android.os.Bundle;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.fl5;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dm6 extends yl6 implements fl5.a {
    @DexIgnore
    public Date e;
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public MutableLiveData<cl7<Date, Date>> g; // = new MutableLiveData<>();
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ u08 j; // = w08.b(false, 1, null);
    @DexIgnore
    public List<GoalTrackingSummary> k; // = new ArrayList();
    @DexIgnore
    public GoalTrackingSummary l;
    @DexIgnore
    public List<GoalTrackingData> m;
    @DexIgnore
    public LiveData<h47<List<GoalTrackingSummary>>> n;
    @DexIgnore
    public Listing<GoalTrackingData> o;
    @DexIgnore
    public /* final */ zl6 p;
    @DexIgnore
    public /* final */ GoalTrackingRepository q;
    @DexIgnore
    public /* final */ no4 r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$addRecord$1", f = "GoalTrackingDetailPresenter.kt", l = {Action.Selfie.TAKE_BURST}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dm6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.dm6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$addRecord$1$1", f = "GoalTrackingDetailPresenter.kt", l = {203}, m = "invokeSuspend")
        /* renamed from: com.fossil.dm6$a$a  reason: collision with other inner class name */
        public static final class C0054a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ GoalTrackingData $goalTrackingData;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0054a(a aVar, GoalTrackingData goalTrackingData, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
                this.$goalTrackingData = goalTrackingData;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0054a aVar = new C0054a(this.this$0, this.$goalTrackingData, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((C0054a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    GoalTrackingRepository goalTrackingRepository = this.this$0.this$0.q;
                    List<GoalTrackingData> i2 = hm7.i(this.$goalTrackingData);
                    this.L$0 = iv7;
                    this.label = 1;
                    if (goalTrackingRepository.insertFromDevice(i2, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(dm6 dm6, Date date, qn7 qn7) {
            super(2, qn7);
            this.this$0 = dm6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$date, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                String uuid = UUID.randomUUID().toString();
                pq7.b(uuid, "UUID.randomUUID().toString()");
                Date date = this.$date;
                TimeZone timeZone = TimeZone.getDefault();
                pq7.b(timeZone, "TimeZone.getDefault()");
                DateTime b = lk5.b(date, timeZone.getRawOffset() / 1000);
                pq7.b(b, "DateHelper.createDateTim\u2026fault().rawOffset / 1000)");
                TimeZone timeZone2 = TimeZone.getDefault();
                pq7.b(timeZone2, "TimeZone.getDefault()");
                GoalTrackingData goalTrackingData = new GoalTrackingData(uuid, b, timeZone2.getRawOffset() / 1000, this.$date, new Date().getTime(), new Date().getTime());
                dv7 i2 = this.this$0.i();
                C0054a aVar = new C0054a(this, goalTrackingData, null);
                this.L$0 = iv7;
                this.L$1 = goalTrackingData;
                this.label = 1;
                if (eu7.g(i2, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                GoalTrackingData goalTrackingData2 = (GoalTrackingData) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1", f = "GoalTrackingDetailPresenter.kt", l = {211}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingData $raw;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dm6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1$1", f = "GoalTrackingDetailPresenter.kt", l = {211}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    GoalTrackingRepository goalTrackingRepository = this.this$0.this$0.q;
                    GoalTrackingData goalTrackingData = this.this$0.$raw;
                    this.L$0 = iv7;
                    this.label = 1;
                    if (goalTrackingRepository.deleteGoalTracking(goalTrackingData, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(dm6 dm6, GoalTrackingData goalTrackingData, qn7 qn7) {
            super(2, qn7);
            this.this$0 = dm6;
            this.$raw = goalTrackingData;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$raw, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(i2, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1", f = "GoalTrackingDetailPresenter.kt", l = {85}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dm6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements ls0<cu0<GoalTrackingData>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ c f809a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.dm6$c$a$a")
            /* renamed from: com.fossil.dm6$c$a$a  reason: collision with other inner class name */
            public static final class C0055a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ cu0 $dataList;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.dm6$c$a$a$a")
                /* renamed from: com.fossil.dm6$c$a$a$a  reason: collision with other inner class name */
                public static final class C0056a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public Object L$1;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0055a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0056a(qn7 qn7, C0055a aVar) {
                        super(2, qn7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        C0056a aVar = new C0056a(qn7, this.this$0);
                        aVar.p$ = (iv7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                        return ((C0056a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final Object invokeSuspend(Object obj) {
                        Object goalTrackingDataInDate;
                        dm6 dm6;
                        Object d = yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            el7.b(obj);
                            iv7 iv7 = this.p$;
                            dm6 dm62 = this.this$0.this$0.f809a.this$0;
                            GoalTrackingRepository goalTrackingRepository = dm62.q;
                            Date date = this.this$0.this$0.f809a.$date;
                            this.L$0 = iv7;
                            this.L$1 = dm62;
                            this.label = 1;
                            goalTrackingDataInDate = goalTrackingRepository.getGoalTrackingDataInDate(date, this);
                            if (goalTrackingDataInDate == d) {
                                return d;
                            }
                            dm6 = dm62;
                        } else if (i == 1) {
                            iv7 iv72 = (iv7) this.L$0;
                            el7.b(obj);
                            goalTrackingDataInDate = obj;
                            dm6 = (dm6) this.L$1;
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        dm6.m = (List) goalTrackingDataInDate;
                        return tl7.f3441a;
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0055a(cu0 cu0, qn7 qn7, a aVar) {
                    super(2, qn7);
                    this.$dataList = cu0;
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0055a aVar = new C0055a(this.$dataList, qn7, this.this$0);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0055a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:20:0x007c  */
                @Override // com.fossil.zn7
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                    /*
                        r7 = this;
                        r1 = 2
                        r3 = 1
                        r6 = 0
                        java.lang.Object r2 = com.fossil.yn7.d()
                        int r0 = r7.label
                        if (r0 == 0) goto L_0x007e
                        if (r0 == r3) goto L_0x005a
                        if (r0 != r1) goto L_0x0052
                        java.lang.Object r0 = r7.L$1
                        com.fossil.u08 r0 = (com.fossil.u08) r0
                        java.lang.Object r1 = r7.L$0
                        com.fossil.iv7 r1 = (com.fossil.iv7) r1
                        com.fossil.el7.b(r8)     // Catch:{ all -> 0x00a0 }
                    L_0x001a:
                        com.fossil.dm6$c$a r1 = r7.this$0     // Catch:{ all -> 0x00a0 }
                        com.fossil.dm6$c r1 = r1.f809a     // Catch:{ all -> 0x00a0 }
                        com.fossil.dm6 r1 = r1.this$0     // Catch:{ all -> 0x00a0 }
                        com.fossil.zl6 r1 = com.fossil.dm6.L(r1)     // Catch:{ all -> 0x00a0 }
                        com.fossil.cu0 r2 = r7.$dataList     // Catch:{ all -> 0x00a0 }
                        r1.n5(r2)     // Catch:{ all -> 0x00a0 }
                        com.fossil.dm6$c$a r1 = r7.this$0     // Catch:{ all -> 0x00a0 }
                        com.fossil.dm6$c r1 = r1.f809a     // Catch:{ all -> 0x00a0 }
                        com.fossil.dm6 r1 = r1.this$0     // Catch:{ all -> 0x00a0 }
                        boolean r1 = com.fossil.dm6.J(r1)     // Catch:{ all -> 0x00a0 }
                        if (r1 == 0) goto L_0x004a
                        com.fossil.dm6$c$a r1 = r7.this$0     // Catch:{ all -> 0x00a0 }
                        com.fossil.dm6$c r1 = r1.f809a     // Catch:{ all -> 0x00a0 }
                        com.fossil.dm6 r1 = r1.this$0     // Catch:{ all -> 0x00a0 }
                        boolean r1 = com.fossil.dm6.I(r1)     // Catch:{ all -> 0x00a0 }
                        if (r1 == 0) goto L_0x004a
                        com.fossil.dm6$c$a r1 = r7.this$0     // Catch:{ all -> 0x00a0 }
                        com.fossil.dm6$c r1 = r1.f809a     // Catch:{ all -> 0x00a0 }
                        com.fossil.dm6 r1 = r1.this$0     // Catch:{ all -> 0x00a0 }
                        com.fossil.dm6.W(r1)     // Catch:{ all -> 0x00a0 }
                    L_0x004a:
                        com.fossil.tl7 r1 = com.fossil.tl7.f3441a     // Catch:{ all -> 0x00a0 }
                        r0.b(r6)
                        com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                    L_0x0051:
                        return r0
                    L_0x0052:
                        java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                        java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                        r0.<init>(r1)
                        throw r0
                    L_0x005a:
                        java.lang.Object r0 = r7.L$1
                        com.fossil.u08 r0 = (com.fossil.u08) r0
                        java.lang.Object r1 = r7.L$0
                        com.fossil.iv7 r1 = (com.fossil.iv7) r1
                        com.fossil.el7.b(r8)
                    L_0x0065:
                        com.fossil.dv7 r3 = com.fossil.bw7.b()     // Catch:{ all -> 0x009b }
                        com.fossil.dm6$c$a$a$a r4 = new com.fossil.dm6$c$a$a$a     // Catch:{ all -> 0x009b }
                        r5 = 0
                        r4.<init>(r5, r7)     // Catch:{ all -> 0x009b }
                        r7.L$0 = r1     // Catch:{ all -> 0x009b }
                        r7.L$1 = r0     // Catch:{ all -> 0x009b }
                        r1 = 2
                        r7.label = r1     // Catch:{ all -> 0x009b }
                        java.lang.Object r1 = com.fossil.eu7.g(r3, r4, r7)     // Catch:{ all -> 0x009b }
                        if (r1 != r2) goto L_0x001a
                        r0 = r2
                        goto L_0x0051
                    L_0x007e:
                        com.fossil.el7.b(r8)
                        com.fossil.iv7 r1 = r7.p$
                        com.fossil.dm6$c$a r0 = r7.this$0
                        com.fossil.dm6$c r0 = r0.f809a
                        com.fossil.dm6 r0 = r0.this$0
                        com.fossil.u08 r0 = com.fossil.dm6.G(r0)
                        r7.L$0 = r1
                        r7.L$1 = r0
                        r7.label = r3
                        java.lang.Object r3 = r0.a(r6, r7)
                        if (r3 != r2) goto L_0x0065
                        r0 = r2
                        goto L_0x0051
                    L_0x009b:
                        r1 = move-exception
                    L_0x009c:
                        r0.b(r6)
                        throw r1
                    L_0x00a0:
                        r1 = move-exception
                        goto L_0x009c
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.fossil.dm6.c.a.C0055a.invokeSuspend(java.lang.Object):java.lang.Object");
                }
            }

            @DexIgnore
            public a(c cVar) {
                this.f809a = cVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(cu0<GoalTrackingData> cu0) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getGoalTrackingDataPaging observer size=");
                sb.append(cu0 != null ? Integer.valueOf(cu0.size()) : null);
                local.d("GoalTrackingDetailPresenter", sb.toString());
                if (cu0 != null) {
                    this.f809a.this$0.i = true;
                    xw7 unused = gu7.d(this.f809a.this$0.k(), null, null, new C0055a(cu0, null, this), 3, null);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(dm6 dm6, Date date, qn7 qn7) {
            super(2, qn7);
            this.this$0 = dm6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$date, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object goalTrackingDataPaging;
            dm6 dm6;
            LiveData pagedList;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dm6 dm62 = this.this$0;
                GoalTrackingRepository goalTrackingRepository = dm62.q;
                Date date = this.$date;
                no4 no4 = this.this$0.r;
                dm6 dm63 = this.this$0;
                this.L$0 = iv7;
                this.L$1 = dm62;
                this.label = 1;
                goalTrackingDataPaging = goalTrackingRepository.getGoalTrackingDataPaging(date, no4, dm63, this);
                if (goalTrackingDataPaging == d) {
                    return d;
                }
                dm6 = dm62;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                goalTrackingDataPaging = obj;
                dm6 = (dm6) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            dm6.o = (Listing) goalTrackingDataPaging;
            Listing listing = this.this$0.o;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                zl6 zl6 = this.this$0.p;
                if (zl6 != null) {
                    pagedList.h((am6) zl6, new a(this));
                } else {
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
                }
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1", f = "GoalTrackingDetailPresenter.kt", l = {142, 260, 167}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dm6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1$1", f = "GoalTrackingDetailPresenter.kt", l = {142}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Date>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Date> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    PortfolioApp c = PortfolioApp.h0.c();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object n0 = c.n0(this);
                    return n0 == d ? d : n0;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends ko7 implements vp7<iv7, qn7<? super GoalTrackingSummary>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(qn7 qn7, d dVar) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(qn7, this.this$0);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super GoalTrackingSummary> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    dm6 dm6 = this.this$0.this$0;
                    return dm6.X(dm6.f, this.this$0.this$0.k);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(dm6 dm6, Date date, qn7 qn7) {
            super(2, qn7);
            this.this$0 = dm6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$date, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00bf  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r16) {
            /*
            // Method dump skipped, instructions count: 548
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.dm6.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1", f = "GoalTrackingDetailPresenter.kt", l = {260, 227, 230}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dm6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1$1$maxValue$1", f = "GoalTrackingDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $data;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(ArrayList arrayList, qn7 qn7) {
                super(2, qn7);
                this.$data = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$data, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                ArrayList<ArrayList<BarChart.b>> d;
                ArrayList<BarChart.b> arrayList;
                int i = 0;
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    Iterator it = this.$data.iterator();
                    if (!it.hasNext()) {
                        obj2 = null;
                    } else {
                        Object next = it.next();
                        if (!it.hasNext()) {
                            obj2 = next;
                        } else {
                            ArrayList<BarChart.b> arrayList2 = ((BarChart.a) next).d().get(0);
                            pq7.b(arrayList2, "it.mListOfBarPoints[0]");
                            Iterator<T> it2 = arrayList2.iterator();
                            int i2 = 0;
                            while (it2.hasNext()) {
                                i2 = ao7.e(it2.next().e()).intValue() + i2;
                            }
                            Integer e = ao7.e(i2);
                            while (true) {
                                next = it.next();
                                ArrayList<BarChart.b> arrayList3 = ((BarChart.a) next).d().get(0);
                                pq7.b(arrayList3, "it.mListOfBarPoints[0]");
                                Iterator<T> it3 = arrayList3.iterator();
                                int i3 = 0;
                                while (it3.hasNext()) {
                                    i3 = ao7.e(it3.next().e()).intValue() + i3;
                                }
                                e = ao7.e(i3);
                                if (e.compareTo(e) >= 0) {
                                    e = e;
                                    next = next;
                                }
                                if (!it.hasNext()) {
                                    break;
                                }
                            }
                            obj2 = next;
                        }
                    }
                    BarChart.a aVar = (BarChart.a) obj2;
                    if (aVar == null || (d = aVar.d()) == null || (arrayList = d.get(0)) == null) {
                        return null;
                    }
                    Iterator<T> it4 = arrayList.iterator();
                    while (it4.hasNext()) {
                        i += ao7.e(it4.next().e()).intValue();
                    }
                    return ao7.e(i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends ko7 implements vp7<iv7, qn7<? super cl7<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(qn7 qn7, e eVar) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(qn7, this.this$0);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super cl7<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return cn6.f632a.c(this.this$0.this$0.f, this.this$0.this$0.m);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(dm6 dm6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = dm6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00d3  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00f9  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0116  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x011a  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
            // Method dump skipped, instructions count: 305
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.dm6.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<h47<? extends List<GoalTrackingSummary>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ dm6 f810a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$1$1", f = "GoalTrackingDetailPresenter.kt", l = {118}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.dm6$f$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$1$1$summary$1", f = "GoalTrackingDetailPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.dm6$f$a$a  reason: collision with other inner class name */
            public static final class C0057a extends ko7 implements vp7<iv7, qn7<? super GoalTrackingSummary>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0057a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0057a aVar = new C0057a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super GoalTrackingSummary> qn7) {
                    return ((C0057a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        dm6 dm6 = this.this$0.this$0.f810a;
                        return dm6.X(dm6.f, this.this$0.this$0.f810a.k);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    dv7 h = this.this$0.f810a.h();
                    C0057a aVar = new C0057a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    g = eu7.g(h, aVar, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) g;
                if (this.this$0.f810a.l == null || (!pq7.a(this.this$0.f810a.l, goalTrackingSummary))) {
                    this.this$0.f810a.l = goalTrackingSummary;
                    this.this$0.f810a.p.X0(this.this$0.f810a.l);
                    if (this.this$0.f810a.h && this.this$0.f810a.i) {
                        this.this$0.f810a.a0();
                    }
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public f(dm6 dm6) {
            this.f810a = dm6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<GoalTrackingSummary>> h47) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingDetailPresenter", "start - mGoalTrackingSummary -- goalTrackingSummary=" + h47);
            if ((h47 != null ? h47.d() : null) != xh5.DATABASE_LOADING) {
                this.f810a.k = h47 != null ? (List) h47.c() : null;
                this.f810a.h = true;
                xw7 unused = gu7.d(this.f810a.k(), null, null, new a(this, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ dm6 f811a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$summaryTransformations$1$1", f = "GoalTrackingDetailPresenter.kt", l = {60, 60}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<GoalTrackingSummary>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, Date date, Date date2, qn7 qn7) {
                super(2, qn7);
                this.this$0 = gVar;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$first, this.$second, qn7);
                aVar.p$ = (hs0) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<GoalTrackingSummary>>> hs0, qn7<? super tl7> qn7) {
                return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r8)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r8)
                    com.fossil.hs0 r0 = r7.p$
                    com.fossil.dm6$g r1 = r7.this$0
                    com.fossil.dm6 r1 = r1.f811a
                    com.portfolio.platform.data.source.GoalTrackingRepository r1 = com.fossil.dm6.C(r1)
                    java.util.Date r2 = r7.$first
                    java.util.Date r3 = r7.$second
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getSummaries(r2, r3, r5, r7)
                    if (r2 != r4) goto L_0x005b
                    r0 = r4
                    goto L_0x0017
                L_0x005b:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.dm6.g.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public g(dm6 dm6) {
            this.f811a = dm6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<GoalTrackingSummary>>> apply(cl7<? extends Date, ? extends Date> cl7) {
            return or0.c(null, 0, new a(this, (Date) cl7.component1(), (Date) cl7.component2(), null), 3, null);
        }
    }

    @DexIgnore
    public dm6(zl6 zl6, GoalTrackingRepository goalTrackingRepository, no4 no4) {
        pq7.c(zl6, "mView");
        pq7.c(goalTrackingRepository, "mGoalTrackingRepository");
        pq7.c(no4, "mAppExecutors");
        this.p = zl6;
        this.q = goalTrackingRepository;
        this.r = no4;
        LiveData<h47<List<GoalTrackingSummary>>> c2 = ss0.c(this.g, new g(this));
        pq7.b(c2, "Transformations.switchMa\u2026d, true))\n        }\n    }");
        this.n = c2;
    }

    @DexIgnore
    public final GoalTrackingSummary X(Date date, List<GoalTrackingSummary> list) {
        T t;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (lk5.m0(next.getDate(), date)) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final void Y(Date date) {
        q();
        xw7 unused = gu7.d(k(), null, null, new c(this, date, null), 3, null);
    }

    @DexIgnore
    public void Z() {
        this.p.M5(this);
    }

    @DexIgnore
    public final xw7 a0() {
        return gu7.d(k(), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final void b0() {
        LiveData<h47<List<GoalTrackingSummary>>> liveData = this.n;
        zl6 zl6 = this.p;
        if (zl6 != null) {
            liveData.h((am6) zl6, new f(this));
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
    }

    @DexIgnore
    @Override // com.fossil.fl5.a
    public void e(fl5.g gVar) {
        pq7.c(gVar, "report");
        if (gVar.b()) {
            this.p.d();
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        b0();
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", "stop");
        LiveData<h47<List<GoalTrackingSummary>>> liveData = this.n;
        zl6 zl6 = this.p;
        if (zl6 != null) {
            liveData.n((am6) zl6);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
    }

    @DexIgnore
    @Override // com.fossil.yl6
    public void n(Date date) {
        pq7.c(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailPresenter", "addRecord date=" + date);
        xw7 unused = gu7.d(k(), null, null, new a(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.yl6
    public void o(GoalTrackingData goalTrackingData) {
        pq7.c(goalTrackingData, OrmLiteConfigUtil.RAW_DIR_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailPresenter", "deleteRecord GoalTrackingData=" + goalTrackingData);
        xw7 unused = gu7.d(k(), null, null, new b(this, goalTrackingData, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.yl6
    public void p(Date date) {
        pq7.c(date, "date");
        Y(date);
    }

    @DexIgnore
    @Override // com.fossil.yl6
    public void q() {
        LiveData<cu0<GoalTrackingData>> pagedList;
        try {
            Listing<GoalTrackingData> listing = this.o;
            if (listing != null && (pagedList = listing.getPagedList()) != null) {
                zl6 zl6 = this.p;
                if (zl6 != null) {
                    pagedList.n((am6) zl6);
                    return;
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.e("GoalTrackingDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.yl6
    public void r() {
        gp7<tl7> retry;
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", "retry all failed request");
        Listing<GoalTrackingData> listing = this.o;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }

    @DexIgnore
    @Override // com.fossil.yl6
    public void s(Bundle bundle) {
        pq7.c(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.f.getTime());
    }

    @DexIgnore
    @Override // com.fossil.yl6
    public void t(Date date) {
        pq7.c(date, "date");
        xw7 unused = gu7.d(k(), null, null, new d(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.yl6
    public void u() {
        Date O = lk5.O(this.f);
        pq7.b(O, "DateHelper.getNextDate(mDate)");
        t(O);
    }

    @DexIgnore
    @Override // com.fossil.yl6
    public void v() {
        Date P = lk5.P(this.f);
        pq7.b(P, "DateHelper.getPrevDate(mDate)");
        t(P);
    }
}
