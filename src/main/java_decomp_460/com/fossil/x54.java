package com.fossil;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Member;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x54 extends AccessibleObject implements Member {
    @DexIgnore
    public /* final */ AccessibleObject b;
    @DexIgnore
    public /* final */ Member c;

    @DexIgnore
    public <M extends AccessibleObject & Member> x54(M m) {
        i14.l(m);
        this.b = m;
        this.c = m;
    }

    @DexIgnore
    public d64<?> a() {
        return d64.of((Class) getDeclaringClass());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof x54)) {
            return false;
        }
        x54 x54 = (x54) obj;
        return a().equals(x54.a()) && this.c.equals(x54.c);
    }

    @DexIgnore
    @Override // java.lang.reflect.AccessibleObject, java.lang.reflect.AnnotatedElement
    public final <A extends Annotation> A getAnnotation(Class<A> cls) {
        return (A) this.b.getAnnotation(cls);
    }

    @DexIgnore
    public final Annotation[] getAnnotations() {
        return this.b.getAnnotations();
    }

    @DexIgnore
    public final Annotation[] getDeclaredAnnotations() {
        return this.b.getDeclaredAnnotations();
    }

    @DexIgnore
    @Override // java.lang.reflect.Member
    public Class<?> getDeclaringClass() {
        return this.c.getDeclaringClass();
    }

    @DexIgnore
    public final int getModifiers() {
        return this.c.getModifiers();
    }

    @DexIgnore
    public final String getName() {
        return this.c.getName();
    }

    @DexIgnore
    public int hashCode() {
        return this.c.hashCode();
    }

    @DexIgnore
    public final boolean isAccessible() {
        return this.b.isAccessible();
    }

    @DexIgnore
    @Override // java.lang.reflect.AccessibleObject, java.lang.reflect.AnnotatedElement
    public final boolean isAnnotationPresent(Class<? extends Annotation> cls) {
        return this.b.isAnnotationPresent(cls);
    }

    @DexIgnore
    public final boolean isSynthetic() {
        return this.c.isSynthetic();
    }

    @DexIgnore
    @Override // java.lang.reflect.AccessibleObject
    public final void setAccessible(boolean z) throws SecurityException {
        this.b.setAccessible(z);
    }

    @DexIgnore
    public String toString() {
        return this.c.toString();
    }
}
