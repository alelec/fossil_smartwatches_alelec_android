package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qe0 {
    @DexIgnore
    public static /* final */ int accessibility_action_clickable_span; // = 2131361817;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_0; // = 2131361818;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_1; // = 2131361819;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_10; // = 2131361820;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_11; // = 2131361821;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_12; // = 2131361822;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_13; // = 2131361823;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_14; // = 2131361824;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_15; // = 2131361825;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_16; // = 2131361826;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_17; // = 2131361827;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_18; // = 2131361828;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_19; // = 2131361829;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_2; // = 2131361830;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_20; // = 2131361831;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_21; // = 2131361832;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_22; // = 2131361833;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_23; // = 2131361834;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_24; // = 2131361835;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_25; // = 2131361836;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_26; // = 2131361837;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_27; // = 2131361838;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_28; // = 2131361839;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_29; // = 2131361840;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_3; // = 2131361841;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_30; // = 2131361842;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_31; // = 2131361843;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_4; // = 2131361844;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_5; // = 2131361845;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_6; // = 2131361846;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_7; // = 2131361847;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_8; // = 2131361848;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_9; // = 2131361849;
    @DexIgnore
    public static /* final */ int action_bar; // = 2131361857;
    @DexIgnore
    public static /* final */ int action_bar_activity_content; // = 2131361858;
    @DexIgnore
    public static /* final */ int action_bar_container; // = 2131361859;
    @DexIgnore
    public static /* final */ int action_bar_root; // = 2131361860;
    @DexIgnore
    public static /* final */ int action_bar_spinner; // = 2131361861;
    @DexIgnore
    public static /* final */ int action_bar_subtitle; // = 2131361862;
    @DexIgnore
    public static /* final */ int action_bar_title; // = 2131361863;
    @DexIgnore
    public static /* final */ int action_container; // = 2131361864;
    @DexIgnore
    public static /* final */ int action_context_bar; // = 2131361865;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361866;
    @DexIgnore
    public static /* final */ int action_image; // = 2131361867;
    @DexIgnore
    public static /* final */ int action_menu_divider; // = 2131361868;
    @DexIgnore
    public static /* final */ int action_menu_presenter; // = 2131361869;
    @DexIgnore
    public static /* final */ int action_mode_bar; // = 2131361870;
    @DexIgnore
    public static /* final */ int action_mode_bar_stub; // = 2131361871;
    @DexIgnore
    public static /* final */ int action_mode_close_button; // = 2131361872;
    @DexIgnore
    public static /* final */ int action_text; // = 2131361873;
    @DexIgnore
    public static /* final */ int actions; // = 2131361874;
    @DexIgnore
    public static /* final */ int activity_chooser_view_content; // = 2131361875;
    @DexIgnore
    public static /* final */ int add; // = 2131361881;
    @DexIgnore
    public static /* final */ int alertTitle; // = 2131361885;
    @DexIgnore
    public static /* final */ int async; // = 2131361895;
    @DexIgnore
    public static /* final */ int blocking; // = 2131361918;
    @DexIgnore
    public static /* final */ int buttonPanel; // = 2131361985;
    @DexIgnore
    public static /* final */ int checkbox; // = 2131362022;
    @DexIgnore
    public static /* final */ int checked; // = 2131362023;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131362026;
    @DexIgnore
    public static /* final */ int content; // = 2131362158;
    @DexIgnore
    public static /* final */ int contentPanel; // = 2131362159;
    @DexIgnore
    public static /* final */ int custom; // = 2131362170;
    @DexIgnore
    public static /* final */ int customPanel; // = 2131362171;
    @DexIgnore
    public static /* final */ int decor_content_parent; // = 2131362198;
    @DexIgnore
    public static /* final */ int default_activity_button; // = 2131362199;
    @DexIgnore
    public static /* final */ int dialog_button; // = 2131362205;
    @DexIgnore
    public static /* final */ int edit_query; // = 2131362217;
    @DexIgnore
    public static /* final */ int expand_activities_button; // = 2131362249;
    @DexIgnore
    public static /* final */ int expanded_menu; // = 2131362250;
    @DexIgnore
    public static /* final */ int forever; // = 2131362328;
    @DexIgnore
    public static /* final */ int group_divider; // = 2131362570;
    @DexIgnore
    public static /* final */ int home; // = 2131362584;
    @DexIgnore
    public static /* final */ int icon; // = 2131362608;
    @DexIgnore
    public static /* final */ int icon_group; // = 2131362609;
    @DexIgnore
    public static /* final */ int image; // = 2131362616;
    @DexIgnore
    public static /* final */ int info; // = 2131362633;
    @DexIgnore
    public static /* final */ int italic; // = 2131362649;
    @DexIgnore
    public static /* final */ int line1; // = 2131362784;
    @DexIgnore
    public static /* final */ int line3; // = 2131362786;
    @DexIgnore
    public static /* final */ int listMode; // = 2131362793;
    @DexIgnore
    public static /* final */ int list_item; // = 2131362794;
    @DexIgnore
    public static /* final */ int message; // = 2131362844;
    @DexIgnore
    public static /* final */ int multiply; // = 2131362876;
    @DexIgnore
    public static /* final */ int none; // = 2131362882;
    @DexIgnore
    public static /* final */ int normal; // = 2131362883;
    @DexIgnore
    public static /* final */ int notification_background; // = 2131362884;
    @DexIgnore
    public static /* final */ int notification_main_column; // = 2131362885;
    @DexIgnore
    public static /* final */ int notification_main_column_container; // = 2131362886;
    @DexIgnore
    public static /* final */ int off; // = 2131362904;
    @DexIgnore
    public static /* final */ int on; // = 2131362905;
    @DexIgnore
    public static /* final */ int parentPanel; // = 2131362919;
    @DexIgnore
    public static /* final */ int progress_circular; // = 2131362971;
    @DexIgnore
    public static /* final */ int progress_horizontal; // = 2131362972;
    @DexIgnore
    public static /* final */ int radio; // = 2131362975;
    @DexIgnore
    public static /* final */ int right_icon; // = 2131362997;
    @DexIgnore
    public static /* final */ int right_side; // = 2131362998;
    @DexIgnore
    public static /* final */ int screen; // = 2131363080;
    @DexIgnore
    public static /* final */ int scrollIndicatorDown; // = 2131363082;
    @DexIgnore
    public static /* final */ int scrollIndicatorUp; // = 2131363083;
    @DexIgnore
    public static /* final */ int scrollView; // = 2131363084;
    @DexIgnore
    public static /* final */ int search_badge; // = 2131363088;
    @DexIgnore
    public static /* final */ int search_bar; // = 2131363089;
    @DexIgnore
    public static /* final */ int search_button; // = 2131363090;
    @DexIgnore
    public static /* final */ int search_close_btn; // = 2131363091;
    @DexIgnore
    public static /* final */ int search_edit_frame; // = 2131363092;
    @DexIgnore
    public static /* final */ int search_go_btn; // = 2131363093;
    @DexIgnore
    public static /* final */ int search_mag_icon; // = 2131363094;
    @DexIgnore
    public static /* final */ int search_plate; // = 2131363095;
    @DexIgnore
    public static /* final */ int search_src_text; // = 2131363096;
    @DexIgnore
    public static /* final */ int search_voice_btn; // = 2131363098;
    @DexIgnore
    public static /* final */ int select_dialog_listview; // = 2131363102;
    @DexIgnore
    public static /* final */ int shortcut; // = 2131363117;
    @DexIgnore
    public static /* final */ int spacer; // = 2131363134;
    @DexIgnore
    public static /* final */ int split_action_bar; // = 2131363136;
    @DexIgnore
    public static /* final */ int src_atop; // = 2131363140;
    @DexIgnore
    public static /* final */ int src_in; // = 2131363141;
    @DexIgnore
    public static /* final */ int src_over; // = 2131363142;
    @DexIgnore
    public static /* final */ int submenuarrow; // = 2131363153;
    @DexIgnore
    public static /* final */ int submit_area; // = 2131363154;
    @DexIgnore
    public static /* final */ int tabMode; // = 2131363174;
    @DexIgnore
    public static /* final */ int tag_accessibility_actions; // = 2131363176;
    @DexIgnore
    public static /* final */ int tag_accessibility_clickable_spans; // = 2131363177;
    @DexIgnore
    public static /* final */ int tag_accessibility_heading; // = 2131363178;
    @DexIgnore
    public static /* final */ int tag_accessibility_pane_title; // = 2131363179;
    @DexIgnore
    public static /* final */ int tag_screen_reader_focusable; // = 2131363180;
    @DexIgnore
    public static /* final */ int tag_transition_group; // = 2131363181;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_event_manager; // = 2131363182;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_listeners; // = 2131363183;
    @DexIgnore
    public static /* final */ int text; // = 2131363190;
    @DexIgnore
    public static /* final */ int text2; // = 2131363191;
    @DexIgnore
    public static /* final */ int textSpacerNoButtons; // = 2131363194;
    @DexIgnore
    public static /* final */ int textSpacerNoTitle; // = 2131363195;
    @DexIgnore
    public static /* final */ int time; // = 2131363213;
    @DexIgnore
    public static /* final */ int title; // = 2131363215;
    @DexIgnore
    public static /* final */ int titleDividerNoCustom; // = 2131363216;
    @DexIgnore
    public static /* final */ int title_template; // = 2131363218;
    @DexIgnore
    public static /* final */ int topPanel; // = 2131363226;
    @DexIgnore
    public static /* final */ int unchecked; // = 2131363432;
    @DexIgnore
    public static /* final */ int uniform; // = 2131363434;
    @DexIgnore
    public static /* final */ int up; // = 2131363437;
    @DexIgnore
    public static /* final */ int wrap_content; // = 2131363560;
}
