package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RingProgressBar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a55 extends z45 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d O; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray P;
    @DexIgnore
    public /* final */ NestedScrollView M;
    @DexIgnore
    public long N;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        P = sparseIntArray;
        sparseIntArray.put(2131362044, 1);
        P.put(2131363285, 2);
        P.put(2131363482, 3);
        P.put(2131362699, 4);
        P.put(2131362043, 5);
        P.put(2131363284, 6);
        P.put(2131363481, 7);
        P.put(2131362698, 8);
        P.put(2131362083, 9);
        P.put(2131363361, 10);
        P.put(2131363494, 11);
        P.put(2131362706, 12);
        P.put(2131362111, 13);
        P.put(2131363400, 14);
        P.put(2131363520, 15);
        P.put(2131362711, 16);
        P.put(2131362125, 17);
        P.put(2131363018, 18);
        P.put(2131363017, 19);
        P.put(2131363019, 20);
        P.put(2131363020, 21);
        P.put(2131362262, 22);
    }
    */

    @DexIgnore
    public a55(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 23, O, P));
    }

    @DexIgnore
    public a55(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[5], (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[9], (ConstraintLayout) objArr[13], (ConstraintLayout) objArr[17], (FlexibleButton) objArr[22], (ImageView) objArr[8], (ImageView) objArr[4], (ImageView) objArr[12], (ImageView) objArr[16], (RingProgressBar) objArr[19], (RingProgressBar) objArr[18], (RingProgressBar) objArr[20], (RingProgressBar) objArr[21], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[14], (View) objArr[7], (View) objArr[3], (View) objArr[11], (View) objArr[15]);
        this.N = -1;
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.M = nestedScrollView;
        nestedScrollView.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.N = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.N != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.N = 1;
        }
        w();
    }
}
