package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class im1 extends dm1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<im1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public im1 a(Parcel parcel) {
            return new im1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public im1 createFromParcel(Parcel parcel) {
            return new im1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public im1[] newArray(int i) {
            return new im1[i];
        }
    }

    @DexIgnore
    public im1() {
        super(fm1.HEART_RATE, null, null, null, 14);
    }

    @DexIgnore
    public /* synthetic */ im1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    public im1(dt1 dt1, et1 et1) {
        super(fm1.HEART_RATE, null, dt1, et1, 2);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ im1(dt1 dt1, et1 et1, int i, kq7 kq7) {
        this(dt1, (i & 2) != 0 ? new et1(et1.CREATOR.a()) : et1);
    }
}
