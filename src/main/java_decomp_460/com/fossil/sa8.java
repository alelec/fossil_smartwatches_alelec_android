package com.fossil;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.fossil.ve0;
import java.util.ArrayList;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sa8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Activity f3229a;
    @DexIgnore
    public List<String> b; // = new ArrayList();
    @DexIgnore
    public List<String> c; // = new ArrayList();
    @DexIgnore
    public List<String> d; // = new ArrayList();
    @DexIgnore
    public int e;
    @DexIgnore
    public ra8 f;
    @DexIgnore
    public List<String> g;
    @DexIgnore
    public List<String> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements DialogInterface.OnClickListener {
        @DexIgnore
        public a(sa8 sa8) {
        }

        @DexIgnore
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements DialogInterface.OnClickListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onClick(DialogInterface dialogInterface, int i) {
            sa8 sa8 = sa8.this;
            sa8.e(sa8.f3229a);
            dialogInterface.dismiss();
        }
    }

    @DexIgnore
    public final boolean b(String[] strArr, String... strArr2) {
        if (Build.VERSION.SDK_INT < 23) {
            return true;
        }
        h();
        if (strArr != null) {
            if (strArr.length != strArr2.length) {
                throw new IndexOutOfBoundsException("\u4f20\u5165\u7684\u63d0\u793a\u6570\u7ec4\u548c\u9700\u8981\u7533\u8bf7\u7684\u6743\u9650\u6570\u7ec4\u957f\u5ea6\u4e0d\u4e00\u81f4");
            } else if (this.g == null) {
                this.g = new ArrayList();
            }
        }
        for (int i = 0; i < strArr2.length; i++) {
            if (this.f3229a.checkSelfPermission(strArr2[i]) == -1) {
                this.b.add(strArr2[i]);
                if (strArr != null) {
                    this.g.add(strArr[i]);
                }
            }
        }
        return this.b.isEmpty();
    }

    @DexIgnore
    public sa8 c(int i, String[] strArr, int[] iArr) {
        List<String> list;
        if (i == this.e) {
            for (int i2 = 0; i2 < strArr.length; i2++) {
                xa8.b("\u8fd4\u56de\u6743\u9650\u5217\u8868" + strArr[i2]);
                if (iArr[i2] == -1) {
                    this.c.add(strArr[i2]);
                    if (this.g != null && this.h == null) {
                        this.h = new ArrayList();
                    }
                    if (!(this.h == null || (list = this.g) == null || list.size() <= 0)) {
                        this.h.add(this.g.get(i2));
                    }
                } else if (iArr[i2] == 0) {
                    this.d.add(strArr[i2]);
                }
            }
            if (!this.c.isEmpty()) {
                List<String> list2 = this.g;
                if (list2 != null && list2.size() > 0) {
                    j();
                }
                this.f.b(this.c, this.d);
            } else {
                this.f.a();
            }
        }
        return this;
    }

    @DexIgnore
    public final CharSequence d() {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        int i = 0;
        int i2 = 0;
        while (i2 < this.c.size()) {
            String str = this.c.get(i2).split("\\.")[2];
            spannableStringBuilder.append((CharSequence) str);
            spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.parseColor("#37ADA4")), i, str.length() + i, 33);
            spannableStringBuilder.append((CharSequence) "\uff1a");
            spannableStringBuilder.append((CharSequence) this.h.get(i2));
            int length = this.h.get(i2).length() + str.length() + i + 2;
            if (i2 != this.c.size() - 1) {
                spannableStringBuilder.append((CharSequence) "\n");
            }
            i2++;
            i = length;
        }
        return spannableStringBuilder;
    }

    @DexIgnore
    public void e(Context context) {
        Intent intent = new Intent();
        intent.addFlags(SQLiteDatabase.CREATE_IF_NECESSARY);
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, context.getPackageName(), null));
        context.startActivity(intent);
    }

    @DexIgnore
    public sa8 f(Activity activity, int i, List<String> list) {
        g(activity, i, null, (String[]) list.toArray(new String[0]));
        return this;
    }

    @DexIgnore
    @TargetApi(23)
    public final sa8 g(Activity activity, int i, String[] strArr, String... strArr2) {
        if (this.f3229a != null) {
            this.e = i;
            if (!b(strArr, strArr2)) {
                Activity activity2 = this.f3229a;
                List<String> list = this.b;
                rk0.u(activity2, (String[]) list.toArray(new String[list.size()]), i);
                for (int i2 = 0; i2 < this.b.size(); i2++) {
                    xa8.b("\u9700\u8981\u7533\u8bf7\u7684\u6743\u9650\u5217\u8868" + this.b.get(i2));
                }
            } else {
                ra8 ra8 = this.f;
                if (ra8 != null) {
                    ra8.a();
                }
            }
            return this;
        }
        throw new NullPointerException("\u83b7\u53d6\u6743\u9650\u7684Activity\u4e0d\u5b58\u5728");
    }

    @DexIgnore
    public final void h() {
        List<String> list = this.c;
        if (list != null) {
            list.clear();
        }
        List<String> list2 = this.h;
        if (list2 != null) {
            list2.clear();
        }
        List<String> list3 = this.b;
        if (list3 != null) {
            list3.clear();
        }
        List<String> list4 = this.g;
        if (list4 != null) {
            list4.clear();
        }
    }

    @DexIgnore
    public sa8 i(ra8 ra8) {
        this.f = ra8;
        return this;
    }

    @DexIgnore
    public final void j() {
        ve0.a aVar = new ve0.a(this.f3229a);
        aVar.o(d());
        aVar.l("\u53bb\u8bbe\u7f6e", new b());
        aVar.h("\u53d6\u6d88", new a(this));
        aVar.a().show();
    }

    @DexIgnore
    public sa8 k(Activity activity) {
        this.f3229a = activity;
        return this;
    }
}
