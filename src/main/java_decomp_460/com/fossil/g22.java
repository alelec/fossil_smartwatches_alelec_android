package com.fossil;

import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g22 implements Factory<f22> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<Executor> f1249a;
    @DexIgnore
    public /* final */ Provider<k22> b;
    @DexIgnore
    public /* final */ Provider<h22> c;
    @DexIgnore
    public /* final */ Provider<s32> d;

    @DexIgnore
    public g22(Provider<Executor> provider, Provider<k22> provider2, Provider<h22> provider3, Provider<s32> provider4) {
        this.f1249a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static g22 a(Provider<Executor> provider, Provider<k22> provider2, Provider<h22> provider3, Provider<s32> provider4) {
        return new g22(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    /* renamed from: b */
    public f22 get() {
        return new f22(this.f1249a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
