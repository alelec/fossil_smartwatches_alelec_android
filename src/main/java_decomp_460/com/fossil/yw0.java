package com.fossil;

import com.fossil.tn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yw0 implements tn7.b {
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ rn7 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements tn7.c<yw0> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public final rn7 b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.tn7
    public <R> R fold(R r, vp7<? super R, ? super tn7.b, ? extends R> vp7) {
        pq7.c(vp7, "operation");
        return (R) tn7.b.a.a(this, r, vp7);
    }

    @DexIgnore
    @Override // com.fossil.tn7, com.fossil.tn7.b
    public <E extends tn7.b> E get(tn7.c<E> cVar) {
        pq7.c(cVar, "key");
        return (E) tn7.b.a.b(this, cVar);
    }

    @DexIgnore
    @Override // com.fossil.tn7.b
    public tn7.c<yw0> getKey() {
        return c;
    }

    @DexIgnore
    @Override // com.fossil.tn7
    public tn7 minusKey(tn7.c<?> cVar) {
        pq7.c(cVar, "key");
        return tn7.b.a.c(this, cVar);
    }

    @DexIgnore
    @Override // com.fossil.tn7
    public tn7 plus(tn7 tn7) {
        pq7.c(tn7, "context");
        return tn7.b.a.d(this, tn7);
    }
}
