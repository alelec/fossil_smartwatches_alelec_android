package com.fossil;

import android.os.RemoteException;
import com.fossil.zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rt2 extends zs2.a {
    @DexIgnore
    public /* final */ /* synthetic */ int f; // = 5;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ Object h;
    @DexIgnore
    public /* final */ /* synthetic */ Object i;
    @DexIgnore
    public /* final */ /* synthetic */ Object j;
    @DexIgnore
    public /* final */ /* synthetic */ zs2 k;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public rt2(zs2 zs2, boolean z, int i2, String str, Object obj, Object obj2, Object obj3) {
        super(false);
        this.k = zs2;
        this.g = str;
        this.h = obj;
        this.i = null;
        this.j = null;
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void a() throws RemoteException {
        this.k.h.logHealthData(this.f, this.g, tg2.n(this.h), tg2.n(this.i), tg2.n(this.j));
    }
}
