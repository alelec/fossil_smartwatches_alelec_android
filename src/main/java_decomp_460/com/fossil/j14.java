package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface j14<T> {
    @DexIgnore
    @CanIgnoreReturnValue
    boolean apply(T t);

    @DexIgnore
    boolean equals(Object obj);
}
