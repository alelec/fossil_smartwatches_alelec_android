package com.fossil;

import com.facebook.share.internal.VideoUploader;
import com.fossil.l56;
import com.fossil.tq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.InstalledApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i56 extends c56 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public /* final */ List<i06> e; // = new ArrayList();
    @DexIgnore
    public /* final */ d56 f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ ArrayList<String> h;
    @DexIgnore
    public /* final */ uq4 i;
    @DexIgnore
    public /* final */ l56 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return i56.k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements rp7<i06, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ i56 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(i56 i56) {
            super(1);
            this.this$0 = i56;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ Boolean invoke(i06 i06) {
            return Boolean.valueOf(invoke(i06));
        }

        @DexIgnore
        public final boolean invoke(i06 i06) {
            pq7.c(i06, "it");
            InstalledApp installedApp = i06.getInstalledApp();
            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
            if (isSelected != null) {
                return isSelected.booleanValue() && i06.getCurrentHandGroup() == this.this$0.g;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qq7 implements rp7<i06, String> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(1);
        }

        @DexIgnore
        public final String invoke(i06 i06) {
            pq7.c(i06, "it");
            return String.valueOf(i06.getUri());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements tq4.d<l56.a, tq4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ i56 f1586a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(i56 i56) {
            this.f1586a = i56;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(tq4.a aVar) {
            pq7.c(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(i56.l.a(), "mGetApps onError");
            this.f1586a.f.k();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(l56.a aVar) {
            InstalledApp installedApp;
            pq7.c(aVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(i56.l.a(), "mGetApps onSuccess");
            ArrayList<i06> arrayList = new ArrayList(aVar.a());
            for (i06 i06 : arrayList) {
                if (i06.getUri() != null) {
                    if (this.f1586a.h.contains(String.valueOf(i06.getUri()))) {
                        InstalledApp installedApp2 = i06.getInstalledApp();
                        if (installedApp2 != null) {
                            installedApp2.setSelected(true);
                        }
                        i06.setCurrentHandGroup(this.f1586a.g);
                    } else if (i06.getCurrentHandGroup() == this.f1586a.g && (installedApp = i06.getInstalledApp()) != null) {
                        installedApp.setSelected(false);
                    }
                }
            }
            this.f1586a.u().addAll(arrayList);
            this.f1586a.f.y2(this.f1586a.u(), this.f1586a.g);
            this.f1586a.f.k();
        }
    }

    /*
    static {
        String simpleName = i56.class.getSimpleName();
        pq7.b(simpleName, "NotificationHybridAppPre\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public i56(d56 d56, int i2, ArrayList<String> arrayList, uq4 uq4, l56 l56) {
        pq7.c(d56, "mView");
        pq7.c(arrayList, "mListAppsSelected");
        pq7.c(uq4, "mUseCaseHandler");
        pq7.c(l56, "mGetHybridApp");
        this.f = d56;
        this.g = i2;
        this.h = arrayList;
        this.i = uq4;
        this.j = l56;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(k, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (this.e.isEmpty()) {
            this.f.m();
            this.i.a(this.j, null, new d(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    @Override // com.fossil.c56
    public int n() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.c56
    public void o() {
        this.f.I(new ArrayList<>(at7.t(at7.o(at7.h(pm7.z(this.e), new b(this)), c.INSTANCE))));
    }

    @DexIgnore
    @Override // com.fossil.c56
    public void p(i06 i06, boolean z) {
        T t;
        int i2;
        pq7.c(i06, "appWrapper");
        FLogger.INSTANCE.getLocal().d(k, "setAppState: appWrapper=" + i06 + ", selected=" + z);
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (pq7.a(next.getUri(), i06.getUri())) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            InstalledApp installedApp = t2.getInstalledApp();
            if (installedApp != null) {
                installedApp.setSelected(z);
            }
            InstalledApp installedApp2 = t2.getInstalledApp();
            Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
            if (isSelected == null) {
                pq7.i();
                throw null;
            } else if (isSelected.booleanValue() && t2.getCurrentHandGroup() != (i2 = this.g)) {
                t2.setCurrentHandGroup(i2);
            }
        }
    }

    @DexIgnore
    public final List<i06> u() {
        return this.e;
    }

    @DexIgnore
    public void v() {
        this.f.M5(this);
    }
}
