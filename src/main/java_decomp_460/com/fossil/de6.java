package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class de6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ xd6 f777a;
    @DexIgnore
    public /* final */ oe6 b;
    @DexIgnore
    public /* final */ ie6 c;

    @DexIgnore
    public de6(xd6 xd6, oe6 oe6, ie6 ie6) {
        pq7.c(xd6, "mActiveTimeOverviewDayView");
        pq7.c(oe6, "mActiveTimeOverviewWeekView");
        pq7.c(ie6, "mActiveTimeOverviewMonthView");
        this.f777a = xd6;
        this.b = oe6;
        this.c = ie6;
    }

    @DexIgnore
    public final xd6 a() {
        return this.f777a;
    }

    @DexIgnore
    public final ie6 b() {
        return this.c;
    }

    @DexIgnore
    public final oe6 c() {
        return this.b;
    }
}
