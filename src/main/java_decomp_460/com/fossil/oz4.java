package com.fossil;

import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oz4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Gson f2755a;

    @DexIgnore
    public oz4() {
        Gson d = new zi4().d();
        pq7.b(d, "GsonBuilder().create()");
        this.f2755a = d;
    }

    @DexIgnore
    public final String a(Date date) {
        String str;
        if (date == null) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = lk5.n.get();
            if (simpleDateFormat != null) {
                str = simpleDateFormat.format(date);
                return str;
            }
            pq7.i();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("ChallengeConverter", "fromOffsetDateTime - e=" + e);
            str = null;
        }
    }

    @DexIgnore
    public final String b(ht4 ht4) {
        pq7.c(ht4, "owner");
        try {
            return this.f2755a.u(ht4, ht4.class);
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final ht4 c(String str) {
        pq7.c(str, "value");
        try {
            return (ht4) this.f2755a.k(str, ht4.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final Date d(String str) {
        Date date;
        if (str == null) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = lk5.n.get();
            if (simpleDateFormat != null) {
                date = simpleDateFormat.parse(str);
                return date;
            }
            pq7.i();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("ChallengeConverter", "toOffsetDateTime - e=" + e);
            date = null;
        }
    }
}
