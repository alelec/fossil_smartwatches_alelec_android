package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ad2 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends RuntimeException {
        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public a(java.lang.String r5, android.os.Parcel r6) {
            /*
                r4 = this;
                int r0 = r6.dataPosition()
                int r1 = r6.dataSize()
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                java.lang.String r3 = java.lang.String.valueOf(r5)
                int r3 = r3.length()
                int r3 = r3 + 41
                r2.<init>(r3)
                r2.append(r5)
                java.lang.String r3 = " Parcel: pos="
                r2.append(r3)
                r2.append(r0)
                java.lang.String r0 = " size="
                r2.append(r0)
                r2.append(r1)
                java.lang.String r0 = r2.toString()
                r4.<init>(r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ad2.a.<init>(java.lang.String, android.os.Parcel):void");
        }
    }

    @DexIgnore
    public static int A(Parcel parcel, int i) {
        return (i & -65536) != -65536 ? (i >> 16) & 65535 : parcel.readInt();
    }

    @DexIgnore
    public static void B(Parcel parcel, int i) {
        parcel.setDataPosition(A(parcel, i) + parcel.dataPosition());
    }

    @DexIgnore
    public static int C(Parcel parcel) {
        int t = t(parcel);
        int A = A(parcel, t);
        int dataPosition = parcel.dataPosition();
        if (l(t) != 20293) {
            String valueOf = String.valueOf(Integer.toHexString(t));
            throw new a(valueOf.length() != 0 ? "Expected object header. Got 0x".concat(valueOf) : new String("Expected object header. Got 0x"), parcel);
        }
        int i = A + dataPosition;
        if (i >= dataPosition && i <= parcel.dataSize()) {
            return i;
        }
        StringBuilder sb = new StringBuilder(54);
        sb.append("Size read is invalid start=");
        sb.append(dataPosition);
        sb.append(" end=");
        sb.append(i);
        throw new a(sb.toString(), parcel);
    }

    @DexIgnore
    public static void D(Parcel parcel, int i, int i2) {
        int A = A(parcel, i);
        if (A != i2) {
            String hexString = Integer.toHexString(A);
            StringBuilder sb = new StringBuilder(String.valueOf(hexString).length() + 46);
            sb.append("Expected size ");
            sb.append(i2);
            sb.append(" got ");
            sb.append(A);
            sb.append(" (0x");
            sb.append(hexString);
            sb.append(")");
            throw new a(sb.toString(), parcel);
        }
    }

    @DexIgnore
    public static void E(Parcel parcel, int i, int i2, int i3) {
        if (i2 != i3) {
            String hexString = Integer.toHexString(i2);
            StringBuilder sb = new StringBuilder(String.valueOf(hexString).length() + 46);
            sb.append("Expected size ");
            sb.append(i3);
            sb.append(" got ");
            sb.append(i2);
            sb.append(" (0x");
            sb.append(hexString);
            sb.append(")");
            throw new a(sb.toString(), parcel);
        }
    }

    @DexIgnore
    public static Bundle a(Parcel parcel, int i) {
        int A = A(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A == 0) {
            return null;
        }
        Bundle readBundle = parcel.readBundle();
        parcel.setDataPosition(A + dataPosition);
        return readBundle;
    }

    @DexIgnore
    public static byte[] b(Parcel parcel, int i) {
        int A = A(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A == 0) {
            return null;
        }
        byte[] createByteArray = parcel.createByteArray();
        parcel.setDataPosition(A + dataPosition);
        return createByteArray;
    }

    @DexIgnore
    public static float[] c(Parcel parcel, int i) {
        int A = A(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A == 0) {
            return null;
        }
        float[] createFloatArray = parcel.createFloatArray();
        parcel.setDataPosition(A + dataPosition);
        return createFloatArray;
    }

    @DexIgnore
    public static int[] d(Parcel parcel, int i) {
        int A = A(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A == 0) {
            return null;
        }
        int[] createIntArray = parcel.createIntArray();
        parcel.setDataPosition(A + dataPosition);
        return createIntArray;
    }

    @DexIgnore
    public static <T extends Parcelable> T e(Parcel parcel, int i, Parcelable.Creator<T> creator) {
        int A = A(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A == 0) {
            return null;
        }
        T createFromParcel = creator.createFromParcel(parcel);
        parcel.setDataPosition(A + dataPosition);
        return createFromParcel;
    }

    @DexIgnore
    public static String f(Parcel parcel, int i) {
        int A = A(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A == 0) {
            return null;
        }
        String readString = parcel.readString();
        parcel.setDataPosition(A + dataPosition);
        return readString;
    }

    @DexIgnore
    public static String[] g(Parcel parcel, int i) {
        int A = A(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A == 0) {
            return null;
        }
        String[] createStringArray = parcel.createStringArray();
        parcel.setDataPosition(A + dataPosition);
        return createStringArray;
    }

    @DexIgnore
    public static ArrayList<String> h(Parcel parcel, int i) {
        int A = A(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A == 0) {
            return null;
        }
        ArrayList<String> createStringArrayList = parcel.createStringArrayList();
        parcel.setDataPosition(A + dataPosition);
        return createStringArrayList;
    }

    @DexIgnore
    public static <T> T[] i(Parcel parcel, int i, Parcelable.Creator<T> creator) {
        int A = A(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A == 0) {
            return null;
        }
        T[] tArr = (T[]) parcel.createTypedArray(creator);
        parcel.setDataPosition(A + dataPosition);
        return tArr;
    }

    @DexIgnore
    public static <T> ArrayList<T> j(Parcel parcel, int i, Parcelable.Creator<T> creator) {
        int A = A(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A == 0) {
            return null;
        }
        ArrayList<T> createTypedArrayList = parcel.createTypedArrayList(creator);
        parcel.setDataPosition(A + dataPosition);
        return createTypedArrayList;
    }

    @DexIgnore
    public static void k(Parcel parcel, int i) {
        if (parcel.dataPosition() != i) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Overread allowed size end=");
            sb.append(i);
            throw new a(sb.toString(), parcel);
        }
    }

    @DexIgnore
    public static int l(int i) {
        return 65535 & i;
    }

    @DexIgnore
    public static boolean m(Parcel parcel, int i) {
        D(parcel, i, 4);
        return parcel.readInt() != 0;
    }

    @DexIgnore
    public static Boolean n(Parcel parcel, int i) {
        int A = A(parcel, i);
        if (A == 0) {
            return null;
        }
        E(parcel, i, A, 4);
        return Boolean.valueOf(parcel.readInt() != 0);
    }

    @DexIgnore
    public static byte o(Parcel parcel, int i) {
        D(parcel, i, 4);
        return (byte) parcel.readInt();
    }

    @DexIgnore
    public static double p(Parcel parcel, int i) {
        D(parcel, i, 8);
        return parcel.readDouble();
    }

    @DexIgnore
    public static Double q(Parcel parcel, int i) {
        int A = A(parcel, i);
        if (A == 0) {
            return null;
        }
        E(parcel, i, A, 8);
        return Double.valueOf(parcel.readDouble());
    }

    @DexIgnore
    public static float r(Parcel parcel, int i) {
        D(parcel, i, 4);
        return parcel.readFloat();
    }

    @DexIgnore
    public static Float s(Parcel parcel, int i) {
        int A = A(parcel, i);
        if (A == 0) {
            return null;
        }
        E(parcel, i, A, 4);
        return Float.valueOf(parcel.readFloat());
    }

    @DexIgnore
    public static int t(Parcel parcel) {
        return parcel.readInt();
    }

    @DexIgnore
    public static IBinder u(Parcel parcel, int i) {
        int A = A(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A == 0) {
            return null;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        parcel.setDataPosition(A + dataPosition);
        return readStrongBinder;
    }

    @DexIgnore
    public static int v(Parcel parcel, int i) {
        D(parcel, i, 4);
        return parcel.readInt();
    }

    @DexIgnore
    public static Integer w(Parcel parcel, int i) {
        int A = A(parcel, i);
        if (A == 0) {
            return null;
        }
        E(parcel, i, A, 4);
        return Integer.valueOf(parcel.readInt());
    }

    @DexIgnore
    public static void x(Parcel parcel, int i, List list, ClassLoader classLoader) {
        int A = A(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A != 0) {
            parcel.readList(list, classLoader);
            parcel.setDataPosition(A + dataPosition);
        }
    }

    @DexIgnore
    public static long y(Parcel parcel, int i) {
        D(parcel, i, 8);
        return parcel.readLong();
    }

    @DexIgnore
    public static Long z(Parcel parcel, int i) {
        int A = A(parcel, i);
        if (A == 0) {
            return null;
        }
        E(parcel, i, A, 8);
        return Long.valueOf(parcel.readLong());
    }
}
