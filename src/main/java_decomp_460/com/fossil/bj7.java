package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import com.zendesk.belvedere.BelvedereCallback;
import com.zendesk.belvedere.BelvedereResult;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bj7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ zi7 f439a;
    @DexIgnore
    public /* final */ gj7 b;
    @DexIgnore
    public /* final */ Map<Integer, BelvedereResult> c; // = new HashMap();
    @DexIgnore
    public /* final */ dj7 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f440a;

        /*
        static {
            int[] iArr = new int[fj7.values().length];
            f440a = iArr;
            try {
                iArr[fj7.Gallery.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f440a[fj7.Camera.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
        */
    }

    @DexIgnore
    public bj7(zi7 zi7, gj7 gj7) {
        this.f439a = zi7;
        this.b = gj7;
        this.d = zi7.b();
    }

    @DexIgnore
    public final boolean a(Context context) {
        if (h(context)) {
            if (!j(context, "android.permission.CAMERA")) {
                return true;
            }
            if (gl0.a(context, "android.permission.CAMERA") == 0) {
                return true;
            }
            this.d.w("BelvedereImagePicker", "Found Camera permission declared in AndroidManifest.xml and the user hasn't granted that permission. Not doing any further efforts to acquire that permission.");
        }
        return false;
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public final List<Uri> b(Intent intent) {
        ArrayList arrayList = new ArrayList();
        if (Build.VERSION.SDK_INT >= 16 && intent.getClipData() != null) {
            ClipData clipData = intent.getClipData();
            int itemCount = clipData.getItemCount();
            for (int i = 0; i < itemCount; i++) {
                ClipData.Item itemAt = clipData.getItemAt(i);
                if (itemAt.getUri() != null) {
                    arrayList.add(itemAt.getUri());
                }
            }
        } else if (intent.getData() != null) {
            arrayList.add(intent.getData());
        }
        return arrayList;
    }

    @DexIgnore
    public List<cj7> c(Context context) {
        TreeSet<fj7> c2 = this.f439a.c();
        ArrayList arrayList = new ArrayList();
        Iterator<fj7> it = c2.iterator();
        while (it.hasNext()) {
            int i = a.f440a[it.next().ordinal()];
            cj7 d2 = i != 1 ? i != 2 ? null : d(context) : g(context);
            if (d2 != null) {
                arrayList.add(d2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final cj7 d(Context context) {
        if (a(context)) {
            return n(context);
        }
        return null;
    }

    @DexIgnore
    public void e(Context context, int i, int i2, Intent intent, BelvedereCallback<List<BelvedereResult>> belvedereCallback) {
        ArrayList arrayList = new ArrayList();
        if (i == this.f439a.h()) {
            this.d.d("BelvedereImagePicker", String.format(Locale.US, "Parsing activity result - Gallery - Ok: %s", Boolean.valueOf(i2 == -1)));
            if (i2 == -1) {
                List<Uri> b2 = b(intent);
                this.d.d("BelvedereImagePicker", String.format(Locale.US, "Number of items received from gallery: %s", Integer.valueOf(b2.size())));
                new ej7(context, this.d, this.b, belvedereCallback).execute(b2.toArray(new Uri[b2.size()]));
                return;
            }
        } else if (this.c.containsKey(Integer.valueOf(i))) {
            this.d.d("BelvedereImagePicker", String.format(Locale.US, "Parsing activity result - Camera - Ok: %s", Boolean.valueOf(i2 == -1)));
            BelvedereResult belvedereResult = this.c.get(Integer.valueOf(i));
            this.b.l(context, belvedereResult.b(), 3);
            if (i2 == -1) {
                arrayList.add(belvedereResult);
                this.d.d("BelvedereImagePicker", String.format(Locale.US, "Image from camera: %s", belvedereResult.a()));
            }
            this.c.remove(Integer.valueOf(i));
        }
        if (belvedereCallback != null) {
            belvedereCallback.internalSuccess(arrayList);
        }
    }

    @DexIgnore
    @TargetApi(19)
    public final Intent f() {
        Intent intent;
        if (Build.VERSION.SDK_INT >= 19) {
            this.d.d("BelvedereImagePicker", "Gallery Intent, using 'ACTION_OPEN_DOCUMENT'");
            intent = new Intent("android.intent.action.OPEN_DOCUMENT");
        } else {
            this.d.d("BelvedereImagePicker", "Gallery Intent, using 'ACTION_GET_CONTENT'");
            intent = new Intent("android.intent.action.GET_CONTENT");
        }
        intent.setType(this.f439a.f());
        intent.addCategory("android.intent.category.OPENABLE");
        if (Build.VERSION.SDK_INT >= 18) {
            intent.putExtra("android.intent.extra.ALLOW_MULTIPLE", this.f439a.a());
        }
        return intent;
    }

    @DexIgnore
    public cj7 g(Context context) {
        if (i(context)) {
            return new cj7(f(), this.f439a.h(), fj7.Gallery);
        }
        return null;
    }

    @DexIgnore
    public final boolean h(Context context) {
        Intent intent = new Intent();
        intent.setAction("android.media.action.IMAGE_CAPTURE");
        PackageManager packageManager = context.getPackageManager();
        boolean z = packageManager.hasSystemFeature("android.hardware.camera") || packageManager.hasSystemFeature("android.hardware.camera.front");
        boolean l = l(intent, context);
        this.d.d("BelvedereImagePicker", String.format(Locale.US, "Camera present: %b, Camera App present: %b", Boolean.valueOf(z), Boolean.valueOf(l)));
        return z && l;
    }

    @DexIgnore
    public final boolean i(Context context) {
        return l(f(), context);
    }

    @DexIgnore
    public final boolean j(Context context, String str) {
        try {
            String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
            if (strArr == null || strArr.length <= 0) {
                return false;
            }
            for (String str2 : strArr) {
                if (str2.equals(str)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            this.d.e("BelvedereImagePicker", "Not able to find permissions in manifest", e);
            return false;
        }
    }

    @DexIgnore
    public boolean k(fj7 fj7, Context context) {
        if (!this.f439a.c().contains(fj7)) {
            return false;
        }
        int i = a.f440a[fj7.ordinal()];
        if (i == 1) {
            return i(context);
        }
        if (i == 2) {
            return a(context);
        }
        return false;
    }

    @DexIgnore
    public final boolean l(Intent intent, Context context) {
        return intent.resolveActivity(context.getPackageManager()) != null;
    }

    @DexIgnore
    public boolean m(Context context) {
        for (fj7 fj7 : fj7.values()) {
            if (k(fj7, context)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final cj7 n(Context context) {
        Set<Integer> keySet = this.c.keySet();
        int d2 = this.f439a.d();
        int e = this.f439a.e();
        while (true) {
            if (e >= this.f439a.d()) {
                e = d2;
                break;
            } else if (!keySet.contains(Integer.valueOf(e))) {
                break;
            } else {
                e++;
            }
        }
        File d3 = this.b.d(context);
        if (d3 == null) {
            this.d.w("BelvedereImagePicker", "Camera Intent. Image path is null. There's something wrong with the storage.");
            return null;
        }
        Uri g = this.b.g(context, d3);
        if (g == null) {
            this.d.w("BelvedereImagePicker", "Camera Intent: Uri to file is null. There's something wrong with the storage or FileProvider configuration.");
            return null;
        }
        this.c.put(Integer.valueOf(e), new BelvedereResult(d3, g));
        this.d.d("BelvedereImagePicker", String.format(Locale.US, "Camera Intent: Request Id: %s - File: %s - Uri: %s", Integer.valueOf(e), d3, g));
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", g);
        this.b.k(context, intent, g, 3);
        return new cj7(intent, e, fj7.Camera);
    }
}
