package com.fossil;

import com.fossil.p18;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h38 implements s28 {
    @DexIgnore
    public static /* final */ List<String> f; // = b28.u("connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade", ":method", ":path", ":scheme", ":authority");
    @DexIgnore
    public static /* final */ List<String> g; // = b28.u("connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Interceptor.Chain f1421a;
    @DexIgnore
    public /* final */ p28 b;
    @DexIgnore
    public /* final */ i38 c;
    @DexIgnore
    public k38 d;
    @DexIgnore
    public /* final */ t18 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends n48 {
        @DexIgnore
        public boolean c; // = false;
        @DexIgnore
        public long d; // = 0;

        @DexIgnore
        public a(c58 c58) {
            super(c58);
        }

        @DexIgnore
        public final void b(IOException iOException) {
            if (!this.c) {
                this.c = true;
                h38 h38 = h38.this;
                h38.b.r(false, h38, this.d, iOException);
            }
        }

        @DexIgnore
        @Override // com.fossil.n48, java.io.Closeable, com.fossil.c58, java.lang.AutoCloseable
        public void close() throws IOException {
            super.close();
            b(null);
        }

        @DexIgnore
        @Override // com.fossil.n48, com.fossil.c58
        public long d0(i48 i48, long j) throws IOException {
            try {
                long d0 = a().d0(i48, j);
                if (d0 > 0) {
                    this.d += d0;
                }
                return d0;
            } catch (IOException e2) {
                b(e2);
                throw e2;
            }
        }
    }

    @DexIgnore
    public h38(OkHttpClient okHttpClient, Interceptor.Chain chain, p28 p28, i38 i38) {
        this.f1421a = chain;
        this.b = p28;
        this.c = i38;
        this.e = okHttpClient.C().contains(t18.H2_PRIOR_KNOWLEDGE) ? t18.H2_PRIOR_KNOWLEDGE : t18.HTTP_2;
    }

    @DexIgnore
    public static List<e38> g(v18 v18) {
        p18 e2 = v18.e();
        ArrayList arrayList = new ArrayList(e2.h() + 4);
        arrayList.add(new e38(e38.f, v18.g()));
        arrayList.add(new e38(e38.g, y28.c(v18.j())));
        String c2 = v18.c("Host");
        if (c2 != null) {
            arrayList.add(new e38(e38.i, c2));
        }
        arrayList.add(new e38(e38.h, v18.j().E()));
        int h = e2.h();
        for (int i = 0; i < h; i++) {
            l48 encodeUtf8 = l48.encodeUtf8(e2.e(i).toLowerCase(Locale.US));
            if (!f.contains(encodeUtf8.utf8())) {
                arrayList.add(new e38(encodeUtf8, e2.i(i)));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static Response.a h(p18 p18, t18 t18) throws IOException {
        p18.a aVar = new p18.a();
        int h = p18.h();
        a38 a38 = null;
        for (int i = 0; i < h; i++) {
            String e2 = p18.e(i);
            String i2 = p18.i(i);
            if (e2.equals(":status")) {
                a38 = a38.a("HTTP/1.1 " + i2);
            } else if (!g.contains(e2)) {
                z18.f4406a.b(aVar, e2, i2);
            }
        }
        if (a38 != null) {
            Response.a aVar2 = new Response.a();
            aVar2.n(t18);
            aVar2.g(a38.b);
            aVar2.k(a38.c);
            aVar2.j(aVar.e());
            return aVar2;
        }
        throw new ProtocolException("Expected ':status' header not present");
    }

    @DexIgnore
    @Override // com.fossil.s28
    public void a() throws IOException {
        this.d.j().close();
    }

    @DexIgnore
    @Override // com.fossil.s28
    public void b(v18 v18) throws IOException {
        if (this.d == null) {
            k38 M = this.c.M(g(v18), v18.a() != null);
            this.d = M;
            M.n().g((long) this.f1421a.a(), TimeUnit.MILLISECONDS);
            this.d.u().g((long) this.f1421a.b(), TimeUnit.MILLISECONDS);
        }
    }

    @DexIgnore
    @Override // com.fossil.s28
    public w18 c(Response response) throws IOException {
        p28 p28 = this.b;
        p28.f.q(p28.e);
        return new x28(response.j("Content-Type"), u28.b(response), s48.d(new a(this.d.k())));
    }

    @DexIgnore
    @Override // com.fossil.s28
    public void cancel() {
        k38 k38 = this.d;
        if (k38 != null) {
            k38.h(d38.CANCEL);
        }
    }

    @DexIgnore
    @Override // com.fossil.s28
    public Response.a d(boolean z) throws IOException {
        Response.a h = h(this.d.s(), this.e);
        if (!z || z18.f4406a.d(h) != 100) {
            return h;
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.s28
    public void e() throws IOException {
        this.c.flush();
    }

    @DexIgnore
    @Override // com.fossil.s28
    public a58 f(v18 v18, long j) {
        return this.d.j();
    }
}
