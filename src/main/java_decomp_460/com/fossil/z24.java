package com.fossil;

import com.fossil.a34;
import com.fossil.f34;
import com.fossil.y24;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import com.google.j2objc.annotations.RetainedWith;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z24<K, V> extends f34<K, V> implements s34<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    @RetainedWith
    @LazyInit
    public transient z24<V, K> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends f34.c<K, V> {
        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.f34.c
        public /* bridge */ /* synthetic */ f34.c b(Object obj, Object obj2) {
            f(obj, obj2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.f34.c
        public /* bridge */ /* synthetic */ f34.c c(Map.Entry entry) {
            g(entry);
            return this;
        }

        @DexIgnore
        public z24<K, V> e() {
            return (z24) super.a();
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public a<K, V> f(K k, V v) {
            super.b(k, v);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public a<K, V> g(Map.Entry<? extends K, ? extends V> entry) {
            super.c(entry);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public a<K, V> h(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.d(iterable);
            return this;
        }
    }

    @DexIgnore
    public z24(a34<K, y24<V>> a34, int i) {
        super(a34, i);
    }

    @DexIgnore
    public static <K, V> a<K, V> builder() {
        return new a<>();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.a34$b */
    /* JADX WARN: Multi-variable type inference failed */
    public static <K, V> z24<K, V> copyOf(y34<? extends K, ? extends V> y34) {
        if (y34.isEmpty()) {
            return of();
        }
        if (y34 instanceof z24) {
            z24<K, V> z24 = (z24) y34;
            if (!z24.isPartialView()) {
                return z24;
            }
        }
        a34.b bVar = new a34.b(y34.asMap().size());
        int i = 0;
        for (Map.Entry<? extends K, Collection<? extends V>> entry : y34.asMap().entrySet()) {
            y24 copyOf = y24.copyOf((Collection) entry.getValue());
            if (!copyOf.isEmpty()) {
                bVar.c(entry.getKey(), copyOf);
                i = copyOf.size() + i;
            }
        }
        return new z24<>(bVar.a(), i);
    }

    @DexIgnore
    public static <K, V> z24<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        a aVar = new a();
        aVar.h(iterable);
        return aVar.e();
    }

    @DexIgnore
    public static <K, V> z24<K, V> of() {
        return g24.INSTANCE;
    }

    @DexIgnore
    public static <K, V> z24<K, V> of(K k, V v) {
        a builder = builder();
        builder.f(k, v);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> z24<K, V> of(K k, V v, K k2, V v2) {
        a builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> z24<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        a builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        builder.f(k3, v3);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> z24<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        a builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        builder.f(k3, v3);
        builder.f(k4, v4);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> z24<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        a builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        builder.f(k3, v3);
        builder.f(k4, v4);
        builder.f(k5, v5);
        return builder.e();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.a34$b */
    /* JADX WARN: Multi-variable type inference failed */
    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            a34.b builder = a34.builder();
            int i = 0;
            int i2 = 0;
            while (i2 < readInt) {
                Object readObject = objectInputStream.readObject();
                int readInt2 = objectInputStream.readInt();
                if (readInt2 > 0) {
                    y24.b builder2 = y24.builder();
                    for (int i3 = 0; i3 < readInt2; i3++) {
                        builder2.g(objectInputStream.readObject());
                    }
                    builder.c(readObject, builder2.i());
                    i2++;
                    i += readInt2;
                } else {
                    throw new InvalidObjectException("Invalid value count " + readInt2);
                }
            }
            try {
                f34.e.f1047a.b(this, builder.a());
                f34.e.b.a(this, i);
            } catch (IllegalArgumentException e) {
                throw ((InvalidObjectException) new InvalidObjectException(e.getMessage()).initCause(e));
            }
        } else {
            throw new InvalidObjectException("Invalid key count " + readInt);
        }
    }

    @DexIgnore
    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        v44.d(this, objectOutputStream);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.z24<K, V> */
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.z24$a */
    /* JADX WARN: Multi-variable type inference failed */
    public final z24<V, K> a() {
        a builder = builder();
        Iterator it = entries().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            builder.f(entry.getValue(), entry.getKey());
        }
        z24<V, K> e = builder.e();
        e.g = this;
        return e;
    }

    @DexIgnore
    @Override // com.fossil.f34, com.fossil.f34, com.fossil.y34
    public y24<V> get(K k) {
        y24<V> y24 = (y24) this.map.get(k);
        return y24 == null ? y24.of() : y24;
    }

    @DexIgnore
    @Override // com.fossil.f34
    public z24<V, K> inverse() {
        z24<V, K> z24 = this.g;
        if (z24 != null) {
            return z24;
        }
        z24<V, K> a2 = a();
        this.g = a2;
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.f34, com.fossil.f34
    @CanIgnoreReturnValue
    @Deprecated
    public y24<V> removeAll(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.f34, com.fossil.f34, com.fossil.u14
    @CanIgnoreReturnValue
    @Deprecated
    public y24<V> replaceValues(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }
}
