package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pu3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ lu3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Callable c;

    @DexIgnore
    public pu3(lu3 lu3, Callable callable) {
        this.b = lu3;
        this.c = callable;
    }

    @DexIgnore
    public final void run() {
        try {
            this.b.u(this.c.call());
        } catch (Exception e) {
            this.b.t(e);
        }
    }
}
