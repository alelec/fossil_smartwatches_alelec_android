package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.s87;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.chrono.BasicChronology;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v87 {
    @DexIgnore
    public static final List<s87> a(List<? extends s87> list) {
        s87 f;
        pq7.c(list, "$this$clone");
        ArrayList arrayList = new ArrayList(im7.m(list, 10));
        for (T t : list) {
            if (t instanceof s87.c) {
                f = s87.c.f(t, null, null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, null, 0, 0, 127, null);
            } else if (t instanceof s87.b) {
                f = s87.b.g(t, null, null, null, null, 0, 0, 63, null);
            } else if (t instanceof s87.a) {
                f = s87.a.f(t, null, null, null, false, null, null, null, null, 0, 0, BasicChronology.CACHE_MASK, null);
            } else {
                throw new al7();
            }
            arrayList.add(f);
        }
        return arrayList;
    }

    @DexIgnore
    public static final String b(lb7 lb7) {
        pq7.c(lb7, "$this$toComplicationId");
        switch (u87.f3550a[lb7.ordinal()]) {
            case 1:
                return "weather";
            case 2:
                return "second-timezone";
            case 3:
                return "active-minutes";
            case 4:
                return Constants.BATTERY;
            case 5:
                return "calories";
            case 6:
                return "chance-of-rain";
            case 7:
                return "date";
            case 8:
                return "heart-rate";
            case 9:
            default:
                return "steps";
        }
    }

    @DexIgnore
    public static final w87 c(jb7 jb7) {
        pq7.c(jb7, "$this$toMetric");
        return new w87(jb7.c(), jb7.d(), jb7.b(), jb7.a(), 1.0f);
    }
}
