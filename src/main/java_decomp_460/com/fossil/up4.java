package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class up4 implements Factory<on5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f3631a;
    @DexIgnore
    public /* final */ Provider<Context> b;

    @DexIgnore
    public up4(uo4 uo4, Provider<Context> provider) {
        this.f3631a = uo4;
        this.b = provider;
    }

    @DexIgnore
    public static up4 a(uo4 uo4, Provider<Context> provider) {
        return new up4(uo4, provider);
    }

    @DexIgnore
    public static on5 c(uo4 uo4, Context context) {
        on5 B = uo4.B(context);
        lk7.c(B, "Cannot return null from a non-@Nullable @Provides method");
        return B;
    }

    @DexIgnore
    /* renamed from: b */
    public on5 get() {
        return c(this.f3631a, this.b.get());
    }
}
