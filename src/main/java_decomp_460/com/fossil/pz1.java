package com.fossil;

import android.util.SparseArray;
import com.fossil.jz1;
import com.portfolio.platform.data.InAppPermission;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pz1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(b bVar);

        @DexIgnore
        public abstract a b(c cVar);

        @DexIgnore
        public abstract pz1 c();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class b extends Enum<b> {
        @DexIgnore
        public static /* final */ SparseArray<b> b;
        @DexIgnore
        public static /* final */ b zza; // = new b("UNKNOWN_MOBILE_SUBTYPE", 0, 0);
        @DexIgnore
        public static /* final */ b zzb; // = new b("GPRS", 1, 1);
        @DexIgnore
        public static /* final */ b zzc; // = new b("EDGE", 2, 2);
        @DexIgnore
        public static /* final */ b zzd; // = new b("UMTS", 3, 3);
        @DexIgnore
        public static /* final */ b zze; // = new b("CDMA", 4, 4);
        @DexIgnore
        public static /* final */ b zzf; // = new b("EVDO_0", 5, 5);
        @DexIgnore
        public static /* final */ b zzg; // = new b("EVDO_A", 6, 6);
        @DexIgnore
        public static /* final */ b zzh; // = new b("RTT", 7, 7);
        @DexIgnore
        public static /* final */ b zzi; // = new b("HSDPA", 8, 8);
        @DexIgnore
        public static /* final */ b zzj; // = new b("HSUPA", 9, 9);
        @DexIgnore
        public static /* final */ b zzk; // = new b("HSPA", 10, 10);
        @DexIgnore
        public static /* final */ b zzl; // = new b("IDEN", 11, 11);
        @DexIgnore
        public static /* final */ b zzm; // = new b("EVDO_B", 12, 12);
        @DexIgnore
        public static /* final */ b zzn; // = new b("LTE", 13, 13);
        @DexIgnore
        public static /* final */ b zzo; // = new b("EHRPD", 14, 14);
        @DexIgnore
        public static /* final */ b zzp; // = new b("HSPAP", 15, 15);
        @DexIgnore
        public static /* final */ b zzq; // = new b("GSM", 16, 16);
        @DexIgnore
        public static /* final */ b zzr; // = new b("TD_SCDMA", 17, 17);
        @DexIgnore
        public static /* final */ b zzs; // = new b("IWLAN", 18, 18);
        @DexIgnore
        public static /* final */ b zzt; // = new b("LTE_CA", 19, 19);
        @DexIgnore
        public static /* final */ b zzu; // = new b("COMBINED", 20, 100);
        @DexIgnore
        public /* final */ int zzw;

        /*
        static {
            SparseArray<b> sparseArray = new SparseArray<>();
            b = sparseArray;
            sparseArray.put(0, zza);
            b.put(1, zzb);
            b.put(2, zzc);
            b.put(3, zzd);
            b.put(4, zze);
            b.put(5, zzf);
            b.put(6, zzg);
            b.put(7, zzh);
            b.put(8, zzi);
            b.put(9, zzj);
            b.put(10, zzk);
            b.put(11, zzl);
            b.put(12, zzm);
            b.put(13, zzn);
            b.put(14, zzo);
            b.put(15, zzp);
            b.put(16, zzq);
            b.put(17, zzr);
            b.put(18, zzs);
            b.put(19, zzt);
        }
        */

        @DexIgnore
        public b(String str, int i, int i2) {
            this.zzw = i2;
        }

        @DexIgnore
        public static b zza(int i) {
            return b.get(i);
        }

        @DexIgnore
        public int zza() {
            return this.zzw;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class c extends Enum<c> {
        @DexIgnore
        public static /* final */ SparseArray<c> b;
        @DexIgnore
        public static /* final */ c zza; // = new c("MOBILE", 0, 0);
        @DexIgnore
        public static /* final */ c zzb; // = new c("WIFI", 1, 1);
        @DexIgnore
        public static /* final */ c zzc; // = new c("MOBILE_MMS", 2, 2);
        @DexIgnore
        public static /* final */ c zzd; // = new c("MOBILE_SUPL", 3, 3);
        @DexIgnore
        public static /* final */ c zze; // = new c("MOBILE_DUN", 4, 4);
        @DexIgnore
        public static /* final */ c zzf; // = new c("MOBILE_HIPRI", 5, 5);
        @DexIgnore
        public static /* final */ c zzg; // = new c("WIMAX", 6, 6);
        @DexIgnore
        public static /* final */ c zzh; // = new c(InAppPermission.BLUETOOTH, 7, 7);
        @DexIgnore
        public static /* final */ c zzi; // = new c("DUMMY", 8, 8);
        @DexIgnore
        public static /* final */ c zzj; // = new c("ETHERNET", 9, 9);
        @DexIgnore
        public static /* final */ c zzk; // = new c("MOBILE_FOTA", 10, 10);
        @DexIgnore
        public static /* final */ c zzl; // = new c("MOBILE_IMS", 11, 11);
        @DexIgnore
        public static /* final */ c zzm; // = new c("MOBILE_CBS", 12, 12);
        @DexIgnore
        public static /* final */ c zzn; // = new c("WIFI_P2P", 13, 13);
        @DexIgnore
        public static /* final */ c zzo; // = new c("MOBILE_IA", 14, 14);
        @DexIgnore
        public static /* final */ c zzp; // = new c("MOBILE_EMERGENCY", 15, 15);
        @DexIgnore
        public static /* final */ c zzq; // = new c("PROXY", 16, 16);
        @DexIgnore
        public static /* final */ c zzr; // = new c("VPN", 17, 17);
        @DexIgnore
        public static /* final */ c zzs; // = new c("NONE", 18, -1);
        @DexIgnore
        public /* final */ int zzu;

        /*
        static {
            SparseArray<c> sparseArray = new SparseArray<>();
            b = sparseArray;
            sparseArray.put(0, zza);
            b.put(1, zzb);
            b.put(2, zzc);
            b.put(3, zzd);
            b.put(4, zze);
            b.put(5, zzf);
            b.put(6, zzg);
            b.put(7, zzh);
            b.put(8, zzi);
            b.put(9, zzj);
            b.put(10, zzk);
            b.put(11, zzl);
            b.put(12, zzm);
            b.put(13, zzn);
            b.put(14, zzo);
            b.put(15, zzp);
            b.put(16, zzq);
            b.put(17, zzr);
            b.put(-1, zzs);
        }
        */

        @DexIgnore
        public c(String str, int i, int i2) {
            this.zzu = i2;
        }

        @DexIgnore
        public static c zza(int i) {
            return b.get(i);
        }

        @DexIgnore
        public int zza() {
            return this.zzu;
        }
    }

    @DexIgnore
    public static a a() {
        return new jz1.b();
    }

    @DexIgnore
    public abstract b b();

    @DexIgnore
    public abstract c c();
}
