package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c94 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Object f585a; // = new Object();
    @DexIgnore
    public ot3<Void> b; // = new ot3<>();
    @DexIgnore
    public /* final */ SharedPreferences c;
    @DexIgnore
    public volatile boolean d;
    @DexIgnore
    public volatile boolean e;
    @DexIgnore
    public /* final */ j64 f;
    @DexIgnore
    public ot3<Void> g; // = new ot3<>();

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0040 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public c94(com.fossil.j64 r6) {
        /*
            r5 = this;
            r1 = 1
            r5.<init>()
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            r5.f585a = r0
            com.fossil.ot3 r0 = new com.fossil.ot3
            r0.<init>()
            r5.b = r0
            com.fossil.ot3 r0 = new com.fossil.ot3
            r0.<init>()
            r5.g = r0
            r5.f = r6
            android.content.Context r0 = r6.g()
            if (r0 == 0) goto L_0x0086
            android.content.SharedPreferences r3 = com.fossil.r84.t(r0)
            r5.c = r3
            r2 = 0
            java.lang.String r4 = "firebase_crashlytics_collection_enabled"
            boolean r3 = r3.contains(r4)
            if (r3 == 0) goto L_0x004e
            android.content.SharedPreferences r0 = r5.c
            java.lang.String r2 = "firebase_crashlytics_collection_enabled"
            boolean r0 = r0.getBoolean(r2, r1)
        L_0x0038:
            r2 = r1
        L_0x0039:
            r5.e = r0
            r5.d = r2
            java.lang.Object r1 = r5.f585a
            monitor-enter(r1)
            boolean r0 = r5.b()     // Catch:{ all -> 0x0083 }
            if (r0 == 0) goto L_0x004c
            com.fossil.ot3<java.lang.Void> r0 = r5.b     // Catch:{ all -> 0x0083 }
            r2 = 0
            r0.e(r2)     // Catch:{ all -> 0x0083 }
        L_0x004c:
            monitor-exit(r1)     // Catch:{ all -> 0x0083 }
            return
        L_0x004e:
            android.content.pm.PackageManager r3 = r0.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0077 }
            if (r3 == 0) goto L_0x008e
            java.lang.String r0 = r0.getPackageName()     // Catch:{ NameNotFoundException -> 0x0077 }
            r4 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r0 = r3.getApplicationInfo(r0, r4)     // Catch:{ NameNotFoundException -> 0x0077 }
            if (r0 == 0) goto L_0x008e
            android.os.Bundle r3 = r0.metaData     // Catch:{ NameNotFoundException -> 0x0077 }
            if (r3 == 0) goto L_0x008e
            android.os.Bundle r3 = r0.metaData     // Catch:{ NameNotFoundException -> 0x0077 }
            java.lang.String r4 = "firebase_crashlytics_collection_enabled"
            boolean r3 = r3.containsKey(r4)     // Catch:{ NameNotFoundException -> 0x0077 }
            if (r3 == 0) goto L_0x008e
            android.os.Bundle r0 = r0.metaData     // Catch:{ NameNotFoundException -> 0x0077 }
            java.lang.String r3 = "firebase_crashlytics_collection_enabled"
            boolean r0 = r0.getBoolean(r3)     // Catch:{ NameNotFoundException -> 0x0077 }
            goto L_0x0038
        L_0x0077:
            r0 = move-exception
            com.fossil.x74 r3 = com.fossil.x74.f()
            java.lang.String r4 = "Unable to get PackageManager. Falling through"
            r3.c(r4, r0)
            r0 = r1
            goto L_0x0039
        L_0x0083:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0086:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "null context"
            r0.<init>(r1)
            throw r0
        L_0x008e:
            r0 = r1
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.c94.<init>(com.fossil.j64):void");
    }

    @DexIgnore
    public void a(boolean z) {
        if (z) {
            this.g.e(null);
            return;
        }
        throw new IllegalStateException("An invalid data collection token was used.");
    }

    @DexIgnore
    public boolean b() {
        return this.d ? this.e : this.f.p();
    }

    @DexIgnore
    public nt3<Void> c() {
        nt3<Void> a2;
        synchronized (this.f585a) {
            a2 = this.b.a();
        }
        return a2;
    }

    @DexIgnore
    public nt3<Void> d() {
        return t94.g(this.g.a(), c());
    }
}
