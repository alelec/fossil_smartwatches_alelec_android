package com.fossil;

import android.graphics.Bitmap;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.s87;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t87 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ t87 f3377a; // = new t87();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.model.ElementConfigHelper", f = "ElementConfigHelper.kt", l = {126, 127, 129}, m = "buildComplicationConfig")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ t87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(t87 t87, qn7 qn7) {
            super(qn7);
            this.this$0 = t87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.model.ElementConfigHelper", f = "ElementConfigHelper.kt", l = {49}, m = "buildFromThemeData")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ t87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(t87 t87, qn7 qn7) {
            super(qn7);
            this.this$0 = t87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, null, null, null, null, this);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x016a  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x01c3  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01c7  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0261  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0269  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x027a  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x028e  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x029b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.sb7 r27, com.fossil.s77 r28, com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository r29, com.portfolio.platform.data.source.UserRepository r30, com.fossil.zm5 r31, com.fossil.qn7<? super com.fossil.s87.a> r32) {
        /*
        // Method dump skipped, instructions count: 690
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.t87.a(com.fossil.sb7, com.fossil.s77, com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository, com.portfolio.platform.data.source.UserRepository, com.fossil.zm5, com.fossil.qn7):java.lang.Object");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v7, types: [java.lang.Iterable] */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0114 A[SYNTHETIC] */
    public final java.lang.Object b(com.fossil.ub7 r23, com.fossil.s77 r24, com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository r25, com.portfolio.platform.data.source.UserRepository r26, com.fossil.zm5 r27, com.fossil.qn7<? super java.util.List<? extends com.fossil.s87>> r28) {
        /*
        // Method dump skipped, instructions count: 279
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.t87.b(com.fossil.ub7, com.fossil.s77, com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository, com.portfolio.platform.data.source.UserRepository, com.fossil.zm5, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final s87.b c(vb7 vb7) {
        pb7 b2 = vb7.b();
        s87.b e = b97.c.e(b2.d());
        if (e != null) {
            e.q(vb7.a());
            e.d(v87.c(b2.b()));
            e.o(b2.a());
            e.p(b2.a());
            return e;
        }
        b97 b97 = b97.c;
        String d = b2.d();
        Bitmap a2 = vb7.a();
        if (a2 != null) {
            b97.a(d, a2);
            String d2 = b2.d();
            String c = vb7.c();
            if (c == null) {
                c = "";
            }
            return new s87.b(r87.b(b2.c()), gm7.b(new x87(d2, c)), null, v87.c(b2.b()), 0, 0, 52, null);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final s87.c d(tb7 tb7) {
        mb7 a2 = tb7.a();
        return new s87.c(a2.f(), z87.d.i(a2.e()), a2.d(), r87.b(a2.c()), v87.c(a2.b()), a2.a(), a2.a());
    }
}
