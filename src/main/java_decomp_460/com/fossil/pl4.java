package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pl4 extends Exception {
    @DexIgnore
    public static /* final */ StackTraceElement[] NO_TRACE; // = new StackTraceElement[0];
    @DexIgnore
    public static /* final */ boolean isStackTrace; // = (System.getProperty("surefire.test.class.path") != null);

    @DexIgnore
    public pl4() {
    }

    @DexIgnore
    public pl4(Throwable th) {
        super(th);
    }

    @DexIgnore
    public final Throwable fillInStackTrace() {
        synchronized (this) {
        }
        return null;
    }
}
