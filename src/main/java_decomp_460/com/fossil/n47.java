package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n47 {
    @DexIgnore
    public static /* final */ yk7 b; // = zk7.a(a.INSTANCE);
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ b d; // = new b(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public on5 f2466a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements gp7<n47> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final n47 invoke() {
            return c.b.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final n47 a() {
            yk7 yk7 = n47.b;
            b bVar = n47.d;
            return (n47) yk7.getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ n47 f2467a; // = new n47();
        @DexIgnore
        public static /* final */ c b; // = new c();

        @DexIgnore
        public final n47 a() {
            return f2467a;
        }
    }

    /*
    static {
        String simpleName = n47.class.getSimpleName();
        pq7.b(simpleName, "UserUtils::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public n47() {
        PortfolioApp.h0.c().M().I(this);
    }

    @DexIgnore
    public final boolean b(int i, int i2) {
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        long currentTimeMillis = System.currentTimeMillis();
        on5 on5 = this.f2466a;
        if (on5 != null) {
            int days = (int) timeUnit.toDays(currentTimeMillis - on5.y());
            FLogger.INSTANCE.getLocal().d(c, "Inside .isHappyUser");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = c;
            local.d(str, "crashThreshold = " + i + ", successfulSyncThreshold = " + i2 + ", delta in Day = " + days);
            if (days >= i) {
                on5 on52 = this.f2466a;
                if (on52 == null) {
                    pq7.n("mSharePrefs");
                    throw null;
                } else if (on52.m() >= i2) {
                    return true;
                }
            }
            return false;
        }
        pq7.n("mSharePrefs");
        throw null;
    }

    @DexIgnore
    public final boolean c() {
        on5 on5 = this.f2466a;
        if (on5 != null) {
            return on5.n0();
        }
        pq7.n("mSharePrefs");
        throw null;
    }
}
