package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class p64 implements d74 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ d74 f2787a; // = new p64();

    @DexIgnore
    @Override // com.fossil.d74
    public final Object a(b74 b74) {
        return n64.d((j64) b74.get(j64.class), (Context) b74.get(Context.class), (ge4) b74.get(ge4.class));
    }
}
