package com.fossil;

import com.fossil.dl7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d08 {
    @DexIgnore
    public static final void a(qn7<? super tl7> qn7, qn7<?> qn72) {
        try {
            qn7 c = xn7.c(qn7);
            dl7.a aVar = dl7.Companion;
            wv7.b(c, dl7.m1constructorimpl(tl7.f3441a));
        } catch (Throwable th) {
            dl7.a aVar2 = dl7.Companion;
            qn72.resumeWith(dl7.m1constructorimpl(el7.a(th)));
        }
    }

    @DexIgnore
    public static final <T> void b(rp7<? super qn7<? super T>, ? extends Object> rp7, qn7<? super T> qn7) {
        try {
            qn7 c = xn7.c(xn7.a(rp7, qn7));
            dl7.a aVar = dl7.Companion;
            wv7.b(c, dl7.m1constructorimpl(tl7.f3441a));
        } catch (Throwable th) {
            dl7.a aVar2 = dl7.Companion;
            qn7.resumeWith(dl7.m1constructorimpl(el7.a(th)));
        }
    }

    @DexIgnore
    public static final <R, T> void c(vp7<? super R, ? super qn7<? super T>, ? extends Object> vp7, R r, qn7<? super T> qn7) {
        try {
            qn7 c = xn7.c(xn7.b(vp7, r, qn7));
            dl7.a aVar = dl7.Companion;
            wv7.b(c, dl7.m1constructorimpl(tl7.f3441a));
        } catch (Throwable th) {
            dl7.a aVar2 = dl7.Companion;
            qn7.resumeWith(dl7.m1constructorimpl(el7.a(th)));
        }
    }
}
