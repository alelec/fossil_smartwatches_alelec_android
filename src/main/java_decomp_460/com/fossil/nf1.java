package com.fossil;

import android.content.Context;
import android.net.Uri;
import com.fossil.af1;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nf1 implements af1<Uri, InputStream> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2511a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements bf1<Uri, InputStream> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Context f2512a;

        @DexIgnore
        public a(Context context) {
            this.f2512a = context;
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Uri, InputStream> b(ef1 ef1) {
            return new nf1(this.f2512a);
        }
    }

    @DexIgnore
    public nf1(Context context) {
        this.f2511a = context.getApplicationContext();
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<InputStream> b(Uri uri, int i, int i2, ob1 ob1) {
        if (jc1.d(i, i2)) {
            return new af1.a<>(new yj1(uri), kc1.e(this.f2511a, uri));
        }
        return null;
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(Uri uri) {
        return jc1.a(uri);
    }
}
