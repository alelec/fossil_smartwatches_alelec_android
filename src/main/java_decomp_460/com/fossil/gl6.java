package com.fossil;

import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.fl5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.service.workout.WorkoutTetherScreenShotManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gl6 extends bl6 implements fl5.a {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public MutableLiveData<cl7<Date, Date>> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ WorkoutTetherScreenShotManager i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public List<ActivitySummary> l;
    @DexIgnore
    public List<ActivitySample> m;
    @DexIgnore
    public ActivitySummary n;
    @DexIgnore
    public List<ActivitySample> o;
    @DexIgnore
    public ai5 p;
    @DexIgnore
    public LiveData<h47<List<ActivitySummary>>> q;
    @DexIgnore
    public LiveData<h47<List<ActivitySample>>> r;
    @DexIgnore
    public Listing<WorkoutSession> s;
    @DexIgnore
    public /* final */ cl6 t;
    @DexIgnore
    public /* final */ SummariesRepository u;
    @DexIgnore
    public /* final */ ActivitiesRepository v;
    @DexIgnore
    public /* final */ UserRepository w;
    @DexIgnore
    public /* final */ WorkoutSessionRepository x;
    @DexIgnore
    public /* final */ no4 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<ActivitySample, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Date date) {
            super(1);
            this.$date = date;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ Boolean invoke(ActivitySample activitySample) {
            return Boolean.valueOf(invoke(activitySample));
        }

        @DexIgnore
        public final boolean invoke(ActivitySample activitySample) {
            pq7.c(activitySample, "it");
            return lk5.m0(activitySample.getDate(), this.$date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$observeWorkoutSessionData$1", f = "ActivityDetailPresenter.kt", l = {100}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gl6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements ls0<cu0<WorkoutSession>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ b f1326a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.gl6$b$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$observeWorkoutSessionData$1$1$1", f = "ActivityDetailPresenter.kt", l = {111}, m = "invokeSuspend")
            /* renamed from: com.fossil.gl6$b$a$a  reason: collision with other inner class name */
            public static final class C0098a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ cu0 $pageList;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.gl6$b$a$a$a")
                /* renamed from: com.fossil.gl6$b$a$a$a  reason: collision with other inner class name */
                public static final class C0099a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0098a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0099a(qn7 qn7, C0098a aVar) {
                        super(2, qn7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        C0099a aVar = new C0099a(qn7, this.this$0);
                        aVar.p$ = (iv7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                        return ((C0099a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    /* JADX WARNING: Removed duplicated region for block: B:25:0x009c  */
                    /* JADX WARNING: Removed duplicated region for block: B:41:0x0014 A[SYNTHETIC] */
                    @Override // com.fossil.zn7
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                        /*
                        // Method dump skipped, instructions count: 319
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gl6.b.a.C0098a.C0099a.invokeSuspend(java.lang.Object):java.lang.Object");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0098a(a aVar, cu0 cu0, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$pageList = cu0;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0098a aVar = new C0098a(this.this$0, this.$pageList, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0098a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        FragmentActivity activity = ((dl6) this.this$0.f1326a.this$0.t).getActivity();
                        if (activity != null) {
                            WorkoutTetherScreenShotManager workoutTetherScreenShotManager = this.this$0.f1326a.this$0.i;
                            pq7.b(activity, "it");
                            workoutTetherScreenShotManager.w(activity);
                            dv7 a2 = bw7.a();
                            C0099a aVar = new C0099a(null, this);
                            this.L$0 = iv7;
                            this.L$1 = activity;
                            this.label = 1;
                            if (eu7.g(a2, aVar, this) == d) {
                                return d;
                            }
                        }
                    } else if (i == 1) {
                        FragmentActivity fragmentActivity = (FragmentActivity) this.L$1;
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    cl6 cl6 = this.this$0.f1326a.this$0.t;
                    ai5 ai5 = this.this$0.f1326a.this$0.p;
                    cu0<WorkoutSession> cu0 = this.$pageList;
                    pq7.b(cu0, "pageList");
                    cl6.s(true, ai5, cu0);
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            public a(b bVar) {
                this.f1326a = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(cu0<WorkoutSession> cu0) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ActivityDetailPresenter", "on workout sessions changed, size " + cu0.size());
                if (nk5.o.y(PortfolioApp.h0.c().J())) {
                    pq7.b(cu0, "pageList");
                    if (pm7.j0(cu0).isEmpty()) {
                        this.f1326a.this$0.t.s(false, this.f1326a.this$0.p, cu0);
                        return;
                    }
                }
                xw7 unused = gu7.d(this.f1326a.this$0.k(), null, null, new C0098a(this, cu0, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(gl6 gl6, Date date, qn7 qn7) {
            super(2, qn7);
            this.this$0 = gl6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$date, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object workoutSessionsPaging;
            gl6 gl6;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                gl6 gl62 = this.this$0;
                WorkoutSessionRepository workoutSessionRepository = gl62.x;
                Date date = this.$date;
                WorkoutSessionRepository workoutSessionRepository2 = this.this$0.x;
                no4 no4 = this.this$0.y;
                gl6 gl63 = this.this$0;
                this.L$0 = iv7;
                this.L$1 = gl62;
                this.label = 1;
                workoutSessionsPaging = workoutSessionRepository.getWorkoutSessionsPaging(date, workoutSessionRepository2, no4, gl63, this);
                if (workoutSessionsPaging == d) {
                    return d;
                }
                gl6 = gl62;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                gl6 = (gl6) this.L$1;
                workoutSessionsPaging = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            gl6.s = (Listing) workoutSessionsPaging;
            Listing listing = this.this$0.s;
            if (listing != null) {
                LiveData pagedList = listing.getPagedList();
                cl6 cl6 = this.this$0.t;
                if (cl6 != null) {
                    pagedList.h((dl6) cl6, new a(this));
                    return tl7.f3441a;
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gl6 f1327a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$sampleTransformations$1$1", f = "ActivityDetailPresenter.kt", l = {79, 79}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<ActivitySample>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Date date, Date date2, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$first, this.$second, qn7);
                aVar.p$ = (hs0) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<ActivitySample>>> hs0, qn7<? super tl7> qn7) {
                return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r8)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r8)
                    com.fossil.hs0 r0 = r7.p$
                    com.fossil.gl6$c r1 = r7.this$0
                    com.fossil.gl6 r1 = r1.f1327a
                    com.portfolio.platform.data.source.ActivitiesRepository r1 = com.fossil.gl6.B(r1)
                    java.util.Date r2 = r7.$first
                    java.util.Date r3 = r7.$second
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getActivityList(r2, r3, r5, r7)
                    if (r2 != r4) goto L_0x005b
                    r0 = r4
                    goto L_0x0017
                L_0x005b:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.gl6.c.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public c(gl6 gl6) {
            this.f1327a = gl6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<ActivitySample>>> apply(cl7<? extends Date, ? extends Date> cl7) {
            return or0.c(bw7.c(), 0, new a(this, (Date) cl7.component1(), (Date) cl7.component2(), null), 2, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$setDate$1", f = "ActivityDetailPresenter.kt", l = {211, 235, 236}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gl6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$setDate$1$1", f = "ActivityDetailPresenter.kt", l = {211}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Date>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Date> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    PortfolioApp c = PortfolioApp.h0.c();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object n0 = c.n0(this);
                    return n0 == d ? d : n0;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$setDate$1$samples$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super List<ActivitySample>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<ActivitySample>> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    gl6 gl6 = this.this$0.this$0;
                    return gl6.h0(gl6.g, this.this$0.this$0.m);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$setDate$1$summary$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super ActivitySummary>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, qn7);
                cVar.p$ = (iv7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ActivitySummary> qn7) {
                return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    gl6 gl6 = this.this$0.this$0;
                    return gl6.i0(gl6.g, this.this$0.this$0.l);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(gl6 gl6, Date date, qn7 qn7) {
            super(2, qn7);
            this.this$0 = gl6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$date, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00cc  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0228  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 563
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.gl6.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$showDetailChart$1", f = "ActivityDetailPresenter.kt", l = {278, 280}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gl6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$showDetailChart$1$maxValue$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $data;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(ArrayList arrayList, qn7 qn7) {
                super(2, qn7);
                this.$data = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$data, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                ArrayList<ArrayList<BarChart.b>> d;
                ArrayList<BarChart.b> arrayList;
                int i = 0;
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    Iterator it = this.$data.iterator();
                    if (!it.hasNext()) {
                        obj2 = null;
                    } else {
                        Object next = it.next();
                        if (!it.hasNext()) {
                            obj2 = next;
                        } else {
                            ArrayList<BarChart.b> arrayList2 = ((BarChart.a) next).d().get(0);
                            pq7.b(arrayList2, "it.mListOfBarPoints[0]");
                            Iterator<T> it2 = arrayList2.iterator();
                            int i2 = 0;
                            while (it2.hasNext()) {
                                i2 = ao7.e(it2.next().e()).intValue() + i2;
                            }
                            Integer e = ao7.e(i2);
                            while (true) {
                                next = it.next();
                                ArrayList<BarChart.b> arrayList3 = ((BarChart.a) next).d().get(0);
                                pq7.b(arrayList3, "it.mListOfBarPoints[0]");
                                Iterator<T> it3 = arrayList3.iterator();
                                int i3 = 0;
                                while (it3.hasNext()) {
                                    i3 = ao7.e(it3.next().e()).intValue() + i3;
                                }
                                e = ao7.e(i3);
                                if (e.compareTo(e) >= 0) {
                                    e = e;
                                    next = next;
                                }
                                if (!it.hasNext()) {
                                    break;
                                }
                            }
                            obj2 = next;
                        }
                    }
                    BarChart.a aVar = (BarChart.a) obj2;
                    if (aVar == null || (d = aVar.d()) == null || (arrayList = d.get(0)) == null) {
                        return null;
                    }
                    Iterator<T> it4 = arrayList.iterator();
                    while (it4.hasNext()) {
                        i += ao7.e(it4.next().e()).intValue();
                    }
                    return ao7.e(i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$showDetailChart$1$pair$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super cl7<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super cl7<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return cn6.f632a.b(this.this$0.this$0.g, this.this$0.this$0.o, 0);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(gl6 gl6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = gl6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0088  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x00a6  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                r7 = 0
                r6 = 2
                r4 = 1
                java.lang.Object r3 = com.fossil.yn7.d()
                int r0 = r8.label
                if (r0 == 0) goto L_0x008a
                if (r0 == r4) goto L_0x005d
                if (r0 != r6) goto L_0x0055
                java.lang.Object r0 = r8.L$2
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                java.lang.Object r1 = r8.L$1
                com.fossil.cl7 r1 = (com.fossil.cl7) r1
                java.lang.Object r2 = r8.L$0
                com.fossil.iv7 r2 = (com.fossil.iv7) r2
                com.fossil.el7.b(r9)
                r2 = r9
                r3 = r1
                r4 = r0
            L_0x0021:
                r0 = r2
                java.lang.Integer r0 = (java.lang.Integer) r0
                com.fossil.sk5$a r1 = com.fossil.sk5.c
                com.fossil.gl6 r2 = r8.this$0
                com.portfolio.platform.data.model.room.fitness.ActivitySummary r2 = com.fossil.gl6.F(r2)
                com.fossil.rh5 r5 = com.fossil.rh5.TOTAL_STEPS
                int r1 = r1.d(r2, r5)
                com.fossil.gl6 r2 = r8.this$0
                com.fossil.cl6 r2 = com.fossil.gl6.P(r2)
                if (r0 == 0) goto L_0x00aa
                int r0 = r0.intValue()
            L_0x003e:
                com.portfolio.platform.ui.view.chart.base.BarChart$c r5 = new com.portfolio.platform.ui.view.chart.base.BarChart$c
                int r6 = r1 / 16
                int r0 = java.lang.Math.max(r0, r6)
                r5.<init>(r0, r1, r4)
                java.lang.Object r0 = r3.getSecond()
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                r2.n(r5, r0)
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0054:
                return r0
            L_0x0055:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x005d:
                java.lang.Object r0 = r8.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r9)
                r2 = r0
                r1 = r9
            L_0x0066:
                r0 = r1
                com.fossil.cl7 r0 = (com.fossil.cl7) r0
                java.lang.Object r1 = r0.getFirst()
                java.util.ArrayList r1 = (java.util.ArrayList) r1
                com.fossil.gl6 r4 = r8.this$0
                com.fossil.dv7 r4 = com.fossil.gl6.z(r4)
                com.fossil.gl6$e$a r5 = new com.fossil.gl6$e$a
                r5.<init>(r1, r7)
                r8.L$0 = r2
                r8.L$1 = r0
                r8.L$2 = r1
                r8.label = r6
                java.lang.Object r2 = com.fossil.eu7.g(r4, r5, r8)
                if (r2 != r3) goto L_0x00a6
                r0 = r3
                goto L_0x0054
            L_0x008a:
                com.fossil.el7.b(r9)
                com.fossil.iv7 r0 = r8.p$
                com.fossil.gl6 r1 = r8.this$0
                com.fossil.dv7 r1 = com.fossil.gl6.z(r1)
                com.fossil.gl6$e$b r2 = new com.fossil.gl6$e$b
                r2.<init>(r8, r7)
                r8.L$0 = r0
                r8.label = r4
                java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r8)
                if (r1 != r3) goto L_0x00ac
                r0 = r3
                goto L_0x0054
            L_0x00a6:
                r3 = r0
                r4 = r1
                goto L_0x0021
            L_0x00aa:
                r0 = 0
                goto L_0x003e
            L_0x00ac:
                r2 = r0
                goto L_0x0066
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.gl6.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1", f = "ActivityDetailPresenter.kt", l = {149}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gl6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$1", f = "ActivityDetailPresenter.kt", l = {150}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super ai5>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ai5> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                String value;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.w;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    if (currentUser == d) {
                        return d;
                    }
                    obj2 = currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    obj2 = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (obj2 != null) {
                    MFUser.UnitGroup unitGroup = ((MFUser) obj2).getUnitGroup();
                    if (unitGroup == null || (value = unitGroup.getDistance()) == null) {
                        value = ai5.METRIC.getValue();
                    }
                    return ai5.fromString(value);
                }
                pq7.i();
                throw null;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements ls0<h47<? extends List<ActivitySummary>>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ f f1328a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$2$1", f = "ActivityDetailPresenter.kt", l = {163}, m = "invokeSuspend")
            public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.gl6$f$b$a$a")
                @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$2$1$summary$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
                /* renamed from: com.fossil.gl6$f$b$a$a  reason: collision with other inner class name */
                public static final class C0100a extends ko7 implements vp7<iv7, qn7<? super ActivitySummary>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0100a(a aVar, qn7 qn7) {
                        super(2, qn7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        C0100a aVar = new C0100a(this.this$0, qn7);
                        aVar.p$ = (iv7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super ActivitySummary> qn7) {
                        return ((C0100a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final Object invokeSuspend(Object obj) {
                        yn7.d();
                        if (this.label == 0) {
                            el7.b(obj);
                            gl6 gl6 = this.this$0.this$0.f1328a.this$0;
                            return gl6.i0(gl6.g, this.this$0.this$0.f1328a.this$0.l);
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(b bVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    a aVar = new a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object g;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        dv7 h = this.this$0.f1328a.this$0.h();
                        C0100a aVar = new C0100a(this, null);
                        this.L$0 = iv7;
                        this.label = 1;
                        g = eu7.g(h, aVar, this);
                        if (g == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        g = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    ActivitySummary activitySummary = (ActivitySummary) g;
                    if (this.this$0.f1328a.this$0.n == null || (!pq7.a(this.this$0.f1328a.this$0.n, activitySummary))) {
                        this.this$0.f1328a.this$0.n = activitySummary;
                        this.this$0.f1328a.this$0.t.J(this.this$0.f1328a.this$0.p, this.this$0.f1328a.this$0.n);
                        if (this.this$0.f1328a.this$0.j && this.this$0.f1328a.this$0.k) {
                            this.this$0.f1328a.this$0.m0();
                        }
                    }
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            public b(f fVar) {
                this.f1328a = fVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(h47<? extends List<ActivitySummary>> h47) {
                xh5 a2 = h47.a();
                List list = (List) h47.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("start - summaryTransformations -- activitySummaries=");
                sb.append(list != null ? Integer.valueOf(list.size()) : null);
                sb.append(", status=");
                sb.append(a2);
                local.d("ActivityDetailPresenter", sb.toString());
                if (a2 == xh5.NETWORK_LOADING || a2 == xh5.SUCCESS) {
                    this.f1328a.this$0.l = list;
                    this.f1328a.this$0.j = true;
                    xw7 unused = gu7.d(this.f1328a.this$0.k(), null, null, new a(this, null), 3, null);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c<T> implements ls0<h47<? extends List<ActivitySample>>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ f f1329a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$3$1", f = "ActivityDetailPresenter.kt", l = {186}, m = "invokeSuspend")
            public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ c this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.gl6$f$c$a$a")
                @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$3$1$samples$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
                /* renamed from: com.fossil.gl6$f$c$a$a  reason: collision with other inner class name */
                public static final class C0101a extends ko7 implements vp7<iv7, qn7<? super List<ActivitySample>>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0101a(a aVar, qn7 qn7) {
                        super(2, qn7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        C0101a aVar = new C0101a(this.this$0, qn7);
                        aVar.p$ = (iv7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super List<ActivitySample>> qn7) {
                        return ((C0101a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final Object invokeSuspend(Object obj) {
                        yn7.d();
                        if (this.label == 0) {
                            el7.b(obj);
                            gl6 gl6 = this.this$0.this$0.f1329a.this$0;
                            return gl6.h0(gl6.g, this.this$0.this$0.f1329a.this$0.m);
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(c cVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = cVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    a aVar = new a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object g;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        dv7 h = this.this$0.f1329a.this$0.h();
                        C0101a aVar = new C0101a(this, null);
                        this.L$0 = iv7;
                        this.label = 1;
                        g = eu7.g(h, aVar, this);
                        if (g == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        g = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    List list = (List) g;
                    if (this.this$0.f1329a.this$0.o == null || (!pq7.a(this.this$0.f1329a.this$0.o, list))) {
                        this.this$0.f1329a.this$0.o = list;
                        if (this.this$0.f1329a.this$0.j && this.this$0.f1329a.this$0.k) {
                            this.this$0.f1329a.this$0.m0();
                        }
                    }
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            public c(f fVar) {
                this.f1329a = fVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(h47<? extends List<ActivitySample>> h47) {
                xh5 a2 = h47.a();
                List list = (List) h47.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("start - sampleTransformations -- activitySamples=");
                sb.append(list != null ? Integer.valueOf(list.size()) : null);
                sb.append(", status=");
                sb.append(a2);
                local.d("ActivityDetailPresenter", sb.toString());
                if (a2 == xh5.NETWORK_LOADING || a2 == xh5.SUCCESS) {
                    this.f1329a.this$0.m = list;
                    this.f1329a.this$0.k = true;
                    xw7 unused = gu7.d(this.f1329a.this$0.k(), null, null, new a(this, null), 3, null);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(gl6 gl6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = gl6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            gl6 gl6;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                gl6 gl62 = this.this$0;
                dv7 i2 = gl62.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.L$1 = gl62;
                this.label = 1;
                g = eu7.g(i2, aVar, this);
                if (g == d) {
                    return d;
                }
                gl6 = gl62;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                gl6 = (gl6) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            pq7.b(g, "withContext(IO) {\n      \u2026TRIC.value)\n            }");
            gl6.p = (ai5) g;
            LiveData liveData = this.this$0.q;
            cl6 cl6 = this.this$0.t;
            if (cl6 != null) {
                liveData.h((dl6) cl6, new b(this));
                this.this$0.r.h((LifecycleOwner) this.this$0.t, new c(this));
                return tl7.f3441a;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gl6 f1330a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$summaryTransformations$1$1", f = "ActivityDetailPresenter.kt", l = {75, 75}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<ActivitySummary>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, Date date, Date date2, qn7 qn7) {
                super(2, qn7);
                this.this$0 = gVar;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$first, this.$second, qn7);
                aVar.p$ = (hs0) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<ActivitySummary>>> hs0, qn7<? super tl7> qn7) {
                return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r8)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r8)
                    com.fossil.hs0 r0 = r7.p$
                    com.fossil.gl6$g r1 = r7.this$0
                    com.fossil.gl6 r1 = r1.f1330a
                    com.portfolio.platform.data.source.SummariesRepository r1 = com.fossil.gl6.L(r1)
                    java.util.Date r2 = r7.$first
                    java.util.Date r3 = r7.$second
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getSummaries(r2, r3, r5, r7)
                    if (r2 != r4) goto L_0x005b
                    r0 = r4
                    goto L_0x0017
                L_0x005b:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.gl6.g.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public g(gl6 gl6) {
            this.f1330a = gl6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<ActivitySummary>>> apply(cl7<? extends Date, ? extends Date> cl7) {
            return or0.c(bw7.c(), 0, new a(this, (Date) cl7.component1(), (Date) cl7.component2(), null), 2, null);
        }
    }

    @DexIgnore
    public gl6(cl6 cl6, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FileRepository fileRepository, no4 no4, PortfolioApp portfolioApp) {
        pq7.c(cl6, "mView");
        pq7.c(summariesRepository, "mSummariesRepository");
        pq7.c(activitiesRepository, "mActivitiesRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(workoutSessionRepository, "mWorkoutSessionRepository");
        pq7.c(fileRepository, "mFileRepository");
        pq7.c(no4, "appExecutors");
        pq7.c(portfolioApp, "mApp");
        this.t = cl6;
        this.u = summariesRepository;
        this.v = activitiesRepository;
        this.w = userRepository;
        this.x = workoutSessionRepository;
        this.y = no4;
        this.e = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.J());
        this.i = new WorkoutTetherScreenShotManager(this.x, fileRepository);
        this.l = new ArrayList();
        this.m = new ArrayList();
        this.p = ai5.METRIC;
        LiveData<h47<List<ActivitySummary>>> c2 = ss0.c(this.h, new g(this));
        pq7.b(c2, "Transformations.switchMa\u2026t, second, true)) }\n    }");
        this.q = c2;
        LiveData<h47<List<ActivitySample>>> c3 = ss0.c(this.h, new c(this));
        pq7.b(c3, "Transformations.switchMa\u2026t, second, true)) }\n    }");
        this.r = c3;
    }

    @DexIgnore
    @Override // com.fossil.fl5.a
    public void e(fl5.g gVar) {
        pq7.c(gVar, "report");
    }

    @DexIgnore
    public final List<ActivitySample> h0(Date date, List<ActivitySample> list) {
        ts7 z;
        ts7 h2;
        if (list == null || (z = pm7.z(list)) == null || (h2 = at7.h(z, new a(date))) == null) {
            return null;
        }
        return at7.u(h2);
    }

    @DexIgnore
    public final ActivitySummary i0(Date date, List<ActivitySummary> list) {
        T t2;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (lk5.m0(next.getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return t2;
    }

    @DexIgnore
    /* renamed from: j0 */
    public WorkoutTetherScreenShotManager p() {
        return this.i;
    }

    @DexIgnore
    public final void k0(Date date) {
        LiveData<cu0<WorkoutSession>> pagedList;
        r();
        Listing<WorkoutSession> listing = this.s;
        if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
            cl6 cl6 = this.t;
            if (cl6 != null) {
                pagedList.n((dl6) cl6);
            } else {
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
            }
        }
        xw7 unused = gu7.d(k(), null, null, new b(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("ActivityDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        xw7 unused = gu7.d(k(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    public void l0() {
        this.t.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("ActivityDetailPresenter", "stop");
        LiveData<h47<List<ActivitySummary>>> liveData = this.q;
        cl6 cl6 = this.t;
        if (cl6 != null) {
            liveData.n((dl6) cl6);
            this.r.n((LifecycleOwner) this.t);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
    }

    @DexIgnore
    public final xw7 m0() {
        return gu7.d(k(), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.bl6
    public FossilDeviceSerialPatternUtil.DEVICE n() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        pq7.b(device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    @Override // com.fossil.bl6
    public ai5 o() {
        return this.p;
    }

    @DexIgnore
    @Override // com.fossil.bl6
    public void q(Date date) {
        pq7.c(date, "date");
        k0(date);
    }

    @DexIgnore
    @Override // com.fossil.bl6
    public void r() {
        LiveData<cu0<WorkoutSession>> pagedList;
        try {
            this.x.removePagingListener();
            Listing<WorkoutSession> listing = this.s;
            if (listing != null && (pagedList = listing.getPagedList()) != null) {
                cl6 cl6 = this.t;
                if (cl6 != null) {
                    pagedList.n((dl6) cl6);
                    return;
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.e("ActivityDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.bl6
    public void s(Bundle bundle) {
        pq7.c(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.g.getTime());
    }

    @DexIgnore
    @Override // com.fossil.bl6
    public void t(Date date) {
        pq7.c(date, "date");
        xw7 unused = gu7.d(k(), null, null, new d(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.bl6
    public void u() {
        Date O = lk5.O(this.g);
        pq7.b(O, "DateHelper.getNextDate(mDate)");
        t(O);
    }

    @DexIgnore
    @Override // com.fossil.bl6
    public void v() {
        Date P = lk5.P(this.g);
        pq7.b(P, "DateHelper.getPrevDate(mDate)");
        t(P);
    }
}
