package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w16 extends u47 implements v16 {
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public /* final */ zp0 k; // = new sr4(this);
    @DexIgnore
    public g37<l95> l;
    @DexIgnore
    public u16 m;
    @DexIgnore
    public po4 s;
    @DexIgnore
    public r16 t;
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return w16.v;
        }

        @DexIgnore
        public final w16 b() {
            return new w16();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ w16 b;

        @DexIgnore
        public b(w16 w16) {
            this.b = w16;
        }

        @DexIgnore
        public final void onClick(View view) {
            w16.A6(this.b).n(0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ w16 b;

        @DexIgnore
        public c(w16 w16) {
            this.b = w16;
        }

        @DexIgnore
        public final void onClick(View view) {
            w16.A6(this.b).n(1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ w16 b;

        @DexIgnore
        public d(w16 w16) {
            this.b = w16;
        }

        @DexIgnore
        public final void onClick(View view) {
            w16.A6(this.b).n(2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ w16 b;

        @DexIgnore
        public e(w16 w16) {
            this.b = w16;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.dismissAllowingStateLoss();
        }
    }

    /*
    static {
        String simpleName = w16.class.getSimpleName();
        pq7.b(simpleName, "NotificationSettingsType\u2026nt::class.java.simpleName");
        v = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ u16 A6(w16 w16) {
        u16 u16 = w16.m;
        if (u16 != null) {
            return u16;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void C6() {
        g37<l95> g37 = this.l;
        if (g37 != null) {
            l95 a2 = g37.a();
            if (a2 != null) {
                RTLImageView rTLImageView = a2.w;
                pq7.b(rTLImageView, "it.ivEveryoneCheck");
                rTLImageView.setVisibility(4);
                RTLImageView rTLImageView2 = a2.x;
                pq7.b(rTLImageView2, "it.ivFavoriteContactsCheck");
                rTLImageView2.setVisibility(4);
                RTLImageView rTLImageView3 = a2.y;
                pq7.b(rTLImageView3, "it.ivNoOneCheck");
                rTLImageView3.setVisibility(4);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: D6 */
    public void M5(u16 u16) {
        pq7.c(u16, "presenter");
        this.m = u16;
    }

    @DexIgnore
    @Override // com.fossil.v16
    public void e2(int i) {
        C6();
        g37<l95> g37 = this.l;
        if (g37 != null) {
            l95 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (i == 0) {
                RTLImageView rTLImageView = a2.w;
                pq7.b(rTLImageView, "it.ivEveryoneCheck");
                rTLImageView.setVisibility(0);
            } else if (i == 1) {
                RTLImageView rTLImageView2 = a2.x;
                pq7.b(rTLImageView2, "it.ivFavoriteContactsCheck");
                rTLImageView2.setVisibility(0);
            } else if (i == 2) {
                RTLImageView rTLImageView3 = a2.y;
                pq7.b(rTLImageView3, "it.ivNoOneCheck");
                rTLImageView3.setVisibility(0);
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        PortfolioApp.h0.c().M().U1(new y16(this)).c(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity = (NotificationCallsAndMessagesActivity) activity;
            po4 po4 = this.s;
            if (po4 != null) {
                ts0 a2 = vs0.f(notificationCallsAndMessagesActivity, po4).a(r16.class);
                pq7.b(a2, "ViewModelProviders.of(ac\u2026ingViewModel::class.java)");
                r16 r16 = (r16) a2;
                this.t = r16;
                u16 u16 = this.m;
                if (u16 == null) {
                    pq7.n("mPresenter");
                    throw null;
                } else if (r16 != null) {
                    u16.o(r16);
                } else {
                    pq7.n("mNotificationSettingViewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModelFactory");
                throw null;
            }
        } else {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        l95 l95 = (l95) aq0.f(layoutInflater, 2131558596, viewGroup, false, this.k);
        String d2 = qn5.l.a().d("nonBrandSurface");
        String d3 = qn5.l.a().d("nonBrandDisableCalendarDay");
        if (!TextUtils.isEmpty(d2)) {
            l95.q.setBackgroundColor(Color.parseColor(d2));
        }
        if (!TextUtils.isEmpty(d3)) {
            int parseColor = Color.parseColor(d3);
            l95.z.setBackgroundColor(parseColor);
            l95.A.setBackgroundColor(parseColor);
            l95.B.setBackgroundColor(parseColor);
        }
        l95.r.setOnClickListener(new b(this));
        l95.s.setOnClickListener(new c(this));
        l95.t.setOnClickListener(new d(this));
        l95.v.setOnClickListener(new e(this));
        this.l = new g37<>(this, l95);
        pq7.b(l95, "binding");
        return l95.n();
    }

    @DexIgnore
    @Override // com.fossil.u47, androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        u16 u16 = this.m;
        if (u16 != null) {
            u16.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        u16 u16 = this.m;
        if (u16 != null) {
            u16.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.v16
    public void t(String str) {
        FlexibleTextView flexibleTextView;
        pq7.c(str, "title");
        g37<l95> g37 = this.l;
        if (g37 != null) {
            l95 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.u) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.u47
    public void z6() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
