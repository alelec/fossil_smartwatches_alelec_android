package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v53 implements w53 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f3718a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.collection.event_safelist", true);

    @DexIgnore
    @Override // com.fossil.w53
    public final boolean zza() {
        return f3718a.o().booleanValue();
    }
}
