package com.fossil;

import com.fossil.av2;
import com.fossil.e13;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zu2 extends e13<zu2, a> implements o23 {
    @DexIgnore
    public static /* final */ zu2 zzd;
    @DexIgnore
    public static volatile z23<zu2> zze;
    @DexIgnore
    public m13<av2> zzc; // = e13.B();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<zu2, a> implements o23 {
        @DexIgnore
        public a() {
            super(zu2.zzd);
        }

        @DexIgnore
        public /* synthetic */ a(tu2 tu2) {
            this();
        }

        @DexIgnore
        public final a x(av2.a aVar) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((zu2) this.c).G((av2) ((e13) aVar.h()));
            return this;
        }

        @DexIgnore
        public final av2 y(int i) {
            return ((zu2) this.c).C(0);
        }
    }

    /*
    static {
        zu2 zu2 = new zu2();
        zzd = zu2;
        e13.u(zu2.class, zu2);
    }
    */

    @DexIgnore
    public static a H() {
        return (a) zzd.w();
    }

    @DexIgnore
    public final av2 C(int i) {
        return this.zzc.get(0);
    }

    @DexIgnore
    public final List<av2> D() {
        return this.zzc;
    }

    @DexIgnore
    public final void G(av2 av2) {
        av2.getClass();
        m13<av2> m13 = this.zzc;
        if (!m13.zza()) {
            this.zzc = e13.q(m13);
        }
        this.zzc.add(av2);
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (tu2.f3469a[i - 1]) {
            case 1:
                return new zu2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzd, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzc", av2.class});
            case 4:
                return zzd;
            case 5:
                z23<zu2> z232 = zze;
                if (z232 != null) {
                    return z232;
                }
                synchronized (zu2.class) {
                    try {
                        z23 = zze;
                        if (z23 == null) {
                            z23 = new e13.c(zzd);
                            zze = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
