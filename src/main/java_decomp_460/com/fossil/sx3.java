package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.os.Build;
import android.util.Property;
import android.view.View;
import android.view.ViewAnimationUtils;
import com.fossil.ux3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sx3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ux3 f3328a;

        @DexIgnore
        public a(ux3 ux3) {
            this.f3328a = ux3;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.f3328a.b();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            this.f3328a.a();
        }
    }

    @DexIgnore
    public static Animator a(ux3 ux3, float f, float f2, float f3) {
        ObjectAnimator ofObject = ObjectAnimator.ofObject(ux3, (Property<ux3, V>) ux3.c.f3657a, (TypeEvaluator) ux3.b.b, (Object[]) new ux3.e[]{new ux3.e(f, f2, f3)});
        if (Build.VERSION.SDK_INT < 21) {
            return ofObject;
        }
        ux3.e revealInfo = ux3.getRevealInfo();
        if (revealInfo != null) {
            Animator createCircularReveal = ViewAnimationUtils.createCircularReveal((View) ux3, (int) f, (int) f2, revealInfo.c, f3);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(ofObject, createCircularReveal);
            return animatorSet;
        }
        throw new IllegalStateException("Caller must set a non-null RevealInfo before calling this.");
    }

    @DexIgnore
    public static Animator.AnimatorListener b(ux3 ux3) {
        return new a(ux3);
    }
}
