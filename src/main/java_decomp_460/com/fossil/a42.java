package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a42 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public y52 f191a;
    @DexIgnore
    public sk2 b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public b e;
    @DexIgnore
    public /* final */ Context f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ long h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f192a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public a(String str, boolean z) {
            this.f192a = str;
            this.b = z;
        }

        @DexIgnore
        public final String a() {
            return this.f192a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public final String toString() {
            String str = this.f192a;
            boolean z = this.b;
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 7);
            sb.append("{");
            sb.append(str);
            sb.append("}");
            sb.append(z);
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Thread {
        @DexIgnore
        public WeakReference<a42> b;
        @DexIgnore
        public long c;
        @DexIgnore
        public CountDownLatch d; // = new CountDownLatch(1);
        @DexIgnore
        public boolean e; // = false;

        @DexIgnore
        public b(a42 a42, long j) {
            this.b = new WeakReference<>(a42);
            this.c = j;
            start();
        }

        @DexIgnore
        public final void a() {
            a42 a42 = this.b.get();
            if (a42 != null) {
                a42.a();
                this.e = true;
            }
        }

        @DexIgnore
        public final void run() {
            try {
                if (!this.d.await(this.c, TimeUnit.MILLISECONDS)) {
                    a();
                }
            } catch (InterruptedException e2) {
                a();
            }
        }
    }

    @DexIgnore
    public a42(Context context, long j, boolean z, boolean z2) {
        Context applicationContext;
        rc2.k(context);
        if (z && (applicationContext = context.getApplicationContext()) != null) {
            context = applicationContext;
        }
        this.f = context;
        this.c = false;
        this.h = j;
        this.g = z2;
    }

    @DexIgnore
    public static a b(Context context) throws IOException, IllegalStateException, e62, f62 {
        c42 c42 = new c42(context);
        boolean a2 = c42.a("gads:ad_id_app_context:enabled", false);
        float b2 = c42.b("gads:ad_id_app_context:ping_ratio", LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        String c2 = c42.c("gads:ad_id_use_shared_preference:experiment_id", "");
        a42 a42 = new a42(context, -1, a2, c42.a("gads:ad_id_use_persistent_service:enabled", false));
        try {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            a42.h(false);
            a c3 = a42.c();
            a42.i(c3, a2, b2, SystemClock.elapsedRealtime() - elapsedRealtime, c2, null);
            a42.a();
            return c3;
        } catch (Throwable th) {
            a42.a();
            throw th;
        }
    }

    @DexIgnore
    public static void d(boolean z) {
    }

    @DexIgnore
    public static y52 e(Context context, boolean z) throws IOException, e62, f62 {
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            int j = d62.h().j(context, h62.f1430a);
            if (j == 0 || j == 2) {
                String str = z ? "com.google.android.gms.ads.identifier.service.PERSISTENT_START" : "com.google.android.gms.ads.identifier.service.START";
                y52 y52 = new y52();
                Intent intent = new Intent(str);
                intent.setPackage("com.google.android.gms");
                try {
                    if (ve2.b().a(context, intent, y52, 1)) {
                        return y52;
                    }
                    throw new IOException("Connection failure");
                } catch (Throwable th) {
                    throw new IOException(th);
                }
            } else {
                throw new IOException("Google Play services not available");
            }
        } catch (PackageManager.NameNotFoundException e2) {
            throw new e62(9);
        }
    }

    @DexIgnore
    public static sk2 f(Context context, y52 y52) throws IOException {
        try {
            return tk2.d(y52.a(ButtonService.CONNECT_TIMEOUT, TimeUnit.MILLISECONDS));
        } catch (InterruptedException e2) {
            throw new IOException("Interrupted exception");
        } catch (Throwable th) {
            throw new IOException(th);
        }
    }

    @DexIgnore
    public final void a() {
        rc2.j("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.f != null && this.f191a != null) {
                try {
                    if (this.c) {
                        ve2.b().c(this.f, this.f191a);
                    }
                } catch (Throwable th) {
                    Log.i("AdvertisingIdClient", "AdvertisingIdClient unbindService failed.", th);
                }
                this.c = false;
                this.b = null;
                this.f191a = null;
            }
        }
    }

    @DexIgnore
    public a c() throws IOException {
        a aVar;
        rc2.j("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (!this.c) {
                synchronized (this.d) {
                    if (this.e == null || !this.e.e) {
                        throw new IOException("AdvertisingIdClient is not connected.");
                    }
                }
                try {
                    h(false);
                    if (!this.c) {
                        throw new IOException("AdvertisingIdClient cannot reconnect.");
                    }
                } catch (Exception e2) {
                    throw new IOException("AdvertisingIdClient cannot reconnect.", e2);
                }
            }
            rc2.k(this.f191a);
            rc2.k(this.b);
            try {
                aVar = new a(this.b.getId(), this.b.x0(true));
            } catch (RemoteException e3) {
                Log.i("AdvertisingIdClient", "GMS remote exception ", e3);
                throw new IOException("Remote exception");
            }
        }
        g();
        return aVar;
    }

    @DexIgnore
    public void finalize() throws Throwable {
        a();
        super.finalize();
    }

    @DexIgnore
    public final void g() {
        synchronized (this.d) {
            if (this.e != null) {
                this.e.d.countDown();
                try {
                    this.e.join();
                } catch (InterruptedException e2) {
                }
            }
            if (this.h > 0) {
                this.e = new b(this, this.h);
            }
        }
    }

    @DexIgnore
    public final void h(boolean z) throws IOException, IllegalStateException, e62, f62 {
        rc2.j("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.c) {
                a();
            }
            y52 e2 = e(this.f, this.g);
            this.f191a = e2;
            this.b = f(this.f, e2);
            this.c = true;
            if (z) {
                g();
            }
        }
    }

    @DexIgnore
    public final boolean i(a aVar, boolean z, float f2, long j, String str, Throwable th) {
        if (Math.random() > ((double) f2)) {
            return false;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("app_context", z ? "1" : "0");
        if (aVar != null) {
            hashMap.put("limit_ad_tracking", aVar.b() ? "1" : "0");
        }
        if (!(aVar == null || aVar.a() == null)) {
            hashMap.put("ad_id_size", Integer.toString(aVar.a().length()));
        }
        if (th != null) {
            hashMap.put("error", th.getClass().getName());
        }
        if (str != null && !str.isEmpty()) {
            hashMap.put("experiment_id", str);
        }
        hashMap.put("tag", "AdvertisingIdClient");
        hashMap.put("time_spent", Long.toString(j));
        new b42(this, hashMap).start();
        return true;
    }
}
