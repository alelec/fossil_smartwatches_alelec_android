package com.fossil;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.fossil.hg4;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"UnwrappedWakefulBroadcastReceiver"})
public abstract class sh4 extends Service {
    @DexIgnore
    public /* final */ ExecutorService b; // = th4.b();
    @DexIgnore
    public Binder c;
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public int e;
    @DexIgnore
    public int f; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements hg4.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.hg4.a
        public nt3<Void> a(Intent intent) {
            return sh4.this.h(intent);
        }
    }

    @DexIgnore
    public final void b(Intent intent) {
        if (intent != null) {
            fg4.b(intent);
        }
        synchronized (this.d) {
            int i = this.f - 1;
            this.f = i;
            if (i == 0) {
                i(this.e);
            }
        }
    }

    @DexIgnore
    public Intent c(Intent intent) {
        return intent;
    }

    @DexIgnore
    public abstract void d(Intent intent);

    @DexIgnore
    public boolean e(Intent intent) {
        return false;
    }

    @DexIgnore
    public final /* synthetic */ void f(Intent intent, nt3 nt3) {
        b(intent);
    }

    @DexIgnore
    public final /* synthetic */ void g(Intent intent, ot3 ot3) {
        try {
            d(intent);
        } finally {
            ot3.c(null);
        }
    }

    @DexIgnore
    public final nt3<Void> h(Intent intent) {
        if (e(intent)) {
            return qt3.f(null);
        }
        ot3 ot3 = new ot3();
        this.b.execute(new ph4(this, intent, ot3));
        return ot3.a();
    }

    @DexIgnore
    public boolean i(int i) {
        return stopSelfResult(i);
    }

    @DexIgnore
    public final IBinder onBind(Intent intent) {
        Binder binder;
        synchronized (this) {
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "Service received bind request");
            }
            if (this.c == null) {
                this.c = new hg4(new a());
            }
            binder = this.c;
        }
        return binder;
    }

    @DexIgnore
    public void onDestroy() {
        this.b.shutdown();
        super.onDestroy();
    }

    @DexIgnore
    public final int onStartCommand(Intent intent, int i, int i2) {
        synchronized (this.d) {
            this.e = i2;
            this.f++;
        }
        Intent c2 = c(intent);
        if (c2 == null) {
            b(intent);
            return 2;
        }
        nt3<Void> h = h(c2);
        if (h.p()) {
            b(intent);
            return 2;
        }
        h.c(qh4.b, new rh4(this, intent));
        return 3;
    }
}
