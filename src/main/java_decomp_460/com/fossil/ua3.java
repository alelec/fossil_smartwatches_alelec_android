package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ua3 implements Parcelable.Creator<ia3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ia3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        boolean z = false;
        sa3 sa3 = null;
        ArrayList arrayList = null;
        boolean z2 = false;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                arrayList = ad2.j(parcel, t, LocationRequest.CREATOR);
            } else if (l == 2) {
                z2 = ad2.m(parcel, t);
            } else if (l == 3) {
                z = ad2.m(parcel, t);
            } else if (l != 5) {
                ad2.B(parcel, t);
            } else {
                sa3 = (sa3) ad2.e(parcel, t, sa3.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new ia3(arrayList, z2, z, sa3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ia3[] newArray(int i) {
        return new ia3[i];
    }
}
