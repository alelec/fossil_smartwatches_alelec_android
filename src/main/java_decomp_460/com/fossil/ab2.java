package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ab2 implements r92 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ ya2 f241a;

    @DexIgnore
    public ab2(ya2 ya2) {
        this.f241a = ya2;
    }

    @DexIgnore
    public /* synthetic */ ab2(ya2 ya2, xa2 xa2) {
        this(ya2);
    }

    @DexIgnore
    @Override // com.fossil.r92
    public final void a(z52 z52) {
        this.f241a.s.lock();
        try {
            this.f241a.k = z52;
            this.f241a.C();
        } finally {
            this.f241a.s.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.r92
    public final void b(Bundle bundle) {
        this.f241a.s.lock();
        try {
            this.f241a.p(bundle);
            this.f241a.k = z52.f;
            this.f241a.C();
        } finally {
            this.f241a.s.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.r92
    public final void c(int i, boolean z) {
        this.f241a.s.lock();
        try {
            if ((this.f241a.m) || this.f241a.l == null || !this.f241a.l.A()) {
                this.f241a.m = false;
                this.f241a.o(i, z);
                return;
            }
            this.f241a.m = true;
            this.f241a.f.d(i);
            this.f241a.s.unlock();
        } finally {
            this.f241a.s.unlock();
        }
    }
}
