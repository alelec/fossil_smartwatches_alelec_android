package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface hs0<T> {
    @DexIgnore
    Object a(LiveData<T> liveData, qn7<? super dw7> qn7);

    @DexIgnore
    Object b(T t, qn7<? super tl7> qn7);
}
