package com.fossil;

import android.app.Activity;
import androidx.fragment.app.FragmentActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n72 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f2478a;

    @DexIgnore
    public n72(Activity activity) {
        rc2.l(activity, "Activity must not be null");
        this.f2478a = activity;
    }

    @DexIgnore
    public Activity a() {
        return (Activity) this.f2478a;
    }

    @DexIgnore
    public FragmentActivity b() {
        return (FragmentActivity) this.f2478a;
    }

    @DexIgnore
    public boolean c() {
        return this.f2478a instanceof FragmentActivity;
    }

    @DexIgnore
    public final boolean d() {
        return this.f2478a instanceof Activity;
    }
}
