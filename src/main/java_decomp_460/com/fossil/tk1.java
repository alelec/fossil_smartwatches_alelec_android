package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.PlaceManager;
import com.fossil.yk1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.joda.time.DateTimeFieldType;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tk1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Handler f3429a;
    @DexIgnore
    public static /* final */ BluetoothAdapter b; // = BluetoothAdapter.getDefaultAdapter();
    @DexIgnore
    public static f c;
    @DexIgnore
    public static /* final */ Hashtable<String, py1<zk1, nr>> d; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ ScanCallback e; // = new i();
    @DexIgnore
    public static /* final */ LinkedHashMap<b, xk1> f; // = new LinkedHashMap<>();
    @DexIgnore
    public static /* final */ BroadcastReceiver g; // = new g();
    @DexIgnore
    public static /* final */ BroadcastReceiver h; // = new h();
    @DexIgnore
    public static boolean i;
    @DexIgnore
    public static a j;
    @DexIgnore
    public static /* final */ tk1 k; // = new tk1();

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(tk1 tk1, c cVar, c cVar2);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void onDeviceFound(yk1 yk1, int i);

        @DexIgnore
        void onStartScanFailed(uk1 uk1);
    }

    @DexIgnore
    public enum c {
        DISABLED(10),
        ENABLING(11),
        ENABLED(12),
        DISABLING(13);
        
        @DexIgnore
        public static /* final */ a d; // = new a(null);
        @DexIgnore
        public /* final */ int b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public /* synthetic */ a(kq7 kq7) {
            }

            @DexIgnore
            public final c a(int i) {
                c cVar;
                c[] values = c.values();
                int length = values.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        cVar = null;
                        break;
                    }
                    cVar = values[i2];
                    if (cVar.a() == i) {
                        break;
                    }
                    i2++;
                }
                return cVar != null ? cVar : c.DISABLED;
            }
        }

        @DexIgnore
        public c(int i) {
            this.b = i;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class d extends Enum<d> {
        @DexIgnore
        public static /* final */ d c;
        @DexIgnore
        public static /* final */ d d;
        @DexIgnore
        public static /* final */ d e;
        @DexIgnore
        public static /* final */ d f;
        @DexIgnore
        public static /* final */ d g;
        @DexIgnore
        public static /* final */ d h;
        @DexIgnore
        public static /* final */ d i;
        @DexIgnore
        public static /* final */ d j;
        @DexIgnore
        public static /* final */ d k;
        @DexIgnore
        public static /* final */ /* synthetic */ d[] l;
        @DexIgnore
        public static /* final */ f m; // = new f(null);
        @DexIgnore
        public /* final */ byte b;

        /*
        static {
            d dVar = new d("FLAG", 0, (byte) 1);
            d dVar2 = new d("INCOMPLETE_16_BIT_SERVICE_CLASS_UUID", 1, (byte) 2);
            c = dVar2;
            d dVar3 = new d("COMPLETE_16_BIT_SERVICE_CLASS_UUID", 2, (byte) 3);
            d = dVar3;
            d dVar4 = new d("INCOMPLETE_32_BIT_SERVICE_CLASS_UUID", 3, (byte) 4);
            e = dVar4;
            d dVar5 = new d("COMPLETE_32_BIT_SERVICE_CLASS_UUID", 4, (byte) 5);
            f = dVar5;
            d dVar6 = new d("INCOMPLETE_128_BIT_SERVICE_CLASS_UUID", 5, (byte) 6);
            g = dVar6;
            d dVar7 = new d("COMPLETE_128_BIT_SERVICE_CLASS_UUID", 6, (byte) 7);
            h = dVar7;
            d dVar8 = new d("COMPLETE_LOCAL_NAME", 7, (byte) 9);
            i = dVar8;
            d dVar9 = new d("SERVICE_DATA_16_BIT_SERVICE_CLASS_UUID", 8, DateTimeFieldType.MILLIS_OF_DAY);
            j = dVar9;
            d dVar10 = new d("MANUFACTURER_SPECIFIC_DATA", 9, (byte) 255);
            k = dVar10;
            l = new d[]{dVar, dVar2, dVar3, dVar4, dVar5, dVar6, dVar7, dVar8, dVar9, dVar10};
        }
        */

        @DexIgnore
        public d(String str, int i2, byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public static d valueOf(String str) {
            return (d) Enum.valueOf(d.class, str);
        }

        @DexIgnore
        public static d[] values() {
            return (d[]) l.clone();
        }
    }

    @DexIgnore
    public enum e {
        START_SCAN,
        START_SCAN_FAILED,
        STOP_SCAN,
        DEVICE_RETRIEVED,
        DEVICE_FOUND,
        GET_GATT_CONNECTED_DEVICES,
        GET_HID_CONNECTED_DEVICES,
        BOND_STATE_CHANGED,
        BLUETOOTH_STATE_CHANGED
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public boolean b;

        @DexIgnore
        public final JSONArray a(List<BluetoothDevice> list) {
            JSONArray jSONArray = new JSONArray();
            for (T t : list) {
                JSONObject jSONObject = new JSONObject();
                jd0 jd0 = jd0.H;
                Object name = t.getName();
                if (name == null) {
                    name = JSONObject.NULL;
                }
                jSONArray.put(g80.k(g80.k(g80.k(jSONObject, jd0, name), jd0.k0, t.getAddress()), jd0.o1, r4.f.a(t.getBondState())));
            }
            return jSONArray;
        }

        @DexIgnore
        public void run() {
            if (!this.b) {
                m80.c.a("BluetoothLeAdapter", "getGattConnectedDevice.", new Object[0]);
                List<BluetoothDevice> b2 = tk1.k.b();
                m80.c.a("BluetoothLeAdapter", "gattConnectedBluetoothDevices: %s.", pm7.N(b2, null, null, null, 0, null, g.b, 31, null));
                d90.i.d(new a90(ey1.a(e.GET_GATT_CONNECTED_DEVICES), v80.h, "", "", "", true, null, null, null, g80.k(new JSONObject(), jd0.D1, a(b2)), 448));
                List<BluetoothDevice> o = tk1.k.o();
                m80.c.a("BluetoothLeAdapter", "hidConnectedBluetoothDevices: %s.", pm7.N(o, null, null, null, 0, null, h.b, 31, null));
                d90.i.d(new a90(ey1.a(e.GET_HID_CONNECTED_DEVICES), v80.h, "", "", "", true, null, null, null, g80.k(new JSONObject(), jd0.E1, a(o)), 448));
                ArrayList arrayList = new ArrayList(b2);
                arrayList.addAll(o);
                for (BluetoothDevice bluetoothDevice : pm7.C(arrayList)) {
                    if (bluetoothDevice.getType() != 1) {
                        w00 w00 = w00.c;
                        String address = bluetoothDevice.getAddress();
                        pq7.b(address, "bluetoothDevice.address");
                        String e = w00.e(address);
                        if (e != null) {
                            e60 a2 = w00.c.a(bluetoothDevice, e);
                            tk1 tk1 = tk1.k;
                            if (tk1.p(a2)) {
                                tk1.d(a2, 0);
                            }
                        } else {
                            tk1 tk12 = tk1.k;
                            if (tk1.d.get(bluetoothDevice.getAddress()) == null) {
                                e60 a3 = w00.c.a(bluetoothDevice, "");
                                HashMap<ym, Object> i = zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L));
                                m80.c.a("BluetoothLeAdapter", "getGattConnectedDevice: makeDeviceReady to get Serial Number.", new Object[0]);
                                py1<zk1, nr> z0 = a3.z0(i);
                                tk1 tk13 = tk1.k;
                                tk1.d.put(bluetoothDevice.getAddress(), z0);
                                z0.m(new i(bluetoothDevice, a3)).l(new j(bluetoothDevice));
                            }
                        }
                    }
                }
                tk1 tk14 = tk1.k;
                tk1.f3429a.postDelayed(this, 5000);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            String action;
            if (context != null && intent != null && (action = intent.getAction()) != null && action.hashCode() == -1530327060 && action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                tk1.k.k(c.d.a(intent.getIntExtra("android.bluetooth.adapter.extra.PREVIOUS_STATE", RecyclerView.UNDEFINED_DURATION)), c.d.a(intent.getIntExtra("android.bluetooth.adapter.extra.STATE", RecyclerView.UNDEFINED_DURATION)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (context != null && intent != null) {
                BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                int intExtra = intent.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", -1);
                int intExtra2 = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -1);
                if (bluetoothDevice != null) {
                    tk1.k.e(bluetoothDevice, intExtra, intExtra2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends ScanCallback {
        @DexIgnore
        public void onScanFailed(int i) {
            tk1.k.m(new uk1(vk1.f.a(i)));
        }

        @DexIgnore
        public void onScanResult(int i, ScanResult scanResult) {
            ScanRecord scanRecord;
            byte[] bytes = (scanResult == null || (scanRecord = scanResult.getScanRecord()) == null) ? null : scanRecord.getBytes();
            BluetoothDevice device = scanResult != null ? scanResult.getDevice() : null;
            if (bytes != null && device != null) {
                if (tk1.k.t(bytes)) {
                    String v = tk1.k.v(bytes);
                    if (zk1.u.d(v)) {
                        e60 a2 = w00.c.a(device, v);
                        if (tk1.k.p(a2)) {
                            tk1.k.d(a2, scanResult.getRssi());
                        }
                    }
                } else if (tk1.k.u(bytes)) {
                    e60 a3 = w00.c.a(device, "");
                    zk1 a4 = zk1.a(a3.u, tk1.k.a(bytes), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, dy1.e(tk1.k.q(bytes), null, 1, null), 131070);
                    a3.u = a4;
                    a4.a(al1.WEAR_OS);
                    tk1.k.d(a3, scanResult.getRssi());
                }
            }
        }
    }

    /*
    static {
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        if (myLooper != null) {
            f3429a = new Handler(myLooper);
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ void l(tk1 tk1, e60 e60) {
        if (tk1.p(e60)) {
            tk1.d(e60, 0);
        }
    }

    @DexIgnore
    public final String a(byte[] bArr) {
        int i2 = 0;
        while (i2 < bArr.length) {
            int i3 = i2 + 1;
            short p = hy1.p((byte) (bArr[i2] & ((byte) 255)));
            if (p == 0 || i3 + 1 > bArr.length) {
                break;
            } else if (d.m.a(bArr[i3]) == d.i) {
                return new String(dm7.k(dm7.k(bArr, i3, i3 + p), 1, p), hd0.y.c());
            } else {
                i2 = p + i3;
            }
        }
        return "";
    }

    @DexIgnore
    public final List<BluetoothDevice> b() {
        List<BluetoothDevice> list = null;
        ArrayList arrayList = new ArrayList();
        Context a2 = id0.i.a();
        BluetoothManager bluetoothManager = (BluetoothManager) (a2 != null ? a2.getSystemService(PlaceManager.PARAM_BLUETOOTH) : null);
        if (bluetoothManager != null) {
            list = bluetoothManager.getConnectedDevices(7);
        }
        if (list != null) {
            arrayList.addAll(list);
        }
        return arrayList;
    }

    @DexIgnore
    public final JSONObject c(e60 e60) {
        return g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.l0, e60.u.getSerialNumber()), jd0.n1, ey1.a(e60.v)), jd0.k0, e60.u.getMacAddress()), jd0.o1, ey1.a(yk1.a.c.b(e60.d.H())));
    }

    @DexIgnore
    public final void d(e60 e60, int i2) {
        Set<Map.Entry<b, xk1>> entrySet;
        synchronized (f) {
            entrySet = f.entrySet();
            pq7.b(entrySet, "activeLeScanCallbacks.entries");
            tl7 tl7 = tl7.f3441a;
        }
        for (T t : entrySet) {
            if (((xk1) t.getValue()).a(e60)) {
                k.h((b) t.getKey(), e60, i2);
            }
        }
    }

    @DexIgnore
    public final void e(BluetoothDevice bluetoothDevice, int i2, int i3) {
        String str;
        m80 m80 = m80.c;
        String address = bluetoothDevice.getAddress();
        String str2 = "UNKNOWN";
        switch (i2) {
            case 10:
                str = "BOND_NONE";
                break;
            case 11:
                str = "BOND_BONDING";
                break;
            case 12:
                str = "BOND_BONDED";
                break;
            default:
                str = "UNKNOWN";
                break;
        }
        switch (i3) {
            case 10:
                str2 = "BOND_NONE";
                break;
            case 11:
                str2 = "BOND_BONDING";
                break;
            case 12:
                str2 = "BOND_BONDED";
                break;
        }
        m80.a("BluetoothLeAdapter", "Bond state changed: device=%s, %s(%d) -> %s(%d).", address, str, Integer.valueOf(i2), str2, Integer.valueOf(i3));
        d90 d90 = d90.i;
        String a2 = ey1.a(e.BOND_STATE_CHANGED);
        v80 v80 = v80.h;
        String address2 = bluetoothDevice.getAddress();
        if (address2 == null) {
            address2 = "";
        }
        JSONObject jSONObject = new JSONObject();
        jd0 jd0 = jd0.k0;
        String address3 = bluetoothDevice.getAddress();
        d90.d(new a90(a2, v80, address2, "", "", true, null, null, null, g80.k(g80.k(g80.k(jSONObject, jd0, address3 != null ? address3 : ""), jd0.C0, ey1.a(yk1.a.c.a(i2))), jd0.B0, ey1.a(yk1.a.c.a(i3))), 448));
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.BLUETOOTH_DEVICE", bluetoothDevice);
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_BOND_STATE", i2);
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_BOND_STATE", i3);
        Context a3 = id0.i.a();
        if (a3 != null) {
            ct0.b(a3).d(intent);
        }
    }

    @DexIgnore
    public final void f(Context context) {
        context.registerReceiver(g, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        context.registerReceiver(h, new IntentFilter("android.bluetooth.device.action.BOND_STATE_CHANGED"));
    }

    @DexIgnore
    public final void g(b bVar) {
        boolean z;
        BluetoothLeScanner bluetoothLeScanner;
        m80.c.a("BluetoothLeAdapter", "internalStopScan invoked: leScanCallback=%s.", hy1.k(bVar.hashCode(), null, 1, null));
        synchronized (f) {
            f.remove(bVar);
            z = f.size() == 0;
            tl7 tl7 = tl7.f3441a;
        }
        if (z) {
            f fVar = c;
            if (fVar != null) {
                fVar.b = true;
            }
            f fVar2 = c;
            if (fVar2 != null) {
                f3429a.removeCallbacks(fVar2);
            }
            c = null;
            Collection<py1<zk1, nr>> values = d.values();
            pq7.b(values, "deviceInFetchingInformation.values");
            Object[] array = values.toArray(new py1[0]);
            if (array != null) {
                for (Object obj : array) {
                    ((py1) obj).d(new nr(null, zq.INTERRUPTED, null, null, 13));
                }
                BluetoothAdapter bluetoothAdapter = b;
                if (!(bluetoothAdapter == null || (bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner()) == null)) {
                    bluetoothLeScanner.stopScan(e);
                }
                w00.c.c();
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public final void h(b bVar, e60 e60, int i2) {
        m80.c.a("BluetoothLeAdapter", "return found device: MAC=%s, Serial=%s, rssi=%d.", e60.u.getMacAddress(), e60.u.getSerialNumber(), Integer.valueOf(i2));
        d90.i.d(new a90(ey1.a(e.DEVICE_FOUND), v80.h, "", "", "", true, null, null, null, g80.k(g80.k(g80.k(new JSONObject(), jd0.F1, hy1.k(bVar.hashCode(), null, 1, null)), jd0.m1, c(e60)), jd0.c, Integer.valueOf(i2)), 448));
        bVar.onDeviceFound(e60, i2);
    }

    @DexIgnore
    public final void i(b bVar, uk1 uk1) {
        d90.i.d(new a90(ey1.a(e.START_SCAN_FAILED), v80.h, "", "", "", false, null, null, null, g80.k(g80.k(new JSONObject(), jd0.F1, String.valueOf(bVar.hashCode())), jd0.G1, uk1.toJSONObject()), 448));
        m80.c.a("BluetoothLeAdapter", "startScan FAILED: leScanCallback=%s, error=%s.", hy1.k(bVar.hashCode(), null, 1, null), uk1.toJSONString(2));
        bVar.onStartScanFailed(uk1);
    }

    @DexIgnore
    public final void j(b bVar, xk1 xk1) {
        boolean z = true;
        m80.c.a("BluetoothLeAdapter", "internalStartScan invoked: deviceScanFilter=%s, leScanCallback=%s.", ox1.toJSONString$default(xk1, 0, 1, null), hy1.k(bVar.hashCode(), null, 1, null));
        if (w() != c.ENABLED) {
            i(bVar, new uk1(vk1.BLUETOOTH_OFF));
            return;
        }
        synchronized (f) {
            f.put(bVar, xk1);
            if (f.size() != 1) {
                z = false;
            }
            tl7 tl7 = tl7.f3441a;
        }
        if (z) {
            ArrayList arrayList = new ArrayList();
            ScanSettings build = new ScanSettings.Builder().setScanMode(2).build();
            pq7.b(build, "ScanSettings.Builder()\n \u2026MODE_LOW_LATENCY).build()");
            n(arrayList, build);
            f fVar = new f();
            c = fVar;
            if (fVar != null) {
                fVar.run();
            }
        }
    }

    @DexIgnore
    public final void k(c cVar, c cVar2) {
        m80.c.a("BluetoothLeAdapter", "Bluetooth state changed: %s -> %s.", cVar, cVar2);
        d90.i.d(new a90(ey1.a(e.BLUETOOTH_STATE_CHANGED), v80.g, "", "", "", true, null, null, null, g80.k(g80.k(new JSONObject(), jd0.C0, ey1.a(cVar)), jd0.B0, ey1.a(cVar2)), 448));
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_STATE", cVar);
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_STATE", cVar2);
        Context a2 = id0.i.a();
        if (a2 != null) {
            ct0.b(a2).d(intent);
        }
        int i2 = k.b[cVar2.ordinal()];
        if (i2 == 2 || i2 == 3 || i2 == 4) {
            s();
            m80.c.a("BluetoothLeAdapter", "Stop all done: %d.", Integer.valueOf(f.size()));
        }
        a aVar = j;
        if (aVar != null) {
            aVar.a(this, cVar, cVar2);
        }
    }

    @DexIgnore
    public final void m(uk1 uk1) {
        Set<b> keySet;
        synchronized (f) {
            keySet = f.keySet();
            pq7.b(keySet, "activeLeScanCallbacks.keys");
            tl7 tl7 = tl7.f3441a;
        }
        Iterator<T> it = keySet.iterator();
        while (it.hasNext()) {
            k.i(it.next(), uk1);
        }
    }

    @DexIgnore
    public final void n(List<ScanFilter> list, ScanSettings scanSettings) {
        BluetoothAdapter bluetoothAdapter = b;
        BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter != null ? bluetoothAdapter.getBluetoothLeScanner() : null;
        if (bluetoothLeScanner == null) {
            m(new uk1(vk1.UNKNOWN_ERROR));
        } else {
            bluetoothLeScanner.startScan(list, scanSettings, e);
        }
    }

    @DexIgnore
    public final List<BluetoothDevice> o() {
        return l80.d.d();
    }

    @DexIgnore
    public final boolean p(e60 e60) {
        return i || e60.u.getDeviceType() != al1.UNKNOWN;
    }

    @DexIgnore
    public final byte[] q(byte[] bArr) {
        int i2 = 0;
        while (i2 < bArr.length) {
            int i3 = i2 + 1;
            short p = hy1.p((byte) (bArr[i2] & ((byte) 255)));
            if (p == 0 || i3 + 1 > bArr.length) {
                break;
            }
            if (d.m.a(bArr[i3]) == d.j && p >= 3) {
                byte[] k2 = dm7.k(bArr, i3, i3 + p);
                ByteBuffer order = ByteBuffer.wrap(dm7.k(k2, 1, 3)).order(ByteOrder.LITTLE_ENDIAN);
                pq7.b(order, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
                if (order.getShort() == -468) {
                    return dm7.k(k2, 3, p);
                }
            }
            i2 = p + i3;
        }
        return new byte[0];
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.HashMap<com.fossil.tk1$d, java.lang.String[]> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d A[LOOP:1: B:17:0x005b->B:18:0x005d, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0096 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.HashMap<com.fossil.tk1.d, java.lang.String[]> r(byte[] r14) {
        /*
            r13 = this;
            r12 = 0
            r1 = 0
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            r0 = r1
        L_0x0008:
            int r2 = r14.length
            if (r0 >= r2) goto L_0x001f
            int r4 = r0 + 1
            byte r0 = r14[r0]
            r2 = 255(0xff, float:3.57E-43)
            byte r2 = (byte) r2
            r0 = r0 & r2
            byte r0 = (byte) r0
            short r5 = com.fossil.hy1.p(r0)
            if (r5 == 0) goto L_0x001f
            int r6 = r4 + 1
            int r0 = r14.length
            if (r6 <= r0) goto L_0x0020
        L_0x001f:
            return r3
        L_0x0020:
            com.fossil.f r0 = com.fossil.tk1.d.m
            byte r2 = r14[r4]
            com.fossil.tk1$d r7 = r0.a(r2)
            if (r7 != 0) goto L_0x006e
        L_0x002a:
            r2 = r1
        L_0x002b:
            if (r7 == 0) goto L_0x0092
            if (r2 <= 0) goto L_0x0092
            int r0 = r4 + r5
            byte[] r6 = java.util.Arrays.copyOfRange(r14, r6, r0)
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            java.lang.Object r0 = r3.get(r7)
            java.lang.String[] r0 = (java.lang.String[]) r0
            if (r0 == 0) goto L_0x004a
            java.lang.String r9 = "currentServiceUUIDs"
            com.fossil.pq7.b(r0, r9)
            com.fossil.mm7.t(r8, r0)
        L_0x004a:
            java.lang.String r0 = "serviceUUIDRaw"
            com.fossil.pq7.b(r6, r0)
            byte[][] r2 = com.fossil.dy1.b(r6, r2)
            java.util.ArrayList r6 = new java.util.ArrayList
            int r0 = r2.length
            r6.<init>(r0)
            int r9 = r2.length
            r0 = r1
        L_0x005b:
            if (r0 >= r9) goto L_0x0084
            r10 = r2[r0]
            byte[] r10 = com.fossil.em7.V(r10)
            r11 = 1
            java.lang.String r10 = com.fossil.dy1.e(r10, r12, r11, r12)
            r6.add(r10)
            int r0 = r0 + 1
            goto L_0x005b
        L_0x006e:
            int[] r0 = com.fossil.k.f1847a
            int r2 = r7.ordinal()
            r0 = r0[r2]
            switch(r0) {
                case 1: goto L_0x007a;
                case 2: goto L_0x007a;
                case 3: goto L_0x0081;
                case 4: goto L_0x0081;
                case 5: goto L_0x007d;
                case 6: goto L_0x007d;
                default: goto L_0x0079;
            }
        L_0x0079:
            goto L_0x002a
        L_0x007a:
            r0 = 2
            r2 = r0
            goto L_0x002b
        L_0x007d:
            r0 = 16
            r2 = r0
            goto L_0x002b
        L_0x0081:
            r0 = 4
            r2 = r0
            goto L_0x002b
        L_0x0084:
            r8.addAll(r6)
            java.lang.String[] r0 = new java.lang.String[r1]
            java.lang.Object[] r0 = r8.toArray(r0)
            if (r0 == 0) goto L_0x0096
            r3.put(r7, r0)
        L_0x0092:
            int r0 = r5 + r4
            goto L_0x0008
        L_0x0096:
            com.fossil.il7 r0 = new com.fossil.il7
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Array<T>"
            r0.<init>(r1)
            throw r0
            switch-data {1->0x007a, 2->0x007a, 3->0x0081, 4->0x0081, 5->0x007d, 6->0x007d, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tk1.r(byte[]):java.util.HashMap");
    }

    @DexIgnore
    public final void s() {
        b[] bVarArr;
        synchronized (f) {
            Set<b> keySet = f.keySet();
            pq7.b(keySet, "activeLeScanCallbacks.keys");
            Object[] array = keySet.toArray(new b[0]);
            if (array != null) {
                bVarArr = (b[]) array;
                tl7 tl7 = tl7.f3441a;
            } else {
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        for (b bVar : bVarArr) {
            k.g(bVar);
        }
    }

    @DexIgnore
    public final boolean t(byte[] bArr) {
        HashMap<d, String[]> r = r(bArr);
        String[] strArr = r.get(d.g);
        if (strArr != null && em7.B(strArr, hd0.y.v())) {
            return true;
        }
        String[] strArr2 = r.get(d.h);
        return strArr2 != null && em7.B(strArr2, hd0.y.v());
    }

    @DexIgnore
    public final boolean u(byte[] bArr) {
        int i2 = 0;
        while (i2 < bArr.length) {
            int i3 = i2 + 1;
            short p = hy1.p((byte) (bArr[i2] & ((byte) 255)));
            if (p == 0 || i3 + 1 > bArr.length) {
                return false;
            }
            if (d.m.a(bArr[i3]) == d.k && p >= 3) {
                ByteBuffer order = ByteBuffer.wrap(dm7.k(dm7.k(bArr, i3, i3 + p), 1, 3)).order(ByteOrder.LITTLE_ENDIAN);
                pq7.b(order, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
                if (order.getShort() == 224) {
                    return true;
                }
            }
            i2 = p + i3;
        }
        return false;
    }

    @DexIgnore
    public final String v(byte[] bArr) {
        byte[] bArr2;
        byte b2;
        if (bArr == null) {
            return new String();
        }
        int i2 = 0;
        while (true) {
            if (i2 >= bArr.length) {
                break;
            }
            int i3 = i2 + 1;
            short p = hy1.p(bArr[i2]);
            if (p != 0 && (b2 = bArr[i3]) != ((byte) 0)) {
                if (b2 == -1 && p >= 13) {
                    int i4 = i3 + 1 + 2;
                    bArr2 = Arrays.copyOfRange(bArr, i4, i4 + 10);
                    pq7.b(bArr2, "Arrays.copyOfRange(adver\u2026RER_SERIAL_NUMBER_LENGTH)");
                    break;
                }
                i2 = p + i3;
            } else {
                break;
            }
        }
        bArr2 = new byte[0];
        return new String(bArr2, et7.f986a);
    }

    @DexIgnore
    public final c w() {
        return b == null ? c.DISABLED : c.d.a(b.getState());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0077  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.yk1 x(java.lang.String r16, java.lang.String r17) throws java.lang.IllegalArgumentException {
        /*
        // Method dump skipped, instructions count: 441
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tk1.x(java.lang.String, java.lang.String):com.fossil.yk1");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void y(com.fossil.xk1 r21, com.fossil.tk1.b r22) throws java.lang.IllegalStateException {
        /*
        // Method dump skipped, instructions count: 421
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tk1.y(com.fossil.xk1, com.fossil.tk1$b):void");
    }

    @DexIgnore
    public final void z(b bVar) {
        d90.i.d(new a90(ey1.a(e.STOP_SCAN), v80.h, "", "", "", true, null, null, null, g80.k(new JSONObject(), jd0.F1, hy1.k(bVar.hashCode(), null, 1, null)), 448));
        g(bVar);
    }
}
