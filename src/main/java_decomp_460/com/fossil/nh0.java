package com.fossil;

import android.annotation.SuppressLint;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.SearchView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"RestrictedAPI"})
public class nh0 extends qp0 implements View.OnClickListener {
    @DexIgnore
    public int A; // = -1;
    @DexIgnore
    public int B; // = -1;
    @DexIgnore
    public int C; // = -1;
    @DexIgnore
    public int D; // = -1;
    @DexIgnore
    public int E; // = -1;
    @DexIgnore
    public /* final */ SearchView m;
    @DexIgnore
    public /* final */ SearchableInfo s;
    @DexIgnore
    public /* final */ Context t;
    @DexIgnore
    public /* final */ WeakHashMap<String, Drawable.ConstantState> u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public boolean w; // = false;
    @DexIgnore
    public int x; // = 1;
    @DexIgnore
    public ColorStateList y;
    @DexIgnore
    public int z; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ TextView f2520a;
        @DexIgnore
        public /* final */ TextView b;
        @DexIgnore
        public /* final */ ImageView c;
        @DexIgnore
        public /* final */ ImageView d;
        @DexIgnore
        public /* final */ ImageView e;

        @DexIgnore
        public a(View view) {
            this.f2520a = (TextView) view.findViewById(16908308);
            this.b = (TextView) view.findViewById(16908309);
            this.c = (ImageView) view.findViewById(16908295);
            this.d = (ImageView) view.findViewById(16908296);
            this.e = (ImageView) view.findViewById(qe0.edit_query);
        }
    }

    @DexIgnore
    public nh0(Context context, SearchView searchView, SearchableInfo searchableInfo, WeakHashMap<String, Drawable.ConstantState> weakHashMap) {
        super(context, searchView.getSuggestionRowLayout(), null, true);
        this.m = searchView;
        this.s = searchableInfo;
        this.v = searchView.getSuggestionCommitIconResId();
        this.t = context;
        this.u = weakHashMap;
    }

    @DexIgnore
    public static String o(Cursor cursor, String str) {
        return w(cursor, cursor.getColumnIndex(str));
    }

    @DexIgnore
    public static String w(Cursor cursor, int i) {
        if (i == -1) {
            return null;
        }
        try {
            return cursor.getString(i);
        } catch (Exception e) {
            Log.e("SuggestionsAdapter", "unexpected error retrieving valid column from cursor, did the remote process die?", e);
            return null;
        }
    }

    @DexIgnore
    public final void A(String str, Drawable drawable) {
        if (drawable != null) {
            this.u.put(str, drawable.getConstantState());
        }
    }

    @DexIgnore
    public final void B(Cursor cursor) {
        Bundle extras = cursor != null ? cursor.getExtras() : null;
        if (extras == null || extras.getBoolean("in_progress")) {
        }
    }

    @DexIgnore
    @Override // com.fossil.op0, com.fossil.pp0.a
    public void a(Cursor cursor) {
        if (this.w) {
            Log.w("SuggestionsAdapter", "Tried to change cursor after adapter was closed.");
            if (cursor != null) {
                cursor.close();
                return;
            }
            return;
        }
        try {
            super.a(cursor);
            if (cursor != null) {
                this.z = cursor.getColumnIndex("suggest_text_1");
                this.A = cursor.getColumnIndex("suggest_text_2");
                this.B = cursor.getColumnIndex("suggest_text_2_url");
                this.C = cursor.getColumnIndex("suggest_icon_1");
                this.D = cursor.getColumnIndex("suggest_icon_2");
                this.E = cursor.getColumnIndex("suggest_flags");
            }
        } catch (Exception e) {
            Log.e("SuggestionsAdapter", "error changing cursor and caching columns", e);
        }
    }

    @DexIgnore
    @Override // com.fossil.op0
    public void c(View view, Context context, Cursor cursor) {
        a aVar = (a) view.getTag();
        int i = this.E;
        int i2 = i != -1 ? cursor.getInt(i) : 0;
        if (aVar.f2520a != null) {
            z(aVar.f2520a, w(cursor, this.z));
        }
        if (aVar.b != null) {
            String w2 = w(cursor, this.B);
            CharSequence l = w2 != null ? l(w2) : w(cursor, this.A);
            if (TextUtils.isEmpty(l)) {
                TextView textView = aVar.f2520a;
                if (textView != null) {
                    textView.setSingleLine(false);
                    aVar.f2520a.setMaxLines(2);
                }
            } else {
                TextView textView2 = aVar.f2520a;
                if (textView2 != null) {
                    textView2.setSingleLine(true);
                    aVar.f2520a.setMaxLines(1);
                }
            }
            z(aVar.b, l);
        }
        ImageView imageView = aVar.c;
        if (imageView != null) {
            y(imageView, t(cursor), 4);
        }
        ImageView imageView2 = aVar.d;
        if (imageView2 != null) {
            y(imageView2, u(cursor), 8);
        }
        int i3 = this.x;
        if (i3 == 2 || (i3 == 1 && (i2 & 1) != 0)) {
            aVar.e.setVisibility(0);
            aVar.e.setTag(aVar.f2520a.getText());
            aVar.e.setOnClickListener(this);
            return;
        }
        aVar.e.setVisibility(8);
    }

    @DexIgnore
    @Override // com.fossil.op0, com.fossil.pp0.a
    public CharSequence d(Cursor cursor) {
        String o;
        String o2;
        if (cursor == null) {
            return null;
        }
        String o3 = o(cursor, "suggest_intent_query");
        if (o3 != null) {
            return o3;
        }
        if (this.s.shouldRewriteQueryFromData() && (o2 = o(cursor, "suggest_intent_data")) != null) {
            return o2;
        }
        if (!this.s.shouldRewriteQueryFromText() || (o = o(cursor, "suggest_text_1")) == null) {
            return null;
        }
        return o;
    }

    @DexIgnore
    @Override // com.fossil.pp0.a
    public Cursor e(CharSequence charSequence) {
        String charSequence2 = charSequence == null ? "" : charSequence.toString();
        if (this.m.getVisibility() == 0 && this.m.getWindowVisibility() == 0) {
            try {
                Cursor v2 = v(this.s, charSequence2, 50);
                if (v2 != null) {
                    v2.getCount();
                    return v2;
                }
            } catch (RuntimeException e) {
                Log.w("SuggestionsAdapter", "Search suggestions query threw an exception.", e);
            }
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.op0
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        try {
            return super.getDropDownView(i, view, viewGroup);
        } catch (RuntimeException e) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", e);
            View g = g(this.e, this.d, viewGroup);
            if (g != null) {
                ((a) g.getTag()).f2520a.setText(e.toString());
            }
            return g;
        }
    }

    @DexIgnore
    @Override // com.fossil.op0
    public View getView(int i, View view, ViewGroup viewGroup) {
        try {
            return super.getView(i, view, viewGroup);
        } catch (RuntimeException e) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", e);
            View h = h(this.e, this.d, viewGroup);
            if (h != null) {
                ((a) h.getTag()).f2520a.setText(e.toString());
            }
            return h;
        }
    }

    @DexIgnore
    @Override // com.fossil.op0, com.fossil.qp0
    public View h(Context context, Cursor cursor, ViewGroup viewGroup) {
        View h = super.h(context, cursor, viewGroup);
        h.setTag(new a(h));
        ((ImageView) h.findViewById(qe0.edit_query)).setImageResource(this.v);
        return h;
    }

    @DexIgnore
    public boolean hasStableIds() {
        return false;
    }

    @DexIgnore
    public final Drawable k(String str) {
        Drawable.ConstantState constantState = this.u.get(str);
        if (constantState == null) {
            return null;
        }
        return constantState.newDrawable();
    }

    @DexIgnore
    public final CharSequence l(CharSequence charSequence) {
        if (this.y == null) {
            TypedValue typedValue = new TypedValue();
            this.e.getTheme().resolveAttribute(le0.textColorSearchUrl, typedValue, true);
            this.y = this.e.getResources().getColorStateList(typedValue.resourceId);
        }
        SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new TextAppearanceSpan(null, 0, 0, this.y, null), 0, charSequence.length(), 33);
        return spannableString;
    }

    @DexIgnore
    public final Drawable m(ComponentName componentName) {
        PackageManager packageManager = this.e.getPackageManager();
        try {
            ActivityInfo activityInfo = packageManager.getActivityInfo(componentName, 128);
            int iconResource = activityInfo.getIconResource();
            if (iconResource == 0) {
                return null;
            }
            Drawable drawable = packageManager.getDrawable(componentName.getPackageName(), iconResource, activityInfo.applicationInfo);
            if (drawable != null) {
                return drawable;
            }
            Log.w("SuggestionsAdapter", "Invalid icon resource " + iconResource + " for " + componentName.flattenToShortString());
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            Log.w("SuggestionsAdapter", e.toString());
            return null;
        }
    }

    @DexIgnore
    public final Drawable n(ComponentName componentName) {
        Drawable.ConstantState constantState = null;
        String flattenToShortString = componentName.flattenToShortString();
        if (this.u.containsKey(flattenToShortString)) {
            Drawable.ConstantState constantState2 = this.u.get(flattenToShortString);
            if (constantState2 == null) {
                return null;
            }
            return constantState2.newDrawable(this.t.getResources());
        }
        Drawable m2 = m(componentName);
        if (m2 != null) {
            constantState = m2.getConstantState();
        }
        this.u.put(flattenToShortString, constantState);
        return m2;
    }

    @DexIgnore
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        B(b());
    }

    @DexIgnore
    public void notifyDataSetInvalidated() {
        super.notifyDataSetInvalidated();
        B(b());
    }

    @DexIgnore
    public void onClick(View view) {
        Object tag = view.getTag();
        if (tag instanceof CharSequence) {
            this.m.onQueryRefine((CharSequence) tag);
        }
    }

    @DexIgnore
    public final Drawable p() {
        Drawable n = n(this.s.getSearchActivity());
        return n != null ? n : this.e.getPackageManager().getDefaultActivityIcon();
    }

    @DexIgnore
    public final Drawable q(Uri uri) {
        try {
            if ("android.resource".equals(uri.getScheme())) {
                try {
                    return r(uri);
                } catch (Resources.NotFoundException e) {
                    throw new FileNotFoundException("Resource does not exist: " + uri);
                }
            } else {
                InputStream openInputStream = this.t.getContentResolver().openInputStream(uri);
                if (openInputStream != null) {
                    try {
                        Drawable createFromStream = Drawable.createFromStream(openInputStream, null);
                        try {
                            return createFromStream;
                        } catch (IOException e2) {
                            Log.e("SuggestionsAdapter", "Error closing icon stream for " + uri, e2);
                            return createFromStream;
                        }
                    } finally {
                        try {
                            openInputStream.close();
                        } catch (IOException e3) {
                            Log.e("SuggestionsAdapter", "Error closing icon stream for " + uri, e3);
                        }
                    }
                } else {
                    throw new FileNotFoundException("Failed to open " + uri);
                }
            }
        } catch (FileNotFoundException e4) {
            Log.w("SuggestionsAdapter", "Icon not found: " + uri + ", " + e4.getMessage());
            return null;
        }
    }

    @DexIgnore
    public Drawable r(Uri uri) throws FileNotFoundException {
        int identifier;
        String authority = uri.getAuthority();
        if (!TextUtils.isEmpty(authority)) {
            try {
                Resources resourcesForApplication = this.e.getPackageManager().getResourcesForApplication(authority);
                List<String> pathSegments = uri.getPathSegments();
                if (pathSegments != null) {
                    int size = pathSegments.size();
                    if (size == 1) {
                        try {
                            identifier = Integer.parseInt(pathSegments.get(0));
                        } catch (NumberFormatException e) {
                            throw new FileNotFoundException("Single path segment is not a resource ID: " + uri);
                        }
                    } else if (size == 2) {
                        identifier = resourcesForApplication.getIdentifier(pathSegments.get(1), pathSegments.get(0), authority);
                    } else {
                        throw new FileNotFoundException("More than two path segments: " + uri);
                    }
                    if (identifier != 0) {
                        return resourcesForApplication.getDrawable(identifier);
                    }
                    throw new FileNotFoundException("No resource found for: " + uri);
                }
                throw new FileNotFoundException("No path: " + uri);
            } catch (PackageManager.NameNotFoundException e2) {
                throw new FileNotFoundException("No package found for authority: " + uri);
            }
        } else {
            throw new FileNotFoundException("No authority: " + uri);
        }
    }

    @DexIgnore
    public final Drawable s(String str) {
        if (str == null || str.isEmpty() || "0".equals(str)) {
            return null;
        }
        try {
            int parseInt = Integer.parseInt(str);
            String str2 = "android.resource://" + this.t.getPackageName() + "/" + parseInt;
            Drawable k = k(str2);
            if (k != null) {
                return k;
            }
            Drawable f = gl0.f(this.t, parseInt);
            A(str2, f);
            return f;
        } catch (NumberFormatException e) {
            Drawable k2 = k(str);
            if (k2 != null) {
                return k2;
            }
            Drawable q = q(Uri.parse(str));
            A(str, q);
            return q;
        } catch (Resources.NotFoundException e2) {
            Log.w("SuggestionsAdapter", "Icon resource not found: " + str);
            return null;
        }
    }

    @DexIgnore
    public final Drawable t(Cursor cursor) {
        int i = this.C;
        if (i == -1) {
            return null;
        }
        Drawable s2 = s(cursor.getString(i));
        return s2 == null ? p() : s2;
    }

    @DexIgnore
    public final Drawable u(Cursor cursor) {
        int i = this.D;
        if (i == -1) {
            return null;
        }
        return s(cursor.getString(i));
    }

    @DexIgnore
    public Cursor v(SearchableInfo searchableInfo, String str, int i) {
        String suggestAuthority;
        String[] strArr;
        if (searchableInfo == null || (suggestAuthority = searchableInfo.getSuggestAuthority()) == null) {
            return null;
        }
        Uri.Builder fragment = new Uri.Builder().scheme("content").authority(suggestAuthority).query("").fragment("");
        String suggestPath = searchableInfo.getSuggestPath();
        if (suggestPath != null) {
            fragment.appendEncodedPath(suggestPath);
        }
        fragment.appendPath("search_suggest_query");
        String suggestSelection = searchableInfo.getSuggestSelection();
        if (suggestSelection != null) {
            strArr = new String[]{str};
        } else {
            fragment.appendPath(str);
            strArr = null;
        }
        if (i > 0) {
            fragment.appendQueryParameter("limit", String.valueOf(i));
        }
        return this.e.getContentResolver().query(fragment.build(), null, suggestSelection, strArr, null);
    }

    @DexIgnore
    public void x(int i) {
        this.x = i;
    }

    @DexIgnore
    public final void y(ImageView imageView, Drawable drawable, int i) {
        imageView.setImageDrawable(drawable);
        if (drawable == null) {
            imageView.setVisibility(i);
            return;
        }
        imageView.setVisibility(0);
        drawable.setVisible(false, false);
        drawable.setVisible(true, false);
    }

    @DexIgnore
    public final void z(TextView textView, CharSequence charSequence) {
        textView.setText(charSequence);
        if (TextUtils.isEmpty(charSequence)) {
            textView.setVisibility(8);
        } else {
            textView.setVisibility(0);
        }
    }
}
