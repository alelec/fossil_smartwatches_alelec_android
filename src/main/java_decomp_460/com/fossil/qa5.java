package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qa5 extends pa5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d X; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Y;
    @DexIgnore
    public long W;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        Y = sparseIntArray;
        sparseIntArray.put(2131362686, 1);
        Y.put(2131362968, 2);
        Y.put(2131363410, 3);
        Y.put(2131363156, 4);
        Y.put(2131362638, 5);
        Y.put(2131362233, 6);
        Y.put(2131362679, 7);
        Y.put(2131362639, 8);
        Y.put(2131362235, 9);
        Y.put(2131362680, 10);
        Y.put(2131362640, 11);
        Y.put(2131362237, 12);
        Y.put(2131362681, 13);
        Y.put(2131362635, 14);
        Y.put(2131362229, 15);
        Y.put(2131362678, 16);
        Y.put(2131362439, 17);
        Y.put(2131362277, 18);
        Y.put(2131362272, 19);
        Y.put(2131362280, 20);
        Y.put(2131362006, 21);
        Y.put(2131362500, 22);
        Y.put(2131362009, 23);
        Y.put(2131362550, 24);
        Y.put(2131362008, 25);
        Y.put(2131362542, 26);
        Y.put(2131362000, 27);
        Y.put(2131362438, 28);
        Y.put(2131361999, 29);
        Y.put(2131362437, 30);
        Y.put(2131362269, 31);
    }
    */

    @DexIgnore
    public qa5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 32, X, Y));
    }

    @DexIgnore
    public qa5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleCheckBox) objArr[29], (FlexibleCheckBox) objArr[27], (FlexibleCheckBox) objArr[21], (FlexibleCheckBox) objArr[25], (FlexibleCheckBox) objArr[23], (FlexibleTextInputEditText) objArr[15], (FlexibleTextInputEditText) objArr[6], (FlexibleTextInputEditText) objArr[9], (FlexibleTextInputEditText) objArr[12], (FlexibleButton) objArr[31], (FlexibleButton) objArr[19], (FlexibleButton) objArr[18], (FlexibleButton) objArr[20], (FlexibleTextView) objArr[30], (FlexibleTextView) objArr[28], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[22], (FlexibleTextView) objArr[26], (FlexibleTextView) objArr[24], (FlexibleTextInputLayout) objArr[14], (FlexibleTextInputLayout) objArr[5], (FlexibleTextInputLayout) objArr[8], (FlexibleTextInputLayout) objArr[11], (RTLImageView) objArr[16], (RTLImageView) objArr[7], (RTLImageView) objArr[10], (RTLImageView) objArr[13], (ImageView) objArr[1], (DashBar) objArr[2], (ConstraintLayout) objArr[0], (ScrollView) objArr[4], (FlexibleTextView) objArr[3]);
        this.W = -1;
        this.T.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.W = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.W != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.W = 1;
        }
        w();
    }
}
