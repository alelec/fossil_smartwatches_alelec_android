package com.fossil;

import com.fossil.zu0;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bx5 extends zu0.d<DailyHeartRateSummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        pq7.c(dailyHeartRateSummary, "oldItem");
        pq7.c(dailyHeartRateSummary2, "newItem");
        return pq7.a(dailyHeartRateSummary, dailyHeartRateSummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        pq7.c(dailyHeartRateSummary, "oldItem");
        pq7.c(dailyHeartRateSummary2, "newItem");
        return lk5.m0(dailyHeartRateSummary.getDate(), dailyHeartRateSummary2.getDate());
    }
}
