package com.fossil;

import android.animation.ObjectAnimator;
import com.portfolio.platform.ui.view.DashBar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x37 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ a f4036a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final void a(DashBar dashBar, boolean z, long j) {
            pq7.c(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 90 : 66;
            h(dashBar, new x57(i, i2, z ? 80 : i2, i2), j);
        }

        @DexIgnore
        public final void b(DashBar dashBar, boolean z, long j) {
            pq7.c(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 60 : 33;
            h(dashBar, new x57(i, i2, z ? 50 : i2, i2), j);
        }

        @DexIgnore
        public final void c(DashBar dashBar, boolean z, long j) {
            pq7.c(dashBar, "dashBar");
            h(dashBar, new x57(z ? 4 : 3, 100, z ? 90 : 100, 100), j);
        }

        @DexIgnore
        public final void d(DashBar dashBar, boolean z, long j) {
            pq7.c(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 50 : 33;
            h(dashBar, new x57(i, i2, z ? 40 : i2, i2), j);
        }

        @DexIgnore
        public final void e(DashBar dashBar, long j) {
            pq7.c(dashBar, "dashBar");
            h(dashBar, new x57(4, 30, 20, 30), j);
        }

        @DexIgnore
        public final void f(DashBar dashBar, long j) {
            pq7.c(dashBar, "dashBar");
            h(dashBar, new x57(4, 10, 10, 20), j);
        }

        @DexIgnore
        public final void g(DashBar dashBar, boolean z, long j) {
            pq7.c(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 40 : 33;
            h(dashBar, new x57(i, i2, z ? 30 : 0, i2), j);
        }

        @DexIgnore
        public final void h(DashBar dashBar, x57 x57, long j) {
            dashBar.setLength(x57.c());
            dashBar.setProgress(x57.d());
            if (x57.e()) {
                ObjectAnimator ofInt = ObjectAnimator.ofInt(dashBar, "progress", x57.b(), x57.a());
                pq7.b(ofInt, "progressAnimator");
                ofInt.setDuration(j);
                ofInt.start();
            }
        }

        @DexIgnore
        public final void i(DashBar dashBar, boolean z, long j) {
            pq7.c(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 80 : 66;
            h(dashBar, new x57(i, i2, z ? 70 : i2, i2), j);
        }
    }
}
