package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vq7 implements hq7 {
    @DexIgnore
    public /* final */ Class<?> b;

    @DexIgnore
    public vq7(Class<?> cls, String str) {
        pq7.c(cls, "jClass");
        pq7.c(str, "moduleName");
        this.b = cls;
    }

    @DexIgnore
    @Override // com.fossil.hq7
    public Class<?> b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof vq7) && pq7.a(b(), ((vq7) obj).b());
    }

    @DexIgnore
    public int hashCode() {
        return b().hashCode();
    }

    @DexIgnore
    public String toString() {
        return b().toString() + " (Kotlin reflection is not available)";
    }
}
