package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.net.Uri;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vv2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static volatile tw2<Boolean> f3839a; // = tw2.zzc();
    @DexIgnore
    public static /* final */ Object b; // = new Object();

    @DexIgnore
    public static boolean a(Context context) {
        try {
            return (context.getPackageManager().getApplicationInfo("com.google.android.gms", 0).flags & 129) != 0;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @DexIgnore
    public static boolean b(Context context, Uri uri) {
        ProviderInfo resolveContentProvider;
        boolean z = false;
        String authority = uri.getAuthority();
        if (!"com.google.android.gms.phenotype".equals(authority)) {
            StringBuilder sb = new StringBuilder(String.valueOf(authority).length() + 91);
            sb.append(authority);
            sb.append(" is an unsupported authority. Only com.google.android.gms.phenotype authority is supported.");
            Log.e("PhenotypeClientHelper", sb.toString());
            return false;
        } else if (f3839a.zza()) {
            return f3839a.zzb().booleanValue();
        } else {
            synchronized (b) {
                if (f3839a.zza()) {
                    return f3839a.zzb().booleanValue();
                }
                if (("com.google.android.gms".equals(context.getPackageName()) || ((resolveContentProvider = context.getPackageManager().resolveContentProvider("com.google.android.gms.phenotype", 0)) != null && "com.google.android.gms".equals(resolveContentProvider.packageName))) && a(context)) {
                    z = true;
                }
                f3839a = tw2.zza(Boolean.valueOf(z));
                return f3839a.zzb().booleanValue();
            }
        }
    }
}
