package com.fossil;

import android.content.Context;
import com.facebook.AccessToken;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nc4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2501a;

    @DexIgnore
    public nc4(Context context) {
        this.f2501a = context;
    }

    @DexIgnore
    public final File a() {
        return new File(new ub4(this.f2501a).b(), "com.crashlytics.settings.json");
    }

    @DexIgnore
    public JSONObject b() {
        FileInputStream fileInputStream;
        Exception e;
        JSONObject jSONObject;
        FileInputStream fileInputStream2 = null;
        x74.f().b("Reading cached settings...");
        try {
            File a2 = a();
            if (a2.exists()) {
                fileInputStream = new FileInputStream(a2);
                try {
                    jSONObject = new JSONObject(r84.I(fileInputStream));
                } catch (Exception e2) {
                    e = e2;
                    try {
                        x74.f().e("Failed to fetch cached settings", e);
                        r84.e(fileInputStream, "Error while closing settings cache file.");
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        fileInputStream2 = fileInputStream;
                        r84.e(fileInputStream2, "Error while closing settings cache file.");
                        throw th;
                    }
                }
            } else {
                x74.f().b("No cached settings found.");
                jSONObject = null;
                fileInputStream = null;
            }
            r84.e(fileInputStream, "Error while closing settings cache file.");
            return jSONObject;
        } catch (Exception e3) {
            e = e3;
            fileInputStream = null;
            x74.f().e("Failed to fetch cached settings", e);
            r84.e(fileInputStream, "Error while closing settings cache file.");
            return null;
        } catch (Throwable th2) {
            th = th2;
            r84.e(fileInputStream2, "Error while closing settings cache file.");
            throw th;
        }
    }

    @DexIgnore
    public void c(long j, JSONObject jSONObject) {
        FileWriter fileWriter;
        Throwable th;
        Exception e;
        x74.f().b("Writing settings to cache file...");
        if (jSONObject != null) {
            try {
                jSONObject.put(AccessToken.EXPIRES_AT_KEY, j);
                fileWriter = new FileWriter(a());
                try {
                    fileWriter.write(jSONObject.toString());
                    fileWriter.flush();
                    r84.e(fileWriter, "Failed to close settings writer.");
                } catch (Exception e2) {
                    e = e2;
                    try {
                        x74.f().e("Failed to cache settings", e);
                        r84.e(fileWriter, "Failed to close settings writer.");
                    } catch (Throwable th2) {
                        th = th2;
                        r84.e(fileWriter, "Failed to close settings writer.");
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    r84.e(fileWriter, "Failed to close settings writer.");
                    throw th;
                }
            } catch (Exception e3) {
                e = e3;
                fileWriter = null;
                x74.f().e("Failed to cache settings", e);
                r84.e(fileWriter, "Failed to close settings writer.");
            } catch (Throwable th4) {
                th = th4;
                fileWriter = null;
                r84.e(fileWriter, "Failed to close settings writer.");
                throw th;
            }
        }
    }
}
