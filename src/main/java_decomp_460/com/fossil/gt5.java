package com.fossil;

import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class gt5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f1358a;

    /*
    static {
        int[] iArr = new int[ServiceActionResult.values().length];
        f1358a = iArr;
        iArr[ServiceActionResult.GET_LATEST_FW.ordinal()] = 1;
        f1358a[ServiceActionResult.LATEST_FW.ordinal()] = 2;
        f1358a[ServiceActionResult.UPDATE_FW_SUCCESS.ordinal()] = 3;
        f1358a[ServiceActionResult.UPDATE_FW_FAILED.ordinal()] = 4;
        f1358a[ServiceActionResult.START_TIMER.ordinal()] = 5;
        f1358a[ServiceActionResult.AUTHORIZE_DEVICE_SUCCESS.ordinal()] = 6;
        f1358a[ServiceActionResult.ASK_FOR_LINK_SERVER.ordinal()] = 7;
        f1358a[ServiceActionResult.ASK_FOR_LABEL_FILE.ordinal()] = 8;
        f1358a[ServiceActionResult.SUCCEEDED.ordinal()] = 9;
        f1358a[ServiceActionResult.FAILED.ordinal()] = 10;
    }
    */
}
