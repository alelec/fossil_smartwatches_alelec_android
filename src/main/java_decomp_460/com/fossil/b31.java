package com.fossil;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b31 implements a31 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f388a;
    @DexIgnore
    public /* final */ jw0<z21> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jw0<z21> {
        @DexIgnore
        public a(b31 b31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, z21 z21) {
            String str = z21.f4407a;
            if (str == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, str);
            }
            String str2 = z21.b;
            if (str2 == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, str2);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR IGNORE INTO `Dependency` (`work_spec_id`,`prerequisite_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public b31(qw0 qw0) {
        this.f388a = qw0;
        this.b = new a(this, qw0);
    }

    @DexIgnore
    @Override // com.fossil.a31
    public void a(z21 z21) {
        this.f388a.assertNotSuspendingTransaction();
        this.f388a.beginTransaction();
        try {
            this.b.insert((jw0<z21>) z21);
            this.f388a.setTransactionSuccessful();
        } finally {
            this.f388a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.a31
    public List<String> b(String str) {
        tw0 f = tw0.f("SELECT work_spec_id FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.f388a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f388a, f, false, null);
        try {
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(b2.getString(0));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.a31
    public boolean c(String str) {
        boolean z = true;
        tw0 f = tw0.f("SELECT COUNT(*)=0 FROM dependency WHERE work_spec_id=? AND prerequisite_id IN (SELECT id FROM workspec WHERE state!=2)", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.f388a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f388a, f, false, null);
        try {
            if (!b2.moveToFirst()) {
                z = false;
            } else if (b2.getInt(0) == 0) {
                z = false;
            }
            return z;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.a31
    public boolean d(String str) {
        boolean z = true;
        tw0 f = tw0.f("SELECT COUNT(*)>0 FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.f388a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f388a, f, false, null);
        try {
            if (!b2.moveToFirst()) {
                z = false;
            } else if (b2.getInt(0) == 0) {
                z = false;
            }
            return z;
        } finally {
            b2.close();
            f.m();
        }
    }
}
