package com.fossil;

import android.os.Build;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o01 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f2600a;
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ i11 c;
    @DexIgnore
    public /* final */ w01 d;
    @DexIgnore
    public /* final */ d11 e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Executor f2601a;
        @DexIgnore
        public i11 b;
        @DexIgnore
        public w01 c;
        @DexIgnore
        public Executor d;
        @DexIgnore
        public d11 e;
        @DexIgnore
        public int f; // = 4;
        @DexIgnore
        public int g; // = 0;
        @DexIgnore
        public int h; // = Integer.MAX_VALUE;
        @DexIgnore
        public int i; // = 20;

        @DexIgnore
        public o01 a() {
            return new o01(this);
        }

        @DexIgnore
        public a b(i11 i11) {
            this.b = i11;
            return this;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        o01 a();
    }

    @DexIgnore
    public o01(a aVar) {
        Executor executor = aVar.f2601a;
        if (executor == null) {
            this.f2600a = a();
        } else {
            this.f2600a = executor;
        }
        Executor executor2 = aVar.d;
        if (executor2 == null) {
            this.b = a();
        } else {
            this.b = executor2;
        }
        i11 i11 = aVar.b;
        if (i11 == null) {
            this.c = i11.c();
        } else {
            this.c = i11;
        }
        w01 w01 = aVar.c;
        if (w01 == null) {
            this.d = w01.c();
        } else {
            this.d = w01;
        }
        d11 d11 = aVar.e;
        if (d11 == null) {
            this.e = new j11();
        } else {
            this.e = d11;
        }
        this.f = aVar.f;
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i;
    }

    @DexIgnore
    public final Executor a() {
        return Executors.newFixedThreadPool(Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 4)));
    }

    @DexIgnore
    public Executor b() {
        return this.f2600a;
    }

    @DexIgnore
    public w01 c() {
        return this.d;
    }

    @DexIgnore
    public int d() {
        return this.h;
    }

    @DexIgnore
    public int e() {
        return Build.VERSION.SDK_INT == 23 ? this.i / 2 : this.i;
    }

    @DexIgnore
    public int f() {
        return this.g;
    }

    @DexIgnore
    public int g() {
        return this.f;
    }

    @DexIgnore
    public d11 h() {
        return this.e;
    }

    @DexIgnore
    public Executor i() {
        return this.b;
    }

    @DexIgnore
    public i11 j() {
        return this.c;
    }
}
