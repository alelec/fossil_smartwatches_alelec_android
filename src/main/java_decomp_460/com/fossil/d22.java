package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class d22 implements Runnable {
    @DexIgnore
    public /* final */ f22 b;

    @DexIgnore
    public d22(f22 f22) {
        this.b = f22;
    }

    @DexIgnore
    public static Runnable a(f22 f22) {
        return new d22(f22);
    }

    @DexIgnore
    public void run() {
        f22 f22;
        f22.d.a(e22.b(this.b));
    }
}
