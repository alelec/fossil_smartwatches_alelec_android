package com.fossil;

import com.fossil.a18;
import com.fossil.u88;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h88<ResponseT, ReturnT> extends r88<ReturnT> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ p88 f1451a;
    @DexIgnore
    public /* final */ a18.a b;
    @DexIgnore
    public /* final */ e88<w18, ResponseT> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<ResponseT, ReturnT> extends h88<ResponseT, ReturnT> {
        @DexIgnore
        public /* final */ b88<ResponseT, ReturnT> d;

        @DexIgnore
        public a(p88 p88, a18.a aVar, e88<w18, ResponseT> e88, b88<ResponseT, ReturnT> b88) {
            super(p88, aVar, e88);
            this.d = b88;
        }

        @DexIgnore
        @Override // com.fossil.h88
        public ReturnT c(Call<ResponseT> call, Object[] objArr) {
            return this.d.b(call);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<ResponseT> extends h88<ResponseT, Object> {
        @DexIgnore
        public /* final */ b88<ResponseT, Call<ResponseT>> d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public b(p88 p88, a18.a aVar, e88<w18, ResponseT> e88, b88<ResponseT, Call<ResponseT>> b88, boolean z) {
            super(p88, aVar, e88);
            this.d = b88;
            this.e = z;
        }

        @DexIgnore
        @Override // com.fossil.h88
        public Object c(Call<ResponseT> call, Object[] objArr) {
            Call<ResponseT> b = this.d.b(call);
            qn7 qn7 = (qn7) objArr[objArr.length - 1];
            try {
                return this.e ? j88.b(b, qn7) : j88.a(b, qn7);
            } catch (Exception e2) {
                return j88.d(e2, qn7);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<ResponseT> extends h88<ResponseT, Object> {
        @DexIgnore
        public /* final */ b88<ResponseT, Call<ResponseT>> d;

        @DexIgnore
        public c(p88 p88, a18.a aVar, e88<w18, ResponseT> e88, b88<ResponseT, Call<ResponseT>> b88) {
            super(p88, aVar, e88);
            this.d = b88;
        }

        @DexIgnore
        @Override // com.fossil.h88
        public Object c(Call<ResponseT> call, Object[] objArr) {
            Call<ResponseT> b = this.d.b(call);
            qn7 qn7 = (qn7) objArr[objArr.length - 1];
            try {
                return j88.c(b, qn7);
            } catch (Exception e) {
                return j88.d(e, qn7);
            }
        }
    }

    @DexIgnore
    public h88(p88 p88, a18.a aVar, e88<w18, ResponseT> e88) {
        this.f1451a = p88;
        this.b = aVar;
        this.c = e88;
    }

    @DexIgnore
    public static <ResponseT, ReturnT> b88<ResponseT, ReturnT> d(Retrofit retrofit3, Method method, Type type, Annotation[] annotationArr) {
        try {
            return (b88<ResponseT, ReturnT>) retrofit3.a(type, annotationArr);
        } catch (RuntimeException e) {
            throw u88.o(method, e, "Unable to create call adapter for %s", type);
        }
    }

    @DexIgnore
    public static <ResponseT> e88<w18, ResponseT> e(Retrofit retrofit3, Method method, Type type) {
        try {
            return retrofit3.i(type, method.getAnnotations());
        } catch (RuntimeException e) {
            throw u88.o(method, e, "Unable to create converter for %s", type);
        }
    }

    @DexIgnore
    public static <ResponseT, ReturnT> h88<ResponseT, ReturnT> f(Retrofit retrofit3, Method method, p88 p88) {
        Type genericReturnType;
        boolean z;
        Annotation[] annotationArr;
        boolean z2 = p88.k;
        Annotation[] annotations = method.getAnnotations();
        if (z2) {
            Type[] genericParameterTypes = method.getGenericParameterTypes();
            Type g = u88.g(0, (ParameterizedType) genericParameterTypes[genericParameterTypes.length - 1]);
            if (u88.i(g) != q88.class || !(g instanceof ParameterizedType)) {
                z = false;
            } else {
                g = u88.h(0, (ParameterizedType) g);
                z = true;
            }
            u88.b bVar = new u88.b(null, Call.class, g);
            annotationArr = t88.a(annotations);
            genericReturnType = bVar;
        } else {
            genericReturnType = method.getGenericReturnType();
            z = false;
            annotationArr = annotations;
        }
        b88 d = d(retrofit3, method, genericReturnType, annotationArr);
        Type a2 = d.a();
        if (a2 == Response.class) {
            throw u88.n(method, "'" + u88.i(a2).getName() + "' is not a valid response body type. Did you mean ResponseBody?", new Object[0]);
        } else if (a2 == q88.class) {
            throw u88.n(method, "Response must include generic type (e.g., Response<String>)", new Object[0]);
        } else if (!p88.c.equals("HEAD") || Void.class.equals(a2)) {
            e88 e = e(retrofit3, method, a2);
            a18.a aVar = retrofit3.b;
            return !z2 ? new a(p88, aVar, e, d) : z ? new c(p88, aVar, e, d) : new b(p88, aVar, e, d, false);
        } else {
            throw u88.n(method, "HEAD method must use Void as response type.", new Object[0]);
        }
    }

    @DexIgnore
    @Override // com.fossil.r88
    public final ReturnT a(Object[] objArr) {
        return c(new k88(this.f1451a, objArr, this.b, this.c), objArr);
    }

    @DexIgnore
    public abstract ReturnT c(Call<ResponseT> call, Object[] objArr);
}
