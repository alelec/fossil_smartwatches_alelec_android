package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x04<T> implements Iterator<T> {
    @DexIgnore
    public b b; // = b.NOT_READY;
    @DexIgnore
    public T c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f4028a;

        /*
        static {
            int[] iArr = new int[b.values().length];
            f4028a = iArr;
            try {
                iArr[b.READY.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f4028a[b.DONE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
        */
    }

    @DexIgnore
    public enum b {
        READY,
        NOT_READY,
        DONE,
        FAILED
    }

    @DexIgnore
    public abstract T a();

    @DexIgnore
    @CanIgnoreReturnValue
    public final T b() {
        this.b = b.DONE;
        return null;
    }

    @DexIgnore
    public final boolean c() {
        this.b = b.FAILED;
        this.c = a();
        if (this.b == b.DONE) {
            return false;
        }
        this.b = b.READY;
        return true;
    }

    @DexIgnore
    public final boolean hasNext() {
        i14.s(this.b != b.FAILED);
        int i = a.f4028a[this.b.ordinal()];
        if (i == 1) {
            return true;
        }
        if (i != 2) {
            return c();
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.Iterator
    public final T next() {
        if (hasNext()) {
            this.b = b.NOT_READY;
            T t = this.c;
            this.c = null;
            return t;
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
