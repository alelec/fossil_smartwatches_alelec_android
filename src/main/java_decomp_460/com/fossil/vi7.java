package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vi7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;
    @DexIgnore
    public /* final */ /* synthetic */ Throwable c;

    @DexIgnore
    public vi7(Context context, Throwable th) {
        this.b = context;
        this.c = th;
    }

    @DexIgnore
    public final void run() {
        try {
            if (fg7.M()) {
                new ch7(new mg7(this.b, ig7.a(this.b, false, null), 99, this.c, pg7.m)).b();
            }
        } catch (Throwable th) {
            th7 th7 = ig7.m;
            th7.d("reportSdkSelfException error: " + th);
        }
    }
}
