package com.fossil;

import android.text.SpannableStringBuilder;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bn0 {
    @DexIgnore
    public static /* final */ fn0 d; // = gn0.c;
    @DexIgnore
    public static /* final */ String e; // = Character.toString('\u200e');
    @DexIgnore
    public static /* final */ String f; // = Character.toString('\u200f');
    @DexIgnore
    public static /* final */ bn0 g; // = new bn0(false, 2, d);
    @DexIgnore
    public static /* final */ bn0 h; // = new bn0(true, 2, d);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ boolean f454a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ fn0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f455a;
        @DexIgnore
        public int b;
        @DexIgnore
        public fn0 c;

        @DexIgnore
        public a() {
            c(bn0.e(Locale.getDefault()));
        }

        @DexIgnore
        public static bn0 b(boolean z) {
            return z ? bn0.h : bn0.g;
        }

        @DexIgnore
        public bn0 a() {
            return (this.b == 2 && this.c == bn0.d) ? b(this.f455a) : new bn0(this.f455a, this.b, this.c);
        }

        @DexIgnore
        public final void c(boolean z) {
            this.f455a = z;
            this.c = bn0.d;
            this.b = 2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static /* final */ byte[] f; // = new byte[1792];

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ CharSequence f456a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public char e;

        /*
        static {
            for (int i = 0; i < 1792; i++) {
                f[i] = Character.getDirectionality(i);
            }
        }
        */

        @DexIgnore
        public b(CharSequence charSequence, boolean z) {
            this.f456a = charSequence;
            this.b = z;
            this.c = charSequence.length();
        }

        @DexIgnore
        public static byte c(char c2) {
            return c2 < '\u0700' ? f[c2] : Character.getDirectionality(c2);
        }

        @DexIgnore
        public byte a() {
            char charAt = this.f456a.charAt(this.d - 1);
            this.e = charAt;
            if (Character.isLowSurrogate(charAt)) {
                int codePointBefore = Character.codePointBefore(this.f456a, this.d);
                this.d -= Character.charCount(codePointBefore);
                return Character.getDirectionality(codePointBefore);
            }
            this.d--;
            byte c2 = c(this.e);
            if (!this.b) {
                return c2;
            }
            char c3 = this.e;
            return c3 == '>' ? h() : c3 == ';' ? f() : c2;
        }

        @DexIgnore
        public byte b() {
            char charAt = this.f456a.charAt(this.d);
            this.e = charAt;
            if (Character.isHighSurrogate(charAt)) {
                int codePointAt = Character.codePointAt(this.f456a, this.d);
                this.d += Character.charCount(codePointAt);
                return Character.getDirectionality(codePointAt);
            }
            this.d++;
            byte c2 = c(this.e);
            if (!this.b) {
                return c2;
            }
            char c3 = this.e;
            return c3 == '<' ? i() : c3 == '&' ? g() : c2;
        }

        @DexIgnore
        public int d() {
            this.d = 0;
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            while (this.d < this.c && i3 == 0) {
                byte b2 = b();
                if (b2 != 0) {
                    if (b2 == 1 || b2 == 2) {
                        if (i == 0) {
                            return 1;
                        }
                    } else if (b2 != 9) {
                        switch (b2) {
                            case 14:
                            case 15:
                                i++;
                                i2 = -1;
                                break;
                            case 16:
                            case 17:
                                i++;
                                i2 = 1;
                                break;
                            case 18:
                                i--;
                                i2 = 0;
                                break;
                        }
                    }
                } else if (i == 0) {
                    return -1;
                }
                i3 = i;
            }
            if (i3 == 0) {
                return 0;
            }
            if (i2 != 0) {
                return i2;
            }
            while (this.d > 0) {
                switch (a()) {
                    case 14:
                    case 15:
                        if (i3 == i) {
                            return -1;
                        }
                        break;
                    case 16:
                    case 17:
                        if (i3 == i) {
                            return 1;
                        }
                        break;
                    case 18:
                        i++;
                        continue;
                }
                i--;
            }
            return 0;
        }

        @DexIgnore
        public int e() {
            this.d = this.c;
            int i = 0;
            int i2 = 0;
            while (this.d > 0) {
                byte a2 = a();
                if (a2 != 0) {
                    if (a2 == 1 || a2 == 2) {
                        if (i2 == 0) {
                            return 1;
                        }
                        if (i != 0) {
                        }
                    } else if (a2 != 9) {
                        switch (a2) {
                            case 14:
                            case 15:
                                if (i == i2) {
                                    return -1;
                                }
                                i2--;
                                break;
                            case 16:
                            case 17:
                                if (i == i2) {
                                    return 1;
                                }
                                i2--;
                                break;
                            case 18:
                                i2++;
                                break;
                            default:
                                if (i != 0) {
                                    break;
                                }
                                break;
                        }
                    } else {
                        continue;
                    }
                } else if (i2 == 0) {
                    return -1;
                } else {
                    if (i != 0) {
                    }
                }
                i = i2;
            }
            return 0;
        }

        @DexIgnore
        public final byte f() {
            char charAt;
            int i = this.d;
            do {
                int i2 = this.d;
                if (i2 <= 0) {
                    break;
                }
                CharSequence charSequence = this.f456a;
                int i3 = i2 - 1;
                this.d = i3;
                charAt = charSequence.charAt(i3);
                this.e = (char) charAt;
                if (charAt == '&') {
                    return 12;
                }
            } while (charAt != ';');
            this.d = i;
            this.e = (char) 59;
            return 13;
        }

        @DexIgnore
        public final byte g() {
            char charAt;
            do {
                int i = this.d;
                if (i >= this.c) {
                    return 12;
                }
                CharSequence charSequence = this.f456a;
                this.d = i + 1;
                charAt = charSequence.charAt(i);
                this.e = (char) charAt;
            } while (charAt != ';');
            return 12;
        }

        @DexIgnore
        public final byte h() {
            char charAt;
            int i = this.d;
            while (true) {
                int i2 = this.d;
                if (i2 <= 0) {
                    break;
                }
                CharSequence charSequence = this.f456a;
                int i3 = i2 - 1;
                this.d = i3;
                char charAt2 = charSequence.charAt(i3);
                this.e = (char) charAt2;
                if (charAt2 == '<') {
                    return 12;
                }
                if (charAt2 == '>') {
                    break;
                } else if (charAt2 == '\"' || charAt2 == '\'') {
                    char c2 = this.e;
                    do {
                        int i4 = this.d;
                        if (i4 <= 0) {
                            break;
                        }
                        CharSequence charSequence2 = this.f456a;
                        int i5 = i4 - 1;
                        this.d = i5;
                        charAt = charSequence2.charAt(i5);
                        this.e = (char) charAt;
                    } while (charAt != c2);
                }
            }
            this.d = i;
            this.e = (char) 62;
            return 13;
        }

        @DexIgnore
        public final byte i() {
            char charAt;
            int i = this.d;
            while (true) {
                int i2 = this.d;
                if (i2 < this.c) {
                    CharSequence charSequence = this.f456a;
                    this.d = i2 + 1;
                    char charAt2 = charSequence.charAt(i2);
                    this.e = (char) charAt2;
                    if (charAt2 == '>') {
                        return 12;
                    }
                    if (charAt2 == '\"' || charAt2 == '\'') {
                        char c2 = this.e;
                        do {
                            int i3 = this.d;
                            if (i3 >= this.c) {
                                break;
                            }
                            CharSequence charSequence2 = this.f456a;
                            this.d = i3 + 1;
                            charAt = charSequence2.charAt(i3);
                            this.e = (char) charAt;
                        } while (charAt != c2);
                    }
                } else {
                    this.d = i;
                    this.e = (char) 60;
                    return 13;
                }
            }
        }
    }

    @DexIgnore
    public bn0(boolean z, int i, fn0 fn0) {
        this.f454a = z;
        this.b = i;
        this.c = fn0;
    }

    @DexIgnore
    public static int a(CharSequence charSequence) {
        return new b(charSequence, false).d();
    }

    @DexIgnore
    public static int b(CharSequence charSequence) {
        return new b(charSequence, false).e();
    }

    @DexIgnore
    public static bn0 c() {
        return new a().a();
    }

    @DexIgnore
    public static boolean e(Locale locale) {
        return hn0.b(locale) == 1;
    }

    @DexIgnore
    public boolean d() {
        return (this.b & 2) != 0;
    }

    @DexIgnore
    public final String f(CharSequence charSequence, fn0 fn0) {
        boolean a2 = fn0.a(charSequence, 0, charSequence.length());
        return (this.f454a || (!a2 && b(charSequence) != 1)) ? (!this.f454a || (a2 && b(charSequence) != -1)) ? "" : f : e;
    }

    @DexIgnore
    public final String g(CharSequence charSequence, fn0 fn0) {
        boolean a2 = fn0.a(charSequence, 0, charSequence.length());
        return (this.f454a || (!a2 && a(charSequence) != 1)) ? (!this.f454a || (a2 && a(charSequence) != -1)) ? "" : f : e;
    }

    @DexIgnore
    public CharSequence h(CharSequence charSequence) {
        return i(charSequence, this.c, true);
    }

    @DexIgnore
    public CharSequence i(CharSequence charSequence, fn0 fn0, boolean z) {
        if (charSequence == null) {
            return null;
        }
        boolean a2 = fn0.a(charSequence, 0, charSequence.length());
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if (d() && z) {
            spannableStringBuilder.append((CharSequence) g(charSequence, a2 ? gn0.b : gn0.f1332a));
        }
        if (a2 != this.f454a) {
            spannableStringBuilder.append(a2 ? (char) '\u202b' : '\u202a');
            spannableStringBuilder.append(charSequence);
            spannableStringBuilder.append('\u202c');
        } else {
            spannableStringBuilder.append(charSequence);
        }
        if (z) {
            spannableStringBuilder.append((CharSequence) f(charSequence, a2 ? gn0.b : gn0.f1332a));
        }
        return spannableStringBuilder;
    }
}
