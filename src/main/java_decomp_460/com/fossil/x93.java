package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x93 extends pu2 implements u93 {
    @DexIgnore
    public x93() {
        super("com.google.android.gms.measurement.api.internal.IBundleReceiver");
    }

    @DexIgnore
    @Override // com.fossil.pu2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        c((Bundle) qt2.a(parcel, Bundle.CREATOR));
        parcel2.writeNoException();
        return true;
    }
}
