package com.fossil;

import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class k84 {
    @DexIgnore
    public static /* final */ Pattern e; // = Pattern.compile("http(s?)://[^\\/]+", 2);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1877a;
    @DexIgnore
    public /* final */ kb4 b;
    @DexIgnore
    public /* final */ ib4 c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public k84(String str, String str2, kb4 kb4, ib4 ib4) {
        if (str2 == null) {
            throw new IllegalArgumentException("url must not be null.");
        } else if (kb4 != null) {
            this.d = str;
            this.f1877a = f(str2);
            this.b = kb4;
            this.c = ib4;
        } else {
            throw new IllegalArgumentException("requestFactory must not be null.");
        }
    }

    @DexIgnore
    public jb4 c() {
        return d(Collections.emptyMap());
    }

    @DexIgnore
    public jb4 d(Map<String, String> map) {
        jb4 a2 = this.b.a(this.c, e(), map);
        a2.d("User-Agent", "Crashlytics Android SDK/" + w84.i());
        a2.d("X-CRASHLYTICS-DEVELOPER-TOKEN", "470fa2b4ae81cd56ecbcda9735803434cec591fa");
        return a2;
    }

    @DexIgnore
    public String e() {
        return this.f1877a;
    }

    @DexIgnore
    public final String f(String str) {
        return !r84.D(this.d) ? e.matcher(str).replaceFirst(this.d) : str;
    }
}
