package com.fossil;

import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ds5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f828a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[AddressWrapper.AddressType.values().length];
        f828a = iArr;
        iArr[AddressWrapper.AddressType.HOME.ordinal()] = 1;
        f828a[AddressWrapper.AddressType.WORK.ordinal()] = 2;
        f828a[AddressWrapper.AddressType.OTHER.ordinal()] = 3;
        int[] iArr2 = new int[LocationSource.ErrorState.values().length];
        b = iArr2;
        iArr2[LocationSource.ErrorState.LOCATION_PERMISSION_OFF.ordinal()] = 1;
        b[LocationSource.ErrorState.LOCATION_SERVICE_OFF.ordinal()] = 2;
    }
    */
}
