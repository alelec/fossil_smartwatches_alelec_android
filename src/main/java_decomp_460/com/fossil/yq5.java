package com.fossil;

import com.misfit.frameworks.buttonservice.communite.CommunicateMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class yq5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f4356a;

    /*
    static {
        int[] iArr = new int[CommunicateMode.values().length];
        f4356a = iArr;
        iArr[CommunicateMode.SENDING_ENCRYPTED_DATA_SESSION.ordinal()] = 1;
        f4356a[CommunicateMode.HW_LOG_SYNC.ordinal()] = 2;
        f4356a[CommunicateMode.SYNC.ordinal()] = 3;
        f4356a[CommunicateMode.GET_BATTERY_LEVEL.ordinal()] = 4;
        f4356a[CommunicateMode.OTA.ordinal()] = 5;
        f4356a[CommunicateMode.MOVE_HAND.ordinal()] = 6;
        f4356a[CommunicateMode.SET_ALARM.ordinal()] = 7;
        f4356a[CommunicateMode.RECONNECT.ordinal()] = 8;
        f4356a[CommunicateMode.EXCHANGE_SECRET_KEY.ordinal()] = 9;
        f4356a[CommunicateMode.SET_WATCH_PARAMS.ordinal()] = 10;
        f4356a[CommunicateMode.READ_CURRENT_WORKOUT_SESSION.ordinal()] = 11;
        f4356a[CommunicateMode.SET_WATCH_APP_FILE_SESSION.ordinal()] = 12;
    }
    */
}
