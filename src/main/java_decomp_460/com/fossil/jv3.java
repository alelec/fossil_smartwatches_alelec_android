package com.fossil;

import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jv3 extends sb2 implements wu3 {
    @DexIgnore
    public jv3(DataHolder dataHolder, int i) {
        super(dataHolder, i);
    }

    @DexIgnore
    public final String e() {
        return c("asset_key");
    }

    @DexIgnore
    @Override // com.fossil.wu3
    public final String getId() {
        return c("asset_id");
    }
}
