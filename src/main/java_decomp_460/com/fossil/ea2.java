package com.fossil;

import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.m62;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ea2 {
    @DexIgnore
    public static /* final */ Status d; // = new Status(8, "The connection to Google Play services was lost");
    @DexIgnore
    public static /* final */ BasePendingResult<?>[] e; // = new BasePendingResult[0];

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set<BasePendingResult<?>> f901a; // = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
    @DexIgnore
    public /* final */ ia2 b; // = new ha2(this);
    @DexIgnore
    public /* final */ Map<m62.c<?>, m62.f> c;

    @DexIgnore
    public ea2(Map<m62.c<?>, m62.f> map) {
        this.c = map;
    }

    @DexIgnore
    public final void a() {
        BasePendingResult[] basePendingResultArr = (BasePendingResult[]) this.f901a.toArray(e);
        for (BasePendingResult basePendingResult : basePendingResultArr) {
            basePendingResult.n(null);
            if (basePendingResult.r() != null) {
                basePendingResult.d(null);
                IBinder w = this.c.get(((i72) basePendingResult).w()).w();
                if (basePendingResult.i()) {
                    basePendingResult.n(new ga2(basePendingResult, null, w, null));
                } else if (w == null || !w.isBinderAlive()) {
                    basePendingResult.n(null);
                    basePendingResult.e();
                    basePendingResult.r().intValue();
                    throw new NullPointerException();
                } else {
                    ga2 ga2 = new ga2(basePendingResult, null, w, null);
                    basePendingResult.n(ga2);
                    try {
                        w.linkToDeath(ga2, 0);
                    } catch (RemoteException e2) {
                        basePendingResult.e();
                        basePendingResult.r().intValue();
                        throw new NullPointerException();
                    }
                }
                this.f901a.remove(basePendingResult);
            } else if (basePendingResult.s()) {
                this.f901a.remove(basePendingResult);
            }
        }
    }

    @DexIgnore
    public final void b(BasePendingResult<? extends z62> basePendingResult) {
        this.f901a.add(basePendingResult);
        basePendingResult.n(this.b);
    }

    @DexIgnore
    public final void c() {
        for (BasePendingResult basePendingResult : (BasePendingResult[]) this.f901a.toArray(e)) {
            basePendingResult.q(d);
        }
    }
}
