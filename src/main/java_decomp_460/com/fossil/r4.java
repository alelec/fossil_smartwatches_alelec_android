package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum r4 {
    BOND_NONE(10),
    BONDING(11),
    BONDED(12);
    
    @DexIgnore
    public static /* final */ p4 f; // = new p4(null);

    @DexIgnore
    public r4(int i) {
    }
}
