package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.yk1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hd extends BroadcastReceiver {
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        zk1 zk1;
        String str = null;
        String action = intent != null ? intent.getAction() : null;
        if (action != null && action.hashCode() == -343131398 && action.equals("com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED")) {
            e60 e60 = (e60) intent.getParcelableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE");
            yk1.d dVar = (yk1.d) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_HID_STATE");
            yk1.d dVar2 = (yk1.d) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_HID_STATE");
            m80 m80 = m80.c;
            zw zwVar = zw.i;
            if (!(e60 == null || (zk1 = e60.u) == null)) {
                str = zk1.getMacAddress();
            }
            m80.a("ConnectionManager", "deviceHIDStateChangeReceiver: device=%s, previousState=%s, newState=%s.", str, dVar, dVar2);
            if (e60 != null && dVar != null && dVar2 != null) {
                zw.i.d(e60, dVar2);
            }
        }
    }
}
