package com.fossil;

import android.content.ComponentName;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cq3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ComponentName b;
    @DexIgnore
    public /* final */ /* synthetic */ aq3 c;

    @DexIgnore
    public cq3(aq3 aq3, ComponentName componentName) {
        this.c = aq3;
        this.b = componentName;
    }

    @DexIgnore
    public final void run() {
        this.c.c.E(this.b);
    }
}
