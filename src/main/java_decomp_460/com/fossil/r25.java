package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.view.indicator.CustomPageIndicator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class r25 extends ViewDataBinding {
    @DexIgnore
    public /* final */ CustomPageIndicator q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ViewPager2 s;

    @DexIgnore
    public r25(Object obj, View view, int i, CustomPageIndicator customPageIndicator, ConstraintLayout constraintLayout, ViewPager2 viewPager2) {
        super(obj, view, i);
        this.q = customPageIndicator;
        this.r = constraintLayout;
        this.s = viewPager2;
    }
}
