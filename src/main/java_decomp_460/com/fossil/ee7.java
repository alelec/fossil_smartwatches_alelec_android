package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ee7 implements ie7 {
    @DexIgnore
    @Override // com.fossil.ie7
    public be7 b() {
        return new be7(g(), h());
    }

    @DexIgnore
    @Override // com.fossil.ie7
    public boolean c() {
        return Boolean.TRUE.equals(a("noResult"));
    }

    @DexIgnore
    @Override // com.fossil.ie7
    public Boolean d() {
        return e("inTransaction");
    }

    @DexIgnore
    public final Boolean e(String str) {
        Object a2 = a(str);
        if (a2 instanceof Boolean) {
            return (Boolean) a2;
        }
        return null;
    }

    @DexIgnore
    public boolean f() {
        return Boolean.TRUE.equals(a("continueOnError"));
    }

    @DexIgnore
    public final String g() {
        return (String) a("sql");
    }

    @DexIgnore
    public final List<Object> h() {
        return (List) a("arguments");
    }
}
