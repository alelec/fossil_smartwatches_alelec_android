package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ce3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ce3> CREATOR; // = new ye3();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ zd3 c;
    @DexIgnore
    public /* final */ Float d;

    @DexIgnore
    public ce3(int i) {
        this(i, (zd3) null, (Float) null);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ce3(int i, IBinder iBinder, Float f) {
        this(i, iBinder == null ? null : new zd3(rg2.a.e(iBinder)), f);
    }

    @DexIgnore
    public ce3(int i, zd3 zd3, Float f) {
        rc2.b(i != 3 || (zd3 != null && (f != null && (f.floatValue() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : (f.floatValue() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 0 : -1)) > 0)), String.format("Invalid Cap: type=%s bitmapDescriptor=%s bitmapRefWidth=%s", Integer.valueOf(i), zd3, f));
        this.b = i;
        this.c = zd3;
        this.d = f;
    }

    @DexIgnore
    public ce3(zd3 zd3, float f) {
        this(3, zd3, Float.valueOf(f));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ce3)) {
            return false;
        }
        ce3 ce3 = (ce3) obj;
        return this.b == ce3.b && pc2.a(this.c, ce3.c) && pc2.a(this.d, ce3.d);
    }

    @DexIgnore
    public int hashCode() {
        return pc2.b(Integer.valueOf(this.b), this.c, this.d);
    }

    @DexIgnore
    public String toString() {
        int i = this.b;
        StringBuilder sb = new StringBuilder(23);
        sb.append("[Cap: type=");
        sb.append(i);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.n(parcel, 2, this.b);
        zd3 zd3 = this.c;
        bd2.m(parcel, 3, zd3 == null ? null : zd3.a().asBinder(), false);
        bd2.l(parcel, 4, this.d, false);
        bd2.b(parcel, a2);
    }
}
