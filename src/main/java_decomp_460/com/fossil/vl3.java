package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vl3 extends BroadcastReceiver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ yq3 f3779a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public vl3(yq3 yq3) {
        rc2.k(yq3);
        this.f3779a = yq3;
    }

    @DexIgnore
    public final void b() {
        this.f3779a.b0();
        this.f3779a.c().h();
        if (!this.b) {
            this.f3779a.e().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.c = this.f3779a.S().x();
            this.f3779a.d().N().b("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.c));
            this.b = true;
        }
    }

    @DexIgnore
    public final void c() {
        this.f3779a.b0();
        this.f3779a.c().h();
        this.f3779a.c().h();
        if (this.b) {
            this.f3779a.d().N().a("Unregistering connectivity change receiver");
            this.b = false;
            this.c = false;
            try {
                this.f3779a.e().unregisterReceiver(this);
            } catch (IllegalArgumentException e) {
                this.f3779a.d().F().b("Failed to unregister the network broadcast receiver", e);
            }
        }
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        this.f3779a.b0();
        String action = intent.getAction();
        this.f3779a.d().N().b("NetworkBroadcastReceiver received action", action);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            boolean x = this.f3779a.S().x();
            if (this.c != x) {
                this.c = x;
                this.f3779a.c().y(new ul3(this, x));
                return;
            }
            return;
        }
        this.f3779a.d().I().b("NetworkBroadcastReceiver received unknown action", action);
    }
}
