package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ej2 implements Parcelable.Creator<dj2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ dj2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        Status status = null;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                arrayList = ad2.j(parcel, t, uh2.CREATOR);
            } else if (l != 2) {
                ad2.B(parcel, t);
            } else {
                status = (Status) ad2.e(parcel, t, Status.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new dj2(arrayList, status);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ dj2[] newArray(int i) {
        return new dj2[i];
    }
}
