package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n93 implements k93 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f2484a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.collection.service.update_with_analytics_fix", false);

    @DexIgnore
    @Override // com.fossil.k93
    public final boolean zza() {
        return f2484a.o().booleanValue();
    }
}
