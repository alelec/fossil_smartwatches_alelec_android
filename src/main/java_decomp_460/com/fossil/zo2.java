package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<zo2> CREATOR; // = new cp2();
    @DexIgnore
    public /* final */ uh2 b;

    @DexIgnore
    public zo2(uh2 uh2) {
        this.b = uh2;
    }

    @DexIgnore
    public final uh2 c() {
        return this.b;
    }

    @DexIgnore
    public final String toString() {
        return String.format("ApplicationUnregistrationRequest{%s}", this.b);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.t(parcel, 1, this.b, i, false);
        bd2.b(parcel, a2);
    }
}
