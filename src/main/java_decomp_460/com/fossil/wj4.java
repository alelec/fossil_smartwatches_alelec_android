package com.fossil;

import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wj4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<Type, aj4<?>> f3950a;
    @DexIgnore
    public /* final */ lk4 b; // = lk4.a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements bk4<T> {
        @DexIgnore
        public a(wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.bk4
        public T a() {
            return (T) new ConcurrentHashMap();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements bk4<T> {
        @DexIgnore
        public b(wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.bk4
        public T a() {
            return (T) new TreeMap();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements bk4<T> {
        @DexIgnore
        public c(wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.bk4
        public T a() {
            return (T) new LinkedHashMap();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements bk4<T> {
        @DexIgnore
        public d(wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.bk4
        public T a() {
            return (T) new ak4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements bk4<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ fk4 f3951a; // = fk4.b();
        @DexIgnore
        public /* final */ /* synthetic */ Class b;
        @DexIgnore
        public /* final */ /* synthetic */ Type c;

        @DexIgnore
        public e(wj4 wj4, Class cls, Type type) {
            this.b = cls;
            this.c = type;
        }

        @DexIgnore
        @Override // com.fossil.bk4
        public T a() {
            try {
                return (T) this.f3951a.c(this.b);
            } catch (Exception e) {
                throw new RuntimeException("Unable to invoke no-args constructor for " + this.c + ". Registering an InstanceCreator with Gson for this type may fix this problem.", e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements bk4<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ aj4 f3952a;
        @DexIgnore
        public /* final */ /* synthetic */ Type b;

        @DexIgnore
        public f(wj4 wj4, aj4 aj4, Type type) {
            this.f3952a = aj4;
            this.b = type;
        }

        @DexIgnore
        @Override // com.fossil.bk4
        public T a() {
            return (T) this.f3952a.createInstance(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements bk4<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ aj4 f3953a;
        @DexIgnore
        public /* final */ /* synthetic */ Type b;

        @DexIgnore
        public g(wj4 wj4, aj4 aj4, Type type) {
            this.f3953a = aj4;
            this.b = type;
        }

        @DexIgnore
        @Override // com.fossil.bk4
        public T a() {
            return (T) this.f3953a.createInstance(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements bk4<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Constructor f3954a;

        @DexIgnore
        public h(wj4 wj4, Constructor constructor) {
            this.f3954a = constructor;
        }

        @DexIgnore
        @Override // com.fossil.bk4
        public T a() {
            try {
                return (T) this.f3954a.newInstance(null);
            } catch (InstantiationException e) {
                throw new RuntimeException("Failed to invoke " + this.f3954a + " with no args", e);
            } catch (InvocationTargetException e2) {
                throw new RuntimeException("Failed to invoke " + this.f3954a + " with no args", e2.getTargetException());
            } catch (IllegalAccessException e3) {
                throw new AssertionError(e3);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements bk4<T> {
        @DexIgnore
        public i(wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.bk4
        public T a() {
            return (T) new TreeSet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements bk4<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Type f3955a;

        @DexIgnore
        public j(wj4 wj4, Type type) {
            this.f3955a = type;
        }

        @DexIgnore
        @Override // com.fossil.bk4
        public T a() {
            Type type = this.f3955a;
            if (type instanceof ParameterizedType) {
                Type type2 = ((ParameterizedType) type).getActualTypeArguments()[0];
                if (type2 instanceof Class) {
                    return (T) EnumSet.noneOf((Class) type2);
                }
                throw new ej4("Invalid EnumSet type: " + this.f3955a.toString());
            }
            throw new ej4("Invalid EnumSet type: " + this.f3955a.toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k implements bk4<T> {
        @DexIgnore
        public k(wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.bk4
        public T a() {
            return (T) new LinkedHashSet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l implements bk4<T> {
        @DexIgnore
        public l(wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.bk4
        public T a() {
            return (T) new ArrayDeque();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class m implements bk4<T> {
        @DexIgnore
        public m(wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.bk4
        public T a() {
            return (T) new ArrayList();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class n implements bk4<T> {
        @DexIgnore
        public n(wj4 wj4) {
        }

        @DexIgnore
        @Override // com.fossil.bk4
        public T a() {
            return (T) new ConcurrentSkipListMap();
        }
    }

    @DexIgnore
    public wj4(Map<Type, aj4<?>> map) {
        this.f3950a = map;
    }

    @DexIgnore
    public <T> bk4<T> a(TypeToken<T> typeToken) {
        Type type = typeToken.getType();
        Class<? super T> rawType = typeToken.getRawType();
        aj4<?> aj4 = this.f3950a.get(type);
        if (aj4 != null) {
            return new f(this, aj4, type);
        }
        aj4<?> aj42 = this.f3950a.get(rawType);
        if (aj42 != null) {
            return new g(this, aj42, type);
        }
        bk4<T> b2 = b(rawType);
        if (b2 != null) {
            return b2;
        }
        bk4<T> c2 = c(type, rawType);
        return c2 == null ? d(type, rawType) : c2;
    }

    @DexIgnore
    public final <T> bk4<T> b(Class<? super T> cls) {
        try {
            Constructor<? super T> declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                this.b.b(declaredConstructor);
            }
            return new h(this, declaredConstructor);
        } catch (NoSuchMethodException e2) {
            return null;
        }
    }

    @DexIgnore
    public final <T> bk4<T> c(Type type, Class<? super T> cls) {
        if (Collection.class.isAssignableFrom(cls)) {
            return SortedSet.class.isAssignableFrom(cls) ? new i(this) : EnumSet.class.isAssignableFrom(cls) ? new j(this, type) : Set.class.isAssignableFrom(cls) ? new k(this) : Queue.class.isAssignableFrom(cls) ? new l(this) : new m(this);
        }
        if (Map.class.isAssignableFrom(cls)) {
            return ConcurrentNavigableMap.class.isAssignableFrom(cls) ? new n(this) : ConcurrentMap.class.isAssignableFrom(cls) ? new a(this) : SortedMap.class.isAssignableFrom(cls) ? new b(this) : (!(type instanceof ParameterizedType) || String.class.isAssignableFrom(TypeToken.get(((ParameterizedType) type).getActualTypeArguments()[0]).getRawType())) ? new d(this) : new c(this);
        }
        return null;
    }

    @DexIgnore
    public final <T> bk4<T> d(Type type, Class<? super T> cls) {
        return new e(this, cls, type);
    }

    @DexIgnore
    public String toString() {
        return this.f3950a.toString();
    }
}
