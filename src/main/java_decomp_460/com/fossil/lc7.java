package com.fossil;

import com.portfolio.platform.workers.TimeChangeReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lc7 implements MembersInjector<TimeChangeReceiver> {
    @DexIgnore
    public static void a(TimeChangeReceiver timeChangeReceiver, bk5 bk5) {
        timeChangeReceiver.f4785a = bk5;
    }

    @DexIgnore
    public static void b(TimeChangeReceiver timeChangeReceiver, on5 on5) {
        timeChangeReceiver.b = on5;
    }
}
