package com.fossil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import com.facebook.places.PlaceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fx1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ fx1 f1233a; // = new fx1();

    @DexIgnore
    public static /* synthetic */ boolean b(fx1 fx1, Context context, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return fx1.a(context, z);
    }

    @DexIgnore
    public final boolean a(Context context, boolean z) {
        Network activeNetwork;
        NetworkCapabilities networkCapabilities;
        NetworkInfo networkInfo = null;
        ConnectivityManager connectivityManager = (ConnectivityManager) (context != null ? context.getSystemService("connectivity") : null);
        if (Build.VERSION.SDK_INT >= 23) {
            return (connectivityManager == null || (activeNetwork = connectivityManager.getActiveNetwork()) == null || (networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork)) == null || (!z && !networkCapabilities.hasCapability(18))) ? false : true;
        }
        if (connectivityManager != null) {
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return z && networkInfo != null && networkInfo.isConnected();
    }

    @DexIgnore
    public final boolean c(Context context) {
        Context applicationContext;
        Network activeNetwork;
        NetworkCapabilities networkCapabilities;
        Object obj = null;
        if (Build.VERSION.SDK_INT >= 23) {
            if (context != null) {
                obj = context.getSystemService("connectivity");
            }
            ConnectivityManager connectivityManager = (ConnectivityManager) obj;
            return (connectivityManager == null || (activeNetwork = connectivityManager.getActiveNetwork()) == null || (networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork)) == null || !networkCapabilities.hasTransport(1)) ? false : true;
        }
        if (!(context == null || (applicationContext = context.getApplicationContext()) == null)) {
            obj = applicationContext.getSystemService(PlaceManager.PARAM_WIFI);
        }
        WifiManager wifiManager = (WifiManager) obj;
        if (wifiManager == null) {
            return false;
        }
        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
        return (!wifiManager.isWifiEnabled() || connectionInfo == null || connectionInfo.getNetworkId() == -1) ? false : true;
    }
}
