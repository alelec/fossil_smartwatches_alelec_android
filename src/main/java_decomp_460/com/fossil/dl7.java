package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dl7<T> implements Serializable {
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public /* final */ Object value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Serializable {
        @DexIgnore
        public /* final */ Throwable exception;

        @DexIgnore
        public b(Throwable th) {
            pq7.c(th, "exception");
            this.exception = th;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return (obj instanceof b) && pq7.a(this.exception, ((b) obj).exception);
        }

        @DexIgnore
        public int hashCode() {
            return this.exception.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Failure(" + this.exception + ')';
        }
    }

    @DexIgnore
    public /* synthetic */ dl7(Object obj) {
        this.value = obj;
    }

    @DexIgnore
    /* renamed from: box-impl  reason: not valid java name */
    public static final /* synthetic */ dl7 m0boximpl(Object obj) {
        return new dl7(obj);
    }

    @DexIgnore
    /* renamed from: constructor-impl  reason: not valid java name */
    public static Object m1constructorimpl(Object obj) {
        return obj;
    }

    @DexIgnore
    /* renamed from: equals-impl  reason: not valid java name */
    public static boolean m2equalsimpl(Object obj, Object obj2) {
        return (obj2 instanceof dl7) && pq7.a(obj, ((dl7) obj2).m9unboximpl());
    }

    @DexIgnore
    /* renamed from: equals-impl0  reason: not valid java name */
    public static final boolean m3equalsimpl0(Object obj, Object obj2) {
        return pq7.a(obj, obj2);
    }

    @DexIgnore
    /* renamed from: exceptionOrNull-impl  reason: not valid java name */
    public static final Throwable m4exceptionOrNullimpl(Object obj) {
        if (obj instanceof b) {
            return ((b) obj).exception;
        }
        return null;
    }

    @DexIgnore
    /* renamed from: hashCode-impl  reason: not valid java name */
    public static int m5hashCodeimpl(Object obj) {
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: isFailure-impl  reason: not valid java name */
    public static final boolean m6isFailureimpl(Object obj) {
        return obj instanceof b;
    }

    @DexIgnore
    /* renamed from: isSuccess-impl  reason: not valid java name */
    public static final boolean m7isSuccessimpl(Object obj) {
        return !(obj instanceof b);
    }

    @DexIgnore
    /* renamed from: toString-impl  reason: not valid java name */
    public static String m8toStringimpl(Object obj) {
        if (obj instanceof b) {
            return obj.toString();
        }
        return "Success(" + obj + ')';
    }

    @DexIgnore
    public static /* synthetic */ void value$annotations() {
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return m2equalsimpl(this.value, obj);
    }

    @DexIgnore
    public int hashCode() {
        return m5hashCodeimpl(this.value);
    }

    @DexIgnore
    public String toString() {
        return m8toStringimpl(this.value);
    }

    @DexIgnore
    /* renamed from: unbox-impl  reason: not valid java name */
    public final /* synthetic */ Object m9unboximpl() {
        return this.value;
    }
}
