package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class p25 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ NumberPicker u;
    @DexIgnore
    public /* final */ NumberPicker v;
    @DexIgnore
    public /* final */ NumberPicker w;

    @DexIgnore
    public p25(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView, NumberPicker numberPicker, NumberPicker numberPicker2, NumberPicker numberPicker3) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleButton;
        this.s = flexibleButton2;
        this.t = flexibleTextView;
        this.u = numberPicker;
        this.v = numberPicker2;
        this.w = numberPicker3;
    }
}
