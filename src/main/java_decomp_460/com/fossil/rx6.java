package com.fossil;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.px6;
import com.fossil.qx6;
import com.fossil.t47;
import com.fossil.tx6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rx6 extends qv5 implements iy6, t47.g, t47.h {
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public hy6 h;
    @DexIgnore
    public ey6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final rx6 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            rx6 rx6 = new rx6();
            rx6.setArguments(bundle);
            return rx6;
        }

        @DexIgnore
        public final String b() {
            String simpleName = rx6.class.getSimpleName();
            pq7.b(simpleName, "PairingFragment::class.java.simpleName");
            return simpleName;
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void D0() {
        if (isActive()) {
            t47.f fVar = new t47.f(2131558481);
            fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886795));
            fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886794));
            fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886797));
            fVar.b(2131363373);
            fVar.h(false);
            fVar.k(getChildFragmentManager(), "NETWORK_ERROR");
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void F2(String str, boolean z) {
        pq7.c(str, "serial");
        ey6 ey6 = this.i;
        if (ey6 instanceof ux6) {
            ((ux6) ey6).t3();
            return;
        }
        ux6 a2 = ux6.x.a(str, z, 1);
        this.i = a2;
        if (a2 != null) {
            G6(a2, "", 2131362158);
            ey6 ey62 = this.i;
            if (ey62 != null) {
                hy6 hy6 = this.h;
                if (hy6 != null) {
                    ey62.M5(hy6);
                } else {
                    pq7.n("mPairingPresenter");
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void G3(List<String> list) {
        pq7.c(list, "instructions");
        ey6 ey6 = this.i;
        if (ey6 instanceof px6) {
            ((px6) ey6).U6(list);
            return;
        }
        px6.a aVar = px6.z;
        hy6 hy6 = this.h;
        if (hy6 != null) {
            px6 a2 = aVar.a(list, hy6.s());
            this.i = a2;
            if (a2 != null) {
                G6(a2, "PairingAuthorizeFragment", 2131362158);
                ey6 ey62 = this.i;
                if (ey62 != null) {
                    hy6 hy62 = this.h;
                    if (hy62 != null) {
                        ey62.M5(hy62);
                    } else {
                        pq7.n("mPairingPresenter");
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mPairingPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void K2(int i2, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String b = k.b();
        local.d(b, "showDeviceConnectFail() - errorCode = " + i2);
        if (!isActive() || getActivity() == null) {
            return;
        }
        if (str != null) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String b2 = k.b();
            StringBuilder sb = new StringBuilder();
            sb.append("finish showDeviceConnectFail isOnboardingFlow ");
            hy6 hy6 = this.h;
            if (hy6 != null) {
                sb.append(hy6.s());
                local2.d(b2, sb.toString());
                e0();
                TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    pq7.b(activity, "activity!!");
                    hy6 hy62 = this.h;
                    if (hy62 != null) {
                        aVar.b(activity, str, true, hy62.s());
                    } else {
                        pq7.n("mPairingPresenter");
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.n("mPairingPresenter");
                throw null;
            }
        } else {
            e0();
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String b3 = k.b();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("finish serial empty showDeviceConnectFail isOnboardingFlow ");
            hy6 hy63 = this.h;
            if (hy63 != null) {
                sb2.append(hy63.s());
                local3.d(b3, sb2.toString());
                TroubleshootingActivity.a aVar2 = TroubleshootingActivity.B;
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    pq7.b(activity2, "activity!!");
                    hy6 hy64 = this.h;
                    if (hy64 != null) {
                        TroubleshootingActivity.a.c(aVar2, activity2, null, true, hy64.s(), 2, null);
                    } else {
                        pq7.n("mPairingPresenter");
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.n("mPairingPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    /* renamed from: K6 */
    public void M5(hy6 hy6) {
        pq7.c(hy6, "presenter");
        this.h = hy6;
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void P3(String str, boolean z, boolean z2) {
        pq7.c(str, "serial");
        ey6 ey6 = this.i;
        if (ey6 instanceof ux6) {
            ((ux6) ey6).A(z2);
            return;
        }
        ux6 a2 = ux6.x.a(str, z, z2 ? 2 : 3);
        this.i = a2;
        if (a2 != null) {
            G6(a2, "", 2131362158);
            ey6 ey62 = this.i;
            if (ey62 != null) {
                hy6 hy6 = this.h;
                if (hy6 != null) {
                    ey62.M5(hy6);
                } else {
                    pq7.n("mPairingPresenter");
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0022, code lost:
        if (r5.equals("SERVER_ERROR") != false) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00fd, code lost:
        if (r5.equals("SERVER_MAINTENANCE") != false) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0024, code lost:
        e0();
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:4:0x0016  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0101  */
    @Override // com.fossil.t47.g, com.fossil.qv5
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void R5(java.lang.String r5, int r6, android.content.Intent r7) {
        /*
        // Method dump skipped, instructions count: 296
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rx6.R5(java.lang.String, int, android.content.Intent):void");
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void Z0() {
        ey6 ey6 = this.i;
        if (ey6 == null || !(ey6 instanceof tx6)) {
            tx6.a aVar = tx6.k;
            hy6 hy6 = this.h;
            if (hy6 != null) {
                tx6 a2 = aVar.a(hy6.s());
                this.i = a2;
                if (a2 != null) {
                    G6(a2, "", 2131362158);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.n("mPairingPresenter");
                throw null;
            }
        }
        ey6 ey62 = this.i;
        if (ey62 != null) {
            hy6 hy62 = this.h;
            if (hy62 != null) {
                ey62.M5(hy62);
            } else {
                pq7.n("mPairingPresenter");
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void b4(boolean z) {
        ey6 ey6 = this.i;
        if (ey6 instanceof px6) {
            ((px6) ey6).X6(z);
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void d0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ExploreWatchActivity.a aVar = ExploreWatchActivity.B;
            pq7.b(activity, "it");
            hy6 hy6 = this.h;
            if (hy6 != null) {
                aVar.a(activity, hy6.s());
            } else {
                pq7.n("mPairingPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            pq7.b(activity, "activity!!");
            if (!activity.isFinishing()) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    pq7.b(activity2, "activity!!");
                    if (!activity2.isDestroyed()) {
                        FragmentActivity activity3 = getActivity();
                        if (activity3 != null) {
                            activity3.finish();
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void h4(int i2, String str, String str2) {
        pq7.c(str, "deviceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String b = k.b();
        local.d(b, "showDeviceConnectFail due to network - errorCode = " + i2);
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.O(childFragmentManager, i2, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.h
    public void i1(String str) {
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void i2(String str) {
        pq7.c(str, "serial");
        ey6 ey6 = this.i;
        if (ey6 instanceof px6) {
            ((px6) ey6).V6(str);
            return;
        }
        px6.a aVar = px6.z;
        hy6 hy6 = this.h;
        if (hy6 != null) {
            px6 b = aVar.b(str, hy6.s());
            this.i = b;
            if (b != null) {
                G6(b, "PairingAuthorizeFragment", 2131362158);
                ey6 ey62 = this.i;
                if (ey62 != null) {
                    hy6 hy62 = this.h;
                    if (hy62 != null) {
                        ey62.M5(hy62);
                    } else {
                        pq7.n("mPairingPresenter");
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mPairingPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void l() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.B;
            pq7.b(activity, "it");
            aVar.a(activity, 2);
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void l4(String str) {
        pq7.c(str, "serial");
        TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
        Context context = getContext();
        if (context != null) {
            pq7.b(context, "context!!");
            hy6 hy6 = this.h;
            if (hy6 != null) {
                aVar.b(context, str, true, hy6.s());
            } else {
                pq7.n("mPairingPresenter");
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void o4(List<cl7<ShineDevice, String>> list) {
        pq7.c(list, "listDevices");
        ey6 ey6 = this.i;
        if (ey6 == null || !(ey6 instanceof qx6)) {
            qx6.a aVar = qx6.s;
            hy6 hy6 = this.h;
            if (hy6 != null) {
                qx6 a2 = aVar.a(hy6.s());
                a2.t2(list);
                this.i = a2;
                if (a2 != null) {
                    G6(a2, "", 2131362158);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.n("mPairingPresenter");
                throw null;
            }
        }
        ey6 ey62 = this.i;
        if (ey62 != null) {
            hy6 hy62 = this.h;
            if (hy62 != null) {
                ey62.M5(hy62);
            } else {
                pq7.n("mPairingPresenter");
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        return layoutInflater.inflate(2131558601, viewGroup, false);
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        hy6 hy6 = this.h;
        if (hy6 != null) {
            hy6.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        pq7.n("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        hy6 hy6 = this.h;
        if (hy6 != null) {
            hy6.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        hy6 hy6 = this.h;
        if (hy6 != null) {
            if (hy6.q() != null) {
                hy6 hy62 = this.h;
                if (hy62 != null) {
                    ShineDevice q = hy62.q();
                    if (q != null) {
                        bundle.putParcelable("PAIRING_DEVICE", q);
                    }
                } else {
                    pq7.n("mPairingPresenter");
                    throw null;
                }
            }
            super.onSaveInstanceState(bundle);
            return;
        }
        pq7.n("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        if (bundle != null && bundle.containsKey("PAIRING_DEVICE")) {
            hy6 hy6 = this.h;
            if (hy6 != null) {
                Parcelable parcelable = bundle.getParcelable("PAIRING_DEVICE");
                if (parcelable != null) {
                    hy6.w((ShineDevice) parcelable);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.n("mPairingPresenter");
                throw null;
            }
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            hy6 hy62 = this.h;
            if (hy62 != null) {
                hy62.z(arguments.getBoolean("IS_ONBOARDING_FLOW"));
            } else {
                pq7.n("mPairingPresenter");
                throw null;
            }
        }
        ey6 ey6 = (ey6) getChildFragmentManager().Y(2131362158);
        this.i = ey6;
        if (ey6 == null) {
            tx6.a aVar = tx6.k;
            hy6 hy63 = this.h;
            if (hy63 != null) {
                tx6 a2 = aVar.a(hy63.s());
                this.i = a2;
                if (a2 != null) {
                    G6(a2, "", 2131362158);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.n("mPairingPresenter");
                throw null;
            }
        }
        ey6 ey62 = this.i;
        if (ey62 != null) {
            hy6 hy64 = this.h;
            if (hy64 != null) {
                ey62.M5(hy64);
                hy6 hy65 = this.h;
                if (hy65 != null) {
                    ey6 ey63 = this.i;
                    hy65.y((ey63 instanceof tx6) || (ey63 instanceof qx6));
                    E6("scan_device_view");
                    return;
                }
                pq7.n("mPairingPresenter");
                throw null;
            }
            pq7.n("mPairingPresenter");
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void s6(String str) {
        pq7.c(str, "serial");
        if (isActive()) {
            s37 s37 = s37.c;
            String J = PortfolioApp.h0.c().J();
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.v0(J, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void t2(List<cl7<ShineDevice, String>> list) {
        pq7.c(list, "deviceList");
        ey6 ey6 = this.i;
        if (ey6 != null && (ey6 instanceof qx6)) {
            ((qx6) ey6).t2(list);
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void v2(String str, boolean z) {
        pq7.c(str, "serial");
        ey6 ey6 = this.i;
        if (ey6 instanceof ux6) {
            ((ux6) ey6).V2();
            return;
        }
        ux6 a2 = ux6.x.a(str, z, 0);
        this.i = a2;
        if (a2 != null) {
            G6(a2, "", 2131362158);
            ey6 ey62 = this.i;
            if (ey62 != null) {
                hy6 hy6 = this.h;
                if (hy6 != null) {
                    ey62.M5(hy6);
                } else {
                    pq7.n("mPairingPresenter");
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void v3(String str) {
        pq7.c(str, "serial");
        if (isActive()) {
            s37 s37 = s37.c;
            String J = PortfolioApp.h0.c().J();
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.w0(J, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void x4() {
        if (getActivity() != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            Context context = getContext();
            if (context != null) {
                pq7.b(context, "context!!");
                String J = PortfolioApp.h0.c().J();
                hy6 hy6 = this.h;
                if (hy6 != null) {
                    aVar.b(context, J, true, hy6.s());
                } else {
                    pq7.n("mPairingPresenter");
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.iy6
    public void y4(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String b = k.b();
        local.d(b, "openWearOSApp(), isChineseUser=" + z);
        if (z) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.w(childFragmentManager);
            return;
        }
        try {
            Intent launchIntentForPackage = PortfolioApp.h0.c().getPackageManager().getLaunchIntentForPackage("com.google.android.wearable.app");
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.startActivity(launchIntentForPackage);
            }
        } catch (Exception e) {
            try {
                FLogger.INSTANCE.getLocal().d(k.b(), "WearOS app not found, open in GG play");
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    activity2.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.google.android.wearable.app")));
                }
            } catch (ActivityNotFoundException e2) {
                FLogger.INSTANCE.getLocal().d(k.b(), "GG play not found, open in browser");
                FragmentActivity activity3 = getActivity();
                if (activity3 != null) {
                    activity3.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.wearable.app")));
                }
            }
        }
    }
}
