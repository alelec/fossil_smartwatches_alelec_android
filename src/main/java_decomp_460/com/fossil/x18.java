package com.fossil;

import java.net.InetSocketAddress;
import java.net.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x18 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ x08 f4031a;
    @DexIgnore
    public /* final */ Proxy b;
    @DexIgnore
    public /* final */ InetSocketAddress c;

    @DexIgnore
    public x18(x08 x08, Proxy proxy, InetSocketAddress inetSocketAddress) {
        if (x08 == null) {
            throw new NullPointerException("address == null");
        } else if (proxy == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress != null) {
            this.f4031a = x08;
            this.b = proxy;
            this.c = inetSocketAddress;
        } else {
            throw new NullPointerException("inetSocketAddress == null");
        }
    }

    @DexIgnore
    public x08 a() {
        return this.f4031a;
    }

    @DexIgnore
    public Proxy b() {
        return this.b;
    }

    @DexIgnore
    public boolean c() {
        return this.f4031a.i != null && this.b.type() == Proxy.Type.HTTP;
    }

    @DexIgnore
    public InetSocketAddress d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof x18) {
            x18 x18 = (x18) obj;
            return x18.f4031a.equals(this.f4031a) && x18.b.equals(this.b) && x18.c.equals(this.c);
        }
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.f4031a.hashCode() + 527) * 31) + this.b.hashCode()) * 31) + this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Route{" + this.c + "}";
    }
}
