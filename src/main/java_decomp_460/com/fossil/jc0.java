package com.fossil;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.fossil.blesdk.model.uiframework.packages.theme.ThemeTemplateFactory", f = "ThemeTemplateFactory.kt", l = {27, 31, 36}, m = "getThemeTemplate$blesdk_productionRelease")
public final class jc0 extends co7 {
    @DexIgnore
    public /* synthetic */ Object b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ /* synthetic */ nc0 d;
    @DexIgnore
    public Object e;
    @DexIgnore
    public Object f;
    @DexIgnore
    public Object g;
    @DexIgnore
    public Object h;
    @DexIgnore
    public Object i;
    @DexIgnore
    public boolean j;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jc0(nc0 nc0, qn7 qn7) {
        super(qn7);
        this.d = nc0;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        this.b = obj;
        this.c |= RecyclerView.UNDEFINED_DURATION;
        return this.d.a(null, null, false, this);
    }
}
