package com.fossil;

import com.fossil.af1;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lf1 implements af1<te1, InputStream> {
    @DexIgnore
    public static /* final */ nb1<Integer> b; // = nb1.f("com.bumptech.glide.load.model.stream.HttpGlideUrlLoader.Timeout", 2500);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ze1<te1, te1> f2187a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements bf1<te1, InputStream> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ze1<te1, te1> f2188a; // = new ze1<>(500);

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<te1, InputStream> b(ef1 ef1) {
            return new lf1(this.f2188a);
        }
    }

    @DexIgnore
    public lf1(ze1<te1, te1> ze1) {
        this.f2187a = ze1;
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<InputStream> b(te1 te1, int i, int i2, ob1 ob1) {
        ze1<te1, te1> ze1 = this.f2187a;
        if (ze1 != null) {
            te1 a2 = ze1.a(te1, 0, 0);
            if (a2 == null) {
                this.f2187a.b(te1, 0, 0, te1);
            } else {
                te1 = a2;
            }
        }
        return new af1.a<>(te1, new cc1(te1, ((Integer) ob1.c(b)).intValue()));
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(te1 te1) {
        return true;
    }
}
