package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qx1<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ry1 f3041a;

    @DexIgnore
    public qx1(ry1 ry1) {
        pq7.c(ry1, "signedVersion");
        this.f3041a = ry1;
    }

    @DexIgnore
    public abstract byte[] a(short s, T t);

    @DexIgnore
    public abstract byte[] b(T t);

    @DexIgnore
    public final ry1 c() {
        return this.f3041a;
    }
}
