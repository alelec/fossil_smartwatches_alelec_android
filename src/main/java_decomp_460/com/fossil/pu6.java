package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pu6 {
    @DexIgnore
    public pu6(InAppNotificationRepository inAppNotificationRepository) {
        pq7.c(inAppNotificationRepository, "repository");
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void a(ws4 ws4, Context context) {
        String str;
        String str2;
        String str3;
        String str4;
        String a2;
        String b;
        String a3;
        String b2;
        String a4;
        String e;
        String str5;
        String a5;
        String str6;
        String c;
        int i;
        String str7;
        String a6;
        int i2 = 99;
        pq7.c(ws4, "notification");
        pq7.c(context, "context");
        FLogger.INSTANCE.getLocal().e("InAppNotificationManager", "notificationId: " + ws4.c());
        String e2 = ws4.e();
        int hashCode = e2.hashCode();
        if (hashCode == 1433363166 ? !e2.equals("Title_Send_Friend_Request") : hashCode != 1898363949 || !e2.equals("Title_Send_Invitation_Challenge")) {
            String e3 = ws4.e();
            String e4 = ws4.e();
            int hashCode2 = e4.hashCode();
            String str8 = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
            String str9 = "Challenge";
            switch (hashCode2) {
                case -1559094286:
                    if (e4.equals("Title_Notification_End_Challenge")) {
                        str4 = um5.c(PortfolioApp.h0.c(), 2131887224);
                        pq7.b(str4, "LanguageHelper.getString\u2026tification_End_Challenge)");
                        hr7 hr7 = hr7.f1520a;
                        String c2 = um5.c(PortfolioApp.h0.c(), 2131886190);
                        pq7.b(c2, "LanguageHelper.getString\u2026tification_End_Challenge)");
                        ss4 b3 = ws4.b();
                        if (!(b3 == null || (b = b3.b()) == null)) {
                            str9 = b;
                        }
                        str2 = String.format(c2, Arrays.copyOf(new Object[]{str9}, 1));
                        pq7.b(str2, "java.lang.String.format(format, *args)");
                        ss4 b4 = ws4.b();
                        if (!(b4 == null || (a2 = b4.a()) == null)) {
                            i2 = a2.hashCode();
                            str3 = str4;
                            str5 = str3;
                            e3 = str2;
                            i = i2;
                            break;
                        }
                        str3 = str4;
                        str5 = str3;
                        e3 = str2;
                        i = i2;
                    }
                    str5 = "";
                    i = 99;
                    break;
                case -764698119:
                    if (e4.equals("Title_Notification_Start_Challenge")) {
                        str4 = um5.c(PortfolioApp.h0.c(), 2131887227);
                        pq7.b(str4, "LanguageHelper.getString\u2026fication_Start_Challenge)");
                        hr7 hr72 = hr7.f1520a;
                        String c3 = um5.c(PortfolioApp.h0.c(), 2131886193);
                        pq7.b(c3, "LanguageHelper.getString\u2026fication_Start_Challenge)");
                        ss4 b5 = ws4.b();
                        if (!(b5 == null || (b2 = b5.b()) == null)) {
                            str9 = b2;
                        }
                        str2 = String.format(c3, Arrays.copyOf(new Object[]{str9}, 1));
                        pq7.b(str2, "java.lang.String.format(format, *args)");
                        ss4 b6 = ws4.b();
                        if (!(b6 == null || (a3 = b6.a()) == null)) {
                            i2 = a3.hashCode();
                            str3 = str4;
                            str5 = str3;
                            e3 = str2;
                            i = i2;
                            break;
                        }
                        str3 = str4;
                        str5 = str3;
                        e3 = str2;
                        i = i2;
                    }
                    str5 = "";
                    i = 99;
                    break;
                case -473527954:
                    if (e4.equals("Title_Respond_Invitation_Challenge")) {
                        str = um5.c(PortfolioApp.h0.c(), 2131887228);
                        pq7.b(str, "LanguageHelper.getString\u2026ond_Invitation_Challenge)");
                        hz4 hz4 = hz4.f1561a;
                        lt4 d = ws4.d();
                        String b7 = d != null ? d.b() : null;
                        lt4 d2 = ws4.d();
                        String d3 = d2 != null ? d2.d() : null;
                        lt4 d4 = ws4.d();
                        if (!(d4 == null || (e = d4.e()) == null)) {
                            str8 = e;
                        }
                        String a7 = hz4.a(b7, d3, str8);
                        hr7 hr73 = hr7.f1520a;
                        String c4 = um5.c(PortfolioApp.h0.c(), 2131886194);
                        pq7.b(c4, "LanguageHelper.getString\u2026ond_Invitation_Challenge)");
                        str2 = String.format(c4, Arrays.copyOf(new Object[]{a7}, 1));
                        pq7.b(str2, "java.lang.String.format(format, *args)");
                        ss4 b8 = ws4.b();
                        if (!(b8 == null || (a4 = b8.a()) == null)) {
                            i2 = a4.hashCode();
                            str3 = str;
                            str5 = str3;
                            e3 = str2;
                            i = i2;
                            break;
                        }
                        str3 = str;
                        str5 = str3;
                        e3 = str2;
                        i = i2;
                    }
                    str5 = "";
                    i = 99;
                    break;
                case 238453777:
                    if (e4.equals("Title_Notification_Reached_Goal_Challenge")) {
                        String c5 = um5.c(PortfolioApp.h0.c(), 2131887225);
                        pq7.b(c5, "LanguageHelper.getString\u2026n_Reached_Goal_Challenge)");
                        String c6 = um5.c(PortfolioApp.h0.c(), 2131886191);
                        pq7.b(c6, "LanguageHelper.getString\u2026n_Reached_Goal_Challenge)");
                        ss4 b9 = ws4.b();
                        i2 = (b9 == null || (a5 = b9.a()) == null) ? 99 : a5.hashCode();
                        str5 = c5;
                        e3 = c6;
                        i = i2;
                        break;
                    }
                    str5 = "";
                    i = 99;
                    break;
                case 634854495:
                    if (e4.equals("Title_Accepted_Friend_Request")) {
                        str = um5.c(PortfolioApp.h0.c(), 2131887223);
                        pq7.b(str, "LanguageHelper.getString\u2026_Accepted_Friend_Request)");
                        hz4 hz42 = hz4.f1561a;
                        lt4 d5 = ws4.d();
                        String b10 = d5 != null ? d5.b() : null;
                        lt4 d6 = ws4.d();
                        String d7 = d6 != null ? d6.d() : null;
                        lt4 d8 = ws4.d();
                        if (d8 == null || (str6 = d8.e()) == null) {
                            str6 = str8;
                        }
                        String a8 = hz42.a(b10, d7, str6);
                        hr7 hr74 = hr7.f1520a;
                        String c7 = um5.c(PortfolioApp.h0.c(), 2131886189);
                        pq7.b(c7, "LanguageHelper.getString\u2026_Accepted_Friend_Request)");
                        str2 = String.format(c7, Arrays.copyOf(new Object[]{a8}, 1));
                        pq7.b(str2, "java.lang.String.format(format, *args)");
                        lt4 d9 = ws4.d();
                        if (!(d9 == null || (c = d9.c()) == null)) {
                            i2 = c.hashCode();
                            str3 = str;
                            str5 = str3;
                            e3 = str2;
                            i = i2;
                            break;
                        }
                        str3 = str;
                        str5 = str3;
                        e3 = str2;
                        i = i2;
                    }
                    str5 = "";
                    i = 99;
                    break;
                case 1669546642:
                    if (e4.equals("Title_Notification_Remind_End_Challenge")) {
                        str4 = um5.c(PortfolioApp.h0.c(), 2131887224);
                        pq7.b(str4, "LanguageHelper.getString\u2026tification_End_Challenge)");
                        hr7 hr75 = hr7.f1520a;
                        String c8 = um5.c(PortfolioApp.h0.c(), 2131886192);
                        pq7.b(c8, "LanguageHelper.getString\u2026ion_Remind_End_Challenge)");
                        ss4 b11 = ws4.b();
                        if (b11 == null || (str7 = b11.b()) == null) {
                            str7 = str9;
                        }
                        str2 = String.format(c8, Arrays.copyOf(new Object[]{str7}, 1));
                        pq7.b(str2, "java.lang.String.format(format, *args)");
                        ss4 b12 = ws4.b();
                        if (!(b12 == null || (a6 = b12.a()) == null)) {
                            i2 = a6.hashCode();
                            str3 = str4;
                            str5 = str3;
                            e3 = str2;
                            i = i2;
                            break;
                        }
                        str3 = str4;
                        str5 = str3;
                        e3 = str2;
                        i = i2;
                    }
                    str5 = "";
                    i = 99;
                    break;
                default:
                    str5 = "";
                    i = 99;
                    break;
            }
            cl5.c.g(PortfolioApp.h0.c(), i, str5, e3, null, null);
            return;
        }
        b(ws4, context);
    }

    @DexIgnore
    public final void b(ws4 ws4, Context context) {
        cl5.c.i(ws4, context);
    }
}
