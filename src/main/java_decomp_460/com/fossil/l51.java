package com.fossil;

import android.graphics.Bitmap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l51 implements j51 {
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ n51<Integer, Bitmap> b; // = new n51<>();
    @DexIgnore
    public /* final */ TreeMap<Integer, Integer> c; // = new TreeMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [('[' char), (r0v2 int), (']' char)] */
    @Override // com.fossil.j51
    public String a(int i, int i2, Bitmap.Config config) {
        pq7.c(config, "config");
        int a2 = y81.f4257a.a(i, i2, config);
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        sb.append(a2);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.j51
    public void b(Bitmap bitmap) {
        pq7.c(bitmap, "bitmap");
        int b2 = w81.b(bitmap);
        this.b.f(Integer.valueOf(b2), bitmap);
        Integer num = this.c.get(Integer.valueOf(b2));
        this.c.put(Integer.valueOf(b2), Integer.valueOf(num == null ? 1 : num.intValue() + 1));
    }

    @DexIgnore
    @Override // com.fossil.j51
    public Bitmap c(int i, int i2, Bitmap.Config config) {
        pq7.c(config, "config");
        int f = f(y81.f4257a.a(i, i2, config));
        Bitmap a2 = this.b.a(Integer.valueOf(f));
        if (a2 != null) {
            e(f, a2);
            a2.reconfigure(i, i2, config);
        }
        return a2;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [('[' char), (r0v1 int), (']' char)] */
    @Override // com.fossil.j51
    public String d(Bitmap bitmap) {
        pq7.c(bitmap, "bitmap");
        int b2 = w81.b(bitmap);
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        sb.append(b2);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    public final void e(int i, Bitmap bitmap) {
        Integer num = this.c.get(Integer.valueOf(i));
        if (num != null) {
            pq7.b(num, "sortedSizes[size] ?: run\u2026, this: $this\")\n        }");
            int intValue = num.intValue();
            if (intValue == 1) {
                this.c.remove(Integer.valueOf(i));
            } else {
                this.c.put(Integer.valueOf(i), Integer.valueOf(intValue - 1));
            }
        } else {
            throw new NullPointerException("Tried to decrement empty size, size: " + i + ", removed: " + d(bitmap) + ", this: " + this);
        }
    }

    @DexIgnore
    public final int f(int i) {
        Integer ceilingKey = this.c.ceilingKey(Integer.valueOf(i));
        return (ceilingKey == null || ceilingKey.intValue() > i * 8) ? i : ceilingKey.intValue();
    }

    @DexIgnore
    @Override // com.fossil.j51
    public Bitmap removeLast() {
        Bitmap e = this.b.e();
        if (e != null) {
            e(w81.b(e), e);
        }
        return e;
    }

    @DexIgnore
    public String toString() {
        return "SizeStrategy: groupedMap=" + this.b + ", sortedSizes=(" + this.c + ')';
    }
}
