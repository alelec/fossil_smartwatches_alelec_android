package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ig extends lp {
    @DexIgnore
    public /* final */ jo1[] C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ig(k5 k5Var, i60 i60, jo1[] jo1Arr, String str, int i) {
        super(k5Var, i60, yp.I, (i & 8) != 0 ? e.a("UUID.randomUUID().toString()") : str, false, 16);
        this.C = jo1Arr;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        boolean z = false;
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        for (jo1 jo1 : this.C) {
            cv1 iconConfig = jo1.getIconConfig();
            if (iconConfig != null) {
                qu1[] a2 = iconConfig.a();
                for (qu1 qu1 : a2) {
                    copyOnWriteArrayList.addIfAbsent(qu1);
                }
            }
        }
        Object[] array = copyOnWriteArrayList.toArray(new qu1[0]);
        if (array != null) {
            qu1[] qu1Arr = (qu1[]) array;
            if (qu1Arr.length == 0) {
                z = true;
            }
            if (!z) {
                try {
                    xa xaVar = xa.d;
                    ry1 ry1 = this.x.a().h().get(Short.valueOf(ob.NOTIFICATION.b));
                    if (ry1 == null) {
                        ry1 = hd0.y.d();
                    }
                    lp.h(this, new zj(this.w, this.x, yp.W, true, 1793, xaVar.h(qu1Arr, 1793, ry1), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), new qr(this), new gf(this), new uf(this), null, null, 48, null);
                } catch (sx1 e) {
                    d90.i.i(e);
                    l(nr.a(this.v, null, zq.INCOMPATIBLE_FIRMWARE, null, null, 13));
                }
            } else {
                H();
            }
        } else {
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(super.C(), jd0.U, px1.a(this.C));
    }

    @DexIgnore
    public final void H() {
        short b = ke.b.b(this.w.x, ob.NOTIFICATION_FILTER);
        try {
            r9 r9Var = r9.d;
            ry1 ry1 = this.x.a().h().get(Short.valueOf(ob.NOTIFICATION_FILTER.b));
            if (ry1 == null) {
                ry1 = hd0.y.d();
            }
            lp.h(this, new zj(this.w, this.x, yp.X, true, b, r9Var.a(b, ry1, this.C), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), new oq(this), new cr(this), null, null, null, 56, null);
        } catch (sx1 e) {
            d90.i.i(e);
            l(nr.a(this.v, null, zq.UNSUPPORTED_FORMAT, null, null, 13));
        }
    }
}
