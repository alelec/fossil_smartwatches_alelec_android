package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import com.fossil.zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bt2 extends zs2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle f;
    @DexIgnore
    public /* final */ /* synthetic */ zs2 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bt2(zs2 zs2, Bundle bundle) {
        super(zs2);
        this.g = zs2;
        this.f = bundle;
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void a() throws RemoteException {
        this.g.h.setConditionalUserProperty(this.f, this.b);
    }
}
