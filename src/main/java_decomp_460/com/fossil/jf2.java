package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jf2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ char[] f1753a; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    @DexIgnore
    public static /* final */ char[] b; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    @DexIgnore
    public static String a(byte[] bArr) {
        char[] cArr = new char[(bArr.length << 1)];
        int i = 0;
        for (byte b2 : bArr) {
            int i2 = b2 & 255;
            int i3 = i + 1;
            char[] cArr2 = b;
            cArr[i] = (char) cArr2[i2 >>> 4];
            i = i3 + 1;
            cArr[i3] = (char) cArr2[i2 & 15];
        }
        return new String(cArr);
    }

    @DexIgnore
    public static String b(byte[] bArr, boolean z) {
        int length = bArr.length;
        StringBuilder sb = new StringBuilder(length << 1);
        int i = 0;
        while (i < length && (!z || i != length - 1 || (bArr[i] & 255) != 0)) {
            sb.append(f1753a[(bArr[i] & 240) >>> 4]);
            sb.append(f1753a[bArr[i] & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY]);
            i++;
        }
        return sb.toString();
    }
}
