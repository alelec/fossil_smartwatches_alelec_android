package com.fossil;

import android.content.Intent;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.ib6;
import com.fossil.iq4;
import com.fossil.jn5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mb6 extends kb6 {
    @DexIgnore
    public LiveData<List<HybridPreset>> e; // = new MutableLiveData();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<HybridPreset> g; // = new ArrayList<>();
    @DexIgnore
    public HybridPreset h;
    @DexIgnore
    public MutableLiveData<String> i; // = this.n.K();
    @DexIgnore
    public int j; // = -1;
    @DexIgnore
    public int k; // = 2;
    @DexIgnore
    public cl7<Boolean, ? extends List<HybridPreset>> l; // = hl7.a(Boolean.FALSE, null);
    @DexIgnore
    public Boolean m;
    @DexIgnore
    public /* final */ PortfolioApp n;
    @DexIgnore
    public /* final */ lb6 o;
    @DexIgnore
    public /* final */ MicroAppRepository p;
    @DexIgnore
    public /* final */ HybridPresetRepository q;
    @DexIgnore
    public /* final */ ib6 r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$createNewPreset$1", f = "HomeHybridCustomizePresenter.kt", l = {291}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mb6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mb6$a$a")
        /* renamed from: com.fossil.mb6$a$a  reason: collision with other inner class name */
        public static final class C0152a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ HybridPreset $newPreset;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0152a(HybridPreset hybridPreset, qn7 qn7, a aVar) {
                super(2, qn7);
                this.$newPreset = hybridPreset;
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0152a aVar = new C0152a(this.$newPreset, qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((C0152a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    mb6 mb6 = this.this$0.this$0;
                    mb6.S(mb6.O() + 1);
                    HybridPresetRepository hybridPresetRepository = this.this$0.this$0.q;
                    HybridPreset hybridPreset = this.$newPreset;
                    this.L$0 = iv7;
                    this.label = 1;
                    if (hybridPresetRepository.upsertHybridPreset(hybridPreset, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(mb6 mb6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = mb6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                Iterator it = this.this$0.g.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    Object next = it.next();
                    if (ao7.a(((HybridPreset) next).isActive()).booleanValue()) {
                        obj2 = next;
                        break;
                    }
                }
                HybridPreset hybridPreset = (HybridPreset) obj2;
                if (hybridPreset != null) {
                    HybridPreset cloneFrom = HybridPreset.Companion.cloneFrom(hybridPreset);
                    dv7 i2 = this.this$0.i();
                    C0152a aVar = new C0152a(cloneFrom, null, this);
                    this.L$0 = iv7;
                    this.L$1 = hybridPreset;
                    this.L$2 = hybridPreset;
                    this.L$3 = cloneFrom;
                    this.label = 1;
                    if (eu7.g(i2, aVar, this) == d) {
                        return d;
                    }
                }
                return tl7.f3441a;
            } else if (i == 1) {
                HybridPreset hybridPreset2 = (HybridPreset) this.L$3;
                HybridPreset hybridPreset3 = (HybridPreset) this.L$2;
                HybridPreset hybridPreset4 = (HybridPreset) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.o.N3();
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.e<ib6.c, ib6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ HybridPreset f2351a;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset b;
        @DexIgnore
        public /* final */ /* synthetic */ mb6 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mb6$b$a$a")
            /* renamed from: com.fossil.mb6$b$a$a  reason: collision with other inner class name */
            public static final class C0153a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0153a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0153a aVar = new C0153a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0153a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        HybridPresetRepository hybridPresetRepository = this.this$0.this$0.c.q;
                        HybridPreset hybridPreset = this.this$0.this$0.f2351a;
                        if (hybridPreset != null) {
                            String id = hybridPreset.getId();
                            this.L$0 = iv7;
                            this.label = 1;
                            if (hybridPresetRepository.deletePresetById(id, this) == d) {
                                return d;
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    dv7 i2 = this.this$0.c.i();
                    C0153a aVar = new C0153a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    if (eu7.g(i2, aVar, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.c.o.w();
                this.this$0.c.o.O(this.this$0.c.O());
                b bVar = this.this$0;
                bVar.c.P(bVar.b);
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public b(HybridPreset hybridPreset, HybridPreset hybridPreset2, mb6 mb6, String str) {
            this.f2351a = hybridPreset;
            this.b = hybridPreset2;
            this.c = mb6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(ib6.a aVar) {
            pq7.c(aVar, "errorValue");
            this.c.o.w();
            int b2 = aVar.b();
            if (b2 == 1101 || b2 == 1112 || b2 == 1113) {
                List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(aVar.a());
                pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                lb6 lb6 = this.c.o;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    lb6.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.c.o.u();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ib6.c cVar) {
            pq7.c(cVar, "responseValue");
            xw7 unused = gu7.d(this.c.k(), null, null, new a(this, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $nextActivePresetId$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $preset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mb6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    HybridPresetRepository hybridPresetRepository = this.this$0.this$0.q;
                    String id = this.this$0.$preset.getId();
                    this.L$0 = iv7;
                    this.label = 1;
                    if (hybridPresetRepository.deletePresetById(id, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(HybridPreset hybridPreset, qn7 qn7, mb6 mb6, String str) {
            super(2, qn7);
            this.$preset = hybridPreset;
            this.this$0 = mb6;
            this.$nextActivePresetId$inlined = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.$preset, qn7, this.this$0, this.$nextActivePresetId$inlined);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(i2, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("mView.showDeleteSuccessfully ");
            sb.append(this.this$0.O() - 1);
            local.d("HomeHybridCustomizeFragment", sb.toString());
            this.this$0.o.O(this.this$0.O() - 1);
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$renameCurrentPreset$1", f = "HomeHybridCustomizePresenter.kt", l = {157}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $name;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mb6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ HybridPreset $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(HybridPreset hybridPreset, qn7 qn7, d dVar) {
                super(2, qn7);
                this.$it = hybridPreset;
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$it, qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    HybridPresetRepository hybridPresetRepository = this.this$0.this$0.q;
                    HybridPreset hybridPreset = this.$it;
                    this.L$0 = iv7;
                    this.label = 1;
                    if (hybridPresetRepository.upsertHybridPreset(hybridPreset, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(mb6 mb6, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = mb6;
            this.$presetId = str;
            this.$name = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$presetId, this.$name, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                Iterator it = this.this$0.g.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    Object next = it.next();
                    if (ao7.a(pq7.a(((HybridPreset) next).getId(), this.$presetId)).booleanValue()) {
                        obj2 = next;
                        break;
                    }
                }
                HybridPreset hybridPreset = (HybridPreset) obj2;
                HybridPreset clone = hybridPreset != null ? hybridPreset.clone() : null;
                if (clone != null) {
                    clone.setName(this.$name);
                    dv7 i2 = this.this$0.i();
                    a aVar = new a(clone, null, this);
                    this.L$0 = iv7;
                    this.L$1 = clone;
                    this.L$2 = clone;
                    this.label = 1;
                    if (eu7.g(i2, aVar, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                HybridPreset hybridPreset2 = (HybridPreset) this.L$2;
                HybridPreset hybridPreset3 = (HybridPreset) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements iq4.e<ib6.c, ib6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ mb6 f2352a;

        @DexIgnore
        public e(mb6 mb6) {
            this.f2352a = mb6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(ib6.a aVar) {
            pq7.c(aVar, "errorValue");
            this.f2352a.o.w();
            int b = aVar.b();
            if (b == 1101 || b == 1112 || b == 1113) {
                List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(aVar.a());
                pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                lb6 lb6 = this.f2352a.o;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    lb6.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.f2352a.o.u();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ib6.c cVar) {
            pq7.c(cVar, "responseValue");
            this.f2352a.o.w();
            this.f2352a.o.m0(0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1", f = "HomeHybridCustomizePresenter.kt", l = {63}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mb6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1$1", f = "HomeHybridCustomizePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends MicroApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends MicroApp>> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.p.getAllMicroApp(this.this$0.this$0.n.J());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements ls0<String> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ f f2353a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a<T> implements ls0<List<? extends HybridPreset>> {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ b f2354a;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mb6$f$b$a$a")
                /* renamed from: com.fossil.mb6$f$b$a$a  reason: collision with other inner class name */
                public static final class C0154a<T> implements Comparator<T> {
                    @DexIgnore
                    @Override // java.util.Comparator
                    public final int compare(T t, T t2) {
                        return mn7.c(Boolean.valueOf(t2.isActive()), Boolean.valueOf(t.isActive()));
                    }
                }

                @DexIgnore
                public a(b bVar) {
                    this.f2354a = bVar;
                }

                @DexIgnore
                /* renamed from: a */
                public final void onChanged(List<HybridPreset> list) {
                    if (list != null) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("HomeHybridCustomizePresenter", "onObserve on preset list change " + list);
                        List b0 = pm7.b0(list, new C0154a());
                        boolean a2 = pq7.a(b0, this.f2354a.f2353a.this$0.g) ^ true;
                        int itemCount = this.f2354a.f2353a.this$0.o.getItemCount();
                        if (a2 || b0.size() != itemCount - 1) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            local2.d("HomeHybridCustomizePresenter", "process change - " + a2 + " - itemCount: " + itemCount + " - sortedPresetSize: " + b0.size());
                            if (this.f2354a.f2353a.this$0.k == 2) {
                                this.f2354a.f2353a.this$0.N(b0);
                                return;
                            }
                            this.f2354a.f2353a.this$0.l = hl7.a(Boolean.TRUE, b0);
                            return;
                        }
                        ArrayList<T> arrayList = this.f2354a.f2353a.this$0.g;
                        ArrayList arrayList2 = new ArrayList(im7.m(arrayList, 10));
                        for (T t : arrayList) {
                            arrayList2.add(this.f2354a.f2353a.this$0.L(t));
                        }
                        if (!arrayList2.isEmpty()) {
                            boolean g = jn5.g(jn5.b, ((gz5) this.f2354a.f2353a.this$0.o).getContext(), ((m66) arrayList2.get(0)).b(), false, false, false, null, 56, null);
                            if (!pq7.a(Boolean.valueOf(g), this.f2354a.f2353a.this$0.m)) {
                                this.f2354a.f2353a.this$0.m = Boolean.valueOf(g);
                                this.f2354a.f2353a.this$0.o.J2(arrayList2);
                            }
                        }
                    }
                }
            }

            @DexIgnore
            public b(f fVar) {
                this.f2353a = fVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(String str) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeHybridCustomizePresenter", "on active serial change " + str + " mCurrentHomeTab " + this.f2353a.this$0.k);
                if (TextUtils.isEmpty(str) || FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
                    this.f2353a.this$0.o.r(true);
                    return;
                }
                mb6 mb6 = this.f2353a.this$0;
                HybridPresetRepository hybridPresetRepository = mb6.q;
                if (str != null) {
                    mb6.e = hybridPresetRepository.getPresetListAsLiveData(str);
                    this.f2353a.this$0.e.h((LifecycleOwner) this.f2353a.this$0.o, new a(this));
                    this.f2353a.this$0.o.r(false);
                    return;
                }
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(mb6 mb6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = mb6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0089  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x002c  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 1
                java.lang.Object r2 = com.fossil.yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0056
                if (r0 != r6) goto L_0x004e
                java.lang.Object r0 = r7.L$1
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                java.lang.Object r1 = r7.L$0
                com.fossil.iv7 r1 = (com.fossil.iv7) r1
                com.fossil.el7.b(r8)
                r1 = r8
                r2 = r0
            L_0x0018:
                r0 = r1
                java.util.Collection r0 = (java.util.Collection) r0
                r2.addAll(r0)
            L_0x001e:
                com.fossil.mb6 r0 = r7.this$0
                androidx.lifecycle.MutableLiveData r1 = com.fossil.mb6.D(r0)
                com.fossil.mb6 r0 = r7.this$0
                com.fossil.lb6 r0 = com.fossil.mb6.F(r0)
                if (r0 == 0) goto L_0x0089
                com.fossil.gz5 r0 = (com.fossil.gz5) r0
                com.fossil.mb6$f$b r2 = new com.fossil.mb6$f$b
                r2.<init>(r7)
                r1.h(r0, r2)
                com.fossil.mb6 r0 = r7.this$0
                com.fossil.ib6 r0 = com.fossil.mb6.E(r0)
                r0.r()
                com.fossil.wq5 r0 = com.fossil.wq5.d
                com.misfit.frameworks.buttonservice.communite.CommunicateMode[] r1 = new com.misfit.frameworks.buttonservice.communite.CommunicateMode[r6]
                r2 = 0
                com.misfit.frameworks.buttonservice.communite.CommunicateMode r3 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_LINK_MAPPING
                r1[r2] = r3
                r0.g(r1)
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x004d:
                return r0
            L_0x004e:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0056:
                com.fossil.el7.b(r8)
                com.fossil.iv7 r1 = r7.p$
                com.fossil.mb6 r0 = r7.this$0
                java.util.ArrayList r0 = com.fossil.mb6.u(r0)
                boolean r0 = r0.isEmpty()
                if (r0 == 0) goto L_0x001e
                com.fossil.mb6 r0 = r7.this$0
                java.util.ArrayList r0 = com.fossil.mb6.u(r0)
                com.fossil.mb6 r3 = r7.this$0
                com.fossil.dv7 r3 = com.fossil.mb6.v(r3)
                com.fossil.mb6$f$a r4 = new com.fossil.mb6$f$a
                r5 = 0
                r4.<init>(r7, r5)
                r7.L$0 = r1
                r7.L$1 = r0
                r7.label = r6
                java.lang.Object r1 = com.fossil.eu7.g(r3, r4, r7)
                if (r1 != r2) goto L_0x0087
                r0 = r2
                goto L_0x004d
            L_0x0087:
                r2 = r0
                goto L_0x0018
            L_0x0089:
                com.fossil.il7 r0 = new com.fossil.il7
                java.lang.String r1 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment"
                r0.<init>(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.mb6.f.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public mb6(PortfolioApp portfolioApp, lb6 lb6, MicroAppRepository microAppRepository, HybridPresetRepository hybridPresetRepository, ib6 ib6, on5 on5) {
        pq7.c(portfolioApp, "mApp");
        pq7.c(lb6, "mView");
        pq7.c(microAppRepository, "mMicroAppRepository");
        pq7.c(hybridPresetRepository, "mHybridPresetRepository");
        pq7.c(ib6, "mSetHybridPresetToWatchUseCase");
        pq7.c(on5, "mSharedPreferencesManager");
        this.n = portfolioApp;
        this.o = lb6;
        this.p = microAppRepository;
        this.q = hybridPresetRepository;
        this.r = ib6;
    }

    @DexIgnore
    public final m66 L(HybridPreset hybridPreset) {
        T t;
        ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
        ArrayList arrayList = new ArrayList();
        List<jn5.a> c2 = hl5.f1493a.c(hybridPreset);
        Iterator<HybridPresetAppSetting> it = buttons.iterator();
        while (it.hasNext()) {
            HybridPresetAppSetting next = it.next();
            String component1 = next.component1();
            String component2 = next.component2();
            Iterator<T> it2 = this.f.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t = null;
                    break;
                }
                T next2 = it2.next();
                if (pq7.a(next2.getId(), component2)) {
                    t = next2;
                    break;
                }
            }
            T t2 = t;
            if (t2 != null) {
                String id = t2.getId();
                String icon = t2.getIcon();
                if (icon == null) {
                    icon = "";
                }
                arrayList.add(new n66(id, icon, um5.d(PortfolioApp.h0.c(), t2.getNameKey(), t2.getName()), component1, null, 16, null));
            }
        }
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "convertPresetToHybridPresetConfigWrapper");
        String id2 = hybridPreset.getId();
        String name = hybridPreset.getName();
        if (name == null) {
            name = "";
        }
        return new m66(id2, name, arrayList, c2, hybridPreset.isActive());
    }

    @DexIgnore
    public final void M(String str) {
        T t;
        T t2;
        jn5 jn5 = jn5.b;
        lb6 lb6 = this.o;
        if (lb6 == null) {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } else if (jn5.c(jn5, ((gz5) lb6).getContext(), jn5.a.SET_BLE_COMMAND, false, false, false, null, 60, null)) {
            List<HybridPreset> e2 = this.e.e();
            if (e2 != null) {
                e2.isEmpty();
            }
            Iterator<T> it = this.g.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (pq7.a(next.getId(), str)) {
                    t = next;
                    break;
                }
            }
            T t3 = t;
            if (t3 != null) {
                Iterator<T> it2 = this.g.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next2 = it2.next();
                    if (next2.isActive()) {
                        t2 = next2;
                        break;
                    }
                }
                T t4 = t2;
                this.o.y();
                FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "delete activePreset " + ((Object) t4) + " set preset " + ((Object) t3) + " as active first");
                this.r.e(new ib6.b(t3), new b(t4, t3, this, str));
            }
        }
    }

    @DexIgnore
    public final void N(List<HybridPreset> list) {
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "doShowingPreset");
        this.g.clear();
        this.g.addAll(list);
        int size = this.g.size();
        int i2 = this.j;
        if (size > i2 && i2 > 0) {
            this.h = this.g.get(i2);
        }
        U();
    }

    @DexIgnore
    public final int O() {
        return this.j;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean P(com.portfolio.platform.data.model.room.microapp.HybridPreset r7) {
        /*
            r6 = this;
            r2 = 1
            if (r7 != 0) goto L_0x0005
            r0 = r2
        L_0x0004:
            return r0
        L_0x0005:
            java.util.ArrayList r0 = r7.getButtons()
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.util.Iterator r4 = r0.iterator()
        L_0x0012:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x002f
            java.lang.Object r1 = r4.next()
            r0 = r1
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r0 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r0
            com.fossil.bl5 r5 = com.fossil.bl5.c
            java.lang.String r0 = r0.getAppId()
            boolean r0 = r5.c(r0)
            if (r0 == 0) goto L_0x0012
            r3.add(r1)
            goto L_0x0012
        L_0x002f:
            r1 = 0
            boolean r0 = r3.isEmpty()
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x007f
            java.util.Iterator r3 = r3.iterator()
        L_0x003c:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x007f
            java.lang.Object r0 = r3.next()
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r0 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r0
            com.fossil.bl5 r4 = com.fossil.bl5.c
            java.lang.String r5 = r0.getAppId()
            boolean r4 = r4.e(r5)
            if (r4 != 0) goto L_0x003c
        L_0x0054:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "setPresetToWatch missingPermissionComp "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r4 = "HomeHybridCustomizePresenter"
            java.lang.String r3 = r3.toString()
            r1.d(r4, r3)
            if (r0 == 0) goto L_0x007d
            com.fossil.lb6 r1 = r6.o
            java.lang.String r0 = r0.getAppId()
            r1.C5(r0)
            r0 = 0
            goto L_0x0004
        L_0x007d:
            r0 = r2
            goto L_0x0004
        L_0x007f:
            r0 = r1
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mb6.P(com.portfolio.platform.data.model.room.microapp.HybridPreset):boolean");
    }

    @DexIgnore
    public void Q(int i2, int i3, Intent intent) {
        if (i2 == 100 && i3 == -1) {
            this.o.c0(0);
        }
    }

    @DexIgnore
    public void R(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "onHomeTabChange - tab: " + i2);
        this.k = i2;
        if (i2 == 2 && this.l.getFirst().booleanValue()) {
            List<HybridPreset> list = (List) this.l.getSecond();
            if (list != null) {
                N(list);
            }
            this.l = hl7.a(Boolean.FALSE, null);
        }
    }

    @DexIgnore
    public final void S(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public void T() {
        this.o.M5(this);
    }

    @DexIgnore
    public final void U() {
        ArrayList<HybridPreset> arrayList = this.g;
        ArrayList arrayList2 = new ArrayList(im7.m(arrayList, 10));
        Iterator<T> it = arrayList.iterator();
        while (it.hasNext()) {
            arrayList2.add(L(it.next()));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "showPresets - size=" + this.g.size() + " uiData " + arrayList2);
        if (this.m == null && !this.g.isEmpty() && !arrayList2.isEmpty()) {
            jn5 jn5 = jn5.b;
            lb6 lb6 = this.o;
            if (lb6 != null) {
                this.m = Boolean.valueOf(jn5.g(jn5, ((gz5) lb6).getContext(), ((m66) arrayList2.get(0)).b(), false, false, false, null, 56, null));
            } else {
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
        }
        this.o.J2(arrayList2);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        xw7 unused = gu7.d(k(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        try {
            LiveData<List<HybridPreset>> liveData = this.e;
            lb6 lb6 = this.o;
            if (lb6 != null) {
                liveData.n((gz5) lb6);
                this.i.n((LifecycleOwner) this.o);
                this.r.w();
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "Exception when remove observer.");
        }
    }

    @DexIgnore
    @Override // com.fossil.kb6
    public void n(int i2) {
        if (this.g.size() > i2) {
            this.j = i2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "changePresetPosition " + this.j);
            HybridPreset hybridPreset = this.g.get(this.j);
            this.h = hybridPreset;
            if (hybridPreset != null) {
                this.o.j2(hybridPreset.isActive());
                return;
            }
            return;
        }
        this.o.j2(false);
    }

    @DexIgnore
    @Override // com.fossil.kb6
    public void o() {
        xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.kb6
    public void p(String str) {
        pq7.c(str, "nextActivePresetId");
        HybridPreset hybridPreset = this.h;
        if (hybridPreset == null) {
            return;
        }
        if (hybridPreset.isActive()) {
            M(str);
        } else {
            xw7 unused = gu7.d(k(), null, null, new c(hybridPreset, null, this, str), 3, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.kb6
    public void q(String str, String str2) {
        pq7.c(str, "name");
        pq7.c(str2, "presetId");
        xw7 unused = gu7.d(k(), null, null, new d(this, str2, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.kb6
    public void r() {
        HybridPreset hybridPreset;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "setPresetToWatch mCurrentPreset=" + this.h);
        jn5 jn5 = jn5.b;
        lb6 lb6 = this.o;
        if (lb6 == null) {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } else if (jn5.c(jn5, ((gz5) lb6).getContext(), jn5.a.SET_BLE_COMMAND, false, false, false, null, 60, null) && (hybridPreset = this.h) != null) {
            this.o.y();
            this.r.e(new ib6.b(hybridPreset), new e(this));
        }
    }
}
