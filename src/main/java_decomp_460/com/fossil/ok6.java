package com.fossil;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ok6 extends RecyclerView.l {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Drawable f2692a;
    @DexIgnore
    public /* final */ Rect b;
    @DexIgnore
    public int c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public ok6(int i, boolean z, boolean z2) {
        this.c = i;
        this.d = z;
        this.e = z2;
        this.b = new Rect();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ok6(int i, boolean z, boolean z2, int i2, kq7 kq7) {
        this((i2 & 1) != 0 ? 1 : i, (i2 & 2) != 0 ? false : z, (i2 & 4) != 0 ? false : z2);
    }

    @DexIgnore
    public final void f(Canvas canvas, RecyclerView recyclerView) {
        int height;
        int i;
        canvas.save();
        if (recyclerView.getClipToPadding()) {
            i = recyclerView.getPaddingTop();
            height = recyclerView.getHeight() - recyclerView.getPaddingBottom();
            canvas.clipRect(recyclerView.getPaddingLeft(), i, recyclerView.getWidth() - recyclerView.getPaddingRight(), height);
        } else {
            height = recyclerView.getHeight();
            i = 0;
        }
        Drawable drawable = this.f2692a;
        if (drawable != null) {
            int childCount = recyclerView.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = recyclerView.getChildAt(i2);
                RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                if (layoutManager != null) {
                    layoutManager.Q(childAt, this.b);
                    int i3 = this.b.right;
                    pq7.b(childAt, "child");
                    int round = Math.round(childAt.getTranslationX()) + i3;
                    drawable.setBounds(round - drawable.getIntrinsicWidth(), i, round, height);
                    drawable.draw(canvas);
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }
        canvas.restore();
    }

    @DexIgnore
    public final void g(Canvas canvas, RecyclerView recyclerView) {
        int width;
        int i;
        canvas.save();
        if (recyclerView.getClipToPadding()) {
            i = recyclerView.getPaddingLeft();
            width = recyclerView.getWidth() - recyclerView.getPaddingRight();
            canvas.clipRect(i, recyclerView.getPaddingTop(), width, recyclerView.getHeight() - recyclerView.getPaddingBottom());
        } else {
            width = recyclerView.getWidth();
            i = 0;
        }
        Drawable drawable = this.f2692a;
        if (drawable != null) {
            int childCount = recyclerView.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = recyclerView.getChildAt(i2);
                recyclerView.getDecoratedBoundsWithMargins(childAt, this.b);
                int i3 = this.b.bottom;
                pq7.b(childAt, "child");
                int round = Math.round(childAt.getTranslationY()) + i3;
                drawable.setBounds(i, round - drawable.getIntrinsicHeight(), width, round);
                drawable.draw(canvas);
            }
        }
        canvas.restore();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.l
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        pq7.c(rect, "outRect");
        pq7.c(view, "view");
        pq7.c(recyclerView, "parent");
        pq7.c(state, "state");
        Drawable drawable = this.f2692a;
        if (drawable != null) {
            RecyclerView.g adapter = recyclerView.getAdapter();
            if (adapter != null) {
                int itemCount = adapter.getItemCount();
                int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DashboardItemDecoration", "getItemOffsets - itemCount=" + itemCount + ", position=" + childAdapterPosition);
                if ((!this.d && childAdapterPosition == 0) || (!this.e && childAdapterPosition == itemCount - 1)) {
                    rect.set(0, 0, 0, 0);
                } else if (this.c == 1) {
                    rect.set(0, 0, 0, drawable.getIntrinsicHeight());
                } else {
                    rect.set(0, 0, drawable.getIntrinsicWidth(), 0);
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            rect.set(0, 0, 0, 0);
        }
    }

    @DexIgnore
    public final void h(Drawable drawable) {
        pq7.c(drawable, ResourceManager.DRAWABLE);
        this.f2692a = drawable;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.l
    public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        pq7.c(canvas, "c");
        pq7.c(recyclerView, "parent");
        pq7.c(state, "state");
        if (recyclerView.getLayoutManager() != null && this.f2692a != null) {
            if (this.c == 1) {
                g(canvas, recyclerView);
            } else {
                f(canvas, recyclerView);
            }
        }
    }
}
