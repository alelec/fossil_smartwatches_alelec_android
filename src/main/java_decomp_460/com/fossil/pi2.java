package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pi2 implements Parcelable.Creator<vh2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ vh2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        int i2 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                str3 = ad2.f(parcel, t);
            } else if (l == 2) {
                str2 = ad2.f(parcel, t);
            } else if (l == 4) {
                str = ad2.f(parcel, t);
            } else if (l == 5) {
                i2 = ad2.v(parcel, t);
            } else if (l != 6) {
                ad2.B(parcel, t);
            } else {
                i = ad2.v(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new vh2(str3, str2, str, i2, i);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ vh2[] newArray(int i) {
        return new vh2[i];
    }
}
