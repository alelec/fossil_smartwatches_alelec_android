package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nt7 {
    @DexIgnore
    public static final <T> void a(Appendable appendable, T t, rp7<? super T, ? extends CharSequence> rp7) {
        pq7.c(appendable, "$this$appendElement");
        if (rp7 != null) {
            appendable.append((CharSequence) rp7.invoke(t));
            return;
        }
        if (t != null ? t instanceof CharSequence : true) {
            appendable.append(t);
        } else if (t instanceof Character) {
            appendable.append(t.charValue());
        } else {
            appendable.append(String.valueOf(t));
        }
    }
}
