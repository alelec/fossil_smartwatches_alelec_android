package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ts5;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ss5 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ArrayList<ts5> f3296a; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<ys5> b;
    @DexIgnore
    public b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public TextView f3297a;
        @DexIgnore
        public ImageView b;
        @DexIgnore
        public RecyclerView c;
        @DexIgnore
        public /* final */ /* synthetic */ ss5 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ss5$a$a")
        /* renamed from: com.fossil.ss5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0222a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public View$OnClickListenerC0222a(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.e();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public b(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.e();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ss5 ss5, View view) {
            super(view);
            pq7.c(view, "itemView");
            this.d = ss5;
            View findViewById = view.findViewById(2131363307);
            if (findViewById != null) {
                TextView textView = (TextView) findViewById;
                textView.setOnClickListener(new View$OnClickListenerC0222a(this));
                this.f3297a = textView;
                View findViewById2 = view.findViewById(2131362663);
                if (findViewById2 != null) {
                    ImageView imageView = (ImageView) findViewById2;
                    imageView.setOnClickListener(new b(this));
                    this.b = imageView;
                    View findViewById3 = view.findViewById(2131363033);
                    if (findViewById3 != null) {
                        this.c = (RecyclerView) findViewById3;
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        public final ImageView b() {
            return this.b;
        }

        @DexIgnore
        public final RecyclerView c() {
            return this.c;
        }

        @DexIgnore
        public final TextView d() {
            return this.f3297a;
        }

        @DexIgnore
        public final void e() {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                ArrayList<ys5> h = this.d.h();
                if (h != null) {
                    boolean c2 = h.get(adapterPosition).c();
                    ArrayList<ys5> h2 = this.d.h();
                    if (h2 != null) {
                        h2.get(adapterPosition).d(!c2);
                        this.d.notifyItemChanged(adapterPosition);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str, int i, int i2, Object obj, Bundle bundle);

        @DexIgnore
        void b(String str, int i, int i2, Object obj, Bundle bundle);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ts5.e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ss5 f3298a;

        @DexIgnore
        public c(ys5 ys5, ss5 ss5) {
            this.f3298a = ss5;
        }

        @DexIgnore
        @Override // com.fossil.ts5.e
        public void a(String str, int i, int i2, Object obj, Bundle bundle) {
            pq7.c(str, "tagName");
            b g = this.f3298a.g();
            if (g != null) {
                g.a(str, i, i2, obj, bundle);
            }
        }

        @DexIgnore
        @Override // com.fossil.ts5.e
        public void b(String str, int i, int i2, Object obj, Bundle bundle) {
            pq7.c(str, "tagName");
            b g = this.f3298a.g();
            if (g != null) {
                g.b(str, i, i2, obj, bundle);
            }
        }
    }

    @DexIgnore
    public final b g() {
        return this.c;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        ArrayList<ys5> arrayList = this.b;
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    @DexIgnore
    public final ArrayList<ys5> h() {
        return this.b;
    }

    @DexIgnore
    /* renamed from: i */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        TextView d = aVar.d();
        ArrayList<ys5> arrayList = this.b;
        if (arrayList != null) {
            d.setText(arrayList.get(i).b());
            RecyclerView c2 = aVar.c();
            c2.setLayoutManager(new LinearLayoutManager(c2.getContext()));
            ts5 ts5 = this.f3296a.get(i);
            pq7.b(ts5, "listDebugChildAdapter[position]");
            ts5 ts52 = ts5;
            ts52.l(i);
            c2.setAdapter(ts52);
            ArrayList<ys5> arrayList2 = this.b;
            if (arrayList2 == null) {
                pq7.i();
                throw null;
            } else if (arrayList2.get(i).c()) {
                aVar.b().setImageResource(2131230996);
                aVar.c().setVisibility(0);
            } else {
                aVar.b().setImageResource(2131230995);
                aVar.c().setVisibility(8);
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: j */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558679, viewGroup, false);
        pq7.b(inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void k(ArrayList<ys5> arrayList) {
        this.f3296a.clear();
        if (arrayList != null) {
            for (T t : arrayList) {
                ArrayList<ts5> arrayList2 = this.f3296a;
                ts5 ts5 = new ts5();
                ts5.k(new c(t, this));
                ts5.j(t.a());
                arrayList2.add(ts5);
            }
        }
        this.b = arrayList;
    }

    @DexIgnore
    public final void l(b bVar) {
        pq7.c(bVar, "itemClickListener");
        this.c = bVar;
    }
}
