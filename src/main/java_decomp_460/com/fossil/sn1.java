package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum sn1 {
    TWELVE(0),
    TWENTY_FOUR(1);
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final sn1 a(int i) {
            sn1[] values = sn1.values();
            for (sn1 sn1 : values) {
                if (sn1.a() == i) {
                    return sn1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public sn1(int i) {
        this.b = i;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }
}
