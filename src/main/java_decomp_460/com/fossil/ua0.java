package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ua0 implements Parcelable.Creator<va0> {
    @DexIgnore
    public /* synthetic */ ua0(kq7 kq7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public va0 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            pq7.b(readString, "parcel.readString()!!");
            aa0 valueOf = aa0.valueOf(readString);
            parcel.setDataPosition(0);
            switch (ta0.f3385a[valueOf.ordinal()]) {
                case 1:
                    return ia0.CREATOR.a(parcel);
                case 2:
                    return ka0.CREATOR.a(parcel);
                case 3:
                    return fb0.CREATOR.a(parcel);
                case 4:
                    return za0.CREATOR.a(parcel);
                case 5:
                    return oa0.CREATOR.a(parcel);
                case 6:
                    return xa0.CREATOR.a(parcel);
                case 7:
                    return bb0.CREATOR.a(parcel);
                case 8:
                    return qa0.CREATOR.a(parcel);
                case 9:
                    return ma0.CREATOR.a(parcel);
                case 10:
                    return ga0.CREATOR.a(parcel);
                case 11:
                    return db0.CREATOR.a(parcel);
                case 12:
                    return sa0.CREATOR.a(parcel);
                default:
                    throw new al7();
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public va0[] newArray(int i) {
        return new va0[i];
    }
}
