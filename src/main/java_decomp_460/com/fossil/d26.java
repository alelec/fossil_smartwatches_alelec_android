package com.fossil;

import com.fossil.iq4;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d26 extends iq4<b, c, a> {
    @DexIgnore
    public static /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.a {
        @DexIgnore
        public a(String str) {
            pq7.c(str, "message");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<ContactGroup> f723a;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends com.fossil.wearables.fsl.contact.ContactGroup> */
        /* JADX WARN: Multi-variable type inference failed */
        public c(List<? extends ContactGroup> list) {
            pq7.c(list, "contactGroups");
            this.f723a = list;
        }

        @DexIgnore
        public final List<ContactGroup> a() {
            return this.f723a;
        }
    }

    /*
    static {
        String simpleName = d26.class.getSimpleName();
        pq7.b(simpleName, "GetAllContactGroups::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return d;
    }

    @DexIgnore
    /* renamed from: m */
    public Object k(b bVar, qn7<Object> qn7) {
        FLogger.INSTANCE.getLocal().d(d, "executeUseCase");
        List<ContactGroup> allContactGroups = mn5.p.a().d().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        return allContactGroups != null ? new c(allContactGroups) : new a("Get all contact group failed");
    }
}
