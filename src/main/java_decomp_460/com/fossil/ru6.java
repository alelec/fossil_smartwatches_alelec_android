package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ru6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ yk7 f3168a; // = zk7.a(a.INSTANCE);
    @DexIgnore
    public static /* final */ b b; // = new b(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements gp7<ru6> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final ru6 invoke() {
            return c.b.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ru6 a() {
            yk7 yk7 = ru6.f3168a;
            b bVar = ru6.b;
            return (ru6) yk7.getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ ru6 f3169a; // = new ru6();
        @DexIgnore
        public static /* final */ c b; // = new c();

        @DexIgnore
        public final ru6 a() {
            return f3169a;
        }
    }

    /*
    static {
        pq7.b(qu6.INSTANCE.getClass().getSimpleName(), "InAppNotificationUtils::\u2026lass.javaClass.simpleName");
    }
    */
}
