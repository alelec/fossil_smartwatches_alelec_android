package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.diana.preset.Background;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p05 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<Background> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<Background> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends TypeToken<ArrayList<RingStyleItem>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends TypeToken<ArrayList<RingStyleItem>> {
    }

    @DexIgnore
    public final String a(Background background) {
        if (background == null) {
            return "";
        }
        String u = new Gson().u(background, new a().getType());
        pq7.b(u, "Gson().toJson(background, type)");
        return u;
    }

    @DexIgnore
    public final Background b(String str) {
        pq7.c(str, "json");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return (Background) new Gson().l(str, new b().getType());
    }

    @DexIgnore
    public final ArrayList<RingStyleItem> c(String str) {
        pq7.c(str, "json");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList<>();
        }
        Object l = new Gson().l(str, new c().getType());
        pq7.b(l, "Gson().fromJson(json, type)");
        return (ArrayList) l;
    }

    @DexIgnore
    public final String d(ArrayList<RingStyleItem> arrayList) {
        if (ff2.a(arrayList)) {
            return "";
        }
        String u = new Gson().u(arrayList, new d().getType());
        pq7.b(u, "Gson().toJson(ringStyles, type)");
        return u;
    }
}
