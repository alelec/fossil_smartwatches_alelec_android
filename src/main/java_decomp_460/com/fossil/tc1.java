package com.fossil;

import com.fossil.af1;
import com.fossil.ua1;
import com.fossil.uc1;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tc1<Transcode> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<af1.a<?>> f3393a; // = new ArrayList();
    @DexIgnore
    public /* final */ List<mb1> b; // = new ArrayList();
    @DexIgnore
    public qa1 c;
    @DexIgnore
    public Object d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Class<?> g;
    @DexIgnore
    public uc1.e h;
    @DexIgnore
    public ob1 i;
    @DexIgnore
    public Map<Class<?>, sb1<?>> j;
    @DexIgnore
    public Class<Transcode> k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public mb1 n;
    @DexIgnore
    public sa1 o;
    @DexIgnore
    public wc1 p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;

    @DexIgnore
    public void a() {
        this.c = null;
        this.d = null;
        this.n = null;
        this.g = null;
        this.k = null;
        this.i = null;
        this.o = null;
        this.j = null;
        this.p = null;
        this.f3393a.clear();
        this.l = false;
        this.b.clear();
        this.m = false;
    }

    @DexIgnore
    public od1 b() {
        return this.c.b();
    }

    @DexIgnore
    public List<mb1> c() {
        if (!this.m) {
            this.m = true;
            this.b.clear();
            List<af1.a<?>> g2 = g();
            int size = g2.size();
            for (int i2 = 0; i2 < size; i2++) {
                af1.a<?> aVar = g2.get(i2);
                if (!this.b.contains(aVar.f261a)) {
                    this.b.add(aVar.f261a);
                }
                for (int i3 = 0; i3 < aVar.b.size(); i3++) {
                    if (!this.b.contains(aVar.b.get(i3))) {
                        this.b.add(aVar.b.get(i3));
                    }
                }
            }
        }
        return this.b;
    }

    @DexIgnore
    public be1 d() {
        return this.h.a();
    }

    @DexIgnore
    public wc1 e() {
        return this.p;
    }

    @DexIgnore
    public int f() {
        return this.f;
    }

    @DexIgnore
    public List<af1.a<?>> g() {
        if (!this.l) {
            this.l = true;
            this.f3393a.clear();
            List i2 = this.c.h().i(this.d);
            int size = i2.size();
            for (int i3 = 0; i3 < size; i3++) {
                af1.a<?> b2 = ((af1) i2.get(i3)).b(this.d, this.e, this.f, this.i);
                if (b2 != null) {
                    this.f3393a.add(b2);
                }
            }
        }
        return this.f3393a;
    }

    @DexIgnore
    public <Data> gd1<Data, ?, Transcode> h(Class<Data> cls) {
        return this.c.h().h(cls, this.g, this.k);
    }

    @DexIgnore
    public Class<?> i() {
        return this.d.getClass();
    }

    @DexIgnore
    public List<af1<File, ?>> j(File file) throws ua1.c {
        return this.c.h().i(file);
    }

    @DexIgnore
    public ob1 k() {
        return this.i;
    }

    @DexIgnore
    public sa1 l() {
        return this.o;
    }

    @DexIgnore
    public List<Class<?>> m() {
        return this.c.h().j(this.d.getClass(), this.g, this.k);
    }

    @DexIgnore
    public <Z> rb1<Z> n(id1<Z> id1) {
        return this.c.h().k(id1);
    }

    @DexIgnore
    public mb1 o() {
        return this.n;
    }

    @DexIgnore
    public <X> jb1<X> p(X x) throws ua1.e {
        return this.c.h().m(x);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.lang.Class<Transcode>, java.lang.Class<?> */
    public Class<?> q() {
        return (Class<Transcode>) this.k;
    }

    @DexIgnore
    public <Z> sb1<Z> r(Class<Z> cls) {
        sb1<Z> sb1 = (sb1<Z>) this.j.get(cls);
        if (sb1 == null) {
            Iterator<Map.Entry<Class<?>, sb1<?>>> it = this.j.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry<Class<?>, sb1<?>> next = it.next();
                if (next.getKey().isAssignableFrom(cls)) {
                    sb1 = (sb1<Z>) next.getValue();
                    break;
                }
            }
        }
        if (sb1 != null) {
            return sb1;
        }
        if (!this.j.isEmpty() || !this.q) {
            return tf1.c();
        }
        throw new IllegalArgumentException("Missing transformation for " + cls + ". If you wish to ignore unknown resource types, use the optional transformation methods.");
    }

    @DexIgnore
    public int s() {
        return this.e;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Class<?> */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean t(Class<?> cls) {
        return h(cls) != null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: java.lang.Class<R> */
    /* JADX WARN: Multi-variable type inference failed */
    public <R> void u(qa1 qa1, Object obj, mb1 mb1, int i2, int i3, wc1 wc1, Class<?> cls, Class<R> cls2, sa1 sa1, ob1 ob1, Map<Class<?>, sb1<?>> map, boolean z, boolean z2, uc1.e eVar) {
        this.c = qa1;
        this.d = obj;
        this.n = mb1;
        this.e = i2;
        this.f = i3;
        this.p = wc1;
        this.g = cls;
        this.h = eVar;
        this.k = cls2;
        this.o = sa1;
        this.i = ob1;
        this.j = map;
        this.q = z;
        this.r = z2;
    }

    @DexIgnore
    public boolean v(id1<?> id1) {
        return this.c.h().n(id1);
    }

    @DexIgnore
    public boolean w() {
        return this.r;
    }

    @DexIgnore
    public boolean x(mb1 mb1) {
        List<af1.a<?>> g2 = g();
        int size = g2.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (g2.get(i2).f261a.equals(mb1)) {
                return true;
            }
        }
        return false;
    }
}
