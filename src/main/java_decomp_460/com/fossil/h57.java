package com.fossil;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h57 {
    @DexIgnore
    public static final String a(Date date) {
        pq7.c(date, "$this$print");
        try {
            String date2 = date.toString();
            pq7.b(date2, "this.toString()");
            return date2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("ViewExt", "Date.toString - e=" + e);
            String format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US).format(date);
            pq7.b(format, "SimpleDateFormat(DateHel\u2026, Locale.US).format(this)");
            return format;
        }
    }

    @DexIgnore
    public static final void b(TextView textView, int i) {
        Drawable mutate;
        pq7.c(textView, "$this$setDrawableTintColor");
        Drawable[] compoundDrawablesRelative = textView.getCompoundDrawablesRelative();
        pq7.b(compoundDrawablesRelative, "this.compoundDrawablesRelative");
        if (compoundDrawablesRelative != null) {
            if (!(compoundDrawablesRelative.length == 0)) {
                for (Drawable drawable : compoundDrawablesRelative) {
                    if (!(drawable == null || (mutate = drawable.mutate()) == null)) {
                        mutate.setColorFilter(new PorterDuffColorFilter(i, PorterDuff.Mode.SRC_IN));
                    }
                }
            }
        }
    }

    @DexIgnore
    public static final void c(TabLayout tabLayout, boolean z) {
        View childAt;
        pq7.c(tabLayout, "$this$subEnabled");
        if (tabLayout.getChildCount() > 0 && (childAt = tabLayout.getChildAt(0)) != null && (childAt instanceof ViewGroup)) {
            ViewGroup viewGroup = (ViewGroup) childAt;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt2 = viewGroup.getChildAt(i);
                if (childAt2 != null) {
                    childAt2.setEnabled(z);
                }
            }
        }
    }
}
