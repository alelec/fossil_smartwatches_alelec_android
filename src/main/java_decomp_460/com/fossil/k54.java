package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class k54 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends l54 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Charset f1863a;

        @DexIgnore
        public a(Charset charset) {
            i14.l(charset);
            this.f1863a = charset;
        }

        @DexIgnore
        @Override // com.fossil.l54
        public Reader a() throws IOException {
            return new InputStreamReader(k54.this.b(), this.f1863a);
        }

        @DexIgnore
        public String toString() {
            return k54.this.toString() + ".asCharSource(" + this.f1863a + ")";
        }
    }

    @DexIgnore
    public l54 a(Charset charset) {
        return new a(charset);
    }

    @DexIgnore
    public abstract InputStream b() throws IOException;
}
