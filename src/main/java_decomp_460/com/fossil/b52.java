package com.fossil;

import android.content.Context;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b52 {
    @DexIgnore
    public static b52 d;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public o42 f396a;
    @DexIgnore
    public GoogleSignInAccount b;
    @DexIgnore
    public GoogleSignInOptions c; // = this.f396a.d();

    @DexIgnore
    public b52(Context context) {
        o42 b2 = o42.b(context);
        this.f396a = b2;
        this.b = b2.c();
    }

    @DexIgnore
    public static b52 c(Context context) {
        b52 d2;
        synchronized (b52.class) {
            try {
                d2 = d(context.getApplicationContext());
            } catch (Throwable th) {
                throw th;
            }
        }
        return d2;
    }

    @DexIgnore
    public static b52 d(Context context) {
        b52 b52;
        synchronized (b52.class) {
            try {
                if (d == null) {
                    d = new b52(context);
                }
                b52 = d;
            } catch (Throwable th) {
                throw th;
            }
        }
        return b52;
    }

    @DexIgnore
    public final void a() {
        synchronized (this) {
            this.f396a.a();
            this.b = null;
        }
    }

    @DexIgnore
    public final void b(GoogleSignInOptions googleSignInOptions, GoogleSignInAccount googleSignInAccount) {
        synchronized (this) {
            this.f396a.f(googleSignInAccount, googleSignInOptions);
            this.b = googleSignInAccount;
            this.c = googleSignInOptions;
        }
    }

    @DexIgnore
    public final GoogleSignInAccount e() {
        GoogleSignInAccount googleSignInAccount;
        synchronized (this) {
            googleSignInAccount = this.b;
        }
        return googleSignInAccount;
    }
}
