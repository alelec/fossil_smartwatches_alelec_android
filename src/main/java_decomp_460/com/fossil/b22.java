package com.fossil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.fossil.u02;
import com.fossil.v02;
import java.util.ArrayList;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b22 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f384a;
    @DexIgnore
    public /* final */ t02 b;
    @DexIgnore
    public /* final */ k22 c;
    @DexIgnore
    public /* final */ h22 d;
    @DexIgnore
    public /* final */ Executor e;
    @DexIgnore
    public /* final */ s32 f;
    @DexIgnore
    public /* final */ t32 g;

    @DexIgnore
    public b22(Context context, t02 t02, k22 k22, h22 h22, Executor executor, s32 s32, t32 t32) {
        this.f384a = context;
        this.b = t02;
        this.c = k22;
        this.d = h22;
        this.e = executor;
        this.f = s32;
        this.g = t32;
    }

    @DexIgnore
    public static /* synthetic */ Object c(b22 b22, v02 v02, Iterable iterable, h02 h02, int i) {
        if (v02.c() == v02.a.TRANSIENT_ERROR) {
            b22.c.h0(iterable);
            b22.d.a(h02, i + 1);
            return null;
        }
        b22.c.g(iterable);
        if (v02.c() == v02.a.OK) {
            b22.c.s(h02, b22.g.a() + v02.b());
        }
        if (!b22.c.f0(h02)) {
            return null;
        }
        b22.d.a(h02, 1);
        return null;
    }

    @DexIgnore
    public static /* synthetic */ void e(b22 b22, h02 h02, int i, Runnable runnable) {
        try {
            s32 s32 = b22.f;
            k22 k22 = b22.c;
            k22.getClass();
            s32.a(z12.b(k22));
            if (!b22.a()) {
                b22.f.a(a22.b(b22, h02, i));
            } else {
                b22.f(h02, i);
            }
        } catch (r32 e2) {
            b22.d.a(h02, i + 1);
        } catch (Throwable th) {
            runnable.run();
            throw th;
        }
        runnable.run();
    }

    @DexIgnore
    public boolean a() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f384a.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @DexIgnore
    public void f(h02 h02, int i) {
        v02 a2;
        b12 b2 = this.b.b(h02.b());
        Iterable<q22> iterable = (Iterable) this.f.a(x12.b(this, h02));
        if (iterable.iterator().hasNext()) {
            if (b2 == null) {
                c12.a("Uploader", "Unknown backend for %s, deleting event batch for it...", h02);
                a2 = v02.a();
            } else {
                ArrayList arrayList = new ArrayList();
                for (q22 q22 : iterable) {
                    arrayList.add(q22.b());
                }
                u02.a a3 = u02.a();
                a3.b(arrayList);
                a3.c(h02.c());
                a2 = b2.a(a3.a());
            }
            this.f.a(y12.b(this, a2, iterable, h02, i));
        }
    }

    @DexIgnore
    public void g(h02 h02, int i, Runnable runnable) {
        this.e.execute(w12.a(this, h02, i, runnable));
    }
}
