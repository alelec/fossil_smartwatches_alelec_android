package com.fossil;

import android.util.Log;
import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ih1 implements rb1<hh1> {
    @DexIgnore
    @Override // com.fossil.rb1
    public ib1 b(ob1 ob1) {
        return ib1.SOURCE;
    }

    @DexIgnore
    /* renamed from: c */
    public boolean a(id1<hh1> id1, File file, ob1 ob1) {
        try {
            zj1.e(id1.get().c(), file);
            return true;
        } catch (IOException e) {
            if (Log.isLoggable("GifEncoder", 5)) {
                Log.w("GifEncoder", "Failed to encode GIF drawable data", e);
            }
            return false;
        }
    }
}
