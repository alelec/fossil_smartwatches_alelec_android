package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p73 implements xw2<s73> {
    @DexIgnore
    public static p73 c; // = new p73();
    @DexIgnore
    public /* final */ xw2<s73> b;

    @DexIgnore
    public p73() {
        this(ww2.b(new r73()));
    }

    @DexIgnore
    public p73(xw2<s73> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((s73) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((s73) c.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((s73) c.zza()).zzc();
    }

    @DexIgnore
    public static boolean d() {
        return ((s73) c.zza()).zzd();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ s73 zza() {
        return this.b.zza();
    }
}
