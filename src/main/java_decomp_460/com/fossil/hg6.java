package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatImageView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.nk5;
import com.fossil.oi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hg6 extends pv5 implements gg6 {
    @DexIgnore
    public g37<z25> g;
    @DexIgnore
    public fg6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public static /* final */ a b; // = new a();

        @DexIgnore
        public final void onClick(View view) {
            CaloriesDetailActivity.a aVar = CaloriesDetailActivity.C;
            Date date = new Date();
            pq7.b(view, "it");
            Context context = view.getContext();
            pq7.b(context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "CaloriesOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    @Override // com.fossil.gg6
    public void G(mv5 mv5, ArrayList<String> arrayList) {
        z25 a2;
        OverviewDayChart overviewDayChart;
        pq7.c(mv5, "baseModel");
        pq7.c(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesOverviewDayFragment", "showDayDetails - baseModel=" + mv5);
        g37<z25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.q) != null) {
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(mv5.f2426a.b(cVar.d()));
            if (!arrayList.isEmpty()) {
                BarChart.H(overviewDayChart, arrayList, false, 2, null);
            } else {
                BarChart.H(overviewDayChart, jl5.b.f(), false, 2, null);
            }
            overviewDayChart.r(mv5);
        }
    }

    @DexIgnore
    public final void K6(vd5 vd5, WorkoutSession workoutSession) {
        AppCompatImageView appCompatImageView;
        if (vd5 != null) {
            View n = vd5.n();
            pq7.b(n, "binding.root");
            Context context = n.getContext();
            oi5.a aVar = oi5.Companion;
            cl7<Integer, Integer> a2 = aVar.a(aVar.d(workoutSession.getEditedType(), workoutSession.getEditedMode()));
            String c = um5.c(context, a2.getSecond().intValue());
            vd5.t.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = vd5.r;
            pq7.b(flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(c);
            FlexibleTextView flexibleTextView2 = vd5.s;
            pq7.b(flexibleTextView2, "it.ftvWorkoutValue");
            hr7 hr7 = hr7.f1520a;
            String c2 = um5.c(context, 2131886628);
            pq7.b(c2, "LanguageHelper.getString\u2026esToday_Text__NumberCals)");
            Float totalCalorie = workoutSession.getTotalCalorie();
            String format = String.format(c2, Arrays.copyOf(new Object[]{dl5.c(totalCalorie != null ? totalCalorie.floatValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1)}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            flexibleTextView2.setText(format);
            FlexibleTextView flexibleTextView3 = vd5.q;
            pq7.b(flexibleTextView3, "it.ftvWorkoutTime");
            flexibleTextView3.setText(lk5.o(workoutSession.getEditedStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
            nk5.a aVar2 = nk5.o;
            fg6 fg6 = this.h;
            String d = aVar2.w(fg6 != null ? fg6.n() : null) ? qn5.l.a().d("dianaActiveCaloriesTab") : qn5.l.a().d("hybridActiveCaloriesTab");
            if (d != null && (appCompatImageView = vd5.t) != null) {
                appCompatImageView.setColorFilter(Color.parseColor(d));
            }
        }
    }

    @DexIgnore
    public final void L6() {
        z25 a2;
        OverviewDayChart overviewDayChart;
        g37<z25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.q) != null) {
            nk5.a aVar = nk5.o;
            fg6 fg6 = this.h;
            if (aVar.w(fg6 != null ? fg6.n() : null)) {
                overviewDayChart.D("dianaActiveCaloriesTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.D("hybridActiveCaloriesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(fg6 fg6) {
        pq7.c(fg6, "presenter");
        this.h = fg6;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        z25 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onCreateView");
        z25 z25 = (z25) aq0.f(layoutInflater, 2131558509, viewGroup, false, A6());
        z25.r.setOnClickListener(a.b);
        this.g = new g37<>(this, z25);
        L6();
        g37<z25> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onResume");
        L6();
        fg6 fg6 = this.h;
        if (fg6 != null) {
            fg6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onStop");
        fg6 fg6 = this.h;
        if (fg6 != null) {
            fg6.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.gg6
    public void v(boolean z, List<WorkoutSession> list) {
        z25 a2;
        View n;
        View n2;
        pq7.c(list, "workoutSessions");
        g37<z25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null) {
            if (z) {
                LinearLayout linearLayout = a2.u;
                pq7.b(linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                K6(a2.s, list.get(0));
                if (size == 1) {
                    vd5 vd5 = a2.t;
                    if (vd5 != null && (n2 = vd5.n()) != null) {
                        n2.setVisibility(8);
                        return;
                    }
                    return;
                }
                vd5 vd52 = a2.t;
                if (!(vd52 == null || (n = vd52.n()) == null)) {
                    n.setVisibility(0);
                }
                K6(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            pq7.b(linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
