package com.fossil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.m47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class so6 extends pv5 implements ro6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public g37<xa5> g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return so6.i;
        }

        @DexIgnore
        public final so6 b() {
            return new so6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ so6 b;

        @DexIgnore
        public b(so6 so6) {
            this.b = so6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = m47.a(m47.c.FEATURES, m47.b.SHOP_BATTERY);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = so6.j.a();
            local.d(a3, "Purchase Battery URL = " + a2);
            so6 so6 = this.b;
            pq7.b(a2, "url");
            so6.N6(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ so6 b;

        @DexIgnore
        public c(so6 so6) {
            this.b = so6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = m47.a(m47.c.FEATURES, m47.b.LOW_BATTERY);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = so6.j.a();
            local.d(a3, "Need Help URL = " + a2);
            so6 so6 = this.b;
            pq7.b(a2, "url");
            so6.N6(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ so6 b;

        @DexIgnore
        public d(so6 so6) {
            this.b = so6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    /*
    static {
        String simpleName = so6.class.getSimpleName();
        if (simpleName != null) {
            pq7.b(simpleName, "ReplaceBatteryFragment::class.java.simpleName!!");
            i = simpleName;
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    /* renamed from: M6 */
    public void M5(qo6 qo6) {
        pq7.c(qo6, "presenter");
        i14.l(qo6);
        pq7.b(qo6, "Preconditions.checkNotNull(presenter)");
    }

    @DexIgnore
    public final void N6(String str) {
        J6(new Intent("android.intent.action.VIEW", Uri.parse(str)), i);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        xa5 xa5 = (xa5) aq0.f(LayoutInflater.from(getContext()), 2131558618, null, false, A6());
        this.g = new g37<>(this, xa5);
        pq7.b(xa5, "binding");
        return xa5.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<xa5> g37 = this.g;
        if (g37 != null) {
            xa5 a2 = g37.a();
            if (a2 != null) {
                a2.s.setOnClickListener(new b(this));
                a2.r.setOnClickListener(new c(this));
                a2.q.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
