package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.CancellationSignal;
import android.util.Log;
import com.fossil.kl0;
import com.fossil.zm0;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yl0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ConcurrentHashMap<Long, kl0.b> f4330a; // = new ConcurrentHashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements c<zm0.f> {
        @DexIgnore
        public a(yl0 yl0) {
        }

        @DexIgnore
        /* renamed from: c */
        public int a(zm0.f fVar) {
            return fVar.d();
        }

        @DexIgnore
        /* renamed from: d */
        public boolean b(zm0.f fVar) {
            return fVar.e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements c<kl0.c> {
        @DexIgnore
        public b(yl0 yl0) {
        }

        @DexIgnore
        /* renamed from: c */
        public int a(kl0.c cVar) {
            return cVar.e();
        }

        @DexIgnore
        /* renamed from: d */
        public boolean b(kl0.c cVar) {
            return cVar.f();
        }
    }

    @DexIgnore
    public interface c<T> {
        @DexIgnore
        int a(T t);

        @DexIgnore
        boolean b(T t);
    }

    @DexIgnore
    public static <T> T g(T[] tArr, int i, c<T> cVar) {
        int i2 = (i & 1) == 0 ? 400 : 700;
        boolean z = (i & 2) != 0;
        T t = null;
        int i3 = Integer.MAX_VALUE;
        for (T t2 : tArr) {
            int abs = (cVar.b(t2) == z ? 0 : 1) + (Math.abs(cVar.a(t2) - i2) * 2);
            if (t == null || i3 > abs) {
                i3 = abs;
                t = t2;
            }
        }
        return t;
    }

    @DexIgnore
    public static long j(Typeface typeface) {
        if (typeface == null) {
            return 0;
        }
        try {
            Field declaredField = Typeface.class.getDeclaredField("native_instance");
            declaredField.setAccessible(true);
            return ((Number) declaredField.get(typeface)).longValue();
        } catch (NoSuchFieldException e) {
            Log.e("TypefaceCompatBaseImpl", "Could not retrieve font from family.", e);
            return 0;
        } catch (IllegalAccessException e2) {
            Log.e("TypefaceCompatBaseImpl", "Could not retrieve font from family.", e2);
            return 0;
        }
    }

    @DexIgnore
    public final void a(Typeface typeface, kl0.b bVar) {
        long j = j(typeface);
        if (j != 0) {
            this.f4330a.put(Long.valueOf(j), bVar);
        }
    }

    @DexIgnore
    public Typeface b(Context context, kl0.b bVar, Resources resources, int i) {
        kl0.c f = f(bVar, i);
        if (f == null) {
            return null;
        }
        Typeface d = sl0.d(context, resources, f.b(), f.a(), i);
        a(d, bVar);
        return d;
    }

    @DexIgnore
    public Typeface c(Context context, CancellationSignal cancellationSignal, zm0.f[] fVarArr, int i) {
        InputStream inputStream;
        Typeface typeface = null;
        if (fVarArr.length >= 1) {
            try {
                inputStream = context.getContentResolver().openInputStream(h(fVarArr, i).c());
                try {
                    typeface = d(context, inputStream);
                    zl0.a(inputStream);
                } catch (IOException e) {
                    zl0.a(inputStream);
                    return typeface;
                } catch (Throwable th) {
                    th = th;
                    zl0.a(inputStream);
                    throw th;
                }
            } catch (IOException e2) {
                inputStream = null;
                zl0.a(inputStream);
                return typeface;
            } catch (Throwable th2) {
                th = th2;
                inputStream = null;
                zl0.a(inputStream);
                throw th;
            }
        }
        return typeface;
    }

    @DexIgnore
    public Typeface d(Context context, InputStream inputStream) {
        File e = zl0.e(context);
        if (e == null) {
            return null;
        }
        try {
            if (!zl0.d(e, inputStream)) {
                return null;
            }
            Typeface createFromFile = Typeface.createFromFile(e.getPath());
            e.delete();
            return createFromFile;
        } catch (RuntimeException e2) {
            return null;
        } finally {
            e.delete();
        }
    }

    @DexIgnore
    public Typeface e(Context context, Resources resources, int i, String str, int i2) {
        File e = zl0.e(context);
        if (e == null) {
            return null;
        }
        try {
            if (!zl0.c(e, resources, i)) {
                return null;
            }
            Typeface createFromFile = Typeface.createFromFile(e.getPath());
            e.delete();
            return createFromFile;
        } catch (RuntimeException e2) {
            return null;
        } finally {
            e.delete();
        }
    }

    @DexIgnore
    public final kl0.c f(kl0.b bVar, int i) {
        return (kl0.c) g(bVar.a(), i, new b(this));
    }

    @DexIgnore
    public zm0.f h(zm0.f[] fVarArr, int i) {
        return (zm0.f) g(fVarArr, i, new a(this));
    }

    @DexIgnore
    public kl0.b i(Typeface typeface) {
        long j = j(typeface);
        if (j == 0) {
            return null;
        }
        return this.f4330a.get(Long.valueOf(j));
    }
}
