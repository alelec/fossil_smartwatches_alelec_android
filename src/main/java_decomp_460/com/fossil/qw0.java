package com.fossil;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Looper;
import android.util.Log;
import com.fossil.mx0;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qw0 {
    @DexIgnore
    public static /* final */ String DB_IMPL_SUFFIX; // = "_Impl";
    @DexIgnore
    public static /* final */ int MAX_BIND_PARAMETER_CNT; // = 999;
    @DexIgnore
    public boolean mAllowMainThreadQueries;
    @DexIgnore
    public /* final */ Map<String, Object> mBackingFieldMap; // = new ConcurrentHashMap();
    @DexIgnore
    @Deprecated
    public List<b> mCallbacks;
    @DexIgnore
    public /* final */ ReentrantReadWriteLock mCloseLock; // = new ReentrantReadWriteLock();
    @DexIgnore
    @Deprecated
    public volatile lx0 mDatabase;
    @DexIgnore
    public /* final */ nw0 mInvalidationTracker; // = createInvalidationTracker();
    @DexIgnore
    public mx0 mOpenHelper;
    @DexIgnore
    public Executor mQueryExecutor;
    @DexIgnore
    public /* final */ ThreadLocal<Integer> mSuspendingTransactionId; // = new ThreadLocal<>();
    @DexIgnore
    public Executor mTransactionExecutor;
    @DexIgnore
    public boolean mWriteAheadLoggingEnabled;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<T extends qw0> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Class<T> f3033a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ Context c;
        @DexIgnore
        public ArrayList<b> d;
        @DexIgnore
        public Executor e;
        @DexIgnore
        public Executor f;
        @DexIgnore
        public mx0.c g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public c i; // = c.AUTOMATIC;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public boolean k; // = true;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public /* final */ d m; // = new d();
        @DexIgnore
        public Set<Integer> n;
        @DexIgnore
        public Set<Integer> o;
        @DexIgnore
        public String p;
        @DexIgnore
        public File q;

        @DexIgnore
        public a(Context context, Class<T> cls, String str) {
            this.c = context;
            this.f3033a = cls;
            this.b = str;
        }

        @DexIgnore
        public a<T> a(b bVar) {
            if (this.d == null) {
                this.d = new ArrayList<>();
            }
            this.d.add(bVar);
            return this;
        }

        @DexIgnore
        public a<T> b(ax0... ax0Arr) {
            if (this.o == null) {
                this.o = new HashSet();
            }
            for (ax0 ax0 : ax0Arr) {
                this.o.add(Integer.valueOf(ax0.startVersion));
                this.o.add(Integer.valueOf(ax0.endVersion));
            }
            this.m.b(ax0Arr);
            return this;
        }

        @DexIgnore
        public a<T> c() {
            this.h = true;
            return this;
        }

        @DexIgnore
        @SuppressLint({"RestrictedApi"})
        public T d() {
            Executor executor;
            if (this.c == null) {
                throw new IllegalArgumentException("Cannot provide null context for the database.");
            } else if (this.f3033a != null) {
                if (this.e == null && this.f == null) {
                    Executor e2 = bi0.e();
                    this.f = e2;
                    this.e = e2;
                } else {
                    Executor executor2 = this.e;
                    if (executor2 != null && this.f == null) {
                        this.f = executor2;
                    } else if (this.e == null && (executor = this.f) != null) {
                        this.e = executor;
                    }
                }
                Set<Integer> set = this.o;
                if (!(set == null || this.n == null)) {
                    for (Integer num : set) {
                        if (this.n.contains(num)) {
                            throw new IllegalArgumentException("Inconsistency detected. A Migration was supplied to addMigration(Migration... migrations) that has a start or end version equal to a start version supplied to fallbackToDestructiveMigrationFrom(int... startVersions). Start version: " + num);
                        }
                    }
                }
                if (this.g == null) {
                    this.g = new sx0();
                }
                if (!(this.p == null && this.q == null)) {
                    if (this.b == null) {
                        throw new IllegalArgumentException("Cannot create from asset or file for an in-memory database.");
                    } else if (this.p == null || this.q == null) {
                        this.g = new ww0(this.p, this.q, this.g);
                    } else {
                        throw new IllegalArgumentException("Both createFromAsset() and createFromFile() was called on this Builder but the database can only be created using one of the two configurations.");
                    }
                }
                Context context = this.c;
                hw0 hw0 = new hw0(context, this.b, this.g, this.m, this.d, this.h, this.i.resolve(context), this.e, this.f, this.j, this.k, this.l, this.n, this.p, this.q);
                T t = (T) ((qw0) pw0.b(this.f3033a, qw0.DB_IMPL_SUFFIX));
                t.init(hw0);
                return t;
            } else {
                throw new IllegalArgumentException("Must provide an abstract class that extends RoomDatabase");
            }
        }

        @DexIgnore
        public a<T> e() {
            this.j = this.b != null;
            return this;
        }

        @DexIgnore
        public a<T> f() {
            this.k = false;
            this.l = true;
            return this;
        }

        @DexIgnore
        public a<T> g(mx0.c cVar) {
            this.g = cVar;
            return this;
        }

        @DexIgnore
        public a<T> h(Executor executor) {
            this.e = executor;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {
        @DexIgnore
        public void onCreate(lx0 lx0) {
        }

        @DexIgnore
        public void onDestructiveMigration(lx0 lx0) {
        }

        @DexIgnore
        public void onOpen(lx0 lx0) {
        }
    }

    @DexIgnore
    public enum c {
        AUTOMATIC,
        TRUNCATE,
        WRITE_AHEAD_LOGGING;

        @DexIgnore
        public static boolean a(ActivityManager activityManager) {
            if (Build.VERSION.SDK_INT >= 19) {
                return activityManager.isLowRamDevice();
            }
            return false;
        }

        @DexIgnore
        @SuppressLint({"NewApi"})
        public c resolve(Context context) {
            ActivityManager activityManager;
            return this != AUTOMATIC ? this : (Build.VERSION.SDK_INT < 16 || (activityManager = (ActivityManager) context.getSystemService(Constants.ACTIVITY)) == null || a(activityManager)) ? TRUNCATE : WRITE_AHEAD_LOGGING;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public HashMap<Integer, TreeMap<Integer, ax0>> f3034a; // = new HashMap<>();

        @DexIgnore
        public final void a(ax0 ax0) {
            TreeMap<Integer, ax0> treeMap;
            int i = ax0.startVersion;
            int i2 = ax0.endVersion;
            TreeMap<Integer, ax0> treeMap2 = this.f3034a.get(Integer.valueOf(i));
            if (treeMap2 == null) {
                TreeMap<Integer, ax0> treeMap3 = new TreeMap<>();
                this.f3034a.put(Integer.valueOf(i), treeMap3);
                treeMap = treeMap3;
            } else {
                treeMap = treeMap2;
            }
            ax0 ax02 = treeMap.get(Integer.valueOf(i2));
            if (ax02 != null) {
                Log.w("ROOM", "Overriding migration " + ax02 + " with " + ax0);
            }
            treeMap.put(Integer.valueOf(i2), ax0);
        }

        @DexIgnore
        public void b(ax0... ax0Arr) {
            for (ax0 ax0 : ax0Arr) {
                a(ax0);
            }
        }

        @DexIgnore
        public List<ax0> c(int i, int i2) {
            if (i == i2) {
                return Collections.emptyList();
            }
            return d(new ArrayList(), i2 > i, i, i2);
        }

        @DexIgnore
        public final List<ax0> d(List<ax0> list, boolean z, int i, int i2) {
            boolean z2;
            int i3;
            boolean z3;
            int i4 = i;
            while (true) {
                if (z) {
                    if (i4 >= i2) {
                        return list;
                    }
                } else if (i4 <= i2) {
                    return list;
                }
                TreeMap<Integer, ax0> treeMap = this.f3034a.get(Integer.valueOf(i4));
                if (treeMap == null) {
                    return null;
                }
                Iterator<Integer> it = (z ? treeMap.descendingKeySet() : treeMap.keySet()).iterator();
                while (true) {
                    if (!it.hasNext()) {
                        z2 = false;
                        i3 = i4;
                        break;
                    }
                    i3 = it.next().intValue();
                    if (!z ? i3 < i2 || i3 >= i4 : i3 > i2 || i3 <= i4) {
                        z3 = false;
                        continue;
                    } else {
                        z3 = true;
                        continue;
                    }
                    if (z3) {
                        list.add(treeMap.get(Integer.valueOf(i3)));
                        z2 = true;
                        break;
                    }
                }
                if (!z2) {
                    return null;
                }
                i4 = i3;
            }
        }
    }

    @DexIgnore
    public static boolean isMainThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    @DexIgnore
    public void assertNotMainThread() {
        if (!this.mAllowMainThreadQueries && isMainThread()) {
            throw new IllegalStateException("Cannot access database on the main thread since it may potentially lock the UI for a long period of time.");
        }
    }

    @DexIgnore
    public void assertNotSuspendingTransaction() {
        if (!inTransaction() && this.mSuspendingTransactionId.get() != null) {
            throw new IllegalStateException("Cannot access database on a different coroutine context inherited from a suspending transaction.");
        }
    }

    @DexIgnore
    @Deprecated
    public void beginTransaction() {
        assertNotMainThread();
        lx0 writableDatabase = this.mOpenHelper.getWritableDatabase();
        this.mInvalidationTracker.q(writableDatabase);
        writableDatabase.beginTransaction();
    }

    @DexIgnore
    public abstract void clearAllTables();

    @DexIgnore
    public void close() {
        if (isOpen()) {
            ReentrantReadWriteLock.WriteLock writeLock = this.mCloseLock.writeLock();
            try {
                writeLock.lock();
                this.mInvalidationTracker.n();
                this.mOpenHelper.close();
            } finally {
                writeLock.unlock();
            }
        }
    }

    @DexIgnore
    public px0 compileStatement(String str) {
        assertNotMainThread();
        assertNotSuspendingTransaction();
        return this.mOpenHelper.getWritableDatabase().compileStatement(str);
    }

    @DexIgnore
    public abstract nw0 createInvalidationTracker();

    @DexIgnore
    public abstract mx0 createOpenHelper(hw0 hw0);

    @DexIgnore
    @Deprecated
    public void endTransaction() {
        this.mOpenHelper.getWritableDatabase().endTransaction();
        if (!inTransaction()) {
            this.mInvalidationTracker.h();
        }
    }

    @DexIgnore
    public Map<String, Object> getBackingFieldMap() {
        return this.mBackingFieldMap;
    }

    @DexIgnore
    public Lock getCloseLock() {
        return this.mCloseLock.readLock();
    }

    @DexIgnore
    public nw0 getInvalidationTracker() {
        return this.mInvalidationTracker;
    }

    @DexIgnore
    public mx0 getOpenHelper() {
        return this.mOpenHelper;
    }

    @DexIgnore
    public Executor getQueryExecutor() {
        return this.mQueryExecutor;
    }

    @DexIgnore
    public ThreadLocal<Integer> getSuspendingTransactionId() {
        return this.mSuspendingTransactionId;
    }

    @DexIgnore
    public Executor getTransactionExecutor() {
        return this.mTransactionExecutor;
    }

    @DexIgnore
    public boolean inTransaction() {
        return this.mOpenHelper.getWritableDatabase().inTransaction();
    }

    @DexIgnore
    public void init(hw0 hw0) {
        boolean z;
        mx0 createOpenHelper = createOpenHelper(hw0);
        this.mOpenHelper = createOpenHelper;
        if (createOpenHelper instanceof vw0) {
            ((vw0) createOpenHelper).b(hw0);
        }
        if (Build.VERSION.SDK_INT >= 16) {
            z = hw0.g == c.WRITE_AHEAD_LOGGING;
            this.mOpenHelper.setWriteAheadLoggingEnabled(z);
        } else {
            z = false;
        }
        this.mCallbacks = hw0.e;
        this.mQueryExecutor = hw0.h;
        this.mTransactionExecutor = new zw0(hw0.i);
        this.mAllowMainThreadQueries = hw0.f;
        this.mWriteAheadLoggingEnabled = z;
        if (hw0.j) {
            this.mInvalidationTracker.l(hw0.b, hw0.c);
        }
    }

    @DexIgnore
    public void internalInitInvalidationTracker(lx0 lx0) {
        this.mInvalidationTracker.f(lx0);
    }

    @DexIgnore
    public boolean isOpen() {
        lx0 lx0 = this.mDatabase;
        return lx0 != null && lx0.isOpen();
    }

    @DexIgnore
    public Cursor query(ox0 ox0) {
        return query(ox0, (CancellationSignal) null);
    }

    @DexIgnore
    public Cursor query(ox0 ox0, CancellationSignal cancellationSignal) {
        assertNotMainThread();
        assertNotSuspendingTransaction();
        return (cancellationSignal == null || Build.VERSION.SDK_INT < 16) ? this.mOpenHelper.getWritableDatabase().query(ox0) : this.mOpenHelper.getWritableDatabase().query(ox0, cancellationSignal);
    }

    @DexIgnore
    public Cursor query(String str, Object[] objArr) {
        return this.mOpenHelper.getWritableDatabase().query(new kx0(str, objArr));
    }

    @DexIgnore
    public <V> V runInTransaction(Callable<V> callable) {
        beginTransaction();
        try {
            V call = callable.call();
            setTransactionSuccessful();
            endTransaction();
            return call;
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e2) {
            gx0.a(e2);
            throw null;
        } catch (Throwable th) {
            endTransaction();
            throw th;
        }
    }

    @DexIgnore
    public void runInTransaction(Runnable runnable) {
        beginTransaction();
        try {
            runnable.run();
            setTransactionSuccessful();
        } finally {
            endTransaction();
        }
    }

    @DexIgnore
    @Deprecated
    public void setTransactionSuccessful() {
        this.mOpenHelper.getWritableDatabase().setTransactionSuccessful();
    }
}
