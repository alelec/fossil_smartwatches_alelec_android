package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p34 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ i54<Object> f2773a; // = new c();
    @DexIgnore
    public static /* final */ Iterator<Object> b; // = new d();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends o14<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Object[] d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(int i, int i2, Object[] objArr, int i3) {
            super(i, i2);
            this.d = objArr;
            this.e = i3;
        }

        @DexIgnore
        @Override // com.fossil.o14
        public T a(int i) {
            return (T) this.d[this.e + i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends h54<T> {
        @DexIgnore
        public boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ Object c;

        @DexIgnore
        public b(Object obj) {
            this.c = obj;
        }

        @DexIgnore
        public boolean hasNext() {
            return !this.b;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (!this.b) {
                this.b = true;
                return (T) this.c;
            }
            throw new NoSuchElementException();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends i54<Object> {
        @DexIgnore
        public boolean hasNext() {
            return false;
        }

        @DexIgnore
        public boolean hasPrevious() {
            return false;
        }

        @DexIgnore
        @Override // java.util.Iterator, java.util.ListIterator
        public Object next() {
            throw new NoSuchElementException();
        }

        @DexIgnore
        public int nextIndex() {
            return 0;
        }

        @DexIgnore
        @Override // java.util.ListIterator
        public Object previous() {
            throw new NoSuchElementException();
        }

        @DexIgnore
        public int previousIndex() {
            return -1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Iterator<Object> {
        @DexIgnore
        public boolean hasNext() {
            return false;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public Object next() {
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            a24.c(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends h54<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator b;

        @DexIgnore
        public e(Iterator it) {
            this.b = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            return (T) this.b.next();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends p14<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator d;
        @DexIgnore
        public /* final */ /* synthetic */ j14 e;

        @DexIgnore
        public f(Iterator it, j14 j14) {
            this.d = it;
            this.e = j14;
        }

        @DexIgnore
        @Override // com.fossil.p14
        public T a() {
            while (this.d.hasNext()) {
                T t = (T) this.d.next();
                if (this.e.apply(t)) {
                    return t;
                }
            }
            return (T) b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends g54<F, T> {
        @DexIgnore
        public /* final */ /* synthetic */ b14 c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(Iterator it, b14 b14) {
            super(it);
            this.c = b14;
        }

        @DexIgnore
        @Override // com.fossil.g54
        public T a(F f) {
            return (T) this.c.apply(f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h<E> implements j44<E> {
        @DexIgnore
        public /* final */ Iterator<? extends E> b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public E d;

        @DexIgnore
        public h(Iterator<? extends E> it) {
            i14.l(it);
            this.b = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.c || this.b.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator, com.fossil.j44
        public E next() {
            if (!this.c) {
                return (E) this.b.next();
            }
            E e = this.d;
            this.c = false;
            this.d = null;
            return e;
        }

        @DexIgnore
        @Override // com.fossil.j44
        public E peek() {
            if (!this.c) {
                this.d = (E) this.b.next();
                this.c = true;
            }
            return this.d;
        }

        @DexIgnore
        public void remove() {
            i14.t(!this.c, "Can't remove after you've peeked at next");
            this.b.remove();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Collection<T> */
    /* JADX WARN: Multi-variable type inference failed */
    @CanIgnoreReturnValue
    public static <T> boolean a(Collection<T> collection, Iterator<? extends T> it) {
        i14.l(collection);
        i14.l(it);
        boolean z = false;
        while (it.hasNext()) {
            z |= collection.add(it.next());
        }
        return z;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static int b(Iterator<?> it, int i) {
        int i2 = 0;
        i14.l(it);
        i14.e(i >= 0, "numberToAdvance must be nonnegative");
        while (i2 < i && it.hasNext()) {
            it.next();
            i2++;
        }
        return i2;
    }

    @DexIgnore
    public static <T> boolean c(Iterator<T> it, j14<? super T> j14) {
        i14.l(j14);
        while (it.hasNext()) {
            if (!j14.apply(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static <T> boolean d(Iterator<T> it, j14<? super T> j14) {
        return p(it, j14) != -1;
    }

    @DexIgnore
    public static void e(Iterator<?> it) {
        i14.l(it);
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
    }

    @DexIgnore
    public static boolean f(Iterator<?> it, Object obj) {
        return d(it, k14.a(obj));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0007  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean g(java.util.Iterator<?> r3, java.util.Iterator<?> r4) {
        /*
            r0 = 0
        L_0x0001:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x001d
            boolean r1 = r4.hasNext()
            if (r1 != 0) goto L_0x000e
        L_0x000d:
            return r0
        L_0x000e:
            java.lang.Object r1 = r3.next()
            java.lang.Object r2 = r4.next()
            boolean r1 = com.fossil.f14.a(r1, r2)
            if (r1 != 0) goto L_0x0001
            goto L_0x000d
        L_0x001d:
            boolean r0 = r4.hasNext()
            r0 = r0 ^ 1
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.p34.g(java.util.Iterator, java.util.Iterator):boolean");
    }

    @DexIgnore
    public static <T> h54<T> h() {
        return i();
    }

    @DexIgnore
    public static <T> i54<T> i() {
        return (i54<T>) f2773a;
    }

    @DexIgnore
    public static <T> Iterator<T> j() {
        return (Iterator<T>) b;
    }

    @DexIgnore
    public static <T> h54<T> k(Iterator<T> it, j14<? super T> j14) {
        i14.l(it);
        i14.l(j14);
        return new f(it, j14);
    }

    @DexIgnore
    @SafeVarargs
    public static <T> h54<T> l(T... tArr) {
        return m(tArr, 0, tArr.length, 0);
    }

    @DexIgnore
    public static <T> i54<T> m(T[] tArr, int i, int i2, int i3) {
        i14.d(i2 >= 0);
        i14.r(i, i + i2, tArr.length);
        i14.p(i3, i2);
        return i2 == 0 ? i() : new a(i2, i3, tArr, i);
    }

    @DexIgnore
    public static <T> T n(Iterator<? extends T> it, T t) {
        return it.hasNext() ? (T) it.next() : t;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> T o(Iterator<T> it) {
        T next = it.next();
        if (!it.hasNext()) {
            return next;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("expected one element but was: <");
        sb.append((Object) next);
        for (int i = 0; i < 4 && it.hasNext(); i++) {
            sb.append(", ");
            sb.append((Object) it.next());
        }
        if (it.hasNext()) {
            sb.append(", ...");
        }
        sb.append('>');
        throw new IllegalArgumentException(sb.toString());
    }

    @DexIgnore
    public static <T> int p(Iterator<T> it, j14<? super T> j14) {
        i14.m(j14, "predicate");
        int i = 0;
        while (it.hasNext()) {
            if (j14.apply(it.next())) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public static <T> j44<T> q(Iterator<? extends T> it) {
        return it instanceof h ? (h) it : new h(it);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static boolean r(Iterator<?> it, Collection<?> collection) {
        return s(it, k14.b(collection));
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> boolean s(Iterator<T> it, j14<? super T> j14) {
        i14.l(j14);
        boolean z = false;
        while (it.hasNext()) {
            if (j14.apply(it.next())) {
                it.remove();
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static boolean t(Iterator<?> it, Collection<?> collection) {
        return s(it, k14.d(k14.b(collection)));
    }

    @DexIgnore
    public static <T> h54<T> u(T t) {
        return new b(t);
    }

    @DexIgnore
    public static String v(Iterator<?> it) {
        d14 d14 = b24.f385a;
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        d14.d(sb, it);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    public static <F, T> Iterator<T> w(Iterator<F> it, b14<? super F, ? extends T> b14) {
        i14.l(b14);
        return new g(it, b14);
    }

    @DexIgnore
    public static <T> h54<T> x(Iterator<? extends T> it) {
        i14.l(it);
        return it instanceof h54 ? (h54) it : new e(it);
    }
}
