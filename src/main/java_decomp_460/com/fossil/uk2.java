package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uk2 extends pk2 implements sk2 {
    @DexIgnore
    public uk2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
    }

    @DexIgnore
    @Override // com.fossil.sk2
    public final String getId() throws RemoteException {
        Parcel e = e(1, d());
        String readString = e.readString();
        e.recycle();
        return readString;
    }

    @DexIgnore
    @Override // com.fossil.sk2
    public final boolean x0(boolean z) throws RemoteException {
        Parcel d = d();
        rk2.a(d, true);
        Parcel e = e(2, d);
        boolean b = rk2.b(e);
        e.recycle();
        return b;
    }
}
