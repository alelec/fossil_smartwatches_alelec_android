package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;
import com.fossil.cg0;
import com.fossil.ig0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ch0 {
    @DexIgnore
    void a(Menu menu, ig0.a aVar);

    @DexIgnore
    boolean b();

    @DexIgnore
    Object c();  // void declaration

    @DexIgnore
    Object collapseActionView();  // void declaration

    @DexIgnore
    boolean d();

    @DexIgnore
    boolean e();

    @DexIgnore
    boolean f();

    @DexIgnore
    boolean g();

    @DexIgnore
    Context getContext();

    @DexIgnore
    CharSequence getTitle();

    @DexIgnore
    Object h();  // void declaration

    @DexIgnore
    void i(mh0 mh0);

    @DexIgnore
    boolean j();

    @DexIgnore
    void k(int i);

    @DexIgnore
    Menu l();

    @DexIgnore
    void m(int i);

    @DexIgnore
    int n();

    @DexIgnore
    ro0 o(int i, long j);

    @DexIgnore
    void p(ig0.a aVar, cg0.a aVar2);

    @DexIgnore
    ViewGroup q();

    @DexIgnore
    void r(boolean z);

    @DexIgnore
    int s();

    @DexIgnore
    void setIcon(int i);

    @DexIgnore
    void setIcon(Drawable drawable);

    @DexIgnore
    void setTitle(CharSequence charSequence);

    @DexIgnore
    void setVisibility(int i);

    @DexIgnore
    void setWindowCallback(Window.Callback callback);

    @DexIgnore
    void setWindowTitle(CharSequence charSequence);

    @DexIgnore
    Object t();  // void declaration

    @DexIgnore
    Object u();  // void declaration

    @DexIgnore
    void v(boolean z);
}
