package com.fossil;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.IntentSender;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w62 extends n62 {
    @DexIgnore
    public w62(Status status) {
        super(status);
    }

    @DexIgnore
    public PendingIntent getResolution() {
        return getStatus().c();
    }

    @DexIgnore
    public void startResolutionForResult(Activity activity, int i) throws IntentSender.SendIntentException {
        getStatus().F(activity, i);
    }
}
