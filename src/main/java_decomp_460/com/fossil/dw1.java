package com.fossil;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.imagefilters.EInkImageFactory;
import com.fossil.imagefilters.Format;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dw1 extends mv1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ vb0 e;
    @DexIgnore
    public gw1 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<dw1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public dw1 createFromParcel(Parcel parcel) {
            return new dw1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public dw1[] newArray(int i) {
            return new dw1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ dw1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.e = vb0.IMAGE;
        Parcelable readParcelable = parcel.readParcelable(gw1.class.getClassLoader());
        if (readParcelable != null) {
            this.f = (gw1) readParcelable;
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public dw1(jv1 jv1, kv1 kv1, byte[] bArr) {
        this(g80.e(0, 1), jv1, kv1, bArr);
    }

    @DexIgnore
    public dw1(String str, jv1 jv1, kv1 kv1, byte[] bArr) {
        super(str, jv1, kv1);
        this.e = vb0.IMAGE;
        this.f = new gw1(str, bArr);
    }

    @DexIgnore
    public dw1(JSONObject jSONObject, cc0[] cc0Arr, String str) throws IllegalArgumentException {
        super(jSONObject, str);
        cc0 cc0;
        this.e = vb0.IMAGE;
        String optString = jSONObject.optString("name");
        int length = cc0Arr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                cc0 = null;
                break;
            }
            cc0 = cc0Arr[i];
            if (pq7.a(optString, cc0.b)) {
                break;
            }
            i++;
        }
        byte[] bArr = cc0 != null ? cc0.c : null;
        if (bArr != null) {
            Bitmap decode = EInkImageFactory.decode(bArr, Format.RLE);
            pq7.b(decode, "EInkImageFactory.decode(\u2026iceImageData, Format.RLE)");
            this.f = new gw1(getName(), cy1.b(decode, null, 1, null));
            decode.recycle();
            return;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    @DexIgnore
    public final cc0 a(String str) {
        return this.f.a(c().toActualSize(240, 240), str);
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public vb0 a() {
        return this.e;
    }

    @DexIgnore
    public final JSONObject b(String str) {
        JSONObject put = d().put("name", str);
        pq7.b(put, "getUIScript()\n          \u2026onstant.NAME, customName)");
        return put;
    }

    @DexIgnore
    @Override // java.lang.Object, com.fossil.mv1, com.fossil.mv1
    public dw1 clone() {
        String name = getName();
        jv1 clone = b().clone();
        kv1 clone2 = c().clone();
        byte[] bitmapImageData = this.f.getBitmapImageData();
        byte[] copyOf = Arrays.copyOf(bitmapImageData, bitmapImageData.length);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new dw1(name, clone, clone2, copyOf);
    }

    @DexIgnore
    public final gw1 e() {
        return this.f;
    }

    @DexIgnore
    public final byte[] getImageData() {
        return this.f.getBitmapImageData();
    }

    @DexIgnore
    public final dw1 setImageData(byte[] bArr) {
        this.f = new gw1(getName(), bArr);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public dw1 setScaledHeight(float f2) {
        mv1 scaledHeight = super.setScaledHeight(f2);
        if (scaledHeight != null) {
            return (dw1) scaledHeight;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.image.ImageElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public dw1 setScaledPosition(jv1 jv1) {
        mv1 scaledPosition = super.setScaledPosition(jv1);
        if (scaledPosition != null) {
            return (dw1) scaledPosition;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.image.ImageElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public dw1 setScaledSize(kv1 kv1) {
        mv1 scaledSize = super.setScaledSize(kv1);
        if (scaledSize != null) {
            return (dw1) scaledSize;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.image.ImageElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public dw1 setScaledWidth(float f2) {
        mv1 scaledWidth = super.setScaledWidth(f2);
        if (scaledWidth != null) {
            return (dw1) scaledWidth;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.image.ImageElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public dw1 setScaledX(float f2) {
        mv1 scaledX = super.setScaledX(f2);
        if (scaledX != null) {
            return (dw1) scaledX;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.image.ImageElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public dw1 setScaledY(float f2) {
        mv1 scaledY = super.setScaledY(f2);
        if (scaledY != null) {
            return (dw1) scaledY;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.image.ImageElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.f, i);
        }
    }
}
