package com.fossil;

import com.fossil.af1;
import com.fossil.wb1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class if1<Model> implements af1<Model, Model> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ if1<?> f1614a; // = new if1<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Model> implements bf1<Model, Model> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ a<?> f1615a; // = new a<>();

        @DexIgnore
        public static <T> a<T> a() {
            return (a<T>) f1615a;
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Model, Model> b(ef1 ef1) {
            return if1.c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Model> implements wb1<Model> {
        @DexIgnore
        public /* final */ Model b;

        @DexIgnore
        public b(Model model) {
            this.b = model;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public gb1 c() {
            return gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void d(sa1 sa1, wb1.a<? super Model> aVar) {
            aVar.e(this.b);
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public Class<Model> getDataClass() {
            return (Class<Model>) this.b.getClass();
        }
    }

    @DexIgnore
    public static <T> if1<T> c() {
        return (if1<T>) f1614a;
    }

    @DexIgnore
    @Override // com.fossil.af1
    public boolean a(Model model) {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.af1
    public af1.a<Model> b(Model model, int i, int i2, ob1 ob1) {
        return new af1.a<>(new yj1(model), new b(model));
    }
}
