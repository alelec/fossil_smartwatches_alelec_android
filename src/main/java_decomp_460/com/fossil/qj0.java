package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.uj0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qj0 extends yj0 {
    @DexIgnore
    public int m0; // = 0;
    @DexIgnore
    public ArrayList<ak0> n0; // = new ArrayList<>(4);
    @DexIgnore
    public boolean o0; // = true;

    @DexIgnore
    public boolean K0() {
        return this.o0;
    }

    @DexIgnore
    public void L0(boolean z) {
        this.o0 = z;
    }

    @DexIgnore
    public void M0(int i) {
        this.m0 = i;
    }

    @DexIgnore
    @Override // com.fossil.uj0
    public void S() {
        super.S();
        this.n0.clear();
    }

    @DexIgnore
    @Override // com.fossil.uj0
    public void U() {
        ak0 f;
        ak0 ak0;
        float f2;
        ak0 f3;
        int i = this.m0;
        float f4 = Float.MAX_VALUE;
        if (i != 0) {
            if (i == 1) {
                f3 = this.u.f();
            } else if (i == 2) {
                f = this.t.f();
            } else if (i == 3) {
                f3 = this.v.f();
            } else {
                return;
            }
            f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            f = f3;
        } else {
            f = this.s.f();
        }
        int size = this.n0.size();
        ak0 ak02 = null;
        float f5 = f4;
        int i2 = 0;
        while (i2 < size) {
            ak0 ak03 = this.n0.get(i2);
            if (ak03.b == 1) {
                int i3 = this.m0;
                if (i3 == 0 || i3 == 2) {
                    f2 = ak03.g;
                    if (f2 < f5) {
                        ak0 = ak03.f;
                    }
                    ak0 = ak02;
                    f2 = f5;
                } else {
                    f2 = ak03.g;
                    if (f2 > f5) {
                        ak0 = ak03.f;
                    }
                    ak0 = ak02;
                    f2 = f5;
                }
                i2++;
                f5 = f2;
                ak02 = ak0;
            } else {
                return;
            }
        }
        if (kj0.x() != null) {
            kj0.x().y++;
        }
        f.f = ak02;
        f.g = f5;
        f.b();
        int i4 = this.m0;
        if (i4 == 0) {
            this.u.f().l(ak02, f5);
        } else if (i4 == 1) {
            this.s.f().l(ak02, f5);
        } else if (i4 == 2) {
            this.v.f().l(ak02, f5);
        } else if (i4 == 3) {
            this.t.f().l(ak02, f5);
        }
    }

    @DexIgnore
    @Override // com.fossil.uj0
    public void b(kj0 kj0) {
        Object[] objArr;
        boolean z;
        int i;
        int i2;
        tj0[] tj0Arr = this.A;
        tj0Arr[0] = this.s;
        tj0Arr[2] = this.t;
        tj0Arr[1] = this.u;
        tj0Arr[3] = this.v;
        int i3 = 0;
        while (true) {
            objArr = this.A;
            if (i3 >= objArr.length) {
                break;
            }
            objArr[i3].i = kj0.r(objArr[i3]);
            i3++;
        }
        int i4 = this.m0;
        if (i4 >= 0 && i4 < 4) {
            tj0 tj0 = objArr[i4];
            int i5 = 0;
            while (true) {
                if (i5 >= this.l0) {
                    z = false;
                    break;
                }
                uj0 uj0 = this.k0[i5];
                if ((this.o0 || uj0.c()) && ((((i = this.m0) == 0 || i == 1) && uj0.s() == uj0.b.MATCH_CONSTRAINT) || (((i2 = this.m0) == 2 || i2 == 3) && uj0.B() == uj0.b.MATCH_CONSTRAINT))) {
                    z = true;
                } else {
                    i5++;
                }
            }
            int i6 = this.m0;
            if (i6 == 0 || i6 == 1 ? u().s() == uj0.b.WRAP_CONTENT : u().B() == uj0.b.WRAP_CONTENT) {
                z = false;
            }
            for (int i7 = 0; i7 < this.l0; i7++) {
                uj0 uj02 = this.k0[i7];
                if (this.o0 || uj02.c()) {
                    oj0 r = kj0.r(uj02.A[this.m0]);
                    tj0[] tj0Arr2 = uj02.A;
                    int i8 = this.m0;
                    tj0Arr2[i8].i = r;
                    if (i8 == 0 || i8 == 2) {
                        kj0.j(tj0.i, r, z);
                    } else {
                        kj0.h(tj0.i, r, z);
                    }
                }
            }
            int i9 = this.m0;
            if (i9 == 0) {
                kj0.e(this.u.i, this.s.i, 0, 6);
                if (!z) {
                    kj0.e(this.s.i, this.D.u.i, 0, 5);
                }
            } else if (i9 == 1) {
                kj0.e(this.s.i, this.u.i, 0, 6);
                if (!z) {
                    kj0.e(this.s.i, this.D.s.i, 0, 5);
                }
            } else if (i9 == 2) {
                kj0.e(this.v.i, this.t.i, 0, 6);
                if (!z) {
                    kj0.e(this.t.i, this.D.v.i, 0, 5);
                }
            } else if (i9 == 3) {
                kj0.e(this.t.i, this.v.i, 0, 6);
                if (!z) {
                    kj0.e(this.t.i, this.D.t.i, 0, 5);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.uj0
    public boolean c() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.uj0
    public void d(int i) {
        ak0 f;
        uj0 uj0 = this.D;
        if (uj0 != null && ((vj0) uj0).X0(2)) {
            int i2 = this.m0;
            if (i2 == 0) {
                f = this.s.f();
            } else if (i2 == 1) {
                f = this.u.f();
            } else if (i2 == 2) {
                f = this.t.f();
            } else if (i2 == 3) {
                f = this.v.f();
            } else {
                return;
            }
            f.p(5);
            int i3 = this.m0;
            if (i3 == 0 || i3 == 1) {
                this.t.f().l(null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.v.f().l(null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            } else {
                this.s.f().l(null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.u.f().l(null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            this.n0.clear();
            for (int i4 = 0; i4 < this.l0; i4++) {
                uj0 uj02 = this.k0[i4];
                if (this.o0 || uj02.c()) {
                    int i5 = this.m0;
                    ak0 f2 = i5 != 0 ? i5 != 1 ? i5 != 2 ? i5 != 3 ? null : uj02.v.f() : uj02.t.f() : uj02.u.f() : uj02.s.f();
                    if (f2 != null) {
                        this.n0.add(f2);
                        f2.a(f);
                    }
                }
            }
        }
    }
}
