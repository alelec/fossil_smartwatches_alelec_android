package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s90 implements Parcelable.Creator<t90> {
    @DexIgnore
    public /* synthetic */ s90(kq7 kq7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public t90 createFromParcel(Parcel parcel) {
        Object[] createTypedArray = parcel.createTypedArray(tu1.CREATOR);
        if (createTypedArray != null) {
            pq7.b(createTypedArray, "parcel.createTypedArray(MicroAppMapping.CREATOR)!!");
            tu1[] tu1Arr = (tu1[]) createTypedArray;
            Parcelable readParcelable = parcel.readParcelable(ry1.class.getClassLoader());
            if (readParcelable != null) {
                return new t90(tu1Arr, (ry1) readParcelable);
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public t90[] newArray(int i) {
        return new t90[i];
    }
}
