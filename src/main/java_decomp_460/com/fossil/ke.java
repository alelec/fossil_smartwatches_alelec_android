package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ke {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ HashMap<cl7<String, ob>, Byte> f1902a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ ke b; // = new ke();

    @DexIgnore
    public final short a(String str, ob obVar) {
        return obVar.b;
    }

    @DexIgnore
    public final short b(String str, ob obVar) {
        Byte b2 = f1902a.get(new cl7(str, obVar));
        byte byteValue = b2 != null ? b2.byteValue() : 0;
        switch (je.b[obVar.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return obVar.b;
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
                return obVar.a((byte) 0);
            case 15:
            case 16:
                return obVar.a((byte) 254);
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
                f1902a.put(new cl7<>(str, obVar), Byte.valueOf((byte) ((hy1.p(byteValue) + 1) % hy1.p((byte) -1))));
                return obVar.a(byteValue);
            default:
                throw new al7();
        }
    }
}
