package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m58 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f2305a; // = 74;
    @DexIgnore
    public int b; // = 1;
    @DexIgnore
    public int c; // = 3;
    @DexIgnore
    public String d; // = System.getProperty("line.separator");
    @DexIgnore
    public String e; // = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
    @DexIgnore
    public String f; // = "--";
    @DexIgnore
    public Comparator g; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Comparator {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // java.util.Comparator
        public int compare(Object obj, Object obj2) {
            return ((p58) obj).getKey().compareToIgnoreCase(((p58) obj2).getKey());
        }
    }

    @DexIgnore
    public String a(int i) {
        StringBuffer stringBuffer = new StringBuffer(i);
        for (int i2 = 0; i2 < i; i2++) {
            stringBuffer.append(' ');
        }
        return stringBuffer.toString();
    }

    @DexIgnore
    public int b(String str, int i, int i2) {
        int indexOf = str.indexOf(10, i2);
        if ((indexOf != -1 && indexOf <= i) || ((indexOf = str.indexOf(9, i2)) != -1 && indexOf <= i)) {
            return indexOf + 1;
        }
        int i3 = i + i2;
        if (i3 >= str.length()) {
            return -1;
        }
        int i4 = i3;
        while (i4 >= i2) {
            char charAt = str.charAt(i4);
            if (charAt == ' ' || charAt == '\n' || charAt == '\r') {
                break;
            }
            i4--;
        }
        if (i4 > i2) {
            return i4;
        }
        while (i3 <= str.length() && (r2 = str.charAt(i3)) != ' ' && r2 != '\n' && r2 != '\r') {
            i3++;
        }
        if (i3 == str.length()) {
            i3 = -1;
        }
        return i3;
    }

    @DexIgnore
    public int c() {
        return this.c;
    }

    @DexIgnore
    public int d() {
        return this.b;
    }

    @DexIgnore
    public Comparator e() {
        return this.g;
    }

    @DexIgnore
    public int f() {
        return this.f2305a;
    }

    @DexIgnore
    public void g(PrintWriter printWriter, int i, s58 s58, int i2, int i3) {
        StringBuffer stringBuffer = new StringBuffer();
        h(stringBuffer, i, s58, i2, i3);
        printWriter.println(stringBuffer.toString());
    }

    @DexIgnore
    public StringBuffer h(StringBuffer stringBuffer, int i, s58 s58, int i2, int i3) {
        String a2 = a(i2);
        String a3 = a(i3);
        ArrayList arrayList = new ArrayList();
        List<p58> helpOptions = s58.helpOptions();
        Collections.sort(helpOptions, e());
        int i4 = 0;
        int i5 = 0;
        for (p58 p58 : helpOptions) {
            StringBuffer stringBuffer2 = new StringBuffer(8);
            if (p58.getOpt() == null) {
                stringBuffer2.append(a2);
                StringBuffer stringBuffer3 = new StringBuffer();
                stringBuffer3.append("   ");
                stringBuffer3.append(this.f);
                stringBuffer2.append(stringBuffer3.toString());
                stringBuffer2.append(p58.getLongOpt());
            } else {
                stringBuffer2.append(a2);
                stringBuffer2.append(this.e);
                stringBuffer2.append(p58.getOpt());
                if (p58.hasLongOpt()) {
                    stringBuffer2.append(',');
                    stringBuffer2.append(this.f);
                    stringBuffer2.append(p58.getLongOpt());
                }
            }
            if (p58.hasArg()) {
                if (p58.hasArgName()) {
                    stringBuffer2.append(" <");
                    stringBuffer2.append(p58.getArgName());
                    stringBuffer2.append(SimpleComparison.GREATER_THAN_OPERATION);
                } else {
                    stringBuffer2.append(' ');
                }
            }
            arrayList.add(stringBuffer2);
            if (stringBuffer2.length() > i5) {
                i5 = stringBuffer2.length();
            }
        }
        Iterator it = helpOptions.iterator();
        while (it.hasNext()) {
            p58 p582 = (p58) it.next();
            StringBuffer stringBuffer4 = new StringBuffer(arrayList.get(i4).toString());
            if (stringBuffer4.length() < i5) {
                stringBuffer4.append(a(i5 - stringBuffer4.length()));
            }
            stringBuffer4.append(a3);
            if (p582.getDescription() != null) {
                stringBuffer4.append(p582.getDescription());
            }
            i(stringBuffer, i, i5 + i3, stringBuffer4.toString());
            if (it.hasNext()) {
                stringBuffer.append(this.d);
            }
            i4++;
        }
        return stringBuffer;
    }

    @DexIgnore
    public StringBuffer i(StringBuffer stringBuffer, int i, int i2, String str) {
        int b2 = b(str, i, 0);
        if (b2 == -1) {
            stringBuffer.append(j(str));
        } else {
            stringBuffer.append(j(str.substring(0, b2)));
            stringBuffer.append(this.d);
            if (i2 >= i) {
                i2 = 1;
            }
            String a2 = a(i2);
            while (true) {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append(a2);
                stringBuffer2.append(str.substring(b2).trim());
                str = stringBuffer2.toString();
                b2 = b(str, i, 0);
                if (b2 == -1) {
                    break;
                }
                if (str.length() > i && b2 == i2 - 1) {
                    b2 = i;
                }
                stringBuffer.append(j(str.substring(0, b2)));
                stringBuffer.append(this.d);
            }
            stringBuffer.append(str);
        }
        return stringBuffer;
    }

    @DexIgnore
    public String j(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        int length = str.length();
        while (length > 0 && Character.isWhitespace(str.charAt(length - 1))) {
            length--;
        }
        return str.substring(0, length);
    }
}
