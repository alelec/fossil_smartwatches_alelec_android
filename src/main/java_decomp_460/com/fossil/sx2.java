package com.fossil;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sx2<E> extends tx2<E> implements List<E>, RandomAccess {
    @DexIgnore
    public static /* final */ bz2<Object> c; // = new vx2(qy2.zza, 0);

    @DexIgnore
    public static <E> sx2<E> zza() {
        return (sx2<E>) qy2.zza;
    }

    @DexIgnore
    public static <E> sx2<E> zza(E e) {
        Object[] objArr = {e};
        for (int i = 0; i < 1; i++) {
            oy2.a(objArr[i], i);
        }
        return zza(objArr, 1);
    }

    @DexIgnore
    public static <E> sx2<E> zza(Object[] objArr) {
        return zza(objArr, objArr.length);
    }

    @DexIgnore
    public static <E> sx2<E> zza(Object[] objArr, int i) {
        return i == 0 ? (sx2<E>) qy2.zza : new qy2(objArr, i);
    }

    @DexIgnore
    @Override // java.util.List
    @Deprecated
    public final void add(int i, E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.List
    @Deprecated
    public final boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public boolean contains(@NullableDecl Object obj) {
        return indexOf(obj) >= 0;
    }

    @DexIgnore
    public boolean equals(@NullableDecl Object obj) {
        sw2.b(this);
        if (obj == this) {
            return true;
        }
        if (obj instanceof List) {
            List list = (List) obj;
            int size = size();
            if (size == list.size()) {
                if (list instanceof RandomAccess) {
                    for (int i = 0; i < size; i++) {
                        if (qw2.a(get(i), list.get(i))) {
                        }
                    }
                    return true;
                }
                int size2 = size();
                Iterator<E> it = list.iterator();
                int i2 = 0;
                while (true) {
                    if (i2 < size2) {
                        if (!it.hasNext()) {
                            break;
                        }
                        E e = get(i2);
                        i2++;
                        if (!qw2.a(e, it.next())) {
                            break;
                        }
                    } else if (!it.hasNext()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = (i * 31) + get(i2).hashCode();
        }
        return i;
    }

    @DexIgnore
    public int indexOf(@NullableDecl Object obj) {
        int i = 0;
        if (obj == null) {
            return -1;
        }
        int size = size();
        if (obj == null) {
            while (i < size) {
                if (get(i) == null) {
                    return i;
                }
                i++;
            }
            return -1;
        }
        while (i < size) {
            if (obj.equals(get(i))) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public int lastIndexOf(@NullableDecl Object obj) {
        if (obj == null) {
            return -1;
        }
        if (obj == null) {
            for (int size = size() - 1; size >= 0; size--) {
                if (get(size) == null) {
                    return size;
                }
            }
            return -1;
        }
        for (int size2 = size() - 1; size2 >= 0; size2--) {
            if (obj.equals(get(size2))) {
                return size2;
            }
        }
        return -1;
    }

    @DexIgnore
    @Override // java.util.List
    public /* synthetic */ ListIterator listIterator() {
        return (bz2) listIterator(0);
    }

    @DexIgnore
    @Override // java.util.List
    public /* synthetic */ ListIterator listIterator(int i) {
        sw2.g(i, size());
        return isEmpty() ? c : new vx2(this, i);
    }

    @DexIgnore
    @Override // java.util.List
    @Deprecated
    public final E remove(int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.List
    @Deprecated
    public final E set(int i, E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    /* renamed from: zza */
    public sx2<E> subList(int i, int i2) {
        sw2.e(i, i2, size());
        int i3 = i2 - i;
        return i3 == size() ? this : i3 == 0 ? (sx2<E>) qy2.zza : new xx2(this, i, i3);
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public int zzb(Object[] objArr, int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            objArr[i + i2] = get(i2);
        }
        return i + size;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    /* renamed from: zzb */
    public final cz2<E> iterator() {
        return (bz2) listIterator();
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final sx2<E> zzc() {
        return this;
    }

    @DexIgnore
    public sx2<E> zzd() {
        return size() <= 1 ? this : new ux2(this);
    }
}
