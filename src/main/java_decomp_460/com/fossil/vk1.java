package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum vk1 implements zx1 {
    BLUETOOTH_OFF(0, 255),
    LOCATION_PERMISSION_NOT_GRANTED(247, 255),
    LOCATION_SERVICE_NOT_ENABLED(248, 255),
    ALREADY_STARTED(249, 1),
    REGISTRATION_FAILED(250, 2),
    SYSTEM_INTERNAL_ERROR(251, 3),
    UNSUPPORTED(252, 4),
    OUT_OF_HARDWARE_RESOURCE(253, 5),
    SCANNING_TOO_FREQUENTLY(254, 6),
    UNKNOWN_ERROR(255, 255);
    
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public /* final */ String b; // = ey1.a(this);
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final vk1 a(int i) {
            vk1 vk1;
            vk1[] values = vk1.values();
            int length = values.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    vk1 = null;
                    break;
                }
                vk1 = values[i2];
                if (vk1.d == i) {
                    break;
                }
                i2++;
            }
            return vk1 != null ? vk1 : vk1.UNKNOWN_ERROR;
        }
    }

    @DexIgnore
    public vk1(int i, int i2) {
        this.c = i;
        this.d = i2;
    }

    @DexIgnore
    @Override // com.fossil.zx1
    public int getCode() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.zx1
    public String getLogName() {
        return this.b;
    }
}
