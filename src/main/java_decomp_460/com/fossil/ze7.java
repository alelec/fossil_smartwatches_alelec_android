package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.ye7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ze7 implements ye7.a {
    @DexIgnore
    public ze7() {
        new Handler(Looper.getMainLooper());
    }

    @DexIgnore
    @Override // com.fossil.ye7.a
    public final void a(String str, String str2) {
        if (ye7.f4308a <= 1) {
            Log.d(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.ye7.a
    public final void b(String str, String str2) {
        if (ye7.f4308a <= 2) {
            Log.i(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.ye7.a
    public final int c() {
        return ye7.f4308a;
    }

    @DexIgnore
    @Override // com.fossil.ye7.a
    public final void d(String str, String str2) {
        if (ye7.f4308a <= 3) {
            Log.w(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.ye7.a
    public final void i(String str, String str2) {
        if (ye7.f4308a <= 4) {
            Log.e(str, str2);
        }
    }
}
