package com.fossil;

import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class v81 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f3731a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[q51.values().length];
        f3731a = iArr;
        iArr[q51.MEMORY.ordinal()] = 1;
        f3731a[q51.DISK.ordinal()] = 2;
        f3731a[q51.NETWORK.ordinal()] = 3;
        int[] iArr2 = new int[ImageView.ScaleType.values().length];
        b = iArr2;
        iArr2[ImageView.ScaleType.FIT_START.ordinal()] = 1;
        b[ImageView.ScaleType.FIT_CENTER.ordinal()] = 2;
        b[ImageView.ScaleType.FIT_END.ordinal()] = 3;
        b[ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 4;
    }
    */
}
