package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Timer;
import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qx4 extends ts0 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MutableLiveData<Boolean> f3043a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<gl7<ks4, String, Integer>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Boolean, ps4>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Boolean, ServerError>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> f; // = new MutableLiveData<>();
    @DexIgnore
    public Timer g;
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public LiveData<ps4> i;
    @DexIgnore
    public Timer j;
    @DexIgnore
    public /* final */ tt4 k;
    @DexIgnore
    public /* final */ on5 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return qx4.m;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ qx4 f3044a;

        @DexIgnore
        public b(qx4 qx4) {
            this.f3044a = qx4;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<ps4> apply(ps4 ps4) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = qx4.n.a();
            local.e(a2, "switchMap - challenge: " + ps4);
            MutableLiveData<ps4> mutableLiveData = new MutableLiveData<>();
            mutableLiveData.l(ps4);
            if (ps4 == null) {
                Timer timer = this.f3044a.g;
                if (timer != null) {
                    timer.cancel();
                }
                if (!this.f3044a.h) {
                    this.f3044a.b.l(Boolean.TRUE);
                    Timer timer2 = this.f3044a.g;
                    if (timer2 != null) {
                        timer2.cancel();
                    }
                    Timer timer3 = this.f3044a.j;
                    if (timer3 != null) {
                        timer3.cancel();
                    }
                    this.f3044a.l.e(0L);
                    this.f3044a.f.l(Boolean.TRUE);
                }
            } else {
                this.f3044a.F(py4.a(ps4));
                this.f3044a.I();
                this.f3044a.A(ps4);
                this.f3044a.E(ps4);
            }
            if (this.f3044a.h) {
                this.f3044a.h = false;
                this.f3044a.G();
            }
            return mutableLiveData;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel", f = "BCCurrentChallengeSubTabViewModel.kt", l = {277, 297}, m = "challengesFromServer")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qx4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(qx4 qx4, qn7 qn7) {
            super(qn7);
            this.this$0 = qx4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.q(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$challengesFromServer$localChallenge$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super ps4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qx4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(qx4 qx4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = qx4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super ps4> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return this.this$0.k.g(new String[]{"running", "waiting"}, xy4.f4212a.a());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$completedChallenge$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {196}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ps4 $currentChallenge;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qx4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$completedChallenge$1$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {197, 200}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends bt4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends bt4>>> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                iv7 iv7;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv72 = this.p$;
                    this.L$0 = iv72;
                    this.label = 1;
                    if (uv7.a(1000, this) == d) {
                        return d;
                    }
                    iv7 = iv72;
                } else if (i == 1) {
                    el7.b(obj);
                    iv7 = (iv7) this.L$0;
                } else if (i == 2) {
                    iv7 iv73 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.this$0.k.i(this.this$0.$currentChallenge.f());
                tt4 tt4 = this.this$0.this$0.k;
                this.L$0 = iv7;
                this.label = 2;
                Object s = tt4.s(tt4, 5, 0, this, 2, null);
                return s == d ? d : s;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(qx4 qx4, ps4 ps4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = qx4;
            this.$currentChallenge = ps4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, this.$currentChallenge, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(b, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$initDisplayPlayer$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ int $target;
        @DexIgnore
        public /* final */ /* synthetic */ String $type;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qx4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(qx4 qx4, String str, String str2, int i, qn7 qn7) {
            super(2, qn7);
            this.this$0 = qx4;
            this.$challengeId = str;
            this.$type = str2;
            this.$target = i;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, this.$challengeId, this.$type, this.$target, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                ks4 j = this.this$0.k.j(this.$challengeId);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = qx4.n.a();
                local.e(a2, "initDisplayPlayer - bcDisplayPlayer: " + j);
                if (j != null) {
                    this.this$0.c.l(new gl7(j, this.$type, ao7.e(this.$target)));
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$refresh$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {177, 181}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qx4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(qx4 qx4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = qx4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, qn7);
            gVar.p$ = (iv7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0061  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 2
                r4 = 1
                java.lang.Object r1 = com.fossil.yn7.d()
                int r0 = r6.label
                if (r0 == 0) goto L_0x0073
                if (r0 == r4) goto L_0x0038
                if (r0 != r5) goto L_0x0030
                java.lang.Object r0 = r6.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r7)
                r0 = r7
            L_0x0016:
                com.fossil.kz4 r0 = (com.fossil.kz4) r0
                java.lang.Object r0 = r0.c()
                com.fossil.pt4 r0 = (com.fossil.pt4) r0
                if (r0 == 0) goto L_0x002d
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.h0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                java.lang.String r0 = r0.a()
                r1.m1(r0)
            L_0x002d:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x002f:
                return r0
            L_0x0030:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0038:
                java.lang.Object r0 = r6.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r7)
            L_0x003f:
                com.fossil.qx4 r2 = r6.this$0
                androidx.lifecycle.MutableLiveData r2 = com.fossil.qx4.h(r2)
                r3 = 0
                java.lang.Boolean r3 = com.fossil.ao7.a(r3)
                r2.l(r3)
                com.fossil.qx4 r2 = r6.this$0
                com.fossil.on5 r2 = com.fossil.qx4.b(r2)
                java.lang.Boolean r2 = r2.l0()
                java.lang.Boolean r3 = com.fossil.ao7.a(r4)
                boolean r2 = com.fossil.pq7.a(r2, r3)
                if (r2 == 0) goto L_0x002d
                com.fossil.qx4 r2 = r6.this$0
                com.fossil.tt4 r2 = com.fossil.qx4.a(r2)
                r6.L$0 = r0
                r6.label = r5
                java.lang.Object r0 = r2.v(r6)
                if (r0 != r1) goto L_0x0016
                r0 = r1
                goto L_0x002f
            L_0x0073:
                com.fossil.el7.b(r7)
                com.fossil.iv7 r0 = r6.p$
                com.fossil.qx4 r2 = r6.this$0
                r6.L$0 = r0
                r6.label = r4
                java.lang.Object r2 = r2.q(r6)
                if (r2 != r1) goto L_0x003f
                r0 = r1
                goto L_0x002f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.qx4.g.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends TimerTask {
        @DexIgnore
        public /* final */ /* synthetic */ qx4 b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(qn7 qn7, h hVar) {
                super(2, qn7);
                this.this$0 = hVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object q;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    tt4 tt4 = this.this$0.b.k;
                    List i2 = hm7.i(this.this$0.c);
                    this.L$0 = iv7;
                    this.label = 1;
                    q = tt4.q(tt4, i2, 3, 3, false, this, 8, null);
                    if (q == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    q = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List<ks4> list = (List) ((kz4) q).c();
                if (list != null && (!list.isEmpty())) {
                    for (ks4 ks4 : list) {
                        if (ao7.a(pq7.a(ks4.a(), this.this$0.c)).booleanValue()) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String a2 = qx4.n.a();
                            local.e(a2, " setupFetchDisplayPlayersTimer - displayPlayer: " + ks4);
                            MutableLiveData mutableLiveData = this.this$0.b.c;
                            h hVar = this.this$0;
                            mutableLiveData.l(new gl7(ks4, hVar.d, ao7.e(hVar.e)));
                        }
                    }
                    throw new NoSuchElementException("Collection contains no element matching the predicate.");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public h(qx4 qx4, String str, String str2, int i) {
            this.b = qx4;
            this.c = str;
            this.d = str2;
            this.e = i;
        }

        @DexIgnore
        public void run() {
            xw7 unused = gu7.d(us0.a(this.b), bw7.b(), null, new a(null, this), 2, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends TimerTask {
        @DexIgnore
        public /* final */ /* synthetic */ qx4 b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;
        @DexIgnore
        public /* final */ /* synthetic */ ps4 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.qx4$i$a$a")
            /* renamed from: com.fossil.qx4$i$a$a  reason: collision with other inner class name */
            public static final class C0202a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0202a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0202a aVar = new C0202a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0202a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        FLogger.INSTANCE.getLocal().e(qx4.n.a(), "starAndStopChallenge - startChallenge");
                        ps4 a2 = this.this$0.this$0.b.k.a(this.this$0.this$0.d.f());
                        if (a2 != null) {
                            a2.v("running");
                            this.this$0.this$0.b.k.B(a2);
                        }
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends bt4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    b bVar = new b(this.this$0, qn7);
                    bVar.p$ = (iv7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends bt4>>> qn7) {
                    return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    iv7 iv7;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv72 = this.p$;
                        FLogger.INSTANCE.getLocal().e(qx4.n.a(), "starAndStopChallenge - stopChallenge");
                        this.L$0 = iv72;
                        this.label = 1;
                        if (uv7.a(1000, this) == d) {
                            return d;
                        }
                        iv7 = iv72;
                    } else if (i == 1) {
                        el7.b(obj);
                        iv7 = (iv7) this.L$0;
                    } else if (i == 2) {
                        iv7 iv73 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    this.this$0.this$0.b.k.i(this.this$0.this$0.d.f());
                    tt4 tt4 = this.this$0.this$0.b.k;
                    this.L$0 = iv7;
                    this.label = 2;
                    Object s = tt4.s(tt4, 5, 0, this, 2, null);
                    return s == d ? d : s;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(qn7 qn7, i iVar) {
                super(2, qn7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    if (this.this$0.c) {
                        dv7 b2 = bw7.b();
                        C0202a aVar = new C0202a(this, null);
                        this.L$0 = iv7;
                        this.label = 1;
                        if (eu7.g(b2, aVar, this) == d) {
                            return d;
                        }
                    } else {
                        dv7 b3 = bw7.b();
                        b bVar = new b(this, null);
                        this.L$0 = iv7;
                        this.label = 2;
                        if (eu7.g(b3, bVar, this) == d) {
                            return d;
                        }
                    }
                } else if (i == 1 || i == 2) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public i(qx4 qx4, boolean z, ps4 ps4) {
            this.b = qx4;
            this.c = z;
            this.d = ps4;
        }

        @DexIgnore
        public void run() {
            xw7 unused = gu7.d(us0.a(this.b), null, null, new a(null, this), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$start$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {94}, m = "invokeSuspend")
    public static final class j extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qx4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$start$1$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {94}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ j this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(j jVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = jVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    qx4 qx4 = this.this$0.this$0;
                    this.L$0 = iv7;
                    this.label = 1;
                    if (qx4.q(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(qx4 qx4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = qx4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            j jVar = new j(this.this$0, qn7);
            jVar.p$ = (iv7) obj;
            return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((j) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(b, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = qx4.class.getSimpleName();
        pq7.b(simpleName, "BCCurrentChallengeSubTab\u2026el::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public qx4(tt4 tt4, on5 on5) {
        pq7.c(tt4, "challengeRepository");
        pq7.c(on5, "shared");
        this.k = tt4;
        this.l = on5;
        String[] strArr = {"running", "waiting"};
        LiveData<ps4> c2 = ss0.c(cz4.a(this.k.h(strArr, xy4.f4212a.a())), new b(this));
        pq7.b(c2, "Transformations.switchMa\u2026    }\n\n        live\n    }");
        this.i = c2;
    }

    @DexIgnore
    public final void A(ps4 ps4) {
        String f2 = ps4.f();
        String r = ps4.r();
        if (r == null) {
            r = "activity_reach_goal";
        }
        Integer q = ps4.q();
        int intValue = q != null ? q.intValue() : -1;
        String n2 = ps4.n();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "initDisplayPlayer - id: " + f2 + " - type: " + r + " - target: " + intValue + " -status: " + n2);
        if (pq7.a("running", n2)) {
            xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new f(this, f2, r, intValue, null), 2, null);
        }
    }

    @DexIgnore
    public final void B() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "pause - playerTimer: " + this.g);
        Timer timer = this.g;
        if (timer != null) {
            timer.cancel();
        }
        Timer timer2 = this.j;
        if (timer2 != null) {
            timer2.cancel();
        }
    }

    @DexIgnore
    public final void C() {
        xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new g(this, null), 2, null);
    }

    @DexIgnore
    public final void D() {
        ps4 e2 = this.i.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "resume - challenge: " + e2);
        if (e2 != null) {
            F(py4.a(e2));
            A(e2);
            E(e2);
        }
    }

    @DexIgnore
    public final void E(ps4 ps4) {
        String f2 = ps4.f();
        String r = ps4.r();
        String str = r != null ? r : "activity_reach_goal";
        Integer q = ps4.q();
        int intValue = q != null ? q.intValue() : -1;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.e(str2, "scheduleFetchDisplayPlayers - challenge: " + ps4);
        if (pq7.a("running", ps4.n())) {
            Timer timer = this.g;
            if (timer != null) {
                timer.cancel();
            }
            Timer timer2 = new Timer();
            this.g = timer2;
            if (timer2 != null) {
                timer2.schedule(new h(this, f2, str, intValue), 0, 15000);
            }
        }
    }

    @DexIgnore
    public final void F(ps4 ps4) {
        long b2;
        long j2 = 0;
        Date m2 = ps4.m();
        long time = m2 != null ? m2.getTime() : 0;
        Date e2 = ps4.e();
        long time2 = e2 != null ? e2.getTime() : 0;
        boolean z = false;
        if (!pq7.a("waiting", ps4.n())) {
            b2 = time2 - xy4.f4212a.b();
        } else if (xy4.f4212a.b() < time2) {
            b2 = time - xy4.f4212a.b();
            z = true;
        } else {
            b2 = 0;
        }
        FLogger.INSTANCE.getLocal().e(m, "starAndStopChallenge - startTime: " + time + " - endTime: " + time2 + " - timeLeft: " + b2 + " - isStart: " + z);
        if (b2 >= 0) {
            j2 = b2;
        }
        Timer timer = this.j;
        if (timer != null) {
            timer.cancel();
        }
        Timer timer2 = new Timer();
        this.j = timer2;
        if (timer2 != null) {
            timer2.schedule(new i(this, z, ps4), j2);
        }
    }

    @DexIgnore
    public final void G() {
        xw7 unused = gu7.d(us0.a(this), null, null, new j(this, null), 3, null);
    }

    @DexIgnore
    public final void H() {
        String n2;
        ps4 e2 = this.i.e();
        if (e2 != null && (n2 = e2.n()) != null) {
            if (pq7.a("waiting", n2)) {
                this.d.l(hl7.a(Boolean.FALSE, e2));
            } else {
                this.d.l(hl7.a(Boolean.TRUE, e2));
            }
        }
    }

    @DexIgnore
    public final void I() {
        this.f3043a.l(Boolean.FALSE);
        this.b.l(Boolean.FALSE);
    }

    @DexIgnore
    public final void J(boolean z) {
        this.f.l(Boolean.valueOf(z));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object q(com.fossil.qn7<? super com.fossil.tl7> r15) {
        /*
        // Method dump skipped, instructions count: 370
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qx4.q(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void r() {
        if (this.l.d().longValue() <= xy4.f4212a.b() || !pq7.a(this.l.l0(), Boolean.TRUE)) {
            this.f.l(Boolean.TRUE);
        } else {
            this.f.l(Boolean.FALSE);
        }
    }

    @DexIgnore
    public final void s() {
        ps4 e2 = this.i.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "completedChallenge - current: " + e2);
        if (e2 != null) {
            xw7 unused = gu7.d(us0.a(this), null, null, new e(this, e2, null), 3, null);
            Timer timer = this.g;
            if (timer != null) {
                timer.cancel();
            }
            this.l.e(0L);
            this.f.l(Boolean.TRUE);
            this.b.l(Boolean.TRUE);
        }
    }

    @DexIgnore
    public final LiveData<ps4> t() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<gl7<ks4, String, Integer>> u() {
        return cz4.a(this.c);
    }

    @DexIgnore
    public final LiveData<Boolean> v() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, ServerError>> w() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Boolean> x() {
        return this.f3043a;
    }

    @DexIgnore
    public final LiveData<Boolean> y() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, ps4>> z() {
        return this.d;
    }
}
