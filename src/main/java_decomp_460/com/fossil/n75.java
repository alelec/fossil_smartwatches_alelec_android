package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n75 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ lg5 F;
    @DexIgnore
    public /* final */ RTLImageView G;
    @DexIgnore
    public /* final */ ConstraintLayout H;
    @DexIgnore
    public /* final */ LinearLayout I;
    @DexIgnore
    public /* final */ ConstraintLayout J;
    @DexIgnore
    public /* final */ LinearLayout K;
    @DexIgnore
    public /* final */ LinearLayout L;
    @DexIgnore
    public /* final */ LinearLayout M;
    @DexIgnore
    public /* final */ ConstraintLayout N;
    @DexIgnore
    public /* final */ RecyclerView O;
    @DexIgnore
    public /* final */ SwitchCompat P;
    @DexIgnore
    public /* final */ FlexibleTextView Q;
    @DexIgnore
    public /* final */ View R;
    @DexIgnore
    public /* final */ View S;
    @DexIgnore
    public /* final */ View T;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public n75(Object obj, View view, int i, FlexibleButton flexibleButton, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, FlexibleTextView flexibleTextView11, FlexibleTextView flexibleTextView12, lg5 lg5, RTLImageView rTLImageView, ConstraintLayout constraintLayout3, LinearLayout linearLayout, ConstraintLayout constraintLayout4, LinearLayout linearLayout2, LinearLayout linearLayout3, LinearLayout linearLayout4, ConstraintLayout constraintLayout5, RecyclerView recyclerView, SwitchCompat switchCompat, FlexibleTextView flexibleTextView13, View view2, View view3, View view4) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = flexibleTextView;
        this.u = flexibleTextView2;
        this.v = flexibleTextView3;
        this.w = flexibleTextView4;
        this.x = flexibleTextView5;
        this.y = flexibleTextView6;
        this.z = flexibleTextView7;
        this.A = flexibleTextView8;
        this.B = flexibleTextView9;
        this.C = flexibleTextView10;
        this.D = flexibleTextView11;
        this.E = flexibleTextView12;
        this.F = lg5;
        x(lg5);
        this.G = rTLImageView;
        this.H = constraintLayout3;
        this.I = linearLayout;
        this.J = constraintLayout4;
        this.K = linearLayout2;
        this.L = linearLayout3;
        this.M = linearLayout4;
        this.N = constraintLayout5;
        this.O = recyclerView;
        this.P = switchCompat;
        this.Q = flexibleTextView13;
        this.R = view2;
        this.S = view3;
        this.T = view4;
    }
}
