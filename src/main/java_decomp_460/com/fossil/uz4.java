package com.fossil;

import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.model.FileType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uz4 {
    @DexIgnore
    public final String a(FileType fileType) {
        if (fileType != null) {
            return fileType.getMValue();
        }
        return null;
    }

    @DexIgnore
    public final FileType b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        FileType.Companion companion = FileType.Companion;
        if (str != null) {
            return companion.from(str);
        }
        pq7.i();
        throw null;
    }
}
