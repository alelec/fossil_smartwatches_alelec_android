package com.fossil;

import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y37 {
    @DexIgnore
    public static ActivitySummary a(DateTime dateTime, double d, double d2, double d3, int i, List<Integer> list, String str) {
        TimeZone timeZone = TimeZone.getTimeZone(str);
        if (timeZone == null || !timeZone.getID().equals(str)) {
            timeZone = TimeZone.getDefault();
        }
        return new ActivitySummary(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), timeZone.getID(), Integer.valueOf(timeZone.inDaylightTime(dateTime.toDate()) ? timeZone.getDSTSavings() : 0), d, d2, d3, list, i, 0, 0, 0);
    }
}
