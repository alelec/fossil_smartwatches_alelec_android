package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o22 implements Factory<Integer> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ o22 f2619a; // = new o22();

    @DexIgnore
    public static o22 a() {
        return f2619a;
    }

    @DexIgnore
    public static int c() {
        return m22.b();
    }

    @DexIgnore
    /* renamed from: b */
    public Integer get() {
        return Integer.valueOf(c());
    }
}
