package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tp1 extends vp1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ du1 d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<tp1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public tp1 createFromParcel(Parcel parcel) {
            return new tp1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public tp1[] newArray(int i) {
            return new tp1[i];
        }
    }

    @DexIgnore
    public tp1(byte b, String str, du1 du1) {
        super(np1.COMMUTE_TIME_WATCH_APP, b);
        this.e = str;
        this.d = du1;
    }

    @DexIgnore
    public /* synthetic */ tp1(Parcel parcel, kq7 kq7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.e = readString;
            this.d = du1.values()[parcel.readInt()];
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(tp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            tp1 tp1 = (tp1) obj;
            if (this.d != tp1.d) {
                return false;
            }
            return !(pq7.a(this.e, tp1.e) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.CommuteTimeWatchAppNotification");
    }

    @DexIgnore
    public final du1 getAction() {
        return this.d;
    }

    @DexIgnore
    public final String getDestination() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public int hashCode() {
        int hashCode = super.hashCode();
        return (((hashCode * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.e);
        }
        if (parcel != null) {
            parcel.writeInt(this.d.ordinal());
        }
    }
}
