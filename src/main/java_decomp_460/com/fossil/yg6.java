package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.mv5;
import com.fossil.nk5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yg6 extends pv5 implements xg6 {
    @DexIgnore
    public g37<d35> g;
    @DexIgnore
    public wg6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "CaloriesOverviewWeekFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void K6() {
        d35 a2;
        OverviewWeekChart overviewWeekChart;
        g37<d35> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewWeekChart = a2.q) != null) {
            nk5.a aVar = nk5.o;
            wg6 wg6 = this.h;
            if (aVar.w(wg6 != null ? wg6.n() : null)) {
                overviewWeekChart.D("dianaActiveCaloriesTab", "nonBrandNonReachGoal");
            } else {
                overviewWeekChart.D("hybridActiveCaloriesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    /* renamed from: L6 */
    public void M5(wg6 wg6) {
        pq7.c(wg6, "presenter");
        this.h = wg6;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        d35 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onCreateView");
        this.g = new g37<>(this, (d35) aq0.f(layoutInflater, 2131558511, viewGroup, false, A6()));
        K6();
        g37<d35> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onResume");
        K6();
        wg6 wg6 = this.h;
        if (wg6 != null) {
            wg6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onStop");
        wg6 wg6 = this.h;
        if (wg6 != null) {
            wg6.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.xg6
    public void p(mv5 mv5) {
        d35 a2;
        OverviewWeekChart overviewWeekChart;
        pq7.c(mv5, "baseModel");
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "showWeekDetails");
        g37<d35> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewWeekChart = a2.q) != null) {
            new ArrayList();
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(mv5.f2426a.b(cVar.d()));
            mv5.a aVar = mv5.f2426a;
            pq7.b(overviewWeekChart, "it");
            Context context = overviewWeekChart.getContext();
            pq7.b(context, "it.context");
            BarChart.H(overviewWeekChart, aVar.a(context, cVar), false, 2, null);
            overviewWeekChart.r(mv5);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
