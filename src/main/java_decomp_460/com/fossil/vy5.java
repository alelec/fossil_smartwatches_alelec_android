package com.fossil;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vy5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ float f3855a;
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ float d;
    @DexIgnore
    public /* final */ b e;
    @DexIgnore
    public /* final */ PointF f; // = new PointF();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f3856a;

        /*
        static {
            int[] iArr = new int[b.values().length];
            f3856a = iArr;
            try {
                iArr[b.TOP_LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f3856a[b.TOP_RIGHT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f3856a[b.BOTTOM_LEFT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f3856a[b.BOTTOM_RIGHT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f3856a[b.LEFT.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f3856a[b.TOP.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f3856a[b.RIGHT.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                f3856a[b.BOTTOM.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                f3856a[b.CENTER.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
        }
        */
    }

    @DexIgnore
    public enum b {
        TOP_LEFT,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT,
        LEFT,
        TOP,
        RIGHT,
        BOTTOM,
        CENTER
    }

    /*
    static {
        new Matrix();
    }
    */

    @DexIgnore
    public vy5(b bVar, uy5 uy5, float f2, float f3) {
        this.e = bVar;
        this.f3855a = uy5.e();
        this.b = uy5.d();
        this.c = uy5.c();
        this.d = uy5.b();
        l(uy5.h(), f2, f3);
    }

    @DexIgnore
    public static float k(float f2, float f3, float f4, float f5) {
        return (f4 - f2) / (f5 - f3);
    }

    @DexIgnore
    public final void a(RectF rectF, float f2, RectF rectF2, int i, float f3, float f4, boolean z, boolean z2) {
        float f5 = (float) i;
        if (f2 > f5) {
            f2 = ((f2 - f5) / 1.05f) + f5;
            this.f.y -= (f2 - f5) / 1.1f;
        }
        float f6 = rectF2.bottom;
        if (f2 > f6) {
            this.f.y -= (f2 - f6) / 2.0f;
        }
        float f7 = rectF2.bottom;
        if (f7 - f2 < f3) {
            f2 = f7;
        }
        float f8 = rectF.top;
        float f9 = this.b;
        if (f2 - f8 < f9) {
            f2 = f8 + f9;
        }
        float f10 = rectF.top;
        float f11 = this.d;
        if (f2 - f10 > f11) {
            f2 = f10 + f11;
        }
        float f12 = rectF2.bottom;
        if (f12 - f2 >= f3) {
            f12 = f2;
        }
        if (f4 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float f13 = rectF.top;
            float f14 = (f12 - f13) * f4;
            float f15 = this.f3855a;
            if (f14 < f15) {
                f12 = Math.min(rectF2.bottom, (f15 / f4) + f13);
                f14 = (f12 - rectF.top) * f4;
            }
            float f16 = this.c;
            if (f14 > f16) {
                f12 = Math.min(rectF2.bottom, rectF.top + (f16 / f4));
                f14 = (f12 - rectF.top) * f4;
            }
            if (!z || !z2) {
                if (z) {
                    float f17 = rectF.right;
                    float f18 = rectF2.left;
                    if (f17 - f14 < f18) {
                        f12 = Math.min(rectF2.bottom, rectF.top + ((f17 - f18) / f4));
                        f14 = (f12 - rectF.top) * f4;
                    }
                }
                if (z2) {
                    float f19 = rectF.left;
                    float f20 = rectF2.right;
                    if (f14 + f19 > f20) {
                        f12 = Math.min(f12, Math.min(rectF2.bottom, ((f20 - f19) / f4) + rectF.top));
                    }
                }
            } else {
                f12 = Math.min(f12, Math.min(rectF2.bottom, rectF.top + (rectF2.width() / f4)));
            }
        }
        rectF.bottom = f12;
    }

    @DexIgnore
    public final void b(RectF rectF, float f2) {
        rectF.bottom = rectF.top + (rectF.width() / f2);
    }

    @DexIgnore
    public final void c(RectF rectF, float f2, RectF rectF2, float f3, float f4, boolean z, boolean z2) {
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            f2 /= 1.05f;
            this.f.x -= f2 / 1.1f;
        }
        float f5 = rectF2.left;
        if (f2 < f5) {
            this.f.x -= (f2 - f5) / 2.0f;
        }
        float f6 = rectF2.left;
        if (f2 - f6 < f3) {
            f2 = f6;
        }
        float f7 = rectF.right;
        float f8 = this.f3855a;
        if (f7 - f2 < f8) {
            f2 = f7 - f8;
        }
        float f9 = rectF.right;
        float f10 = this.c;
        if (f9 - f2 > f10) {
            f2 = f9 - f10;
        }
        float f11 = rectF2.left;
        if (f2 - f11 >= f3) {
            f11 = f2;
        }
        if (f4 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float f12 = rectF.right;
            float f13 = (f12 - f11) / f4;
            float f14 = this.b;
            if (f13 < f14) {
                f11 = Math.max(rectF2.left, f12 - (f14 * f4));
                f13 = (rectF.right - f11) / f4;
            }
            float f15 = this.d;
            if (f13 > f15) {
                f11 = Math.max(rectF2.left, rectF.right - (f15 * f4));
                f13 = (rectF.right - f11) / f4;
            }
            if (!z || !z2) {
                if (z) {
                    float f16 = rectF.bottom;
                    float f17 = rectF2.top;
                    if (f16 - f13 < f17) {
                        f11 = Math.max(rectF2.left, rectF.right - ((f16 - f17) * f4));
                        f13 = (rectF.right - f11) / f4;
                    }
                }
                if (z2) {
                    float f18 = rectF.top;
                    float f19 = rectF2.bottom;
                    if (f13 + f18 > f19) {
                        f11 = Math.max(f11, Math.max(rectF2.left, rectF.right - ((f19 - f18) * f4)));
                    }
                }
            } else {
                f11 = Math.max(f11, Math.max(rectF2.left, rectF.right - (rectF2.height() * f4)));
            }
        }
        rectF.left = f11;
    }

    @DexIgnore
    public final void d(RectF rectF, float f2) {
        rectF.left = rectF.right - (rectF.height() * f2);
    }

    @DexIgnore
    public final void e(RectF rectF, RectF rectF2, float f2) {
        rectF.inset((rectF.width() - (rectF.height() * f2)) / 2.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float f3 = rectF.left;
        float f4 = rectF2.left;
        if (f3 < f4) {
            rectF.offset(f4 - f3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        float f5 = rectF.right;
        float f6 = rectF2.right;
        if (f5 > f6) {
            rectF.offset(f6 - f5, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public final void f(RectF rectF, float f2, RectF rectF2, int i, float f3, float f4, boolean z, boolean z2) {
        float f5 = (float) i;
        if (f2 > f5) {
            f2 = ((f2 - f5) / 1.05f) + f5;
            this.f.x -= (f2 - f5) / 1.1f;
        }
        float f6 = rectF2.right;
        if (f2 > f6) {
            this.f.x -= (f2 - f6) / 2.0f;
        }
        float f7 = rectF2.right;
        if (f7 - f2 < f3) {
            f2 = f7;
        }
        float f8 = rectF.left;
        float f9 = this.f3855a;
        if (f2 - f8 < f9) {
            f2 = f8 + f9;
        }
        float f10 = rectF.left;
        float f11 = this.c;
        if (f2 - f10 > f11) {
            f2 = f10 + f11;
        }
        float f12 = rectF2.right;
        if (f12 - f2 >= f3) {
            f12 = f2;
        }
        if (f4 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float f13 = rectF.left;
            float f14 = (f12 - f13) / f4;
            float f15 = this.b;
            if (f14 < f15) {
                f12 = Math.min(rectF2.right, (f15 * f4) + f13);
                f14 = (f12 - rectF.left) / f4;
            }
            float f16 = this.d;
            if (f14 > f16) {
                f12 = Math.min(rectF2.right, rectF.left + (f16 * f4));
                f14 = (f12 - rectF.left) / f4;
            }
            if (!z || !z2) {
                if (z) {
                    float f17 = rectF.bottom;
                    float f18 = rectF2.top;
                    if (f17 - f14 < f18) {
                        f12 = Math.min(rectF2.right, rectF.left + ((f17 - f18) * f4));
                        f14 = (f12 - rectF.left) / f4;
                    }
                }
                if (z2) {
                    float f19 = rectF.top;
                    float f20 = rectF2.bottom;
                    if (f14 + f19 > f20) {
                        f12 = Math.min(f12, Math.min(rectF2.right, ((f20 - f19) * f4) + rectF.left));
                    }
                }
            } else {
                f12 = Math.min(f12, Math.min(rectF2.right, rectF.left + (rectF2.height() * f4)));
            }
        }
        rectF.right = f12;
    }

    @DexIgnore
    public final void g(RectF rectF, float f2) {
        rectF.right = rectF.left + (rectF.height() * f2);
    }

    @DexIgnore
    public final void h(RectF rectF, float f2, RectF rectF2, float f3, float f4, boolean z, boolean z2) {
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            f2 /= 1.05f;
            this.f.y -= f2 / 1.1f;
        }
        float f5 = rectF2.top;
        if (f2 < f5) {
            this.f.y -= (f2 - f5) / 2.0f;
        }
        float f6 = rectF2.top;
        if (f2 - f6 < f3) {
            f2 = f6;
        }
        float f7 = rectF.bottom;
        float f8 = this.b;
        if (f7 - f2 < f8) {
            f2 = f7 - f8;
        }
        float f9 = rectF.bottom;
        float f10 = this.d;
        if (f9 - f2 > f10) {
            f2 = f9 - f10;
        }
        float f11 = rectF2.top;
        if (f2 - f11 >= f3) {
            f11 = f2;
        }
        if (f4 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float f12 = rectF.bottom;
            float f13 = (f12 - f11) * f4;
            float f14 = this.f3855a;
            if (f13 < f14) {
                f11 = Math.max(rectF2.top, f12 - (f14 / f4));
                f13 = (rectF.bottom - f11) * f4;
            }
            float f15 = this.c;
            if (f13 > f15) {
                f11 = Math.max(rectF2.top, rectF.bottom - (f15 / f4));
                f13 = (rectF.bottom - f11) * f4;
            }
            if (!z || !z2) {
                if (z) {
                    float f16 = rectF.right;
                    float f17 = rectF2.left;
                    if (f16 - f13 < f17) {
                        f11 = Math.max(rectF2.top, rectF.bottom - ((f16 - f17) / f4));
                        f13 = (rectF.bottom - f11) * f4;
                    }
                }
                if (z2) {
                    float f18 = rectF.left;
                    float f19 = rectF2.right;
                    if (f13 + f18 > f19) {
                        f11 = Math.max(f11, Math.max(rectF2.top, rectF.bottom - ((f19 - f18) / f4)));
                    }
                }
            } else {
                f11 = Math.max(f11, Math.max(rectF2.top, rectF.bottom - (rectF2.width() / f4)));
            }
        }
        rectF.top = f11;
    }

    @DexIgnore
    public final void i(RectF rectF, RectF rectF2, float f2) {
        rectF.inset(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (rectF.height() - (rectF.width() / f2)) / 2.0f);
        float f3 = rectF.top;
        float f4 = rectF2.top;
        if (f3 < f4) {
            rectF.offset(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f4 - f3);
        }
        float f5 = rectF.bottom;
        float f6 = rectF2.bottom;
        if (f5 > f6) {
            rectF.offset(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f6 - f5);
        }
    }

    @DexIgnore
    public final void j(RectF rectF, float f2) {
        rectF.top = rectF.bottom - (rectF.width() / f2);
    }

    @DexIgnore
    public final void l(RectF rectF, float f2, float f3) {
        float f4;
        float f5;
        float f6;
        float f7;
        float f8;
        float f9 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        switch (a.f3856a[this.e.ordinal()]) {
            case 1:
                f9 = rectF.left - f2;
                f4 = rectF.top;
                f7 = f4 - f3;
                f8 = f9;
                break;
            case 2:
                f9 = rectF.right - f2;
                f4 = rectF.top;
                f7 = f4 - f3;
                f8 = f9;
                break;
            case 3:
                f9 = rectF.left - f2;
                f4 = rectF.bottom;
                f7 = f4 - f3;
                f8 = f9;
                break;
            case 4:
                f9 = rectF.right - f2;
                f4 = rectF.bottom;
                f7 = f4 - f3;
                f8 = f9;
                break;
            case 5:
                f5 = rectF.left;
                f6 = f5 - f2;
                f7 = 0.0f;
                f8 = f6;
                break;
            case 6:
                f4 = rectF.top;
                f7 = f4 - f3;
                f8 = f9;
                break;
            case 7:
                f5 = rectF.right;
                f6 = f5 - f2;
                f7 = 0.0f;
                f8 = f6;
                break;
            case 8:
                f4 = rectF.bottom;
                f7 = f4 - f3;
                f8 = f9;
                break;
            case 9:
                f9 = rectF.centerX() - f2;
                f4 = rectF.centerY();
                f7 = f4 - f3;
                f8 = f9;
                break;
            default:
                f6 = 0.0f;
                f7 = 0.0f;
                f8 = f6;
                break;
        }
        PointF pointF = this.f;
        pointF.x = f8;
        pointF.y = f7;
    }

    @DexIgnore
    public void m(RectF rectF, float f2, float f3, RectF rectF2, int i, int i2, float f4, boolean z, float f5) {
        PointF pointF = this.f;
        float f6 = f2 + pointF.x;
        float f7 = f3 + pointF.y;
        if (this.e == b.CENTER) {
            n(rectF, f6, f7, rectF2, i, i2, f4);
        } else if (z) {
            o(rectF, f6, f7, rectF2, i, i2, f4, f5);
        } else {
            p(rectF, f6, f7, rectF2, i, i2, f4);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005b, code lost:
        if ((r3 + r1) <= r10.bottom) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002b, code lost:
        if ((r3 + r0) <= r10.right) goto L_0x003c;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void n(android.graphics.RectF r7, float r8, float r9, android.graphics.RectF r10, int r11, int r12, float r13) {
        /*
            r6 = this;
            float r0 = r7.centerX()
            float r0 = r8 - r0
            float r1 = r7.centerY()
            float r1 = r9 - r1
            float r2 = r7.left
            float r3 = r2 + r0
            r4 = 0
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 < 0) goto L_0x002d
            float r3 = r7.right
            float r4 = r3 + r0
            float r5 = (float) r11
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 > 0) goto L_0x002d
            float r2 = r2 + r0
            float r4 = r10.left
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 < 0) goto L_0x002d
            float r2 = r3 + r0
            float r3 = r10.right
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x003c
        L_0x002d:
            r2 = 1065772646(0x3f866666, float:1.05)
            float r0 = r0 / r2
            android.graphics.PointF r2 = r6.f
            float r3 = r2.x
            r4 = 1073741824(0x40000000, float:2.0)
            float r4 = r0 / r4
            float r3 = r3 - r4
            r2.x = r3
        L_0x003c:
            float r2 = r7.top
            float r3 = r2 + r1
            r4 = 0
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 < 0) goto L_0x005d
            float r3 = r7.bottom
            float r4 = r3 + r1
            float r5 = (float) r12
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 > 0) goto L_0x005d
            float r2 = r2 + r1
            float r4 = r10.top
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 < 0) goto L_0x005d
            float r2 = r3 + r1
            float r3 = r10.bottom
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x006c
        L_0x005d:
            r2 = 1065772646(0x3f866666, float:1.05)
            float r1 = r1 / r2
            android.graphics.PointF r2 = r6.f
            float r3 = r2.y
            r4 = 1073741824(0x40000000, float:2.0)
            float r4 = r1 / r4
            float r3 = r3 - r4
            r2.y = r3
        L_0x006c:
            r7.offset(r0, r1)
            r6.q(r7, r10, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vy5.n(android.graphics.RectF, float, float, android.graphics.RectF, int, int, float):void");
    }

    @DexIgnore
    public final void o(RectF rectF, float f2, float f3, RectF rectF2, int i, int i2, float f4, float f5) {
        switch (a.f3856a[this.e.ordinal()]) {
            case 1:
                if (k(f2, f3, rectF.right, rectF.bottom) < f5) {
                    h(rectF, f3, rectF2, f4, f5, true, false);
                    d(rectF, f5);
                    return;
                }
                c(rectF, f2, rectF2, f4, f5, true, false);
                j(rectF, f5);
                return;
            case 2:
                if (k(rectF.left, f3, f2, rectF.bottom) < f5) {
                    h(rectF, f3, rectF2, f4, f5, false, true);
                    g(rectF, f5);
                    return;
                }
                f(rectF, f2, rectF2, i, f4, f5, true, false);
                j(rectF, f5);
                return;
            case 3:
                if (k(f2, rectF.top, rectF.right, f3) < f5) {
                    a(rectF, f3, rectF2, i2, f4, f5, true, false);
                    d(rectF, f5);
                    return;
                }
                c(rectF, f2, rectF2, f4, f5, false, true);
                b(rectF, f5);
                return;
            case 4:
                if (k(rectF.left, rectF.top, f2, f3) < f5) {
                    a(rectF, f3, rectF2, i2, f4, f5, false, true);
                    g(rectF, f5);
                    return;
                }
                f(rectF, f2, rectF2, i, f4, f5, false, true);
                b(rectF, f5);
                return;
            case 5:
                c(rectF, f2, rectF2, f4, f5, true, true);
                i(rectF, rectF2, f5);
                return;
            case 6:
                h(rectF, f3, rectF2, f4, f5, true, true);
                e(rectF, rectF2, f5);
                return;
            case 7:
                f(rectF, f2, rectF2, i, f4, f5, true, true);
                i(rectF, rectF2, f5);
                return;
            case 8:
                a(rectF, f3, rectF2, i2, f4, f5, true, true);
                e(rectF, rectF2, f5);
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void p(RectF rectF, float f2, float f3, RectF rectF2, int i, int i2, float f4) {
        switch (a.f3856a[this.e.ordinal()]) {
            case 1:
                h(rectF, f3, rectF2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                c(rectF, f2, rectF2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            case 2:
                h(rectF, f3, rectF2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                f(rectF, f2, rectF2, i, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            case 3:
                a(rectF, f3, rectF2, i2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                c(rectF, f2, rectF2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            case 4:
                a(rectF, f3, rectF2, i2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                f(rectF, f2, rectF2, i, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            case 5:
                c(rectF, f2, rectF2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            case 6:
                h(rectF, f3, rectF2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            case 7:
                f(rectF, f2, rectF2, i, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            case 8:
                a(rectF, f3, rectF2, i2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void q(RectF rectF, RectF rectF2, float f2) {
        float f3 = rectF.left;
        float f4 = rectF2.left;
        if (f3 < f4 + f2) {
            rectF.offset(f4 - f3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        float f5 = rectF.top;
        float f6 = rectF2.top;
        if (f5 < f6 + f2) {
            rectF.offset(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f6 - f5);
        }
        float f7 = rectF.right;
        float f8 = rectF2.right;
        if (f7 > f8 - f2) {
            rectF.offset(f8 - f7, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        float f9 = rectF.bottom;
        float f10 = rectF2.bottom;
        if (f9 > f10 - f2) {
            rectF.offset(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f10 - f9);
        }
    }
}
