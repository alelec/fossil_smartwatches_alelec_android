package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bg extends lp {
    @DexIgnore
    public lw1 C;
    @DexIgnore
    public /* final */ lw1 D;

    @DexIgnore
    public bg(k5 k5Var, i60 i60, lw1 lw1) {
        super(k5Var, i60, yp.M0, null, false, 24);
        this.D = lw1;
        this.C = lw1;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        if (this.x.a().getUiPackageOSVersion().isCompatible(this.C.h(), true)) {
            xw7 unused = gu7.d(id0.i.e(), null, null, new vq(this, null), 3, null);
            return;
        }
        kw1 edit = this.C.edit();
        if (edit != null) {
            xw7 unused2 = gu7.d(id0.i.e(), null, null, new jr(this, edit, null), 3, null);
        } else {
            l(nr.a(this.v, null, zq.INCOMPATIBLE_FIRMWARE, null, null, 13));
        }
    }

    @DexIgnore
    public final void J() {
        lp.h(this, new wk(this.w, this.x, this.C.getBundleId(), null, 8), new ze(this), new nf(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        return this.C;
    }
}
