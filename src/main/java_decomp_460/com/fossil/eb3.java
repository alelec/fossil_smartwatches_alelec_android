package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eb3 extends sq2 {
    @DexIgnore
    public /* final */ /* synthetic */ ot3 b;

    @DexIgnore
    public eb3(ea3 ea3, ot3 ot3) {
        this.b = ot3;
    }

    @DexIgnore
    @Override // com.fossil.rq2
    public final void M(pq2 pq2) throws RemoteException {
        Status a2 = pq2.a();
        if (a2 == null) {
            this.b.d(new n62(new Status(8, "Got null status from location service")));
        } else if (a2.f() == 0) {
            this.b.c(Boolean.TRUE);
        } else {
            this.b.d(xb2.a(a2));
        }
    }
}
