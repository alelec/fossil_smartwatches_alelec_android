package com.fossil;

import com.fossil.af1;
import java.io.InputStream;
import java.net.URL;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qf1 implements af1<URL, InputStream> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ af1<te1, InputStream> f2972a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements bf1<URL, InputStream> {
        @DexIgnore
        @Override // com.fossil.bf1
        public af1<URL, InputStream> b(ef1 ef1) {
            return new qf1(ef1.d(te1.class, InputStream.class));
        }
    }

    @DexIgnore
    public qf1(af1<te1, InputStream> af1) {
        this.f2972a = af1;
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<InputStream> b(URL url, int i, int i2, ob1 ob1) {
        return this.f2972a.b(new te1(url), i, i2, ob1);
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(URL url) {
        return true;
    }
}
