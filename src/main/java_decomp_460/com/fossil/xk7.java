package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xk7 implements Comparable<xk7> {
    @DexIgnore
    public static /* final */ xk7 f; // = new xk7(1, 3, 72);
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public xk7(int i, int i2, int i3) {
        this.c = i;
        this.d = i2;
        this.e = i3;
        this.b = b(i, i2, i3);
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(xk7 xk7) {
        pq7.c(xk7, FacebookRequestErrorClassification.KEY_OTHER);
        return this.b - xk7.b;
    }

    @DexIgnore
    public final int b(int i, int i2, int i3) {
        if (i >= 0 && 255 >= i && i2 >= 0 && 255 >= i2 && i3 >= 0 && 255 >= i3) {
            return (i << 16) + (i2 << 8) + i3;
        }
        throw new IllegalArgumentException(("Version components are out of range: " + i + '.' + i2 + '.' + i3).toString());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        xk7 xk7 = (xk7) (!(obj instanceof xk7) ? null : obj);
        if (xk7 == null) {
            return false;
        }
        return this.b == xk7.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0007: IGET  (r1v0 int) = (r3v0 'this' com.fossil.xk7 A[IMMUTABLE_TYPE, THIS]) com.fossil.xk7.c int), ('.' char), (wrap: int : 0x000f: IGET  (r1v1 int) = (r3v0 'this' com.fossil.xk7 A[IMMUTABLE_TYPE, THIS]) com.fossil.xk7.d int), ('.' char), (wrap: int : 0x0017: IGET  (r1v2 int) = (r3v0 'this' com.fossil.xk7 A[IMMUTABLE_TYPE, THIS]) com.fossil.xk7.e int)] */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.c);
        sb.append('.');
        sb.append(this.d);
        sb.append('.');
        sb.append(this.e);
        return sb.toString();
    }
}
