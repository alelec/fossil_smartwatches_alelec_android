package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class of5 extends nf5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d s; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray t; // = null;
    @DexIgnore
    public long r;

    @DexIgnore
    public of5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 1, s, t));
    }

    @DexIgnore
    public of5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleTextView) objArr[0]);
        this.r = -1;
        this.q.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.r = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.r != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.r = 1;
        }
        w();
    }
}
