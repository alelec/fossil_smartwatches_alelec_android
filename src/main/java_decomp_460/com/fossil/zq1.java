package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zq1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ox1 implements Parcelable {
        @DexIgnore
        public static /* final */ C0308a CREATOR; // = new C0308a(null);
        @DexIgnore
        public /* final */ nn1 b;
        @DexIgnore
        public /* final */ long c;
        @DexIgnore
        public /* final */ byte[] d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.zq1$a$a")
        /* renamed from: com.fossil.zq1$a$a  reason: collision with other inner class name */
        public static final class C0308a implements Parcelable.Creator<a> {
            @DexIgnore
            public /* synthetic */ C0308a(kq7 kq7) {
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // android.os.Parcelable.Creator
            public a createFromParcel(Parcel parcel) {
                nn1 nn1 = nn1.values()[parcel.readInt()];
                long readLong = parcel.readLong();
                byte[] createByteArray = parcel.createByteArray();
                if (createByteArray != null) {
                    pq7.b(createByteArray, "parcel.createByteArray()!!");
                    return new a(nn1, readLong, createByteArray);
                }
                pq7.i();
                throw null;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object[]' to match base method */
            @Override // android.os.Parcelable.Creator
            public a[] newArray(int i) {
                return new a[i];
            }
        }

        @DexIgnore
        public a(nn1 nn1, long j, byte[] bArr) {
            this.b = nn1;
            this.c = j;
            this.d = bArr;
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public final nn1 getChannel() {
            return this.b;
        }

        @DexIgnore
        public final byte[] getData() {
            return this.d;
        }

        @DexIgnore
        public final long getTimestampInMs() {
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.ox1
        public JSONObject toJSONObject() {
            return g80.k(g80.k(g80.k(new JSONObject(), jd0.D2, ey1.a(this.b)), jd0.Q0, Double.valueOf(hy1.f(this.c))), jd0.Y2, dy1.e(this.d, null, 1, null));
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.b.ordinal());
            parcel.writeLong(this.c);
            parcel.writeByteArray(this.d);
        }
    }
}
