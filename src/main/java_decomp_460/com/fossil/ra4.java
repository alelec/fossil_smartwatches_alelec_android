package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ra4 extends ta4.d.e {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f3091a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.d.e.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Integer f3092a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Boolean d;

        @DexIgnore
        @Override // com.fossil.ta4.d.e.a
        public ta4.d.e a() {
            String str = "";
            if (this.f3092a == null) {
                str = " platform";
            }
            if (this.b == null) {
                str = str + " version";
            }
            if (this.c == null) {
                str = str + " buildVersion";
            }
            if (this.d == null) {
                str = str + " jailbroken";
            }
            if (str.isEmpty()) {
                return new ra4(this.f3092a.intValue(), this.b, this.c, this.d.booleanValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.e.a
        public ta4.d.e.a b(String str) {
            if (str != null) {
                this.c = str;
                return this;
            }
            throw new NullPointerException("Null buildVersion");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.e.a
        public ta4.d.e.a c(boolean z) {
            this.d = Boolean.valueOf(z);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.e.a
        public ta4.d.e.a d(int i) {
            this.f3092a = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.e.a
        public ta4.d.e.a e(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null version");
        }
    }

    @DexIgnore
    public ra4(int i, String str, String str2, boolean z) {
        this.f3091a = i;
        this.b = str;
        this.c = str2;
        this.d = z;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.e
    public String b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.e
    public int c() {
        return this.f3091a;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.e
    public String d() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.e
    public boolean e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.d.e)) {
            return false;
        }
        ta4.d.e eVar = (ta4.d.e) obj;
        return this.f3091a == eVar.c() && this.b.equals(eVar.d()) && this.c.equals(eVar.b()) && this.d == eVar.e();
    }

    @DexIgnore
    public int hashCode() {
        int i = this.f3091a;
        int hashCode = this.b.hashCode();
        return (this.d ? 1231 : 1237) ^ ((((((i ^ 1000003) * 1000003) ^ hashCode) * 1000003) ^ this.c.hashCode()) * 1000003);
    }

    @DexIgnore
    public String toString() {
        return "OperatingSystem{platform=" + this.f3091a + ", version=" + this.b + ", buildVersion=" + this.c + ", jailbroken=" + this.d + "}";
    }
}
