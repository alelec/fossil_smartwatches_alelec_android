package com.fossil;

import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ty7<E> extends jy7<E> {
    @DexIgnore
    public static /* final */ vz7 e; // = new vz7("EMPTY");
    @DexIgnore
    public /* final */ ReentrantLock c; // = new ReentrantLock();
    @DexIgnore
    public Object d; // = e;

    @DexIgnore
    @Override // com.fossil.ly7
    public String e() {
        return "(value=" + this.d + ')';
    }

    @DexIgnore
    @Override // com.fossil.jy7
    public boolean p(uy7<? super E> uy7) {
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            return super.p(uy7);
        } finally {
            reentrantLock.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.jy7
    public final boolean q() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.jy7
    public final boolean r() {
        return this.d == e;
    }

    @DexIgnore
    @Override // com.fossil.jy7
    public Object u() {
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            if (this.d == e) {
                Object f = f();
                if (f == null) {
                    f = ky7.b;
                }
                return f;
            }
            Object obj = this.d;
            this.d = e;
            tl7 tl7 = tl7.f3441a;
            reentrantLock.unlock();
            return obj;
        } finally {
            reentrantLock.unlock();
        }
    }

    @DexIgnore
    public Object x(E e2) {
        wy7<E> l;
        vz7 e3;
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            py7<?> f = f();
            if (f != null) {
                return f;
            }
            if (this.d == e) {
                do {
                    l = l();
                    if (l != null) {
                        if (l instanceof py7) {
                            if (l != null) {
                                reentrantLock.unlock();
                                return l;
                            }
                            pq7.i();
                            throw null;
                        } else if (l != null) {
                            e3 = l.e(e2, null);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                } while (e3 == null);
                if (nv7.a()) {
                    if (!(e3 == mu7.f2424a)) {
                        throw new AssertionError();
                    }
                }
                tl7 tl7 = tl7.f3441a;
                reentrantLock.unlock();
                if (l != null) {
                    l.d(e2);
                    if (l != null) {
                        return l.a();
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            this.d = e2;
            Object obj = ky7.f2119a;
            reentrantLock.unlock();
            return obj;
        } finally {
            reentrantLock.unlock();
        }
    }
}
