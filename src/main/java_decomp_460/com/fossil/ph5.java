package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ph5 {
    PORTFOLIO(0, "Diana"),
    KATESPADE(1, "Kate Spade"),
    MICHAELKORS(2, "Micheal Kors"),
    DIESEL(3, "Diesel"),
    SKAGEN(4, " Skagen"),
    CHAPS(5, "Chaps"),
    EA(6, "Emporio Armani"),
    AX(7, "Armani Exchange"),
    MJ(8, "Marc Jacobs"),
    RELIC(9, "Relic"),
    FOSSIL(10, "Fossil"),
    UNIVERSAL(11, "Hybrid time"),
    CITIZEN(12, "Citizen");
    
    @DexIgnore
    public /* final */ String name;
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public ph5(int i, String str) {
        this.value = i;
        this.name = str;
    }

    @DexIgnore
    public static ph5 fromInt(int i) {
        ph5[] values = values();
        for (ph5 ph5 : values) {
            if (ph5.value == i) {
                return ph5;
            }
        }
        return PORTFOLIO;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }
}
