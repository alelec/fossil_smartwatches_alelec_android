package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface g52 extends IInterface {
    @DexIgnore
    void M0(e52 e52, GoogleSignInOptions googleSignInOptions) throws RemoteException;

    @DexIgnore
    void d1(e52 e52, GoogleSignInOptions googleSignInOptions) throws RemoteException;
}
