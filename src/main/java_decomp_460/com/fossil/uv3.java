package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uv3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<uv3> CREATOR; // = new vv3();
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ byte i;
    @DexIgnore
    public /* final */ byte j;
    @DexIgnore
    public /* final */ byte k;
    @DexIgnore
    public /* final */ byte l;
    @DexIgnore
    public /* final */ String m;

    @DexIgnore
    public uv3(int i2, String str, String str2, String str3, String str4, String str5, String str6, byte b2, byte b3, byte b4, byte b5, String str7) {
        this.b = i2;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = str4;
        this.g = str5;
        this.h = str6;
        this.i = (byte) b2;
        this.j = (byte) b3;
        this.k = (byte) b4;
        this.l = (byte) b5;
        this.m = str7;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && uv3.class == obj.getClass()) {
            uv3 uv3 = (uv3) obj;
            if (this.b != uv3.b) {
                return false;
            }
            if (this.i != uv3.i) {
                return false;
            }
            if (this.j != uv3.j) {
                return false;
            }
            if (this.k != uv3.k) {
                return false;
            }
            if (this.l != uv3.l) {
                return false;
            }
            if (!this.c.equals(uv3.c)) {
                return false;
            }
            String str = this.d;
            if (str == null ? uv3.d != null : !str.equals(uv3.d)) {
                return false;
            }
            if (!this.e.equals(uv3.e)) {
                return false;
            }
            if (!this.f.equals(uv3.f)) {
                return false;
            }
            if (!this.g.equals(uv3.g)) {
                return false;
            }
            String str2 = this.h;
            if (str2 == null ? uv3.h != null : !str2.equals(uv3.h)) {
                return false;
            }
            String str3 = this.m;
            String str4 = uv3.m;
            if (str3 != null) {
                return str3.equals(str4);
            }
            if (str4 == null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final int hashCode() {
        int i2 = 0;
        int i3 = this.b;
        int hashCode = this.c.hashCode();
        String str = this.d;
        int hashCode2 = str != null ? str.hashCode() : 0;
        int hashCode3 = this.e.hashCode();
        int hashCode4 = this.f.hashCode();
        int hashCode5 = this.g.hashCode();
        String str2 = this.h;
        int hashCode6 = str2 != null ? str2.hashCode() : 0;
        byte b2 = this.i;
        byte b3 = this.j;
        byte b4 = this.k;
        byte b5 = this.l;
        String str3 = this.m;
        if (str3 != null) {
            i2 = str3.hashCode();
        }
        return ((((((((((((((((((hashCode2 + ((((i3 + 31) * 31) + hashCode) * 31)) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + b2) * 31) + b3) * 31) + b4) * 31) + b5) * 31) + i2;
    }

    @DexIgnore
    public final String toString() {
        int i2 = this.b;
        String str = this.c;
        String str2 = this.d;
        String str3 = this.e;
        String str4 = this.f;
        String str5 = this.g;
        String str6 = this.h;
        byte b2 = this.i;
        byte b3 = this.j;
        byte b4 = this.k;
        byte b5 = this.l;
        String str7 = this.m;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 211 + String.valueOf(str2).length() + String.valueOf(str3).length() + String.valueOf(str4).length() + String.valueOf(str5).length() + String.valueOf(str6).length() + String.valueOf(str7).length());
        sb.append("AncsNotificationParcelable{, id=");
        sb.append(i2);
        sb.append(", appId='");
        sb.append(str);
        sb.append('\'');
        sb.append(", dateTime='");
        sb.append(str2);
        sb.append('\'');
        sb.append(", notificationText='");
        sb.append(str3);
        sb.append('\'');
        sb.append(", title='");
        sb.append(str4);
        sb.append('\'');
        sb.append(", subtitle='");
        sb.append(str5);
        sb.append('\'');
        sb.append(", displayName='");
        sb.append(str6);
        sb.append('\'');
        sb.append(", eventId=");
        sb.append((int) b2);
        sb.append(", eventFlags=");
        sb.append((int) b3);
        sb.append(", categoryId=");
        sb.append((int) b4);
        sb.append(", categoryCount=");
        sb.append((int) b5);
        sb.append(", packageName='");
        sb.append(str7);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = bd2.a(parcel);
        bd2.n(parcel, 2, this.b);
        bd2.u(parcel, 3, this.c, false);
        bd2.u(parcel, 4, this.d, false);
        bd2.u(parcel, 5, this.e, false);
        bd2.u(parcel, 6, this.f, false);
        bd2.u(parcel, 7, this.g, false);
        String str = this.h;
        if (str == null) {
            str = this.c;
        }
        bd2.u(parcel, 8, str, false);
        bd2.f(parcel, 9, this.i);
        bd2.f(parcel, 10, this.j);
        bd2.f(parcel, 11, this.k);
        bd2.f(parcel, 12, this.l);
        bd2.u(parcel, 13, this.m, false);
        bd2.b(parcel, a2);
    }
}
