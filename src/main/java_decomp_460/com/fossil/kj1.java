package com.fossil;

import android.graphics.Bitmap;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kj1 extends nj1<Bitmap> {
    @DexIgnore
    public kj1(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    /* renamed from: q */
    public void o(Bitmap bitmap) {
        ((ImageView) this.b).setImageBitmap(bitmap);
    }
}
