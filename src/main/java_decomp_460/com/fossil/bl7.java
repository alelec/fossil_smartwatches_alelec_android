package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bl7 extends Error {
    @DexIgnore
    public bl7() {
        this(null, 1, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bl7(String str) {
        super(str);
        pq7.c(str, "message");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ bl7(String str, int i, kq7 kq7) {
        this((i & 1) != 0 ? "An operation is not implemented." : str);
    }
}
