package com.fossil;

import com.fossil.n18;
import com.fossil.p18;
import com.fossil.q18;
import com.fossil.s18;
import com.fossil.v18;
import java.io.IOException;
import java.util.regex.Pattern;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o88 {
    @DexIgnore
    public static /* final */ char[] l; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    @DexIgnore
    public static /* final */ Pattern m; // = Pattern.compile("(.*/)?(\\.|%2e|%2E){1,2}(/.*)?");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2648a;
    @DexIgnore
    public /* final */ q18 b;
    @DexIgnore
    public String c;
    @DexIgnore
    public q18.a d;
    @DexIgnore
    public /* final */ v18.a e; // = new v18.a();
    @DexIgnore
    public /* final */ p18.a f;
    @DexIgnore
    public r18 g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public s18.a i;
    @DexIgnore
    public n18.a j;
    @DexIgnore
    public RequestBody k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends RequestBody {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ RequestBody f2649a;
        @DexIgnore
        public /* final */ r18 b;

        @DexIgnore
        public a(RequestBody requestBody, r18 r18) {
            this.f2649a = requestBody;
            this.b = r18;
        }

        @DexIgnore
        @Override // okhttp3.RequestBody
        public long a() throws IOException {
            return this.f2649a.a();
        }

        @DexIgnore
        @Override // okhttp3.RequestBody
        public r18 b() {
            return this.b;
        }

        @DexIgnore
        @Override // okhttp3.RequestBody
        public void h(j48 j48) throws IOException {
            this.f2649a.h(j48);
        }
    }

    @DexIgnore
    public o88(String str, q18 q18, String str2, p18 p18, r18 r18, boolean z, boolean z2, boolean z3) {
        this.f2648a = str;
        this.b = q18;
        this.c = str2;
        this.g = r18;
        this.h = z;
        if (p18 != null) {
            this.f = p18.f();
        } else {
            this.f = new p18.a();
        }
        if (z2) {
            this.j = new n18.a();
        } else if (z3) {
            s18.a aVar = new s18.a();
            this.i = aVar;
            aVar.f(s18.f);
        }
    }

    @DexIgnore
    public static String g(String str, boolean z) {
        int length = str.length();
        int i2 = 0;
        while (i2 < length) {
            int codePointAt = str.codePointAt(i2);
            if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                i48 i48 = new i48();
                i48.E0(str, 0, i2);
                h(i48, str, i2, length, z);
                return i48.b0();
            }
            i2 += Character.charCount(codePointAt);
        }
        return str;
    }

    @DexIgnore
    public static void h(i48 i48, String str, int i2, int i3, boolean z) {
        i48 i482 = null;
        while (i2 < i3) {
            int codePointAt = str.codePointAt(i2);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                    if (i482 == null) {
                        i482 = new i48();
                    }
                    i482.F0(codePointAt);
                    while (!i482.u()) {
                        int readByte = i482.readByte() & 255;
                        i48.w0(37);
                        i48.w0(l[(readByte >> 4) & 15]);
                        i48.w0(l[readByte & 15]);
                    }
                } else {
                    i48.F0(codePointAt);
                }
            }
            i2 += Character.charCount(codePointAt);
        }
    }

    @DexIgnore
    public void a(String str, String str2, boolean z) {
        if (z) {
            this.j.b(str, str2);
        } else {
            this.j.a(str, str2);
        }
    }

    @DexIgnore
    public void b(String str, String str2) {
        if ("Content-Type".equalsIgnoreCase(str)) {
            try {
                this.g = r18.c(str2);
            } catch (IllegalArgumentException e2) {
                throw new IllegalArgumentException("Malformed content type: " + str2, e2);
            }
        } else {
            this.f.a(str, str2);
        }
    }

    @DexIgnore
    public void c(p18 p18, RequestBody requestBody) {
        this.i.c(p18, requestBody);
    }

    @DexIgnore
    public void d(s18.b bVar) {
        this.i.d(bVar);
    }

    @DexIgnore
    public void e(String str, String str2, boolean z) {
        if (this.c != null) {
            String g2 = g(str2, z);
            String str3 = this.c;
            String replace = str3.replace("{" + str + "}", g2);
            if (!m.matcher(replace).matches()) {
                this.c = replace;
                return;
            }
            throw new IllegalArgumentException("@Path parameters shouldn't perform path traversal ('.' or '..'): " + str2);
        }
        throw new AssertionError();
    }

    @DexIgnore
    public void f(String str, String str2, boolean z) {
        String str3 = this.c;
        if (str3 != null) {
            q18.a q = this.b.q(str3);
            this.d = q;
            if (q != null) {
                this.c = null;
            } else {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.b + ", Relative: " + this.c);
            }
        }
        if (z) {
            this.d.a(str, str2);
        } else {
            this.d.b(str, str2);
        }
    }

    @DexIgnore
    public v18.a i() {
        q18 D;
        q18.a aVar = this.d;
        if (aVar != null) {
            D = aVar.c();
        } else {
            D = this.b.D(this.c);
            if (D == null) {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.b + ", Relative: " + this.c);
            }
        }
        a aVar2 = this.k;
        if (aVar2 == null) {
            n18.a aVar3 = this.j;
            if (aVar3 != null) {
                aVar2 = aVar3.c();
            } else {
                s18.a aVar4 = this.i;
                if (aVar4 != null) {
                    aVar2 = aVar4.e();
                } else if (this.h) {
                    aVar2 = RequestBody.f(null, new byte[0]);
                }
            }
        }
        r18 r18 = this.g;
        if (r18 != null) {
            if (aVar2 != null) {
                aVar2 = new a(aVar2, r18);
            } else {
                this.f.a("Content-Type", r18.toString());
            }
        }
        v18.a aVar5 = this.e;
        aVar5.l(D);
        aVar5.f(this.f.e());
        aVar5.g(this.f2648a, aVar2);
        return aVar5;
    }

    @DexIgnore
    public void j(RequestBody requestBody) {
        this.k = requestBody;
    }

    @DexIgnore
    public void k(Object obj) {
        this.c = obj.toString();
    }
}
