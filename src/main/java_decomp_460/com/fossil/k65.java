package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentContainerView;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k65 extends j65 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d I; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray J;
    @DexIgnore
    public long H;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        J = sparseIntArray;
        sparseIntArray.put(2131362666, 1);
        J.put(2131363410, 2);
        J.put(2131362691, 3);
        J.put(2131363319, 4);
        J.put(2131363381, 5);
        J.put(2131363356, 6);
        J.put(2131362478, 7);
        J.put(2131363164, 8);
        J.put(2131362428, 9);
        J.put(2131362079, 10);
        J.put(2131362251, 11);
        J.put(2131362075, 12);
        J.put(2131362471, 13);
        J.put(2131362472, 14);
        J.put(2131362469, 15);
        J.put(2131362470, 16);
    }
    */

    @DexIgnore
    public k65(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 17, I, J));
    }

    @DexIgnore
    public k65(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[12], (ConstraintLayout) objArr[10], (FragmentContainerView) objArr[11], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[7], (RTLImageView) objArr[1], (ImageView) objArr[3], (ConstraintLayout) objArr[0], (FlexibleSwitchCompat) objArr[8], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[2]);
        this.H = -1;
        this.B.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.H = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.H != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.H = 1;
        }
        w();
    }
}
