package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ll4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f2215a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return this.f2215a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof ll4) {
            ll4 ll4 = (ll4) obj;
            return this.f2215a == ll4.f2215a && this.b == ll4.b;
        }
    }

    @DexIgnore
    public int hashCode() {
        return (this.f2215a * 32713) + this.b;
    }

    @DexIgnore
    public String toString() {
        return this.f2215a + "x" + this.b;
    }
}
