package com.fossil;

import android.os.Bundle;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yl6 extends fq4 {
    @DexIgnore
    public abstract void n(Date date);

    @DexIgnore
    public abstract void o(GoalTrackingData goalTrackingData);

    @DexIgnore
    public abstract void p(Date date);

    @DexIgnore
    public abstract void q();

    @DexIgnore
    public abstract void r();

    @DexIgnore
    public abstract void s(Bundle bundle);

    @DexIgnore
    public abstract void t(Date date);

    @DexIgnore
    public abstract void u();

    @DexIgnore
    public abstract void v();
}
