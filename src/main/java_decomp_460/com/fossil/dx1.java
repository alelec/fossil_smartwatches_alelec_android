package com.fossil;

import java.nio.charset.Charset;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dx1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Locale f845a;
    @DexIgnore
    public static /* final */ Charset b; // = et7.f986a;

    /*
    static {
        Locale locale = Locale.ROOT;
        pq7.b(locale, "Locale.ROOT");
        f845a = locale;
    }
    */

    @DexIgnore
    public static final Charset a() {
        return b;
    }

    @DexIgnore
    public static final Locale b() {
        return f845a;
    }
}
