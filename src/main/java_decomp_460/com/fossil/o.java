package com.fossil;

import com.fossil.yx1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o<V, E extends yx1> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ V f2599a;
    @DexIgnore
    public /* final */ E b;

    @DexIgnore
    public o(E e) {
        this.f2599a = null;
        this.b = e;
    }

    @DexIgnore
    public o(V v) {
        this.f2599a = v;
        this.b = null;
    }
}
