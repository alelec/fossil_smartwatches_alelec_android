package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z15 extends y15 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d t; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray u;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public long s;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        u = sparseIntArray;
        sparseIntArray.put(2131363554, 1);
    }
    */

    @DexIgnore
    public z15(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 2, t, u));
    }

    @DexIgnore
    public z15(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (OverviewWeekChart) objArr[1]);
        this.s = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.r = constraintLayout;
        constraintLayout.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.s = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.s != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.s = 1;
        }
        w();
    }
}
