package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum lv7 {
    DEFAULT,
    LAZY,
    ATOMIC,
    UNDISPATCHED;

    @DexIgnore
    public static /* synthetic */ void isLazy$annotations() {
    }

    @DexIgnore
    public final <T> void invoke(rp7<? super qn7<? super T>, ? extends Object> rp7, qn7<? super T> qn7) {
        int i = kv7.f2104a[ordinal()];
        if (i == 1) {
            d08.b(rp7, qn7);
        } else if (i == 2) {
            sn7.a(rp7, qn7);
        } else if (i == 3) {
            e08.a(rp7, qn7);
        } else if (i != 4) {
            throw new al7();
        }
    }

    @DexIgnore
    public final <R, T> void invoke(vp7<? super R, ? super qn7<? super T>, ? extends Object> vp7, R r, qn7<? super T> qn7) {
        int i = kv7.b[ordinal()];
        if (i == 1) {
            d08.c(vp7, r, qn7);
        } else if (i == 2) {
            sn7.b(vp7, r, qn7);
        } else if (i == 3) {
            e08.b(vp7, r, qn7);
        } else if (i != 4) {
            throw new al7();
        }
    }

    @DexIgnore
    public final boolean isLazy() {
        return this == LAZY;
    }
}
