package com.fossil;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p01 {
    @DexIgnore
    public static /* final */ p01 i; // = new a().a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public y01 f2758a; // = y01.NOT_REQUIRED;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public long f; // = -1;
    @DexIgnore
    public long g; // = -1;
    @DexIgnore
    public q01 h; // = new q01();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f2759a; // = false;
        @DexIgnore
        public boolean b; // = false;
        @DexIgnore
        public y01 c; // = y01.NOT_REQUIRED;
        @DexIgnore
        public boolean d; // = false;
        @DexIgnore
        public boolean e; // = false;
        @DexIgnore
        public long f; // = -1;
        @DexIgnore
        public long g; // = -1;
        @DexIgnore
        public q01 h; // = new q01();

        @DexIgnore
        public p01 a() {
            return new p01(this);
        }

        @DexIgnore
        public a b(y01 y01) {
            this.c = y01;
            return this;
        }
    }

    @DexIgnore
    public p01() {
    }

    @DexIgnore
    public p01(a aVar) {
        this.b = aVar.f2759a;
        this.c = Build.VERSION.SDK_INT >= 23 && aVar.b;
        this.f2758a = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        if (Build.VERSION.SDK_INT >= 24) {
            this.h = aVar.h;
            this.f = aVar.f;
            this.g = aVar.g;
        }
    }

    @DexIgnore
    public p01(p01 p01) {
        this.b = p01.b;
        this.c = p01.c;
        this.f2758a = p01.f2758a;
        this.d = p01.d;
        this.e = p01.e;
        this.h = p01.h;
    }

    @DexIgnore
    public q01 a() {
        return this.h;
    }

    @DexIgnore
    public y01 b() {
        return this.f2758a;
    }

    @DexIgnore
    public long c() {
        return this.f;
    }

    @DexIgnore
    public long d() {
        return this.g;
    }

    @DexIgnore
    public boolean e() {
        return this.h.c() > 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || p01.class != obj.getClass()) {
            return false;
        }
        p01 p01 = (p01) obj;
        if (this.b == p01.b && this.c == p01.c && this.d == p01.d && this.e == p01.e && this.f == p01.f && this.g == p01.g && this.f2758a == p01.f2758a) {
            return this.h.equals(p01.h);
        }
        return false;
    }

    @DexIgnore
    public boolean f() {
        return this.d;
    }

    @DexIgnore
    public boolean g() {
        return this.b;
    }

    @DexIgnore
    public boolean h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.f2758a.hashCode();
        boolean z = this.b;
        boolean z2 = this.c;
        boolean z3 = this.d;
        boolean z4 = this.e;
        long j = this.f;
        int i2 = (int) (j ^ (j >>> 32));
        long j2 = this.g;
        return (((((((((((((hashCode * 31) + (z ? 1 : 0)) * 31) + (z2 ? 1 : 0)) * 31) + (z3 ? 1 : 0)) * 31) + (z4 ? 1 : 0)) * 31) + i2) * 31) + ((int) ((j2 >>> 32) ^ j2))) * 31) + this.h.hashCode();
    }

    @DexIgnore
    public boolean i() {
        return this.e;
    }

    @DexIgnore
    public void j(q01 q01) {
        this.h = q01;
    }

    @DexIgnore
    public void k(y01 y01) {
        this.f2758a = y01;
    }

    @DexIgnore
    public void l(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public void m(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public void n(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public void o(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public void p(long j) {
        this.f = j;
    }

    @DexIgnore
    public void q(long j) {
        this.g = j;
    }
}
