package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.oj0;
import com.fossil.tj0;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kj0 {
    @DexIgnore
    public static int p; // = 1000;
    @DexIgnore
    public static lj0 q;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f1921a;
    @DexIgnore
    public HashMap<String, oj0> b;
    @DexIgnore
    public a c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public hj0[] f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean[] h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public /* final */ ij0 l;
    @DexIgnore
    public oj0[] m;
    @DexIgnore
    public int n;
    @DexIgnore
    public /* final */ a o;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(oj0 oj0);

        @DexIgnore
        oj0 b(kj0 kj0, boolean[] zArr);

        @DexIgnore
        void c(a aVar);

        @DexIgnore
        Object clear();  // void declaration

        @DexIgnore
        oj0 getKey();
    }

    @DexIgnore
    public kj0() {
        this.f1921a = 0;
        this.b = null;
        this.d = 32;
        this.e = 32;
        this.f = null;
        this.g = false;
        this.h = new boolean[32];
        this.i = 1;
        this.j = 0;
        this.k = 32;
        this.m = new oj0[p];
        this.n = 0;
        this.f = new hj0[32];
        D();
        ij0 ij0 = new ij0();
        this.l = ij0;
        this.c = new jj0(ij0);
        this.o = new hj0(this.l);
    }

    @DexIgnore
    public static hj0 t(kj0 kj0, oj0 oj0, oj0 oj02, oj0 oj03, float f2, boolean z) {
        hj0 s = kj0.s();
        if (z) {
            kj0.g(s);
        }
        s.i(oj0, oj02, oj03, f2);
        return s;
    }

    @DexIgnore
    public static lj0 x() {
        return q;
    }

    @DexIgnore
    public void A() throws Exception {
        boolean z = false;
        lj0 lj0 = q;
        if (lj0 != null) {
            lj0.e++;
        }
        if (this.g) {
            lj0 lj02 = q;
            if (lj02 != null) {
                lj02.q++;
            }
            int i2 = 0;
            while (true) {
                if (i2 >= this.j) {
                    z = true;
                    break;
                } else if (!this.f[i2].e) {
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                B(this.c);
                return;
            }
            lj0 lj03 = q;
            if (lj03 != null) {
                lj03.p++;
            }
            o();
            return;
        }
        B(this.c);
    }

    @DexIgnore
    public void B(a aVar) throws Exception {
        lj0 lj0 = q;
        if (lj0 != null) {
            lj0.s++;
            lj0.t = Math.max(lj0.t, (long) this.i);
            lj0 lj02 = q;
            lj02.u = Math.max(lj02.u, (long) this.j);
        }
        F((hj0) aVar);
        v(aVar);
        C(aVar, false);
        o();
    }

    @DexIgnore
    public final int C(a aVar, boolean z) {
        lj0 lj0 = q;
        if (lj0 != null) {
            lj0.h++;
        }
        for (int i2 = 0; i2 < this.i; i2++) {
            this.h[i2] = false;
        }
        boolean z2 = false;
        int i3 = 0;
        while (!z2) {
            lj0 lj02 = q;
            if (lj02 != null) {
                lj02.i++;
            }
            int i4 = i3 + 1;
            if (i4 >= this.i * 2) {
                return i4;
            }
            if (aVar.getKey() != null) {
                this.h[aVar.getKey().b] = true;
            }
            oj0 b2 = aVar.b(this, this.h);
            if (b2 != null) {
                boolean[] zArr = this.h;
                int i5 = b2.b;
                if (zArr[i5]) {
                    return i4;
                }
                zArr[i5] = true;
            }
            if (b2 != null) {
                float f2 = Float.MAX_VALUE;
                int i6 = -1;
                for (int i7 = 0; i7 < this.j; i7++) {
                    hj0 hj0 = this.f[i7];
                    if (hj0.f1485a.g != oj0.a.UNRESTRICTED && !hj0.e && hj0.s(b2)) {
                        float f3 = hj0.d.f(b2);
                        if (f3 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            float f4 = (-hj0.b) / f3;
                            if (f4 < f2) {
                                i6 = i7;
                                f2 = f4;
                            }
                        }
                    }
                }
                if (i6 > -1) {
                    hj0 hj02 = this.f[i6];
                    hj02.f1485a.c = -1;
                    lj0 lj03 = q;
                    if (lj03 != null) {
                        lj03.j++;
                    }
                    hj02.v(b2);
                    oj0 oj0 = hj02.f1485a;
                    oj0.c = i6;
                    oj0.f(hj02);
                    i3 = i4;
                }
            }
            z2 = true;
            i3 = i4;
        }
        return i3;
    }

    @DexIgnore
    public final void D() {
        int i2 = 0;
        while (true) {
            hj0[] hj0Arr = this.f;
            if (i2 < hj0Arr.length) {
                hj0 hj0 = hj0Arr[i2];
                if (hj0 != null) {
                    this.l.f1635a.a(hj0);
                }
                this.f[i2] = null;
                i2++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public void E() {
        ij0 ij0;
        int i2 = 0;
        while (true) {
            ij0 = this.l;
            oj0[] oj0Arr = ij0.c;
            if (i2 >= oj0Arr.length) {
                break;
            }
            oj0 oj0 = oj0Arr[i2];
            if (oj0 != null) {
                oj0.d();
            }
            i2++;
        }
        ij0.b.c(this.m, this.n);
        this.n = 0;
        Arrays.fill(this.l.c, (Object) null);
        HashMap<String, oj0> hashMap = this.b;
        if (hashMap != null) {
            hashMap.clear();
        }
        this.f1921a = 0;
        this.c.clear();
        this.i = 1;
        for (int i3 = 0; i3 < this.j; i3++) {
            this.f[i3].c = false;
        }
        D();
        this.j = 0;
    }

    @DexIgnore
    public final void F(hj0 hj0) {
        if (this.j > 0) {
            hj0.d.o(hj0, this.f);
            if (hj0.d.f1313a == 0) {
                hj0.e = true;
            }
        }
    }

    @DexIgnore
    public final oj0 a(oj0.a aVar, String str) {
        oj0 oj0;
        oj0 b2 = this.l.b.b();
        if (b2 == null) {
            oj0 oj02 = new oj0(aVar, str);
            oj02.e(aVar, str);
            oj0 = oj02;
        } else {
            b2.d();
            b2.e(aVar, str);
            oj0 = b2;
        }
        int i2 = this.n;
        int i3 = p;
        if (i2 >= i3) {
            int i4 = i3 * 2;
            p = i4;
            this.m = (oj0[]) Arrays.copyOf(this.m, i4);
        }
        oj0[] oj0Arr = this.m;
        int i5 = this.n;
        this.n = i5 + 1;
        oj0Arr[i5] = oj0;
        return oj0;
    }

    @DexIgnore
    public void b(uj0 uj0, uj0 uj02, float f2, int i2) {
        oj0 r = r(uj0.h(tj0.d.LEFT));
        oj0 r2 = r(uj0.h(tj0.d.TOP));
        oj0 r3 = r(uj0.h(tj0.d.RIGHT));
        oj0 r4 = r(uj0.h(tj0.d.BOTTOM));
        oj0 r5 = r(uj02.h(tj0.d.LEFT));
        oj0 r6 = r(uj02.h(tj0.d.TOP));
        oj0 r7 = r(uj02.h(tj0.d.RIGHT));
        oj0 r8 = r(uj02.h(tj0.d.BOTTOM));
        hj0 s = s();
        double d2 = (double) f2;
        double d3 = (double) i2;
        s.p(r2, r4, r6, r8, (float) (Math.sin(d2) * d3));
        d(s);
        hj0 s2 = s();
        s2.p(r, r3, r5, r7, (float) (Math.cos(d2) * d3));
        d(s2);
    }

    @DexIgnore
    public void c(oj0 oj0, oj0 oj02, int i2, float f2, oj0 oj03, oj0 oj04, int i3, int i4) {
        hj0 s = s();
        s.g(oj0, oj02, i2, f2, oj03, oj04, i3);
        if (i4 != 6) {
            s.d(this, i4);
        }
        d(s);
    }

    @DexIgnore
    public void d(hj0 hj0) {
        oj0 u;
        boolean z = true;
        if (hj0 != null) {
            lj0 lj0 = q;
            if (lj0 != null) {
                lj0.f++;
                if (hj0.e) {
                    lj0.g++;
                }
            }
            if (this.j + 1 >= this.k || this.i + 1 >= this.e) {
                z();
            }
            if (!hj0.e) {
                F(hj0);
                if (!hj0.t()) {
                    hj0.q();
                    if (hj0.f(this)) {
                        oj0 q2 = q();
                        hj0.f1485a = q2;
                        m(hj0);
                        this.o.c(hj0);
                        C(this.o, true);
                        if (q2.c == -1) {
                            if (hj0.f1485a == q2 && (u = hj0.u(q2)) != null) {
                                lj0 lj02 = q;
                                if (lj02 != null) {
                                    lj02.j++;
                                }
                                hj0.v(u);
                            }
                            if (!hj0.e) {
                                hj0.f1485a.f(hj0);
                            }
                            this.j--;
                        }
                    } else {
                        z = false;
                    }
                    if (!hj0.r()) {
                        return;
                    }
                } else {
                    return;
                }
            } else {
                z = false;
            }
            if (!z) {
                m(hj0);
            }
        }
    }

    @DexIgnore
    public hj0 e(oj0 oj0, oj0 oj02, int i2, int i3) {
        hj0 s = s();
        s.m(oj0, oj02, i2);
        if (i3 != 6) {
            s.d(this, i3);
        }
        d(s);
        return s;
    }

    @DexIgnore
    public void f(oj0 oj0, int i2) {
        int i3 = oj0.c;
        if (i3 != -1) {
            hj0 hj0 = this.f[i3];
            if (hj0.e) {
                hj0.b = (float) i2;
            } else if (hj0.d.f1313a == 0) {
                hj0.e = true;
                hj0.b = (float) i2;
            } else {
                hj0 s = s();
                s.l(oj0, i2);
                d(s);
            }
        } else {
            hj0 s2 = s();
            s2.h(oj0, i2);
            d(s2);
        }
    }

    @DexIgnore
    public final void g(hj0 hj0) {
        hj0.d(this, 0);
    }

    @DexIgnore
    public void h(oj0 oj0, oj0 oj02, boolean z) {
        hj0 s = s();
        oj0 u = u();
        u.d = 0;
        s.n(oj0, oj02, u, 0);
        if (z) {
            n(s, (int) (s.d.f(u) * -1.0f), 1);
        }
        d(s);
    }

    @DexIgnore
    public void i(oj0 oj0, oj0 oj02, int i2, int i3) {
        hj0 s = s();
        oj0 u = u();
        u.d = 0;
        s.n(oj0, oj02, u, i2);
        if (i3 != 6) {
            n(s, (int) (s.d.f(u) * -1.0f), i3);
        }
        d(s);
    }

    @DexIgnore
    public void j(oj0 oj0, oj0 oj02, boolean z) {
        hj0 s = s();
        oj0 u = u();
        u.d = 0;
        s.o(oj0, oj02, u, 0);
        if (z) {
            n(s, (int) (s.d.f(u) * -1.0f), 1);
        }
        d(s);
    }

    @DexIgnore
    public void k(oj0 oj0, oj0 oj02, int i2, int i3) {
        hj0 s = s();
        oj0 u = u();
        u.d = 0;
        s.o(oj0, oj02, u, i2);
        if (i3 != 6) {
            n(s, (int) (s.d.f(u) * -1.0f), i3);
        }
        d(s);
    }

    @DexIgnore
    public void l(oj0 oj0, oj0 oj02, oj0 oj03, oj0 oj04, float f2, int i2) {
        hj0 s = s();
        s.j(oj0, oj02, oj03, oj04, f2);
        if (i2 != 6) {
            s.d(this, i2);
        }
        d(s);
    }

    @DexIgnore
    public final void m(hj0 hj0) {
        hj0[] hj0Arr = this.f;
        int i2 = this.j;
        if (hj0Arr[i2] != null) {
            this.l.f1635a.a(hj0Arr[i2]);
        }
        hj0[] hj0Arr2 = this.f;
        int i3 = this.j;
        hj0Arr2[i3] = hj0;
        oj0 oj0 = hj0.f1485a;
        oj0.c = i3;
        this.j = i3 + 1;
        oj0.f(hj0);
    }

    @DexIgnore
    public void n(hj0 hj0, int i2, int i3) {
        hj0.e(p(i3, null), i2);
    }

    @DexIgnore
    public final void o() {
        for (int i2 = 0; i2 < this.j; i2++) {
            hj0 hj0 = this.f[i2];
            hj0.f1485a.e = hj0.b;
        }
    }

    @DexIgnore
    public oj0 p(int i2, String str) {
        lj0 lj0 = q;
        if (lj0 != null) {
            lj0.l++;
        }
        if (this.i + 1 >= this.e) {
            z();
        }
        oj0 a2 = a(oj0.a.ERROR, str);
        int i3 = this.f1921a + 1;
        this.f1921a = i3;
        this.i++;
        a2.b = i3;
        a2.d = i2;
        this.l.c[i3] = a2;
        this.c.a(a2);
        return a2;
    }

    @DexIgnore
    public oj0 q() {
        lj0 lj0 = q;
        if (lj0 != null) {
            lj0.n++;
        }
        if (this.i + 1 >= this.e) {
            z();
        }
        oj0 a2 = a(oj0.a.SLACK, null);
        int i2 = this.f1921a + 1;
        this.f1921a = i2;
        this.i++;
        a2.b = i2;
        this.l.c[i2] = a2;
        return a2;
    }

    @DexIgnore
    public oj0 r(Object obj) {
        oj0 oj0 = null;
        if (obj != null) {
            if (this.i + 1 >= this.e) {
                z();
            }
            if (obj instanceof tj0) {
                tj0 tj0 = (tj0) obj;
                oj0 = tj0.g();
                if (oj0 == null) {
                    tj0.n(this.l);
                    oj0 = tj0.g();
                }
                int i2 = oj0.b;
                if (i2 == -1 || i2 > this.f1921a || this.l.c[i2] == null) {
                    if (oj0.b != -1) {
                        oj0.d();
                    }
                    int i3 = this.f1921a + 1;
                    this.f1921a = i3;
                    this.i++;
                    oj0.b = i3;
                    oj0.g = oj0.a.UNRESTRICTED;
                    this.l.c[i3] = oj0;
                }
            }
        }
        return oj0;
    }

    @DexIgnore
    public hj0 s() {
        hj0 b2 = this.l.f1635a.b();
        if (b2 == null) {
            b2 = new hj0(this.l);
        } else {
            b2.w();
        }
        oj0.b();
        return b2;
    }

    @DexIgnore
    public oj0 u() {
        lj0 lj0 = q;
        if (lj0 != null) {
            lj0.m++;
        }
        if (this.i + 1 >= this.e) {
            z();
        }
        oj0 a2 = a(oj0.a.SLACK, null);
        int i2 = this.f1921a + 1;
        this.f1921a = i2;
        this.i++;
        a2.b = i2;
        this.l.c[i2] = a2;
        return a2;
    }

    @DexIgnore
    public final int v(a aVar) throws Exception {
        boolean z;
        boolean z2;
        int i2 = 0;
        while (true) {
            if (i2 >= this.j) {
                z = false;
                break;
            }
            hj0[] hj0Arr = this.f;
            if (hj0Arr[i2].f1485a.g != oj0.a.UNRESTRICTED && hj0Arr[i2].b < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z = true;
                break;
            }
            i2++;
        }
        if (!z) {
            return 0;
        }
        boolean z3 = false;
        int i3 = 0;
        while (!z3) {
            lj0 lj0 = q;
            if (lj0 != null) {
                lj0.k++;
            }
            int i4 = i3 + 1;
            float f2 = Float.MAX_VALUE;
            int i5 = -1;
            int i6 = -1;
            int i7 = 0;
            for (int i8 = 0; i8 < this.j; i8++) {
                hj0 hj0 = this.f[i8];
                if (hj0.f1485a.g != oj0.a.UNRESTRICTED && !hj0.e && hj0.b < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    for (int i9 = 1; i9 < this.i; i9++) {
                        oj0 oj0 = this.l.c[i9];
                        float f3 = hj0.d.f(oj0);
                        if (f3 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            int i10 = 0;
                            while (i10 < 7) {
                                float f4 = oj0.f[i10] / f3;
                                if ((f4 >= f2 || i10 != i7) && i10 <= i7) {
                                    f4 = f2;
                                } else {
                                    i6 = i9;
                                    i7 = i10;
                                    i5 = i8;
                                }
                                i10++;
                                f2 = f4;
                            }
                        }
                    }
                }
            }
            if (i5 != -1) {
                hj0 hj02 = this.f[i5];
                hj02.f1485a.c = -1;
                lj0 lj02 = q;
                if (lj02 != null) {
                    lj02.j++;
                }
                hj02.v(this.l.c[i6]);
                oj0 oj02 = hj02.f1485a;
                oj02.c = i5;
                oj02.f(hj02);
                z2 = z3;
            } else {
                z2 = true;
            }
            if (i4 > this.i / 2) {
                z2 = true;
            }
            z3 = z2;
            i3 = i4;
        }
        return i3;
    }

    @DexIgnore
    public ij0 w() {
        return this.l;
    }

    @DexIgnore
    public int y(Object obj) {
        oj0 g2 = ((tj0) obj).g();
        if (g2 != null) {
            return (int) (g2.e + 0.5f);
        }
        return 0;
    }

    @DexIgnore
    public final void z() {
        int i2 = this.d * 2;
        this.d = i2;
        this.f = (hj0[]) Arrays.copyOf(this.f, i2);
        ij0 ij0 = this.l;
        ij0.c = (oj0[]) Arrays.copyOf(ij0.c, this.d);
        int i3 = this.d;
        this.h = new boolean[i3];
        this.e = i3;
        this.k = i3;
        lj0 lj0 = q;
        if (lj0 != null) {
            lj0.d++;
            lj0.o = Math.max(lj0.o, (long) i3);
            lj0 lj02 = q;
            lj02.A = lj02.o;
        }
    }
}
