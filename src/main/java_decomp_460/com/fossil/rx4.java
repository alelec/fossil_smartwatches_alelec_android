package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rx4 implements Factory<qx4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<tt4> f3180a;
    @DexIgnore
    public /* final */ Provider<on5> b;

    @DexIgnore
    public rx4(Provider<tt4> provider, Provider<on5> provider2) {
        this.f3180a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static rx4 a(Provider<tt4> provider, Provider<on5> provider2) {
        return new rx4(provider, provider2);
    }

    @DexIgnore
    public static qx4 c(tt4 tt4, on5 on5) {
        return new qx4(tt4, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public qx4 get() {
        return c(this.f3180a.get(), this.b.get());
    }
}
