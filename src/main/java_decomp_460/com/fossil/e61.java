package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface e61<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <T> boolean a(e61<T> e61, T t) {
            pq7.c(t, "data");
            return true;
        }
    }

    @DexIgnore
    boolean a(T t);

    @DexIgnore
    String b(T t);

    @DexIgnore
    Object c(g51 g51, T t, f81 f81, x51 x51, qn7<? super d61> qn7);
}
