package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e37 implements Factory<d37> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<DeviceRepository> f875a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;
    @DexIgnore
    public /* final */ Provider<q27> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;

    @DexIgnore
    public e37(Provider<DeviceRepository> provider, Provider<UserRepository> provider2, Provider<q27> provider3, Provider<PortfolioApp> provider4) {
        this.f875a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static e37 a(Provider<DeviceRepository> provider, Provider<UserRepository> provider2, Provider<q27> provider3, Provider<PortfolioApp> provider4) {
        return new e37(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static d37 c(DeviceRepository deviceRepository, UserRepository userRepository, q27 q27, PortfolioApp portfolioApp) {
        return new d37(deviceRepository, userRepository, q27, portfolioApp);
    }

    @DexIgnore
    /* renamed from: b */
    public d37 get() {
        return c(this.f875a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
