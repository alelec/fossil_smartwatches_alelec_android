package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface lv3 extends IInterface {
    @DexIgnore
    void L0(sv3 sv3) throws RemoteException;

    @DexIgnore
    void O(DataHolder dataHolder) throws RemoteException;

    @DexIgnore
    void T1(pv3 pv3) throws RemoteException;

    @DexIgnore
    void U2(List<pv3> list) throws RemoteException;

    @DexIgnore
    void a1(nv3 nv3) throws RemoteException;

    @DexIgnore
    void i0(pv3 pv3) throws RemoteException;

    @DexIgnore
    void k2(uv3 uv3) throws RemoteException;

    @DexIgnore
    void r(ev3 ev3) throws RemoteException;

    @DexIgnore
    void t0(av3 av3) throws RemoteException;
}
