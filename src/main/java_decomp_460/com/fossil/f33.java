package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface f33<T> {
    @DexIgnore
    void a(T t, byte[] bArr, int i, int i2, sz2 sz2) throws IOException;

    @DexIgnore
    void b(T t, r43 r43) throws IOException;

    @DexIgnore
    int zza(T t);

    @DexIgnore
    T zza();

    @DexIgnore
    boolean zza(T t, T t2);

    @DexIgnore
    int zzb(T t);

    @DexIgnore
    void zzb(T t, T t2);

    @DexIgnore
    void zzc(T t);

    @DexIgnore
    boolean zzd(T t);
}
