package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f98 implements e88<w18, Short> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ f98 f1091a; // = new f98();

    @DexIgnore
    /* renamed from: b */
    public Short a(w18 w18) throws IOException {
        return Short.valueOf(w18.string());
    }
}
