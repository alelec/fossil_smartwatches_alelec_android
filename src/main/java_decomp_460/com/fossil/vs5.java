package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vs5 extends us5 {
    @DexIgnore
    public /* final */ List<String> d;
    @DexIgnore
    public String e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vs5(String str, String str2, List<String> list, String str3) {
        super(str, str2);
        pq7.c(str, "tagName");
        pq7.c(str2, "title");
        pq7.c(list, "values");
        pq7.c(str3, "btnText");
        this.d = list;
        this.e = str3;
    }

    @DexIgnore
    public final String f() {
        return this.e;
    }

    @DexIgnore
    public final List<String> g() {
        return this.d;
    }
}
