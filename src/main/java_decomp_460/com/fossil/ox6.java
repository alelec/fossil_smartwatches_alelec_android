package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.n04;
import com.fossil.t47;
import com.fossil.x37;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ox6 extends pv5 implements qw6, t47.g {
    @DexIgnore
    public static /* final */ a v; // = new a(null);
    @DexIgnore
    public pw6 g;
    @DexIgnore
    public g37<ub5> h;
    @DexIgnore
    public gr4 i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public int k;
    @DexIgnore
    public String l; // = "";
    @DexIgnore
    public /* final */ String m; // = qn5.l.a().d("disabledButton");
    @DexIgnore
    public /* final */ String s; // = qn5.l.a().d("primaryColor");
    @DexIgnore
    public /* final */ String t; // = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ox6 a(boolean z, String str) {
            pq7.c(str, "serial");
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            bundle.putString("SERIAL", str);
            ox6 ox6 = new ox6();
            ox6.setArguments(bundle);
            return ox6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ox6 b;

        @DexIgnore
        public b(ox6 ox6) {
            this.b = ox6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.P6().r();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ub5 f2747a;
        @DexIgnore
        public /* final */ /* synthetic */ ox6 b;

        @DexIgnore
        public c(ub5 ub5, ox6 ox6) {
            this.f2747a = ub5;
            this.b = ox6;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            Drawable e;
            Drawable e2;
            super.b(i, f, i2);
            if (!TextUtils.isEmpty(this.b.s)) {
                int parseColor = Color.parseColor(this.b.s);
                TabLayout.g v = this.f2747a.A.v(i);
                if (!(v == null || (e2 = v.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.m) && this.b.k != i) {
                int parseColor2 = Color.parseColor(this.b.m);
                TabLayout.g v2 = this.f2747a.A.v(this.b.k);
                if (!(v2 == null || (e = v2.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.b.k = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ox6 b;

        @DexIgnore
        public d(ox6 ox6) {
            this.b = ox6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.P6().s();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ox6 b;

        @DexIgnore
        public e(ox6 ox6) {
            this.b = ox6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
                pq7.b(activity, "it");
                TroubleshootingActivity.a.c(aVar, activity, this.b.l, false, false, 12, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements n04.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ox6 f2748a;

        @DexIgnore
        public f(ox6 ox6) {
            this.f2748a = ox6;
        }

        @DexIgnore
        @Override // com.fossil.n04.b
        public final void a(TabLayout.g gVar, int i) {
            pq7.c(gVar, "tab");
            if (!TextUtils.isEmpty(this.f2748a.m) && !TextUtils.isEmpty(this.f2748a.s)) {
                int parseColor = Color.parseColor(this.f2748a.m);
                int parseColor2 = Color.parseColor(this.f2748a.s);
                gVar.o(2131230966);
                if (i == this.f2748a.k) {
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable e2 = gVar.e();
                if (e2 != null) {
                    e2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw6
    public void A(boolean z) {
        if (isActive()) {
            g37<ub5> g37 = this.h;
            if (g37 != null) {
                ub5 a2 = g37.a();
                if (a2 != null) {
                    pw6 pw6 = this.g;
                    if (pw6 == null) {
                        pq7.n("mPresenter");
                        throw null;
                    } else if (pw6.q()) {
                        if (z) {
                            ConstraintLayout constraintLayout = a2.q;
                            pq7.b(constraintLayout, "it.clUpdateFwFail");
                            constraintLayout.setVisibility(8);
                            ConstraintLayout constraintLayout2 = a2.r;
                            pq7.b(constraintLayout2, "it.clUpdatingFw");
                            constraintLayout2.setVisibility(0);
                            FlexibleButton flexibleButton = a2.s;
                            pq7.b(flexibleButton, "it.fbContinue");
                            flexibleButton.setVisibility(0);
                            FlexibleTextView flexibleTextView = a2.z;
                            pq7.b(flexibleTextView, "it.ftvUpdateWarning");
                            flexibleTextView.setVisibility(4);
                            FlexibleProgressBar flexibleProgressBar = a2.D;
                            pq7.b(flexibleProgressBar, "it.progressUpdate");
                            flexibleProgressBar.setProgress(1000);
                            FlexibleTextView flexibleTextView2 = a2.y;
                            pq7.b(flexibleTextView2, "it.ftvUpdate");
                            flexibleTextView2.setText(um5.c(PortfolioApp.h0.c(), 2131887045));
                            return;
                        }
                        ConstraintLayout constraintLayout3 = a2.q;
                        pq7.b(constraintLayout3, "it.clUpdateFwFail");
                        constraintLayout3.setVisibility(0);
                        ConstraintLayout constraintLayout4 = a2.r;
                        pq7.b(constraintLayout4, "it.clUpdatingFw");
                        constraintLayout4.setVisibility(8);
                    } else if (getActivity() == null) {
                    } else {
                        if (z) {
                            pw6 pw62 = this.g;
                            if (pw62 != null) {
                                pw62.n();
                                l();
                                return;
                            }
                            pq7.n("mPresenter");
                            throw null;
                        }
                        TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
                        Context requireContext = requireContext();
                        pq7.b(requireContext, "requireContext()");
                        TroubleshootingActivity.a.c(aVar, requireContext, this.l, false, false, 12, null);
                    }
                }
            } else {
                pq7.n("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw6
    public void K0() {
        g37<ub5> g37 = this.h;
        if (g37 != null) {
            ub5 a2 = g37.a();
            DashBar dashBar = a2 != null ? a2.C : null;
            if (dashBar != null) {
                pq7.b(dashBar, "mBinding.get()?.progressBar!!");
                dashBar.setVisibility(8);
                return;
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qw6
    public void O1() {
        if (isAdded()) {
            t47.f fVar = new t47.f(2131558481);
            fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886795));
            fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886794));
            fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886797));
            fVar.b(2131363373);
            fVar.h(false);
            fVar.k(getChildFragmentManager(), "NETWORK_ERROR");
        }
    }

    @DexIgnore
    public final pw6 P6() {
        pw6 pw6 = this.g;
        if (pw6 != null) {
            return pw6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    /* renamed from: Q6 */
    public void M5(pw6 pw6) {
        pq7.c(pw6, "presenter");
        this.g = pw6;
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -879828873) {
            if (hashCode == 927511079 && str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("UpdateFirmwareFragment", "Update firmware fail isOnboardingFlow " + this.j);
                if (i2 == 2131362290) {
                    pw6 pw6 = this.g;
                    if (pw6 != null) {
                        pw6.s();
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                } else if (i2 != 2131362385) {
                    if (i2 == 2131362686) {
                        pw6 pw62 = this.g;
                        if (pw62 != null) {
                            pw62.n();
                            requireActivity().finish();
                            return;
                        }
                        pq7.n("mPresenter");
                        throw null;
                    }
                } else if (getActivity() != null) {
                    HelpActivity.a aVar = HelpActivity.B;
                    FragmentActivity requireActivity = requireActivity();
                    pq7.b(requireActivity, "requireActivity()");
                    aVar.a(requireActivity);
                }
            }
        } else if (str.equals("NETWORK_ERROR") && i2 == 2131362279) {
            pw6 pw63 = this.g;
            if (pw63 != null) {
                pw63.o();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void R6() {
        g37<ub5> g37 = this.h;
        if (g37 != null) {
            ub5 a2 = g37.a();
            TabLayout tabLayout = a2 != null ? a2.A : null;
            if (tabLayout != null) {
                g37<ub5> g372 = this.h;
                if (g372 != null) {
                    ub5 a3 = g372.a();
                    ViewPager2 viewPager2 = a3 != null ? a3.F : null;
                    if (viewPager2 != null) {
                        new n04(tabLayout, viewPager2, new f(this)).a();
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.qw6
    public void V2() {
        if (isActive()) {
            g37<ub5> g37 = this.h;
            if (g37 != null) {
                ub5 a2 = g37.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    pq7.b(constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    pq7.b(constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.qw6
    public void X(List<? extends Explore> list) {
        pq7.c(list, "data");
        gr4 gr4 = this.i;
        if (gr4 != null) {
            gr4.h(list);
        } else {
            pq7.n("mAdapterUpdateFirmware");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.qw6
    public void Z(int i2) {
        FlexibleProgressBar flexibleProgressBar;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i2);
        g37<ub5> g37 = this.h;
        if (g37 != null) {
            ub5 a2 = g37.a();
            if (a2 != null && (flexibleProgressBar = a2.D) != null) {
                flexibleProgressBar.setProgress(i2);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qw6
    public void d0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ExploreWatchActivity.a aVar = ExploreWatchActivity.B;
            pq7.b(activity, "it");
            pw6 pw6 = this.g;
            if (pw6 != null) {
                aVar.a(activity, pw6.q());
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw6
    public void f() {
        DashBar dashBar;
        g37<ub5> g37 = this.h;
        if (g37 != null) {
            ub5 a2 = g37.a();
            if (a2 != null && (dashBar = a2.C) != null) {
                g37<ub5> g372 = this.h;
                if (g372 != null) {
                    ub5 a3 = g372.a();
                    DashBar dashBar2 = a3 != null ? a3.C : null;
                    if (dashBar2 != null) {
                        pq7.b(dashBar2, "mBinding.get()?.progressBar!!");
                        dashBar2.setVisibility(0);
                        x37.a aVar = x37.f4036a;
                        pq7.b(dashBar, "this");
                        aVar.i(dashBar, this.j, 500);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qw6
    public void l() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.B;
            pq7.b(activity, "it");
            HomeActivity.a.b(aVar, activity, null, 2, null);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        ub5 ub5 = (ub5) aq0.f(layoutInflater, 2131558632, viewGroup, false, A6());
        this.h = new g37<>(this, ub5);
        pq7.b(ub5, "binding");
        return ub5.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        pw6 pw6 = this.g;
        if (pw6 != null) {
            pw6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        pw6 pw6 = this.g;
        if (pw6 != null) {
            pw6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.j = arguments.getBoolean("IS_ONBOARDING_FLOW");
            String string = arguments.getString("SERIAL");
            if (string == null) {
                string = "";
            }
            this.l = string;
            pw6 pw6 = this.g;
            if (pw6 != null) {
                pw6.p(this.j, string);
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
        this.i = new gr4(new ArrayList());
        g37<ub5> g37 = this.h;
        if (g37 != null) {
            ub5 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                pq7.b(constraintLayout, "binding.clUpdateFwFail");
                constraintLayout.setVisibility(8);
                ConstraintLayout constraintLayout2 = a2.r;
                pq7.b(constraintLayout2, "binding.clUpdatingFw");
                constraintLayout2.setVisibility(0);
                FlexibleProgressBar flexibleProgressBar = a2.D;
                pq7.b(flexibleProgressBar, "binding.progressUpdate");
                flexibleProgressBar.setMax(1000);
                FlexibleButton flexibleButton = a2.s;
                pq7.b(flexibleButton, "binding.fbContinue");
                flexibleButton.setVisibility(8);
                FlexibleTextView flexibleTextView = a2.z;
                pq7.b(flexibleTextView, "binding.ftvUpdateWarning");
                flexibleTextView.setVisibility(0);
                a2.s.setOnClickListener(new b(this));
                ViewPager2 viewPager2 = a2.F;
                pq7.b(viewPager2, "binding.rvpTutorial");
                gr4 gr4 = this.i;
                if (gr4 != null) {
                    viewPager2.setAdapter(gr4);
                    if (!TextUtils.isEmpty(this.t)) {
                        TabLayout tabLayout = a2.A;
                        pq7.b(tabLayout, "binding.indicator");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.t)));
                    }
                    R6();
                    a2.F.g(new c(a2, this));
                    a2.t.setOnClickListener(new d(this));
                    a2.w.setOnClickListener(new e(this));
                    return;
                }
                pq7.n("mAdapterUpdateFirmware");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qw6
    public void t3() {
        if (isActive()) {
            g37<ub5> g37 = this.h;
            if (g37 != null) {
                ub5 a2 = g37.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    pq7.b(constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    pq7.b(constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    FlexibleButton flexibleButton = a2.s;
                    pq7.b(flexibleButton, "it.fbContinue");
                    flexibleButton.setVisibility(0);
                    FlexibleTextView flexibleTextView = a2.z;
                    pq7.b(flexibleTextView, "it.ftvUpdateWarning");
                    flexibleTextView.setVisibility(4);
                    FlexibleProgressBar flexibleProgressBar = a2.D;
                    pq7.b(flexibleProgressBar, "it.progressUpdate");
                    flexibleProgressBar.setVisibility(8);
                    FlexibleTextView flexibleTextView2 = a2.u;
                    pq7.b(flexibleTextView2, "it.ftvCountdownTime");
                    flexibleTextView2.setVisibility(8);
                    FlexibleTextView flexibleTextView3 = a2.y;
                    pq7.b(flexibleTextView3, "it.ftvUpdate");
                    flexibleTextView3.setText(um5.c(PortfolioApp.h0.c(), 2131886783));
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
