package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tb5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ConstraintLayout f3390a;
    @DexIgnore
    public /* final */ FlexibleButton b;
    @DexIgnore
    public /* final */ FlexibleButton c;
    @DexIgnore
    public /* final */ FlexibleTextView d;
    @DexIgnore
    public /* final */ ig5 e;
    @DexIgnore
    public /* final */ RTLImageView f;
    @DexIgnore
    public /* final */ ConstraintLayout g;
    @DexIgnore
    public /* final */ ScrollView h;

    @DexIgnore
    public tb5(ConstraintLayout constraintLayout, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView, ig5 ig5, RTLImageView rTLImageView, ConstraintLayout constraintLayout2, ScrollView scrollView) {
        this.f3390a = constraintLayout;
        this.b = flexibleButton;
        this.c = flexibleButton2;
        this.d = flexibleTextView;
        this.e = ig5;
        this.f = rTLImageView;
        this.g = constraintLayout2;
        this.h = scrollView;
    }

    @DexIgnore
    public static tb5 a(View view) {
        int i = 2131362686;
        FlexibleButton flexibleButton = (FlexibleButton) view.findViewById(2131362266);
        if (flexibleButton != null) {
            FlexibleButton flexibleButton2 = (FlexibleButton) view.findViewById(2131362290);
            if (flexibleButton2 != null) {
                FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(2131362546);
                if (flexibleTextView != null) {
                    View findViewById = view.findViewById(2131362630);
                    if (findViewById != null) {
                        ig5 a2 = ig5.a(findViewById);
                        RTLImageView rTLImageView = (RTLImageView) view.findViewById(2131362686);
                        if (rTLImageView != null) {
                            ConstraintLayout constraintLayout = (ConstraintLayout) view;
                            i = 2131363073;
                            ScrollView scrollView = (ScrollView) view.findViewById(2131363073);
                            if (scrollView != null) {
                                return new tb5(constraintLayout, flexibleButton, flexibleButton2, flexibleTextView, a2, rTLImageView, constraintLayout, scrollView);
                            }
                        }
                    } else {
                        i = 2131362630;
                    }
                } else {
                    i = 2131362546;
                }
            } else {
                i = 2131362290;
            }
        } else {
            i = 2131362266;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @DexIgnore
    public static tb5 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    @DexIgnore
    public static tb5 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(2131558630, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    @DexIgnore
    public ConstraintLayout b() {
        return this.f3390a;
    }
}
