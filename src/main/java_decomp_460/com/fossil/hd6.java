package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hd6 implements Factory<dh6> {
    @DexIgnore
    public static dh6 a(dd6 dd6) {
        dh6 d = dd6.d();
        lk7.c(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
