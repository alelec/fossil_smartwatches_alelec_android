package com.fossil;

import com.misfit.frameworks.buttonservice.db.DataLogService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oo5 {
    @rj4("appId")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2703a;
    @DexIgnore
    @rj4("buttonPosition")
    public String b;
    @DexIgnore
    @rj4("localUpdatedAt")
    public String c;

    @DexIgnore
    public oo5(String str, String str2, String str3) {
        pq7.c(str, "appId");
        pq7.c(str2, "position");
        pq7.c(str3, DataLogService.COLUMN_UPDATE_AT);
        this.f2703a = str;
        this.b = str2;
        this.c = str3;
    }

    @DexIgnore
    public final String a() {
        return this.f2703a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }

    @DexIgnore
    public final void d(String str) {
        pq7.c(str, "<set-?>");
        this.f2703a = str;
    }

    @DexIgnore
    public final void e(String str) {
        pq7.c(str, "<set-?>");
        this.b = str;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof oo5) {
                oo5 oo5 = (oo5) obj;
                if (!pq7.a(this.f2703a, oo5.f2703a) || !pq7.a(this.b, oo5.b) || !pq7.a(this.c, oo5.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f2703a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "PresetButton(appId=" + this.f2703a + ", position=" + this.b + ", updateAt=" + this.c + ")";
    }
}
