package com.fossil;

import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum qh5 {
    FEMALE(DeviceIdentityUtils.FLASH_SERIAL_NUMBER_PREFIX),
    MALE("M"),
    OTHER("O");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public /* final */ String value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final qh5 a(String str) {
            if (TextUtils.isEmpty(str)) {
                return qh5.OTHER;
            }
            qh5[] values = qh5.values();
            for (qh5 qh5 : values) {
                if (vt7.j(str, qh5.getValue(), true)) {
                    return qh5;
                }
            }
            return qh5.OTHER;
        }
    }

    @DexIgnore
    public qh5(String str) {
        this.value = str;
    }

    @DexIgnore
    public final String getValue() {
        return this.value;
    }

    @DexIgnore
    public String toString() {
        return this.value;
    }
}
