package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface yc7 {
    @DexIgnore
    int a();

    @DexIgnore
    Bitmap b(String str);

    @DexIgnore
    void c(String str, Bitmap bitmap);

    @DexIgnore
    int size();
}
