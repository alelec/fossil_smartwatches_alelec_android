package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iq extends qq7 implements rp7<fs, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ af b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public iq(af afVar) {
        super(1);
        this.b = afVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(fs fsVar) {
        mw mwVar = fsVar.v;
        if (mwVar.f == kt.h) {
            af afVar = this.b;
            int i = afVar.L;
            if (i < 5) {
                int i2 = i + 1;
                afVar.L = i2;
                if (i2 > 2) {
                    afVar.D();
                    this.b.T();
                } else {
                    af.O(afVar, afVar.D);
                    af afVar2 = this.b;
                    long j = afVar2.D;
                    af.M(afVar2, j - (j % 6144));
                    af.S(this.b);
                }
            } else {
                afVar.o(mwVar);
            }
        } else {
            this.b.o(mwVar);
        }
        return tl7.f3441a;
    }
}
