package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ze3 implements Parcelable.Creator<ee3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ee3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        double d = 0.0d;
        boolean z = false;
        boolean z2 = false;
        float f = 0.0f;
        int i = 0;
        int i2 = 0;
        float f2 = 0.0f;
        ArrayList arrayList = null;
        LatLng latLng = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 2:
                    latLng = (LatLng) ad2.e(parcel, t, LatLng.CREATOR);
                    break;
                case 3:
                    d = ad2.p(parcel, t);
                    break;
                case 4:
                    f2 = ad2.r(parcel, t);
                    break;
                case 5:
                    i2 = ad2.v(parcel, t);
                    break;
                case 6:
                    i = ad2.v(parcel, t);
                    break;
                case 7:
                    f = ad2.r(parcel, t);
                    break;
                case 8:
                    z2 = ad2.m(parcel, t);
                    break;
                case 9:
                    z = ad2.m(parcel, t);
                    break;
                case 10:
                    arrayList = ad2.j(parcel, t, me3.CREATOR);
                    break;
                default:
                    ad2.B(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new ee3(latLng, d, f2, i2, i, f, z2, z, arrayList);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ee3[] newArray(int i) {
        return new ee3[i];
    }
}
