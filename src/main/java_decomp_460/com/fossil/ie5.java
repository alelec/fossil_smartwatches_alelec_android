package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.watchface.WatchFaceCoverView;
import com.portfolio.platform.view.watchface.WatchFacePreviewView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ie5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ View A;
    @DexIgnore
    public /* final */ View B;
    @DexIgnore
    public /* final */ View C;
    @DexIgnore
    public /* final */ ConstraintLayout D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ View G;
    @DexIgnore
    public /* final */ View H;
    @DexIgnore
    public /* final */ CustomizeWidget I;
    @DexIgnore
    public /* final */ CustomizeWidget J;
    @DexIgnore
    public /* final */ CustomizeWidget K;
    @DexIgnore
    public /* final */ WatchFacePreviewView L;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ View r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ FloatingActionButton u;
    @DexIgnore
    public /* final */ FlexibleButton v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ RTLImageView x;
    @DexIgnore
    public /* final */ WatchFaceCoverView y;
    @DexIgnore
    public /* final */ ImageView z;

    @DexIgnore
    public ie5(Object obj, View view, int i, FlexibleButton flexibleButton, View view2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FloatingActionButton floatingActionButton, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView, RTLImageView rTLImageView, WatchFaceCoverView watchFaceCoverView, ImageView imageView, View view3, View view4, View view5, ConstraintLayout constraintLayout3, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, View view6, View view7, CustomizeWidget customizeWidget, CustomizeWidget customizeWidget2, CustomizeWidget customizeWidget3, WatchFacePreviewView watchFacePreviewView) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = view2;
        this.s = constraintLayout;
        this.t = constraintLayout2;
        this.u = floatingActionButton;
        this.v = flexibleButton2;
        this.w = flexibleTextView;
        this.x = rTLImageView;
        this.y = watchFaceCoverView;
        this.z = imageView;
        this.A = view3;
        this.B = view4;
        this.C = view5;
        this.D = constraintLayout3;
        this.E = flexibleTextView2;
        this.F = flexibleTextView3;
        this.G = view6;
        this.H = view7;
        this.I = customizeWidget;
        this.J = customizeWidget2;
        this.K = customizeWidget3;
        this.L = watchFacePreviewView;
    }

    @DexIgnore
    @Deprecated
    public static ie5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z2, Object obj) {
        return (ie5) ViewDataBinding.p(layoutInflater, 2131558684, viewGroup, z2, obj);
    }

    @DexIgnore
    public static ie5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z2) {
        return A(layoutInflater, viewGroup, z2, aq0.d());
    }
}
