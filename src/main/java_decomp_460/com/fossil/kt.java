package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class kt extends Enum<kt> implements mt {
    @DexIgnore
    public static /* final */ kt d;
    @DexIgnore
    public static /* final */ kt e;
    @DexIgnore
    public static /* final */ kt f;
    @DexIgnore
    public static /* final */ kt g;
    @DexIgnore
    public static /* final */ kt h;
    @DexIgnore
    public static /* final */ kt i;
    @DexIgnore
    public static /* final */ /* synthetic */ kt[] j;
    @DexIgnore
    public static /* final */ it k; // = new it(null);
    @DexIgnore
    public /* final */ String b; // = ey1.a(this);
    @DexIgnore
    public /* final */ byte c;

    /*
    static {
        kt ktVar = new kt("SUCCESS", 0, (byte) 0);
        d = ktVar;
        kt ktVar2 = new kt("INVALID_OPERATION", 1, (byte) 1);
        e = ktVar2;
        kt ktVar3 = new kt("INVALID_FILE_HANDLE", 2, (byte) 2);
        f = ktVar3;
        kt ktVar4 = new kt("INVALID_OPERATION_DATA", 3, (byte) 3);
        kt ktVar5 = new kt("OPERATION_IN_PROGRESS", 4, (byte) 4);
        g = ktVar5;
        kt ktVar6 = new kt("VERIFICATION_FAIL", 5, (byte) 5);
        h = ktVar6;
        kt ktVar7 = new kt("UNKNOWN", 6, (byte) 255);
        i = ktVar7;
        j = new kt[]{ktVar, ktVar2, ktVar3, ktVar4, ktVar5, ktVar6, ktVar7};
    }
    */

    @DexIgnore
    public kt(String str, int i2, byte b2) {
        this.c = (byte) b2;
    }

    @DexIgnore
    public static kt valueOf(String str) {
        return (kt) Enum.valueOf(kt.class, str);
    }

    @DexIgnore
    public static kt[] values() {
        return (kt[]) j.clone();
    }

    @DexIgnore
    @Override // com.fossil.mt
    public boolean a() {
        return this == d;
    }

    @DexIgnore
    @Override // com.fossil.mt
    public String getLogName() {
        return this.b;
    }
}
