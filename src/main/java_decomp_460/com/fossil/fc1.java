package com.fossil;

import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import com.fossil.xb1;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fc1 implements xb1<ParcelFileDescriptor> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ b f1101a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements xb1.a<ParcelFileDescriptor> {
        @DexIgnore
        /* renamed from: b */
        public xb1<ParcelFileDescriptor> a(ParcelFileDescriptor parcelFileDescriptor) {
            return new fc1(parcelFileDescriptor);
        }

        @DexIgnore
        @Override // com.fossil.xb1.a
        public Class<ParcelFileDescriptor> getDataClass() {
            return ParcelFileDescriptor.class;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ParcelFileDescriptor f1102a;

        @DexIgnore
        public b(ParcelFileDescriptor parcelFileDescriptor) {
            this.f1102a = parcelFileDescriptor;
        }

        @DexIgnore
        public ParcelFileDescriptor a() throws IOException {
            try {
                Os.lseek(this.f1102a.getFileDescriptor(), 0, OsConstants.SEEK_SET);
                return this.f1102a;
            } catch (ErrnoException e) {
                throw new IOException(e);
            }
        }
    }

    @DexIgnore
    public fc1(ParcelFileDescriptor parcelFileDescriptor) {
        this.f1101a = new b(parcelFileDescriptor);
    }

    @DexIgnore
    public static boolean c() {
        return Build.VERSION.SDK_INT >= 21;
    }

    @DexIgnore
    @Override // com.fossil.xb1
    public void a() {
    }

    @DexIgnore
    /* renamed from: d */
    public ParcelFileDescriptor b() throws IOException {
        return this.f1101a.a();
    }
}
