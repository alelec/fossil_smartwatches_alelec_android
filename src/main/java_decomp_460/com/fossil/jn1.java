package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jn1 extends ym1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ kn1[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<jn1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final jn1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length >= 6) {
                ArrayList arrayList = new ArrayList();
                ur7 l = bs7.l(em7.G(bArr), 6);
                int a2 = l.a();
                int b = l.b();
                int c = l.c();
                if (c < 0 ? a2 >= b : a2 <= b) {
                    while (true) {
                        arrayList.add(kn1.CREATOR.a(dm7.k(bArr, a2, a2 + 6)));
                        if (a2 == b) {
                            break;
                        }
                        a2 += c;
                    }
                }
                Object[] array = arrayList.toArray(new kn1[0]);
                if (array != null) {
                    return new jn1((kn1[]) array);
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
            throw new IllegalArgumentException(e.b(e.e("Invalid data size: "), bArr.length, ", require at least: 6").toString());
        }

        @DexIgnore
        public jn1 b(Parcel parcel) {
            return new jn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public jn1 createFromParcel(Parcel parcel) {
            return new jn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public jn1[] newArray(int i) {
            return new jn1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ jn1(Parcel parcel, kq7 kq7) {
        super(parcel);
        kn1[] kn1Arr = (kn1[]) parcel.createTypedArray(kn1.CREATOR);
        this.c = kn1Arr == null ? new kn1[0] : kn1Arr;
    }

    @DexIgnore
    public jn1(kn1[] kn1Arr) {
        super(zm1.AUTO_WORKOUT_DETECTION);
        this.c = kn1Arr;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public byte[] b() {
        byte[] bArr = new byte[0];
        for (kn1 kn1 : this.c) {
            bArr = dm7.q(bArr, kn1.a());
        }
        return bArr;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public JSONArray c() {
        JSONArray jSONArray = new JSONArray();
        try {
            for (kn1 kn1 : this.c) {
                jSONArray.put(kn1.toJSONObject());
            }
        } catch (JSONException e) {
            d90.i.i(e);
        }
        return jSONArray;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(jn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.c, ((jn1) obj).c);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.autoworkoutdectection.AutoWorkoutDetectionConfig");
    }

    @DexIgnore
    public final kn1[] getAutoWorkoutDetectionItems() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public int hashCode() {
        return Arrays.hashCode(this.c);
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.c, i);
        }
    }
}
