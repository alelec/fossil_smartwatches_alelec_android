package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.portfolio.platform.view.ColorPanelView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v47 extends BaseAdapter {
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public /* final */ int[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public View f3716a;
        @DexIgnore
        public ColorPanelView b;
        @DexIgnore
        public ImageView c;
        @DexIgnore
        public int d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ int b;

            @DexIgnore
            public a(int i) {
                this.b = i;
            }

            @DexIgnore
            public void onClick(View view) {
                v47 v47 = v47.this;
                int i = v47.d;
                int i2 = this.b;
                if (i != i2) {
                    v47.d = i2;
                    v47.notifyDataSetChanged();
                }
                v47 v472 = v47.this;
                v472.b.a(v472.c[this.b]);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v47$b$b")
        /* renamed from: com.fossil.v47$b$b  reason: collision with other inner class name */
        public class View$OnLongClickListenerC0256b implements View.OnLongClickListener {
            @DexIgnore
            public View$OnLongClickListenerC0256b() {
            }

            @DexIgnore
            public boolean onLongClick(View view) {
                b.this.b.d();
                return true;
            }
        }

        @DexIgnore
        public b(Context context) {
            View inflate = View.inflate(context, v47.this.e == 0 ? 2131558456 : 2131558455, null);
            this.f3716a = inflate;
            this.b = (ColorPanelView) inflate.findViewById(2131362166);
            this.c = (ImageView) this.f3716a.findViewById(2131362163);
            this.d = this.b.getBorderColor();
            this.f3716a.setTag(this);
        }

        @DexIgnore
        public final void a(int i) {
            v47 v47 = v47.this;
            if (i != v47.d || pl0.b(v47.c[i]) < 0.65d) {
                this.c.setColorFilter((ColorFilter) null);
            } else {
                this.c.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
            }
        }

        @DexIgnore
        public final void b(int i) {
            this.b.setOnClickListener(new a(i));
            this.b.setOnLongClickListener(new View$OnLongClickListenerC0256b());
        }

        @DexIgnore
        public void c(int i) {
            int i2 = v47.this.c[i];
            int alpha = Color.alpha(i2);
            this.b.setColor(i2);
            this.c.setImageResource(v47.this.d == i ? 2131230942 : 0);
            if (alpha == 255) {
                a(i);
            } else if (alpha <= 165) {
                this.b.setBorderColor(i2 | -16777216);
                this.c.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
            } else {
                this.b.setBorderColor(this.d);
                this.c.setColorFilter(-1, PorterDuff.Mode.SRC_IN);
            }
            b(i);
        }
    }

    @DexIgnore
    public v47(a aVar, int[] iArr, int i, int i2) {
        this.b = aVar;
        this.c = iArr;
        this.d = i;
        this.e = i2;
    }

    @DexIgnore
    public void a() {
        this.d = -1;
        notifyDataSetChanged();
    }

    @DexIgnore
    public int getCount() {
        return this.c.length;
    }

    @DexIgnore
    public Object getItem(int i) {
        return Integer.valueOf(this.c[i]);
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    public View getView(int i, View view, ViewGroup viewGroup) {
        b bVar;
        if (view == null) {
            bVar = new b(viewGroup.getContext());
            view = bVar.f3716a;
        } else {
            bVar = (b) view.getTag();
        }
        bVar.c(i);
        return view;
    }
}
