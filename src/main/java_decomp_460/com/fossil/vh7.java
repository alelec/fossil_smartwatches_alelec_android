package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vh7 {
    @DexIgnore
    public static xh7 c;
    @DexIgnore
    public static th7 d; // = ei7.p();
    @DexIgnore
    public static JSONObject e; // = new JSONObject();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Integer f3770a; // = null;
    @DexIgnore
    public String b; // = null;

    @DexIgnore
    public vh7(Context context) {
        try {
            a(context);
            this.f3770a = ei7.I(context.getApplicationContext());
            this.b = tg7.a(context).e();
        } catch (Throwable th) {
            d.e(th);
        }
    }

    @DexIgnore
    public static xh7 a(Context context) {
        xh7 xh7;
        synchronized (vh7.class) {
            try {
                if (c == null) {
                    c = new xh7(context.getApplicationContext());
                }
                xh7 = c;
            } catch (Throwable th) {
                throw th;
            }
        }
        return xh7;
    }

    @DexIgnore
    public void b(JSONObject jSONObject, Thread thread) {
        String str;
        String str2;
        JSONObject jSONObject2 = new JSONObject();
        try {
            if (c != null) {
                c.a(jSONObject2, thread);
            }
            ji7.d(jSONObject2, "cn", this.b);
            if (this.f3770a != null) {
                jSONObject2.put("tn", this.f3770a);
            }
            if (thread == null) {
                str = "ev";
                str2 = jSONObject2;
            } else {
                str = "errkv";
                str2 = jSONObject2.toString();
            }
            jSONObject.put(str, str2);
            if (e != null && e.length() > 0) {
                jSONObject.put("eva", e);
            }
        } catch (Throwable th) {
            d.e(th);
        }
    }
}
