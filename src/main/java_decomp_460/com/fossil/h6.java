package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h6 extends u5 {
    @DexIgnore
    public int k; // = 23;
    @DexIgnore
    public /* final */ int l;

    @DexIgnore
    public h6(int i, n4 n4Var) {
        super(v5.g, n4Var);
        this.l = i;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void d(k5 k5Var) {
        k5Var.b(this.l);
    }

    @DexIgnore
    @Override // com.fossil.u5
    public boolean i(h7 h7Var) {
        return h7Var instanceof k7;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public fd0<h7> j() {
        return this.j.c;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void k(h7 h7Var) {
        this.k = ((k7) h7Var).b;
    }
}
