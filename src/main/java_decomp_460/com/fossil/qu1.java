package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qu1 extends nu1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<qu1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public qu1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                byte[] createByteArray = parcel.createByteArray();
                if (createByteArray != null) {
                    pq7.b(createByteArray, "parcel.createByteArray()!!");
                    return new qu1(readString, createByteArray);
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public qu1[] newArray(int i) {
            return new qu1[i];
        }
    }

    @DexIgnore
    public qu1(String str, byte[] bArr) {
        super(str, bArr);
    }

    @DexIgnore
    @Override // com.fossil.nu1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            qu1 qu1 = (qu1) obj;
            return (b() == qu1.b() || pq7.a(hy1.k((int) b(), null, 1, null), qu1.getFileName())) ? true : pq7.a(getFileName(), hy1.k((int) qu1.b(), null, 1, null));
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.file.NotificationIcon");
    }

    @DexIgnore
    @Override // com.fossil.nu1
    public int hashCode() {
        return (int) b();
    }
}
