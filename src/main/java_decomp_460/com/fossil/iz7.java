package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iz7<E> {
    @DexIgnore
    public static Object a(Object obj) {
        return obj;
    }

    @DexIgnore
    public static /* synthetic */ Object b(Object obj, int i, kq7 kq7) {
        if ((i & 1) != 0) {
            obj = null;
        }
        a(obj);
        return obj;
    }

    @DexIgnore
    public static final Object c(Object obj, E e) {
        if (nv7.a() && !(!(e instanceof List))) {
            throw new AssertionError();
        } else if (obj == null) {
            a(e);
            return e;
        } else if (!(obj instanceof ArrayList)) {
            ArrayList arrayList = new ArrayList(4);
            arrayList.add(obj);
            arrayList.add(e);
            a(arrayList);
            return arrayList;
        } else if (obj != null) {
            ((ArrayList) obj).add(e);
            a(obj);
            return obj;
        } else {
            throw new il7("null cannot be cast to non-null type kotlin.collections.ArrayList<E> /* = java.util.ArrayList<E> */");
        }
    }
}
