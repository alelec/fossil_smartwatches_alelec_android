package com.fossil;

import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dn6 implements MembersInjector<WorkoutDetailActivity> {
    @DexIgnore
    public static void a(WorkoutDetailActivity workoutDetailActivity, WorkoutSessionRepository workoutSessionRepository) {
        workoutDetailActivity.b = workoutSessionRepository;
    }
}
