package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vt1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ aq1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<vt1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public vt1 createFromParcel(Parcel parcel) {
            return new vt1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public vt1[] newArray(int i) {
            return new vt1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ vt1(Parcel parcel, kq7 kq7) {
        super((jq1) parcel.readParcelable(jq1.class.getClassLoader()), (nt1) parcel.readParcelable(nt1.class.getClassLoader()));
        Parcelable readParcelable = parcel.readParcelable(aq1.class.getClassLoader());
        if (readParcelable != null) {
            this.d = (aq1) readParcelable;
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public vt1(aq1 aq1, nt1 nt1) {
        super(null, nt1);
        this.d = aq1;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        Integer valueOf = Integer.valueOf(this.d.b());
        JSONObject jSONObject = new JSONObject();
        try {
            nt1 deviceMessage = getDeviceMessage();
            if (deviceMessage != null) {
                jSONObject.put("workoutApp._.config.response", deviceMessage.toJSONObject());
                JSONObject jSONObject2 = new JSONObject();
                String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
                try {
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("id", valueOf);
                    jSONObject3.put("set", jSONObject);
                    jSONObject2.put(str, jSONObject3);
                } catch (JSONException e) {
                    d90.i.i(e);
                }
                String jSONObject4 = jSONObject2.toString();
                pq7.b(jSONObject4, "deviceResponseJSONObject.toString()");
                Charset c = hd0.y.c();
                if (jSONObject4 != null) {
                    byte[] bytes = jSONObject4.getBytes(c);
                    pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
                    return bytes;
                }
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
            pq7.i();
            throw null;
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.ox1, com.fossil.mt1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = super.toJSONObject();
        try {
            g80.k(jSONObject, jd0.o, this.d.toJSONObject());
        } catch (JSONException e) {
            d90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }
}
