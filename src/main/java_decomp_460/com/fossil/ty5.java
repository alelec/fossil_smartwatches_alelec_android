package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import com.facebook.places.internal.LocationScannerImpl;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ty5 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ty5> CREATOR; // = new a();
    @DexIgnore
    public float A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public int H;
    @DexIgnore
    public int I;
    @DexIgnore
    public CharSequence J;
    @DexIgnore
    public int K;
    @DexIgnore
    public Uri L;
    @DexIgnore
    public Bitmap.CompressFormat M;
    @DexIgnore
    public int N;
    @DexIgnore
    public int O;
    @DexIgnore
    public int P;
    @DexIgnore
    public CropImageView.j Q;
    @DexIgnore
    public boolean R;
    @DexIgnore
    public Rect S;
    @DexIgnore
    public int T;
    @DexIgnore
    public boolean U;
    @DexIgnore
    public boolean V;
    @DexIgnore
    public boolean W;
    @DexIgnore
    public int X;
    @DexIgnore
    public boolean Y;
    @DexIgnore
    public boolean Z;
    @DexIgnore
    public CharSequence a0;
    @DexIgnore
    public CropImageView.c b;
    @DexIgnore
    public int b0;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public CropImageView.d e;
    @DexIgnore
    public CropImageView.k f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public int k;
    @DexIgnore
    public float l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public float u;
    @DexIgnore
    public int v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public float y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ty5> {
        @DexIgnore
        /* renamed from: a */
        public ty5 createFromParcel(Parcel parcel) {
            return new ty5(parcel);
        }

        @DexIgnore
        /* renamed from: b */
        public ty5[] newArray(int i) {
            return new ty5[i];
        }
    }

    @DexIgnore
    public ty5() {
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        this.b = CropImageView.c.RECTANGLE;
        this.c = TypedValue.applyDimension(1, 3.0f, displayMetrics);
        this.d = TypedValue.applyDimension(1, 24.0f, displayMetrics);
        this.e = CropImageView.d.ON_TOUCH;
        this.f = CropImageView.k.FIT_CENTER;
        this.g = true;
        this.h = true;
        this.i = true;
        this.j = false;
        this.k = 4;
        this.l = 0.1f;
        this.m = false;
        this.s = 1;
        this.t = 1;
        this.u = TypedValue.applyDimension(1, 3.0f, displayMetrics);
        this.v = Color.argb(170, 255, 255, 255);
        this.w = TypedValue.applyDimension(1, 2.0f, displayMetrics);
        this.x = TypedValue.applyDimension(1, 5.0f, displayMetrics);
        this.y = TypedValue.applyDimension(1, 14.0f, displayMetrics);
        this.z = -1;
        this.A = TypedValue.applyDimension(1, 1.0f, displayMetrics);
        this.B = Color.argb(170, 255, 255, 255);
        this.C = Color.argb(200, 0, 0, 0);
        this.D = (int) TypedValue.applyDimension(1, 42.0f, displayMetrics);
        this.E = (int) TypedValue.applyDimension(1, 42.0f, displayMetrics);
        this.F = 40;
        this.G = 40;
        this.H = 99999;
        this.I = 99999;
        this.J = "";
        this.K = 0;
        this.L = Uri.EMPTY;
        this.M = Bitmap.CompressFormat.JPEG;
        this.N = 90;
        this.O = 0;
        this.P = 0;
        this.Q = CropImageView.j.NONE;
        this.R = false;
        this.S = null;
        this.T = -1;
        this.U = true;
        this.V = true;
        this.W = false;
        this.X = 90;
        this.Y = false;
        this.Z = false;
        this.a0 = null;
        this.b0 = 0;
    }

    @DexIgnore
    public ty5(Parcel parcel) {
        boolean z2 = true;
        this.b = CropImageView.c.values()[parcel.readInt()];
        this.c = parcel.readFloat();
        this.d = parcel.readFloat();
        this.e = CropImageView.d.values()[parcel.readInt()];
        this.f = CropImageView.k.values()[parcel.readInt()];
        this.g = parcel.readByte() != 0;
        this.h = parcel.readByte() != 0;
        this.i = parcel.readByte() != 0;
        this.j = parcel.readByte() != 0;
        this.k = parcel.readInt();
        this.l = parcel.readFloat();
        this.m = parcel.readByte() != 0;
        this.s = parcel.readInt();
        this.t = parcel.readInt();
        this.u = parcel.readFloat();
        this.v = parcel.readInt();
        this.w = parcel.readFloat();
        this.x = parcel.readFloat();
        this.y = parcel.readFloat();
        this.z = parcel.readInt();
        this.A = parcel.readFloat();
        this.B = parcel.readInt();
        this.C = parcel.readInt();
        this.D = parcel.readInt();
        this.E = parcel.readInt();
        this.F = parcel.readInt();
        this.G = parcel.readInt();
        this.H = parcel.readInt();
        this.I = parcel.readInt();
        this.J = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.K = parcel.readInt();
        this.L = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.M = Bitmap.CompressFormat.valueOf(parcel.readString());
        this.N = parcel.readInt();
        this.O = parcel.readInt();
        this.P = parcel.readInt();
        this.Q = CropImageView.j.values()[parcel.readInt()];
        this.R = parcel.readByte() != 0;
        this.S = (Rect) parcel.readParcelable(Rect.class.getClassLoader());
        this.T = parcel.readInt();
        this.U = parcel.readByte() != 0;
        this.V = parcel.readByte() != 0;
        this.W = parcel.readByte() != 0;
        this.X = parcel.readInt();
        this.Y = parcel.readByte() != 0;
        this.Z = parcel.readByte() == 0 ? false : z2;
        this.a0 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.b0 = parcel.readInt();
    }

    @DexIgnore
    public void a() {
        if (this.k < 0) {
            throw new IllegalArgumentException("Cannot set max zoom to a number < 1");
        } else if (this.d >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float f2 = this.l;
            if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || ((double) f2) >= 0.5d) {
                throw new IllegalArgumentException("Cannot set initial crop window padding value to a number < 0 or >= 0.5");
            } else if (this.s <= 0) {
                throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
            } else if (this.t <= 0) {
                throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
            } else if (this.u < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                throw new IllegalArgumentException("Cannot set line thickness value to a number less than 0.");
            } else if (this.w < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                throw new IllegalArgumentException("Cannot set corner thickness value to a number less than 0.");
            } else if (this.A < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                throw new IllegalArgumentException("Cannot set guidelines thickness value to a number less than 0.");
            } else if (this.E >= 0) {
                int i2 = this.F;
                if (i2 >= 0) {
                    int i3 = this.G;
                    if (i3 < 0) {
                        throw new IllegalArgumentException("Cannot set min crop result height value to a number < 0 ");
                    } else if (this.H < i2) {
                        throw new IllegalArgumentException("Cannot set max crop result width to smaller value than min crop result width");
                    } else if (this.I < i3) {
                        throw new IllegalArgumentException("Cannot set max crop result height to smaller value than min crop result height");
                    } else if (this.O < 0) {
                        throw new IllegalArgumentException("Cannot set request width value to a number < 0 ");
                    } else if (this.P >= 0) {
                        int i4 = this.X;
                        if (i4 < 0 || i4 > 360) {
                            throw new IllegalArgumentException("Cannot set rotation degrees value to a number < 0 or > 360");
                        }
                    } else {
                        throw new IllegalArgumentException("Cannot set request height value to a number < 0 ");
                    }
                } else {
                    throw new IllegalArgumentException("Cannot set min crop result width value to a number < 0 ");
                }
            } else {
                throw new IllegalArgumentException("Cannot set min crop window height value to a number < 0 ");
            }
        } else {
            throw new IllegalArgumentException("Cannot set touch radius value to a number <= 0 ");
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.b.ordinal());
        parcel.writeFloat(this.c);
        parcel.writeFloat(this.d);
        parcel.writeInt(this.e.ordinal());
        parcel.writeInt(this.f.ordinal());
        parcel.writeByte(this.g ? (byte) 1 : 0);
        parcel.writeByte(this.h ? (byte) 1 : 0);
        parcel.writeByte(this.i ? (byte) 1 : 0);
        parcel.writeByte(this.j ? (byte) 1 : 0);
        parcel.writeInt(this.k);
        parcel.writeFloat(this.l);
        parcel.writeByte(this.m ? (byte) 1 : 0);
        parcel.writeInt(this.s);
        parcel.writeInt(this.t);
        parcel.writeFloat(this.u);
        parcel.writeInt(this.v);
        parcel.writeFloat(this.w);
        parcel.writeFloat(this.x);
        parcel.writeFloat(this.y);
        parcel.writeInt(this.z);
        parcel.writeFloat(this.A);
        parcel.writeInt(this.B);
        parcel.writeInt(this.C);
        parcel.writeInt(this.D);
        parcel.writeInt(this.E);
        parcel.writeInt(this.F);
        parcel.writeInt(this.G);
        parcel.writeInt(this.H);
        parcel.writeInt(this.I);
        TextUtils.writeToParcel(this.J, parcel, i2);
        parcel.writeInt(this.K);
        parcel.writeParcelable(this.L, i2);
        parcel.writeString(this.M.name());
        parcel.writeInt(this.N);
        parcel.writeInt(this.O);
        parcel.writeInt(this.P);
        parcel.writeInt(this.Q.ordinal());
        parcel.writeInt(this.R ? 1 : 0);
        parcel.writeParcelable(this.S, i2);
        parcel.writeInt(this.T);
        parcel.writeByte(this.U ? (byte) 1 : 0);
        parcel.writeByte(this.V ? (byte) 1 : 0);
        parcel.writeByte(this.W ? (byte) 1 : 0);
        parcel.writeInt(this.X);
        parcel.writeByte(this.Y ? (byte) 1 : 0);
        parcel.writeByte(this.Z ? (byte) 1 : 0);
        TextUtils.writeToParcel(this.a0, parcel, i2);
        parcel.writeInt(this.b0);
    }
}
