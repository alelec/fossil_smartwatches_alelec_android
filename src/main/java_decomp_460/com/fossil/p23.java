package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface p23 extends o23, Cloneable {
    @DexIgnore
    p23 A(byte[] bArr) throws l13;

    @DexIgnore
    p23 F(m23 m23);

    @DexIgnore
    m23 b();

    @DexIgnore
    m23 h();

    @DexIgnore
    p23 k(byte[] bArr, q03 q03) throws l13;
}
