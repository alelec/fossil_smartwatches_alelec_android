package com.fossil;

import com.google.android.gms.common.api.internal.LifecycleCallback;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jb2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ LifecycleCallback b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ ib2 d;

    @DexIgnore
    public jb2(ib2 ib2, LifecycleCallback lifecycleCallback, String str) {
        this.d = ib2;
        this.b = lifecycleCallback;
        this.c = str;
    }

    @DexIgnore
    public final void run() {
        if (ib2.v6(this.d) > 0) {
            this.b.f(ib2.x6(this.d) != null ? ib2.x6(this.d).getBundle(this.c) : null);
        }
        if (ib2.v6(this.d) >= 2) {
            this.b.j();
        }
        if (ib2.v6(this.d) >= 3) {
            this.b.h();
        }
        if (ib2.v6(this.d) >= 4) {
            this.b.k();
        }
        if (ib2.v6(this.d) >= 5) {
            this.b.g();
        }
    }
}
