package com.fossil;

import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nu {
    @DexIgnore
    public /* synthetic */ nu(kq7 kq7) {
    }

    @DexIgnore
    public final byte[] a(ul1[] ul1Arr) {
        ByteBuffer allocate = ByteBuffer.allocate(ul1Arr.length * 5);
        for (ul1 ul1 : ul1Arr) {
            allocate.put(ul1.a());
        }
        byte[] array = allocate.array();
        pq7.b(array, "array.array()");
        return array;
    }
}
