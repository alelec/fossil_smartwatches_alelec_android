package com.fossil;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface kw0 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends Binder implements kw0 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kw0$a$a")
        /* renamed from: com.fossil.kw0$a$a  reason: collision with other inner class name */
        public static class C0137a implements kw0 {
            @DexIgnore
            public IBinder b;

            @DexIgnore
            public C0137a(IBinder iBinder) {
                this.b = iBinder;
            }

            @DexIgnore
            @Override // com.fossil.kw0
            public void T(String[] strArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationCallback");
                    obtain.writeStringArray(strArr);
                    this.b.transact(1, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            @DexIgnore
            public IBinder asBinder() {
                return this.b;
            }
        }

        @DexIgnore
        public a() {
            attachInterface(this, "androidx.room.IMultiInstanceInvalidationCallback");
        }

        @DexIgnore
        public static kw0 d(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("androidx.room.IMultiInstanceInvalidationCallback");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof kw0)) ? new C0137a(iBinder) : (kw0) queryLocalInterface;
        }

        @DexIgnore
        public IBinder asBinder() {
            return this;
        }

        @DexIgnore
        @Override // android.os.Binder
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationCallback");
                T(parcel.createStringArray());
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("androidx.room.IMultiInstanceInvalidationCallback");
                return true;
            }
        }
    }

    @DexIgnore
    void T(String[] strArr) throws RemoteException;
}
