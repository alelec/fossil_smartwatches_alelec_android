package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rm {
    @DexIgnore
    public /* synthetic */ rm(kq7 kq7) {
    }

    @DexIgnore
    public final byte[] a(rl1[] rl1Arr, short s, ry1 ry1) {
        Object[] array = em7.D(rl1Arr).toArray(new rl1[0]);
        if (array != null) {
            rl1[] rl1Arr2 = (rl1[]) array;
            if (rl1Arr2.length == 0) {
                return new byte[0];
            }
            try {
                return xa.d.a(s, ry1, rl1Arr2);
            } catch (sx1 e) {
                return new byte[0];
            }
        } else {
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }
}
