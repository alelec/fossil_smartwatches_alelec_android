package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o6 {
    @DexIgnore
    public /* synthetic */ o6(kq7 kq7) {
    }

    @DexIgnore
    public final n6 a(UUID uuid) {
        return pq7.a(uuid, hd0.y.h()) ? n6.MODEL_NUMBER : pq7.a(uuid, hd0.y.i()) ? n6.SERIAL_NUMBER : pq7.a(uuid, hd0.y.g()) ? n6.FIRMWARE_VERSION : pq7.a(uuid, hd0.y.j()) ? n6.SOFTWARE_REVISION : pq7.a(uuid, hd0.y.q()) ? n6.DC : pq7.a(uuid, hd0.y.r()) ? n6.FTC : pq7.a(uuid, hd0.y.s()) ? n6.FTD : pq7.a(uuid, hd0.y.o()) ? n6.ASYNC : pq7.a(uuid, hd0.y.t()) ? n6.FTD_1 : pq7.a(uuid, hd0.y.p()) ? n6.AUTHENTICATION : pq7.a(uuid, hd0.y.l()) ? n6.HEART_RATE : n6.UNKNOWN;
    }
}
