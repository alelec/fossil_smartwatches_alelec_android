package com.fossil;

import com.fossil.mz2;
import com.fossil.nz2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mz2<MessageType extends nz2<MessageType, BuilderType>, BuilderType extends mz2<MessageType, BuilderType>> implements p23 {
    @DexIgnore
    @Override // com.fossil.p23
    public final /* synthetic */ p23 A(byte[] bArr) throws l13 {
        p(bArr, 0, bArr.length);
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.mz2<MessageType extends com.fossil.nz2<MessageType, BuilderType>, BuilderType extends com.fossil.mz2<MessageType, BuilderType>> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.p23
    public final /* synthetic */ p23 F(m23 m23) {
        if (d().getClass().isInstance(m23)) {
            o((nz2) m23);
            return this;
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }

    @DexIgnore
    @Override // com.fossil.p23
    public final /* synthetic */ p23 k(byte[] bArr, q03 q03) throws l13 {
        q(bArr, 0, bArr.length, q03);
        return this;
    }

    @DexIgnore
    public abstract BuilderType o(MessageType messagetype);

    @DexIgnore
    public abstract BuilderType p(byte[] bArr, int i, int i2) throws l13;

    @DexIgnore
    public abstract BuilderType q(byte[] bArr, int i, int i2, q03 q03) throws l13;
}
