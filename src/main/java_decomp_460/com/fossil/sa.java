package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sa extends xx1<ac0[]> {
    @DexIgnore
    public static /* final */ sa f; // = new sa();

    @DexIgnore
    public sa() {
        super(DateTimeFieldType.SECOND_OF_MINUTE, (byte) 253, new ry1(3, 0));
    }

    @DexIgnore
    /* renamed from: f */
    public ac0[] d(byte[] bArr) {
        ArrayList arrayList = new ArrayList();
        int i = 12;
        while (i < bArr.length - 1) {
            int i2 = i + 2;
            int n = hy1.n(ByteBuffer.wrap(dm7.k(bArr, i, i2)).order(ByteOrder.LITTLE_ENDIAN).getShort(0));
            if (i >= (bArr.length - 1) - n) {
                break;
            }
            ac0 a2 = ac0.CREATOR.a(dm7.k(bArr, i2, i2 + n));
            if (a2 != null) {
                arrayList.add(a2);
            }
            i += n + 2;
        }
        Object[] array = arrayList.toArray(new ac0[0]);
        if (array != null) {
            return (ac0[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
