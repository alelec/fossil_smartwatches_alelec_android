package com.fossil;

import androidx.lifecycle.MutableLiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zc6 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public MutableLiveData<a> f4447a; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Integer f4448a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public a(Integer num, boolean z) {
            this.f4448a = num;
            this.b = z;
        }

        @DexIgnore
        public final Integer a() {
            return this.f4448a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!pq7.a(this.f4448a, aVar.f4448a) || this.b != aVar.b) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            Integer num = this.f4448a;
            int hashCode = num != null ? num.hashCode() : 0;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(tutorialResource=" + this.f4448a + ", showLoading=" + this.b + ")";
        }
    }

    /*
    static {
        pq7.b(zc6.class.getSimpleName(), "CustomizeTutorialViewModel::class.java.simpleName");
    }
    */

    @DexIgnore
    public static /* synthetic */ void b(zc6 zc6, Integer num, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            num = null;
        }
        if ((i & 2) != 0) {
            z = false;
        }
        zc6.a(num, z);
    }

    @DexIgnore
    public final void a(Integer num, boolean z) {
        this.f4447a.l(new a(num, z));
    }

    @DexIgnore
    public final MutableLiveData<a> c() {
        return this.f4447a;
    }

    @DexIgnore
    public final void d(String str) {
        pq7.c(str, "watchAppId");
        b(this, ol5.c.d(str), false, 2, null);
    }
}
