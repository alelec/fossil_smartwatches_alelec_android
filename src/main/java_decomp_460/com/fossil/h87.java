package com.fossil;

import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h87 extends hq4 {
    @DexIgnore
    public /* final */ MutableLiveData<c> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<b> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Complication> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<List<Complication>> l; // = or0.c(null, 0, new d(this, null), 3, null);
    @DexIgnore
    public /* final */ MutableLiveData<List<DianaAppSetting>> m;
    @DexIgnore
    public /* final */ ComplicationRepository n;
    @DexIgnore
    public /* final */ DianaAppSettingRepository o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel$1", f = "WatchFaceComplicationViewModel.kt", l = {48}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ h87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(h87 h87, qn7 qn7) {
            super(2, qn7);
            this.this$0 = h87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v4, types: [androidx.lifecycle.MutableLiveData] */
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            MutableLiveData<List<DianaAppSetting>> u;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                u = this.this$0.u();
                DianaAppSettingRepository dianaAppSettingRepository = this.this$0.o;
                this.L$0 = iv7;
                this.L$1 = u;
                this.label = 1;
                obj = dianaAppSettingRepository.getAllDianaAppSettings("complication", this);
                if (obj == d) {
                    return d;
                }
            } else if (i == 1) {
                u = (MutableLiveData) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            u.l(obj);
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f1449a;
        @DexIgnore
        public String b;

        @DexIgnore
        public b(String str, String str2) {
            pq7.c(str, "complicationId");
            pq7.c(str2, MicroAppSetting.SETTING);
            this.f1449a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.f1449a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (!pq7.a(this.f1449a, bVar.f1449a) || !pq7.a(this.b, bVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.f1449a;
            int hashCode = str != null ? str.hashCode() : 0;
            String str2 = this.b;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "SettingScreenNavigation(complicationId=" + this.f1449a + ", setting=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f1450a;

        @DexIgnore
        public c() {
            this(null, 1, null);
        }

        @DexIgnore
        public c(String str) {
            pq7.c(str, "content");
            this.f1450a = str;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(String str, int i, kq7 kq7) {
            this((i & 1) != 0 ? "" : str);
        }

        @DexIgnore
        public final String a() {
            return this.f1450a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || ((obj instanceof c) && pq7.a(this.f1450a, ((c) obj).f1450a));
        }

        @DexIgnore
        public int hashCode() {
            String str = this.f1450a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "SettingValue(content=" + this.f1450a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel$complicationsLiveData$1", f = "WatchFaceComplicationViewModel.kt", l = {39, 39}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<hs0<List<? extends Complication>>, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public hs0 p$;
        @DexIgnore
        public /* final */ /* synthetic */ h87 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(h87 h87, qn7 qn7) {
            super(2, qn7);
            this.this$0 = h87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (hs0) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(hs0<List<? extends Complication>> hs0, qn7<? super tl7> qn7) {
            return ((d) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r6) {
            /*
                r5 = this;
                r4 = 2
                r2 = 1
                java.lang.Object r3 = com.fossil.yn7.d()
                int r0 = r5.label
                if (r0 == 0) goto L_0x0038
                if (r0 == r2) goto L_0x0020
                if (r0 != r4) goto L_0x0018
                java.lang.Object r0 = r5.L$0
                com.fossil.hs0 r0 = (com.fossil.hs0) r0
                com.fossil.el7.b(r6)
            L_0x0015:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r5.L$1
                com.fossil.hs0 r0 = (com.fossil.hs0) r0
                java.lang.Object r1 = r5.L$0
                com.fossil.hs0 r1 = (com.fossil.hs0) r1
                com.fossil.el7.b(r6)
                r2 = r1
            L_0x002c:
                r5.L$0 = r2
                r5.label = r4
                java.lang.Object r0 = r0.b(r6, r5)
                if (r0 != r3) goto L_0x0015
                r0 = r3
                goto L_0x0017
            L_0x0038:
                com.fossil.el7.b(r6)
                com.fossil.hs0 r1 = r5.p$
                com.fossil.h87 r0 = r5.this$0
                com.portfolio.platform.data.source.ComplicationRepository r0 = com.fossil.h87.n(r0)
                r5.L$0 = r1
                r5.L$1 = r1
                r5.label = r2
                java.lang.Object r6 = r0.getAllComplicationRaw(r5)
                if (r6 != r3) goto L_0x0051
                r0 = r3
                goto L_0x0017
            L_0x0051:
                r0 = r1
                r2 = r1
                goto L_0x002c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.h87.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel$getComplication$1", f = "WatchFaceComplicationViewModel.kt", l = {88}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $complicationId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ h87 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel$getComplication$1$complication$1", f = "WatchFaceComplicationViewModel.kt", l = {89}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Complication>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Complication> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    ComplicationRepository complicationRepository = this.this$0.this$0.n;
                    String str = this.this$0.$complicationId;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object complicationById = complicationRepository.getComplicationById(str, this);
                    return complicationById == d ? d : complicationById;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(h87 h87, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = h87;
            this.$complicationId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, this.$complicationId, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchFaceComplicationViewModel", "getComplication, complicationId = " + this.$complicationId);
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.w().o((Complication) g);
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel$getComplicationSetting$1", f = "WatchFaceComplicationViewModel.kt", l = {100}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $complicationId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ h87 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel$getComplicationSetting$1$complicationSetting$1", f = "WatchFaceComplicationViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super DianaAppSetting>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super DianaAppSetting> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                T t;
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    List<DianaAppSetting> e = this.this$0.this$0.u().e();
                    if (e == null) {
                        return null;
                    }
                    Iterator<T> it = e.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        if (ao7.a(pq7.a(next.getAppId(), this.this$0.$complicationId)).booleanValue()) {
                            t = next;
                            break;
                        }
                    }
                    return t;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(h87 h87, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = h87;
            this.$complicationId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, this.$complicationId, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            String a2;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchFaceComplicationViewModel", "getComplicationSetting - complicationId = " + this.$complicationId);
                if (!ik5.d.d(this.$complicationId)) {
                    this.this$0.y().o(new c(null, 1, null));
                    return tl7.f3441a;
                }
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            DianaAppSetting dianaAppSetting = (DianaAppSetting) g;
            if (dianaAppSetting == null || !(!vt7.l(dianaAppSetting.getSetting()))) {
                a2 = ik5.d.a(this.$complicationId);
            } else {
                Gson gson = new Gson();
                try {
                    String str = this.$complicationId;
                    int hashCode = str.hashCode();
                    if (hashCode != -829740640) {
                        if (hashCode == 134170930 && str.equals("second-timezone")) {
                            a2 = ((SecondTimezoneSetting) gson.k(dianaAppSetting.getSetting(), SecondTimezoneSetting.class)).getTimeZoneName();
                        }
                    } else if (str.equals("commute-time")) {
                        a2 = ((CommuteTimeSetting) gson.k(dianaAppSetting.getSetting(), CommuteTimeSetting.class)).getAddress();
                    }
                    a2 = "";
                } catch (Exception e) {
                    FLogger.INSTANCE.getLocal().e("WatchFaceComplicationViewModel", e.getMessage());
                    e.printStackTrace();
                    a2 = ik5.d.a(this.$complicationId);
                }
            }
            this.this$0.y().o(new c(a2));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel$navigateToSettingScreen$1", f = "WatchFaceComplicationViewModel.kt", l = {72}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ h87 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $complicationId;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str, qn7 qn7, g gVar) {
                super(2, qn7);
                this.$complicationId = str;
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$complicationId, qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                T t;
                String setting;
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    List<DianaAppSetting> e = this.this$0.this$0.u().e();
                    if (e != null) {
                        Iterator<T> it = e.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t = null;
                                break;
                            }
                            T next = it.next();
                            if (ao7.a(pq7.a(next.getAppId(), this.$complicationId)).booleanValue()) {
                                t = next;
                                break;
                            }
                        }
                        T t2 = t;
                        return (t2 == null || (setting = t2.getSetting()) == null) ? "" : setting;
                    }
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(h87 h87, qn7 qn7) {
            super(2, qn7);
            this.this$0 = h87;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, qn7);
            gVar.p$ = (iv7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            String complicationId;
            Object g;
            String str;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                Complication e = this.this$0.w().e();
                if (!(e == null || (complicationId = e.getComplicationId()) == null)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchFaceComplicationViewModel", "navigateToSettingScreen, complicationId = " + complicationId);
                    dv7 b = bw7.b();
                    a aVar = new a(complicationId, null, this);
                    this.L$0 = iv7;
                    this.L$1 = complicationId;
                    this.label = 1;
                    g = eu7.g(b, aVar, this);
                    if (g == d) {
                        return d;
                    }
                    str = complicationId;
                }
                return tl7.f3441a;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                str = (String) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.x().l(new b(str, (String) g));
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public h87(ComplicationRepository complicationRepository, DianaAppSettingRepository dianaAppSettingRepository) {
        pq7.c(complicationRepository, "mComplicationRepository");
        pq7.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        this.n = complicationRepository;
        this.o = dianaAppSettingRepository;
        new MutableLiveData();
        this.m = new MutableLiveData<>();
        xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new a(this, null), 2, null);
    }

    @DexIgnore
    public final void A(String str, Parcelable parcelable) {
        Object obj;
        pq7.c(str, "appId");
        pq7.c(parcelable, "data");
        ArrayList arrayList = new ArrayList();
        List<DianaAppSetting> e2 = this.m.e();
        if (e2 == null) {
            e2 = new ArrayList<>();
        }
        arrayList.addAll(e2);
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            Object next = it.next();
            if (pq7.a(((DianaAppSetting) next).getAppId(), str)) {
                obj = next;
                break;
            }
        }
        DianaAppSetting dianaAppSetting = (DianaAppSetting) obj;
        if (dianaAppSetting != null) {
            dianaAppSetting.setSetting(jj5.a(parcelable));
        } else {
            String e3 = ym5.e(24);
            pq7.b(e3, "StringHelper.randomUUID(24)");
            arrayList.add(new DianaAppSetting(e3, str, "complication", jj5.a(parcelable)));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceComplicationViewModel", "setting is changed " + arrayList);
        this.m.l(arrayList);
    }

    @DexIgnore
    public final void p(String str) {
        pq7.c(str, "complicationId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceComplicationViewModel", "buildData, complicationId = " + str);
        s(str);
        t(str);
    }

    @DexIgnore
    public final void q() {
        List<Complication> e2 = this.l.e();
        if (e2 != null) {
            Iterator<T> it = e2.iterator();
            while (it.hasNext()) {
                if (!ik5.d.f(it.next().getComplicationId())) {
                    this.j.l(Boolean.FALSE);
                    return;
                }
            }
        }
        this.j.l(Boolean.TRUE);
    }

    @DexIgnore
    public final MutableLiveData<Boolean> r() {
        return this.j;
    }

    @DexIgnore
    public final xw7 s(String str) {
        return gu7.d(us0.a(this), null, null, new e(this, str, null), 3, null);
    }

    @DexIgnore
    public final xw7 t(String str) {
        return gu7.d(us0.a(this), null, null, new f(this, str, null), 3, null);
    }

    @DexIgnore
    public final MutableLiveData<List<DianaAppSetting>> u() {
        return this.m;
    }

    @DexIgnore
    public final LiveData<List<Complication>> v() {
        return this.l;
    }

    @DexIgnore
    public final MutableLiveData<Complication> w() {
        return this.k;
    }

    @DexIgnore
    public final MutableLiveData<b> x() {
        return this.i;
    }

    @DexIgnore
    public final MutableLiveData<c> y() {
        return this.h;
    }

    @DexIgnore
    public final xw7 z() {
        return gu7.d(us0.a(this), null, null, new g(this, null), 3, null);
    }
}
