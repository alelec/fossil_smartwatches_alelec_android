package com.fossil;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class mj2 extends hk2 {
    @DexIgnore
    public bq2 g; // = cq2.f638a;

    @DexIgnore
    public static void g(Bundle bundle) {
        Iterator<String> it = bundle.keySet().iterator();
        while (it.hasNext()) {
            String next = it.next();
            if (next != null && next.startsWith("google.c.")) {
                it.remove();
            }
        }
    }

    @DexIgnore
    public void c() {
    }

    @DexIgnore
    public void d(String str, Bundle bundle) {
    }

    @DexIgnore
    public void e(String str) {
    }

    @DexIgnore
    public void f(String str, String str2) {
    }

    @DexIgnore
    @Override // com.fossil.hk2
    public void handleIntent(Intent intent) {
        boolean z;
        if (!"com.google.android.c2dm.intent.RECEIVE".equals(intent.getAction())) {
            String valueOf = String.valueOf(intent.getAction());
            Log.w("GcmListenerService", valueOf.length() != 0 ? "Unknown intent action: ".concat(valueOf) : new String("Unknown intent action: "));
            return;
        }
        String stringExtra = intent.getStringExtra("message_type");
        if (stringExtra == null) {
            stringExtra = "gcm";
        }
        char c = '\uffff';
        switch (stringExtra.hashCode()) {
            case -2062414158:
                if (stringExtra.equals("deleted_messages")) {
                    c = 1;
                    break;
                }
                break;
            case 102161:
                if (stringExtra.equals("gcm")) {
                    c = 0;
                    break;
                }
                break;
            case 814694033:
                if (stringExtra.equals("send_error")) {
                    c = 3;
                    break;
                }
                break;
            case 814800675:
                if (stringExtra.equals("send_event")) {
                    c = 2;
                    break;
                }
                break;
        }
        if (c == 0) {
            Bundle extras = intent.getExtras();
            extras.remove("message_type");
            extras.remove("androidx.contentpager.content.wakelockid");
            if ("1".equals(sj2.b(extras, "gcm.n.e")) || sj2.b(extras, "gcm.n.icon") != null) {
                if (!((KeyguardManager) getSystemService("keyguard")).inKeyguardRestrictedInputMode()) {
                    int myPid = Process.myPid();
                    List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) getSystemService(Constants.ACTIVITY)).getRunningAppProcesses();
                    if (runningAppProcesses != null) {
                        Iterator<ActivityManager.RunningAppProcessInfo> it = runningAppProcesses.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                ActivityManager.RunningAppProcessInfo next = it.next();
                                if (next.pid == myPid) {
                                    if (next.importance == 100) {
                                        z = true;
                                    }
                                }
                            }
                        }
                    }
                }
                z = false;
                if (!z) {
                    sj2.a(this).d(extras);
                    return;
                }
                Bundle bundle = new Bundle();
                Iterator<String> it2 = extras.keySet().iterator();
                while (it2.hasNext()) {
                    String next2 = it2.next();
                    String string = extras.getString(next2);
                    if (next2.startsWith("gcm.notification.")) {
                        next2 = next2.replace("gcm.notification.", "gcm.n.");
                    }
                    if (next2.startsWith("gcm.n.")) {
                        if (!"gcm.n.e".equals(next2)) {
                            bundle.putString(next2.substring(6), string);
                        }
                        it2.remove();
                    }
                }
                String string2 = bundle.getString("sound2");
                if (string2 != null) {
                    bundle.remove("sound2");
                    bundle.putString(Constants.YO_PARAMS_SOUND, string2);
                }
                if (!bundle.isEmpty()) {
                    extras.putBundle("notification", bundle);
                }
            }
            String string3 = extras.getString("from");
            extras.remove("from");
            g(extras);
            this.g.a("onMessageReceived");
            d(string3, extras);
        } else if (c == 1) {
            c();
        } else if (c == 2) {
            e(intent.getStringExtra("google.message_id"));
        } else if (c != 3) {
            String valueOf2 = String.valueOf(stringExtra);
            Log.w("GcmListenerService", valueOf2.length() != 0 ? "Received message with unknown type: ".concat(valueOf2) : new String("Received message with unknown type: "));
        } else {
            String stringExtra2 = intent.getStringExtra("google.message_id");
            if (stringExtra2 == null) {
                stringExtra2 = intent.getStringExtra("message_id");
            }
            f(stringExtra2, intent.getStringExtra("error"));
        }
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        cq2.a();
        getClass();
        this.g = cq2.f638a;
    }
}
