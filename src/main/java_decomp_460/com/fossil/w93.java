package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w93 extends ss2 implements u93 {
    @DexIgnore
    public w93(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IBundleReceiver");
    }

    @DexIgnore
    @Override // com.fossil.u93
    public final void c(Bundle bundle) throws RemoteException {
        Parcel d = d();
        qt2.c(d, bundle);
        i(1, d);
    }
}
