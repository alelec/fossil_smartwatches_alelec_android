package com.fossil;

import com.fossil.tu3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dv3 implements su3 {
    @DexIgnore
    public /* final */ tu3.b b;

    @DexIgnore
    public dv3(tu3.b bVar) {
        this.b = bVar;
    }

    @DexIgnore
    @Override // com.fossil.su3
    public final void a(ru3 ru3) {
        this.b.b(cv3.s(ru3));
    }

    @DexIgnore
    @Override // com.fossil.su3
    public final void b(ru3 ru3, int i, int i2) {
        this.b.a(cv3.s(ru3), i, i2);
    }

    @DexIgnore
    @Override // com.fossil.su3
    public final void c(ru3 ru3, int i, int i2) {
        this.b.c(cv3.s(ru3), i, i2);
    }

    @DexIgnore
    @Override // com.fossil.su3
    public final void d(ru3 ru3, int i, int i2) {
        this.b.d(cv3.s(ru3), i, i2);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || dv3.class != obj.getClass()) {
            return false;
        }
        return this.b.equals(((dv3) obj).b);
    }

    @DexIgnore
    public final int hashCode() {
        return this.b.hashCode();
    }
}
