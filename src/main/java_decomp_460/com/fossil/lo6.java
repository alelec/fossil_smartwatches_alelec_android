package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lo6 extends go6 {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ ho6 e;

    /*
    static {
        String simpleName = lo6.class.getSimpleName();
        pq7.b(simpleName, "AboutPresenter::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public lo6(ho6 ho6) {
        pq7.c(ho6, "mView");
        this.e = ho6;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(f, "presenter start");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(f, "presenter stop");
    }

    @DexIgnore
    public void n() {
        this.e.M5(this);
    }
}
