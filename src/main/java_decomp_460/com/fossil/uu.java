package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uu extends mu {
    @DexIgnore
    public int L; // = 20;
    @DexIgnore
    public /* final */ boolean M;

    @DexIgnore
    public uu(k5 k5Var) {
        super(fu.k, hs.h, k5Var, 0, 8);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(super.A(), jd0.K0, Integer.valueOf(this.L));
    }

    @DexIgnore
    @Override // com.fossil.ps
    public JSONObject F(byte[] bArr) {
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 2) {
            int n = hy1.n(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0));
            this.L = n;
            g80.k(jSONObject, jd0.K0, Integer.valueOf(n));
            this.v = mw.a(this.v, null, null, lw.b, null, null, 27);
        } else {
            this.v = mw.a(this.v, null, null, lw.k, null, null, 27);
        }
        this.E = true;
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public boolean O() {
        return this.M;
    }
}
