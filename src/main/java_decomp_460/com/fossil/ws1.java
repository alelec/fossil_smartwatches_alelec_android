package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ws1 extends ox1 implements Serializable, Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ zs1[] d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ws1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ws1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    pq7.b(readString2, "parcel.readString()!!");
                    Object[] createTypedArray = parcel.createTypedArray(zs1.CREATOR);
                    if (createTypedArray != null) {
                        pq7.b(createTypedArray, "parcel.createTypedArray(Player.CREATOR)!!");
                        return new ws1(readString, readString2, (zs1[]) createTypedArray);
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ws1[] newArray(int i) {
            return new ws1[i];
        }
    }

    @DexIgnore
    public ws1(String str, String str2, zs1[] zs1Arr) {
        this.b = str;
        this.c = str2;
        this.d = zs1Arr;
    }

    @DexIgnore
    public final JSONObject a() {
        JSONObject k = g80.k(g80.k(g80.k(new JSONObject(), jd0.D4, this.b), jd0.H, this.c), jd0.H4, Integer.valueOf(this.d.length));
        g80.k(k, jd0.F4, px1.a(this.d));
        return k;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getChallengeId() {
        return this.b;
    }

    @DexIgnore
    public final String getName() {
        return this.c;
    }

    @DexIgnore
    public final zs1[] getPlayers() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject k = g80.k(g80.k(g80.k(new JSONObject(), jd0.D4, this.b), jd0.H, this.c), jd0.H4, Integer.valueOf(this.d.length));
        g80.k(k, jd0.F4, px1.a(this.d));
        return k;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeTypedArray(this.d, i);
    }
}
