package com.fossil;

import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ue4 implements Runnable {
    @DexIgnore
    public /* final */ FirebaseInstanceId b;

    @DexIgnore
    public ue4(FirebaseInstanceId firebaseInstanceId) {
        this.b = firebaseInstanceId;
    }

    @DexIgnore
    public final void run() {
        this.b.D();
    }
}
