package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yv1 extends sv1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public bw1 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<yv1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public yv1 createFromParcel(Parcel parcel) {
            return new yv1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public yv1[] newArray(int i) {
            return new yv1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ yv1(Parcel parcel, kq7 kq7) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(bw1.class.getClassLoader());
        if (readParcelable != null) {
            this.k = (bw1) readParcelable;
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public yv1(jv1 jv1, kv1 kv1, bw1 bw1) {
        super(vv1.SECOND_TIMEZONE, jv1, kv1, false, null, null, 56);
        this.k = bw1;
    }

    @DexIgnore
    public yv1(JSONObject jSONObject, cc0[] cc0Arr) throws IllegalArgumentException {
        super(jSONObject, cc0Arr, null, 4);
        JSONObject optJSONObject = jSONObject.optJSONObject("data");
        this.k = new bw1(optJSONObject == null ? new JSONObject() : optJSONObject);
    }

    @DexIgnore
    @Override // com.fossil.sv1, java.lang.Object, com.fossil.mv1, com.fossil.mv1
    public yv1 clone() {
        return new yv1(b().clone(), c().clone(), this.k.clone());
    }

    @DexIgnore
    @Override // com.fossil.sv1
    public JSONObject e() {
        return this.k.a();
    }

    @DexIgnore
    public final bw1 getTimeZoneDataConfig() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.sv1
    public yv1 setBackgroundImage(tv1 tv1) {
        return (yv1) super.setBackgroundImage(tv1);
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public yv1 setScaledHeight(float f) {
        mv1 scaledHeight = super.setScaledHeight(f);
        if (scaledHeight != null) {
            return (yv1) scaledHeight;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.SecondTimeZoneComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public yv1 setScaledPosition(jv1 jv1) {
        mv1 scaledPosition = super.setScaledPosition(jv1);
        if (scaledPosition != null) {
            return (yv1) scaledPosition;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.SecondTimeZoneComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public yv1 setScaledSize(kv1 kv1) {
        mv1 scaledSize = super.setScaledSize(kv1);
        if (scaledSize != null) {
            return (yv1) scaledSize;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.SecondTimeZoneComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public yv1 setScaledWidth(float f) {
        mv1 scaledWidth = super.setScaledWidth(f);
        if (scaledWidth != null) {
            return (yv1) scaledWidth;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.SecondTimeZoneComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public yv1 setScaledX(float f) {
        mv1 scaledX = super.setScaledX(f);
        if (scaledX != null) {
            return (yv1) scaledX;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.SecondTimeZoneComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public yv1 setScaledY(float f) {
        mv1 scaledY = super.setScaledY(f);
        if (scaledY != null) {
            return (yv1) scaledY;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.SecondTimeZoneComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.sv1
    public yv1 setTheme(uv1 uv1) {
        sv1 theme = super.setTheme(uv1);
        if (theme != null) {
            return (yv1) theme;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.SecondTimeZoneComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.sv1, com.fossil.mv1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.k, i);
        }
    }
}
