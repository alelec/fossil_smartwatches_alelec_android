package com.fossil;

import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kz4<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ T f2123a;
    @DexIgnore
    public /* final */ Range b;
    @DexIgnore
    public /* final */ ServerError c;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public kz4(ServerError serverError) {
        this(null, null, serverError);
        pq7.c(serverError, "error");
    }

    @DexIgnore
    public kz4(T t, Range range) {
        this(t, range, null);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ kz4(Object obj, Range range, int i, kq7 kq7) {
        this(obj, (i & 2) != 0 ? null : range);
    }

    @DexIgnore
    public kz4(T t, Range range, ServerError serverError) {
        this.f2123a = t;
        this.b = range;
        this.c = serverError;
    }

    @DexIgnore
    public final ServerError a() {
        return this.c;
    }

    @DexIgnore
    public final Range b() {
        return this.b;
    }

    @DexIgnore
    public final T c() {
        return this.f2123a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof kz4) {
                kz4 kz4 = (kz4) obj;
                if (!pq7.a(this.f2123a, kz4.f2123a) || !pq7.a(this.b, kz4.b) || !pq7.a(this.c, kz4.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        T t = this.f2123a;
        int hashCode = t != null ? t.hashCode() : 0;
        Range range = this.b;
        int hashCode2 = range != null ? range.hashCode() : 0;
        ServerError serverError = this.c;
        if (serverError != null) {
            i = serverError.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "ResultWrapper(value=" + ((Object) this.f2123a) + ", range=" + this.b + ", error=" + this.c + ")";
    }
}
