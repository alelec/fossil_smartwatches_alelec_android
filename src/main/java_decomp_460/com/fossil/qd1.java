package com.fossil;

import com.fossil.zd1;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qd1<T extends zd1> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Queue<T> f2963a; // = jk1.f(20);

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public T b() {
        T poll = this.f2963a.poll();
        return poll == null ? a() : poll;
    }

    @DexIgnore
    public void c(T t) {
        if (this.f2963a.size() < 20) {
            this.f2963a.offer(t);
        }
    }
}
