package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cf3 implements Parcelable.Creator<je3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ je3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        String str = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            if (ad2.l(t) != 2) {
                ad2.B(parcel, t);
            } else {
                str = ad2.f(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new je3(str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ je3[] newArray(int i) {
        return new je3[i];
    }
}
