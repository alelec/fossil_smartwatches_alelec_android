package com.fossil;

import com.fossil.e13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ev2 extends e13<ev2, a> implements o23 {
    @DexIgnore
    public static /* final */ ev2 zzj;
    @DexIgnore
    public static volatile z23<ev2> zzk;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public long zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public String zzf; // = "";
    @DexIgnore
    public long zzg;
    @DexIgnore
    public float zzh;
    @DexIgnore
    public double zzi;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<ev2, a> implements o23 {
        @DexIgnore
        public a() {
            super(ev2.zzj);
        }

        @DexIgnore
        public /* synthetic */ a(tu2 tu2) {
            this();
        }

        @DexIgnore
        public final a B(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((ev2) this.c).J(str);
            return this;
        }

        @DexIgnore
        public final a C() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((ev2) this.c).c0();
            return this;
        }

        @DexIgnore
        public final a E(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((ev2) this.c).M(j);
            return this;
        }

        @DexIgnore
        public final a G(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((ev2) this.c).Q(str);
            return this;
        }

        @DexIgnore
        public final a H() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((ev2) this.c).d0();
            return this;
        }

        @DexIgnore
        public final a x() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((ev2) this.c).b0();
            return this;
        }

        @DexIgnore
        public final a y(double d) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((ev2) this.c).C(d);
            return this;
        }

        @DexIgnore
        public final a z(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((ev2) this.c).D(j);
            return this;
        }
    }

    /*
    static {
        ev2 ev2 = new ev2();
        zzj = ev2;
        e13.u(ev2.class, ev2);
    }
    */

    @DexIgnore
    public static a Z() {
        return (a) zzj.w();
    }

    @DexIgnore
    public final void C(double d) {
        this.zzc |= 32;
        this.zzi = d;
    }

    @DexIgnore
    public final void D(long j) {
        this.zzc |= 1;
        this.zzd = j;
    }

    @DexIgnore
    public final void J(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    @DexIgnore
    public final boolean K() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final long L() {
        return this.zzd;
    }

    @DexIgnore
    public final void M(long j) {
        this.zzc |= 8;
        this.zzg = j;
    }

    @DexIgnore
    public final void Q(String str) {
        str.getClass();
        this.zzc |= 4;
        this.zzf = str;
    }

    @DexIgnore
    public final String R() {
        return this.zze;
    }

    @DexIgnore
    public final boolean T() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final String U() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean V() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final long W() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean X() {
        return (this.zzc & 32) != 0;
    }

    @DexIgnore
    public final double Y() {
        return this.zzi;
    }

    @DexIgnore
    public final void b0() {
        this.zzc &= -5;
        this.zzf = zzj.zzf;
    }

    @DexIgnore
    public final void c0() {
        this.zzc &= -9;
        this.zzg = 0;
    }

    @DexIgnore
    public final void d0() {
        this.zzc &= -33;
        this.zzi = 0.0d;
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (tu2.f3469a[i - 1]) {
            case 1:
                return new ev2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u1002\u0000\u0002\u1008\u0001\u0003\u1008\u0002\u0004\u1002\u0003\u0005\u1001\u0004\u0006\u1000\u0005", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi"});
            case 4:
                return zzj;
            case 5:
                z23<ev2> z232 = zzk;
                if (z232 != null) {
                    return z232;
                }
                synchronized (ev2.class) {
                    try {
                        z23 = zzk;
                        if (z23 == null) {
                            z23 = new e13.c(zzj);
                            zzk = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
