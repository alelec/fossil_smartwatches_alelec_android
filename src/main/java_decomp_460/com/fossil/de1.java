package com.fossil;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class de1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<String, a> f772a; // = new HashMap();
    @DexIgnore
    public /* final */ b b; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Lock f773a; // = new ReentrantLock();
        @DexIgnore
        public int b;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Queue<a> f774a; // = new ArrayDeque();

        @DexIgnore
        public a a() {
            a poll;
            synchronized (this.f774a) {
                poll = this.f774a.poll();
            }
            return poll == null ? new a() : poll;
        }

        @DexIgnore
        public void b(a aVar) {
            synchronized (this.f774a) {
                if (this.f774a.size() < 10) {
                    this.f774a.offer(aVar);
                }
            }
        }
    }

    @DexIgnore
    public void a(String str) {
        a aVar;
        synchronized (this) {
            aVar = this.f772a.get(str);
            if (aVar == null) {
                aVar = this.b.a();
                this.f772a.put(str, aVar);
            }
            aVar.b++;
        }
        aVar.f773a.lock();
    }

    @DexIgnore
    public void b(String str) {
        a aVar;
        synchronized (this) {
            a aVar2 = this.f772a.get(str);
            ik1.d(aVar2);
            aVar = aVar2;
            if (aVar.b >= 1) {
                int i = aVar.b - 1;
                aVar.b = i;
                if (i == 0) {
                    a remove = this.f772a.remove(str);
                    if (remove.equals(aVar)) {
                        this.b.b(remove);
                    } else {
                        throw new IllegalStateException("Removed the wrong lock, expected to remove: " + aVar + ", but actually removed: " + remove + ", safeKey: " + str);
                    }
                }
            } else {
                throw new IllegalStateException("Cannot release a lock that is not held, safeKey: " + str + ", interestedThreads: " + aVar.b);
            }
        }
        aVar.f773a.unlock();
    }
}
