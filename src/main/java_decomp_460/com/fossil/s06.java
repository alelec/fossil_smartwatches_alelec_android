package com.fossil;

import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s06 implements Factory<r06> {
    @DexIgnore
    public static r06 a(n06 n06, uq4 uq4, v36 v36, d26 d26, u06 u06, on5 on5, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new r06(n06, uq4, v36, d26, u06, on5, notificationSettingsDatabase);
    }
}
