package com.fossil;

import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p04 extends s04 {
    @DexIgnore
    public p04(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    @Override // com.fossil.s04
    public void a() {
        this.f3188a.setEndIconOnClickListener(null);
        this.f3188a.setEndIconOnLongClickListener(null);
    }
}
