package com.fossil;

import android.animation.Animator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uy3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Animator f3672a;

    @DexIgnore
    public void a() {
        Animator animator = this.f3672a;
        if (animator != null) {
            animator.cancel();
        }
    }

    @DexIgnore
    public void b() {
        this.f3672a = null;
    }

    @DexIgnore
    public void c(Animator animator) {
        a();
        this.f3672a = animator;
    }
}
