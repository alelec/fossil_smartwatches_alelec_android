package com.fossil;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cr4 extends ArrayAdapter<AutocompletePrediction> implements Filterable {
    @DexIgnore
    public static /* final */ String g; // = "PlaceAutocompleteAdapter";
    @DexIgnore
    public static /* final */ StyleSpan h; // = new StyleSpan(1);
    @DexIgnore
    public List<? extends AutocompletePrediction> b; // = new ArrayList();
    @DexIgnore
    public a c;
    @DexIgnore
    public AutocompleteSessionToken d;
    @DexIgnore
    public Date e;
    @DexIgnore
    public /* final */ PlacesClient f;

    @DexIgnore
    public interface a {
        @DexIgnore
        void D1(boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Filter {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ cr4 f641a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(cr4 cr4) {
            this.f641a = cr4;
        }

        @DexIgnore
        public CharSequence convertResultToString(Object obj) {
            SpannableString fullText;
            pq7.c(obj, "resultValue");
            AutocompletePrediction autocompletePrediction = (AutocompletePrediction) (!(obj instanceof AutocompletePrediction) ? null : obj);
            if (autocompletePrediction != null && (fullText = autocompletePrediction.getFullText(null)) != null) {
                return fullText;
            }
            CharSequence convertResultToString = super.convertResultToString(obj);
            pq7.b(convertResultToString, "super.convertResultToString(resultValue)");
            return convertResultToString;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            Filter.FilterResults filterResults = new Filter.FilterResults();
            List arrayList = new ArrayList();
            if (charSequence != null) {
                arrayList = this.f641a.e(charSequence);
            }
            filterResults.values = arrayList;
            if (arrayList != null) {
                filterResults.count = arrayList.size();
            } else {
                filterResults.count = 0;
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            if (filterResults == null || filterResults.count <= 0) {
                this.f641a.notifyDataSetInvalidated();
                if (this.f641a.c != null) {
                    a aVar = this.f641a.c;
                    if (aVar != null) {
                        aVar.D1(true);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            } else {
                cr4 cr4 = this.f641a;
                Object obj = filterResults.values;
                if (obj != null) {
                    cr4.b = (List) obj;
                    this.f641a.notifyDataSetChanged();
                    if (this.f641a.c != null) {
                        a aVar2 = this.f641a.c;
                        if (aVar2 != null) {
                            aVar2.D1(false);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type kotlin.collections.List<com.google.android.libraries.places.api.model.AutocompletePrediction>");
                }
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cr4(Context context, PlacesClient placesClient) {
        super(context, 2131558489, 2131362965);
        pq7.c(context, "context");
        this.f = placesClient;
    }

    @DexIgnore
    public final void d() {
        this.d = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0025, code lost:
        if ((r2 - r0.getTime()) >= ((long) 180000)) goto L_0x0027;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.libraries.places.api.model.AutocompletePrediction> e(java.lang.CharSequence r7) {
        /*
            r6 = this;
            r1 = 0
            com.google.android.libraries.places.api.net.PlacesClient r0 = r6.f
            if (r0 == 0) goto L_0x00bb
            com.google.android.libraries.places.api.model.AutocompleteSessionToken r0 = r6.d
            if (r0 == 0) goto L_0x0027
            java.util.Date r0 = r6.e
            if (r0 == 0) goto L_0x0034
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            long r2 = r0.getTime()
            java.util.Date r0 = r6.e
            if (r0 == 0) goto L_0x0095
            long r4 = r0.getTime()
            long r2 = r2 - r4
            r0 = 180000(0x2bf20, float:2.52234E-40)
            long r4 = (long) r0
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 < 0) goto L_0x0034
        L_0x0027:
            com.google.android.libraries.places.api.model.AutocompleteSessionToken r0 = com.google.android.libraries.places.api.model.AutocompleteSessionToken.newInstance()
            r6.d = r0
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            r6.e = r0
        L_0x0034:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.cr4.g
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Starting autocomplete query for: "
            r3.append(r4)
            r3.append(r7)
            java.lang.String r3 = r3.toString()
            r0.d(r2, r3)
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest$Builder r0 = com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest.builder()
            java.lang.String r2 = r7.toString()
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest$Builder r0 = r0.setQuery(r2)
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest$Builder r0 = r0.setCountry(r1)
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest$Builder r0 = r0.setLocationBias(r1)
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest$Builder r0 = r0.setLocationRestriction(r1)
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest$Builder r0 = r0.setTypeFilter(r1)
            java.lang.String r2 = "FindAutocompletePredicti\u2026     .setTypeFilter(null)"
            com.fossil.pq7.b(r0, r2)
            com.google.android.libraries.places.api.model.AutocompleteSessionToken r2 = r6.d
            r0.setSessionToken(r2)
            com.google.android.libraries.places.api.net.PlacesClient r2 = r6.f
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest r0 = r0.build()
            com.fossil.nt3 r0 = r2.findAutocompletePredictions(r0)
            java.lang.String r2 = "mPlacesClient.findAutoco\u2026s(requestBuilder.build())"
            com.fossil.pq7.b(r0, r2)
            java.lang.Object r0 = com.fossil.qt3.a(r0)     // Catch:{ Exception -> 0x0099 }
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse r0 = (com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse) r0     // Catch:{ Exception -> 0x0099 }
            java.lang.String r2 = "response"
            com.fossil.pq7.b(r0, r2)     // Catch:{ Exception -> 0x0099 }
            java.util.List r0 = r0.getAutocompletePredictions()     // Catch:{ Exception -> 0x0099 }
        L_0x0094:
            return r0
        L_0x0095:
            com.fossil.pq7.i()
            throw r1
        L_0x0099:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.fossil.cr4.g
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Query failed. Received Exception="
            r4.append(r5)
            r4.append(r0)
            java.lang.String r4 = r4.toString()
            r2.d(r3, r4)
            r0.printStackTrace()
            r0 = r1
            goto L_0x0094
        L_0x00bb:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.cr4.g
            java.lang.String r3 = "client is not connected for autocomplete query."
            r0.e(r2, r3)
            r0 = r1
            goto L_0x0094
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cr4.e(java.lang.CharSequence):java.util.List");
    }

    @DexIgnore
    /* renamed from: f */
    public AutocompletePrediction getItem(int i) {
        return (AutocompletePrediction) this.b.get(i);
    }

    @DexIgnore
    public final AutocompleteSessionToken g() {
        return this.d;
    }

    @DexIgnore
    public int getCount() {
        return this.b.size();
    }

    @DexIgnore
    public Filter getFilter() {
        return new b(this);
    }

    @DexIgnore
    public View getView(int i, View view, ViewGroup viewGroup) {
        pq7.c(viewGroup, "parent");
        View view2 = super.getView(i, view, viewGroup);
        pq7.b(view2, "super.getView(position, convertView, parent)");
        AutocompletePrediction f2 = getItem(i);
        if (f2 != null) {
            FlexibleTextView flexibleTextView = (FlexibleTextView) view2.findViewById(2131362564);
            FlexibleTextView flexibleTextView2 = (FlexibleTextView) view2.findViewById(2131362965);
            FlexibleTextView flexibleTextView3 = (FlexibleTextView) view2.findViewById(2131363099);
            pq7.b(flexibleTextView, "fullTv");
            flexibleTextView.setText(f2.getFullText(h));
            pq7.b(flexibleTextView2, "textView1");
            flexibleTextView2.setText(f2.getPrimaryText(h));
            pq7.b(flexibleTextView3, "textView2");
            flexibleTextView3.setText(f2.getSecondaryText(h));
        }
        return view2;
    }

    @DexIgnore
    public final void h(a aVar) {
        this.c = aVar;
    }
}
