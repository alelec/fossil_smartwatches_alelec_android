package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qc4 implements rc4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2959a;
    @DexIgnore
    public /* final */ bd4 b;
    @DexIgnore
    public /* final */ sc4 c;
    @DexIgnore
    public /* final */ b94 d;
    @DexIgnore
    public /* final */ nc4 e;
    @DexIgnore
    public /* final */ fd4 f;
    @DexIgnore
    public /* final */ c94 g;
    @DexIgnore
    public /* final */ AtomicReference<zc4> h; // = new AtomicReference<>();
    @DexIgnore
    public /* final */ AtomicReference<ot3<wc4>> i; // = new AtomicReference<>(new ot3());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements mt3<Void, Void> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* renamed from: a */
        public nt3<Void> then(Void r7) throws Exception {
            JSONObject a2 = qc4.this.f.a(qc4.this.b, true);
            if (a2 != null) {
                ad4 b = qc4.this.c.b(a2);
                qc4.this.e.c(b.d(), a2);
                qc4.this.q(a2, "Loaded settings: ");
                qc4 qc4 = qc4.this;
                qc4.r(qc4.b.f);
                qc4.this.h.set(b);
                ((ot3) qc4.this.i.get()).e(b.c());
                ot3 ot3 = new ot3();
                ot3.e(b.c());
                qc4.this.i.set(ot3);
            }
            return qt3.f(null);
        }
    }

    @DexIgnore
    public qc4(Context context, bd4 bd4, b94 b94, sc4 sc4, nc4 nc4, fd4 fd4, c94 c94) {
        this.f2959a = context;
        this.b = bd4;
        this.d = b94;
        this.c = sc4;
        this.e = nc4;
        this.f = fd4;
        this.g = c94;
        this.h.set(oc4.e(b94));
    }

    @DexIgnore
    public static qc4 l(Context context, String str, h94 h94, kb4 kb4, String str2, String str3, String str4, c94 c94) {
        String e2 = h94.e();
        r94 r94 = new r94();
        return new qc4(context, new bd4(str, h94.f(), h94.g(), h94.h(), h94, r84.h(r84.p(context), str, str3, str2), str3, str2, e94.determineFrom(e2).getId()), r94, new sc4(r94), new nc4(context), new ed4(str4, String.format(Locale.US, "https://firebase-settings.crashlytics.com/spi/v2/platforms/android/gmp/%s/settings", str), kb4), c94);
    }

    @DexIgnore
    @Override // com.fossil.rc4
    public nt3<wc4> a() {
        return this.i.get().a();
    }

    @DexIgnore
    @Override // com.fossil.rc4
    public zc4 b() {
        return this.h.get();
    }

    @DexIgnore
    public boolean k() {
        return !n().equals(this.b.f);
    }

    @DexIgnore
    public final ad4 m(pc4 pc4) {
        Exception e2;
        ad4 ad4;
        try {
            if (pc4.SKIP_CACHE_LOOKUP.equals(pc4)) {
                return null;
            }
            JSONObject b2 = this.e.b();
            if (b2 != null) {
                ad4 = this.c.b(b2);
                if (ad4 != null) {
                    q(b2, "Loaded cached settings: ");
                    long a2 = this.d.a();
                    if (pc4.IGNORE_CACHE_EXPIRATION.equals(pc4) || !ad4.e(a2)) {
                        try {
                            x74.f().b("Returning cached settings.");
                            return ad4;
                        } catch (Exception e3) {
                            e2 = e3;
                            x74.f().e("Failed to get cached settings", e2);
                            return ad4;
                        }
                    } else {
                        x74.f().b("Cached settings have expired.");
                        return null;
                    }
                } else {
                    x74.f().e("Failed to parse cached settings data.", null);
                    return null;
                }
            } else {
                x74.f().b("No cached settings data found.");
                return null;
            }
        } catch (Exception e4) {
            e2 = e4;
            ad4 = null;
            x74.f().e("Failed to get cached settings", e2);
            return ad4;
        }
    }

    @DexIgnore
    public final String n() {
        return r84.t(this.f2959a).getString("existing_instance_identifier", "");
    }

    @DexIgnore
    public nt3<Void> o(pc4 pc4, Executor executor) {
        ad4 m;
        if (k() || (m = m(pc4)) == null) {
            ad4 m2 = m(pc4.IGNORE_CACHE_EXPIRATION);
            if (m2 != null) {
                this.h.set(m2);
                this.i.get().e(m2.c());
            }
            return this.g.d().s(executor, new a());
        }
        this.h.set(m);
        this.i.get().e(m.c());
        return qt3.f(null);
    }

    @DexIgnore
    public nt3<Void> p(Executor executor) {
        return o(pc4.USE_CACHE, executor);
    }

    @DexIgnore
    public final void q(JSONObject jSONObject, String str) throws JSONException {
        x74 f2 = x74.f();
        f2.b(str + jSONObject.toString());
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public final boolean r(String str) {
        SharedPreferences.Editor edit = r84.t(this.f2959a).edit();
        edit.putString("existing_instance_identifier", str);
        edit.apply();
        return true;
    }
}
