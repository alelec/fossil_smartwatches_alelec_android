package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f22 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f1039a;
    @DexIgnore
    public /* final */ k22 b;
    @DexIgnore
    public /* final */ h22 c;
    @DexIgnore
    public /* final */ s32 d;

    @DexIgnore
    public f22(Executor executor, k22 k22, h22 h22, s32 s32) {
        this.f1039a = executor;
        this.b = k22;
        this.c = h22;
        this.d = s32;
    }

    @DexIgnore
    public static /* synthetic */ Object b(f22 f22) {
        for (h02 h02 : f22.b.w()) {
            f22.c.a(h02, 1);
        }
        return null;
    }

    @DexIgnore
    public void a() {
        this.f1039a.execute(d22.a(this));
    }
}
