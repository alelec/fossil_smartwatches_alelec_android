package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jr4 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public oi5 f1795a;
    @DexIgnore
    public ArrayList<oi5> b;
    @DexIgnore
    public a c;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(oi5 oi5);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public CustomizeWidget f1796a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public oi5 d;
        @DexIgnore
        public /* final */ /* synthetic */ jr4 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a j;
                if (this.b.e.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && (j = this.b.e.j()) != null) {
                    Object obj = this.b.e.b.get(this.b.getAdapterPosition());
                    pq7.b(obj, "mData[adapterPosition]");
                    j.a((oi5) obj);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(jr4 jr4, View view) {
            super(view);
            pq7.c(view, "view");
            this.e = jr4;
            View findViewById = view.findViewById(2131363552);
            pq7.b(findViewById, "view.findViewById(R.id.wc_workout_type)");
            this.f1796a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363429);
            pq7.b(findViewById2, "view.findViewById(R.id.tv_workout_type_name)");
            this.b = (TextView) findViewById2;
            this.c = view.findViewById(2131362728);
            this.f1796a.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void a(oi5 oi5) {
            pq7.c(oi5, "workoutWrapperType");
            this.d = oi5;
            if (oi5 != null) {
                cl7<Integer, Integer> a2 = oi5.Companion.a(oi5);
                this.f1796a.c0(a2.getFirst().intValue());
                this.b.setText(um5.c(PortfolioApp.h0.c(), a2.getSecond().intValue()));
            }
            this.f1796a.setSelectedWc(oi5.ordinal() == this.e.f1795a.ordinal());
            if (oi5.ordinal() == this.e.f1795a.ordinal()) {
                View view = this.c;
                pq7.b(view, "ivIndicator");
                view.setBackground(gl0.f(PortfolioApp.h0.c(), 2131230956));
                return;
            }
            View view2 = this.c;
            pq7.b(view2, "ivIndicator");
            view2.setBackground(gl0.f(PortfolioApp.h0.c(), 2131230957));
        }
    }

    @DexIgnore
    public jr4(ArrayList<oi5> arrayList, a aVar) {
        pq7.c(arrayList, "mData");
        this.b = arrayList;
        this.c = aVar;
        this.f1795a = oi5.WORKOUT;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ jr4(ArrayList arrayList, a aVar, int i, kq7 kq7) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : aVar);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public final int i(oi5 oi5) {
        T t;
        boolean z;
        pq7.c(oi5, "workoutWrapperType");
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (next == oi5) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return this.b.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    public final a j() {
        return this.c;
    }

    @DexIgnore
    /* renamed from: k */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        if (getItemCount() > i && i != -1) {
            oi5 oi5 = this.b.get(i);
            pq7.b(oi5, "mData[position]");
            bVar.a(oi5);
        }
    }

    @DexIgnore
    /* renamed from: l */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558730, viewGroup, false);
        pq7.b(inflate, "LayoutInflater.from(pare\u2026kout_type, parent, false)");
        return new b(this, inflate);
    }

    @DexIgnore
    public final void m(List<? extends oi5> list) {
        pq7.c(list, "data");
        this.b.clear();
        this.b.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void n(a aVar) {
        pq7.c(aVar, "listener");
        this.c = aVar;
    }

    @DexIgnore
    public final void o(oi5 oi5) {
        pq7.c(oi5, "workoutWrapperType");
        this.f1795a = oi5;
        notifyDataSetChanged();
    }
}
