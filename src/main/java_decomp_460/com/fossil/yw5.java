package com.fossil;

import com.fossil.zu0;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yw5 extends zu0.d<GoalTrackingSummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        pq7.c(goalTrackingSummary, "oldItem");
        pq7.c(goalTrackingSummary2, "newItem");
        return pq7.a(goalTrackingSummary, goalTrackingSummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        pq7.c(goalTrackingSummary, "oldItem");
        pq7.c(goalTrackingSummary2, "newItem");
        return lk5.m0(goalTrackingSummary.getDate(), goalTrackingSummary2.getDate());
    }
}
