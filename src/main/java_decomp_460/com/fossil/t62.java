package com.fossil;

import com.fossil.z62;
import com.google.android.gms.common.api.Status;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class t62<R extends z62> {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Status status);
    }

    @DexIgnore
    public abstract void b(a aVar);

    @DexIgnore
    public abstract R c(long j, TimeUnit timeUnit);

    @DexIgnore
    public abstract void d(a72<? super R> a72);
}
