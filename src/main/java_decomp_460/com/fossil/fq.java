package com.fossil;

import com.fossil.crypto.EllipticCurveKeyPair;
import com.fossil.ix1;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fq extends lp {
    @DexIgnore
    public /* final */ ArrayList<ow> C; // = by1.a(this.i, hm7.c(ow.AUTHENTICATION));
    @DexIgnore
    public byte[] D;
    @DexIgnore
    public /* final */ EllipticCurveKeyPair E;
    @DexIgnore
    public /* final */ byte[] F;

    @DexIgnore
    public fq(k5 k5Var, i60 i60, byte[] bArr) {
        super(k5Var, i60, yp.R, null, false, 24);
        this.F = bArr;
        EllipticCurveKeyPair create = EllipticCurveKeyPair.create();
        pq7.b(create, "EllipticCurveKeyPair.create()");
        this.E = create;
    }

    @DexIgnore
    public static final /* synthetic */ void G(fq fqVar) {
        k5 k5Var = fqVar.w;
        byte[] publicKey = fqVar.E.publicKey();
        pq7.b(publicKey, "keyPair.publicKey()");
        lp.i(fqVar, new dt(k5Var, publicKey), new go(fqVar), new so(fqVar), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        byte[] bArr = this.F;
        if (bArr.length != 16) {
            k(zq.INVALID_PARAMETER);
        } else {
            lp.i(this, new et(this.w, rt.PRE_SHARED_KEY, bArr), new fp(this), new sp(this), null, null, null, 56, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(super.C(), jd0.q2, dy1.e(this.F, null, 1, null));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        JSONObject E2 = super.E();
        byte[] bArr = this.D;
        if (bArr != null) {
            g80.k(E2, jd0.t2, Long.valueOf(ix1.f1688a.b(bArr, ix1.a.CRC32)));
        }
        return E2;
    }

    @DexIgnore
    public final void H() {
        byte[] bArr = this.D;
        if (bArr != null) {
            lp.h(this, new ji(this.w, this.x, hd0.y.b(), dm7.k(bArr, 0, 16), this.z), in.b, un.b, null, new wm(this), null, 40, null);
        } else {
            l(nr.a(this.v, null, zq.FLOW_BROKEN, null, null, 13));
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        byte[] bArr = this.D;
        return bArr != null ? bArr : new byte[0];
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.C;
    }
}
