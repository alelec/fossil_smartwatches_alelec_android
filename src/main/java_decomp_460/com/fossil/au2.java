package com.fossil;

import android.app.Activity;
import android.os.RemoteException;
import com.fossil.zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class au2 extends zs2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Activity f;
    @DexIgnore
    public /* final */ /* synthetic */ zs2.b g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public au2(zs2.b bVar, Activity activity) {
        super(zs2.this);
        this.g = bVar;
        this.f = activity;
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void a() throws RemoteException {
        zs2.this.h.onActivityPaused(tg2.n(this.f), this.c);
    }
}
