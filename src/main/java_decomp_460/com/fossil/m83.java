package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m83 implements xw2<l83> {
    @DexIgnore
    public static m83 c; // = new m83();
    @DexIgnore
    public /* final */ xw2<l83> b;

    @DexIgnore
    public m83() {
        this(ww2.b(new o83()));
    }

    @DexIgnore
    public m83(xw2<l83> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((l83) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ l83 zza() {
        return this.b.zza();
    }
}
