package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.fossil.m62;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c92 implements s92, va2 {
    @DexIgnore
    public /* final */ Lock b;
    @DexIgnore
    public /* final */ Condition c;
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ d62 e;
    @DexIgnore
    public /* final */ e92 f;
    @DexIgnore
    public /* final */ Map<m62.c<?>, m62.f> g;
    @DexIgnore
    public /* final */ Map<m62.c<?>, z52> h; // = new HashMap();
    @DexIgnore
    public /* final */ ac2 i;
    @DexIgnore
    public /* final */ Map<m62<?>, Boolean> j;
    @DexIgnore
    public /* final */ m62.a<? extends ys3, gs3> k;
    @DexIgnore
    public volatile d92 l;
    @DexIgnore
    public z52 m; // = null;
    @DexIgnore
    public int s;
    @DexIgnore
    public /* final */ t82 t;
    @DexIgnore
    public /* final */ r92 u;

    @DexIgnore
    public c92(Context context, t82 t82, Lock lock, Looper looper, d62 d62, Map<m62.c<?>, m62.f> map, ac2 ac2, Map<m62<?>, Boolean> map2, m62.a<? extends ys3, gs3> aVar, ArrayList<wa2> arrayList, r92 r92) {
        this.d = context;
        this.b = lock;
        this.e = d62;
        this.g = map;
        this.i = ac2;
        this.j = map2;
        this.k = aVar;
        this.t = t82;
        this.u = r92;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            wa2 wa2 = arrayList.get(i2);
            i2++;
            wa2.a(this);
        }
        this.f = new e92(this, looper);
        this.c = lock.newCondition();
        this.l = new u82(this);
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final void a() {
        if (this.l.a()) {
            this.h.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final void b() {
        this.l.b();
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final boolean c() {
        return this.l instanceof g82;
    }

    @DexIgnore
    @Override // com.fossil.k72
    public final void d(int i2) {
        this.b.lock();
        try {
            this.l.d(i2);
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.k72
    public final void e(Bundle bundle) {
        this.b.lock();
        try {
            this.l.e(bundle);
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final void f(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String concat = String.valueOf(str).concat("  ");
        printWriter.append((CharSequence) str).append("mState=").println(this.l);
        for (m62<?> m62 : this.j.keySet()) {
            printWriter.append((CharSequence) str).append((CharSequence) m62.b()).println(":");
            this.g.get(m62.a()).f(concat, fileDescriptor, printWriter, strArr);
        }
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final boolean g(t72 t72) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final void h() {
    }

    @DexIgnore
    @Override // com.fossil.va2
    public final void i(z52 z52, m62<?> m62, boolean z) {
        this.b.lock();
        try {
            this.l.i(z52, m62, z);
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final <A extends m62.b, T extends i72<? extends z62, A>> T j(T t2) {
        t2.t();
        return (T) this.l.j(t2);
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final <A extends m62.b, R extends z62, T extends i72<R, A>> T k(T t2) {
        t2.t();
        return (T) this.l.k(t2);
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final void l() {
        if (c()) {
            ((g82) this.l).g();
        }
    }

    @DexIgnore
    @Override // com.fossil.s92
    public final z52 m() {
        b();
        while (n()) {
            try {
                this.c.await();
            } catch (InterruptedException e2) {
                Thread.currentThread().interrupt();
                return new z52(15, null);
            }
        }
        if (c()) {
            return z52.f;
        }
        z52 z52 = this.m;
        return z52 == null ? new z52(13, null) : z52;
    }

    @DexIgnore
    public final boolean n() {
        return this.l instanceof h82;
    }

    @DexIgnore
    public final void p(f92 f92) {
        this.f.sendMessage(this.f.obtainMessage(1, f92));
    }

    @DexIgnore
    public final void q(RuntimeException runtimeException) {
        this.f.sendMessage(this.f.obtainMessage(2, runtimeException));
    }

    @DexIgnore
    public final void r() {
        this.b.lock();
        try {
            this.l = new h82(this, this.i, this.j, this.e, this.k, this.b, this.d);
            this.l.f();
            this.c.signalAll();
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void s() {
        this.b.lock();
        try {
            this.t.D();
            this.l = new g82(this);
            this.l.f();
            this.c.signalAll();
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void u(z52 z52) {
        this.b.lock();
        try {
            this.m = z52;
            this.l = new u82(this);
            this.l.f();
            this.c.signalAll();
        } finally {
            this.b.unlock();
        }
    }
}
