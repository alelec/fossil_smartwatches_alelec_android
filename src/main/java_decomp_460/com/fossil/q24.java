package com.fossil;

import java.util.Collection;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class q24<E> extends l24<E> implements Set<E> {
    @DexIgnore
    @Override // com.fossil.l24, com.fossil.l24, com.fossil.o24
    public abstract /* bridge */ /* synthetic */ Object delegate();

    @DexIgnore
    @Override // com.fossil.l24, com.fossil.l24, com.fossil.o24
    public abstract /* bridge */ /* synthetic */ Collection delegate();

    @DexIgnore
    @Override // com.fossil.l24, com.fossil.l24, com.fossil.o24
    public abstract Set<E> delegate();

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || delegate().equals(obj);
    }

    @DexIgnore
    public int hashCode() {
        return delegate().hashCode();
    }

    @DexIgnore
    public boolean standardEquals(Object obj) {
        return x44.a(this, obj);
    }

    @DexIgnore
    public int standardHashCode() {
        return x44.b(this);
    }

    @DexIgnore
    @Override // com.fossil.l24
    public boolean standardRemoveAll(Collection<?> collection) {
        i14.l(collection);
        return x44.f(this, collection);
    }
}
