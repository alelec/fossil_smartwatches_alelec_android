package com.fossil;

import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.LruCache;
import com.misfit.frameworks.buttonservice.log.FileLogWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tm5 {
    @DexIgnore
    public static /* final */ tm5 c; // = new tm5();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public LruCache<String, String> f3442a; // = new LruCache<>(FileLogWriter.FILE_LOG_SIZE_THRESHOLD);
    @DexIgnore
    public LruCache<String, BitmapDrawable> b; // = new a(this, FileLogWriter.FILE_LOG_SIZE_THRESHOLD);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends LruCache<String, BitmapDrawable> {
        @DexIgnore
        public a(tm5 tm5, int i) {
            super(i);
        }

        @DexIgnore
        /* renamed from: a */
        public int sizeOf(String str, BitmapDrawable bitmapDrawable) {
            return bitmapDrawable.getBitmap().getByteCount() / 1024;
        }
    }

    @DexIgnore
    public static tm5 d() {
        return c;
    }

    @DexIgnore
    public boolean a(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return !TextUtils.isEmpty(this.f3442a.get(str));
    }

    @DexIgnore
    public void b() {
        this.f3442a.evictAll();
        this.b.evictAll();
    }

    @DexIgnore
    public BitmapDrawable c(String str) {
        return this.b.get(str);
    }

    @DexIgnore
    public String e(String str) {
        return this.f3442a.remove(str);
    }

    @DexIgnore
    public String f(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        String str2 = this.f3442a.get(str);
        if (TextUtils.isEmpty(str2)) {
            try {
                str2 = ym5.f(str);
                if (str2 != null) {
                    this.f3442a.put(str, str2);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (TextUtils.isEmpty(str2)) {
            return str2;
        }
        if (str2.contains("\\n")) {
            str2 = str2.replace("\\n", "\n");
        }
        return str2.replaceAll("\\\\", "");
    }

    @DexIgnore
    public void g(String str, String str2) {
        this.f3442a.put(str, str2);
    }

    @DexIgnore
    public void h(String str, BitmapDrawable bitmapDrawable) {
        this.b.put(str, bitmapDrawable);
    }
}
