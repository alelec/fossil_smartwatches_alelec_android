package com.fossil;

import android.content.Context;
import com.fossil.n02;
import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yz1 extends n02 {
    @DexIgnore
    public Provider<Executor> b;
    @DexIgnore
    public Provider<Context> c;
    @DexIgnore
    public Provider d;
    @DexIgnore
    public Provider e;
    @DexIgnore
    public Provider f;
    @DexIgnore
    public Provider<j32> g;
    @DexIgnore
    public Provider<v12> h;
    @DexIgnore
    public Provider<h22> i;
    @DexIgnore
    public Provider<i12> j;
    @DexIgnore
    public Provider<b22> k;
    @DexIgnore
    public Provider<f22> l;
    @DexIgnore
    public Provider<m02> m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements n02.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Context f4396a;

        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.n02.a
        public /* bridge */ /* synthetic */ n02.a a(Context context) {
            b(context);
            return this;
        }

        @DexIgnore
        public b b(Context context) {
            lk7.b(context);
            this.f4396a = context;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.n02.a
        public n02 build() {
            lk7.a(this.f4396a, Context.class);
            return new yz1(this.f4396a);
        }
    }

    @DexIgnore
    public yz1(Context context) {
        f(context);
    }

    @DexIgnore
    public static n02.a c() {
        return new b();
    }

    @DexIgnore
    @Override // com.fossil.n02
    public k22 a() {
        return this.g.get();
    }

    @DexIgnore
    @Override // com.fossil.n02
    public m02 b() {
        return this.m.get();
    }

    @DexIgnore
    public final void f(Context context) {
        this.b = ik7.a(e02.a());
        Factory a2 = jk7.a(context);
        this.c = a2;
        y02 a3 = y02.a(a2, v32.a(), w32.a());
        this.d = a3;
        this.e = ik7.a(a12.a(this.c, a3));
        this.f = q32.a(this.c, n22.a(), o22.a());
        this.g = ik7.a(k32.a(v32.a(), w32.a(), p22.a(), this.f));
        m12 b2 = m12.b(v32.a());
        this.h = b2;
        o12 a4 = o12.a(this.c, this.g, b2, w32.a());
        this.i = a4;
        Provider<Executor> provider = this.b;
        Provider provider2 = this.e;
        Provider<j32> provider3 = this.g;
        this.j = j12.a(provider, provider2, a4, provider3, provider3);
        Provider<Context> provider4 = this.c;
        Provider provider5 = this.e;
        Provider<j32> provider6 = this.g;
        this.k = c22.a(provider4, provider5, provider6, this.i, this.b, provider6, v32.a());
        Provider<Executor> provider7 = this.b;
        Provider<j32> provider8 = this.g;
        this.l = g22.a(provider7, provider8, this.i, provider8);
        this.m = ik7.a(o02.a(v32.a(), w32.a(), this.j, this.k, this.l));
    }
}
