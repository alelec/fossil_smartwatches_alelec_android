package com.fossil;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.util.StateSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tz3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ boolean f3498a; // = (Build.VERSION.SDK_INT >= 21);
    @DexIgnore
    public static /* final */ int[] b; // = {16842919};
    @DexIgnore
    public static /* final */ int[] c; // = {16843623, 16842908};
    @DexIgnore
    public static /* final */ int[] d; // = {16842908};
    @DexIgnore
    public static /* final */ int[] e; // = {16843623};
    @DexIgnore
    public static /* final */ int[] f; // = {16842913, 16842919};
    @DexIgnore
    public static /* final */ int[] g; // = {16842913, 16843623, 16842908};
    @DexIgnore
    public static /* final */ int[] h; // = {16842913, 16842908};
    @DexIgnore
    public static /* final */ int[] i; // = {16842913, 16843623};
    @DexIgnore
    public static /* final */ int[] j; // = {16842913};
    @DexIgnore
    public static /* final */ int[] k; // = {16842910, 16842919};
    @DexIgnore
    public static /* final */ String l; // = tz3.class.getSimpleName();

    @DexIgnore
    public static ColorStateList a(ColorStateList colorStateList) {
        if (f3498a) {
            int[] iArr = j;
            int c2 = c(colorStateList, f);
            int[] iArr2 = StateSet.NOTHING;
            return new ColorStateList(new int[][]{iArr, iArr2}, new int[]{c2, c(colorStateList, b)});
        }
        int[] iArr3 = f;
        int c3 = c(colorStateList, iArr3);
        int[] iArr4 = g;
        int c4 = c(colorStateList, iArr4);
        int[] iArr5 = h;
        int c5 = c(colorStateList, iArr5);
        int[] iArr6 = i;
        int c6 = c(colorStateList, iArr6);
        int[] iArr7 = j;
        int[] iArr8 = b;
        int c7 = c(colorStateList, iArr8);
        int[] iArr9 = c;
        int c8 = c(colorStateList, iArr9);
        int[] iArr10 = d;
        int c9 = c(colorStateList, iArr10);
        int[] iArr11 = e;
        int c10 = c(colorStateList, iArr11);
        return new ColorStateList(new int[][]{iArr3, iArr4, iArr5, iArr6, iArr7, iArr8, iArr9, iArr10, iArr11, StateSet.NOTHING}, new int[]{c3, c4, c5, c6, 0, c7, c8, c9, c10, 0});
    }

    @DexIgnore
    @TargetApi(21)
    public static int b(int i2) {
        return pl0.h(i2, Math.min(Color.alpha(i2) * 2, 255));
    }

    @DexIgnore
    public static int c(ColorStateList colorStateList, int[] iArr) {
        int colorForState = colorStateList != null ? colorStateList.getColorForState(iArr, colorStateList.getDefaultColor()) : 0;
        return f3498a ? b(colorForState) : colorForState;
    }

    @DexIgnore
    public static ColorStateList d(ColorStateList colorStateList) {
        if (colorStateList == null) {
            return ColorStateList.valueOf(0);
        }
        int i2 = Build.VERSION.SDK_INT;
        if (i2 < 22 || i2 > 27 || Color.alpha(colorStateList.getDefaultColor()) != 0 || Color.alpha(colorStateList.getColorForState(k, 0)) == 0) {
            return colorStateList;
        }
        Log.w(l, "Use a non-transparent color for the default color as it will be used to finish ripple animations.");
        return colorStateList;
    }

    @DexIgnore
    public static boolean e(int[] iArr) {
        boolean z = false;
        boolean z2 = false;
        for (int i2 : iArr) {
            if (i2 == 16842910) {
                z2 = true;
            } else if (i2 == 16842908 || i2 == 16842919 || i2 == 16843623) {
                z = true;
            }
        }
        return z2 && z;
    }
}
