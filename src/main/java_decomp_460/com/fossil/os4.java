package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class os4 {
    @rj4("profileID")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2717a;
    @DexIgnore
    @rj4("status")
    public String b;

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof os4) {
                os4 os4 = (os4) obj;
                if (!pq7.a(this.f2717a, os4.f2717a) || !pq7.a(this.b, os4.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f2717a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "BlockResult(id=" + this.f2717a + ", status=" + this.b + ")";
    }
}
