package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zd5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ConstraintLayout f4452a;
    @DexIgnore
    public /* final */ ConstraintLayout b;
    @DexIgnore
    public /* final */ View c;
    @DexIgnore
    public /* final */ RTLImageView d;
    @DexIgnore
    public /* final */ FlexibleTextView e;
    @DexIgnore
    public /* final */ CustomizeWidget f;

    @DexIgnore
    public zd5(ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, View view, RTLImageView rTLImageView, FlexibleTextView flexibleTextView, CustomizeWidget customizeWidget) {
        this.f4452a = constraintLayout;
        this.b = constraintLayout2;
        this.c = view;
        this.d = rTLImageView;
        this.e = flexibleTextView;
        this.f = customizeWidget;
    }

    @DexIgnore
    public static zd5 a(View view) {
        int i = 2131363544;
        ConstraintLayout constraintLayout = (ConstraintLayout) view;
        View findViewById = view.findViewById(2131362728);
        if (findViewById != null) {
            RTLImageView rTLImageView = (RTLImageView) view.findViewById(2131362767);
            if (rTLImageView != null) {
                FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(2131363294);
                if (flexibleTextView != null) {
                    CustomizeWidget customizeWidget = (CustomizeWidget) view.findViewById(2131363544);
                    if (customizeWidget != null) {
                        return new zd5(constraintLayout, constraintLayout, findViewById, rTLImageView, flexibleTextView, customizeWidget);
                    }
                } else {
                    i = 2131363294;
                }
            } else {
                i = 2131362767;
            }
        } else {
            i = 2131362728;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @DexIgnore
    public static zd5 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(2131558669, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    @DexIgnore
    public ConstraintLayout b() {
        return this.f4452a;
    }
}
