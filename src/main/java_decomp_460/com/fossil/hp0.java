package com.fossil;

import android.widget.ListView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hp0 extends cp0 {
    @DexIgnore
    public /* final */ ListView y;

    @DexIgnore
    public hp0(ListView listView) {
        super(listView);
        this.y = listView;
    }

    @DexIgnore
    @Override // com.fossil.cp0
    public boolean a(int i) {
        return false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027 A[RETURN, SYNTHETIC] */
    @Override // com.fossil.cp0
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b(int r6) {
        /*
            r5 = this;
            r0 = 0
            android.widget.ListView r1 = r5.y
            int r2 = r1.getCount()
            if (r2 != 0) goto L_0x000a
        L_0x0009:
            return r0
        L_0x000a:
            int r3 = r1.getChildCount()
            int r4 = r1.getFirstVisiblePosition()
            if (r6 <= 0) goto L_0x0029
            int r4 = r4 + r3
            if (r4 < r2) goto L_0x0027
            int r2 = r3 + -1
            android.view.View r2 = r1.getChildAt(r2)
            int r2 = r2.getBottom()
            int r1 = r1.getHeight()
            if (r2 <= r1) goto L_0x0009
        L_0x0027:
            r0 = 1
            goto L_0x0009
        L_0x0029:
            if (r6 >= 0) goto L_0x0009
            if (r4 > 0) goto L_0x0027
            android.view.View r1 = r1.getChildAt(r0)
            int r1 = r1.getTop()
            if (r1 < 0) goto L_0x0027
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.hp0.b(int):boolean");
    }

    @DexIgnore
    @Override // com.fossil.cp0
    public void j(int i, int i2) {
        ip0.b(this.y, i2);
    }
}
