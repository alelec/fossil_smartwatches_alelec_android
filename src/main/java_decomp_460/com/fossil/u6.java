package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum u6 {
    CLIENT_CHARACTERISTIC_CONFIGURATION,
    UNKNOWN;

    @DexIgnore
    public final UUID a() {
        if (t6.f3369a[ordinal()] != 1) {
            return null;
        }
        return hd0.y.a();
    }
}
