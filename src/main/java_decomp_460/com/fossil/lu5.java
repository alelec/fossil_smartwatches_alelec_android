package com.fossil;

import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lu5 {
    @DexIgnore
    public static /* final */ String c; // = "lu5";

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ NotificationsRepository f2252a;
    @DexIgnore
    public a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore
    public lu5(NotificationsRepository notificationsRepository) {
        i14.l(notificationsRepository);
        this.f2252a = notificationsRepository;
    }

    @DexIgnore
    public final List<Contact> a() {
        List<ContactGroup> allContactGroups = this.f2252a.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        List<ContactGroup> allContactGroups2 = this.f2252a.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        ArrayList arrayList = new ArrayList();
        if (allContactGroups != null) {
            for (ContactGroup contactGroup : allContactGroups) {
                arrayList.addAll(contactGroup.getContacts());
            }
        }
        if (allContactGroups2 != null) {
            for (ContactGroup contactGroup2 : allContactGroups2) {
                arrayList.addAll(contactGroup2.getContacts());
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final void b(Contact contact, NotificationsRepository notificationsRepository, List<String> list) {
        notificationsRepository.removePhoneNumberByContactGroupId(contact.getContactGroup().getDbRowId());
        if (list != null) {
            for (String str : list) {
                PhoneNumber phoneNumber = new PhoneNumber();
                phoneNumber.setNumber(str);
                phoneNumber.setContact(contact);
                notificationsRepository.savePhoneNumber(phoneNumber);
            }
        }
    }

    @DexIgnore
    public void c(a aVar) {
        this.b = aVar;
    }

    @DexIgnore
    public void d(String str, String str2, List<String> list, String str3) {
        a aVar;
        FLogger.INSTANCE.getLocal().d(c, "start update contact");
        boolean z = false;
        for (Contact contact : a()) {
            if (contact.getContactId() == Integer.parseInt(str)) {
                contact.setFirstName(str2);
                contact.setPhotoThumbUri(str3);
                e(this.f2252a, contact, list);
                z = true;
            }
        }
        if (z && (aVar = this.b) != null) {
            aVar.a();
        }
    }

    @DexIgnore
    public final void e(NotificationsRepository notificationsRepository, Contact contact, List<String> list) {
        notificationsRepository.saveContact(contact);
        b(contact, notificationsRepository, list);
    }
}
