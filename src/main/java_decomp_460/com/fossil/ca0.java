package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class ca0 extends Enum<ca0> {
    @DexIgnore
    public static /* final */ ca0 c;
    @DexIgnore
    public static /* final */ ca0 d;
    @DexIgnore
    public static /* final */ /* synthetic */ ca0[] e;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        ca0 ca0 = new ca0("HOUR", 0, (byte) 1);
        c = ca0;
        ca0 ca02 = new ca0("MINUTE", 1, (byte) 2);
        d = ca02;
        e = new ca0[]{ca0, ca02, new ca0("SUB_EYE", 2, (byte) 4)};
    }
    */

    @DexIgnore
    public ca0(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static ca0 valueOf(String str) {
        return (ca0) Enum.valueOf(ca0.class, str);
    }

    @DexIgnore
    public static ca0[] values() {
        return (ca0[]) e.clone();
    }
}
