package com.fossil;

import android.app.Activity;
import android.os.RemoteException;
import com.fossil.zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cu2 extends zs2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Activity f;
    @DexIgnore
    public /* final */ /* synthetic */ r93 g;
    @DexIgnore
    public /* final */ /* synthetic */ zs2.b h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cu2(zs2.b bVar, Activity activity, r93 r93) {
        super(zs2.this);
        this.h = bVar;
        this.f = activity;
        this.g = r93;
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void a() throws RemoteException {
        zs2.this.h.onActivitySaveInstanceState(tg2.n(this.f), this.g, this.c);
    }
}
