package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.appcompat.widget.Toolbar;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.cg0;
import com.fossil.ig0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uh0 implements ch0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Toolbar f3585a;
    @DexIgnore
    public int b;
    @DexIgnore
    public View c;
    @DexIgnore
    public View d;
    @DexIgnore
    public Drawable e;
    @DexIgnore
    public Drawable f;
    @DexIgnore
    public Drawable g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public CharSequence i;
    @DexIgnore
    public CharSequence j;
    @DexIgnore
    public CharSequence k;
    @DexIgnore
    public Window.Callback l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public rg0 n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public Drawable q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ wf0 b; // = new wf0(uh0.this.f3585a.getContext(), 0, 16908332, 0, 0, uh0.this.i);

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            uh0 uh0 = uh0.this;
            Window.Callback callback = uh0.l;
            if (callback != null && uh0.m) {
                callback.onMenuItemSelected(0, this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends to0 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f3586a; // = false;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public b(int i) {
            this.b = i;
        }

        @DexIgnore
        @Override // com.fossil.so0, com.fossil.to0
        public void a(View view) {
            this.f3586a = true;
        }

        @DexIgnore
        @Override // com.fossil.so0
        public void b(View view) {
            if (!this.f3586a) {
                uh0.this.f3585a.setVisibility(this.b);
            }
        }

        @DexIgnore
        @Override // com.fossil.so0, com.fossil.to0
        public void c(View view) {
            uh0.this.f3585a.setVisibility(0);
        }
    }

    @DexIgnore
    public uh0(Toolbar toolbar, boolean z) {
        this(toolbar, z, se0.abc_action_bar_up_description, pe0.abc_ic_ab_back_material);
    }

    @DexIgnore
    public uh0(Toolbar toolbar, boolean z, int i2, int i3) {
        Drawable drawable;
        this.o = 0;
        this.p = 0;
        this.f3585a = toolbar;
        this.i = toolbar.getTitle();
        this.j = toolbar.getSubtitle();
        this.h = this.i != null;
        this.g = toolbar.getNavigationIcon();
        th0 v = th0.v(toolbar.getContext(), null, ue0.ActionBar, le0.actionBarStyle, 0);
        this.q = v.g(ue0.ActionBar_homeAsUpIndicator);
        if (z) {
            CharSequence p2 = v.p(ue0.ActionBar_title);
            if (!TextUtils.isEmpty(p2)) {
                setTitle(p2);
            }
            CharSequence p3 = v.p(ue0.ActionBar_subtitle);
            if (!TextUtils.isEmpty(p3)) {
                D(p3);
            }
            Drawable g2 = v.g(ue0.ActionBar_logo);
            if (g2 != null) {
                z(g2);
            }
            Drawable g3 = v.g(ue0.ActionBar_icon);
            if (g3 != null) {
                setIcon(g3);
            }
            if (this.g == null && (drawable = this.q) != null) {
                C(drawable);
            }
            k(v.k(ue0.ActionBar_displayOptions, 0));
            int n2 = v.n(ue0.ActionBar_customNavigationLayout, 0);
            if (n2 != 0) {
                x(LayoutInflater.from(this.f3585a.getContext()).inflate(n2, (ViewGroup) this.f3585a, false));
                k(this.b | 16);
            }
            int m2 = v.m(ue0.ActionBar_height, 0);
            if (m2 > 0) {
                ViewGroup.LayoutParams layoutParams = this.f3585a.getLayoutParams();
                layoutParams.height = m2;
                this.f3585a.setLayoutParams(layoutParams);
            }
            int e2 = v.e(ue0.ActionBar_contentInsetStart, -1);
            int e3 = v.e(ue0.ActionBar_contentInsetEnd, -1);
            if (e2 >= 0 || e3 >= 0) {
                this.f3585a.I(Math.max(e2, 0), Math.max(e3, 0));
            }
            int n3 = v.n(ue0.ActionBar_titleTextStyle, 0);
            if (n3 != 0) {
                Toolbar toolbar2 = this.f3585a;
                toolbar2.N(toolbar2.getContext(), n3);
            }
            int n4 = v.n(ue0.ActionBar_subtitleTextStyle, 0);
            if (n4 != 0) {
                Toolbar toolbar3 = this.f3585a;
                toolbar3.L(toolbar3.getContext(), n4);
            }
            int n5 = v.n(ue0.ActionBar_popupTheme, 0);
            if (n5 != 0) {
                this.f3585a.setPopupTheme(n5);
            }
        } else {
            this.b = w();
        }
        v.w();
        y(i2);
        this.k = this.f3585a.getNavigationContentDescription();
        this.f3585a.setNavigationOnClickListener(new a());
    }

    @DexIgnore
    public void A(int i2) {
        B(i2 == 0 ? null : getContext().getString(i2));
    }

    @DexIgnore
    public void B(CharSequence charSequence) {
        this.k = charSequence;
        F();
    }

    @DexIgnore
    public void C(Drawable drawable) {
        this.g = drawable;
        G();
    }

    @DexIgnore
    public void D(CharSequence charSequence) {
        this.j = charSequence;
        if ((this.b & 8) != 0) {
            this.f3585a.setSubtitle(charSequence);
        }
    }

    @DexIgnore
    public final void E(CharSequence charSequence) {
        this.i = charSequence;
        if ((this.b & 8) != 0) {
            this.f3585a.setTitle(charSequence);
        }
    }

    @DexIgnore
    public final void F() {
        if ((this.b & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.k)) {
            this.f3585a.setNavigationContentDescription(this.p);
        } else {
            this.f3585a.setNavigationContentDescription(this.k);
        }
    }

    @DexIgnore
    public final void G() {
        if ((this.b & 4) != 0) {
            Toolbar toolbar = this.f3585a;
            Drawable drawable = this.g;
            if (drawable == null) {
                drawable = this.q;
            }
            toolbar.setNavigationIcon(drawable);
            return;
        }
        this.f3585a.setNavigationIcon((Drawable) null);
    }

    @DexIgnore
    public final void H() {
        Drawable drawable;
        int i2 = this.b;
        if ((i2 & 2) == 0) {
            drawable = null;
        } else if ((i2 & 1) != 0) {
            drawable = this.f;
            if (drawable == null) {
                drawable = this.e;
            }
        } else {
            drawable = this.e;
        }
        this.f3585a.setLogo(drawable);
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void a(Menu menu, ig0.a aVar) {
        if (this.n == null) {
            rg0 rg0 = new rg0(this.f3585a.getContext());
            this.n = rg0;
            rg0.s(qe0.action_menu_presenter);
        }
        this.n.g(aVar);
        this.f3585a.J((cg0) menu, this.n);
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public boolean b() {
        return this.f3585a.A();
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void c() {
        this.m = true;
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void collapseActionView() {
        this.f3585a.e();
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public boolean d() {
        return this.f3585a.d();
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public boolean e() {
        return this.f3585a.z();
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public boolean f() {
        return this.f3585a.w();
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public boolean g() {
        return this.f3585a.Q();
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public Context getContext() {
        return this.f3585a.getContext();
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public CharSequence getTitle() {
        return this.f3585a.getTitle();
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void h() {
        this.f3585a.f();
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void i(mh0 mh0) {
        Toolbar toolbar;
        View view = this.c;
        if (view != null && view.getParent() == (toolbar = this.f3585a)) {
            toolbar.removeView(this.c);
        }
        this.c = mh0;
        if (mh0 != null && this.o == 2) {
            this.f3585a.addView(mh0, 0);
            Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) this.c.getLayoutParams();
            ((ViewGroup.MarginLayoutParams) layoutParams).width = -2;
            ((ViewGroup.MarginLayoutParams) layoutParams).height = -2;
            layoutParams.f21a = 8388691;
            mh0.setAllowCollapse(true);
        }
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public boolean j() {
        return this.f3585a.v();
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void k(int i2) {
        View view;
        int i3 = this.b ^ i2;
        this.b = i2;
        if (i3 != 0) {
            if ((i3 & 4) != 0) {
                if ((i2 & 4) != 0) {
                    F();
                }
                G();
            }
            if ((i3 & 3) != 0) {
                H();
            }
            if ((i3 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    this.f3585a.setTitle(this.i);
                    this.f3585a.setSubtitle(this.j);
                } else {
                    this.f3585a.setTitle((CharSequence) null);
                    this.f3585a.setSubtitle((CharSequence) null);
                }
            }
            if ((i3 & 16) != 0 && (view = this.d) != null) {
                if ((i2 & 16) != 0) {
                    this.f3585a.addView(view);
                } else {
                    this.f3585a.removeView(view);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public Menu l() {
        return this.f3585a.getMenu();
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void m(int i2) {
        z(i2 != 0 ? gf0.d(getContext(), i2) : null);
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public int n() {
        return this.o;
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public ro0 o(int i2, long j2) {
        ro0 c2 = mo0.c(this.f3585a);
        c2.a(i2 == 0 ? 1.0f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        c2.d(j2);
        c2.f(new b(i2));
        return c2;
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void p(ig0.a aVar, cg0.a aVar2) {
        this.f3585a.K(aVar, aVar2);
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public ViewGroup q() {
        return this.f3585a;
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void r(boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public int s() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void setIcon(int i2) {
        setIcon(i2 != 0 ? gf0.d(getContext(), i2) : null);
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void setIcon(Drawable drawable) {
        this.e = drawable;
        H();
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void setTitle(CharSequence charSequence) {
        this.h = true;
        E(charSequence);
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void setVisibility(int i2) {
        this.f3585a.setVisibility(i2);
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void setWindowCallback(Window.Callback callback) {
        this.l = callback;
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void setWindowTitle(CharSequence charSequence) {
        if (!this.h) {
            E(charSequence);
        }
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void t() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void u() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    @DexIgnore
    @Override // com.fossil.ch0
    public void v(boolean z) {
        this.f3585a.setCollapsible(z);
    }

    @DexIgnore
    public final int w() {
        if (this.f3585a.getNavigationIcon() == null) {
            return 11;
        }
        this.q = this.f3585a.getNavigationIcon();
        return 15;
    }

    @DexIgnore
    public void x(View view) {
        View view2 = this.d;
        if (!(view2 == null || (this.b & 16) == 0)) {
            this.f3585a.removeView(view2);
        }
        this.d = view;
        if (view != null && (this.b & 16) != 0) {
            this.f3585a.addView(view);
        }
    }

    @DexIgnore
    public void y(int i2) {
        if (i2 != this.p) {
            this.p = i2;
            if (TextUtils.isEmpty(this.f3585a.getNavigationContentDescription())) {
                A(this.p);
            }
        }
    }

    @DexIgnore
    public void z(Drawable drawable) {
        this.f = drawable;
        H();
    }
}
