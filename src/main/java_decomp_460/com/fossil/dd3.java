package com.fossil;

import android.graphics.Bitmap;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface dd3 extends IInterface {
    @DexIgnore
    void onSnapshotReady(Bitmap bitmap) throws RemoteException;

    @DexIgnore
    void q1(rg2 rg2) throws RemoteException;
}
