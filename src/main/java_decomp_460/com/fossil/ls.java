package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ls extends rs {
    @DexIgnore
    public /* final */ byte[] A;
    @DexIgnore
    public /* final */ c2 B;

    @DexIgnore
    public ls(k5 k5Var, c2 c2Var) {
        super(hs.G, k5Var);
        this.B = c2Var;
        byte[] a2 = c2Var.a();
        ByteBuffer order = ByteBuffer.allocate(a2.length + 3).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order, "ByteBuffer\n             \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(ot.c.b);
        order.put(c2Var.b.b);
        order.put(c2Var.c);
        order.put(a2);
        byte[] array = order.array();
        pq7.b(array, "ackData.array()");
        this.A = array;
    }

    @DexIgnore
    @Override // com.fossil.ns
    public u5 D() {
        return new j6(n6.ASYNC, this.A, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(super.z(), jd0.l1, this.B.toJSONObject());
    }
}
