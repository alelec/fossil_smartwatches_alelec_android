package com.fossil;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class is3 implements Parcelable.Creator<js3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ js3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        Intent intent = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                i2 = ad2.v(parcel, t);
            } else if (l == 2) {
                i = ad2.v(parcel, t);
            } else if (l != 3) {
                ad2.B(parcel, t);
            } else {
                intent = (Intent) ad2.e(parcel, t, Intent.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new js3(i2, i, intent);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ js3[] newArray(int i) {
        return new js3[i];
    }
}
