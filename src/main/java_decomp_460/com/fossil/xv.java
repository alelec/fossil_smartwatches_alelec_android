package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xv extends uv {
    @DexIgnore
    public short Q;
    @DexIgnore
    public long R;
    @DexIgnore
    public /* final */ byte[] S;
    @DexIgnore
    public byte[] T;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ xv(short s, k5 k5Var, int i, int i2) {
        super(sv.LEGACY_LIST_FILE, s, hs.V, k5Var, (i2 & 4) != 0 ? 3 : i);
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(sv.LEGACY_LIST_FILE.b).array();
        pq7.b(array, "ByteBuffer.allocate(1)\n \u2026ode)\n            .array()");
        this.S = array;
        byte[] array2 = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(sv.LEGACY_LIST_FILE.a()).array();
        pq7.b(array2, "ByteBuffer.allocate(1)\n \u2026e())\n            .array()");
        this.T = array2;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(g80.k(super.A(), jd0.h3, Short.valueOf(this.Q)), jd0.i3, Long.valueOf(this.R));
    }

    @DexIgnore
    @Override // com.fossil.ps, com.fossil.tv
    public byte[] M() {
        return this.S;
    }

    @DexIgnore
    @Override // com.fossil.ps, com.fossil.tv
    public byte[] P() {
        return this.T;
    }

    @DexIgnore
    @Override // com.fossil.tv
    public void Q(byte[] bArr) {
        this.T = bArr;
    }

    @DexIgnore
    @Override // com.fossil.uv
    public void S(byte[] bArr) {
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order, "ByteBuffer.wrap(received\u2026(ByteOrder.LITTLE_ENDIAN)");
        this.Q = hy1.p(order.get(0));
        this.R = hy1.o(order.getInt(1));
    }

    @DexIgnore
    @Override // com.fossil.tv, com.fossil.fs
    public JSONObject z() {
        return g80.k(super.z(), jd0.A0, hy1.l(this.K, null, 1, null));
    }
}
