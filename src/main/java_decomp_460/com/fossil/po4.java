package com.fossil;

import androidx.lifecycle.ViewModelProvider;
import java.util.Iterator;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class po4 implements ViewModelProvider.Factory {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<Class<? extends ts0>, Provider<ts0>> f2852a;

    @DexIgnore
    public po4(Map<Class<? extends ts0>, Provider<ts0>> map) {
        pq7.c(map, "creators");
        this.f2852a = map;
    }

    @DexIgnore
    @Override // androidx.lifecycle.ViewModelProvider.Factory
    public <T extends ts0> T create(Class<T> cls) {
        T t;
        pq7.c(cls, "modelClass");
        Provider<ts0> provider = this.f2852a.get(cls);
        if (provider == null) {
            Iterator<T> it = this.f2852a.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (cls.isAssignableFrom((Class) next.getKey())) {
                    t = next;
                    break;
                }
            }
            T t2 = t;
            provider = t2 != null ? (Provider) t2.getValue() : null;
        }
        if (provider != null) {
            try {
                ts0 ts0 = provider.get();
                if (ts0 != null) {
                    return (T) ts0;
                }
                throw new il7("null cannot be cast to non-null type T");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new IllegalArgumentException("unknown model class; " + cls);
        }
    }
}
