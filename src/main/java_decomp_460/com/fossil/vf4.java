package com.fossil;

import android.util.Log;
import android.util.Pair;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vf4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f3759a;
    @DexIgnore
    public /* final */ Map<Pair<String, String>, nt3<if4>> b; // = new zi0();

    @DexIgnore
    public interface a {
        @DexIgnore
        nt3<if4> start();
    }

    @DexIgnore
    public vf4(Executor executor) {
        this.f3759a = executor;
    }

    @DexIgnore
    public nt3<if4> a(String str, String str2, a aVar) {
        nt3<if4> nt3;
        synchronized (this) {
            Pair<String, String> pair = new Pair<>(str, str2);
            nt3 = this.b.get(pair);
            if (nt3 == null) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(pair);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 24);
                    sb.append("Making new request for: ");
                    sb.append(valueOf);
                    Log.d("FirebaseInstanceId", sb.toString());
                }
                nt3 = aVar.start().k(this.f3759a, new uf4(this, pair));
                this.b.put(pair, nt3);
            } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf2 = String.valueOf(pair);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 29);
                sb2.append("Joining ongoing request for: ");
                sb2.append(valueOf2);
                Log.d("FirebaseInstanceId", sb2.toString());
            }
        }
        return nt3;
    }

    @DexIgnore
    public final /* synthetic */ nt3 b(Pair pair, nt3 nt3) throws Exception {
        synchronized (this) {
            this.b.remove(pair);
        }
        return nt3;
    }
}
