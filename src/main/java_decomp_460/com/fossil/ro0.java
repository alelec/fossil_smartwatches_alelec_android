package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ro0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public WeakReference<View> f3135a;
    @DexIgnore
    public Runnable b; // = null;
    @DexIgnore
    public Runnable c; // = null;
    @DexIgnore
    public int d; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ so0 f3136a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;

        @DexIgnore
        public a(ro0 ro0, so0 so0, View view) {
            this.f3136a = so0;
            this.b = view;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            this.f3136a.a(this.b);
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.f3136a.b(this.b);
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            this.f3136a.c(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ValueAnimator.AnimatorUpdateListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uo0 f3137a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;

        @DexIgnore
        public b(ro0 ro0, uo0 uo0, View view) {
            this.f3137a = uo0;
            this.b = view;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.f3137a.a(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements so0 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public ro0 f3138a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public c(ro0 ro0) {
            this.f3138a = ro0;
        }

        @DexIgnore
        @Override // com.fossil.so0
        public void a(View view) {
            Object tag = view.getTag(2113929216);
            so0 so0 = tag instanceof so0 ? (so0) tag : null;
            if (so0 != null) {
                so0.a(view);
            }
        }

        @DexIgnore
        @Override // com.fossil.so0
        @SuppressLint({"WrongConstant"})
        public void b(View view) {
            int i = this.f3138a.d;
            if (i > -1) {
                view.setLayerType(i, null);
                this.f3138a.d = -1;
            }
            if (Build.VERSION.SDK_INT >= 16 || !this.b) {
                ro0 ro0 = this.f3138a;
                Runnable runnable = ro0.c;
                if (runnable != null) {
                    ro0.c = null;
                    runnable.run();
                }
                Object tag = view.getTag(2113929216);
                so0 so0 = tag instanceof so0 ? (so0) tag : null;
                if (so0 != null) {
                    so0.b(view);
                }
                this.b = true;
            }
        }

        @DexIgnore
        @Override // com.fossil.so0
        public void c(View view) {
            this.b = false;
            if (this.f3138a.d > -1) {
                view.setLayerType(2, null);
            }
            ro0 ro0 = this.f3138a;
            Runnable runnable = ro0.b;
            if (runnable != null) {
                ro0.b = null;
                runnable.run();
            }
            Object tag = view.getTag(2113929216);
            so0 so0 = tag instanceof so0 ? (so0) tag : null;
            if (so0 != null) {
                so0.c(view);
            }
        }
    }

    @DexIgnore
    public ro0(View view) {
        this.f3135a = new WeakReference<>(view);
    }

    @DexIgnore
    public ro0 a(float f) {
        View view = this.f3135a.get();
        if (view != null) {
            view.animate().alpha(f);
        }
        return this;
    }

    @DexIgnore
    public void b() {
        View view = this.f3135a.get();
        if (view != null) {
            view.animate().cancel();
        }
    }

    @DexIgnore
    public long c() {
        View view = this.f3135a.get();
        if (view != null) {
            return view.animate().getDuration();
        }
        return 0;
    }

    @DexIgnore
    public ro0 d(long j) {
        View view = this.f3135a.get();
        if (view != null) {
            view.animate().setDuration(j);
        }
        return this;
    }

    @DexIgnore
    public ro0 e(Interpolator interpolator) {
        View view = this.f3135a.get();
        if (view != null) {
            view.animate().setInterpolator(interpolator);
        }
        return this;
    }

    @DexIgnore
    public ro0 f(so0 so0) {
        View view = this.f3135a.get();
        if (view != null) {
            if (Build.VERSION.SDK_INT >= 16) {
                g(view, so0);
            } else {
                view.setTag(2113929216, so0);
                g(view, new c(this));
            }
        }
        return this;
    }

    @DexIgnore
    public final void g(View view, so0 so0) {
        if (so0 != null) {
            view.animate().setListener(new a(this, so0, view));
        } else {
            view.animate().setListener(null);
        }
    }

    @DexIgnore
    public ro0 h(long j) {
        View view = this.f3135a.get();
        if (view != null) {
            view.animate().setStartDelay(j);
        }
        return this;
    }

    @DexIgnore
    public ro0 i(uo0 uo0) {
        View view = this.f3135a.get();
        if (view != null && Build.VERSION.SDK_INT >= 19) {
            b bVar = null;
            if (uo0 != null) {
                bVar = new b(this, uo0, view);
            }
            view.animate().setUpdateListener(bVar);
        }
        return this;
    }

    @DexIgnore
    public void j() {
        View view = this.f3135a.get();
        if (view != null) {
            view.animate().start();
        }
    }

    @DexIgnore
    public ro0 k(float f) {
        View view = this.f3135a.get();
        if (view != null) {
            view.animate().translationY(f);
        }
        return this;
    }
}
