package com.fossil;

import com.google.j2objc.annotations.Weak;
import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class c34<K, V> extends h34<Map.Entry<K, V>> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ a34<K, V> map;

        @DexIgnore
        public a(a34<K, V> a34) {
            this.map = a34;
        }

        @DexIgnore
        public Object readResolve() {
            return this.map.entrySet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<K, V> extends c34<K, V> {
        @DexIgnore
        @Weak
        public /* final */ transient a34<K, V> c;
        @DexIgnore
        public /* final */ transient Map.Entry<K, V>[] d;

        @DexIgnore
        public b(a34<K, V> a34, Map.Entry<K, V>[] entryArr) {
            this.c = a34;
            this.d = entryArr;
        }

        @DexIgnore
        @Override // com.fossil.h34
        public y24<Map.Entry<K, V>> createAsList() {
            return new m44(this, this.d);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.u24, com.fossil.u24, com.fossil.h34, com.fossil.h34, java.lang.Iterable
        public h54<Map.Entry<K, V>> iterator() {
            return p34.l(this.d);
        }

        @DexIgnore
        @Override // com.fossil.c34
        public a34<K, V> map() {
            return this.c;
        }
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean contains(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        V v = map().get(entry.getKey());
        return v != null && v.equals(entry.getValue());
    }

    @DexIgnore
    @Override // com.fossil.h34
    public int hashCode() {
        return map().hashCode();
    }

    @DexIgnore
    @Override // com.fossil.h34
    public boolean isHashCodeFast() {
        return map().isHashCodeFast();
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean isPartialView() {
        return map().isPartialView();
    }

    @DexIgnore
    public abstract a34<K, V> map();

    @DexIgnore
    public int size() {
        return map().size();
    }

    @DexIgnore
    @Override // com.fossil.u24, com.fossil.h34
    public Object writeReplace() {
        return new a(map());
    }
}
