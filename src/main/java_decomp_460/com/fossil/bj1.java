package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface bj1 {
    @DexIgnore
    boolean b();

    @DexIgnore
    Object clear();  // void declaration

    @DexIgnore
    Object f();  // void declaration

    @DexIgnore
    boolean g(bj1 bj1);

    @DexIgnore
    boolean h();

    @DexIgnore
    boolean isRunning();

    @DexIgnore
    boolean j();

    @DexIgnore
    Object pause();  // void declaration
}
