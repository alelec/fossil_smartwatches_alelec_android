package com.fossil;

import java.util.ListIterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qm7 implements ListIterator, jr7 {
    @DexIgnore
    public static /* final */ qm7 b; // = new qm7();

    @DexIgnore
    public Void a() {
        throw new NoSuchElementException();
    }

    @DexIgnore
    @Override // java.util.ListIterator
    public /* synthetic */ void add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public Void b() {
        throw new NoSuchElementException();
    }

    @DexIgnore
    public boolean hasNext() {
        return false;
    }

    @DexIgnore
    public boolean hasPrevious() {
        return false;
    }

    @DexIgnore
    @Override // java.util.Iterator, java.util.ListIterator
    public /* bridge */ /* synthetic */ Object next() {
        a();
        throw null;
    }

    @DexIgnore
    public int nextIndex() {
        return 0;
    }

    @DexIgnore
    @Override // java.util.ListIterator
    public /* bridge */ /* synthetic */ Object previous() {
        b();
        throw null;
    }

    @DexIgnore
    public int previousIndex() {
        return -1;
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.ListIterator
    public /* synthetic */ void set(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
