package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k35 extends j35 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d H; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray I;
    @DexIgnore
    public long G;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        I = sparseIntArray;
        sparseIntArray.put(2131363410, 1);
        I.put(2131362666, 2);
        I.put(2131361987, 3);
        I.put(2131362244, 4);
        I.put(2131361979, 5);
        I.put(2131362466, 6);
        I.put(2131362003, 7);
        I.put(2131363457, 8);
        I.put(2131363169, 9);
        I.put(2131363038, 10);
        I.put(2131362428, 11);
        I.put(2131361981, 12);
        I.put(2131361982, 13);
        I.put(2131361972, 14);
        I.put(2131363326, 15);
    }
    */

    @DexIgnore
    public k35(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 16, H, I));
    }

    @DexIgnore
    public k35(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[14], (ImageView) objArr[5], (FlexibleButton) objArr[12], (FlexibleButton) objArr[13], (ConstraintLayout) objArr[3], (FlexibleCheckBox) objArr[7], (FlexibleEditText) objArr[4], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[6], (RTLImageView) objArr[2], (ConstraintLayout) objArr[0], (RecyclerView) objArr[10], (SwipeRefreshLayout) objArr[9], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[1], (View) objArr[8]);
        this.G = -1;
        this.A.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.G = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.G != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.G = 1;
        }
        w();
    }
}
