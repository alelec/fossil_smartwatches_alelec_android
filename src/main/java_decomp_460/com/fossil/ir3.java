package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ir3 implements Parcelable.Creator<fr3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ fr3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        long j = 0;
        int i = 0;
        Double d = null;
        String str = null;
        String str2 = null;
        Float f = null;
        Long l = null;
        String str3 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 1:
                    i = ad2.v(parcel, t);
                    break;
                case 2:
                    str3 = ad2.f(parcel, t);
                    break;
                case 3:
                    j = ad2.y(parcel, t);
                    break;
                case 4:
                    l = ad2.z(parcel, t);
                    break;
                case 5:
                    f = ad2.s(parcel, t);
                    break;
                case 6:
                    str2 = ad2.f(parcel, t);
                    break;
                case 7:
                    str = ad2.f(parcel, t);
                    break;
                case 8:
                    d = ad2.q(parcel, t);
                    break;
                default:
                    ad2.B(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new fr3(i, str3, j, l, f, str2, str, d);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ fr3[] newArray(int i) {
        return new fr3[i];
    }
}
