package com.fossil;

import android.content.Context;
import com.fossil.c02;
import com.fossil.h02;
import com.fossil.n02;
import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m02 implements l02 {
    @DexIgnore
    public static volatile n02 e;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ t32 f2286a;
    @DexIgnore
    public /* final */ t32 b;
    @DexIgnore
    public /* final */ k12 c;
    @DexIgnore
    public /* final */ b22 d;

    @DexIgnore
    public m02(t32 t32, t32 t322, k12 k12, b22 b22, f22 f22) {
        this.f2286a = t32;
        this.b = t322;
        this.c = k12;
        this.d = b22;
        f22.a();
    }

    @DexIgnore
    public static m02 c() {
        n02 n02 = e;
        if (n02 != null) {
            return n02.b();
        }
        throw new IllegalStateException("Not initialized!");
    }

    @DexIgnore
    public static Set<ty1> d(zz1 zz1) {
        return zz1 instanceof a02 ? Collections.unmodifiableSet(((a02) zz1).a()) : Collections.singleton(ty1.b("proto"));
    }

    @DexIgnore
    public static void f(Context context) {
        if (e == null) {
            synchronized (m02.class) {
                try {
                    if (e == null) {
                        n02.a c2 = yz1.c();
                        c2.a(context);
                        e = c2.build();
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.l02
    public void a(g02 g02, zy1 zy1) {
        this.c.a(g02.f().e(g02.c().c()), b(g02), zy1);
    }

    @DexIgnore
    public final c02 b(g02 g02) {
        c02.a a2 = c02.a();
        a2.i(this.f2286a.a());
        a2.k(this.b.a());
        a2.j(g02.g());
        a2.h(new b02(g02.b(), g02.d()));
        a2.g(g02.c().a());
        return a2.d();
    }

    @DexIgnore
    public b22 e() {
        return this.d;
    }

    @DexIgnore
    public yy1 g(zz1 zz1) {
        Set<ty1> d2 = d(zz1);
        h02.a a2 = h02.a();
        a2.b(zz1.getName());
        a2.c(zz1.getExtras());
        return new i02(d2, a2.a(), this);
    }

    @DexIgnore
    @Deprecated
    public yy1 h(String str) {
        Set<ty1> d2 = d(null);
        h02.a a2 = h02.a();
        a2.b(str);
        return new i02(d2, a2.a(), this);
    }
}
