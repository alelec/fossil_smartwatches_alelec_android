package com.fossil;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.OverScroller;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class up0 {
    @DexIgnore
    public static /* final */ Interpolator w; // = new a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f3630a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public float[] d;
    @DexIgnore
    public float[] e;
    @DexIgnore
    public float[] f;
    @DexIgnore
    public float[] g;
    @DexIgnore
    public int[] h;
    @DexIgnore
    public int[] i;
    @DexIgnore
    public int[] j;
    @DexIgnore
    public int k;
    @DexIgnore
    public VelocityTracker l;
    @DexIgnore
    public float m;
    @DexIgnore
    public float n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public OverScroller q;
    @DexIgnore
    public /* final */ c r;
    @DexIgnore
    public View s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public /* final */ ViewGroup u;
    @DexIgnore
    public /* final */ Runnable v; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Interpolator {
        @DexIgnore
        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            up0.this.K(0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c {
        @DexIgnore
        public abstract int a(View view, int i, int i2);

        @DexIgnore
        public abstract int b(View view, int i, int i2);

        @DexIgnore
        public int c(int i) {
            return i;
        }

        @DexIgnore
        public int d(View view) {
            return 0;
        }

        @DexIgnore
        public int e(View view) {
            return 0;
        }

        @DexIgnore
        public void f(int i, int i2) {
        }

        @DexIgnore
        public boolean g(int i) {
            return false;
        }

        @DexIgnore
        public void h(int i, int i2) {
        }

        @DexIgnore
        public void i(View view, int i) {
        }

        @DexIgnore
        public abstract void j(int i);

        @DexIgnore
        public abstract void k(View view, int i, int i2, int i3, int i4);

        @DexIgnore
        public abstract void l(View view, float f, float f2);

        @DexIgnore
        public abstract boolean m(View view, int i);
    }

    @DexIgnore
    public up0(Context context, ViewGroup viewGroup, c cVar) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("Parent view may not be null");
        } else if (cVar != null) {
            this.u = viewGroup;
            this.r = cVar;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            this.o = (int) ((context.getResources().getDisplayMetrics().density * 20.0f) + 0.5f);
            this.b = viewConfiguration.getScaledTouchSlop();
            this.m = (float) viewConfiguration.getScaledMaximumFlingVelocity();
            this.n = (float) viewConfiguration.getScaledMinimumFlingVelocity();
            this.q = new OverScroller(context, w);
        } else {
            throw new IllegalArgumentException("Callback may not be null");
        }
    }

    @DexIgnore
    public static up0 o(ViewGroup viewGroup, float f2, c cVar) {
        up0 p2 = p(viewGroup, cVar);
        p2.b = (int) (((float) p2.b) * (1.0f / f2));
        return p2;
    }

    @DexIgnore
    public static up0 p(ViewGroup viewGroup, c cVar) {
        return new up0(viewGroup.getContext(), viewGroup, cVar);
    }

    @DexIgnore
    public int A() {
        return this.f3630a;
    }

    @DexIgnore
    public boolean B(int i2, int i3) {
        return E(this.s, i2, i3);
    }

    @DexIgnore
    public boolean C(int i2) {
        return (this.k & (1 << i2)) != 0;
    }

    @DexIgnore
    public final boolean D(int i2) {
        if (C(i2)) {
            return true;
        }
        Log.e("ViewDragHelper", "Ignoring pointerId=" + i2 + " because ACTION_DOWN was not received for this pointer before ACTION_MOVE. It likely happened because  ViewDragHelper did not receive all the events in the event stream.");
        return false;
    }

    @DexIgnore
    public boolean E(View view, int i2, int i3) {
        return view != null && i2 >= view.getLeft() && i2 < view.getRight() && i3 >= view.getTop() && i3 < view.getBottom();
    }

    @DexIgnore
    public void F(MotionEvent motionEvent) {
        int i2;
        int i3 = 0;
        int actionMasked = motionEvent.getActionMasked();
        int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            b();
        }
        if (this.l == null) {
            this.l = VelocityTracker.obtain();
        }
        this.l.addMovement(motionEvent);
        if (actionMasked == 0) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int pointerId = motionEvent.getPointerId(0);
            View u2 = u((int) x, (int) y);
            I(x, y, pointerId);
            Q(u2, pointerId);
            int i4 = this.h[pointerId];
            int i5 = this.p;
            if ((i4 & i5) != 0) {
                this.r.h(i4 & i5, pointerId);
            }
        } else if (actionMasked == 1) {
            if (this.f3630a == 1) {
                G();
            }
            b();
        } else if (actionMasked != 2) {
            if (actionMasked == 3) {
                if (this.f3630a == 1) {
                    q(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                b();
            } else if (actionMasked == 5) {
                int pointerId2 = motionEvent.getPointerId(actionIndex);
                float x2 = motionEvent.getX(actionIndex);
                float y2 = motionEvent.getY(actionIndex);
                I(x2, y2, pointerId2);
                if (this.f3630a == 0) {
                    Q(u((int) x2, (int) y2), pointerId2);
                    int i6 = this.h[pointerId2];
                    int i7 = this.p;
                    if ((i6 & i7) != 0) {
                        this.r.h(i6 & i7, pointerId2);
                    }
                } else if (B((int) x2, (int) y2)) {
                    Q(this.s, pointerId2);
                }
            } else if (actionMasked == 6) {
                int pointerId3 = motionEvent.getPointerId(actionIndex);
                if (this.f3630a == 1 && pointerId3 == this.c) {
                    int pointerCount = motionEvent.getPointerCount();
                    while (true) {
                        if (i3 >= pointerCount) {
                            i2 = -1;
                            break;
                        }
                        int pointerId4 = motionEvent.getPointerId(i3);
                        if (pointerId4 != this.c) {
                            View u3 = u((int) motionEvent.getX(i3), (int) motionEvent.getY(i3));
                            View view = this.s;
                            if (u3 == view && Q(view, pointerId4)) {
                                i2 = this.c;
                                break;
                            }
                        }
                        i3++;
                    }
                    if (i2 == -1) {
                        G();
                    }
                }
                k(pointerId3);
            }
        } else if (this.f3630a != 1) {
            int pointerCount2 = motionEvent.getPointerCount();
            while (i3 < pointerCount2) {
                int pointerId5 = motionEvent.getPointerId(i3);
                if (D(pointerId5)) {
                    float x3 = motionEvent.getX(i3);
                    float y3 = motionEvent.getY(i3);
                    float f2 = x3 - this.d[pointerId5];
                    float f3 = y3 - this.e[pointerId5];
                    H(f2, f3, pointerId5);
                    if (this.f3630a != 1) {
                        View u4 = u((int) x3, (int) y3);
                        if (g(u4, f2, f3) && Q(u4, pointerId5)) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                i3++;
            }
            J(motionEvent);
        } else if (D(this.c)) {
            int findPointerIndex = motionEvent.findPointerIndex(this.c);
            float x4 = motionEvent.getX(findPointerIndex);
            float y4 = motionEvent.getY(findPointerIndex);
            float[] fArr = this.f;
            int i8 = this.c;
            int i9 = (int) (x4 - fArr[i8]);
            int i10 = (int) (y4 - this.g[i8]);
            s(this.s.getLeft() + i9, this.s.getTop() + i10, i9, i10);
            J(motionEvent);
        }
    }

    @DexIgnore
    public final void G() {
        this.l.computeCurrentVelocity(1000, this.m);
        q(h(this.l.getXVelocity(this.c), this.n, this.m), h(this.l.getYVelocity(this.c), this.n, this.m));
    }

    @DexIgnore
    public final void H(float f2, float f3, int i2) {
        int i3 = 1;
        if (!d(f2, f3, i2, 1)) {
            i3 = 0;
        }
        if (d(f3, f2, i2, 4)) {
            i3 |= 4;
        }
        if (d(f2, f3, i2, 2)) {
            i3 |= 2;
        }
        if (d(f3, f2, i2, 8)) {
            i3 |= 8;
        }
        if (i3 != 0) {
            int[] iArr = this.i;
            iArr[i2] = iArr[i2] | i3;
            this.r.f(i3, i2);
        }
    }

    @DexIgnore
    public final void I(float f2, float f3, int i2) {
        t(i2);
        float[] fArr = this.d;
        this.f[i2] = f2;
        fArr[i2] = f2;
        float[] fArr2 = this.e;
        this.g[i2] = f3;
        fArr2[i2] = f3;
        this.h[i2] = y((int) f2, (int) f3);
        this.k |= 1 << i2;
    }

    @DexIgnore
    public final void J(MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();
        for (int i2 = 0; i2 < pointerCount; i2++) {
            int pointerId = motionEvent.getPointerId(i2);
            if (D(pointerId)) {
                float x = motionEvent.getX(i2);
                float y = motionEvent.getY(i2);
                this.f[pointerId] = x;
                this.g[pointerId] = y;
            }
        }
    }

    @DexIgnore
    public void K(int i2) {
        this.u.removeCallbacks(this.v);
        if (this.f3630a != i2) {
            this.f3630a = i2;
            this.r.j(i2);
            if (this.f3630a == 0) {
                this.s = null;
            }
        }
    }

    @DexIgnore
    public void L(int i2) {
        this.p = i2;
    }

    @DexIgnore
    public void M(float f2) {
        this.n = f2;
    }

    @DexIgnore
    public boolean N(int i2, int i3) {
        if (this.t) {
            return v(i2, i3, (int) this.l.getXVelocity(this.c), (int) this.l.getYVelocity(this.c));
        }
        throw new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00df, code lost:
        if (r8 != r7) goto L_0x00ee;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean O(android.view.MotionEvent r14) {
        /*
        // Method dump skipped, instructions count: 312
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.up0.O(android.view.MotionEvent):boolean");
    }

    @DexIgnore
    public boolean P(View view, int i2, int i3) {
        this.s = view;
        this.c = -1;
        boolean v2 = v(i2, i3, 0, 0);
        if (!v2 && this.f3630a == 0 && this.s != null) {
            this.s = null;
        }
        return v2;
    }

    @DexIgnore
    public boolean Q(View view, int i2) {
        if (view == this.s && this.c == i2) {
            return true;
        }
        if (view == null || !this.r.m(view, i2)) {
            return false;
        }
        this.c = i2;
        c(view, i2);
        return true;
    }

    @DexIgnore
    public void a() {
        b();
        if (this.f3630a == 2) {
            int currX = this.q.getCurrX();
            int currY = this.q.getCurrY();
            this.q.abortAnimation();
            int currX2 = this.q.getCurrX();
            int currY2 = this.q.getCurrY();
            this.r.k(this.s, currX2, currY2, currX2 - currX, currY2 - currY);
        }
        K(0);
    }

    @DexIgnore
    public void b() {
        this.c = -1;
        j();
        VelocityTracker velocityTracker = this.l;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.l = null;
        }
    }

    @DexIgnore
    public void c(View view, int i2) {
        if (view.getParent() == this.u) {
            this.s = view;
            this.c = i2;
            this.r.i(view, i2);
            K(1);
            return;
        }
        throw new IllegalArgumentException("captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view (" + this.u + ")");
    }

    @DexIgnore
    public final boolean d(float f2, float f3, int i2, int i3) {
        float abs = Math.abs(f2);
        float abs2 = Math.abs(f3);
        if ((this.h[i2] & i3) != i3 || (this.p & i3) == 0 || (this.j[i2] & i3) == i3 || (this.i[i2] & i3) == i3) {
            return false;
        }
        int i4 = this.b;
        if (abs <= ((float) i4) && abs2 <= ((float) i4)) {
            return false;
        }
        if (abs >= abs2 * 0.5f || !this.r.g(i3)) {
            return (this.i[i2] & i3) == 0 && abs > ((float) this.b);
        }
        int[] iArr = this.j;
        iArr[i2] = iArr[i2] | i3;
        return false;
    }

    @DexIgnore
    public boolean e(int i2) {
        int length = this.d.length;
        for (int i3 = 0; i3 < length; i3++) {
            if (f(i2, i3)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean f(int i2, int i3) {
        boolean z = true;
        if (!C(i3)) {
            return false;
        }
        boolean z2 = (i2 & 1) == 1;
        boolean z3 = (i2 & 2) == 2;
        float f2 = this.f[i3] - this.d[i3];
        float f3 = this.g[i3] - this.e[i3];
        if (z2 && z3) {
            int i4 = this.b;
            if ((f2 * f2) + (f3 * f3) <= ((float) (i4 * i4))) {
                z = false;
            }
            return z;
        } else if (!z2) {
            return z3 && Math.abs(f3) > ((float) this.b);
        } else {
            if (Math.abs(f2) <= ((float) this.b)) {
                z = false;
            }
            return z;
        }
    }

    @DexIgnore
    public final boolean g(View view, float f2, float f3) {
        boolean z = true;
        if (view == null) {
            return false;
        }
        boolean z2 = this.r.d(view) > 0;
        boolean z3 = this.r.e(view) > 0;
        if (z2 && z3) {
            int i2 = this.b;
            if ((f2 * f2) + (f3 * f3) <= ((float) (i2 * i2))) {
                z = false;
            }
            return z;
        } else if (!z2) {
            return z3 && Math.abs(f3) > ((float) this.b);
        } else {
            if (Math.abs(f2) <= ((float) this.b)) {
                z = false;
            }
            return z;
        }
    }

    @DexIgnore
    public final float h(float f2, float f3, float f4) {
        float abs = Math.abs(f2);
        return abs < f3 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : abs > f4 ? f2 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? -f4 : f4 : f2;
    }

    @DexIgnore
    public final int i(int i2, int i3, int i4) {
        int abs = Math.abs(i2);
        if (abs < i3) {
            return 0;
        }
        return abs > i4 ? i2 <= 0 ? -i4 : i4 : i2;
    }

    @DexIgnore
    public final void j() {
        float[] fArr = this.d;
        if (fArr != null) {
            Arrays.fill(fArr, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            Arrays.fill(this.e, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            Arrays.fill(this.f, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            Arrays.fill(this.g, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            Arrays.fill(this.h, 0);
            Arrays.fill(this.i, 0);
            Arrays.fill(this.j, 0);
            this.k = 0;
        }
    }

    @DexIgnore
    public final void k(int i2) {
        if (this.d != null && C(i2)) {
            this.d[i2] = 0.0f;
            this.e[i2] = 0.0f;
            this.f[i2] = 0.0f;
            this.g[i2] = 0.0f;
            this.h[i2] = 0;
            this.i[i2] = 0;
            this.j[i2] = 0;
            this.k = (1 << i2) & this.k;
        }
    }

    @DexIgnore
    public final int l(int i2, int i3, int i4) {
        if (i2 == 0) {
            return 0;
        }
        int width = this.u.getWidth();
        float f2 = (float) (width / 2);
        float r2 = r(Math.min(1.0f, ((float) Math.abs(i2)) / ((float) width)));
        int abs = Math.abs(i3);
        return Math.min(abs > 0 ? Math.round(Math.abs(((r2 * f2) + f2) / ((float) abs)) * 1000.0f) * 4 : (int) (((((float) Math.abs(i2)) / ((float) i4)) + 1.0f) * 256.0f), 600);
    }

    @DexIgnore
    public final int m(View view, int i2, int i3, int i4, int i5) {
        float f2;
        float f3;
        float f4;
        float f5;
        int i6 = i(i4, (int) this.n, (int) this.m);
        int i7 = i(i5, (int) this.n, (int) this.m);
        int abs = Math.abs(i2);
        int abs2 = Math.abs(i3);
        int abs3 = Math.abs(i6);
        int abs4 = Math.abs(i7);
        int i8 = abs3 + abs4;
        int i9 = abs + abs2;
        if (i6 != 0) {
            f2 = (float) abs3;
            f3 = (float) i8;
        } else {
            f2 = (float) abs;
            f3 = (float) i9;
        }
        float f6 = f2 / f3;
        if (i7 != 0) {
            f4 = (float) abs4;
            f5 = (float) i8;
        } else {
            f4 = (float) abs2;
            f5 = (float) i9;
        }
        float f7 = f4 / f5;
        int l2 = l(i2, i6, this.r.d(view));
        return (int) ((f7 * ((float) l(i3, i7, this.r.e(view)))) + (((float) l2) * f6));
    }

    @DexIgnore
    public boolean n(boolean z) {
        boolean z2;
        if (this.f3630a == 2) {
            boolean computeScrollOffset = this.q.computeScrollOffset();
            int currX = this.q.getCurrX();
            int currY = this.q.getCurrY();
            int left = currX - this.s.getLeft();
            int top = currY - this.s.getTop();
            if (left != 0) {
                mo0.V(this.s, left);
            }
            if (top != 0) {
                mo0.W(this.s, top);
            }
            if (!(left == 0 && top == 0)) {
                this.r.k(this.s, currX, currY, left, top);
            }
            if (computeScrollOffset && currX == this.q.getFinalX() && currY == this.q.getFinalY()) {
                this.q.abortAnimation();
                z2 = false;
            } else {
                z2 = computeScrollOffset;
            }
            if (!z2) {
                if (z) {
                    this.u.post(this.v);
                } else {
                    K(0);
                }
            }
        }
        return this.f3630a == 2;
    }

    @DexIgnore
    public final void q(float f2, float f3) {
        this.t = true;
        this.r.l(this.s, f2, f3);
        this.t = false;
        if (this.f3630a == 1) {
            K(0);
        }
    }

    @DexIgnore
    public final float r(float f2) {
        return (float) Math.sin((double) ((f2 - 0.5f) * 0.47123894f));
    }

    @DexIgnore
    public final void s(int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int left = this.s.getLeft();
        int top = this.s.getTop();
        if (i4 != 0) {
            i6 = this.r.a(this.s, i2, i4);
            mo0.V(this.s, i6 - left);
        } else {
            i6 = i2;
        }
        if (i5 != 0) {
            i7 = this.r.b(this.s, i3, i5);
            mo0.W(this.s, i7 - top);
        } else {
            i7 = i3;
        }
        if (i4 != 0 || i5 != 0) {
            this.r.k(this.s, i6, i7, i6 - left, i7 - top);
        }
    }

    @DexIgnore
    public final void t(int i2) {
        float[] fArr = this.d;
        if (fArr == null || fArr.length <= i2) {
            int i3 = i2 + 1;
            float[] fArr2 = new float[i3];
            float[] fArr3 = new float[i3];
            float[] fArr4 = new float[i3];
            float[] fArr5 = new float[i3];
            int[] iArr = new int[i3];
            int[] iArr2 = new int[i3];
            int[] iArr3 = new int[i3];
            float[] fArr6 = this.d;
            if (fArr6 != null) {
                System.arraycopy(fArr6, 0, fArr2, 0, fArr6.length);
                float[] fArr7 = this.e;
                System.arraycopy(fArr7, 0, fArr3, 0, fArr7.length);
                float[] fArr8 = this.f;
                System.arraycopy(fArr8, 0, fArr4, 0, fArr8.length);
                float[] fArr9 = this.g;
                System.arraycopy(fArr9, 0, fArr5, 0, fArr9.length);
                int[] iArr4 = this.h;
                System.arraycopy(iArr4, 0, iArr, 0, iArr4.length);
                int[] iArr5 = this.i;
                System.arraycopy(iArr5, 0, iArr2, 0, iArr5.length);
                int[] iArr6 = this.j;
                System.arraycopy(iArr6, 0, iArr3, 0, iArr6.length);
            }
            this.d = fArr2;
            this.e = fArr3;
            this.f = fArr4;
            this.g = fArr5;
            this.h = iArr;
            this.i = iArr2;
            this.j = iArr3;
        }
    }

    @DexIgnore
    public View u(int i2, int i3) {
        for (int childCount = this.u.getChildCount() - 1; childCount >= 0; childCount--) {
            ViewGroup viewGroup = this.u;
            this.r.c(childCount);
            View childAt = viewGroup.getChildAt(childCount);
            if (i2 >= childAt.getLeft() && i2 < childAt.getRight() && i3 >= childAt.getTop() && i3 < childAt.getBottom()) {
                return childAt;
            }
        }
        return null;
    }

    @DexIgnore
    public final boolean v(int i2, int i3, int i4, int i5) {
        int left = this.s.getLeft();
        int top = this.s.getTop();
        int i6 = i2 - left;
        int i7 = i3 - top;
        if (i6 == 0 && i7 == 0) {
            this.q.abortAnimation();
            K(0);
            return false;
        }
        this.q.startScroll(left, top, i6, i7, m(this.s, i6, i7, i4, i5));
        K(2);
        return true;
    }

    @DexIgnore
    public View w() {
        return this.s;
    }

    @DexIgnore
    public int x() {
        return this.o;
    }

    @DexIgnore
    public final int y(int i2, int i3) {
        int i4 = i2 < this.u.getLeft() + this.o ? 1 : 0;
        if (i3 < this.u.getTop() + this.o) {
            i4 |= 4;
        }
        if (i2 > this.u.getRight() - this.o) {
            i4 |= 2;
        }
        return i3 > this.u.getBottom() - this.o ? i4 | 8 : i4;
    }

    @DexIgnore
    public int z() {
        return this.b;
    }
}
