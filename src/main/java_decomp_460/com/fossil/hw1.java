package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.imagefilters.Format;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hw1 extends fw1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<hw1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public hw1 createFromParcel(Parcel parcel) {
            return new hw1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public hw1[] newArray(int i) {
            return new hw1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ hw1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    public hw1(String str, byte[] bArr) {
        super(str, bArr, Format.RAW);
    }

    @DexIgnore
    @Override // com.fossil.fw1, com.fossil.fw1, java.lang.Object
    public hw1 clone() {
        String name = getName();
        byte[] bitmapImageData = getBitmapImageData();
        byte[] copyOf = Arrays.copyOf(bitmapImageData, bitmapImageData.length);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new hw1(name, copyOf);
    }
}
