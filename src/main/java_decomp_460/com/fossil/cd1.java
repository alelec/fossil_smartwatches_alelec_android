package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cd1<Z> implements id1<Z> {
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ id1<Z> d;
    @DexIgnore
    public /* final */ a e;
    @DexIgnore
    public /* final */ mb1 f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;

    @DexIgnore
    public interface a {
        @DexIgnore
        void d(mb1 mb1, cd1<?> cd1);
    }

    @DexIgnore
    public cd1(id1<Z> id1, boolean z, boolean z2, mb1 mb1, a aVar) {
        ik1.d(id1);
        this.d = id1;
        this.b = z;
        this.c = z2;
        this.f = mb1;
        ik1.d(aVar);
        this.e = aVar;
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            if (!this.h) {
                this.g++;
            } else {
                throw new IllegalStateException("Cannot acquire a recycled resource");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.id1
    public void b() {
        synchronized (this) {
            if (this.g > 0) {
                throw new IllegalStateException("Cannot recycle a resource while it is still acquired");
            } else if (!this.h) {
                this.h = true;
                if (this.c) {
                    this.d.b();
                }
            } else {
                throw new IllegalStateException("Cannot recycle a resource that has already been recycled");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.id1
    public int c() {
        return this.d.c();
    }

    @DexIgnore
    @Override // com.fossil.id1
    public Class<Z> d() {
        return this.d.d();
    }

    @DexIgnore
    public id1<Z> e() {
        return this.d;
    }

    @DexIgnore
    public boolean f() {
        return this.b;
    }

    @DexIgnore
    public void g() {
        boolean z;
        synchronized (this) {
            if (this.g > 0) {
                z = true;
                int i = this.g - 1;
                this.g = i;
                if (i != 0) {
                    z = false;
                }
            } else {
                throw new IllegalStateException("Cannot release a recycled or not yet acquired resource");
            }
        }
        if (z) {
            this.e.d(this.f, this);
        }
    }

    @DexIgnore
    @Override // com.fossil.id1
    public Z get() {
        return this.d.get();
    }

    @DexIgnore
    public String toString() {
        String str;
        synchronized (this) {
            str = "EngineResource{isMemoryCacheable=" + this.b + ", listener=" + this.e + ", key=" + this.f + ", acquired=" + this.g + ", isRecycled=" + this.h + ", resource=" + this.d + '}';
        }
        return str;
    }
}
