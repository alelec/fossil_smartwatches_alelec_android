package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.fossil.rd7;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sd7 extends rd7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3244a;

    @DexIgnore
    public sd7(Context context) {
        this.f3244a = context;
    }

    @DexIgnore
    public static Bitmap j(Resources resources, int i, pd7 pd7) {
        BitmapFactory.Options d = rd7.d(pd7);
        if (rd7.g(d)) {
            BitmapFactory.decodeResource(resources, i, d);
            rd7.b(pd7.h, pd7.i, d, pd7);
        }
        return BitmapFactory.decodeResource(resources, i, d);
    }

    @DexIgnore
    @Override // com.fossil.rd7
    public boolean c(pd7 pd7) {
        if (pd7.e != 0) {
            return true;
        }
        return "android.resource".equals(pd7.d.getScheme());
    }

    @DexIgnore
    @Override // com.fossil.rd7
    public rd7.a f(pd7 pd7, int i) throws IOException {
        Resources o = xd7.o(this.f3244a, pd7);
        return new rd7.a(j(o, xd7.n(o, pd7), pd7), Picasso.LoadedFrom.DISK);
    }
}
