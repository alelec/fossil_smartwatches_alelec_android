package com.fossil;

import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.GuestApiService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q37 implements MembersInjector<p37> {
    @DexIgnore
    public static void a(p37 p37, DeviceRepository deviceRepository) {
        p37.f2774a = deviceRepository;
    }

    @DexIgnore
    public static void b(p37 p37, FirmwareFileRepository firmwareFileRepository) {
        p37.e = firmwareFileRepository;
    }

    @DexIgnore
    public static void c(p37 p37, GuestApiService guestApiService) {
        p37.d = guestApiService;
    }

    @DexIgnore
    public static void d(p37 p37, on5 on5) {
        p37.b = on5;
    }

    @DexIgnore
    public static void e(p37 p37, UserRepository userRepository) {
        p37.c = userRepository;
    }
}
