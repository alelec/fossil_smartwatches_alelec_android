package com.fossil;

import android.content.Context;
import android.graphics.PointF;
import android.util.DisplayMetrics;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mv0 extends qv0 {
    @DexIgnore
    public lv0 d;
    @DexIgnore
    public lv0 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends gv0 {
        @DexIgnore
        public a(Context context) {
            super(context);
        }

        @DexIgnore
        @Override // com.fossil.gv0, androidx.recyclerview.widget.RecyclerView.v
        public void o(View view, RecyclerView.State state, RecyclerView.v.a aVar) {
            mv0 mv0 = mv0.this;
            int[] c = mv0.c(mv0.f3029a.getLayoutManager(), view);
            int i = c[0];
            int i2 = c[1];
            int w = w(Math.max(Math.abs(i), Math.abs(i2)));
            if (w > 0) {
                aVar.d(i, i2, w, this.j);
            }
        }

        @DexIgnore
        @Override // com.fossil.gv0
        public float v(DisplayMetrics displayMetrics) {
            return 100.0f / ((float) displayMetrics.densityDpi);
        }

        @DexIgnore
        @Override // com.fossil.gv0
        public int x(int i) {
            return Math.min(100, super.x(i));
        }
    }

    @DexIgnore
    @Override // com.fossil.qv0
    public int[] c(RecyclerView.m mVar, View view) {
        int[] iArr = new int[2];
        if (mVar.l()) {
            iArr[0] = m(mVar, view, o(mVar));
        } else {
            iArr[0] = 0;
        }
        if (mVar.m()) {
            iArr[1] = m(mVar, view, q(mVar));
        } else {
            iArr[1] = 0;
        }
        return iArr;
    }

    @DexIgnore
    @Override // com.fossil.qv0
    public gv0 f(RecyclerView.m mVar) {
        if (!(mVar instanceof RecyclerView.v.b)) {
            return null;
        }
        return new a(this.f3029a.getContext());
    }

    @DexIgnore
    @Override // com.fossil.qv0
    public View h(RecyclerView.m mVar) {
        if (mVar.m()) {
            return n(mVar, q(mVar));
        }
        if (mVar.l()) {
            return n(mVar, o(mVar));
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.qv0
    public int i(RecyclerView.m mVar, int i, int i2) {
        int Z = mVar.Z();
        if (Z == 0) {
            return -1;
        }
        lv0 p = p(mVar);
        if (p == null) {
            return -1;
        }
        int i3 = RecyclerView.UNDEFINED_DURATION;
        int i4 = Integer.MAX_VALUE;
        int K = mVar.K();
        View view = null;
        View view2 = null;
        for (int i5 = 0; i5 < K; i5++) {
            View J = mVar.J(i5);
            if (J != null) {
                int m = m(mVar, J, p);
                if (m <= 0 && m > i3) {
                    view = J;
                    i3 = m;
                }
                if (m >= 0 && m < i4) {
                    view2 = J;
                    i4 = m;
                }
            }
        }
        boolean r = r(mVar, i, i2);
        if (r && view2 != null) {
            return mVar.i0(view2);
        }
        if (!(r || view == null)) {
            return mVar.i0(view);
        }
        if (!r) {
            view = view2;
        }
        if (view == null) {
            return -1;
        }
        int i0 = (s(mVar) == r ? -1 : 1) + mVar.i0(view);
        if (i0 < 0 || i0 >= Z) {
            return -1;
        }
        return i0;
    }

    @DexIgnore
    public final int m(RecyclerView.m mVar, View view, lv0 lv0) {
        return (lv0.g(view) + (lv0.e(view) / 2)) - (lv0.m() + (lv0.n() / 2));
    }

    @DexIgnore
    public final View n(RecyclerView.m mVar, lv0 lv0) {
        View view = null;
        int K = mVar.K();
        if (K != 0) {
            int m = lv0.m();
            int n = lv0.n() / 2;
            int i = Integer.MAX_VALUE;
            int i2 = 0;
            while (i2 < K) {
                View J = mVar.J(i2);
                int abs = Math.abs((lv0.g(J) + (lv0.e(J) / 2)) - (m + n));
                if (abs >= i) {
                    abs = i;
                    J = view;
                }
                i2++;
                i = abs;
                view = J;
            }
        }
        return view;
    }

    @DexIgnore
    public final lv0 o(RecyclerView.m mVar) {
        lv0 lv0 = this.e;
        if (lv0 == null || lv0.f2253a != mVar) {
            this.e = lv0.a(mVar);
        }
        return this.e;
    }

    @DexIgnore
    public final lv0 p(RecyclerView.m mVar) {
        if (mVar.m()) {
            return q(mVar);
        }
        if (mVar.l()) {
            return o(mVar);
        }
        return null;
    }

    @DexIgnore
    public final lv0 q(RecyclerView.m mVar) {
        lv0 lv0 = this.d;
        if (lv0 == null || lv0.f2253a != mVar) {
            this.d = lv0.c(mVar);
        }
        return this.d;
    }

    @DexIgnore
    public final boolean r(RecyclerView.m mVar, int i, int i2) {
        return mVar.l() ? i > 0 : i2 > 0;
    }

    @DexIgnore
    public final boolean s(RecyclerView.m mVar) {
        PointF a2;
        int Z = mVar.Z();
        if (!(mVar instanceof RecyclerView.v.b) || (a2 = ((RecyclerView.v.b) mVar).a(Z - 1)) == null) {
            return false;
        }
        return a2.x < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || a2.y < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }
}
