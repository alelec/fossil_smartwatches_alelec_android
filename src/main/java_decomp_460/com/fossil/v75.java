package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleFitnessTab;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.RingProgressBar;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v75 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public /* final */ ConstraintLayout B;
    @DexIgnore
    public /* final */ CollapsingToolbarLayout C;
    @DexIgnore
    public /* final */ FlexibleButton D;
    @DexIgnore
    public /* final */ FlexibleFitnessTab E;
    @DexIgnore
    public /* final */ FlexibleFitnessTab F;
    @DexIgnore
    public /* final */ FlexibleFitnessTab G;
    @DexIgnore
    public /* final */ FlexibleFitnessTab H;
    @DexIgnore
    public /* final */ FlexibleFitnessTab I;
    @DexIgnore
    public /* final */ FlexibleFitnessTab J;
    @DexIgnore
    public /* final */ FlexibleTextView K;
    @DexIgnore
    public /* final */ FlexibleTextView L;
    @DexIgnore
    public /* final */ FlexibleTextView M;
    @DexIgnore
    public /* final */ ImageView N;
    @DexIgnore
    public /* final */ RTLImageView O;
    @DexIgnore
    public /* final */ NestedScrollView P;
    @DexIgnore
    public /* final */ FlexibleProgressBar Q;
    @DexIgnore
    public /* final */ ConstraintLayout R;
    @DexIgnore
    public /* final */ RingProgressBar S;
    @DexIgnore
    public /* final */ RingProgressBar T;
    @DexIgnore
    public /* final */ RingProgressBar U;
    @DexIgnore
    public /* final */ RingProgressBar V;
    @DexIgnore
    public /* final */ ViewPager2 W;
    @DexIgnore
    public /* final */ CustomSwipeRefreshLayout X;
    @DexIgnore
    public /* final */ FlexibleProgressBar Y;
    @DexIgnore
    public /* final */ FlexibleTextView Z;
    @DexIgnore
    public /* final */ FlexibleTextView a0;
    @DexIgnore
    public /* final */ FlexibleTextView b0;
    @DexIgnore
    public /* final */ FlexibleTextView c0;
    @DexIgnore
    public /* final */ View d0;
    @DexIgnore
    public /* final */ View e0;
    @DexIgnore
    public /* final */ View f0;
    @DexIgnore
    public /* final */ AppBarLayout q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ CoordinatorLayout u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ ConstraintLayout w;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public /* final */ ConstraintLayout z;

    @DexIgnore
    public v75(Object obj, View view, int i, AppBarLayout appBarLayout, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, ConstraintLayout constraintLayout, CoordinatorLayout coordinatorLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ConstraintLayout constraintLayout5, ConstraintLayout constraintLayout6, ConstraintLayout constraintLayout7, ConstraintLayout constraintLayout8, CollapsingToolbarLayout collapsingToolbarLayout, FlexibleButton flexibleButton, FlexibleFitnessTab flexibleFitnessTab, FlexibleFitnessTab flexibleFitnessTab2, FlexibleFitnessTab flexibleFitnessTab3, FlexibleFitnessTab flexibleFitnessTab4, FlexibleFitnessTab flexibleFitnessTab5, FlexibleFitnessTab flexibleFitnessTab6, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, ImageView imageView, RTLImageView rTLImageView, NestedScrollView nestedScrollView, FlexibleProgressBar flexibleProgressBar, ConstraintLayout constraintLayout9, RingProgressBar ringProgressBar, RingProgressBar ringProgressBar2, RingProgressBar ringProgressBar3, RingProgressBar ringProgressBar4, ViewPager2 viewPager2, CustomSwipeRefreshLayout customSwipeRefreshLayout, FlexibleProgressBar flexibleProgressBar2, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, View view2, View view3, View view4) {
        super(obj, view, i);
        this.q = appBarLayout;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = constraintLayout;
        this.u = coordinatorLayout;
        this.v = constraintLayout2;
        this.w = constraintLayout3;
        this.x = constraintLayout4;
        this.y = constraintLayout5;
        this.z = constraintLayout6;
        this.A = constraintLayout7;
        this.B = constraintLayout8;
        this.C = collapsingToolbarLayout;
        this.D = flexibleButton;
        this.E = flexibleFitnessTab;
        this.F = flexibleFitnessTab2;
        this.G = flexibleFitnessTab3;
        this.H = flexibleFitnessTab4;
        this.I = flexibleFitnessTab5;
        this.J = flexibleFitnessTab6;
        this.K = flexibleTextView3;
        this.L = flexibleTextView4;
        this.M = flexibleTextView5;
        this.N = imageView;
        this.O = rTLImageView;
        this.P = nestedScrollView;
        this.Q = flexibleProgressBar;
        this.R = constraintLayout9;
        this.S = ringProgressBar;
        this.T = ringProgressBar2;
        this.U = ringProgressBar3;
        this.V = ringProgressBar4;
        this.W = viewPager2;
        this.X = customSwipeRefreshLayout;
        this.Y = flexibleProgressBar2;
        this.Z = flexibleTextView6;
        this.a0 = flexibleTextView7;
        this.b0 = flexibleTextView8;
        this.c0 = flexibleTextView9;
        this.d0 = view2;
        this.e0 = view3;
        this.f0 = view4;
    }
}
