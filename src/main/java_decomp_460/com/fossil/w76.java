package com.fossil;

import com.google.android.libraries.places.api.net.PlacesClient;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface w76 extends gq4<v76> {
    @DexIgnore
    void H(PlacesClient placesClient);

    @DexIgnore
    void S3(String str);

    @DexIgnore
    void V1(String str);

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    void h0(List<String> list);

    @DexIgnore
    void setTitle(String str);
}
