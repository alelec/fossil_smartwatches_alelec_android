package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.pc2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ue3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ue3> CREATOR; // = new if3();
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ float c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public float f3574a;
        @DexIgnore
        public float b;

        @DexIgnore
        public final a a(float f) {
            this.f3574a = f;
            return this;
        }

        @DexIgnore
        public final ue3 b() {
            return new ue3(this.b, this.f3574a);
        }

        @DexIgnore
        public final a c(float f) {
            this.b = f;
            return this;
        }
    }

    @DexIgnore
    public ue3(float f, float f2) {
        boolean z = -90.0f <= f && f <= 90.0f;
        StringBuilder sb = new StringBuilder(62);
        sb.append("Tilt needs to be between -90 and 90 inclusive: ");
        sb.append(f);
        rc2.b(z, sb.toString());
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES + f;
        this.c = (((double) f2) <= 0.0d ? (f2 % 360.0f) + 360.0f : f2) % 360.0f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ue3)) {
            return false;
        }
        ue3 ue3 = (ue3) obj;
        return Float.floatToIntBits(this.b) == Float.floatToIntBits(ue3.b) && Float.floatToIntBits(this.c) == Float.floatToIntBits(ue3.c);
    }

    @DexIgnore
    public int hashCode() {
        return pc2.b(Float.valueOf(this.b), Float.valueOf(this.c));
    }

    @DexIgnore
    public String toString() {
        pc2.a c2 = pc2.c(this);
        c2.a("tilt", Float.valueOf(this.b));
        c2.a("bearing", Float.valueOf(this.c));
        return c2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.j(parcel, 2, this.b);
        bd2.j(parcel, 3, this.c);
        bd2.b(parcel, a2);
    }
}
