package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ox7 extends iu7 {
    @DexIgnore
    public /* final */ lz7 b;

    @DexIgnore
    public ox7(lz7 lz7) {
        this.b = lz7;
    }

    @DexIgnore
    @Override // com.fossil.ju7
    public void a(Throwable th) {
        this.b.r();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
        a(th);
        return tl7.f3441a;
    }

    @DexIgnore
    public String toString() {
        return "RemoveOnCancel[" + this.b + ']';
    }
}
