package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kv extends dv {
    @DexIgnore
    public n6 M;
    @DexIgnore
    public /* final */ long N;
    @DexIgnore
    public /* final */ long O;
    @DexIgnore
    public /* final */ long P;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ kv(long j, long j2, long j3, short s, k5 k5Var, int i, int i2) {
        super(iu.e, s, hs.m, k5Var, (i2 & 32) != 0 ? 3 : i);
        this.N = j;
        this.O = j2;
        this.P = j3;
        this.M = n6.UNKNOWN;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(super.A(), jd0.G0, this.M.b);
    }

    @DexIgnore
    @Override // com.fossil.ps
    public JSONObject F(byte[] bArr) {
        n6 n6Var;
        boolean z = true;
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length != 0) {
            z = false;
        }
        if (!z) {
            n6Var = n6.q.a(bArr[0]);
        } else {
            n6Var = n6.FTD;
        }
        this.M = n6Var;
        return g80.k(jSONObject, jd0.G0, n6Var.b);
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(12).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.N).putInt((int) this.O).putInt((int) this.P).array();
        pq7.b(array, "ByteBuffer.allocate(12)\n\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.dv, com.fossil.fs
    public JSONObject z() {
        return g80.k(g80.k(g80.k(super.z(), jd0.c1, Long.valueOf(this.N)), jd0.d1, Long.valueOf(this.O)), jd0.e1, Long.valueOf(this.P));
    }
}
