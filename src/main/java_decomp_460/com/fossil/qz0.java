package com.fossil;

import android.view.View;
import android.view.WindowId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qz0 implements rz0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ WindowId f3051a;

    @DexIgnore
    public qz0(View view) {
        this.f3051a = view.getWindowId();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof qz0) && ((qz0) obj).f3051a.equals(this.f3051a);
    }

    @DexIgnore
    public int hashCode() {
        return this.f3051a.hashCode();
    }
}
