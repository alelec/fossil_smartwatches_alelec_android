package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q65 extends p65 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d S; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray T;
    @DexIgnore
    public long R;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        T = sparseIntArray;
        sparseIntArray.put(2131362117, 1);
        T.put(2131362666, 2);
        T.put(2131363410, 3);
        T.put(2131362089, 4);
        T.put(2131362667, 5);
        T.put(2131362402, 6);
        T.put(2131362788, 7);
        T.put(2131362401, 8);
        T.put(2131362783, 9);
        T.put(2131362399, 10);
        T.put(2131362397, 11);
        T.put(2131362735, 12);
        T.put(2131362161, 13);
        T.put(2131362051, 14);
        T.put(2131362091, 15);
        T.put(2131362925, 16);
        T.put(2131362505, 17);
        T.put(2131362445, 18);
        T.put(2131362189, 19);
        T.put(2131363455, 20);
        T.put(2131362097, 21);
        T.put(2131362098, 22);
        T.put(2131362779, 23);
        T.put(2131361881, 24);
        T.put(2131363372, 25);
        T.put(2131363051, 26);
    }
    */

    @DexIgnore
    public q65(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 27, S, T));
    }

    @DexIgnore
    public q65(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[24], (ConstraintLayout) objArr[14], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[15], (ConstraintLayout) objArr[21], (ConstraintLayout) objArr[22], (ConstraintLayout) objArr[1], (CoordinatorLayout) objArr[13], (OverviewDayGoalChart) objArr[19], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[18], (FlexibleTextView) objArr[17], (RTLImageView) objArr[2], (RTLImageView) objArr[5], (RTLImageView) objArr[12], (ConstraintLayout) objArr[23], (View) objArr[9], (View) objArr[7], (FlexibleProgressBar) objArr[16], (ConstraintLayout) objArr[0], (RecyclerViewEmptySupport) objArr[26], (FlexibleTextView) objArr[25], (FlexibleTextView) objArr[3], (View) objArr[20]);
        this.R = -1;
        this.M.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.R = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.R != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.R = 1;
        }
        w();
    }
}
