package com.fossil;

import com.fossil.p72;
import com.google.android.gms.location.LocationAvailability;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class br2 implements p72.b<fa3> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ LocationAvailability f487a;

    @DexIgnore
    public br2(zq2 zq2, LocationAvailability locationAvailability) {
        this.f487a = locationAvailability;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.p72.b
    public final /* synthetic */ void a(fa3 fa3) {
        fa3.onLocationAvailability(this.f487a);
    }

    @DexIgnore
    @Override // com.fossil.p72.b
    public final void b() {
    }
}
