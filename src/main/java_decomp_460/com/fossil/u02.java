package com.fossil;

import com.fossil.p02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class u02 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract u02 a();

        @DexIgnore
        public abstract a b(Iterable<c02> iterable);

        @DexIgnore
        public abstract a c(byte[] bArr);
    }

    @DexIgnore
    public static a a() {
        return new p02.b();
    }

    @DexIgnore
    public abstract Iterable<c02> b();

    @DexIgnore
    public abstract byte[] c();
}
