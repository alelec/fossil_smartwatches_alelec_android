package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u73 implements xw2<t73> {
    @DexIgnore
    public static u73 c; // = new u73();
    @DexIgnore
    public /* final */ xw2<t73> b;

    @DexIgnore
    public u73() {
        this(ww2.b(new w73()));
    }

    @DexIgnore
    public u73(xw2<t73> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((t73) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((t73) c.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((t73) c.zza()).zzc();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ t73 zza() {
        return this.b.zza();
    }
}
