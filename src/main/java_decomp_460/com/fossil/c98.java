package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c98 implements e88<w18, Float> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ c98 f586a; // = new c98();

    @DexIgnore
    /* renamed from: b */
    public Float a(w18 w18) throws IOException {
        return Float.valueOf(w18.string());
    }
}
