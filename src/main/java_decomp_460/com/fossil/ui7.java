package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ui7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;
    @DexIgnore
    public /* final */ /* synthetic */ jg7 c;

    @DexIgnore
    public ui7(Context context, jg7 jg7) {
        this.b = context;
        this.c = jg7;
    }

    @DexIgnore
    public final void run() {
        Context context = this.b;
        if (context == null) {
            ig7.m.f("The Context of StatService.onPause() can not be null!");
        } else {
            ig7.J(context, ei7.D(context), this.c);
        }
    }
}
