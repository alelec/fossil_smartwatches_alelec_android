package com.fossil;

import com.google.maps.model.TravelMode;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX WARN: Init of enum CAR can be incorrect */
public enum mh5 {
    CAR(r0, r1);
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public /* final */ TravelMode travelMode;
    @DexIgnore
    public /* final */ String type;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final TravelMode a(String str) {
            mh5 mh5;
            TravelMode travelMode;
            pq7.c(str, "type");
            mh5[] values = mh5.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    mh5 = null;
                    break;
                }
                mh5 = values[i];
                if (pq7.a(mh5.getType(), str)) {
                    break;
                }
                i++;
            }
            return (mh5 == null || (travelMode = mh5.getTravelMode()) == null) ? mh5.CAR.getTravelMode() : travelMode;
        }
    }

    /*
    static {
        TravelMode travelMode2 = TravelMode.DRIVING;
        pq7.b(um5.c(PortfolioApp.h0.c(), 2131887318), "LanguageHelper.getString\u2026nstance, R.string.by_car)");
    }
    */

    @DexIgnore
    public mh5(TravelMode travelMode2, String str) {
        this.travelMode = travelMode2;
        this.type = str;
    }

    @DexIgnore
    public static final TravelMode getTravelModeFromType(String str) {
        return Companion.a(str);
    }

    @DexIgnore
    public final TravelMode getTravelMode() {
        return this.travelMode;
    }

    @DexIgnore
    public final String getType() {
        return this.type;
    }
}
