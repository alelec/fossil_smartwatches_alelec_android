package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.j32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class u22 implements j32.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ long f3506a;

    @DexIgnore
    public u22(long j) {
        this.f3506a = j;
    }

    @DexIgnore
    public static j32.b a(long j) {
        return new u22(j);
    }

    @DexIgnore
    @Override // com.fossil.j32.b
    public Object apply(Object obj) {
        return Integer.valueOf(((SQLiteDatabase) obj).delete("events", "timestamp_ms < ?", new String[]{String.valueOf(this.f3506a)}));
    }
}
