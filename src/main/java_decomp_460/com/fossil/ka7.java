package com.fossil;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.fossil.zu0;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ka7 extends iv0<b, d> {
    @DexIgnore
    public static /* final */ String b;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ a f1892a;

    @DexIgnore
    public interface a {
        @DexIgnore
        void i6(String str);

        @DexIgnore
        void s3(List<b> list);

        @DexIgnore
        void u6(String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ DianaWatchFaceUser f1893a;
        @DexIgnore
        public /* final */ List<s87> b;
        @DexIgnore
        public /* final */ Bitmap c;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends com.fossil.s87> */
        /* JADX WARN: Multi-variable type inference failed */
        public b(DianaWatchFaceUser dianaWatchFaceUser, List<? extends s87> list, Bitmap bitmap) {
            pq7.c(dianaWatchFaceUser, "watchFace");
            pq7.c(list, MessengerShareContentUtility.ELEMENTS);
            this.f1893a = dianaWatchFaceUser;
            this.b = list;
            this.c = bitmap;
        }

        @DexIgnore
        public final Bitmap a() {
            return this.c;
        }

        @DexIgnore
        public final List<s87> b() {
            return this.b;
        }

        @DexIgnore
        public final DianaWatchFaceUser c() {
            return this.f1893a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (!pq7.a(this.f1893a, bVar.f1893a) || !pq7.a(this.b, bVar.b) || !pq7.a(this.c, bVar.c)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            DianaWatchFaceUser dianaWatchFaceUser = this.f1893a;
            int hashCode = dianaWatchFaceUser != null ? dianaWatchFaceUser.hashCode() : 0;
            List<s87> list = this.b;
            int hashCode2 = list != null ? list.hashCode() : 0;
            Bitmap bitmap = this.c;
            if (bitmap != null) {
                i = bitmap.hashCode();
            }
            return (((hashCode * 31) + hashCode2) * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "ViewHolderModel(watchFace=" + this.f1893a + ", elements=" + this.b + ", background=" + this.c + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends zu0.d<b> {
        @DexIgnore
        /* renamed from: a */
        public boolean areContentsTheSame(b bVar, b bVar2) {
            pq7.c(bVar, "oldItem");
            pq7.c(bVar2, "newItem");
            return pq7.a(bVar.c(), bVar2.c());
        }

        @DexIgnore
        /* renamed from: b */
        public boolean areItemsTheSame(b bVar, b bVar2) {
            pq7.c(bVar, "oldItem");
            pq7.c(bVar2, "newItem");
            return pq7.a(bVar.c().getId(), bVar2.c().getId());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ke5 f1894a;
        @DexIgnore
        public /* final */ /* synthetic */ ka7 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d b;
            @DexIgnore
            public /* final */ /* synthetic */ DianaWatchFaceUser c;

            @DexIgnore
            public a(d dVar, DianaWatchFaceUser dianaWatchFaceUser) {
                this.b = dVar;
                this.c = dianaWatchFaceUser;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.b.f1892a.i6(this.c.getId());
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d b;
            @DexIgnore
            public /* final */ /* synthetic */ DianaWatchFaceUser c;

            @DexIgnore
            public b(d dVar, DianaWatchFaceUser dianaWatchFaceUser) {
                this.b = dVar;
                this.c = dianaWatchFaceUser;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.b.f1892a.u6(this.c.getId());
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ka7 ka7, ke5 ke5) {
            super(ke5.b());
            pq7.c(ke5, "binding");
            this.b = ka7;
            this.f1894a = ke5;
        }

        @DexIgnore
        public final void a(b bVar) {
            pq7.c(bVar, "viewHolderModel");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = ka7.b;
            local.d(str, "bind viewHolderModel=" + bVar);
            DianaWatchFaceUser c = bVar.c();
            FlexibleTextView flexibleTextView = this.f1894a.e;
            pq7.b(flexibleTextView, "binding.tvTitle");
            flexibleTextView.setText(c.getName());
            this.f1894a.b.setOnClickListener(new a(this, c));
            this.f1894a.c.setOnClickListener(new b(this, c));
            if (c.getPreviewURL().length() > 0) {
                this.f1894a.g.P(c.getPreviewURL());
            } else {
                this.f1894a.g.Q(bVar.b(), bVar.a());
            }
        }
    }

    /*
    static {
        String simpleName = ka7.class.getSimpleName();
        pq7.b(simpleName, "WatchFaceListAdapter::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ka7(a aVar) {
        super(new c());
        pq7.c(aVar, "mEvent");
        this.f1892a = aVar;
    }

    @DexIgnore
    /* renamed from: i */
    public void onBindViewHolder(d dVar, int i) {
        pq7.c(dVar, "holder");
        Object item = getItem(i);
        pq7.b(item, "getItem(position)");
        dVar.a((b) item);
    }

    @DexIgnore
    /* renamed from: j */
    public d onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        ke5 c2 = ke5.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(c2, "ItemFacesBinding.inflate\u2026.context), parent, false)");
        return new d(this, c2);
    }

    @DexIgnore
    public final void k(List<b> list) {
        pq7.c(list, "updatedList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "setData updatedList.size=" + list.size());
        submitList(list);
    }

    @DexIgnore
    @Override // com.fossil.iv0
    public void onCurrentListChanged(List<b> list, List<b> list2) {
        pq7.c(list, "previousList");
        pq7.c(list2, "currentList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "onCurrentListChanged previous size: " + list.size() + " current size: " + list2.size());
        this.f1892a.s3(list2);
    }
}
