package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.field.types.BooleanCharType;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ww1 extends xw1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ww1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public ww1 a(Parcel parcel) {
            return new ww1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ww1 createFromParcel(Parcel parcel) {
            return new ww1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ww1[] newArray(int i) {
            return new ww1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ww1(Parcel parcel, kq7 kq7) {
        super(yw1.values()[parcel.readInt()]);
        String[] createStringArray = parcel.createStringArray();
        if (createStringArray != null) {
            this.c = createStringArray;
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public ww1(String[] strArr) throws IllegalArgumentException {
        super(yw1.COMMUTE);
        this.c = strArr;
        if (strArr.length > 10) {
            StringBuilder e = e.e("Destinations(");
            e.append(this.c);
            e.append(") must be less than or equal to ");
            e.append(BooleanCharType.DEFAULT_TRUE_FALSE_FORMAT);
            throw new IllegalArgumentException(e.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.xw1
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.xw1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(ww1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.c, ((ww1) obj).c);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.CommuteTimeWatchAppDataConfig");
    }

    @DexIgnore
    public final String[] getDestinations() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.xw1
    public int hashCode() {
        return (super.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put("commuteApp._.config.destinations", ay1.b(this.c));
        pq7.b(put, "JSONObject()\n           \u2026stinations.toJSONArray())");
        return put;
    }

    @DexIgnore
    @Override // com.fossil.xw1
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeStringArray(this.c);
        }
    }
}
