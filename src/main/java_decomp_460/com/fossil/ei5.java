package com.fossil;

import com.fossil.fitness.CadenceUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ei5 {
    UNKNOWN("unknown"),
    SPM("spm"),
    RPM("rpm");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ei5 a(Integer num) {
            int ordinal = CadenceUnit.SPM.ordinal();
            if (num != null && num.intValue() == ordinal) {
                return ei5.SPM;
            }
            return (num != null && num.intValue() == CadenceUnit.RPM.ordinal()) ? ei5.RPM : ei5.UNKNOWN;
        }
    }

    @DexIgnore
    public ei5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        pq7.c(str, "<set-?>");
        this.mValue = str;
    }
}
