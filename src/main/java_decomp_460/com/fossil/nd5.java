package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nd5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ View t;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat u;

    @DexIgnore
    public nd5(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView, ImageView imageView, View view2, FlexibleSwitchCompat flexibleSwitchCompat) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleTextView;
        this.s = imageView;
        this.t = view2;
        this.u = flexibleSwitchCompat;
    }

    @DexIgnore
    @Deprecated
    public static nd5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (nd5) ViewDataBinding.p(layoutInflater, 2131558660, viewGroup, z, obj);
    }

    @DexIgnore
    public static nd5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, aq0.d());
    }
}
