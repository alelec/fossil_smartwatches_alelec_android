package com.fossil;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ot0 implements mt0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2719a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public ot0(String str, int i, int i2) {
        this.f2719a = str;
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ot0)) {
            return false;
        }
        ot0 ot0 = (ot0) obj;
        return TextUtils.equals(this.f2719a, ot0.f2719a) && this.b == ot0.b && this.c == ot0.c;
    }

    @DexIgnore
    public int hashCode() {
        return kn0.b(this.f2719a, Integer.valueOf(this.b), Integer.valueOf(this.c));
    }
}
