package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class no2 extends tn2 implements mo2 {
    @DexIgnore
    public no2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IStatusCallback");
    }

    @DexIgnore
    @Override // com.fossil.mo2
    public final void m0(Status status) throws RemoteException {
        Parcel d = d();
        qo2.b(d, status);
        i(1, d);
    }
}
