package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.IOException;
import java.io.Reader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class l54 {
    @DexIgnore
    public abstract Reader a() throws IOException;

    @DexIgnore
    @CanIgnoreReturnValue
    public <T> T b(r54<T> r54) throws IOException {
        i14.l(r54);
        o54 a2 = o54.a();
        try {
            Reader a3 = a();
            a2.b(a3);
            T t = (T) m54.b(a3, r54);
            a2.close();
            return t;
        } catch (Throwable th) {
            a2.close();
            throw th;
        }
    }
}
