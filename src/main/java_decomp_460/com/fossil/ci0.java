package com.fossil;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ci0 extends di0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f617a; // = new Object();
    @DexIgnore
    public /* final */ ExecutorService b; // = Executors.newFixedThreadPool(4, new a(this));
    @DexIgnore
    public volatile Handler c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ThreadFactory {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ AtomicInteger f618a; // = new AtomicInteger(0);

        @DexIgnore
        public a(ci0 ci0) {
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setName(String.format("arch_disk_io_%d", Integer.valueOf(this.f618a.getAndIncrement())));
            return thread;
        }
    }

    @DexIgnore
    public static Handler e(Looper looper) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            return Handler.createAsync(looper);
        }
        if (i >= 16) {
            try {
                return (Handler) Handler.class.getDeclaredConstructor(Looper.class, Handler.Callback.class, Boolean.TYPE).newInstance(looper, null, Boolean.TRUE);
            } catch (IllegalAccessException | InstantiationException | NoSuchMethodException e) {
            } catch (InvocationTargetException e2) {
                return new Handler(looper);
            }
        }
        return new Handler(looper);
    }

    @DexIgnore
    @Override // com.fossil.di0
    public void a(Runnable runnable) {
        this.b.execute(runnable);
    }

    @DexIgnore
    @Override // com.fossil.di0
    public boolean c() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    @DexIgnore
    @Override // com.fossil.di0
    public void d(Runnable runnable) {
        if (this.c == null) {
            synchronized (this.f617a) {
                if (this.c == null) {
                    this.c = e(Looper.getMainLooper());
                }
            }
        }
        this.c.post(runnable);
    }
}
