package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jm4 implements nm4 {
    @DexIgnore
    public static String d(CharSequence charSequence, int i) {
        int charAt = (charSequence.charAt(i) * '\u0640') + (charSequence.charAt(i + 1) * '(') + charSequence.charAt(i + 2) + 1;
        return new String(new char[]{(char) (charAt / 256), (char) (charAt % 256)});
    }

    @DexIgnore
    public static void g(om4 om4, StringBuilder sb) {
        om4.s(d(sb, 0));
        sb.delete(0, 3);
    }

    @DexIgnore
    @Override // com.fossil.nm4
    public void a(om4 om4) {
        int n;
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!om4.i()) {
                break;
            }
            char c = om4.c();
            om4.f++;
            int c2 = c(c, sb);
            int length = ((sb.length() / 3) << 1) + om4.a();
            om4.q(length);
            int a2 = om4.g().a() - length;
            if (om4.i()) {
                if (sb.length() % 3 == 0 && (n = qm4.n(om4.d(), om4.f, e())) != e()) {
                    om4.o(n);
                    break;
                }
            } else {
                StringBuilder sb2 = new StringBuilder();
                if (sb.length() % 3 == 2 && (a2 < 2 || a2 > 2)) {
                    c2 = b(om4, sb, sb2, c2);
                }
                while (sb.length() % 3 == 1 && ((c2 <= 3 && a2 != 1) || c2 > 3)) {
                    c2 = b(om4, sb, sb2, c2);
                }
            }
        }
        f(om4, sb);
    }

    @DexIgnore
    public final int b(om4 om4, StringBuilder sb, StringBuilder sb2, int i) {
        int length = sb.length();
        sb.delete(length - i, length);
        om4.f--;
        int c = c(om4.c(), sb2);
        om4.k();
        return c;
    }

    @DexIgnore
    public int c(char c, StringBuilder sb) {
        if (c == ' ') {
            sb.append((char) 3);
            return 1;
        } else if (c >= '0' && c <= '9') {
            sb.append((char) ((c - '0') + 4));
            return 1;
        } else if (c >= 'A' && c <= 'Z') {
            sb.append((char) ((c - 'A') + 14));
            return 1;
        } else if (c >= 0 && c <= 31) {
            sb.append((char) 0);
            sb.append(c);
            return 2;
        } else if (c >= '!' && c <= '/') {
            sb.append((char) 1);
            sb.append((char) (c - '!'));
            return 2;
        } else if (c >= ':' && c <= '@') {
            sb.append((char) 1);
            sb.append((char) ((c - ':') + 15));
            return 2;
        } else if (c >= '[' && c <= '_') {
            sb.append((char) 1);
            sb.append((char) ((c - '[') + 22));
            return 2;
        } else if (c >= '`' && c <= '\u007f') {
            sb.append((char) 2);
            sb.append((char) (c - '`'));
            return 2;
        } else if (c >= '\u0080') {
            sb.append("\u0001\u001e");
            return c((char) (c - '\u0080'), sb) + 2;
        } else {
            throw new IllegalArgumentException("Illegal character: " + c);
        }
    }

    @DexIgnore
    public int e() {
        return 1;
    }

    @DexIgnore
    public void f(om4 om4, StringBuilder sb) {
        int length = sb.length() % 3;
        int length2 = ((sb.length() / 3) << 1) + om4.a();
        om4.q(length2);
        int a2 = om4.g().a() - length2;
        if (length == 2) {
            sb.append((char) 0);
            while (sb.length() >= 3) {
                g(om4, sb);
            }
            if (om4.i()) {
                om4.r('\u00fe');
            }
        } else if (a2 == 1 && length == 1) {
            while (sb.length() >= 3) {
                g(om4, sb);
            }
            if (om4.i()) {
                om4.r('\u00fe');
            }
            om4.f--;
        } else if (length == 0) {
            while (sb.length() >= 3) {
                g(om4, sb);
            }
            if (a2 > 0 || om4.i()) {
                om4.r('\u00fe');
            }
        } else {
            throw new IllegalStateException("Unexpected case. Please report!");
        }
        om4.o(0);
    }
}
