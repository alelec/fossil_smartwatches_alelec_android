package com.fossil;

import android.content.Context;
import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vg2<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3765a;
    @DexIgnore
    public T b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Exception {
        @DexIgnore
        public a(String str) {
            super(str);
        }

        @DexIgnore
        public a(String str, Throwable th) {
            super(str, th);
        }
    }

    @DexIgnore
    public vg2(String str) {
        this.f3765a = str;
    }

    @DexIgnore
    public abstract T a(IBinder iBinder);

    @DexIgnore
    public final T b(Context context) throws a {
        if (this.b == null) {
            rc2.k(context);
            Context d = h62.d(context);
            if (d != null) {
                try {
                    this.b = a((IBinder) d.getClassLoader().loadClass(this.f3765a).newInstance());
                } catch (ClassNotFoundException e) {
                    throw new a("Could not load creator class.", e);
                } catch (InstantiationException e2) {
                    throw new a("Could not instantiate creator.", e2);
                } catch (IllegalAccessException e3) {
                    throw new a("Could not access creator.", e3);
                }
            } else {
                throw new a("Could not get remote context.");
            }
        }
        return this.b;
    }
}
