package com.fossil;

import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface h01 {
    @DexIgnore
    Parcelable c();

    @DexIgnore
    void f(Parcelable parcelable);
}
