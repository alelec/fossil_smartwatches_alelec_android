package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class kc3 extends ds2 implements jc3 {
    @DexIgnore
    public kc3() {
        super("com.google.android.gms.maps.internal.IOnMapClickListener");
    }

    @DexIgnore
    @Override // com.fossil.ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        onMapClick((LatLng) es2.b(parcel, LatLng.CREATOR));
        parcel2.writeNoException();
        return true;
    }
}
