package com.fossil;

import android.database.Cursor;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v77 implements u77 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f3729a;
    @DexIgnore
    public /* final */ jw0<p77> b;
    @DexIgnore
    public /* final */ o77 c; // = new o77();
    @DexIgnore
    public /* final */ xw0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jw0<p77> {
        @DexIgnore
        public a(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, p77 p77) {
            if (p77.d() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, p77.d());
            }
            if (p77.b() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, p77.b());
            }
            if (p77.f() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, p77.f());
            }
            String b = v77.this.c.b(p77.c());
            if (b == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, b);
            }
            px0.bindLong(5, p77.g() ? 1 : 0);
            String f = v77.this.c.f(p77.e());
            if (f == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, f);
            }
            px0.bindLong(7, (long) v77.this.c.a(p77.a()));
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `wf_asset` (`id`,`category`,`name`,`data`,`isNew`,`meta`,`assetType`) VALUES (?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends xw0 {
        @DexIgnore
        public b(v77 v77, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE  FROM wf_asset WHERE assetType = ?";
        }
    }

    @DexIgnore
    public v77(qw0 qw0) {
        this.f3729a = qw0;
        this.b = new a(qw0);
        this.d = new b(this, qw0);
    }

    @DexIgnore
    @Override // com.fossil.u77
    public void a(l77 l77) {
        this.f3729a.assertNotSuspendingTransaction();
        px0 acquire = this.d.acquire();
        acquire.bindLong(1, (long) this.c.a(l77));
        this.f3729a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f3729a.setTransactionSuccessful();
        } finally {
            this.f3729a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.u77
    public List<p77> b(l77 l77) {
        tw0 f = tw0.f("SELECT * FROM wf_asset WHERE assetType = ?", 1);
        f.bindLong(1, (long) this.c.a(l77));
        this.f3729a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3729a, f, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, "category");
            int c4 = dx0.c(b2, "name");
            int c5 = dx0.c(b2, "data");
            int c6 = dx0.c(b2, "isNew");
            int c7 = dx0.c(b2, Constants.META);
            int c8 = dx0.c(b2, "assetType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new p77(b2.getString(c2), b2.getString(c3), b2.getString(c4), this.c.h(b2.getString(c5)), b2.getInt(c6) != 0, this.c.e(b2.getString(c7)), this.c.g(b2.getInt(c8))));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.u77
    public p77 c(l77 l77, String str) {
        p77 p77 = null;
        boolean z = true;
        tw0 f = tw0.f("SELECT * FROM wf_asset WHERE assetType = ? and name =?", 2);
        f.bindLong(1, (long) this.c.a(l77));
        if (str == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, str);
        }
        this.f3729a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3729a, f, false, null);
        try {
            int c2 = dx0.c(b2, "id");
            int c3 = dx0.c(b2, "category");
            int c4 = dx0.c(b2, "name");
            int c5 = dx0.c(b2, "data");
            int c6 = dx0.c(b2, "isNew");
            int c7 = dx0.c(b2, Constants.META);
            int c8 = dx0.c(b2, "assetType");
            if (b2.moveToFirst()) {
                String string = b2.getString(c2);
                String string2 = b2.getString(c3);
                String string3 = b2.getString(c4);
                q77 h = this.c.h(b2.getString(c5));
                if (b2.getInt(c6) == 0) {
                    z = false;
                }
                p77 = new p77(string, string2, string3, h, z, this.c.e(b2.getString(c7)), this.c.g(b2.getInt(c8)));
            }
            return p77;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.u77
    public Long[] insert(List<p77> list) {
        this.f3729a.assertNotSuspendingTransaction();
        this.f3729a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.f3729a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.f3729a.endTransaction();
        }
    }
}
