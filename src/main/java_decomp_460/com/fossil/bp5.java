package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.dl7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.watchface.WatchFacePreviewView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bp5 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<dp5> f475a; // = new ArrayList();
    @DexIgnore
    public /* final */ Map<String, List<s87>> b; // = new LinkedHashMap();
    @DexIgnore
    public /* final */ Bitmap c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ b e;
    @DexIgnore
    public /* final */ cp5 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public dp5 b;
        @DexIgnore
        public /* final */ ie5 c;
        @DexIgnore
        public /* final */ cp5 d;
        @DexIgnore
        public /* final */ /* synthetic */ bp5 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(bp5 bp5, ie5 ie5, cp5 cp5) {
            super(ie5.n());
            pq7.c(ie5, "binding");
            pq7.c(cp5, "listener");
            this.e = bp5;
            this.c = ie5;
            this.d = cp5;
            ie5.K.setOnClickListener(this);
            ie5.J.setOnClickListener(this);
            ie5.I.setOnClickListener(this);
            ie5.u.setOnClickListener(this);
            ie5.F.setOnClickListener(this);
            ie5.q.setOnClickListener(this);
            ie5.E.setOnClickListener(this);
            ie5.v.setOnClickListener(this);
        }

        @DexIgnore
        public final void a(dp5 dp5, List<? extends s87> list) {
            Bitmap bitmap;
            rb7 a2;
            pq7.c(dp5, "uiDianaPreset");
            pq7.c(list, "elementConfigs");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaPresetAdapter", "setData presetName=" + dp5.d());
            this.b = dp5;
            ie5 ie5 = this.c;
            if (!TextUtils.isEmpty(dp5.e()) && !dp5.h()) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DianaPresetAdapter", "preview url=" + dp5.e());
                WatchFacePreviewView watchFacePreviewView = ie5.L;
                String e2 = dp5.e();
                if (e2 != null) {
                    watchFacePreviewView.P(e2);
                    FlexibleTextView flexibleTextView = ie5.w;
                    pq7.b(flexibleTextView, "ftvFailed");
                    flexibleTextView.setVisibility(8);
                    FlexibleButton flexibleButton = ie5.v;
                    pq7.b(flexibleButton, "fbReloadWf");
                    flexibleButton.setVisibility(8);
                } else {
                    pq7.i();
                    throw null;
                }
            } else if (dp5.f() != null) {
                ub7 f = dp5.f();
                if (f == null || (a2 = f.a()) == null || (bitmap = a2.b()) == null) {
                    bitmap = this.e.c;
                }
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DianaPresetAdapter", "preview elementConfigs=" + list + ", backgroundBitmap=" + bitmap);
                ie5.L.Q(list, bitmap);
                FlexibleTextView flexibleTextView2 = ie5.w;
                pq7.b(flexibleTextView2, "ftvFailed");
                flexibleTextView2.setVisibility(8);
                FlexibleButton flexibleButton2 = ie5.v;
                pq7.b(flexibleButton2, "fbReloadWf");
                flexibleButton2.setVisibility(8);
            } else {
                FLogger.INSTANCE.getLocal().d("DianaPresetAdapter", "preview empty");
                ie5.L.U();
                FlexibleTextView flexibleTextView3 = ie5.w;
                pq7.b(flexibleTextView3, "ftvFailed");
                flexibleTextView3.setVisibility(0);
                FlexibleButton flexibleButton3 = ie5.v;
                pq7.b(flexibleButton3, "fbReloadWf");
                flexibleButton3.setVisibility(0);
                FlexibleTextView flexibleTextView4 = ie5.w;
                pq7.b(flexibleTextView4, "ftvFailed");
                FlexibleTextView flexibleTextView5 = ie5.w;
                pq7.b(flexibleTextView5, "ftvFailed");
                flexibleTextView4.setText(um5.c(flexibleTextView5.getContext(), 2131886571));
            }
            FlexibleTextView flexibleTextView6 = ie5.F;
            pq7.b(flexibleTextView6, "tvPresetName");
            flexibleTextView6.setText(dp5.d());
            List<oo5> a3 = dp5.a();
            if (a3 != null) {
                for (oo5 oo5 : a3) {
                    String b2 = oo5.b();
                    int hashCode = b2.hashCode();
                    if (hashCode != -1383228885) {
                        if (hashCode != -1074341483) {
                            if (hashCode == 115029 && b2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                ie5.K.b0(oo5.a());
                                ie5.K.T();
                            }
                        } else if (b2.equals("middle")) {
                            ie5.J.b0(oo5.a());
                            ie5.J.T();
                        }
                    } else if (b2.equals("bottom")) {
                        ie5.I.b0(oo5.a());
                        ie5.I.T();
                    }
                }
            }
            if (dp5.g()) {
                FlexibleButton flexibleButton4 = ie5.q;
                pq7.b(flexibleButton4, "btnSetToWatch");
                flexibleButton4.setText(um5.c(PortfolioApp.h0.c(), 2131886550));
                FlexibleButton flexibleButton5 = ie5.q;
                pq7.b(flexibleButton5, "btnSetToWatch");
                flexibleButton5.setClickable(false);
                ie5.q.d("flexible_button_right_applied");
                if (!TextUtils.isEmpty(this.e.d)) {
                    int parseColor = Color.parseColor(this.e.d);
                    Drawable drawable = PortfolioApp.h0.c().getDrawable(2131231055);
                    if (drawable != null) {
                        drawable.setTint(parseColor);
                        ie5.q.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
            } else {
                FlexibleButton flexibleButton6 = ie5.q;
                pq7.b(flexibleButton6, "btnSetToWatch");
                flexibleButton6.setText(um5.c(PortfolioApp.h0.c(), 2131886542));
                FlexibleButton flexibleButton7 = ie5.q;
                pq7.b(flexibleButton7, "btnSetToWatch");
                flexibleButton7.setClickable(true);
                ie5.q.d("flexible_button_right_apply");
                ie5.q.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
            FlexibleTextView flexibleTextView7 = ie5.E;
            pq7.b(flexibleTextView7, "tvLeft");
            flexibleTextView7.setVisibility(this.e.j().size() > 1 ? 0 : 4);
        }

        @DexIgnore
        public void onClick(View view) {
            dp5 dp5;
            if (getAdapterPosition() != -1 && view != null && (dp5 = this.b) != null) {
                switch (view.getId()) {
                    case 2131361980:
                        this.d.f(dp5.c());
                        return;
                    case 2131362254:
                        cp5 cp5 = this.d;
                        String c2 = dp5.c();
                        dp5 dp52 = this.b;
                        cp5.b(c2, dp52 != null ? dp52.h() : true, view);
                        return;
                    case 2131362283:
                        ie5 ie5 = this.c;
                        FlexibleTextView flexibleTextView = ie5.w;
                        pq7.b(flexibleTextView, "ftvFailed");
                        flexibleTextView.setText(um5.c(view.getContext(), 2131886552));
                        FlexibleButton flexibleButton = ie5.v;
                        pq7.b(flexibleButton, "fbReloadWf");
                        flexibleButton.setVisibility(8);
                        this.d.e(dp5.c(), dp5.b());
                        return;
                    case 2131362528:
                        this.d.f(dp5.c());
                        return;
                    case 2131363348:
                        this.d.a();
                        return;
                    case 2131363376:
                        this.d.N(dp5.d(), dp5.c());
                        return;
                    case 2131363529:
                        this.d.d(dp5.c(), "bottom");
                        return;
                    case 2131363530:
                        this.d.d(dp5.c(), "middle");
                        return;
                    case 2131363531:
                        this.d.d(dp5.c(), ViewHierarchy.DIMENSION_TOP_KEY);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RecyclerView.AdapterDataObserver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ bp5 f476a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(bp5 bp5) {
            this.f476a = bp5;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void a() {
            this.f476a.f.c();
        }
    }

    @DexIgnore
    public bp5(Context context, cp5 cp5) {
        pq7.c(context, "context");
        pq7.c(cp5, "listener");
        this.f = cp5;
        this.c = BitmapFactory.decodeResource(context.getResources(), 2131231268);
        this.d = qn5.l.a().d("onPrimaryButton");
        this.e = new b(this);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f475a.size();
    }

    @DexIgnore
    public final List<dp5> j() {
        return this.f475a;
    }

    @DexIgnore
    /* renamed from: k */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        dp5 dp5 = this.f475a.get(i);
        List<s87> list = this.b.get(dp5.c());
        if (list != null) {
            aVar.a(dp5, list);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: l */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        ie5 z = ie5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemDianaPresetLayoutBin\u2026tInflater, parent, false)");
        return new a(this, z, this.f);
    }

    @DexIgnore
    public final void m(List<dp5> list, Map<String, ? extends List<? extends s87>> map) {
        pq7.c(list, "newUiPresets");
        pq7.c(map, "newConfigPerPreset");
        this.f475a.clear();
        this.b.clear();
        this.f475a.addAll(list);
        this.b.putAll(map);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void n(dp5 dp5, List<? extends s87> list) {
        boolean z = false;
        pq7.c(dp5, "uiDianaPreset");
        Iterator<dp5> it = this.f475a.iterator();
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            } else if (pq7.a(it.next().c(), dp5.c())) {
                break;
            } else {
                i++;
            }
        }
        if (i != -1) {
            if (list == null || list.isEmpty()) {
                z = true;
            }
            if (!z) {
                this.b.put(dp5.c(), list);
            }
            this.f475a.set(i, dp5);
            notifyItemChanged(i);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        pq7.c(recyclerView, "recyclerView");
        try {
            dl7.a aVar = dl7.Companion;
            registerAdapterDataObserver(this.e);
            dl7.m1constructorimpl(tl7.f3441a);
        } catch (Throwable th) {
            dl7.a aVar2 = dl7.Companion;
            dl7.m1constructorimpl(el7.a(th));
        }
        super.onAttachedToRecyclerView(recyclerView);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        pq7.c(recyclerView, "recyclerView");
        try {
            dl7.a aVar = dl7.Companion;
            unregisterAdapterDataObserver(this.e);
            dl7.m1constructorimpl(tl7.f3441a);
        } catch (Throwable th) {
            dl7.a aVar2 = dl7.Companion;
            dl7.m1constructorimpl(el7.a(th));
        }
        super.onDetachedFromRecyclerView(recyclerView);
    }
}
