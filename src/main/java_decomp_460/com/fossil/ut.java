package com.fossil;

import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ut {
    SEND_PHONE_RANDOM_NUMBER(st.SET, tt.SEND_PHONE_RANDOM_NUMBER, null, null, 12),
    SEND_BOTH_SIDES_RANDOM_NUMBERS(st.SET, tt.SEND_BOTH_RANDOM_NUMBER, null, null, 12),
    EXCHANGE_PUBLIC_KEYS(st.SET, tt.EXCHANGE_PUBLIC_KEY, null, null, 12),
    PROCESS_USER_AUTHORIZATION(st.GET, tt.PROCESS_USER_AUTHORIZATION, null, null, 12),
    STOP_PROCESS(st.SET, tt.STOP_PROCESS, null, null, 12);
    
    @DexIgnore
    public /* final */ st b;
    @DexIgnore
    public /* final */ tt c;
    @DexIgnore
    public /* final */ st d;
    @DexIgnore
    public /* final */ tt e;

    @DexIgnore
    public /* synthetic */ ut(st stVar, tt ttVar, st stVar2, tt ttVar2, int i) {
        stVar2 = (i & 4) != 0 ? st.RESPONSE : stVar2;
        ttVar2 = (i & 8) != 0 ? ttVar : ttVar2;
        this.b = stVar;
        this.c = ttVar;
        this.d = stVar2;
        this.e = ttVar2;
    }

    @DexIgnore
    public final byte[] a() {
        byte[] array = ByteBuffer.allocate(2).put(this.b.b).put(this.c.b).array();
        pq7.b(array, "ByteBuffer.allocate(2)\n \u2026\n                .array()");
        return array;
    }
}
