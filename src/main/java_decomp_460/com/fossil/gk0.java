package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gk0 {
    @DexIgnore
    public static /* final */ int bottom; // = 2131361919;
    @DexIgnore
    public static /* final */ int end; // = 2131362225;
    @DexIgnore
    public static /* final */ int gone; // = 2131362568;
    @DexIgnore
    public static /* final */ int invisible; // = 2131362648;
    @DexIgnore
    public static /* final */ int left; // = 2131362781;
    @DexIgnore
    public static /* final */ int packed; // = 2131362914;
    @DexIgnore
    public static /* final */ int parent; // = 2131362918;
    @DexIgnore
    public static /* final */ int percent; // = 2131362929;
    @DexIgnore
    public static /* final */ int right; // = 2131362996;
    @DexIgnore
    public static /* final */ int spread; // = 2131363137;
    @DexIgnore
    public static /* final */ int spread_inside; // = 2131363138;
    @DexIgnore
    public static /* final */ int start; // = 2131363146;
    @DexIgnore
    public static /* final */ int top; // = 2131363225;
    @DexIgnore
    public static /* final */ int wrap; // = 2131363559;
}
