package com.fossil;

import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.util.ArrayList;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface cl6 extends gq4<bl6> {
    @DexIgnore
    void J(ai5 ai5, ActivitySummary activitySummary);

    @DexIgnore
    void j(Date date, boolean z, boolean z2, boolean z3);

    @DexIgnore
    void n(mv5 mv5, ArrayList<String> arrayList);

    @DexIgnore
    void s(boolean z, ai5 ai5, cu0<WorkoutSession> cu0);
}
