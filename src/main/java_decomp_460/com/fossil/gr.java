package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gr extends lp {
    @DexIgnore
    public int C;
    @DexIgnore
    public se D;
    @DexIgnore
    public /* final */ ArrayList<ow> E; // = by1.a(this.i, hm7.c(ow.DEVICE_CONFIG));
    @DexIgnore
    public /* final */ ve[] F;

    @DexIgnore
    public gr(k5 k5Var, i60 i60, String str, ve[] veVarArr) {
        super(k5Var, i60, yp.T, str, false, 16);
        this.F = veVarArr;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        I();
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        JSONObject E2 = super.E();
        jd0 jd0 = jd0.n4;
        se seVar = this.D;
        return g80.k(E2, jd0, seVar != null ? seVar.toJSONObject() : null);
    }

    @DexIgnore
    public final void I() {
        int i = this.C;
        ve[] veVarArr = this.F;
        if (i < veVarArr.length) {
            lp.i(this, new xu(veVarArr[i], this.w), new eq(this), new sq(this), null, null, null, 56, null);
        } else {
            l(nr.a(this.v, null, zq.EXCHANGED_VALUE_NOT_SATISFIED, null, null, 13));
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        se seVar = this.D;
        return seVar != null ? seVar : new se(0, 0, 0);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.E;
    }
}
