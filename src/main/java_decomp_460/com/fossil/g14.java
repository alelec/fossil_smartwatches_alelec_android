package com.fossil;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class g14<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterable<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.g14$a$a")
        /* renamed from: com.fossil.g14$a$a  reason: collision with other inner class name */
        public class C0094a extends x04<T> {
            @DexIgnore
            public /* final */ Iterator<? extends g14<? extends T>> d;

            @DexIgnore
            public C0094a() {
                Iterator<T> it = a.this.b.iterator();
                i14.l(it);
                this.d = it;
            }

            @DexIgnore
            @Override // com.fossil.x04
            public T a() {
                while (this.d.hasNext()) {
                    g14 g14 = (g14) this.d.next();
                    if (g14.isPresent()) {
                        return (T) g14.get();
                    }
                }
                return (T) b();
            }
        }

        @DexIgnore
        public a(Iterable iterable) {
            this.b = iterable;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return new C0094a();
        }
    }

    @DexIgnore
    public static <T> g14<T> absent() {
        return w04.withType();
    }

    @DexIgnore
    public static <T> g14<T> fromNullable(T t) {
        return t == null ? absent() : new l14(t);
    }

    @DexIgnore
    public static <T> g14<T> of(T t) {
        i14.l(t);
        return new l14(t);
    }

    @DexIgnore
    public static <T> Iterable<T> presentInstances(Iterable<? extends g14<? extends T>> iterable) {
        i14.l(iterable);
        return new a(iterable);
    }

    @DexIgnore
    public abstract Set<T> asSet();

    @DexIgnore
    public abstract boolean equals(Object obj);

    @DexIgnore
    public abstract T get();

    @DexIgnore
    public abstract int hashCode();

    @DexIgnore
    public abstract boolean isPresent();

    @DexIgnore
    public abstract g14<T> or(g14<? extends T> g14);

    @DexIgnore
    public abstract T or(m14<? extends T> m14);

    @DexIgnore
    public abstract T or(T t);

    @DexIgnore
    public abstract T orNull();

    @DexIgnore
    public abstract String toString();

    @DexIgnore
    public abstract <V> g14<V> transform(b14<? super T, V> b14);
}
