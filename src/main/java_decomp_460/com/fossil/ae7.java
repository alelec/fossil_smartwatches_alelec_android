package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.facebook.GraphRequest;
import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ae7 implements FlutterPlugin, MethodChannel.MethodCallHandler {
    @DexIgnore
    public static /* final */ Map<String, Integer> d; // = new HashMap();
    @DexIgnore
    public static boolean e; // = false;
    @DexIgnore
    public static int f; // = 10;
    @DexIgnore
    public static int g; // = 0;
    @DexIgnore
    public static /* final */ Object h; // = new Object();
    @DexIgnore
    public static /* final */ Object i; // = new Object();
    @DexIgnore
    public static String j;
    @DexIgnore
    public static int k; // = 0;
    @DexIgnore
    public static HandlerThread l;
    @DexIgnore
    public static Handler m;
    @DexIgnore
    @SuppressLint({"UseSparseArrays"})
    public static /* final */ Map<Integer, yd7> s; // = new HashMap();
    @DexIgnore
    public Context b;
    @DexIgnore
    public MethodChannel c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall b;
        @DexIgnore
        public /* final */ /* synthetic */ i c;
        @DexIgnore
        public /* final */ /* synthetic */ yd7 d;

        @DexIgnore
        public a(MethodCall methodCall, i iVar, yd7 yd7) {
            this.b = methodCall;
            this.c = iVar;
            this.d = yd7;
        }

        @DexIgnore
        public void run() {
            ae7.this.M(this.d, new he7(this.b, this.c));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall b;
        @DexIgnore
        public /* final */ /* synthetic */ i c;
        @DexIgnore
        public /* final */ /* synthetic */ yd7 d;

        @DexIgnore
        public b(MethodCall methodCall, i iVar, yd7 yd7) {
            this.b = methodCall;
            this.c = iVar;
            this.d = yd7;
        }

        @DexIgnore
        public void run() {
            he7 he7 = new he7(this.b, this.c);
            boolean c2 = he7.c();
            boolean f = he7.f();
            ArrayList arrayList = new ArrayList();
            for (Map map : (List) this.b.argument("operations")) {
                fe7 fe7 = new fe7(map, c2);
                String j = fe7.j();
                char c3 = '\uffff';
                switch (j.hashCode()) {
                    case -1319569547:
                        if (j.equals("execute")) {
                            c3 = 0;
                            break;
                        }
                        break;
                    case -1183792455:
                        if (j.equals("insert")) {
                            c3 = 1;
                            break;
                        }
                        break;
                    case -838846263:
                        if (j.equals("update")) {
                            c3 = 3;
                            break;
                        }
                        break;
                    case 107944136:
                        if (j.equals(ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME)) {
                            c3 = 2;
                            break;
                        }
                        break;
                }
                if (c3 != 0) {
                    if (c3 != 1) {
                        if (c3 != 2) {
                            if (c3 != 3) {
                                this.c.error("bad_param", "Batch method '" + j + "' not supported", null);
                                return;
                            } else if (ae7.this.O(this.d, fe7)) {
                                fe7.o(arrayList);
                            } else if (f) {
                                fe7.n(arrayList);
                            } else {
                                fe7.m(this.c);
                                return;
                            }
                        } else if (ae7.this.M(this.d, fe7)) {
                            fe7.o(arrayList);
                        } else if (f) {
                            fe7.n(arrayList);
                        } else {
                            fe7.m(this.c);
                            return;
                        }
                    } else if (ae7.this.w(this.d, fe7)) {
                        fe7.o(arrayList);
                    } else if (f) {
                        fe7.n(arrayList);
                    } else {
                        fe7.m(this.c);
                        return;
                    }
                } else if (ae7.this.o(this.d, fe7)) {
                    fe7.o(arrayList);
                } else if (f) {
                    fe7.n(arrayList);
                } else {
                    fe7.m(this.c);
                    return;
                }
            }
            if (c2) {
                this.c.success(null);
            } else {
                this.c.success(arrayList);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall b;
        @DexIgnore
        public /* final */ /* synthetic */ i c;
        @DexIgnore
        public /* final */ /* synthetic */ yd7 d;

        @DexIgnore
        public c(MethodCall methodCall, i iVar, yd7 yd7) {
            this.b = methodCall;
            this.c = iVar;
            this.d = yd7;
        }

        @DexIgnore
        public void run() {
            ae7.this.w(this.d, new he7(this.b, this.c));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ yd7 b;
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall c;
        @DexIgnore
        public /* final */ /* synthetic */ i d;

        @DexIgnore
        public d(yd7 yd7, MethodCall methodCall, i iVar) {
            this.b = yd7;
            this.c = methodCall;
            this.d = iVar;
        }

        @DexIgnore
        public void run() {
            if (ae7.this.p(this.b, this.c, this.d) != null) {
                this.d.success(null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall b;
        @DexIgnore
        public /* final */ /* synthetic */ i c;
        @DexIgnore
        public /* final */ /* synthetic */ yd7 d;

        @DexIgnore
        public e(MethodCall methodCall, i iVar, yd7 yd7) {
            this.b = methodCall;
            this.c = iVar;
            this.d = yd7;
        }

        @DexIgnore
        public void run() {
            ae7.this.O(this.d, new he7(this.b, this.c));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ i d;
        @DexIgnore
        public /* final */ /* synthetic */ Boolean e;
        @DexIgnore
        public /* final */ /* synthetic */ yd7 f;
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall g;
        @DexIgnore
        public /* final */ /* synthetic */ boolean h;
        @DexIgnore
        public /* final */ /* synthetic */ int i;

        @DexIgnore
        public f(boolean z, String str, i iVar, Boolean bool, yd7 yd7, MethodCall methodCall, boolean z2, int i2) {
            this.b = z;
            this.c = str;
            this.d = iVar;
            this.e = bool;
            this.f = yd7;
            this.g = methodCall;
            this.h = z2;
            this.i = i2;
        }

        @DexIgnore
        public void run() {
            synchronized (ae7.i) {
                if (!this.b) {
                    File file = new File(new File(this.c).getParent());
                    if (!file.exists() && !file.mkdirs() && !file.exists()) {
                        i iVar = this.d;
                        iVar.error("sqlite_error", "open_failed " + this.c, null);
                        return;
                    }
                }
                try {
                    if (Boolean.TRUE.equals(this.e)) {
                        this.f.h();
                    } else {
                        this.f.g();
                    }
                    synchronized (ae7.h) {
                        if (this.h) {
                            ae7.d.put(this.c, Integer.valueOf(this.i));
                        }
                        ae7.s.put(Integer.valueOf(this.i), this.f);
                    }
                    if (zd7.b(this.f.d)) {
                        Log.d("Sqflite", this.f.d() + "opened " + this.i + " " + this.c);
                    }
                    this.d.success(ae7.y(this.i, false, false));
                } catch (Exception e2) {
                    ae7.this.v(e2, new he7(this.g, this.d), this.f);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ yd7 b;
        @DexIgnore
        public /* final */ /* synthetic */ i c;

        @DexIgnore
        public g(yd7 yd7, i iVar) {
            this.b = yd7;
            this.c = iVar;
        }

        @DexIgnore
        public void run() {
            synchronized (ae7.i) {
                ae7.this.k(this.b);
            }
            this.c.success(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ yd7 b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ i d;

        @DexIgnore
        public h(yd7 yd7, String str, i iVar) {
            this.b = yd7;
            this.c = str;
            this.d = iVar;
        }

        @DexIgnore
        public void run() {
            synchronized (ae7.i) {
                if (this.b != null) {
                    ae7.this.k(this.b);
                }
                try {
                    if (zd7.c(ae7.g)) {
                        Log.d("Sqflite", "delete database " + this.c);
                    }
                    yd7.b(this.c);
                } catch (Exception e2) {
                    Log.e("Sqflite", "error " + e2 + " while closing database " + ae7.k);
                }
            }
            this.d.success(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements MethodChannel.Result {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Handler f260a;
        @DexIgnore
        public /* final */ MethodChannel.Result b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Object b;

            @DexIgnore
            public a(Object obj) {
                this.b = obj;
            }

            @DexIgnore
            public void run() {
                i.this.b.success(this.b);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ String b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ Object d;

            @DexIgnore
            public b(String str, String str2, Object obj) {
                this.b = str;
                this.c = str2;
                this.d = obj;
            }

            @DexIgnore
            public void run() {
                i.this.b.error(this.b, this.c, this.d);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class c implements Runnable {
            @DexIgnore
            public c() {
            }

            @DexIgnore
            public void run() {
                i.this.b.notImplemented();
            }
        }

        @DexIgnore
        public i(ae7 ae7, MethodChannel.Result result) {
            this.f260a = new Handler();
            this.b = result;
        }

        @DexIgnore
        public /* synthetic */ i(ae7 ae7, MethodChannel.Result result, a aVar) {
            this(ae7, result);
        }

        @DexIgnore
        @Override // io.flutter.plugin.common.MethodChannel.Result
        public void error(String str, String str2, Object obj) {
            this.f260a.post(new b(str, str2, obj));
        }

        @DexIgnore
        @Override // io.flutter.plugin.common.MethodChannel.Result
        public void notImplemented() {
            this.f260a.post(new c());
        }

        @DexIgnore
        @Override // io.flutter.plugin.common.MethodChannel.Result
        public void success(Object obj) {
            this.f260a.post(new a(obj));
        }
    }

    @DexIgnore
    public static String N(Object obj) {
        if (obj == null) {
            return null;
        }
        if (!(obj instanceof byte[])) {
            return obj instanceof Map ? r((Map) obj).toString() : obj.toString();
        }
        ArrayList arrayList = new ArrayList();
        for (byte b2 : (byte[]) obj) {
            arrayList.add(Integer.valueOf(b2));
        }
        return arrayList.toString();
    }

    @DexIgnore
    public static List<Object> l(Cursor cursor, int i2) {
        ArrayList arrayList = new ArrayList(i2);
        for (int i3 = 0; i3 < i2; i3++) {
            Object n = n(cursor, i3);
            if (ce7.c) {
                String str = null;
                if (n != null) {
                    str = n.getClass().isArray() ? "array(" + n.getClass().getComponentType().getName() + ")" : n.getClass().getName();
                }
                StringBuilder sb = new StringBuilder();
                sb.append("column ");
                sb.append(i3);
                sb.append(" ");
                sb.append(cursor.getType(i3));
                sb.append(": ");
                sb.append(n);
                sb.append(str == null ? "" : " (" + str + ")");
                Log.d("Sqflite", sb.toString());
            }
            arrayList.add(n);
        }
        return arrayList;
    }

    @DexIgnore
    public static Map<String, Object> m(Cursor cursor) {
        HashMap hashMap = new HashMap();
        String[] columnNames = cursor.getColumnNames();
        int length = columnNames.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (ce7.c) {
                Log.d("Sqflite", "column " + i2 + " " + cursor.getType(i2));
            }
            int type = cursor.getType(i2);
            if (type == 0) {
                hashMap.put(columnNames[i2], null);
            } else if (type == 1) {
                hashMap.put(columnNames[i2], Long.valueOf(cursor.getLong(i2)));
            } else if (type == 2) {
                hashMap.put(columnNames[i2], Double.valueOf(cursor.getDouble(i2)));
            } else if (type == 3) {
                hashMap.put(columnNames[i2], cursor.getString(i2));
            } else if (type == 4) {
                hashMap.put(columnNames[i2], cursor.getBlob(i2));
            }
        }
        return hashMap;
    }

    @DexIgnore
    public static Object n(Cursor cursor, int i2) {
        int type = cursor.getType(i2);
        if (type == 1) {
            return Long.valueOf(cursor.getLong(i2));
        }
        if (type == 2) {
            return Double.valueOf(cursor.getDouble(i2));
        }
        if (type == 3) {
            return cursor.getString(i2);
        }
        if (type != 4) {
            return null;
        }
        return cursor.getBlob(i2);
    }

    @DexIgnore
    public static Map<String, Object> r(Map<Object, Object> map) {
        HashMap hashMap = new HashMap();
        for (Map.Entry<Object, Object> entry : map.entrySet()) {
            Object value = entry.getValue();
            hashMap.put(N(entry.getKey()), value instanceof Map ? r((Map) value) : N(value));
        }
        return hashMap;
    }

    @DexIgnore
    public static boolean x(String str) {
        return str == null || str.equals(SQLiteDatabase.MEMORY);
    }

    @DexIgnore
    public static Map y(int i2, boolean z, boolean z2) {
        HashMap hashMap = new HashMap();
        hashMap.put("id", Integer.valueOf(i2));
        if (z) {
            hashMap.put("recovered", Boolean.TRUE);
        }
        if (z2) {
            hashMap.put("recoveredInTransaction", Boolean.TRUE);
        }
        return hashMap;
    }

    @DexIgnore
    public final void A(MethodCall methodCall, MethodChannel.Result result) {
        yd7 t = t(methodCall, result);
        if (t != null) {
            m.post(new b(methodCall, new i(this, result, null), t));
        }
    }

    @DexIgnore
    public final void B(MethodCall methodCall, MethodChannel.Result result) {
        int intValue = ((Integer) methodCall.argument("id")).intValue();
        yd7 t = t(methodCall, result);
        if (t != null) {
            if (zd7.b(t.d)) {
                Log.d("Sqflite", t.d() + "closing " + intValue + " " + t.b);
            }
            String str = t.b;
            synchronized (h) {
                s.remove(Integer.valueOf(intValue));
                if (t.f4305a) {
                    d.remove(str);
                }
            }
            m.post(new g(t, new i(this, result, null)));
        }
    }

    @DexIgnore
    public final void C(MethodCall methodCall, MethodChannel.Result result) {
        HashMap hashMap = new HashMap();
        if ("get".equals((String) methodCall.argument("cmd"))) {
            int i2 = g;
            if (i2 > 0) {
                hashMap.put("logLevel", Integer.valueOf(i2));
            }
            if (!s.isEmpty()) {
                HashMap hashMap2 = new HashMap();
                for (Map.Entry<Integer, yd7> entry : s.entrySet()) {
                    yd7 value = entry.getValue();
                    HashMap hashMap3 = new HashMap();
                    hashMap3.put("path", value.b);
                    hashMap3.put("singleInstance", Boolean.valueOf(value.f4305a));
                    int i3 = value.d;
                    if (i3 > 0) {
                        hashMap3.put("logLevel", Integer.valueOf(i3));
                    }
                    hashMap2.put(entry.getKey().toString(), hashMap3);
                }
                hashMap.put("databases", hashMap2);
            }
        }
        result.success(hashMap);
    }

    @DexIgnore
    public final void D(MethodCall methodCall, MethodChannel.Result result) {
        ce7.f602a = Boolean.TRUE.equals(methodCall.arguments());
        ce7.c = ce7.b && ce7.f602a;
        if (!ce7.f602a) {
            g = 0;
        } else if (ce7.c) {
            g = 2;
        } else if (ce7.f602a) {
            g = 1;
        }
        result.success(null);
    }

    @DexIgnore
    public final void E(MethodCall methodCall, MethodChannel.Result result) {
        yd7 yd7;
        String str = (String) methodCall.argument("path");
        synchronized (h) {
            if (zd7.c(g)) {
                Log.d("Sqflite", "Look for " + str + " in " + d.keySet());
            }
            Integer num = d.get(str);
            if (num == null || (yd7 = s.get(num)) == null || !yd7.e.isOpen()) {
                yd7 = null;
            } else {
                if (zd7.c(g)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(yd7.d());
                    sb.append("found single instance ");
                    sb.append(yd7.f ? "(in transaction) " : "");
                    sb.append(num);
                    sb.append(" ");
                    sb.append(str);
                    Log.d("Sqflite", sb.toString());
                }
                s.remove(num);
                d.remove(str);
            }
        }
        h hVar = new h(yd7, str, new i(this, result, null));
        Handler handler = m;
        if (handler != null) {
            handler.post(hVar);
        } else {
            hVar.run();
        }
    }

    @DexIgnore
    public final void F(MethodCall methodCall, MethodChannel.Result result) {
        yd7 t = t(methodCall, result);
        if (t != null) {
            m.post(new d(t, methodCall, new i(this, result, null)));
        }
    }

    @DexIgnore
    public void G(MethodCall methodCall, MethodChannel.Result result) {
        if (j == null) {
            j = this.b.getDatabasePath("tekartik_sqflite.db").getParent();
        }
        result.success(j);
    }

    @DexIgnore
    public final void H(MethodCall methodCall, MethodChannel.Result result) {
        yd7 t = t(methodCall, result);
        if (t != null) {
            m.post(new c(methodCall, new i(this, result, null), t));
        }
    }

    @DexIgnore
    public final void I(MethodCall methodCall, MethodChannel.Result result) {
        int i2;
        yd7 yd7;
        boolean z = true;
        String str = (String) methodCall.argument("path");
        Boolean bool = (Boolean) methodCall.argument(DatabaseFieldConfigLoader.FIELD_NAME_READ_ONLY);
        boolean x = x(str);
        if (Boolean.FALSE.equals(methodCall.argument("singleInstance")) || x) {
            z = false;
        }
        if (z) {
            synchronized (h) {
                if (zd7.c(g)) {
                    Log.d("Sqflite", "Look for " + str + " in " + d.keySet());
                }
                Integer num = d.get(str);
                if (!(num == null || (yd7 = s.get(num)) == null)) {
                    if (yd7.e.isOpen()) {
                        if (zd7.c(g)) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(yd7.d());
                            sb.append("re-opened single instance ");
                            sb.append(yd7.f ? "(in transaction) " : "");
                            sb.append(num);
                            sb.append(" ");
                            sb.append(str);
                            Log.d("Sqflite", sb.toString());
                        }
                        result.success(y(num.intValue(), true, yd7.f));
                        return;
                    } else if (zd7.c(g)) {
                        Log.d("Sqflite", yd7.d() + "single instance database of " + str + " not opened");
                    }
                }
            }
        }
        synchronized (h) {
            i2 = k + 1;
            k = i2;
        }
        yd7 yd72 = new yd7(str, i2, z, g);
        i iVar = new i(this, result, null);
        synchronized (h) {
            if (m == null) {
                HandlerThread handlerThread = new HandlerThread("Sqflite", f);
                l = handlerThread;
                handlerThread.start();
                m = new Handler(l.getLooper());
                if (zd7.b(yd72.d)) {
                    Log.d("Sqflite", yd72.d() + "starting thread" + l + " priority " + f);
                }
            }
            if (zd7.b(yd72.d)) {
                Log.d("Sqflite", yd72.d() + "opened " + i2 + " " + str);
            }
            m.post(new f(x, str, iVar, bool, yd72, methodCall, z, i2));
        }
    }

    @DexIgnore
    public void J(MethodCall methodCall, MethodChannel.Result result) {
        Object argument = methodCall.argument("queryAsMapList");
        if (argument != null) {
            e = Boolean.TRUE.equals(argument);
        }
        Object argument2 = methodCall.argument("androidThreadPriority");
        if (argument2 != null) {
            f = ((Integer) argument2).intValue();
        }
        Integer a2 = zd7.a(methodCall);
        if (a2 != null) {
            g = a2.intValue();
        }
        result.success(null);
    }

    @DexIgnore
    public final void K(MethodCall methodCall, MethodChannel.Result result) {
        yd7 t = t(methodCall, result);
        if (t != null) {
            m.post(new a(methodCall, new i(this, result, null), t));
        }
    }

    @DexIgnore
    public final void L(MethodCall methodCall, MethodChannel.Result result) {
        yd7 t = t(methodCall, result);
        if (t != null) {
            m.post(new e(methodCall, new i(this, result, null), t));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ca  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean M(com.fossil.yd7 r11, com.fossil.ie7 r12) {
        /*
            r10 = this;
            r3 = 0
            r1 = 0
            com.fossil.be7 r0 = r12.b()
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            int r2 = r11.d
            boolean r2 = com.fossil.zd7.b(r2)
            if (r2 == 0) goto L_0x002b
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = r11.d()
            r2.append(r4)
            r2.append(r0)
            java.lang.String r4 = "Sqflite"
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r4, r2)
        L_0x002b:
            boolean r7 = com.fossil.ae7.e
            com.fossil.be7 r0 = r0.i()     // Catch:{ Exception -> 0x00d4, all -> 0x00ce }
            android.database.sqlite.SQLiteDatabase r2 = r11.c()     // Catch:{ Exception -> 0x00d4, all -> 0x00ce }
            java.lang.String r4 = r0.e()     // Catch:{ Exception -> 0x00d4, all -> 0x00ce }
            java.lang.String[] r0 = r0.b()     // Catch:{ Exception -> 0x00d4, all -> 0x00ce }
            android.database.Cursor r0 = r2.rawQuery(r4, r0)     // Catch:{ Exception -> 0x00d4, all -> 0x00ce }
            r4 = r3
            r2 = r1
            r5 = r1
        L_0x0044:
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0078, all -> 0x00c7 }
            if (r1 == 0) goto L_0x00b0
            if (r7 == 0) goto L_0x0084
            java.util.Map r1 = m(r0)     // Catch:{ Exception -> 0x0078, all -> 0x00c7 }
            int r8 = r11.d     // Catch:{ Exception -> 0x0078, all -> 0x00c7 }
            boolean r8 = com.fossil.zd7.b(r8)     // Catch:{ Exception -> 0x0078, all -> 0x00c7 }
            if (r8 == 0) goto L_0x0074
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0078, all -> 0x00c7 }
            r8.<init>()     // Catch:{ Exception -> 0x0078, all -> 0x00c7 }
            java.lang.String r9 = r11.d()     // Catch:{ Exception -> 0x0078, all -> 0x00c7 }
            r8.append(r9)     // Catch:{ Exception -> 0x0078, all -> 0x00c7 }
            java.lang.String r9 = N(r1)     // Catch:{ Exception -> 0x0078, all -> 0x00c7 }
            r8.append(r9)     // Catch:{ Exception -> 0x0078, all -> 0x00c7 }
            java.lang.String r9 = "Sqflite"
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0078, all -> 0x00c7 }
            android.util.Log.d(r9, r8)     // Catch:{ Exception -> 0x0078, all -> 0x00c7 }
        L_0x0074:
            r6.add(r1)     // Catch:{ Exception -> 0x0078, all -> 0x00c7 }
            goto L_0x0044
        L_0x0078:
            r1 = move-exception
            r2 = r1
        L_0x007a:
            r10.v(r2, r12, r11)     // Catch:{ all -> 0x00d7 }
            if (r0 == 0) goto L_0x0082
            r0.close()
        L_0x0082:
            r0 = r3
        L_0x0083:
            return r0
        L_0x0084:
            if (r2 != 0) goto L_0x00dd
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            int r1 = r0.getColumnCount()
            java.lang.String r5 = "columns"
            java.lang.String[] r8 = r0.getColumnNames()
            java.util.List r8 = java.util.Arrays.asList(r8)
            r2.put(r5, r8)
            java.lang.String r5 = "rows"
            r2.put(r5, r4)
            r5 = r4
        L_0x00a7:
            java.util.List r4 = l(r0, r1)
            r5.add(r4)
            r4 = r1
            goto L_0x0044
        L_0x00b0:
            if (r7 == 0) goto L_0x00bc
            r12.success(r6)
        L_0x00b5:
            if (r0 == 0) goto L_0x00ba
            r0.close()
        L_0x00ba:
            r0 = 1
            goto L_0x0083
        L_0x00bc:
            if (r2 != 0) goto L_0x00db
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
        L_0x00c3:
            r12.success(r1)
            goto L_0x00b5
        L_0x00c7:
            r1 = move-exception
        L_0x00c8:
            if (r0 == 0) goto L_0x00cd
            r0.close()
        L_0x00cd:
            throw r1
        L_0x00ce:
            r0 = move-exception
            r2 = r0
            r3 = r1
        L_0x00d1:
            r0 = r3
            r1 = r2
            goto L_0x00c8
        L_0x00d4:
            r2 = move-exception
            r0 = r1
            goto L_0x007a
        L_0x00d7:
            r1 = move-exception
            r2 = r1
            r3 = r0
            goto L_0x00d1
        L_0x00db:
            r1 = r2
            goto L_0x00c3
        L_0x00dd:
            r1 = r4
            goto L_0x00a7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ae7.M(com.fossil.yd7, com.fossil.ie7):boolean");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0098  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean O(com.fossil.yd7 r7, com.fossil.ie7 r8) {
        /*
            r6 = this;
            r0 = 1
            r2 = 0
            r1 = 0
            boolean r3 = r6.q(r7, r8)
            if (r3 != 0) goto L_0x000b
            r0 = r2
        L_0x000a:
            return r0
        L_0x000b:
            boolean r3 = r8.c()
            if (r3 == 0) goto L_0x0015
            r8.success(r1)
            goto L_0x000a
        L_0x0015:
            android.database.sqlite.SQLiteDatabase r3 = r7.f()     // Catch:{ Exception -> 0x0089 }
            java.lang.String r4 = "SELECT changes()"
            r5 = 0
            android.database.Cursor r1 = r3.rawQuery(r4, r5)     // Catch:{ Exception -> 0x0089 }
            if (r1 == 0) goto L_0x0065
            int r3 = r1.getCount()     // Catch:{ Exception -> 0x009c, all -> 0x009e }
            if (r3 <= 0) goto L_0x0065
            boolean r3 = r1.moveToFirst()     // Catch:{ Exception -> 0x009c, all -> 0x009e }
            if (r3 == 0) goto L_0x0065
            r3 = 0
            int r3 = r1.getInt(r3)     // Catch:{ Exception -> 0x009c, all -> 0x009e }
            int r4 = r7.d     // Catch:{ Exception -> 0x009c, all -> 0x009e }
            boolean r4 = com.fossil.zd7.b(r4)     // Catch:{ Exception -> 0x009c, all -> 0x009e }
            if (r4 == 0) goto L_0x0058
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009c, all -> 0x009e }
            r4.<init>()     // Catch:{ Exception -> 0x009c, all -> 0x009e }
            java.lang.String r5 = r7.d()     // Catch:{ Exception -> 0x009c, all -> 0x009e }
            r4.append(r5)     // Catch:{ Exception -> 0x009c, all -> 0x009e }
            java.lang.String r5 = "changed "
            r4.append(r5)     // Catch:{ Exception -> 0x009c, all -> 0x009e }
            r4.append(r3)     // Catch:{ Exception -> 0x009c, all -> 0x009e }
            java.lang.String r5 = "Sqflite"
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x009c, all -> 0x009e }
            android.util.Log.d(r5, r4)     // Catch:{ Exception -> 0x009c, all -> 0x009e }
        L_0x0058:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x009c, all -> 0x009e }
            r8.success(r3)     // Catch:{ Exception -> 0x009c, all -> 0x009e }
            if (r1 == 0) goto L_0x000a
            r1.close()
            goto L_0x000a
        L_0x0065:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = r7.d()
            r3.append(r4)
            java.lang.String r4 = "fail to read changes for Update/Delete"
            r3.append(r4)
            java.lang.String r4 = "Sqflite"
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r4, r3)
            r3 = 0
            r8.success(r3)
            if (r1 == 0) goto L_0x000a
            r1.close()
            goto L_0x000a
        L_0x0089:
            r0 = move-exception
        L_0x008a:
            r6.v(r0, r8, r7)     // Catch:{ all -> 0x0095 }
            if (r1 == 0) goto L_0x0092
            r1.close()
        L_0x0092:
            r0 = r2
            goto L_0x000a
        L_0x0095:
            r0 = move-exception
        L_0x0096:
            if (r1 == 0) goto L_0x009b
            r1.close()
        L_0x009b:
            throw r0
        L_0x009c:
            r0 = move-exception
            goto L_0x008a
        L_0x009e:
            r0 = move-exception
            goto L_0x0096
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ae7.O(com.fossil.yd7, com.fossil.ie7):boolean");
    }

    @DexIgnore
    public final void k(yd7 yd7) {
        try {
            if (zd7.b(yd7.d)) {
                Log.d("Sqflite", yd7.d() + "closing database " + l);
            }
            yd7.a();
        } catch (Exception e2) {
            Log.e("Sqflite", "error " + e2 + " while closing database " + k);
        }
        synchronized (h) {
            if (s.isEmpty() && m != null) {
                if (zd7.b(yd7.d)) {
                    Log.d("Sqflite", yd7.d() + "stopping thread" + l);
                }
                l.quit();
                l = null;
                m = null;
            }
        }
    }

    @DexIgnore
    public final boolean o(yd7 yd7, ie7 ie7) {
        if (!q(yd7, ie7)) {
            return false;
        }
        ie7.success(null);
        return true;
    }

    @DexIgnore
    @Override // io.flutter.embedding.engine.plugins.FlutterPlugin
    public void onAttachedToEngine(FlutterPlugin.FlutterPluginBinding flutterPluginBinding) {
        z(flutterPluginBinding.getApplicationContext(), flutterPluginBinding.getBinaryMessenger());
    }

    @DexIgnore
    @Override // io.flutter.embedding.engine.plugins.FlutterPlugin
    public void onDetachedFromEngine(FlutterPlugin.FlutterPluginBinding flutterPluginBinding) {
        this.b = null;
        this.c.setMethodCallHandler(null);
        this.c = null;
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // io.flutter.plugin.common.MethodChannel.MethodCallHandler
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        char c2;
        String str = methodCall.method;
        switch (str.hashCode()) {
            case -1319569547:
                if (str.equals("execute")) {
                    c2 = 5;
                    break;
                }
                c2 = '\uffff';
                break;
            case -1253581933:
                if (str.equals("closeDatabase")) {
                    c2 = 1;
                    break;
                }
                c2 = '\uffff';
                break;
            case -1249474914:
                if (str.equals("options")) {
                    c2 = '\b';
                    break;
                }
                c2 = '\uffff';
                break;
            case -1183792455:
                if (str.equals("insert")) {
                    c2 = 3;
                    break;
                }
                c2 = '\uffff';
                break;
            case -838846263:
                if (str.equals("update")) {
                    c2 = 4;
                    break;
                }
                c2 = '\uffff';
                break;
            case -263511994:
                if (str.equals("deleteDatabase")) {
                    c2 = '\n';
                    break;
                }
                c2 = '\uffff';
                break;
            case -198450538:
                if (str.equals("debugMode")) {
                    c2 = '\f';
                    break;
                }
                c2 = '\uffff';
                break;
            case -17190427:
                if (str.equals("openDatabase")) {
                    c2 = 6;
                    break;
                }
                c2 = '\uffff';
                break;
            case 93509434:
                if (str.equals(GraphRequest.BATCH_PARAM)) {
                    c2 = 7;
                    break;
                }
                c2 = '\uffff';
                break;
            case 95458899:
                if (str.equals("debug")) {
                    c2 = 11;
                    break;
                }
                c2 = '\uffff';
                break;
            case 107944136:
                if (str.equals(ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME)) {
                    c2 = 2;
                    break;
                }
                c2 = '\uffff';
                break;
            case 1385449135:
                if (str.equals("getPlatformVersion")) {
                    c2 = 0;
                    break;
                }
                c2 = '\uffff';
                break;
            case 1863829223:
                if (str.equals("getDatabasesPath")) {
                    c2 = '\t';
                    break;
                }
                c2 = '\uffff';
                break;
            default:
                c2 = '\uffff';
                break;
        }
        switch (c2) {
            case 0:
                result.success("Android " + Build.VERSION.RELEASE);
                return;
            case 1:
                B(methodCall, result);
                return;
            case 2:
                K(methodCall, result);
                return;
            case 3:
                H(methodCall, result);
                return;
            case 4:
                L(methodCall, result);
                return;
            case 5:
                F(methodCall, result);
                return;
            case 6:
                I(methodCall, result);
                return;
            case 7:
                A(methodCall, result);
                return;
            case '\b':
                J(methodCall, result);
                return;
            case '\t':
                G(methodCall, result);
                return;
            case '\n':
                E(methodCall, result);
                return;
            case 11:
                C(methodCall, result);
                return;
            case '\f':
                D(methodCall, result);
                return;
            default:
                result.notImplemented();
                return;
        }
    }

    @DexIgnore
    public final yd7 p(yd7 yd7, MethodCall methodCall, MethodChannel.Result result) {
        if (q(yd7, new ge7(result, u(methodCall), (Boolean) methodCall.argument("inTransaction")))) {
            return yd7;
        }
        return null;
    }

    @DexIgnore
    public final boolean q(yd7 yd7, ie7 ie7) {
        be7 b2 = ie7.b();
        if (zd7.b(yd7.d)) {
            Log.d("Sqflite", yd7.d() + b2);
        }
        Boolean d2 = ie7.d();
        try {
            yd7.f().execSQL(b2.e(), b2.f());
            if (Boolean.TRUE.equals(d2)) {
                yd7.f = true;
            }
            if (!Boolean.FALSE.equals(d2)) {
                return true;
            }
            yd7.f = false;
            return true;
        } catch (Exception e2) {
            v(e2, ie7, yd7);
            if (Boolean.FALSE.equals(d2)) {
                yd7.f = false;
            }
            return false;
        } catch (Throwable th) {
            if (Boolean.FALSE.equals(d2)) {
                yd7.f = false;
            }
            throw th;
        }
    }

    @DexIgnore
    public final yd7 s(int i2) {
        return s.get(Integer.valueOf(i2));
    }

    @DexIgnore
    public final yd7 t(MethodCall methodCall, MethodChannel.Result result) {
        int intValue = ((Integer) methodCall.argument("id")).intValue();
        yd7 s2 = s(intValue);
        if (s2 != null) {
            return s2;
        }
        result.error("sqlite_error", "database_closed " + intValue, null);
        return null;
    }

    @DexIgnore
    public final be7 u(MethodCall methodCall) {
        return new be7((String) methodCall.argument("sql"), (List) methodCall.argument("arguments"));
    }

    @DexIgnore
    public final void v(Exception exc, ie7 ie7, yd7 yd7) {
        if (exc instanceof SQLiteCantOpenDatabaseException) {
            ie7.error("sqlite_error", "open_failed " + yd7.b, null);
        } else if (exc instanceof SQLException) {
            ie7.error("sqlite_error", exc.getMessage(), ke7.a(ie7));
        } else {
            ie7.error("sqlite_error", exc.getMessage(), ke7.a(ie7));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00d1  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00da  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean w(com.fossil.yd7 r8, com.fossil.ie7 r9) {
        /*
            r7 = this;
            r2 = 0
            r1 = 0
            r0 = 1
            boolean r3 = r7.q(r8, r9)
            if (r3 != 0) goto L_0x000b
            r0 = r2
        L_0x000a:
            return r0
        L_0x000b:
            boolean r3 = r9.c()
            if (r3 == 0) goto L_0x0015
            r9.success(r1)
            goto L_0x000a
        L_0x0015:
            android.database.sqlite.SQLiteDatabase r3 = r8.f()     // Catch:{ Exception -> 0x00cb, all -> 0x00de }
            java.lang.String r4 = "SELECT changes(), last_insert_rowid()"
            r5 = 0
            android.database.Cursor r1 = r3.rawQuery(r4, r5)     // Catch:{ Exception -> 0x00cb, all -> 0x00de }
            if (r1 == 0) goto L_0x00a6
            int r3 = r1.getCount()     // Catch:{ Exception -> 0x00e0 }
            if (r3 <= 0) goto L_0x00a6
            boolean r3 = r1.moveToFirst()     // Catch:{ Exception -> 0x00e0 }
            if (r3 == 0) goto L_0x00a6
            r3 = 0
            int r3 = r1.getInt(r3)     // Catch:{ Exception -> 0x00e0 }
            if (r3 != 0) goto L_0x006e
            int r3 = r8.d     // Catch:{ Exception -> 0x00e0 }
            boolean r3 = com.fossil.zd7.b(r3)     // Catch:{ Exception -> 0x00e0 }
            if (r3 == 0) goto L_0x0064
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e0 }
            r3.<init>()     // Catch:{ Exception -> 0x00e0 }
            java.lang.String r4 = r8.d()     // Catch:{ Exception -> 0x00e0 }
            r3.append(r4)     // Catch:{ Exception -> 0x00e0 }
            java.lang.String r4 = "no changes (id was "
            r3.append(r4)     // Catch:{ Exception -> 0x00e0 }
            r4 = 1
            long r4 = r1.getLong(r4)     // Catch:{ Exception -> 0x00e0 }
            r3.append(r4)     // Catch:{ Exception -> 0x00e0 }
            java.lang.String r4 = ")"
            r3.append(r4)     // Catch:{ Exception -> 0x00e0 }
            java.lang.String r4 = "Sqflite"
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00e0 }
            android.util.Log.d(r4, r3)     // Catch:{ Exception -> 0x00e0 }
        L_0x0064:
            r3 = 0
            r9.success(r3)     // Catch:{ Exception -> 0x00e0 }
            if (r1 == 0) goto L_0x000a
            r1.close()
            goto L_0x000a
        L_0x006e:
            r3 = 1
            long r4 = r1.getLong(r3)
            int r3 = r8.d
            boolean r3 = com.fossil.zd7.b(r3)
            if (r3 == 0) goto L_0x0098
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r6 = r8.d()
            r3.append(r6)
            java.lang.String r6 = "inserted "
            r3.append(r6)
            r3.append(r4)
            java.lang.String r6 = "Sqflite"
            java.lang.String r3 = r3.toString()
            android.util.Log.d(r6, r3)
        L_0x0098:
            java.lang.Long r3 = java.lang.Long.valueOf(r4)
            r9.success(r3)
            if (r1 == 0) goto L_0x000a
            r1.close()
            goto L_0x000a
        L_0x00a6:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = r8.d()
            r3.append(r4)
            java.lang.String r4 = "fail to read changes for Insert"
            r3.append(r4)
            java.lang.String r4 = "Sqflite"
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r4, r3)
            r3 = 0
            r9.success(r3)
            if (r1 == 0) goto L_0x000a
            r1.close()
            goto L_0x000a
        L_0x00cb:
            r0 = move-exception
        L_0x00cc:
            r7.v(r0, r9, r8)     // Catch:{ all -> 0x00d7 }
            if (r1 == 0) goto L_0x00d4
            r1.close()
        L_0x00d4:
            r0 = r2
            goto L_0x000a
        L_0x00d7:
            r0 = move-exception
        L_0x00d8:
            if (r1 == 0) goto L_0x00dd
            r1.close()
        L_0x00dd:
            throw r0
        L_0x00de:
            r0 = move-exception
            goto L_0x00d8
        L_0x00e0:
            r0 = move-exception
            goto L_0x00cc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ae7.w(com.fossil.yd7, com.fossil.ie7):boolean");
    }

    @DexIgnore
    public final void z(Context context, BinaryMessenger binaryMessenger) {
        this.b = context;
        MethodChannel methodChannel = new MethodChannel(binaryMessenger, "com.tekartik.sqflite");
        this.c = methodChannel;
        methodChannel.setMethodCallHandler(this);
    }
}
