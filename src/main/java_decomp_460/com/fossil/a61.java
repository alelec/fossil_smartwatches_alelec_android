package com.fossil;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a61 implements e61<Uri> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f202a;

    @DexIgnore
    public a61(Context context) {
        pq7.c(context, "context");
        this.f202a = context;
    }

    @DexIgnore
    /* renamed from: d */
    public Object c(g51 g51, Uri uri, f81 f81, x51 x51, qn7<? super d61> qn7) {
        InputStream openInputStream;
        if (f(uri)) {
            AssetFileDescriptor openAssetFileDescriptor = this.f202a.getContentResolver().openAssetFileDescriptor(uri, "r");
            openInputStream = openAssetFileDescriptor != null ? openAssetFileDescriptor.createInputStream() : null;
            if (openInputStream == null) {
                throw new IllegalStateException(("Unable to find a contact photo associated with '" + uri + "'.").toString());
            }
        } else {
            openInputStream = this.f202a.getContentResolver().openInputStream(uri);
            if (openInputStream == null) {
                throw new IllegalStateException(("Unable to open '" + uri + "'.").toString());
            }
        }
        return new k61(s48.d(s48.l(openInputStream)), this.f202a.getContentResolver().getType(uri), q51.DISK);
    }

    @DexIgnore
    /* renamed from: e */
    public boolean a(Uri uri) {
        pq7.c(uri, "data");
        return pq7.a(uri.getScheme(), "content");
    }

    @DexIgnore
    public final boolean f(Uri uri) {
        pq7.c(uri, "data");
        return pq7.a(uri.getAuthority(), "com.android.contacts") && pq7.a(uri.getLastPathSegment(), "display_photo");
    }

    @DexIgnore
    /* renamed from: g */
    public String b(Uri uri) {
        pq7.c(uri, "data");
        String uri2 = uri.toString();
        pq7.b(uri2, "data.toString()");
        return uri2;
    }
}
