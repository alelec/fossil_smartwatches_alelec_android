package com.fossil;

import com.portfolio.platform.data.source.WorkoutSettingRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nu6 implements Factory<mu6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<fu6> f2577a;
    @DexIgnore
    public /* final */ Provider<WorkoutSettingRepository> b;

    @DexIgnore
    public nu6(Provider<fu6> provider, Provider<WorkoutSettingRepository> provider2) {
        this.f2577a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static nu6 a(Provider<fu6> provider, Provider<WorkoutSettingRepository> provider2) {
        return new nu6(provider, provider2);
    }

    @DexIgnore
    public static mu6 c(fu6 fu6, WorkoutSettingRepository workoutSettingRepository) {
        return new mu6(fu6, workoutSettingRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public mu6 get() {
        return c(this.f2577a.get(), this.b.get());
    }
}
