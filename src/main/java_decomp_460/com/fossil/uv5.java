package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uv5 extends pv5 implements zv6 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public g37<l65> g;
    @DexIgnore
    public yv6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return uv5.j;
        }

        @DexIgnore
        public final uv5 b() {
            return new uv5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ uv5 b;

        @DexIgnore
        public b(uv5 uv5) {
            this.b = uv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ l65 b;
        @DexIgnore
        public /* final */ /* synthetic */ uv5 c;

        @DexIgnore
        public c(l65 l65, uv5 uv5) {
            this.b = l65;
            this.c = uv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            yv6 K6 = uv5.K6(this.c);
            FlexibleTextInputEditText flexibleTextInputEditText = this.b.t;
            pq7.b(flexibleTextInputEditText, "binding.etEmail");
            K6.o(String.valueOf(flexibleTextInputEditText.getText()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ l65 b;
        @DexIgnore
        public /* final */ /* synthetic */ uv5 c;

        @DexIgnore
        public d(l65 l65, uv5 uv5) {
            this.b = l65;
            this.c = uv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            yv6 K6 = uv5.K6(this.c);
            FlexibleTextInputEditText flexibleTextInputEditText = this.b.t;
            pq7.b(flexibleTextInputEditText, "binding.etEmail");
            K6.o(String.valueOf(flexibleTextInputEditText.getText()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ uv5 b;

        @DexIgnore
        public e(uv5 uv5) {
            this.b = uv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ uv5 b;

        @DexIgnore
        public f(uv5 uv5) {
            this.b = uv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpActivity.a aVar = SignUpActivity.F;
            pq7.b(view, "it");
            Context context = view.getContext();
            pq7.b(context, "it.context");
            aVar.a(context);
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ uv5 b;

        @DexIgnore
        public g(uv5 uv5) {
            this.b = uv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ uv5 b;

        @DexIgnore
        public h(uv5 uv5) {
            this.b = uv5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String str;
            yv6 K6 = uv5.K6(this.b);
            if (charSequence == null || (str = charSequence.toString()) == null) {
                str = "";
            }
            K6.n(str);
        }
    }

    /*
    static {
        String simpleName = uv5.class.getSimpleName();
        if (simpleName != null) {
            pq7.b(simpleName, "ForgotPasswordFragment::class.java.simpleName!!");
            j = simpleName;
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ yv6 K6(uv5 uv5) {
        yv6 yv6 = uv5.h;
        if (yv6 != null) {
            return yv6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return j;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.zv6
    public void H2() {
        g37<l65> g37 = this.g;
        if (g37 != null) {
            l65 a2 = g37.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.v;
                pq7.b(flexibleButton, "it.fbSendLink");
                flexibleButton.setEnabled(false);
                FlexibleButton flexibleButton2 = a2.v;
                pq7.b(flexibleButton2, "it.fbSendLink");
                flexibleButton2.setClickable(false);
                FlexibleButton flexibleButton3 = a2.v;
                pq7.b(flexibleButton3, "it.fbSendLink");
                flexibleButton3.setFocusable(false);
                a2.v.d("flexible_button_disabled");
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(yv6 yv6) {
        pq7.c(yv6, "presenter");
        this.h = yv6;
    }

    @DexIgnore
    @Override // com.fossil.zv6
    public void P4(boolean z) {
        FlexibleButton flexibleButton;
        g37<l65> g37 = this.g;
        if (g37 != null) {
            l65 a2 = g37.a();
            if (a2 != null && (flexibleButton = a2.z) != null) {
                if (z) {
                    pq7.b(flexibleButton, "it");
                    flexibleButton.setVisibility(0);
                    return;
                }
                pq7.b(flexibleButton, "it");
                flexibleButton.setVisibility(8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zv6
    public void S0() {
        g37<l65> g37 = this.g;
        if (g37 != null) {
            l65 a2 = g37.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.v;
                pq7.b(flexibleButton, "it.fbSendLink");
                flexibleButton.setEnabled(true);
                FlexibleButton flexibleButton2 = a2.v;
                pq7.b(flexibleButton2, "it.fbSendLink");
                flexibleButton2.setClickable(true);
                FlexibleButton flexibleButton3 = a2.v;
                pq7.b(flexibleButton3, "it.fbSendLink");
                flexibleButton3.setFocusable(true);
                a2.v.d("flexible_button_primary");
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zv6
    public void h() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.zv6
    public void i() {
        b();
    }

    @DexIgnore
    @Override // com.fossil.zv6
    public void m3() {
        g37<l65> g37 = this.g;
        if (g37 != null) {
            l65 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                pq7.b(constraintLayout, "it.clResetPwSuccess");
                if (constraintLayout.getVisibility() == 0) {
                    s37 s37 = s37.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    pq7.b(childFragmentManager, "childFragmentManager");
                    s37.m0(childFragmentManager);
                    return;
                }
                ConstraintLayout constraintLayout2 = a2.q;
                pq7.b(constraintLayout2, "it.clResetPw");
                constraintLayout2.setVisibility(8);
                ConstraintLayout constraintLayout3 = a2.r;
                pq7.b(constraintLayout3, "it.clResetPwSuccess");
                constraintLayout3.setVisibility(0);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zv6
    public void n6(String str) {
        FlexibleTextInputEditText flexibleTextInputEditText;
        pq7.c(str, Constants.EMAIL);
        g37<l65> g37 = this.g;
        if (g37 != null) {
            l65 a2 = g37.a();
            if (a2 != null && (flexibleTextInputEditText = a2.t) != null) {
                flexibleTextInputEditText.setText(str);
                flexibleTextInputEditText.requestFocus();
                xk5 xk5 = xk5.f4136a;
                pq7.b(flexibleTextInputEditText, "it");
                Context context = flexibleTextInputEditText.getContext();
                pq7.b(context, "it.context");
                xk5.b(flexibleTextInputEditText, context);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zv6
    public void o(int i2, String str) {
        pq7.c(str, "errorMessage");
        s37 s37 = s37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        s37.n0(i2, str, childFragmentManager);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        g37<l65> g37 = new g37<>(this, (l65) aq0.f(layoutInflater, 2131558556, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            l65 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        yv6 yv6 = this.h;
        if (yv6 != null) {
            yv6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<l65> g37 = this.g;
        if (g37 != null) {
            l65 a2 = g37.a();
            if (a2 != null) {
                a2.u.setOnClickListener(new b(this));
                a2.v.setOnClickListener(new c(a2, this));
                a2.x.setOnClickListener(new d(a2, this));
                a2.C.setOnClickListener(new e(this));
                a2.z.setOnClickListener(new f(this));
                a2.D.setOnClickListener(new g(this));
                a2.t.addTextChangedListener(new h(this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zv6
    public void r5(String str) {
        pq7.c(str, "message");
        g37<l65> g37 = this.g;
        if (g37 != null) {
            l65 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextInputLayout flexibleTextInputLayout = a2.B;
                pq7.b(flexibleTextInputLayout, "it.inputEmail");
                flexibleTextInputLayout.setErrorEnabled(false);
                FlexibleTextInputLayout flexibleTextInputLayout2 = a2.B;
                pq7.b(flexibleTextInputLayout2, "it.inputEmail");
                flexibleTextInputLayout2.setError(str);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
