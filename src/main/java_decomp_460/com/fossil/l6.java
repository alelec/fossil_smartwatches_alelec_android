package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l6 {
    @DexIgnore
    public /* synthetic */ l6(kq7 kq7) {
    }

    @DexIgnore
    public final n6 a(byte b) {
        n6 n6Var;
        n6[] values = n6.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                n6Var = null;
                break;
            }
            n6Var = values[i];
            if (n6Var.c == b) {
                break;
            }
            i++;
        }
        return n6Var != null ? n6Var : n6.UNKNOWN;
    }
}
