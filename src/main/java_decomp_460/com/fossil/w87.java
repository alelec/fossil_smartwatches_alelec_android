package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w87 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ float f3901a;
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ float d;
    @DexIgnore
    public /* final */ float e;

    @DexIgnore
    public w87(float f, float f2, float f3, float f4, float f5) {
        this.f3901a = f;
        this.b = f2;
        this.c = f3;
        this.d = f4;
        this.e = f5;
    }

    @DexIgnore
    public final float a() {
        return this.d;
    }

    @DexIgnore
    public final float b() {
        return this.e;
    }

    @DexIgnore
    public final float c() {
        return this.c;
    }

    @DexIgnore
    public final float d() {
        return this.f3901a;
    }

    @DexIgnore
    public final float e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof w87) {
                w87 w87 = (w87) obj;
                if (!(Float.compare(this.f3901a, w87.f3901a) == 0 && Float.compare(this.b, w87.b) == 0 && Float.compare(this.c, w87.c) == 0 && Float.compare(this.d, w87.d) == 0 && Float.compare(this.e, w87.e) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final jb7 f() {
        return new jb7(this.f3901a, this.b, this.c, this.d);
    }

    @DexIgnore
    public int hashCode() {
        return (((((((Float.floatToIntBits(this.f3901a) * 31) + Float.floatToIntBits(this.b)) * 31) + Float.floatToIntBits(this.c)) * 31) + Float.floatToIntBits(this.d)) * 31) + Float.floatToIntBits(this.e);
    }

    @DexIgnore
    public String toString() {
        return "Metric(xPos=" + this.f3901a + ", yPos=" + this.b + ", width=" + this.c + ", height=" + this.d + ", scaleFactor=" + this.e + ")";
    }
}
