package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.CancellationSignal;
import android.util.Log;
import androidx.collection.SimpleArrayMap;
import com.fossil.kl0;
import com.fossil.zm0;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ul0 extends yl0 {
    @DexIgnore
    public static /* final */ Class<?> b;
    @DexIgnore
    public static /* final */ Constructor<?> c;
    @DexIgnore
    public static /* final */ Method d;
    @DexIgnore
    public static /* final */ Method e;

    /*
    static {
        Method method;
        Method method2;
        Class<?> cls;
        Constructor<?> constructor;
        try {
            cls = Class.forName("android.graphics.FontFamily");
            constructor = cls.getConstructor(new Class[0]);
            method2 = cls.getMethod("addFontWeightStyle", ByteBuffer.class, Integer.TYPE, List.class, Integer.TYPE, Boolean.TYPE);
            method = Typeface.class.getMethod("createFromFamiliesWithDefault", Array.newInstance(cls, 1).getClass());
        } catch (ClassNotFoundException | NoSuchMethodException e2) {
            Log.e("TypefaceCompatApi24Impl", e2.getClass().getName(), e2);
            method = null;
            method2 = null;
            cls = null;
            constructor = null;
        }
        c = constructor;
        b = cls;
        d = method2;
        e = method;
    }
    */

    @DexIgnore
    public static boolean k(Object obj, ByteBuffer byteBuffer, int i, int i2, boolean z) {
        try {
            return ((Boolean) d.invoke(obj, byteBuffer, Integer.valueOf(i), null, Integer.valueOf(i2), Boolean.valueOf(z))).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e2) {
            return false;
        }
    }

    @DexIgnore
    public static Typeface l(Object obj) {
        try {
            Object newInstance = Array.newInstance(b, 1);
            Array.set(newInstance, 0, obj);
            return (Typeface) e.invoke(null, newInstance);
        } catch (IllegalAccessException | InvocationTargetException e2) {
            return null;
        }
    }

    @DexIgnore
    public static boolean m() {
        if (d == null) {
            Log.w("TypefaceCompatApi24Impl", "Unable to collect necessary private methods.Fallback to legacy implementation.");
        }
        return d != null;
    }

    @DexIgnore
    public static Object n() {
        try {
            return c.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e2) {
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yl0
    public Typeface b(Context context, kl0.b bVar, Resources resources, int i) {
        Object n = n();
        if (n == null) {
            return null;
        }
        kl0.c[] a2 = bVar.a();
        for (kl0.c cVar : a2) {
            ByteBuffer b2 = zl0.b(context, resources, cVar.b());
            if (b2 == null || !k(n, b2, cVar.c(), cVar.e(), cVar.f())) {
                return null;
            }
        }
        return l(n);
    }

    @DexIgnore
    @Override // com.fossil.yl0
    public Typeface c(Context context, CancellationSignal cancellationSignal, zm0.f[] fVarArr, int i) {
        Object n = n();
        if (n == null) {
            return null;
        }
        SimpleArrayMap simpleArrayMap = new SimpleArrayMap();
        for (zm0.f fVar : fVarArr) {
            Uri c2 = fVar.c();
            ByteBuffer byteBuffer = (ByteBuffer) simpleArrayMap.get(c2);
            if (byteBuffer == null) {
                byteBuffer = zl0.f(context, cancellationSignal, c2);
                simpleArrayMap.put(c2, byteBuffer);
            }
            if (byteBuffer == null) {
                return null;
            }
            if (!k(n, byteBuffer, fVar.b(), fVar.d(), fVar.e())) {
                return null;
            }
        }
        Typeface l = l(n);
        if (l == null) {
            return null;
        }
        return Typeface.create(l, i);
    }
}
