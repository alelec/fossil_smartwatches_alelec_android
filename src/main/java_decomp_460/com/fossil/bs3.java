package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.Button;
import android.widget.FrameLayout;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class bs3 extends FrameLayout {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Context f493a;

        @DexIgnore
        public a(Context context) {
            this.f493a = context;
        }

        @DexIgnore
        @Override // com.fossil.bs3.d
        public final Drawable a(int i) {
            return this.f493a.getResources().getDrawable(17301508);
        }

        @DexIgnore
        @Override // com.fossil.bs3.d
        public final boolean isValid() {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Context f494a;

        @DexIgnore
        public b(Context context) {
            this.f494a = context;
        }

        @DexIgnore
        @Override // com.fossil.bs3.d
        public final Drawable a(int i) {
            try {
                Resources resources = this.f494a.createPackageContext("com.google.android.gms", 4).getResources();
                return resources.getDrawable(resources.getIdentifier(i != 0 ? i != 1 ? i != 2 ? "ic_plusone_standard" : "ic_plusone_tall" : "ic_plusone_medium" : "ic_plusone_small", ResourceManager.DRAWABLE, "com.google.android.gms"));
            } catch (PackageManager.NameNotFoundException e) {
                return null;
            }
        }

        @DexIgnore
        @Override // com.fossil.bs3.d
        public final boolean isValid() {
            try {
                this.f494a.createPackageContext("com.google.android.gms", 4).getResources();
                return true;
            } catch (PackageManager.NameNotFoundException e) {
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Context f495a;

        @DexIgnore
        public c(Context context) {
            this.f495a = context;
        }

        @DexIgnore
        @Override // com.fossil.bs3.d
        public final Drawable a(int i) {
            return this.f495a.getResources().getDrawable(this.f495a.getResources().getIdentifier(i != 0 ? i != 1 ? i != 2 ? "ic_plusone_standard_off_client" : "ic_plusone_tall_off_client" : "ic_plusone_medium_off_client" : "ic_plusone_small_off_client", ResourceManager.DRAWABLE, this.f495a.getPackageName()));
        }

        @DexIgnore
        @Override // com.fossil.bs3.d
        public final boolean isValid() {
            return (this.f495a.getResources().getIdentifier("ic_plusone_small_off_client", ResourceManager.DRAWABLE, this.f495a.getPackageName()) == 0 || this.f495a.getResources().getIdentifier("ic_plusone_medium_off_client", ResourceManager.DRAWABLE, this.f495a.getPackageName()) == 0 || this.f495a.getResources().getIdentifier("ic_plusone_tall_off_client", ResourceManager.DRAWABLE, this.f495a.getPackageName()) == 0 || this.f495a.getResources().getIdentifier("ic_plusone_standard_off_client", ResourceManager.DRAWABLE, this.f495a.getPackageName()) == 0) ? false : true;
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        Drawable a(int i);

        @DexIgnore
        boolean isValid();
    }

    @DexIgnore
    @Deprecated
    public bs3(Context context, int i) {
        super(context);
        int i2;
        Button button = new Button(context);
        button.setEnabled(false);
        d bVar = new b(getContext());
        bVar = !bVar.isValid() ? new c(getContext()) : bVar;
        button.setBackgroundDrawable((!bVar.isValid() ? new a(getContext()) : bVar).a(i));
        Point point = new Point();
        int i3 = 20;
        if (i == 0) {
            i3 = 14;
            i2 = 24;
        } else if (i == 1) {
            i2 = 32;
        } else if (i != 2) {
            i2 = 38;
            i3 = 24;
        } else {
            i2 = 50;
        }
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float applyDimension = TypedValue.applyDimension(1, (float) i2, displayMetrics);
        float applyDimension2 = TypedValue.applyDimension(1, (float) i3, displayMetrics);
        point.x = (int) (((double) applyDimension) + 0.5d);
        point.y = (int) (((double) applyDimension2) + 0.5d);
        addView(button, new FrameLayout.LayoutParams(point.x, point.y, 17));
    }
}
