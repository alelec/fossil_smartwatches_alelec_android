package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import java.util.LinkedHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gd7 implements yc7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ LinkedHashMap<String, Bitmap> f1294a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;

    @DexIgnore
    public gd7(int i) {
        if (i > 0) {
            this.b = i;
            this.f1294a = new LinkedHashMap<>(0, 0.75f, true);
            return;
        }
        throw new IllegalArgumentException("Max size must be positive.");
    }

    @DexIgnore
    public gd7(Context context) {
        this(xd7.b(context));
    }

    @DexIgnore
    @Override // com.fossil.yc7
    public final int a() {
        int i;
        synchronized (this) {
            i = this.b;
        }
        return i;
    }

    @DexIgnore
    @Override // com.fossil.yc7
    public Bitmap b(String str) {
        if (str != null) {
            synchronized (this) {
                Bitmap bitmap = this.f1294a.get(str);
                if (bitmap != null) {
                    this.f++;
                    return bitmap;
                }
                this.g++;
                return null;
            }
        }
        throw new NullPointerException("key == null");
    }

    @DexIgnore
    @Override // com.fossil.yc7
    public void c(String str, Bitmap bitmap) {
        if (str == null || bitmap == null) {
            throw new NullPointerException("key == null || bitmap == null");
        }
        synchronized (this) {
            this.d++;
            this.c += xd7.k(bitmap);
            Bitmap put = this.f1294a.put(str, bitmap);
            if (put != null) {
                this.c -= xd7.k(put);
            }
        }
        d(this.b);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0073, code lost:
        throw new java.lang.IllegalStateException(getClass().getName() + ".sizeOf() is reporting inconsistent results!");
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void d(int r4) {
        /*
            r3 = this;
        L_0x0000:
            monitor-enter(r3)
            int r0 = r3.c     // Catch:{ all -> 0x0052 }
            if (r0 < 0) goto L_0x0055
            java.util.LinkedHashMap<java.lang.String, android.graphics.Bitmap> r0 = r3.f1294a     // Catch:{ all -> 0x0052 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0052 }
            if (r0 == 0) goto L_0x0011
            int r0 = r3.c     // Catch:{ all -> 0x0052 }
            if (r0 != 0) goto L_0x0055
        L_0x0011:
            int r0 = r3.c     // Catch:{ all -> 0x0052 }
            if (r0 <= r4) goto L_0x001d
            java.util.LinkedHashMap<java.lang.String, android.graphics.Bitmap> r0 = r3.f1294a     // Catch:{ all -> 0x0052 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0052 }
            if (r0 == 0) goto L_0x001f
        L_0x001d:
            monitor-exit(r3)     // Catch:{ all -> 0x0052 }
            return
        L_0x001f:
            java.util.LinkedHashMap<java.lang.String, android.graphics.Bitmap> r0 = r3.f1294a     // Catch:{ all -> 0x0052 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x0052 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0052 }
            java.lang.Object r0 = r0.next()     // Catch:{ all -> 0x0052 }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x0052 }
            java.lang.Object r1 = r0.getKey()     // Catch:{ all -> 0x0052 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0052 }
            java.lang.Object r0 = r0.getValue()     // Catch:{ all -> 0x0052 }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ all -> 0x0052 }
            java.util.LinkedHashMap<java.lang.String, android.graphics.Bitmap> r2 = r3.f1294a     // Catch:{ all -> 0x0052 }
            r2.remove(r1)     // Catch:{ all -> 0x0052 }
            int r1 = r3.c     // Catch:{ all -> 0x0052 }
            int r0 = com.fossil.xd7.k(r0)     // Catch:{ all -> 0x0052 }
            int r0 = r1 - r0
            r3.c = r0     // Catch:{ all -> 0x0052 }
            int r0 = r3.e     // Catch:{ all -> 0x0052 }
            int r0 = r0 + 1
            r3.e = r0     // Catch:{ all -> 0x0052 }
            monitor-exit(r3)     // Catch:{ all -> 0x0052 }
            goto L_0x0000
        L_0x0052:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0052 }
            throw r0
        L_0x0055:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.Class r2 = r3.getClass()
            java.lang.String r2 = r2.getName()
            r1.append(r2)
            java.lang.String r2 = ".sizeOf() is reporting inconsistent results!"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gd7.d(int):void");
    }

    @DexIgnore
    @Override // com.fossil.yc7
    public final int size() {
        int i;
        synchronized (this) {
            i = this.c;
        }
        return i;
    }
}
