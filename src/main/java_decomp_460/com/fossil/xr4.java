package com.fossil;

import android.content.Context;
import android.content.Intent;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xr4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xr4 f4164a; // = new xr4();

    @DexIgnore
    public final void a(String str, String str2, String str3, Context context) {
        pq7.c(str, "type");
        pq7.c(str2, "challengeId");
        pq7.c(str3, "profileId");
        pq7.c(context, "context");
        Intent intent = new Intent(str);
        intent.putExtra("challenge_id_extra", str2);
        intent.putExtra("current_user_id_extra", str3);
        ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void b(String str, String str2, String str3, Context context) {
        pq7.c(str, "type");
        pq7.c(str2, "currentId");
        pq7.c(str3, "friendId");
        pq7.c(context, "context");
        Intent intent = new Intent(str);
        intent.putExtra("current_user_id_extra", str2);
        intent.putExtra("friend_id_extra", str3);
        ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void c(String str, String str2, int i, int i2, boolean z, Context context) {
        pq7.c(str, "challengeId");
        pq7.c(str2, "currentId");
        pq7.c(context, "context");
        Intent intent = new Intent("bc_complete_challenge");
        intent.putExtra("challenge_id_extra", str);
        intent.putExtra("current_user_id_extra", str2);
        intent.putExtra("current_steps_extra", i2);
        intent.putExtra("rank_extra", i);
        intent.putExtra("reach_goal_or_top", z);
        ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void d(ps4 ps4, long j, String str, Context context) {
        pq7.c(ps4, "challenge");
        pq7.c(str, "startType");
        pq7.c(context, "context");
        Intent intent = new Intent("bc_create_challenge");
        intent.putExtra("challenge_extra", ps4);
        intent.putExtra("start_tracking_time_extra", j);
        intent.putExtra("start_type_extra", str);
        ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void e(String str, Context context) {
        pq7.c(str, "socialProfileId");
        pq7.c(context, "context");
        Intent intent = new Intent("bc_create_social_profile");
        intent.putExtra("current_user_id_extra", str);
        ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void f(Context context) {
        pq7.c(context, "context");
        Intent intent = new Intent("bc_look_for_friend");
        intent.putExtra("current_user_id_extra", PortfolioApp.h0.c().l0());
        ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void g(Context context) {
        pq7.c(context, "context");
        Intent intent = new Intent("bc_look_for_friend_challenge");
        intent.putExtra("current_user_id_extra", PortfolioApp.h0.c().l0());
        ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void h(String str, String str2, String str3, int i, Context context) {
        pq7.c(str, "type");
        pq7.c(str2, "challengeId");
        pq7.c(str3, "profileId");
        pq7.c(context, "context");
        Intent intent = new Intent(str);
        intent.putExtra("challenge_id_extra", str2);
        intent.putExtra("current_user_id_extra", str3);
        intent.putExtra("current_steps_extra", i);
        ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void i(String str, String str2, Context context) {
        pq7.c(str, "currentId");
        pq7.c(str2, "friendId");
        pq7.c(context, "context");
        Intent intent = new Intent("bc_send_friend_request");
        intent.putExtra("current_user_id_extra", str);
        intent.putExtra("friend_id_extra", str2);
        ct0.b(context).d(intent);
    }
}
