package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d14 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f708a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends d14 {
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(d14 d14, String str) {
            super(d14, null);
            this.b = str;
        }

        @DexIgnore
        @Override // com.fossil.d14
        public CharSequence j(Object obj) {
            return obj == null ? this.b : d14.this.j(obj);
        }

        @DexIgnore
        @Override // com.fossil.d14
        public d14 k(String str) {
            throw new UnsupportedOperationException("already specified useForNull");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ d14 f709a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(d14 d14, String str) {
            this.f709a = d14;
            i14.l(str);
            this.b = str;
        }

        @DexIgnore
        public /* synthetic */ b(d14 d14, String str, a aVar) {
            this(d14, str);
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public <A extends Appendable> A a(A a2, Iterator<? extends Map.Entry<?, ?>> it) throws IOException {
            i14.l(a2);
            if (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                a2.append(this.f709a.j(entry.getKey()));
                a2.append(this.b);
                a2.append(this.f709a.j(entry.getValue()));
                while (it.hasNext()) {
                    a2.append(this.f709a.f708a);
                    Map.Entry entry2 = (Map.Entry) it.next();
                    a2.append(this.f709a.j(entry2.getKey()));
                    a2.append(this.b);
                    a2.append(this.f709a.j(entry2.getValue()));
                }
            }
            return a2;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public StringBuilder b(StringBuilder sb, Iterable<? extends Map.Entry<?, ?>> iterable) {
            c(sb, iterable.iterator());
            return sb;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public StringBuilder c(StringBuilder sb, Iterator<? extends Map.Entry<?, ?>> it) {
            try {
                a(sb, it);
                return sb;
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public StringBuilder d(StringBuilder sb, Map<?, ?> map) {
            b(sb, map.entrySet());
            return sb;
        }
    }

    @DexIgnore
    public d14(d14 d14) {
        this.f708a = d14.f708a;
    }

    @DexIgnore
    public /* synthetic */ d14(d14 d14, a aVar) {
        this(d14);
    }

    @DexIgnore
    public d14(String str) {
        i14.l(str);
        this.f708a = str;
    }

    @DexIgnore
    public static d14 h(char c) {
        return new d14(String.valueOf(c));
    }

    @DexIgnore
    public static d14 i(String str) {
        return new d14(str);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <A extends Appendable> A b(A a2, Iterator<?> it) throws IOException {
        i14.l(a2);
        if (it.hasNext()) {
            a2.append(j(it.next()));
            while (it.hasNext()) {
                a2.append(this.f708a);
                a2.append(j(it.next()));
            }
        }
        return a2;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public final StringBuilder c(StringBuilder sb, Iterable<?> iterable) {
        d(sb, iterable.iterator());
        return sb;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public final StringBuilder d(StringBuilder sb, Iterator<?> it) {
        try {
            b(sb, it);
            return sb;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public final String e(Iterable<?> iterable) {
        return f(iterable.iterator());
    }

    @DexIgnore
    public final String f(Iterator<?> it) {
        StringBuilder sb = new StringBuilder();
        d(sb, it);
        return sb.toString();
    }

    @DexIgnore
    public final String g(Object[] objArr) {
        return e(Arrays.asList(objArr));
    }

    @DexIgnore
    public CharSequence j(Object obj) {
        i14.l(obj);
        return obj instanceof CharSequence ? (CharSequence) obj : obj.toString();
    }

    @DexIgnore
    public d14 k(String str) {
        i14.l(str);
        return new a(this, str);
    }

    @DexIgnore
    public b l(String str) {
        return new b(this, str, null);
    }
}
