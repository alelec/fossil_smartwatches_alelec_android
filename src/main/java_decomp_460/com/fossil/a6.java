package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a6 extends u5 {
    @DexIgnore
    public r4 k; // = r4.BOND_NONE;

    @DexIgnore
    public a6(n4 n4Var) {
        super(v5.k, n4Var);
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void d(k5 k5Var) {
        k5Var.x();
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void f(h7 h7Var) {
        s5 a2;
        k(h7Var);
        g7 g7Var = h7Var.f1435a;
        if (g7Var.b == f7.SUCCESS) {
            a2 = this.k == r4.BONDED ? s5.a(this.e, null, r5.b, null, 5) : s5.a(this.e, null, r5.e, null, 5);
        } else {
            s5 a3 = s5.e.a(g7Var);
            a2 = s5.a(this.e, null, a3.c, a3.d, 1);
        }
        this.e = a2;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public boolean i(h7 h7Var) {
        r4 r4Var;
        return (h7Var instanceof a7) && ((r4Var = ((a7) h7Var).b) == r4.BONDED || r4Var == r4.BOND_NONE);
    }

    @DexIgnore
    @Override // com.fossil.u5
    public fd0<h7> j() {
        return this.j.i;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void k(h7 h7Var) {
        this.k = ((a7) h7Var).b;
    }
}
