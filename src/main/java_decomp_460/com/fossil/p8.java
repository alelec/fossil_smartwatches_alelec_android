package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class p8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2797a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[r8.values().length];
        f2797a = iArr;
        iArr[r8.FIRE_TIME.ordinal()] = 1;
        f2797a[r8.TITLE.ordinal()] = 2;
        f2797a[r8.MESSAGE.ordinal()] = 3;
        int[] iArr2 = new int[r8.values().length];
        b = iArr2;
        iArr2[r8.FIRE_TIME.ordinal()] = 1;
        b[r8.TITLE.ordinal()] = 2;
        b[r8.MESSAGE.ordinal()] = 3;
    }
    */
}
