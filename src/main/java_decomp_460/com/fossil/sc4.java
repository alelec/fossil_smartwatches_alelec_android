package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sc4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ b94 f3238a;

    @DexIgnore
    public sc4(b94 b94) {
        this.f3238a = b94;
    }

    @DexIgnore
    public static tc4 a(int i) {
        return i != 3 ? new oc4() : new uc4();
    }

    @DexIgnore
    public ad4 b(JSONObject jSONObject) throws JSONException {
        return a(jSONObject.getInt("settings_version")).a(this.f3238a, jSONObject);
    }
}
