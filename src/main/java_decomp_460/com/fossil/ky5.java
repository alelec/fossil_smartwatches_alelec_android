package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ky5 extends u47 {
    @DexIgnore
    public static /* final */ String C;
    @DexIgnore
    public static /* final */ a D; // = new a(null);
    @DexIgnore
    public String A;
    @DexIgnore
    public HashMap B;
    @DexIgnore
    public /* final */ zp0 k; // = new sr4(this);
    @DexIgnore
    public g37<i15> l;
    @DexIgnore
    public List<? extends Date> m; // = hm7.e();
    @DexIgnore
    public Date s; // = xy4.f4212a.a();
    @DexIgnore
    public String[] t; // = new String[0];
    @DexIgnore
    public Long[] u; // = new Long[0];
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public ly5 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ky5.C;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ i15 f2116a;
        @DexIgnore
        public /* final */ /* synthetic */ ky5 b;

        @DexIgnore
        public b(i15 i15, ky5 ky5) {
            this.f2116a = i15;
            this.b = ky5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ky5.D.a();
            local.e(a2, "dateStrPicker.onValueChanged: old: " + i + " - newVal: " + i2);
            if (i2 == 0) {
                ky5 ky5 = this.b;
                ky5.P6(ky5.w);
                ky5 ky52 = this.b;
                ky52.R6(ky52.v);
                NumberPicker numberPicker2 = this.f2116a.u;
                pq7.b(numberPicker2, "hourPicker");
                if (numberPicker2.getValue() < this.b.w) {
                    ky5 ky53 = this.b;
                    ky53.x = ky53.w;
                    NumberPicker numberPicker3 = this.f2116a.u;
                    pq7.b(numberPicker3, "hourPicker");
                    numberPicker3.setValue(this.b.x);
                }
                NumberPicker numberPicker4 = this.f2116a.v;
                pq7.b(numberPicker4, "minutePicker");
                if (numberPicker4.getValue() < this.b.v) {
                    ky5 ky54 = this.b;
                    ky54.y = ky54.v;
                    NumberPicker numberPicker5 = this.f2116a.v;
                    pq7.b(numberPicker5, "minutePicker");
                    numberPicker5.setValue(this.b.v);
                    return;
                }
                return;
            }
            this.b.P6(0);
            NumberPicker numberPicker6 = this.f2116a.u;
            pq7.b(numberPicker6, "hourPicker");
            numberPicker6.setValue(this.b.x);
            this.b.R6(0);
            NumberPicker numberPicker7 = this.f2116a.v;
            pq7.b(numberPicker7, "minutePicker");
            numberPicker7.setValue(this.b.y);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ i15 f2117a;
        @DexIgnore
        public /* final */ /* synthetic */ ky5 b;

        @DexIgnore
        public c(i15 i15, ky5 ky5) {
            this.f2117a = i15;
            this.b = ky5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ky5.D.a();
            local.e(a2, "hourPicker.onValueChanged: old: " + i + " - newVal: " + i2);
            this.b.x = i2;
            if (i2 == this.b.w) {
                NumberPicker numberPicker2 = this.f2117a.s;
                pq7.b(numberPicker2, "dateStrPicker");
                if (numberPicker2.getValue() == 0) {
                    ky5 ky5 = this.b;
                    ky5.R6(ky5.v);
                    NumberPicker numberPicker3 = this.f2117a.v;
                    pq7.b(numberPicker3, "minutePicker");
                    if (numberPicker3.getValue() < this.b.v) {
                        ky5 ky52 = this.b;
                        ky52.y = ky52.v;
                        NumberPicker numberPicker4 = this.f2117a.v;
                        pq7.b(numberPicker4, "minutePicker");
                        numberPicker4.setValue(this.b.v);
                        return;
                    }
                    return;
                }
            }
            this.b.R6(0);
            NumberPicker numberPicker5 = this.f2117a.v;
            pq7.b(numberPicker5, "minutePicker");
            numberPicker5.setValue(this.b.y);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ky5 f2118a;

        @DexIgnore
        public d(ky5 ky5) {
            this.f2118a = ky5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            this.f2118a.y = i2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ky5.D.a();
            local.e(a2, "minutePicker.onValueChanged: old: " + i + " - newVal: " + i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ky5 b;

        @DexIgnore
        public e(ky5 ky5) {
            this.b = ky5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Date M6 = this.b.M6();
            ly5 ly5 = this.b.z;
            if (ly5 != null) {
                ly5.a(M6);
            }
            this.b.dismiss();
        }
    }

    /*
    static {
        String simpleName = ky5.class.getSimpleName();
        pq7.b(simpleName, "BottomDateTimeDialog::class.java.simpleName");
        C = simpleName;
    }
    */

    @DexIgnore
    public final void L6() {
        if (!this.m.isEmpty()) {
            g37<i15> g37 = this.l;
            if (g37 != null) {
                i15 a2 = g37.a();
                if (a2 != null) {
                    NumberPicker numberPicker = a2.s;
                    pq7.b(numberPicker, "dateStrPicker");
                    numberPicker.setMinValue(0);
                    NumberPicker numberPicker2 = a2.s;
                    pq7.b(numberPicker2, "dateStrPicker");
                    numberPicker2.setMaxValue(this.t.length - 1);
                    a2.s.setDisplayedValues(this.t);
                    P6(0);
                    R6(0);
                    NumberPicker numberPicker3 = a2.s;
                    pq7.b(numberPicker3, "dateStrPicker");
                    numberPicker3.setValue(1);
                    NumberPicker numberPicker4 = a2.u;
                    pq7.b(numberPicker4, "hourPicker");
                    numberPicker4.setValue(this.w);
                    NumberPicker numberPicker5 = a2.v;
                    pq7.b(numberPicker5, "minutePicker");
                    numberPicker5.setValue(this.v);
                    return;
                }
                return;
            }
            pq7.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public final Date M6() {
        g37<i15> g37 = this.l;
        if (g37 != null) {
            i15 a2 = g37.a();
            if (a2 == null) {
                return new Date();
            }
            Long[] lArr = this.u;
            NumberPicker numberPicker = a2.s;
            pq7.b(numberPicker, "dateStrPicker");
            long longValue = lArr[numberPicker.getValue()].longValue();
            NumberPicker numberPicker2 = a2.u;
            pq7.b(numberPicker2, "hourPicker");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = a2.v;
            pq7.b(numberPicker3, "minutePicker");
            return new Date(longValue + ((long) (value * 60 * 60 * 1000)) + ((long) (numberPicker3.getValue() * 60 * 1000)));
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final void N6() {
        if (!this.m.isEmpty()) {
            cl7<String[], Long[]> f = py4.f(this.m);
            this.t = f.getFirst();
            this.u = f.getSecond();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = C;
            StringBuilder sb = new StringBuilder();
            sb.append("dateStrValues: ");
            String[] strArr = this.t;
            ArrayList arrayList = new ArrayList(strArr.length);
            for (String str2 : strArr) {
                arrayList.add(str2);
            }
            sb.append(arrayList);
            sb.append(" - dateStrMetadata: ");
            Long[] lArr = this.u;
            ArrayList arrayList2 = new ArrayList(lArr.length);
            for (Long l2 : lArr) {
                arrayList2.add(Long.valueOf(l2.longValue()));
            }
            sb.append(arrayList2);
            local.e(str, sb.toString());
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "calendar");
            instance.setTime(this.s);
            this.w = instance.get(11);
            int i = instance.get(12);
            this.v = i;
            this.x = this.w;
            this.y = i;
        }
    }

    @DexIgnore
    public final void O6(List<? extends Date> list) {
        pq7.c(list, "newData");
        this.m = list;
        if (!list.isEmpty()) {
            this.s = (Date) list.get(0);
        }
    }

    @DexIgnore
    public final void P6(int i) {
        g37<i15> g37 = this.l;
        if (g37 != null) {
            i15 a2 = g37.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.u;
                pq7.b(numberPicker, "hourPicker");
                numberPicker.setMinValue(i);
                NumberPicker numberPicker2 = a2.u;
                pq7.b(numberPicker2, "hourPicker");
                numberPicker2.setMaxValue(23);
                return;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final void Q6(ly5 ly5) {
        pq7.c(ly5, "listener");
        this.z = ly5;
    }

    @DexIgnore
    public final void R6(int i) {
        g37<i15> g37 = this.l;
        if (g37 != null) {
            i15 a2 = g37.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.v;
                pq7.b(numberPicker, "minutePicker");
                numberPicker.setMinValue(i);
                NumberPicker numberPicker2 = a2.v;
                pq7.b(numberPicker2, "minutePicker");
                numberPicker2.setMaxValue(59);
                return;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final void S6() {
        g37<i15> g37 = this.l;
        if (g37 != null) {
            i15 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                pq7.b(flexibleTextView, "ftvTitle");
                flexibleTextView.setText(this.A);
                a2.s.setOnValueChangedListener(new b(a2, this));
                a2.u.setOnValueChangedListener(new c(a2, this));
                a2.v.setOnValueChangedListener(new d(this));
                a2.q.setOnClickListener(new e(this));
                return;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        i15 i15 = (i15) aq0.f(layoutInflater, 2131558442, viewGroup, false, this.k);
        this.l = new g37<>(this, i15);
        pq7.b(i15, "binding");
        return i15.n();
    }

    @DexIgnore
    @Override // com.fossil.u47, androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        S6();
        N6();
        L6();
    }

    @DexIgnore
    public final void setTitle(String str) {
        pq7.c(str, "title");
        this.A = str;
    }

    @DexIgnore
    @Override // com.fossil.u47
    public void z6() {
        HashMap hashMap = this.B;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
