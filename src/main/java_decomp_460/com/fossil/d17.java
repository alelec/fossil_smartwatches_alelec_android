package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.f17;
import com.fossil.hq4;
import com.fossil.nk5;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardActivity;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d17 extends qv5 implements t47.g {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public g37<fc5> h;
    @DexIgnore
    public f17 i;
    @DexIgnore
    public String j; // = qn5.l.a().d("primaryText");
    @DexIgnore
    public wa1 k;
    @DexIgnore
    public po4 l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return d17.s;
        }

        @DexIgnore
        public final d17 b() {
            return new d17();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<hq4.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ d17 f716a;

        @DexIgnore
        public b(d17 d17) {
            this.f716a = d17;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.b bVar) {
            if (bVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = d17.t.a();
                local.d(a2, "loadingState start " + bVar.a() + " stop " + bVar.b());
                if (bVar.a()) {
                    this.f716a.i();
                }
                if (bVar.b()) {
                    this.f716a.h();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<hq4.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ d17 f717a;

        @DexIgnore
        public c(d17 d17) {
            this.f717a = d17;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.c cVar) {
            if (!cVar.a().isEmpty()) {
                d17 d17 = this.f717a;
                Object[] array = cVar.a().toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    d17.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<f17.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ d17 f718a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public d(d17 d17, String str) {
            this.f718a = d17;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(f17.c cVar) {
            if (cVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = d17.t.a();
                local.d(a2, "onUIState changed, modelWrapper=" + cVar);
                if (cVar.k() != null) {
                    d17 d17 = this.f718a;
                    f17.d k = cVar.k();
                    if (k != null) {
                        d17.w3(k);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                if (cVar.a()) {
                    this.f718a.e0();
                }
                if (cVar.l() != null) {
                    d17 d172 = this.f718a;
                    String l = cVar.l();
                    if (l != null) {
                        d172.W6(l);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                Integer m = cVar.m();
                if (m != null) {
                    m.intValue();
                    d17 d173 = this.f718a;
                    Integer m2 = cVar.m();
                    if (m2 != null) {
                        d173.X6(m2.intValue());
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                if (cVar.f() != null) {
                    d17 d174 = this.f718a;
                    cl7<Integer, String> f = cVar.f();
                    if (f != null) {
                        int intValue = f.getFirst().intValue();
                        cl7<Integer, String> f2 = cVar.f();
                        if (f2 != null) {
                            d174.o(intValue, f2.getSecond());
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                if (cVar.d()) {
                    this.f718a.Q6(this.b);
                }
                if (cVar.c() != null) {
                    d17 d175 = this.f718a;
                    String c = cVar.c();
                    if (c != null) {
                        d175.P6(c);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                if (cVar.i() != null) {
                    d17 d176 = this.f718a;
                    String i = cVar.i();
                    if (i != null) {
                        d176.T6(i);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                if (cVar.e() != null) {
                    d17 d177 = this.f718a;
                    String e = cVar.e();
                    if (e != null) {
                        d177.R6(e);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                if (cVar.b() != null) {
                    d17 d178 = this.f718a;
                    String b2 = cVar.b();
                    if (b2 != null) {
                        d178.O6(b2);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                if (cVar.h() != null) {
                    d17 d179 = this.f718a;
                    String h = cVar.h();
                    if (h != null) {
                        d179.S6(h);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                if (cVar.g()) {
                    this.f718a.c();
                }
                if (cVar.j()) {
                    this.f718a.V6();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<f17.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ d17 f719a;

        @DexIgnore
        public e(d17 d17) {
            this.f719a = d17;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(f17.a aVar) {
            boolean b = aVar.b();
            boolean c = aVar.c();
            if (b) {
                String c2 = c ? um5.c(PortfolioApp.h0.c(), 2131886257) : um5.c(PortfolioApp.h0.c(), 2131886256);
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = this.f719a.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                pq7.b(c2, "title");
                String c3 = um5.c(PortfolioApp.h0.c(), 2131886242);
                pq7.b(c3, "LanguageHelper.getString\u2026ourCurrentChallengeFirst)");
                s37.o(childFragmentManager, c2, c3, aVar.a());
            } else if (!c) {
                s37 s372 = s37.c;
                FragmentManager childFragmentManager2 = this.f719a.getChildFragmentManager();
                pq7.b(childFragmentManager2, "childFragmentManager");
                s372.d0(childFragmentManager2, d17.K6(this.f719a).L());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d17 b;

        @DexIgnore
        public f(d17 d17) {
            this.b = d17;
        }

        @DexIgnore
        public final void onClick(View view) {
            d17.K6(this.b).G(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d17 b;

        @DexIgnore
        public g(d17 d17) {
            this.b = d17;
        }

        @DexIgnore
        public final void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = d17.t.a();
            local.d(a2, "getWatchSerial=" + d17.K6(this.b).P());
            if (this.b.isActive() && !TextUtils.isEmpty(d17.K6(this.b).P())) {
                FindDeviceActivity.a aVar = FindDeviceActivity.B;
                FragmentActivity requireActivity = this.b.requireActivity();
                pq7.b(requireActivity, "requireActivity()");
                String P = d17.K6(this.b).P();
                if (P != null) {
                    aVar.a(requireActivity, P);
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d17 b;

        @DexIgnore
        public h(d17 d17) {
            this.b = d17;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.isActive() && !TextUtils.isEmpty(d17.K6(this.b).P())) {
                CalibrationActivity.a aVar = CalibrationActivity.B;
                FragmentActivity requireActivity = this.b.requireActivity();
                pq7.b(requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d17 b;

        @DexIgnore
        public i(d17 d17) {
            this.b = d17;
        }

        @DexIgnore
        public final void onClick(View view) {
            d17.K6(this.b).f0(100);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d17 b;

        @DexIgnore
        public j(d17 d17) {
            this.b = d17;
        }

        @DexIgnore
        public final void onClick(View view) {
            d17.K6(this.b).f0(50);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d17 b;

        @DexIgnore
        public k(d17 d17) {
            this.b = d17;
        }

        @DexIgnore
        public final void onClick(View view) {
            d17.K6(this.b).f0(25);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d17 b;

        @DexIgnore
        public l(d17 d17) {
            this.b = d17;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d17 b;

        @DexIgnore
        public m(d17 d17) {
            this.b = d17;
        }

        @DexIgnore
        public final void onClick(View view) {
            d17.K6(this.b).F();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements CloudImageHelper.OnImageCallbackListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fc5 f720a;
        @DexIgnore
        public /* final */ /* synthetic */ d17 b;

        @DexIgnore
        public n(fc5 fc5, d17 d17, f17.d dVar) {
            this.f720a = fc5;
            this.b = d17;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            pq7.c(str, "serial");
            pq7.c(str2, "filePath");
            this.b.N6().t(str2).F0(this.f720a.z);
        }
    }

    /*
    static {
        String simpleName = d17.class.getSimpleName();
        pq7.b(simpleName, "WatchSettingFragment::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ f17 K6(d17 d17) {
        f17 f17 = d17.i;
        if (f17 != null) {
            return f17;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return s;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    public final String M6(boolean z, int i2) {
        String c2 = um5.c(PortfolioApp.h0.c(), z ? 2131887198 : 2131887173);
        if (!z || i2 <= 0) {
            pq7.b(c2, "connectedString");
            return c2;
        }
        return c2 + ", " + i2 + '%';
    }

    @DexIgnore
    public final wa1 N6() {
        wa1 wa1 = this.k;
        if (wa1 != null) {
            return wa1;
        }
        pq7.n("mRequestManager");
        throw null;
    }

    @DexIgnore
    public final void O6(String str) {
        pq7.c(str, "serial");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.e0(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void P6(String str) {
        pq7.c(str, "serial");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.v0(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void Q6(String str) {
        pq7.c(str, "serial");
        if (getActivity() != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            Context requireContext = requireContext();
            pq7.b(requireContext, "requireContext()");
            TroubleshootingActivity.a.c(aVar, requireContext, str, false, false, 12, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        switch (str.hashCode()) {
            case -2051261777:
                if (str.equals("REMOVE_DEVICE_WORKOUT")) {
                    String stringExtra = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra == null) {
                        return;
                    }
                    if (i2 == 2131363291) {
                        f17 f17 = this.i;
                        if (f17 != null) {
                            f17.W(stringExtra);
                            return;
                        } else {
                            pq7.n("mViewModel");
                            throw null;
                        }
                    } else if (i2 == 2131363373) {
                        f17 f172 = this.i;
                        if (f172 != null) {
                            f172.S(stringExtra);
                            return;
                        } else {
                            pq7.n("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -1138109835:
                if (str.equals("SWITCH_DEVICE_ERASE_FAIL")) {
                    String stringExtra2 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra2 != null && i2 == 2131363373) {
                        f17 f173 = this.i;
                        if (f173 != null) {
                            f173.U(stringExtra2);
                            return;
                        } else {
                            pq7.n("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -693701870:
                if (str.equals("CONFIRM_REMOVE_DEVICE")) {
                    if (i2 == 2131363373) {
                        f17 f174 = this.i;
                        if (f174 != null) {
                            f174.Z();
                            return;
                        } else {
                            pq7.n("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -454228492:
                if (str.equals("REMOVE_DEVICE_SYNC_FAIL")) {
                    String stringExtra3 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra3 != null && i2 == 2131363373) {
                        f17 f175 = this.i;
                        if (f175 != null) {
                            f175.R(stringExtra3);
                            return;
                        } else {
                            pq7.n("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 39550276:
                if (str.equals("SWITCH_DEVICE_SYNC_FAIL")) {
                    String stringExtra4 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra4 != null && i2 == 2131363373) {
                        f17 f176 = this.i;
                        if (f176 != null) {
                            f176.V(stringExtra4);
                            return;
                        } else {
                            pq7.n("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 603997695:
                if (str.equals("SWITCH_DEVICE_WORKOUT")) {
                    String stringExtra5 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra5 == null) {
                        return;
                    }
                    if (i2 == 2131363291) {
                        f17 f177 = this.i;
                        if (f177 != null) {
                            f177.X(stringExtra5);
                            return;
                        } else {
                            pq7.n("mViewModel");
                            throw null;
                        }
                    } else if (i2 == 2131363373) {
                        f17 f178 = this.i;
                        if (f178 != null) {
                            f178.T(stringExtra5);
                            return;
                        } else {
                            pq7.n("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 1970588827:
                if (str.equals("LEAVE_CHALLENGE")) {
                    a();
                    if (i2 == 2131363373) {
                        ps4 ps4 = intent != null ? (ps4) intent.getParcelableExtra("CHALLENGE") : null;
                        if (ps4 != null) {
                            U6(ps4);
                            return;
                        }
                        return;
                    }
                    return;
                }
                break;
        }
        super.R5(str, i2, intent);
    }

    @DexIgnore
    public final void R6(String str) {
        pq7.c(str, "serial");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.u0(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void S6(String str) {
        pq7.c(str, "serial");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.f0(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void T6(String str) {
        pq7.c(str, "serial");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.w0(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void U6(ps4 ps4) {
        long b2 = xy4.f4212a.b();
        Date m2 = ps4.m();
        if (b2 > (m2 != null ? m2.getTime() : 0)) {
            BCOverviewLeaderBoardActivity.A.a(this, ps4, null);
        } else {
            BCWaitingChallengeDetailActivity.B.a(this, ps4, "joined_challenge", -1, false);
        }
    }

    @DexIgnore
    public final void V6() {
        FlexibleButton flexibleButton;
        g37<fc5> g37 = this.h;
        if (g37 != null) {
            fc5 a2 = g37.a();
            if (a2 != null && (flexibleButton = a2.s) != null) {
                flexibleButton.setText(um5.c(PortfolioApp.h0.c(), 2131887184));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void W6(String str) {
        pq7.c(str, "lastSync");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = s;
        local.d(str2, "updateLastSync, lastSync=" + str);
        if (isActive()) {
            g37<fc5> g37 = this.h;
            if (g37 != null) {
                fc5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.M;
                    pq7.b(flexibleTextView, "it.tvLastSyncValue");
                    flexibleTextView.setText(str);
                    FlexibleTextView flexibleTextView2 = a2.M;
                    pq7.b(flexibleTextView2, "it.tvLastSyncValue");
                    flexibleTextView2.setSelected(true);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void X6(int i2) {
        g37<fc5> g37 = this.h;
        if (g37 != null) {
            fc5 a2 = g37.a();
            if (a2 != null) {
                a2.w.d("flexible_button_secondary");
                a2.y.d("flexible_button_secondary");
                a2.x.d("flexible_button_secondary");
                if (i2 == 25) {
                    a2.x.d("flexible_button_primary");
                } else if (i2 == 50) {
                    a2.y.d("flexible_button_primary");
                } else if (i2 == 100) {
                    a2.w.d("flexible_button_primary");
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void c() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    public final void e0() {
        FragmentActivity activity;
        FLogger.INSTANCE.getLocal().d(s, VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        if (isActive() && (activity = getActivity()) != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d(s, "stopLoading");
        a();
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d(s, "startLoading");
        b();
    }

    @DexIgnore
    public final void o(int i2, String str) {
        pq7.c(str, "message");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        fc5 fc5 = (fc5) aq0.f(layoutInflater, 2131558639, viewGroup, false, A6());
        PortfolioApp.h0.c().M().K0().a(this);
        po4 po4 = this.l;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(f17.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ingViewModel::class.java)");
            f17 f17 = (f17) a2;
            this.i = f17;
            if (f17 != null) {
                f17.b0();
                Bundle arguments = getArguments();
                Object obj = arguments != null ? arguments.get("SERIAL") : null;
                if (obj != null) {
                    String str = (String) obj;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = s;
                    local.d(str2, "serial=" + str);
                    f17 f172 = this.i;
                    if (f172 != null) {
                        f172.a0(str);
                        f17 f173 = this.i;
                        if (f173 != null) {
                            f173.j().h(getViewLifecycleOwner(), new b(this));
                            f17 f174 = this.i;
                            if (f174 != null) {
                                f174.l().h(getViewLifecycleOwner(), new c(this));
                                f17 f175 = this.i;
                                if (f175 != null) {
                                    f175.O().h(getViewLifecycleOwner(), new d(this, str));
                                    f17 f176 = this.i;
                                    if (f176 != null) {
                                        if (f176.g0()) {
                                            FlexibleTextView flexibleTextView = fc5.R;
                                            pq7.b(flexibleTextView, "bindingLocal.tvVibration");
                                            flexibleTextView.setVisibility(0);
                                            FlexibleButton flexibleButton = fc5.x;
                                            pq7.b(flexibleButton, "bindingLocal.fbVibrationLow");
                                            flexibleButton.setVisibility(0);
                                            FlexibleButton flexibleButton2 = fc5.y;
                                            pq7.b(flexibleButton2, "bindingLocal.fbVibrationMedium");
                                            flexibleButton2.setVisibility(0);
                                            FlexibleButton flexibleButton3 = fc5.w;
                                            pq7.b(flexibleButton3, "bindingLocal.fbVibrationHigh");
                                            flexibleButton3.setVisibility(0);
                                        } else {
                                            FlexibleTextView flexibleTextView2 = fc5.R;
                                            pq7.b(flexibleTextView2, "bindingLocal.tvVibration");
                                            flexibleTextView2.setVisibility(8);
                                            FlexibleButton flexibleButton4 = fc5.x;
                                            pq7.b(flexibleButton4, "bindingLocal.fbVibrationLow");
                                            flexibleButton4.setVisibility(8);
                                            FlexibleButton flexibleButton5 = fc5.y;
                                            pq7.b(flexibleButton5, "bindingLocal.fbVibrationMedium");
                                            flexibleButton5.setVisibility(8);
                                            FlexibleButton flexibleButton6 = fc5.w;
                                            pq7.b(flexibleButton6, "bindingLocal.fbVibrationHigh");
                                            flexibleButton6.setVisibility(8);
                                        }
                                        f17 f177 = this.i;
                                        if (f177 != null) {
                                            f177.K().h(getViewLifecycleOwner(), new e(this));
                                            g37<fc5> g37 = new g37<>(this, fc5);
                                            this.h = g37;
                                            if (g37 != null) {
                                                fc5 a3 = g37.a();
                                                if (a3 != null) {
                                                    pq7.b(a3, "mBinding.get()!!");
                                                    return a3.n();
                                                }
                                                pq7.i();
                                                throw null;
                                            }
                                            pq7.n("mBinding");
                                            throw null;
                                        }
                                        pq7.n("mViewModel");
                                        throw null;
                                    }
                                    pq7.n("mViewModel");
                                    throw null;
                                }
                                pq7.n("mViewModel");
                                throw null;
                            }
                            pq7.n("mViewModel");
                            throw null;
                        }
                        pq7.n("mViewModel");
                        throw null;
                    }
                    pq7.n("mViewModel");
                    throw null;
                }
                throw new il7("null cannot be cast to non-null type kotlin.String");
            }
            pq7.n("mViewModel");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        f17 f17 = this.i;
        if (f17 != null) {
            f17.c0();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        f17 f17 = this.i;
        if (f17 != null) {
            f17.b0();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ConstraintLayout constraintLayout;
        ConstraintLayout constraintLayout2;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        wa1 v = oa1.v(this);
        pq7.b(v, "Glide.with(this)");
        this.k = v;
        g37<fc5> g37 = this.h;
        if (g37 != null) {
            fc5 a2 = g37.a();
            if (a2 != null) {
                a2.N.setOnClickListener(new f(this));
                a2.I.setOnClickListener(new g(this));
                a2.E.setOnClickListener(new h(this));
                String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
                if (!TextUtils.isEmpty(d2) && (constraintLayout2 = a2.u) != null) {
                    constraintLayout2.setBackgroundColor(Color.parseColor(d2));
                }
                String d3 = qn5.l.a().d("nonBrandSurface");
                if (!TextUtils.isEmpty(d3) && (constraintLayout = a2.t) != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(d3));
                }
                a2.w.setOnClickListener(new i(this));
                a2.y.setOnClickListener(new j(this));
                a2.x.setOnClickListener(new k(this));
                a2.r.setOnClickListener(new l(this));
                a2.s.setOnClickListener(new m(this));
            }
            E6("watch_setting_view");
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void w3(f17.d dVar) {
        pq7.c(dVar, "watchSetting");
        FLogger.INSTANCE.getLocal().d(s, "updateDeviceInfo");
        if (isActive()) {
            g37<fc5> g37 = this.h;
            if (g37 != null) {
                fc5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.P;
                    pq7.b(flexibleTextView, "it.tvSerialValue");
                    flexibleTextView.setText(dVar.a().getDeviceId());
                    FlexibleTextView flexibleTextView2 = a2.K;
                    pq7.b(flexibleTextView2, "it.tvFwVersionValue");
                    flexibleTextView2.setText(dVar.a().getFirmwareRevision());
                    FlexibleTextView flexibleTextView3 = a2.H;
                    pq7.b(flexibleTextView3, "it.tvDeviceName");
                    flexibleTextView3.setText(dVar.b());
                    boolean d2 = dVar.d();
                    if (d2) {
                        Boolean c2 = dVar.c();
                        if (c2 != null && c2.booleanValue()) {
                            Integer vibrationStrength = dVar.a().getVibrationStrength();
                            if (vibrationStrength != null) {
                                X6(vibrationStrength.intValue());
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                        if (dVar.e()) {
                            FlexibleTextView flexibleTextView4 = a2.H;
                            pq7.b(flexibleTextView4, "it.tvDeviceName");
                            flexibleTextView4.setAlpha(1.0f);
                            a2.H.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, gl0.f(PortfolioApp.h0.c(), 2131231075), (Drawable) null);
                            FlexibleTextView flexibleTextView5 = a2.G;
                            pq7.b(flexibleTextView5, "it.tvConnectionStatus");
                            flexibleTextView5.setAlpha(1.0f);
                            FlexibleTextView flexibleTextView6 = a2.G;
                            pq7.b(flexibleTextView6, "it.tvConnectionStatus");
                            flexibleTextView6.setText(M6(true, dVar.a().getBatteryLevel()));
                            if (dVar.a().getBatteryLevel() >= 0) {
                                int batteryLevel = dVar.a().getBatteryLevel();
                                a2.G.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, gl0.f(PortfolioApp.h0.c(), (batteryLevel >= 0 && 25 >= batteryLevel) ? 2131231008 : (25 <= batteryLevel && 50 >= batteryLevel) ? 2131231010 : (50 <= batteryLevel && 75 >= batteryLevel) ? 2131231012 : 2131231006), (Drawable) null);
                            }
                            FlexibleButton flexibleButton = a2.q;
                            pq7.b(flexibleButton, "it.btActive");
                            flexibleButton.setVisibility(0);
                            FlexibleButton flexibleButton2 = a2.s;
                            pq7.b(flexibleButton2, "it.btConnect");
                            flexibleButton2.setVisibility(8);
                            FlexibleTextView flexibleTextView7 = a2.E;
                            pq7.b(flexibleTextView7, "it.tvCalibration");
                            flexibleTextView7.setAlpha(1.0f);
                            FlexibleButton flexibleButton3 = a2.w;
                            pq7.b(flexibleButton3, "it.fbVibrationHigh");
                            flexibleButton3.setEnabled(true);
                            FlexibleButton flexibleButton4 = a2.x;
                            pq7.b(flexibleButton4, "it.fbVibrationLow");
                            flexibleButton4.setEnabled(true);
                            FlexibleButton flexibleButton5 = a2.y;
                            pq7.b(flexibleButton5, "it.fbVibrationMedium");
                            flexibleButton5.setEnabled(true);
                            FlexibleTextView flexibleTextView8 = a2.E;
                            pq7.b(flexibleTextView8, "it.tvCalibration");
                            flexibleTextView8.setEnabled(true);
                        } else {
                            a2.H.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                            String str = this.j;
                            if (str != null) {
                                a2.H.setTextColor(Color.parseColor(str));
                            }
                            FlexibleTextView flexibleTextView9 = a2.H;
                            pq7.b(flexibleTextView9, "it.tvDeviceName");
                            flexibleTextView9.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView10 = a2.G;
                            pq7.b(flexibleTextView10, "it.tvConnectionStatus");
                            flexibleTextView10.setText(M6(false, dVar.a().getBatteryLevel()));
                            FlexibleTextView flexibleTextView11 = a2.G;
                            pq7.b(flexibleTextView11, "it.tvConnectionStatus");
                            flexibleTextView11.setAlpha(0.4f);
                            FlexibleButton flexibleButton6 = a2.q;
                            pq7.b(flexibleButton6, "it.btActive");
                            flexibleButton6.setVisibility(8);
                            FlexibleButton flexibleButton7 = a2.s;
                            pq7.b(flexibleButton7, "it.btConnect");
                            flexibleButton7.setVisibility(0);
                            FlexibleButton flexibleButton8 = a2.s;
                            pq7.b(flexibleButton8, "it.btConnect");
                            flexibleButton8.setText(um5.c(PortfolioApp.h0.c(), 2131887184));
                            FlexibleTextView flexibleTextView12 = a2.E;
                            pq7.b(flexibleTextView12, "it.tvCalibration");
                            flexibleTextView12.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView13 = a2.E;
                            pq7.b(flexibleTextView13, "it.tvCalibration");
                            flexibleTextView13.setEnabled(false);
                        }
                    } else if (!d2) {
                        a2.w.d("flexible_button_secondary");
                        a2.y.d("flexible_button_secondary");
                        a2.x.d("flexible_button_secondary");
                        a2.H.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                        String str2 = this.j;
                        if (str2 != null) {
                            a2.H.setTextColor(Color.parseColor(str2));
                        }
                        FlexibleTextView flexibleTextView14 = a2.H;
                        pq7.b(flexibleTextView14, "it.tvDeviceName");
                        flexibleTextView14.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView15 = a2.G;
                        pq7.b(flexibleTextView15, "it.tvConnectionStatus");
                        flexibleTextView15.setText(M6(false, dVar.a().getBatteryLevel()));
                        FlexibleTextView flexibleTextView16 = a2.G;
                        pq7.b(flexibleTextView16, "it.tvConnectionStatus");
                        flexibleTextView16.setAlpha(0.4f);
                        FlexibleButton flexibleButton9 = a2.q;
                        pq7.b(flexibleButton9, "it.btActive");
                        flexibleButton9.setVisibility(8);
                        FlexibleButton flexibleButton10 = a2.s;
                        pq7.b(flexibleButton10, "it.btConnect");
                        flexibleButton10.setText(um5.c(PortfolioApp.h0.c(), 2131887185));
                        FlexibleTextView flexibleTextView17 = a2.E;
                        pq7.b(flexibleTextView17, "it.tvCalibration");
                        flexibleTextView17.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView18 = a2.E;
                        pq7.b(flexibleTextView18, "it.tvCalibration");
                        flexibleTextView18.setEnabled(false);
                    }
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(dVar.a().getDeviceId()).setSerialPrefix(nk5.o.m(dVar.a().getDeviceId())).setType(Constants.DeviceType.TYPE_LARGE);
                    ImageView imageView = a2.z;
                    pq7.b(imageView, "it.ivDevice");
                    type.setPlaceHolder(imageView, nk5.o.i(dVar.a().getDeviceId(), nk5.b.SMALL)).setImageCallback(new n(a2, this, dVar)).download();
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }
}
