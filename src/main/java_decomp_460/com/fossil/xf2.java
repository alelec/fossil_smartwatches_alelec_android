package com.fossil;

import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xf2 {
    @DexIgnore
    public static boolean a() {
        return Looper.getMainLooper() == Looper.myLooper();
    }
}
