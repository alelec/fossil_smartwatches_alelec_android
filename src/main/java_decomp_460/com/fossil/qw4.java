package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qw4 implements Factory<ow4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<tt4> f3035a;
    @DexIgnore
    public /* final */ Provider<zt4> b;
    @DexIgnore
    public /* final */ Provider<on5> c;

    @DexIgnore
    public qw4(Provider<tt4> provider, Provider<zt4> provider2, Provider<on5> provider3) {
        this.f3035a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static qw4 a(Provider<tt4> provider, Provider<zt4> provider2, Provider<on5> provider3) {
        return new qw4(provider, provider2, provider3);
    }

    @DexIgnore
    public static ow4 c(tt4 tt4, zt4 zt4, on5 on5) {
        return new ow4(tt4, zt4, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public ow4 get() {
        return c(this.f3035a.get(), this.b.get(), this.c.get());
    }
}
