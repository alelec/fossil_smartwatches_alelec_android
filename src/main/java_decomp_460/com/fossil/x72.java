package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x72 {
    @DexIgnore
    public static void a(Status status, ot3<Void> ot3) {
        b(status, null, ot3);
    }

    @DexIgnore
    public static <TResult> void b(Status status, TResult tresult, ot3<TResult> ot3) {
        if (status.D()) {
            ot3.c(tresult);
        } else {
            ot3.b(new n62(status));
        }
    }

    @DexIgnore
    @Deprecated
    public static nt3<Void> c(nt3<Boolean> nt3) {
        return nt3.h(new ba2());
    }
}
