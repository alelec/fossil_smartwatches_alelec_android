package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.wearables.fsl.appfilter.AppFilterProviderImpl;
import com.fossil.wearables.fsl.codeword.CodeWordProviderImpl;
import com.fossil.wearables.fsl.contact.ContactProviderImpl;
import com.fossil.wearables.fsl.fitness.FitnessProviderFactory;
import com.fossil.wearables.fsl.goal.GoalProviderImpl;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingProviderImpl;
import com.fossil.wearables.fsl.history.HistoryProviderImpl;
import com.fossil.wearables.fsl.keyvalue.KeyValueProviderImpl;
import com.fossil.wearables.fsl.location.LocationProviderImpl;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProviderImp;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.db.DataFileProvider;
import com.misfit.frameworks.buttonservice.db.DataLogServiceProvider;
import com.misfit.frameworks.buttonservice.db.HeartRateProvider;
import com.misfit.frameworks.buttonservice.db.HwLogProvider;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.db.DBConstants;
import com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProviderImp;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProviderImp;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kk5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ kk5 f1925a; // = new kk5();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Context b;

        @DexIgnore
        public a(Context context) {
            this.b = context;
        }

        @DexIgnore
        public final void run() {
            Toast.makeText(this.b, "DB Exported!", 1).show();
        }
    }

    @DexIgnore
    public final List<String> a(Context context, String str, String str2) {
        pq7.c(context, "context");
        pq7.c(str, ButtonService.USER_ID);
        pq7.c(str2, "outputDir");
        FLogger.INSTANCE.getLocal().d("TAG", "------------ userId=" + str + ", pkg=" + context.getPackageName());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("------------ userId=");
        sb.append(str);
        sb.append(", path=");
        sb.append(context.getDatabasePath(str + LocaleConverter.LOCALE_DELIMITER + FitnessProviderFactory.DB_NAME));
        local.d("TAG", sb.toString());
        File file = new File("/data/data/" + context.getPackageName() + "/databases/");
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("FileName: ");
                pq7.b(file2, "file");
                sb2.append(file2.getName());
                local2.d("Files", sb2.toString());
            }
        }
        for (int i = 0; i < 15; i++) {
            String str3 = new String[]{"alarm.db", SecondTimezoneProviderImp.DB_NAME, ContactProviderImpl.DB_NAME, AppFilterProviderImpl.DB_NAME, GoalProviderImpl.DB_NAME, HistoryProviderImpl.DB_NAME, LocationProviderImpl.DB_NAME, CodeWordProviderImpl.DB_NAME, KeyValueProviderImpl.DB_NAME, "pin.db", "firmwares.db", DeviceProviderImp.DB_NAME, GoalTrackingProviderImpl.DB_NAME, "microAppSetting.db", "serverSetting.db"}[i];
            if (!arrayList.contains(str3)) {
                arrayList.add(b(context, str + LocaleConverter.LOCALE_DELIMITER + str3, str2));
            }
        }
        arrayList.add(b(context, "hybridCustomize.db", str2));
        arrayList.add(b(context, "hybridCustomize.db-wal", str2));
        arrayList.add(b(context, "buddy_challenge.db", str2));
        arrayList.add(b(context, "buddy_challenge.db-wal", str2));
        arrayList.add(b(context, "dianaCustomize.db", str2));
        arrayList.add(b(context, "dianaCustomize.db-wal", str2));
        arrayList.add(b(context, "devices.db", str2));
        arrayList.add(b(context, "devices.db-wal", str2));
        arrayList.add(b(context, "category.db", str2));
        arrayList.add(b(context, "category.db-wal", str2));
        arrayList.add(b(context, "goalTracking.db", str2));
        arrayList.add(b(context, "goalTracking.db-wal", str2));
        arrayList.add(b(context, "theme.db", str2));
        arrayList.add(b(context, "theme.db-wal", str2));
        arrayList.add(b(context, "quickResponse.db", str2));
        arrayList.add(b(context, "quickResponse.db-wal", str2));
        arrayList.add(b(context, "file.db", str2));
        arrayList.add(b(context, "file.db-wal", str2));
        arrayList.add(b(context, "heartRate.db", str2));
        arrayList.add(b(context, "heartRate.db-wal", str2));
        arrayList.add(b(context, "fitnessData.db", str2));
        arrayList.add(b(context, "fitnessData.db-wal", str2));
        arrayList.add(b(context, DBConstants.LOG_DB_NAME, str2));
        arrayList.add(b(context, "log_db.db-wal", str2));
        String str4 = str + LocaleConverter.LOCALE_DELIMITER;
        arrayList.add(b(context, str4 + FitnessProviderFactory.DB_NAME, str2));
        arrayList.add(b(context, str4 + "fitness.db-wal", str2));
        arrayList.add(b(context, str4 + MFSleepSessionProviderImp.DB_NAME, str2));
        arrayList.add(b(context, str4 + "sleep.db-wal", str2));
        if (!pq7.a(str, "Anonymous")) {
            arrayList.add(b(context, "Anonymous_fitness.db", str2));
            arrayList.add(b(context, "Anonymous_fitness.db-wal", str2));
            arrayList.add(b(context, "Anonymous_sleep.db", str2));
            arrayList.add(b(context, "Anonymous_sleep.db-wal", str2));
        }
        arrayList.add(b(context, "thirdParty.db", str2));
        arrayList.add(b(context, "thirdParty.db-wal", str2));
        arrayList.add(b(context, "user.db", str2));
        arrayList.add(b(context, "user.db-wal", str2));
        arrayList.add(b(context, "workoutSetting.db", str2));
        arrayList.add(b(context, "workoutSetting.db-wal", str2));
        for (int i2 = 0; i2 < 5; i2++) {
            arrayList.add(b(context, new String[]{DataLogServiceProvider.DB_NAME, DataFileProvider.DB_NAME, HeartRateProvider.DB_NAME, HwLogProvider.DB_NAME, "ParseOfflineStore"}[i2], str2));
        }
        ((Activity) context).runOnUiThread(new a(context));
        return arrayList;
    }

    @DexIgnore
    public final String b(Context context, String str, String str2) {
        File databasePath = context.getDatabasePath(str);
        pq7.b(databasePath, "context.getDatabasePath(databaseName)");
        File file = new File(databasePath.getAbsolutePath());
        File file2 = new File(str2, str);
        try {
            FileChannel channel = new FileInputStream(file).getChannel();
            pq7.b(channel, "FileInputStream(currentDB).channel");
            FileChannel channel2 = new FileOutputStream(file2).getChannel();
            pq7.b(channel2, "FileOutputStream(backupDB).channel");
            channel2.transferFrom(channel, 0, channel.size());
            channel.close();
            channel2.close();
            String absolutePath = file2.getAbsolutePath();
            pq7.b(absolutePath, "backupDB.absolutePath");
            return absolutePath;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    @DexIgnore
    public final String c(Context context, String str) {
        pq7.c(context, "context");
        pq7.c(str, "outputDir");
        return b(context, HwLogProvider.DB_NAME, str);
    }
}
