package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class se0 {
    @DexIgnore
    public static /* final */ int abc_action_bar_home_description; // = 2131887235;
    @DexIgnore
    public static /* final */ int abc_action_bar_up_description; // = 2131887236;
    @DexIgnore
    public static /* final */ int abc_action_menu_overflow_description; // = 2131887237;
    @DexIgnore
    public static /* final */ int abc_action_mode_done; // = 2131887238;
    @DexIgnore
    public static /* final */ int abc_activity_chooser_view_see_all; // = 2131887239;
    @DexIgnore
    public static /* final */ int abc_activitychooserview_choose_application; // = 2131887240;
    @DexIgnore
    public static /* final */ int abc_capital_off; // = 2131887241;
    @DexIgnore
    public static /* final */ int abc_capital_on; // = 2131887242;
    @DexIgnore
    public static /* final */ int abc_menu_alt_shortcut_label; // = 2131887243;
    @DexIgnore
    public static /* final */ int abc_menu_ctrl_shortcut_label; // = 2131887244;
    @DexIgnore
    public static /* final */ int abc_menu_delete_shortcut_label; // = 2131887245;
    @DexIgnore
    public static /* final */ int abc_menu_enter_shortcut_label; // = 2131887246;
    @DexIgnore
    public static /* final */ int abc_menu_function_shortcut_label; // = 2131887247;
    @DexIgnore
    public static /* final */ int abc_menu_meta_shortcut_label; // = 2131887248;
    @DexIgnore
    public static /* final */ int abc_menu_shift_shortcut_label; // = 2131887249;
    @DexIgnore
    public static /* final */ int abc_menu_space_shortcut_label; // = 2131887250;
    @DexIgnore
    public static /* final */ int abc_menu_sym_shortcut_label; // = 2131887251;
    @DexIgnore
    public static /* final */ int abc_prepend_shortcut_label; // = 2131887252;
    @DexIgnore
    public static /* final */ int abc_search_hint; // = 2131887253;
    @DexIgnore
    public static /* final */ int abc_searchview_description_clear; // = 2131887254;
    @DexIgnore
    public static /* final */ int abc_searchview_description_query; // = 2131887255;
    @DexIgnore
    public static /* final */ int abc_searchview_description_search; // = 2131887256;
    @DexIgnore
    public static /* final */ int abc_searchview_description_submit; // = 2131887257;
    @DexIgnore
    public static /* final */ int abc_searchview_description_voice; // = 2131887258;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with; // = 2131887259;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with_application; // = 2131887260;
    @DexIgnore
    public static /* final */ int abc_toolbar_collapse_description; // = 2131887261;
    @DexIgnore
    public static /* final */ int search_menu_title; // = 2131887559;
    @DexIgnore
    public static /* final */ int status_bar_notification_info_overflow; // = 2131887575;
}
