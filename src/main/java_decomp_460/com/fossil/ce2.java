package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ce2 implements Parcelable.Creator<uc2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ uc2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        Scope[] scopeArr = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                i3 = ad2.v(parcel, t);
            } else if (l == 2) {
                i2 = ad2.v(parcel, t);
            } else if (l == 3) {
                i = ad2.v(parcel, t);
            } else if (l != 4) {
                ad2.B(parcel, t);
            } else {
                scopeArr = (Scope[]) ad2.i(parcel, t, Scope.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new uc2(i3, i2, i, scopeArr);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ uc2[] newArray(int i) {
        return new uc2[i];
    }
}
