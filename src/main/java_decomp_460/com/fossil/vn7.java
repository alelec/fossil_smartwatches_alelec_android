package com.fossil;

import com.fossil.dl7;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vn7<T> implements qn7<T>, do7 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater<vn7<?>, Object> c; // = AtomicReferenceFieldUpdater.newUpdater(vn7.class, Object.class, Constants.RESULT);
    @DexIgnore
    public /* final */ qn7<T> b;
    @DexIgnore
    public volatile Object result;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public vn7(qn7<? super T> qn7) {
        this(qn7, wn7.UNDECIDED);
        pq7.c(qn7, "delegate");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.qn7<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public vn7(qn7<? super T> qn7, Object obj) {
        pq7.c(qn7, "delegate");
        this.b = qn7;
        this.result = obj;
    }

    @DexIgnore
    public final Object a() {
        Object obj = this.result;
        wn7 wn7 = wn7.UNDECIDED;
        if (obj == wn7) {
            if (c.compareAndSet(this, wn7, yn7.d())) {
                return yn7.d();
            }
            obj = this.result;
        }
        if (obj == wn7.RESUMED) {
            return yn7.d();
        }
        if (!(obj instanceof dl7.b)) {
            return obj;
        }
        throw ((dl7.b) obj).exception;
    }

    @DexIgnore
    @Override // com.fossil.do7
    public do7 getCallerFrame() {
        qn7<T> qn7 = this.b;
        if (!(qn7 instanceof do7)) {
            qn7 = null;
        }
        return (do7) qn7;
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public tn7 getContext() {
        return this.b.getContext();
    }

    @DexIgnore
    @Override // com.fossil.do7
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public void resumeWith(Object obj) {
        while (true) {
            Object obj2 = this.result;
            wn7 wn7 = wn7.UNDECIDED;
            if (obj2 == wn7) {
                if (c.compareAndSet(this, wn7, obj)) {
                    return;
                }
            } else if (obj2 != yn7.d()) {
                throw new IllegalStateException("Already resumed");
            } else if (c.compareAndSet(this, yn7.d(), wn7.RESUMED)) {
                this.b.resumeWith(obj);
                return;
            }
        }
    }

    @DexIgnore
    public String toString() {
        return "SafeContinuation for " + this.b;
    }
}
