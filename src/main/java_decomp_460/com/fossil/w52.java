package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w52 {
    @DexIgnore
    public static /* final */ int common_google_play_services_enable_button; // = 2131887361;
    @DexIgnore
    public static /* final */ int common_google_play_services_enable_text; // = 2131887362;
    @DexIgnore
    public static /* final */ int common_google_play_services_enable_title; // = 2131887363;
    @DexIgnore
    public static /* final */ int common_google_play_services_install_button; // = 2131887364;
    @DexIgnore
    public static /* final */ int common_google_play_services_install_text; // = 2131887365;
    @DexIgnore
    public static /* final */ int common_google_play_services_install_title; // = 2131887366;
    @DexIgnore
    public static /* final */ int common_google_play_services_notification_channel_name; // = 2131887367;
    @DexIgnore
    public static /* final */ int common_google_play_services_notification_ticker; // = 2131887368;
    @DexIgnore
    public static /* final */ int common_google_play_services_unsupported_text; // = 2131887370;
    @DexIgnore
    public static /* final */ int common_google_play_services_update_button; // = 2131887371;
    @DexIgnore
    public static /* final */ int common_google_play_services_update_text; // = 2131887372;
    @DexIgnore
    public static /* final */ int common_google_play_services_update_title; // = 2131887373;
    @DexIgnore
    public static /* final */ int common_google_play_services_updating_text; // = 2131887374;
    @DexIgnore
    public static /* final */ int common_google_play_services_wear_update_text; // = 2131887375;
    @DexIgnore
    public static /* final */ int common_open_on_phone; // = 2131887376;
    @DexIgnore
    public static /* final */ int common_signin_button_text; // = 2131887377;
    @DexIgnore
    public static /* final */ int common_signin_button_text_long; // = 2131887378;
}
