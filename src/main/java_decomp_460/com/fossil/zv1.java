package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zv1 extends sv1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<zv1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public zv1 createFromParcel(Parcel parcel) {
            return new zv1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public zv1[] newArray(int i) {
            return new zv1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ zv1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    public zv1(jv1 jv1, kv1 kv1) {
        super(vv1.STEPS, jv1, kv1, false, null, null, 56);
    }

    @DexIgnore
    public zv1(JSONObject jSONObject, cc0[] cc0Arr) {
        super(jSONObject, cc0Arr, null, 4);
    }

    @DexIgnore
    @Override // com.fossil.sv1, java.lang.Object, com.fossil.mv1, com.fossil.mv1
    public zv1 clone() {
        return new zv1(b().clone(), c().clone());
    }

    @DexIgnore
    @Override // com.fossil.sv1
    public zv1 setBackgroundImage(tv1 tv1) {
        return (zv1) super.setBackgroundImage(tv1);
    }

    @DexIgnore
    @Override // com.fossil.sv1
    public zv1 setPercentageCircleEnable(boolean z) {
        return (zv1) super.setPercentageCircleEnable(z);
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public zv1 setScaledHeight(float f) {
        mv1 scaledHeight = super.setScaledHeight(f);
        if (scaledHeight != null) {
            return (zv1) scaledHeight;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.StepComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public zv1 setScaledPosition(jv1 jv1) {
        mv1 scaledPosition = super.setScaledPosition(jv1);
        if (scaledPosition != null) {
            return (zv1) scaledPosition;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.StepComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public zv1 setScaledSize(kv1 kv1) {
        mv1 scaledSize = super.setScaledSize(kv1);
        if (scaledSize != null) {
            return (zv1) scaledSize;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.StepComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public zv1 setScaledWidth(float f) {
        mv1 scaledWidth = super.setScaledWidth(f);
        if (scaledWidth != null) {
            return (zv1) scaledWidth;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.StepComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public zv1 setScaledX(float f) {
        mv1 scaledX = super.setScaledX(f);
        if (scaledX != null) {
            return (zv1) scaledX;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.StepComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.mv1
    public zv1 setScaledY(float f) {
        mv1 scaledY = super.setScaledY(f);
        if (scaledY != null) {
            return (zv1) scaledY;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.StepComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.sv1
    public zv1 setTheme(uv1 uv1) {
        sv1 theme = super.setTheme(uv1);
        if (theme != null) {
            return (zv1) theme;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.StepComplicationElement");
    }
}
