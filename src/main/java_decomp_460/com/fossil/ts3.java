package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ts3 implements Parcelable.Creator<us3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ us3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        tc2 tc2 = null;
        z52 z52 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                i = ad2.v(parcel, t);
            } else if (l == 2) {
                z52 = (z52) ad2.e(parcel, t, z52.CREATOR);
            } else if (l != 3) {
                ad2.B(parcel, t);
            } else {
                tc2 = (tc2) ad2.e(parcel, t, tc2.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new us3(i, z52, tc2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ us3[] newArray(int i) {
        return new us3[i];
    }
}
