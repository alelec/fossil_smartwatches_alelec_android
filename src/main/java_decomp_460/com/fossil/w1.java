package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w1 implements Parcelable.Creator<y1> {
    @DexIgnore
    public /* synthetic */ w1(kq7 kq7) {
    }

    @DexIgnore
    public y1 a(Parcel parcel) {
        return new y1(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public y1 createFromParcel(Parcel parcel) {
        return new y1(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public y1[] newArray(int i) {
        return new y1[i];
    }
}
