package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.fossil.tu4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pu4 extends pv5 implements tu4.a {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public g37<d45> h;
    @DexIgnore
    public ru4 i;
    @DexIgnore
    public tu4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return pu4.l;
        }

        @DexIgnore
        public final pu4 b() {
            return new pu4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pu4 b;

        @DexIgnore
        public b(pu4 pu4) {
            this.b = pu4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ pu4 f2871a;

        @DexIgnore
        public c(pu4 pu4) {
            this.f2871a = pu4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ProgressBar progressBar;
            d45 d45 = (d45) pu4.L6(this.f2871a).a();
            if (d45 != null && (progressBar = d45.s) != null) {
                pq7.b(bool, "it");
                progressBar.setVisibility(bool.booleanValue() ? 0 : 8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<List<vs4>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ pu4 f2872a;

        @DexIgnore
        public d(pu4 pu4) {
            this.f2872a = pu4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<vs4> list) {
            tu4 tu4;
            if (list != null && (tu4 = this.f2872a.j) != null) {
                tu4.n(list);
            }
        }
    }

    /*
    static {
        String simpleName = pu4.class.getSimpleName();
        pq7.b(simpleName, "BCCreateChallengeIntroFr\u2026nt::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 L6(pu4 pu4) {
        g37<d45> g37 = pu4.h;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(0);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 == null) {
            return true;
        }
        activity2.finish();
        return true;
    }

    @DexIgnore
    public final void N6() {
        g37<d45> g37 = this.h;
        if (g37 != null) {
            d45 a2 = g37.a();
            if (a2 != null) {
                tu4 tu4 = new tu4();
                this.j = tu4;
                if (tu4 != null) {
                    tu4.m(this);
                }
                RecyclerView recyclerView = a2.u;
                pq7.b(recyclerView, "rvTemplates");
                recyclerView.setAdapter(this.j);
                RecyclerView recyclerView2 = a2.u;
                pq7.b(recyclerView2, "rvTemplates");
                recyclerView2.setLayoutManager(new LinearLayoutManager(getContext()));
                a2.r.setOnClickListener(new b(this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void O6() {
        ru4 ru4 = this.i;
        if (ru4 != null) {
            ru4.e().h(getViewLifecycleOwner(), new c(this));
            ru4 ru42 = this.i;
            if (ru42 != null) {
                ru42.f().h(getViewLifecycleOwner(), new d(this));
            } else {
                pq7.n("mViewModelHome");
                throw null;
            }
        } else {
            pq7.n("mViewModelHome");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.tu4.a
    public void k6(vs4 vs4) {
        pq7.c(vs4, MessengerShareContentUtility.ATTACHMENT_TEMPLATE_TYPE);
        BCCreateChallengeInputActivity.A.b(this, vs4);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 11 && i3 == -1) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.setResult(i3);
            }
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                activity2.finish();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().B0().a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(ru4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026troViewModel::class.java)");
            this.i = (ru4) a2;
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        d45 d45 = (d45) aq0.f(layoutInflater, 2131558526, viewGroup, false, A6());
        this.h = new g37<>(this, d45);
        pq7.b(d45, "binding");
        return d45.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ck5 g2 = ck5.f.g();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            g2.m("bc_challenge_mode", activity);
            return;
        }
        throw new il7("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        ru4 ru4 = this.i;
        if (ru4 != null) {
            ru4.d();
            N6();
            O6();
            return;
        }
        pq7.n("mViewModelHome");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
