package com.fossil;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ro5 implements qo5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f3139a;
    @DexIgnore
    public /* final */ jw0<mo5> b;
    @DexIgnore
    public /* final */ po5 c; // = new po5();
    @DexIgnore
    public /* final */ iw0<mo5> d;
    @DexIgnore
    public /* final */ xw0 e;
    @DexIgnore
    public /* final */ xw0 f;
    @DexIgnore
    public /* final */ xw0 g;
    @DexIgnore
    public /* final */ xw0 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jw0<mo5> {
        @DexIgnore
        public a(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, mo5 mo5) {
            if (mo5.c() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, mo5.c());
            }
            if (mo5.l() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, mo5.l());
            }
            px0.bindLong(3, (long) mo5.h());
            if (mo5.e() == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, mo5.e());
            }
            String a2 = ro5.this.c.a(mo5.a());
            if (a2 == null) {
                px0.bindNull(5);
            } else {
                px0.bindString(5, a2);
            }
            if (mo5.b() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, mo5.b());
            }
            if (mo5.d() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, mo5.d());
            }
            if (mo5.i() == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, mo5.i());
            }
            px0.bindLong(9, mo5.m() ? 1 : 0);
            if (mo5.f() == null) {
                px0.bindNull(10);
            } else {
                px0.bindString(10, mo5.f());
            }
            if (mo5.j() == null) {
                px0.bindNull(11);
            } else {
                px0.bindString(11, mo5.j());
            }
            if (mo5.k() == null) {
                px0.bindNull(12);
            } else {
                px0.bindString(12, mo5.k());
            }
            if (mo5.g() == null) {
                px0.bindNull(13);
            } else {
                px0.bindString(13, mo5.g());
            }
            px0.bindLong(14, mo5.n() ? 1 : 0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `diana_watchface_preset` (`createdAt`,`updatedAt`,`pinType`,`id`,`buttons`,`checksumFace`,`faceUrl`,`previewFaceUrl`,`isActive`,`name`,`serialNumber`,`uid`,`originalItemIdInStore`,`isNew`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends iw0<mo5> {
        @DexIgnore
        public b(ro5 ro5, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, mo5 mo5) {
            if (mo5.e() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, mo5.e());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0, com.fossil.iw0
        public String createQuery() {
            return "DELETE FROM `diana_watchface_preset` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends xw0 {
        @DexIgnore
        public c(ro5 ro5, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM diana_watchface_preset WHERE serialNumber=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends xw0 {
        @DexIgnore
        public d(ro5 ro5, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM diana_watchface_preset WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends xw0 {
        @DexIgnore
        public e(ro5 ro5, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "UPDATE diana_watchface_preset SET pinType=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends xw0 {
        @DexIgnore
        public f(ro5 ro5, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM diana_watchface_preset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements Callable<List<mo5>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tw0 f3141a;

        @DexIgnore
        public g(tw0 tw0) {
            this.f3141a = tw0;
        }

        @DexIgnore
        /* renamed from: a */
        public List<mo5> call() throws Exception {
            Cursor b2 = ex0.b(ro5.this.f3139a, this.f3141a, false, null);
            try {
                int c = dx0.c(b2, "createdAt");
                int c2 = dx0.c(b2, "updatedAt");
                int c3 = dx0.c(b2, "pinType");
                int c4 = dx0.c(b2, "id");
                int c5 = dx0.c(b2, "buttons");
                int c6 = dx0.c(b2, "checksumFace");
                int c7 = dx0.c(b2, "faceUrl");
                int c8 = dx0.c(b2, "previewFaceUrl");
                int c9 = dx0.c(b2, "isActive");
                int c10 = dx0.c(b2, "name");
                int c11 = dx0.c(b2, "serialNumber");
                int c12 = dx0.c(b2, "uid");
                int c13 = dx0.c(b2, "originalItemIdInStore");
                int c14 = dx0.c(b2, "isNew");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    mo5 mo5 = new mo5(b2.getString(c4), ro5.this.c.b(b2.getString(c5)), b2.getString(c6), b2.getString(c7), b2.getString(c8), b2.getInt(c9) != 0, b2.getString(c10), b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getInt(c14) != 0);
                    mo5.q(b2.getString(c));
                    mo5.t(b2.getString(c2));
                    mo5.s(b2.getInt(c3));
                    arrayList.add(mo5);
                }
                return arrayList;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.f3141a.m();
        }
    }

    @DexIgnore
    public ro5(qw0 qw0) {
        this.f3139a = qw0;
        this.b = new a(qw0);
        this.d = new b(this, qw0);
        this.e = new c(this, qw0);
        this.f = new d(this, qw0);
        this.g = new e(this, qw0);
        this.h = new f(this, qw0);
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public void a() {
        this.f3139a.assertNotSuspendingTransaction();
        px0 acquire = this.h.acquire();
        this.f3139a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f3139a.setTransactionSuccessful();
        } finally {
            this.f3139a.endTransaction();
            this.h.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public mo5 b(String str) {
        Throwable th;
        mo5 mo5;
        tw0 f2 = tw0.f("SELECT * FROM diana_watchface_preset WHERE isActive = 1 AND serialNumber=?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f3139a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3139a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "createdAt");
            int c3 = dx0.c(b2, "updatedAt");
            int c4 = dx0.c(b2, "pinType");
            int c5 = dx0.c(b2, "id");
            int c6 = dx0.c(b2, "buttons");
            int c7 = dx0.c(b2, "checksumFace");
            int c8 = dx0.c(b2, "faceUrl");
            int c9 = dx0.c(b2, "previewFaceUrl");
            int c10 = dx0.c(b2, "isActive");
            int c11 = dx0.c(b2, "name");
            int c12 = dx0.c(b2, "serialNumber");
            int c13 = dx0.c(b2, "uid");
            int c14 = dx0.c(b2, "originalItemIdInStore");
            try {
                int c15 = dx0.c(b2, "isNew");
                if (b2.moveToFirst()) {
                    mo5 = new mo5(b2.getString(c5), this.c.b(b2.getString(c6)), b2.getString(c7), b2.getString(c8), b2.getString(c9), b2.getInt(c10) != 0, b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getString(c14), b2.getInt(c15) != 0);
                    mo5.q(b2.getString(c2));
                    mo5.t(b2.getString(c3));
                    mo5.s(b2.getInt(c4));
                } else {
                    mo5 = null;
                }
                b2.close();
                f2.m();
                return mo5;
            } catch (Throwable th2) {
                th = th2;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public void c(mo5 mo5) {
        this.f3139a.assertNotSuspendingTransaction();
        this.f3139a.beginTransaction();
        try {
            this.d.handle(mo5);
            this.f3139a.setTransactionSuccessful();
        } finally {
            this.f3139a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public List<mo5> d(String str) {
        tw0 f2 = tw0.f("SELECT * FROM diana_watchface_preset WHERE serialNumber = ? AND pinType <> 0", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f3139a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3139a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "createdAt");
            int c3 = dx0.c(b2, "updatedAt");
            int c4 = dx0.c(b2, "pinType");
            int c5 = dx0.c(b2, "id");
            int c6 = dx0.c(b2, "buttons");
            int c7 = dx0.c(b2, "checksumFace");
            int c8 = dx0.c(b2, "faceUrl");
            int c9 = dx0.c(b2, "previewFaceUrl");
            int c10 = dx0.c(b2, "isActive");
            int c11 = dx0.c(b2, "name");
            int c12 = dx0.c(b2, "serialNumber");
            int c13 = dx0.c(b2, "uid");
            int c14 = dx0.c(b2, "originalItemIdInStore");
            try {
                int c15 = dx0.c(b2, "isNew");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    mo5 mo5 = new mo5(b2.getString(c5), this.c.b(b2.getString(c6)), b2.getString(c7), b2.getString(c8), b2.getString(c9), b2.getInt(c10) != 0, b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getString(c14), b2.getInt(c15) != 0);
                    mo5.q(b2.getString(c2));
                    mo5.t(b2.getString(c3));
                    mo5.s(b2.getInt(c4));
                    arrayList.add(mo5);
                }
                b2.close();
                f2.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public void e(String str) {
        this.f3139a.assertNotSuspendingTransaction();
        px0 acquire = this.f.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.f3139a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f3139a.setTransactionSuccessful();
        } finally {
            this.f3139a.endTransaction();
            this.f.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public List<mo5> f(String str) {
        tw0 f2 = tw0.f("SELECT * FROM diana_watchface_preset WHERE serialNumber = ? AND pinType <> 3 ORDER BY createdAt ASC", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f3139a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3139a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "createdAt");
            int c3 = dx0.c(b2, "updatedAt");
            int c4 = dx0.c(b2, "pinType");
            int c5 = dx0.c(b2, "id");
            int c6 = dx0.c(b2, "buttons");
            int c7 = dx0.c(b2, "checksumFace");
            int c8 = dx0.c(b2, "faceUrl");
            int c9 = dx0.c(b2, "previewFaceUrl");
            int c10 = dx0.c(b2, "isActive");
            int c11 = dx0.c(b2, "name");
            int c12 = dx0.c(b2, "serialNumber");
            int c13 = dx0.c(b2, "uid");
            int c14 = dx0.c(b2, "originalItemIdInStore");
            try {
                int c15 = dx0.c(b2, "isNew");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    mo5 mo5 = new mo5(b2.getString(c5), this.c.b(b2.getString(c6)), b2.getString(c7), b2.getString(c8), b2.getString(c9), b2.getInt(c10) != 0, b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getString(c14), b2.getInt(c15) != 0);
                    mo5.q(b2.getString(c2));
                    mo5.t(b2.getString(c3));
                    mo5.s(b2.getInt(c4));
                    arrayList.add(mo5);
                }
                b2.close();
                f2.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public void g(String str) {
        this.f3139a.assertNotSuspendingTransaction();
        px0 acquire = this.e.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.f3139a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f3139a.setTransactionSuccessful();
        } finally {
            this.f3139a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public mo5 getPresetById(String str) {
        Throwable th;
        mo5 mo5;
        tw0 f2 = tw0.f("SELECT * FROM diana_watchface_preset WHERE id=?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f3139a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3139a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "createdAt");
            int c3 = dx0.c(b2, "updatedAt");
            int c4 = dx0.c(b2, "pinType");
            int c5 = dx0.c(b2, "id");
            int c6 = dx0.c(b2, "buttons");
            int c7 = dx0.c(b2, "checksumFace");
            int c8 = dx0.c(b2, "faceUrl");
            int c9 = dx0.c(b2, "previewFaceUrl");
            int c10 = dx0.c(b2, "isActive");
            int c11 = dx0.c(b2, "name");
            int c12 = dx0.c(b2, "serialNumber");
            int c13 = dx0.c(b2, "uid");
            int c14 = dx0.c(b2, "originalItemIdInStore");
            try {
                int c15 = dx0.c(b2, "isNew");
                if (b2.moveToFirst()) {
                    mo5 = new mo5(b2.getString(c5), this.c.b(b2.getString(c6)), b2.getString(c7), b2.getString(c8), b2.getString(c9), b2.getInt(c10) != 0, b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getString(c14), b2.getInt(c15) != 0);
                    mo5.q(b2.getString(c2));
                    mo5.t(b2.getString(c3));
                    mo5.s(b2.getInt(c4));
                } else {
                    mo5 = null;
                }
                b2.close();
                f2.m();
                return mo5;
            } catch (Throwable th2) {
                th = th2;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public int h(String str) {
        int i = 0;
        tw0 f2 = tw0.f("SELECT COUNT(id) FROM diana_watchface_preset WHERE serialNumber=? AND pinType <> 3", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.f3139a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3139a, f2, false, null);
        try {
            if (b2.moveToFirst()) {
                i = b2.getInt(0);
            }
            return i;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public void i(String str, int i) {
        this.f3139a.assertNotSuspendingTransaction();
        px0 acquire = this.g.acquire();
        acquire.bindLong(1, (long) i);
        if (str == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str);
        }
        this.f3139a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f3139a.setTransactionSuccessful();
        } finally {
            this.f3139a.endTransaction();
            this.g.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public Long[] insert(List<mo5> list) {
        this.f3139a.assertNotSuspendingTransaction();
        this.f3139a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.f3139a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.f3139a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public void j(List<mo5> list) {
        this.f3139a.assertNotSuspendingTransaction();
        this.f3139a.beginTransaction();
        try {
            this.b.insert(list);
            this.f3139a.setTransactionSuccessful();
        } finally {
            this.f3139a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public mo5 k() {
        Throwable th;
        mo5 mo5;
        tw0 f2 = tw0.f("SELECT * FROM diana_watchface_preset WHERE isActive = 1", 0);
        this.f3139a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f3139a, f2, false, null);
        try {
            int c2 = dx0.c(b2, "createdAt");
            int c3 = dx0.c(b2, "updatedAt");
            int c4 = dx0.c(b2, "pinType");
            int c5 = dx0.c(b2, "id");
            int c6 = dx0.c(b2, "buttons");
            int c7 = dx0.c(b2, "checksumFace");
            int c8 = dx0.c(b2, "faceUrl");
            int c9 = dx0.c(b2, "previewFaceUrl");
            int c10 = dx0.c(b2, "isActive");
            int c11 = dx0.c(b2, "name");
            int c12 = dx0.c(b2, "serialNumber");
            int c13 = dx0.c(b2, "uid");
            int c14 = dx0.c(b2, "originalItemIdInStore");
            try {
                int c15 = dx0.c(b2, "isNew");
                if (b2.moveToFirst()) {
                    mo5 = new mo5(b2.getString(c5), this.c.b(b2.getString(c6)), b2.getString(c7), b2.getString(c8), b2.getString(c9), b2.getInt(c10) != 0, b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getString(c14), b2.getInt(c15) != 0);
                    mo5.q(b2.getString(c2));
                    mo5.t(b2.getString(c3));
                    mo5.s(b2.getInt(c4));
                } else {
                    mo5 = null;
                }
                b2.close();
                f2.m();
                return mo5;
            } catch (Throwable th2) {
                th = th2;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public long l(mo5 mo5) {
        this.f3139a.assertNotSuspendingTransaction();
        this.f3139a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(mo5);
            this.f3139a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.f3139a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.qo5
    public LiveData<List<mo5>> m(String str) {
        tw0 f2 = tw0.f("SELECT * FROM diana_watchface_preset WHERE serialNumber = ? AND pinType <> 3 ORDER BY createdAt ASC", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        nw0 invalidationTracker = this.f3139a.getInvalidationTracker();
        g gVar = new g(f2);
        return invalidationTracker.d(new String[]{"diana_watchface_preset"}, false, gVar);
    }
}
