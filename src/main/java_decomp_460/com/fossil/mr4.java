package com.fossil;

import android.database.Cursor;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mr4 implements lr4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f2412a;
    @DexIgnore
    public /* final */ jw0<rr4> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jw0<rr4> {
        @DexIgnore
        public a(mr4 mr4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, rr4 rr4) {
            if (rr4.a() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, rr4.a());
            }
            if (rr4.b() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, rr4.b());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `flag` (`id`,`variantKey`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends xw0 {
        @DexIgnore
        public b(mr4 mr4, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM flag";
        }
    }

    @DexIgnore
    public mr4(qw0 qw0) {
        this.f2412a = qw0;
        this.b = new a(this, qw0);
        new b(this, qw0);
    }

    @DexIgnore
    @Override // com.fossil.lr4
    public rr4 a(String str) {
        rr4 rr4 = null;
        tw0 f = tw0.f("SELECT * FROM flag WHERE id = ? LIMIT 1", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.f2412a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f2412a, f, false, null);
        try {
            int c = dx0.c(b2, "id");
            int c2 = dx0.c(b2, "variantKey");
            if (b2.moveToFirst()) {
                rr4 = new rr4(b2.getString(c), b2.getString(c2));
            }
            return rr4;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.lr4
    public Long[] insert(List<rr4> list) {
        this.f2412a.assertNotSuspendingTransaction();
        this.f2412a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.f2412a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.f2412a.endTransaction();
        }
    }
}
