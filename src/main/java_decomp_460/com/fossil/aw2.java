package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class aw2 implements xw2 {
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore
    public aw2(Context context) {
        this.b = context;
    }

    @DexIgnore
    @Override // com.fossil.xw2
    public final Object zza() {
        return wv2.b(this.b);
    }
}
