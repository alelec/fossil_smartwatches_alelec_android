package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "hourNotification")
public class gp5 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<gp5> CREATOR; // = new a();
    @DexIgnore
    @DatabaseField(columnName = AppFilter.COLUMN_HOUR)
    public int b;
    @DexIgnore
    @DatabaseField(columnName = AppFilter.COLUMN_IS_VIBRATION_ONLY)
    public boolean c;
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    public long d;
    @DexIgnore
    @DatabaseField(columnName = "extraId")
    public String e;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String f;
    @DexIgnore
    @DatabaseField(columnName = "deviceFamily")
    public String g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<gp5> {
        @DexIgnore
        /* renamed from: a */
        public gp5 createFromParcel(Parcel parcel) {
            return new gp5(parcel);
        }

        @DexIgnore
        /* renamed from: b */
        public gp5[] newArray(int i) {
            return new gp5[i];
        }
    }

    @DexIgnore
    public gp5() {
    }

    @DexIgnore
    public gp5(int i, boolean z, String str, String str2) {
        this.b = i;
        this.c = z;
        this.e = str;
        this.g = str2;
        this.f = str + str2;
    }

    @DexIgnore
    public gp5(Parcel parcel) {
        this.b = parcel.readInt();
        this.c = parcel.readByte() != 0;
        this.d = parcel.readLong();
        this.e = parcel.readString();
        this.f = parcel.readString();
        this.g = parcel.readString();
    }

    @DexIgnore
    public long a() {
        return this.d;
    }

    @DexIgnore
    public String b() {
        return this.g;
    }

    @DexIgnore
    public String c() {
        return this.e;
    }

    @DexIgnore
    public int d() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String e() {
        return this.f;
    }

    @DexIgnore
    public boolean f() {
        return this.c;
    }

    @DexIgnore
    public void g(long j) {
        this.d = j;
    }

    @DexIgnore
    public void h(String str) {
        this.g = str;
    }

    @DexIgnore
    public void i(String str) {
        this.e = str;
    }

    @DexIgnore
    public void k(int i) {
        this.b = i;
    }

    @DexIgnore
    public void m(String str) {
        this.f = str;
    }

    @DexIgnore
    public void n(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b);
        parcel.writeByte(this.c ? (byte) 1 : 0);
        parcel.writeLong(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
    }
}
