package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yr2 extends nq2 {
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest s;
    @DexIgnore
    public /* final */ /* synthetic */ ga3 t;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public yr2(xr2 xr2, r62 r62, LocationRequest locationRequest, ga3 ga3) {
        super(r62);
        this.s = locationRequest;
        this.t = ga3;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.m62$b] */
    @Override // com.fossil.i72
    public final /* synthetic */ void u(fr2 fr2) throws RemoteException {
        fr2.x0(this.s, q72.a(this.t, pr2.b(), ga3.class.getSimpleName()), new oq2(this));
    }
}
