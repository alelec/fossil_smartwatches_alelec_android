package com.fossil;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ej5 {
    @DexIgnore
    public static final <T> void a(Bundle bundle, String str, T t) {
        pq7.c(bundle, "$this$put");
        pq7.c(str, "key");
        if (t instanceof Integer) {
            bundle.putInt(str, t.intValue());
        } else if (t instanceof String) {
            bundle.putString(str, t);
        } else if (t instanceof Boolean) {
            bundle.putBoolean(str, t.booleanValue());
        } else if (t instanceof Serializable) {
            bundle.putSerializable(str, t);
        } else if (t instanceof Parcelable) {
            bundle.putParcelable(str, t);
        } else {
            throw new IllegalArgumentException("Wrong value type");
        }
    }

    @DexIgnore
    public static final String b(String str) {
        return str == null || vt7.l(str) ? "NA" : str;
    }
}
