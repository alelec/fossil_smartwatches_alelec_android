package com.fossil;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w48 implements k48 {
    @DexIgnore
    public /* final */ i48 b; // = new i48();
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ c58 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends InputStream {
        @DexIgnore
        public /* final */ /* synthetic */ w48 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(w48 w48) {
            this.b = w48;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int available() {
            w48 w48 = this.b;
            if (!w48.c) {
                return (int) Math.min(w48.b.p0(), (long) Integer.MAX_VALUE);
            }
            throw new IOException("closed");
        }

        @DexIgnore
        @Override // java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
        public void close() {
            this.b.close();
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read() {
            w48 w48 = this.b;
            if (!w48.c) {
                if (w48.b.p0() == 0) {
                    w48 w482 = this.b;
                    if (w482.d.d0(w482.b, (long) 8192) == -1) {
                        return -1;
                    }
                }
                return this.b.b.readByte() & 255;
            }
            throw new IOException("closed");
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) {
            pq7.c(bArr, "data");
            if (!this.b.c) {
                f48.b((long) bArr.length, (long) i, (long) i2);
                if (this.b.b.p0() == 0) {
                    w48 w48 = this.b;
                    if (w48.d.d0(w48.b, (long) 8192) == -1) {
                        return -1;
                    }
                }
                return this.b.b.read(bArr, i, i2);
            }
            throw new IOException("closed");
        }

        @DexIgnore
        public String toString() {
            return this.b + ".inputStream()";
        }
    }

    @DexIgnore
    public w48(c58 c58) {
        pq7.c(c58, "source");
        this.d = c58;
    }

    @DexIgnore
    @Override // com.fossil.k48
    public boolean H(long j, l48 l48) {
        pq7.c(l48, "bytes");
        return c(j, l48, 0, l48.size());
    }

    @DexIgnore
    @Override // com.fossil.k48
    public String I(Charset charset) {
        pq7.c(charset, "charset");
        this.b.N(this.d);
        return this.b.I(charset);
    }

    @DexIgnore
    @Override // com.fossil.k48
    public boolean R(long j) {
        if (!(j >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (!this.c) {
            while (this.b.p0() < j) {
                if (this.d.d0(this.b, (long) 8192) == -1) {
                    return false;
                }
            }
            return true;
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.k48
    public String U() {
        return z(Long.MAX_VALUE);
    }

    @DexIgnore
    @Override // com.fossil.k48
    public byte[] W(long j) {
        j0(j);
        return this.b.W(j);
    }

    @DexIgnore
    public long a(byte b2) {
        return b(b2, 0, Long.MAX_VALUE);
    }

    @DexIgnore
    public long b(byte b2, long j, long j2) {
        boolean z = true;
        if (!this.c) {
            if (0 > j || j2 < j) {
                z = false;
            }
            if (z) {
                long j3 = j;
                while (j3 < j2) {
                    long P = this.b.P(b2, j3, j2);
                    if (P == -1) {
                        long p0 = this.b.p0();
                        if (p0 >= j2) {
                            break;
                        } else if (this.d.d0(this.b, (long) 8192) == -1) {
                            return -1;
                        } else {
                            j3 = Math.max(j3, p0);
                        }
                    } else {
                        return P;
                    }
                }
                return -1;
            }
            throw new IllegalArgumentException(("fromIndex=" + j + " toIndex=" + j2).toString());
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    public boolean c(long j, l48 l48, int i, int i2) {
        pq7.c(l48, "bytes");
        if (!this.c) {
            if (j >= 0 && i >= 0 && i2 >= 0 && l48.size() - i >= i2) {
                for (int i3 = 0; i3 < i2; i3++) {
                    long j2 = ((long) i3) + j;
                    if (R(1 + j2) && this.b.M(j2) == l48.getByte(i + i3)) {
                    }
                }
                return true;
            }
            return false;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.c58, java.lang.AutoCloseable, java.nio.channels.Channel
    public void close() {
        if (!this.c) {
            this.c = true;
            this.d.close();
            this.b.j();
        }
    }

    @DexIgnore
    @Override // com.fossil.k48
    public i48 d() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.c58
    public long d0(i48 i48, long j) {
        pq7.c(i48, "sink");
        if (!(j >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (!(!this.c)) {
            throw new IllegalStateException("closed".toString());
        } else if (this.b.p0() == 0 && this.d.d0(this.b, (long) 8192) == -1) {
            return -1;
        } else {
            return this.b.d0(i48, Math.min(j, this.b.p0()));
        }
    }

    @DexIgnore
    @Override // com.fossil.c58
    public d58 e() {
        return this.d.e();
    }

    @DexIgnore
    @Override // com.fossil.k48
    public long e0(a58 a58) {
        pq7.c(a58, "sink");
        long j = 0;
        while (this.d.d0(this.b, (long) 8192) != -1) {
            long o = this.b.o();
            if (o > 0) {
                j += o;
                a58.K(this.b, o);
            }
        }
        if (this.b.p0() <= 0) {
            return j;
        }
        long p0 = j + this.b.p0();
        i48 i48 = this.b;
        a58.K(i48, i48.p0());
        return p0;
    }

    @DexIgnore
    public int f() {
        j0(4);
        return this.b.T();
    }

    @DexIgnore
    public short h() {
        j0(2);
        return this.b.V();
    }

    @DexIgnore
    @Override // com.fossil.k48
    public l48 i(long j) {
        j0(j);
        return this.b.i(j);
    }

    @DexIgnore
    public boolean isOpen() {
        return !this.c;
    }

    @DexIgnore
    @Override // com.fossil.k48
    public void j0(long j) {
        if (!R(j)) {
            throw new EOFException();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0041  */
    @Override // com.fossil.k48
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long m0() {
        /*
            r7 = this;
            r6 = 16
            r0 = 1
            r7.j0(r0)
            r0 = 0
        L_0x0008:
            int r1 = r0 + 1
            long r2 = (long) r1
            boolean r2 = r7.R(r2)
            if (r2 == 0) goto L_0x0038
            com.fossil.i48 r2 = r7.b
            long r4 = (long) r0
            byte r2 = r2.M(r4)
            r3 = 48
            byte r3 = (byte) r3
            if (r2 < r3) goto L_0x0022
            r3 = 57
            byte r3 = (byte) r3
            if (r2 <= r3) goto L_0x003f
        L_0x0022:
            r3 = 97
            byte r3 = (byte) r3
            if (r2 < r3) goto L_0x002c
            r3 = 102(0x66, float:1.43E-43)
            byte r3 = (byte) r3
            if (r2 <= r3) goto L_0x003f
        L_0x002c:
            r3 = 65
            byte r3 = (byte) r3
            if (r2 < r3) goto L_0x0036
            r3 = 70
            byte r3 = (byte) r3
            if (r2 <= r3) goto L_0x003f
        L_0x0036:
            if (r0 == 0) goto L_0x0041
        L_0x0038:
            com.fossil.i48 r0 = r7.b
            long r0 = r0.m0()
            return r0
        L_0x003f:
            r0 = r1
            goto L_0x0008
        L_0x0041:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Expected leading [0-9a-fA-F] character but was 0x"
            r0.append(r1)
            com.fossil.ct7.a(r6)
            com.fossil.ct7.a(r6)
            java.lang.String r1 = java.lang.Integer.toString(r2, r6)
            java.lang.String r2 = "java.lang.Integer.toStri\u2026(this, checkRadix(radix))"
            com.fossil.pq7.b(r1, r2)
            r0.append(r1)
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.w48.m0():long");
    }

    @DexIgnore
    @Override // com.fossil.k48
    public InputStream n0() {
        return new a(this);
    }

    @DexIgnore
    @Override // com.fossil.k48
    public k48 peek() {
        return s48.d(new u48(this));
    }

    @DexIgnore
    @Override // com.fossil.k48
    public byte[] r() {
        this.b.N(this.d);
        return this.b.r();
    }

    @DexIgnore
    @Override // java.nio.channels.ReadableByteChannel
    public int read(ByteBuffer byteBuffer) {
        pq7.c(byteBuffer, "sink");
        if (this.b.p0() == 0 && this.d.d0(this.b, (long) 8192) == -1) {
            return -1;
        }
        return this.b.read(byteBuffer);
    }

    @DexIgnore
    @Override // com.fossil.k48
    public byte readByte() {
        j0(1);
        return this.b.readByte();
    }

    @DexIgnore
    @Override // com.fossil.k48
    public void readFully(byte[] bArr) {
        pq7.c(bArr, "sink");
        try {
            j0((long) bArr.length);
            this.b.readFully(bArr);
        } catch (EOFException e) {
            int i = 0;
            while (this.b.p0() > 0) {
                i48 i48 = this.b;
                int read = i48.read(bArr, i, (int) i48.p0());
                if (read != -1) {
                    i += read;
                } else {
                    throw new AssertionError();
                }
            }
            throw e;
        }
    }

    @DexIgnore
    @Override // com.fossil.k48
    public int readInt() {
        j0(4);
        return this.b.readInt();
    }

    @DexIgnore
    @Override // com.fossil.k48
    public short readShort() {
        j0(2);
        return this.b.readShort();
    }

    @DexIgnore
    @Override // com.fossil.k48
    public void skip(long j) {
        if (!this.c) {
            while (j > 0) {
                if (this.b.p0() == 0 && this.d.d0(this.b, (long) 8192) == -1) {
                    throw new EOFException();
                }
                long min = Math.min(j, this.b.p0());
                this.b.skip(min);
                j -= min;
            }
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.k48
    public i48 t() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        return "buffer(" + this.d + ')';
    }

    @DexIgnore
    @Override // com.fossil.k48
    public boolean u() {
        if (!this.c) {
            return this.b.u() && this.d.d0(this.b, (long) 8192) == -1;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.k48
    public long y() {
        int i;
        j0(1);
        long j = 0;
        while (true) {
            long j2 = j + 1;
            if (!R(j2)) {
                break;
            }
            byte M = this.b.M(j);
            if ((M >= ((byte) 48) && M <= ((byte) 57)) || (j == 0 && M == ((byte) 45))) {
                j = j2;
            } else if (i == 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("Expected leading [0-9] or '-' character but was 0x");
                ct7.a(16);
                ct7.a(16);
                String num = Integer.toString(M, 16);
                pq7.b(num, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
                sb.append(num);
                throw new NumberFormatException(sb.toString());
            }
        }
        return this.b.y();
    }

    @DexIgnore
    @Override // com.fossil.k48
    public String z(long j) {
        if (j >= 0) {
            long j2 = j == Long.MAX_VALUE ? Long.MAX_VALUE : j + 1;
            byte b2 = (byte) 10;
            long b3 = b(b2, 0, j2);
            if (b3 != -1) {
                return e58.b(this.b, b3);
            }
            if (j2 < Long.MAX_VALUE && R(j2) && this.b.M(j2 - 1) == ((byte) 13) && R(1 + j2) && this.b.M(j2) == b2) {
                return e58.b(this.b, j2);
            }
            i48 i48 = new i48();
            i48 i482 = this.b;
            i482.C(i48, 0, Math.min((long) 32, i482.p0()));
            throw new EOFException("\\n not found: limit=" + Math.min(this.b.p0(), j) + " content=" + i48.S().hex() + "\u2026");
        }
        throw new IllegalArgumentException(("limit < 0: " + j).toString());
    }
}
