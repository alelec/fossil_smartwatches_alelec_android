package com.fossil;

import android.content.Intent;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class gf4 implements Callable {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Intent f1301a;

    @DexIgnore
    public gf4(Intent intent) {
        this.f1301a = intent;
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return Integer.valueOf(hf4.c(this.f1301a));
    }
}
