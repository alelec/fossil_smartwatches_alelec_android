package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xj4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ int f4134a; // = a();

    @DexIgnore
    public static int a() {
        return d(System.getProperty("java.version"));
    }

    @DexIgnore
    public static int b(String str) {
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.length(); i++) {
                char charAt = str.charAt(i);
                if (!Character.isDigit(charAt)) {
                    break;
                }
                sb.append(charAt);
            }
            return Integer.parseInt(sb.toString());
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    @DexIgnore
    public static int c() {
        return f4134a;
    }

    @DexIgnore
    public static int d(String str) {
        int f = f(str);
        if (f == -1) {
            f = b(str);
        }
        if (f == -1) {
            return 6;
        }
        return f;
    }

    @DexIgnore
    public static boolean e() {
        return f4134a >= 9;
    }

    @DexIgnore
    public static int f(String str) {
        try {
            String[] split = str.split("[._]");
            int parseInt = Integer.parseInt(split[0]);
            return (parseInt != 1 || split.length <= 1) ? parseInt : Integer.parseInt(split[1]);
        } catch (NumberFormatException e) {
            return -1;
        }
    }
}
