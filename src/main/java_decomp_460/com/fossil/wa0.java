package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wa0 implements Parcelable.Creator<xa0> {
    @DexIgnore
    public /* synthetic */ wa0(kq7 kq7) {
    }

    @DexIgnore
    public xa0 a(Parcel parcel) {
        return new xa0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public xa0 createFromParcel(Parcel parcel) {
        return new xa0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public xa0[] newArray(int i) {
        return new xa0[i];
    }
}
