package com.fossil;

import android.accounts.Account;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.fossil.gc2;
import com.fossil.lc2;
import com.google.android.gms.common.api.Scope;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yb2<T extends IInterface> {
    @DexIgnore
    public static /* final */ b62[] A; // = new b62[0];

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f4292a;
    @DexIgnore
    public long b;
    @DexIgnore
    public long c;
    @DexIgnore
    public int d;
    @DexIgnore
    public long e;
    @DexIgnore
    public je2 f;
    @DexIgnore
    public /* final */ Context g;
    @DexIgnore
    public /* final */ gc2 h;
    @DexIgnore
    public /* final */ d62 i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public /* final */ Object k;
    @DexIgnore
    public /* final */ Object l;
    @DexIgnore
    public nc2 m;
    @DexIgnore
    public c n;
    @DexIgnore
    public T o;
    @DexIgnore
    public /* final */ ArrayList<h<?>> p;
    @DexIgnore
    public i q;
    @DexIgnore
    public int r;
    @DexIgnore
    public /* final */ a s;
    @DexIgnore
    public /* final */ b t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ String v;
    @DexIgnore
    public z52 w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public volatile ee2 y;
    @DexIgnore
    public AtomicInteger z;

    @DexIgnore
    public interface a {
        @DexIgnore
        void d(int i);

        @DexIgnore
        void e(Bundle bundle);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void n(z52 z52);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(z52 z52);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements c {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.yb2.c
        public void a(z52 z52) {
            if (z52.A()) {
                yb2 yb2 = yb2.this;
                yb2.i(null, yb2.H());
            } else if (yb2.this.t != null) {
                yb2.this.t.n(z52);
            }
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class f extends h<Boolean> {
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ Bundle e;

        @DexIgnore
        public f(int i, Bundle bundle) {
            super(Boolean.TRUE);
            this.d = i;
            this.e = bundle;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.yb2.h
        public final /* synthetic */ void c(Boolean bool) {
            PendingIntent pendingIntent = null;
            if (bool == null) {
                yb2.this.X(1, null);
                return;
            }
            int i = this.d;
            if (i != 0) {
                if (i != 10) {
                    yb2.this.X(1, null);
                    Bundle bundle = this.e;
                    if (bundle != null) {
                        pendingIntent = (PendingIntent) bundle.getParcelable("pendingIntent");
                    }
                    f(new z52(this.d, pendingIntent));
                    return;
                }
                yb2.this.X(1, null);
                throw new IllegalStateException(String.format("A fatal developer error has occurred. Class name: %s. Start service action: %s. Service Descriptor: %s. ", getClass().getSimpleName(), yb2.this.x(), yb2.this.p()));
            } else if (!g()) {
                yb2.this.X(1, null);
                f(new z52(8, null));
            }
        }

        @DexIgnore
        @Override // com.fossil.yb2.h
        public final void d() {
        }

        @DexIgnore
        public abstract void f(z52 z52);

        @DexIgnore
        public abstract boolean g();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g extends xl2 {
        @DexIgnore
        public g(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public static void a(Message message) {
            h hVar = (h) message.obj;
            hVar.d();
            hVar.b();
        }

        @DexIgnore
        public static boolean b(Message message) {
            int i = message.what;
            return i == 2 || i == 1 || i == 7;
        }

        @DexIgnore
        public final void handleMessage(Message message) {
            if (yb2.this.z.get() == message.arg1) {
                int i = message.what;
                if ((i == 1 || i == 7 || ((i == 4 && !yb2.this.B()) || message.what == 5)) && !yb2.this.j()) {
                    a(message);
                    return;
                }
                int i2 = message.what;
                if (i2 == 4) {
                    yb2.this.w = new z52(message.arg2);
                    if (!yb2.this.n0() || yb2.this.x) {
                        z52 z52 = yb2.this.w != null ? yb2.this.w : new z52(8);
                        yb2.this.n.a(z52);
                        yb2.this.M(z52);
                        return;
                    }
                    yb2.this.X(3, null);
                } else if (i2 == 5) {
                    z52 z522 = yb2.this.w != null ? yb2.this.w : new z52(8);
                    yb2.this.n.a(z522);
                    yb2.this.M(z522);
                } else if (i2 == 3) {
                    Object obj = message.obj;
                    z52 z523 = new z52(message.arg2, obj instanceof PendingIntent ? (PendingIntent) obj : null);
                    yb2.this.n.a(z523);
                    yb2.this.M(z523);
                } else if (i2 == 6) {
                    yb2.this.X(5, null);
                    if (yb2.this.s != null) {
                        yb2.this.s.d(message.arg2);
                    }
                    yb2.this.N(message.arg2);
                    yb2.this.c0(5, 1, null);
                } else if (i2 == 2 && !yb2.this.c()) {
                    a(message);
                } else if (b(message)) {
                    ((h) message.obj).e();
                } else {
                    int i3 = message.what;
                    StringBuilder sb = new StringBuilder(45);
                    sb.append("Don't know how to handle message: ");
                    sb.append(i3);
                    Log.wtf("GmsClient", sb.toString(), new Exception());
                }
            } else if (b(message)) {
                a(message);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class h<TListener> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public TListener f4295a;
        @DexIgnore
        public boolean b; // = false;

        @DexIgnore
        public h(TListener tlistener) {
            this.f4295a = tlistener;
        }

        @DexIgnore
        public final void a() {
            synchronized (this) {
                this.f4295a = null;
            }
        }

        @DexIgnore
        public final void b() {
            a();
            synchronized (yb2.this.p) {
                yb2.this.p.remove(this);
            }
        }

        @DexIgnore
        public abstract void c(TListener tlistener);

        @DexIgnore
        public abstract void d();

        @DexIgnore
        public final void e() {
            TListener tlistener;
            synchronized (this) {
                tlistener = this.f4295a;
                if (this.b) {
                    String valueOf = String.valueOf(this);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Callback proxy ");
                    sb.append(valueOf);
                    sb.append(" being reused. This is not safe.");
                    Log.w("GmsClient", sb.toString());
                }
            }
            if (tlistener != null) {
                try {
                    c(tlistener);
                } catch (RuntimeException e) {
                    d();
                    throw e;
                }
            } else {
                d();
            }
            synchronized (this) {
                this.b = true;
            }
            b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i implements ServiceConnection {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f4296a;

        @DexIgnore
        public i(int i) {
            this.f4296a = i;
        }

        @DexIgnore
        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            nc2 mc2;
            if (iBinder == null) {
                yb2.this.e0(16);
                return;
            }
            synchronized (yb2.this.l) {
                yb2 yb2 = yb2.this;
                if (iBinder == null) {
                    mc2 = null;
                } else {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    mc2 = (queryLocalInterface == null || !(queryLocalInterface instanceof nc2)) ? new mc2(iBinder) : (nc2) queryLocalInterface;
                }
                yb2.m = mc2;
            }
            yb2.this.W(0, null, this.f4296a);
        }

        @DexIgnore
        public final void onServiceDisconnected(ComponentName componentName) {
            synchronized (yb2.this.l) {
                yb2.this.m = null;
            }
            Handler handler = yb2.this.j;
            handler.sendMessage(handler.obtainMessage(6, this.f4296a, 1));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends lc2.a {
        @DexIgnore
        public yb2 b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public j(yb2 yb2, int i) {
            this.b = yb2;
            this.c = i;
        }

        @DexIgnore
        @Override // com.fossil.lc2
        public final void W0(int i, Bundle bundle) {
            Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
        }

        @DexIgnore
        @Override // com.fossil.lc2
        public final void d2(int i, IBinder iBinder, ee2 ee2) {
            rc2.l(this.b, "onPostInitCompleteWithConnectionInfo can be called only once per call togetRemoteService");
            rc2.k(ee2);
            this.b.b0(ee2);
            i1(i, iBinder, ee2.b);
        }

        @DexIgnore
        @Override // com.fossil.lc2
        public final void i1(int i, IBinder iBinder, Bundle bundle) {
            rc2.l(this.b, "onPostInitComplete can be called only once per call to getRemoteService");
            this.b.O(i, iBinder, bundle, this.c);
            this.b = null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k extends f {
        @DexIgnore
        public /* final */ IBinder g;

        @DexIgnore
        public k(int i, IBinder iBinder, Bundle bundle) {
            super(i, bundle);
            this.g = iBinder;
        }

        @DexIgnore
        @Override // com.fossil.yb2.f
        public final void f(z52 z52) {
            if (yb2.this.t != null) {
                yb2.this.t.n(z52);
            }
            yb2.this.M(z52);
        }

        @DexIgnore
        @Override // com.fossil.yb2.f
        public final boolean g() {
            try {
                String interfaceDescriptor = this.g.getInterfaceDescriptor();
                if (!yb2.this.p().equals(interfaceDescriptor)) {
                    String p = yb2.this.p();
                    StringBuilder sb = new StringBuilder(String.valueOf(p).length() + 34 + String.valueOf(interfaceDescriptor).length());
                    sb.append("service descriptor mismatch: ");
                    sb.append(p);
                    sb.append(" vs. ");
                    sb.append(interfaceDescriptor);
                    Log.e("GmsClient", sb.toString());
                    return false;
                }
                IInterface q = yb2.this.q(this.g);
                if (q == null) {
                    return false;
                }
                if (!yb2.this.c0(2, 4, q) && !yb2.this.c0(3, 4, q)) {
                    return false;
                }
                yb2.this.w = null;
                Bundle y = yb2.this.y();
                if (yb2.this.s != null) {
                    yb2.this.s.e(y);
                }
                return true;
            } catch (RemoteException e) {
                Log.w("GmsClient", "service probably died");
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l extends f {
        @DexIgnore
        public l(int i, Bundle bundle) {
            super(i, null);
        }

        @DexIgnore
        @Override // com.fossil.yb2.f
        public final void f(z52 z52) {
            if (!yb2.this.B() || !yb2.this.n0()) {
                yb2.this.n.a(z52);
                yb2.this.M(z52);
                return;
            }
            yb2.this.e0(16);
        }

        @DexIgnore
        @Override // com.fossil.yb2.f
        public final boolean g() {
            yb2.this.n.a(z52.f);
            return true;
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public yb2(android.content.Context r10, android.os.Looper r11, int r12, com.fossil.yb2.a r13, com.fossil.yb2.b r14, java.lang.String r15) {
        /*
            r9 = this;
            com.fossil.gc2 r3 = com.fossil.gc2.b(r10)
            com.fossil.d62 r4 = com.fossil.d62.h()
            com.fossil.rc2.k(r13)
            r6 = r13
            com.fossil.yb2$a r6 = (com.fossil.yb2.a) r6
            com.fossil.rc2.k(r14)
            r7 = r14
            com.fossil.yb2$b r7 = (com.fossil.yb2.b) r7
            r0 = r9
            r1 = r10
            r2 = r11
            r5 = r12
            r8 = r15
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yb2.<init>(android.content.Context, android.os.Looper, int, com.fossil.yb2$a, com.fossil.yb2$b, java.lang.String):void");
    }

    @DexIgnore
    public yb2(Context context, Looper looper, gc2 gc2, d62 d62, int i2, a aVar, b bVar, String str) {
        this.k = new Object();
        this.l = new Object();
        this.p = new ArrayList<>();
        this.r = 1;
        this.w = null;
        this.x = false;
        this.y = null;
        this.z = new AtomicInteger(0);
        rc2.l(context, "Context must not be null");
        this.g = context;
        rc2.l(looper, "Looper must not be null");
        rc2.l(gc2, "Supervisor must not be null");
        this.h = gc2;
        rc2.l(d62, "API availability must not be null");
        this.i = d62;
        this.j = new g(looper);
        this.u = i2;
        this.s = aVar;
        this.t = bVar;
        this.v = str;
    }

    @DexIgnore
    public final void A() {
        if (!c()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    @DexIgnore
    public boolean B() {
        return false;
    }

    @DexIgnore
    public Account C() {
        return null;
    }

    @DexIgnore
    public b62[] D() {
        return A;
    }

    @DexIgnore
    public final Context E() {
        return this.g;
    }

    @DexIgnore
    public Bundle F() {
        return new Bundle();
    }

    @DexIgnore
    public String G() {
        return null;
    }

    @DexIgnore
    public Set<Scope> H() {
        return Collections.emptySet();
    }

    @DexIgnore
    public final T I() throws DeadObjectException {
        T t2;
        synchronized (this.k) {
            if (this.r != 5) {
                A();
                rc2.o(this.o != null, "Client is connected but service is null");
                t2 = this.o;
            } else {
                throw new DeadObjectException();
            }
        }
        return t2;
    }

    @DexIgnore
    public String J() {
        return "com.google.android.gms";
    }

    @DexIgnore
    public boolean K() {
        return false;
    }

    @DexIgnore
    public void L(T t2) {
        this.c = System.currentTimeMillis();
    }

    @DexIgnore
    public void M(z52 z52) {
        this.d = z52.c();
        this.e = System.currentTimeMillis();
    }

    @DexIgnore
    public void N(int i2) {
        this.f4292a = i2;
        this.b = System.currentTimeMillis();
    }

    @DexIgnore
    public void O(int i2, IBinder iBinder, Bundle bundle, int i3) {
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(1, i3, -1, new k(i2, iBinder, bundle)));
    }

    @DexIgnore
    public void P(int i2, T t2) {
    }

    @DexIgnore
    public boolean Q() {
        return false;
    }

    @DexIgnore
    public void R(int i2) {
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(6, this.z.get(), i2));
    }

    @DexIgnore
    public void S(c cVar, int i2, PendingIntent pendingIntent) {
        rc2.l(cVar, "Connection progress callbacks cannot be null.");
        this.n = cVar;
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(3, this.z.get(), i2, pendingIntent));
    }

    @DexIgnore
    public final void W(int i2, Bundle bundle, int i3) {
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(7, i3, -1, new l(i2, null)));
    }

    @DexIgnore
    public final void X(int i2, T t2) {
        boolean z2 = false;
        if ((i2 == 4) == (t2 != null)) {
            z2 = true;
        }
        rc2.a(z2);
        synchronized (this.k) {
            this.r = i2;
            this.o = t2;
            P(i2, t2);
            if (i2 != 1) {
                if (i2 == 2 || i2 == 3) {
                    if (!(this.q == null || this.f == null)) {
                        String d2 = this.f.d();
                        String a2 = this.f.a();
                        StringBuilder sb = new StringBuilder(String.valueOf(d2).length() + 70 + String.valueOf(a2).length());
                        sb.append("Calling connect() while still connected, missing disconnect() for ");
                        sb.append(d2);
                        sb.append(" on ");
                        sb.append(a2);
                        Log.e("GmsClient", sb.toString());
                        this.h.c(this.f.d(), this.f.a(), this.f.c(), this.q, l0(), this.f.b());
                        this.z.incrementAndGet();
                    }
                    this.q = new i(this.z.get());
                    je2 je2 = (this.r != 3 || G() == null) ? new je2(J(), x(), false, gc2.a(), K()) : new je2(E().getPackageName(), G(), true, gc2.a(), false);
                    this.f = je2;
                    if (!je2.b() || s() >= 17895000) {
                        if (!this.h.d(new gc2.a(this.f.d(), this.f.a(), this.f.c(), this.f.b()), this.q, l0())) {
                            String d3 = this.f.d();
                            String a3 = this.f.a();
                            StringBuilder sb2 = new StringBuilder(String.valueOf(d3).length() + 34 + String.valueOf(a3).length());
                            sb2.append("unable to connect to service: ");
                            sb2.append(d3);
                            sb2.append(" on ");
                            sb2.append(a3);
                            Log.e("GmsClient", sb2.toString());
                            W(16, null, this.z.get());
                        }
                    } else {
                        String valueOf = String.valueOf(this.f.d());
                        throw new IllegalStateException(valueOf.length() != 0 ? "Internal Error, the minimum apk version of this BaseGmsClient is too low to support dynamic lookup. Start service action: ".concat(valueOf) : new String("Internal Error, the minimum apk version of this BaseGmsClient is too low to support dynamic lookup. Start service action: "));
                    }
                } else if (i2 == 4) {
                    L(t2);
                }
            } else if (this.q != null) {
                this.h.c(this.f.d(), this.f.a(), this.f.c(), this.q, l0(), this.f.b());
                this.q = null;
            }
        }
    }

    @DexIgnore
    public void a() {
        this.z.incrementAndGet();
        synchronized (this.p) {
            int size = this.p.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.p.get(i2).a();
            }
            this.p.clear();
        }
        synchronized (this.l) {
            this.m = null;
        }
        X(1, null);
    }

    @DexIgnore
    public final void b0(ee2 ee2) {
        this.y = ee2;
    }

    @DexIgnore
    public boolean c() {
        boolean z2;
        synchronized (this.k) {
            z2 = this.r == 4;
        }
        return z2;
    }

    @DexIgnore
    public final boolean c0(int i2, int i3, T t2) {
        synchronized (this.k) {
            if (this.r != i2) {
                return false;
            }
            X(i3, t2);
            return true;
        }
    }

    @DexIgnore
    public final void e0(int i2) {
        int i3;
        if (m0()) {
            i3 = 5;
            this.x = true;
        } else {
            i3 = 4;
        }
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(i3, this.z.get(), 16));
    }

    @DexIgnore
    public void f(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int i2;
        T t2;
        nc2 nc2;
        synchronized (this.k) {
            i2 = this.r;
            t2 = this.o;
        }
        synchronized (this.l) {
            nc2 = this.m;
        }
        printWriter.append((CharSequence) str).append("mConnectState=");
        if (i2 == 1) {
            printWriter.print("DISCONNECTED");
        } else if (i2 == 2) {
            printWriter.print("REMOTE_CONNECTING");
        } else if (i2 == 3) {
            printWriter.print("LOCAL_CONNECTING");
        } else if (i2 == 4) {
            printWriter.print("CONNECTED");
        } else if (i2 != 5) {
            printWriter.print("UNKNOWN");
        } else {
            printWriter.print("DISCONNECTING");
        }
        printWriter.append(" mService=");
        if (t2 == null) {
            printWriter.append("null");
        } else {
            printWriter.append((CharSequence) p()).append("@").append((CharSequence) Integer.toHexString(System.identityHashCode(t2.asBinder())));
        }
        printWriter.append(" mServiceBroker=");
        if (nc2 == null) {
            printWriter.println("null");
        } else {
            printWriter.append("IGmsServiceBroker@").println(Integer.toHexString(System.identityHashCode(nc2.asBinder())));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        if (this.c > 0) {
            PrintWriter append = printWriter.append((CharSequence) str).append("lastConnectedTime=");
            long j2 = this.c;
            String format = simpleDateFormat.format(new Date(this.c));
            StringBuilder sb = new StringBuilder(String.valueOf(format).length() + 21);
            sb.append(j2);
            sb.append(" ");
            sb.append(format);
            append.println(sb.toString());
        }
        if (this.b > 0) {
            printWriter.append((CharSequence) str).append("lastSuspendedCause=");
            int i3 = this.f4292a;
            if (i3 == 1) {
                printWriter.append("CAUSE_SERVICE_DISCONNECTED");
            } else if (i3 != 2) {
                printWriter.append((CharSequence) String.valueOf(i3));
            } else {
                printWriter.append("CAUSE_NETWORK_LOST");
            }
            PrintWriter append2 = printWriter.append(" lastSuspendedTime=");
            long j3 = this.b;
            String format2 = simpleDateFormat.format(new Date(this.b));
            StringBuilder sb2 = new StringBuilder(String.valueOf(format2).length() + 21);
            sb2.append(j3);
            sb2.append(" ");
            sb2.append(format2);
            append2.println(sb2.toString());
        }
        if (this.e > 0) {
            printWriter.append((CharSequence) str).append("lastFailedStatus=").append((CharSequence) p62.getStatusCodeString(this.d));
            PrintWriter append3 = printWriter.append(" lastFailedTime=");
            long j4 = this.e;
            String format3 = simpleDateFormat.format(new Date(this.e));
            StringBuilder sb3 = new StringBuilder(String.valueOf(format3).length() + 21);
            sb3.append(j4);
            sb3.append(" ");
            sb3.append(format3);
            append3.println(sb3.toString());
        }
    }

    @DexIgnore
    public boolean g() {
        return false;
    }

    @DexIgnore
    public void i(jc2 jc2, Set<Scope> set) {
        Bundle F = F();
        dc2 dc2 = new dc2(this.u);
        dc2.e = this.g.getPackageName();
        dc2.h = F;
        if (set != null) {
            dc2.g = (Scope[]) set.toArray(new Scope[set.size()]);
        }
        if (v()) {
            dc2.i = C() != null ? C() : new Account("<<default account>>", "com.google");
            if (jc2 != null) {
                dc2.f = jc2.asBinder();
            }
        } else if (Q()) {
            dc2.i = C();
        }
        dc2.j = A;
        dc2.k = D();
        try {
            synchronized (this.l) {
                if (this.m != null) {
                    this.m.Y(new j(this, this.z.get()), dc2);
                } else {
                    Log.w("GmsClient", "mServiceBroker is null, client disconnected");
                }
            }
        } catch (DeadObjectException e2) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e2);
            R(1);
        } catch (SecurityException e3) {
            throw e3;
        } catch (RemoteException | RuntimeException e4) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e4);
            O(8, null, null, this.z.get());
        }
    }

    @DexIgnore
    public boolean j() {
        boolean z2;
        synchronized (this.k) {
            z2 = this.r == 2 || this.r == 3;
        }
        return z2;
    }

    @DexIgnore
    public String k() {
        je2 je2;
        if (c() && (je2 = this.f) != null) {
            return je2.a();
        }
        throw new RuntimeException("Failed to connect when checking package");
    }

    @DexIgnore
    public void l(c cVar) {
        rc2.l(cVar, "Connection progress callbacks cannot be null.");
        this.n = cVar;
        X(2, null);
    }

    @DexIgnore
    public final String l0() {
        String str = this.v;
        return str == null ? this.g.getClass().getName() : str;
    }

    @DexIgnore
    public final boolean m0() {
        boolean z2;
        synchronized (this.k) {
            z2 = this.r == 3;
        }
        return z2;
    }

    @DexIgnore
    public void n(e eVar) {
        eVar.a();
    }

    @DexIgnore
    public final boolean n0() {
        if (this.x || TextUtils.isEmpty(p()) || TextUtils.isEmpty(G())) {
            return false;
        }
        try {
            Class.forName(p());
            return true;
        } catch (ClassNotFoundException e2) {
            return false;
        }
    }

    @DexIgnore
    public abstract String p();

    @DexIgnore
    public abstract T q(IBinder iBinder);

    @DexIgnore
    public boolean r() {
        return true;
    }

    @DexIgnore
    public int s() {
        return d62.f740a;
    }

    @DexIgnore
    public final b62[] t() {
        ee2 ee2 = this.y;
        if (ee2 == null) {
            return null;
        }
        return ee2.c;
    }

    @DexIgnore
    public Intent u() {
        throw new UnsupportedOperationException("Not a sign in API");
    }

    @DexIgnore
    public boolean v() {
        return false;
    }

    @DexIgnore
    public IBinder w() {
        synchronized (this.l) {
            if (this.m == null) {
                return null;
            }
            return this.m.asBinder();
        }
    }

    @DexIgnore
    public abstract String x();

    @DexIgnore
    public Bundle y() {
        return null;
    }

    @DexIgnore
    public void z() {
        int j2 = this.i.j(this.g, s());
        if (j2 != 0) {
            X(1, null);
            S(new d(), j2, null);
            return;
        }
        l(new d());
    }
}
