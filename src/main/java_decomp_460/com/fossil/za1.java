package com.fossil;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class za1 implements Closeable {
    @DexIgnore
    public /* final */ InputStream b;
    @DexIgnore
    public /* final */ Charset c;
    @DexIgnore
    public byte[] d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ByteArrayOutputStream {
        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        public String toString() {
            int i = ((ByteArrayOutputStream) this).count;
            try {
                return new String(((ByteArrayOutputStream) this).buf, 0, (i <= 0 || ((ByteArrayOutputStream) this).buf[i + -1] != 13) ? ((ByteArrayOutputStream) this).count : i - 1, za1.this.c.name());
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError(e);
            }
        }
    }

    @DexIgnore
    public za1(InputStream inputStream, int i, Charset charset) {
        if (inputStream == null || charset == null) {
            throw null;
        } else if (i < 0) {
            throw new IllegalArgumentException("capacity <= 0");
        } else if (charset.equals(ab1.f240a)) {
            this.b = inputStream;
            this.c = charset;
            this.d = new byte[i];
        } else {
            throw new IllegalArgumentException("Unsupported encoding");
        }
    }

    @DexIgnore
    public za1(InputStream inputStream, Charset charset) {
        this(inputStream, 8192, charset);
    }

    @DexIgnore
    public final void b() throws IOException {
        InputStream inputStream = this.b;
        byte[] bArr = this.d;
        int read = inputStream.read(bArr, 0, bArr.length);
        if (read != -1) {
            this.e = 0;
            this.f = read;
            return;
        }
        throw new EOFException();
    }

    @DexIgnore
    public boolean c() {
        return this.f == -1;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        synchronized (this.b) {
            if (this.d != null) {
                this.d = null;
                this.b.close();
            }
        }
    }

    @DexIgnore
    public String f() throws IOException {
        int i;
        String aVar;
        int i2;
        synchronized (this.b) {
            if (this.d != null) {
                if (this.e >= this.f) {
                    b();
                }
                int i3 = this.e;
                while (true) {
                    if (i3 == this.f) {
                        a aVar2 = new a((this.f - this.e) + 80);
                        loop1:
                        while (true) {
                            aVar2.write(this.d, this.e, this.f - this.e);
                            this.f = -1;
                            b();
                            i = this.e;
                            while (true) {
                                if (i != this.f) {
                                    if (this.d[i] == 10) {
                                        break loop1;
                                    }
                                    i++;
                                }
                            }
                        }
                        if (i != this.e) {
                            aVar2.write(this.d, this.e, i - this.e);
                        }
                        this.e = i + 1;
                        aVar = aVar2.toString();
                    } else if (this.d[i3] == 10) {
                        if (i3 != this.e) {
                            int i4 = i3 - 1;
                            if (this.d[i4] == 13) {
                                i2 = i4;
                                aVar = new String(this.d, this.e, i2 - this.e, this.c.name());
                                this.e = i3 + 1;
                            }
                        }
                        i2 = i3;
                        aVar = new String(this.d, this.e, i2 - this.e, this.c.name());
                        this.e = i3 + 1;
                    } else {
                        i3++;
                    }
                }
            } else {
                throw new IOException("LineReader is closed");
            }
        }
        return aVar;
    }
}
