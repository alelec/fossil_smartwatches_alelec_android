package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x58 extends t58 {
    @DexIgnore
    public String option;

    @DexIgnore
    public x58(String str) {
        super(str);
    }

    @DexIgnore
    public x58(String str, String str2) {
        this(str);
        this.option = str2;
    }

    @DexIgnore
    public String getOption() {
        return this.option;
    }
}
