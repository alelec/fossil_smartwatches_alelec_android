package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d48 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ byte[] f737a; // = l48.Companion.d("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/").getData$okio();
    @DexIgnore
    public static /* final */ byte[] b; // = l48.Companion.d("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_").getData$okio();

    @DexIgnore
    public static final byte[] a(String str) {
        int i;
        int i2;
        pq7.c(str, "$this$decodeBase64ToArray");
        int length = str.length();
        while (length > 0) {
            char charAt = str.charAt(length - 1);
            if (charAt != '=' && charAt != '\n' && charAt != '\r' && charAt != ' ' && charAt != '\t') {
                break;
            }
            length--;
        }
        int i3 = (int) ((((long) length) * 6) / 8);
        byte[] bArr = new byte[i3];
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (i7 < length) {
            char charAt2 = str.charAt(i7);
            if ('A' <= charAt2 && 'Z' >= charAt2) {
                i = charAt2 - 'A';
            } else if ('a' <= charAt2 && 'z' >= charAt2) {
                i = charAt2 - 'G';
            } else if ('0' <= charAt2 && '9' >= charAt2) {
                i = charAt2 + 4;
            } else if (charAt2 == '+' || charAt2 == '-') {
                i = 62;
            } else if (charAt2 == '/' || charAt2 == '_') {
                i = 63;
            } else if (charAt2 == '\n' || charAt2 == '\r' || charAt2 == ' ') {
                i2 = i5;
                i7++;
                i5 = i2;
            } else if (charAt2 != '\t') {
                return null;
            } else {
                i2 = i5;
                i7++;
                i5 = i2;
            }
            i2 = i | (i5 << 6);
            int i8 = i6 + 1;
            if (i8 % 4 == 0) {
                int i9 = i4 + 1;
                bArr[i4] = (byte) ((byte) (i2 >> 16));
                int i10 = i9 + 1;
                bArr[i9] = (byte) ((byte) (i2 >> 8));
                bArr[i10] = (byte) ((byte) i2);
                i4 = i10 + 1;
                i6 = i8;
            } else {
                i6 = i8;
            }
            i7++;
            i5 = i2;
        }
        int i11 = i6 % 4;
        if (i11 == 1) {
            return null;
        }
        if (i11 == 2) {
            bArr[i4] = (byte) ((byte) ((i5 << 12) >> 16));
            i4++;
        } else if (i11 == 3) {
            int i12 = i5 << 6;
            int i13 = i4 + 1;
            bArr[i4] = (byte) ((byte) (i12 >> 16));
            i4 = i13 + 1;
            bArr[i13] = (byte) ((byte) (i12 >> 8));
        }
        if (i4 == i3) {
            return bArr;
        }
        byte[] copyOf = Arrays.copyOf(bArr, i4);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, newSize)");
        return copyOf;
    }

    @DexIgnore
    public static final String b(byte[] bArr, byte[] bArr2) {
        pq7.c(bArr, "$this$encodeBase64");
        pq7.c(bArr2, Constants.MAP);
        byte[] bArr3 = new byte[(((bArr.length + 2) / 3) * 4)];
        int length = bArr.length - (bArr.length % 3);
        int i = 0;
        int i2 = 0;
        while (i2 < length) {
            int i3 = i2 + 1;
            byte b2 = bArr[i2];
            int i4 = i3 + 1;
            byte b3 = bArr[i3];
            byte b4 = bArr[i4];
            int i5 = i + 1;
            bArr3[i] = (byte) bArr2[(b2 & 255) >> 2];
            int i6 = i5 + 1;
            bArr3[i5] = (byte) bArr2[((b2 & 3) << 4) | ((b3 & 255) >> 4)];
            int i7 = i6 + 1;
            bArr3[i6] = (byte) bArr2[((b3 & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY) << 2) | ((b4 & 255) >> 6)];
            i = i7 + 1;
            bArr3[i7] = (byte) bArr2[b4 & 63];
            i2 = i4 + 1;
        }
        int length2 = bArr.length - length;
        if (length2 == 1) {
            byte b5 = bArr[i2];
            int i8 = i + 1;
            bArr3[i] = (byte) bArr2[(b5 & 255) >> 2];
            int i9 = i8 + 1;
            bArr3[i8] = (byte) bArr2[(b5 & 3) << 4];
            byte b6 = (byte) 61;
            bArr3[i9] = (byte) b6;
            bArr3[i9 + 1] = (byte) b6;
        } else if (length2 == 2) {
            byte b7 = bArr[i2];
            byte b8 = bArr[i2 + 1];
            int i10 = i + 1;
            bArr3[i] = (byte) bArr2[(b7 & 255) >> 2];
            int i11 = i10 + 1;
            bArr3[i10] = (byte) bArr2[((b7 & 3) << 4) | ((b8 & 255) >> 4)];
            bArr3[i11] = (byte) bArr2[(b8 & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY) << 2];
            bArr3[i11 + 1] = (byte) ((byte) 61);
        }
        return e48.b(bArr3);
    }

    @DexIgnore
    public static /* synthetic */ String c(byte[] bArr, byte[] bArr2, int i, Object obj) {
        if ((i & 1) != 0) {
            bArr2 = f737a;
        }
        return b(bArr, bArr2);
    }

    @DexIgnore
    public static final byte[] d() {
        return b;
    }
}
