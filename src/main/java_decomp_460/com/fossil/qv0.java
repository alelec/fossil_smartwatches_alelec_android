package com.fossil;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qv0 extends RecyclerView.o {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public RecyclerView f3029a;
    @DexIgnore
    public Scroller b;
    @DexIgnore
    public /* final */ RecyclerView.q c; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.q {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f3030a; // = false;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            super.onScrollStateChanged(recyclerView, i);
            if (i == 0 && this.f3030a) {
                this.f3030a = false;
                qv0.this.l();
            }
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            if (i != 0 || i2 != 0) {
                this.f3030a = true;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends gv0 {
        @DexIgnore
        public b(Context context) {
            super(context);
        }

        @DexIgnore
        @Override // com.fossil.gv0, androidx.recyclerview.widget.RecyclerView.v
        public void o(View view, RecyclerView.State state, RecyclerView.v.a aVar) {
            qv0 qv0 = qv0.this;
            RecyclerView recyclerView = qv0.f3029a;
            if (recyclerView != null) {
                int[] c = qv0.c(recyclerView.getLayoutManager(), view);
                int i = c[0];
                int i2 = c[1];
                int w = w(Math.max(Math.abs(i), Math.abs(i2)));
                if (w > 0) {
                    aVar.d(i, i2, w, this.j);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.gv0
        public float v(DisplayMetrics displayMetrics) {
            return 100.0f / ((float) displayMetrics.densityDpi);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.o
    public boolean a(int i, int i2) {
        RecyclerView.m layoutManager = this.f3029a.getLayoutManager();
        if (layoutManager == null || this.f3029a.getAdapter() == null) {
            return false;
        }
        int minFlingVelocity = this.f3029a.getMinFlingVelocity();
        return (Math.abs(i2) > minFlingVelocity || Math.abs(i) > minFlingVelocity) && k(layoutManager, i, i2);
    }

    @DexIgnore
    public void b(RecyclerView recyclerView) throws IllegalStateException {
        RecyclerView recyclerView2 = this.f3029a;
        if (recyclerView2 != recyclerView) {
            if (recyclerView2 != null) {
                g();
            }
            this.f3029a = recyclerView;
            if (recyclerView != null) {
                j();
                this.b = new Scroller(this.f3029a.getContext(), new DecelerateInterpolator());
                l();
            }
        }
    }

    @DexIgnore
    public abstract int[] c(RecyclerView.m mVar, View view);

    @DexIgnore
    public int[] d(int i, int i2) {
        this.b.fling(0, 0, i, i2, RecyclerView.UNDEFINED_DURATION, Integer.MAX_VALUE, RecyclerView.UNDEFINED_DURATION, Integer.MAX_VALUE);
        return new int[]{this.b.getFinalX(), this.b.getFinalY()};
    }

    @DexIgnore
    public RecyclerView.v e(RecyclerView.m mVar) {
        return f(mVar);
    }

    @DexIgnore
    @Deprecated
    public gv0 f(RecyclerView.m mVar) {
        if (!(mVar instanceof RecyclerView.v.b)) {
            return null;
        }
        return new b(this.f3029a.getContext());
    }

    @DexIgnore
    public final void g() {
        this.f3029a.removeOnScrollListener(this.c);
        this.f3029a.setOnFlingListener(null);
    }

    @DexIgnore
    public abstract View h(RecyclerView.m mVar);

    @DexIgnore
    public abstract int i(RecyclerView.m mVar, int i, int i2);

    @DexIgnore
    public final void j() throws IllegalStateException {
        if (this.f3029a.getOnFlingListener() == null) {
            this.f3029a.addOnScrollListener(this.c);
            this.f3029a.setOnFlingListener(this);
            return;
        }
        throw new IllegalStateException("An instance of OnFlingListener already set.");
    }

    @DexIgnore
    public final boolean k(RecyclerView.m mVar, int i, int i2) {
        RecyclerView.v e;
        int i3;
        if (!(mVar instanceof RecyclerView.v.b) || (e = e(mVar)) == null || (i3 = i(mVar, i, i2)) == -1) {
            return false;
        }
        e.p(i3);
        mVar.K1(e);
        return true;
    }

    @DexIgnore
    public void l() {
        RecyclerView.m layoutManager;
        View h;
        RecyclerView recyclerView = this.f3029a;
        if (recyclerView != null && (layoutManager = recyclerView.getLayoutManager()) != null && (h = h(layoutManager)) != null) {
            int[] c2 = c(layoutManager, h);
            if (c2[0] != 0 || c2[1] != 0) {
                this.f3029a.smoothScrollBy(c2[0], c2[1]);
            }
        }
    }
}
