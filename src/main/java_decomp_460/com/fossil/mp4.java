package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mp4 implements Factory<hn5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f2408a;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> b;
    @DexIgnore
    public /* final */ Provider<dr5> c;
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> d;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> e;
    @DexIgnore
    public /* final */ Provider<on5> f;
    @DexIgnore
    public /* final */ Provider<a37> g;
    @DexIgnore
    public /* final */ Provider<or5> h;
    @DexIgnore
    public /* final */ Provider<fs5> i;

    @DexIgnore
    public mp4(uo4 uo4, Provider<HybridPresetRepository> provider, Provider<dr5> provider2, Provider<QuickResponseRepository> provider3, Provider<AlarmsRepository> provider4, Provider<on5> provider5, Provider<a37> provider6, Provider<or5> provider7, Provider<fs5> provider8) {
        this.f2408a = uo4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
        this.h = provider7;
        this.i = provider8;
    }

    @DexIgnore
    public static mp4 a(uo4 uo4, Provider<HybridPresetRepository> provider, Provider<dr5> provider2, Provider<QuickResponseRepository> provider3, Provider<AlarmsRepository> provider4, Provider<on5> provider5, Provider<a37> provider6, Provider<or5> provider7, Provider<fs5> provider8) {
        return new mp4(uo4, provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8);
    }

    @DexIgnore
    public static hn5 c(uo4 uo4, HybridPresetRepository hybridPresetRepository, dr5 dr5, QuickResponseRepository quickResponseRepository, AlarmsRepository alarmsRepository, on5 on5, a37 a37, or5 or5, fs5 fs5) {
        hn5 t = uo4.t(hybridPresetRepository, dr5, quickResponseRepository, alarmsRepository, on5, a37, or5, fs5);
        lk7.c(t, "Cannot return null from a non-@Nullable @Provides method");
        return t;
    }

    @DexIgnore
    /* renamed from: b */
    public hn5 get() {
        return c(this.f2408a, this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get());
    }
}
