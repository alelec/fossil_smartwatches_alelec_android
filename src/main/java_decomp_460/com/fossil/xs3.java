package com.fossil;

import android.content.Context;
import android.os.Looper;
import com.fossil.m62;
import com.fossil.r62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xs3 extends m62.a<hs3, gs3> {
    @DexIgnore
    /* Return type fixed from 'com.fossil.m62$f' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [android.content.Context, android.os.Looper, com.fossil.ac2, java.lang.Object, com.fossil.r62$b, com.fossil.r62$c] */
    @Override // com.fossil.m62.a
    public final /* synthetic */ hs3 c(Context context, Looper looper, ac2 ac2, gs3 gs3, r62.b bVar, r62.c cVar) {
        gs3 gs32 = gs3;
        return new hs3(context, looper, true, ac2, gs32 == null ? gs3.k : gs32, bVar, cVar);
    }
}
