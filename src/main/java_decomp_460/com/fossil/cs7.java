package com.fossil;

import java.lang.annotation.Annotation;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface cs7 {
    @DexIgnore
    List<Annotation> getAnnotations();
}
