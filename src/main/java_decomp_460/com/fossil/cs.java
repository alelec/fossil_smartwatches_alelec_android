package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cs extends ss {
    @DexIgnore
    public long A; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public r4 B; // = r4.BOND_NONE;

    @DexIgnore
    public cs(k5 k5Var) {
        super(hs.Z, k5Var);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(super.A(), jd0.D0, ey1.a(this.B));
    }

    @DexIgnore
    @Override // com.fossil.ns
    public u5 D() {
        return new a6(this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void f(long j) {
        this.A = j;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void g(u5 u5Var) {
        this.B = ((a6) u5Var).k;
        this.g.add(new hw(0, null, null, g80.k(new JSONObject(), jd0.D0, ey1.a(this.B)), 7));
    }

    @DexIgnore
    @Override // com.fossil.fs
    public long x() {
        return this.A;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(super.z(), jd0.o1, ey1.a(this.y.H()));
    }
}
