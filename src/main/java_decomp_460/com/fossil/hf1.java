package com.fossil;

import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import com.fossil.af1;
import java.io.File;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hf1<Data> implements af1<String, Data> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ af1<Uri, Data> f1472a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements bf1<String, AssetFileDescriptor> {
        @DexIgnore
        @Override // com.fossil.bf1
        public af1<String, AssetFileDescriptor> b(ef1 ef1) {
            return new hf1(ef1.d(Uri.class, AssetFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements bf1<String, ParcelFileDescriptor> {
        @DexIgnore
        @Override // com.fossil.bf1
        public af1<String, ParcelFileDescriptor> b(ef1 ef1) {
            return new hf1(ef1.d(Uri.class, ParcelFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements bf1<String, InputStream> {
        @DexIgnore
        @Override // com.fossil.bf1
        public af1<String, InputStream> b(ef1 ef1) {
            return new hf1(ef1.d(Uri.class, InputStream.class));
        }
    }

    @DexIgnore
    public hf1(af1<Uri, Data> af1) {
        this.f1472a = af1;
    }

    @DexIgnore
    public static Uri e(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.charAt(0) == '/') {
            return f(str);
        }
        Uri parse = Uri.parse(str);
        return parse.getScheme() == null ? f(str) : parse;
    }

    @DexIgnore
    public static Uri f(String str) {
        return Uri.fromFile(new File(str));
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<Data> b(String str, int i, int i2, ob1 ob1) {
        Uri e = e(str);
        if (e == null || !this.f1472a.a(e)) {
            return null;
        }
        return this.f1472a.b(e, i, i2, ob1);
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(String str) {
        return true;
    }
}
