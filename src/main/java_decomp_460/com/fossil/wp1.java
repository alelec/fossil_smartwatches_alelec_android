package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ix1;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wp1 extends vp1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ gu1 d;
    @DexIgnore
    public /* final */ fu1 e;
    @DexIgnore
    public /* final */ iu1 f;
    @DexIgnore
    public /* final */ byte[] g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<wp1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public wp1 createFromParcel(Parcel parcel) {
            return new wp1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public wp1[] newArray(int i) {
            return new wp1[i];
        }
    }

    @DexIgnore
    public wp1(byte b, gu1 gu1, fu1 fu1, iu1 iu1, byte[] bArr, int i2, int i3) {
        super(np1.ENCRYPTED_DATA, b);
        this.d = gu1;
        this.e = fu1;
        this.f = iu1;
        this.g = bArr;
        this.h = i2;
        this.i = i3;
    }

    @DexIgnore
    public /* synthetic */ wp1(Parcel parcel, kq7 kq7) {
        super(parcel);
        gu1 a2 = gu1.d.a(parcel.readByte());
        if (a2 != null) {
            this.d = a2;
            fu1 a3 = fu1.d.a(parcel.readByte());
            if (a3 != null) {
                this.e = a3;
                iu1 a4 = iu1.d.a(parcel.readByte());
                if (a4 != null) {
                    this.f = a4;
                    byte[] createByteArray = parcel.createByteArray();
                    this.g = createByteArray == null ? new byte[0] : createByteArray;
                    this.h = parcel.readInt();
                    this.i = parcel.readInt();
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(wp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            wp1 wp1 = (wp1) obj;
            if (this.d != wp1.d) {
                return false;
            }
            if (this.e != wp1.e) {
                return false;
            }
            if (this.f != wp1.f) {
                return false;
            }
            if (!Arrays.equals(this.g, wp1.g)) {
                return false;
            }
            if (this.h != wp1.h) {
                return false;
            }
            return this.i == wp1.i;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.EncryptedDataNotification");
    }

    @DexIgnore
    public final fu1 getEncryptMethod() {
        return this.e;
    }

    @DexIgnore
    public final byte[] getEncryptedData() {
        return this.g;
    }

    @DexIgnore
    public final gu1 getEncryptedDataType() {
        return this.d;
    }

    @DexIgnore
    public final iu1 getKeyType() {
        return this.f;
    }

    @DexIgnore
    public final short getSequence() {
        return hy1.p(a());
    }

    @DexIgnore
    public final int getXorKeyFirstOffset() {
        return this.h;
    }

    @DexIgnore
    public final int getXorKeySecondOffset() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public int hashCode() {
        int hashCode = super.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        int hashCode4 = this.f.hashCode();
        int hashCode5 = this.g.hashCode();
        return (((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + Integer.valueOf(this.h).hashCode()) * 31) + Integer.valueOf(this.i).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(super.toJSONObject(), jd0.j0, ey1.a(this.d)), jd0.k5, ey1.a(this.e)), jd0.l5, ey1.a(this.f)), jd0.U0, Long.valueOf(ix1.f1688a.b(this.g, ix1.a.CRC32))), jd0.z5, Integer.valueOf(this.h)), jd0.A5, Integer.valueOf(this.i));
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeByte(this.d.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.f.a());
        }
        if (parcel != null) {
            parcel.writeByteArray(this.g);
        }
        if (parcel != null) {
            parcel.writeInt(this.h);
        }
        if (parcel != null) {
            parcel.writeInt(this.i);
        }
    }
}
