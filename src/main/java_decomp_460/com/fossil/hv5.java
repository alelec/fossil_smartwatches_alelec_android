package com.fossil;

import com.fossil.iq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.manager.SoLibraryLoader;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hv5 extends iq4<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public /* final */ xn5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return hv5.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ WeakReference<ls5> f1539a;

        @DexIgnore
        public b(WeakReference<ls5> weakReference) {
            pq7.c(weakReference, "activityContext");
            this.f1539a = weakReference;
        }

        @DexIgnore
        public final WeakReference<ls5> a() {
            return this.f1539a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1540a;
        @DexIgnore
        public /* final */ z52 b;

        @DexIgnore
        public c(int i, z52 z52) {
            this.f1540a = i;
            this.b = z52;
        }

        @DexIgnore
        public final int a() {
            return this.f1540a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ SignUpSocialAuth f1541a;

        @DexIgnore
        public d(SignUpSocialAuth signUpSocialAuth) {
            pq7.c(signUpSocialAuth, "auth");
            this.f1541a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.f1541a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements yn5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ hv5 f1542a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(hv5 hv5) {
            this.f1542a = hv5;
        }

        @DexIgnore
        @Override // com.fossil.yn5
        public void a(SignUpSocialAuth signUpSocialAuth) {
            pq7.c(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = hv5.f.a();
            local.d(a2, "Inside .onLoginSuccess with auth=" + signUpSocialAuth);
            this.f1542a.j(new d(signUpSocialAuth));
        }

        @DexIgnore
        @Override // com.fossil.yn5
        public void b(int i, z52 z52, String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = hv5.f.a();
            local.d(a2, "Inside .onLoginFailed with errorCode=" + i + ", connectionResult=" + z52);
            this.f1542a.i(new c(i, z52));
        }
    }

    /*
    static {
        String simpleName = hv5.class.getSimpleName();
        pq7.b(simpleName, "LoginWeiboUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public hv5(xn5 xn5) {
        pq7.c(xn5, "mLoginWeiboManager");
        this.d = xn5;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return e;
    }

    @DexIgnore
    /* renamed from: n */
    public Object k(b bVar, qn7<Object> qn7) {
        String str;
        try {
            FLogger.INSTANCE.getLocal().d(e, "running UseCase");
            xn5 xn5 = this.d;
            Access c2 = SoLibraryLoader.f().c(PortfolioApp.h0.c());
            if (c2 == null || (str = c2.getF()) == null) {
                str = "";
            }
            xn5.f(str, rq4.G.B(), rq4.G.C());
            xn5 xn52 = this.d;
            WeakReference<ls5> a2 = bVar != null ? bVar.a() : null;
            if (a2 != null) {
                ls5 ls5 = a2.get();
                if (ls5 != null) {
                    pq7.b(ls5, "requestValues?.activityContext!!.get()!!");
                    xn52.d(ls5, new e(this));
                    return tl7.f3441a;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = e;
            local.d(str2, "Inside .run failed with exception=" + e2);
            return new c(600, null);
        }
    }
}
