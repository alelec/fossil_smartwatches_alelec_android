package com.fossil;

import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vn3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference b;
    @DexIgnore
    public /* final */ /* synthetic */ un3 c;

    @DexIgnore
    public vn3(un3 un3, AtomicReference atomicReference) {
        this.c = un3;
        this.b = atomicReference;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.b) {
            try {
                this.b.set(Boolean.valueOf(this.c.m().J(this.c.q().C())));
                this.b.notify();
            } catch (Throwable th) {
                this.b.notify();
                throw th;
            }
        }
    }
}
