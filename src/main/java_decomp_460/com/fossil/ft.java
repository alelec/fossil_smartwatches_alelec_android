package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ft extends bt {
    @DexIgnore
    public byte[] K; // = new byte[0];
    @DexIgnore
    public /* final */ rt L;
    @DexIgnore
    public /* final */ byte[] M;

    @DexIgnore
    public ft(k5 k5Var, rt rtVar, byte[] bArr) {
        super(k5Var, ut.SEND_PHONE_RANDOM_NUMBER, hs.H, 0, 8);
        this.L = rtVar;
        this.M = bArr;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(super.A(), jd0.q2, dy1.e(this.K, null, 1, null));
    }

    @DexIgnore
    @Override // com.fossil.ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 17) {
            if (this.L.b == bArr[0]) {
                byte[] k = dm7.k(bArr, 1, 17);
                this.K = k;
                g80.k(jSONObject, jd0.q2, dy1.e(k, null, 1, null));
                this.v = mw.a(this.v, null, null, lw.b, null, null, 27);
            } else {
                this.v = mw.a(this.v, null, null, lw.p, null, null, 27);
            }
        } else {
            this.v = mw.a(this.v, null, null, lw.k, null, null, 27);
        }
        this.E = true;
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(this.M.length + 1).order(ByteOrder.LITTLE_ENDIAN).put(this.L.b).put(this.M).array();
        pq7.b(array, "ByteBuffer.allocate(1 + \u2026\n                .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(g80.k(super.z(), jd0.u2, ey1.a(this.L)), jd0.o2, dy1.e(this.M, null, 1, null));
    }
}
