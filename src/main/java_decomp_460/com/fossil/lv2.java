package com.fossil;

import android.database.ContentObserver;
import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lv2 extends ContentObserver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ jv2 f2254a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public lv2(jv2 jv2, Handler handler) {
        super(null);
        this.f2254a = jv2;
    }

    @DexIgnore
    public final void onChange(boolean z) {
        this.f2254a.c();
    }
}
