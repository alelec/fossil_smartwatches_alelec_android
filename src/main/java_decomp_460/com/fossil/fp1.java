package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fp1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ rn1 c;
    @DexIgnore
    public /* final */ float d;
    @DexIgnore
    public /* final */ tn1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<fp1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public fp1 createFromParcel(Parcel parcel) {
            long readLong = parcel.readLong();
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                rn1 valueOf = rn1.valueOf(readString);
                float readFloat = parcel.readFloat();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    pq7.b(readString2, "parcel.readString()!!");
                    return new fp1(readLong, valueOf, readFloat, tn1.valueOf(readString2));
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public fp1[] newArray(int i) {
            return new fp1[i];
        }
    }

    @DexIgnore
    public fp1(long j, rn1 rn1, float f, tn1 tn1) {
        this.b = j;
        this.c = rn1;
        this.d = hy1.e(f, 2);
        this.e = tn1;
    }

    @DexIgnore
    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("alive", this.b);
            jSONObject.put(Constants.PROFILE_KEY_UNIT, ey1.a(this.c));
            jSONObject.put("temp", Float.valueOf(this.d));
            jSONObject.put("cond_id", this.e.a());
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(fp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            fp1 fp1 = (fp1) obj;
            if (this.b != fp1.b) {
                return false;
            }
            if (this.c != fp1.c) {
                return false;
            }
            if (this.d != fp1.d) {
                return false;
            }
            return this.e == fp1.e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.CompactWeatherInfo");
    }

    @DexIgnore
    public final long getExpiredTimeStampInSecond() {
        return this.b;
    }

    @DexIgnore
    public final float getTemperature() {
        return this.d;
    }

    @DexIgnore
    public final rn1 getTemperatureUnit() {
        return this.c;
    }

    @DexIgnore
    public final tn1 getWeatherCondition() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        return (((((((int) this.b) * 31) + hashCode) * 31) + Float.valueOf(this.d).hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.M2, Long.valueOf(this.b)), jd0.u, ey1.a(this.c)), jd0.T1, Float.valueOf(this.d)), jd0.t, ey1.a(this.e));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeFloat(this.d);
        }
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
    }
}
