package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Base64;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yh implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ mi b;
    @DexIgnore
    public /* final */ /* synthetic */ File c;
    @DexIgnore
    public /* final */ /* synthetic */ int d;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList e;

    @DexIgnore
    public yh(mi miVar, File file, int i, ArrayList arrayList) {
        this.b = miVar;
        this.c = file;
        this.d = i;
        this.e = arrayList;
    }

    @DexIgnore
    public final void run() {
        Boolean bool;
        byte[] b2;
        Context a2 = id0.i.a();
        ActivityManager activityManager = (ActivityManager) (a2 != null ? a2.getSystemService(Constants.ACTIVITY) : null);
        if (activityManager != null) {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            activityManager.getMemoryInfo(memoryInfo);
            bool = Boolean.valueOf(memoryInfo.lowMemory);
        } else {
            bool = null;
        }
        if (pq7.a(bool, Boolean.FALSE) && (b2 = jy1.f1832a.b(this.c)) != null) {
            w80 w80 = w80.i;
            v80 v80 = v80.j;
            mi miVar = this.b;
            w80.d(new a90("gps_log", v80, miVar.w.x, ey1.a(miVar.y), this.b.z, true, null, null, null, g80.k(new JSONObject(), jd0.Y2, Base64.encodeToString(b2, 2)), 448));
        }
        this.c.delete();
        if (this.d == this.e.size() - 1) {
            z80.c(w80.i, 0, 1, null);
        }
    }
}
