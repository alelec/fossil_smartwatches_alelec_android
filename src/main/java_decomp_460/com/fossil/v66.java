package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v66 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ e76 f3725a;

    @DexIgnore
    public v66(e76 e76, String str, String str2) {
        pq7.c(e76, "mView");
        pq7.c(str, "mPresetId");
        this.f3725a = e76;
    }

    @DexIgnore
    public final e76 a() {
        return this.f3725a;
    }
}
