package com.fossil;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.FileRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jb6 extends iq4<b, c, a> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public b e;
    @DexIgnore
    public /* final */ d f; // = new d();
    @DexIgnore
    public mo5 g;
    @DexIgnore
    public ArrayList<oo5> h; // = new ArrayList<>();
    @DexIgnore
    public /* final */ uo5 i;
    @DexIgnore
    public /* final */ FileRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1737a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public a(int i, ArrayList<Integer> arrayList) {
            pq7.c(arrayList, "mBLEErrorCodes");
            this.f1737a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.f1737a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1738a;
        @DexIgnore
        public /* final */ List<oo5> b;
        @DexIgnore
        public /* final */ List<DianaAppSetting> c;

        @DexIgnore
        public b(String str, List<oo5> list, List<DianaAppSetting> list2) {
            pq7.c(str, "presetId");
            pq7.c(list, "presetButtons");
            pq7.c(list2, Constants.USER_SETTING);
            this.f1738a = str;
            this.b = list;
            this.c = list2;
        }

        @DexIgnore
        public final List<oo5> a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.f1738a;
        }

        @DexIgnore
        public final List<DianaAppSetting> c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements wq5.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase$SetPresetBroadcastReceiver$receive$1", f = "SetWatchAppUseCase.kt", l = {52}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Intent $intent;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, Intent intent, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
                this.$intent = intent;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$intent, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    jb6 jb6 = jb6.this;
                    this.L$0 = iv7;
                    this.label = 1;
                    if (jb6.s(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (this.$intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    FLogger.INSTANCE.getLocal().d("SetWatchAppUseCase", "onReceive - success");
                    jb6.this.j(new c());
                } else {
                    FLogger.INSTANCE.getLocal().d("SetWatchAppUseCase", "onReceive - failed isSettingChangedOnly");
                    int intExtra = this.$intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                    ArrayList<Integer> integerArrayListExtra = this.$intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>(intExtra);
                    }
                    jb6.this.i(new a(intExtra, integerArrayListExtra));
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetWatchAppUseCase", "onReceive - communicateMode=" + communicateMode);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            if (jb6.this.n()) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("SetWatchAppUseCase", "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
                if (communicateMode == CommunicateMode.SET_WATCH_APPS) {
                    jb6.this.q(false);
                    xw7 unused = gu7.d(jb6.this.g(), null, null, new a(this, intent, null), 3, null);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase", f = "SetWatchAppUseCase.kt", l = {92, 107, 111}, m = "run")
    public static final class e extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ jb6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(jb6 jb6, qn7 qn7) {
            super(qn7);
            this.this$0 = jb6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase", f = "SetWatchAppUseCase.kt", l = {77, 81}, m = "updatePresetButtons")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ jb6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(jb6 jb6, qn7 qn7) {
            super(qn7);
            this.this$0 = jb6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(this);
        }
    }

    @DexIgnore
    public jb6(uo5 uo5, FileRepository fileRepository, DianaAppSettingRepository dianaAppSettingRepository, DianaAppSettingRepository dianaAppSettingRepository2) {
        pq7.c(uo5, "mDianaPresetRepository");
        pq7.c(fileRepository, "mFileRepository");
        pq7.c(dianaAppSettingRepository, "mDianaAppSetting");
        pq7.c(dianaAppSettingRepository2, "mDianaAppSettingRepository");
        this.i = uo5;
        this.j = fileRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "SetWatchAppUseCase";
    }

    @DexIgnore
    public final boolean n() {
        return this.d;
    }

    @DexIgnore
    public final void o() {
        wq5.d.e(this.f, CommunicateMode.SET_WATCH_APPS);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x016b  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01fd  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x021b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* renamed from: p */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.jb6.b r13, com.fossil.qn7<java.lang.Object> r14) {
        /*
        // Method dump skipped, instructions count: 561
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb6.k(com.fossil.jb6$b, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void q(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final void r() {
        wq5.d.j(this.f, CommunicateMode.SET_WATCH_APPS);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object s(com.fossil.qn7<? super com.fossil.tl7> r9) {
        /*
            r8 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 2
            boolean r0 = r9 instanceof com.fossil.jb6.f
            if (r0 == 0) goto L_0x0049
            r0 = r9
            com.fossil.jb6$f r0 = (com.fossil.jb6.f) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0049
            int r1 = r1 + r3
            r0.label = r1
            r3 = r0
        L_0x0015:
            java.lang.Object r2 = r3.result
            java.lang.Object r5 = com.fossil.yn7.d()
            int r0 = r3.label
            if (r0 == 0) goto L_0x0084
            if (r0 == r4) goto L_0x0058
            if (r0 != r7) goto L_0x0050
            java.lang.Object r0 = r3.L$3
            com.portfolio.platform.data.model.ServerError r0 = (com.portfolio.platform.data.model.ServerError) r0
            java.lang.Object r0 = r3.L$2
            com.fossil.kz4 r0 = (com.fossil.kz4) r0
            java.lang.Object r0 = r3.L$1
            com.fossil.mo5 r0 = (com.fossil.mo5) r0
            java.lang.Object r1 = r3.L$0
            com.fossil.jb6 r1 = (com.fossil.jb6) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r4 = r0
        L_0x0038:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.qo5 r0 = r0.getDianaPresetDao()
            long r0 = r0.l(r4)
            com.fossil.ao7.f(r0)
        L_0x0046:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0048:
            return r0
        L_0x0049:
            com.fossil.jb6$f r0 = new com.fossil.jb6$f
            r0.<init>(r8, r9)
            r3 = r0
            goto L_0x0015
        L_0x0050:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0058:
            java.lang.Object r0 = r3.L$1
            com.fossil.mo5 r0 = (com.fossil.mo5) r0
            java.lang.Object r1 = r3.L$0
            com.fossil.jb6 r1 = (com.fossil.jb6) r1
            com.fossil.el7.b(r2)
            r4 = r0
        L_0x0064:
            r0 = r2
            com.fossil.kz4 r0 = (com.fossil.kz4) r0
            com.portfolio.platform.data.model.ServerError r2 = r0.a()
            if (r2 == 0) goto L_0x0046
            r4.s(r7)
            com.fossil.bn5 r6 = com.fossil.bn5.j
            r3.L$0 = r1
            r3.L$1 = r4
            r3.L$2 = r0
            r3.L$3 = r2
            r3.label = r7
            java.lang.Object r1 = r6.v(r3)
            if (r1 != r5) goto L_0x0038
            r0 = r5
            goto L_0x0048
        L_0x0084:
            com.fossil.el7.b(r2)
            com.fossil.mo5 r0 = r8.g
            if (r0 == 0) goto L_0x0046
            java.util.List r1 = r0.a()
            if (r1 == 0) goto L_0x0094
            r1.clear()
        L_0x0094:
            java.util.List r1 = r0.a()
            if (r1 == 0) goto L_0x00a3
            java.util.ArrayList<com.fossil.oo5> r2 = r8.h
            boolean r1 = r1.addAll(r2)
            com.fossil.ao7.a(r1)
        L_0x00a3:
            com.fossil.uo5 r1 = r8.i
            com.portfolio.platform.data.source.FileRepository r2 = r8.j
            com.fossil.jo5 r2 = com.fossil.lo5.c(r0, r2)
            r3.L$0 = r8
            r3.L$1 = r0
            r3.label = r4
            java.lang.Object r2 = r1.q(r2, r3)
            if (r2 != r5) goto L_0x00b9
            r0 = r5
            goto L_0x0048
        L_0x00b9:
            r1 = r8
            r4 = r0
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb6.s(com.fossil.qn7):java.lang.Object");
    }
}
