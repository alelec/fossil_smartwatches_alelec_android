package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class ea0 extends Enum<ea0> {
    @DexIgnore
    public static /* final */ ea0 c;
    @DexIgnore
    public static /* final */ /* synthetic */ ea0[] d;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        ea0 ea0 = new ea0("FULL", 0, (byte) 0);
        c = ea0;
        d = new ea0[]{ea0, new ea0("HALF", 1, (byte) 1), new ea0("QUARTER", 2, (byte) 2), new ea0("EIGHTH", 3, (byte) 3), new ea0("SIXTEENTH", 4, (byte) 4)};
    }
    */

    @DexIgnore
    public ea0(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static ea0 valueOf(String str) {
        return (ea0) Enum.valueOf(ea0.class, str);
    }

    @DexIgnore
    public static ea0[] values() {
        return (ea0[]) d.clone();
    }
}
