package com.fossil;

import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.vk4;
import com.fossil.xk4;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wk4 {
    @DexIgnore
    public static /* final */ rk4 g; // = new a();
    @DexIgnore
    public static /* final */ Logger h; // = Logger.getLogger(wk4.class.getName());
    @DexIgnore
    public static /* final */ Map<Character, Character> i;
    @DexIgnore
    public static /* final */ Map<Character, Character> j;
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ Pattern l; // = Pattern.compile("[+\uff0b]+");
    @DexIgnore
    public static /* final */ Pattern m; // = Pattern.compile("[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e]+");
    @DexIgnore
    public static /* final */ Pattern n; // = Pattern.compile("(\\p{Nd})");
    @DexIgnore
    public static /* final */ Pattern o; // = Pattern.compile("[+\uff0b\\p{Nd}]");
    @DexIgnore
    public static /* final */ Pattern p; // = Pattern.compile("[\\\\/] *x");
    @DexIgnore
    public static /* final */ Pattern q; // = Pattern.compile("[[\\P{N}&&\\P{L}]&&[^#]]+$");
    @DexIgnore
    public static /* final */ Pattern r; // = Pattern.compile("(?:.*?[A-Za-z]){3}.*");
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ String t; // = d("x\uff58#\uff03~\uff5e".length() != 0 ? ",".concat("x\uff58#\uff03~\uff5e") : new String(","));
    @DexIgnore
    public static /* final */ Pattern u;
    @DexIgnore
    public static /* final */ Pattern v;
    @DexIgnore
    public static /* final */ Pattern w; // = Pattern.compile("(\\$\\d)");
    @DexIgnore
    public static /* final */ Pattern x; // = Pattern.compile("\\$CC");
    @DexIgnore
    public static wk4 y; // = null;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ tk4 f3962a;
    @DexIgnore
    public /* final */ Map<Integer, List<String>> b;
    @DexIgnore
    public /* final */ Set<String> c; // = new HashSet(35);
    @DexIgnore
    public /* final */ yk4 d; // = new yk4(100);
    @DexIgnore
    public /* final */ Set<String> e; // = new HashSet(320);
    @DexIgnore
    public /* final */ Set<Integer> f; // = new HashSet();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements rk4 {
        @DexIgnore
        @Override // com.fossil.rk4
        public InputStream a(String str) {
            return wk4.class.getResourceAsStream(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f3963a;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] c;

        /*
        static {
            int[] iArr = new int[d.values().length];
            c = iArr;
            try {
                iArr[d.PREMIUM_RATE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                c[d.TOLL_FREE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                c[d.MOBILE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                c[d.FIXED_LINE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                c[d.FIXED_LINE_OR_MOBILE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                c[d.SHARED_COST.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                c[d.VOIP.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                c[d.PERSONAL_NUMBER.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                c[d.PAGER.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                c[d.UAN.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                c[d.VOICEMAIL.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            int[] iArr2 = new int[c.values().length];
            b = iArr2;
            try {
                iArr2[c.E164.ordinal()] = 1;
            } catch (NoSuchFieldError e12) {
            }
            try {
                b[c.INTERNATIONAL.ordinal()] = 2;
            } catch (NoSuchFieldError e13) {
            }
            try {
                b[c.RFC3966.ordinal()] = 3;
            } catch (NoSuchFieldError e14) {
            }
            try {
                b[c.NATIONAL.ordinal()] = 4;
            } catch (NoSuchFieldError e15) {
            }
            int[] iArr3 = new int[xk4.a.values().length];
            f3963a = iArr3;
            try {
                iArr3[xk4.a.FROM_NUMBER_WITH_PLUS_SIGN.ordinal()] = 1;
            } catch (NoSuchFieldError e16) {
            }
            try {
                f3963a[xk4.a.FROM_NUMBER_WITH_IDD.ordinal()] = 2;
            } catch (NoSuchFieldError e17) {
            }
            try {
                f3963a[xk4.a.FROM_NUMBER_WITHOUT_PLUS_SIGN.ordinal()] = 3;
            } catch (NoSuchFieldError e18) {
            }
            try {
                f3963a[xk4.a.FROM_DEFAULT_COUNTRY.ordinal()] = 4;
            } catch (NoSuchFieldError e19) {
            }
        }
        */
    }

    @DexIgnore
    public enum c {
        E164,
        INTERNATIONAL,
        NATIONAL,
        RFC3966
    }

    @DexIgnore
    public enum d {
        FIXED_LINE,
        MOBILE,
        FIXED_LINE_OR_MOBILE,
        TOLL_FREE,
        PREMIUM_RATE,
        SHARED_COST,
        VOIP,
        PERSONAL_NUMBER,
        PAGER,
        UAN,
        VOICEMAIL,
        UNKNOWN
    }

    @DexIgnore
    public enum e {
        IS_POSSIBLE,
        INVALID_COUNTRY_CODE,
        TOO_SHORT,
        TOO_LONG
    }

    /*
    static {
        HashMap hashMap = new HashMap();
        hashMap.put(52, "1");
        hashMap.put(54, CrashDumperPlugin.OPTION_KILL_DEFAULT);
        Collections.unmodifiableMap(hashMap);
        HashSet hashSet = new HashSet();
        hashSet.add(52);
        hashSet.add(54);
        hashSet.add(55);
        Collections.unmodifiableSet(hashSet);
        HashMap hashMap2 = new HashMap();
        hashMap2.put('0', '0');
        hashMap2.put('1', '1');
        hashMap2.put('2', '2');
        hashMap2.put('3', '3');
        hashMap2.put('4', '4');
        hashMap2.put('5', '5');
        hashMap2.put('6', '6');
        hashMap2.put('7', '7');
        hashMap2.put('8', '8');
        hashMap2.put('9', '9');
        HashMap hashMap3 = new HashMap(40);
        hashMap3.put('A', '2');
        hashMap3.put('B', '2');
        hashMap3.put('C', '2');
        hashMap3.put('D', '3');
        hashMap3.put('E', '3');
        hashMap3.put('F', '3');
        hashMap3.put('G', '4');
        hashMap3.put('H', '4');
        hashMap3.put('I', '4');
        hashMap3.put('J', '5');
        hashMap3.put('K', '5');
        hashMap3.put('L', '5');
        hashMap3.put('M', '6');
        hashMap3.put('N', '6');
        hashMap3.put('O', '6');
        hashMap3.put('P', '7');
        hashMap3.put('Q', '7');
        hashMap3.put('R', '7');
        hashMap3.put('S', '7');
        hashMap3.put('T', '8');
        hashMap3.put('U', '8');
        hashMap3.put('V', '8');
        hashMap3.put('W', '9');
        hashMap3.put('X', '9');
        hashMap3.put('Y', '9');
        hashMap3.put('Z', '9');
        i = Collections.unmodifiableMap(hashMap3);
        HashMap hashMap4 = new HashMap(100);
        hashMap4.putAll(i);
        hashMap4.putAll(hashMap2);
        j = Collections.unmodifiableMap(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.putAll(hashMap2);
        hashMap5.put('+', '+');
        hashMap5.put('*', '*');
        Collections.unmodifiableMap(hashMap5);
        HashMap hashMap6 = new HashMap();
        for (Character ch : i.keySet()) {
            char charValue = ch.charValue();
            hashMap6.put(Character.valueOf(Character.toLowerCase(charValue)), Character.valueOf(charValue));
            hashMap6.put(Character.valueOf(charValue), Character.valueOf(charValue));
        }
        hashMap6.putAll(hashMap2);
        hashMap6.put('-', '-');
        hashMap6.put('\uff0d', '-');
        hashMap6.put('\u2010', '-');
        hashMap6.put('\u2011', '-');
        hashMap6.put('\u2012', '-');
        hashMap6.put('\u2013', '-');
        hashMap6.put('\u2014', '-');
        hashMap6.put('\u2015', '-');
        hashMap6.put('\u2212', '-');
        hashMap6.put('/', '/');
        hashMap6.put('\uff0f', '/');
        hashMap6.put(' ', ' ');
        hashMap6.put('\u3000', ' ');
        hashMap6.put('\u2060', ' ');
        hashMap6.put('.', '.');
        hashMap6.put('\uff0e', '.');
        Collections.unmodifiableMap(hashMap6);
        Pattern.compile("[\\d]+(?:[~\u2053\u223c\uff5e][\\d]+)?");
        String valueOf = String.valueOf(Arrays.toString(i.keySet().toArray()).replaceAll("[, \\[\\]]", ""));
        String valueOf2 = String.valueOf(Arrays.toString(i.keySet().toArray()).toLowerCase().replaceAll("[, \\[\\]]", ""));
        k = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
        String valueOf3 = String.valueOf(String.valueOf(k));
        StringBuilder sb = new StringBuilder("\\p{Nd}{2}|[+\uff0b]*+(?:[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*]*\\p{Nd}){3,}[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*".length() + 2 + valueOf3.length() + "\\p{Nd}".length());
        sb.append("\\p{Nd}{2}|[+\uff0b]*+(?:[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*]*\\p{Nd}){3,}[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*");
        sb.append(valueOf3);
        sb.append("\\p{Nd}");
        sb.append("]*");
        s = sb.toString();
        d("x\uff58#\uff03~\uff5e");
        String valueOf4 = String.valueOf(String.valueOf(t));
        StringBuilder sb2 = new StringBuilder(valueOf4.length() + 5);
        sb2.append("(?:");
        sb2.append(valueOf4);
        sb2.append(")$");
        u = Pattern.compile(sb2.toString(), 66);
        String valueOf5 = String.valueOf(String.valueOf(s));
        String valueOf6 = String.valueOf(String.valueOf(t));
        StringBuilder sb3 = new StringBuilder(valueOf5.length() + 5 + valueOf6.length());
        sb3.append(valueOf5);
        sb3.append("(?:");
        sb3.append(valueOf6);
        sb3.append(")?");
        v = Pattern.compile(sb3.toString(), 66);
        Pattern.compile("(\\D+)");
        Pattern.compile("\\$NP");
        Pattern.compile("\\$FG");
        Pattern.compile("\\(?\\$1\\)?");
    }
    */

    @DexIgnore
    public wk4(tk4 tk4, Map<Integer, List<String>> map) {
        this.f3962a = tk4;
        this.b = map;
        for (Map.Entry<Integer, List<String>> entry : map.entrySet()) {
            List<String> value = entry.getValue();
            if (value.size() != 1 || !"001".equals(value.get(0))) {
                this.e.addAll(value);
            } else {
                this.f.add(entry.getKey());
            }
        }
        if (this.e.remove("001")) {
            h.log(Level.WARNING, "invalid metadata (country calling code was mapped to the non-geo entity as well as specific region(s))");
        }
        this.c.addAll(map.get(1));
    }

    @DexIgnore
    public static String C(String str) {
        return r.matcher(str).matches() ? G(str, j, true) : F(str);
    }

    @DexIgnore
    public static void D(StringBuilder sb) {
        sb.replace(0, sb.length(), C(sb.toString()));
    }

    @DexIgnore
    public static StringBuilder E(String str, boolean z) {
        StringBuilder sb = new StringBuilder(str.length());
        char[] charArray = str.toCharArray();
        for (char c2 : charArray) {
            int digit = Character.digit(c2, 10);
            if (digit != -1) {
                sb.append(digit);
            } else if (z) {
                sb.append(c2);
            }
        }
        return sb;
    }

    @DexIgnore
    public static String F(String str) {
        return E(str, false).toString();
    }

    @DexIgnore
    public static String G(String str, Map<Character, Character> map, boolean z) {
        StringBuilder sb = new StringBuilder(str.length());
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            Character ch = map.get(Character.valueOf(Character.toUpperCase(charAt)));
            if (ch != null) {
                sb.append(ch);
            } else if (!z) {
                sb.append(charAt);
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public static void M(wk4 wk4) {
        synchronized (wk4.class) {
            try {
                y = wk4;
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    public static void N(String str, xk4 xk4) {
        if (str.length() > 1 && str.charAt(0) == '0') {
            xk4.setItalianLeadingZero(true);
            int i2 = 1;
            while (i2 < str.length() - 1 && str.charAt(i2) == '0') {
                i2++;
            }
            if (i2 != 1) {
                xk4.setNumberOfLeadingZeros(i2);
            }
        }
    }

    @DexIgnore
    public static String d(String str) {
        String valueOf = String.valueOf(String.valueOf(str));
        StringBuilder sb = new StringBuilder(";ext=(\\p{Nd}{1,7})|[ \u00a0\\t,]*(?:e?xt(?:ensi(?:o\u0301?|\u00f3))?n?|\uff45?\uff58\uff54\uff4e?|[".length() + 48 + valueOf.length() + "(\\p{Nd}{1,7})".length() + "\\p{Nd}".length());
        sb.append(";ext=(\\p{Nd}{1,7})|[ \u00a0\\t,]*(?:e?xt(?:ensi(?:o\u0301?|\u00f3))?n?|\uff45?\uff58\uff54\uff4e?|[");
        sb.append(valueOf);
        sb.append("]|int|anexo|\uff49\uff4e\uff54)");
        sb.append("[:\\.\uff0e]?[ \u00a0\\t,-]*");
        sb.append("(\\p{Nd}{1,7})");
        sb.append("#?|");
        sb.append("[- ]+(");
        sb.append("\\p{Nd}");
        sb.append("{1,5})#");
        return sb.toString();
    }

    @DexIgnore
    public static wk4 e(rk4 rk4) {
        if (rk4 != null) {
            return f(new uk4(rk4));
        }
        throw new IllegalArgumentException("metadataLoader could not be null.");
    }

    @DexIgnore
    public static wk4 f(tk4 tk4) {
        if (tk4 != null) {
            return new wk4(tk4, qk4.a());
        }
        throw new IllegalArgumentException("metadataSource could not be null.");
    }

    @DexIgnore
    public static String h(String str) {
        Matcher matcher = o.matcher(str);
        if (!matcher.find()) {
            return "";
        }
        String substring = str.substring(matcher.start());
        Matcher matcher2 = q.matcher(substring);
        if (matcher2.find()) {
            String substring2 = substring.substring(0, matcher2.start());
            Logger logger = h;
            Level level = Level.FINER;
            String valueOf = String.valueOf(substring2);
            logger.log(level, valueOf.length() != 0 ? "Stripped trailing characters: ".concat(valueOf) : new String("Stripped trailing characters: "));
            substring = substring2;
        }
        Matcher matcher3 = p.matcher(substring);
        return matcher3.find() ? substring.substring(0, matcher3.start()) : substring;
    }

    @DexIgnore
    public static wk4 n() {
        wk4 wk4;
        synchronized (wk4.class) {
            try {
                if (y == null) {
                    M(e(g));
                }
                wk4 = y;
            } catch (Throwable th) {
                throw th;
            }
        }
        return wk4;
    }

    @DexIgnore
    public static boolean w(String str) {
        if (str.length() < 2) {
            return false;
        }
        return v.matcher(str).matches();
    }

    @DexIgnore
    public xk4.a A(StringBuilder sb, String str) {
        if (sb.length() == 0) {
            return xk4.a.FROM_DEFAULT_COUNTRY;
        }
        Matcher matcher = l.matcher(sb);
        if (matcher.lookingAt()) {
            sb.delete(0, matcher.end());
            D(sb);
            return xk4.a.FROM_NUMBER_WITH_PLUS_SIGN;
        }
        Pattern a2 = this.d.a(str);
        D(sb);
        return K(a2, sb) ? xk4.a.FROM_NUMBER_WITH_IDD : xk4.a.FROM_DEFAULT_COUNTRY;
    }

    @DexIgnore
    public boolean B(StringBuilder sb, bl4 bl4, StringBuilder sb2) {
        int length = sb.length();
        String str = bl4.t;
        if (length == 0 || str.length() == 0) {
            return false;
        }
        Matcher matcher = this.d.a(str).matcher(sb);
        if (!matcher.lookingAt()) {
            return false;
        }
        Pattern a2 = this.d.a(bl4.f444a.f802a);
        boolean matches = a2.matcher(sb).matches();
        int groupCount = matcher.groupCount();
        String str2 = bl4.u;
        if (str2 != null && str2.length() != 0 && matcher.group(groupCount) != null) {
            StringBuilder sb3 = new StringBuilder(sb);
            sb3.replace(0, length, matcher.replaceFirst(str2));
            if (matches && !a2.matcher(sb3.toString()).matches()) {
                return false;
            }
            if (sb2 != null && groupCount > 1) {
                sb2.append(matcher.group(1));
            }
            sb.replace(0, sb.length(), sb3.toString());
            return true;
        } else if (matches && !a2.matcher(sb.substring(matcher.end())).matches()) {
            return false;
        } else {
            if (!(sb2 == null || groupCount <= 0 || matcher.group(groupCount) == null)) {
                sb2.append(matcher.group(1));
            }
            sb.delete(0, matcher.end());
            return true;
        }
    }

    @DexIgnore
    public xk4 H(String str, String str2) throws vk4 {
        xk4 xk4 = new xk4();
        I(str, str2, xk4);
        return xk4;
    }

    @DexIgnore
    public void I(String str, String str2, xk4 xk4) throws vk4 {
        J(str, str2, false, true, xk4);
    }

    @DexIgnore
    public final void J(String str, String str2, boolean z, boolean z2, xk4 xk4) throws vk4 {
        int y2;
        if (str == null) {
            throw new vk4(vk4.a.NOT_A_NUMBER, "The phone number supplied was null.");
        } else if (str.length() <= 250) {
            StringBuilder sb = new StringBuilder();
            a(str, sb);
            if (!w(sb.toString())) {
                throw new vk4(vk4.a.NOT_A_NUMBER, "The string supplied did not seem to be a phone number.");
            } else if (!z2 || b(sb.toString(), str2)) {
                if (z) {
                    xk4.setRawInput(str);
                }
                String z3 = z(sb);
                if (z3.length() > 0) {
                    xk4.setExtension(z3);
                }
                bl4 p2 = p(str2);
                StringBuilder sb2 = new StringBuilder();
                try {
                    y2 = y(sb.toString(), p2, sb2, z, xk4);
                } catch (vk4 e2) {
                    Matcher matcher = l.matcher(sb.toString());
                    if (e2.getErrorType() != vk4.a.INVALID_COUNTRY_CODE || !matcher.lookingAt()) {
                        throw new vk4(e2.getErrorType(), e2.getMessage());
                    }
                    y2 = y(sb.substring(matcher.end()), p2, sb2, z, xk4);
                    if (y2 == 0) {
                        throw new vk4(vk4.a.INVALID_COUNTRY_CODE, "Could not interpret numbers after plus-sign.");
                    }
                }
                if (y2 != 0) {
                    String s2 = s(y2);
                    if (!s2.equals(str2)) {
                        p2 = q(y2, s2);
                    }
                } else {
                    D(sb);
                    sb2.append((CharSequence) sb);
                    if (str2 != null) {
                        xk4.setCountryCode(p2.q);
                    } else if (z) {
                        xk4.clearCountryCodeSource();
                    }
                }
                if (sb2.length() >= 2) {
                    if (p2 != null) {
                        StringBuilder sb3 = new StringBuilder();
                        StringBuilder sb4 = new StringBuilder(sb2);
                        B(sb4, p2, sb3);
                        if (!u(p2, sb4.toString())) {
                            if (z) {
                                xk4.setPreferredDomesticCarrierCode(sb3.toString());
                            }
                            sb2 = sb4;
                        }
                    }
                    int length = sb2.length();
                    if (length < 2) {
                        throw new vk4(vk4.a.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
                    } else if (length <= 17) {
                        N(sb2.toString(), xk4);
                        xk4.setNationalNumber(Long.parseLong(sb2.toString()));
                    } else {
                        throw new vk4(vk4.a.TOO_LONG, "The string supplied is too long to be a phone number.");
                    }
                } else {
                    throw new vk4(vk4.a.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
                }
            } else {
                throw new vk4(vk4.a.INVALID_COUNTRY_CODE, "Missing or invalid default region.");
            }
        } else {
            throw new vk4(vk4.a.TOO_LONG, "The string supplied was too long to parse.");
        }
    }

    @DexIgnore
    public final boolean K(Pattern pattern, StringBuilder sb) {
        Matcher matcher = pattern.matcher(sb);
        if (!matcher.lookingAt()) {
            return false;
        }
        int end = matcher.end();
        Matcher matcher2 = n.matcher(sb.substring(end));
        if (matcher2.find() && F(matcher2.group(1)).equals("0")) {
            return false;
        }
        sb.delete(0, end);
        return true;
    }

    @DexIgnore
    public final void L(int i2, c cVar, StringBuilder sb) {
        int i3 = b.b[cVar.ordinal()];
        if (i3 == 1) {
            sb.insert(0, i2).insert(0, '+');
        } else if (i3 == 2) {
            sb.insert(0, " ").insert(0, i2).insert(0, '+');
        } else if (i3 == 3) {
            sb.insert(0, ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).insert(0, i2).insert(0, '+').insert(0, "tel:");
        }
    }

    @DexIgnore
    public final e O(Pattern pattern, String str) {
        Matcher matcher = pattern.matcher(str);
        return matcher.matches() ? e.IS_POSSIBLE : matcher.lookingAt() ? e.TOO_LONG : e.TOO_SHORT;
    }

    @DexIgnore
    public final void a(String str, StringBuilder sb) {
        int indexOf = str.indexOf(";phone-context=");
        if (indexOf > 0) {
            int i2 = indexOf + 15;
            if (str.charAt(i2) == '+') {
                int indexOf2 = str.indexOf(59, i2);
                if (indexOf2 > 0) {
                    sb.append(str.substring(i2, indexOf2));
                } else {
                    sb.append(str.substring(i2));
                }
            }
            int indexOf3 = str.indexOf("tel:");
            sb.append(str.substring(indexOf3 >= 0 ? indexOf3 + 4 : 0, indexOf));
        } else {
            sb.append(h(str));
        }
        int indexOf4 = sb.indexOf(";isub=");
        if (indexOf4 > 0) {
            sb.delete(indexOf4, sb.length());
        }
    }

    @DexIgnore
    public final boolean b(String str, String str2) {
        return v(str2) || !(str == null || str.length() == 0 || !l.matcher(str).lookingAt());
    }

    @DexIgnore
    public al4 c(al4[] al4Arr, String str) {
        for (al4 al4 : al4Arr) {
            String[] strArr = al4.c;
            int length = strArr.length;
            if ((length == 0 || this.d.a(strArr[length - 1]).matcher(str).lookingAt()) && this.d.a(al4.f286a).matcher(str).matches()) {
                return al4;
            }
        }
        return null;
    }

    @DexIgnore
    public int g(StringBuilder sb, StringBuilder sb2) {
        if (!(sb.length() == 0 || sb.charAt(0) == '0')) {
            int length = sb.length();
            int i2 = 1;
            while (i2 <= 3 && i2 <= length) {
                int parseInt = Integer.parseInt(sb.substring(0, i2));
                if (this.b.containsKey(Integer.valueOf(parseInt))) {
                    sb2.append(sb.substring(i2));
                    return parseInt;
                }
                i2++;
            }
        }
        return 0;
    }

    @DexIgnore
    public String i(xk4 xk4, c cVar) {
        if (xk4.getNationalNumber() == 0 && xk4.hasRawInput()) {
            String rawInput = xk4.getRawInput();
            if (rawInput.length() > 0) {
                return rawInput;
            }
        }
        StringBuilder sb = new StringBuilder(20);
        j(xk4, cVar, sb);
        return sb.toString();
    }

    @DexIgnore
    public void j(xk4 xk4, c cVar, StringBuilder sb) {
        sb.setLength(0);
        int countryCode = xk4.getCountryCode();
        String r2 = r(xk4);
        if (cVar == c.E164) {
            sb.append(r2);
            L(countryCode, c.E164, sb);
        } else if (!t(countryCode)) {
            sb.append(r2);
        } else {
            bl4 q2 = q(countryCode, s(countryCode));
            sb.append(k(r2, q2, cVar));
            x(xk4, q2, cVar, sb);
            L(countryCode, cVar, sb);
        }
    }

    @DexIgnore
    public final String k(String str, bl4 bl4, c cVar) {
        return l(str, bl4, cVar, null);
    }

    @DexIgnore
    public final String l(String str, bl4 bl4, c cVar, String str2) {
        al4[] al4Arr = bl4.w;
        if (al4Arr.length == 0 || cVar == c.NATIONAL) {
            al4Arr = bl4.v;
        }
        al4 c2 = c(al4Arr, str);
        return c2 == null ? str : m(str, c2, cVar, str2);
    }

    @DexIgnore
    public final String m(String str, al4 al4, c cVar, String str2) {
        String replaceAll;
        String str3 = al4.b;
        Matcher matcher = this.d.a(al4.f286a).matcher(str);
        if (cVar != c.NATIONAL || str2 == null || str2.length() <= 0 || al4.e.length() <= 0) {
            String str4 = al4.d;
            replaceAll = (cVar != c.NATIONAL || str4 == null || str4.length() <= 0) ? matcher.replaceAll(str3) : matcher.replaceAll(w.matcher(str3).replaceFirst(str4));
        } else {
            replaceAll = matcher.replaceAll(w.matcher(str3).replaceFirst(x.matcher(al4.e).replaceFirst(str2)));
        }
        if (cVar != c.RFC3966) {
            return replaceAll;
        }
        Matcher matcher2 = m.matcher(replaceAll);
        if (matcher2.lookingAt()) {
            replaceAll = matcher2.replaceFirst("");
        }
        return matcher2.reset(replaceAll).replaceAll(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
    }

    @DexIgnore
    public bl4 o(int i2) {
        if (!this.b.containsKey(Integer.valueOf(i2))) {
            return null;
        }
        return this.f3962a.a(i2);
    }

    @DexIgnore
    public bl4 p(String str) {
        if (!v(str)) {
            return null;
        }
        return this.f3962a.b(str);
    }

    @DexIgnore
    public final bl4 q(int i2, String str) {
        return "001".equals(str) ? o(i2) : p(str);
    }

    @DexIgnore
    public String r(xk4 xk4) {
        StringBuilder sb = new StringBuilder();
        if (xk4.isItalianLeadingZero()) {
            char[] cArr = new char[xk4.getNumberOfLeadingZeros()];
            Arrays.fill(cArr, '0');
            sb.append(new String(cArr));
        }
        sb.append(xk4.getNationalNumber());
        return sb.toString();
    }

    @DexIgnore
    public String s(int i2) {
        List<String> list = this.b.get(Integer.valueOf(i2));
        return list == null ? "ZZ" : list.get(0);
    }

    @DexIgnore
    public final boolean t(int i2) {
        return this.b.containsKey(Integer.valueOf(i2));
    }

    @DexIgnore
    public final boolean u(bl4 bl4, String str) {
        return O(this.d.a(bl4.f444a.b), str) == e.TOO_SHORT;
    }

    @DexIgnore
    public final boolean v(String str) {
        return str != null && this.e.contains(str);
    }

    @DexIgnore
    public final void x(xk4 xk4, bl4 bl4, c cVar, StringBuilder sb) {
        if (xk4.hasExtension() && xk4.getExtension().length() > 0) {
            if (cVar == c.RFC3966) {
                sb.append(";ext=");
                sb.append(xk4.getExtension());
            } else if (!bl4.s.equals("")) {
                sb.append(bl4.s);
                sb.append(xk4.getExtension());
            } else {
                sb.append(" ext. ");
                sb.append(xk4.getExtension());
            }
        }
    }

    @DexIgnore
    public int y(String str, bl4 bl4, StringBuilder sb, boolean z, xk4 xk4) throws vk4 {
        if (str.length() == 0) {
            return 0;
        }
        StringBuilder sb2 = new StringBuilder(str);
        xk4.a A = A(sb2, bl4 != null ? bl4.r : "NonMatch");
        if (z) {
            xk4.setCountryCodeSource(A);
        }
        if (A == xk4.a.FROM_DEFAULT_COUNTRY) {
            if (bl4 != null) {
                int i2 = bl4.q;
                String valueOf = String.valueOf(i2);
                String sb3 = sb2.toString();
                if (sb3.startsWith(valueOf)) {
                    StringBuilder sb4 = new StringBuilder(sb3.substring(valueOf.length()));
                    dl4 dl4 = bl4.f444a;
                    Pattern a2 = this.d.a(dl4.f802a);
                    B(sb4, bl4, null);
                    Pattern a3 = this.d.a(dl4.b);
                    if ((!a2.matcher(sb2).matches() && a2.matcher(sb4).matches()) || O(a3, sb2.toString()) == e.TOO_LONG) {
                        sb.append((CharSequence) sb4);
                        if (z) {
                            xk4.setCountryCodeSource(xk4.a.FROM_NUMBER_WITHOUT_PLUS_SIGN);
                        }
                        xk4.setCountryCode(i2);
                        return i2;
                    }
                }
            }
            xk4.setCountryCode(0);
            return 0;
        } else if (sb2.length() > 2) {
            int g2 = g(sb2, sb);
            if (g2 != 0) {
                xk4.setCountryCode(g2);
                return g2;
            }
            throw new vk4(vk4.a.INVALID_COUNTRY_CODE, "Country calling code supplied was not recognised.");
        } else {
            throw new vk4(vk4.a.TOO_SHORT_AFTER_IDD, "Phone number had an IDD, but after this was not long enough to be a viable phone number.");
        }
    }

    @DexIgnore
    public String z(StringBuilder sb) {
        Matcher matcher = u.matcher(sb);
        if (matcher.find() && w(sb.substring(0, matcher.start()))) {
            int groupCount = matcher.groupCount();
            for (int i2 = 1; i2 <= groupCount; i2++) {
                if (matcher.group(i2) != null) {
                    String group = matcher.group(i2);
                    sb.delete(matcher.start(), sb.length());
                    return group;
                }
            }
        }
        return "";
    }
}
