package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f4398a;
    @DexIgnore
    public /* final */ jw0<j0> b;
    @DexIgnore
    public /* final */ xw0 c;
    @DexIgnore
    public /* final */ xw0 d;
    @DexIgnore
    public /* final */ xw0 e;

    @DexIgnore
    public z(qw0 qw0) {
        this.f4398a = qw0;
        this.b = new v(this, qw0);
        this.c = new w(this, qw0);
        this.d = new x(this, qw0);
        this.e = new y(this, qw0);
    }
}
