package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pm1 extends ym1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ int g; // = hy1.a(gr7.f1349a);
    @DexIgnore
    public static /* final */ int h; // = hy1.a(gr7.f1349a);
    @DexIgnore
    public static /* final */ int i; // = hy1.a(gr7.f1349a);
    @DexIgnore
    public static /* final */ int j; // = hy1.a(gr7.f1349a);
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<pm1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final pm1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 12) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new pm1(hy1.n(order.getShort(1)), hy1.n(order.getShort(3)), hy1.n(order.getShort(5)), hy1.n(order.getShort(7)));
            }
            throw new IllegalArgumentException(e.b(e.e("Invalid data size: "), bArr.length, ", require: 12"));
        }

        @DexIgnore
        public pm1 b(Parcel parcel) {
            return new pm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public pm1 createFromParcel(Parcel parcel) {
            return new pm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public pm1[] newArray(int i) {
            return new pm1[i];
        }
    }

    @DexIgnore
    public pm1(int i2, int i3, int i4, int i5) throws IllegalArgumentException {
        super(zm1.CYCLING_CADENCE);
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.f = i5;
        d();
    }

    @DexIgnore
    public /* synthetic */ pm1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = parcel.readInt();
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        d();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(12).order(ByteOrder.LITTLE_ENDIAN).put((byte) 2).putShort((short) this.c).putShort((short) this.d).putShort((short) this.e).putShort((short) this.f).putShort(0).array();
        pq7.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            g80.k(jSONObject, jd0.B5, Integer.valueOf(this.c));
            g80.k(jSONObject, jd0.C5, Integer.valueOf(this.d));
            g80.k(jSONObject, jd0.D5, Integer.valueOf(this.e));
            g80.k(jSONObject, jd0.E5, Integer.valueOf(this.f));
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        boolean z = true;
        int i2 = g;
        int i3 = this.c;
        if (i3 >= 0 && i2 >= i3) {
            int i4 = h;
            int i5 = this.d;
            if (i5 >= 0 && i4 >= i5) {
                int i6 = i;
                int i7 = this.e;
                if (i7 >= 0 && i6 >= i7) {
                    int i8 = j;
                    int i9 = this.f;
                    if (i9 < 0 || i8 < i9) {
                        z = false;
                    }
                    if (!z) {
                        StringBuilder e2 = e.e("cog(");
                        e2.append(this.f);
                        e2.append(") is out of range ");
                        e2.append("[0, ");
                        throw new IllegalArgumentException(e.b(e2, j, "]."));
                    }
                    return;
                }
                StringBuilder e3 = e.e("chainring(");
                e3.append(this.e);
                e3.append(") is out of range ");
                e3.append("[0, ");
                throw new IllegalArgumentException(e.b(e3, i, "]."));
            }
            StringBuilder e4 = e.e("tire size(");
            e4.append(this.d);
            e4.append(") is out of range ");
            e4.append("[0, ");
            throw new IllegalArgumentException(e.b(e4, h, "]."));
        }
        StringBuilder e5 = e.e("diameter(");
        e5.append(this.c);
        e5.append(") is out of range ");
        e5.append("[0, ");
        throw new IllegalArgumentException(e.b(e5, g, "]."));
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(pm1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            pm1 pm1 = (pm1) obj;
            if (this.c != pm1.c) {
                return false;
            }
            if (this.d != pm1.d) {
                return false;
            }
            if (this.e != pm1.e) {
                return false;
            }
            return this.f == pm1.f;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.CyclingCadenceConfig");
    }

    @DexIgnore
    public final int getChainring() {
        return this.e;
    }

    @DexIgnore
    public final int getCog() {
        return this.f;
    }

    @DexIgnore
    public final int getDiameterInMillimeter() {
        return this.c;
    }

    @DexIgnore
    public final int getTireSizeInMillimeter() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public int hashCode() {
        return (((((this.c * 31) + this.d) * 31) + this.e) * 31) + this.f;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
        if (parcel != null) {
            parcel.writeInt(this.f);
        }
    }
}
