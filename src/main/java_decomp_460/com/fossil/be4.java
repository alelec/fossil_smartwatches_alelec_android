package com.fossil;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class be4 implements xd4<be4> {
    @DexIgnore
    public static /* final */ sd4<Object> e; // = yd4.b();
    @DexIgnore
    public static /* final */ ud4<String> f; // = zd4.b();
    @DexIgnore
    public static /* final */ ud4<Boolean> g; // = ae4.b();
    @DexIgnore
    public static /* final */ b h; // = new b(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<Class<?>, sd4<?>> f418a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, ud4<?>> b; // = new HashMap();
    @DexIgnore
    public sd4<Object> c; // = e;
    @DexIgnore
    public boolean d; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements pd4 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.pd4
        public void a(Object obj, Writer writer) throws IOException {
            ce4 ce4 = new ce4(writer, be4.this.f418a, be4.this.b, be4.this.c, be4.this.d);
            ce4.i(obj, false);
            ce4.r();
        }

        @DexIgnore
        @Override // com.fossil.pd4
        public String b(Object obj) {
            StringWriter stringWriter = new StringWriter();
            try {
                a(obj, stringWriter);
            } catch (IOException e) {
            }
            return stringWriter.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ud4<Date> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ DateFormat f420a;

        /*
        static {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            f420a = simpleDateFormat;
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        */

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(a aVar) {
            this();
        }

        @DexIgnore
        /* renamed from: b */
        public void a(Date date, vd4 vd4) throws IOException {
            vd4.d(f420a.format(date));
        }
    }

    @DexIgnore
    public be4() {
        m(String.class, f);
        m(Boolean.class, g);
        m(Date.class, h);
    }

    @DexIgnore
    public static /* synthetic */ void i(Object obj, td4 td4) throws IOException {
        throw new rd4("Couldn't find encoder for type " + obj.getClass().getCanonicalName());
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.xd4' to match base method */
    @Override // com.fossil.xd4
    public /* bridge */ /* synthetic */ be4 a(Class cls, sd4 sd4) {
        l(cls, sd4);
        return this;
    }

    @DexIgnore
    public pd4 f() {
        return new a();
    }

    @DexIgnore
    public be4 g(wd4 wd4) {
        wd4.a(this);
        return this;
    }

    @DexIgnore
    public be4 h(boolean z) {
        this.d = z;
        return this;
    }

    @DexIgnore
    public <T> be4 l(Class<T> cls, sd4<? super T> sd4) {
        this.f418a.put(cls, sd4);
        this.b.remove(cls);
        return this;
    }

    @DexIgnore
    public <T> be4 m(Class<T> cls, ud4<? super T> ud4) {
        this.b.put(cls, ud4);
        this.f418a.remove(cls);
        return this;
    }
}
