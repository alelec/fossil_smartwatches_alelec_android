package com.fossil;

import android.graphics.drawable.Drawable;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class o81 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends o81 {
        @DexIgnore
        public a(Drawable drawable) {
            super(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends o81 {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Drawable drawable, boolean z) {
            super(null);
            pq7.c(drawable, ResourceManager.DRAWABLE);
        }
    }

    @DexIgnore
    public o81() {
    }

    @DexIgnore
    public /* synthetic */ o81(kq7 kq7) {
        this();
    }
}
