package com.fossil;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r33 extends AbstractSet<Map.Entry<K, V>> {
    @DexIgnore
    public /* final */ /* synthetic */ g33 b;

    @DexIgnore
    public r33(g33 g33) {
        this.b = g33;
    }

    @DexIgnore
    public /* synthetic */ r33(g33 g33, j33 j33) {
        this(g33);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public /* synthetic */ boolean add(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (contains(entry)) {
            return false;
        }
        this.b.put((Comparable) entry.getKey(), entry.getValue());
        return true;
    }

    @DexIgnore
    public void clear() {
        this.b.clear();
    }

    @DexIgnore
    public boolean contains(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = this.b.get(entry.getKey());
        Object value = entry.getValue();
        return obj2 == value || (obj2 != null && obj2.equals(value));
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public Iterator<Map.Entry<K, V>> iterator() {
        return new o33(this.b, null);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (!contains(entry)) {
            return false;
        }
        this.b.remove(entry.getKey());
        return true;
    }

    @DexIgnore
    public int size() {
        return this.b.size();
    }
}
