package com.portfolio.platform.data;

import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SleepSession extends ServerError {
    @DexIgnore
    public static /* final */ String TAG; // = SleepSession.class.getSimpleName();
    @DexIgnore
    public /* final */ Date bookmarkTime;
    @DexIgnore
    public /* final */ String date;
    @DexIgnore
    public /* final */ String deviceSerialNumber;
    @DexIgnore
    public /* final */ Date editedEndTime;
    @DexIgnore
    public /* final */ int editedSleepMinutes;
    @DexIgnore
    public /* final */ int[] editedSleepStateDistInMinute;
    @DexIgnore
    public /* final */ Date editedStartTime;
    @DexIgnore
    public SleepSessionHeartRate heartRate;
    @DexIgnore
    public /* final */ String id;
    @DexIgnore
    public /* final */ Date realEndTime;
    @DexIgnore
    public /* final */ int realSleepMinutes;
    @DexIgnore
    public /* final */ int[] realSleepStateDistInMinute;
    @DexIgnore
    public /* final */ Date realStartTime;
    @DexIgnore
    public /* final */ double sleepQuality;
    @DexIgnore
    public /* final */ List<int[]> sleepStates;
    @DexIgnore
    public /* final */ String source;
    @DexIgnore
    public /* final */ Date syncTime;
    @DexIgnore
    public /* final */ int timezoneOffset;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends TypeToken<List<WrapperSleepStateChange>> {
        @DexIgnore
        public Anon1() {
        }
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v12, types: [java.util.List] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SleepSession(java.lang.String r7, com.portfolio.platform.data.model.room.sleep.MFSleepSession r8) {
        /*
            r6 = this;
            r6.<init>()
            int r0 = r8.getTimezoneOffset()
            r6.timezoneOffset = r0
            java.util.TimeZone r0 = com.fossil.zd5.a(r0)
            java.util.Calendar r1 = java.util.Calendar.getInstance()
            r1.setTimeZone(r0)
            com.fossil.ge5$a r2 = com.fossil.ge5.c
            java.lang.String r7 = r2.a(r7, r8)
            r6.id = r7
            java.util.Date r7 = new java.util.Date
            long r2 = r8.getDate()
            r7.<init>(r2)
            java.lang.String r7 = com.fossil.zd5.a(r7, r0)
            r6.date = r7
            com.fossil.db5[] r7 = com.fossil.db5.values()
            int r0 = r8.getSource()
            r7 = r7[r0]
            java.lang.String r7 = r7.getValue()
            java.lang.String r7 = r7.toLowerCase()
            r6.source = r7
            java.lang.String r7 = r8.getDeviceSerialNumber()
            r6.deviceSerialNumber = r7
            java.lang.Integer r7 = r8.getSyncTime()
            int r7 = r7.intValue()
            long r2 = (long) r7
            r4 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 * r4
            r1.setTimeInMillis(r2)
            java.util.Date r7 = r1.getTime()
            r6.syncTime = r7
            java.lang.Integer r7 = r8.getBookmarkTime()
            int r7 = r7.intValue()
            long r2 = (long) r7
            long r2 = r2 * r4
            r1.setTimeInMillis(r2)
            java.util.Date r7 = r1.getTime()
            r6.bookmarkTime = r7
            int r7 = r8.getRealStartTime()
            long r2 = (long) r7
            long r2 = r2 * r4
            r1.setTimeInMillis(r2)
            java.util.Date r7 = r1.getTime()
            r6.realStartTime = r7
            int r7 = r8.getRealEndTime()
            long r2 = (long) r7
            long r2 = r2 * r4
            r1.setTimeInMillis(r2)
            java.util.Date r7 = r1.getTime()
            r6.realEndTime = r7
            int r7 = r8.getRealSleepMinutes()
            r6.realSleepMinutes = r7
            com.portfolio.platform.data.model.room.sleep.SleepDistribution r7 = r8.getRealSleepStateDistInMinute()
            int[] r7 = r7.getArrayDistribution()
            r6.realSleepStateDistInMinute = r7
            int r7 = r8.getStartTime()
            long r2 = (long) r7
            long r2 = r2 * r4
            r1.setTimeInMillis(r2)
            java.util.Date r7 = r1.getTime()
            r6.editedStartTime = r7
            int r7 = r8.getEndTime()
            long r2 = (long) r7
            long r2 = r2 * r4
            r1.setTimeInMillis(r2)
            java.util.Date r7 = r1.getTime()
            r6.editedEndTime = r7
            int r7 = r8.getSleepMinutes()
            r6.editedSleepMinutes = r7
            com.portfolio.platform.data.model.room.sleep.SleepDistribution r7 = r8.getSleepState()
            int[] r7 = r7.getArrayDistribution()
            r6.editedSleepStateDistInMinute = r7
            double r0 = r8.getNormalizedSleepQuality()
            r6.sleepQuality = r0
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            com.google.gson.Gson r0 = new com.google.gson.Gson     // Catch:{ Exception -> 0x00f4 }
            r0.<init>()     // Catch:{ Exception -> 0x00f4 }
            java.lang.String r1 = r8.getSleepStates()     // Catch:{ Exception -> 0x00f4 }
            com.portfolio.platform.data.SleepSession$Anon1 r2 = new com.portfolio.platform.data.SleepSession$Anon1     // Catch:{ Exception -> 0x00f4 }
            r2.<init>()     // Catch:{ Exception -> 0x00f4 }
            java.lang.reflect.Type r2 = r2.getType()     // Catch:{ Exception -> 0x00f4 }
            java.lang.Object r0 = r0.a(r1, r2)     // Catch:{ Exception -> 0x00f4 }
            java.util.List r0 = (java.util.List) r0     // Catch:{ Exception -> 0x00f4 }
            r7 = r0
            goto L_0x0118
        L_0x00f4:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.SleepSession.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "exception="
            r3.append(r4)
            java.lang.String r4 = r0.getMessage()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.e(r2, r3)
            r0.printStackTrace()
        L_0x0118:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r6.sleepStates = r0
            java.util.Iterator r7 = r7.iterator()
        L_0x0123:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x0149
            java.lang.Object r0 = r7.next()
            com.portfolio.platform.service.syncmodel.WrapperSleepStateChange r0 = (com.portfolio.platform.service.syncmodel.WrapperSleepStateChange) r0
            if (r0 == 0) goto L_0x0123
            java.util.List<int[]> r1 = r6.sleepStates
            r2 = 2
            int[] r2 = new int[r2]
            r3 = 0
            int r4 = r0.getState()
            r2[r3] = r4
            r3 = 1
            long r4 = r0.getIndex()
            int r0 = (int) r4
            r2[r3] = r0
            r1.add(r2)
            goto L_0x0123
        L_0x0149:
            com.portfolio.platform.data.model.sleep.SleepSessionHeartRate r7 = r8.getHeartRate()
            r6.heartRate = r7
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.SleepSession.<init>(java.lang.String, com.portfolio.platform.data.model.room.sleep.MFSleepSession):void");
    }

    @DexIgnore
    public Date getRealEndTime() {
        return this.realEndTime;
    }
}
