package com.portfolio.platform.data;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fb7;
import com.fossil.rb7;
import com.fossil.tb7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.RingStyleRepository", f = "RingStyleRepository.kt", l = {19}, m = "downloadRingStyleList")
public final class RingStyleRepository$downloadRingStyleList$Anon1 extends rb7 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ RingStyleRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingStyleRepository$downloadRingStyleList$Anon1(RingStyleRepository ringStyleRepository, fb7 fb7) {
        super(fb7);
        this.this$0 = ringStyleRepository;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.downloadRingStyleList(null, false, this);
    }
}
