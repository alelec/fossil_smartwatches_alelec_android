package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.ee7;
import com.fossil.x97;
import com.portfolio.platform.data.ServerWorkoutSessionWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionWrapperKt {
    @DexIgnore
    public static final List<ServerWorkoutSessionWrapper> toServerWorkoutSessionWrapperList(List<WorkoutSessionWrapper> list) {
        ee7.b(list, "$this$toServerWorkoutSessionWrapperList");
        ArrayList arrayList = new ArrayList(x97.a(list, 10));
        for (Iterator<T> it = list.iterator(); it.hasNext(); it = it) {
            T next = it.next();
            arrayList.add(new ServerWorkoutSessionWrapper(next.getId(), next.getStartTime(), next.getEndTime(), next.getTimezoneOffsetInSecond(), next.getDuration(), next.getType(), next.getMode(), next.getPace(), next.getCadence(), next.getStateChanges(), next.getEncodedGpsData(), next.getStep(), next.getCalorie(), next.getDistance(), next.getHeartRate()));
        }
        return arrayList;
    }
}
