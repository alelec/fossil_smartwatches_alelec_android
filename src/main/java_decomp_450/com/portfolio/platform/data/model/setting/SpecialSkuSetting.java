package com.portfolio.platform.data.model.setting;

import com.fossil.be5;
import com.fossil.ee7;
import com.fossil.mh7;
import com.fossil.zd7;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SpecialSkuSetting {

    @DexIgnore
    public enum AngleSubeye {
        NONE(0, 0, 0),
        MOVEMBER(113, 158, Action.Selfie.TAKE_BURST);
        
        @DexIgnore
        public /* final */ int angleForApp;
        @DexIgnore
        public /* final */ int angleForCall;
        @DexIgnore
        public /* final */ int angleForSms;

        @DexIgnore
        public AngleSubeye(int i, int i2, int i3) {
            this.angleForApp = i;
            this.angleForCall = i2;
            this.angleForSms = i3;
        }

        @DexIgnore
        public final int getAngleForApp() {
            return this.angleForApp;
        }

        @DexIgnore
        public final int getAngleForCall() {
            return this.angleForCall;
        }

        @DexIgnore
        public final int getAngleForSms() {
            return this.angleForSms;
        }
    }

    @DexIgnore
    public enum SpecialSku {
        NONE("NONE", AngleSubeye.NONE, ""),
        MOVEMBER("FTW1175", AngleSubeye.MOVEMBER, "W0FA01");
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        public /* final */ AngleSubeye angleSubeye;
        @DexIgnore
        public /* final */ String prefixSerialNumber;
        @DexIgnore
        public /* final */ String sku;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final SpecialSku fromSerialNumber(String str) {
                ee7.b(str, "serial");
                SpecialSku[] values = SpecialSku.values();
                for (SpecialSku specialSku : values) {
                    if (mh7.b(be5.o.b(str), specialSku.getPrefixSerialNumber(), true)) {
                        return specialSku;
                    }
                }
                return SpecialSku.NONE;
            }

            @DexIgnore
            public final SpecialSku fromType(String str) {
                ee7.b(str, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
                SpecialSku[] values = SpecialSku.values();
                for (SpecialSku specialSku : values) {
                    if (mh7.b(specialSku.getSku(), str, true)) {
                        return specialSku;
                    }
                }
                return SpecialSku.NONE;
            }

            @DexIgnore
            public /* synthetic */ Companion(zd7 zd7) {
                this();
            }
        }

        @DexIgnore
        public SpecialSku(String str, AngleSubeye angleSubeye2, String str2) {
            this.sku = str;
            this.angleSubeye = angleSubeye2;
            this.prefixSerialNumber = str2;
        }

        @DexIgnore
        public final AngleSubeye getAngleSubeye() {
            return this.angleSubeye;
        }

        @DexIgnore
        public final String getPrefixSerialNumber() {
            return this.prefixSerialNumber;
        }

        @DexIgnore
        public final String getSku() {
            return this.sku;
        }
    }
}
