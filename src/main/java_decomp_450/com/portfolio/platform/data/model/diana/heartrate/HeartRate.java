package com.portfolio.platform.data.model.diana.heartrate;

import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.fossil.zd5;
import com.portfolio.platform.data.model.ServerError;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRate extends ServerError {
    @DexIgnore
    @te4(GoalTrackingSummary.COLUMN_AVERAGE)
    public /* final */ float mAverage;
    @DexIgnore
    @te4("createdAt")
    public /* final */ Date mCreatedAt;
    @DexIgnore
    @te4("date")
    public /* final */ Date mDate;
    @DexIgnore
    @te4(SampleRaw.COLUMN_END_TIME)
    public /* final */ String mEndTime;
    @DexIgnore
    @te4("id")
    public /* final */ String mId;
    @DexIgnore
    @te4("max")
    public /* final */ int mMax;
    @DexIgnore
    @te4("min")
    public /* final */ int mMin;
    @DexIgnore
    @te4("minuteCount")
    public /* final */ int mMinuteCount;
    @DexIgnore
    @te4("resting")
    public /* final */ Resting mResting;
    @DexIgnore
    @te4(SampleRaw.COLUMN_START_TIME)
    public /* final */ String mStartTime;
    @DexIgnore
    @te4("timezoneOffset")
    public /* final */ int mTimeZoneOffsetInSecond;
    @DexIgnore
    @te4("updatedAt")
    public /* final */ Date mUpdatedAt;

    @DexIgnore
    public HeartRate(String str, float f, Date date, Date date2, Date date3, String str2, String str3, int i, int i2, int i3, int i4, Resting resting) {
        ee7.b(str, "mId");
        ee7.b(date, "mDate");
        ee7.b(date2, "mCreatedAt");
        ee7.b(date3, "mUpdatedAt");
        ee7.b(str2, "mEndTime");
        ee7.b(str3, "mStartTime");
        this.mId = str;
        this.mAverage = f;
        this.mDate = date;
        this.mCreatedAt = date2;
        this.mUpdatedAt = date3;
        this.mEndTime = str2;
        this.mStartTime = str3;
        this.mTimeZoneOffsetInSecond = i;
        this.mMin = i2;
        this.mMax = i3;
        this.mMinuteCount = i4;
        this.mResting = resting;
    }

    @DexIgnore
    public final HeartRateSample toHeartRateSample() {
        Date date;
        Date date2;
        Date date3;
        try {
            Calendar instance = Calendar.getInstance();
            Date a = zd5.a(this.mTimeZoneOffsetInSecond, this.mStartTime);
            try {
                ee7.a((Object) instance, "calendar");
                instance.setTime(a);
                instance.set(13, 0);
                instance.set(14, 0);
                date = instance.getTime();
                date3 = zd5.a(this.mTimeZoneOffsetInSecond, this.mEndTime);
                try {
                    instance.setTime(date3);
                    instance.set(13, 0);
                    instance.set(14, 0);
                    date2 = instance.getTime();
                } catch (ParseException e) {
                    e = e;
                }
            } catch (ParseException e2) {
                e = e2;
                date3 = null;
                e.printStackTrace();
                date2 = date3;
                String str = this.mId;
                float f = this.mAverage;
                Date date4 = this.mDate;
                long time = this.mCreatedAt.getTime();
                long time2 = this.mUpdatedAt.getTime();
                DateTime a2 = zd5.a(date2, this.mTimeZoneOffsetInSecond);
                ee7.a((Object) a2, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                DateTime a3 = zd5.a(date, this.mTimeZoneOffsetInSecond);
                ee7.a((Object) a3, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                return new HeartRateSample(str, f, date4, time, time2, a2, a3, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
            }
        } catch (ParseException e3) {
            e = e3;
            date = null;
            date3 = null;
            e.printStackTrace();
            date2 = date3;
            String str2 = this.mId;
            float f2 = this.mAverage;
            Date date42 = this.mDate;
            long time3 = this.mCreatedAt.getTime();
            long time22 = this.mUpdatedAt.getTime();
            DateTime a22 = zd5.a(date2, this.mTimeZoneOffsetInSecond);
            ee7.a((Object) a22, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            DateTime a32 = zd5.a(date, this.mTimeZoneOffsetInSecond);
            ee7.a((Object) a32, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            return new HeartRateSample(str2, f2, date42, time3, time22, a22, a32, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
        }
        try {
            String str22 = this.mId;
            float f22 = this.mAverage;
            Date date422 = this.mDate;
            long time32 = this.mCreatedAt.getTime();
            long time222 = this.mUpdatedAt.getTime();
            DateTime a222 = zd5.a(date2, this.mTimeZoneOffsetInSecond);
            ee7.a((Object) a222, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            DateTime a322 = zd5.a(date, this.mTimeZoneOffsetInSecond);
            ee7.a((Object) a322, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            return new HeartRateSample(str22, f22, date422, time32, time222, a222, a322, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
        } catch (Exception e4) {
            e4.printStackTrace();
            return null;
        }
    }
}
