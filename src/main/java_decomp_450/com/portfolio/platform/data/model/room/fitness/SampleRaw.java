package com.portfolio.platform.data.model.room.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.VideoUploader;
import com.fossil.ee7;
import com.fossil.x87;
import com.fossil.zd5;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.ActivityIntensities;
import java.net.URI;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SampleRaw implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public int activeTime;
    @DexIgnore
    public double calories;
    @DexIgnore
    public double distance;
    @DexIgnore
    public Date endTime;
    @DexIgnore
    public String id;
    @DexIgnore
    public String intensity;
    @DexIgnore
    public ActivityIntensities intensityDistInSteps;
    @DexIgnore
    public String movementTypeValue;
    @DexIgnore
    public int pinType;
    @DexIgnore
    public String sourceId;
    @DexIgnore
    public String sourceTypeValue;
    @DexIgnore
    public Date startTime;
    @DexIgnore
    public double steps;
    @DexIgnore
    public String timeZoneID;
    @DexIgnore
    public int uaPinType;
    @DexIgnore
    public String uri;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SampleRaw> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SampleRaw createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new SampleRaw(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SampleRaw[] newArray(int i) {
            return new SampleRaw[i];
        }
    }

    @DexIgnore
    public SampleRaw(Date date, Date date2, String str, String str2, String str3, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, String str4) {
        ee7.b(date, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
        ee7.b(date2, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
        ee7.b(str, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
        ee7.b(str2, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
        ee7.b(activityIntensities, "intensityDistInSteps");
        this.startTime = date;
        this.endTime = date2;
        this.sourceId = str;
        this.sourceTypeValue = str2;
        this.movementTypeValue = str3;
        this.steps = d;
        this.calories = d2;
        this.distance = d3;
        this.activeTime = i;
        this.intensityDistInSteps = activityIntensities;
        this.timeZoneID = str4;
        TimeZone timeZone = TimeZone.getTimeZone(str4);
        if (timeZone == null || (!ee7.a((Object) timeZone.getID(), (Object) this.timeZoneID))) {
            TimeZone timeZone2 = TimeZone.getDefault();
            ee7.a((Object) timeZone2, "TimeZone.getDefault()");
            this.timeZoneID = timeZone2.getID();
        }
        String aSCIIString = generateSampleRawURI().toASCIIString();
        ee7.a((Object) aSCIIString, "generateSampleRawURI().toASCIIString()");
        this.uri = aSCIIString;
        this.id = aSCIIString;
        this.pinType = 0;
        this.uaPinType = 0;
    }

    @DexIgnore
    public static /* synthetic */ SampleRaw copy$default(SampleRaw sampleRaw, Date date, Date date2, String str, String str2, String str3, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, String str4, int i2, Object obj) {
        return sampleRaw.copy((i2 & 1) != 0 ? sampleRaw.startTime : date, (i2 & 2) != 0 ? sampleRaw.endTime : date2, (i2 & 4) != 0 ? sampleRaw.sourceId : str, (i2 & 8) != 0 ? sampleRaw.sourceTypeValue : str2, (i2 & 16) != 0 ? sampleRaw.movementTypeValue : str3, (i2 & 32) != 0 ? sampleRaw.steps : d, (i2 & 64) != 0 ? sampleRaw.calories : d2, (i2 & 128) != 0 ? sampleRaw.distance : d3, (i2 & 256) != 0 ? sampleRaw.activeTime : i, (i2 & 512) != 0 ? sampleRaw.intensityDistInSteps : activityIntensities, (i2 & 1024) != 0 ? sampleRaw.timeZoneID : str4);
    }

    @DexIgnore
    private final URI generateSampleRawURI() {
        StringBuilder sb = new StringBuilder();
        sb.append("sample:");
        String str = this.sourceTypeValue;
        if (str != null) {
            String lowerCase = str.toLowerCase();
            ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            sb.append(lowerCase);
            String sb2 = sb.toString();
            String y = zd5.y(getStartTimeLocal());
            URI create = URI.create("urn:fsl:fitness:" + sb2 + ':' + this.sourceId + ':' + y);
            ee7.a((Object) create, "URI.create(\"urn:fsl:fitn\u2026ty:$sourceId:$timestamp\")");
            return create;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    private final Date getEndTimeLocal() {
        Date b = zd5.b(this.endTime, TimeZone.getTimeZone(this.timeZoneID));
        ee7.a((Object) b, "DateHelper.getDateByTime\u2026.getTimeZone(timeZoneID))");
        return b;
    }

    @DexIgnore
    private final Date getStartTimeLocal() {
        Date b = zd5.b(this.startTime, TimeZone.getTimeZone(this.timeZoneID));
        ee7.a((Object) b, "DateHelper.getDateByTime\u2026.getTimeZone(timeZoneID))");
        return b;
    }

    @DexIgnore
    private final int getTimeZoneOffset() {
        return zd5.a(this.timeZoneID, this.startTime, false);
    }

    @DexIgnore
    public final Date component1() {
        return this.startTime;
    }

    @DexIgnore
    public final ActivityIntensities component10() {
        return this.intensityDistInSteps;
    }

    @DexIgnore
    public final String component11() {
        return this.timeZoneID;
    }

    @DexIgnore
    public final Date component2() {
        return this.endTime;
    }

    @DexIgnore
    public final String component3() {
        return this.sourceId;
    }

    @DexIgnore
    public final String component4() {
        return this.sourceTypeValue;
    }

    @DexIgnore
    public final String component5() {
        return this.movementTypeValue;
    }

    @DexIgnore
    public final double component6() {
        return this.steps;
    }

    @DexIgnore
    public final double component7() {
        return this.calories;
    }

    @DexIgnore
    public final double component8() {
        return this.distance;
    }

    @DexIgnore
    public final int component9() {
        return this.activeTime;
    }

    @DexIgnore
    public final SampleRaw copy(Date date, Date date2, String str, String str2, String str3, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, String str4) {
        ee7.b(date, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
        ee7.b(date2, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
        ee7.b(str, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
        ee7.b(str2, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
        ee7.b(activityIntensities, "intensityDistInSteps");
        return new SampleRaw(date, date2, str, str2, str3, d, d2, d3, i, activityIntensities, str4);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SampleRaw)) {
            return false;
        }
        SampleRaw sampleRaw = (SampleRaw) obj;
        return ee7.a(this.startTime, sampleRaw.startTime) && ee7.a(this.endTime, sampleRaw.endTime) && ee7.a(this.sourceId, sampleRaw.sourceId) && ee7.a(this.sourceTypeValue, sampleRaw.sourceTypeValue) && ee7.a(this.movementTypeValue, sampleRaw.movementTypeValue) && Double.compare(this.steps, sampleRaw.steps) == 0 && Double.compare(this.calories, sampleRaw.calories) == 0 && Double.compare(this.distance, sampleRaw.distance) == 0 && this.activeTime == sampleRaw.activeTime && ee7.a(this.intensityDistInSteps, sampleRaw.intensityDistInSteps) && ee7.a(this.timeZoneID, sampleRaw.timeZoneID);
    }

    @DexIgnore
    public final int getActiveTime() {
        return this.activeTime;
    }

    @DexIgnore
    public final double getAverageSteps() {
        return this.steps / ((double) TimeUnit.MILLISECONDS.toMinutes(this.endTime.getTime() - this.startTime.getTime()));
    }

    @DexIgnore
    public final double getCalories() {
        return this.calories;
    }

    @DexIgnore
    public final double getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final Date getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final ActivityIntensities getIntensityDistInSteps() {
        return this.intensityDistInSteps;
    }

    @DexIgnore
    public final String getMovementTypeValue() {
        return this.movementTypeValue;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSourceId() {
        return this.sourceId;
    }

    @DexIgnore
    public final String getSourceTypeValue() {
        return this.sourceTypeValue;
    }

    @DexIgnore
    public final Date getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final double getSteps() {
        return this.steps;
    }

    @DexIgnore
    public final String getTimeZoneID() {
        return this.timeZoneID;
    }

    @DexIgnore
    public final int getUaPinType() {
        return this.uaPinType;
    }

    @DexIgnore
    public final String getUri() {
        return this.uri;
    }

    @DexIgnore
    public int hashCode() {
        Date date = this.startTime;
        int i = 0;
        int hashCode = (date != null ? date.hashCode() : 0) * 31;
        Date date2 = this.endTime;
        int hashCode2 = (hashCode + (date2 != null ? date2.hashCode() : 0)) * 31;
        String str = this.sourceId;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.sourceTypeValue;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.movementTypeValue;
        int hashCode5 = (((((((((hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31) + Double.doubleToLongBits(this.steps)) * 31) + Double.doubleToLongBits(this.calories)) * 31) + Double.doubleToLongBits(this.distance)) * 31) + this.activeTime) * 31;
        ActivityIntensities activityIntensities = this.intensityDistInSteps;
        int hashCode6 = (hashCode5 + (activityIntensities != null ? activityIntensities.hashCode() : 0)) * 31;
        String str4 = this.timeZoneID;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode6 + i;
    }

    @DexIgnore
    public final void setActiveTime(int i) {
        this.activeTime = i;
    }

    @DexIgnore
    public final void setCalories(double d) {
        this.calories = d;
    }

    @DexIgnore
    public final void setDistance(double d) {
        this.distance = d;
    }

    @DexIgnore
    public final void setEndTime(Date date) {
        ee7.b(date, "<set-?>");
        this.endTime = date;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setIntensityDistInSteps(ActivityIntensities activityIntensities) {
        ee7.b(activityIntensities, "<set-?>");
        this.intensityDistInSteps = activityIntensities;
    }

    @DexIgnore
    public final void setMovementTypeValue(String str) {
        this.movementTypeValue = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSourceId(String str) {
        ee7.b(str, "<set-?>");
        this.sourceId = str;
    }

    @DexIgnore
    public final void setSourceTypeValue(String str) {
        ee7.b(str, "<set-?>");
        this.sourceTypeValue = str;
    }

    @DexIgnore
    public final void setStartTime(Date date) {
        ee7.b(date, "<set-?>");
        this.startTime = date;
    }

    @DexIgnore
    public final void setSteps(double d) {
        this.steps = d;
    }

    @DexIgnore
    public final void setTimeZoneID(String str) {
        this.timeZoneID = str;
    }

    @DexIgnore
    public final void setUaPinType(int i) {
        this.uaPinType = i;
    }

    @DexIgnore
    public final void setUri(String str) {
        ee7.b(str, "<set-?>");
        this.uri = str;
    }

    @DexIgnore
    public final ActivitySample toActivitySample() {
        FLogger.INSTANCE.getLocal().d("SampleRaw", "toActivitySample");
        DateTimeZone forTimeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone(this.timeZoneID));
        return new ActivitySample("", this.startTime, new DateTime(getStartTimeLocal().getTime(), forTimeZone), new DateTime(getEndTimeLocal(), forTimeZone), this.steps, this.calories, this.distance, this.activeTime, this.intensityDistInSteps, getTimeZoneOffset(), this.sourceId, 0, 0, 0);
    }

    @DexIgnore
    public String toString() {
        return "SampleRaw(startTime=" + this.startTime + ", endTime=" + this.endTime + ", sourceId=" + this.sourceId + ", sourceTypeValue=" + this.sourceTypeValue + ", movementTypeValue=" + this.movementTypeValue + ", steps=" + this.steps + ", calories=" + this.calories + ", distance=" + this.distance + ", activeTime=" + this.activeTime + ", intensityDistInSteps=" + this.intensityDistInSteps + ", timeZoneID=" + this.timeZoneID + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeSerializable(this.startTime);
        parcel.writeSerializable(this.endTime);
        parcel.writeString(this.sourceId);
        parcel.writeString(this.sourceTypeValue);
        parcel.writeString(this.movementTypeValue);
        parcel.writeDouble(this.steps);
        parcel.writeDouble(this.calories);
        parcel.writeDouble(this.distance);
        parcel.writeInt(this.activeTime);
        parcel.writeParcelable(this.intensityDistInSteps, i);
        parcel.writeString(this.id);
        parcel.writeString(this.uri);
        parcel.writeInt(this.pinType);
        parcel.writeInt(this.uaPinType);
        parcel.writeString(this.intensity);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ SampleRaw(java.util.Date r17, java.util.Date r18, java.lang.String r19, java.lang.String r20, java.lang.String r21, double r22, double r24, double r26, int r28, com.portfolio.platform.data.ActivityIntensities r29, java.lang.String r30, int r31, com.fossil.zd7 r32) {
        /*
            r16 = this;
            r0 = r31
            r0 = r0 & 1024(0x400, float:1.435E-42)
            if (r0 == 0) goto L_0x0015
            java.util.TimeZone r0 = java.util.TimeZone.getDefault()
            java.lang.String r1 = "TimeZone.getDefault()"
            com.fossil.ee7.a(r0, r1)
            java.lang.String r0 = r0.getID()
            r15 = r0
            goto L_0x0017
        L_0x0015:
            r15 = r30
        L_0x0017:
            r1 = r16
            r2 = r17
            r3 = r18
            r4 = r19
            r5 = r20
            r6 = r21
            r7 = r22
            r9 = r24
            r11 = r26
            r13 = r28
            r14 = r29
            r1.<init>(r2, r3, r4, r5, r6, r7, r9, r11, r13, r14, r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.room.fitness.SampleRaw.<init>(java.util.Date, java.util.Date, java.lang.String, java.lang.String, java.lang.String, double, double, double, int, com.portfolio.platform.data.ActivityIntensities, java.lang.String, int, com.fossil.zd7):void");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SampleRaw(android.os.Parcel r29) {
        /*
            r28 = this;
            r13 = r28
            r12 = r29
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r12, r0)
            java.io.Serializable r0 = r29.readSerializable()
            java.lang.String r1 = "null cannot be cast to non-null type java.util.Date"
            if (r0 == 0) goto L_0x00ae
            r2 = r0
            java.util.Date r2 = (java.util.Date) r2
            java.io.Serializable r0 = r29.readSerializable()
            if (r0 == 0) goto L_0x00a7
            r3 = r0
            java.util.Date r3 = (java.util.Date) r3
            java.lang.String r0 = r29.readString()
            java.lang.String r17 = ""
            if (r0 == 0) goto L_0x0027
            r4 = r0
            goto L_0x0029
        L_0x0027:
            r4 = r17
        L_0x0029:
            java.lang.String r0 = r29.readString()
            if (r0 == 0) goto L_0x0031
            r5 = r0
            goto L_0x0033
        L_0x0031:
            r5 = r17
        L_0x0033:
            java.lang.String r6 = r29.readString()
            double r7 = r29.readDouble()
            double r9 = r29.readDouble()
            double r18 = r29.readDouble()
            int r20 = r29.readInt()
            java.lang.Class<com.portfolio.platform.data.ActivityIntensities> r0 = com.portfolio.platform.data.ActivityIntensities.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r12.readParcelable(r0)
            com.portfolio.platform.data.ActivityIntensities r0 = (com.portfolio.platform.data.ActivityIntensities) r0
            if (r0 == 0) goto L_0x0058
            r21 = r0
            goto L_0x0065
        L_0x0058:
            com.portfolio.platform.data.ActivityIntensities r0 = new com.portfolio.platform.data.ActivityIntensities
            r22 = 0
            r24 = 0
            r26 = 0
            r21 = r0
            r21.<init>(r22, r24, r26)
        L_0x0065:
            r14 = 0
            r15 = 1024(0x400, float:1.435E-42)
            r16 = 0
            r0 = r28
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r7
            r8 = r9
            r10 = r18
            r12 = r20
            r13 = r21
            r0.<init>(r1, r2, r3, r4, r5, r6, r8, r10, r12, r13, r14, r15, r16)
            java.lang.String r0 = r29.readString()
            r2 = r28
            if (r0 == 0) goto L_0x0085
            goto L_0x0087
        L_0x0085:
            r0 = r17
        L_0x0087:
            r2.id = r0
            java.lang.String r0 = r29.readString()
            if (r0 == 0) goto L_0x0090
            goto L_0x0092
        L_0x0090:
            r0 = r17
        L_0x0092:
            r2.uri = r0
            int r0 = r29.readInt()
            r2.pinType = r0
            int r0 = r29.readInt()
            r2.uaPinType = r0
            java.lang.String r0 = r29.readString()
            r2.intensity = r0
            return
        L_0x00a7:
            r2 = r13
            com.fossil.x87 r0 = new com.fossil.x87
            r0.<init>(r1)
            throw r0
        L_0x00ae:
            r2 = r13
            com.fossil.x87 r0 = new com.fossil.x87
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.room.fitness.SampleRaw.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SampleRaw(Date date, Date date2, String str, String str2, String str3, String str4, double d, double d2, double d3, int i, ActivityIntensities activityIntensities) {
        this(date, date2, str2, str3, str4, d, d2, d3, i, activityIntensities, str);
        ee7.b(date, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        ee7.b(date2, "end");
        ee7.b(str, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
        ee7.b(str2, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
        ee7.b(str3, "sourceType");
        ee7.b(activityIntensities, "intensityDistInSteps");
        TimeZone timeZone = TimeZone.getTimeZone(str);
        if (timeZone == null || (!ee7.a((Object) timeZone.getID(), (Object) str))) {
            TimeZone timeZone2 = TimeZone.getDefault();
            ee7.a((Object) timeZone2, "TimeZone.getDefault()");
            this.timeZoneID = timeZone2.getID();
            return;
        }
        this.timeZoneID = timeZone.getID();
    }
}
