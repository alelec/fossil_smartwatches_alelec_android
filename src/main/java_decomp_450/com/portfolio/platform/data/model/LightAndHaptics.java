package com.portfolio.platform.data.model;

import com.portfolio.platform.data.NotificationType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LightAndHaptics implements Comparable<LightAndHaptics> {
    @DexIgnore
    public int hour;
    @DexIgnore
    public NotificationPriority mPriority;
    @DexIgnore
    public /* final */ String packageName;
    @DexIgnore
    public /* final */ String senderName;
    @DexIgnore
    public long timestamp; // = System.currentTimeMillis();
    @DexIgnore
    public NotificationType type;

    @DexIgnore
    public LightAndHaptics(String str, NotificationType notificationType, String str2, int i, NotificationPriority notificationPriority) {
        this.packageName = str;
        this.type = notificationType;
        this.senderName = str2;
        this.hour = i;
        this.mPriority = notificationPriority;
    }

    @DexIgnore
    public int getHour() {
        return this.hour;
    }

    @DexIgnore
    public NotificationType getNotificationType() {
        return this.type;
    }

    @DexIgnore
    public String getPackageName() {
        return this.packageName;
    }

    @DexIgnore
    public NotificationPriority getPriority() {
        return this.mPriority;
    }

    @DexIgnore
    public String getSenderName() {
        return this.senderName;
    }

    @DexIgnore
    public long getTimestamp() {
        return this.timestamp;
    }

    @DexIgnore
    public NotificationType getType() {
        return this.type;
    }

    @DexIgnore
    public void setHour(int i) {
        this.hour = i;
    }

    @DexIgnore
    public void setNotificationType(NotificationType notificationType) {
        this.type = notificationType;
    }

    @DexIgnore
    public void setTimestamp(long j) {
        this.timestamp = j;
    }

    @DexIgnore
    public int compareTo(LightAndHaptics lightAndHaptics) {
        return this.mPriority.compareTo((Enum) lightAndHaptics.mPriority);
    }
}
