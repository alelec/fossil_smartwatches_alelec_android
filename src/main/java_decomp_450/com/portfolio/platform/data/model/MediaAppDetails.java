package com.portfolio.platform.data.model;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.session.MediaSession;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.session.MediaSessionCompat;
import com.fossil.sw6;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MediaAppDetails implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<MediaAppDetails> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ String appName;
    @DexIgnore
    public /* final */ ComponentName componentName;
    @DexIgnore
    public /* final */ Bitmap icon;
    @DexIgnore
    public /* final */ String packageName;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<MediaAppDetails> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public MediaAppDetails createFromParcel(Parcel parcel) {
            return new MediaAppDetails(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public MediaAppDetails[] newArray(int i) {
            return new MediaAppDetails[i];
        }
    }

    @DexIgnore
    public MediaAppDetails(String str, String str2, Bitmap bitmap, MediaSessionCompat.Token token) {
        this.packageName = str;
        this.appName = str2;
        this.icon = bitmap;
        this.componentName = null;
    }

    @DexIgnore
    public static ServiceInfo findServiceInfo(String str, PackageManager packageManager) {
        for (ResolveInfo resolveInfo : packageManager.queryIntentServices(new Intent("android.media.browse.MediaBrowserService"), 64)) {
            if (resolveInfo.serviceInfo.packageName.equals(str)) {
                return resolveInfo.serviceInfo;
            }
        }
        return null;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        return new Gson().a(this);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.packageName);
        parcel.writeString(this.appName);
        parcel.writeParcelable(this.icon, i);
        parcel.writeParcelable(this.componentName, i);
    }

    @DexIgnore
    public MediaAppDetails(String str, String str2, Bitmap bitmap, MediaSession.Token token) {
        this(str, str2, bitmap, MediaSessionCompat.Token.a(token));
    }

    @DexIgnore
    public MediaAppDetails(ServiceInfo serviceInfo, PackageManager packageManager, Resources resources) {
        this.packageName = serviceInfo.packageName;
        this.appName = serviceInfo.loadLabel(packageManager).toString();
        this.icon = sw6.a(resources, serviceInfo.loadIcon(packageManager));
        this.componentName = new ComponentName(serviceInfo.packageName, serviceInfo.name);
    }

    @DexIgnore
    public MediaAppDetails(Parcel parcel) {
        this.packageName = parcel.readString();
        this.appName = parcel.readString();
        this.icon = (Bitmap) parcel.readParcelable(MediaAppDetails.class.getClassLoader());
        this.componentName = (ComponentName) parcel.readParcelable(MediaAppDetails.class.getClassLoader());
    }
}
