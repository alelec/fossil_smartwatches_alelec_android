package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.ee7;
import com.fossil.fitness.ActiveMinute;
import com.fossil.fitness.Calorie;
import com.fossil.fitness.Distance;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.GoalTracking;
import com.fossil.fitness.HeartRate;
import com.fossil.fitness.Resting;
import com.fossil.fitness.SleepSession;
import com.fossil.fitness.Step;
import com.fossil.fitness.WorkoutSession;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.service.syncmodel.WrapperTapEventSummary;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDataWrapper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public ActiveMinuteWrapper activeMinute;
    @DexIgnore
    public CalorieWrapper calorie;
    @DexIgnore
    public DistanceWrapper distance;
    @DexIgnore
    public DateTime endTime;
    @DexIgnore
    public List<WrapperTapEventSummary> goalTrackings;
    @DexIgnore
    public HeartRateWrapper heartRate;
    @DexIgnore
    public List<RestingWrapper> resting;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public List<SleepSessionWrapper> sleeps;
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public StepWrapper step;
    @DexIgnore
    public StressWrapper stress;
    @DexIgnore
    public DateTime syncTime;
    @DexIgnore
    public int timezoneOffsetInSecond;
    @DexIgnore
    public List<WorkoutSessionWrapper> workouts;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = FitnessDataWrapper.class.getSimpleName();
        ee7.a((Object) simpleName, "FitnessDataWrapper::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public FitnessDataWrapper(DateTime dateTime, DateTime dateTime2, DateTime dateTime3, int i, String str) {
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        ee7.b(dateTime3, "syncTime");
        ee7.b(str, "serialNumber");
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.syncTime = dateTime3;
        this.timezoneOffsetInSecond = i;
        this.serialNumber = str;
        this.resting = new ArrayList();
        this.sleeps = new ArrayList();
        this.workouts = new ArrayList();
        this.goalTrackings = new ArrayList();
    }

    @DexIgnore
    public static /* synthetic */ FitnessDataWrapper copy$default(FitnessDataWrapper fitnessDataWrapper, DateTime dateTime, DateTime dateTime2, DateTime dateTime3, int i, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            dateTime = fitnessDataWrapper.startTime;
        }
        if ((i2 & 2) != 0) {
            dateTime2 = fitnessDataWrapper.endTime;
        }
        if ((i2 & 4) != 0) {
            dateTime3 = fitnessDataWrapper.syncTime;
        }
        if ((i2 & 8) != 0) {
            i = fitnessDataWrapper.timezoneOffsetInSecond;
        }
        if ((i2 & 16) != 0) {
            str = fitnessDataWrapper.serialNumber;
        }
        return fitnessDataWrapper.copy(dateTime, dateTime2, dateTime3, i, str);
    }

    @DexIgnore
    public final DateTime component1() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.endTime;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.syncTime;
    }

    @DexIgnore
    public final int component4() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final String component5() {
        return this.serialNumber;
    }

    @DexIgnore
    public final FitnessDataWrapper copy(DateTime dateTime, DateTime dateTime2, DateTime dateTime3, int i, String str) {
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        ee7.b(dateTime3, "syncTime");
        ee7.b(str, "serialNumber");
        return new FitnessDataWrapper(dateTime, dateTime2, dateTime3, i, str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FitnessDataWrapper)) {
            return false;
        }
        FitnessDataWrapper fitnessDataWrapper = (FitnessDataWrapper) obj;
        return ee7.a(this.startTime, fitnessDataWrapper.startTime) && ee7.a(this.endTime, fitnessDataWrapper.endTime) && ee7.a(this.syncTime, fitnessDataWrapper.syncTime) && this.timezoneOffsetInSecond == fitnessDataWrapper.timezoneOffsetInSecond && ee7.a(this.serialNumber, fitnessDataWrapper.serialNumber);
    }

    @DexIgnore
    public final ActiveMinuteWrapper getActiveMinute() {
        ActiveMinuteWrapper activeMinuteWrapper = this.activeMinute;
        if (activeMinuteWrapper != null) {
            return activeMinuteWrapper;
        }
        ee7.d("activeMinute");
        throw null;
    }

    @DexIgnore
    public final CalorieWrapper getCalorie() {
        CalorieWrapper calorieWrapper = this.calorie;
        if (calorieWrapper != null) {
            return calorieWrapper;
        }
        ee7.d("calorie");
        throw null;
    }

    @DexIgnore
    public final DistanceWrapper getDistance() {
        DistanceWrapper distanceWrapper = this.distance;
        if (distanceWrapper != null) {
            return distanceWrapper;
        }
        ee7.d("distance");
        throw null;
    }

    @DexIgnore
    public final long getEndLongTime() {
        return this.endTime.getMillis();
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final DateTime getEndTimeTZ() {
        return new DateTime(this.endTime.getMillis(), DateTimeZone.forOffsetMillis(this.timezoneOffsetInSecond * 1000));
    }

    @DexIgnore
    public final List<WrapperTapEventSummary> getGoalTrackings() {
        return this.goalTrackings;
    }

    @DexIgnore
    public final HeartRateWrapper getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final List<RestingWrapper> getResting() {
        return this.resting;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final List<SleepSessionWrapper> getSleeps() {
        return this.sleeps;
    }

    @DexIgnore
    public final long getStartLongTime() {
        return this.startTime.getMillis();
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime getStartTimeTZ() {
        return new DateTime(this.startTime.getMillis(), DateTimeZone.forOffsetMillis(this.timezoneOffsetInSecond * 1000));
    }

    @DexIgnore
    public final StepWrapper getStep() {
        StepWrapper stepWrapper = this.step;
        if (stepWrapper != null) {
            return stepWrapper;
        }
        ee7.d("step");
        throw null;
    }

    @DexIgnore
    public final StressWrapper getStress() {
        return this.stress;
    }

    @DexIgnore
    public final DateTime getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final List<WorkoutSessionWrapper> getWorkouts() {
        return this.workouts;
    }

    @DexIgnore
    public int hashCode() {
        DateTime dateTime = this.startTime;
        int i = 0;
        int hashCode = (dateTime != null ? dateTime.hashCode() : 0) * 31;
        DateTime dateTime2 = this.endTime;
        int hashCode2 = (hashCode + (dateTime2 != null ? dateTime2.hashCode() : 0)) * 31;
        DateTime dateTime3 = this.syncTime;
        int hashCode3 = (((hashCode2 + (dateTime3 != null ? dateTime3.hashCode() : 0)) * 31) + this.timezoneOffsetInSecond) * 31;
        String str = this.serialNumber;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public final void setActiveMinute(ActiveMinuteWrapper activeMinuteWrapper) {
        ee7.b(activeMinuteWrapper, "<set-?>");
        this.activeMinute = activeMinuteWrapper;
    }

    @DexIgnore
    public final void setCalorie(CalorieWrapper calorieWrapper) {
        ee7.b(calorieWrapper, "<set-?>");
        this.calorie = calorieWrapper;
    }

    @DexIgnore
    public final void setDistance(DistanceWrapper distanceWrapper) {
        ee7.b(distanceWrapper, "<set-?>");
        this.distance = distanceWrapper;
    }

    @DexIgnore
    public final void setEndTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.endTime = dateTime;
    }

    @DexIgnore
    public final void setGoalTrackings(List<WrapperTapEventSummary> list) {
        ee7.b(list, "<set-?>");
        this.goalTrackings = list;
    }

    @DexIgnore
    public final void setHeartRate(HeartRateWrapper heartRateWrapper) {
        this.heartRate = heartRateWrapper;
    }

    @DexIgnore
    public final void setResting(List<RestingWrapper> list) {
        ee7.b(list, "<set-?>");
        this.resting = list;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        ee7.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setSleeps(List<SleepSessionWrapper> list) {
        ee7.b(list, "<set-?>");
        this.sleeps = list;
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setStep(StepWrapper stepWrapper) {
        ee7.b(stepWrapper, "<set-?>");
        this.step = stepWrapper;
    }

    @DexIgnore
    public final void setStress(StressWrapper stressWrapper) {
        this.stress = stressWrapper;
    }

    @DexIgnore
    public final void setSyncTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.syncTime = dateTime;
    }

    @DexIgnore
    public final void setTimezoneOffsetInSecond(int i) {
        this.timezoneOffsetInSecond = i;
    }

    @DexIgnore
    public final void setWorkouts(List<WorkoutSessionWrapper> list) {
        ee7.b(list, "<set-?>");
        this.workouts = list;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("steps ");
        StepWrapper stepWrapper = this.step;
        if (stepWrapper != null) {
            sb.append(stepWrapper);
            sb.append(" \n activeMinute ");
            ActiveMinuteWrapper activeMinuteWrapper = this.activeMinute;
            if (activeMinuteWrapper != null) {
                sb.append(activeMinuteWrapper);
                sb.append(" \n calorie ");
                CalorieWrapper calorieWrapper = this.calorie;
                if (calorieWrapper != null) {
                    sb.append(calorieWrapper);
                    sb.append(" \n distance ");
                    DistanceWrapper distanceWrapper = this.distance;
                    if (distanceWrapper != null) {
                        sb.append(distanceWrapper);
                        sb.append(" \n sleeps ");
                        sb.append(this.sleeps);
                        sb.append(" \n heartrate ");
                        sb.append(this.heartRate);
                        return sb.toString();
                    }
                    ee7.d("distance");
                    throw null;
                }
                ee7.d("calorie");
                throw null;
            }
            ee7.d("activeMinute");
            throw null;
        }
        ee7.d("step");
        throw null;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public FitnessDataWrapper(FitnessData fitnessData, String str, DateTime dateTime) {
        this(new DateTime(((long) fitnessData.getStartTime()) * 1000, DateTimeZone.forOffsetMillis(fitnessData.getTimezoneOffsetInSecond() * 1000)), new DateTime(((long) fitnessData.getEndTime()) * 1000, DateTimeZone.forOffsetMillis(fitnessData.getTimezoneOffsetInSecond() * 1000)), dateTime, fitnessData.getTimezoneOffsetInSecond(), str);
        ee7.b(fitnessData, "fitnessData");
        ee7.b(str, "serialNumber");
        ee7.b(dateTime, "syncTime");
        Step step2 = fitnessData.getStep();
        ee7.a((Object) step2, "fitnessData.step");
        this.step = new StepWrapper(step2);
        ActiveMinute activeMinute2 = fitnessData.getActiveMinute();
        if (activeMinute2 != null) {
            ee7.a((Object) activeMinute2, "localActiveMinute");
            this.activeMinute = new ActiveMinuteWrapper(activeMinute2);
        }
        Calorie calorie2 = fitnessData.getCalorie();
        ee7.a((Object) calorie2, "fitnessData.calorie");
        this.calorie = new CalorieWrapper(calorie2);
        Distance distance2 = fitnessData.getDistance();
        ee7.a((Object) distance2, "fitnessData.distance");
        int resolutionInSecond = distance2.getResolutionInSecond();
        Distance distance3 = fitnessData.getDistance();
        ee7.a((Object) distance3, "fitnessData.distance");
        ArrayList<Double> values = distance3.getValues();
        ee7.a((Object) values, "fitnessData.distance.values");
        Distance distance4 = fitnessData.getDistance();
        ee7.a((Object) distance4, "fitnessData.distance");
        this.distance = new DistanceWrapper(resolutionInSecond, values, distance4.getTotal());
        ArrayList<Resting> resting2 = fitnessData.getResting();
        ee7.a((Object) resting2, "fitnessData.resting");
        for (T t : resting2) {
            List<RestingWrapper> list = this.resting;
            ee7.a((Object) t, "it");
            DateTime withZone = new DateTime(((long) t.getTimestamp()) * 1000).withZone(DateTimeZone.forOffsetHours(t.getTimezoneOffsetInSecond() / 3600));
            ee7.a((Object) withZone, "DateTime(it.timestamp * \u2026neOffsetInSecond / 3600))");
            list.add(new RestingWrapper(withZone, t.getTimezoneOffsetInSecond(), t.getValue()));
        }
        HeartRate heartrate = fitnessData.getHeartrate();
        if (heartrate != null) {
            ee7.a((Object) heartrate, "heartRateData");
            this.heartRate = new HeartRateWrapper(heartrate);
        }
        ArrayList<SleepSession> sleeps2 = fitnessData.getSleeps();
        if (sleeps2 != null) {
            for (T t2 : sleeps2) {
                List<SleepSessionWrapper> list2 = this.sleeps;
                ee7.a((Object) t2, "it");
                list2.add(new SleepSessionWrapper(t2));
            }
        }
        ArrayList<WorkoutSession> workouts2 = fitnessData.getWorkouts();
        if (workouts2 != null) {
            for (T t3 : workouts2) {
                List<WorkoutSessionWrapper> list3 = this.workouts;
                ee7.a((Object) t3, "it");
                list3.add(new WorkoutSessionWrapper(t3));
            }
        } else {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.e(str2, "getWorkoutSessions(), list of workout nullObjectObject, fitnessData startTime: " + fitnessData.getStartTime());
        }
        ArrayList<GoalTracking> goals = fitnessData.getGoals();
        ee7.a((Object) goals, "fitnessData.goals");
        for (T t4 : goals) {
            List<WrapperTapEventSummary> list4 = this.goalTrackings;
            ee7.a((Object) t4, "it");
            list4.add(new WrapperTapEventSummary(t4.getTimestamp(), t4.getTimezoneOffsetInSecond(), t4.getGoalId()));
        }
    }
}
