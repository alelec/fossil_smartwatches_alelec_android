package com.portfolio.platform.data.model;

import com.fossil.ee7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Style {
    @DexIgnore
    public String key;
    @DexIgnore
    public String type;
    @DexIgnore
    public String value;

    @DexIgnore
    public Style(String str, String str2, String str3) {
        ee7.b(str, "key");
        ee7.b(str2, "type");
        ee7.b(str3, "value");
        this.key = str;
        this.type = str2;
        this.value = str3;
    }

    @DexIgnore
    public static /* synthetic */ Style copy$default(Style style, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = style.key;
        }
        if ((i & 2) != 0) {
            str2 = style.type;
        }
        if ((i & 4) != 0) {
            str3 = style.value;
        }
        return style.copy(str, str2, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.key;
    }

    @DexIgnore
    public final String component2() {
        return this.type;
    }

    @DexIgnore
    public final String component3() {
        return this.value;
    }

    @DexIgnore
    public final Style copy(String str, String str2, String str3) {
        ee7.b(str, "key");
        ee7.b(str2, "type");
        ee7.b(str3, "value");
        return new Style(str, str2, str3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Style)) {
            return false;
        }
        Style style = (Style) obj;
        return ee7.a(this.key, style.key) && ee7.a(this.type, style.type) && ee7.a(this.value, style.value);
    }

    @DexIgnore
    public final String getKey() {
        return this.key;
    }

    @DexIgnore
    public final String getType() {
        return this.type;
    }

    @DexIgnore
    public final String getValue() {
        return this.value;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.key;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.type;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.value;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final void setKey(String str) {
        ee7.b(str, "<set-?>");
        this.key = str;
    }

    @DexIgnore
    public final void setType(String str) {
        ee7.b(str, "<set-?>");
        this.type = str;
    }

    @DexIgnore
    public final void setValue(String str) {
        ee7.b(str, "<set-?>");
        this.value = str;
    }

    @DexIgnore
    public String toString() {
        return "Style(key=" + this.key + ", type=" + this.type + ", value=" + this.value + ")";
    }
}
