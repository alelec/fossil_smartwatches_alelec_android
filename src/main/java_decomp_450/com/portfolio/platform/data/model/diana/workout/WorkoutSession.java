package com.portfolio.platform.data.model.diana.workout;

import com.fossil.ac5;
import com.fossil.c;
import com.fossil.ee7;
import com.fossil.nd5;
import com.fossil.ub5;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.fossil.xb5;
import com.sina.weibo.sdk.api.ImageObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSession {
    @DexIgnore
    public /* final */ WorkoutCadence cadence;
    @DexIgnore
    public /* final */ WorkoutCalorie calorie;
    @DexIgnore
    public /* final */ long createdAt;
    @DexIgnore
    public /* final */ Date date;
    @DexIgnore
    public /* final */ String deviceSerialNumber;
    @DexIgnore
    public /* final */ WorkoutDistance distance;
    @DexIgnore
    public /* final */ int duration;
    @DexIgnore
    public DateTime editedEndTime;
    @DexIgnore
    public ub5 editedMode;
    @DexIgnore
    public DateTime editedStartTime;
    @DexIgnore
    public ac5 editedType;
    @DexIgnore
    public /* final */ DateTime endTime;
    @DexIgnore
    public /* final */ String gpsDataPoints;
    @DexIgnore
    public /* final */ WorkoutHeartRate heartRate;
    @DexIgnore
    public /* final */ String id;
    @DexIgnore
    public /* final */ ub5 mode;
    @DexIgnore
    public /* final */ WorkoutPace pace;
    @DexIgnore
    @nd5
    public String screenShotUri;
    @DexIgnore
    public /* final */ xb5 sourceType;
    @DexIgnore
    public WorkoutSpeed speed;
    @DexIgnore
    public /* final */ DateTime startTime;
    @DexIgnore
    public List<WorkoutStateChange> states;
    @DexIgnore
    public /* final */ WorkoutStep step;
    @DexIgnore
    public /* final */ int timezoneOffsetInSecond;
    @DexIgnore
    public /* final */ long updatedAt;
    @DexIgnore
    public /* final */ List<WorkoutGpsPoint> workoutGpsPoints;
    @DexIgnore
    public /* final */ ac5 workoutType;

    @DexIgnore
    public WorkoutSession(String str, Date date2, DateTime dateTime, DateTime dateTime2, String str2, WorkoutStep workoutStep, WorkoutCalorie workoutCalorie, WorkoutDistance workoutDistance, WorkoutHeartRate workoutHeartRate, xb5 xb5, ac5 ac5, int i, int i2, long j, long j2, List<WorkoutGpsPoint> list, String str3, ub5 ub5, WorkoutPace workoutPace, WorkoutCadence workoutCadence, DateTime dateTime3, DateTime dateTime4, ac5 ac52, ub5 ub52) {
        ee7.b(str, "id");
        ee7.b(date2, "date");
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        ee7.b(dateTime3, MFSleepSession.COLUMN_EDITED_START_TIME);
        ee7.b(dateTime4, MFSleepSession.COLUMN_EDITED_END_TIME);
        this.id = str;
        this.date = date2;
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.deviceSerialNumber = str2;
        this.step = workoutStep;
        this.calorie = workoutCalorie;
        this.distance = workoutDistance;
        this.heartRate = workoutHeartRate;
        this.sourceType = xb5;
        this.workoutType = ac5;
        this.timezoneOffsetInSecond = i;
        this.duration = i2;
        this.createdAt = j;
        this.updatedAt = j2;
        this.workoutGpsPoints = list;
        this.gpsDataPoints = str3;
        this.mode = ub5;
        this.pace = workoutPace;
        this.cadence = workoutCadence;
        this.editedStartTime = dateTime3;
        this.editedEndTime = dateTime4;
        this.editedType = ac52;
        this.editedMode = ub52;
        this.speed = workoutDistance != null ? workoutDistance.calculateSpeed() : null;
        this.states = new ArrayList();
    }

    @DexIgnore
    public static /* synthetic */ WorkoutSession copy$default(WorkoutSession workoutSession, String str, Date date2, DateTime dateTime, DateTime dateTime2, String str2, WorkoutStep workoutStep, WorkoutCalorie workoutCalorie, WorkoutDistance workoutDistance, WorkoutHeartRate workoutHeartRate, xb5 xb5, ac5 ac5, int i, int i2, long j, long j2, List list, String str3, ub5 ub5, WorkoutPace workoutPace, WorkoutCadence workoutCadence, DateTime dateTime3, DateTime dateTime4, ac5 ac52, ub5 ub52, int i3, Object obj) {
        return workoutSession.copy((i3 & 1) != 0 ? workoutSession.id : str, (i3 & 2) != 0 ? workoutSession.date : date2, (i3 & 4) != 0 ? workoutSession.startTime : dateTime, (i3 & 8) != 0 ? workoutSession.endTime : dateTime2, (i3 & 16) != 0 ? workoutSession.deviceSerialNumber : str2, (i3 & 32) != 0 ? workoutSession.step : workoutStep, (i3 & 64) != 0 ? workoutSession.calorie : workoutCalorie, (i3 & 128) != 0 ? workoutSession.distance : workoutDistance, (i3 & 256) != 0 ? workoutSession.heartRate : workoutHeartRate, (i3 & 512) != 0 ? workoutSession.sourceType : xb5, (i3 & 1024) != 0 ? workoutSession.workoutType : ac5, (i3 & 2048) != 0 ? workoutSession.timezoneOffsetInSecond : i, (i3 & 4096) != 0 ? workoutSession.duration : i2, (i3 & 8192) != 0 ? workoutSession.createdAt : j, (i3 & 16384) != 0 ? workoutSession.updatedAt : j2, (i3 & 32768) != 0 ? workoutSession.workoutGpsPoints : list, (65536 & i3) != 0 ? workoutSession.gpsDataPoints : str3, (i3 & 131072) != 0 ? workoutSession.mode : ub5, (i3 & 262144) != 0 ? workoutSession.pace : workoutPace, (i3 & 524288) != 0 ? workoutSession.cadence : workoutCadence, (i3 & 1048576) != 0 ? workoutSession.editedStartTime : dateTime3, (i3 & ImageObject.DATA_SIZE) != 0 ? workoutSession.editedEndTime : dateTime4, (i3 & 4194304) != 0 ? workoutSession.editedType : ac52, (i3 & 8388608) != 0 ? workoutSession.editedMode : ub52);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final xb5 component10() {
        return this.sourceType;
    }

    @DexIgnore
    public final ac5 component11() {
        return this.workoutType;
    }

    @DexIgnore
    public final int component12() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int component13() {
        return this.duration;
    }

    @DexIgnore
    public final long component14() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component15() {
        return this.updatedAt;
    }

    @DexIgnore
    public final List<WorkoutGpsPoint> component16() {
        return this.workoutGpsPoints;
    }

    @DexIgnore
    public final String component17() {
        return this.gpsDataPoints;
    }

    @DexIgnore
    public final ub5 component18() {
        return this.mode;
    }

    @DexIgnore
    public final WorkoutPace component19() {
        return this.pace;
    }

    @DexIgnore
    public final Date component2() {
        return this.date;
    }

    @DexIgnore
    public final WorkoutCadence component20() {
        return this.cadence;
    }

    @DexIgnore
    public final DateTime component21() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final DateTime component22() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final ac5 component23() {
        return this.editedType;
    }

    @DexIgnore
    public final ub5 component24() {
        return this.editedMode;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component4() {
        return this.endTime;
    }

    @DexIgnore
    public final String component5() {
        return this.deviceSerialNumber;
    }

    @DexIgnore
    public final WorkoutStep component6() {
        return this.step;
    }

    @DexIgnore
    public final WorkoutCalorie component7() {
        return this.calorie;
    }

    @DexIgnore
    public final WorkoutDistance component8() {
        return this.distance;
    }

    @DexIgnore
    public final WorkoutHeartRate component9() {
        return this.heartRate;
    }

    @DexIgnore
    public final WorkoutSession copy(String str, Date date2, DateTime dateTime, DateTime dateTime2, String str2, WorkoutStep workoutStep, WorkoutCalorie workoutCalorie, WorkoutDistance workoutDistance, WorkoutHeartRate workoutHeartRate, xb5 xb5, ac5 ac5, int i, int i2, long j, long j2, List<WorkoutGpsPoint> list, String str3, ub5 ub5, WorkoutPace workoutPace, WorkoutCadence workoutCadence, DateTime dateTime3, DateTime dateTime4, ac5 ac52, ub5 ub52) {
        ee7.b(str, "id");
        ee7.b(date2, "date");
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        ee7.b(dateTime3, MFSleepSession.COLUMN_EDITED_START_TIME);
        ee7.b(dateTime4, MFSleepSession.COLUMN_EDITED_END_TIME);
        return new WorkoutSession(str, date2, dateTime, dateTime2, str2, workoutStep, workoutCalorie, workoutDistance, workoutHeartRate, xb5, ac5, i, i2, j, j2, list, str3, ub5, workoutPace, workoutCadence, dateTime3, dateTime4, ac52, ub52);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutSession)) {
            return false;
        }
        WorkoutSession workoutSession = (WorkoutSession) obj;
        return ee7.a(this.id, workoutSession.id) && ee7.a(this.date, workoutSession.date) && ee7.a(this.startTime, workoutSession.startTime) && ee7.a(this.endTime, workoutSession.endTime) && ee7.a(this.deviceSerialNumber, workoutSession.deviceSerialNumber) && ee7.a(this.step, workoutSession.step) && ee7.a(this.calorie, workoutSession.calorie) && ee7.a(this.distance, workoutSession.distance) && ee7.a(this.heartRate, workoutSession.heartRate) && ee7.a(this.sourceType, workoutSession.sourceType) && ee7.a(this.workoutType, workoutSession.workoutType) && this.timezoneOffsetInSecond == workoutSession.timezoneOffsetInSecond && this.duration == workoutSession.duration && this.createdAt == workoutSession.createdAt && this.updatedAt == workoutSession.updatedAt && ee7.a(this.workoutGpsPoints, workoutSession.workoutGpsPoints) && ee7.a(this.gpsDataPoints, workoutSession.gpsDataPoints) && ee7.a(this.mode, workoutSession.mode) && ee7.a(this.pace, workoutSession.pace) && ee7.a(this.cadence, workoutSession.cadence) && ee7.a(this.editedStartTime, workoutSession.editedStartTime) && ee7.a(this.editedEndTime, workoutSession.editedEndTime) && ee7.a(this.editedType, workoutSession.editedType) && ee7.a(this.editedMode, workoutSession.editedMode);
    }

    @DexIgnore
    public final Float getAverageHeartRate() {
        WorkoutHeartRate workoutHeartRate = this.heartRate;
        if (workoutHeartRate != null) {
            return Float.valueOf(workoutHeartRate.getAverage());
        }
        return null;
    }

    @DexIgnore
    public final WorkoutCadence getCadence() {
        return this.cadence;
    }

    @DexIgnore
    public final WorkoutCalorie getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }

    @DexIgnore
    public final WorkoutDistance getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final int getDuration() {
        return this.duration;
    }

    @DexIgnore
    public final DateTime getEditedEndTime() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final ub5 getEditedMode() {
        return this.editedMode;
    }

    @DexIgnore
    public final DateTime getEditedStartTime() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final ac5 getEditedType() {
        return this.editedType;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final String getGpsDataPoints() {
        return this.gpsDataPoints;
    }

    @DexIgnore
    public final WorkoutHeartRate getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final Integer getMaxHeartRate() {
        WorkoutHeartRate workoutHeartRate = this.heartRate;
        if (workoutHeartRate != null) {
            return Integer.valueOf(workoutHeartRate.getMax());
        }
        return null;
    }

    @DexIgnore
    public final ub5 getMode() {
        return this.mode;
    }

    @DexIgnore
    public final WorkoutPace getPace() {
        return this.pace;
    }

    @DexIgnore
    public final String getScreenShotUri() {
        return this.screenShotUri;
    }

    @DexIgnore
    public final xb5 getSourceType() {
        return this.sourceType;
    }

    @DexIgnore
    public final WorkoutSpeed getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final List<WorkoutStateChange> getStates() {
        return this.states;
    }

    @DexIgnore
    public final WorkoutStep getStep() {
        return this.step;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final Float getTotalCalorie() {
        WorkoutCalorie workoutCalorie = this.calorie;
        if (workoutCalorie != null) {
            return Float.valueOf(workoutCalorie.getTotal());
        }
        return null;
    }

    @DexIgnore
    public final Double getTotalDistance() {
        WorkoutDistance workoutDistance = this.distance;
        if (workoutDistance != null) {
            return Double.valueOf(workoutDistance.getTotal());
        }
        return null;
    }

    @DexIgnore
    public final Integer getTotalSteps() {
        WorkoutStep workoutStep = this.step;
        if (workoutStep != null) {
            return Integer.valueOf(workoutStep.getTotal());
        }
        return null;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final List<WorkoutGpsPoint> getWorkoutGpsPoints() {
        return this.workoutGpsPoints;
    }

    @DexIgnore
    public final ac5 getWorkoutType() {
        return this.workoutType;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Date date2 = this.date;
        int hashCode2 = (hashCode + (date2 != null ? date2.hashCode() : 0)) * 31;
        DateTime dateTime = this.startTime;
        int hashCode3 = (hashCode2 + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.endTime;
        int hashCode4 = (hashCode3 + (dateTime2 != null ? dateTime2.hashCode() : 0)) * 31;
        String str2 = this.deviceSerialNumber;
        int hashCode5 = (hashCode4 + (str2 != null ? str2.hashCode() : 0)) * 31;
        WorkoutStep workoutStep = this.step;
        int hashCode6 = (hashCode5 + (workoutStep != null ? workoutStep.hashCode() : 0)) * 31;
        WorkoutCalorie workoutCalorie = this.calorie;
        int hashCode7 = (hashCode6 + (workoutCalorie != null ? workoutCalorie.hashCode() : 0)) * 31;
        WorkoutDistance workoutDistance = this.distance;
        int hashCode8 = (hashCode7 + (workoutDistance != null ? workoutDistance.hashCode() : 0)) * 31;
        WorkoutHeartRate workoutHeartRate = this.heartRate;
        int hashCode9 = (hashCode8 + (workoutHeartRate != null ? workoutHeartRate.hashCode() : 0)) * 31;
        xb5 xb5 = this.sourceType;
        int hashCode10 = (hashCode9 + (xb5 != null ? xb5.hashCode() : 0)) * 31;
        ac5 ac5 = this.workoutType;
        int hashCode11 = (((((((((hashCode10 + (ac5 != null ? ac5.hashCode() : 0)) * 31) + this.timezoneOffsetInSecond) * 31) + this.duration) * 31) + c.a(this.createdAt)) * 31) + c.a(this.updatedAt)) * 31;
        List<WorkoutGpsPoint> list = this.workoutGpsPoints;
        int hashCode12 = (hashCode11 + (list != null ? list.hashCode() : 0)) * 31;
        String str3 = this.gpsDataPoints;
        int hashCode13 = (hashCode12 + (str3 != null ? str3.hashCode() : 0)) * 31;
        ub5 ub5 = this.mode;
        int hashCode14 = (hashCode13 + (ub5 != null ? ub5.hashCode() : 0)) * 31;
        WorkoutPace workoutPace = this.pace;
        int hashCode15 = (hashCode14 + (workoutPace != null ? workoutPace.hashCode() : 0)) * 31;
        WorkoutCadence workoutCadence = this.cadence;
        int hashCode16 = (hashCode15 + (workoutCadence != null ? workoutCadence.hashCode() : 0)) * 31;
        DateTime dateTime3 = this.editedStartTime;
        int hashCode17 = (hashCode16 + (dateTime3 != null ? dateTime3.hashCode() : 0)) * 31;
        DateTime dateTime4 = this.editedEndTime;
        int hashCode18 = (hashCode17 + (dateTime4 != null ? dateTime4.hashCode() : 0)) * 31;
        ac5 ac52 = this.editedType;
        int hashCode19 = (hashCode18 + (ac52 != null ? ac52.hashCode() : 0)) * 31;
        ub5 ub52 = this.editedMode;
        if (ub52 != null) {
            i = ub52.hashCode();
        }
        return hashCode19 + i;
    }

    @DexIgnore
    public final void setEditedEndTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.editedEndTime = dateTime;
    }

    @DexIgnore
    public final void setEditedMode(ub5 ub5) {
        this.editedMode = ub5;
    }

    @DexIgnore
    public final void setEditedStartTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.editedStartTime = dateTime;
    }

    @DexIgnore
    public final void setEditedType(ac5 ac5) {
        this.editedType = ac5;
    }

    @DexIgnore
    public final void setScreenShotUri(String str) {
        this.screenShotUri = str;
    }

    @DexIgnore
    public final void setSpeed(WorkoutSpeed workoutSpeed) {
        this.speed = workoutSpeed;
    }

    @DexIgnore
    public final void setStates(List<WorkoutStateChange> list) {
        ee7.b(list, "<set-?>");
        this.states = list;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSession(id=" + this.id + ", date=" + this.date + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", deviceSerialNumber=" + this.deviceSerialNumber + ", step=" + this.step + ", calorie=" + this.calorie + ", distance=" + this.distance + ", heartRate=" + this.heartRate + ", sourceType=" + this.sourceType + ", workoutType=" + this.workoutType + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", duration=" + this.duration + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", workoutGpsPoints=" + this.workoutGpsPoints + ", gpsDataPoints=" + this.gpsDataPoints + ", mode=" + this.mode + ", pace=" + this.pace + ", cadence=" + this.cadence + ", editedStartTime=" + this.editedStartTime + ", editedEndTime=" + this.editedEndTime + ", editedType=" + this.editedType + ", editedMode=" + this.editedMode + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public WorkoutSession(com.portfolio.platform.data.model.fitnessdata.WorkoutSessionWrapper r31, java.lang.String r32, java.lang.String r33) {
        /*
            r30 = this;
            r0 = r33
            java.lang.String r1 = "workoutSession"
            r2 = r31
            com.fossil.ee7.b(r2, r1)
            java.lang.String r1 = "serialNumber"
            r8 = r32
            com.fossil.ee7.b(r8, r1)
            java.lang.String r1 = "userId"
            com.fossil.ee7.b(r0, r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            java.lang.String r0 = ":device:"
            r1.append(r0)
            org.joda.time.DateTime r0 = r31.getStartTime()
            long r3 = r0.getMillis()
            r0 = 1000(0x3e8, float:1.401E-42)
            long r5 = (long) r0
            long r3 = r3 / r5
            r1.append(r3)
            java.lang.String r4 = r1.toString()
            org.joda.time.DateTime r0 = r31.getStartTime()
            org.joda.time.LocalDateTime r0 = r0.toLocalDateTime()
            java.util.Date r5 = r0.toDate()
            java.lang.String r0 = "workoutSession.startTime\u2026oLocalDateTime().toDate()"
            com.fossil.ee7.a(r5, r0)
            org.joda.time.DateTime r6 = r31.getStartTime()
            org.joda.time.DateTime r7 = r31.getEndTime()
            com.portfolio.platform.data.model.diana.workout.WorkoutStep r9 = new com.portfolio.platform.data.model.diana.workout.WorkoutStep
            com.portfolio.platform.data.model.fitnessdata.StepWrapper r0 = r31.getStep()
            r9.<init>(r0)
            com.portfolio.platform.data.model.diana.workout.WorkoutCalorie r10 = new com.portfolio.platform.data.model.diana.workout.WorkoutCalorie
            com.portfolio.platform.data.model.fitnessdata.CalorieWrapper r0 = r31.getCalorie()
            r10.<init>(r0)
            com.portfolio.platform.data.model.fitnessdata.DistanceWrapper r0 = r31.getDistance()
            com.portfolio.platform.data.model.diana.workout.WorkoutDistance r11 = com.portfolio.platform.data.model.diana.workout.WorkoutDistanceKt.toWorkoutDistance(r0)
            com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper r0 = r31.getHeartRate()
            if (r0 == 0) goto L_0x0075
            com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate r3 = new com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate
            r3.<init>(r0)
            r12 = r3
            goto L_0x0076
        L_0x0075:
            r12 = 0
        L_0x0076:
            com.fossil.xb5 r13 = com.fossil.xb5.DEVICE
            com.fossil.ac5$a r0 = com.fossil.ac5.Companion
            int r3 = r31.getType()
            com.fossil.ac5 r14 = r0.a(r3)
            int r15 = r31.getTimezoneOffsetInSecond()
            int r16 = r31.getDuration()
            long r17 = java.lang.System.currentTimeMillis()
            long r19 = java.lang.System.currentTimeMillis()
            java.util.List r0 = r31.getGpsPoints()
            if (r0 == 0) goto L_0x00c7
            java.util.ArrayList r3 = new java.util.ArrayList
            r1 = 10
            int r1 = com.fossil.x97.a(r0, r1)
            r3.<init>(r1)
            java.util.Iterator r0 = r0.iterator()
        L_0x00a7:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x00c0
            java.lang.Object r1 = r0.next()
            com.portfolio.platform.data.model.fitnessdata.GpsDataPointWrapper r1 = (com.portfolio.platform.data.model.fitnessdata.GpsDataPointWrapper) r1
            r21 = r0
            com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint r0 = new com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint
            r0.<init>(r1)
            r3.add(r0)
            r0 = r21
            goto L_0x00a7
        L_0x00c0:
            java.util.List r0 = com.fossil.ea7.d(r3)
            r21 = r0
            goto L_0x00c9
        L_0x00c7:
            r21 = 0
        L_0x00c9:
            java.lang.String r22 = r31.getEncodedGpsData()
            com.fossil.ub5$a r0 = com.fossil.ub5.Companion
            int r1 = r31.getMode()
            com.fossil.ub5 r23 = r0.a(r1)
            com.portfolio.platform.data.model.fitnessdata.PaceWrapper r0 = r31.getPace()
            if (r0 == 0) goto L_0x00e5
            com.portfolio.platform.data.model.diana.workout.WorkoutPace r1 = new com.portfolio.platform.data.model.diana.workout.WorkoutPace
            r1.<init>(r0)
            r24 = r1
            goto L_0x00e7
        L_0x00e5:
            r24 = 0
        L_0x00e7:
            com.portfolio.platform.data.model.fitnessdata.CadenceWrapper r0 = r31.getCadence()
            if (r0 == 0) goto L_0x00f5
            com.portfolio.platform.data.model.diana.workout.WorkoutCadence r1 = new com.portfolio.platform.data.model.diana.workout.WorkoutCadence
            r1.<init>(r0)
            r25 = r1
            goto L_0x00f7
        L_0x00f5:
            r25 = 0
        L_0x00f7:
            org.joda.time.DateTime r26 = r31.getStartTime()
            org.joda.time.DateTime r27 = r31.getEndTime()
            com.fossil.ac5$a r0 = com.fossil.ac5.Companion
            int r1 = r31.getType()
            com.fossil.ac5 r28 = r0.a(r1)
            com.fossil.ub5$a r0 = com.fossil.ub5.Companion
            int r1 = r31.getMode()
            com.fossil.ub5 r29 = r0.a(r1)
            r3 = r30
            r8 = r32
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r19, r21, r22, r23, r24, r25, r26, r27, r28, r29)
            java.util.List r0 = r31.getStateChanges()
            java.util.Iterator r0 = r0.iterator()
        L_0x0122:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0153
            java.lang.Object r1 = r0.next()
            com.portfolio.platform.data.model.fitnessdata.WorkoutStateChangeWrapper r1 = (com.portfolio.platform.data.model.fitnessdata.WorkoutStateChangeWrapper) r1
            r2 = r30
            java.util.List<com.portfolio.platform.data.model.diana.workout.WorkoutStateChange> r3 = r2.states
            com.portfolio.platform.data.model.diana.workout.WorkoutStateChange r4 = new com.portfolio.platform.data.model.diana.workout.WorkoutStateChange
            com.fossil.yb5$a r5 = com.fossil.yb5.Companion
            int r6 = r1.getState()
            com.fossil.yb5 r5 = r5.a(r6)
            java.util.Date r6 = new java.util.Date
            int r1 = r1.getIndexInSecond()
            long r7 = (long) r1
            r9 = 1000(0x3e8, double:4.94E-321)
            long r7 = r7 * r9
            r6.<init>(r7)
            r4.<init>(r5, r6)
            r3.add(r4)
            goto L_0x0122
        L_0x0153:
            r2 = r30
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.diana.workout.WorkoutSession.<init>(com.portfolio.platform.data.model.fitnessdata.WorkoutSessionWrapper, java.lang.String, java.lang.String):void");
    }
}
