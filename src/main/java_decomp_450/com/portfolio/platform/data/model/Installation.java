package com.portfolio.platform.data.model;

import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.te4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Installation {
    @DexIgnore
    @te4("appBuildNumber")
    public int appBuildNumber;
    @DexIgnore
    @te4("appMarketingVersion")
    public String appMarketingVersion;
    @DexIgnore
    @te4("appName")
    public String appName;
    @DexIgnore
    @te4("appPermissions")
    public String[] appPermissions;
    @DexIgnore
    @te4("appVersion")
    public String appVersion;
    @DexIgnore
    @te4("badge")
    public int badge;
    @DexIgnore
    @te4("channels")
    public String[] channels;
    @DexIgnore
    @te4("deviceToken")
    public String deviceToken;
    @DexIgnore
    @te4("deviceType")
    public String deviceType;
    @DexIgnore
    @te4("gcmSenderId")
    public String gcmId;
    @DexIgnore
    @te4("id")
    public String installationId;
    @DexIgnore
    @te4("localeIdentifier")
    public String localeIdentifier;
    @DexIgnore
    @te4(DeviceRequestsHelper.DEVICE_INFO_MODEL)
    public String model;
    @DexIgnore
    @te4("osVersion")
    public String osVersion;
    @DexIgnore
    @te4("pushType")
    public String pushType;
    @DexIgnore
    @te4("timeZone")
    public String timeZone;
    @DexIgnore
    public transient MFUser user;

    @DexIgnore
    public final int getAppBuildNumber() {
        return this.appBuildNumber;
    }

    @DexIgnore
    public final String getAppMarketingVersion() {
        return this.appMarketingVersion;
    }

    @DexIgnore
    public final String getAppName() {
        return this.appName;
    }

    @DexIgnore
    public final String[] getAppPermissions() {
        return this.appPermissions;
    }

    @DexIgnore
    public final String getAppVersion() {
        return this.appVersion;
    }

    @DexIgnore
    public final int getBadge() {
        return this.badge;
    }

    @DexIgnore
    public final String[] getChannels() {
        return this.channels;
    }

    @DexIgnore
    public final String getDeviceToken() {
        return this.deviceToken;
    }

    @DexIgnore
    public final String getDeviceType() {
        return this.deviceType;
    }

    @DexIgnore
    public final String getGcmId() {
        return this.gcmId;
    }

    @DexIgnore
    public final String getInstallationId() {
        return this.installationId;
    }

    @DexIgnore
    public final String getLocaleIdentifier() {
        return this.localeIdentifier;
    }

    @DexIgnore
    public final String getModel() {
        return this.model;
    }

    @DexIgnore
    public final String getOsVersion() {
        return this.osVersion;
    }

    @DexIgnore
    public final String getPushType() {
        return this.pushType;
    }

    @DexIgnore
    public final String getTimeZone() {
        return this.timeZone;
    }

    @DexIgnore
    public final MFUser getUser() {
        return this.user;
    }

    @DexIgnore
    public final void setAppBuildNumber(int i) {
        this.appBuildNumber = i;
    }

    @DexIgnore
    public final void setAppMarketingVersion(String str) {
        this.appMarketingVersion = str;
    }

    @DexIgnore
    public final void setAppName(String str) {
        this.appName = str;
    }

    @DexIgnore
    public final void setAppPermissions(String[] strArr) {
        this.appPermissions = strArr;
    }

    @DexIgnore
    public final void setAppVersion(String str) {
        this.appVersion = str;
    }

    @DexIgnore
    public final void setBadge(int i) {
        this.badge = i;
    }

    @DexIgnore
    public final void setChannels(String[] strArr) {
        this.channels = strArr;
    }

    @DexIgnore
    public final void setDeviceToken(String str) {
        this.deviceToken = str;
    }

    @DexIgnore
    public final void setDeviceType(String str) {
        this.deviceType = str;
    }

    @DexIgnore
    public final void setGcmId(String str) {
        this.gcmId = str;
    }

    @DexIgnore
    public final void setInstallationId(String str) {
        this.installationId = str;
    }

    @DexIgnore
    public final void setLocaleIdentifier(String str) {
        this.localeIdentifier = str;
    }

    @DexIgnore
    public final void setModel(String str) {
        this.model = str;
    }

    @DexIgnore
    public final void setOsVersion(String str) {
        this.osVersion = str;
    }

    @DexIgnore
    public final void setPushType(String str) {
        this.pushType = str;
    }

    @DexIgnore
    public final void setTimeZone(String str) {
        this.timeZone = str;
    }

    @DexIgnore
    public final void setUser(MFUser mFUser) {
        this.user = mFUser;
    }
}
