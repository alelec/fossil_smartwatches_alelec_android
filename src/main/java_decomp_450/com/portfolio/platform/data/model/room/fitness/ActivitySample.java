package com.portfolio.platform.data.model.room.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.c;
import com.fossil.ee7;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.zd7;
import com.portfolio.platform.data.ActivityIntensities;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySample implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public static /* final */ String ID; // = "id";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "activity_sample";
    @DexIgnore
    public int activeTime;
    @DexIgnore
    public double calories;
    @DexIgnore
    public long createdAt;
    @DexIgnore
    public Date date;
    @DexIgnore
    public double distance;
    @DexIgnore
    public DateTime endTime;
    @DexIgnore
    public String id;
    @DexIgnore
    public ActivityIntensities intensityDistInSteps;
    @DexIgnore
    public String sourceId;
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public double steps;
    @DexIgnore
    public long syncTime;
    @DexIgnore
    public int timeZoneOffsetInSecond;
    @DexIgnore
    public /* final */ String uid;
    @DexIgnore
    public long updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ActivitySample> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActivitySample createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new ActivitySample(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActivitySample[] newArray(int i) {
            return new ActivitySample[i];
        }
    }

    @DexIgnore
    public ActivitySample(String str, Date date2, DateTime dateTime, DateTime dateTime2, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, int i2, String str2, long j, long j2, long j3) {
        ee7.b(str, "uid");
        ee7.b(date2, "date");
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        ee7.b(activityIntensities, "intensityDistInSteps");
        ee7.b(str2, SampleRaw.COLUMN_SOURCE_ID);
        this.uid = str;
        this.date = date2;
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.steps = d;
        this.calories = d2;
        this.distance = d3;
        this.activeTime = i;
        this.intensityDistInSteps = activityIntensities;
        this.timeZoneOffsetInSecond = i2;
        this.sourceId = str2;
        this.syncTime = j;
        this.createdAt = j2;
        this.updatedAt = j3;
        this.id = generateId();
    }

    @DexIgnore
    public static /* synthetic */ ActivitySample copy$default(ActivitySample activitySample, String str, Date date2, DateTime dateTime, DateTime dateTime2, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, int i2, String str2, long j, long j2, long j3, int i3, Object obj) {
        return activitySample.copy((i3 & 1) != 0 ? activitySample.uid : str, (i3 & 2) != 0 ? activitySample.date : date2, (i3 & 4) != 0 ? activitySample.startTime : dateTime, (i3 & 8) != 0 ? activitySample.endTime : dateTime2, (i3 & 16) != 0 ? activitySample.steps : d, (i3 & 32) != 0 ? activitySample.calories : d2, (i3 & 64) != 0 ? activitySample.distance : d3, (i3 & 128) != 0 ? activitySample.activeTime : i, (i3 & 256) != 0 ? activitySample.intensityDistInSteps : activityIntensities, (i3 & 512) != 0 ? activitySample.timeZoneOffsetInSecond : i2, (i3 & 1024) != 0 ? activitySample.sourceId : str2, (i3 & 2048) != 0 ? activitySample.syncTime : j, (i3 & 4096) != 0 ? activitySample.createdAt : j2, (i3 & 8192) != 0 ? activitySample.updatedAt : j3);
    }

    @DexIgnore
    private final String generateId() {
        return this.uid + ":device:" + (this.startTime.getMillis() / ((long) 1000));
    }

    @DexIgnore
    public final String component1() {
        return this.uid;
    }

    @DexIgnore
    public final int component10() {
        return this.timeZoneOffsetInSecond;
    }

    @DexIgnore
    public final String component11() {
        return this.sourceId;
    }

    @DexIgnore
    public final long component12() {
        return this.syncTime;
    }

    @DexIgnore
    public final long component13() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component14() {
        return this.updatedAt;
    }

    @DexIgnore
    public final Date component2() {
        return this.date;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component4() {
        return this.endTime;
    }

    @DexIgnore
    public final double component5() {
        return this.steps;
    }

    @DexIgnore
    public final double component6() {
        return this.calories;
    }

    @DexIgnore
    public final double component7() {
        return this.distance;
    }

    @DexIgnore
    public final int component8() {
        return this.activeTime;
    }

    @DexIgnore
    public final ActivityIntensities component9() {
        return this.intensityDistInSteps;
    }

    @DexIgnore
    public final ActivitySample copy(String str, Date date2, DateTime dateTime, DateTime dateTime2, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, int i2, String str2, long j, long j2, long j3) {
        ee7.b(str, "uid");
        ee7.b(date2, "date");
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        ee7.b(activityIntensities, "intensityDistInSteps");
        ee7.b(str2, SampleRaw.COLUMN_SOURCE_ID);
        return new ActivitySample(str, date2, dateTime, dateTime2, d, d2, d3, i, activityIntensities, i2, str2, j, j2, j3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActivitySample)) {
            return false;
        }
        ActivitySample activitySample = (ActivitySample) obj;
        return ee7.a(this.uid, activitySample.uid) && ee7.a(this.date, activitySample.date) && ee7.a(this.startTime, activitySample.startTime) && ee7.a(this.endTime, activitySample.endTime) && Double.compare(this.steps, activitySample.steps) == 0 && Double.compare(this.calories, activitySample.calories) == 0 && Double.compare(this.distance, activitySample.distance) == 0 && this.activeTime == activitySample.activeTime && ee7.a(this.intensityDistInSteps, activitySample.intensityDistInSteps) && this.timeZoneOffsetInSecond == activitySample.timeZoneOffsetInSecond && ee7.a(this.sourceId, activitySample.sourceId) && this.syncTime == activitySample.syncTime && this.createdAt == activitySample.createdAt && this.updatedAt == activitySample.updatedAt;
    }

    @DexIgnore
    public final int getActiveTime() {
        return this.activeTime;
    }

    @DexIgnore
    public final double getCalories() {
        return this.calories;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final double getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final ActivityIntensities getIntensityDistInSteps() {
        return this.intensityDistInSteps;
    }

    @DexIgnore
    public final String getSourceId() {
        return this.sourceId;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final double getSteps() {
        return this.steps;
    }

    @DexIgnore
    public final long getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public final int getTimeZoneOffsetInSecond() {
        return this.timeZoneOffsetInSecond;
    }

    @DexIgnore
    public final String getUid() {
        return this.uid;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.uid;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Date date2 = this.date;
        int hashCode2 = (hashCode + (date2 != null ? date2.hashCode() : 0)) * 31;
        DateTime dateTime = this.startTime;
        int hashCode3 = (hashCode2 + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.endTime;
        int hashCode4 = (((((((((hashCode3 + (dateTime2 != null ? dateTime2.hashCode() : 0)) * 31) + Double.doubleToLongBits(this.steps)) * 31) + Double.doubleToLongBits(this.calories)) * 31) + Double.doubleToLongBits(this.distance)) * 31) + this.activeTime) * 31;
        ActivityIntensities activityIntensities = this.intensityDistInSteps;
        int hashCode5 = (((hashCode4 + (activityIntensities != null ? activityIntensities.hashCode() : 0)) * 31) + this.timeZoneOffsetInSecond) * 31;
        String str2 = this.sourceId;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return ((((((hashCode5 + i) * 31) + c.a(this.syncTime)) * 31) + c.a(this.createdAt)) * 31) + c.a(this.updatedAt);
    }

    @DexIgnore
    public final void setActiveTime(int i) {
        this.activeTime = i;
    }

    @DexIgnore
    public final void setCalories(double d) {
        this.calories = d;
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        ee7.b(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setDistance(double d) {
        this.distance = d;
    }

    @DexIgnore
    public final void setEndTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.endTime = dateTime;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setIntensityDistInSteps(ActivityIntensities activityIntensities) {
        ee7.b(activityIntensities, "<set-?>");
        this.intensityDistInSteps = activityIntensities;
    }

    @DexIgnore
    public final void setSourceId(String str) {
        ee7.b(str, "<set-?>");
        this.sourceId = str;
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setStartTimeId(DateTime dateTime) {
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        this.startTime = dateTime;
        this.id = generateId();
    }

    @DexIgnore
    public final void setSteps(double d) {
        this.steps = d;
    }

    @DexIgnore
    public final void setSyncTime(long j) {
        this.syncTime = j;
    }

    @DexIgnore
    public final void setTimeZoneOffsetInSecond(int i) {
        this.timeZoneOffsetInSecond = i;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "ActivitySample(uid=" + this.uid + ", date=" + this.date + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", steps=" + this.steps + ", calories=" + this.calories + ", distance=" + this.distance + ", activeTime=" + this.activeTime + ", intensityDistInSteps=" + this.intensityDistInSteps + ", timeZoneOffsetInSecond=" + this.timeZoneOffsetInSecond + ", sourceId=" + this.sourceId + ", syncTime=" + this.syncTime + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeString(this.uid);
        parcel.writeSerializable(this.date);
        parcel.writeSerializable(this.startTime);
        parcel.writeSerializable(this.endTime);
        parcel.writeDouble(this.steps);
        parcel.writeDouble(this.calories);
        parcel.writeDouble(this.distance);
        parcel.writeInt(this.activeTime);
        parcel.writeParcelable(this.intensityDistInSteps, i);
        parcel.writeInt(this.timeZoneOffsetInSecond);
        parcel.writeLong(this.syncTime);
        parcel.writeLong(this.createdAt);
        parcel.writeLong(this.updatedAt);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivitySample(String str, Date date2, DateTime dateTime, DateTime dateTime2, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, int i2, String str2, long j) {
        this(str, date2, dateTime, dateTime2, d, d2, d3, i, activityIntensities, i2, str2, j, System.currentTimeMillis(), System.currentTimeMillis());
        ee7.b(str, "uid");
        ee7.b(date2, "date");
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        ee7.b(activityIntensities, "intensityDistInSteps");
        ee7.b(str2, SampleRaw.COLUMN_SOURCE_ID);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ActivitySample(android.os.Parcel r25) {
        /*
            r24 = this;
            r0 = r25
            java.lang.String r1 = "parcel"
            com.fossil.ee7.b(r0, r1)
            java.lang.String r1 = r25.readString()
            java.lang.String r2 = ""
            if (r1 == 0) goto L_0x0011
            r4 = r1
            goto L_0x0012
        L_0x0011:
            r4 = r2
        L_0x0012:
            java.io.Serializable r1 = r25.readSerializable()
            if (r1 == 0) goto L_0x0088
            r5 = r1
            java.util.Date r5 = (java.util.Date) r5
            java.io.Serializable r1 = r25.readSerializable()
            java.lang.String r3 = "null cannot be cast to non-null type org.joda.time.DateTime"
            if (r1 == 0) goto L_0x0082
            r6 = r1
            org.joda.time.DateTime r6 = (org.joda.time.DateTime) r6
            java.io.Serializable r1 = r25.readSerializable()
            if (r1 == 0) goto L_0x007c
            r7 = r1
            org.joda.time.DateTime r7 = (org.joda.time.DateTime) r7
            double r8 = r25.readDouble()
            double r10 = r25.readDouble()
            double r12 = r25.readDouble()
            int r14 = r25.readInt()
            java.lang.Class<com.portfolio.platform.data.ActivityIntensities> r1 = com.portfolio.platform.data.ActivityIntensities.class
            java.lang.ClassLoader r1 = r1.getClassLoader()
            android.os.Parcelable r1 = r0.readParcelable(r1)
            com.portfolio.platform.data.ActivityIntensities r1 = (com.portfolio.platform.data.ActivityIntensities) r1
            if (r1 == 0) goto L_0x004f
            r15 = r1
            goto L_0x005b
        L_0x004f:
            com.portfolio.platform.data.ActivityIntensities r1 = new com.portfolio.platform.data.ActivityIntensities
            r16 = 0
            r18 = 0
            r20 = 0
            r15 = r1
            r15.<init>(r16, r18, r20)
        L_0x005b:
            int r16 = r25.readInt()
            java.lang.String r1 = r25.readString()
            if (r1 == 0) goto L_0x0068
            r17 = r1
            goto L_0x006a
        L_0x0068:
            r17 = r2
        L_0x006a:
            long r18 = r25.readLong()
            long r20 = r25.readLong()
            long r22 = r25.readLong()
            r3 = r24
            r3.<init>(r4, r5, r6, r7, r8, r10, r12, r14, r15, r16, r17, r18, r20, r22)
            return
        L_0x007c:
            com.fossil.x87 r0 = new com.fossil.x87
            r0.<init>(r3)
            throw r0
        L_0x0082:
            com.fossil.x87 r0 = new com.fossil.x87
            r0.<init>(r3)
            throw r0
        L_0x0088:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r1 = "null cannot be cast to non-null type java.util.Date"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.room.fitness.ActivitySample.<init>(android.os.Parcel):void");
    }
}
