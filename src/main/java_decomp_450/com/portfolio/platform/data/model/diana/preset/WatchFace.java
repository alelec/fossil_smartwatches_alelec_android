package com.portfolio.platform.data.model.diana.preset;

import com.fossil.ee7;
import com.fossil.te4;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFace {
    @DexIgnore
    @te4(Explore.COLUMN_BACKGROUND)
    public Background background;
    @DexIgnore
    @te4("id")
    public String id;
    @DexIgnore
    @te4("name")
    public String name;
    @DexIgnore
    @te4("previewUrl")
    public String previewUrl;
    @DexIgnore
    @te4("ringStyles")
    public ArrayList<RingStyleItem> ringStyleItems;
    @DexIgnore
    public String serial;
    @DexIgnore
    public int watchFaceType;

    @DexIgnore
    public WatchFace(String str, String str2, ArrayList<RingStyleItem> arrayList, Background background2, String str3, String str4, int i) {
        ee7.b(str, "id");
        ee7.b(str2, "name");
        ee7.b(background2, Explore.COLUMN_BACKGROUND);
        ee7.b(str3, "previewUrl");
        ee7.b(str4, "serial");
        this.id = str;
        this.name = str2;
        this.ringStyleItems = arrayList;
        this.background = background2;
        this.previewUrl = str3;
        this.serial = str4;
        this.watchFaceType = i;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.portfolio.platform.data.model.diana.preset.WatchFace */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ WatchFace copy$default(WatchFace watchFace, String str, String str2, ArrayList arrayList, Background background2, String str3, String str4, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = watchFace.id;
        }
        if ((i2 & 2) != 0) {
            str2 = watchFace.name;
        }
        if ((i2 & 4) != 0) {
            arrayList = watchFace.ringStyleItems;
        }
        if ((i2 & 8) != 0) {
            background2 = watchFace.background;
        }
        if ((i2 & 16) != 0) {
            str3 = watchFace.previewUrl;
        }
        if ((i2 & 32) != 0) {
            str4 = watchFace.serial;
        }
        if ((i2 & 64) != 0) {
            i = watchFace.watchFaceType;
        }
        return watchFace.copy(str, str2, arrayList, background2, str3, str4, i);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final ArrayList<RingStyleItem> component3() {
        return this.ringStyleItems;
    }

    @DexIgnore
    public final Background component4() {
        return this.background;
    }

    @DexIgnore
    public final String component5() {
        return this.previewUrl;
    }

    @DexIgnore
    public final String component6() {
        return this.serial;
    }

    @DexIgnore
    public final int component7() {
        return this.watchFaceType;
    }

    @DexIgnore
    public final WatchFace copy(String str, String str2, ArrayList<RingStyleItem> arrayList, Background background2, String str3, String str4, int i) {
        ee7.b(str, "id");
        ee7.b(str2, "name");
        ee7.b(background2, Explore.COLUMN_BACKGROUND);
        ee7.b(str3, "previewUrl");
        ee7.b(str4, "serial");
        return new WatchFace(str, str2, arrayList, background2, str3, str4, i);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WatchFace)) {
            return false;
        }
        WatchFace watchFace = (WatchFace) obj;
        return ee7.a(this.id, watchFace.id) && ee7.a(this.name, watchFace.name) && ee7.a(this.ringStyleItems, watchFace.ringStyleItems) && ee7.a(this.background, watchFace.background) && ee7.a(this.previewUrl, watchFace.previewUrl) && ee7.a(this.serial, watchFace.serial) && this.watchFaceType == watchFace.watchFaceType;
    }

    @DexIgnore
    public final Background getBackground() {
        return this.background;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getPreviewUrl() {
        return this.previewUrl;
    }

    @DexIgnore
    public final ArrayList<RingStyleItem> getRingStyleItems() {
        return this.ringStyleItems;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public final int getWatchFaceType() {
        return this.watchFaceType;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        ArrayList<RingStyleItem> arrayList = this.ringStyleItems;
        int hashCode3 = (hashCode2 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        Background background2 = this.background;
        int hashCode4 = (hashCode3 + (background2 != null ? background2.hashCode() : 0)) * 31;
        String str3 = this.previewUrl;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.serial;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return ((hashCode5 + i) * 31) + this.watchFaceType;
    }

    @DexIgnore
    public final void setBackground(Background background2) {
        ee7.b(background2, "<set-?>");
        this.background = background2;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        ee7.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setPreviewUrl(String str) {
        ee7.b(str, "<set-?>");
        this.previewUrl = str;
    }

    @DexIgnore
    public final void setRingStyleItems(ArrayList<RingStyleItem> arrayList) {
        this.ringStyleItems = arrayList;
    }

    @DexIgnore
    public final void setSerial(String str) {
        ee7.b(str, "<set-?>");
        this.serial = str;
    }

    @DexIgnore
    public final void setWatchFaceType(int i) {
        this.watchFaceType = i;
    }

    @DexIgnore
    public String toString() {
        return "WatchFace(id=" + this.id + ", name=" + this.name + ", ringStyleItems=" + this.ringStyleItems + ", background=" + this.background + ", previewUrl=" + this.previewUrl + ", serial=" + this.serial + ", watchFaceType=" + this.watchFaceType + ")";
    }
}
