package com.portfolio.platform.data.model.ua;

import com.fossil.te4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UALink {
    @DexIgnore
    @te4("href")
    public String href;
    @DexIgnore
    @te4("id")
    public String id;

    @DexIgnore
    public final String getHref() {
        return this.href;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final void setHref(String str) {
        this.href = str;
    }

    @DexIgnore
    public final void setId(String str) {
        this.id = str;
    }
}
