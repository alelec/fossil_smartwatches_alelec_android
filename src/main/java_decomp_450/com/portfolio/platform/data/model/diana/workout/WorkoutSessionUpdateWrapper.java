package com.portfolio.platform.data.model.diana.workout;

import com.fossil.ee7;
import com.fossil.wearables.fsl.sleep.MFSleepSession;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionUpdateWrapper {
    @DexIgnore
    public /* final */ String editedEndTime;
    @DexIgnore
    public /* final */ String editedMode;
    @DexIgnore
    public /* final */ String editedStartTime;
    @DexIgnore
    public /* final */ String editedType;
    @DexIgnore
    public /* final */ String id;

    @DexIgnore
    public WorkoutSessionUpdateWrapper(String str, String str2, String str3, String str4, String str5) {
        ee7.b(str, "id");
        ee7.b(str2, MFSleepSession.COLUMN_EDITED_END_TIME);
        ee7.b(str3, MFSleepSession.COLUMN_EDITED_START_TIME);
        ee7.b(str4, "editedType");
        this.id = str;
        this.editedEndTime = str2;
        this.editedStartTime = str3;
        this.editedType = str4;
        this.editedMode = str5;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutSessionUpdateWrapper copy$default(WorkoutSessionUpdateWrapper workoutSessionUpdateWrapper, String str, String str2, String str3, String str4, String str5, int i, Object obj) {
        if ((i & 1) != 0) {
            str = workoutSessionUpdateWrapper.id;
        }
        if ((i & 2) != 0) {
            str2 = workoutSessionUpdateWrapper.editedEndTime;
        }
        if ((i & 4) != 0) {
            str3 = workoutSessionUpdateWrapper.editedStartTime;
        }
        if ((i & 8) != 0) {
            str4 = workoutSessionUpdateWrapper.editedType;
        }
        if ((i & 16) != 0) {
            str5 = workoutSessionUpdateWrapper.editedMode;
        }
        return workoutSessionUpdateWrapper.copy(str, str2, str3, str4, str5);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final String component3() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final String component4() {
        return this.editedType;
    }

    @DexIgnore
    public final String component5() {
        return this.editedMode;
    }

    @DexIgnore
    public final WorkoutSessionUpdateWrapper copy(String str, String str2, String str3, String str4, String str5) {
        ee7.b(str, "id");
        ee7.b(str2, MFSleepSession.COLUMN_EDITED_END_TIME);
        ee7.b(str3, MFSleepSession.COLUMN_EDITED_START_TIME);
        ee7.b(str4, "editedType");
        return new WorkoutSessionUpdateWrapper(str, str2, str3, str4, str5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutSessionUpdateWrapper)) {
            return false;
        }
        WorkoutSessionUpdateWrapper workoutSessionUpdateWrapper = (WorkoutSessionUpdateWrapper) obj;
        return ee7.a(this.id, workoutSessionUpdateWrapper.id) && ee7.a(this.editedEndTime, workoutSessionUpdateWrapper.editedEndTime) && ee7.a(this.editedStartTime, workoutSessionUpdateWrapper.editedStartTime) && ee7.a(this.editedType, workoutSessionUpdateWrapper.editedType) && ee7.a(this.editedMode, workoutSessionUpdateWrapper.editedMode);
    }

    @DexIgnore
    public final String getEditedEndTime() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final String getEditedMode() {
        return this.editedMode;
    }

    @DexIgnore
    public final String getEditedStartTime() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final String getEditedType() {
        return this.editedType;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.editedEndTime;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.editedStartTime;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.editedType;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.editedMode;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSessionUpdateWrapper(id=" + this.id + ", editedEndTime=" + this.editedEndTime + ", editedStartTime=" + this.editedStartTime + ", editedType=" + this.editedType + ", editedMode=" + this.editedMode + ")";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0069, code lost:
        if (r8 != null) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x003c, code lost:
        if (r0 != null) goto L_0x0054;
     */
    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public WorkoutSessionUpdateWrapper(com.portfolio.platform.data.model.diana.workout.WorkoutSession r8) {
        /*
            r7 = this;
            java.lang.String r0 = "workoutSession"
            com.fossil.ee7.b(r8, r0)
            java.lang.String r2 = r8.getId()
            org.joda.time.DateTime r0 = r8.getEditedEndTime()
            java.lang.String r3 = com.fossil.zd5.a(r0)
            java.lang.String r0 = "DateHelper.printServerDa\u2026outSession.editedEndTime)"
            com.fossil.ee7.a(r3, r0)
            org.joda.time.DateTime r0 = r8.getEditedStartTime()
            java.lang.String r4 = com.fossil.zd5.a(r0)
            java.lang.String r0 = "DateHelper.printServerDa\u2026tSession.editedStartTime)"
            com.fossil.ee7.a(r4, r0)
            com.fossil.ac5 r0 = r8.getEditedType()
            java.lang.String r1 = "null cannot be cast to non-null type java.lang.String"
            java.lang.String r5 = "(this as java.lang.String).toLowerCase()"
            if (r0 == 0) goto L_0x0045
            java.lang.String r0 = r0.getMValue()
            if (r0 == 0) goto L_0x0045
            if (r0 == 0) goto L_0x003f
            java.lang.String r0 = r0.toLowerCase()
            com.fossil.ee7.a(r0, r5)
            if (r0 == 0) goto L_0x0045
            goto L_0x0054
        L_0x003f:
            com.fossil.x87 r8 = new com.fossil.x87
            r8.<init>(r1)
            throw r8
        L_0x0045:
            com.fossil.ac5 r0 = com.fossil.ac5.UNKNOWN
            java.lang.String r0 = r0.getMValue()
            if (r0 == 0) goto L_0x008e
            java.lang.String r0 = r0.toLowerCase()
            com.fossil.ee7.a(r0, r5)
        L_0x0054:
            com.fossil.ub5 r8 = r8.getEditedMode()
            if (r8 == 0) goto L_0x0072
            java.lang.String r8 = r8.getMValue()
            if (r8 == 0) goto L_0x0072
            if (r8 == 0) goto L_0x006c
            java.lang.String r8 = r8.toLowerCase()
            com.fossil.ee7.a(r8, r5)
            if (r8 == 0) goto L_0x0072
            goto L_0x0081
        L_0x006c:
            com.fossil.x87 r8 = new com.fossil.x87
            r8.<init>(r1)
            throw r8
        L_0x0072:
            com.fossil.ub5 r8 = com.fossil.ub5.INDOOR
            java.lang.String r8 = r8.getMValue()
            if (r8 == 0) goto L_0x0088
            java.lang.String r8 = r8.toLowerCase()
            com.fossil.ee7.a(r8, r5)
        L_0x0081:
            r6 = r8
            r1 = r7
            r5 = r0
            r1.<init>(r2, r3, r4, r5, r6)
            return
        L_0x0088:
            com.fossil.x87 r8 = new com.fossil.x87
            r8.<init>(r1)
            throw r8
        L_0x008e:
            com.fossil.x87 r8 = new com.fossil.x87
            r8.<init>(r1)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.diana.workout.WorkoutSessionUpdateWrapper.<init>(com.portfolio.platform.data.model.diana.workout.WorkoutSession):void");
    }
}
