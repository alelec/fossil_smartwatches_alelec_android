package com.portfolio.platform.data.model.room.microapp;

import com.fossil.ee7;
import com.fossil.te4;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetAppSetting {
    @DexIgnore
    @te4("appId")
    public String appId;
    @DexIgnore
    @te4("localUpdatedAt")
    public String localUpdateAt;
    @DexIgnore
    @te4("buttonPosition")
    public String position;
    @DexIgnore
    @te4(Constants.USER_SETTING)
    public String settings;

    @DexIgnore
    public HybridPresetAppSetting(String str, String str2, String str3, String str4) {
        ee7.b(str, "position");
        ee7.b(str2, "appId");
        ee7.b(str3, "localUpdateAt");
        this.position = str;
        this.appId = str2;
        this.localUpdateAt = str3;
        this.settings = str4;
    }

    @DexIgnore
    public static /* synthetic */ HybridPresetAppSetting copy$default(HybridPresetAppSetting hybridPresetAppSetting, String str, String str2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = hybridPresetAppSetting.position;
        }
        if ((i & 2) != 0) {
            str2 = hybridPresetAppSetting.appId;
        }
        if ((i & 4) != 0) {
            str3 = hybridPresetAppSetting.localUpdateAt;
        }
        if ((i & 8) != 0) {
            str4 = hybridPresetAppSetting.settings;
        }
        return hybridPresetAppSetting.copy(str, str2, str3, str4);
    }

    @DexIgnore
    public final String component1() {
        return this.position;
    }

    @DexIgnore
    public final String component2() {
        return this.appId;
    }

    @DexIgnore
    public final String component3() {
        return this.localUpdateAt;
    }

    @DexIgnore
    public final String component4() {
        return this.settings;
    }

    @DexIgnore
    public final HybridPresetAppSetting copy(String str, String str2, String str3, String str4) {
        ee7.b(str, "position");
        ee7.b(str2, "appId");
        ee7.b(str3, "localUpdateAt");
        return new HybridPresetAppSetting(str, str2, str3, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HybridPresetAppSetting)) {
            return false;
        }
        HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) obj;
        return ee7.a(this.position, hybridPresetAppSetting.position) && ee7.a(this.appId, hybridPresetAppSetting.appId) && ee7.a(this.localUpdateAt, hybridPresetAppSetting.localUpdateAt) && ee7.a(this.settings, hybridPresetAppSetting.settings);
    }

    @DexIgnore
    public final String getAppId() {
        return this.appId;
    }

    @DexIgnore
    public final String getLocalUpdateAt() {
        return this.localUpdateAt;
    }

    @DexIgnore
    public final String getPosition() {
        return this.position;
    }

    @DexIgnore
    public final String getSettings() {
        return this.settings;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.position;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.appId;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.localUpdateAt;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.settings;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public final void setAppId(String str) {
        ee7.b(str, "<set-?>");
        this.appId = str;
    }

    @DexIgnore
    public final void setLocalUpdateAt(String str) {
        ee7.b(str, "<set-?>");
        this.localUpdateAt = str;
    }

    @DexIgnore
    public final void setPosition(String str) {
        ee7.b(str, "<set-?>");
        this.position = str;
    }

    @DexIgnore
    public final void setSettings(String str) {
        this.settings = str;
    }

    @DexIgnore
    public String toString() {
        return "HybridPresetAppSetting(position=" + this.position + ", appId=" + this.appId + ", localUpdateAt=" + this.localUpdateAt + ", settings=" + this.settings + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public HybridPresetAppSetting(String str, String str2, String str3) {
        this(str, str2, str3, "");
        ee7.b(str, "position");
        ee7.b(str2, "complicationId");
        ee7.b(str3, "localUpdateAt");
    }
}
