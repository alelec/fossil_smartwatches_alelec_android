package com.portfolio.platform.data.model.diana.workout;

import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.zd7;
import java.io.Serializable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutScreenshotWrapper implements Serializable {
    @DexIgnore
    @te4("height")
    public /* final */ int height;
    @DexIgnore
    @te4("listCoor")
    public List<WorkoutRouterGpsWrapper> listCoor;
    @DexIgnore
    public int pinType;
    @DexIgnore
    @te4("resetState")
    public boolean resetState;
    @DexIgnore
    @te4("width")
    public int width;
    @DexIgnore
    @te4("workoutId")
    public String workoutId;

    @DexIgnore
    public WorkoutScreenshotWrapper(String str, int i, int i2, boolean z, List<WorkoutRouterGpsWrapper> list) {
        ee7.b(str, "workoutId");
        this.workoutId = str;
        this.width = i;
        this.height = i2;
        this.resetState = z;
        this.listCoor = list;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.portfolio.platform.data.model.diana.workout.WorkoutScreenshotWrapper */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ WorkoutScreenshotWrapper copy$default(WorkoutScreenshotWrapper workoutScreenshotWrapper, String str, int i, int i2, boolean z, List list, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            str = workoutScreenshotWrapper.workoutId;
        }
        if ((i3 & 2) != 0) {
            i = workoutScreenshotWrapper.width;
        }
        if ((i3 & 4) != 0) {
            i2 = workoutScreenshotWrapper.height;
        }
        if ((i3 & 8) != 0) {
            z = workoutScreenshotWrapper.resetState;
        }
        if ((i3 & 16) != 0) {
            list = workoutScreenshotWrapper.listCoor;
        }
        return workoutScreenshotWrapper.copy(str, i, i2, z, list);
    }

    @DexIgnore
    public final String component1() {
        return this.workoutId;
    }

    @DexIgnore
    public final int component2() {
        return this.width;
    }

    @DexIgnore
    public final int component3() {
        return this.height;
    }

    @DexIgnore
    public final boolean component4() {
        return this.resetState;
    }

    @DexIgnore
    public final List<WorkoutRouterGpsWrapper> component5() {
        return this.listCoor;
    }

    @DexIgnore
    public final WorkoutScreenshotWrapper copy(String str, int i, int i2, boolean z, List<WorkoutRouterGpsWrapper> list) {
        ee7.b(str, "workoutId");
        return new WorkoutScreenshotWrapper(str, i, i2, z, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutScreenshotWrapper)) {
            return false;
        }
        WorkoutScreenshotWrapper workoutScreenshotWrapper = (WorkoutScreenshotWrapper) obj;
        return ee7.a(this.workoutId, workoutScreenshotWrapper.workoutId) && this.width == workoutScreenshotWrapper.width && this.height == workoutScreenshotWrapper.height && this.resetState == workoutScreenshotWrapper.resetState && ee7.a(this.listCoor, workoutScreenshotWrapper.listCoor);
    }

    @DexIgnore
    public final int getHeight() {
        return this.height;
    }

    @DexIgnore
    public final List<WorkoutRouterGpsWrapper> getListCoor() {
        return this.listCoor;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final boolean getResetState() {
        return this.resetState;
    }

    @DexIgnore
    public final int getWidth() {
        return this.width;
    }

    @DexIgnore
    public final String getWorkoutId() {
        return this.workoutId;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.workoutId;
        int i = 0;
        int hashCode = (((((str != null ? str.hashCode() : 0) * 31) + this.width) * 31) + this.height) * 31;
        boolean z = this.resetState;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = (hashCode + i2) * 31;
        List<WorkoutRouterGpsWrapper> list = this.listCoor;
        if (list != null) {
            i = list.hashCode();
        }
        return i4 + i;
    }

    @DexIgnore
    public final void setListCoor(List<WorkoutRouterGpsWrapper> list) {
        this.listCoor = list;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setResetState(boolean z) {
        this.resetState = z;
    }

    @DexIgnore
    public final void setWidth(int i) {
        this.width = i;
    }

    @DexIgnore
    public final void setWorkoutId(String str) {
        ee7.b(str, "<set-?>");
        this.workoutId = str;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutScreenshotWrapper(workoutId=" + this.workoutId + ", width=" + this.width + ", height=" + this.height + ", resetState=" + this.resetState + ", listCoor=" + this.listCoor + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WorkoutScreenshotWrapper(String str, int i, int i2, boolean z, List list, int i3, zd7 zd7) {
        this((i3 & 1) != 0 ? "0" : str, i, i2, (i3 & 8) != 0 ? true : z, list);
    }
}
