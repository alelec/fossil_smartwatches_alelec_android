package com.portfolio.platform.data.model.thirdparty.googlefit;

import com.fossil.c;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitWOCalorie {
    @DexIgnore
    public float calorie;
    @DexIgnore
    public long endTime;
    @DexIgnore
    public long startTime;

    @DexIgnore
    public GFitWOCalorie(float f, long j, long j2) {
        this.calorie = f;
        this.startTime = j;
        this.endTime = j2;
    }

    @DexIgnore
    public static /* synthetic */ GFitWOCalorie copy$default(GFitWOCalorie gFitWOCalorie, float f, long j, long j2, int i, Object obj) {
        if ((i & 1) != 0) {
            f = gFitWOCalorie.calorie;
        }
        if ((i & 2) != 0) {
            j = gFitWOCalorie.startTime;
        }
        if ((i & 4) != 0) {
            j2 = gFitWOCalorie.endTime;
        }
        return gFitWOCalorie.copy(f, j, j2);
    }

    @DexIgnore
    public final float component1() {
        return this.calorie;
    }

    @DexIgnore
    public final long component2() {
        return this.startTime;
    }

    @DexIgnore
    public final long component3() {
        return this.endTime;
    }

    @DexIgnore
    public final GFitWOCalorie copy(float f, long j, long j2) {
        return new GFitWOCalorie(f, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GFitWOCalorie)) {
            return false;
        }
        GFitWOCalorie gFitWOCalorie = (GFitWOCalorie) obj;
        return Float.compare(this.calorie, gFitWOCalorie.calorie) == 0 && this.startTime == gFitWOCalorie.startTime && this.endTime == gFitWOCalorie.endTime;
    }

    @DexIgnore
    public final float getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public final long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public int hashCode() {
        return (((Float.floatToIntBits(this.calorie) * 31) + c.a(this.startTime)) * 31) + c.a(this.endTime);
    }

    @DexIgnore
    public final void setCalorie(float f) {
        this.calorie = f;
    }

    @DexIgnore
    public final void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public final void setStartTime(long j) {
        this.startTime = j;
    }

    @DexIgnore
    public String toString() {
        return "GFitWOCalorie(calorie=" + this.calorie + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ")";
    }
}
