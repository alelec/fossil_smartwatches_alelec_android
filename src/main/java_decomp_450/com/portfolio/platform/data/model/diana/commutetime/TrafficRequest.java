package com.portfolio.platform.data.model.diana.commutetime;

import com.facebook.share.internal.ShareConstants;
import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.w97;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TrafficRequest {
    @DexIgnore
    @te4("avoid")
    public List<String> avoid;
    @DexIgnore
    @te4(ShareConstants.DESTINATION)
    public Address destination;
    @DexIgnore
    @te4("mode")
    public String mode; // = "driving";
    @DexIgnore
    @te4("origin")
    public Address origin;

    @DexIgnore
    public TrafficRequest(boolean z, Address address, Address address2) {
        ee7.b(address, ShareConstants.DESTINATION);
        ee7.b(address2, "origin");
        this.destination = address;
        this.origin = address2;
        this.avoid = z ? w97.a((Object[]) new String[]{"tolls"}) : null;
        this.mode = "driving";
    }

    @DexIgnore
    public final List<String> getAvoid() {
        return this.avoid;
    }

    @DexIgnore
    public final Address getDestination() {
        return this.destination;
    }

    @DexIgnore
    public final String getMode() {
        return this.mode;
    }

    @DexIgnore
    public final Address getOrigin() {
        return this.origin;
    }

    @DexIgnore
    public final void setAvoid(List<String> list) {
        this.avoid = list;
    }

    @DexIgnore
    public final void setDestination(Address address) {
        ee7.b(address, "<set-?>");
        this.destination = address;
    }

    @DexIgnore
    public final void setMode(String str) {
        ee7.b(str, "<set-?>");
        this.mode = str;
    }

    @DexIgnore
    public final void setOrigin(Address address) {
        ee7.b(address, "<set-?>");
        this.origin = address;
    }
}
