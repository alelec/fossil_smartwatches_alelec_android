package com.portfolio.platform.data.model.room;

import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface UserDao {
    @DexIgnore
    void clearAllUser();

    @DexIgnore
    void deleteUser(MFUser mFUser);

    @DexIgnore
    MFUser getCurrentUser();

    @DexIgnore
    void insertUser(MFUser mFUser);

    @DexIgnore
    void updateUser(MFUser mFUser);
}
