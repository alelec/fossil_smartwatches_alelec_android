package com.portfolio.platform.data.model.diana.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.nd5;
import com.fossil.te4;
import com.fossil.zd7;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherLocationWrapper implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @te4("fullName")
    public String fullName;
    @DexIgnore
    @te4("id")
    public String id;
    @DexIgnore
    @nd5
    public boolean isEnableLocation;
    @DexIgnore
    @nd5
    public boolean isUseCurrentLocation;
    @DexIgnore
    @te4(Constants.LAT)
    public double lat;
    @DexIgnore
    @te4("lng")
    public double lng;
    @DexIgnore
    @te4("name")
    public String name;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherLocationWrapper> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherLocationWrapper createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new WeatherLocationWrapper(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherLocationWrapper[] newArray(int i) {
            return new WeatherLocationWrapper[i];
        }
    }

    @DexIgnore
    public WeatherLocationWrapper() {
        this(null, 0.0d, 0.0d, null, null, false, false, 127, null);
    }

    @DexIgnore
    public WeatherLocationWrapper(String str, double d, double d2, String str2, String str3, boolean z, boolean z2) {
        ee7.b(str, "id");
        ee7.b(str2, "name");
        ee7.b(str3, "fullName");
        this.id = str;
        this.lat = d;
        this.lng = d2;
        this.name = str2;
        this.fullName = str3;
        this.isUseCurrentLocation = z;
        this.isEnableLocation = z2;
    }

    @DexIgnore
    public static /* synthetic */ WeatherLocationWrapper copy$default(WeatherLocationWrapper weatherLocationWrapper, String str, double d, double d2, String str2, String str3, boolean z, boolean z2, int i, Object obj) {
        return weatherLocationWrapper.copy((i & 1) != 0 ? weatherLocationWrapper.id : str, (i & 2) != 0 ? weatherLocationWrapper.lat : d, (i & 4) != 0 ? weatherLocationWrapper.lng : d2, (i & 8) != 0 ? weatherLocationWrapper.name : str2, (i & 16) != 0 ? weatherLocationWrapper.fullName : str3, (i & 32) != 0 ? weatherLocationWrapper.isUseCurrentLocation : z, (i & 64) != 0 ? weatherLocationWrapper.isEnableLocation : z2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final double component2() {
        return this.lat;
    }

    @DexIgnore
    public final double component3() {
        return this.lng;
    }

    @DexIgnore
    public final String component4() {
        return this.name;
    }

    @DexIgnore
    public final String component5() {
        return this.fullName;
    }

    @DexIgnore
    public final boolean component6() {
        return this.isUseCurrentLocation;
    }

    @DexIgnore
    public final boolean component7() {
        return this.isEnableLocation;
    }

    @DexIgnore
    public final WeatherLocationWrapper copy(String str, double d, double d2, String str2, String str3, boolean z, boolean z2) {
        ee7.b(str, "id");
        ee7.b(str2, "name");
        ee7.b(str3, "fullName");
        return new WeatherLocationWrapper(str, d, d2, str2, str3, z, z2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WeatherLocationWrapper)) {
            return false;
        }
        WeatherLocationWrapper weatherLocationWrapper = (WeatherLocationWrapper) obj;
        return ee7.a(this.id, weatherLocationWrapper.id) && Double.compare(this.lat, weatherLocationWrapper.lat) == 0 && Double.compare(this.lng, weatherLocationWrapper.lng) == 0 && ee7.a(this.name, weatherLocationWrapper.name) && ee7.a(this.fullName, weatherLocationWrapper.fullName) && this.isUseCurrentLocation == weatherLocationWrapper.isUseCurrentLocation && this.isEnableLocation == weatherLocationWrapper.isEnableLocation;
    }

    @DexIgnore
    public final String getFullName() {
        return this.fullName;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final double getLat() {
        return this.lat;
    }

    @DexIgnore
    public final double getLng() {
        return this.lng;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v7, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r1v4, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r2v8, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r2v9, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (((((str != null ? str.hashCode() : 0) * 31) + Double.doubleToLongBits(this.lat)) * 31) + Double.doubleToLongBits(this.lng)) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.fullName;
        if (str3 != null) {
            i = str3.hashCode();
        }
        int i2 = (hashCode2 + i) * 31;
        boolean z = this.isUseCurrentLocation;
        int i3 = 1;
        if (z) {
            z = true;
        }
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = (i2 + i4) * 31;
        boolean z2 = this.isEnableLocation;
        if (z2 == 0) {
            i3 = z2;
        }
        return i6 + i3;
    }

    @DexIgnore
    public final boolean isEnableLocation() {
        return this.isEnableLocation;
    }

    @DexIgnore
    public final boolean isUseCurrentLocation() {
        return this.isUseCurrentLocation;
    }

    @DexIgnore
    public final void setEnableLocation(boolean z) {
        this.isEnableLocation = z;
    }

    @DexIgnore
    public final void setFullName(String str) {
        ee7.b(str, "<set-?>");
        this.fullName = str;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setLat(double d) {
        this.lat = d;
    }

    @DexIgnore
    public final void setLng(double d) {
        this.lng = d;
    }

    @DexIgnore
    public final void setName(String str) {
        ee7.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setUseCurrentLocation(boolean z) {
        this.isUseCurrentLocation = z;
    }

    @DexIgnore
    public String toString() {
        return "WeatherLocationWrapper(id=" + this.id + ", lat=" + this.lat + ", lng=" + this.lng + ", name=" + this.name + ", fullName=" + this.fullName + ", isUseCurrentLocation=" + this.isUseCurrentLocation + ", isEnableLocation=" + this.isEnableLocation + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeDouble(this.lat);
        parcel.writeDouble(this.lng);
        parcel.writeString(this.name);
        parcel.writeString(this.fullName);
        parcel.writeString(String.valueOf(this.isUseCurrentLocation));
        parcel.writeString(String.valueOf(this.isEnableLocation));
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ WeatherLocationWrapper(java.lang.String r10, double r11, double r13, java.lang.String r15, java.lang.String r16, boolean r17, boolean r18, int r19, com.fossil.zd7 r20) {
        /*
            r9 = this;
            r0 = r19 & 1
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x0008
            r0 = r1
            goto L_0x0009
        L_0x0008:
            r0 = r10
        L_0x0009:
            r2 = r19 & 2
            r3 = 0
            if (r2 == 0) goto L_0x0011
            r5 = r3
            goto L_0x0012
        L_0x0011:
            r5 = r11
        L_0x0012:
            r2 = r19 & 4
            if (r2 == 0) goto L_0x0017
            goto L_0x0018
        L_0x0017:
            r3 = r13
        L_0x0018:
            r2 = r19 & 8
            if (r2 == 0) goto L_0x001e
            r2 = r1
            goto L_0x001f
        L_0x001e:
            r2 = r15
        L_0x001f:
            r7 = r19 & 16
            if (r7 == 0) goto L_0x0024
            goto L_0x0026
        L_0x0024:
            r1 = r16
        L_0x0026:
            r7 = r19 & 32
            if (r7 == 0) goto L_0x002c
            r7 = 0
            goto L_0x002e
        L_0x002c:
            r7 = r17
        L_0x002e:
            r8 = r19 & 64
            if (r8 == 0) goto L_0x0034
            r8 = 1
            goto L_0x0036
        L_0x0034:
            r8 = r18
        L_0x0036:
            r10 = r9
            r11 = r0
            r12 = r5
            r14 = r3
            r16 = r2
            r17 = r1
            r18 = r7
            r19 = r8
            r10.<init>(r11, r12, r14, r16, r17, r18, r19)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper.<init>(java.lang.String, double, double, java.lang.String, java.lang.String, boolean, boolean, int, com.fossil.zd7):void");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public WeatherLocationWrapper(android.os.Parcel r13) {
        /*
            r12 = this;
            java.lang.String r0 = "in"
            com.fossil.ee7.b(r13, r0)
            java.lang.String r0 = r13.readString()
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x000f
            r3 = r0
            goto L_0x0010
        L_0x000f:
            r3 = r1
        L_0x0010:
            double r4 = r13.readDouble()
            double r6 = r13.readDouble()
            java.lang.String r0 = r13.readString()
            if (r0 == 0) goto L_0x0020
            r8 = r0
            goto L_0x0021
        L_0x0020:
            r8 = r1
        L_0x0021:
            java.lang.String r0 = r13.readString()
            if (r0 == 0) goto L_0x0029
            r9 = r0
            goto L_0x002a
        L_0x0029:
            r9 = r1
        L_0x002a:
            java.lang.String r0 = r13.readString()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            java.lang.String r1 = "java.lang.Boolean.valueOf(`in`.readString())"
            com.fossil.ee7.a(r0, r1)
            boolean r10 = r0.booleanValue()
            java.lang.String r13 = r13.readString()
            java.lang.Boolean r13 = java.lang.Boolean.valueOf(r13)
            com.fossil.ee7.a(r13, r1)
            boolean r11 = r13.booleanValue()
            r2 = r12
            r2.<init>(r3, r4, r6, r8, r9, r10, r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper.<init>(android.os.Parcel):void");
    }
}
