package com.portfolio.platform.data.model.diana;

import com.fossil.ee7;
import com.fossil.re4;
import com.fossil.te4;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppData {
    @DexIgnore
    @re4
    @te4("createdAt")
    public DateTime createdAt;
    @DexIgnore
    @re4
    @te4("executableBinaryDataUrl")
    public String executableBinaryDataUrl;
    @DexIgnore
    @re4
    @te4("id")
    public String id;
    @DexIgnore
    @re4
    @te4("maxAppVersion")
    public String maxAppVersion;
    @DexIgnore
    @re4
    @te4("maxFirmwareOSVersion")
    public String maxFirmwareOSVersion;
    @DexIgnore
    @re4
    @te4("minAppVersion")
    public String minAppVersion;
    @DexIgnore
    @re4
    @te4("minFirmwareOSVersion")
    public String minFirmwareOSVersion;
    @DexIgnore
    @re4
    @te4("type")
    public String type;
    @DexIgnore
    @re4
    @te4("updatedAt")
    public DateTime updatedAt;
    @DexIgnore
    @re4
    @te4("version")
    public String version;

    @DexIgnore
    public WatchAppData(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, DateTime dateTime, DateTime dateTime2) {
        ee7.b(str, "id");
        ee7.b(str2, "type");
        ee7.b(str3, "maxAppVersion");
        ee7.b(str4, "maxFirmwareOSVersion");
        ee7.b(str5, "minAppVersion");
        ee7.b(str6, "minFirmwareOSVersion");
        ee7.b(str7, "version");
        ee7.b(str8, "executableBinaryDataUrl");
        ee7.b(dateTime, "updatedAt");
        ee7.b(dateTime2, "createdAt");
        this.id = str;
        this.type = str2;
        this.maxAppVersion = str3;
        this.maxFirmwareOSVersion = str4;
        this.minAppVersion = str5;
        this.minFirmwareOSVersion = str6;
        this.version = str7;
        this.executableBinaryDataUrl = str8;
        this.updatedAt = dateTime;
        this.createdAt = dateTime2;
    }

    @DexIgnore
    public static /* synthetic */ WatchAppData copy$default(WatchAppData watchAppData, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, DateTime dateTime, DateTime dateTime2, int i, Object obj) {
        return watchAppData.copy((i & 1) != 0 ? watchAppData.id : str, (i & 2) != 0 ? watchAppData.type : str2, (i & 4) != 0 ? watchAppData.maxAppVersion : str3, (i & 8) != 0 ? watchAppData.maxFirmwareOSVersion : str4, (i & 16) != 0 ? watchAppData.minAppVersion : str5, (i & 32) != 0 ? watchAppData.minFirmwareOSVersion : str6, (i & 64) != 0 ? watchAppData.version : str7, (i & 128) != 0 ? watchAppData.executableBinaryDataUrl : str8, (i & 256) != 0 ? watchAppData.updatedAt : dateTime, (i & 512) != 0 ? watchAppData.createdAt : dateTime2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final DateTime component10() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component2() {
        return this.type;
    }

    @DexIgnore
    public final String component3() {
        return this.maxAppVersion;
    }

    @DexIgnore
    public final String component4() {
        return this.maxFirmwareOSVersion;
    }

    @DexIgnore
    public final String component5() {
        return this.minAppVersion;
    }

    @DexIgnore
    public final String component6() {
        return this.minFirmwareOSVersion;
    }

    @DexIgnore
    public final String component7() {
        return this.version;
    }

    @DexIgnore
    public final String component8() {
        return this.executableBinaryDataUrl;
    }

    @DexIgnore
    public final DateTime component9() {
        return this.updatedAt;
    }

    @DexIgnore
    public final WatchAppData copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, DateTime dateTime, DateTime dateTime2) {
        ee7.b(str, "id");
        ee7.b(str2, "type");
        ee7.b(str3, "maxAppVersion");
        ee7.b(str4, "maxFirmwareOSVersion");
        ee7.b(str5, "minAppVersion");
        ee7.b(str6, "minFirmwareOSVersion");
        ee7.b(str7, "version");
        ee7.b(str8, "executableBinaryDataUrl");
        ee7.b(dateTime, "updatedAt");
        ee7.b(dateTime2, "createdAt");
        return new WatchAppData(str, str2, str3, str4, str5, str6, str7, str8, dateTime, dateTime2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WatchAppData)) {
            return false;
        }
        WatchAppData watchAppData = (WatchAppData) obj;
        return ee7.a(this.id, watchAppData.id) && ee7.a(this.type, watchAppData.type) && ee7.a(this.maxAppVersion, watchAppData.maxAppVersion) && ee7.a(this.maxFirmwareOSVersion, watchAppData.maxFirmwareOSVersion) && ee7.a(this.minAppVersion, watchAppData.minAppVersion) && ee7.a(this.minFirmwareOSVersion, watchAppData.minFirmwareOSVersion) && ee7.a(this.version, watchAppData.version) && ee7.a(this.executableBinaryDataUrl, watchAppData.executableBinaryDataUrl) && ee7.a(this.updatedAt, watchAppData.updatedAt) && ee7.a(this.createdAt, watchAppData.createdAt);
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getExecutableBinaryDataUrl() {
        return this.executableBinaryDataUrl;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getMaxAppVersion() {
        return this.maxAppVersion;
    }

    @DexIgnore
    public final String getMaxFirmwareOSVersion() {
        return this.maxFirmwareOSVersion;
    }

    @DexIgnore
    public final String getMinAppVersion() {
        return this.minAppVersion;
    }

    @DexIgnore
    public final String getMinFirmwareOSVersion() {
        return this.minFirmwareOSVersion;
    }

    @DexIgnore
    public final String getType() {
        return this.type;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getVersion() {
        return this.version;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.type;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.maxAppVersion;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.maxFirmwareOSVersion;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.minAppVersion;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.minFirmwareOSVersion;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.version;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.executableBinaryDataUrl;
        int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
        DateTime dateTime = this.updatedAt;
        int hashCode9 = (hashCode8 + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.createdAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return hashCode9 + i;
    }

    @DexIgnore
    public final void setCreatedAt(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.createdAt = dateTime;
    }

    @DexIgnore
    public final void setExecutableBinaryDataUrl(String str) {
        ee7.b(str, "<set-?>");
        this.executableBinaryDataUrl = str;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMaxAppVersion(String str) {
        ee7.b(str, "<set-?>");
        this.maxAppVersion = str;
    }

    @DexIgnore
    public final void setMaxFirmwareOSVersion(String str) {
        ee7.b(str, "<set-?>");
        this.maxFirmwareOSVersion = str;
    }

    @DexIgnore
    public final void setMinAppVersion(String str) {
        ee7.b(str, "<set-?>");
        this.minAppVersion = str;
    }

    @DexIgnore
    public final void setMinFirmwareOSVersion(String str) {
        ee7.b(str, "<set-?>");
        this.minFirmwareOSVersion = str;
    }

    @DexIgnore
    public final void setType(String str) {
        ee7.b(str, "<set-?>");
        this.type = str;
    }

    @DexIgnore
    public final void setUpdatedAt(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.updatedAt = dateTime;
    }

    @DexIgnore
    public final void setVersion(String str) {
        ee7.b(str, "<set-?>");
        this.version = str;
    }

    @DexIgnore
    public String toString() {
        return "WatchAppData(id=" + this.id + ", type=" + this.type + ", maxAppVersion=" + this.maxAppVersion + ", maxFirmwareOSVersion=" + this.maxFirmwareOSVersion + ", minAppVersion=" + this.minAppVersion + ", minFirmwareOSVersion=" + this.minFirmwareOSVersion + ", version=" + this.version + ", executableBinaryDataUrl=" + this.executableBinaryDataUrl + ", updatedAt=" + this.updatedAt + ", createdAt=" + this.createdAt + ")";
    }
}
