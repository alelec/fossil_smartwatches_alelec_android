package com.portfolio.platform.data.model;

import com.fossil.ee7;
import com.misfit.frameworks.buttonservice.model.FileType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocalFile {
    @DexIgnore
    public String checksum;
    @DexIgnore
    public String fileName;
    @DexIgnore
    public String localUri;
    @DexIgnore
    public int pinType; // = 1;
    @DexIgnore
    public String remoteUrl;
    @DexIgnore
    public FileType type; // = FileType.UNKNOWN;

    @DexIgnore
    public LocalFile(String str, String str2, String str3, String str4) {
        ee7.b(str, "fileName");
        ee7.b(str2, "localUri");
        ee7.b(str3, "remoteUrl");
        this.fileName = str;
        this.localUri = str2;
        this.remoteUrl = str3;
        this.checksum = str4;
    }

    @DexIgnore
    public static /* synthetic */ LocalFile copy$default(LocalFile localFile, String str, String str2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = localFile.fileName;
        }
        if ((i & 2) != 0) {
            str2 = localFile.localUri;
        }
        if ((i & 4) != 0) {
            str3 = localFile.remoteUrl;
        }
        if ((i & 8) != 0) {
            str4 = localFile.checksum;
        }
        return localFile.copy(str, str2, str3, str4);
    }

    @DexIgnore
    public final String component1() {
        return this.fileName;
    }

    @DexIgnore
    public final String component2() {
        return this.localUri;
    }

    @DexIgnore
    public final String component3() {
        return this.remoteUrl;
    }

    @DexIgnore
    public final String component4() {
        return this.checksum;
    }

    @DexIgnore
    public final LocalFile copy(String str, String str2, String str3, String str4) {
        ee7.b(str, "fileName");
        ee7.b(str2, "localUri");
        ee7.b(str3, "remoteUrl");
        return new LocalFile(str, str2, str3, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocalFile)) {
            return false;
        }
        LocalFile localFile = (LocalFile) obj;
        return ee7.a(this.fileName, localFile.fileName) && ee7.a(this.localUri, localFile.localUri) && ee7.a(this.remoteUrl, localFile.remoteUrl) && ee7.a(this.checksum, localFile.checksum);
    }

    @DexIgnore
    public final String getChecksum() {
        return this.checksum;
    }

    @DexIgnore
    public final String getFileName() {
        return this.fileName;
    }

    @DexIgnore
    public final String getLocalUri() {
        return this.localUri;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getRemoteUrl() {
        return this.remoteUrl;
    }

    @DexIgnore
    public final FileType getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.fileName;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.localUri;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.remoteUrl;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.checksum;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public final void setChecksum(String str) {
        this.checksum = str;
    }

    @DexIgnore
    public final void setFileName(String str) {
        ee7.b(str, "<set-?>");
        this.fileName = str;
    }

    @DexIgnore
    public final void setLocalUri(String str) {
        ee7.b(str, "<set-?>");
        this.localUri = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setRemoteUrl(String str) {
        ee7.b(str, "<set-?>");
        this.remoteUrl = str;
    }

    @DexIgnore
    public final void setType(FileType fileType) {
        ee7.b(fileType, "<set-?>");
        this.type = fileType;
    }

    @DexIgnore
    public String toString() {
        return "LocalFile(fileName=" + this.fileName + ", localUri=" + this.localUri + ", remoteUrl=" + this.remoteUrl + ", checksum=" + this.checksum + ")";
    }
}
