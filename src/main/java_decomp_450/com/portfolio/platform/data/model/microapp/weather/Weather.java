package com.portfolio.platform.data.model.microapp.weather;

import com.facebook.places.model.PlaceFields;
import com.fossil.af7;
import com.fossil.ee7;
import com.fossil.rb5;
import com.fossil.te4;
import com.fossil.zd5;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.CurrentWeatherInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherDayForecast;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherHourForecast;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherWatchAppInfo;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Weather {
    @DexIgnore
    public String address; // = "";
    @DexIgnore
    @te4("currently")
    public /* final */ Currently currently;
    @DexIgnore
    @te4("daily")
    public /* final */ ArrayList<Daily> daily;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_EXPIRED_AT)
    public /* final */ DateTime expiredAt;
    @DexIgnore
    @te4("hourly")
    public /* final */ ArrayList<Hourly> hourly;
    @DexIgnore
    @te4(PlaceFields.LOCATION)
    public /* final */ Location location;
    @DexIgnore
    public long updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Currently {
        @DexIgnore
        @te4("forecast")
        public /* final */ String forecast;
        @DexIgnore
        @te4("rainProbability")
        public /* final */ float rainProbability;
        @DexIgnore
        @te4("sunriseTime")
        public /* final */ DateTime sunriseTime;
        @DexIgnore
        @te4("sunsetTime")
        public /* final */ DateTime sunsetTime;
        @DexIgnore
        @te4("temperature")
        public /* final */ Temperature temperature;
        @DexIgnore
        @te4(LogBuilder.KEY_TIME)
        public /* final */ DateTime time;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Currently() {
        }

        @DexIgnore
        public final String getForecast() {
            return this.forecast;
        }

        @DexIgnore
        public final float getRainProbability() {
            return this.rainProbability;
        }

        @DexIgnore
        public final DateTime getSunriseTime() {
            return this.sunriseTime;
        }

        @DexIgnore
        public final DateTime getSunsetTime() {
            return this.sunsetTime;
        }

        @DexIgnore
        public final Temperature getTemperature() {
            return this.temperature;
        }

        @DexIgnore
        public final DateTime getTime() {
            return this.time;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Daily {
        @DexIgnore
        @te4("date")
        public /* final */ Date date;
        @DexIgnore
        @te4("forecast")
        public /* final */ String forecast;
        @DexIgnore
        @te4("rainProbability")
        public /* final */ float rainProbability;
        @DexIgnore
        @te4("sunriseTime")
        public /* final */ DateTime sunriseTime;
        @DexIgnore
        @te4("sunsetTime")
        public /* final */ DateTime sunsetTime;
        @DexIgnore
        @te4("temperature")
        public /* final */ Temperature temperature;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Daily() {
        }

        @DexIgnore
        public final Date getDate() {
            return this.date;
        }

        @DexIgnore
        public final String getForecast() {
            return this.forecast;
        }

        @DexIgnore
        public final float getRainProbability() {
            return this.rainProbability;
        }

        @DexIgnore
        public final DateTime getSunriseTime() {
            return this.sunriseTime;
        }

        @DexIgnore
        public final DateTime getSunsetTime() {
            return this.sunsetTime;
        }

        @DexIgnore
        public final Temperature getTemperature() {
            return this.temperature;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Hourly {
        @DexIgnore
        @te4("forecast")
        public /* final */ String forecast;
        @DexIgnore
        @te4("rainProbability")
        public /* final */ float rainProbability;
        @DexIgnore
        @te4("sunriseTime")
        public /* final */ DateTime sunriseTime;
        @DexIgnore
        @te4("sunsetTime")
        public /* final */ DateTime sunsetTime;
        @DexIgnore
        @te4("temperature")
        public /* final */ Temperature temperature;
        @DexIgnore
        @te4(LogBuilder.KEY_TIME)
        public /* final */ DateTime time;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Hourly() {
        }

        @DexIgnore
        public final String getForecast() {
            return this.forecast;
        }

        @DexIgnore
        public final float getRainProbability() {
            return this.rainProbability;
        }

        @DexIgnore
        public final DateTime getSunriseTime() {
            return this.sunriseTime;
        }

        @DexIgnore
        public final DateTime getSunsetTime() {
            return this.sunsetTime;
        }

        @DexIgnore
        public final Temperature getTemperature() {
            return this.temperature;
        }

        @DexIgnore
        public final DateTime getTime() {
            return this.time;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Latlong {
        @DexIgnore
        @te4(Constants.LAT)
        public /* final */ float lat;
        @DexIgnore
        @te4("lng")
        public /* final */ float lng;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Latlong() {
        }

        @DexIgnore
        public final float getLat() {
            return this.lat;
        }

        @DexIgnore
        public final float getLng() {
            return this.lng;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Location {
        @DexIgnore
        @te4("cachedAt")
        public /* final */ Latlong cachedAt;
        @DexIgnore
        @te4("requestedAt")
        public /* final */ Latlong requestedAt;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Location() {
        }

        @DexIgnore
        public final Latlong getCachedAt() {
            return this.cachedAt;
        }

        @DexIgnore
        public final Latlong getRequestedAt() {
            return this.requestedAt;
        }
    }

    @DexIgnore
    public enum TEMP_UNIT {
        CELSIUS("c"),
        FAHRENHEIT("f");
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        public /* final */ String value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final TEMP_UNIT getTempUnit(String str) {
                ee7.b(str, "value");
                TEMP_UNIT[] values = TEMP_UNIT.values();
                for (TEMP_UNIT temp_unit : values) {
                    if (ee7.a((Object) temp_unit.getValue(), (Object) str)) {
                        return temp_unit;
                    }
                }
                return TEMP_UNIT.CELSIUS;
            }

            @DexIgnore
            public /* synthetic */ Companion(zd7 zd7) {
                this();
            }
        }

        @DexIgnore
        public TEMP_UNIT(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[TEMP_UNIT.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[TEMP_UNIT.CELSIUS.ordinal()] = 1;
        }
        */
    }

    @DexIgnore
    public Weather(Location location2, Currently currently2, DateTime dateTime, ArrayList<Daily> arrayList, ArrayList<Hourly> arrayList2) {
        ee7.b(location2, PlaceFields.LOCATION);
        ee7.b(currently2, "currently");
        ee7.b(dateTime, Constants.PROFILE_KEY_EXPIRED_AT);
        ee7.b(arrayList, "daily");
        ee7.b(arrayList2, "hourly");
        this.location = location2;
        this.currently = currently2;
        this.expiredAt = dateTime;
        this.daily = arrayList;
        this.hourly = arrayList2;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.portfolio.platform.data.model.microapp.weather.Weather */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Weather copy$default(Weather weather, Location location2, Currently currently2, DateTime dateTime, ArrayList arrayList, ArrayList arrayList2, int i, Object obj) {
        if ((i & 1) != 0) {
            location2 = weather.location;
        }
        if ((i & 2) != 0) {
            currently2 = weather.currently;
        }
        if ((i & 4) != 0) {
            dateTime = weather.expiredAt;
        }
        if ((i & 8) != 0) {
            arrayList = weather.daily;
        }
        if ((i & 16) != 0) {
            arrayList2 = weather.hourly;
        }
        return weather.copy(location2, currently2, dateTime, arrayList, arrayList2);
    }

    @DexIgnore
    private final long getExpiredTimeInSecondForWatchApp() {
        Calendar c = zd5.c(Long.valueOf(this.expiredAt.getMillis()));
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "Calendar.getInstance()");
        if (c.compareTo(zd5.c(Long.valueOf(instance.getTimeInMillis()))) <= 0) {
            return this.expiredAt.getMillis() / ((long) 1000);
        }
        Calendar c2 = zd5.c(Long.valueOf(this.expiredAt.getMillis()));
        ee7.a((Object) c2, "calendar");
        return c2.getTimeInMillis() / ((long) 1000);
    }

    @DexIgnore
    private final WeatherDayForecast.WeatherWeekDay getWeatherWeekDay(Date date) {
        switch (zd5.a(Long.valueOf(date.getTime()))) {
            case 1:
                return WeatherDayForecast.WeatherWeekDay.SUNDAY;
            case 2:
                return WeatherDayForecast.WeatherWeekDay.MONDAY;
            case 3:
                return WeatherDayForecast.WeatherWeekDay.TUESDAY;
            case 4:
                return WeatherDayForecast.WeatherWeekDay.WEDNESDAY;
            case 5:
                return WeatherDayForecast.WeatherWeekDay.THURSDAY;
            case 6:
                return WeatherDayForecast.WeatherWeekDay.FRIDAY;
            default:
                return WeatherDayForecast.WeatherWeekDay.SATURDAY;
        }
    }

    @DexIgnore
    public final Location component1() {
        return this.location;
    }

    @DexIgnore
    public final Currently component2() {
        return this.currently;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.expiredAt;
    }

    @DexIgnore
    public final ArrayList<Daily> component4() {
        return this.daily;
    }

    @DexIgnore
    public final ArrayList<Hourly> component5() {
        return this.hourly;
    }

    @DexIgnore
    public final Weather copy(Location location2, Currently currently2, DateTime dateTime, ArrayList<Daily> arrayList, ArrayList<Hourly> arrayList2) {
        ee7.b(location2, PlaceFields.LOCATION);
        ee7.b(currently2, "currently");
        ee7.b(dateTime, Constants.PROFILE_KEY_EXPIRED_AT);
        ee7.b(arrayList, "daily");
        ee7.b(arrayList2, "hourly");
        return new Weather(location2, currently2, dateTime, arrayList, arrayList2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Weather)) {
            return false;
        }
        Weather weather = (Weather) obj;
        return ee7.a(this.location, weather.location) && ee7.a(this.currently, weather.currently) && ee7.a(this.expiredAt, weather.expiredAt) && ee7.a(this.daily, weather.daily) && ee7.a(this.hourly, weather.hourly);
    }

    @DexIgnore
    public final String getAddress() {
        return this.address;
    }

    @DexIgnore
    public final Currently getCurrently() {
        return this.currently;
    }

    @DexIgnore
    public final ArrayList<Daily> getDaily() {
        return this.daily;
    }

    @DexIgnore
    public final DateTime getExpiredAt() {
        return this.expiredAt;
    }

    @DexIgnore
    public final ArrayList<Hourly> getHourly() {
        return this.hourly;
    }

    @DexIgnore
    public final Location getLocation() {
        return this.location;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        Location location2 = this.location;
        int i = 0;
        int hashCode = (location2 != null ? location2.hashCode() : 0) * 31;
        Currently currently2 = this.currently;
        int hashCode2 = (hashCode + (currently2 != null ? currently2.hashCode() : 0)) * 31;
        DateTime dateTime = this.expiredAt;
        int hashCode3 = (hashCode2 + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        ArrayList<Daily> arrayList = this.daily;
        int hashCode4 = (hashCode3 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<Hourly> arrayList2 = this.hourly;
        if (arrayList2 != null) {
            i = arrayList2.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public final void setAddress(String str) {
        ee7.b(str, "<set-?>");
        this.address = str;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public final ChanceOfRainComplicationAppInfo toChanceOfRainComplicationAppInfo() {
        return new ChanceOfRainComplicationAppInfo(af7.a(this.currently.getRainProbability() * ((float) 100)), this.expiredAt.getMillis() / ((long) 1000));
    }

    @DexIgnore
    public String toString() {
        return "Weather(location=" + this.location + ", currently=" + this.currently + ", expiredAt=" + this.expiredAt + ", daily=" + this.daily + ", hourly=" + this.hourly + ")";
    }

    @DexIgnore
    public final WeatherComplicationAppInfo toWeatherComplicationAppInfo() {
        WeatherComplicationAppInfo.TemperatureUnit temperatureUnit;
        WeatherComplicationAppInfo.WeatherCondition a = rb5.Companion.a(this.currently.getForecast());
        Temperature temperature = this.currently.getTemperature();
        if (temperature != null) {
            if (ee7.a((Object) temperature.getUnit(), (Object) TEMP_UNIT.FAHRENHEIT.getValue())) {
                temperatureUnit = WeatherComplicationAppInfo.TemperatureUnit.F;
            } else {
                temperatureUnit = WeatherComplicationAppInfo.TemperatureUnit.C;
            }
            return new WeatherComplicationAppInfo(this.currently.getTemperature().getCurrently(), temperatureUnit, a, this.expiredAt.getMillis() / ((long) 1000));
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final WeatherWatchAppInfo toWeatherWatchAppInfo() {
        UserDisplayUnit.TemperatureUnit temperatureUnit;
        int a = af7.a(this.currently.getRainProbability() * ((float) 100));
        Temperature temperature = this.currently.getTemperature();
        if (temperature != null) {
            CurrentWeatherInfo currentWeatherInfo = new CurrentWeatherInfo(a, temperature.getCurrently(), rb5.Companion.a(this.currently.getForecast()), this.currently.getTemperature().getMax(), this.currently.getTemperature().getMin());
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            Iterator<Daily> it = this.daily.iterator();
            while (it.hasNext()) {
                Daily next = it.next();
                Temperature temperature2 = next.getTemperature();
                if (temperature2 != null) {
                    float max = temperature2.getMax();
                    float min = next.getTemperature().getMin();
                    WeatherComplicationAppInfo.WeatherCondition a2 = rb5.Companion.a(next.getForecast());
                    Date date = next.getDate();
                    if (date != null) {
                        arrayList.add(new WeatherDayForecast(max, min, a2, getWeatherWeekDay(date)));
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "Calendar.getInstance()");
            int b = zd5.b(Long.valueOf(instance.getTimeInMillis()));
            Iterator<Hourly> it2 = this.hourly.iterator();
            while (it2.hasNext()) {
                Hourly next2 = it2.next();
                DateTime time = next2.getTime();
                if (time != null) {
                    int b2 = zd5.b(Long.valueOf(time.getMillis()));
                    if ((((b2 - b) + 24) - 1) % 4 == 0) {
                        Temperature temperature3 = next2.getTemperature();
                        if (temperature3 != null) {
                            arrayList2.add(new WeatherHourForecast(b2, temperature3.getCurrently(), rb5.Companion.a(next2.getForecast())));
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            TEMP_UNIT.Companion companion = TEMP_UNIT.Companion;
            String unit = this.currently.getTemperature().getUnit();
            ee7.a((Object) unit, "currently.temperature.unit");
            if (WhenMappings.$EnumSwitchMapping$0[companion.getTempUnit(unit).ordinal()] != 1) {
                temperatureUnit = UserDisplayUnit.TemperatureUnit.F;
            } else {
                temperatureUnit = UserDisplayUnit.TemperatureUnit.C;
            }
            return new WeatherWatchAppInfo(this.address, temperatureUnit, currentWeatherInfo, arrayList, arrayList2, getExpiredTimeInSecondForWatchApp());
        }
        ee7.a();
        throw null;
    }
}
