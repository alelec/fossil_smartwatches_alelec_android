package com.portfolio.platform.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SecondTimezoneRaw implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @te4("cityCode")
    public String cityCode;
    @DexIgnore
    @te4("cityName")
    public String cityName;
    @DexIgnore
    @te4("countryName")
    public String countryName;
    @DexIgnore
    @te4("timeZoneId")
    public String timeZoneId;
    @DexIgnore
    @te4("timeZoneName")
    public String timeZoneName;
    @DexIgnore
    @te4("timeZoneOffset")
    public int timezoneOffset;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SecondTimezoneRaw> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SecondTimezoneRaw createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new SecondTimezoneRaw(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SecondTimezoneRaw[] newArray(int i) {
            return new SecondTimezoneRaw[i];
        }
    }

    @DexIgnore
    public SecondTimezoneRaw() {
        this(null, null, null, null, 0, null, 63, null);
    }

    @DexIgnore
    public SecondTimezoneRaw(String str, String str2, String str3, String str4, int i, String str5) {
        ee7.b(str, "cityName");
        ee7.b(str2, "countryName");
        ee7.b(str3, "timeZoneName");
        ee7.b(str4, "timeZoneId");
        ee7.b(str5, "cityCode");
        this.cityName = str;
        this.countryName = str2;
        this.timeZoneName = str3;
        this.timeZoneId = str4;
        this.timezoneOffset = i;
        this.cityCode = str5;
    }

    @DexIgnore
    public static /* synthetic */ SecondTimezoneRaw copy$default(SecondTimezoneRaw secondTimezoneRaw, String str, String str2, String str3, String str4, int i, String str5, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = secondTimezoneRaw.cityName;
        }
        if ((i2 & 2) != 0) {
            str2 = secondTimezoneRaw.countryName;
        }
        if ((i2 & 4) != 0) {
            str3 = secondTimezoneRaw.timeZoneName;
        }
        if ((i2 & 8) != 0) {
            str4 = secondTimezoneRaw.timeZoneId;
        }
        if ((i2 & 16) != 0) {
            i = secondTimezoneRaw.timezoneOffset;
        }
        if ((i2 & 32) != 0) {
            str5 = secondTimezoneRaw.cityCode;
        }
        return secondTimezoneRaw.copy(str, str2, str3, str4, i, str5);
    }

    @DexIgnore
    public final String component1() {
        return this.cityName;
    }

    @DexIgnore
    public final String component2() {
        return this.countryName;
    }

    @DexIgnore
    public final String component3() {
        return this.timeZoneName;
    }

    @DexIgnore
    public final String component4() {
        return this.timeZoneId;
    }

    @DexIgnore
    public final int component5() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public final String component6() {
        return this.cityCode;
    }

    @DexIgnore
    public final SecondTimezoneRaw copy(String str, String str2, String str3, String str4, int i, String str5) {
        ee7.b(str, "cityName");
        ee7.b(str2, "countryName");
        ee7.b(str3, "timeZoneName");
        ee7.b(str4, "timeZoneId");
        ee7.b(str5, "cityCode");
        return new SecondTimezoneRaw(str, str2, str3, str4, i, str5);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SecondTimezoneRaw)) {
            return false;
        }
        SecondTimezoneRaw secondTimezoneRaw = (SecondTimezoneRaw) obj;
        return ee7.a(this.cityName, secondTimezoneRaw.cityName) && ee7.a(this.countryName, secondTimezoneRaw.countryName) && ee7.a(this.timeZoneName, secondTimezoneRaw.timeZoneName) && ee7.a(this.timeZoneId, secondTimezoneRaw.timeZoneId) && this.timezoneOffset == secondTimezoneRaw.timezoneOffset && ee7.a(this.cityCode, secondTimezoneRaw.cityCode);
    }

    @DexIgnore
    public final String getCityCode() {
        return this.cityCode;
    }

    @DexIgnore
    public final String getCityName() {
        return this.cityName;
    }

    @DexIgnore
    public final String getCountryName() {
        return this.countryName;
    }

    @DexIgnore
    public final String getTimeZoneId() {
        return this.timeZoneId;
    }

    @DexIgnore
    public final String getTimeZoneName() {
        return this.timeZoneName;
    }

    @DexIgnore
    public final int getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.cityName;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.countryName;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.timeZoneName;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.timeZoneId;
        int hashCode4 = (((hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31) + this.timezoneOffset) * 31;
        String str5 = this.cityCode;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public final void setCityCode(String str) {
        ee7.b(str, "<set-?>");
        this.cityCode = str;
    }

    @DexIgnore
    public final void setCityName(String str) {
        ee7.b(str, "<set-?>");
        this.cityName = str;
    }

    @DexIgnore
    public final void setCountryName(String str) {
        ee7.b(str, "<set-?>");
        this.countryName = str;
    }

    @DexIgnore
    public final void setTimeZoneId(String str) {
        ee7.b(str, "<set-?>");
        this.timeZoneId = str;
    }

    @DexIgnore
    public final void setTimeZoneName(String str) {
        ee7.b(str, "<set-?>");
        this.timeZoneName = str;
    }

    @DexIgnore
    public final void setTimezoneOffset(int i) {
        this.timezoneOffset = i;
    }

    @DexIgnore
    public String toString() {
        return "SecondTimezoneRaw(cityName=" + this.cityName + ", countryName=" + this.countryName + ", timeZoneName=" + this.timeZoneName + ", timeZoneId=" + this.timeZoneId + ", timezoneOffset=" + this.timezoneOffset + ", cityCode=" + this.cityCode + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.cityName);
        parcel.writeString(this.countryName);
        parcel.writeString(this.timeZoneName);
        parcel.writeString(this.timeZoneId);
        parcel.writeInt(this.timezoneOffset);
        parcel.writeString(this.cityCode);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ SecondTimezoneRaw(String str, String str2, String str3, String str4, int i, String str5, int i2, zd7 zd7) {
        this((i2 & 1) != 0 ? "" : str, (i2 & 2) != 0 ? "" : str2, (i2 & 4) != 0 ? "" : str3, (i2 & 8) != 0 ? "" : str4, (i2 & 16) != 0 ? 0 : i, (i2 & 32) != 0 ? "" : str5);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SecondTimezoneRaw(android.os.Parcel r10) {
        /*
            r9 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r10, r0)
            java.lang.String r0 = r10.readString()
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x000f
            r3 = r0
            goto L_0x0010
        L_0x000f:
            r3 = r1
        L_0x0010:
            java.lang.String r0 = r10.readString()
            if (r0 == 0) goto L_0x0018
            r4 = r0
            goto L_0x0019
        L_0x0018:
            r4 = r1
        L_0x0019:
            java.lang.String r0 = r10.readString()
            if (r0 == 0) goto L_0x0021
            r5 = r0
            goto L_0x0022
        L_0x0021:
            r5 = r1
        L_0x0022:
            java.lang.String r0 = r10.readString()
            if (r0 == 0) goto L_0x002a
            r6 = r0
            goto L_0x002b
        L_0x002a:
            r6 = r1
        L_0x002b:
            int r7 = r10.readInt()
            java.lang.String r10 = r10.readString()
            if (r10 == 0) goto L_0x0037
            r8 = r10
            goto L_0x0038
        L_0x0037:
            r8 = r1
        L_0x0038:
            r2 = r9
            r2.<init>(r3, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.SecondTimezoneRaw.<init>(android.os.Parcel):void");
    }
}
