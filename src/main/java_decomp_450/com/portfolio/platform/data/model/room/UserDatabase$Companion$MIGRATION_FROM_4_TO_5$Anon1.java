package com.portfolio.platform.data.model.room;

import com.fossil.li;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserDatabase$Companion$MIGRATION_FROM_4_TO_5$Anon1 extends li {
    @DexIgnore
    public UserDatabase$Companion$MIGRATION_FROM_4_TO_5$Anon1(int i, int i2) {
        super(i, i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:279:0x069f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:280:0x06a1, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:281:0x06a2, code lost:
        r5 = r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:283:0x06a7, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:284:0x06a9, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:285:0x06aa, code lost:
        r2 = r61;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:293:0x06c8, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:294:0x06c9, code lost:
        r16 = "DROP TABLE IF EXISTS user_temp";
        r5 = com.portfolio.platform.data.model.room.UserDatabase.TAG;
        r2 = r61;
        r1 = "DROP TABLE IF EXISTS user";
        r3 = r0;
        r14 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:298:0x06fc, code lost:
        r14.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:304:0x0717, code lost:
        r5.close();
        r2 = com.fossil.i97.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0023, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0024, code lost:
        r1 = r0;
        r5 = null;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:283:0x06a7 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:265:0x0511] */
    /* JADX WARNING: Removed duplicated region for block: B:298:0x06fc  */
    /* JADX WARNING: Removed duplicated region for block: B:304:0x0717  */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0023 A[ExcHandler: all (r0v14 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x001f] */
    @Override // com.fossil.li
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void migrate(com.fossil.wi r61) {
        /*
            r60 = this;
            r1 = r61
            java.lang.String r2 = "DROP TABLE IF EXISTS user"
            java.lang.String r3 = "DROP TABLE IF EXISTS user_temp"
            java.lang.String r4 = "',"
            java.lang.String r5 = "database"
            com.fossil.ee7.b(r1, r5)
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = "UserDatabase"
            java.lang.String r7 = "Migration 4 to 5 Start"
            r5.d(r6, r7)
            r61.beginTransaction()
            java.lang.String r7 = "ALTER TABLE user ADD COLUMN emailVerified INTEGER NOT NULL DEFAULT 0"
            r1.execSQL(r7)     // Catch:{ Exception -> 0x0028, all -> 0x0023 }
            goto L_0x0048
        L_0x0023:
            r0 = move-exception
            r1 = r0
            r5 = 0
            goto L_0x0715
        L_0x0028:
            r0 = move-exception
            r7 = r0
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            r9.<init>()     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            java.lang.String r10 = "Migration 4 to 5 Exception when add column emailVerified- "
            r9.append(r10)     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            java.lang.String r7 = r7.getMessage()     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            r9.append(r7)     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            java.lang.String r7 = r9.toString()     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            r8.e(r6, r7)     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
        L_0x0048:
            r1.execSQL(r3)     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            java.lang.String r7 = "CREATE TABLE user_temp (pinType TEXT NOT NULL DEFAULT '0', isOnboardingComplete INTEGER NOT NULL DEFAULT 0, averageSleep INTEGER NOT NULL DEFAULT 0, averageStep INTEGER NOT NULL DEFAULT 0, createdAt TEXT NOT NULL DEFAULT '2018-03-20T07:14:33.627Z', updatedAt TEXT NOT NULL DEFAULT '2018-03-20T07:14:33.627Z', uid TEXT NOT NULL DEFAULT '' , authType TEXT NOT NULL DEFAULT 'email' ,birthday TEXT NOT NULL DEFAULT '', brand TEXT NOT NULL DEFAULT '', diagnosticEnabled INTEGER NOT NULL DEFAULT 0,email TEXT NOT NULL DEFAULT '' , emailOptIn INTEGER NOT NULL DEFAULT 0, emailVerified INTEGER NOT NULL DEFAULT 0, firstName TEXT NOT NULL DEFAULT '' , gender TEXT NOT NULL DEFAULT 'M', heightInCentimeters INTEGER NOT NULL DEFAULT 170, integrations TEXT NOT NULL DEFAULT '', lastName TEXT NOT NULL DEFAULT '' , profilePicture TEXT NOT NULL DEFAULT '', registerDate TEXT NOT NULL DEFAULT '2015-11-29', registrationComplete INTEGER NOT NULL DEFAULT 0, useDefaultBiometric INTEGER NOT NULL DEFAULT 0,useDefaultGoals INTEGER NOT NULL DEFAULT 0, username TEXT NOT NULL DEFAULT '' , weightInGrams INTEGER NOT NULL DEFAULT 68030, userAccessToken TEXT NOT NULL DEFAULT '', refreshToken TEXT NOT NULL DEFAULT '', accessTokenExpiresAt TEXT NOT NULL DEFAULT '',accessTokenExpiresIn INTEGER NOT NULL DEFAULT 86400, home TEXT NOT NULL DEFAULT '', work TEXT NOT NULL DEFAULT '' , distanceUnit TEXT NOT NULL DEFAULT 'metric', weightUnit TEXT NOT NULL DEFAULT 'metric', heightUnit TEXT NOT NULL DEFAULT 'metric',activeDeviceId TEXT NOT NULL DEFAULT '', temperatureUnit TEXT NOT NULL DEFAULT 'metric', PRIMARY KEY(uid))"
            r1.execSQL(r7)     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            java.util.Date r7 = new java.util.Date     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            r7.<init>()     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            java.lang.String r7 = com.fossil.zd5.y(r7)     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            com.fossil.za5 r8 = com.fossil.za5.EMAIL     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            java.lang.String r8 = r8.getValue()     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            com.fossil.ob5 r9 = com.fossil.ob5.IMPERIAL     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            java.lang.String r9 = r9.getValue()     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            com.fossil.ob5 r10 = com.fossil.ob5.IMPERIAL     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            java.lang.String r10 = r10.getValue()     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            com.fossil.ob5 r11 = com.fossil.ob5.IMPERIAL     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            java.lang.String r11 = r11.getValue()     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            com.fossil.ob5 r12 = com.fossil.ob5.IMPERIAL     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            java.lang.String r12 = r12.getValue()     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            com.fossil.fb5 r13 = com.fossil.fb5.OTHER     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            java.lang.String r13 = r13.getValue()     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            java.lang.String r14 = "SELECT * FROM user"
            android.database.Cursor r14 = r1.query(r14)     // Catch:{ Exception -> 0x06c8, all -> 0x0023 }
            boolean r15 = r14.moveToFirst()     // Catch:{ Exception -> 0x06bb, all -> 0x06b4 }
            r16 = 170(0xaa, float:2.38E-43)
            r17 = 68039(0x109c7, float:9.5343E-41)
            r18 = 8
            r19 = 86400(0x15180, float:1.21072E-40)
            r20 = 0
            java.lang.String r21 = ""
            if (r15 == 0) goto L_0x04ca
            java.lang.String r8 = "cursor"
            com.fossil.ee7.a(r14, r8)     // Catch:{ Exception -> 0x04be }
            java.lang.String r8 = "accessTokenExpiresIn"
            int r8 = r14.getColumnIndex(r8)     // Catch:{ Exception -> 0x04be }
            boolean r9 = r14.isNull(r8)     // Catch:{ Exception -> 0x04be }
            if (r9 == 0) goto L_0x00aa
            r8 = 0
            goto L_0x00b2
        L_0x00aa:
            int r8 = r14.getInt(r8)     // Catch:{ Exception -> 0x04be }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x04be }
        L_0x00b2:
            if (r8 == 0) goto L_0x00ba
            int r8 = r8.intValue()     // Catch:{ Exception -> 0x04be }
            r19 = r8
        L_0x00ba:
            java.lang.String r8 = "useDefaultGoals"
            int r8 = r14.getColumnIndex(r8)     // Catch:{ Exception -> 0x04be }
            boolean r9 = r14.isNull(r8)     // Catch:{ Exception -> 0x04be }
            if (r9 == 0) goto L_0x00c8
            r8 = 0
            goto L_0x00d0
        L_0x00c8:
            int r8 = r14.getInt(r8)     // Catch:{ Exception -> 0x04be }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x04be }
        L_0x00d0:
            if (r8 == 0) goto L_0x00d8
            int r8 = r8.intValue()     // Catch:{ Exception -> 0x04be }
            r18 = r8
        L_0x00d8:
            java.lang.String r8 = "useDefaultBiometric"
            int r8 = r14.getColumnIndex(r8)     // Catch:{ Exception -> 0x04be }
            boolean r9 = r14.isNull(r8)     // Catch:{ Exception -> 0x04be }
            if (r9 == 0) goto L_0x00e6
            r8 = 0
            goto L_0x00ee
        L_0x00e6:
            int r8 = r14.getInt(r8)     // Catch:{ Exception -> 0x04be }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x04be }
        L_0x00ee:
            if (r8 == 0) goto L_0x00f5
            int r8 = r8.intValue()     // Catch:{ Exception -> 0x04be }
            goto L_0x00f6
        L_0x00f5:
            r8 = 0
        L_0x00f6:
            java.lang.String r9 = "registrationComplete"
            int r9 = r14.getColumnIndex(r9)     // Catch:{ Exception -> 0x04be }
            boolean r10 = r14.isNull(r9)     // Catch:{ Exception -> 0x04be }
            if (r10 == 0) goto L_0x0104
            r9 = 0
            goto L_0x010c
        L_0x0104:
            int r9 = r14.getInt(r9)     // Catch:{ Exception -> 0x04be }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x04be }
        L_0x010c:
            if (r9 == 0) goto L_0x0113
            int r9 = r9.intValue()     // Catch:{ Exception -> 0x04be }
            goto L_0x0114
        L_0x0113:
            r9 = 0
        L_0x0114:
            java.lang.String r10 = "diagnosticEnabled"
            int r10 = r14.getColumnIndex(r10)     // Catch:{ Exception -> 0x04be }
            boolean r11 = r14.isNull(r10)     // Catch:{ Exception -> 0x04be }
            if (r11 == 0) goto L_0x0122
            r10 = 0
            goto L_0x012a
        L_0x0122:
            int r10 = r14.getInt(r10)     // Catch:{ Exception -> 0x04be }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x04be }
        L_0x012a:
            if (r10 == 0) goto L_0x0131
            int r10 = r10.intValue()     // Catch:{ Exception -> 0x04be }
            goto L_0x0132
        L_0x0131:
            r10 = 0
        L_0x0132:
            java.lang.String r11 = "averageStep"
            int r11 = r14.getColumnIndex(r11)     // Catch:{ Exception -> 0x04be }
            boolean r12 = r14.isNull(r11)     // Catch:{ Exception -> 0x04be }
            if (r12 == 0) goto L_0x0140
            r11 = 0
            goto L_0x0148
        L_0x0140:
            int r11 = r14.getInt(r11)     // Catch:{ Exception -> 0x04be }
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ Exception -> 0x04be }
        L_0x0148:
            if (r11 == 0) goto L_0x014f
            int r11 = r11.intValue()     // Catch:{ Exception -> 0x04be }
            goto L_0x0150
        L_0x014f:
            r11 = 0
        L_0x0150:
            java.lang.String r12 = "averageSleep"
            int r12 = r14.getColumnIndex(r12)     // Catch:{ Exception -> 0x04be }
            boolean r13 = r14.isNull(r12)     // Catch:{ Exception -> 0x04be }
            if (r13 == 0) goto L_0x015e
            r12 = 0
            goto L_0x0166
        L_0x015e:
            int r12 = r14.getInt(r12)     // Catch:{ Exception -> 0x04be }
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x04be }
        L_0x0166:
            if (r12 == 0) goto L_0x016d
            int r12 = r12.intValue()     // Catch:{ Exception -> 0x04be }
            goto L_0x016e
        L_0x016d:
            r12 = 0
        L_0x016e:
            java.lang.String r13 = "isOnboardingComplete"
            int r13 = r14.getColumnIndex(r13)     // Catch:{ Exception -> 0x04be }
            boolean r15 = r14.isNull(r13)     // Catch:{ Exception -> 0x04be }
            if (r15 == 0) goto L_0x017c
            r13 = 0
            goto L_0x0184
        L_0x017c:
            int r13 = r14.getInt(r13)     // Catch:{ Exception -> 0x04be }
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x04be }
        L_0x0184:
            if (r13 == 0) goto L_0x018b
            int r13 = r13.intValue()     // Catch:{ Exception -> 0x04be }
            goto L_0x018c
        L_0x018b:
            r13 = 0
        L_0x018c:
            java.lang.String r15 = "uid"
            int r15 = r14.getColumnIndex(r15)     // Catch:{ Exception -> 0x04be }
            boolean r22 = r14.isNull(r15)     // Catch:{ Exception -> 0x04be }
            if (r22 == 0) goto L_0x019a
            r15 = 0
            goto L_0x019e
        L_0x019a:
            java.lang.String r15 = r14.getString(r15)     // Catch:{ Exception -> 0x04be }
        L_0x019e:
            if (r15 == 0) goto L_0x01a1
            goto L_0x01a3
        L_0x01a1:
            r15 = r21
        L_0x01a3:
            java.lang.String r5 = "userAccessToken"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r23 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r23 == 0) goto L_0x01b1
            r5 = 0
            goto L_0x01b5
        L_0x01b1:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x01b5:
            if (r5 == 0) goto L_0x01ba
            r23 = r5
            goto L_0x01bc
        L_0x01ba:
            r23 = r21
        L_0x01bc:
            java.lang.String r5 = "refreshToken"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r24 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r24 == 0) goto L_0x01ca
            r5 = 0
            goto L_0x01ce
        L_0x01ca:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x01ce:
            if (r5 == 0) goto L_0x01d3
            r24 = r5
            goto L_0x01d5
        L_0x01d3:
            r24 = r21
        L_0x01d5:
            java.lang.String r5 = "accessTokenExpiresAt"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r25 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r25 == 0) goto L_0x01e3
            r5 = 0
            goto L_0x01e7
        L_0x01e3:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x01e7:
            if (r5 == 0) goto L_0x01ec
            r25 = r5
            goto L_0x01ee
        L_0x01ec:
            r25 = r7
        L_0x01ee:
            java.lang.String r5 = "createdAt"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r26 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r26 == 0) goto L_0x01fc
            r5 = 0
            goto L_0x0200
        L_0x01fc:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x0200:
            if (r5 == 0) goto L_0x0205
            r26 = r5
            goto L_0x0207
        L_0x0205:
            r26 = r7
        L_0x0207:
            java.lang.String r5 = "updatedAt"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r27 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r27 == 0) goto L_0x0215
            r5 = 0
            goto L_0x0219
        L_0x0215:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x0219:
            if (r5 == 0) goto L_0x021e
            r27 = r5
            goto L_0x0220
        L_0x021e:
            r27 = r7
        L_0x0220:
            java.lang.String r5 = "authType"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r28 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r28 == 0) goto L_0x022e
            r5 = 0
            goto L_0x0232
        L_0x022e:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x0232:
            if (r5 == 0) goto L_0x0237
        L_0x0234:
            r28 = r5
            goto L_0x023e
        L_0x0237:
            com.fossil.za5 r5 = com.fossil.za5.EMAIL     // Catch:{ Exception -> 0x04be }
            java.lang.String r5 = r5.getValue()     // Catch:{ Exception -> 0x04be }
            goto L_0x0234
        L_0x023e:
            java.lang.String r5 = "username"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r29 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r29 == 0) goto L_0x024c
            r5 = 0
            goto L_0x0250
        L_0x024c:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x0250:
            if (r5 == 0) goto L_0x0255
            r29 = r5
            goto L_0x0257
        L_0x0255:
            r29 = r21
        L_0x0257:
            java.lang.String r5 = "firstName"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r30 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r30 == 0) goto L_0x0265
            r5 = 0
            goto L_0x0269
        L_0x0265:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x0269:
            if (r5 == 0) goto L_0x026e
            r30 = r5
            goto L_0x0270
        L_0x026e:
            r30 = r21
        L_0x0270:
            java.lang.String r5 = "lastName"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r31 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r31 == 0) goto L_0x027e
            r5 = 0
            goto L_0x0282
        L_0x027e:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x0282:
            if (r5 == 0) goto L_0x0287
            r31 = r5
            goto L_0x0289
        L_0x0287:
            r31 = r21
        L_0x0289:
            java.lang.String r5 = "weightInGrams"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r32 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r32 == 0) goto L_0x0297
            r5 = 0
            goto L_0x029f
        L_0x0297:
            int r5 = r14.getInt(r5)     // Catch:{ Exception -> 0x04be }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x04be }
        L_0x029f:
            if (r5 == 0) goto L_0x02a7
            int r5 = r5.intValue()     // Catch:{ Exception -> 0x04be }
            r17 = r5
        L_0x02a7:
            java.lang.String r5 = "heightInCentimeters"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r32 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r32 == 0) goto L_0x02b5
            r5 = 0
            goto L_0x02bd
        L_0x02b5:
            int r5 = r14.getInt(r5)     // Catch:{ Exception -> 0x04be }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x04be }
        L_0x02bd:
            if (r5 == 0) goto L_0x02c5
            int r5 = r5.intValue()     // Catch:{ Exception -> 0x04be }
            r16 = r5
        L_0x02c5:
            java.lang.String r5 = "heightUnit"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r32 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r32 == 0) goto L_0x02d3
            r5 = 0
            goto L_0x02d7
        L_0x02d3:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x02d7:
            if (r5 == 0) goto L_0x02dc
        L_0x02d9:
            r32 = r5
            goto L_0x02e3
        L_0x02dc:
            com.fossil.ob5 r5 = com.fossil.ob5.IMPERIAL     // Catch:{ Exception -> 0x04be }
            java.lang.String r5 = r5.getValue()     // Catch:{ Exception -> 0x04be }
            goto L_0x02d9
        L_0x02e3:
            java.lang.String r5 = "weightUnit"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r33 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r33 == 0) goto L_0x02f1
            r5 = 0
            goto L_0x02f5
        L_0x02f1:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x02f5:
            if (r5 == 0) goto L_0x02fa
        L_0x02f7:
            r33 = r5
            goto L_0x0301
        L_0x02fa:
            com.fossil.ob5 r5 = com.fossil.ob5.IMPERIAL     // Catch:{ Exception -> 0x04be }
            java.lang.String r5 = r5.getValue()     // Catch:{ Exception -> 0x04be }
            goto L_0x02f7
        L_0x0301:
            java.lang.String r5 = "distanceUnit"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r34 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r34 == 0) goto L_0x030f
            r5 = 0
            goto L_0x0313
        L_0x030f:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x0313:
            if (r5 == 0) goto L_0x0318
        L_0x0315:
            r34 = r5
            goto L_0x031f
        L_0x0318:
            com.fossil.ob5 r5 = com.fossil.ob5.IMPERIAL     // Catch:{ Exception -> 0x04be }
            java.lang.String r5 = r5.getValue()     // Catch:{ Exception -> 0x04be }
            goto L_0x0315
        L_0x031f:
            java.lang.String r5 = "temperatureUnit"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r35 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r35 == 0) goto L_0x032d
            r5 = 0
            goto L_0x0331
        L_0x032d:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x0331:
            if (r5 == 0) goto L_0x0336
        L_0x0333:
            r35 = r5
            goto L_0x033d
        L_0x0336:
            com.fossil.ob5 r5 = com.fossil.ob5.IMPERIAL     // Catch:{ Exception -> 0x04be }
            java.lang.String r5 = r5.getValue()     // Catch:{ Exception -> 0x04be }
            goto L_0x0333
        L_0x033d:
            java.lang.String r5 = "gender"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r36 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r36 == 0) goto L_0x034b
            r5 = 0
            goto L_0x034f
        L_0x034b:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x034f:
            if (r5 == 0) goto L_0x0354
        L_0x0351:
            r36 = r5
            goto L_0x035b
        L_0x0354:
            com.fossil.fb5 r5 = com.fossil.fb5.OTHER     // Catch:{ Exception -> 0x04be }
            java.lang.String r5 = r5.getValue()     // Catch:{ Exception -> 0x04be }
            goto L_0x0351
        L_0x035b:
            java.lang.String r5 = "emailOptIn"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r37 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r37 == 0) goto L_0x0369
            r5 = 0
            goto L_0x0371
        L_0x0369:
            int r5 = r14.getInt(r5)     // Catch:{ Exception -> 0x04be }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x04be }
        L_0x0371:
            if (r5 == 0) goto L_0x037a
            int r5 = r5.intValue()     // Catch:{ Exception -> 0x04be }
            r37 = r5
            goto L_0x037c
        L_0x037a:
            r37 = 0
        L_0x037c:
            java.lang.String r5 = "registerDate"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r38 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r38 == 0) goto L_0x038a
            r5 = 0
            goto L_0x038e
        L_0x038a:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x038e:
            if (r5 == 0) goto L_0x0391
            r7 = r5
        L_0x0391:
            java.lang.String r5 = "email"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r38 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r38 == 0) goto L_0x039f
            r5 = 0
            goto L_0x03a3
        L_0x039f:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x03a3:
            if (r5 == 0) goto L_0x03a8
            r38 = r5
            goto L_0x03aa
        L_0x03a8:
            r38 = r21
        L_0x03aa:
            java.lang.String r5 = "birthday"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r39 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r39 == 0) goto L_0x03b8
            r5 = 0
            goto L_0x03bc
        L_0x03b8:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x03bc:
            if (r5 == 0) goto L_0x03c1
            r39 = r5
            goto L_0x03c3
        L_0x03c1:
            r39 = r21
        L_0x03c3:
            java.lang.String r5 = "profilePicture"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r40 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r40 == 0) goto L_0x03d1
            r5 = 0
            goto L_0x03d5
        L_0x03d1:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x03d5:
            if (r5 == 0) goto L_0x03da
            r40 = r5
            goto L_0x03dc
        L_0x03da:
            r40 = r21
        L_0x03dc:
            java.lang.String r5 = "integrations"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r41 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r41 == 0) goto L_0x03ea
            r5 = 0
            goto L_0x03ee
        L_0x03ea:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x03ee:
            if (r5 == 0) goto L_0x03f3
            r41 = r5
            goto L_0x03f5
        L_0x03f3:
            r41 = r21
        L_0x03f5:
            java.lang.String r5 = "brand"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r42 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r42 == 0) goto L_0x0403
            r5 = 0
            goto L_0x0407
        L_0x0403:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x0407:
            if (r5 == 0) goto L_0x040c
            r42 = r5
            goto L_0x040e
        L_0x040c:
            r42 = r21
        L_0x040e:
            java.lang.String r5 = "home"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r43 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r43 == 0) goto L_0x041c
            r5 = 0
            goto L_0x0420
        L_0x041c:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x0420:
            if (r5 == 0) goto L_0x0425
            r43 = r5
            goto L_0x0427
        L_0x0425:
            r43 = r21
        L_0x0427:
            java.lang.String r5 = "work"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r44 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r44 == 0) goto L_0x0435
            r5 = 0
            goto L_0x0439
        L_0x0435:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x0439:
            if (r5 == 0) goto L_0x043e
            r44 = r5
            goto L_0x0440
        L_0x043e:
            r44 = r21
        L_0x0440:
            java.lang.String r5 = "pinType"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r45 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r45 == 0) goto L_0x044e
            r5 = 0
            goto L_0x0456
        L_0x044e:
            int r5 = r14.getInt(r5)     // Catch:{ Exception -> 0x04be }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x04be }
        L_0x0456:
            if (r5 == 0) goto L_0x045e
            int r5 = r5.intValue()     // Catch:{ Exception -> 0x04be }
            r20 = r5
        L_0x045e:
            java.lang.String r5 = "activeDeviceId"
            int r5 = r14.getColumnIndex(r5)     // Catch:{ Exception -> 0x04be }
            boolean r45 = r14.isNull(r5)     // Catch:{ Exception -> 0x04be }
            if (r45 == 0) goto L_0x046c
            r5 = 0
            goto L_0x0470
        L_0x046c:
            java.lang.String r5 = r14.getString(r5)     // Catch:{ Exception -> 0x04be }
        L_0x0470:
            if (r5 == 0) goto L_0x0474
            r21 = r5
        L_0x0474:
            r1 = r19
            r47 = r20
            r58 = r21
            r5 = r26
            r48 = r29
            r49 = r30
            r50 = r31
            r22 = r35
            r21 = r36
            r46 = r37
            r51 = r38
            r52 = r39
            r53 = r40
            r54 = r41
            r55 = r42
            r56 = r43
            r57 = r44
            r19 = r2
            r20 = r7
            r26 = r16
            r2 = r18
            r7 = r25
            r29 = r27
            r25 = r32
            r16 = r3
            r18 = r6
            r3 = r9
            r27 = r17
            r6 = r23
            r23 = r34
            r9 = r8
            r17 = r14
            r14 = r15
            r15 = r10
            r10 = r24
            r24 = r33
            r59 = r13
            r13 = r11
            r11 = r59
            goto L_0x0511
        L_0x04be:
            r0 = move-exception
            r16 = r3
            r5 = r6
            r3 = r0
            r59 = r2
            r2 = r1
            r1 = r59
            goto L_0x06d3
        L_0x04ca:
            r19 = r2
            r16 = r3
            r18 = r6
            r5 = r7
            r20 = r5
            r29 = r20
            r28 = r8
            r25 = r9
            r24 = r10
            r23 = r11
            r22 = r12
            r17 = r14
            r6 = r21
            r10 = r6
            r14 = r10
            r48 = r14
            r49 = r48
            r50 = r49
            r51 = r50
            r52 = r51
            r53 = r52
            r54 = r53
            r55 = r54
            r56 = r55
            r57 = r56
            r58 = r57
            r1 = 86400(0x15180, float:1.21072E-40)
            r2 = 8
            r3 = 0
            r9 = 0
            r11 = 0
            r12 = 0
            r15 = 0
            r26 = 170(0xaa, float:2.38E-43)
            r27 = 68039(0x109c7, float:9.5343E-41)
            r46 = 0
            r47 = 0
            r21 = r13
            r13 = 0
        L_0x0511:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.<init>()     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r30 = r5
            java.lang.String r5 = "INSERT INTO user_temp(accessTokenExpiresIn,useDefaultGoals,useDefaultBiometric,registrationComplete,diagnosticEnabled,averageStep,averageSleep,isOnboardingComplete,uid, userAccessToken, refreshToken,accessTokenExpiresAt, createdAt, updatedAt, authType,username,firstname,lastname, weightInGrams,heightInCentimeters, heightUnit, weightUnit, distanceUnit,temperatureUnit, gender,emailOptIn,registerDate,email,birthday,profilePicture,integrations,brand,home,work,pinType,activeDeviceId) VALUES("
            r8.append(r5)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r1 = 44
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r9)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r15)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r13)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r12)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r11)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r2 = 39
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r14)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r6)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r10)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r7)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r7 = r30
            r8.append(r7)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r7 = r29
            r8.append(r7)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r28
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r48
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r49
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r50
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r27
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r26
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r9 = r25
            r8.append(r9)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r10 = r24
            r8.append(r10)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r11 = r23
            r8.append(r11)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r12 = r22
            r8.append(r12)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r13 = r21
            r8.append(r13)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r46
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r7 = r20
            r8.append(r7)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r51
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r52
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r53
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r54
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r55
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r56
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r57
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r4)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r3 = r47
            r8.append(r3)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r1 = r58
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r8.append(r2)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            java.lang.String r1 = ")"
            r8.append(r1)     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            java.lang.String r1 = r8.toString()     // Catch:{ Exception -> 0x06a9, all -> 0x06a7 }
            r2 = r61
            r2.execSQL(r1)     // Catch:{ Exception -> 0x06a5, all -> 0x06a7 }
            r1 = r19
            r2.execSQL(r1)     // Catch:{ Exception -> 0x06a1, all -> 0x06a7 }
            java.lang.String r3 = "ALTER TABLE user_temp RENAME TO user"
            r2.execSQL(r3)     // Catch:{ Exception -> 0x06a1, all -> 0x06a7 }
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x06a1, all -> 0x06a7 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()     // Catch:{ Exception -> 0x06a1, all -> 0x06a7 }
            java.lang.String r4 = "Migration 4 to 5 Success"
            r5 = r18
            r3.d(r5, r4)     // Catch:{ Exception -> 0x069f, all -> 0x06a7 }
            if (r17 == 0) goto L_0x0700
            r17.close()
        L_0x069b:
            com.fossil.i97 r1 = com.fossil.i97.a
            goto L_0x0700
        L_0x069f:
            r0 = move-exception
            goto L_0x06b0
        L_0x06a1:
            r0 = move-exception
            r5 = r18
            goto L_0x06b0
        L_0x06a5:
            r0 = move-exception
            goto L_0x06ac
        L_0x06a7:
            r0 = move-exception
            goto L_0x06b7
        L_0x06a9:
            r0 = move-exception
            r2 = r61
        L_0x06ac:
            r5 = r18
            r1 = r19
        L_0x06b0:
            r3 = r0
            r14 = r17
            goto L_0x06d3
        L_0x06b4:
            r0 = move-exception
            r17 = r14
        L_0x06b7:
            r1 = r0
            r5 = r17
            goto L_0x0715
        L_0x06bb:
            r0 = move-exception
            r16 = r3
            r5 = r6
            r17 = r14
            r59 = r2
            r2 = r1
            r1 = r59
            r3 = r0
            goto L_0x06d3
        L_0x06c8:
            r0 = move-exception
            r16 = r3
            r5 = r6
            r59 = r2
            r2 = r1
            r1 = r59
            r3 = r0
            r14 = 0
        L_0x06d3:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0712 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()     // Catch:{ all -> 0x0712 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0712 }
            r6.<init>()     // Catch:{ all -> 0x0712 }
            java.lang.String r7 = "Migration 4 to 5 Exception "
            r6.append(r7)     // Catch:{ all -> 0x0712 }
            r6.append(r3)     // Catch:{ all -> 0x0712 }
            java.lang.String r3 = r6.toString()     // Catch:{ all -> 0x0712 }
            r4.e(r5, r3)     // Catch:{ all -> 0x0712 }
            r2.execSQL(r1)     // Catch:{ all -> 0x0712 }
            r1 = r16
            r2.execSQL(r1)     // Catch:{ all -> 0x0712 }
            java.lang.String r1 = "CREATE TABLE user (pinType TEXT NOT NULL DEFAULT '0', isOnboardingComplete INTEGER NOT NULL DEFAULT 0, averageSleep INTEGER NOT NULL DEFAULT 0, averageStep INTEGER NOT NULL DEFAULT 0, createdAt TEXT NOT NULL DEFAULT '2018-03-20T07:14:33.627Z', updatedAt TEXT NOT NULL DEFAULT '2018-03-20T07:14:33.627Z', uid TEXT NOT NULL DEFAULT '' , authType TEXT NOT NULL DEFAULT 'email' ,birthday TEXT NOT NULL DEFAULT '', brand TEXT NOT NULL DEFAULT '', diagnosticEnabled INTEGER NOT NULL DEFAULT 0,email TEXT NOT NULL DEFAULT '' , emailOptIn INTEGER NOT NULL DEFAULT 0, emailVerified INTEGER NOT NULL DEFAULT 0, firstName TEXT NOT NULL DEFAULT '' , gender TEXT NOT NULL DEFAULT 'M', heightInCentimeters INTEGER NOT NULL DEFAULT 170, integrations TEXT NOT NULL DEFAULT '', lastName TEXT NOT NULL DEFAULT '' , profilePicture TEXT NOT NULL DEFAULT '', registerDate TEXT NOT NULL DEFAULT '2015-11-29', registrationComplete INTEGER NOT NULL DEFAULT 0, useDefaultBiometric INTEGER NOT NULL DEFAULT 0,useDefaultGoals INTEGER NOT NULL DEFAULT 0, username TEXT NOT NULL DEFAULT '' , weightInGrams INTEGER NOT NULL DEFAULT 68030, userAccessToken TEXT NOT NULL DEFAULT '', refreshToken TEXT NOT NULL DEFAULT '', accessTokenExpiresAt TEXT NOT NULL DEFAULT '',accessTokenExpiresIn INTEGER NOT NULL DEFAULT 86400, home TEXT NOT NULL DEFAULT '', work TEXT NOT NULL DEFAULT '' , distanceUnit TEXT NOT NULL DEFAULT 'metric', weightUnit TEXT NOT NULL DEFAULT 'metric', heightUnit TEXT NOT NULL DEFAULT 'metric',activeDeviceId TEXT NOT NULL DEFAULT '', temperatureUnit TEXT NOT NULL DEFAULT 'metric', PRIMARY KEY(uid))"
            r2.execSQL(r1)     // Catch:{ all -> 0x0712 }
            if (r14 == 0) goto L_0x0700
            r14.close()
            goto L_0x069b
        L_0x0700:
            r61.setTransactionSuccessful()
            r61.endTransaction()
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = "Migration 4 to 5 Done"
            r1.d(r5, r2)
            return
        L_0x0712:
            r0 = move-exception
            r1 = r0
            r5 = r14
        L_0x0715:
            if (r5 == 0) goto L_0x071c
            r5.close()
            com.fossil.i97 r2 = com.fossil.i97.a
        L_0x071c:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.room.UserDatabase$Companion$MIGRATION_FROM_4_TO_5$Anon1.migrate(com.fossil.wi):void");
    }
}
