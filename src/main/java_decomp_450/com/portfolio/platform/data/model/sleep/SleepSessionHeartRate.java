package com.portfolio.platform.data.model.sleep;

import com.fossil.ee7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionHeartRate {
    @DexIgnore
    public float average;
    @DexIgnore
    public int max;
    @DexIgnore
    public int min;
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public List<Short> values;

    @DexIgnore
    public SleepSessionHeartRate(float f, int i, int i2, int i3, List<Short> list) {
        ee7.b(list, "values");
        this.average = f;
        this.max = i;
        this.min = i2;
        this.resolutionInSecond = i3;
        this.values = list;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.portfolio.platform.data.model.sleep.SleepSessionHeartRate */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ SleepSessionHeartRate copy$default(SleepSessionHeartRate sleepSessionHeartRate, float f, int i, int i2, int i3, List list, int i4, Object obj) {
        if ((i4 & 1) != 0) {
            f = sleepSessionHeartRate.average;
        }
        if ((i4 & 2) != 0) {
            i = sleepSessionHeartRate.max;
        }
        if ((i4 & 4) != 0) {
            i2 = sleepSessionHeartRate.min;
        }
        if ((i4 & 8) != 0) {
            i3 = sleepSessionHeartRate.resolutionInSecond;
        }
        if ((i4 & 16) != 0) {
            list = sleepSessionHeartRate.values;
        }
        return sleepSessionHeartRate.copy(f, i, i2, i3, list);
    }

    @DexIgnore
    public final float component1() {
        return this.average;
    }

    @DexIgnore
    public final int component2() {
        return this.max;
    }

    @DexIgnore
    public final int component3() {
        return this.min;
    }

    @DexIgnore
    public final int component4() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Short> component5() {
        return this.values;
    }

    @DexIgnore
    public final SleepSessionHeartRate copy(float f, int i, int i2, int i3, List<Short> list) {
        ee7.b(list, "values");
        return new SleepSessionHeartRate(f, i, i2, i3, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SleepSessionHeartRate)) {
            return false;
        }
        SleepSessionHeartRate sleepSessionHeartRate = (SleepSessionHeartRate) obj;
        return Float.compare(this.average, sleepSessionHeartRate.average) == 0 && this.max == sleepSessionHeartRate.max && this.min == sleepSessionHeartRate.min && this.resolutionInSecond == sleepSessionHeartRate.resolutionInSecond && ee7.a(this.values, sleepSessionHeartRate.values);
    }

    @DexIgnore
    public final float getAverage() {
        return this.average;
    }

    @DexIgnore
    public final int getMax() {
        return this.max;
    }

    @DexIgnore
    public final int getMin() {
        return this.min;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Short> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int floatToIntBits = ((((((Float.floatToIntBits(this.average) * 31) + this.max) * 31) + this.min) * 31) + this.resolutionInSecond) * 31;
        List<Short> list = this.values;
        return floatToIntBits + (list != null ? list.hashCode() : 0);
    }

    @DexIgnore
    public final void setAverage(float f) {
        this.average = f;
    }

    @DexIgnore
    public final void setMax(int i) {
        this.max = i;
    }

    @DexIgnore
    public final void setMin(int i) {
        this.min = i;
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setValues(List<Short> list) {
        ee7.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "SleepSessionHeartRate(average=" + this.average + ", max=" + this.max + ", min=" + this.min + ", resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ")";
    }
}
