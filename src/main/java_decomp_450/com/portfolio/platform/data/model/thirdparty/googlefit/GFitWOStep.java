package com.portfolio.platform.data.model.thirdparty.googlefit;

import com.fossil.c;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitWOStep {
    @DexIgnore
    public long endTime;
    @DexIgnore
    public long startTime;
    @DexIgnore
    public int step;

    @DexIgnore
    public GFitWOStep(int i, long j, long j2) {
        this.step = i;
        this.startTime = j;
        this.endTime = j2;
    }

    @DexIgnore
    public static /* synthetic */ GFitWOStep copy$default(GFitWOStep gFitWOStep, int i, long j, long j2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = gFitWOStep.step;
        }
        if ((i2 & 2) != 0) {
            j = gFitWOStep.startTime;
        }
        if ((i2 & 4) != 0) {
            j2 = gFitWOStep.endTime;
        }
        return gFitWOStep.copy(i, j, j2);
    }

    @DexIgnore
    public final int component1() {
        return this.step;
    }

    @DexIgnore
    public final long component2() {
        return this.startTime;
    }

    @DexIgnore
    public final long component3() {
        return this.endTime;
    }

    @DexIgnore
    public final GFitWOStep copy(int i, long j, long j2) {
        return new GFitWOStep(i, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GFitWOStep)) {
            return false;
        }
        GFitWOStep gFitWOStep = (GFitWOStep) obj;
        return this.step == gFitWOStep.step && this.startTime == gFitWOStep.startTime && this.endTime == gFitWOStep.endTime;
    }

    @DexIgnore
    public final long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final int getStep() {
        return this.step;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.step * 31) + c.a(this.startTime)) * 31) + c.a(this.endTime);
    }

    @DexIgnore
    public final void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public final void setStartTime(long j) {
        this.startTime = j;
    }

    @DexIgnore
    public final void setStep(int i) {
        this.step = i;
    }

    @DexIgnore
    public String toString() {
        return "GFitWOStep(step=" + this.step + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ")";
    }
}
