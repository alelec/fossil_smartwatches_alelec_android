package com.portfolio.platform.data.model;

import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.xs7;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Device {
    @DexIgnore
    @te4(LegacyDeviceModel.COLUMN_BATTERY_LEVEL)
    public int batteryLevel;
    @DexIgnore
    @te4("createdAt")
    public String createdAt;
    @DexIgnore
    @te4("id")
    public String deviceId;
    @DexIgnore
    @te4("deviceType")
    public String deviceType;
    @DexIgnore
    @te4(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION)
    public String firmwareRevision;
    @DexIgnore
    @te4("hardwareRevision")
    public String hardwareRevision;
    @DexIgnore
    @te4("isActive")
    public boolean isActive;
    @DexIgnore
    @te4("macAddress")
    public String macAddress;
    @DexIgnore
    @te4(MicroAppVariant.COLUMN_MAJOR_NUMBER)
    public int major;
    @DexIgnore
    @te4("manufacturer")
    public String manufacturer;
    @DexIgnore
    @te4(MicroAppVariant.COLUMN_MINOR_NUMBER)
    public int minor;
    @DexIgnore
    @te4("uid")
    public String owner;
    @DexIgnore
    @te4("productDisplayName")
    public String productDisplayName;
    @DexIgnore
    @te4("deviceModel")
    public String sku;
    @DexIgnore
    @te4("softwareRevision")
    public String softwareRevision;
    @DexIgnore
    @te4("updatedAt")
    public String updatedAt;
    @DexIgnore
    @te4("vibrationStrength")
    public Integer vibrationStrength;

    @DexIgnore
    public Device(String str, String str2, String str3, String str4, int i, Integer num, boolean z) {
        ee7.b(str, "deviceId");
        this.deviceId = str;
        this.macAddress = str2;
        this.sku = str3;
        this.firmwareRevision = str4;
        this.batteryLevel = i;
        this.vibrationStrength = num;
        this.isActive = z;
        this.createdAt = "";
        this.updatedAt = "";
        this.owner = "";
        this.deviceType = "";
        this.productDisplayName = "";
        this.manufacturer = "";
        this.softwareRevision = "";
        this.hardwareRevision = "";
    }

    @DexIgnore
    public static /* synthetic */ Device copy$default(Device device, String str, String str2, String str3, String str4, int i, Integer num, boolean z, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = device.deviceId;
        }
        if ((i2 & 2) != 0) {
            str2 = device.macAddress;
        }
        if ((i2 & 4) != 0) {
            str3 = device.sku;
        }
        if ((i2 & 8) != 0) {
            str4 = device.firmwareRevision;
        }
        if ((i2 & 16) != 0) {
            i = device.batteryLevel;
        }
        if ((i2 & 32) != 0) {
            num = device.vibrationStrength;
        }
        if ((i2 & 64) != 0) {
            z = device.isActive;
        }
        return device.copy(str, str2, str3, str4, i, num, z);
    }

    @DexIgnore
    public final void appendAdditionalInfo(MisfitDeviceProfile misfitDeviceProfile) {
        ee7.b(misfitDeviceProfile, "sdkDevice");
        this.deviceId = misfitDeviceProfile.getDeviceSerial();
        this.firmwareRevision = misfitDeviceProfile.getFirmwareVersion();
        this.productDisplayName = misfitDeviceProfile.getProductName();
        this.deviceType = "Misfit";
        this.manufacturer = "Misfit";
        this.hardwareRevision = xs7.a(misfitDeviceProfile.getDeviceSerial(), 0, 4);
        this.softwareRevision = ButtonService.Companion.getSdkVersionV2();
        this.macAddress = misfitDeviceProfile.getAddress();
        this.sku = misfitDeviceProfile.getDeviceModel();
        this.batteryLevel = misfitDeviceProfile.getBatteryLevel();
        this.major = misfitDeviceProfile.getMicroAppMajorVersion();
        this.minor = misfitDeviceProfile.getMicroAppMinorVersion();
    }

    @DexIgnore
    public final String component1() {
        return this.deviceId;
    }

    @DexIgnore
    public final String component2() {
        return this.macAddress;
    }

    @DexIgnore
    public final String component3() {
        return this.sku;
    }

    @DexIgnore
    public final String component4() {
        return this.firmwareRevision;
    }

    @DexIgnore
    public final int component5() {
        return this.batteryLevel;
    }

    @DexIgnore
    public final Integer component6() {
        return this.vibrationStrength;
    }

    @DexIgnore
    public final boolean component7() {
        return this.isActive;
    }

    @DexIgnore
    public final Device copy(String str, String str2, String str3, String str4, int i, Integer num, boolean z) {
        ee7.b(str, "deviceId");
        return new Device(str, str2, str3, str4, i, num, z);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Device)) {
            return false;
        }
        Device device = (Device) obj;
        return ee7.a(this.deviceId, device.deviceId) && ee7.a(this.macAddress, device.macAddress) && ee7.a(this.sku, device.sku) && ee7.a(this.firmwareRevision, device.firmwareRevision) && this.batteryLevel == device.batteryLevel && ee7.a(this.vibrationStrength, device.vibrationStrength) && this.isActive == device.isActive;
    }

    @DexIgnore
    public final int getBatteryLevel() {
        return this.batteryLevel;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getDeviceId() {
        return this.deviceId;
    }

    @DexIgnore
    public final String getDeviceType() {
        return this.deviceType;
    }

    @DexIgnore
    public final String getFirmwareRevision() {
        return this.firmwareRevision;
    }

    @DexIgnore
    public final String getHardwareRevision() {
        return this.hardwareRevision;
    }

    @DexIgnore
    public final String getMacAddress() {
        return this.macAddress;
    }

    @DexIgnore
    public final int getMajor() {
        return this.major;
    }

    @DexIgnore
    public final String getManufacturer() {
        return this.manufacturer;
    }

    @DexIgnore
    public final int getMinor() {
        return this.minor;
    }

    @DexIgnore
    public final String getOwner() {
        return this.owner;
    }

    @DexIgnore
    public final String getProductDisplayName() {
        return this.productDisplayName;
    }

    @DexIgnore
    public final String getSku() {
        return this.sku;
    }

    @DexIgnore
    public final String getSoftwareRevision() {
        return this.softwareRevision;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final Integer getVibrationStrength() {
        return this.vibrationStrength;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.deviceId;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.macAddress;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.sku;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.firmwareRevision;
        int hashCode4 = (((hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31) + this.batteryLevel) * 31;
        Integer num = this.vibrationStrength;
        if (num != null) {
            i = num.hashCode();
        }
        int i2 = (hashCode4 + i) * 31;
        boolean z = this.isActive;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return i2 + i3;
    }

    @DexIgnore
    public final boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public final void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public final void setBatteryLevel(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDeviceId(String str) {
        ee7.b(str, "<set-?>");
        this.deviceId = str;
    }

    @DexIgnore
    public final void setDeviceType(String str) {
        this.deviceType = str;
    }

    @DexIgnore
    public final void setFirmwareRevision(String str) {
        this.firmwareRevision = str;
    }

    @DexIgnore
    public final void setHardwareRevision(String str) {
        this.hardwareRevision = str;
    }

    @DexIgnore
    public final void setMacAddress(String str) {
        this.macAddress = str;
    }

    @DexIgnore
    public final void setMajor(int i) {
        this.major = i;
    }

    @DexIgnore
    public final void setManufacturer(String str) {
        this.manufacturer = str;
    }

    @DexIgnore
    public final void setMinor(int i) {
        this.minor = i;
    }

    @DexIgnore
    public final void setOwner(String str) {
        this.owner = str;
    }

    @DexIgnore
    public final void setProductDisplayName(String str) {
        this.productDisplayName = str;
    }

    @DexIgnore
    public final void setSku(String str) {
        this.sku = str;
    }

    @DexIgnore
    public final void setSoftwareRevision(String str) {
        this.softwareRevision = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setVibrationStrength(Integer num) {
        this.vibrationStrength = num;
    }

    @DexIgnore
    public String toString() {
        return "Device(deviceId=" + this.deviceId + ", macAddress=" + this.macAddress + ", sku=" + this.sku + ", firmwareRevision=" + this.firmwareRevision + ", batteryLevel=" + this.batteryLevel + ", vibrationStrength=" + this.vibrationStrength + ", isActive=" + this.isActive + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Device(String str, String str2, String str3, String str4, int i, Integer num, boolean z, int i2, zd7 zd7) {
        this(str, (i2 & 2) != 0 ? "" : str2, (i2 & 4) != 0 ? "" : str3, (i2 & 8) != 0 ? "" : str4, i, (i2 & 32) != 0 ? 25 : num, (i2 & 64) != 0 ? false : z);
    }
}
