package com.portfolio.platform.data;

import com.portfolio.platform.data.source.local.AddressDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocationSource_Factory implements Factory<LocationSource> {
    @DexIgnore
    public /* final */ Provider<AddressDao> addressDaoProvider;

    @DexIgnore
    public LocationSource_Factory(Provider<AddressDao> provider) {
        this.addressDaoProvider = provider;
    }

    @DexIgnore
    public static LocationSource_Factory create(Provider<AddressDao> provider) {
        return new LocationSource_Factory(provider);
    }

    @DexIgnore
    public static LocationSource newInstance() {
        return new LocationSource();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public LocationSource get() {
        LocationSource newInstance = newInstance();
        LocationSource_MembersInjector.injectAddressDao(newInstance, this.addressDaoProvider.get());
        return newInstance;
    }
}
