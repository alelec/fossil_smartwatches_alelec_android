package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.ee7;
import com.fossil.gj5;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRepository$migrateMicroAppVariants$Anon1 implements MicroAppVariantDataSource.GetVariantListRemoteCallback {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantDataSource.MigrateVariantCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ int $major;
    @DexIgnore
    public /* final */ /* synthetic */ int $minor;
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantRepository this$0;

    @DexIgnore
    public MicroAppVariantRepository$migrateMicroAppVariants$Anon1(MicroAppVariantRepository microAppVariantRepository, String str, int i, int i2, MicroAppVariantDataSource.MigrateVariantCallback migrateVariantCallback) {
        this.this$0 = microAppVariantRepository;
        this.$serialNumber = str;
        this.$major = i;
        this.$minor = i2;
        this.$callback = migrateVariantCallback;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback
    public void onFail(int i) {
        String tag = MicroAppVariantRepository.Companion.getTAG();
        MFLogger.d(tag, "getAllMicroAppVariants remote serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onFail");
        MicroAppVariantDataSource.MigrateVariantCallback migrateVariantCallback = this.$callback;
        if (migrateVariantCallback != null) {
            migrateVariantCallback.onDone();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback
    public void onSuccess(ArrayList<gj5> arrayList) {
        ee7.b(arrayList, "variantParserList");
        String tag = MicroAppVariantRepository.Companion.getTAG();
        MFLogger.d(tag, "getAllMicroAppVariants remote serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onSuccess");
        this.this$0.mAppExecutors.a().execute(new MicroAppVariantRepository$migrateMicroAppVariants$Anon1$onSuccess$Anon1_Level2(this, arrayList));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback
    public void onSuccess(List<MicroAppVariant> list) {
        ee7.b(list, "variantList");
    }
}
