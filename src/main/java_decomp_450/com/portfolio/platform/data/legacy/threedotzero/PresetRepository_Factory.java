package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.pj4;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PresetRepository_Factory implements Factory<PresetRepository> {
    @DexIgnore
    public /* final */ Provider<pj4> appExecutorsProvider;
    @DexIgnore
    public /* final */ Provider<PresetDataSource> mappingLocalSetDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<PresetDataSource> mappingRemoteSetDataSourceProvider;

    @DexIgnore
    public PresetRepository_Factory(Provider<PresetDataSource> provider, Provider<PresetDataSource> provider2, Provider<pj4> provider3) {
        this.mappingRemoteSetDataSourceProvider = provider;
        this.mappingLocalSetDataSourceProvider = provider2;
        this.appExecutorsProvider = provider3;
    }

    @DexIgnore
    public static PresetRepository_Factory create(Provider<PresetDataSource> provider, Provider<PresetDataSource> provider2, Provider<pj4> provider3) {
        return new PresetRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static PresetRepository newInstance(PresetDataSource presetDataSource, PresetDataSource presetDataSource2, pj4 pj4) {
        return new PresetRepository(presetDataSource, presetDataSource2, pj4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public PresetRepository get() {
        return newInstance(this.mappingRemoteSetDataSourceProvider.get(), this.mappingLocalSetDataSourceProvider.get(), this.appExecutorsProvider.get());
    }
}
