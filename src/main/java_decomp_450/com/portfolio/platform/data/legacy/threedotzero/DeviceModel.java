package com.portfolio.platform.data.legacy.threedotzero;

import com.facebook.places.PlaceManager;
import com.fossil.te4;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.buttonservice.db.DataLogService;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "device")
public final class DeviceModel {
    @DexIgnore
    @DatabaseField(columnName = "deviceBattery")
    @te4(LegacyDeviceModel.COLUMN_BATTERY_LEVEL)
    public int batteryLevel;
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    @te4("createdAt")
    public String createdAt;
    @DexIgnore
    @DatabaseField(columnName = "deviceId", id = true)
    @te4("id")
    public String deviceId;
    @DexIgnore
    @DatabaseField(columnName = LegacyDeviceModel.COLUMN_FIRMWARE_VERSION)
    @te4(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION)
    public String firmwareRevision;
    @DexIgnore
    @DatabaseField(columnName = "macAddress")
    @te4(PlaceManager.PARAM_MAC_ADDRESS)
    public String macAddress;
    @DexIgnore
    @DatabaseField(columnName = "major")
    @te4(MicroAppVariant.COLUMN_MAJOR_NUMBER)
    public int major;
    @DexIgnore
    @DatabaseField(columnName = "minor")
    @te4(MicroAppVariant.COLUMN_MINOR_NUMBER)
    public int minor;
    @DexIgnore
    @DatabaseField(columnName = LegacyDeviceModel.COLUMN_DEVICE_MODEL)
    @te4(LegacyDeviceModel.COLUMN_DEVICE_MODEL)
    public String sku;
    @DexIgnore
    @DatabaseField(columnName = DataLogService.COLUMN_UPDATE_AT)
    @te4("updatedAt")
    public String updateAt;
    @DexIgnore
    @DatabaseField(columnName = "vibrationStrength")
    public int vibrationStrength; // = 50;

    @DexIgnore
    public final int getBatteryLevel() {
        return this.batteryLevel;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getDeviceId() {
        return this.deviceId;
    }

    @DexIgnore
    public final String getFirmwareRevision() {
        return this.firmwareRevision;
    }

    @DexIgnore
    public final String getMacAddress() {
        return this.macAddress;
    }

    @DexIgnore
    public final int getMajor() {
        return this.major;
    }

    @DexIgnore
    public final int getMinor() {
        return this.minor;
    }

    @DexIgnore
    public final String getSku() {
        return this.sku;
    }

    @DexIgnore
    public final String getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public final int getVibrationStrength() {
        return this.vibrationStrength;
    }

    @DexIgnore
    public final void setBatteryLevel(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDeviceId(String str) {
        this.deviceId = str;
    }

    @DexIgnore
    public final void setFirmwareRevision(String str) {
        this.firmwareRevision = str;
    }

    @DexIgnore
    public final void setMacAddress(String str) {
        this.macAddress = str;
    }

    @DexIgnore
    public final void setMajor(int i) {
        this.major = i;
    }

    @DexIgnore
    public final void setMinor(int i) {
        this.minor = i;
    }

    @DexIgnore
    public final void setSku(String str) {
        this.sku = str;
    }

    @DexIgnore
    public final void setUpdateAt(String str) {
        this.updateAt = str;
    }

    @DexIgnore
    public final void setVibrationStrength(int i) {
        this.vibrationStrength = i;
    }
}
