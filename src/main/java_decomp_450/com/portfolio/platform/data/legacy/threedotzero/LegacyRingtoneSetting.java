package com.portfolio.platform.data.legacy.threedotzero;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LegacyRingtoneSetting implements Serializable {
    @DexIgnore
    public String mExtension;
    @DexIgnore
    public String mId;
    @DexIgnore
    public String mName;

    @DexIgnore
    public LegacyRingtoneSetting() {
        this.mName = "";
    }

    @DexIgnore
    public String getId() {
        return this.mId;
    }

    @DexIgnore
    public String getName() {
        return this.mName;
    }

    @DexIgnore
    public void setExtension(String str) {
        this.mExtension = str;
    }

    @DexIgnore
    public void setId(String str) {
        this.mId = str;
    }

    @DexIgnore
    public String toRingtoneNameWithExtension() {
        return this.mName + '.' + this.mExtension;
    }

    @DexIgnore
    public String toString() {
        return this.mName;
    }

    @DexIgnore
    public LegacyRingtoneSetting(String str, String str2) {
        this.mId = str;
        this.mName = str2;
        this.mExtension = "";
    }

    @DexIgnore
    public LegacyRingtoneSetting(String str) {
        this.mName = str;
        this.mExtension = "";
    }

    @DexIgnore
    public LegacyRingtoneSetting(String str, String str2, int i, String str3) {
        this.mName = str;
        this.mExtension = str2;
        this.mId = "";
    }
}
