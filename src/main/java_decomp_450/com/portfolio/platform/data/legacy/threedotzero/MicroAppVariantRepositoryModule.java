package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.ee7;
import com.fossil.pj4;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRepositoryModule {
    @DexIgnore
    @Local
    public final MicroAppVariantDataSource provideMicroAppVariantLocalDataSource$app_fossilRelease() {
        return new MicroAppVariantLocalDataSource();
    }

    @DexIgnore
    @Remote
    public final MicroAppVariantDataSource provideMicroAppVariantRemoteDataSource$app_fossilRelease(ShortcutApiService shortcutApiService, pj4 pj4) {
        ee7.b(shortcutApiService, "shortcutApiService");
        ee7.b(pj4, "appExecutors");
        return new MicroAppVariantRemoteDataSource(shortcutApiService, pj4);
    }
}
