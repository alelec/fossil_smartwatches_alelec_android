package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.ee7;
import com.fossil.ie4;
import com.fossil.pj4;
import com.fossil.ru7;
import com.fossil.zd7;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRemoteDataSource extends MicroAppVariantDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ pj4 mAppExecutors;
    @DexIgnore
    public /* final */ ShortcutApiService mShortcutApiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppVariantRemoteDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            ee7.b(str, "<set-?>");
            MicroAppVariantRemoteDataSource.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppVariantRemoteDataSource.class.getSimpleName();
        ee7.a((Object) simpleName, "MicroAppVariantRemoteDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppVariantRemoteDataSource(ShortcutApiService shortcutApiService, pj4 pj4) {
        ee7.b(shortcutApiService, "mShortcutApiService");
        ee7.b(pj4, "mAppExecutors");
        this.mShortcutApiService = shortcutApiService;
        this.mAppExecutors = pj4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource
    public void getAllMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        ee7.b(str, "serialNumber");
        String str2 = TAG;
        MFLogger.d(str2, "getAllMicroAppVariants serialNumber=" + str);
        getAllMicroAppVariants$app_fossilRelease(str, i, i2, 0, 100, new MicroAppVariantRemoteDataSource$getAllMicroAppVariants$resultCallback$Anon1(this, str, new ArrayList(), i, i2, getVariantListCallback));
    }

    @DexIgnore
    public final void getAllMicroAppVariants$app_fossilRelease(String str, int i, int i2, int i3, int i4, ru7<ie4> ru7) {
        ee7.b(str, "serialNumber");
        ee7.b(ru7, Constants.CALLBACK);
    }
}
