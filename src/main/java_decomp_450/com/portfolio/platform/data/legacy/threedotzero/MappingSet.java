package com.portfolio.platform.data.legacy.threedotzero;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fossil.de4;
import com.fossil.ke4;
import com.fossil.ne5;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.model.Mapping;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration;
import com.portfolio.platform.data.model.room.microapp.ButtonMapping;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "mappingSet")
public class MappingSet implements Parcelable {
    @DexIgnore
    public static /* final */ String COLUMN_CHECKSUM; // = "checksum";
    @DexIgnore
    public static /* final */ String COLUMN_CREATE_AT; // = "createAt";
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_FAMILY; // = "deviceFamily";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_IS_ACTIVE; // = "isActive";
    @DexIgnore
    public static /* final */ String COLUMN_LIST_MAPPING_JSON; // = "listMappingJson";
    @DexIgnore
    public static /* final */ String COLUMN_NAME; // = "name";
    @DexIgnore
    public static /* final */ String COLUMN_OBJECT_ID; // = "objectId";
    @DexIgnore
    public static /* final */ String COLUMN_TYPE; // = "type";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ String COLUMN_URI; // = "uri";
    @DexIgnore
    public static /* final */ Parcelable.Creator<MappingSet> CREATOR; // = new Anon1();
    @DexIgnore
    @DatabaseField(columnName = "checksum")
    public String checkSum;
    @DexIgnore
    @DatabaseField(columnName = "createAt")
    public long createAt;
    @DexIgnore
    @DatabaseField(columnName = "deviceFamily")
    public int deviceFamily;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String id;
    @DexIgnore
    @DatabaseField(columnName = "isActive")
    public boolean isActive;
    @DexIgnore
    public List<Mapping> mappingList;
    @DexIgnore
    @DatabaseField(columnName = "listMappingJson")
    public String mappingListJsonString;
    @DexIgnore
    @DatabaseField(columnName = "name")
    public String name;
    @DexIgnore
    @DatabaseField(columnName = "objectId")
    public String objectId;
    @DexIgnore
    @DatabaseField(columnName = "type")
    public int type;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    public long updateAt;
    @DexIgnore
    @DatabaseField(columnName = "uri")
    public String uri;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<MappingSet> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public MappingSet createFromParcel(Parcel parcel) {
            return new MappingSet(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public MappingSet[] newArray(int i) {
            return new MappingSet[i];
        }
    }

    @DexIgnore
    public enum MappingSetType {
        FEATURE(0),
        DEFAULT(1),
        USER_SAVED(2),
        USER_NOT_SAVED(3);
        
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public MappingSetType(int i) {
            this.value = i;
        }

        @DexIgnore
        public static MappingSetType fromInt(int i) {
            MappingSetType[] values = values();
            for (MappingSetType mappingSetType : values) {
                if (mappingSetType.getValue() == i) {
                    return mappingSetType;
                }
            }
            return FEATURE;
        }

        @DexIgnore
        public int getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public MappingSet() {
        this.mappingList = new ArrayList();
    }

    @DexIgnore
    private List<Mapping> getListMappingFromJson() {
        Gson gson = new Gson();
        de4 c = new ke4().a(this.mappingListJsonString).c();
        ArrayList arrayList = new ArrayList();
        Iterator<JsonElement> it = c.iterator();
        while (it.hasNext()) {
            arrayList.add((Mapping) gson.a(it.next(), Mapping.class));
        }
        return arrayList;
    }

    @DexIgnore
    public ActivePreset convertToActivePreset(String str) {
        ActivePreset activePreset = new ActivePreset();
        activePreset.setCreateAt(System.currentTimeMillis());
        activePreset.setUpdateAt(System.currentTimeMillis());
        activePreset.setSerialNumber(str);
        ArrayList arrayList = new ArrayList();
        for (Mapping mapping : getMappingList()) {
            if (mapping.getAction() == 2005) {
                return null;
            }
            MicroAppInstruction.MicroAppID a = ne5.a.a(Integer.valueOf(mapping.getAction()));
            if (a != null) {
                PusherConfiguration.Pusher pusherByGesture = PusherConfiguration.getPusherByGesture(mapping.getGesture());
                if (pusherByGesture == null) {
                    return null;
                }
                arrayList.add(new ButtonMapping(pusherByGesture.getValue(), a.getValue()));
            }
        }
        try {
            activePreset.setButtonMappingList(arrayList);
            return activePreset;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public SavedPreset convertToSavedPreset() {
        SavedPreset savedPreset = new SavedPreset();
        savedPreset.setCreateAt(System.currentTimeMillis());
        savedPreset.setUpdateAt(System.currentTimeMillis());
        savedPreset.setName(this.name);
        ArrayList arrayList = new ArrayList();
        for (Mapping mapping : getMappingList()) {
            if (mapping.getAction() == 2005) {
                return null;
            }
            MicroAppInstruction.MicroAppID a = ne5.a.a(Integer.valueOf(mapping.getAction()));
            if (a != null) {
                PusherConfiguration.Pusher pusherByGesture = PusherConfiguration.getPusherByGesture(mapping.getGesture());
                if (pusherByGesture == null) {
                    return null;
                }
                arrayList.add(new ButtonMapping(pusherByGesture.getValue(), a.getValue()));
            }
        }
        try {
            savedPreset.setButtonMappingList(arrayList);
            return savedPreset;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String getCheckSum() {
        return this.checkSum;
    }

    @DexIgnore
    public long getCreateAt() {
        return this.createAt;
    }

    @DexIgnore
    public MFDeviceFamily getDeviceFamily() {
        return MFDeviceFamily.fromInt(this.deviceFamily);
    }

    @DexIgnore
    public String getId() {
        return this.id;
    }

    @DexIgnore
    public List<Mapping> getMappingList() {
        if (this.mappingList.size() == 0 && !TextUtils.isEmpty(this.mappingListJsonString)) {
            this.mappingList = new ArrayList(getListMappingFromJson());
        }
        return this.mappingList;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public String getObjectId() {
        return this.objectId;
    }

    @DexIgnore
    public MappingSetType getType() {
        return MappingSetType.fromInt(this.type);
    }

    @DexIgnore
    public long getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public String getUri() {
        return this.uri;
    }

    @DexIgnore
    public boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public boolean isSameMappingList(MappingSet mappingSet) {
        if (mappingSet == null || getMappingList().size() != mappingSet.getMappingList().size()) {
            return false;
        }
        boolean z = true;
        for (int i = 0; i < getMappingList().size(); i++) {
            if (getMappingList().get(i).getAction() != mappingSet.getMappingList().get(i).getAction()) {
                z = false;
            }
        }
        return z;
    }

    @DexIgnore
    public void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public void setCheckSum(String str) {
        this.checkSum = str;
    }

    @DexIgnore
    public void setCreateAt(long j) {
        this.createAt = j;
    }

    @DexIgnore
    public void setDeviceFamily(MFDeviceFamily mFDeviceFamily) {
        this.deviceFamily = mFDeviceFamily.getValue();
    }

    @DexIgnore
    public void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public void setObjectId(String str) {
        this.objectId = str;
    }

    @DexIgnore
    public void setType(MappingSetType mappingSetType) {
        this.type = mappingSetType.getValue();
    }

    @DexIgnore
    public void setUpdateAt(long j) {
        this.updateAt = j;
    }

    @DexIgnore
    public void setUri(String str) {
        this.uri = str;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.mappingList);
        parcel.writeString(this.name);
        parcel.writeString(this.mappingListJsonString);
        parcel.writeInt(this.deviceFamily);
        parcel.writeLong(this.createAt);
        parcel.writeLong(this.updateAt);
        parcel.writeString(this.id);
        parcel.writeByte(this.isActive ? (byte) 1 : 0);
        parcel.writeInt(this.type);
        parcel.writeString(this.uri);
        parcel.writeString(this.objectId);
        parcel.writeString(this.checkSum);
    }

    @DexIgnore
    public MappingSet(Parcel parcel) {
        ArrayList arrayList = new ArrayList();
        this.mappingList = arrayList;
        parcel.readTypedList(arrayList, Mapping.CREATOR);
        this.name = parcel.readString();
        this.mappingListJsonString = parcel.readString();
        this.deviceFamily = parcel.readInt();
        this.createAt = parcel.readLong();
        this.updateAt = parcel.readLong();
        this.id = parcel.readString();
        this.isActive = parcel.readByte() != 0;
        this.type = parcel.readInt();
        this.uri = parcel.readString();
        this.objectId = parcel.readString();
        this.checkSum = parcel.readString();
    }
}
