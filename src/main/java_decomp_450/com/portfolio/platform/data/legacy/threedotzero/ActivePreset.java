package com.portfolio.platform.data.legacy.threedotzero;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fossil.de4;
import com.fossil.ie4;
import com.fossil.te4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonWriter;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.room.microapp.ButtonMapping;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "activePreset")
public class ActivePreset implements Parcelable {
    @DexIgnore
    public static /* final */ String COLUMN_BUTTONS; // = "buttons";
    @DexIgnore
    public static /* final */ String COLUMN_CREATE_AT; // = "createAt";
    @DexIgnore
    public static /* final */ String COLUMN_ORIGINAL_ID; // = "originalId";
    @DexIgnore
    public static /* final */ String COLUMN_PIN_TYPE; // = "pinType";
    @DexIgnore
    public static /* final */ String COLUMN_SERIAL; // = "serialNumber";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ Parcelable.Creator<ActivePreset> CREATOR; // = new Anon1();
    @DexIgnore
    public static /* final */ String TAG; // = ActivePreset.class.getSimpleName();
    @DexIgnore
    public List<ButtonMapping> buttonMappingList;
    @DexIgnore
    @DatabaseField(columnName = "buttons")
    @te4("buttons")
    public String buttons;
    @DexIgnore
    @DatabaseField(columnName = "createAt")
    @te4("createAt")
    public long createAt;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_ORIGINAL_ID)
    @te4(COLUMN_ORIGINAL_ID)
    public String originalId;
    @DexIgnore
    @DatabaseField(columnName = "pinType")
    public int pinType;
    @DexIgnore
    @DatabaseField(columnName = "serialNumber", id = true)
    @te4("serialNumber")
    public String serialNumber;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    @te4("updatedAt")
    public long updateAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<ActivePreset> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActivePreset createFromParcel(Parcel parcel) {
            return new ActivePreset(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActivePreset[] newArray(int i) {
            return new ActivePreset[i];
        }
    }

    @DexIgnore
    public ActivePreset() {
        this.buttonMappingList = new ArrayList();
        this.pinType = 0;
        this.originalId = "";
    }

    @DexIgnore
    private List<ButtonMapping> getListMappingFromJson() {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = new JSONArray(this.buttons);
            if (jSONArray.length() > 0) {
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    ButtonMapping buttonMapping = new ButtonMapping("", "");
                    if (jSONObject.has("button")) {
                        buttonMapping.setButton(jSONObject.getString("button"));
                    }
                    if (jSONObject.has("appId")) {
                        buttonMapping.setMicroAppId(jSONObject.getString("appId"));
                    }
                    arrayList.add(buttonMapping);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    @DexIgnore
    private void write(JsonWriter jsonWriter, List<ButtonMapping> list) throws IOException {
        jsonWriter.c();
        for (ButtonMapping buttonMapping : list) {
            jsonWriter.e();
            jsonWriter.b("button").e(buttonMapping.getButton());
            jsonWriter.b("appId").e(buttonMapping.getMicroAppId());
            jsonWriter.k();
        }
        jsonWriter.g();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public List<ButtonMapping> getButtonMappingList() {
        if (this.buttonMappingList.isEmpty() && !TextUtils.isEmpty(this.buttons)) {
            this.buttonMappingList = new ArrayList(getListMappingFromJson());
        }
        return this.buttonMappingList;
    }

    @DexIgnore
    public String getButtons() {
        return this.buttons;
    }

    @DexIgnore
    public long getCreateAt() {
        return this.createAt;
    }

    @DexIgnore
    public ie4 getJsonObject() {
        ie4 ie4 = new ie4();
        try {
            ie4.a("createAt", Long.valueOf(this.createAt));
            ie4.a("updatedAt", Long.valueOf(this.updateAt));
            ie4.a(COLUMN_ORIGINAL_ID, this.originalId == null ? "" : this.originalId);
            ie4.a("serialNumber", this.serialNumber);
            ie4.a("buttons", (JsonElement) new Gson().a(this.buttons, de4.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        MFLogger.d("SavedPreset", "initJsonData - json: " + ie4);
        return ie4;
    }

    @DexIgnore
    public String getOriginalId() {
        return this.originalId;
    }

    @DexIgnore
    public int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public long getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public void setButtonMappingList(List<ButtonMapping> list) throws IOException {
        StringWriter stringWriter = new StringWriter();
        JsonWriter jsonWriter = new JsonWriter(stringWriter);
        write(jsonWriter, list);
        this.buttons = stringWriter.toString();
        this.buttonMappingList.clear();
        this.buttonMappingList.addAll(list);
        try {
            jsonWriter.close();
            stringWriter.close();
        } catch (Exception e) {
            String str = TAG;
            MFLogger.d(str, "setButtonMappingList error when close writer e=" + e);
        }
    }

    @DexIgnore
    public void setButtons(String str) {
        this.buttons = str;
        if (!TextUtils.isEmpty(str)) {
            this.buttonMappingList = new ArrayList(getListMappingFromJson());
        }
    }

    @DexIgnore
    public void setCreateAt(long j) {
        this.createAt = j / 1000;
    }

    @DexIgnore
    public void setOriginalId(String str) {
        this.originalId = str;
    }

    @DexIgnore
    public void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public void setSerialNumber(String str) {
        this.serialNumber = str;
    }

    @DexIgnore
    public void setUpdateAt(long j) {
        this.updateAt = j / 1000;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.buttonMappingList);
        parcel.writeString(this.buttons);
        parcel.writeLong(this.createAt);
        parcel.writeLong(this.updateAt);
        parcel.writeString(this.originalId);
        parcel.writeInt(this.pinType);
    }

    @DexIgnore
    public ActivePreset(ActivePreset activePreset) {
        this.buttonMappingList = new ArrayList();
        this.createAt = activePreset.createAt;
        this.updateAt = activePreset.updateAt;
        this.originalId = activePreset.originalId;
        this.serialNumber = activePreset.serialNumber;
        this.originalId = activePreset.originalId;
        setButtons(activePreset.buttons);
        this.pinType = 0;
    }

    @DexIgnore
    public ActivePreset(Parcel parcel) {
        ArrayList arrayList = new ArrayList();
        this.buttonMappingList = arrayList;
        parcel.readTypedList(arrayList, ButtonMapping.CREATOR);
        this.buttons = parcel.readString();
        this.createAt = parcel.readLong();
        this.updateAt = parcel.readLong();
        this.originalId = parcel.readString();
        this.pinType = parcel.readInt();
    }
}
