package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.ee7;
import com.fossil.fv7;
import com.fossil.i97;
import com.fossil.ru7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.List;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRemoteDataSource$getMicroAppGallery$Anon1 implements ru7<ApiResponse<MicroApp>> {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppGalleryDataSource.GetMicroAppGalleryCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceSerial;

    @DexIgnore
    public MicroAppGalleryRemoteDataSource$getMicroAppGallery$Anon1(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        this.$deviceSerial = str;
        this.$callback = getMicroAppGalleryCallback;
    }

    @DexIgnore
    @Override // com.fossil.ru7
    public void onFailure(Call<ApiResponse<MicroApp>> call, Throwable th) {
        ee7.b(call, "call");
        ee7.b(th, "throwable");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = MicroAppGalleryRemoteDataSource.Companion.getTAG();
        StringBuilder sb = new StringBuilder();
        sb.append("getMicroAppGallery deviceSerial=");
        sb.append(this.$deviceSerial);
        sb.append(" onFailure ex=");
        th.printStackTrace();
        sb.append(i97.a);
        local.d(tag, sb.toString());
        MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onFail();
        }
    }

    @DexIgnore
    @Override // com.fossil.ru7
    public void onResponse(Call<ApiResponse<MicroApp>> call, fv7<ApiResponse<MicroApp>> fv7) {
        ee7.b(call, "call");
        ee7.b(fv7, "response");
        if (fv7.d()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = MicroAppGalleryRemoteDataSource.Companion.getTAG();
            local.d(tag, "getMicroAppGallery deviceSerial=" + this.$deviceSerial + " onSuccess response=" + fv7.a());
            ApiResponse<MicroApp> a = fv7.a();
            List<MicroApp> list = a != null ? a.get_items() : null;
            if (list == null || !(!list.isEmpty())) {
                MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
                if (getMicroAppGalleryCallback != null) {
                    getMicroAppGalleryCallback.onFail();
                    return;
                }
                return;
            }
            MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback2 = this.$callback;
            if (getMicroAppGalleryCallback2 != null) {
                getMicroAppGalleryCallback2.onSuccess(list);
                return;
            }
            return;
        }
        MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback3 = this.$callback;
        if (getMicroAppGalleryCallback3 != null) {
            getMicroAppGalleryCallback3.onFail();
        }
    }
}
