package com.portfolio.platform.data.legacy.onedotfive;

import android.content.Context;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting;
import java.sql.SQLException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SecondTimezoneProviderImp extends BaseDbProvider implements SecondTimezoneProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "secondTimezone.db";
    @DexIgnore
    public static /* final */ String TAG; // = "SecondTimezoneProviderImp";

    @DexIgnore
    public SecondTimezoneProviderImp(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<LegacySecondTimezoneSetting, Integer> getSecondTimezoneSession() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(LegacySecondTimezoneSetting.class);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProvider
    public void addOrUpdateTimeZone(LegacySecondTimezoneSetting legacySecondTimezoneSetting) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "addOrUpdateTimeZone - secondTimezone" + legacySecondTimezoneSetting);
        if (legacySecondTimezoneSetting != null) {
            try {
                getSecondTimezoneSession().createOrUpdate(legacySecondTimezoneSetting);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e(TAG, "addOrUpdateTimeZone - e=" + e);
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProvider
    public LegacySecondTimezoneSetting getActiveSecondTimezone() {
        try {
            QueryBuilder<LegacySecondTimezoneSetting, Integer> queryBuilder = getSecondTimezoneSession().queryBuilder();
            queryBuilder.where().eq("isActiveAlarm", true);
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e(TAG, "getActiveSecondTimezone - e=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{LegacySecondTimezoneSetting.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    public String getDbPath() {
        return ((BaseDbProvider) this).databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 1;
    }
}
