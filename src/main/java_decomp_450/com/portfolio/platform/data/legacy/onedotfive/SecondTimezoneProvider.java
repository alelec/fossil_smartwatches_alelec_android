package com.portfolio.platform.data.legacy.onedotfive;

import com.fossil.wearables.fsl.BaseProvider;
import com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface SecondTimezoneProvider extends BaseProvider {
    @DexIgnore
    void addOrUpdateTimeZone(LegacySecondTimezoneSetting legacySecondTimezoneSetting);

    @DexIgnore
    LegacySecondTimezoneSetting getActiveSecondTimezone();
}
