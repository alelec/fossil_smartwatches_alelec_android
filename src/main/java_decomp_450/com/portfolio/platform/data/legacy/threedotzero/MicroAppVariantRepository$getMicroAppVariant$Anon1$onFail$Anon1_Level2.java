package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.ee7;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRepository$getMicroAppVariant$Anon1$onFail$Anon1_Level2 implements MicroAppVariantDataSource.GetVariantListCallback {
    @DexIgnore
    public /* final */ /* synthetic */ int $errorCode;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantRepository$getMicroAppVariant$Anon1 this$0;

    @DexIgnore
    public MicroAppVariantRepository$getMicroAppVariant$Anon1$onFail$Anon1_Level2(MicroAppVariantRepository$getMicroAppVariant$Anon1 microAppVariantRepository$getMicroAppVariant$Anon1, int i) {
        this.this$0 = microAppVariantRepository$getMicroAppVariant$Anon1;
        this.$errorCode = i;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback
    public void onFail(int i) {
        String tag = MicroAppVariantRepository.Companion.getTAG();
        MFLogger.d(tag, "getMicroAppVariant remote serialNumber=" + this.this$0.$serialNumber + " major=" + this.this$0.$major + " minor=" + this.this$0.$minor + " onFail");
        MicroAppVariantDataSource.GetVariantCallback getVariantCallback = this.this$0.$callback;
        if (getVariantCallback != null) {
            getVariantCallback.onFail(i);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback
    public void onSuccess(List<MicroAppVariant> list) {
        T t;
        ee7.b(list, "variantList");
        String tag = MicroAppVariantRepository.Companion.getTAG();
        MFLogger.d(tag, "getMicroAppVariant remote serialNumber=" + this.this$0.$serialNumber + " major=" + this.this$0.$major + " minor=" + this.this$0.$minor + " onSuccess");
        T t2 = null;
        if (MicroAppVariantRepository.WhenMappings.$EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.this$0.$microAppId).ordinal()] != 1) {
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                T next = it.next();
                if (ee7.a((Object) next.getAppId(), (Object) this.this$0.$microAppId)) {
                    t2 = next;
                    break;
                }
            }
            t = t2;
        } else {
            Iterator<T> it2 = list.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                T next2 = it2.next();
                if (ee7.a((Object) next2.getName(), (Object) this.this$0.$variantName)) {
                    t2 = next2;
                    break;
                }
            }
            t = t2;
        }
        if (t != null) {
            MicroAppVariantDataSource.GetVariantCallback getVariantCallback = this.this$0.$callback;
            if (getVariantCallback != null) {
                getVariantCallback.onSuccess(t);
                return;
            }
            return;
        }
        MicroAppVariantDataSource.GetVariantCallback getVariantCallback2 = this.this$0.$callback;
        if (getVariantCallback2 != null) {
            getVariantCallback2.onFail(this.$errorCode);
        }
    }
}
