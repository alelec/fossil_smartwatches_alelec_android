package com.portfolio.platform.data.legacy.threedotzero;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.te4;
import com.fossil.wearables.fsl.goaltracking.Frequency;
import com.fossil.wearables.fsl.goaltracking.GoalTracking;
import com.fossil.wearables.fsl.goaltracking.PeriodType;
import com.fossil.zd5;
import com.google.gson.Gson;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LegacyGoalTrackingSettings implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<LegacyGoalTrackingSettings> CREATOR; // = new Anon1();
    @DexIgnore
    @te4("frequency")
    public int mFrequency;
    @DexIgnore
    @te4("goalId")
    public int mGoalId;
    @DexIgnore
    @te4("goalName")
    public String mName;
    @DexIgnore
    @te4(GoalTracking.COLUMN_PERIOD_TYPE)
    public int mPeriodType;
    @DexIgnore
    @te4(GoalTracking.COLUMN_PERIOD_VALUE)
    public int mPeriodValue;
    @DexIgnore
    @te4("target")
    public int mTarget;
    @DexIgnore
    @te4("updatedDate")
    public String mUpdatedDate;
    @DexIgnore
    @te4("value")
    public int mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<LegacyGoalTrackingSettings> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public LegacyGoalTrackingSettings createFromParcel(Parcel parcel) {
            return new LegacyGoalTrackingSettings(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public LegacyGoalTrackingSettings[] newArray(int i) {
            return new LegacyGoalTrackingSettings[i];
        }
    }

    @DexIgnore
    public LegacyGoalTrackingSettings() {
        this.mGoalId = 1;
        this.mName = "Drink Water";
        this.mFrequency = Frequency.DAILY.getValue();
        this.mTarget = 1;
        this.mPeriodType = PeriodType.UNKNOWN.getValue();
        this.mPeriodValue = -1;
        this.mValue = 0;
        this.mUpdatedDate = zd5.k(Calendar.getInstance().getTime());
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public Frequency getFrequency() {
        return Frequency.fromInt(this.mFrequency);
    }

    @DexIgnore
    public int getGoalId() {
        return this.mGoalId;
    }

    @DexIgnore
    public String getName() {
        return this.mName;
    }

    @DexIgnore
    public PeriodType getPeriodType() {
        return PeriodType.fromInt(this.mPeriodType);
    }

    @DexIgnore
    public int getPeriodValue() {
        return this.mPeriodValue;
    }

    @DexIgnore
    public int getTarget() {
        return this.mTarget;
    }

    @DexIgnore
    public Date getUpdatedDate() {
        return zd5.a(this.mUpdatedDate);
    }

    @DexIgnore
    public int getValue() {
        return this.mValue;
    }

    @DexIgnore
    public void setFrequency(Frequency frequency) {
        this.mFrequency = frequency.getValue();
    }

    @DexIgnore
    public void setGoalId(int i) {
        this.mGoalId = i;
    }

    @DexIgnore
    public void setName(String str) {
        this.mName = str;
    }

    @DexIgnore
    public void setPeriodType(PeriodType periodType) {
        this.mPeriodType = periodType.getValue();
    }

    @DexIgnore
    public void setPeriodValue(int i) {
        this.mPeriodValue = i;
    }

    @DexIgnore
    public void setTarget(int i) {
        this.mTarget = i;
    }

    @DexIgnore
    public void setUpdatedDate(Date date) {
        this.mUpdatedDate = zd5.k(date);
    }

    @DexIgnore
    public void setValue(int i) {
        if (i < 0) {
            i = 0;
        }
        this.mValue = i;
    }

    @DexIgnore
    public String toJson() {
        return new Gson().a(this);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mGoalId);
        parcel.writeString(this.mName);
        parcel.writeInt(this.mFrequency);
        parcel.writeInt(this.mTarget);
        parcel.writeInt(this.mPeriodType);
        parcel.writeInt(this.mPeriodValue);
        parcel.writeInt(this.mValue);
        parcel.writeString(this.mUpdatedDate);
    }

    @DexIgnore
    public LegacyGoalTrackingSettings(int i, String str, Frequency frequency, int i2, PeriodType periodType, int i3, int i4, Date date) {
        this.mGoalId = i;
        this.mName = str;
        this.mFrequency = frequency.getValue();
        this.mTarget = i2;
        this.mPeriodType = periodType.getValue();
        this.mPeriodValue = i3;
        this.mValue = i4;
        this.mUpdatedDate = zd5.k(date);
    }

    @DexIgnore
    public LegacyGoalTrackingSettings(Parcel parcel) {
        this.mGoalId = parcel.readInt();
        this.mName = parcel.readString();
        this.mFrequency = parcel.readInt();
        this.mTarget = parcel.readInt();
        this.mPeriodType = parcel.readInt();
        this.mPeriodValue = parcel.readInt();
        this.mValue = parcel.readInt();
        this.mUpdatedDate = parcel.readString();
    }
}
