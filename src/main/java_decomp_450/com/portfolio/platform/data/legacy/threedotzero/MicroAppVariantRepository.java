package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.ee7;
import com.fossil.gj5;
import com.fossil.ja7;
import com.fossil.pj4;
import com.fossil.w97;
import com.fossil.x87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import com.portfolio.platform.data.source.UAppSystemVersionRepository;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MicroAppVariantRepository extends MicroAppVariantDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ pj4 mAppExecutors;
    @DexIgnore
    public /* final */ MicroAppVariantDataSource mMicroAppVariantLocalDataSource;
    @DexIgnore
    public /* final */ MicroAppVariantDataSource mMicroAppVariantRemoteDataSource;
    @DexIgnore
    public List<ShortcutDownloadingObserver> mShortcutDownloadingObservers;
    @DexIgnore
    public /* final */ UAppSystemVersionRepository mUAppSystemVersionRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppVariantRepository.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            ee7.b(str, "<set-?>");
            MicroAppVariantRepository.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[MicroAppInstruction.MicroAppID.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.ordinal()] = 1;
        }
        */
    }

    /*
    static {
        String simpleName = MicroAppVariantRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "MicroAppVariantRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppVariantRepository(@Remote MicroAppVariantDataSource microAppVariantDataSource, @Local MicroAppVariantDataSource microAppVariantDataSource2, UAppSystemVersionRepository uAppSystemVersionRepository, pj4 pj4) {
        ee7.b(microAppVariantDataSource, "mMicroAppVariantRemoteDataSource");
        ee7.b(microAppVariantDataSource2, "mMicroAppVariantLocalDataSource");
        ee7.b(uAppSystemVersionRepository, "mUAppSystemVersionRepository");
        ee7.b(pj4, "mAppExecutors");
        this.mMicroAppVariantRemoteDataSource = microAppVariantDataSource;
        this.mMicroAppVariantLocalDataSource = microAppVariantDataSource2;
        this.mUAppSystemVersionRepository = uAppSystemVersionRepository;
        this.mAppExecutors = pj4;
    }

    @DexIgnore
    private final void saveDeclarationFileList(List<? extends DeclarationFile> list, MicroAppVariant microAppVariant) {
        for (DeclarationFile declarationFile : list) {
            declarationFile.setMicroAppVariant(microAppVariant);
            addOrUpdateDeclarationFile(declarationFile, new MicroAppVariantRepository$saveDeclarationFileList$Anon1());
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource
    public void addOrUpdateDeclarationFile(DeclarationFile declarationFile, MicroAppVariantDataSource.AddOrUpdateDeclarationFileCallback addOrUpdateDeclarationFileCallback) {
        ee7.b(declarationFile, "declarationFile");
        ee7.b(addOrUpdateDeclarationFileCallback, Constants.CALLBACK);
        String str = TAG;
        MFLogger.d(str, "addOrUpdateDeclarationFile fileId=" + declarationFile.getId());
        this.mMicroAppVariantLocalDataSource.addOrUpdateDeclarationFile(declarationFile, addOrUpdateDeclarationFileCallback);
    }

    @DexIgnore
    public final void addStatusObserver(ShortcutDownloadingObserver shortcutDownloadingObserver) {
        ee7.b(shortcutDownloadingObserver, "observer");
        if (this.mShortcutDownloadingObservers == null) {
            this.mShortcutDownloadingObservers = new CopyOnWriteArrayList();
        }
        List<ShortcutDownloadingObserver> list = this.mShortcutDownloadingObservers;
        if (list != null) {
            list.add(shortcutDownloadingObserver);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public void downloadAllVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        ee7.b(str, "serialNumber");
        notifyStatusChanged("DECLARATION_FILES_DOWNLOADING", str);
        this.mMicroAppVariantRemoteDataSource.getAllMicroAppVariants(str, i, i2, new MicroAppVariantRepository$downloadAllVariants$Anon1(this, str, i, i2, getVariantListCallback));
    }

    @DexIgnore
    public final ArrayList<gj5> filterVariantList$app_fossilRelease(List<? extends gj5> list) {
        ee7.b(list, "microAppVariants");
        HashMap hashMap = new HashMap();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            gj5 gj5 = (gj5) list.get(i);
            Iterator it = w97.a((Collection<?>) list).iterator();
            while (it.hasNext()) {
                gj5 gj52 = (gj5) list.get(((ja7) it).a());
                if (ee7.a((Object) gj52.b(), (Object) gj5.b()) && ee7.a((Object) gj52.e(), (Object) gj5.e()) && gj52.d() > gj5.d()) {
                    gj5 = gj52;
                }
            }
            hashMap.put(gj5.b() + gj5.e(), gj5);
        }
        return new ArrayList<>(hashMap.values());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource
    public void getAllMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        ee7.b(str, "serialNumber");
        String str2 = TAG;
        MFLogger.d(str2, "getAllMicroAppVariants serialNumber=" + str + " major=" + i + " minor=" + i2);
        this.mMicroAppVariantLocalDataSource.getAllMicroAppVariants(str, i, i2, new MicroAppVariantRepository$getAllMicroAppVariants$Anon1(this, str, i, i2, getVariantListCallback));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource
    public void getMicroAppVariant(String str, String str2, String str3, int i, int i2, MicroAppVariantDataSource.GetVariantCallback getVariantCallback) {
        ee7.b(str, "serialNumber");
        ee7.b(str2, "microAppId");
        ee7.b(str3, "variantName");
        String str4 = TAG;
        MFLogger.d(str4, "getMicroAppVariant serialNumber=" + str + "microAppId=" + str2 + " major=" + i + " minor=" + i2 + " name=" + str3);
        this.mMicroAppVariantLocalDataSource.getMicroAppVariant(str, str2, str3, i, i2, new MicroAppVariantRepository$getMicroAppVariant$Anon1(this, str, i, i2, getVariantCallback, str2, str3));
    }

    @DexIgnore
    public final void migrateMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.MigrateVariantCallback migrateVariantCallback) {
        ee7.b(str, "serialNumber");
        notifyStatusChanged("DECLARATION_FILES_DOWNLOADING", str);
        this.mMicroAppVariantRemoteDataSource.getAllMicroAppVariants(str, i, i2, new MicroAppVariantRepository$migrateMicroAppVariants$Anon1(this, str, i, i2, migrateVariantCallback));
    }

    @DexIgnore
    public final void notifyStatusChanged(String str, String str2) {
        ee7.b(str, "status");
        ee7.b(str2, "serial");
        List<ShortcutDownloadingObserver> list = this.mShortcutDownloadingObservers;
        if (list == null) {
            return;
        }
        if (list == null) {
            ee7.a();
            throw null;
        } else if (!list.isEmpty()) {
            List<ShortcutDownloadingObserver> list2 = this.mShortcutDownloadingObservers;
            if (list2 != null) {
                Iterator<T> it = list2.iterator();
                while (it.hasNext()) {
                    it.next().onStatusChanged(str, str2);
                }
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void removeStatusObserver(ShortcutDownloadingObserver shortcutDownloadingObserver) {
        ee7.b(shortcutDownloadingObserver, "observer");
        List<ShortcutDownloadingObserver> list = this.mShortcutDownloadingObservers;
        if (list == null) {
            ee7.a();
            throw null;
        } else if (list.contains(shortcutDownloadingObserver)) {
            List<ShortcutDownloadingObserver> list2 = this.mShortcutDownloadingObservers;
            if (list2 != null) {
                list2.remove(shortcutDownloadingObserver);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void saveMicroAppVariant$app_fossilRelease(String str, List<? extends gj5> list) {
        ee7.b(str, "serialNumber");
        ee7.b(list, "filter");
        for (gj5 gj5 : list) {
            gj5.d(str);
            MicroAppVariant a = gj5.a();
            ee7.a((Object) a, "parser.convertToMicroAppVariant()");
            String str2 = TAG;
            MFLogger.d(str2, "saveMicroAppVariant variantId=" + a.getId());
            MicroAppVariantDataSource microAppVariantDataSource = this.mMicroAppVariantLocalDataSource;
            if (microAppVariantDataSource != null) {
                ((MicroAppVariantLocalDataSource) microAppVariantDataSource).addOrUpDateVariant(a);
                List<DeclarationFile> c = gj5.c();
                ee7.a((Object) c, "declarationFileList");
                saveDeclarationFileList(c, a);
            } else {
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantLocalDataSource");
            }
        }
    }
}
