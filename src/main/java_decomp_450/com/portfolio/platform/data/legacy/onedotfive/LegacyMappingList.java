package com.portfolio.platform.data.legacy.onedotfive;

import com.fossil.ee7;
import com.fossil.te4;
import com.misfit.frameworks.buttonservice.model.Mapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LegacyMappingList {
    @DexIgnore
    @te4("action")
    public int action;
    @DexIgnore
    @te4("deviceId")
    public String deviceId;
    @DexIgnore
    @te4(Mapping.COLUMN_EXTRA_INFO)
    public String extraInfo;
    @DexIgnore
    @te4("gesture")
    public String gesture;
    @DexIgnore
    @te4("isServiceCommand")
    public boolean isServiceCommand;
    @DexIgnore
    @te4("mDeviceFamily")
    public String mDeviceFamily;
    @DexIgnore
    @te4("updatedAt")
    public String updatedAt;

    @DexIgnore
    public LegacyMappingList(boolean z, int i, String str, String str2, String str3, String str4, String str5) {
        ee7.b(str, "mDeviceFamily");
        ee7.b(str2, "deviceId");
        ee7.b(str3, Mapping.COLUMN_EXTRA_INFO);
        ee7.b(str4, "gesture");
        ee7.b(str5, "updatedAt");
        this.isServiceCommand = z;
        this.action = i;
        this.mDeviceFamily = str;
        this.deviceId = str2;
        this.extraInfo = str3;
        this.gesture = str4;
        this.updatedAt = str5;
    }

    @DexIgnore
    public final int getAction() {
        return this.action;
    }

    @DexIgnore
    public final String getDeviceId() {
        return this.deviceId;
    }

    @DexIgnore
    public final String getExtraInfo() {
        return this.extraInfo;
    }

    @DexIgnore
    public final String getGesture() {
        return this.gesture;
    }

    @DexIgnore
    public final String getMDeviceFamily() {
        return this.mDeviceFamily;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final boolean isServiceCommand() {
        return this.isServiceCommand;
    }

    @DexIgnore
    public final void setAction(int i) {
        this.action = i;
    }

    @DexIgnore
    public final void setDeviceId(String str) {
        ee7.b(str, "<set-?>");
        this.deviceId = str;
    }

    @DexIgnore
    public final void setExtraInfo(String str) {
        ee7.b(str, "<set-?>");
        this.extraInfo = str;
    }

    @DexIgnore
    public final void setGesture(String str) {
        ee7.b(str, "<set-?>");
        this.gesture = str;
    }

    @DexIgnore
    public final void setMDeviceFamily(String str) {
        ee7.b(str, "<set-?>");
        this.mDeviceFamily = str;
    }

    @DexIgnore
    public final void setServiceCommand(boolean z) {
        this.isServiceCommand = z;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        ee7.b(str, "<set-?>");
        this.updatedAt = str;
    }
}
