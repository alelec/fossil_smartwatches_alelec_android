package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.ee7;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRepository$getMicroApp$Anon1 implements MicroAppGalleryDataSource.GetMicroAppCallback {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppGalleryDataSource.GetMicroAppCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceSerial;
    @DexIgnore
    public /* final */ /* synthetic */ String $microAppId;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppGalleryRepository this$0;

    @DexIgnore
    public MicroAppGalleryRepository$getMicroApp$Anon1(MicroAppGalleryRepository microAppGalleryRepository, String str, MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback, String str2) {
        this.this$0 = microAppGalleryRepository;
        this.$microAppId = str;
        this.$callback = getMicroAppCallback;
        this.$deviceSerial = str2;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppCallback
    public void onFail() {
        String tag = MicroAppGalleryRepository.Companion.getTAG();
        MFLogger.d(tag, "getMicroApp microAppId=" + this.$microAppId + " local onFail");
        this.this$0.mMicroAppSettingRemoteDataSource.getMicroAppGallery(this.$deviceSerial, new MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1_Level2(this));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppCallback
    public void onSuccess(MicroApp microApp) {
        ee7.b(microApp, "microApp");
        String tag = MicroAppGalleryRepository.Companion.getTAG();
        MFLogger.d(tag, "getMicroApp microAppId=" + this.$microAppId + " local onSuccess");
        MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback = this.$callback;
        if (getMicroAppCallback != null) {
            getMicroAppCallback.onSuccess(microApp);
        }
    }
}
