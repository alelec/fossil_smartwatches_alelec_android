package com.portfolio.platform.data.legacy.threedotzero;

import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.misfit.frameworks.common.enums.Gesture;

public class PusherConfiguration {
    public static final String TAG = "PusherConfiguration";

    public static /* synthetic */ class Anon1 {
        public static final /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$common$enums$Gesture;
        public static final /* synthetic */ int[] $SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher;

        /* JADX WARNING: Can't wrap try/catch for region: R(52:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|60) */
        /* JADX WARNING: Can't wrap try/catch for region: R(53:0|(2:1|2)|3|(2:5|6)|7|9|10|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|60) */
        /* JADX WARNING: Can't wrap try/catch for region: R(54:0|(2:1|2)|3|5|6|7|9|10|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|60) */
        /* JADX WARNING: Can't wrap try/catch for region: R(56:0|1|2|3|5|6|7|9|10|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|60) */
        /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0044 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x004e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0058 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x006d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0078 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0083 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x008f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x009b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x00a7 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x00b3 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00bf */
        /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x00cb */
        /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x00d7 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x00e3 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:49:0x00ef */
        /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x00fb */
        /* JADX WARNING: Missing exception handler attribute for start block: B:53:0x0107 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:55:0x0113 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:57:0x011f */
        /*
        static {
            /*
                com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration$Pusher[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Pusher.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher = r0
                r1 = 1
                com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration$Pusher r2 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Pusher.TOP_PUSHER     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                r0 = 2
                int[] r2 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher     // Catch:{ NoSuchFieldError -> 0x001d }
                com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration$Pusher r3 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Pusher.MID_PUSHER     // Catch:{ NoSuchFieldError -> 0x001d }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                r2 = 3
                int[] r3 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration$Pusher r4 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Pusher.BOTTOM_PUSHER     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                r3 = 4
                int[] r4 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration$Pusher r5 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Pusher.TRIPLE_TAP     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                com.misfit.frameworks.common.enums.Gesture[] r4 = com.misfit.frameworks.common.enums.Gesture.values()
                int r4 = r4.length
                int[] r4 = new int[r4]
                com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture = r4
                com.misfit.frameworks.common.enums.Gesture r5 = com.misfit.frameworks.common.enums.Gesture.SAM_BT1_PRESSED     // Catch:{ NoSuchFieldError -> 0x0044 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0044 }
                r4[r5] = r1     // Catch:{ NoSuchFieldError -> 0x0044 }
            L_0x0044:
                int[] r1 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x004e }
                com.misfit.frameworks.common.enums.Gesture r4 = com.misfit.frameworks.common.enums.Gesture.SAM_BT1_SINGLE_PRESS     // Catch:{ NoSuchFieldError -> 0x004e }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x004e }
                r1[r4] = r0     // Catch:{ NoSuchFieldError -> 0x004e }
            L_0x004e:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x0058 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT1_SINGLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x0058 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0058 }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0058 }
            L_0x0058:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x0062 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT1_DOUBLE_PRESS     // Catch:{ NoSuchFieldError -> 0x0062 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0062 }
            L_0x0062:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x006d }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT1_DOUBLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x006d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006d }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006d }
            L_0x006d:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x0078 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT1_TRIPLE_PRESS     // Catch:{ NoSuchFieldError -> 0x0078 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0078 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0078 }
            L_0x0078:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x0083 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT1_TRIPLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x0083 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0083 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0083 }
            L_0x0083:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x008f }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT2_PRESSED     // Catch:{ NoSuchFieldError -> 0x008f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x008f }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x008f }
            L_0x008f:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x009b }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT2_SINGLE_PRESS     // Catch:{ NoSuchFieldError -> 0x009b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x009b }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x009b }
            L_0x009b:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00a7 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT2_SINGLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x00a7 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00a7 }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00a7 }
            L_0x00a7:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00b3 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT2_DOUBLE_PRESS     // Catch:{ NoSuchFieldError -> 0x00b3 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00b3 }
                r2 = 11
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00b3 }
            L_0x00b3:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00bf }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT2_DOUBLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x00bf }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00bf }
                r2 = 12
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00bf }
            L_0x00bf:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00cb }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT2_TRIPLE_PRESS     // Catch:{ NoSuchFieldError -> 0x00cb }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00cb }
                r2 = 13
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00cb }
            L_0x00cb:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00d7 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT2_TRIPLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x00d7 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00d7 }
                r2 = 14
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00d7 }
            L_0x00d7:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00e3 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT3_PRESSED     // Catch:{ NoSuchFieldError -> 0x00e3 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00e3 }
                r2 = 15
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00e3 }
            L_0x00e3:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00ef }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT3_SINGLE_PRESS     // Catch:{ NoSuchFieldError -> 0x00ef }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00ef }
                r2 = 16
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00ef }
            L_0x00ef:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00fb }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT3_SINGLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x00fb }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00fb }
                r2 = 17
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00fb }
            L_0x00fb:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x0107 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT3_DOUBLE_PRESS     // Catch:{ NoSuchFieldError -> 0x0107 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0107 }
                r2 = 18
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0107 }
            L_0x0107:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x0113 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT3_DOUBLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x0113 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0113 }
                r2 = 19
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0113 }
            L_0x0113:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x011f }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT3_TRIPLE_PRESS     // Catch:{ NoSuchFieldError -> 0x011f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x011f }
                r2 = 20
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x011f }
            L_0x011f:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x012b }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT3_TRIPLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x012b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x012b }
                r2 = 21
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x012b }
            L_0x012b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration.Anon1.<clinit>():void");
        }
        */
    }

    public enum Pusher {
        TOP_PUSHER(ViewHierarchy.DIMENSION_TOP_KEY),
        MID_PUSHER("middle"),
        BOTTOM_PUSHER("bottom"),
        DOUBLE_TAP("double tap"),
        TRIPLE_TAP("triple tap");
        
        public final String value;

        public Pusher(String str) {
            this.value = str;
        }

        public static Pusher getPusherFromValue(String str) {
            Pusher[] values = values();
            for (Pusher pusher : values) {
                if (pusher.value.equals(str)) {
                    return pusher;
                }
            }
            return TOP_PUSHER;
        }

        public String getValue() {
            return this.value;
        }

        public String toString() {
            return this.value;
        }
    }

    public static Gesture getGestureWithLinkAction(Pusher pusher, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Get action of pusher=" + pusher);
        int i2 = Anon1.$SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher[pusher.ordinal()];
        if (i2 != 1) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 == 4) {
                        return Gesture.TRIPLE_PRESS;
                    }
                    FLogger.INSTANCE.getLocal().d(TAG, "Error!!! Gesture is undefined");
                    return Gesture.NONE;
                } else if (i == 102) {
                    return Gesture.SAM_BT3_DOUBLE_PRESS;
                } else {
                    if (i == 103) {
                        return Gesture.SAM_BT3_SINGLE_PRESS_AND_HOLD;
                    }
                    if (i == 202) {
                        return Gesture.SAM_BT3_SINGLE_PRESS_AND_HOLD;
                    }
                    if (Action.DisplayMode.isActionBelongToThisType(i)) {
                        return Gesture.SAM_BT3_PRESSED;
                    }
                    return Gesture.SAM_BT3_SINGLE_PRESS;
                }
            } else if (i == 102) {
                return Gesture.SAM_BT2_DOUBLE_PRESS;
            } else {
                if (i == 103) {
                    return Gesture.SAM_BT2_SINGLE_PRESS_AND_HOLD;
                }
                if (i == 202) {
                    return Gesture.SAM_BT2_SINGLE_PRESS_AND_HOLD;
                }
                if (Action.DisplayMode.isActionBelongToThisType(i)) {
                    return Gesture.SAM_BT2_PRESSED;
                }
                return Gesture.SAM_BT2_SINGLE_PRESS;
            }
        } else if (i == 102) {
            return Gesture.SAM_BT1_DOUBLE_PRESS;
        } else {
            if (i == 103) {
                return Gesture.SAM_BT1_SINGLE_PRESS_AND_HOLD;
            }
            if (i == 202) {
                return Gesture.SAM_BT1_SINGLE_PRESS_AND_HOLD;
            }
            if (Action.DisplayMode.isActionBelongToThisType(i)) {
                return Gesture.SAM_BT1_PRESSED;
            }
            return Gesture.SAM_BT1_SINGLE_PRESS;
        }
    }

    public static Pusher getPusherByGesture(Gesture gesture) {
        if (gesture == null) {
            return null;
        }
        switch (Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture[gesture.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                return Pusher.TOP_PUSHER;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
                return Pusher.MID_PUSHER;
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
                return Pusher.BOTTOM_PUSHER;
            default:
                return null;
        }
    }
}
