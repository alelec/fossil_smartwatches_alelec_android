package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.c87;
import com.fossil.pj4;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory implements Factory<MicroAppSettingDataSource> {
    @DexIgnore
    public /* final */ Provider<pj4> appExecutorsProvider;
    @DexIgnore
    public /* final */ MicroAppSettingRepositoryModule module;
    @DexIgnore
    public /* final */ Provider<ShortcutApiService> shortcutApiServiceProvider;

    @DexIgnore
    public MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory(MicroAppSettingRepositoryModule microAppSettingRepositoryModule, Provider<ShortcutApiService> provider, Provider<pj4> provider2) {
        this.module = microAppSettingRepositoryModule;
        this.shortcutApiServiceProvider = provider;
        this.appExecutorsProvider = provider2;
    }

    @DexIgnore
    public static MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory create(MicroAppSettingRepositoryModule microAppSettingRepositoryModule, Provider<ShortcutApiService> provider, Provider<pj4> provider2) {
        return new MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory(microAppSettingRepositoryModule, provider, provider2);
    }

    @DexIgnore
    public static MicroAppSettingDataSource provideFavoriteMappingSetRemoteDataSource(MicroAppSettingRepositoryModule microAppSettingRepositoryModule, ShortcutApiService shortcutApiService, pj4 pj4) {
        MicroAppSettingDataSource provideFavoriteMappingSetRemoteDataSource = microAppSettingRepositoryModule.provideFavoriteMappingSetRemoteDataSource(shortcutApiService, pj4);
        c87.a(provideFavoriteMappingSetRemoteDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideFavoriteMappingSetRemoteDataSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public MicroAppSettingDataSource get() {
        return provideFavoriteMappingSetRemoteDataSource(this.module, this.shortcutApiServiceProvider.get(), this.appExecutorsProvider.get());
    }
}
