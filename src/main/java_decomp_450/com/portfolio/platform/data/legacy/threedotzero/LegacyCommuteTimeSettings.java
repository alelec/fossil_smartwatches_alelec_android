package com.portfolio.platform.data.legacy.threedotzero;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.te4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LegacyCommuteTimeSettings implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<LegacyCommuteTimeSettings> CREATOR; // = new Anon1();
    @DexIgnore
    @te4("destinationAddress")
    public String mDestination;
    @DexIgnore
    @te4("avoidTolls")
    public boolean mIsAvoidTolls;
    @DexIgnore
    @te4("locationType")
    public /* final */ LOCATION_TYPE mLocationType;
    @DexIgnore
    @te4("formatType")
    public TIME_FORMAT mTimeFormat;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<LegacyCommuteTimeSettings> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public LegacyCommuteTimeSettings createFromParcel(Parcel parcel) {
            return new LegacyCommuteTimeSettings(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public LegacyCommuteTimeSettings[] newArray(int i) {
            return new LegacyCommuteTimeSettings[i];
        }
    }

    @DexIgnore
    public enum LOCATION_TYPE {
        HOME(0),
        WORK(1);
        
        @DexIgnore
        public /* final */ int mValue;

        @DexIgnore
        public LOCATION_TYPE(int i) {
            this.mValue = i;
        }

        @DexIgnore
        public static LOCATION_TYPE getLocationType(int i) {
            LOCATION_TYPE[] values = values();
            for (LOCATION_TYPE location_type : values) {
                if (location_type.mValue == i) {
                    return location_type;
                }
            }
            return WORK;
        }

        @DexIgnore
        public int getValue() {
            return this.mValue;
        }
    }

    @DexIgnore
    public enum TIME_FORMAT {
        TRAVEL("travel"),
        ETA("eta");
        
        @DexIgnore
        public /* final */ String mValue;

        @DexIgnore
        public TIME_FORMAT(String str) {
            this.mValue = str;
        }

        @DexIgnore
        public static TIME_FORMAT getTimeFormat(String str) {
            TIME_FORMAT[] values = values();
            for (TIME_FORMAT time_format : values) {
                if (time_format.mValue.equals(str)) {
                    return time_format;
                }
            }
            return ETA;
        }

        @DexIgnore
        public String getValue() {
            return this.mValue;
        }
    }

    @DexIgnore
    public LegacyCommuteTimeSettings() {
        this.mDestination = "";
        this.mTimeFormat = TIME_FORMAT.ETA;
        this.mIsAvoidTolls = false;
        this.mLocationType = LOCATION_TYPE.WORK;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String getDestination() {
        return this.mDestination;
    }

    @DexIgnore
    public TIME_FORMAT getTimeFormat() {
        return this.mTimeFormat;
    }

    @DexIgnore
    public boolean isIsAvoidTolls() {
        return this.mIsAvoidTolls;
    }

    @DexIgnore
    public void setDestination(String str) {
        this.mDestination = str;
    }

    @DexIgnore
    public void setIsAvoidTolls(boolean z) {
        this.mIsAvoidTolls = z;
    }

    @DexIgnore
    public void setTimeFormat(TIME_FORMAT time_format) {
        this.mTimeFormat = time_format;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mDestination);
        parcel.writeString(this.mTimeFormat.getValue());
        parcel.writeString(String.valueOf(this.mIsAvoidTolls));
        parcel.writeInt(this.mLocationType.getValue());
    }

    @DexIgnore
    public LegacyCommuteTimeSettings(LegacyCommuteTimeSettings legacyCommuteTimeSettings) {
        this.mDestination = legacyCommuteTimeSettings.mDestination;
        this.mTimeFormat = legacyCommuteTimeSettings.mTimeFormat;
        this.mIsAvoidTolls = legacyCommuteTimeSettings.mIsAvoidTolls;
        this.mLocationType = legacyCommuteTimeSettings.mLocationType;
    }

    @DexIgnore
    public LegacyCommuteTimeSettings(String str, TIME_FORMAT time_format, boolean z) {
        this.mDestination = str;
        this.mTimeFormat = time_format;
        this.mIsAvoidTolls = z;
        this.mLocationType = LOCATION_TYPE.WORK;
    }

    @DexIgnore
    public LegacyCommuteTimeSettings(Parcel parcel) {
        this.mDestination = parcel.readString();
        this.mTimeFormat = TIME_FORMAT.getTimeFormat(parcel.readString());
        this.mIsAvoidTolls = Boolean.valueOf(parcel.readString()).booleanValue();
        this.mLocationType = LOCATION_TYPE.getLocationType(parcel.readInt());
    }
}
