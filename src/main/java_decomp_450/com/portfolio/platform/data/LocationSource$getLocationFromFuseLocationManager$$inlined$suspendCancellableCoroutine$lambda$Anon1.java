package com.portfolio.platform.data;

import android.content.Context;
import android.location.Location;
import com.fossil.ai7;
import com.fossil.b53;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.io3;
import com.fossil.jo3;
import com.fossil.kd7;
import com.fossil.kl7;
import com.fossil.nb7;
import com.fossil.no3;
import com.fossil.s87;
import com.fossil.se7;
import com.fossil.t87;
import com.fossil.yi7;
import com.fossil.zb7;
import com.google.android.gms.location.LocationRequest;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.LocationSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Context $context$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ai7 $continuation;
    @DexIgnore
    public /* final */ /* synthetic */ b53 $fusedLocationClient;
    @DexIgnore
    public /* final */ /* synthetic */ se7 $location;
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest $locationRequest;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LocationSource this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super Location>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1, fb7 fb7) {
            super(2, fb7);
            this.this$0 = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, fb7);
            anon1_Level2.p$ = (yi7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Location> fb7) {
            return ((Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 = this.this$0;
                LocationSource locationSource = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1.this$0;
                b53 b53 = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1.$fusedLocationClient;
                ee7.a((Object) b53, "fusedLocationClient");
                LocationRequest locationRequest = this.this$0.$locationRequest;
                this.L$0 = yi7;
                this.label = 1;
                obj = locationSource.requestLocationUpdates(b53, locationRequest, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2_Level2<TResult> implements jo3<Location> {
        @DexIgnore
        public /* final */ /* synthetic */ LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$0;

        @DexIgnore
        public Anon2_Level2(LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1) {
            this.this$0 = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        public final void onSuccess(Location location) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getLocationFromFuseLocationManager retrieveLastLocation success " + location);
            ai7 ai7 = this.this$0.$continuation;
            LocationSource.Result result = new LocationSource.Result(location, LocationSource.ErrorState.SUCCESS, false, 4, null);
            s87.a aVar = s87.Companion;
            ai7.resumeWith(s87.m60constructorimpl(result));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3_Level2 implements io3 {
        @DexIgnore
        public /* final */ /* synthetic */ LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$0;

        @DexIgnore
        public Anon3_Level2(LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1) {
            this.this$0 = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.io3
        public final void onFailure(Exception exc) {
            ee7.b(exc, "it");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getLocationFromFuseLocationManager retrieveLastLocation fail " + exc);
            ai7 ai7 = this.this$0.$continuation;
            LocationSource.Result result = new LocationSource.Result(null, LocationSource.ErrorState.UNKNOWN, false, 4, null);
            s87.a aVar = s87.Companion;
            ai7.resumeWith(s87.m60constructorimpl(result));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1(se7 se7, b53 b53, LocationRequest locationRequest, ai7 ai7, fb7 fb7, LocationSource locationSource, Context context) {
        super(2, fb7);
        this.$location = se7;
        this.$fusedLocationClient = b53;
        this.$locationRequest = locationRequest;
        this.$continuation = ai7;
        this.this$0 = locationSource;
        this.$context$inlined = context;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 = new LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1(this.$location, this.$fusedLocationClient, this.$locationRequest, this.$continuation, fb7, this.this$0, this.$context$inlined);
        locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1.p$ = (yi7) obj;
        return locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        se7 se7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            se7 se72 = this.$location;
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, null);
            this.L$0 = yi7;
            this.L$1 = se72;
            this.label = 1;
            obj = kl7.a(5000, anon1_Level2, this);
            if (obj == a) {
                return a;
            }
            se7 = se72;
        } else if (i == 1) {
            se7 = (se7) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        se7.element = (T) ((Location) obj);
        if (this.$location.element == null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getLocationFromFuseLocationManager timeout, try to get lastLocation=" + ((Object) this.$location.element));
            b53 b53 = this.$fusedLocationClient;
            ee7.a((Object) b53, "fusedLocationClient");
            no3<Location> i2 = b53.i();
            i2.a(new Anon2_Level2(this));
            i2.a(new Anon3_Level2(this));
            ee7.a((Object) i2, "fusedLocationClient.last\u2026                        }");
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease2 = LocationSource.Companion.getTAG$app_fossilRelease();
            local2.d(tAG$app_fossilRelease2, "getLocationFromFuseLocationManager success lastLocation=" + ((Object) this.$location.element));
            ai7 ai7 = this.$continuation;
            LocationSource.Result result = new LocationSource.Result(this.$location.element, LocationSource.ErrorState.SUCCESS, false, 4, null);
            s87.a aVar = s87.Companion;
            ai7.resumeWith(s87.m60constructorimpl(result));
        }
        return i97.a;
    }
}
