package com.portfolio.platform.data;

import com.fossil.ee7;
import com.fossil.t97;
import com.fossil.te4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserWrapper {
    @DexIgnore
    @te4("activeDeviceId")
    public /* final */ String mActiveDeviceId;
    @DexIgnore
    @te4("addresses")
    public /* final */ CommuteAddress mAddress;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_AUTHTYPE)
    public /* final */ String mAuthType;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_BIRTHDAY)
    public /* final */ String mBirthday;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_BRAND)
    public /* final */ String mBrand;
    @DexIgnore
    @te4("createdAt")
    public /* final */ String mCreatedAt;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_DIAGNOSTIC_ENABLE)
    public /* final */ Boolean mDiagnosticEnabled;
    @DexIgnore
    @te4(Constants.EMAIL)
    public /* final */ String mEmail;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_EMAIL_OPT_IN)
    public /* final */ Boolean mEmailOptIn;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_EMAIL_PROGRESS)
    public /* final */ Boolean mEmailProgress;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_EXTERNALID)
    public /* final */ String mExternalId;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_FIRST_NAME)
    public /* final */ String mFirstName;
    @DexIgnore
    @te4("gender")
    public /* final */ String mGender;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_HEIGHT_IN_CM)
    public /* final */ Double mHeightInCentimeters;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_INTEGRATIONS)
    public /* final */ String[] mIntegrations;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_IS_ONBOARDING_COMPLETE)
    public /* final */ Boolean mIsOnboardingComplete;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_LAST_NAME)
    public /* final */ String mLastName;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_PROFILE_PIC)
    public /* final */ String mProfilePicture;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_REGISTER_DATE)
    public /* final */ String mRegisterDate;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_REGISTRATION_COMPLETE)
    public /* final */ Boolean mRegistrationComplete;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_UNIT_GROUP)
    public /* final */ UnitGroup mUnitGroup;
    @DexIgnore
    @te4("updatedAt")
    public /* final */ String mUpdatedAt;
    @DexIgnore
    @te4("useDefaultGoals")
    public /* final */ Boolean mUseDefaultGoals;
    @DexIgnore
    @te4("useDefaultBiometric")
    public /* final */ Boolean mUserDefaultBiometric;
    @DexIgnore
    @te4("id")
    public /* final */ String mUserId;
    @DexIgnore
    @te4("username")
    public /* final */ String mUsername;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_WEIGHT_IN_GRAMS)
    public /* final */ Double mWeightInGrams;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CommuteAddress {
        @DexIgnore
        @te4("home")
        public /* final */ String home;
        @DexIgnore
        @te4("work")
        public /* final */ String work;

        @DexIgnore
        public CommuteAddress(String str, String str2) {
            ee7.b(str, "home");
            ee7.b(str2, "work");
            this.home = str;
            this.work = str2;
        }

        @DexIgnore
        public final String getHome$app_fossilRelease() {
            return this.home;
        }

        @DexIgnore
        public final String getWork$app_fossilRelease() {
            return this.work;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class UnitGroup {
        @DexIgnore
        @te4("distance")
        public /* final */ String distance;
        @DexIgnore
        @te4("height")
        public /* final */ String height;
        @DexIgnore
        @te4("temperature")
        public /* final */ String temperature;
        @DexIgnore
        @te4(Constants.PROFILE_KEY_UNITS_WEIGHT)
        public /* final */ String weight;

        @DexIgnore
        public UnitGroup(String str, String str2, String str3, String str4) {
            ee7.b(str, "distance");
            ee7.b(str2, Constants.PROFILE_KEY_UNITS_WEIGHT);
            ee7.b(str3, "height");
            ee7.b(str4, "temperature");
            this.distance = str;
            this.weight = str2;
            this.height = str3;
            this.temperature = str4;
        }

        @DexIgnore
        public final String getDistance$app_fossilRelease() {
            return this.distance;
        }

        @DexIgnore
        public final String getHeight$app_fossilRelease() {
            return this.height;
        }

        @DexIgnore
        public final String getTemperature$app_fossilRelease() {
            return this.temperature;
        }

        @DexIgnore
        public final String getWeight$app_fossilRelease() {
            return this.weight;
        }
    }

    @DexIgnore
    public UserWrapper(String str, String str2, String str3, String str4, String str5, String str6, UnitGroup unitGroup, CommuteAddress commuteAddress, Boolean bool, Boolean bool2, String str7, String[] strArr, String str8, String str9, String str10, Double d, Double d2, String str11, String str12, String str13, String str14, Boolean bool3, Boolean bool4, Boolean bool5, String str15, Boolean bool6, Boolean bool7) {
        ee7.b(str, "mUserId");
        ee7.b(str2, "mCreatedAt");
        ee7.b(str3, "mUpdatedAt");
        ee7.b(str5, "mAuthType");
        ee7.b(str8, "mFirstName");
        ee7.b(str9, "mLastName");
        ee7.b(str11, "mBirthday");
        this.mUserId = str;
        this.mCreatedAt = str2;
        this.mUpdatedAt = str3;
        this.mUsername = str4;
        this.mAuthType = str5;
        this.mExternalId = str6;
        this.mUnitGroup = unitGroup;
        this.mAddress = commuteAddress;
        this.mEmailProgress = bool;
        this.mEmailOptIn = bool2;
        this.mActiveDeviceId = str7;
        this.mIntegrations = strArr;
        this.mFirstName = str8;
        this.mLastName = str9;
        this.mEmail = str10;
        this.mWeightInGrams = d;
        this.mHeightInCentimeters = d2;
        this.mBirthday = str11;
        this.mGender = str12;
        this.mProfilePicture = str13;
        this.mBrand = str14;
        this.mDiagnosticEnabled = bool3;
        this.mRegistrationComplete = bool4;
        this.mIsOnboardingComplete = bool5;
        this.mRegisterDate = str15;
        this.mUserDefaultBiometric = bool6;
        this.mUseDefaultGoals = bool7;
    }

    @DexIgnore
    public final MFUser toMFUser() {
        String str;
        String str2;
        MFUser mFUser = new MFUser();
        mFUser.setUserId(this.mUserId);
        mFUser.setCreatedAt(this.mCreatedAt);
        mFUser.setUpdatedAt(this.mUpdatedAt);
        String str3 = this.mEmail;
        String str4 = "";
        if (str3 == null) {
            str3 = str4;
        }
        mFUser.setEmail(str3);
        mFUser.setAuthType(this.mAuthType);
        String str5 = this.mUsername;
        if (str5 == null) {
            str5 = str4;
        }
        mFUser.setUsername(str5);
        String str6 = this.mActiveDeviceId;
        if (str6 == null) {
            str6 = str4;
        }
        mFUser.setActiveDeviceId(str6);
        String str7 = this.mFirstName;
        if (str7 == null) {
            str7 = str4;
        }
        mFUser.setFirstName(str7);
        String str8 = this.mLastName;
        if (str8 == null) {
            str8 = str4;
        }
        mFUser.setLastName(str8);
        Double d = this.mWeightInGrams;
        boolean z = false;
        mFUser.setWeightInGrams(d != null ? (int) d.doubleValue() : 0);
        Double d2 = this.mHeightInCentimeters;
        mFUser.setHeightInCentimeters(d2 != null ? (int) d2.doubleValue() : 0);
        Boolean bool = this.mUserDefaultBiometric;
        boolean z2 = true;
        mFUser.setUseDefaultBiometric(bool != null ? bool.booleanValue() : true);
        Boolean bool2 = this.mUseDefaultGoals;
        if (bool2 != null) {
            z2 = bool2.booleanValue();
        }
        mFUser.setUseDefaultGoals(z2);
        if (this.mUnitGroup != null) {
            mFUser.getUnitGroup().setHeight(this.mUnitGroup.getHeight$app_fossilRelease());
            mFUser.getUnitGroup().setWeight(this.mUnitGroup.getWeight$app_fossilRelease());
            mFUser.getUnitGroup().setDistance(this.mUnitGroup.getDistance$app_fossilRelease());
            mFUser.getUnitGroup().setTemperature(this.mUnitGroup.getTemperature$app_fossilRelease());
        }
        MFUser.Address addresses = mFUser.getAddresses();
        CommuteAddress commuteAddress = this.mAddress;
        if (commuteAddress == null || (str = commuteAddress.getHome$app_fossilRelease()) == null) {
            str = str4;
        }
        addresses.setHome(str);
        MFUser.Address addresses2 = mFUser.getAddresses();
        CommuteAddress commuteAddress2 = this.mAddress;
        if (commuteAddress2 == null || (str2 = commuteAddress2.getWork$app_fossilRelease()) == null) {
            str2 = str4;
        }
        addresses2.setWork(str2);
        Boolean bool3 = this.mEmailOptIn;
        mFUser.setEmailOptIn(bool3 != null ? bool3.booleanValue() : false);
        String str9 = this.mRegisterDate;
        if (str9 == null) {
            str9 = str4;
        }
        mFUser.setRegisterDate(str9);
        String str10 = this.mBirthday;
        if (str10 == null) {
            str10 = str4;
        }
        mFUser.setBirthday(str10);
        String str11 = this.mGender;
        if (str11 == null) {
            str11 = "M";
        }
        mFUser.setGender(str11);
        String str12 = this.mProfilePicture;
        if (str12 == null) {
            str12 = str4;
        }
        mFUser.setProfilePicture(str12);
        String str13 = this.mBrand;
        if (str13 != null) {
            str4 = str13;
        }
        mFUser.setBrand(str4);
        Boolean bool4 = this.mDiagnosticEnabled;
        mFUser.setDiagnosticEnabled(bool4 != null ? bool4.booleanValue() : false);
        Boolean bool5 = this.mIsOnboardingComplete;
        mFUser.setOnboardingComplete(bool5 != null ? bool5.booleanValue() : false);
        Boolean bool6 = this.mRegistrationComplete;
        if (bool6 != null) {
            z = bool6.booleanValue();
        }
        mFUser.setRegistrationComplete(z);
        String[] strArr = this.mIntegrations;
        mFUser.setIntegrationsList(strArr != null ? t97.i(strArr) : null);
        return mFUser;
    }
}
