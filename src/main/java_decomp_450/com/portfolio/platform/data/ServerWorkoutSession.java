package com.portfolio.platform.data;

import android.util.Base64;
import com.facebook.places.PlaceManager;
import com.fossil.ac5;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.fitness.GpsDataEncoder;
import com.fossil.fitness.GpsDataPoint;
import com.fossil.i97;
import com.fossil.te4;
import com.fossil.ub5;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.fossil.x97;
import com.fossil.xb5;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.workout.WorkoutCadence;
import com.portfolio.platform.data.model.diana.workout.WorkoutCalorie;
import com.portfolio.platform.data.model.diana.workout.WorkoutDistance;
import com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutPace;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.diana.workout.WorkoutSpeed;
import com.portfolio.platform.data.model.diana.workout.WorkoutStateChange;
import com.portfolio.platform.data.model.diana.workout.WorkoutStep;
import com.portfolio.platform.data.model.fitnessdata.GpsDataPointWrapper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerWorkoutSession extends ServerError {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    @te4("cadence")
    public /* final */ WorkoutCadence cadence;
    @DexIgnore
    @te4(MFSleepSession.COLUMN_EDITED_END_TIME)
    public /* final */ String editedEndTime;
    @DexIgnore
    @te4("editedMode")
    public /* final */ ub5 editedMode;
    @DexIgnore
    @te4(MFSleepSession.COLUMN_EDITED_START_TIME)
    public /* final */ String editedStartTime;
    @DexIgnore
    @te4("editedType")
    public /* final */ ac5 editedType;
    @DexIgnore
    @te4("gpsDataPoints")
    public /* final */ String gpsDataPoints;
    @DexIgnore
    @te4("calorie")
    public /* final */ WorkoutCalorie mCalorie;
    @DexIgnore
    @te4("createdAt")
    public /* final */ DateTime mCreatedAt;
    @DexIgnore
    @te4("date")
    public /* final */ Date mDate;
    @DexIgnore
    @te4(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER)
    public /* final */ String mDeviceSerialNumber;
    @DexIgnore
    @te4("distance")
    public /* final */ WorkoutDistance mDistance;
    @DexIgnore
    @te4("duration")
    public /* final */ int mDuration;
    @DexIgnore
    @te4(SampleRaw.COLUMN_END_TIME)
    public /* final */ String mEndTime;
    @DexIgnore
    @te4("heartRate")
    public /* final */ WorkoutHeartRate mHeartRate;
    @DexIgnore
    @te4("id")
    public /* final */ String mId;
    @DexIgnore
    @te4("source")
    public /* final */ xb5 mSourceType;
    @DexIgnore
    @te4(SampleRaw.COLUMN_START_TIME)
    public /* final */ String mStartTime;
    @DexIgnore
    @te4("step")
    public /* final */ WorkoutStep mStep;
    @DexIgnore
    @te4("timezoneOffset")
    public /* final */ int mTimeZoneOffsetInSecond;
    @DexIgnore
    @te4("updatedAt")
    public /* final */ DateTime mUpdatedAt;
    @DexIgnore
    @te4("type")
    public /* final */ ac5 mWorkoutType;
    @DexIgnore
    @te4("mode")
    public /* final */ ub5 mode;
    @DexIgnore
    @te4("pace")
    public /* final */ WorkoutPace pace;
    @DexIgnore
    @te4(PlaceManager.PARAM_SPEED)
    public WorkoutSpeed speed;
    @DexIgnore
    @te4("states")
    public List<WorkoutStateChange> states;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = ServerWorkoutSession.class.getSimpleName();
        ee7.a((Object) simpleName, "ServerWorkoutSession::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ServerWorkoutSession(String str, WorkoutCalorie workoutCalorie, Date date, String str2, WorkoutDistance workoutDistance, String str3, String str4, WorkoutHeartRate workoutHeartRate, xb5 xb5, WorkoutStep workoutStep, int i, ac5 ac5, DateTime dateTime, DateTime dateTime2, int i2, WorkoutSpeed workoutSpeed, List list, String str5, String str6, ac5 ac52, ub5 ub5, String str7, ub5 ub52, WorkoutPace workoutPace, WorkoutCadence workoutCadence, int i3, zd7 zd7) {
        this(str, workoutCalorie, date, str2, workoutDistance, str3, str4, workoutHeartRate, xb5, workoutStep, i, ac5, dateTime, dateTime2, i2, workoutSpeed, (i3 & 65536) != 0 ? new ArrayList() : list, str5, str6, ac52, ub5, str7, ub52, workoutPace, workoutCadence);
    }

    @DexIgnore
    public final WorkoutCadence getCadence() {
        return this.cadence;
    }

    @DexIgnore
    public final String getEditedEndTime() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final ub5 getEditedMode() {
        return this.editedMode;
    }

    @DexIgnore
    public final String getEditedStartTime() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final ac5 getEditedType() {
        return this.editedType;
    }

    @DexIgnore
    public final String getGpsDataPoints() {
        return this.gpsDataPoints;
    }

    @DexIgnore
    public final WorkoutCalorie getMCalorie() {
        return this.mCalorie;
    }

    @DexIgnore
    public final DateTime getMCreatedAt() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final Date getMDate() {
        return this.mDate;
    }

    @DexIgnore
    public final String getMDeviceSerialNumber() {
        return this.mDeviceSerialNumber;
    }

    @DexIgnore
    public final WorkoutDistance getMDistance() {
        return this.mDistance;
    }

    @DexIgnore
    public final int getMDuration() {
        return this.mDuration;
    }

    @DexIgnore
    public final String getMEndTime() {
        return this.mEndTime;
    }

    @DexIgnore
    public final WorkoutHeartRate getMHeartRate() {
        return this.mHeartRate;
    }

    @DexIgnore
    public final String getMId() {
        return this.mId;
    }

    @DexIgnore
    public final xb5 getMSourceType() {
        return this.mSourceType;
    }

    @DexIgnore
    public final String getMStartTime() {
        return this.mStartTime;
    }

    @DexIgnore
    public final WorkoutStep getMStep() {
        return this.mStep;
    }

    @DexIgnore
    public final int getMTimeZoneOffsetInSecond() {
        return this.mTimeZoneOffsetInSecond;
    }

    @DexIgnore
    public final DateTime getMUpdatedAt() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public final ac5 getMWorkoutType() {
        return this.mWorkoutType;
    }

    @DexIgnore
    public final ub5 getMode() {
        return this.mode;
    }

    @DexIgnore
    public final WorkoutPace getPace() {
        return this.pace;
    }

    @DexIgnore
    public final WorkoutSpeed getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public final List<WorkoutStateChange> getStates() {
        return this.states;
    }

    @DexIgnore
    public final void setSpeed(WorkoutSpeed workoutSpeed) {
        this.speed = workoutSpeed;
    }

    @DexIgnore
    public final void setStates(List<WorkoutStateChange> list) {
        ee7.b(list, "<set-?>");
        this.states = list;
    }

    @DexIgnore
    public final WorkoutSession toWorkoutSession() {
        WorkoutSession workoutSession;
        DateTime dateTime;
        DateTime dateTime2;
        List list;
        try {
            DateTimeFormatter withZone = ISODateTimeFormat.dateTime().withZone(DateTimeZone.forOffsetMillis(this.mTimeZoneOffsetInSecond * 1000));
            DateTime parseDateTime = withZone.parseDateTime(this.mStartTime);
            DateTime parseDateTime2 = withZone.parseDateTime(this.mEndTime);
            if (this.editedStartTime == null || (dateTime = withZone.parseDateTime(this.editedStartTime)) == null) {
                dateTime = parseDateTime;
            }
            if (this.editedEndTime == null || (dateTime2 = withZone.parseDateTime(this.editedEndTime)) == null) {
                dateTime2 = parseDateTime2;
            }
            ac5 ac5 = this.editedType;
            if (ac5 == null) {
                ac5 = this.mWorkoutType;
            }
            ub5 ub5 = this.editedMode;
            if (ub5 == null) {
                ub5 = this.mode;
            }
            String str = this.gpsDataPoints;
            if (str != null) {
                byte[] decode = Base64.decode(str, 0);
                IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                FLogger.Component component = FLogger.Component.APP;
                FLogger.Session session = FLogger.Session.SYNC;
                StringBuilder sb = new StringBuilder();
                sb.append("[Calculate GPS Points] Encoded GPS string from server ");
                sb.append(decode != null ? Integer.valueOf(decode.length) : null);
                sb.append(' ');
                remote.i(component, session, "", "WorkoutSessionWrapper", sb.toString());
                ArrayList<GpsDataPoint> decode2 = GpsDataEncoder.decode(decode);
                ee7.a((Object) decode2, "GpsDataEncoder.decode(encodedRawData)");
                ArrayList arrayList = new ArrayList(x97.a(decode2, 10));
                for (T t : decode2) {
                    ee7.a((Object) t, "it");
                    arrayList.add(new WorkoutGpsPoint(new GpsDataPointWrapper(t, this.mTimeZoneOffsetInSecond)));
                }
                list = ea7.d((Collection) arrayList);
            } else {
                list = null;
            }
            IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
            FLogger.Component component2 = FLogger.Component.APP;
            FLogger.Session session2 = FLogger.Session.SYNC;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("[Calculate GPS Points] GPS points size after decrypt from server ");
            sb2.append(list != null ? Integer.valueOf(list.size()) : null);
            sb2.append(' ');
            remote2.i(component2, session2, "", "WorkoutSessionWrapper", sb2.toString());
            String str2 = this.mId;
            Date date = this.mDate;
            ee7.a((Object) parseDateTime, SampleRaw.COLUMN_START_TIME);
            ee7.a((Object) parseDateTime2, SampleRaw.COLUMN_END_TIME);
            String str3 = this.mDeviceSerialNumber;
            WorkoutStep workoutStep = this.mStep;
            WorkoutCalorie workoutCalorie = this.mCalorie;
            WorkoutDistance workoutDistance = this.mDistance;
            WorkoutHeartRate workoutHeartRate = this.mHeartRate;
            xb5 xb5 = this.mSourceType;
            ac5 ac52 = this.mWorkoutType;
            int i = this.mTimeZoneOffsetInSecond;
            int i2 = this.mDuration;
            long millis = this.mCreatedAt.getMillis();
            long millis2 = this.mUpdatedAt.getMillis();
            String str4 = this.gpsDataPoints;
            ub5 ub52 = this.mode;
            WorkoutPace workoutPace = this.pace;
            WorkoutCadence workoutCadence = this.cadence;
            ee7.a((Object) dateTime, MFSleepSession.COLUMN_EDITED_START_TIME);
            ee7.a((Object) dateTime2, MFSleepSession.COLUMN_EDITED_END_TIME);
            WorkoutSession workoutSession2 = new WorkoutSession(str2, date, parseDateTime, parseDateTime2, str3, workoutStep, workoutCalorie, workoutDistance, workoutHeartRate, xb5, ac52, i, i2, millis, millis2, list, str4, ub52, workoutPace, workoutCadence, dateTime, dateTime2, ac5, ub5);
            try {
                workoutSession = workoutSession2;
                try {
                    workoutSession.setSpeed(this.speed);
                    workoutSession.getStates().addAll(this.states);
                } catch (Exception e) {
                    e = e;
                }
            } catch (Exception e2) {
                e = e2;
                workoutSession = workoutSession2;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str5 = TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("toWorkoutSession exception=");
                e.printStackTrace();
                sb3.append(i97.a);
                local.d(str5, sb3.toString());
                e.printStackTrace();
                return workoutSession;
            }
        } catch (Exception e3) {
            e = e3;
            workoutSession = null;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str52 = TAG;
            StringBuilder sb32 = new StringBuilder();
            sb32.append("toWorkoutSession exception=");
            e.printStackTrace();
            sb32.append(i97.a);
            local2.d(str52, sb32.toString());
            e.printStackTrace();
            return workoutSession;
        }
        return workoutSession;
    }

    @DexIgnore
    public ServerWorkoutSession(String str, WorkoutCalorie workoutCalorie, Date date, String str2, WorkoutDistance workoutDistance, String str3, String str4, WorkoutHeartRate workoutHeartRate, xb5 xb5, WorkoutStep workoutStep, int i, ac5 ac5, DateTime dateTime, DateTime dateTime2, int i2, WorkoutSpeed workoutSpeed, List<WorkoutStateChange> list, String str5, String str6, ac5 ac52, ub5 ub5, String str7, ub5 ub52, WorkoutPace workoutPace, WorkoutCadence workoutCadence) {
        ee7.b(str, "mId");
        ee7.b(date, "mDate");
        ee7.b(str3, "mStartTime");
        ee7.b(str4, "mEndTime");
        ee7.b(dateTime, "mCreatedAt");
        ee7.b(dateTime2, "mUpdatedAt");
        ee7.b(list, "states");
        this.mId = str;
        this.mCalorie = workoutCalorie;
        this.mDate = date;
        this.mDeviceSerialNumber = str2;
        this.mDistance = workoutDistance;
        this.mStartTime = str3;
        this.mEndTime = str4;
        this.mHeartRate = workoutHeartRate;
        this.mSourceType = xb5;
        this.mStep = workoutStep;
        this.mTimeZoneOffsetInSecond = i;
        this.mWorkoutType = ac5;
        this.mCreatedAt = dateTime;
        this.mUpdatedAt = dateTime2;
        this.mDuration = i2;
        this.speed = workoutSpeed;
        this.states = list;
        this.editedStartTime = str5;
        this.editedEndTime = str6;
        this.editedType = ac52;
        this.editedMode = ub5;
        this.gpsDataPoints = str7;
        this.mode = ub52;
        this.pace = workoutPace;
        this.cadence = workoutCadence;
    }
}
