package com.portfolio.platform.data.source;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.kv4;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.model.UserSettings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserSettingDao_Impl implements UserSettingDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<UserSettings> __insertionAdapterOfUserSettings;
    @DexIgnore
    public /* final */ ji __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ kv4 __userSettingConverter; // = new kv4();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<UserSettings> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `userSettings` (`isShowGoalRing`,`acceptedLocationDataSharing`,`acceptedPrivacies`,`acceptedTermsOfService`,`uid`,`id`,`isLatestLocationDataSharingAccepted`,`isLatestPrivacyAccepted`,`isLatestTermsOfServiceAccepted`,`latestLocationDataSharingVersion`,`latestPrivacyVersion`,`latestTermsOfServiceVersion`,`startDayOfWeek`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, UserSettings userSettings) {
            ajVar.bindLong(1, userSettings.isShowGoalRing() ? 1 : 0);
            String a = UserSettingDao_Impl.this.__userSettingConverter.a(userSettings.getAcceptedLocationDataSharing());
            if (a == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, a);
            }
            String a2 = UserSettingDao_Impl.this.__userSettingConverter.a(userSettings.getAcceptedPrivacies());
            if (a2 == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, a2);
            }
            String a3 = UserSettingDao_Impl.this.__userSettingConverter.a(userSettings.getAcceptedTermsOfService());
            if (a3 == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, a3);
            }
            if (userSettings.getUid() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, userSettings.getUid());
            }
            if (userSettings.getId() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, userSettings.getId());
            }
            ajVar.bindLong(7, userSettings.isLatestLocationDataSharingAccepted() ? 1 : 0);
            ajVar.bindLong(8, userSettings.isLatestPrivacyAccepted() ? 1 : 0);
            ajVar.bindLong(9, userSettings.isLatestTermsOfServiceAccepted() ? 1 : 0);
            if (userSettings.getLatestLocationDataSharingVersion() == null) {
                ajVar.bindNull(10);
            } else {
                ajVar.bindString(10, userSettings.getLatestLocationDataSharingVersion());
            }
            if (userSettings.getLatestPrivacyVersion() == null) {
                ajVar.bindNull(11);
            } else {
                ajVar.bindString(11, userSettings.getLatestPrivacyVersion());
            }
            if (userSettings.getLatestTermsOfServiceVersion() == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, userSettings.getLatestTermsOfServiceVersion());
            }
            if (userSettings.getStartDayOfWeek() == null) {
                ajVar.bindNull(13);
            } else {
                ajVar.bindString(13, userSettings.getStartDayOfWeek());
            }
            if (userSettings.getCreatedAt() == null) {
                ajVar.bindNull(14);
            } else {
                ajVar.bindString(14, userSettings.getCreatedAt());
            }
            if (userSettings.getUpdatedAt() == null) {
                ajVar.bindNull(15);
            } else {
                ajVar.bindString(15, userSettings.getUpdatedAt());
            }
            ajVar.bindLong(16, (long) userSettings.getPinType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM userSettings";
        }
    }

    @DexIgnore
    public UserSettingDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfUserSettings = new Anon1(ciVar);
        this.__preparedStmtOfCleanUp = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDao
    public void addOrUpdateUserSetting(UserSettings userSettings) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfUserSettings.insert(userSettings);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDao
    public UserSettings getCurrentUserSetting() {
        fi fiVar;
        UserSettings userSettings;
        fi b = fi.b("SELECT * FROM userSettings LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "isShowGoalRing");
            int b3 = oi.b(a, "acceptedLocationDataSharing");
            int b4 = oi.b(a, "acceptedPrivacies");
            int b5 = oi.b(a, "acceptedTermsOfService");
            int b6 = oi.b(a, "uid");
            int b7 = oi.b(a, "id");
            int b8 = oi.b(a, "isLatestLocationDataSharingAccepted");
            int b9 = oi.b(a, "isLatestPrivacyAccepted");
            int b10 = oi.b(a, "isLatestTermsOfServiceAccepted");
            int b11 = oi.b(a, "latestLocationDataSharingVersion");
            int b12 = oi.b(a, "latestPrivacyVersion");
            int b13 = oi.b(a, "latestTermsOfServiceVersion");
            int b14 = oi.b(a, "startDayOfWeek");
            fiVar = b;
            try {
                int b15 = oi.b(a, "createdAt");
                int b16 = oi.b(a, "updatedAt");
                int b17 = oi.b(a, "pinType");
                if (a.moveToFirst()) {
                    UserSettings userSettings2 = new UserSettings();
                    userSettings2.setShowGoalRing(a.getInt(b2) != 0);
                    userSettings2.setAcceptedLocationDataSharing(this.__userSettingConverter.a(a.getString(b3)));
                    userSettings2.setAcceptedPrivacies(this.__userSettingConverter.a(a.getString(b4)));
                    userSettings2.setAcceptedTermsOfService(this.__userSettingConverter.a(a.getString(b5)));
                    userSettings2.setUid(a.getString(b6));
                    userSettings2.setId(a.getString(b7));
                    userSettings2.setLatestLocationDataSharingAccepted(a.getInt(b8) != 0);
                    userSettings2.setLatestPrivacyAccepted(a.getInt(b9) != 0);
                    userSettings2.setLatestTermsOfServiceAccepted(a.getInt(b10) != 0);
                    userSettings2.setLatestLocationDataSharingVersion(a.getString(b11));
                    userSettings2.setLatestPrivacyVersion(a.getString(b12));
                    userSettings2.setLatestTermsOfServiceVersion(a.getString(b13));
                    userSettings2.setStartDayOfWeek(a.getString(b14));
                    userSettings2.setCreatedAt(a.getString(b15));
                    userSettings2.setUpdatedAt(a.getString(b16));
                    userSettings2.setPinType(a.getInt(b17));
                    userSettings = userSettings2;
                } else {
                    userSettings = null;
                }
                a.close();
                fiVar.c();
                return userSettings;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDao
    public UserSettings getPendingUserSetting() {
        fi fiVar;
        UserSettings userSettings;
        fi b = fi.b("SELECT * FROM userSettings WHERE pinType = 1 LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "isShowGoalRing");
            int b3 = oi.b(a, "acceptedLocationDataSharing");
            int b4 = oi.b(a, "acceptedPrivacies");
            int b5 = oi.b(a, "acceptedTermsOfService");
            int b6 = oi.b(a, "uid");
            int b7 = oi.b(a, "id");
            int b8 = oi.b(a, "isLatestLocationDataSharingAccepted");
            int b9 = oi.b(a, "isLatestPrivacyAccepted");
            int b10 = oi.b(a, "isLatestTermsOfServiceAccepted");
            int b11 = oi.b(a, "latestLocationDataSharingVersion");
            int b12 = oi.b(a, "latestPrivacyVersion");
            int b13 = oi.b(a, "latestTermsOfServiceVersion");
            int b14 = oi.b(a, "startDayOfWeek");
            fiVar = b;
            try {
                int b15 = oi.b(a, "createdAt");
                int b16 = oi.b(a, "updatedAt");
                int b17 = oi.b(a, "pinType");
                if (a.moveToFirst()) {
                    UserSettings userSettings2 = new UserSettings();
                    userSettings2.setShowGoalRing(a.getInt(b2) != 0);
                    userSettings2.setAcceptedLocationDataSharing(this.__userSettingConverter.a(a.getString(b3)));
                    userSettings2.setAcceptedPrivacies(this.__userSettingConverter.a(a.getString(b4)));
                    userSettings2.setAcceptedTermsOfService(this.__userSettingConverter.a(a.getString(b5)));
                    userSettings2.setUid(a.getString(b6));
                    userSettings2.setId(a.getString(b7));
                    userSettings2.setLatestLocationDataSharingAccepted(a.getInt(b8) != 0);
                    userSettings2.setLatestPrivacyAccepted(a.getInt(b9) != 0);
                    userSettings2.setLatestTermsOfServiceAccepted(a.getInt(b10) != 0);
                    userSettings2.setLatestLocationDataSharingVersion(a.getString(b11));
                    userSettings2.setLatestPrivacyVersion(a.getString(b12));
                    userSettings2.setLatestTermsOfServiceVersion(a.getString(b13));
                    userSettings2.setStartDayOfWeek(a.getString(b14));
                    userSettings2.setCreatedAt(a.getString(b15));
                    userSettings2.setUpdatedAt(a.getString(b16));
                    userSettings2.setPinType(a.getInt(b17));
                    userSettings = userSettings2;
                } else {
                    userSettings = null;
                }
                a.close();
                fiVar.c();
                return userSettings;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }
}
