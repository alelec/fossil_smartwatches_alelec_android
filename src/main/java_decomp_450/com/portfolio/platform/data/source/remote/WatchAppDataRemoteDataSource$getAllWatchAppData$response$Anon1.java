package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.zb7;
import com.portfolio.platform.data.model.diana.WatchAppData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource$getAllWatchAppData$response$1", f = "WatchAppDataRemoteDataSource.kt", l = {14}, m = "invokeSuspend")
public final class WatchAppDataRemoteDataSource$getAllWatchAppData$response$Anon1 extends zb7 implements gd7<fb7<? super fv7<ApiResponse<WatchAppData>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $appVersion;
    @DexIgnore
    public /* final */ /* synthetic */ String $firmwareVersion;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppDataRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppDataRemoteDataSource$getAllWatchAppData$response$Anon1(WatchAppDataRemoteDataSource watchAppDataRemoteDataSource, String str, String str2, fb7 fb7) {
        super(1, fb7);
        this.this$0 = watchAppDataRemoteDataSource;
        this.$appVersion = str;
        this.$firmwareVersion = str2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(fb7<?> fb7) {
        ee7.b(fb7, "completion");
        return new WatchAppDataRemoteDataSource$getAllWatchAppData$response$Anon1(this.this$0, this.$appVersion, this.$firmwareVersion, fb7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public final Object invoke(fb7<? super fv7<ApiResponse<WatchAppData>>> fb7) {
        return ((WatchAppDataRemoteDataSource$getAllWatchAppData$response$Anon1) create(fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            ApiServiceV2 access$getMApiServiceV2$p = this.this$0.mApiServiceV2;
            String str = this.$appVersion;
            String str2 = this.$firmwareVersion;
            this.label = 1;
            obj = access$getMApiServiceV2$p.getAllWatchAppData(str, str2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
