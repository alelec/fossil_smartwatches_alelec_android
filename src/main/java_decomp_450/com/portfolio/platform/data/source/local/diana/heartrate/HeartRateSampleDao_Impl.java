package com.portfolio.platform.data.source.local.diana.heartrate;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.dv4;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.mu4;
import com.fossil.oi;
import com.fossil.ou4;
import com.fossil.pi;
import com.fossil.vh;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSampleDao_Impl extends HeartRateSampleDao {
    @DexIgnore
    public /* final */ mu4 __dateShortStringConverter; // = new mu4();
    @DexIgnore
    public /* final */ ou4 __dateTimeISOStringConverter; // = new ou4();
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<HeartRateSample> __insertionAdapterOfHeartRateSample;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteAllHeartRateSamples;
    @DexIgnore
    public /* final */ dv4 __restingConverter; // = new dv4();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<HeartRateSample> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `heart_rate_sample` (`id`,`average`,`date`,`createdAt`,`updatedAt`,`endTime`,`startTime`,`timezoneOffset`,`min`,`max`,`minuteCount`,`resting`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, HeartRateSample heartRateSample) {
            if (heartRateSample.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, heartRateSample.getId());
            }
            ajVar.bindDouble(2, (double) heartRateSample.getAverage());
            String a = HeartRateSampleDao_Impl.this.__dateShortStringConverter.a(heartRateSample.getDate());
            if (a == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, a);
            }
            ajVar.bindLong(4, heartRateSample.getCreatedAt());
            ajVar.bindLong(5, heartRateSample.getUpdatedAt());
            String a2 = HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(heartRateSample.getEndTime());
            if (a2 == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, a2);
            }
            String a3 = HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(heartRateSample.getStartTime());
            if (a3 == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, a3);
            }
            ajVar.bindLong(8, (long) heartRateSample.getTimezoneOffsetInSecond());
            ajVar.bindLong(9, (long) heartRateSample.getMin());
            ajVar.bindLong(10, (long) heartRateSample.getMax());
            ajVar.bindLong(11, (long) heartRateSample.getMinuteCount());
            String a4 = HeartRateSampleDao_Impl.this.__restingConverter.a(heartRateSample.getResting());
            if (a4 == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, a4);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM heart_rate_sample";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<HeartRateSample>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon3(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<HeartRateSample> call() throws Exception {
            Cursor a = pi.a(HeartRateSampleDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "id");
                int b2 = oi.b(a, GoalTrackingSummary.COLUMN_AVERAGE);
                int b3 = oi.b(a, "date");
                int b4 = oi.b(a, "createdAt");
                int b5 = oi.b(a, "updatedAt");
                int b6 = oi.b(a, SampleRaw.COLUMN_END_TIME);
                int b7 = oi.b(a, SampleRaw.COLUMN_START_TIME);
                int b8 = oi.b(a, "timezoneOffset");
                int b9 = oi.b(a, "min");
                int b10 = oi.b(a, "max");
                int b11 = oi.b(a, "minuteCount");
                int b12 = oi.b(a, "resting");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new HeartRateSample(a.getString(b), a.getFloat(b2), HeartRateSampleDao_Impl.this.__dateShortStringConverter.a(a.getString(b3)), a.getLong(b4), a.getLong(b5), HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b6)), HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b7)), a.getInt(b8), a.getInt(b9), a.getInt(b10), a.getInt(b11), HeartRateSampleDao_Impl.this.__restingConverter.a(a.getString(b12))));
                    b = b;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public HeartRateSampleDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfHeartRateSample = new Anon1(ciVar);
        this.__preparedStmtOfDeleteAllHeartRateSamples = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public void deleteAllHeartRateSamples() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteAllHeartRateSamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllHeartRateSamples.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public HeartRateSample getHeartRateSample(String str) {
        fi b = fi.b("SELECT * FROM heart_rate_sample WHERE id = ?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        HeartRateSample heartRateSample = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, GoalTrackingSummary.COLUMN_AVERAGE);
            int b4 = oi.b(a, "date");
            int b5 = oi.b(a, "createdAt");
            int b6 = oi.b(a, "updatedAt");
            int b7 = oi.b(a, SampleRaw.COLUMN_END_TIME);
            int b8 = oi.b(a, SampleRaw.COLUMN_START_TIME);
            int b9 = oi.b(a, "timezoneOffset");
            int b10 = oi.b(a, "min");
            int b11 = oi.b(a, "max");
            int b12 = oi.b(a, "minuteCount");
            int b13 = oi.b(a, "resting");
            if (a.moveToFirst()) {
                heartRateSample = new HeartRateSample(a.getString(b2), a.getFloat(b3), this.__dateShortStringConverter.a(a.getString(b4)), a.getLong(b5), a.getLong(b6), this.__dateTimeISOStringConverter.a(a.getString(b7)), this.__dateTimeISOStringConverter.a(a.getString(b8)), a.getInt(b9), a.getInt(b10), a.getInt(b11), a.getInt(b12), this.__restingConverter.a(a.getString(b13)));
            }
            return heartRateSample;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public LiveData<List<HeartRateSample>> getHeartRateSamples(Date date, Date date2) {
        fi b = fi.b("SELECT * FROM heart_rate_sample WHERE date >= ? AND date <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"heart_rate_sample"}, false, (Callable) new Anon3(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public void insertHeartRateSample(HeartRateSample heartRateSample) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHeartRateSample.insert(heartRateSample);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public void upsertHeartRateSampleList(List<HeartRateSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHeartRateSample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
