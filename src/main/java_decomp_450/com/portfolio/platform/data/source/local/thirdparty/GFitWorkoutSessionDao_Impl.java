package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.tu4;
import com.fossil.uh;
import com.fossil.uu4;
import com.fossil.vh;
import com.fossil.vu4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wu4;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitWorkoutSessionDao_Impl implements GFitWorkoutSessionDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ uh<GFitWorkoutSession> __deletionAdapterOfGFitWorkoutSession;
    @DexIgnore
    public /* final */ tu4 __gFitWOCaloriesConverter; // = new tu4();
    @DexIgnore
    public /* final */ uu4 __gFitWODistancesConverter; // = new uu4();
    @DexIgnore
    public /* final */ vu4 __gFitWOHeartRatesConverter; // = new vu4();
    @DexIgnore
    public /* final */ wu4 __gFitWOStepsConverter; // = new wu4();
    @DexIgnore
    public /* final */ vh<GFitWorkoutSession> __insertionAdapterOfGFitWorkoutSession;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<GFitWorkoutSession> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitWorkoutSession` (`id`,`startTime`,`endTime`,`workoutType`,`steps`,`calories`,`distances`,`heartRates`) VALUES (nullif(?, 0),?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, GFitWorkoutSession gFitWorkoutSession) {
            ajVar.bindLong(1, (long) gFitWorkoutSession.getId());
            ajVar.bindLong(2, gFitWorkoutSession.getStartTime());
            ajVar.bindLong(3, gFitWorkoutSession.getEndTime());
            ajVar.bindLong(4, (long) gFitWorkoutSession.getWorkoutType());
            String a = GFitWorkoutSessionDao_Impl.this.__gFitWOStepsConverter.a(gFitWorkoutSession.getSteps());
            if (a == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a);
            }
            String a2 = GFitWorkoutSessionDao_Impl.this.__gFitWOCaloriesConverter.a(gFitWorkoutSession.getCalories());
            if (a2 == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, a2);
            }
            String a3 = GFitWorkoutSessionDao_Impl.this.__gFitWODistancesConverter.a(gFitWorkoutSession.getDistances());
            if (a3 == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, a3);
            }
            String a4 = GFitWorkoutSessionDao_Impl.this.__gFitWOHeartRatesConverter.a(gFitWorkoutSession.getHeartRates());
            if (a4 == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, a4);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends uh<GFitWorkoutSession> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.uh, com.fossil.ji
        public String createQuery() {
            return "DELETE FROM `gFitWorkoutSession` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(aj ajVar, GFitWorkoutSession gFitWorkoutSession) {
            ajVar.bindLong(1, (long) gFitWorkoutSession.getId());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM gFitWorkoutSession";
        }
    }

    @DexIgnore
    public GFitWorkoutSessionDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfGFitWorkoutSession = new Anon1(ciVar);
        this.__deletionAdapterOfGFitWorkoutSession = new Anon2(ciVar);
        this.__preparedStmtOfClearAll = new Anon3(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public void deleteGFitWorkoutSession(GFitWorkoutSession gFitWorkoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitWorkoutSession.handle(gFitWorkoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public List<GFitWorkoutSession> getAllGFitWorkoutSession() {
        fi b = fi.b("SELECT * FROM gFitWorkoutSession", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, SampleRaw.COLUMN_START_TIME);
            int b4 = oi.b(a, SampleRaw.COLUMN_END_TIME);
            int b5 = oi.b(a, "workoutType");
            int b6 = oi.b(a, "steps");
            int b7 = oi.b(a, "calories");
            int b8 = oi.b(a, "distances");
            int b9 = oi.b(a, "heartRates");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GFitWorkoutSession gFitWorkoutSession = new GFitWorkoutSession(a.getLong(b3), a.getLong(b4), a.getInt(b5), this.__gFitWOStepsConverter.a(a.getString(b6)), this.__gFitWOCaloriesConverter.a(a.getString(b7)), this.__gFitWODistancesConverter.a(a.getString(b8)), this.__gFitWOHeartRatesConverter.a(a.getString(b9)));
                gFitWorkoutSession.setId(a.getInt(b2));
                arrayList.add(gFitWorkoutSession);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public void insertGFitWorkoutSession(GFitWorkoutSession gFitWorkoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitWorkoutSession.insert(gFitWorkoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public void insertListGFitWorkoutSession(List<GFitWorkoutSession> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitWorkoutSession.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
