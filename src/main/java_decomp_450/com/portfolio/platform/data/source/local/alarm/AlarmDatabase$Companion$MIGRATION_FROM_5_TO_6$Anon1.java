package com.portfolio.platform.data.source.local.alarm;

import com.fossil.ee7;
import com.fossil.li;
import com.fossil.wi;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDatabase$Companion$MIGRATION_FROM_5_TO_6$Anon1 extends li {
    @DexIgnore
    public AlarmDatabase$Companion$MIGRATION_FROM_5_TO_6$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.li
    public void migrate(wi wiVar) {
        ee7.b(wiVar, "database");
        FLogger.INSTANCE.getLocal().d(AlarmDatabase.TAG, "MIGRATION_FROM_5_TO_6 - start");
        wiVar.beginTransaction();
        try {
            FLogger.INSTANCE.getLocal().d(AlarmDatabase.TAG, "Migrate Alarm - start");
            wiVar.execSQL("ALTER TABLE alarm ADD COLUMN message TEXT NOT NULL DEFAULT ''");
            FLogger.INSTANCE.getLocal().d(AlarmDatabase.TAG, "Migrate Alarm - end");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = AlarmDatabase.TAG;
            local.e(access$getTAG$cp, "MIGRATION_FROM_5_TO_6 - end with exception -- e=" + e);
        }
        wiVar.setTransactionSuccessful();
        wiVar.endTransaction();
    }
}
