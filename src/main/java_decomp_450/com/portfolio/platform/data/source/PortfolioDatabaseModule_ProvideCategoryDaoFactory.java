package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.local.CategoryDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideCategoryDaoFactory implements Factory<CategoryDao> {
    @DexIgnore
    public /* final */ Provider<CategoryDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideCategoryDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<CategoryDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideCategoryDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<CategoryDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideCategoryDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static CategoryDao provideCategoryDao(PortfolioDatabaseModule portfolioDatabaseModule, CategoryDatabase categoryDatabase) {
        CategoryDao provideCategoryDao = portfolioDatabaseModule.provideCategoryDao(categoryDatabase);
        c87.a(provideCategoryDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideCategoryDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public CategoryDao get() {
        return provideCategoryDao(this.module, this.dbProvider.get());
    }
}
