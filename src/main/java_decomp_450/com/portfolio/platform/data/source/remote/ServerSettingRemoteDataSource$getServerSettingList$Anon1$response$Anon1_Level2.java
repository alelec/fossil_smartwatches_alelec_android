package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.zb7;
import com.portfolio.platform.data.model.ServerSettingList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource$getServerSettingList$1$response$1", f = "ServerSettingRemoteDataSource.kt", l = {32}, m = "invokeSuspend")
public final class ServerSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2 extends zb7 implements gd7<fb7<? super fv7<ServerSettingList>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRemoteDataSource$getServerSettingList$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ServerSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2(ServerSettingRemoteDataSource$getServerSettingList$Anon1 serverSettingRemoteDataSource$getServerSettingList$Anon1, fb7 fb7) {
        super(1, fb7);
        this.this$0 = serverSettingRemoteDataSource$getServerSettingList$Anon1;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(fb7<?> fb7) {
        ee7.b(fb7, "completion");
        return new ServerSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2(this.this$0, fb7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public final Object invoke(fb7<? super fv7<ServerSettingList>> fb7) {
        return ((ServerSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2) create(fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            ApiServiceV2 access$getApiService$p = this.this$0.this$0.apiService;
            this.label = 1;
            obj = access$getApiService$p.getServerSettingList(20, 0, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
