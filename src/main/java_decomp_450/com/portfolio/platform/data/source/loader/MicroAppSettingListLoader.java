package com.portfolio.platform.data.source.loader;

import android.content.Context;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MicroAppSettingListLoader extends BaseLoader<List<MicroAppSetting>> implements MicroAppSettingRepository.MicroAppSettingRepositoryObserver {
    @DexIgnore
    public static /* final */ String TAG; // = "MicroAppSettingListLoader";
    @DexIgnore
    public /* final */ MicroAppSettingRepository mRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 implements MicroAppSettingDataSource.MicroAppSettingListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ List val$result;

        @DexIgnore
        public Anon1(List list) {
            this.val$result = list;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource.MicroAppSettingListCallback
        public void onFail() {
            MFLogger.d(MicroAppSettingListLoader.TAG, "executeUseCase onFail");
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource.MicroAppSettingListCallback
        public void onSuccess(List<MicroAppSetting> list) {
            MFLogger.d(MicroAppSettingListLoader.TAG, "executeUseCase onSuccess");
            this.val$result.addAll(list);
        }
    }

    @DexIgnore
    public MicroAppSettingListLoader(Context context, MicroAppSettingRepository microAppSettingRepository) {
        super(context);
        this.mRepository = microAppSettingRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository.MicroAppSettingRepositoryObserver
    public void onMicroAppChanged() {
        if (isStarted()) {
            MFLogger.d(TAG, "Force load micro app settings");
            forceLoad();
        }
    }

    @DexIgnore
    @Override // com.fossil.oe, com.portfolio.platform.data.source.loader.BaseLoader
    public void onReset() {
        this.mRepository.removeMicroAppSettingRepositoryObserver(this);
        super.onReset();
    }

    @DexIgnore
    @Override // com.fossil.oe
    public void onStartLoading() {
        String str = TAG;
        MFLogger.d(str, "Inside onStartLoading isCachedAvailable=" + this.mRepository.isCachedSettingListAvailable());
        if (this.mRepository.isCachedSettingListAvailable()) {
            deliverResult(this.mRepository.getCachedSettingList());
        }
        this.mRepository.addMicroAppSettingRepositoryObserver(this);
        if (takeContentChanged() || !this.mRepository.isCachedSettingListAvailable()) {
            MFLogger.d(TAG, "Inside onStartLoading forceReload");
            forceLoad();
        }
    }

    @DexIgnore
    public void setMicroAppId(String str) {
        String str2 = TAG;
        MFLogger.d(str2, "setMicroAppId microAppId=" + str);
    }

    @DexIgnore
    @Override // com.fossil.me
    public List<MicroAppSetting> loadInBackground() {
        MFLogger.d(TAG, "loadInBackground");
        String str = TAG;
        MFLogger.d(str, "Inside loadInBackground in thread=" + Thread.currentThread().getName());
        ArrayList arrayList = new ArrayList();
        this.mRepository.getMicroAppSettingListInDB(new Anon1(arrayList));
        MFLogger.d(TAG, "After call get micro app setting in db");
        return arrayList;
    }
}
