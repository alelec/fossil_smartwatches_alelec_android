package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.fossil.co4;
import com.fossil.zo4;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesNotificationLocalFactory implements Factory<zo4> {
    @DexIgnore
    public /* final */ Provider<co4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesNotificationLocalFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<co4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesNotificationLocalFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<co4> provider) {
        return new PortfolioDatabaseModule_ProvidesNotificationLocalFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static zo4 providesNotificationLocal(PortfolioDatabaseModule portfolioDatabaseModule, co4 co4) {
        zo4 providesNotificationLocal = portfolioDatabaseModule.providesNotificationLocal(co4);
        c87.a(providesNotificationLocal, "Cannot return null from a non-@Nullable @Provides method");
        return providesNotificationLocal;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public zo4 get() {
        return providesNotificationLocal(this.module, this.daoProvider.get());
    }
}
