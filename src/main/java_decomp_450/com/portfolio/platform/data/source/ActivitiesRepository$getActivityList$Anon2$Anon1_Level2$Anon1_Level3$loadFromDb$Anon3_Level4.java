package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.ge;
import com.fossil.t3;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon3_Level4<I, O> implements t3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository$getActivityList$Anon2.Anon1_Level2.Anon1_Level3 this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level5<I, O> implements t3<X, Y> {
        @DexIgnore
        public /* final */ /* synthetic */ List $resultList;
        @DexIgnore
        public /* final */ /* synthetic */ ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon3_Level4 this$0;

        @DexIgnore
        public Anon1_Level5(ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon3_Level4 activitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon3_Level4, List list) {
            this.this$0 = activitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon3_Level4;
            this.$resultList = list;
        }

        @DexIgnore
        public final List<ActivitySample> apply(List<ActivitySample> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ActivitiesRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getActivityList getActivityList Transformations activitySamples=" + list);
            ee7.a((Object) list, "activitySamples");
            if (!list.isEmpty()) {
                List list2 = this.$resultList;
                this.this$0.this$0.this$0.this$0.this$0.mFitnessHelper.a(list, list.get(0).getUid());
                list2.addAll(list);
            }
            return this.$resultList;
        }
    }

    @DexIgnore
    public ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon3_Level4(ActivitiesRepository$getActivityList$Anon2.Anon1_Level2.Anon1_Level3 anon1_Level3) {
        this.this$0 = anon1_Level3;
    }

    @DexIgnore
    public final LiveData<List<ActivitySample>> apply(List<ActivitySample> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tAG$app_fossilRelease = ActivitiesRepository.Companion.getTAG$app_fossilRelease();
        local.d(tAG$app_fossilRelease, "getActivityList loadFromDb: resultList=" + list);
        ActivitiesRepository$getActivityList$Anon2.Anon1_Level2 anon1_Level2 = this.this$0.this$0;
        ActivitiesRepository activitiesRepository = anon1_Level2.this$0.this$0;
        Date date = anon1_Level2.$endDate;
        ee7.a((Object) date, GoalPhase.COLUMN_END_DATE);
        return ge.a(activitiesRepository.getActivitySamplesInDate(date, this.this$0.this$0.$fitnessDatabase.activitySampleDao()), new Anon1_Level5(this, list));
    }
}
