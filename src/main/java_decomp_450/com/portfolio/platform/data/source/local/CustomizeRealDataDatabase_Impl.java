package com.portfolio.platform.data.source.local;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeRealDataDatabase_Impl extends CustomizeRealDataDatabase {
    @DexIgnore
    public volatile CustomizeRealDataDao _customizeRealDataDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `customizeRealData` (`id` TEXT NOT NULL, `value` TEXT NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '6ca914ed7b753a02dc3ff84030dc3e24')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `customizeRealData`");
            if (((ci) CustomizeRealDataDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) CustomizeRealDataDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) CustomizeRealDataDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) CustomizeRealDataDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) CustomizeRealDataDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) CustomizeRealDataDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) CustomizeRealDataDatabase_Impl.this).mDatabase = wiVar;
            CustomizeRealDataDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) CustomizeRealDataDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) CustomizeRealDataDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) CustomizeRealDataDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(2);
            hashMap.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap.put("value", new ti.a("value", "TEXT", true, 0, null, 1));
            ti tiVar = new ti("customizeRealData", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "customizeRealData");
            if (tiVar.equals(a)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "customizeRealData(com.portfolio.platform.data.model.CustomizeRealData).\n Expected:\n" + tiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `customizeRealData`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "customizeRealData");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(1), "6ca914ed7b753a02dc3ff84030dc3e24", "8e8005f3461a63f640274ec577069e4f");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDatabase
    public CustomizeRealDataDao realDataDao() {
        CustomizeRealDataDao customizeRealDataDao;
        if (this._customizeRealDataDao != null) {
            return this._customizeRealDataDao;
        }
        synchronized (this) {
            if (this._customizeRealDataDao == null) {
                this._customizeRealDataDao = new CustomizeRealDataDao_Impl(this);
            }
            customizeRealDataDao = this._customizeRealDataDao;
        }
        return customizeRealDataDao;
    }
}
