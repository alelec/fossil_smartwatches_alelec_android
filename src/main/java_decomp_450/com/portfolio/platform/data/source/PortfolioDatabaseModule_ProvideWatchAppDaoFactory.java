package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.WatchAppDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideWatchAppDaoFactory implements Factory<WatchAppDao> {
    @DexIgnore
    public /* final */ Provider<DianaCustomizeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideWatchAppDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideWatchAppDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideWatchAppDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static WatchAppDao provideWatchAppDao(PortfolioDatabaseModule portfolioDatabaseModule, DianaCustomizeDatabase dianaCustomizeDatabase) {
        WatchAppDao provideWatchAppDao = portfolioDatabaseModule.provideWatchAppDao(dianaCustomizeDatabase);
        c87.a(provideWatchAppDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideWatchAppDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WatchAppDao get() {
        return provideWatchAppDao(this.module, this.dbProvider.get());
    }
}
