package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.WatchAppDataDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesWatchAppDataDaoFactory implements Factory<WatchAppDataDao> {
    @DexIgnore
    public /* final */ Provider<DianaCustomizeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesWatchAppDataDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesWatchAppDataDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvidesWatchAppDataDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static WatchAppDataDao providesWatchAppDataDao(PortfolioDatabaseModule portfolioDatabaseModule, DianaCustomizeDatabase dianaCustomizeDatabase) {
        WatchAppDataDao providesWatchAppDataDao = portfolioDatabaseModule.providesWatchAppDataDao(dianaCustomizeDatabase);
        c87.a(providesWatchAppDataDao, "Cannot return null from a non-@Nullable @Provides method");
        return providesWatchAppDataDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WatchAppDataDao get() {
        return providesWatchAppDataDao(this.module, this.dbProvider.get());
    }
}
