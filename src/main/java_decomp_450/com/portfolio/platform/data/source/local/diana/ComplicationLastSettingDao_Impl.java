package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.ComplicationLastSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationLastSettingDao_Impl implements ComplicationLastSettingDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<ComplicationLastSetting> __insertionAdapterOfComplicationLastSetting;
    @DexIgnore
    public /* final */ ji __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteComplicationLastSettingByComplicationId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<ComplicationLastSetting> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `complicationLastSetting` (`complicationId`,`updatedAt`,`setting`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, ComplicationLastSetting complicationLastSetting) {
            if (complicationLastSetting.getComplicationId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, complicationLastSetting.getComplicationId());
            }
            if (complicationLastSetting.getUpdatedAt() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, complicationLastSetting.getUpdatedAt());
            }
            if (complicationLastSetting.getSetting() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, complicationLastSetting.getSetting());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM complicationLastSetting WHERE complicationId=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM complicationLastSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<ComplicationLastSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon4(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<ComplicationLastSetting> call() throws Exception {
            Cursor a = pi.a(ComplicationLastSettingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "complicationId");
                int b2 = oi.b(a, "updatedAt");
                int b3 = oi.b(a, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new ComplicationLastSetting(a.getString(b), a.getString(b2), a.getString(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public ComplicationLastSettingDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfComplicationLastSetting = new Anon1(ciVar);
        this.__preparedStmtOfDeleteComplicationLastSettingByComplicationId = new Anon2(ciVar);
        this.__preparedStmtOfCleanUp = new Anon3(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao
    public void deleteComplicationLastSettingByComplicationId(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteComplicationLastSettingByComplicationId.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteComplicationLastSettingByComplicationId.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao
    public List<ComplicationLastSetting> getAllComplicationLastSetting() {
        fi b = fi.b("SELECT * FROM complicationLastSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "complicationId");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new ComplicationLastSetting(a.getString(b2), a.getString(b3), a.getString(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao
    public LiveData<List<ComplicationLastSetting>> getAllComplicationLastSettingAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"complicationLastSetting"}, false, (Callable) new Anon4(fi.b("SELECT * FROM complicationLastSetting", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao
    public ComplicationLastSetting getComplicationLastSetting(String str) {
        fi b = fi.b("SELECT * FROM complicationLastSetting WHERE complicationId=? ", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        ComplicationLastSetting complicationLastSetting = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "complicationId");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, MicroAppSetting.SETTING);
            if (a.moveToFirst()) {
                complicationLastSetting = new ComplicationLastSetting(a.getString(b2), a.getString(b3), a.getString(b4));
            }
            return complicationLastSetting;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao
    public void upsertComplicationLastSetting(ComplicationLastSetting complicationLastSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfComplicationLastSetting.insert(complicationLastSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
