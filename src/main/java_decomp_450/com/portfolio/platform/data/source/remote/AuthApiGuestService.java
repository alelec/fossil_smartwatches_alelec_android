package com.portfolio.platform.data.source.remote;

import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ie4;
import com.fossil.iw7;
import com.fossil.xv7;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface AuthApiGuestService {
    @DexIgnore
    @iw7("rpc/auth/check-account-existence-by-email")
    Object checkAuthenticationEmailExisting(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("rpc/auth/check-account-existence-by-social")
    Object checkAuthenticationSocialExisting(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("rpc/auth/login")
    Object loginWithEmail(@xv7 ie4 ie4, fb7<? super fv7<Auth>> fb7);

    @DexIgnore
    @iw7("rpc/auth/login-with")
    Object loginWithSocial(@xv7 ie4 ie4, fb7<? super fv7<Auth>> fb7);

    @DexIgnore
    @iw7("rpc/auth/password/request-reset")
    Object passwordRequestReset(@xv7 ie4 ie4, fb7<? super fv7<Void>> fb7);

    @DexIgnore
    @iw7("rpc/auth/register")
    Object registerEmail(@xv7 SignUpEmailAuth signUpEmailAuth, fb7<? super fv7<Auth>> fb7);

    @DexIgnore
    @iw7("rpc/auth/register-with")
    Object registerSocial(@xv7 SignUpSocialAuth signUpSocialAuth, fb7<? super fv7<Auth>> fb7);

    @DexIgnore
    @iw7("rpc/auth/request-email-otp")
    Object requestEmailOtp(@xv7 ie4 ie4, fb7<? super fv7<Void>> fb7);

    @DexIgnore
    @iw7("rpc/auth/token/exchange-legacy")
    Call<Auth> tokenExchangeLegacy(@xv7 ie4 ie4);

    @DexIgnore
    @iw7("rpc/auth/token/refresh")
    Call<Auth> tokenRefresh(@xv7 ie4 ie4);

    @DexIgnore
    @iw7("rpc/auth/verify-email-otp")
    Object verifyEmailOtp(@xv7 ie4 ie4, fb7<? super fv7<Void>> fb7);
}
