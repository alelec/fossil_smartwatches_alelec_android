package com.portfolio.platform.data.source;

import com.fossil.ie5;
import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository_Factory implements Factory<ThirdPartyRepository> {
    @DexIgnore
    public /* final */ Provider<ActivitiesRepository> mActivitiesRepositoryProvider;
    @DexIgnore
    public /* final */ Provider<ie5> mGoogleFitHelperProvider;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> mPortfolioAppProvider;

    @DexIgnore
    public ThirdPartyRepository_Factory(Provider<ie5> provider, Provider<ActivitiesRepository> provider2, Provider<PortfolioApp> provider3) {
        this.mGoogleFitHelperProvider = provider;
        this.mActivitiesRepositoryProvider = provider2;
        this.mPortfolioAppProvider = provider3;
    }

    @DexIgnore
    public static ThirdPartyRepository_Factory create(Provider<ie5> provider, Provider<ActivitiesRepository> provider2, Provider<PortfolioApp> provider3) {
        return new ThirdPartyRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static ThirdPartyRepository newInstance(ie5 ie5, ActivitiesRepository activitiesRepository, PortfolioApp portfolioApp) {
        return new ThirdPartyRepository(ie5, activitiesRepository, portfolioApp);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ThirdPartyRepository get() {
        return newInstance(this.mGoogleFitHelperProvider.get(), this.mActivitiesRepositoryProvider.get(), this.mPortfolioAppProvider.get());
    }
}
