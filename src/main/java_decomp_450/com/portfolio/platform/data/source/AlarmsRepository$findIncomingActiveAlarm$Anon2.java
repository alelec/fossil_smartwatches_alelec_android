package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.source.local.alarm.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.AlarmsRepository$findIncomingActiveAlarm$2", f = "AlarmsRepository.kt", l = {42}, m = "invokeSuspend")
public final class AlarmsRepository$findIncomingActiveAlarm$Anon2 extends zb7 implements kd7<yi7, fb7<? super Alarm>, Object> {
    @DexIgnore
    public int I$0;
    @DexIgnore
    public int I$1;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;

    @DexIgnore
    public AlarmsRepository$findIncomingActiveAlarm$Anon2(fb7 fb7) {
        super(2, fb7);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        AlarmsRepository$findIncomingActiveAlarm$Anon2 alarmsRepository$findIncomingActiveAlarm$Anon2 = new AlarmsRepository$findIncomingActiveAlarm$Anon2(fb7);
        alarmsRepository$findIncomingActiveAlarm$Anon2.p$ = (yi7) obj;
        return alarmsRepository$findIncomingActiveAlarm$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super Alarm> fb7) {
        return ((AlarmsRepository$findIncomingActiveAlarm$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a2, code lost:
        if ((r8.length == 0) != false) goto L_0x00a9;
     */
    @DexIgnore
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
            r11 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r11.label
            r2 = 7
            r3 = 1
            if (r1 == 0) goto L_0x0024
            if (r1 != r3) goto L_0x001c
            int r0 = r11.I$1
            int r1 = r11.I$0
            java.lang.Object r4 = r11.L$1
            java.util.Calendar r4 = (java.util.Calendar) r4
            java.lang.Object r4 = r11.L$0
            com.fossil.yi7 r4 = (com.fossil.yi7) r4
            com.fossil.t87.a(r12)
            goto L_0x0064
        L_0x001c:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r0)
            throw r12
        L_0x0024:
            com.fossil.t87.a(r12)
            com.fossil.yi7 r12 = r11.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.AlarmsRepository.TAG
            java.lang.String r5 = "findIncomingActiveAlarm()"
            r1.d(r4, r5)
            java.util.Calendar r1 = java.util.Calendar.getInstance()
            r4 = 11
            int r4 = r1.get(r4)
            int r4 = r4 * 60
            r5 = 12
            int r5 = r1.get(r5)
            int r4 = r4 + r5
            int r5 = r1.get(r2)
            com.fossil.pg5 r6 = com.fossil.pg5.i
            r11.L$0 = r12
            r11.L$1 = r1
            r11.I$0 = r4
            r11.I$1 = r5
            r11.label = r3
            java.lang.Object r12 = r6.a(r11)
            if (r12 != r0) goto L_0x0062
            return r0
        L_0x0062:
            r1 = r4
            r0 = r5
        L_0x0064:
            com.portfolio.platform.data.source.local.alarm.AlarmDatabase r12 = (com.portfolio.platform.data.source.local.alarm.AlarmDatabase) r12
            com.portfolio.platform.data.source.local.alarm.AlarmDao r12 = r12.alarmDao()
            java.util.List r12 = r12.getActiveAlarms()
            r4 = 0
            if (r12 == 0) goto L_0x00f3
            java.util.Iterator r12 = r12.iterator()
            r5 = r4
            r6 = r5
        L_0x0077:
            boolean r7 = r12.hasNext()
            if (r7 == 0) goto L_0x00f1
            java.lang.Object r7 = r12.next()
            com.portfolio.platform.data.source.local.alarm.Alarm r7 = (com.portfolio.platform.data.source.local.alarm.Alarm) r7
            int[] r8 = r7.getDays()
            r9 = 0
            if (r8 == 0) goto L_0x00c8
            int[] r8 = r7.getDays()
            if (r8 == 0) goto L_0x00c4
            boolean r8 = com.fossil.t97.a(r8, r0)
            if (r8 != 0) goto L_0x00a9
            int[] r8 = r7.getDays()
            if (r8 == 0) goto L_0x00a5
            int r8 = r8.length
            if (r8 != 0) goto L_0x00a1
            r8 = 1
            goto L_0x00a2
        L_0x00a1:
            r8 = 0
        L_0x00a2:
            if (r8 == 0) goto L_0x00c8
            goto L_0x00a9
        L_0x00a5:
            com.fossil.ee7.a()
            throw r4
        L_0x00a9:
            int r8 = r7.getTotalMinutes()
            if (r8 > r1) goto L_0x0077
            if (r5 != 0) goto L_0x00b2
            goto L_0x00be
        L_0x00b2:
            if (r5 == 0) goto L_0x00c0
            int r8 = r5.getTotalMinutes()
            int r9 = r7.getTotalMinutes()
            if (r8 <= r9) goto L_0x0077
        L_0x00be:
            r5 = r7
            goto L_0x0077
        L_0x00c0:
            com.fossil.ee7.a()
            throw r4
        L_0x00c4:
            com.fossil.ee7.a()
            throw r4
        L_0x00c8:
            int r8 = r7.getTotalMinutes()
            if (r8 <= r1) goto L_0x0077
            int[] r8 = new int[r3]
            if (r0 >= r2) goto L_0x00d7
            int r10 = r0 + 1
            r8[r9] = r10
            goto L_0x00d9
        L_0x00d7:
            r8[r9] = r3
        L_0x00d9:
            r7.setDays(r8)
            if (r6 != 0) goto L_0x00df
            goto L_0x00eb
        L_0x00df:
            if (r6 == 0) goto L_0x00ed
            int r8 = r6.getTotalMinutes()
            int r9 = r7.getTotalMinutes()
            if (r8 <= r9) goto L_0x0077
        L_0x00eb:
            r6 = r7
            goto L_0x0077
        L_0x00ed:
            com.fossil.ee7.a()
            throw r4
        L_0x00f1:
            r4 = r5
            goto L_0x00f4
        L_0x00f3:
            r6 = r4
        L_0x00f4:
            if (r4 == 0) goto L_0x00f7
            goto L_0x00f8
        L_0x00f7:
            r4 = r6
        L_0x00f8:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.AlarmsRepository$findIncomingActiveAlarm$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
