package com.portfolio.platform.data.source;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.ac2;
import com.fossil.bi7;
import com.fossil.dc2;
import com.fossil.dl7;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fitness.WorkoutType;
import com.fossil.gc2;
import com.fossil.hc2;
import com.fossil.i97;
import com.fossil.ic2;
import com.fossil.id2;
import com.fossil.ie5;
import com.fossil.ik7;
import com.fossil.jf7;
import com.fossil.lc2;
import com.fossil.mb7;
import com.fossil.nb7;
import com.fossil.no3;
import com.fossil.qe7;
import com.fossil.qf7;
import com.fossil.qj7;
import com.fossil.qy1;
import com.fossil.s87;
import com.fossil.vb7;
import com.fossil.vh7;
import com.fossil.w97;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOCalorie;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWODistance;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOStep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.CoroutineExceptionHandler;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "ThirdPartyRepository";
    @DexIgnore
    public ActivitiesRepository mActivitiesRepository;
    @DexIgnore
    public /* final */ CoroutineExceptionHandler mExceptionHandling; // = new ThirdPartyRepository$$special$$inlined$CoroutineExceptionHandler$Anon1(CoroutineExceptionHandler.n);
    @DexIgnore
    public ie5 mGoogleFitHelper;
    @DexIgnore
    public PortfolioApp mPortfolioApp;
    @DexIgnore
    public /* final */ yi7 mPushDataSupervisorJob; // = zi7.a(dl7.a(null, 1, null).plus(qj7.b()).plus(this.mExceptionHandling));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingThirdPartyDataCallback {
        @DexIgnore
        void onComplete();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[WorkoutType.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[WorkoutType.RUNNING.ordinal()] = 1;
            $EnumSwitchMapping$0[WorkoutType.CYCLING.ordinal()] = 2;
            $EnumSwitchMapping$0[WorkoutType.TREADMILL.ordinal()] = 3;
            $EnumSwitchMapping$0[WorkoutType.ELLIPTICAL.ordinal()] = 4;
            $EnumSwitchMapping$0[WorkoutType.WEIGHTS.ordinal()] = 5;
            $EnumSwitchMapping$0[WorkoutType.UNKNOWN.ordinal()] = 6;
        }
        */
    }

    @DexIgnore
    public ThirdPartyRepository(ie5 ie5, ActivitiesRepository activitiesRepository, PortfolioApp portfolioApp) {
        ee7.b(ie5, "mGoogleFitHelper");
        ee7.b(activitiesRepository, "mActivitiesRepository");
        ee7.b(portfolioApp, "mPortfolioApp");
        this.mGoogleFitHelper = ie5;
        this.mActivitiesRepository = activitiesRepository;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    private final List<GFitSleep> convertListMFSleepSessionToListGFitSleep(List<MFSleepSession> list) {
        ArrayList arrayList = new ArrayList();
        for (MFSleepSession mFSleepSession : list) {
            long j = (long) 1000;
            arrayList.add(new GFitSleep(mFSleepSession.getRealSleepMinutes(), ((long) mFSleepSession.getRealStartTime()) * j, j * ((long) mFSleepSession.getRealEndTime())));
        }
        return arrayList;
    }

    @DexIgnore
    private final DataSet createDataSetForActiveMinutes(GFitActiveTime gFitActiveTime, String str) {
        gc2.a aVar = new gc2.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.j);
        aVar.b(this.mPortfolioApp.getString(2131887234));
        aVar.a(0);
        aVar.a(new hc2(this.mPortfolioApp.getString(2131887234), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        List k = ea7.k(gFitActiveTime.getActiveTimes());
        jf7 a2 = qf7.a(qf7.d(0, k.size()), 2);
        int first = a2.getFirst();
        int last = a2.getLast();
        int a3 = a2.a();
        if (a3 < 0 ? first >= last : first <= last) {
            while (true) {
                DataPoint e = a.e();
                e.a(((Number) k.get(first)).longValue(), ((Number) k.get(first + 1)).longValue(), TimeUnit.MILLISECONDS);
                e.a(ic2.d).b("unknown");
                a.a(e);
                if (first == last) {
                    break;
                }
                first += a3;
            }
        }
        ee7.a((Object) a, "dataSetForActiveMinutes");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForCalories(List<GFitSample> list, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForCalories samples " + list);
        gc2.a aVar = new gc2.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.q);
        aVar.b(this.mPortfolioApp.getString(2131887234));
        aVar.a(0);
        aVar.a(new hc2(this.mPortfolioApp.getString(2131887234), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitSample gFitSample : list) {
            float component3 = gFitSample.component3();
            long component4 = gFitSample.component4();
            long component5 = gFitSample.component5();
            try {
                DataPoint e = a.e();
                e.a(component4, component5, TimeUnit.MILLISECONDS);
                e.a(ic2.I).a(component3);
                a.a(e);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d(TAG, "add calories " + component3 + " as data point " + e);
            } catch (Exception e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.e(TAG, "createDataSetForCalories() fall into error=" + e2.getMessage());
            }
        }
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        local4.d(TAG, "createDataSetForCalories " + a);
        ee7.a((Object) a, "dataSetForCalories");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForDistances(List<GFitSample> list, String str) {
        gc2.a aVar = new gc2.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.z);
        aVar.b(this.mPortfolioApp.getString(2131887234));
        aVar.a(0);
        aVar.a(new hc2(this.mPortfolioApp.getString(2131887234), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitSample gFitSample : list) {
            float component2 = gFitSample.component2();
            long component4 = gFitSample.component4();
            long component5 = gFitSample.component5();
            DataPoint e = a.e();
            e.a(component4, component5, TimeUnit.MILLISECONDS);
            e.a(ic2.w).a(component2);
            a.a(e);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForDistances " + a);
        ee7.a((Object) a, "dataSetForDistances");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForHeartRates(List<GFitHeartRate> list, String str) {
        gc2.a aVar = new gc2.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.w);
        aVar.b(this.mPortfolioApp.getString(2131887234));
        aVar.a(0);
        aVar.a(new hc2(this.mPortfolioApp.getString(2131887234), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitHeartRate gFitHeartRate : list) {
            float component1 = gFitHeartRate.component1();
            long component2 = gFitHeartRate.component2();
            long component3 = gFitHeartRate.component3();
            DataPoint e = a.e();
            e.a(component2, component3, TimeUnit.MILLISECONDS);
            e.a(ic2.r).a(component1);
            a.a(e);
        }
        ee7.a((Object) a, "dataSet");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForSteps(List<GFitSample> list, String str) {
        gc2.a aVar = new gc2.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.e);
        aVar.b(this.mPortfolioApp.getString(2131887234));
        aVar.a(0);
        aVar.a(new hc2(this.mPortfolioApp.getString(2131887234), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitSample gFitSample : list) {
            int component1 = gFitSample.component1();
            long component4 = gFitSample.component4();
            long component5 = gFitSample.component5();
            DataPoint e = a.e();
            e.a(component4, component5, TimeUnit.MILLISECONDS);
            e.a(ic2.g).a(component1);
            a.a(e);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForSteps " + a);
        ee7.a((Object) a, "dataSetForSteps");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForWOCalories(List<GFitWOCalorie> list, String str, long j, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForWOCalories steps " + list + " startTime " + j + " endTime " + j2);
        gc2.a aVar = new gc2.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.q);
        aVar.b(this.mPortfolioApp.getString(2131887234));
        aVar.a(0);
        aVar.a(new hc2(this.mPortfolioApp.getString(2131887234), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        try {
            for (GFitWOCalorie gFitWOCalorie : list) {
                if (gFitWOCalorie.getStartTime() >= j && gFitWOCalorie.getEndTime() <= j2) {
                    DataPoint e = a.e();
                    e.a(gFitWOCalorie.getStartTime(), gFitWOCalorie.getEndTime(), TimeUnit.MILLISECONDS);
                    e.a(ic2.I).a(gFitWOCalorie.getCalorie());
                    a.a(e);
                }
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "exception when add workout calories data point " + e2);
        }
        ee7.a((Object) a, "dataSetForCalories");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForWODistances(List<GFitWODistance> list, String str, long j, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForWODistances listDistances " + list + " startTime " + j + " endTime " + j2);
        gc2.a aVar = new gc2.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.z);
        aVar.b(this.mPortfolioApp.getString(2131887234));
        aVar.a(0);
        aVar.a(new hc2(this.mPortfolioApp.getString(2131887234), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitWODistance gFitWODistance : list) {
            if (gFitWODistance.getStartTime() >= j && gFitWODistance.getEndTime() <= j2) {
                DataPoint e = a.e();
                e.a(gFitWODistance.getStartTime(), gFitWODistance.getEndTime(), TimeUnit.MILLISECONDS);
                e.a(ic2.w).a(gFitWODistance.getDistance());
                a.a(e);
            }
        }
        ee7.a((Object) a, "dataSetForDistances");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForWOHeartRates(List<GFitWOHeartRate> list, String str, long j, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForWOHeartRates listHeartRates " + list + " startTime " + j + " endTime " + j2);
        gc2.a aVar = new gc2.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.w);
        aVar.b(this.mPortfolioApp.getString(2131887234));
        aVar.a(0);
        aVar.a(new hc2(this.mPortfolioApp.getString(2131887234), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        try {
            for (GFitWOHeartRate gFitWOHeartRate : list) {
                if (gFitWOHeartRate.getStartTime() >= j && gFitWOHeartRate.getEndTime() <= j2) {
                    DataPoint e = a.e();
                    e.a(gFitWOHeartRate.getStartTime(), gFitWOHeartRate.getEndTime(), TimeUnit.MILLISECONDS);
                    e.a(ic2.r).a(gFitWOHeartRate.getHeartRate());
                    a.a(e);
                }
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "exception when add workout heartrate data point " + e2);
        }
        ee7.a((Object) a, "dataSet");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForWOSteps(List<GFitWOStep> list, String str, long j, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForWOSteps steps " + list + " startTime " + j + " endTime " + j2);
        gc2.a aVar = new gc2.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.e);
        aVar.b(this.mPortfolioApp.getString(2131887234));
        aVar.a(0);
        aVar.a(new hc2(this.mPortfolioApp.getString(2131887234), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        try {
            for (GFitWOStep gFitWOStep : list) {
                if (gFitWOStep.getStartTime() >= j && gFitWOStep.getEndTime() <= j2) {
                    DataPoint e = a.e();
                    e.a(gFitWOStep.getStartTime(), gFitWOStep.getEndTime(), TimeUnit.MILLISECONDS);
                    e.a(ic2.g).a(gFitWOStep.getStep());
                    a.a(e);
                }
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "exception when add workout steps data point " + e2);
        }
        ee7.a((Object) a, "dataSetForSteps");
        return a;
    }

    @DexIgnore
    private final id2 createWorkoutSession(GFitWorkoutSession gFitWorkoutSession, String str) {
        DataSet createDataSetForWOSteps = createDataSetForWOSteps(gFitWorkoutSession.getSteps(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWOCalories = createDataSetForWOCalories(gFitWorkoutSession.getCalories(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWODistances = createDataSetForWODistances(gFitWorkoutSession.getDistances(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWOHeartRates = createDataSetForWOHeartRates(gFitWorkoutSession.getHeartRates(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createWorkoutSession dataSetForSteps " + createDataSetForWOSteps + " dataSetForCalories " + createDataSetForWOCalories + " dataSetForDistances " + createDataSetForWODistances + " dataSetForHeartRates " + createDataSetForWOHeartRates);
        WorkoutType workoutType = getWorkoutType(gFitWorkoutSession.getWorkoutType());
        lc2.a aVar = new lc2.a();
        aVar.d(workoutType.name());
        aVar.a(getFitnessActivitiesType(workoutType));
        aVar.b(gFitWorkoutSession.getStartTime(), TimeUnit.MILLISECONDS);
        aVar.a(gFitWorkoutSession.getEndTime(), TimeUnit.MILLISECONDS);
        lc2 a = aVar.a();
        id2.a aVar2 = new id2.a();
        aVar2.a(a);
        if (!createDataSetForWOSteps.isEmpty()) {
            aVar2.a(createDataSetForWOSteps);
        }
        if (!createDataSetForWOCalories.isEmpty()) {
            aVar2.a(createDataSetForWOCalories);
        }
        if (!createDataSetForWODistances.isEmpty()) {
            aVar2.a(createDataSetForWODistances);
        }
        if (!createDataSetForWOHeartRates.isEmpty()) {
            aVar2.a(createDataSetForWOHeartRates);
        }
        id2 a2 = aVar2.a();
        ee7.a((Object) a2, "sessionBuilder.build()");
        return a2;
    }

    @DexIgnore
    private final String getFitnessActivitiesType(WorkoutType workoutType) {
        switch (WhenMappings.$EnumSwitchMapping$0[workoutType.ordinal()]) {
            case 1:
                return "running";
            case 2:
                return "biking";
            case 3:
                return "walking.treadmill";
            case 4:
                return "elliptical";
            case 5:
                return "weightlifting";
            case 6:
                return "unknown";
            default:
                return FacebookRequestErrorClassification.KEY_OTHER;
        }
    }

    @DexIgnore
    private final WorkoutType getWorkoutType(int i) {
        if (i < WorkoutType.values().length) {
            return WorkoutType.values()[i];
        }
        return WorkoutType.UNKNOWN;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.portfolio.platform.data.source.ThirdPartyRepository */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Object saveData$default(ThirdPartyRepository thirdPartyRepository, List list, GFitActiveTime gFitActiveTime, List list2, List list3, List list4, fb7 fb7, int i, Object obj) {
        if ((i & 1) != 0) {
            list = w97.a();
        }
        if ((i & 2) != 0) {
            gFitActiveTime = null;
        }
        if ((i & 4) != 0) {
            list2 = w97.a();
        }
        if ((i & 8) != 0) {
            list3 = w97.a();
        }
        if ((i & 16) != 0) {
            list4 = w97.a();
        }
        return thirdPartyRepository.saveData(list, gFitActiveTime, list2, list3, list4, fb7);
    }

    @DexIgnore
    public static /* synthetic */ Object uploadData$default(ThirdPartyRepository thirdPartyRepository, PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, fb7 fb7, int i, Object obj) {
        if ((i & 1) != 0) {
            pushPendingThirdPartyDataCallback = null;
        }
        return thirdPartyRepository.uploadData(pushPendingThirdPartyDataCallback, fb7);
    }

    @DexIgnore
    public final DataSet createDataSetForSleepData(GFitSleep gFitSleep, hc2 hc2) {
        ee7.b(gFitSleep, "gFitSleep");
        ee7.b(hc2, "device");
        gc2.a aVar = new gc2.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.j);
        aVar.b(this.mPortfolioApp.getString(2131887234));
        aVar.a(0);
        aVar.a(hc2);
        DataSet a = DataSet.a(aVar.a());
        long startTime = gFitSleep.getStartTime();
        long endTime = gFitSleep.getEndTime();
        DataPoint e = a.e();
        e.a(startTime, endTime, TimeUnit.MILLISECONDS);
        e.a(ic2.d).a(gFitSleep.getSleepMins());
        a.a(e);
        ee7.a((Object) a, "dataSetForSleep");
        return a;
    }

    @DexIgnore
    public final ActivitiesRepository getMActivitiesRepository() {
        return this.mActivitiesRepository;
    }

    @DexIgnore
    public final PortfolioApp getMPortfolioApp() {
        return this.mPortfolioApp;
    }

    @DexIgnore
    public final Object pushPendingData(PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, fb7<? super ik7> fb7) {
        return vh7.a(qj7.b(), new ThirdPartyRepository$pushPendingData$Anon2(this, pushPendingThirdPartyDataCallback, null), fb7);
    }

    @DexIgnore
    public final Object saveData(List<GFitSample> list, GFitActiveTime gFitActiveTime, List<GFitHeartRate> list2, List<GFitWorkoutSession> list3, List<MFSleepSession> list4, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new ThirdPartyRepository$saveData$Anon2(this, list, gFitActiveTime, list2, list3, list4, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final /* synthetic */ Object saveGFitActiveTimeToGoogleFit(List<GFitActiveTime> list, String str, fb7<Object> fb7) {
        bi7 bi7 = new bi7(mb7.a(fb7), 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "Start saveGFitActiveTimeToGoogleFit");
        GoogleSignInAccount a = qy1.a(getMPortfolioApp());
        if (a == null) {
            FLogger.INSTANCE.getLocal().d(str2, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(str2, "End saveGFitActiveTimeToGoogleFit");
            if (bi7.isActive()) {
                s87.a aVar = s87.Companion;
                bi7.resumeWith(s87.m60constructorimpl(null));
            }
        } else {
            dc2 a2 = ac2.a(getMPortfolioApp(), a);
            FLogger.INSTANCE.getLocal().d(str2, "Sending GFitActiveTime to Google Fit");
            int size = list.size();
            qe7 qe7 = new qe7();
            qe7.element = 0;
            for (Iterator<T> it = list.iterator(); it.hasNext(); it = it) {
                T next = it.next();
                DataSet access$createDataSetForActiveMinutes = createDataSetForActiveMinutes(next, str);
                List<DataPoint> g = access$createDataSetForActiveMinutes.g();
                ee7.a((Object) g, "dataSet.dataPoints");
                Iterator<T> it2 = g.iterator();
                while (it2.hasNext()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d(str2, "saveGFitActiveTimeToGoogleFit send data " + ((Object) it2.next()));
                }
                no3<Void> a3 = a2.a(access$createDataSetForActiveMinutes);
                a3.a(new ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(next, a2, qe7, size, bi7, this, list, str));
                a3.a(new ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(a2, qe7, size, bi7, this, list, str));
                str2 = str2;
            }
        }
        Object g2 = bi7.g();
        if (g2 == nb7.a()) {
            vb7.c(fb7);
        }
        return g2;
    }

    @DexIgnore
    public final /* synthetic */ Object saveGFitHeartRateToGoogleFit(List<GFitHeartRate> list, String str, fb7<Object> fb7) {
        bi7 bi7 = new bi7(mb7.a(fb7), 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "Start saveGFitHeartRateToGoogleFit");
        GoogleSignInAccount a = qy1.a(getMPortfolioApp());
        if (a == null) {
            FLogger.INSTANCE.getLocal().d(str2, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(str2, "End saveGFitHeartRateToGoogleFit");
            if (bi7.isActive()) {
                s87.a aVar = s87.Companion;
                bi7.resumeWith(s87.m60constructorimpl(null));
            }
        } else {
            dc2 a2 = ac2.a(getMPortfolioApp(), a);
            FLogger.INSTANCE.getLocal().d(str2, "Sending GFitHeartRate to Google Fit");
            List<List> b = ea7.b(list, 500);
            int size = b.size();
            qe7 qe7 = new qe7();
            qe7.element = 0;
            for (List list2 : b) {
                DataSet access$createDataSetForHeartRates = createDataSetForHeartRates(list2, str);
                List<DataPoint> g = access$createDataSetForHeartRates.g();
                ee7.a((Object) g, "dataSet.dataPoints");
                Iterator<T> it = g.iterator();
                while (it.hasNext()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d(str2, "saveGFitHeartRateToGoogleFit send data " + ((Object) it.next()));
                }
                no3<Void> a3 = a2.a(access$createDataSetForHeartRates);
                a3.a(new ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(list2, a2, qe7, size, bi7, this, list, str));
                a3.a(new ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(a2, qe7, size, bi7, this, list, str));
                str2 = str2;
            }
        }
        Object g2 = bi7.g();
        if (g2 == nb7.a()) {
            vb7.c(fb7);
        }
        return g2;
    }

    @DexIgnore
    public final /* synthetic */ Object saveGFitSampleToGoogleFit(List<GFitSample> list, String str, fb7<Object> fb7) {
        dc2 dc2;
        List list2;
        qe7 qe7;
        qe7 qe72;
        ThirdPartyRepository thirdPartyRepository = this;
        boolean z = true;
        bi7 bi7 = new bi7(mb7.a(fb7), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitSampleToGoogleFit");
        GoogleSignInAccount a = qy1.a(getMPortfolioApp());
        if (a == null) {
            FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitSampleToGoogleFit");
            if (bi7.isActive()) {
                s87.a aVar = s87.Companion;
                bi7.resumeWith(s87.m60constructorimpl(null));
            }
        } else {
            dc2 a2 = ac2.a(getMPortfolioApp(), a);
            FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitSample to Google Fit");
            List<List> b = ea7.b(list, 500);
            int size = b.size();
            qe7 qe73 = new qe7();
            int i = 0;
            qe73.element = 0;
            for (List list3 : b) {
                ArrayList arrayList = new ArrayList();
                DataSet access$createDataSetForSteps = thirdPartyRepository.createDataSetForSteps(list3, str);
                DataSet access$createDataSetForDistances = thirdPartyRepository.createDataSetForDistances(list3, str);
                DataSet access$createDataSetForCalories = thirdPartyRepository.createDataSetForCalories(list3, str);
                arrayList.add(access$createDataSetForSteps);
                arrayList.add(access$createDataSetForDistances);
                arrayList.add(access$createDataSetForCalories);
                int size2 = arrayList.size();
                qe7 qe74 = new qe7();
                qe74.element = i;
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    DataSet dataSet = (DataSet) it.next();
                    ee7.a((Object) dataSet, "dataSet");
                    List<DataPoint> g = dataSet.g();
                    ee7.a((Object) g, "dataSet.dataPoints");
                    if (g.isEmpty() ^ z) {
                        List<DataPoint> g2 = dataSet.g();
                        ee7.a((Object) g2, "dataSet.dataPoints");
                        Iterator<T> it2 = g2.iterator();
                        while (it2.hasNext()) {
                            FLogger.INSTANCE.getLocal().d(TAG, "saveGFitSampleToGoogleFit send data " + ((Object) it2.next()));
                        }
                        no3<Void> a3 = a2.a(dataSet);
                        qe7 = qe74;
                        list2 = list3;
                        dc2 = a2;
                        a3.a(new ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(qe74, size2, list3, a2, qe73, bi7, size, this, list, str));
                        a3.a(new ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(size2, dc2, qe73, bi7, size, this, list, str));
                        ee7.a((Object) a3, "historyClient.insertData\u2026                        }");
                        qe72 = qe73;
                    } else {
                        qe7 = qe74;
                        list2 = list3;
                        qe72 = qe73;
                        dc2 = a2;
                        qe72.element++;
                    }
                    qe73 = qe72;
                    qe74 = qe7;
                    list3 = list2;
                    a2 = dc2;
                    i = 0;
                    z = true;
                }
                thirdPartyRepository = this;
            }
        }
        Object g3 = bi7.g();
        if (g3 == nb7.a()) {
            vb7.c(fb7);
        }
        return g3;
    }

    @DexIgnore
    public final Object saveGFitSleepDataToGoogleFit(List<GFitSleep> list, String str, fb7<Object> fb7) {
        String str2 = str;
        bi7 bi7 = new bi7(mb7.a(fb7), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitSleepDataToGoogleFit");
        GoogleSignInAccount a = qy1.a(getMPortfolioApp());
        if (a != null) {
            int size = list.size();
            qe7 qe7 = new qe7();
            qe7.element = 0;
            int i = 2131887234;
            hc2 hc2 = new hc2(getMPortfolioApp().getString(2131887234), DeviceIdentityUtils.getNameBySerial(str), str2, 3);
            ArrayList arrayList = new ArrayList();
            for (T t : list) {
                DataSet createDataSetForSleepData = createDataSetForSleepData(t, hc2);
                lc2.a aVar = new lc2.a();
                aVar.c(str2 + new DateTime());
                aVar.d(getMPortfolioApp().getString(i));
                aVar.b("User Sleep");
                aVar.b(t.getStartTime(), TimeUnit.MILLISECONDS);
                aVar.a(t.getEndTime(), TimeUnit.MILLISECONDS);
                aVar.a("sleep");
                lc2 a2 = aVar.a();
                id2.a aVar2 = new id2.a();
                aVar2.a(a2);
                aVar2.a(createDataSetForSleepData);
                no3<Void> a3 = ac2.b(getMPortfolioApp(), a).a(aVar2.a());
                a3.a(new ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(t, hc2, a, qe7, arrayList, size, bi7, this, list, str));
                a3.a(new ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(hc2, a, qe7, arrayList, size, bi7, this, list, str));
                str2 = str;
                size = size;
                hc2 = hc2;
                i = 2131887234;
            }
        } else {
            FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitSleepDataToGoogleFit");
            if (bi7.isActive()) {
                s87.a aVar3 = s87.Companion;
                bi7.resumeWith(s87.m60constructorimpl(null));
            }
        }
        Object g = bi7.g();
        if (g == nb7.a()) {
            vb7.c(fb7);
        }
        return g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:40:0x01bc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveGFitWorkoutSessionToGoogleFit(java.util.List<com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession> r20, java.lang.String r21, com.fossil.fb7<java.lang.Object> r22) {
        /*
            r19 = this;
            java.lang.String r1 = "dataSet.dataPoints"
            com.fossil.bi7 r11 = new com.fossil.bi7
            com.fossil.fb7 r0 = com.fossil.mb7.a(r22)
            r12 = 1
            r11.<init>(r0, r12)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r13 = "ThirdPartyRepository"
            java.lang.String r2 = "Start saveGFitWorkoutSessionToGoogleFit"
            r0.d(r13, r2)
            com.fossil.a12$a r0 = new com.fossil.a12$a
            com.portfolio.platform.PortfolioApp r2 = r19.getMPortfolioApp()
            r0.<init>(r2)
            com.fossil.v02<com.fossil.v02$d$d> r2 = com.fossil.ac2.b
            r3 = 0
            com.google.android.gms.common.api.Scope[] r4 = new com.google.android.gms.common.api.Scope[r3]
            r0.a(r2, r4)
            java.lang.String r2 = "https://www.googleapis.com/auth/fitness.location.write"
            java.lang.String r4 = "https://www.googleapis.com/auth/fitness.activity.write"
            java.lang.String r5 = "https://www.googleapis.com/auth/fitness.body.write"
            java.lang.String[] r2 = new java.lang.String[]{r4, r2, r2, r5}
            r0.a(r2)
            com.fossil.a12 r0 = r0.a()
            com.fossil.i02 r2 = r0.a()
            java.lang.String r4 = "result"
            com.fossil.ee7.a(r2, r4)
            boolean r2 = r2.x()
            java.lang.String r15 = "End saveGFitWorkoutSessionToGoogleFit"
            if (r2 == 0) goto L_0x018e
            java.lang.String r2 = "googleApiClient"
            com.fossil.ee7.a(r0, r2)
            boolean r0 = r0.g()
            if (r0 == 0) goto L_0x018e
            com.portfolio.platform.PortfolioApp r0 = r19.getMPortfolioApp()
            com.google.android.gms.auth.api.signin.GoogleSignInAccount r10 = com.fossil.qy1.a(r0)
            if (r10 == 0) goto L_0x0169
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = "Sending GFitWorkoutSession to Google Fit"
            r0.d(r13, r2)
            int r16 = r20.size()
            com.fossil.qe7 r9 = new com.fossil.qe7
            r9.<init>()
            r9.element = r3
            java.util.Iterator r17 = r20.iterator()
        L_0x007b:
            boolean r0 = r17.hasNext()
            if (r0 == 0) goto L_0x01b2
            java.lang.Object r0 = r17.next()
            r3 = r0
            com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession r3 = (com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession) r3
            r8 = r19
            r7 = r21
            com.fossil.id2 r0 = r8.createWorkoutSession(r3, r7)     // Catch:{ Exception -> 0x012c }
            java.util.List r2 = r0.g()     // Catch:{ Exception -> 0x012c }
            java.lang.String r4 = "sessionInsertRequest.dataSets"
            com.fossil.ee7.a(r2, r4)     // Catch:{ Exception -> 0x012c }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ Exception -> 0x012c }
        L_0x009d:
            boolean r4 = r2.hasNext()     // Catch:{ Exception -> 0x012c }
            if (r4 == 0) goto L_0x00f1
            java.lang.Object r4 = r2.next()     // Catch:{ Exception -> 0x012c }
            com.google.android.gms.fitness.data.DataSet r4 = (com.google.android.gms.fitness.data.DataSet) r4     // Catch:{ Exception -> 0x012c }
            java.lang.String r5 = "dataSet"
            com.fossil.ee7.a(r4, r5)     // Catch:{ Exception -> 0x012c }
            java.util.List r5 = r4.g()     // Catch:{ Exception -> 0x012c }
            com.fossil.ee7.a(r5, r1)     // Catch:{ Exception -> 0x012c }
            boolean r5 = r5.isEmpty()     // Catch:{ Exception -> 0x012c }
            r5 = r5 ^ r12
            if (r5 == 0) goto L_0x00ef
            java.util.List r4 = r4.g()     // Catch:{ Exception -> 0x012c }
            com.fossil.ee7.a(r4, r1)     // Catch:{ Exception -> 0x012c }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ Exception -> 0x012c }
        L_0x00c7:
            boolean r5 = r4.hasNext()     // Catch:{ Exception -> 0x012c }
            if (r5 == 0) goto L_0x00ef
            java.lang.Object r5 = r4.next()     // Catch:{ Exception -> 0x012c }
            com.google.android.gms.fitness.data.DataPoint r5 = (com.google.android.gms.fitness.data.DataPoint) r5     // Catch:{ Exception -> 0x012c }
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x012c }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()     // Catch:{ Exception -> 0x012c }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012c }
            r12.<init>()     // Catch:{ Exception -> 0x012c }
            java.lang.String r14 = "Sending GFitWorkoutSession data "
            r12.append(r14)     // Catch:{ Exception -> 0x012c }
            r12.append(r5)     // Catch:{ Exception -> 0x012c }
            java.lang.String r5 = r12.toString()     // Catch:{ Exception -> 0x012c }
            r6.d(r13, r5)     // Catch:{ Exception -> 0x012c }
            r12 = 1
            goto L_0x00c7
        L_0x00ef:
            r12 = 1
            goto L_0x009d
        L_0x00f1:
            com.portfolio.platform.PortfolioApp r2 = r19.getMPortfolioApp()     // Catch:{ Exception -> 0x012c }
            com.fossil.fc2 r2 = com.fossil.ac2.b(r2, r10)     // Catch:{ Exception -> 0x012c }
            com.fossil.no3 r0 = r2.a(r0)     // Catch:{ Exception -> 0x012c }
            com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 r12 = new com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1     // Catch:{ Exception -> 0x012c }
            r2 = r12
            r4 = r10
            r5 = r9
            r6 = r16
            r7 = r11
            r8 = r19
            r14 = r9
            r9 = r20
            r18 = r10
            r10 = r21
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x012a }
            r0.a(r12)     // Catch:{ Exception -> 0x012a }
            com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2 r10 = new com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2     // Catch:{ Exception -> 0x012a }
            r2 = r10
            r3 = r18
            r4 = r14
            r5 = r16
            r6 = r11
            r7 = r19
            r8 = r20
            r9 = r21
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x012a }
            r0.a(r10)     // Catch:{ Exception -> 0x012a }
            goto L_0x0163
        L_0x012a:
            r0 = move-exception
            goto L_0x0130
        L_0x012c:
            r0 = move-exception
            r14 = r9
            r18 = r10
        L_0x0130:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "exception when create workout session "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r2.d(r13, r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            r0.d(r13, r15)
            boolean r0 = r11.isActive()
            if (r0 == 0) goto L_0x0163
            com.fossil.s87$a r0 = com.fossil.s87.Companion
            r2 = 0
            java.lang.Object r0 = com.fossil.s87.m60constructorimpl(r2)
            r11.resumeWith(r0)
        L_0x0163:
            r9 = r14
            r10 = r18
            r12 = 1
            goto L_0x007b
        L_0x0169:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = "GoogleSignInAccount is null"
            r0.d(r13, r1)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            r0.d(r13, r15)
            boolean r0 = r11.isActive()
            if (r0 == 0) goto L_0x01b2
            com.fossil.s87$a r0 = com.fossil.s87.Companion
            r1 = 0
            java.lang.Object r0 = com.fossil.s87.m60constructorimpl(r1)
            r11.resumeWith(r0)
            goto L_0x01b2
        L_0x018e:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = "Not sending GFitWorkoutSession to Google Fit"
            r0.d(r13, r1)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            r0.d(r13, r15)
            boolean r0 = r11.isActive()
            if (r0 == 0) goto L_0x01b2
            com.fossil.s87$a r0 = com.fossil.s87.Companion
            r1 = 0
            java.lang.Object r0 = com.fossil.s87.m60constructorimpl(r1)
            r11.resumeWith(r0)
        L_0x01b2:
            java.lang.Object r0 = r11.g()
            java.lang.Object r1 = com.fossil.nb7.a()
            if (r0 != r1) goto L_0x01bf
            com.fossil.vb7.c(r22)
        L_0x01bf:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ThirdPartyRepository.saveGFitWorkoutSessionToGoogleFit(java.util.List, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void setMActivitiesRepository(ActivitiesRepository activitiesRepository) {
        ee7.b(activitiesRepository, "<set-?>");
        this.mActivitiesRepository = activitiesRepository;
    }

    @DexIgnore
    public final void setMPortfolioApp(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "<set-?>");
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final Object uploadData(PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, fb7<? super ik7> fb7) {
        return xh7.b(this.mPushDataSupervisorJob, null, null, new ThirdPartyRepository$uploadData$Anon2(this, pushPendingThirdPartyDataCallback, null), 3, null);
    }
}
