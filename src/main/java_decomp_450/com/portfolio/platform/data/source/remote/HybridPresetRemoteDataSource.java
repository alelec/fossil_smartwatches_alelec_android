package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "HybridPresetRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public HybridPresetRemoteDataSource(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deletePreset(com.portfolio.platform.data.model.room.microapp.HybridPreset r9, com.fossil.fb7<? super com.fossil.zi5<java.lang.Void>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$Anon1 r0 = (com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$Anon1 r0 = new com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$Anon1
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0042
            if (r2 != r3) goto L_0x003a
            java.lang.Object r9 = r0.L$3
            com.fossil.de4 r9 = (com.fossil.de4) r9
            java.lang.Object r9 = r0.L$2
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$1
            com.portfolio.platform.data.model.room.microapp.HybridPreset r9 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r9
            java.lang.Object r9 = r0.L$0
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource r9 = (com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource) r9
            com.fossil.t87.a(r10)
            goto L_0x0071
        L_0x003a:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0042:
            com.fossil.t87.a(r10)
            com.fossil.ie4 r10 = new com.fossil.ie4
            r10.<init>()
            com.fossil.de4 r2 = new com.fossil.de4
            r2.<init>()
            java.lang.String r5 = r9.getId()
            r2.a(r5)
            java.lang.String r5 = "_ids"
            r10.a(r5, r2)
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$response$Anon1 r5 = new com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$deletePreset$response$Anon1
            r5.<init>(r8, r10, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.L$3 = r2
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r5, r0)
            if (r10 != r1) goto L_0x0071
            return r1
        L_0x0071:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x007f
            com.fossil.bj5 r9 = new com.fossil.bj5
            r10 = 0
            r0 = 2
            r9.<init>(r4, r10, r0, r4)
            goto L_0x0099
        L_0x007f:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x009a
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 28
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x0099:
            return r9
        L_0x009a:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource.deletePreset(com.portfolio.platform.data.model.room.microapp.HybridPreset, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadHybridPresetList(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridPreset>>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1 r0 = (com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1 r0 = new com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x003a
            if (r2 != r4) goto L_0x0032
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource) r0
            com.fossil.t87.a(r10)
            goto L_0x004f
        L_0x0032:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003a:
            com.fossil.t87.a(r10)
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1 r10 = new com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1
            r10.<init>(r8, r9, r3)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r4
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x004f
            return r1
        L_0x004f:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r0 = r10 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x0097
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            boolean r1 = r10.b()
            if (r1 != 0) goto L_0x008d
            java.lang.Object r1 = r10.a()
            if (r1 == 0) goto L_0x0089
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            java.util.List r1 = r1.get_items()
            java.util.Iterator r1 = r1.iterator()
        L_0x0072:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x008d
            java.lang.Object r2 = r1.next()
            com.portfolio.platform.data.model.room.microapp.HybridPreset r2 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r2
            r3 = 0
            r2.setPinType(r3)
            r2.setSerialNumber(r9)
            r0.add(r2)
            goto L_0x0072
        L_0x0089:
            com.fossil.ee7.a()
            throw r3
        L_0x008d:
            com.fossil.bj5 r9 = new com.fossil.bj5
            boolean r10 = r10.b()
            r9.<init>(r0, r10)
            goto L_0x00b1
        L_0x0097:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00b2
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 28
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x00b1:
            return r9
        L_0x00b2:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource.downloadHybridPresetList(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadHybridRecommendPresetList(java.lang.String r18, com.fossil.fb7<? super com.fossil.zi5<java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset>>> r19) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            boolean r3 = r2 instanceof com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1 r3 = (com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1 r3 = new com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 0
            r7 = 1
            java.lang.String r8 = "HybridPresetRemoteDataSource"
            if (r5 == 0) goto L_0x0042
            if (r5 != r7) goto L_0x003a
            java.lang.Object r1 = r3.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r3 = r3.L$0
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource r3 = (com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource) r3
            com.fossil.t87.a(r2)
            goto L_0x0071
        L_0x003a:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0042:
            com.fossil.t87.a(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r9 = "downloadHybridRecommendPresetList "
            r5.append(r9)
            r5.append(r1)
            java.lang.String r5 = r5.toString()
            r2.d(r8, r5)
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1 r2 = new com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1
            r2.<init>(r0, r1, r6)
            r3.L$0 = r0
            r3.L$1 = r1
            r3.label = r7
            java.lang.Object r2 = com.fossil.aj5.a(r2, r3)
            if (r2 != r4) goto L_0x0071
            return r4
        L_0x0071:
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            boolean r3 = r2 instanceof com.fossil.bj5
            if (r3 == 0) goto L_0x00ed
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = "downloadHybridRecommendPresetList success "
            r5.append(r7)
            r5.append(r3)
            java.lang.String r7 = " isFromCache "
            r5.append(r7)
            com.fossil.bj5 r2 = (com.fossil.bj5) r2
            boolean r7 = r2.b()
            r5.append(r7)
            java.lang.String r5 = r5.toString()
            r4.d(r8, r5)
            java.util.Date r4 = new java.util.Date
            long r7 = java.lang.System.currentTimeMillis()
            r4.<init>(r7)
            java.lang.String r4 = com.fossil.zd5.y(r4)
            java.lang.Object r5 = r2.a()
            if (r5 == 0) goto L_0x00e9
            com.portfolio.platform.data.source.remote.ApiResponse r5 = (com.portfolio.platform.data.source.remote.ApiResponse) r5
            java.util.List r5 = r5.get_items()
            java.util.Iterator r5 = r5.iterator()
        L_0x00c1:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x00df
            java.lang.Object r6 = r5.next()
            com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset r6 = (com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset) r6
            r6.setSerialNumber(r1)
            java.lang.String r7 = "timestamp"
            com.fossil.ee7.a(r4, r7)
            r6.setCreatedAt(r4)
            r6.setUpdatedAt(r4)
            r3.add(r6)
            goto L_0x00c1
        L_0x00df:
            com.fossil.bj5 r1 = new com.fossil.bj5
            boolean r2 = r2.b()
            r1.<init>(r3, r2)
            goto L_0x0132
        L_0x00e9:
            com.fossil.ee7.a()
            throw r6
        L_0x00ed:
            boolean r1 = r2 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x0133
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "downloadHybridRecommendPresetList fail code "
            r3.append(r4)
            com.fossil.yi5 r2 = (com.fossil.yi5) r2
            int r4 = r2.a()
            r3.append(r4)
            java.lang.String r4 = " serverError "
            r3.append(r4)
            com.portfolio.platform.data.model.ServerError r4 = r2.c()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.d(r8, r3)
            com.fossil.yi5 r1 = new com.fossil.yi5
            int r10 = r2.a()
            com.portfolio.platform.data.model.ServerError r11 = r2.c()
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 28
            r16 = 0
            r9 = r1
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
        L_0x0132:
            return r1
        L_0x0133:
            com.fossil.p87 r1 = new com.fossil.p87
            r1.<init>()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource.downloadHybridRecommendPresetList(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object replaceHybridPresetList(java.util.List<com.portfolio.platform.data.model.room.microapp.HybridPreset> r18, com.fossil.fb7<? super com.fossil.zi5<java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridPreset>>> r19) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            boolean r3 = r2 instanceof com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$replaceHybridPresetList$Anon1
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$replaceHybridPresetList$Anon1 r3 = (com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$replaceHybridPresetList$Anon1) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$replaceHybridPresetList$Anon1 r3 = new com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$replaceHybridPresetList$Anon1
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 1
            r7 = 0
            java.lang.String r8 = "HybridPresetRemoteDataSource"
            r9 = 0
            if (r5 == 0) goto L_0x004b
            if (r5 != r6) goto L_0x0043
            java.lang.Object r1 = r3.L$3
            com.fossil.ie4 r1 = (com.fossil.ie4) r1
            java.lang.Object r1 = r3.L$2
            com.google.gson.Gson r1 = (com.google.gson.Gson) r1
            java.lang.Object r1 = r3.L$1
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r1 = r3.L$0
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource r1 = (com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource) r1
            com.fossil.t87.a(r2)
            goto L_0x00a5
        L_0x0043:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x004b:
            com.fossil.t87.a(r2)
            com.fossil.be4 r2 = new com.fossil.be4
            r2.<init>()
            com.fossil.od5 r5 = new com.fossil.od5
            r5.<init>()
            r2.b(r5)
            com.google.gson.Gson r2 = r2.a()
            com.fossil.ie4 r5 = new com.fossil.ie4
            r5.<init>()
            com.portfolio.platform.data.model.room.microapp.HybridPreset[] r10 = new com.portfolio.platform.data.model.room.microapp.HybridPreset[r9]
            java.lang.Object[] r10 = r1.toArray(r10)
            if (r10 == 0) goto L_0x0147
            com.google.gson.JsonElement r10 = r2.b(r10)
            java.lang.String r11 = "_items"
            r5.a(r11, r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "replaceHybridPresetList jsonObject "
            r11.append(r12)
            r11.append(r5)
            java.lang.String r11 = r11.toString()
            r10.d(r8, r11)
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$replaceHybridPresetList$response$Anon1 r10 = new com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$replaceHybridPresetList$response$Anon1
            r10.<init>(r0, r5, r7)
            r3.L$0 = r0
            r3.L$1 = r1
            r3.L$2 = r2
            r3.L$3 = r5
            r3.label = r6
            java.lang.Object r2 = com.fossil.aj5.a(r10, r3)
            if (r2 != r4) goto L_0x00a5
            return r4
        L_0x00a5:
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            boolean r1 = r2 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x00fa
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            com.fossil.bj5 r2 = (com.fossil.bj5) r2
            java.lang.Object r2 = r2.a()
            if (r2 == 0) goto L_0x00f6
            com.portfolio.platform.data.source.remote.ApiResponse r2 = (com.portfolio.platform.data.source.remote.ApiResponse) r2
            java.util.List r2 = r2.get_items()
            java.util.Iterator r2 = r2.iterator()
        L_0x00c2:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x00d5
            java.lang.Object r3 = r2.next()
            com.portfolio.platform.data.model.room.microapp.HybridPreset r3 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r3
            r3.setPinType(r9)
            r1.add(r3)
            goto L_0x00c2
        L_0x00d5:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "replaceHybridPresetList success "
            r3.append(r4)
            r3.append(r1)
            java.lang.String r3 = r3.toString()
            r2.d(r8, r3)
            com.fossil.bj5 r2 = new com.fossil.bj5
            r3 = 2
            r2.<init>(r1, r9, r3, r7)
            goto L_0x0140
        L_0x00f6:
            com.fossil.ee7.a()
            throw r7
        L_0x00fa:
            boolean r1 = r2 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x0141
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "replaceHybridPresetList fail code "
            r3.append(r4)
            com.fossil.yi5 r2 = (com.fossil.yi5) r2
            int r4 = r2.a()
            r3.append(r4)
            java.lang.String r4 = " serverError "
            r3.append(r4)
            com.portfolio.platform.data.model.ServerError r4 = r2.c()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.d(r8, r3)
            com.fossil.yi5 r1 = new com.fossil.yi5
            int r10 = r2.a()
            com.portfolio.platform.data.model.ServerError r11 = r2.c()
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 28
            r16 = 0
            r9 = r1
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
            r2 = r1
        L_0x0140:
            return r2
        L_0x0141:
            com.fossil.p87 r1 = new com.fossil.p87
            r1.<init>()
            throw r1
        L_0x0147:
            com.fossil.x87 r1 = new com.fossil.x87
            java.lang.String r2 = "null cannot be cast to non-null type kotlin.Array<T>"
            r1.<init>(r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource.replaceHybridPresetList(java.util.List, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object upsertHybridPresetList(java.util.List<com.portfolio.platform.data.model.room.microapp.HybridPreset> r18, com.fossil.fb7<? super com.fossil.zi5<java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridPreset>>> r19) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            boolean r3 = r2 instanceof com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$upsertHybridPresetList$Anon1
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$upsertHybridPresetList$Anon1 r3 = (com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$upsertHybridPresetList$Anon1) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$upsertHybridPresetList$Anon1 r3 = new com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$upsertHybridPresetList$Anon1
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 1
            r7 = 0
            java.lang.String r8 = "HybridPresetRemoteDataSource"
            r9 = 0
            if (r5 == 0) goto L_0x004b
            if (r5 != r6) goto L_0x0043
            java.lang.Object r1 = r3.L$3
            com.fossil.ie4 r1 = (com.fossil.ie4) r1
            java.lang.Object r1 = r3.L$2
            com.google.gson.Gson r1 = (com.google.gson.Gson) r1
            java.lang.Object r1 = r3.L$1
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r1 = r3.L$0
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource r1 = (com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource) r1
            com.fossil.t87.a(r2)
            goto L_0x00a5
        L_0x0043:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x004b:
            com.fossil.t87.a(r2)
            com.fossil.be4 r2 = new com.fossil.be4
            r2.<init>()
            com.fossil.od5 r5 = new com.fossil.od5
            r5.<init>()
            r2.b(r5)
            com.google.gson.Gson r2 = r2.a()
            com.fossil.ie4 r5 = new com.fossil.ie4
            r5.<init>()
            com.portfolio.platform.data.model.room.microapp.HybridPreset[] r10 = new com.portfolio.platform.data.model.room.microapp.HybridPreset[r9]
            java.lang.Object[] r10 = r1.toArray(r10)
            if (r10 == 0) goto L_0x0147
            com.google.gson.JsonElement r10 = r2.b(r10)
            java.lang.String r11 = "_items"
            r5.a(r11, r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "upsertHybridPresetList jsonObject "
            r11.append(r12)
            r11.append(r5)
            java.lang.String r11 = r11.toString()
            r10.d(r8, r11)
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$upsertHybridPresetList$response$Anon1 r10 = new com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$upsertHybridPresetList$response$Anon1
            r10.<init>(r0, r5, r7)
            r3.L$0 = r0
            r3.L$1 = r1
            r3.L$2 = r2
            r3.L$3 = r5
            r3.label = r6
            java.lang.Object r2 = com.fossil.aj5.a(r10, r3)
            if (r2 != r4) goto L_0x00a5
            return r4
        L_0x00a5:
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            boolean r1 = r2 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x00fa
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            com.fossil.bj5 r2 = (com.fossil.bj5) r2
            java.lang.Object r2 = r2.a()
            if (r2 == 0) goto L_0x00f6
            com.portfolio.platform.data.source.remote.ApiResponse r2 = (com.portfolio.platform.data.source.remote.ApiResponse) r2
            java.util.List r2 = r2.get_items()
            java.util.Iterator r2 = r2.iterator()
        L_0x00c2:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x00d5
            java.lang.Object r3 = r2.next()
            com.portfolio.platform.data.model.room.microapp.HybridPreset r3 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r3
            r3.setPinType(r9)
            r1.add(r3)
            goto L_0x00c2
        L_0x00d5:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "upsertHybridPresetList success "
            r3.append(r4)
            r3.append(r1)
            java.lang.String r3 = r3.toString()
            r2.d(r8, r3)
            com.fossil.bj5 r2 = new com.fossil.bj5
            r3 = 2
            r2.<init>(r1, r9, r3, r7)
            goto L_0x0140
        L_0x00f6:
            com.fossil.ee7.a()
            throw r7
        L_0x00fa:
            boolean r1 = r2 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x0141
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "upsertHybridPresetList fail code "
            r3.append(r4)
            com.fossil.yi5 r2 = (com.fossil.yi5) r2
            int r4 = r2.a()
            r3.append(r4)
            java.lang.String r4 = " serverError "
            r3.append(r4)
            com.portfolio.platform.data.model.ServerError r4 = r2.c()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.d(r8, r3)
            com.fossil.yi5 r1 = new com.fossil.yi5
            int r10 = r2.a()
            com.portfolio.platform.data.model.ServerError r11 = r2.c()
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 28
            r16 = 0
            r9 = r1
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
            r2 = r1
        L_0x0140:
            return r2
        L_0x0141:
            com.fossil.p87 r1 = new com.fossil.p87
            r1.<init>()
            throw r1
        L_0x0147:
            com.fossil.x87 r1 = new com.fossil.x87
            java.lang.String r2 = "null cannot be cast to non-null type kotlin.Array<T>"
            r1.<init>(r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource.upsertHybridPresetList(java.util.List, com.fossil.fb7):java.lang.Object");
    }
}
