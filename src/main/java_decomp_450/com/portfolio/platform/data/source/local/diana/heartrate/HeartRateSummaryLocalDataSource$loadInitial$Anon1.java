package com.portfolio.platform.data.source.local.diana.heartrate;

import com.fossil.ee7;
import com.fossil.ik7;
import com.fossil.te5;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryLocalDataSource$loadInitial$Anon1 implements te5.b {
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryLocalDataSource this$0;

    @DexIgnore
    public HeartRateSummaryLocalDataSource$loadInitial$Anon1(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        this.this$0 = heartRateSummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.fossil.te5.b
    public final void run(te5.b.a aVar) {
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource = this.this$0;
        heartRateSummaryLocalDataSource.calculateStartDate(heartRateSummaryLocalDataSource.mCreatedDate);
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource2 = this.this$0;
        Date mStartDate = heartRateSummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        ee7.a((Object) aVar, "helperCallback");
        ik7 unused = heartRateSummaryLocalDataSource2.loadData(mStartDate, mEndDate, aVar);
    }
}
