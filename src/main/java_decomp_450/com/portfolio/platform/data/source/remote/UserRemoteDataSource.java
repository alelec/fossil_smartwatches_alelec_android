package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ AuthApiGuestService mAuthApiGuestService;
    @DexIgnore
    public /* final */ AuthApiUserService mAuthApiUserService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = UserRemoteDataSource.class.getSimpleName();
        ee7.a((Object) simpleName, "UserRemoteDataSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public UserRemoteDataSource(ApiServiceV2 apiServiceV2, AuthApiGuestService authApiGuestService, AuthApiUserService authApiUserService) {
        ee7.b(apiServiceV2, "mApiService");
        ee7.b(authApiGuestService, "mAuthApiGuestService");
        ee7.b(authApiUserService, "mAuthApiUserService");
        this.mApiService = apiServiceV2;
        this.mAuthApiGuestService = authApiGuestService;
        this.mAuthApiUserService = authApiUserService;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object checkAuthenticationEmailExisting(java.lang.String r11, com.fossil.fb7<? super com.fossil.zi5<java.lang.Boolean>> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$checkAuthenticationEmailExisting$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$checkAuthenticationEmailExisting$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$checkAuthenticationEmailExisting$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$checkAuthenticationEmailExisting$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$checkAuthenticationEmailExisting$Anon1
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003e
            if (r2 != r3) goto L_0x0036
            java.lang.Object r11 = r0.L$2
            com.fossil.ie4 r11 = (com.fossil.ie4) r11
            java.lang.Object r11 = r0.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r11 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r11
            com.fossil.t87.a(r12)
            goto L_0x0099
        L_0x0036:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003e:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "checkAuthenticationEmailExisting for "
            r5.append(r6)
            r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.d(r2, r5)
            com.fossil.ie4 r12 = new com.fossil.ie4
            r12.<init>()
            java.lang.String r2 = "email"
            r12.a(r2, r11)     // Catch:{ Exception -> 0x0068 }
            goto L_0x0085
        L_0x0068:
            r2 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "Exception when generate jsonObject="
            r7.append(r8)
            r7.append(r2)
            java.lang.String r2 = r7.toString()
            r5.d(r6, r2)
        L_0x0085:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$checkAuthenticationEmailExisting$repoResponse$Anon1 r2 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$checkAuthenticationEmailExisting$repoResponse$Anon1
            r2.<init>(r10, r12, r4)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.label = r3
            java.lang.Object r12 = com.fossil.aj5.a(r2, r0)
            if (r12 != r1) goto L_0x0099
            return r1
        L_0x0099:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r11 = r12 instanceof com.fossil.bj5
            if (r11 == 0) goto L_0x00eb
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r1 = "checkAuthenticationEmailExisting Success"
            r11.d(r0, r1)
            com.fossil.bj5 r12 = (com.fossil.bj5) r12
            java.lang.Object r11 = r12.a()
            r0 = 2
            r1 = 0
            if (r11 == 0) goto L_0x00e1
            java.lang.Object r11 = r12.a()
            com.fossil.ie4 r11 = (com.fossil.ie4) r11
            java.lang.String r2 = "existing"
            boolean r11 = r11.d(r2)
            if (r11 == 0) goto L_0x00e1
            com.fossil.bj5 r11 = new com.fossil.bj5
            java.lang.Object r12 = r12.a()
            com.fossil.ie4 r12 = (com.fossil.ie4) r12
            com.google.gson.JsonElement r12 = r12.a(r2)
            java.lang.String r2 = "repoResponse.response.get(JSON_KEY_IS_EXISTING)"
            com.fossil.ee7.a(r12, r2)
            boolean r12 = r12.a()
            java.lang.Boolean r12 = com.fossil.pb7.a(r12)
            r11.<init>(r12, r1, r0, r4)
            goto L_0x013a
        L_0x00e1:
            com.fossil.bj5 r11 = new com.fossil.bj5
            java.lang.Boolean r12 = com.fossil.pb7.a(r1)
            r11.<init>(r12, r1, r0, r4)
            goto L_0x013a
        L_0x00eb:
            boolean r11 = r12 instanceof com.fossil.yi5
            if (r11 == 0) goto L_0x013b
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "checkAuthenticationEmailExisting failed with error="
            r1.append(r2)
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            int r2 = r12.a()
            r1.append(r2)
            java.lang.String r2 = " message="
            r1.append(r2)
            com.portfolio.platform.data.model.ServerError r2 = r12.c()
            if (r2 == 0) goto L_0x0119
            java.lang.String r4 = r2.getMessage()
        L_0x0119:
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            r11.d(r0, r1)
            com.fossil.yi5 r11 = new com.fossil.yi5
            int r3 = r12.a()
            com.portfolio.platform.data.model.ServerError r4 = r12.c()
            java.lang.Throwable r5 = r12.d()
            r6 = 0
            r7 = 0
            r8 = 24
            r9 = 0
            r2 = r11
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)
        L_0x013a:
            return r11
        L_0x013b:
            com.fossil.p87 r11 = new com.fossil.p87
            r11.<init>()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.checkAuthenticationEmailExisting(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object checkAuthenticationSocialExisting(java.lang.String r10, java.lang.String r11, com.fossil.fb7<? super com.fossil.zi5<java.lang.Boolean>> r12) {
        /*
            r9 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$checkAuthenticationSocialExisting$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$checkAuthenticationSocialExisting$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$checkAuthenticationSocialExisting$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$checkAuthenticationSocialExisting$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$checkAuthenticationSocialExisting$Anon1
            r0.<init>(r9, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0042
            if (r2 != r3) goto L_0x003a
            java.lang.Object r10 = r0.L$3
            com.fossil.ie4 r10 = (com.fossil.ie4) r10
            java.lang.Object r10 = r0.L$2
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r10 = r0.L$1
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r10 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r10 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r10
            com.fossil.t87.a(r12)
            goto L_0x0088
        L_0x003a:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x0042:
            com.fossil.t87.a(r12)
            com.fossil.ie4 r12 = new com.fossil.ie4
            r12.<init>()
            java.lang.String r2 = "service"
            r12.a(r2, r10)     // Catch:{ Exception -> 0x0055 }
            java.lang.String r2 = "token"
            r12.a(r2, r11)     // Catch:{ Exception -> 0x0055 }
            goto L_0x0072
        L_0x0055:
            r2 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "Exception when generate jsonObject="
            r7.append(r8)
            r7.append(r2)
            java.lang.String r2 = r7.toString()
            r5.d(r6, r2)
        L_0x0072:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$checkAuthenticationSocialExisting$repoResponse$Anon1 r2 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$checkAuthenticationSocialExisting$repoResponse$Anon1
            r2.<init>(r9, r12, r4)
            r0.L$0 = r9
            r0.L$1 = r10
            r0.L$2 = r11
            r0.L$3 = r12
            r0.label = r3
            java.lang.Object r12 = com.fossil.aj5.a(r2, r0)
            if (r12 != r1) goto L_0x0088
            return r1
        L_0x0088:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r10 = r12 instanceof com.fossil.bj5
            if (r10 == 0) goto L_0x00da
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r0 = "checkAuthenticationSocialExisting Success"
            r10.d(r11, r0)
            com.fossil.bj5 r12 = (com.fossil.bj5) r12
            java.lang.Object r10 = r12.a()
            r11 = 2
            r0 = 0
            if (r10 == 0) goto L_0x00d0
            java.lang.Object r10 = r12.a()
            com.fossil.ie4 r10 = (com.fossil.ie4) r10
            java.lang.String r1 = "existing"
            boolean r10 = r10.d(r1)
            if (r10 == 0) goto L_0x00d0
            com.fossil.bj5 r10 = new com.fossil.bj5
            java.lang.Object r12 = r12.a()
            com.fossil.ie4 r12 = (com.fossil.ie4) r12
            com.google.gson.JsonElement r12 = r12.a(r1)
            java.lang.String r1 = "repoResponse.response.get(JSON_KEY_IS_EXISTING)"
            com.fossil.ee7.a(r12, r1)
            boolean r12 = r12.a()
            java.lang.Boolean r12 = com.fossil.pb7.a(r12)
            r10.<init>(r12, r0, r11, r4)
            goto L_0x0129
        L_0x00d0:
            com.fossil.bj5 r10 = new com.fossil.bj5
            java.lang.Boolean r12 = com.fossil.pb7.a(r0)
            r10.<init>(r12, r0, r11, r4)
            goto L_0x0129
        L_0x00da:
            boolean r10 = r12 instanceof com.fossil.yi5
            if (r10 == 0) goto L_0x012a
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "checkAuthenticationSocialExisting failed with error="
            r0.append(r1)
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            int r1 = r12.a()
            r0.append(r1)
            java.lang.String r1 = " message="
            r0.append(r1)
            com.portfolio.platform.data.model.ServerError r1 = r12.c()
            if (r1 == 0) goto L_0x0108
            java.lang.String r4 = r1.getMessage()
        L_0x0108:
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            r10.d(r11, r0)
            com.fossil.yi5 r10 = new com.fossil.yi5
            int r2 = r12.a()
            com.portfolio.platform.data.model.ServerError r3 = r12.c()
            java.lang.Throwable r4 = r12.d()
            r5 = 0
            r6 = 0
            r7 = 24
            r8 = 0
            r1 = r10
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
        L_0x0129:
            return r10
        L_0x012a:
            com.fossil.p87 r10 = new com.fossil.p87
            r10.<init>()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.checkAuthenticationSocialExisting(java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deleteUser(com.fossil.fb7<? super java.lang.Integer> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$deleteUser$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$deleteUser$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$deleteUser$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$deleteUser$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$deleteUser$Anon1
            r0.<init>(r5, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r0
            com.fossil.t87.a(r6)
            goto L_0x0056
        L_0x002d:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L_0x0035:
            com.fossil.t87.a(r6)
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r4 = "deleteUser"
            r6.d(r2, r4)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$deleteUser$response$Anon1 r6 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$deleteUser$response$Anon1
            r2 = 0
            r6.<init>(r5, r2)
            r0.L$0 = r5
            r0.label = r3
            java.lang.Object r6 = com.fossil.aj5.a(r6, r0)
            if (r6 != r1) goto L_0x0056
            return r1
        L_0x0056:
            com.fossil.zi5 r6 = (com.fossil.zi5) r6
            boolean r0 = r6 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x005f
            r6 = 200(0xc8, float:2.8E-43)
            goto L_0x0069
        L_0x005f:
            boolean r0 = r6 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x006e
            com.fossil.yi5 r6 = (com.fossil.yi5) r6
            int r6 = r6.a()
        L_0x0069:
            java.lang.Integer r6 = com.fossil.pb7.a(r6)
            return r6
        L_0x006e:
            com.fossil.p87 r6 = new com.fossil.p87
            r6.<init>()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.deleteUser(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getUserSettingFromServer(com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.model.UserSettings>> r12) {
        /*
            r11 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$getUserSettingFromServer$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$getUserSettingFromServer$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$getUserSettingFromServer$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$getUserSettingFromServer$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$getUserSettingFromServer$Anon1
            r0.<init>(r11, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0036
            if (r2 != r3) goto L_0x002e
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r0
            com.fossil.t87.a(r12)
            goto L_0x0056
        L_0x002e:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r0)
            throw r12
        L_0x0036:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r5 = "getUserSettingFromServer()"
            r12.d(r2, r5)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$getUserSettingFromServer$response$Anon1 r12 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$getUserSettingFromServer$response$Anon1
            r12.<init>(r11, r4)
            r0.L$0 = r11
            r0.label = r3
            java.lang.Object r12 = com.fossil.aj5.a(r12, r0)
            if (r12 != r1) goto L_0x0056
            return r1
        L_0x0056:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r0 = r12 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x008c
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "getUserSettingFromServer() success, userSetting="
            r2.append(r3)
            com.fossil.bj5 r12 = (com.fossil.bj5) r12
            java.lang.Object r3 = r12.a()
            com.portfolio.platform.data.model.UserSettings r3 = (com.portfolio.platform.data.model.UserSettings) r3
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            com.fossil.bj5 r0 = new com.fossil.bj5
            java.lang.Object r12 = r12.a()
            r1 = 0
            r2 = 2
            r0.<init>(r12, r1, r2, r4)
            goto L_0x00db
        L_0x008c:
            boolean r0 = r12 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x00dc
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "getUserSettingFromServer() failed, errorCode="
            r2.append(r3)
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            int r3 = r12.a()
            r2.append(r3)
            java.lang.String r3 = ", message="
            r2.append(r3)
            com.portfolio.platform.data.model.ServerError r3 = r12.c()
            if (r3 == 0) goto L_0x00ba
            java.lang.String r4 = r3.getMessage()
        L_0x00ba:
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            com.fossil.yi5 r0 = new com.fossil.yi5
            int r4 = r12.a()
            com.portfolio.platform.data.model.ServerError r5 = r12.c()
            java.lang.Throwable r6 = r12.d()
            r7 = 0
            r8 = 0
            r9 = 24
            r10 = 0
            r3 = r0
            r3.<init>(r4, r5, r6, r7, r8, r9, r10)
        L_0x00db:
            return r0
        L_0x00dc:
            com.fossil.p87 r12 = new com.fossil.p87
            r12.<init>()
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.getUserSettingFromServer(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01c7  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object loadUserInfo(com.portfolio.platform.data.model.MFUser r14, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.model.MFUser>> r15) {
        /*
            r13 = this;
            boolean r0 = r15 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$loadUserInfo$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r15
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$loadUserInfo$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$loadUserInfo$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$loadUserInfo$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$loadUserInfo$Anon1
            r0.<init>(r13, r15)
        L_0x0018:
            java.lang.Object r15 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003a
            if (r2 != r3) goto L_0x0032
            java.lang.Object r14 = r0.L$1
            com.portfolio.platform.data.model.MFUser r14 = (com.portfolio.platform.data.model.MFUser) r14
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r0
            com.fossil.t87.a(r15)
            goto L_0x005c
        L_0x0032:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.String r15 = "call to 'resume' before 'invoke' with coroutine"
            r14.<init>(r15)
            throw r14
        L_0x003a:
            com.fossil.t87.a(r15)
            com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r5 = "loadUserInfo"
            r15.d(r2, r5)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$loadUserInfo$response$Anon1 r15 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$loadUserInfo$response$Anon1
            r15.<init>(r13, r4)
            r0.L$0 = r13
            r0.L$1 = r14
            r0.label = r3
            java.lang.Object r15 = com.fossil.aj5.a(r15, r0)
            if (r15 != r1) goto L_0x005c
            return r1
        L_0x005c:
            com.fossil.zi5 r15 = (com.fossil.zi5) r15
            boolean r0 = r15 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x01c7
            com.fossil.bj5 r15 = (com.fossil.bj5) r15
            java.lang.Object r15 = r15.a()
            com.portfolio.platform.data.UserWrapper r15 = (com.portfolio.platform.data.UserWrapper) r15
            if (r15 == 0) goto L_0x01aa
            com.portfolio.platform.data.model.MFUser r15 = r15.toMFUser()
            java.lang.String r0 = r14.getUpdatedAt()
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "loadUserInfo Success responseAfterParse "
            r3.append(r5)
            r3.append(r15)
            java.lang.String r5 = " localUpdateAt "
            r3.append(r5)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            r1 = 2
            r2 = 0
            if (r0 == 0) goto L_0x016e
            java.lang.String r3 = r15.getUpdatedAt()
            if (r3 == 0) goto L_0x0151
            java.util.Date r3 = com.fossil.zd5.d(r3)
            java.lang.String r6 = "DateHelper.parseJodaTime(it)"
            com.fossil.ee7.a(r3, r6)
            long r6 = r3.getTime()
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r8 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "loadUserInfo Success "
            r9.append(r10)
            r9.append(r15)
            java.lang.String r11 = " remoteUpdateAt "
            r9.append(r11)
            r9.append(r6)
            java.lang.String r9 = r9.toString()
            r3.d(r8, r9)
            java.util.Date r0 = com.fossil.zd5.d(r0)
            java.lang.String r3 = "DateHelper.parseJodaTime(localUpdateAt)"
            com.fossil.ee7.a(r0, r3)
            long r8 = r0.getTime()
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            r11.append(r10)
            r11.append(r15)
            r11.append(r5)
            r11.append(r8)
            java.lang.String r5 = r11.toString()
            r0.d(r3, r5)
            int r0 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r0 < 0) goto L_0x012a
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "response user created at "
            r3.append(r5)
            java.lang.String r5 = r15.getCreatedAt()
            r3.append(r5)
            java.lang.String r3 = r3.toString()
            r14.d(r0, r3)
            com.fossil.bj5 r14 = new com.fossil.bj5
            r14.<init>(r15, r2, r1, r4)
            goto L_0x0150
        L_0x012a:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "user created at "
            r5.append(r6)
            java.lang.String r15 = r15.getCreatedAt()
            r5.append(r15)
            java.lang.String r15 = r5.toString()
            r0.d(r3, r15)
            com.fossil.bj5 r15 = new com.fossil.bj5
            r15.<init>(r14, r2, r1, r4)
            r14 = r15
        L_0x0150:
            return r14
        L_0x0151:
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r15 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r0 = "remoteUpdateAt is null"
            r14.e(r15, r0)
            com.fossil.yi5 r14 = new com.fossil.yi5
            r2 = 600(0x258, float:8.41E-43)
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 24
            r8 = 0
            r1 = r14
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            return r14
        L_0x016e:
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "localCreateAt is null - remoteUpdateAt: "
            r3.append(r5)
            java.lang.String r5 = r15.getUpdatedAt()
            r3.append(r5)
            java.lang.String r3 = r3.toString()
            r14.e(r0, r3)
            java.lang.String r14 = r15.getUpdatedAt()
            if (r14 == 0) goto L_0x019a
            com.fossil.bj5 r14 = new com.fossil.bj5
            r14.<init>(r15, r2, r1, r4)
            return r14
        L_0x019a:
            com.fossil.yi5 r14 = new com.fossil.yi5
            r6 = 600(0x258, float:8.41E-43)
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 24
            r12 = 0
            r5 = r14
            r5.<init>(r6, r7, r8, r9, r10, r11, r12)
            return r14
        L_0x01aa:
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r15 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r0 = "server return null-response"
            r14.e(r15, r0)
            com.fossil.yi5 r14 = new com.fossil.yi5
            r2 = 600(0x258, float:8.41E-43)
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 24
            r8 = 0
            r1 = r14
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            return r14
        L_0x01c7:
            boolean r14 = r15 instanceof com.fossil.yi5
            if (r14 == 0) goto L_0x0217
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "loadUserInfo Failure code="
            r1.append(r2)
            com.fossil.yi5 r15 = (com.fossil.yi5) r15
            int r2 = r15.a()
            r1.append(r2)
            java.lang.String r2 = " message="
            r1.append(r2)
            com.portfolio.platform.data.model.ServerError r2 = r15.c()
            if (r2 == 0) goto L_0x01f5
            java.lang.String r4 = r2.getMessage()
        L_0x01f5:
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            r14.d(r0, r1)
            com.fossil.yi5 r14 = new com.fossil.yi5
            int r3 = r15.a()
            com.portfolio.platform.data.model.ServerError r4 = r15.c()
            java.lang.Throwable r5 = r15.d()
            r6 = 0
            r7 = 0
            r8 = 24
            r9 = 0
            r2 = r14
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)
            return r14
        L_0x0217:
            com.fossil.p87 r14 = new com.fossil.p87
            r14.<init>()
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.loadUserInfo(com.portfolio.platform.data.model.MFUser, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object loginEmail(java.lang.String r9, java.lang.String r10, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.Auth>> r11) {
        /*
            r8 = this;
            boolean r0 = r11 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginEmail$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginEmail$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginEmail$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginEmail$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginEmail$Anon1
            r0.<init>(r8, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0042
            if (r2 != r4) goto L_0x003a
            java.lang.Object r9 = r0.L$3
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$2
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r9 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r9
            com.fossil.t87.a(r11)
            goto L_0x0084
        L_0x003a:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0042:
            com.fossil.t87.a(r11)
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r5 = "loginEmail"
            r11.d(r2, r5)
            com.fossil.ie4 r11 = new com.fossil.ie4
            r11.<init>()
            java.lang.String r2 = "email"
            r11.a(r2, r9)
            java.lang.String r2 = "password"
            r11.a(r2, r10)
            com.fossil.rd5$a r2 = com.fossil.rd5.g
            java.lang.String r5 = ""
            java.lang.String r2 = r2.a(r5)
            java.lang.String r5 = "clientId"
            r11.a(r5, r2)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginEmail$response$Anon1 r2 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginEmail$response$Anon1
            r2.<init>(r8, r11, r3)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.L$3 = r11
            r0.label = r4
            java.lang.Object r11 = com.fossil.aj5.a(r2, r0)
            if (r11 != r1) goto L_0x0084
            return r1
        L_0x0084:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            boolean r9 = r11 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x0098
            com.fossil.bj5 r9 = new com.fossil.bj5
            com.fossil.bj5 r11 = (com.fossil.bj5) r11
            java.lang.Object r10 = r11.a()
            r11 = 0
            r0 = 2
            r9.<init>(r10, r11, r0, r3)
            goto L_0x00b5
        L_0x0098:
            boolean r9 = r11 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00b6
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r11 = (com.fossil.yi5) r11
            int r1 = r11.a()
            com.portfolio.platform.data.model.ServerError r2 = r11.c()
            java.lang.Throwable r3 = r11.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x00b5:
            return r9
        L_0x00b6:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.loginEmail(java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object loginWithSocial(java.lang.String r9, java.lang.String r10, java.lang.String r11, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.Auth>> r12) {
        /*
            r8 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginWithSocial$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginWithSocial$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginWithSocial$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginWithSocial$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginWithSocial$Anon1
            r0.<init>(r8, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0046
            if (r2 != r4) goto L_0x003e
            java.lang.Object r9 = r0.L$4
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$3
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$2
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r9 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r9
            com.fossil.t87.a(r12)
            goto L_0x00a9
        L_0x003e:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0046:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "loginWithSocial service "
            r5.append(r6)
            r5.append(r9)
            java.lang.String r6 = " token "
            r5.append(r6)
            r5.append(r10)
            java.lang.String r6 = " clientId "
            r5.append(r6)
            r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.d(r2, r5)
            com.fossil.ie4 r12 = new com.fossil.ie4
            r12.<init>()
            java.lang.String r2 = "service"
            r12.a(r2, r9)
            java.lang.String r2 = "token"
            r12.a(r2, r10)
            com.fossil.rd5$a r2 = com.fossil.rd5.g
            java.lang.String r5 = ""
            java.lang.String r2 = r2.a(r5)
            java.lang.String r5 = "clientId"
            r12.a(r5, r2)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginWithSocial$response$Anon1 r2 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginWithSocial$response$Anon1
            r2.<init>(r8, r12, r3)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.L$3 = r11
            r0.L$4 = r12
            r0.label = r4
            java.lang.Object r12 = com.fossil.aj5.a(r2, r0)
            if (r12 != r1) goto L_0x00a9
            return r1
        L_0x00a9:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r9 = r12 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x00bd
            com.fossil.bj5 r9 = new com.fossil.bj5
            com.fossil.bj5 r12 = (com.fossil.bj5) r12
            java.lang.Object r10 = r12.a()
            r11 = 0
            r12 = 2
            r9.<init>(r10, r11, r12, r3)
            goto L_0x00da
        L_0x00bd:
            boolean r9 = r12 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00db
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            int r1 = r12.a()
            com.portfolio.platform.data.model.ServerError r2 = r12.c()
            java.lang.Throwable r3 = r12.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x00da:
            return r9
        L_0x00db:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.loginWithSocial(java.lang.String, java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object logoutUser(com.fossil.fb7<? super java.lang.Integer> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$Anon1
            r0.<init>(r5, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r0
            com.fossil.t87.a(r6)
            goto L_0x0056
        L_0x002d:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L_0x0035:
            com.fossil.t87.a(r6)
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r4 = "logoutUser"
            r6.d(r2, r4)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$response$Anon1 r6 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$response$Anon1
            r2 = 0
            r6.<init>(r5, r2)
            r0.L$0 = r5
            r0.label = r3
            java.lang.Object r6 = com.fossil.aj5.a(r6, r0)
            if (r6 != r1) goto L_0x0056
            return r1
        L_0x0056:
            com.fossil.zi5 r6 = (com.fossil.zi5) r6
            boolean r0 = r6 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x005f
            r6 = 200(0xc8, float:2.8E-43)
            goto L_0x0069
        L_0x005f:
            boolean r0 = r6 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x006e
            com.fossil.yi5 r6 = (com.fossil.yi5) r6
            int r6 = r6.a()
        L_0x0069:
            java.lang.Integer r6 = com.fossil.pb7.a(r6)
            return r6
        L_0x006e:
            com.fossil.p87 r6 = new com.fossil.p87
            r6.<init>()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.logoutUser(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object requestEmailOtp(java.lang.String r11, com.fossil.fb7<? super com.fossil.zi5<java.lang.Void>> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$requestEmailOtp$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$requestEmailOtp$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$requestEmailOtp$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$requestEmailOtp$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$requestEmailOtp$Anon1
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003e
            if (r2 != r3) goto L_0x0036
            java.lang.Object r11 = r0.L$2
            com.fossil.ie4 r11 = (com.fossil.ie4) r11
            java.lang.Object r11 = r0.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r11 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r11
            com.fossil.t87.a(r12)
            goto L_0x007b
        L_0x0036:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003e:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "requestEmailOtp email: "
            r5.append(r6)
            r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.d(r2, r5)
            com.fossil.ie4 r12 = new com.fossil.ie4
            r12.<init>()
            java.lang.String r2 = "email"
            r12.a(r2, r11)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$requestEmailOtp$repoResponse$Anon1 r2 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$requestEmailOtp$repoResponse$Anon1
            r2.<init>(r10, r12, r4)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.label = r3
            java.lang.Object r12 = com.fossil.aj5.a(r2, r0)
            if (r12 != r1) goto L_0x007b
            return r1
        L_0x007b:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r11 = r12 instanceof com.fossil.bj5
            if (r11 == 0) goto L_0x0096
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r12 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r0 = "requestEmailOtp Success"
            r11.d(r12, r0)
            com.fossil.bj5 r11 = new com.fossil.bj5
            r12 = 0
            r0 = 2
            r11.<init>(r4, r12, r0, r4)
            goto L_0x00e5
        L_0x0096:
            boolean r11 = r12 instanceof com.fossil.yi5
            if (r11 == 0) goto L_0x00e6
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "requestEmailOtp failed with error="
            r1.append(r2)
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            int r2 = r12.a()
            r1.append(r2)
            java.lang.String r2 = " message="
            r1.append(r2)
            com.portfolio.platform.data.model.ServerError r2 = r12.c()
            if (r2 == 0) goto L_0x00c4
            java.lang.String r4 = r2.getMessage()
        L_0x00c4:
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            r11.d(r0, r1)
            com.fossil.yi5 r11 = new com.fossil.yi5
            int r3 = r12.a()
            com.portfolio.platform.data.model.ServerError r4 = r12.c()
            java.lang.Throwable r5 = r12.d()
            r6 = 0
            r7 = 0
            r8 = 24
            r9 = 0
            r2 = r11
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)
        L_0x00e5:
            return r11
        L_0x00e6:
            com.fossil.p87 r11 = new com.fossil.p87
            r11.<init>()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.requestEmailOtp(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object resetPassword(java.lang.String r11, com.fossil.fb7<? super com.fossil.zi5<java.lang.Integer>> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$resetPassword$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$resetPassword$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$resetPassword$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$resetPassword$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$resetPassword$Anon1
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003e
            if (r2 != r3) goto L_0x0036
            java.lang.Object r11 = r0.L$2
            com.fossil.ie4 r11 = (com.fossil.ie4) r11
            java.lang.Object r11 = r0.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r11 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r11
            com.fossil.t87.a(r12)
            goto L_0x006c
        L_0x0036:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003e:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r5 = "resetPassword"
            r12.d(r2, r5)
            com.fossil.ie4 r12 = new com.fossil.ie4
            r12.<init>()
            java.lang.String r2 = "email"
            r12.a(r2, r11)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$resetPassword$repoResponse$Anon1 r2 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$resetPassword$repoResponse$Anon1
            r2.<init>(r10, r12, r4)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.label = r3
            java.lang.Object r12 = com.fossil.aj5.a(r2, r0)
            if (r12 != r1) goto L_0x006c
            return r1
        L_0x006c:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r11 = r12 instanceof com.fossil.bj5
            if (r11 == 0) goto L_0x008d
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r12 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r0 = "resetPassword Success"
            r11.d(r12, r0)
            com.fossil.bj5 r11 = new com.fossil.bj5
            r12 = 200(0xc8, float:2.8E-43)
            java.lang.Integer r12 = com.fossil.pb7.a(r12)
            r0 = 0
            r1 = 2
            r11.<init>(r12, r0, r1, r4)
            goto L_0x00dc
        L_0x008d:
            boolean r11 = r12 instanceof com.fossil.yi5
            if (r11 == 0) goto L_0x00dd
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "resetPassword failed with error="
            r1.append(r2)
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            int r2 = r12.a()
            r1.append(r2)
            java.lang.String r2 = " message="
            r1.append(r2)
            com.portfolio.platform.data.model.ServerError r2 = r12.c()
            if (r2 == 0) goto L_0x00bb
            java.lang.String r4 = r2.getMessage()
        L_0x00bb:
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            r11.d(r0, r1)
            com.fossil.yi5 r11 = new com.fossil.yi5
            int r3 = r12.a()
            com.portfolio.platform.data.model.ServerError r4 = r12.c()
            java.lang.Throwable r5 = r12.d()
            r6 = 0
            r7 = 0
            r8 = 24
            r9 = 0
            r2 = r11
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)
        L_0x00dc:
            return r11
        L_0x00dd:
            com.fossil.p87 r11 = new com.fossil.p87
            r11.<init>()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.resetPassword(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object sendUserSettingToServer(com.portfolio.platform.data.model.UserSettings r11, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.model.UserSettings>> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$sendUserSettingToServer$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$sendUserSettingToServer$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$sendUserSettingToServer$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$sendUserSettingToServer$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$sendUserSettingToServer$Anon1
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003e
            if (r2 != r3) goto L_0x0036
            java.lang.Object r11 = r0.L$2
            com.fossil.ie4 r11 = (com.fossil.ie4) r11
            java.lang.Object r11 = r0.L$1
            com.portfolio.platform.data.model.UserSettings r11 = (com.portfolio.platform.data.model.UserSettings) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r11 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r11
            com.fossil.t87.a(r12)
            goto L_0x008c
        L_0x0036:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003e:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "updateUserSetting, userSetting="
            r5.append(r6)
            r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.d(r2, r5)
            com.fossil.ie4 r12 = new com.fossil.ie4
            r12.<init>()
            java.lang.String r2 = r11.getUid()
            java.lang.String r5 = "uid"
            r12.a(r5, r2)
            boolean r2 = r11.isShowGoalRing()
            java.lang.Boolean r2 = com.fossil.pb7.a(r2)
            java.lang.String r5 = "isShowGoalRing"
            r12.a(r5, r2)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$sendUserSettingToServer$response$Anon1 r2 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$sendUserSettingToServer$response$Anon1
            r2.<init>(r10, r12, r4)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.label = r3
            java.lang.Object r12 = com.fossil.aj5.a(r2, r0)
            if (r12 != r1) goto L_0x008c
            return r1
        L_0x008c:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r11 = r12 instanceof com.fossil.bj5
            if (r11 == 0) goto L_0x00ad
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r1 = "updateUserSetting success"
            r11.d(r0, r1)
            com.fossil.bj5 r11 = new com.fossil.bj5
            com.fossil.bj5 r12 = (com.fossil.bj5) r12
            java.lang.Object r12 = r12.a()
            r0 = 0
            r1 = 2
            r11.<init>(r12, r0, r1, r4)
            goto L_0x00fc
        L_0x00ad:
            boolean r11 = r12 instanceof com.fossil.yi5
            if (r11 == 0) goto L_0x00fd
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "updateUserSetting failed, errorCode="
            r1.append(r2)
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            int r2 = r12.a()
            r1.append(r2)
            java.lang.String r2 = ", message="
            r1.append(r2)
            com.portfolio.platform.data.model.ServerError r2 = r12.c()
            if (r2 == 0) goto L_0x00db
            java.lang.String r4 = r2.getMessage()
        L_0x00db:
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            r11.d(r0, r1)
            com.fossil.yi5 r11 = new com.fossil.yi5
            int r3 = r12.a()
            com.portfolio.platform.data.model.ServerError r4 = r12.c()
            java.lang.Throwable r5 = r12.d()
            r6 = 0
            r7 = 0
            r8 = 24
            r9 = 0
            r2 = r11
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)
        L_0x00fc:
            return r11
        L_0x00fd:
            com.fossil.p87 r11 = new com.fossil.p87
            r11.<init>()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.sendUserSettingToServer(com.portfolio.platform.data.model.UserSettings, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object signUpEmail(com.portfolio.platform.data.SignUpEmailAuth r9, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.Auth>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$Anon1
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x003a
            if (r2 != r4) goto L_0x0032
            java.lang.Object r9 = r0.L$1
            com.portfolio.platform.data.SignUpEmailAuth r9 = (com.portfolio.platform.data.SignUpEmailAuth) r9
            java.lang.Object r9 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r9 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r9
            com.fossil.t87.a(r10)
            goto L_0x006b
        L_0x0032:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003a:
            com.fossil.t87.a(r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "signUpEmail auth "
            r5.append(r6)
            r5.append(r9)
            java.lang.String r5 = r5.toString()
            r10.d(r2, r5)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$response$Anon1 r10 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$response$Anon1
            r10.<init>(r8, r9, r3)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r4
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x006b
            return r1
        L_0x006b:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x007f
            com.fossil.bj5 r9 = new com.fossil.bj5
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r10 = r10.a()
            r0 = 0
            r1 = 2
            r9.<init>(r10, r0, r1, r3)
            goto L_0x009c
        L_0x007f:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x009d
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x009c:
            return r9
        L_0x009d:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.signUpEmail(com.portfolio.platform.data.SignUpEmailAuth, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object signUpSocial(com.portfolio.platform.data.SignUpSocialAuth r9, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.Auth>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$Anon1
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x003a
            if (r2 != r4) goto L_0x0032
            java.lang.Object r9 = r0.L$1
            com.portfolio.platform.data.SignUpSocialAuth r9 = (com.portfolio.platform.data.SignUpSocialAuth) r9
            java.lang.Object r9 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r9 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r9
            com.fossil.t87.a(r10)
            goto L_0x006b
        L_0x0032:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003a:
            com.fossil.t87.a(r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "signUpSocial auth "
            r5.append(r6)
            r5.append(r9)
            java.lang.String r5 = r5.toString()
            r10.d(r2, r5)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$response$Anon1 r10 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$response$Anon1
            r10.<init>(r8, r9, r3)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r4
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x006b
            return r1
        L_0x006b:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x007f
            com.fossil.bj5 r9 = new com.fossil.bj5
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r10 = r10.a()
            r0 = 0
            r1 = 2
            r9.<init>(r10, r0, r1, r3)
            goto L_0x009c
        L_0x007f:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x009d
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x009c:
            return r9
        L_0x009d:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.signUpSocial(com.portfolio.platform.data.SignUpSocialAuth, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0294  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x02b8  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0031  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object updateUser(com.portfolio.platform.data.model.MFUser r17, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.model.MFUser>> r18) {
        /*
            r16 = this;
            r1 = r16
            r0 = r18
            java.lang.String r2 = "weightInGrams"
            java.lang.String r3 = "heightInCentimeters"
            java.lang.String r4 = "StandardCharsets.UTF_8"
            java.lang.String r5 = "(this as java.lang.String).getBytes(charset)"
            boolean r6 = r0 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$updateUser$Anon1
            if (r6 == 0) goto L_0x001f
            r6 = r0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$updateUser$Anon1 r6 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$updateUser$Anon1) r6
            int r7 = r6.label
            r8 = -2147483648(0xffffffff80000000, float:-0.0)
            r9 = r7 & r8
            if (r9 == 0) goto L_0x001f
            int r7 = r7 - r8
            r6.label = r7
            goto L_0x0024
        L_0x001f:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$updateUser$Anon1 r6 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$updateUser$Anon1
            r6.<init>(r1, r0)
        L_0x0024:
            java.lang.Object r0 = r6.result
            java.lang.Object r7 = com.fossil.nb7.a()
            int r8 = r6.label
            r9 = 1
            r11 = 0
            r12 = 0
            if (r8 == 0) goto L_0x004c
            if (r8 != r9) goto L_0x0044
            java.lang.Object r2 = r6.L$2
            com.fossil.ie4 r2 = (com.fossil.ie4) r2
            java.lang.Object r2 = r6.L$1
            com.portfolio.platform.data.model.MFUser r2 = (com.portfolio.platform.data.model.MFUser) r2
            java.lang.Object r2 = r6.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r2 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r2
            com.fossil.t87.a(r0)
            goto L_0x028e
        L_0x0044:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x004c:
            com.fossil.t87.a(r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r8 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r13 = "updateUser"
            r0.d(r8, r13)
            com.fossil.ie4 r8 = new com.fossil.ie4
            r8.<init>()
            java.lang.String r0 = "firstName"
            java.lang.String r13 = r17.getFirstName()     // Catch:{ Exception -> 0x025a }
            java.lang.String r14 = ""
            if (r13 == 0) goto L_0x006c
            goto L_0x006d
        L_0x006c:
            r13 = r14
        L_0x006d:
            java.nio.charset.Charset r15 = com.fossil.sg7.a
            java.lang.String r9 = "null cannot be cast to non-null type java.lang.String"
            if (r13 == 0) goto L_0x0254
            byte[] r13 = r13.getBytes(r15)
            com.fossil.ee7.a(r13, r5)
            java.nio.charset.Charset r15 = java.nio.charset.StandardCharsets.UTF_8
            com.fossil.ee7.a(r15, r4)
            java.lang.String r10 = new java.lang.String
            r10.<init>(r13, r15)
            r8.a(r0, r10)
            java.lang.String r0 = "lastName"
            java.lang.String r10 = r17.getLastName()
            if (r10 == 0) goto L_0x0090
            goto L_0x0091
        L_0x0090:
            r10 = r14
        L_0x0091:
            java.nio.charset.Charset r13 = com.fossil.sg7.a
            if (r10 == 0) goto L_0x024e
            byte[] r9 = r10.getBytes(r13)
            com.fossil.ee7.a(r9, r5)
            java.nio.charset.Charset r5 = java.nio.charset.StandardCharsets.UTF_8
            com.fossil.ee7.a(r5, r4)
            java.lang.String r4 = new java.lang.String
            r4.<init>(r9, r5)
            r8.a(r0, r4)
            int r0 = r17.getHeightInCentimeters()
            java.lang.Integer r0 = com.fossil.pb7.a(r0)
            r8.a(r3, r0)
            int r0 = r17.getWeightInGrams()
            java.lang.Integer r0 = com.fossil.pb7.a(r0)
            r8.a(r2, r0)
            java.lang.String r0 = "gender"
            java.lang.String r4 = r17.getGender()
            r8.a(r0, r4)
            com.fossil.ie4 r0 = new com.fossil.ie4
            r0.<init>()
            java.lang.String r4 = "height"
            com.portfolio.platform.data.model.MFUser$UnitGroup r5 = r17.getUnitGroup()
            if (r5 == 0) goto L_0x00dc
            java.lang.String r5 = r5.getHeight()
            if (r5 == 0) goto L_0x00dc
            goto L_0x00e2
        L_0x00dc:
            com.fossil.ob5 r5 = com.fossil.ob5.IMPERIAL
            java.lang.String r5 = r5.getValue()
        L_0x00e2:
            r0.a(r4, r5)
            java.lang.String r4 = "weight"
            com.portfolio.platform.data.model.MFUser$UnitGroup r5 = r17.getUnitGroup()
            if (r5 == 0) goto L_0x00f4
            java.lang.String r5 = r5.getWeight()
            if (r5 == 0) goto L_0x00f4
            goto L_0x00fa
        L_0x00f4:
            com.fossil.ob5 r5 = com.fossil.ob5.IMPERIAL
            java.lang.String r5 = r5.getValue()
        L_0x00fa:
            r0.a(r4, r5)
            java.lang.String r4 = "distance"
            com.portfolio.platform.data.model.MFUser$UnitGroup r5 = r17.getUnitGroup()
            if (r5 == 0) goto L_0x010c
            java.lang.String r5 = r5.getDistance()
            if (r5 == 0) goto L_0x010c
            goto L_0x0112
        L_0x010c:
            com.fossil.ob5 r5 = com.fossil.ob5.IMPERIAL
            java.lang.String r5 = r5.getValue()
        L_0x0112:
            r0.a(r4, r5)
            java.lang.String r4 = "temperature"
            com.portfolio.platform.data.model.MFUser$UnitGroup r5 = r17.getUnitGroup()
            if (r5 == 0) goto L_0x0124
            java.lang.String r5 = r5.getTemperature()
            if (r5 == 0) goto L_0x0124
            goto L_0x012a
        L_0x0124:
            com.fossil.ob5 r5 = com.fossil.ob5.IMPERIAL
            java.lang.String r5 = r5.getValue()
        L_0x012a:
            r0.a(r4, r5)
            java.lang.String r4 = "unitGroup"
            r8.a(r4, r0)
            int r0 = r17.getHeightInCentimeters()
            java.lang.Integer r0 = com.fossil.pb7.a(r0)
            r8.a(r3, r0)
            int r0 = r17.getWeightInGrams()
            java.lang.Integer r0 = com.fossil.pb7.a(r0)
            r8.a(r2, r0)
            java.lang.String r0 = "emailOptIn"
            boolean r2 = r17.getEmailOptIn()
            java.lang.Boolean r2 = com.fossil.pb7.a(r2)
            r8.a(r0, r2)
            java.lang.String r0 = "diagnosticEnabled"
            boolean r2 = r17.getDiagnosticEnabled()
            java.lang.Boolean r2 = com.fossil.pb7.a(r2)
            r8.a(r0, r2)
            java.lang.String r0 = "useDefaultGoals"
            boolean r2 = r17.getUseDefaultGoals()
            java.lang.Boolean r2 = com.fossil.pb7.a(r2)
            r8.a(r0, r2)
            java.lang.String r0 = "useDefaultBiometric"
            boolean r2 = r17.getUseDefaultBiometric()
            java.lang.Boolean r2 = com.fossil.pb7.a(r2)
            r8.a(r0, r2)
            java.lang.String r0 = r17.getBirthday()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x018f
            java.lang.String r0 = "birthday"
            java.lang.String r2 = r17.getBirthday()
            r8.a(r0, r2)
        L_0x018f:
            com.fossil.de4 r0 = new com.fossil.de4
            r0.<init>()
            java.util.List r2 = r17.getIntegrationsList()
            java.util.Iterator r2 = r2.iterator()
        L_0x019c:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x01c8
            java.lang.Object r3 = r2.next()
            java.lang.String r3 = (java.lang.String) r3
            if (r3 == 0) goto L_0x01bd
            if (r3 == 0) goto L_0x01b5
            java.lang.CharSequence r4 = com.fossil.nh7.d(r3)
            java.lang.String r4 = r4.toString()
            goto L_0x01be
        L_0x01b5:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r2 = "null cannot be cast to non-null type kotlin.CharSequence"
            r0.<init>(r2)
            throw r0
        L_0x01bd:
            r4 = r12
        L_0x01be:
            boolean r4 = android.text.TextUtils.isEmpty(r4)
            if (r4 != 0) goto L_0x019c
            r0.a(r3)
            goto L_0x019c
        L_0x01c8:
            java.lang.String r2 = "integrations"
            r8.a(r2, r0)
            com.fossil.ie4 r0 = new com.fossil.ie4
            r0.<init>()
            java.lang.String r2 = "home"
            com.portfolio.platform.data.model.MFUser$Address r3 = r17.getAddresses()
            if (r3 == 0) goto L_0x01e1
            java.lang.String r3 = r3.getHome()
            if (r3 == 0) goto L_0x01e1
            goto L_0x01e2
        L_0x01e1:
            r3 = r14
        L_0x01e2:
            r0.a(r2, r3)
            java.lang.String r2 = "work"
            com.portfolio.platform.data.model.MFUser$Address r3 = r17.getAddresses()
            if (r3 == 0) goto L_0x01f4
            java.lang.String r3 = r3.getWork()
            if (r3 == 0) goto L_0x01f4
            r14 = r3
        L_0x01f4:
            r0.a(r2, r14)
            java.lang.String r2 = "addresses"
            r8.a(r2, r0)
            java.lang.String r0 = r17.getProfilePicture()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0277
            java.lang.String r0 = r17.getProfilePicture()
            if (r0 == 0) goto L_0x0218
            java.lang.String r2 = "https://"
            r3 = 2
            boolean r0 = com.fossil.nh7.a(r0, r2, r11, r3, r12)
            java.lang.Boolean r0 = com.fossil.pb7.a(r0)
            goto L_0x0219
        L_0x0218:
            r0 = r12
        L_0x0219:
            if (r0 == 0) goto L_0x024a
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0277
            java.lang.String r0 = r17.getProfilePicture()
            if (r0 == 0) goto L_0x0233
            java.lang.String r2 = "http://"
            r3 = 2
            boolean r0 = com.fossil.nh7.a(r0, r2, r11, r3, r12)
            java.lang.Boolean r0 = com.fossil.pb7.a(r0)
            goto L_0x0234
        L_0x0233:
            r0 = r12
        L_0x0234:
            if (r0 == 0) goto L_0x0246
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0277
            java.lang.String r0 = "profilePicture"
            java.lang.String r2 = r17.getProfilePicture()
            r8.a(r0, r2)
            goto L_0x0277
        L_0x0246:
            com.fossil.ee7.a()
            throw r12
        L_0x024a:
            com.fossil.ee7.a()
            throw r12
        L_0x024e:
            com.fossil.x87 r0 = new com.fossil.x87
            r0.<init>(r9)
            throw r0
        L_0x0254:
            com.fossil.x87 r0 = new com.fossil.x87
            r0.<init>(r9)
            throw r0
        L_0x025a:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Exception when generating user json object "
            r4.append(r5)
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r2.d(r3, r0)
        L_0x0277:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$updateUser$response$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$updateUser$response$Anon1
            r0.<init>(r1, r8, r12)
            r6.L$0 = r1
            r2 = r17
            r6.L$1 = r2
            r6.L$2 = r8
            r2 = 1
            r6.label = r2
            java.lang.Object r0 = com.fossil.aj5.a(r0, r6)
            if (r0 != r7) goto L_0x028e
            return r7
        L_0x028e:
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            boolean r2 = r0 instanceof com.fossil.bj5
            if (r2 == 0) goto L_0x02b8
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r4 = "updateUser Success"
            r2.d(r3, r4)
            com.fossil.bj5 r0 = (com.fossil.bj5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.UserWrapper r0 = (com.portfolio.platform.data.UserWrapper) r0
            if (r0 == 0) goto L_0x02b0
            com.portfolio.platform.data.model.MFUser r0 = r0.toMFUser()
            goto L_0x02b1
        L_0x02b0:
            r0 = r12
        L_0x02b1:
            com.fossil.bj5 r2 = new com.fossil.bj5
            r3 = 2
            r2.<init>(r0, r11, r3, r12)
            goto L_0x0307
        L_0x02b8:
            boolean r2 = r0 instanceof com.fossil.yi5
            if (r2 == 0) goto L_0x0308
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "updateUser Failure code="
            r4.append(r5)
            com.fossil.yi5 r0 = (com.fossil.yi5) r0
            int r5 = r0.a()
            r4.append(r5)
            java.lang.String r5 = " message="
            r4.append(r5)
            com.portfolio.platform.data.model.ServerError r5 = r0.c()
            if (r5 == 0) goto L_0x02e6
            java.lang.String r12 = r5.getMessage()
        L_0x02e6:
            r4.append(r12)
            java.lang.String r4 = r4.toString()
            r2.d(r3, r4)
            com.fossil.yi5 r2 = new com.fossil.yi5
            int r6 = r0.a()
            com.portfolio.platform.data.model.ServerError r7 = r0.c()
            java.lang.Throwable r8 = r0.d()
            r9 = 0
            r10 = 0
            r11 = 24
            r12 = 0
            r5 = r2
            r5.<init>(r6, r7, r8, r9, r10, r11, r12)
        L_0x0307:
            return r2
        L_0x0308:
            com.fossil.p87 r0 = new com.fossil.p87
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.updateUser(com.portfolio.platform.data.model.MFUser, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object verifyEmailOtp(java.lang.String r10, java.lang.String r11, com.fossil.fb7<? super com.fossil.zi5<java.lang.Void>> r12) {
        /*
            r9 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$verifyEmailOtp$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$verifyEmailOtp$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$verifyEmailOtp$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$verifyEmailOtp$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$verifyEmailOtp$Anon1
            r0.<init>(r9, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0042
            if (r2 != r3) goto L_0x003a
            java.lang.Object r10 = r0.L$3
            com.fossil.ie4 r10 = (com.fossil.ie4) r10
            java.lang.Object r10 = r0.L$2
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r10 = r0.L$1
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r10 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r10 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r10
            com.fossil.t87.a(r12)
            goto L_0x0086
        L_0x003a:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x0042:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "verifyEmailOtp email: "
            r5.append(r6)
            r5.append(r10)
            java.lang.String r5 = r5.toString()
            r12.d(r2, r5)
            com.fossil.ie4 r12 = new com.fossil.ie4
            r12.<init>()
            java.lang.String r2 = "email"
            r12.a(r2, r10)
            java.lang.String r2 = "otp"
            r12.a(r2, r11)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$verifyEmailOtp$repoResponse$Anon1 r2 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$verifyEmailOtp$repoResponse$Anon1
            r2.<init>(r9, r12, r4)
            r0.L$0 = r9
            r0.L$1 = r10
            r0.L$2 = r11
            r0.L$3 = r12
            r0.label = r3
            java.lang.Object r12 = com.fossil.aj5.a(r2, r0)
            if (r12 != r1) goto L_0x0086
            return r1
        L_0x0086:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r10 = r12 instanceof com.fossil.bj5
            if (r10 == 0) goto L_0x00a1
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r12 = "verifyEmailOtp Success"
            r10.d(r11, r12)
            com.fossil.bj5 r10 = new com.fossil.bj5
            r11 = 0
            r12 = 2
            r10.<init>(r4, r11, r12, r4)
            goto L_0x00f0
        L_0x00a1:
            boolean r10 = r12 instanceof com.fossil.yi5
            if (r10 == 0) goto L_0x00f1
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "verifyEmailOtp failed with error="
            r0.append(r1)
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            int r1 = r12.a()
            r0.append(r1)
            java.lang.String r1 = " message="
            r0.append(r1)
            com.portfolio.platform.data.model.ServerError r1 = r12.c()
            if (r1 == 0) goto L_0x00cf
            java.lang.String r4 = r1.getMessage()
        L_0x00cf:
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            r10.d(r11, r0)
            com.fossil.yi5 r10 = new com.fossil.yi5
            int r2 = r12.a()
            com.portfolio.platform.data.model.ServerError r3 = r12.c()
            java.lang.Throwable r4 = r12.d()
            r5 = 0
            r6 = 0
            r7 = 24
            r8 = 0
            r1 = r10
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
        L_0x00f0:
            return r10
        L_0x00f1:
            com.fossil.p87 r10 = new com.fossil.p87
            r10.<init>()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.verifyEmailOtp(java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }
}
