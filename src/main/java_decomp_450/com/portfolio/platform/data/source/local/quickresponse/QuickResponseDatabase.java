package com.portfolio.platform.data.source.local.quickresponse;

import com.fossil.ci;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class QuickResponseDatabase extends ci {
    @DexIgnore
    public abstract QuickResponseMessageDao quickResponseMessageDao();

    @DexIgnore
    public abstract QuickResponseSenderDao quickResponseSenderDao();
}
