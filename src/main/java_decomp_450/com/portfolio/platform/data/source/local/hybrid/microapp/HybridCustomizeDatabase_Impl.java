package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.os.Build;
import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridCustomizeDatabase_Impl extends HybridCustomizeDatabase {
    @DexIgnore
    public volatile HybridPresetDao _hybridPresetDao;
    @DexIgnore
    public volatile MicroAppDao _microAppDao;
    @DexIgnore
    public volatile MicroAppLastSettingDao _microAppLastSettingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `microApp` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `nameKey` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `categories` TEXT NOT NULL, `description` TEXT NOT NULL, `descriptionKey` TEXT NOT NULL, `icon` TEXT, PRIMARY KEY(`id`, `serialNumber`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `microAppSetting` (`id` TEXT NOT NULL, `appId` TEXT NOT NULL, `setting` TEXT, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`appId`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `microAppVariant` (`id` TEXT NOT NULL, `appId` TEXT NOT NULL, `name` TEXT NOT NULL, `description` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `majorNumber` INTEGER NOT NULL, `minorNumber` INTEGER NOT NULL, `serialNumber` TEXT NOT NULL, PRIMARY KEY(`appId`, `serialNumber`, `name`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `microAppLastSetting` (`appId` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`appId`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `declarationFile` (`appId` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `variantName` TEXT NOT NULL, `id` TEXT NOT NULL, `description` TEXT, `content` TEXT, PRIMARY KEY(`appId`, `serialNumber`, `variantName`, `id`), FOREIGN KEY(`appId`, `serialNumber`, `variantName`) REFERENCES `microAppVariant`(`appId`, `serialNumber`, `name`) ON UPDATE CASCADE ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `hybridPreset` (`pinType` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT, `id` TEXT NOT NULL, `name` TEXT, `serialNumber` TEXT NOT NULL, `buttons` TEXT NOT NULL, `isActive` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `hybridRecommendPreset` (`id` TEXT NOT NULL, `name` TEXT, `serialNumber` TEXT NOT NULL, `buttons` TEXT NOT NULL, `isDefault` INTEGER NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '1cd16a2fbeeede74e77b1faa1351cfe8')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `microApp`");
            wiVar.execSQL("DROP TABLE IF EXISTS `microAppSetting`");
            wiVar.execSQL("DROP TABLE IF EXISTS `microAppVariant`");
            wiVar.execSQL("DROP TABLE IF EXISTS `microAppLastSetting`");
            wiVar.execSQL("DROP TABLE IF EXISTS `declarationFile`");
            wiVar.execSQL("DROP TABLE IF EXISTS `hybridPreset`");
            wiVar.execSQL("DROP TABLE IF EXISTS `hybridRecommendPreset`");
            if (((ci) HybridCustomizeDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) HybridCustomizeDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) HybridCustomizeDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) HybridCustomizeDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) HybridCustomizeDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) HybridCustomizeDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) HybridCustomizeDatabase_Impl.this).mDatabase = wiVar;
            wiVar.execSQL("PRAGMA foreign_keys = ON");
            HybridCustomizeDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) HybridCustomizeDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) HybridCustomizeDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) HybridCustomizeDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(8);
            hashMap.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap.put("name", new ti.a("name", "TEXT", true, 0, null, 1));
            hashMap.put("nameKey", new ti.a("nameKey", "TEXT", true, 0, null, 1));
            hashMap.put("serialNumber", new ti.a("serialNumber", "TEXT", true, 2, null, 1));
            hashMap.put("categories", new ti.a("categories", "TEXT", true, 0, null, 1));
            hashMap.put("description", new ti.a("description", "TEXT", true, 0, null, 1));
            hashMap.put("descriptionKey", new ti.a("descriptionKey", "TEXT", true, 0, null, 1));
            hashMap.put("icon", new ti.a("icon", "TEXT", false, 0, null, 1));
            ti tiVar = new ti("microApp", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "microApp");
            if (!tiVar.equals(a)) {
                return new ei.b(false, "microApp(com.portfolio.platform.data.model.room.microapp.MicroApp).\n Expected:\n" + tiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(6);
            hashMap2.put("id", new ti.a("id", "TEXT", true, 0, null, 1));
            hashMap2.put("appId", new ti.a("appId", "TEXT", true, 1, null, 1));
            hashMap2.put(MicroAppSetting.SETTING, new ti.a(MicroAppSetting.SETTING, "TEXT", false, 0, null, 1));
            hashMap2.put("createdAt", new ti.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap2.put("updatedAt", new ti.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap2.put("pinType", new ti.a("pinType", "INTEGER", true, 0, null, 1));
            ti tiVar2 = new ti(MicroAppSetting.TABLE_NAME, hashMap2, new HashSet(0), new HashSet(0));
            ti a2 = ti.a(wiVar, MicroAppSetting.TABLE_NAME);
            if (!tiVar2.equals(a2)) {
                return new ei.b(false, "microAppSetting(com.portfolio.platform.data.model.room.microapp.MicroAppSetting).\n Expected:\n" + tiVar2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(9);
            hashMap3.put("id", new ti.a("id", "TEXT", true, 0, null, 1));
            hashMap3.put("appId", new ti.a("appId", "TEXT", true, 1, null, 1));
            hashMap3.put("name", new ti.a("name", "TEXT", true, 3, null, 1));
            hashMap3.put("description", new ti.a("description", "TEXT", true, 0, null, 1));
            hashMap3.put("createdAt", new ti.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap3.put("updatedAt", new ti.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap3.put(MicroAppVariant.COLUMN_MAJOR_NUMBER, new ti.a(MicroAppVariant.COLUMN_MAJOR_NUMBER, "INTEGER", true, 0, null, 1));
            hashMap3.put(MicroAppVariant.COLUMN_MINOR_NUMBER, new ti.a(MicroAppVariant.COLUMN_MINOR_NUMBER, "INTEGER", true, 0, null, 1));
            hashMap3.put("serialNumber", new ti.a("serialNumber", "TEXT", true, 2, null, 1));
            ti tiVar3 = new ti("microAppVariant", hashMap3, new HashSet(0), new HashSet(0));
            ti a3 = ti.a(wiVar, "microAppVariant");
            if (!tiVar3.equals(a3)) {
                return new ei.b(false, "microAppVariant(com.portfolio.platform.data.model.room.microapp.MicroAppVariant).\n Expected:\n" + tiVar3 + "\n Found:\n" + a3);
            }
            HashMap hashMap4 = new HashMap(3);
            hashMap4.put("appId", new ti.a("appId", "TEXT", true, 1, null, 1));
            hashMap4.put("updatedAt", new ti.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap4.put(MicroAppSetting.SETTING, new ti.a(MicroAppSetting.SETTING, "TEXT", true, 0, null, 1));
            ti tiVar4 = new ti("microAppLastSetting", hashMap4, new HashSet(0), new HashSet(0));
            ti a4 = ti.a(wiVar, "microAppLastSetting");
            if (!tiVar4.equals(a4)) {
                return new ei.b(false, "microAppLastSetting(com.portfolio.platform.data.model.microapp.MicroAppLastSetting).\n Expected:\n" + tiVar4 + "\n Found:\n" + a4);
            }
            HashMap hashMap5 = new HashMap(6);
            hashMap5.put("appId", new ti.a("appId", "TEXT", true, 1, null, 1));
            hashMap5.put("serialNumber", new ti.a("serialNumber", "TEXT", true, 2, null, 1));
            hashMap5.put("variantName", new ti.a("variantName", "TEXT", true, 3, null, 1));
            hashMap5.put("id", new ti.a("id", "TEXT", true, 4, null, 1));
            hashMap5.put("description", new ti.a("description", "TEXT", false, 0, null, 1));
            hashMap5.put("content", new ti.a("content", "TEXT", false, 0, null, 1));
            HashSet hashSet = new HashSet(1);
            hashSet.add(new ti.b("microAppVariant", "NO ACTION", "CASCADE", Arrays.asList("appId", "serialNumber", "variantName"), Arrays.asList("appId", "serialNumber", "name")));
            ti tiVar5 = new ti("declarationFile", hashMap5, hashSet, new HashSet(0));
            ti a5 = ti.a(wiVar, "declarationFile");
            if (!tiVar5.equals(a5)) {
                return new ei.b(false, "declarationFile(com.portfolio.platform.data.model.room.microapp.DeclarationFile).\n Expected:\n" + tiVar5 + "\n Found:\n" + a5);
            }
            HashMap hashMap6 = new HashMap(8);
            hashMap6.put("pinType", new ti.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap6.put("createdAt", new ti.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap6.put("updatedAt", new ti.a("updatedAt", "TEXT", false, 0, null, 1));
            hashMap6.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap6.put("name", new ti.a("name", "TEXT", false, 0, null, 1));
            hashMap6.put("serialNumber", new ti.a("serialNumber", "TEXT", true, 0, null, 1));
            hashMap6.put("buttons", new ti.a("buttons", "TEXT", true, 0, null, 1));
            hashMap6.put("isActive", new ti.a("isActive", "INTEGER", true, 0, null, 1));
            ti tiVar6 = new ti("hybridPreset", hashMap6, new HashSet(0), new HashSet(0));
            ti a6 = ti.a(wiVar, "hybridPreset");
            if (!tiVar6.equals(a6)) {
                return new ei.b(false, "hybridPreset(com.portfolio.platform.data.model.room.microapp.HybridPreset).\n Expected:\n" + tiVar6 + "\n Found:\n" + a6);
            }
            HashMap hashMap7 = new HashMap(7);
            hashMap7.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap7.put("name", new ti.a("name", "TEXT", false, 0, null, 1));
            hashMap7.put("serialNumber", new ti.a("serialNumber", "TEXT", true, 0, null, 1));
            hashMap7.put("buttons", new ti.a("buttons", "TEXT", true, 0, null, 1));
            hashMap7.put("isDefault", new ti.a("isDefault", "INTEGER", true, 0, null, 1));
            hashMap7.put("createdAt", new ti.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap7.put("updatedAt", new ti.a("updatedAt", "TEXT", true, 0, null, 1));
            ti tiVar7 = new ti("hybridRecommendPreset", hashMap7, new HashSet(0), new HashSet(0));
            ti a7 = ti.a(wiVar, "hybridRecommendPreset");
            if (tiVar7.equals(a7)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "hybridRecommendPreset(com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset).\n Expected:\n" + tiVar7 + "\n Found:\n" + a7);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        boolean z = Build.VERSION.SDK_INT >= 21;
        if (!z) {
            try {
                writableDatabase.execSQL("PRAGMA foreign_keys = FALSE");
            } catch (Throwable th) {
                super.endTransaction();
                if (!z) {
                    writableDatabase.execSQL("PRAGMA foreign_keys = TRUE");
                }
                writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
                if (!writableDatabase.inTransaction()) {
                    writableDatabase.execSQL("VACUUM");
                }
                throw th;
            }
        }
        super.beginTransaction();
        if (z) {
            writableDatabase.execSQL("PRAGMA defer_foreign_keys = TRUE");
        }
        writableDatabase.execSQL("DELETE FROM `microApp`");
        writableDatabase.execSQL("DELETE FROM `microAppSetting`");
        writableDatabase.execSQL("DELETE FROM `microAppVariant`");
        writableDatabase.execSQL("DELETE FROM `microAppLastSetting`");
        writableDatabase.execSQL("DELETE FROM `declarationFile`");
        writableDatabase.execSQL("DELETE FROM `hybridPreset`");
        writableDatabase.execSQL("DELETE FROM `hybridRecommendPreset`");
        super.setTransactionSuccessful();
        super.endTransaction();
        if (!z) {
            writableDatabase.execSQL("PRAGMA foreign_keys = TRUE");
        }
        writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
        if (!writableDatabase.inTransaction()) {
            writableDatabase.execSQL("VACUUM");
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "microApp", MicroAppSetting.TABLE_NAME, "microAppVariant", "microAppLastSetting", "declarationFile", "hybridPreset", "hybridRecommendPreset");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(10), "1cd16a2fbeeede74e77b1faa1351cfe8", "c7fc8c005bafc0b2f864a0c6cb79cc7b");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase
    public MicroAppDao microAppDao() {
        MicroAppDao microAppDao;
        if (this._microAppDao != null) {
            return this._microAppDao;
        }
        synchronized (this) {
            if (this._microAppDao == null) {
                this._microAppDao = new MicroAppDao_Impl(this);
            }
            microAppDao = this._microAppDao;
        }
        return microAppDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase
    public MicroAppLastSettingDao microAppLastSettingDao() {
        MicroAppLastSettingDao microAppLastSettingDao;
        if (this._microAppLastSettingDao != null) {
            return this._microAppLastSettingDao;
        }
        synchronized (this) {
            if (this._microAppLastSettingDao == null) {
                this._microAppLastSettingDao = new MicroAppLastSettingDao_Impl(this);
            }
            microAppLastSettingDao = this._microAppLastSettingDao;
        }
        return microAppLastSettingDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase
    public HybridPresetDao presetDao() {
        HybridPresetDao hybridPresetDao;
        if (this._hybridPresetDao != null) {
            return this._hybridPresetDao;
        }
        synchronized (this) {
            if (this._hybridPresetDao == null) {
                this._hybridPresetDao = new HybridPresetDao_Impl(this);
            }
            hybridPresetDao = this._hybridPresetDao;
        }
        return hybridPresetDao;
    }
}
