package com.portfolio.platform.data.source.remote;

import com.fossil.aj5;
import com.fossil.bj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi5;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import java.net.SocketTimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource$getServerSettingList$1", f = "ServerSettingRemoteDataSource.kt", l = {32}, m = "invokeSuspend")
public final class ServerSettingRemoteDataSource$getServerSettingList$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingDataSource.OnGetServerSettingList $callback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ServerSettingRemoteDataSource$getServerSettingList$Anon1(ServerSettingRemoteDataSource serverSettingRemoteDataSource, ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList, fb7 fb7) {
        super(2, fb7);
        this.this$0 = serverSettingRemoteDataSource;
        this.$callback = onGetServerSettingList;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        ServerSettingRemoteDataSource$getServerSettingList$Anon1 serverSettingRemoteDataSource$getServerSettingList$Anon1 = new ServerSettingRemoteDataSource$getServerSettingList$Anon1(this.this$0, this.$callback, fb7);
        serverSettingRemoteDataSource$getServerSettingList$Anon1.p$ = (yi7) obj;
        return serverSettingRemoteDataSource$getServerSettingList$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((ServerSettingRemoteDataSource$getServerSettingList$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            ServerSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2 serverSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2 = new ServerSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2(this, null);
            this.L$0 = yi7;
            this.label = 1;
            obj = aj5.a(serverSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        zi5 zi5 = (zi5) obj;
        if (zi5 instanceof bj5) {
            FLogger.INSTANCE.getLocal().e(ServerSettingRemoteDataSource.Companion.getTAG$app_fossilRelease(), "getServerSettingList- is successful");
            this.$callback.onSuccess((ServerSettingList) ((bj5) zi5).a());
        } else if (zi5 instanceof yi5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ServerSettingRemoteDataSource.Companion.getTAG$app_fossilRelease();
            StringBuilder sb = new StringBuilder();
            sb.append("getServerSettingList - Failure, code=");
            yi5 yi5 = (yi5) zi5;
            sb.append(yi5.a());
            local.e(tAG$app_fossilRelease, sb.toString());
            if (yi5.d() instanceof SocketTimeoutException) {
                this.$callback.onFailed(MFNetworkReturnCode.CLIENT_TIMEOUT);
            } else {
                this.$callback.onFailed(601);
            }
        }
        return i97.a;
    }
}
