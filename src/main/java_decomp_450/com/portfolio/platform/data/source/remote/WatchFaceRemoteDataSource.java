package com.portfolio.platform.data.source.remote;

import com.fossil.aj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.mo7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceRemoteDataSource {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 api;

    @DexIgnore
    public WatchFaceRemoteDataSource(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "api");
        this.api = apiServiceV2;
        String simpleName = WatchFaceRemoteDataSource.class.getSimpleName();
        ee7.a((Object) simpleName, "WatchFaceRemoteDataSource::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    public final Object downloadWatchFaceFile(String str, String str2, fb7<? super zi5<mo7>> fb7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = this.TAG;
        local.d(str3, "download: " + str + " path: " + str2);
        return aj5.a(new WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2(this, str, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWatchFacesFromServer(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.WatchFace>>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1 r0 = (com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1 r0 = new com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003a
            if (r2 != r3) goto L_0x0032
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource r9 = (com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource) r9
            com.fossil.t87.a(r10)
            goto L_0x0050
        L_0x0032:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003a:
            com.fossil.t87.a(r10)
            com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1 r10 = new com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1
            r10.<init>(r8, r9, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x004f
            return r1
        L_0x004f:
            r9 = r8
        L_0x0050:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r0 = r10 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x00ac
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r9 = r9.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "data: "
            r1.append(r2)
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r2 = r10.a()
            if (r2 == 0) goto L_0x00a8
            com.portfolio.platform.data.source.remote.ApiResponse r2 = (com.portfolio.platform.data.source.remote.ApiResponse) r2
            java.util.List r2 = r2.get_items()
            r1.append(r2)
            java.lang.String r2 = " - isFromCache: "
            r1.append(r2)
            boolean r2 = r10.b()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.d(r9, r1)
            java.lang.Object r9 = r10.a()
            com.portfolio.platform.data.source.remote.ApiResponse r9 = (com.portfolio.platform.data.source.remote.ApiResponse) r9
            java.util.List r9 = r9.get_items()
            boolean r0 = r9 instanceof java.util.ArrayList
            if (r0 != 0) goto L_0x009b
            goto L_0x009c
        L_0x009b:
            r4 = r9
        L_0x009c:
            java.util.ArrayList r4 = (java.util.ArrayList) r4
            com.fossil.bj5 r9 = new com.fossil.bj5
            boolean r10 = r10.b()
            r9.<init>(r4, r10)
            goto L_0x00c6
        L_0x00a8:
            com.fossil.ee7.a()
            throw r4
        L_0x00ac:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00c7
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 28
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x00c6:
            return r9
        L_0x00c7:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource.getWatchFacesFromServer(java.lang.String, com.fossil.fb7):java.lang.Object");
    }
}
