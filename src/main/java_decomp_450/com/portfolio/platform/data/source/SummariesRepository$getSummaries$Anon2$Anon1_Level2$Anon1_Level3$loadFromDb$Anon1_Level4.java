package com.portfolio.platform.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fb7;
import com.fossil.rb7;
import com.fossil.tb7;
import com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SummariesRepository$getSummaries$2$1$1", f = "SummariesRepository.kt", l = {266, 270}, m = "loadFromDb")
public final class SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 extends rb7 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon2.Anon1_Level2.Anon1_Level3 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4(SummariesRepository$getSummaries$Anon2.Anon1_Level2.Anon1_Level3 anon1_Level3, fb7 fb7) {
        super(fb7);
        this.this$0 = anon1_Level3;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.loadFromDb(this);
    }
}
