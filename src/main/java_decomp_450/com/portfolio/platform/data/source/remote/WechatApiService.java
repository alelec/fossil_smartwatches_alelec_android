package com.portfolio.platform.data.source.remote;

import com.fossil.bw7;
import com.fossil.nw7;
import com.portfolio.platform.data.WechatToken;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface WechatApiService {
    @DexIgnore
    @bw7("oauth2/access_token")
    Call<WechatToken> getWechatToken(@nw7("appid") String str, @nw7("secret") String str2, @nw7("code") String str3, @nw7("grant_type") String str4);
}
