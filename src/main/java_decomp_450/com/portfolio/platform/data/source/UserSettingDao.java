package com.portfolio.platform.data.source;

import com.portfolio.platform.data.model.UserSettings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface UserSettingDao {
    @DexIgnore
    void addOrUpdateUserSetting(UserSettings userSettings);

    @DexIgnore
    void cleanUp();

    @DexIgnore
    UserSettings getCurrentUserSetting();

    @DexIgnore
    UserSettings getPendingUserSetting();
}
