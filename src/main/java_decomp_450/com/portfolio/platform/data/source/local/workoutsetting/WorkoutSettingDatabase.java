package com.portfolio.platform.data.source.local.workoutsetting;

import com.fossil.ci;
import com.fossil.li;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class WorkoutSettingDatabase extends ci {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ li MIGRATION_FROM_1_TO_2; // = new WorkoutSettingDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1(1, 2);
    @DexIgnore
    public static /* final */ String TAG; // = "WorkoutSettingDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final li getMIGRATION_FROM_1_TO_2() {
            return WorkoutSettingDatabase.MIGRATION_FROM_1_TO_2;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public abstract WorkoutSettingDao workoutSettingDao();
}
