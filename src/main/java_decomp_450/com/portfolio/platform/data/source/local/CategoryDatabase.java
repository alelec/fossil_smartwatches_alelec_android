package com.portfolio.platform.data.source.local;

import com.fossil.ci;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class CategoryDatabase extends ci {
    @DexIgnore
    public abstract CategoryDao categoryDao();
}
