package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fe7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.nf;
import com.fossil.pj4;
import com.fossil.qf;
import com.fossil.qj7;
import com.fossil.t3;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.te5;
import com.fossil.ti7;
import com.fossil.vc7;
import com.fossil.vh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataPaging$2", f = "GoalTrackingRepository.kt", l = {319}, m = "invokeSuspend")
public final class GoalTrackingRepository$getGoalTrackingDataPaging$Anon2 extends zb7 implements kd7<yi7, fb7<? super Listing<GoalTrackingData>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ pj4 $appExecutors;
    @DexIgnore
    public /* final */ /* synthetic */ Date $currentDate;
    @DexIgnore
    public /* final */ /* synthetic */ te5.a $listener;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public static /* final */ Anon1_Level2 INSTANCE; // = new Anon1_Level2();

        @DexIgnore
        public final LiveData<NetworkState> apply(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
            return goalTrackingDataLocalDataSource.getMNetworkState();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2_Level2 extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(GoalTrackingDataSourceFactory goalTrackingDataSourceFactory) {
            super(0);
            this.$sourceFactory = goalTrackingDataSourceFactory;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            GoalTrackingDataLocalDataSource a = this.$sourceFactory.getSourceLiveData().a();
            if (a != null) {
                a.invalidate();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3_Level2 extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(GoalTrackingDataSourceFactory goalTrackingDataSourceFactory) {
            super(0);
            this.$sourceFactory = goalTrackingDataSourceFactory;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            te5 mHelper;
            GoalTrackingDataLocalDataSource a = this.$sourceFactory.getSourceLiveData().a();
            if (a != null && (mHelper = a.getMHelper()) != null) {
                mHelper.b();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getGoalTrackingDataPaging$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, pj4 pj4, te5.a aVar, fb7 fb7) {
        super(2, fb7);
        this.this$0 = goalTrackingRepository;
        this.$currentDate = date;
        this.$appExecutors = pj4;
        this.$listener = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        GoalTrackingRepository$getGoalTrackingDataPaging$Anon2 goalTrackingRepository$getGoalTrackingDataPaging$Anon2 = new GoalTrackingRepository$getGoalTrackingDataPaging$Anon2(this.this$0, this.$currentDate, this.$appExecutors, this.$listener, fb7);
        goalTrackingRepository$getGoalTrackingDataPaging$Anon2.p$ = (yi7) obj;
        return goalTrackingRepository$getGoalTrackingDataPaging$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super Listing<GoalTrackingData>> fb7) {
        return ((GoalTrackingRepository$getGoalTrackingDataPaging$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "getGoalTrackingDataPaging");
            ti7 b = qj7.b();
            GoalTrackingRepository$getGoalTrackingDataPaging$Anon2$goalTrackingDatabase$Anon1_Level2 goalTrackingRepository$getGoalTrackingDataPaging$Anon2$goalTrackingDatabase$Anon1_Level2 = new GoalTrackingRepository$getGoalTrackingDataPaging$Anon2$goalTrackingDatabase$Anon1_Level2(null);
            this.L$0 = yi7;
            this.label = 1;
            obj = vh7.a(b, goalTrackingRepository$getGoalTrackingDataPaging$Anon2$goalTrackingDatabase$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        GoalTrackingDataSourceFactory goalTrackingDataSourceFactory = new GoalTrackingDataSourceFactory(this.this$0, (GoalTrackingDatabase) obj, this.$currentDate, this.$appExecutors, this.$listener);
        this.this$0.mSourceDataFactoryList.add(goalTrackingDataSourceFactory);
        qf.f.a aVar = new qf.f.a();
        aVar.a(100);
        aVar.a(false);
        aVar.b(100);
        aVar.c(5);
        qf.f a2 = aVar.a();
        ee7.a((Object) a2, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a3 = new nf(goalTrackingDataSourceFactory, a2).a();
        ee7.a((Object) a3, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData b2 = ge.b(goalTrackingDataSourceFactory.getSourceLiveData(), Anon1_Level2.INSTANCE);
        ee7.a((Object) b2, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing(a3, b2, new Anon2_Level2(goalTrackingDataSourceFactory), new Anon3_Level2(goalTrackingDataSourceFactory));
    }
}
