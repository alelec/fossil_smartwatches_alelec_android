package com.portfolio.platform.data.source.local.thirdparty;

import com.fossil.ci;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ThirdPartyDatabase extends ci {
    @DexIgnore
    public abstract GFitActiveTimeDao getGFitActiveTimeDao();

    @DexIgnore
    public abstract GFitHeartRateDao getGFitHeartRateDao();

    @DexIgnore
    public abstract GFitSampleDao getGFitSampleDao();

    @DexIgnore
    public abstract GFitSleepDao getGFitSleepDao();

    @DexIgnore
    public abstract GFitWorkoutSessionDao getGFitWorkoutSessionDao();

    @DexIgnore
    public abstract UASampleDao getUASampleDao();
}
