package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyleRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "RingStyleRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public RingStyleRemoteDataSource(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mService");
        this.mService = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getRingStyleList(java.lang.String r10, boolean r11, com.fossil.fb7<? super com.fossil.zi5<java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle>>> r12) {
        /*
            r9 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource$getRingStyleList$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource$getRingStyleList$Anon1 r0 = (com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource$getRingStyleList$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource$getRingStyleList$Anon1 r0 = new com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource$getRingStyleList$Anon1
            r0.<init>(r9, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003c
            if (r2 != r3) goto L_0x0034
            boolean r10 = r0.Z$0
            java.lang.Object r10 = r0.L$1
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r10 = r0.L$0
            com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource r10 = (com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource) r10
            com.fossil.t87.a(r12)
            goto L_0x0053
        L_0x0034:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x003c:
            com.fossil.t87.a(r12)
            com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource$getRingStyleList$response$Anon1 r12 = new com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource$getRingStyleList$response$Anon1
            r12.<init>(r9, r11, r10, r4)
            r0.L$0 = r9
            r0.L$1 = r10
            r0.Z$0 = r11
            r0.label = r3
            java.lang.Object r12 = com.fossil.aj5.a(r12, r0)
            if (r12 != r1) goto L_0x0053
            return r1
        L_0x0053:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r10 = r12 instanceof com.fossil.bj5
            java.lang.String r11 = "RingStyleRemoteDataSource"
            if (r10 == 0) goto L_0x00af
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "getRingStyleList() data: "
            r0.append(r1)
            com.fossil.bj5 r12 = (com.fossil.bj5) r12
            java.lang.Object r1 = r12.a()
            if (r1 == 0) goto L_0x00ab
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            java.util.List r1 = r1.get_items()
            r0.append(r1)
            java.lang.String r1 = " - isFromCache: "
            r0.append(r1)
            boolean r1 = r12.b()
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r10.d(r11, r0)
            java.lang.Object r10 = r12.a()
            com.portfolio.platform.data.source.remote.ApiResponse r10 = (com.portfolio.platform.data.source.remote.ApiResponse) r10
            java.util.List r10 = r10.get_items()
            boolean r11 = r10 instanceof java.util.ArrayList
            if (r11 != 0) goto L_0x009e
            goto L_0x009f
        L_0x009e:
            r4 = r10
        L_0x009f:
            java.util.ArrayList r4 = (java.util.ArrayList) r4
            com.fossil.bj5 r10 = new com.fossil.bj5
            boolean r11 = r12.b()
            r10.<init>(r4, r11)
            goto L_0x00e7
        L_0x00ab:
            com.fossil.ee7.a()
            throw r4
        L_0x00af:
            boolean r10 = r12 instanceof com.fossil.yi5
            if (r10 == 0) goto L_0x00e8
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "getRingStyleList() error="
            r0.append(r1)
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            com.portfolio.platform.data.model.ServerError r1 = r12.c()
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r10.e(r11, r0)
            com.fossil.yi5 r10 = new com.fossil.yi5
            int r2 = r12.a()
            com.portfolio.platform.data.model.ServerError r3 = r12.c()
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 28
            r8 = 0
            r1 = r10
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
        L_0x00e7:
            return r10
        L_0x00e8:
            com.fossil.p87 r10 = new com.fossil.p87
            r10.<init>()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource.getRingStyleList(java.lang.String, boolean, com.fossil.fb7):java.lang.Object");
    }
}
