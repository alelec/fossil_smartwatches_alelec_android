package com.portfolio.platform.data.source;

import com.fossil.ai7;
import com.fossil.dc2;
import com.fossil.ee7;
import com.fossil.io3;
import com.fossil.qe7;
import com.fossil.s87;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2 implements io3 {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ai7 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ qe7 $countSizeOfList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $gFitActiveTimeList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ dc2 $historyClient$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfGFitActiveTimeList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore
    public ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(dc2 dc2, qe7 qe7, int i, ai7 ai7, ThirdPartyRepository thirdPartyRepository, List list, String str) {
        this.$historyClient$inlined = dc2;
        this.$countSizeOfList$inlined = qe7;
        this.$sizeOfGFitActiveTimeList$inlined = i;
        this.$continuation$inlined = ai7;
        this.this$0 = thirdPartyRepository;
        this.$gFitActiveTimeList$inlined = list;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    @Override // com.fossil.io3
    public final void onFailure(Exception exc) {
        ee7.b(exc, "it");
        FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "saveGFitActiveTimeToGoogleFit - Failure");
        qe7 qe7 = this.$countSizeOfList$inlined;
        int i = qe7.element + 1;
        qe7.element = i;
        if (i >= this.$sizeOfGFitActiveTimeList$inlined && this.$continuation$inlined.isActive()) {
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveGFitActiveTimeToGoogleFit - Failure");
            ai7 ai7 = this.$continuation$inlined;
            s87.a aVar = s87.Companion;
            ai7.resumeWith(s87.m60constructorimpl(null));
        }
    }
}
