package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SummariesRepository$loadSummaries$2$response$1", f = "SummariesRepository.kt", l = {Action.Presenter.NEXT}, m = "invokeSuspend")
public final class SummariesRepository$loadSummaries$Anon2$response$Anon1_Level2 extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$loadSummaries$Anon2 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$loadSummaries$Anon2$response$Anon1_Level2(SummariesRepository$loadSummaries$Anon2 summariesRepository$loadSummaries$Anon2, fb7 fb7) {
        super(1, fb7);
        this.this$0 = summariesRepository$loadSummaries$Anon2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(fb7<?> fb7) {
        ee7.b(fb7, "completion");
        return new SummariesRepository$loadSummaries$Anon2$response$Anon1_Level2(this.this$0, fb7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public final Object invoke(fb7<? super fv7<ie4>> fb7) {
        return ((SummariesRepository$loadSummaries$Anon2$response$Anon1_Level2) create(fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            ApiServiceV2 access$getMApiServiceV2$p = this.this$0.this$0.mApiServiceV2;
            String g = zd5.g(this.this$0.$startDate);
            ee7.a((Object) g, "DateHelper.formatShortDate(startDate)");
            String g2 = zd5.g(this.this$0.$endDate);
            ee7.a((Object) g2, "DateHelper.formatShortDate(endDate)");
            this.label = 1;
            obj = access$getMApiServiceV2$p.getSummaries(g, g2, 0, 100, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
