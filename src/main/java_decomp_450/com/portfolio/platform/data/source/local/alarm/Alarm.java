package com.portfolio.platform.data.source.local.alarm;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.re4;
import com.fossil.te4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.fossil.x87;
import com.fossil.zd7;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Alarm implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @te4("createdAt")
    public String createdAt;
    @DexIgnore
    @te4(com.misfit.frameworks.buttonservice.model.Alarm.COLUMN_DAYS)
    public int[] days;
    @DexIgnore
    @te4(AppFilter.COLUMN_HOUR)
    public int hour;
    @DexIgnore
    @te4("id")
    public String id;
    @DexIgnore
    @te4("isActive")
    public boolean isActive;
    @DexIgnore
    @te4("isRepeated")
    public boolean isRepeated;
    @DexIgnore
    @te4("message")
    public String message;
    @DexIgnore
    @te4(MFSleepGoal.COLUMN_MINUTE)
    public int minute;
    @DexIgnore
    @re4
    public int pinType;
    @DexIgnore
    @te4("title")
    public String title;
    @DexIgnore
    @te4("updatedAt")
    public String updatedAt;
    @DexIgnore
    @te4("uri")
    public String uri;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<Alarm> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Alarm createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new Alarm(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Alarm[] newArray(int i) {
            return new Alarm[i];
        }
    }

    @DexIgnore
    public Alarm(String str, String str2, String str3, String str4, int i, int i2, int[] iArr, boolean z, boolean z2, String str5, String str6, int i3) {
        ee7.b(str2, "uri");
        ee7.b(str3, "title");
        ee7.b(str4, "message");
        ee7.b(str6, "updatedAt");
        this.id = str;
        this.uri = str2;
        this.title = str3;
        this.message = str4;
        this.hour = i;
        this.minute = i2;
        this.days = iArr;
        this.isActive = z;
        this.isRepeated = z2;
        this.createdAt = str5;
        this.updatedAt = str6;
        this.pinType = i3;
    }

    @DexIgnore
    public static /* synthetic */ Alarm copy$default(Alarm alarm, String str, String str2, String str3, String str4, int i, int i2, int[] iArr, boolean z, boolean z2, String str5, String str6, int i3, int i4, Object obj) {
        return alarm.copy((i4 & 1) != 0 ? alarm.id : str, (i4 & 2) != 0 ? alarm.uri : str2, (i4 & 4) != 0 ? alarm.title : str3, (i4 & 8) != 0 ? alarm.message : str4, (i4 & 16) != 0 ? alarm.hour : i, (i4 & 32) != 0 ? alarm.minute : i2, (i4 & 64) != 0 ? alarm.days : iArr, (i4 & 128) != 0 ? alarm.isActive : z, (i4 & 256) != 0 ? alarm.isRepeated : z2, (i4 & 512) != 0 ? alarm.createdAt : str5, (i4 & 1024) != 0 ? alarm.updatedAt : str6, (i4 & 2048) != 0 ? alarm.pinType : i3);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component10() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component11() {
        return this.updatedAt;
    }

    @DexIgnore
    public final int component12() {
        return this.pinType;
    }

    @DexIgnore
    public final String component2() {
        return this.uri;
    }

    @DexIgnore
    public final String component3() {
        return this.title;
    }

    @DexIgnore
    public final String component4() {
        return this.message;
    }

    @DexIgnore
    public final int component5() {
        return this.hour;
    }

    @DexIgnore
    public final int component6() {
        return this.minute;
    }

    @DexIgnore
    public final int[] component7() {
        return this.days;
    }

    @DexIgnore
    public final boolean component8() {
        return this.isActive;
    }

    @DexIgnore
    public final boolean component9() {
        return this.isRepeated;
    }

    @DexIgnore
    public final Alarm copy(String str, String str2, String str3, String str4, int i, int i2, int[] iArr, boolean z, boolean z2, String str5, String str6, int i3) {
        ee7.b(str2, "uri");
        ee7.b(str3, "title");
        ee7.b(str4, "message");
        ee7.b(str6, "updatedAt");
        return new Alarm(str, str2, str3, str4, i, i2, iArr, z, z2, str5, str6, i3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Boolean bool;
        if (this == obj) {
            return true;
        }
        if (!ee7.a(Alarm.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Alarm alarm = (Alarm) obj;
            if ((!ee7.a((Object) this.id, (Object) alarm.id)) || (!ee7.a((Object) this.uri, (Object) alarm.uri)) || (!ee7.a((Object) this.title, (Object) alarm.title)) || (!ee7.a((Object) this.message, (Object) alarm.message)) || this.hour != alarm.hour || this.minute != alarm.minute || this.isActive != alarm.isActive || this.isRepeated != alarm.isRepeated) {
                return false;
            }
            int[] iArr = this.days;
            Integer valueOf = iArr != null ? Integer.valueOf(iArr.length) : null;
            int[] iArr2 = alarm.days;
            if (!(!ee7.a(valueOf, iArr2 != null ? Integer.valueOf(iArr2.length) : null))) {
                int[] iArr3 = this.days;
                if (iArr3 != null) {
                    int[] iArr4 = alarm.days;
                    if (iArr4 != null) {
                        bool = Boolean.valueOf(Arrays.equals(iArr3, iArr4));
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    bool = null;
                }
                if (bool != null) {
                    return bool.booleanValue();
                }
                ee7.a();
                throw null;
            }
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.data.source.local.alarm.Alarm");
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final int[] getDays() {
        return this.days;
    }

    @DexIgnore
    public final int getHour() {
        return this.hour;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getMessage() {
        return this.message;
    }

    @DexIgnore
    public final long getMillisecond() {
        return ((long) ((this.minute + (this.hour * 60)) * 60)) * 1000;
    }

    @DexIgnore
    public final int getMinute() {
        return this.minute;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getTitle() {
        return this.title;
    }

    @DexIgnore
    public final int getTotalMinutes() {
        return this.minute + (this.hour * 60);
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getUri() {
        return this.uri;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        return ((str != null ? str.hashCode() : 0) * 31) + this.uri.hashCode();
    }

    @DexIgnore
    public final boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public final boolean isRepeated() {
        return this.isRepeated;
    }

    @DexIgnore
    public final void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDays(int[] iArr) {
        this.days = iArr;
    }

    @DexIgnore
    public final void setHour(int i) {
        this.hour = i;
    }

    @DexIgnore
    public final void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public final void setMessage(String str) {
        ee7.b(str, "<set-?>");
        this.message = str;
    }

    @DexIgnore
    public final void setMinute(int i) {
        this.minute = i;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setRepeated(boolean z) {
        this.isRepeated = z;
    }

    @DexIgnore
    public final void setTitle(String str) {
        ee7.b(str, "<set-?>");
        this.title = str;
    }

    @DexIgnore
    public final void setTotalMinutes(int i) {
        this.hour = i / 60;
        this.minute = i % 60;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        ee7.b(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setUri(String str) {
        ee7.b(str, "<set-?>");
        this.uri = str;
    }

    @DexIgnore
    public String toString() {
        return "Alarm(id=" + this.id + ", uri=" + this.uri + ", title=" + this.title + ", message=" + this.message + ", hour=" + this.hour + ", minute=" + this.minute + ", days=" + Arrays.toString(this.days) + ", isActive=" + this.isActive + ", isRepeated=" + this.isRepeated + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", pinType=" + this.pinType + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeString(this.uri);
        parcel.writeString(this.title);
        parcel.writeString(this.message);
        parcel.writeInt(this.hour);
        parcel.writeInt(this.minute);
        parcel.writeIntArray(this.days);
        parcel.writeByte(this.isActive ? (byte) 1 : 0);
        parcel.writeByte(this.isRepeated ? (byte) 1 : 0);
        parcel.writeString(this.createdAt);
        parcel.writeString(this.updatedAt);
        parcel.writeInt(this.pinType);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Alarm(String str, String str2, String str3, String str4, int i, int i2, int[] iArr, boolean z, boolean z2, String str5, String str6, int i3, int i4, zd7 zd7) {
        this(str, str2, str3, str4, i, i2, iArr, z, z2, str5, str6, (i4 & 2048) != 0 ? 1 : i3);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Alarm(android.os.Parcel r15) {
        /*
            r14 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r15, r0)
            java.lang.String r2 = r15.readString()
            java.lang.String r0 = r15.readString()
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x0013
            r3 = r0
            goto L_0x0014
        L_0x0013:
            r3 = r1
        L_0x0014:
            java.lang.String r0 = r15.readString()
            if (r0 == 0) goto L_0x001c
            r4 = r0
            goto L_0x001d
        L_0x001c:
            r4 = r1
        L_0x001d:
            java.lang.String r0 = r15.readString()
            if (r0 == 0) goto L_0x0025
            r5 = r0
            goto L_0x0026
        L_0x0025:
            r5 = r1
        L_0x0026:
            int r6 = r15.readInt()
            int r7 = r15.readInt()
            int[] r8 = r15.createIntArray()
            byte r0 = r15.readByte()
            r9 = 0
            byte r10 = (byte) r9
            r11 = 1
            if (r0 == r10) goto L_0x003d
            r0 = 1
            goto L_0x003e
        L_0x003d:
            r0 = 0
        L_0x003e:
            byte r12 = r15.readByte()
            if (r12 == r10) goto L_0x0046
            r10 = 1
            goto L_0x0047
        L_0x0046:
            r10 = 0
        L_0x0047:
            java.lang.String r9 = r15.readString()
            if (r9 == 0) goto L_0x004f
            r11 = r9
            goto L_0x0050
        L_0x004f:
            r11 = r1
        L_0x0050:
            java.lang.String r9 = r15.readString()
            if (r9 == 0) goto L_0x0058
            r12 = r9
            goto L_0x0059
        L_0x0058:
            r12 = r1
        L_0x0059:
            int r13 = r15.readInt()
            r1 = r14
            r9 = r0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.local.alarm.Alarm.<init>(android.os.Parcel):void");
    }
}
