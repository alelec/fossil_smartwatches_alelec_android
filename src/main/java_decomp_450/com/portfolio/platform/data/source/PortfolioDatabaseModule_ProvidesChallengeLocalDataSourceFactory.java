package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.fossil.nn4;
import com.fossil.po4;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory implements Factory<po4> {
    @DexIgnore
    public /* final */ Provider<nn4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<nn4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<nn4> provider) {
        return new PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static po4 providesChallengeLocalDataSource(PortfolioDatabaseModule portfolioDatabaseModule, nn4 nn4) {
        po4 providesChallengeLocalDataSource = portfolioDatabaseModule.providesChallengeLocalDataSource(nn4);
        c87.a(providesChallengeLocalDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return providesChallengeLocalDataSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public po4 get() {
        return providesChallengeLocalDataSource(this.module, this.daoProvider.get());
    }
}
