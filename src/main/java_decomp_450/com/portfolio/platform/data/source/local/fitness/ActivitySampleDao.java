package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.ge;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ActivitySampleDao {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = ActivitySampleDao.class.getSimpleName();
        ee7.a((Object) simpleName, "ActivitySampleDao::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    private final void calculateSample(ActivitySample activitySample, ActivitySample activitySample2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateSample - currentSample=" + activitySample + ", newSample=" + activitySample2);
        double steps = activitySample2.getSteps() + activitySample.getSteps();
        double calories = activitySample2.getCalories() + activitySample.getCalories();
        double distance = activitySample2.getDistance() + activitySample.getDistance();
        activitySample2.setSteps(steps);
        activitySample2.setCalories(calories);
        activitySample2.setDistance(distance);
        activitySample2.setActiveTime(activitySample2.getActiveTime() + activitySample.getActiveTime());
        activitySample2.setCreatedAt(activitySample.getCreatedAt());
        activitySample2.getIntensityDistInSteps().updateActivityIntensities(activitySample.getIntensityDistInSteps());
        if (activitySample2.getSteps() != activitySample.getSteps()) {
            activitySample2.setUpdatedAt(new Date().getTime());
        }
    }

    @DexIgnore
    public abstract void deleteAllActivitySamples();

    @DexIgnore
    public abstract ActivitySample getActivitySample(String str);

    @DexIgnore
    public final LiveData<List<ActivitySample>> getActivitySamplesLiveData(Date date, Date date2) {
        ee7.b(date, GoalPhase.COLUMN_START_DATE);
        ee7.b(date2, GoalPhase.COLUMN_END_DATE);
        LiveData<List<ActivitySample>> b = ge.b(getActivitySamplesLiveDataV2(date, date2), new ActivitySampleDao$getActivitySamplesLiveData$Anon1(this, date, date2));
        ee7.a((Object) b, "Transformations.switchMa\u2026}\n            }\n        }");
        return b;
    }

    @DexIgnore
    public abstract LiveData<List<SampleRaw>> getActivitySamplesLiveDataV1(Date date, Date date2);

    @DexIgnore
    public abstract LiveData<List<ActivitySample>> getActivitySamplesLiveDataV2(Date date, Date date2);

    @DexIgnore
    public final void insertActivitySamples(List<ActivitySample> list) {
        ee7.b(list, "activitySamples");
        for (T t : list) {
            ActivitySample activitySample = getActivitySample(t.getId());
            if (activitySample != null) {
                calculateSample(activitySample, t);
            } else {
                t.setCreatedAt(new Date().getTime());
                t.setUpdatedAt(new Date().getTime());
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "XXX- upsertActivitySamples " + list);
        upsertListActivitySample(list);
    }

    @DexIgnore
    public abstract void upsertListActivitySample(List<ActivitySample> list);
}
