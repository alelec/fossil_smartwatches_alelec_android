package com.portfolio.platform.data.source.local;

import com.portfolio.platform.data.model.Theme;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ThemeDao {
    @DexIgnore
    public abstract void clearThemeTable();

    @DexIgnore
    public abstract void deleteTheme(Theme theme);

    @DexIgnore
    public abstract Theme getCurrentTheme();

    @DexIgnore
    public abstract String getCurrentThemeId();

    @DexIgnore
    public abstract List<Theme> getListTheme();

    @DexIgnore
    public abstract List<String> getListThemeId();

    @DexIgnore
    public abstract List<String> getListThemeIdByType(String str);

    @DexIgnore
    public abstract String getNameById(String str);

    @DexIgnore
    public abstract Theme getThemeById(String str);

    @DexIgnore
    public abstract void insertListTheme(List<Theme> list);

    @DexIgnore
    public abstract void resetAllCurrentTheme();

    @DexIgnore
    public abstract void setCurrentThemeId(String str);

    @DexIgnore
    public abstract void upsertTheme(Theme theme);
}
