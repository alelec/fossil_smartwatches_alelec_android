package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.pu4;
import com.fossil.ru4;
import com.fossil.uh;
import com.fossil.vh;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDataDao_Impl extends FitnessDataDao {
    @DexIgnore
    public /* final */ pu4 __dateTimeUTCStringConverter; // = new pu4();
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ uh<FitnessDataWrapper> __deletionAdapterOfFitnessDataWrapper;
    @DexIgnore
    public /* final */ ru4 __fitnessDataConverter; // = new ru4();
    @DexIgnore
    public /* final */ vh<FitnessDataWrapper> __insertionAdapterOfFitnessDataWrapper;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteAllFitnessData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<FitnessDataWrapper> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR IGNORE INTO `fitness_data` (`step`,`activeMinute`,`calorie`,`distance`,`stress`,`resting`,`heartRate`,`sleeps`,`workouts`,`startTime`,`endTime`,`syncTime`,`timezoneOffsetInSecond`,`serialNumber`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, FitnessDataWrapper fitnessDataWrapper) {
            String a = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.step);
            if (a == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, a);
            }
            String a2 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.activeMinute);
            if (a2 == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, a2);
            }
            String a3 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.calorie);
            if (a3 == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, a3);
            }
            String a4 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.distance);
            if (a4 == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, a4);
            }
            String a5 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.getStress());
            if (a5 == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a5);
            }
            String a6 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.getResting());
            if (a6 == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, a6);
            }
            String a7 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.getHeartRate());
            if (a7 == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, a7);
            }
            String b = FitnessDataDao_Impl.this.__fitnessDataConverter.b(fitnessDataWrapper.getSleeps());
            if (b == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, b);
            }
            String c = FitnessDataDao_Impl.this.__fitnessDataConverter.c(fitnessDataWrapper.getWorkouts());
            if (c == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindString(9, c);
            }
            String a8 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getStartTime());
            if (a8 == null) {
                ajVar.bindNull(10);
            } else {
                ajVar.bindString(10, a8);
            }
            String a9 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getEndTime());
            if (a9 == null) {
                ajVar.bindNull(11);
            } else {
                ajVar.bindString(11, a9);
            }
            String a10 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getSyncTime());
            if (a10 == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, a10);
            }
            ajVar.bindLong(13, (long) fitnessDataWrapper.getTimezoneOffsetInSecond());
            if (fitnessDataWrapper.getSerialNumber() == null) {
                ajVar.bindNull(14);
            } else {
                ajVar.bindString(14, fitnessDataWrapper.getSerialNumber());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends uh<FitnessDataWrapper> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.uh, com.fossil.ji
        public String createQuery() {
            return "DELETE FROM `fitness_data` WHERE `startTime` = ?";
        }

        @DexIgnore
        public void bind(aj ajVar, FitnessDataWrapper fitnessDataWrapper) {
            String a = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getStartTime());
            if (a == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, a);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM fitness_data";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<FitnessDataWrapper>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon4(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<FitnessDataWrapper> call() throws Exception {
            Cursor a = pi.a(FitnessDataDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "step");
                int b2 = oi.b(a, "activeMinute");
                int b3 = oi.b(a, "calorie");
                int b4 = oi.b(a, "distance");
                int b5 = oi.b(a, "stress");
                int b6 = oi.b(a, "resting");
                int b7 = oi.b(a, "heartRate");
                int b8 = oi.b(a, "sleeps");
                int b9 = oi.b(a, "workouts");
                int b10 = oi.b(a, SampleRaw.COLUMN_START_TIME);
                int b11 = oi.b(a, SampleRaw.COLUMN_END_TIME);
                int b12 = oi.b(a, "syncTime");
                int b13 = oi.b(a, "timezoneOffsetInSecond");
                int b14 = oi.b(a, "serialNumber");
                int i = b9;
                int i2 = b8;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(a.getString(b10)), FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(a.getString(b11)), FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(a.getString(b12)), a.getInt(b13), a.getString(b14));
                    fitnessDataWrapper.step = FitnessDataDao_Impl.this.__fitnessDataConverter.g(a.getString(b));
                    fitnessDataWrapper.activeMinute = FitnessDataDao_Impl.this.__fitnessDataConverter.a(a.getString(b2));
                    fitnessDataWrapper.calorie = FitnessDataDao_Impl.this.__fitnessDataConverter.b(a.getString(b3));
                    fitnessDataWrapper.distance = FitnessDataDao_Impl.this.__fitnessDataConverter.c(a.getString(b4));
                    fitnessDataWrapper.setStress(FitnessDataDao_Impl.this.__fitnessDataConverter.h(a.getString(b5)));
                    fitnessDataWrapper.setResting(FitnessDataDao_Impl.this.__fitnessDataConverter.e(a.getString(b6)));
                    fitnessDataWrapper.setHeartRate(FitnessDataDao_Impl.this.__fitnessDataConverter.d(a.getString(b7)));
                    String string = a.getString(i2);
                    i2 = i2;
                    fitnessDataWrapper.setSleeps(FitnessDataDao_Impl.this.__fitnessDataConverter.f(string));
                    String string2 = a.getString(i);
                    i = i;
                    fitnessDataWrapper.setWorkouts(FitnessDataDao_Impl.this.__fitnessDataConverter.i(string2));
                    arrayList.add(fitnessDataWrapper);
                    b10 = b10;
                    b = b;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public FitnessDataDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfFitnessDataWrapper = new Anon1(ciVar);
        this.__deletionAdapterOfFitnessDataWrapper = new Anon2(ciVar);
        this.__preparedStmtOfDeleteAllFitnessData = new Anon3(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public void deleteAllFitnessData() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteAllFitnessData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllFitnessData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public void deleteFitnessData(List<FitnessDataWrapper> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfFitnessDataWrapper.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public List<FitnessDataWrapper> getListFitnessData(DateTime dateTime, DateTime dateTime2) {
        fi fiVar;
        fi b = fi.b("SELECT * FROM fitness_data WHERE startTime >= ? AND startTime <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateTimeUTCStringConverter.a(dateTime);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateTimeUTCStringConverter.a(dateTime2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a3, "step");
            int b3 = oi.b(a3, "activeMinute");
            int b4 = oi.b(a3, "calorie");
            int b5 = oi.b(a3, "distance");
            int b6 = oi.b(a3, "stress");
            int b7 = oi.b(a3, "resting");
            int b8 = oi.b(a3, "heartRate");
            int b9 = oi.b(a3, "sleeps");
            int b10 = oi.b(a3, "workouts");
            int b11 = oi.b(a3, SampleRaw.COLUMN_START_TIME);
            int b12 = oi.b(a3, SampleRaw.COLUMN_END_TIME);
            int b13 = oi.b(a3, "syncTime");
            int b14 = oi.b(a3, "timezoneOffsetInSecond");
            fiVar = b;
            try {
                int b15 = oi.b(a3, "serialNumber");
                int i = b10;
                int i2 = b9;
                ArrayList arrayList = new ArrayList(a3.getCount());
                while (a3.moveToNext()) {
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(this.__dateTimeUTCStringConverter.a(a3.getString(b11)), this.__dateTimeUTCStringConverter.a(a3.getString(b12)), this.__dateTimeUTCStringConverter.a(a3.getString(b13)), a3.getInt(b14), a3.getString(b15));
                    fitnessDataWrapper.step = this.__fitnessDataConverter.g(a3.getString(b2));
                    fitnessDataWrapper.activeMinute = this.__fitnessDataConverter.a(a3.getString(b3));
                    fitnessDataWrapper.calorie = this.__fitnessDataConverter.b(a3.getString(b4));
                    fitnessDataWrapper.distance = this.__fitnessDataConverter.c(a3.getString(b5));
                    fitnessDataWrapper.setStress(this.__fitnessDataConverter.h(a3.getString(b6)));
                    fitnessDataWrapper.setResting(this.__fitnessDataConverter.e(a3.getString(b7)));
                    fitnessDataWrapper.setHeartRate(this.__fitnessDataConverter.d(a3.getString(b8)));
                    String string = a3.getString(i2);
                    i2 = i2;
                    fitnessDataWrapper.setSleeps(this.__fitnessDataConverter.f(string));
                    String string2 = a3.getString(i);
                    i = i;
                    fitnessDataWrapper.setWorkouts(this.__fitnessDataConverter.i(string2));
                    arrayList.add(fitnessDataWrapper);
                    b11 = b11;
                    b2 = b2;
                }
                a3.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a3.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a3.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public LiveData<List<FitnessDataWrapper>> getListFitnessDataLiveData(DateTime dateTime, DateTime dateTime2) {
        fi b = fi.b("SELECT * FROM fitness_data WHERE startTime >= ? AND startTime <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateTimeUTCStringConverter.a(dateTime);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateTimeUTCStringConverter.a(dateTime2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"fitness_data"}, false, (Callable) new Anon4(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public List<FitnessDataWrapper> getPendingFitnessData() {
        fi fiVar;
        fi b = fi.b("SELECT * FROM fitness_data ORDER BY startTime ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "step");
            int b3 = oi.b(a, "activeMinute");
            int b4 = oi.b(a, "calorie");
            int b5 = oi.b(a, "distance");
            int b6 = oi.b(a, "stress");
            int b7 = oi.b(a, "resting");
            int b8 = oi.b(a, "heartRate");
            int b9 = oi.b(a, "sleeps");
            int b10 = oi.b(a, "workouts");
            int b11 = oi.b(a, SampleRaw.COLUMN_START_TIME);
            int b12 = oi.b(a, SampleRaw.COLUMN_END_TIME);
            int b13 = oi.b(a, "syncTime");
            int b14 = oi.b(a, "timezoneOffsetInSecond");
            fiVar = b;
            try {
                int b15 = oi.b(a, "serialNumber");
                int i = b10;
                int i2 = b9;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(this.__dateTimeUTCStringConverter.a(a.getString(b11)), this.__dateTimeUTCStringConverter.a(a.getString(b12)), this.__dateTimeUTCStringConverter.a(a.getString(b13)), a.getInt(b14), a.getString(b15));
                    fitnessDataWrapper.step = this.__fitnessDataConverter.g(a.getString(b2));
                    fitnessDataWrapper.activeMinute = this.__fitnessDataConverter.a(a.getString(b3));
                    fitnessDataWrapper.calorie = this.__fitnessDataConverter.b(a.getString(b4));
                    fitnessDataWrapper.distance = this.__fitnessDataConverter.c(a.getString(b5));
                    fitnessDataWrapper.setStress(this.__fitnessDataConverter.h(a.getString(b6)));
                    fitnessDataWrapper.setResting(this.__fitnessDataConverter.e(a.getString(b7)));
                    fitnessDataWrapper.setHeartRate(this.__fitnessDataConverter.d(a.getString(b8)));
                    String string = a.getString(i2);
                    i2 = i2;
                    fitnessDataWrapper.setSleeps(this.__fitnessDataConverter.f(string));
                    String string2 = a.getString(i);
                    i = i;
                    fitnessDataWrapper.setWorkouts(this.__fitnessDataConverter.i(string2));
                    arrayList.add(fitnessDataWrapper);
                    b11 = b11;
                    b2 = b2;
                }
                a.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public void insertFitnessDataList(List<FitnessDataWrapper> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfFitnessDataWrapper.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
