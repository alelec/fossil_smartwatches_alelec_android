package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.ik7;
import com.fossil.of;
import com.fossil.pj4;
import com.fossil.qj7;
import com.fossil.r87;
import com.fossil.te5;
import com.fossil.ue5;
import com.fossil.w97;
import com.fossil.xh7;
import com.fossil.zd5;
import com.fossil.zd7;
import com.fossil.zh;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummaryLocalDataSource extends of<Date, GoalTrackingSummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "GoalTrackingSummaryLocalDataSource";
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ te5.a listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public te5 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ zh.c mObserver;
    @DexIgnore
    public List<r87<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public Date mStartDate; // = new Date();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends zh.c {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = goalTrackingSummaryLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.zh.c
        public void onInvalidated(Set<String> set) {
            ee7.b(set, "tables");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            ee7.b(date, "date");
            ee7.b(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(GoalTrackingSummaryLocalDataSource.TAG, "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar c = zd5.c(instance);
            if (zd5.b(date2, c.getTime())) {
                c.setTime(date2);
            }
            Date time = c.getTime();
            ee7.a((Object) time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public GoalTrackingSummaryLocalDataSource(GoalTrackingRepository goalTrackingRepository, Date date, GoalTrackingDatabase goalTrackingDatabase, pj4 pj4, te5.a aVar, Calendar calendar) {
        ee7.b(goalTrackingRepository, "mGoalTrackingRepository");
        ee7.b(date, "mCreatedDate");
        ee7.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        ee7.b(pj4, "appExecutors");
        ee7.b(aVar, "listener");
        ee7.b(calendar, "key");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mCreatedDate = date;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.listener = aVar;
        this.key = calendar;
        te5 te5 = new te5(pj4.a());
        this.mHelper = te5;
        this.mNetworkState = ue5.a(te5);
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "goalTrackingDay", new String[0]);
        this.mGoalTrackingDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar c = zd5.c(instance);
        c.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + c.getTime());
        Date time = c.getTime();
        ee7.a((Object) time, "calendar.time");
        this.mStartDate = time;
        if (zd5.b(date, time)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final List<GoalTrackingSummary> calculateSummaries(List<GoalTrackingSummary> list) {
        int i;
        int i2;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "endCalendar");
            instance.setTime(((GoalTrackingSummary) ea7.f((List) list)).getDate());
            if (instance.get(7) != 1) {
                Calendar s = zd5.s(instance.getTime());
                ee7.a((Object) s, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                GoalTrackingDao goalTrackingDao = this.mGoalTrackingDatabase.getGoalTrackingDao();
                Date time = s.getTime();
                ee7.a((Object) time, "startCalendar.time");
                Date time2 = instance.getTime();
                ee7.a((Object) time2, "endCalendar.time");
                GoalTrackingDao.TotalSummary goalTrackingValueAndTarget = goalTrackingDao.getGoalTrackingValueAndTarget(time, time2);
                i = goalTrackingValueAndTarget.getValues();
                i2 = goalTrackingValueAndTarget.getTargets();
            } else {
                i2 = 0;
                i = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            ee7.a((Object) instance2, "calendar");
            instance2.setTime(((GoalTrackingSummary) ea7.d((List) list)).getDate());
            Calendar s2 = zd5.s(instance2.getTime());
            ee7.a((Object) s2, "DateHelper.getStartOfWeek(calendar.time)");
            s2.add(5, -1);
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            for (T t : list) {
                int i7 = i6 + 1;
                if (i6 >= 0) {
                    T t2 = t;
                    Date component1 = t2.component1();
                    int component2 = t2.component2();
                    int component3 = t2.component3();
                    if (zd5.d(component1, s2.getTime())) {
                        list.get(i3).setTotalValueOfWeek(i4);
                        list.get(i3).setTotalTargetOfWeek(i5);
                        s2.add(5, -7);
                        i3 = i6;
                        i4 = 0;
                        i5 = 0;
                    }
                    i4 += component2;
                    i5 += component3;
                    if (i6 == list.size() - 1) {
                        i4 += i;
                        i5 += i2;
                    }
                    i6 = i7;
                } else {
                    w97.c();
                    throw null;
                }
            }
            list.get(i3).setTotalValueOfWeek(i4);
            list.get(i3).setTotalTargetOfWeek(i5);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(TAG, "calculateSummaries summaries.size=" + list.size());
        }
        return list;
    }

    @DexIgnore
    private final GoalTrackingSummary dummySummary(Date date) {
        return new GoalTrackingSummary(date, 0, this.mGoalTrackingDatabase.getGoalTrackingDao().getNearestGoalTrackingTargetFromDate(date), new Date().getTime(), new Date().getTime());
    }

    @DexIgnore
    private final List<GoalTrackingSummary> getDataInDatabase(Date date, Date date2) {
        FLogger.INSTANCE.getLocal().d(TAG, "getDataInDatabase - startDate=" + date + ", endDate=" + date2);
        List<GoalTrackingSummary> goalTrackingSummaries = this.mGoalTrackingRepository.getGoalTrackingSummaries(date, date2, this.mGoalTrackingDatabase.getGoalTrackingDao());
        ArrayList arrayList = new ArrayList();
        int size = goalTrackingSummaries.size() + -1;
        FLogger.INSTANCE.getLocal().d(TAG, "getDataInDatabase - summaries.size=" + goalTrackingSummaries.size() + ", " + "startDate=" + date + ", endDateToFill=" + date2);
        while (zd5.c(date2, date)) {
            if (size < 0 || !zd5.d(goalTrackingSummaries.get(size).getDate(), date2)) {
                arrayList.add(dummySummary(date2));
            } else {
                arrayList.add(goalTrackingSummaries.get(size));
                size--;
            }
            date2 = zd5.p(date2);
            ee7.a((Object) date2, "DateHelper.getPrevDate(endDateToFill)");
        }
        calculateSummaries(arrayList);
        if (!arrayList.isEmpty()) {
            GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) ea7.d((List) arrayList);
            Boolean w = zd5.w(goalTrackingSummary.getDate());
            ee7.a((Object) w, "DateHelper.isToday(todaySummary.date)");
            if (w.booleanValue()) {
                arrayList.add(0, GoalTrackingSummary.copy$default(goalTrackingSummary, goalTrackingSummary.getDate(), goalTrackingSummary.getTotalTracked(), goalTrackingSummary.getGoalTarget(), 0, 0, 24, null));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final ik7 loadData(te5.d dVar, Date date, Date date2, te5.b.a aVar) {
        return xh7.b(zi7.a(qj7.b()), null, null, new GoalTrackingSummaryLocalDataSource$loadData$Anon1(this, date, date2, dVar, aVar, null), 3, null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final te5 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    @Override // com.fossil.lf
    public boolean isInvalid() {
        this.mGoalTrackingDatabase.getInvalidationTracker().b();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadAfter(of.f<Date> fVar, of.a<Date, GoalTrackingSummary> aVar) {
        ee7.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Object) fVar.a));
        if (zd5.b(fVar.a, this.mCreatedDate)) {
            Key key2 = fVar.a;
            ee7.a((Object) key2, "params.key");
            Key key3 = key2;
            Companion companion = Companion;
            Key key4 = fVar.a;
            ee7.a((Object) key4, "params.key");
            Date calculateNextKey = companion.calculateNextKey(key4, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date o = zd5.d(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : zd5.o(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + o + ", endQueryDate=" + ((Object) key3));
            ee7.a((Object) o, "startQueryDate");
            aVar.a(getDataInDatabase(o, key3), calculateNextKey);
            if (zd5.b(this.mStartDate, (Date) key3)) {
                this.mEndDate = key3;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d(TAG, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new r87<>(this.mStartDate, this.mEndDate));
                this.mHelper.a(te5.d.AFTER, new GoalTrackingSummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadBefore(of.f<Date> fVar, of.a<Date, GoalTrackingSummary> aVar) {
        ee7.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadInitial(of.e<Date> eVar, of.c<Date, GoalTrackingSummary> cVar) {
        ee7.b(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date o = zd5.d(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : zd5.o(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + o + ", endQueryDate=" + date);
        ee7.a((Object) o, "startQueryDate");
        cVar.a(getDataInDatabase(o, date), null, this.key.getTime());
        this.mHelper.a(te5.d.INITIAL, new GoalTrackingSummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mGoalTrackingDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        ee7.b(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(te5 te5) {
        ee7.b(te5, "<set-?>");
        this.mHelper = te5;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        ee7.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        ee7.b(date, "<set-?>");
        this.mStartDate = date;
    }
}
