package com.portfolio.platform.data.source.local.dnd;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DNDSettingsDatabase_Impl extends DNDSettingsDatabase {
    @DexIgnore
    public volatile DNDScheduledTimeDao _dNDScheduledTimeDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `dndScheduledTimeModel` (`scheduledTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, `scheduledTimeType` INTEGER NOT NULL, PRIMARY KEY(`scheduledTimeName`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'edbea9a651a6427903efe74c61fd6b87')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `dndScheduledTimeModel`");
            if (((ci) DNDSettingsDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) DNDSettingsDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) DNDSettingsDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) DNDSettingsDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) DNDSettingsDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) DNDSettingsDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) DNDSettingsDatabase_Impl.this).mDatabase = wiVar;
            DNDSettingsDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) DNDSettingsDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) DNDSettingsDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) DNDSettingsDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("scheduledTimeName", new ti.a("scheduledTimeName", "TEXT", true, 1, null, 1));
            hashMap.put("minutes", new ti.a("minutes", "INTEGER", true, 0, null, 1));
            hashMap.put("scheduledTimeType", new ti.a("scheduledTimeType", "INTEGER", true, 0, null, 1));
            ti tiVar = new ti("dndScheduledTimeModel", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "dndScheduledTimeModel");
            if (tiVar.equals(a)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "dndScheduledTimeModel(com.portfolio.platform.data.model.DNDScheduledTimeModel).\n Expected:\n" + tiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `dndScheduledTimeModel`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "dndScheduledTimeModel");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(1), "edbea9a651a6427903efe74c61fd6b87", "1a7527e5c35fa2c8a6aef70719b78d76");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase
    public DNDScheduledTimeDao getDNDScheduledTimeDao() {
        DNDScheduledTimeDao dNDScheduledTimeDao;
        if (this._dNDScheduledTimeDao != null) {
            return this._dNDScheduledTimeDao;
        }
        synchronized (this) {
            if (this._dNDScheduledTimeDao == null) {
                this._dNDScheduledTimeDao = new DNDScheduledTimeDao_Impl(this);
            }
            dNDScheduledTimeDao = this._dNDScheduledTimeDao;
        }
        return dNDScheduledTimeDao;
    }
}
