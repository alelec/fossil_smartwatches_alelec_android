package com.portfolio.platform.data.source.local.diana.notification;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationSettingsDao_Impl implements NotificationSettingsDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<NotificationSettingsModel> __insertionAdapterOfNotificationSettingsModel;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<NotificationSettingsModel> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `notificationSettings` (`settingsName`,`settingsType`,`isCall`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, NotificationSettingsModel notificationSettingsModel) {
            if (notificationSettingsModel.getSettingsName() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, notificationSettingsModel.getSettingsName());
            }
            ajVar.bindLong(2, (long) notificationSettingsModel.getSettingsType());
            ajVar.bindLong(3, notificationSettingsModel.isCall() ? 1 : 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM notificationSettings";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<NotificationSettingsModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon3(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<NotificationSettingsModel> call() throws Exception {
            Cursor a = pi.a(NotificationSettingsDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "settingsName");
                int b2 = oi.b(a, "settingsType");
                int b3 = oi.b(a, "isCall");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new NotificationSettingsModel(a.getString(b), a.getInt(b2), a.getInt(b3) != 0));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<NotificationSettingsModel> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon4(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public NotificationSettingsModel call() throws Exception {
            NotificationSettingsModel notificationSettingsModel = null;
            boolean z = false;
            Cursor a = pi.a(NotificationSettingsDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "settingsName");
                int b2 = oi.b(a, "settingsType");
                int b3 = oi.b(a, "isCall");
                if (a.moveToFirst()) {
                    String string = a.getString(b);
                    int i = a.getInt(b2);
                    if (a.getInt(b3) != 0) {
                        z = true;
                    }
                    notificationSettingsModel = new NotificationSettingsModel(string, i, z);
                }
                return notificationSettingsModel;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public NotificationSettingsDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfNotificationSettingsModel = new Anon1(ciVar);
        this.__preparedStmtOfDelete = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public LiveData<List<NotificationSettingsModel>> getListNotificationSettings() {
        return this.__db.getInvalidationTracker().a(new String[]{"notificationSettings"}, false, (Callable) new Anon3(fi.b("SELECT * FROM notificationSettings", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public List<NotificationSettingsModel> getListNotificationSettingsNoLiveData() {
        fi b = fi.b("SELECT * FROM notificationSettings", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "settingsName");
            int b3 = oi.b(a, "settingsType");
            int b4 = oi.b(a, "isCall");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new NotificationSettingsModel(a.getString(b2), a.getInt(b3), a.getInt(b4) != 0));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public LiveData<NotificationSettingsModel> getNotificationSettingsWithFieldIsCall(boolean z) {
        fi b = fi.b("SELECT * FROM notificationSettings WHERE isCall = ?", 1);
        b.bindLong(1, z ? 1 : 0);
        return this.__db.getInvalidationTracker().a(new String[]{"notificationSettings"}, false, (Callable) new Anon4(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public NotificationSettingsModel getNotificationSettingsWithIsCallNoLiveData(boolean z) {
        boolean z2 = true;
        fi b = fi.b("SELECT * FROM notificationSettings WHERE isCall = ?", 1);
        b.bindLong(1, z ? 1 : 0);
        this.__db.assertNotSuspendingTransaction();
        NotificationSettingsModel notificationSettingsModel = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "settingsName");
            int b3 = oi.b(a, "settingsType");
            int b4 = oi.b(a, "isCall");
            if (a.moveToFirst()) {
                String string = a.getString(b2);
                int i = a.getInt(b3);
                if (a.getInt(b4) == 0) {
                    z2 = false;
                }
                notificationSettingsModel = new NotificationSettingsModel(string, i, z2);
            }
            return notificationSettingsModel;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public void insertListNotificationSettings(List<NotificationSettingsModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfNotificationSettingsModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public void insertNotificationSettings(NotificationSettingsModel notificationSettingsModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfNotificationSettingsModel.insert(notificationSettingsModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
