package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.kd7;
import com.fossil.lx6;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.qx6;
import com.fossil.r87;
import com.fossil.t3;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.ti7;
import com.fossil.vh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SummariesRepository$getSummaries$2", f = "SummariesRepository.kt", l = {229}, m = "invokeSuspend")
public final class SummariesRepository$getSummaries$Anon2 extends zb7 implements kd7<yi7, fb7<? super LiveData<qx6<? extends List<ActivitySummary>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ FitnessDatabase $fitnessDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends lx6<List<ActivitySummary>, ie4> {
            @DexIgnore
            public /* final */ /* synthetic */ r87 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, r87 r87) {
                this.this$0 = anon1_Level2;
                this.$downloadingDate = r87;
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object createCall(fb7<? super fv7<ie4>> fb7) {
                Date date;
                Date date2;
                ApiServiceV2 access$getMApiServiceV2$p = this.this$0.this$0.this$0.mApiServiceV2;
                r87 r87 = this.$downloadingDate;
                if (r87 == null || (date = (Date) r87.getFirst()) == null) {
                    date = this.this$0.this$0.$startDate;
                }
                String g = zd5.g(date);
                ee7.a((Object) g, "DateHelper.formatShortDa\u2026            ?: startDate)");
                r87 r872 = this.$downloadingDate;
                if (r872 == null || (date2 = (Date) r872.getSecond()) == null) {
                    date2 = this.this$0.this$0.$endDate;
                }
                String g2 = zd5.g(date2);
                ee7.a((Object) g2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return access$getMApiServiceV2$p.getSummaries(g, g2, 0, 100, fb7);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:14:0x0047  */
            /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
            @Override // com.fossil.lx6
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object loadFromDb(com.fossil.fb7<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>>> r8) {
                /*
                    r7 = this;
                    boolean r0 = r8 instanceof com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    if (r0 == 0) goto L_0x0013
                    r0 = r8
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = (com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = -2147483648(0xffffffff80000000, float:-0.0)
                    r3 = r1 & r2
                    if (r3 == 0) goto L_0x0013
                    int r1 = r1 - r2
                    r0.label = r1
                    goto L_0x0018
                L_0x0013:
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = new com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    r0.<init>(r7, r8)
                L_0x0018:
                    java.lang.Object r8 = r0.result
                    java.lang.Object r1 = com.fossil.nb7.a()
                    int r2 = r0.label
                    r3 = 2
                    r4 = 1
                    java.lang.String r5 = "SummariesRepository"
                    if (r2 == 0) goto L_0x0047
                    if (r2 == r4) goto L_0x003f
                    if (r2 != r3) goto L_0x0037
                    java.lang.Object r1 = r0.L$1
                    androidx.lifecycle.LiveData r1 = (androidx.lifecycle.LiveData) r1
                    java.lang.Object r0 = r0.L$0
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3 r0 = (com.portfolio.platform.data.source.SummariesRepository.getSummaries.Anon2.Anon1_Level2.Anon1_Level3) r0
                    com.fossil.t87.a(r8)
                    goto L_0x00d8
                L_0x0037:
                    java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r8.<init>(r0)
                    throw r8
                L_0x003f:
                    java.lang.Object r0 = r0.L$0
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3 r0 = (com.portfolio.platform.data.source.SummariesRepository.getSummaries.Anon2.Anon1_Level2.Anon1_Level3) r0
                    com.fossil.t87.a(r8)
                    goto L_0x009d
                L_0x0047:
                    com.fossil.t87.a(r8)
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2 r8 = r7.this$0
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2 r8 = r8.this$0
                    java.util.Date r8 = r8.$endDate
                    java.lang.Boolean r8 = com.fossil.zd5.w(r8)
                    boolean r8 = r8.booleanValue()
                    r2 = 0
                    if (r8 != 0) goto L_0x00a0
                    com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r6 = "getSummaries - loadFromDb -- isNotToday - startDate="
                    r3.append(r6)
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2 r6 = r7.this$0
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2 r6 = r6.this$0
                    java.util.Date r6 = r6.$startDate
                    r3.append(r6)
                    java.lang.String r6 = ", endDate="
                    r3.append(r6)
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2 r6 = r7.this$0
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2 r6 = r6.this$0
                    java.util.Date r6 = r6.$endDate
                    r3.append(r6)
                    java.lang.String r3 = r3.toString()
                    r8.d(r5, r3)
                    com.fossil.ti7 r8 = com.fossil.qj7.b()
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4 r3 = new com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4
                    r3.<init>(r7, r2)
                    r0.L$0 = r7
                    r0.label = r4
                    java.lang.Object r8 = com.fossil.vh7.a(r8, r3, r0)
                    if (r8 != r1) goto L_0x009d
                    return r1
                L_0x009d:
                    androidx.lifecycle.LiveData r8 = (androidx.lifecycle.LiveData) r8
                    goto L_0x0102
                L_0x00a0:
                    com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                    java.lang.String r4 = "getSummaries - loadFromDb -- isToday"
                    r8.d(r5, r4)
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2 r8 = r7.this$0
                    com.portfolio.platform.data.source.local.fitness.FitnessDatabase r8 = r8.$fitnessDatabase
                    com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao r8 = r8.activitySummaryDao()
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2 r4 = r7.this$0
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2 r4 = r4.this$0
                    java.util.Date r6 = r4.$startDate
                    java.util.Date r4 = r4.$endDate
                    androidx.lifecycle.LiveData r8 = r8.getActivitySummariesLiveData(r6, r4)
                    com.fossil.ti7 r4 = com.fossil.qj7.b()
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$rawSummaryToday$Anon1_Level4 r6 = new com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$rawSummaryToday$Anon1_Level4
                    r6.<init>(r7, r2)
                    r0.L$0 = r7
                    r0.L$1 = r8
                    r0.label = r3
                    java.lang.Object r0 = com.fossil.vh7.a(r4, r6, r0)
                    if (r0 != r1) goto L_0x00d5
                    return r1
                L_0x00d5:
                    r1 = r8
                    r8 = r0
                    r0 = r7
                L_0x00d8:
                    com.portfolio.platform.data.model.room.fitness.ActivitySummary r8 = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) r8
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r4 = "todayRawSummary "
                    r3.append(r4)
                    r3.append(r8)
                    java.lang.String r8 = r3.toString()
                    r2.d(r5, r8)
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon3_Level4 r8 = new com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon3_Level4
                    r8.<init>(r0, r1)
                    androidx.lifecycle.LiveData r8 = com.fossil.ge.a(r1, r8)
                    java.lang.String r0 = "Transformations.map(resu\u2026                        }"
                    com.fossil.ee7.a(r8, r0)
                L_0x0102:
                    return r8
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.getSummaries.Anon2.Anon1_Level2.Anon1_Level3.loadFromDb(com.fossil.fb7):java.lang.Object");
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getSummaries - onFetchFailed");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object saveCallResult(ie4 ie4, fb7 fb7) {
                return saveCallResult(ie4, (fb7<? super i97>) fb7);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:16:0x0059  */
            /* JADX WARNING: Removed duplicated region for block: B:30:0x0113  */
            /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object saveCallResult(com.fossil.ie4 r12, com.fossil.fb7<? super com.fossil.i97> r13) {
                /*
                    r11 = this;
                    boolean r0 = r13 instanceof com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    if (r0 == 0) goto L_0x0013
                    r0 = r13
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = (com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = -2147483648(0xffffffff80000000, float:-0.0)
                    r3 = r1 & r2
                    if (r3 == 0) goto L_0x0013
                    int r1 = r1 - r2
                    r0.label = r1
                    goto L_0x0018
                L_0x0013:
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = new com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    r0.<init>(r11, r13)
                L_0x0018:
                    java.lang.Object r13 = r0.result
                    java.lang.Object r1 = com.fossil.nb7.a()
                    int r2 = r0.label
                    r3 = 0
                    r4 = 2
                    r5 = 1
                    java.lang.String r6 = "SummariesRepository"
                    if (r2 == 0) goto L_0x0059
                    if (r2 == r5) goto L_0x0048
                    if (r2 != r4) goto L_0x0040
                    java.lang.Object r12 = r0.L$3
                    java.util.List r12 = (java.util.List) r12
                    java.lang.Object r12 = r0.L$2
                    java.util.ArrayList r12 = (java.util.ArrayList) r12
                    java.lang.Object r12 = r0.L$1
                    com.fossil.ie4 r12 = (com.fossil.ie4) r12
                    java.lang.Object r12 = r0.L$0
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3 r12 = (com.portfolio.platform.data.source.SummariesRepository.getSummaries.Anon2.Anon1_Level2.Anon1_Level3) r12
                    com.fossil.t87.a(r13)     // Catch:{ Exception -> 0x0147 }
                    goto L_0x0167
                L_0x0040:
                    java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
                    java.lang.String r13 = "call to 'resume' before 'invoke' with coroutine"
                    r12.<init>(r13)
                    throw r12
                L_0x0048:
                    java.lang.Object r12 = r0.L$2
                    java.util.ArrayList r12 = (java.util.ArrayList) r12
                    java.lang.Object r2 = r0.L$1
                    com.fossil.ie4 r2 = (com.fossil.ie4) r2
                    java.lang.Object r5 = r0.L$0
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3 r5 = (com.portfolio.platform.data.source.SummariesRepository.getSummaries.Anon2.Anon1_Level2.Anon1_Level3) r5
                    com.fossil.t87.a(r13)
                    goto L_0x00ed
                L_0x0059:
                    com.fossil.t87.a(r13)
                    com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r7 = "getSummaries - saveCallResult -- item="
                    r2.append(r7)
                    r2.append(r12)
                    java.lang.String r2 = r2.toString()
                    r13.d(r6, r2)
                    java.util.ArrayList r13 = new java.util.ArrayList
                    r13.<init>()
                    com.fossil.be4 r2 = new com.fossil.be4
                    r2.<init>()
                    java.lang.Class<java.util.Date> r7 = java.util.Date.class
                    com.portfolio.platform.helper.GsonConvertDate r8 = new com.portfolio.platform.helper.GsonConvertDate
                    r8.<init>()
                    r2.a(r7, r8)
                    java.lang.Class<org.joda.time.DateTime> r7 = org.joda.time.DateTime.class
                    com.portfolio.platform.helper.GsonConvertDateTime r8 = new com.portfolio.platform.helper.GsonConvertDateTime
                    r8.<init>()
                    r2.a(r7, r8)
                    com.google.gson.Gson r2 = r2.a()
                    java.lang.String r7 = r12.toString()
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 r8 = new com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4
                    r8.<init>()
                    java.lang.reflect.Type r8 = r8.getType()
                    java.lang.Object r2 = r2.a(r7, r8)
                    com.portfolio.platform.data.source.remote.ApiResponse r2 = (com.portfolio.platform.data.source.remote.ApiResponse) r2
                    if (r2 == 0) goto L_0x00d0
                    java.util.List r2 = r2.get_items()
                    if (r2 == 0) goto L_0x00d0
                    java.util.Iterator r2 = r2.iterator()
                L_0x00b7:
                    boolean r7 = r2.hasNext()
                    if (r7 == 0) goto L_0x00d0
                    java.lang.Object r7 = r2.next()
                    com.portfolio.platform.data.model.FitnessDayData r7 = (com.portfolio.platform.data.model.FitnessDayData) r7
                    com.portfolio.platform.data.model.room.fitness.ActivitySummary r7 = r7.toActivitySummary()
                    java.lang.String r8 = "it.toActivitySummary()"
                    com.fossil.ee7.a(r7, r8)
                    r13.add(r7)
                    goto L_0x00b7
                L_0x00d0:
                    com.fossil.ti7 r2 = com.fossil.qj7.b()
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4 r7 = new com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4
                    r7.<init>(r11, r3)
                    r0.L$0 = r11
                    r0.L$1 = r12
                    r0.L$2 = r13
                    r0.label = r5
                    java.lang.Object r2 = com.fossil.vh7.a(r2, r7, r0)
                    if (r2 != r1) goto L_0x00e8
                    return r1
                L_0x00e8:
                    r5 = r11
                    r10 = r2
                    r2 = r12
                    r12 = r13
                    r13 = r10
                L_0x00ed:
                    java.util.List r13 = (java.util.List) r13
                    com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder
                    r8.<init>()
                    java.lang.String r9 = "fitnessDatasSize "
                    r8.append(r9)
                    int r9 = r13.size()
                    r8.append(r9)
                    java.lang.String r8 = r8.toString()
                    r7.d(r6, r8)
                    boolean r7 = r13.isEmpty()
                    if (r7 == 0) goto L_0x0167
                    com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder
                    r8.<init>()
                    java.lang.String r9 = "upsert 1 list "
                    r8.append(r9)
                    r8.append(r12)
                    java.lang.String r8 = r8.toString()
                    r7.d(r6, r8)
                    com.fossil.ti7 r7 = com.fossil.qj7.b()
                    com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon4_Level4 r8 = new com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon4_Level4
                    r8.<init>(r5, r12, r3)
                    r0.L$0 = r5
                    r0.L$1 = r2
                    r0.L$2 = r12
                    r0.L$3 = r13
                    r0.label = r4
                    java.lang.Object r12 = com.fossil.vh7.a(r7, r8, r0)
                    if (r12 != r1) goto L_0x0167
                    return r1
                L_0x0147:
                    r12 = move-exception
                    com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
                    java.lang.StringBuilder r0 = new java.lang.StringBuilder
                    r0.<init>()
                    java.lang.String r1 = "getSummaries - saveCallResult -- e="
                    r0.append(r1)
                    r12.printStackTrace()
                    com.fossil.i97 r12 = com.fossil.i97.a
                    r0.append(r12)
                    java.lang.String r12 = r0.toString()
                    r13.e(r6, r12)
                L_0x0167:
                    com.fossil.i97 r12 = com.fossil.i97.a
                    return r12
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.getSummaries.Anon2.Anon1_Level2.Anon1_Level3.saveCallResult(com.fossil.ie4, com.fossil.fb7):java.lang.Object");
            }

            @DexIgnore
            public boolean shouldFetch(List<ActivitySummary> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(SummariesRepository$getSummaries$Anon2 summariesRepository$getSummaries$Anon2, FitnessDatabase fitnessDatabase) {
            this.this$0 = summariesRepository$getSummaries$Anon2;
            this.$fitnessDatabase = fitnessDatabase;
        }

        @DexIgnore
        public final LiveData<qx6<List<ActivitySummary>>> apply(List<FitnessDataWrapper> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "getSummaries - startDate=" + this.this$0.$startDate + ", endDate=" + this.this$0.$endDate + " fitnessDataList=" + list.size());
            ee7.a((Object) list, "fitnessDataList");
            SummariesRepository$getSummaries$Anon2 summariesRepository$getSummaries$Anon2 = this.this$0;
            return new Anon1_Level3(this, FitnessDataWrapperKt.calculateRangeDownload(list, summariesRepository$getSummaries$Anon2.$startDate, summariesRepository$getSummaries$Anon2.$endDate)).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummaries$Anon2(SummariesRepository summariesRepository, Date date, Date date2, boolean z, fb7 fb7) {
        super(2, fb7);
        this.this$0 = summariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SummariesRepository$getSummaries$Anon2 summariesRepository$getSummaries$Anon2 = new SummariesRepository$getSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, this.$shouldFetch, fb7);
        summariesRepository$getSummaries$Anon2.p$ = (yi7) obj;
        return summariesRepository$getSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LiveData<qx6<? extends List<ActivitySummary>>>> fb7) {
        return ((SummariesRepository$getSummaries$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "getSummaries - startDate=" + this.$startDate + ", endDate=" + this.$endDate);
            ti7 b = qj7.b();
            SummariesRepository$getSummaries$Anon2$fitnessDatabase$Anon1_Level2 summariesRepository$getSummaries$Anon2$fitnessDatabase$Anon1_Level2 = new SummariesRepository$getSummaries$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = yi7;
            this.label = 1;
            obj = vh7.a(b, summariesRepository$getSummaries$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) obj;
        return ge.b(fitnessDatabase.getFitnessDataDao().getFitnessDataLiveData(this.$startDate, this.$endDate), new Anon1_Level2(this, fitnessDatabase));
    }
}
