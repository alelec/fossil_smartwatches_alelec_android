package com.portfolio.platform.data.source.local.inapp;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.zd7;
import com.portfolio.platform.data.InAppNotification;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InAppNotificationRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "InAppNotificationRepository";
    @DexIgnore
    public /* final */ InAppNotificationDao mInAppNotificationDao;
    @DexIgnore
    public /* final */ List<InAppNotification> mNotificationList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public InAppNotificationRepository(InAppNotificationDao inAppNotificationDao) {
        ee7.b(inAppNotificationDao, "mInAppNotificationDao");
        this.mInAppNotificationDao = inAppNotificationDao;
    }

    @DexIgnore
    public final Object delete(InAppNotification inAppNotification, fb7<? super i97> fb7) {
        this.mInAppNotificationDao.delete(inAppNotification);
        return i97.a;
    }

    @DexIgnore
    public final LiveData<List<InAppNotification>> getAllInAppNotifications() {
        return this.mInAppNotificationDao.getAllInAppNotification();
    }

    @DexIgnore
    public final void handleReceivingNotification(InAppNotification inAppNotification) {
        ee7.b(inAppNotification, "inAppNotification");
        this.mNotificationList.add(inAppNotification);
    }

    @DexIgnore
    public final Object insert(List<InAppNotification> list, fb7<? super i97> fb7) {
        this.mInAppNotificationDao.insertListInAppNotification(list);
        return i97.a;
    }

    @DexIgnore
    public final void removeNotificationAfterSending(InAppNotification inAppNotification) {
        ee7.b(inAppNotification, "inAppNotification");
        this.mNotificationList.remove(inAppNotification);
    }
}
