package com.portfolio.platform.data.source.local.sleep;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.bj5;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.ie4;
import com.fossil.ik7;
import com.fossil.of;
import com.fossil.pj4;
import com.fossil.qj7;
import com.fossil.r87;
import com.fossil.te5;
import com.fossil.ue5;
import com.fossil.w97;
import com.fossil.wearables.fsl.sleep.MFSleepDay;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.fossil.xh7;
import com.fossil.yi5;
import com.fossil.zd5;
import com.fossil.zd7;
import com.fossil.zh;
import com.fossil.zi5;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummaryLocalDataSource extends of<Date, SleepSummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "SleepSummaryLocalDataSource";
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ te5.a listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public te5 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ zh.c mObserver;
    @DexIgnore
    public List<r87<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public /* final */ SleepDatabase mSleepDatabase;
    @DexIgnore
    public /* final */ SleepSessionsRepository mSleepSessionsRepository;
    @DexIgnore
    public /* final */ SleepSummariesRepository mSleepSummariesRepository;
    @DexIgnore
    public Date mStartDate; // = new Date();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends zh.c {
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = sleepSummaryLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.zh.c
        public void onInvalidated(Set<String> set) {
            ee7.b(set, "tables");
            FLogger.INSTANCE.getLocal().d(SleepSummaryLocalDataSource.TAG, "XXX- sleep summary invalidate");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            ee7.b(date, "date");
            ee7.b(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(SleepSummaryLocalDataSource.TAG, "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar c = zd5.c(instance);
            if (zd5.b(date2, c.getTime())) {
                c.setTime(date2);
            }
            Date time = c.getTime();
            ee7.a((Object) time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public SleepSummaryLocalDataSource(SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDatabase sleepDatabase, Date date, pj4 pj4, te5.a aVar, Calendar calendar) {
        ee7.b(sleepSummariesRepository, "mSleepSummariesRepository");
        ee7.b(sleepSessionsRepository, "mSleepSessionsRepository");
        ee7.b(fitnessDataRepository, "mFitnessDataRepository");
        ee7.b(sleepDatabase, "mSleepDatabase");
        ee7.b(date, "mCreatedDate");
        ee7.b(pj4, "appExecutors");
        ee7.b(aVar, "listener");
        ee7.b(calendar, "key");
        this.mSleepSummariesRepository = sleepSummariesRepository;
        this.mSleepSessionsRepository = sleepSessionsRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mSleepDatabase = sleepDatabase;
        this.mCreatedDate = date;
        this.listener = aVar;
        this.key = calendar;
        te5 te5 = new te5(pj4.a());
        this.mHelper = te5;
        this.mNetworkState = ue5.a(te5);
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, MFSleepDay.TABLE_NAME, new String[]{MFSleepSession.TABLE_NAME});
        this.mSleepDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar c = zd5.c(instance);
        c.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + c.getTime());
        Date time = c.getTime();
        ee7.a((Object) time, "calendar.time");
        this.mStartDate = time;
        if (zd5.b(date, time)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final List<SleepSummary> calculateSummaries(List<SleepSummary> list) {
        int i;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "endCalendar");
            instance.setTime(((SleepSummary) ea7.f((List) list)).getDate());
            if (instance.get(7) != 1) {
                Calendar s = zd5.s(instance.getTime());
                ee7.a((Object) s, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                SleepDao sleepDao = this.mSleepDatabase.sleepDao();
                Date time = s.getTime();
                ee7.a((Object) time, "startCalendar.time");
                Date time2 = instance.getTime();
                ee7.a((Object) time2, "endCalendar.time");
                i = sleepDao.getTotalSleep(time, time2);
            } else {
                i = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            ee7.a((Object) instance2, "calendar");
            instance2.setTime(((SleepSummary) ea7.d((List) list)).getDate());
            Calendar s2 = zd5.s(instance2.getTime());
            ee7.a((Object) s2, "DateHelper.getStartOfWeek(calendar.time)");
            s2.add(5, -1);
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            double d = 0.0d;
            for (T t : list) {
                int i5 = i3 + 1;
                if (i3 >= 0) {
                    T t2 = t;
                    if (zd5.d(t2.getDate(), s2.getTime())) {
                        com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay = list.get(i2).getSleepDay();
                        if (sleepDay != null) {
                            if (i4 > 1) {
                                d /= (double) i4;
                            }
                            sleepDay.setAverageSleepOfWeek(Double.valueOf(d));
                        }
                        s2.add(5, -7);
                        i2 = i3;
                        i4 = 0;
                        d = 0.0d;
                    }
                    com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay2 = t2.getSleepDay();
                    d += (double) (sleepDay2 != null ? sleepDay2.getSleepMinutes() : 0);
                    com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay3 = t2.getSleepDay();
                    if ((sleepDay3 != null ? sleepDay3.getSleepMinutes() : 0) > 0) {
                        i4++;
                    }
                    if (i3 == list.size() - 1) {
                        d += (double) i;
                    }
                    i3 = i5;
                } else {
                    w97.c();
                    throw null;
                }
            }
            com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay4 = list.get(i2).getSleepDay();
            if (sleepDay4 != null) {
                if (i4 > 1) {
                    d /= (double) i4;
                }
                sleepDay4.setAverageSleepOfWeek(Double.valueOf(d));
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "calculateSummaries summaries.size=" + list.size());
        return list;
    }

    @DexIgnore
    private final void combineData(te5.d dVar, zi5<ie4> zi5, zi5<ie4> zi52, te5.b.a aVar) {
        if (!(zi5 instanceof bj5) || !(zi52 instanceof bj5)) {
            String str = "";
            if (zi5 instanceof yi5) {
                yi5 yi5 = (yi5) zi5;
                if (yi5.d() != null) {
                    aVar.a(yi5.d());
                } else if (yi5.c() != null) {
                    ServerError c = yi5.c();
                    String userMessage = c.getUserMessage();
                    if (userMessage == null) {
                        userMessage = c.getMessage();
                    }
                    if (userMessage != null) {
                        str = userMessage;
                    }
                    aVar.a(new Throwable(str));
                }
            } else if (zi52 instanceof yi5) {
                yi5 yi52 = (yi5) zi52;
                if (yi52.d() != null) {
                    aVar.a(yi52.d());
                } else if (yi52.c() != null) {
                    ServerError c2 = yi52.c();
                    String userMessage2 = c2.getUserMessage();
                    if (userMessage2 == null) {
                        userMessage2 = c2.getMessage();
                    }
                    if (userMessage2 != null) {
                        str = userMessage2;
                    }
                    aVar.a(new Throwable(str));
                }
            }
        } else {
            if (dVar == te5.d.AFTER && (!this.mRequestAfterQueue.isEmpty())) {
                this.mRequestAfterQueue.remove(0);
            }
            aVar.a();
        }
    }

    @DexIgnore
    private final SleepSummary dummySummary(SleepSummary sleepSummary, Date date) {
        com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay = sleepSummary.getSleepDay();
        return new SleepSummary(new com.portfolio.platform.data.model.room.sleep.MFSleepDay(date, sleepDay != null ? sleepDay.getGoalMinutes() : 0, 0, new SleepDistribution(0, 0, 0, 7, null), DateTime.now(), DateTime.now()), null, 2, null);
    }

    @DexIgnore
    private final List<SleepSummary> getDataInDatabase(Date date, Date date2) {
        Object obj;
        Object obj2;
        Date date3;
        Date date4 = date;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getDataInDatabase - startDate=" + date4 + ", endDate=" + date2);
        List<SleepSummary> calculateSummaries = calculateSummaries(this.mSleepDatabase.sleepDao().getSleepSummariesDesc(zd5.b(date, date2) ? date2 : date4, date2));
        ArrayList arrayList = new ArrayList();
        com.portfolio.platform.data.model.room.sleep.MFSleepDay lastSleepDay = this.mSleepDatabase.sleepDao().getLastSleepDay();
        Date date5 = (lastSleepDay == null || (date3 = lastSleepDay.getDate()) == null) ? date4 : date3;
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(calculateSummaries);
        SleepSummary sleepSummary = this.mSleepDatabase.sleepDao().getSleepSummary(date2);
        if (sleepSummary == null) {
            obj = null;
            sleepSummary = new SleepSummary(new com.portfolio.platform.data.model.room.sleep.MFSleepDay(date2, this.mSleepDatabase.sleepDao().getNearestSleepGoalFromDate(date2), 0, new SleepDistribution(0, 0, 0, 7, null), DateTime.now(), DateTime.now()), null, 2, null);
        } else {
            obj = null;
        }
        if (!zd5.b(date4, date5)) {
            date4 = date5;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "getDataInDatabase - summaries.size=" + calculateSummaries.size() + ", summaryParent=" + sleepSummary + ", " + "lastDate=" + date5 + ", startDateToFill=" + date4);
        Date date6 = date2;
        while (zd5.c(date6, date4)) {
            Iterator it = arrayList2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj2 = obj;
                    break;
                }
                obj2 = it.next();
                if (zd5.d(((SleepSummary) obj2).getDate(), date6)) {
                    break;
                }
            }
            SleepSummary sleepSummary2 = (SleepSummary) obj2;
            if (sleepSummary2 == null) {
                arrayList.add(dummySummary(sleepSummary, date6));
            } else {
                arrayList.add(sleepSummary2);
                arrayList2.remove(sleepSummary2);
            }
            date6 = zd5.p(date6);
            ee7.a((Object) date6, "DateHelper.getPrevDate(endDateToFill)");
        }
        if (!arrayList.isEmpty()) {
            SleepSummary sleepSummary3 = (SleepSummary) ea7.d((List) arrayList);
            Boolean w = zd5.w(sleepSummary3.getDate());
            ee7.a((Object) w, "DateHelper.isToday(todaySummary.getDate())");
            if (w.booleanValue()) {
                arrayList.add(0, sleepSummary3.copy(sleepSummary3.getSleepDay(), sleepSummary3.getSleepSessions()));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final ik7 loadData(te5.d dVar, Date date, Date date2, te5.b.a aVar) {
        return xh7.b(zi7.a(qj7.b()), null, null, new SleepSummaryLocalDataSource$loadData$Anon1(this, date, date2, dVar, aVar, null), 3, null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final te5 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    @Override // com.fossil.lf
    public boolean isInvalid() {
        this.mSleepDatabase.getInvalidationTracker().b();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadAfter(of.f<Date> fVar, of.a<Date, SleepSummary> aVar) {
        ee7.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Object) fVar.a));
        if (zd5.b(fVar.a, this.mCreatedDate)) {
            Key key2 = fVar.a;
            ee7.a((Object) key2, "params.key");
            Key key3 = key2;
            Companion companion = Companion;
            Key key4 = fVar.a;
            ee7.a((Object) key4, "params.key");
            Date calculateNextKey = companion.calculateNextKey(key4, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date o = zd5.d(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : zd5.o(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + o + ", endQueryDate=" + ((Object) key3));
            ee7.a((Object) o, "startQueryDate");
            aVar.a(getDataInDatabase(o, key3), this.key.getTime());
            if (zd5.b(this.mStartDate, (Date) key3)) {
                this.mEndDate = key3;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d(TAG, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new r87<>(this.mStartDate, this.mEndDate));
                this.mHelper.a(te5.d.AFTER, new SleepSummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadBefore(of.f<Date> fVar, of.a<Date, SleepSummary> aVar) {
        ee7.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadInitial(of.e<Date> eVar, of.c<Date, SleepSummary> cVar) {
        ee7.b(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date o = zd5.d(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : zd5.o(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + o + ", endQueryDate=" + date);
        ee7.a((Object) o, "startQueryDate");
        cVar.a(getDataInDatabase(o, date), null, this.key.getTime());
        this.mHelper.a(te5.d.INITIAL, new SleepSummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mSleepDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        ee7.b(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(te5 te5) {
        ee7.b(te5, "<set-?>");
        this.mHelper = te5;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        ee7.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        ee7.b(date, "<set-?>");
        this.mStartDate = date;
    }
}
