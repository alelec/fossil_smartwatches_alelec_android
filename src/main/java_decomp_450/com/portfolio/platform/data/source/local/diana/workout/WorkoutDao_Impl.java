package com.portfolio.platform.data.source.local.diana.workout;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.facebook.places.PlaceManager;
import com.fossil.ac5;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.mu4;
import com.fossil.mv4;
import com.fossil.oi;
import com.fossil.ou4;
import com.fossil.pi;
import com.fossil.ub5;
import com.fossil.vh;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.fossil.xb5;
import com.portfolio.platform.data.model.diana.workout.WorkoutCadence;
import com.portfolio.platform.data.model.diana.workout.WorkoutCalorie;
import com.portfolio.platform.data.model.diana.workout.WorkoutDistance;
import com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutPace;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.diana.workout.WorkoutStep;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDao_Impl implements WorkoutDao {
    @DexIgnore
    public /* final */ mu4 __dateShortStringConverter; // = new mu4();
    @DexIgnore
    public /* final */ ou4 __dateTimeISOStringConverter; // = new ou4();
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<WorkoutSession> __insertionAdapterOfWorkoutSession;
    @DexIgnore
    public /* final */ vh<WorkoutSession> __insertionAdapterOfWorkoutSession_1;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteAllWorkoutSession;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteWorkoutSessionById;
    @DexIgnore
    public /* final */ mv4 __workoutTypeConverter; // = new mv4();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<WorkoutSession> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR IGNORE INTO `workout_session` (`speed`,`screenShotUri`,`states`,`id`,`date`,`startTime`,`endTime`,`deviceSerialNumber`,`step`,`calorie`,`distance`,`heartRate`,`sourceType`,`workoutType`,`timezoneOffset`,`duration`,`createdAt`,`updatedAt`,`workoutGpsPoints`,`gpsDataPoints`,`mode`,`pace`,`cadence`,`editedStartTime`,`editedEndTime`,`editedType`,`editedMode`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, WorkoutSession workoutSession) {
            String a = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getSpeed());
            if (a == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, a);
            }
            if (workoutSession.getScreenShotUri() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, workoutSession.getScreenShotUri());
            }
            String b = WorkoutDao_Impl.this.__workoutTypeConverter.b(workoutSession.getStates());
            if (b == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, b);
            }
            if (workoutSession.getId() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, workoutSession.getId());
            }
            String a2 = WorkoutDao_Impl.this.__dateShortStringConverter.a(workoutSession.getDate());
            if (a2 == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a2);
            }
            String a3 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getStartTime());
            if (a3 == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, a3);
            }
            String a4 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getEndTime());
            if (a4 == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, a4);
            }
            if (workoutSession.getDeviceSerialNumber() == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, workoutSession.getDeviceSerialNumber());
            }
            String a5 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getStep());
            if (a5 == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindString(9, a5);
            }
            String a6 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getCalorie());
            if (a6 == null) {
                ajVar.bindNull(10);
            } else {
                ajVar.bindString(10, a6);
            }
            String a7 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getDistance());
            if (a7 == null) {
                ajVar.bindNull(11);
            } else {
                ajVar.bindString(11, a7);
            }
            String a8 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getHeartRate());
            if (a8 == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, a8);
            }
            String a9 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getSourceType());
            if (a9 == null) {
                ajVar.bindNull(13);
            } else {
                ajVar.bindString(13, a9);
            }
            String a10 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getWorkoutType());
            if (a10 == null) {
                ajVar.bindNull(14);
            } else {
                ajVar.bindString(14, a10);
            }
            ajVar.bindLong(15, (long) workoutSession.getTimezoneOffsetInSecond());
            ajVar.bindLong(16, (long) workoutSession.getDuration());
            ajVar.bindLong(17, workoutSession.getCreatedAt());
            ajVar.bindLong(18, workoutSession.getUpdatedAt());
            String a11 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getWorkoutGpsPoints());
            if (a11 == null) {
                ajVar.bindNull(19);
            } else {
                ajVar.bindString(19, a11);
            }
            if (workoutSession.getGpsDataPoints() == null) {
                ajVar.bindNull(20);
            } else {
                ajVar.bindString(20, workoutSession.getGpsDataPoints());
            }
            String a12 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getMode());
            if (a12 == null) {
                ajVar.bindNull(21);
            } else {
                ajVar.bindString(21, a12);
            }
            String a13 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getPace());
            if (a13 == null) {
                ajVar.bindNull(22);
            } else {
                ajVar.bindString(22, a13);
            }
            String a14 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getCadence());
            if (a14 == null) {
                ajVar.bindNull(23);
            } else {
                ajVar.bindString(23, a14);
            }
            String a15 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getEditedStartTime());
            if (a15 == null) {
                ajVar.bindNull(24);
            } else {
                ajVar.bindString(24, a15);
            }
            String a16 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getEditedEndTime());
            if (a16 == null) {
                ajVar.bindNull(25);
            } else {
                ajVar.bindString(25, a16);
            }
            String a17 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getEditedType());
            if (a17 == null) {
                ajVar.bindNull(26);
            } else {
                ajVar.bindString(26, a17);
            }
            String a18 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getEditedMode());
            if (a18 == null) {
                ajVar.bindNull(27);
            } else {
                ajVar.bindString(27, a18);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh<WorkoutSession> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `workout_session` (`speed`,`screenShotUri`,`states`,`id`,`date`,`startTime`,`endTime`,`deviceSerialNumber`,`step`,`calorie`,`distance`,`heartRate`,`sourceType`,`workoutType`,`timezoneOffset`,`duration`,`createdAt`,`updatedAt`,`workoutGpsPoints`,`gpsDataPoints`,`mode`,`pace`,`cadence`,`editedStartTime`,`editedEndTime`,`editedType`,`editedMode`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, WorkoutSession workoutSession) {
            String a = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getSpeed());
            if (a == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, a);
            }
            if (workoutSession.getScreenShotUri() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, workoutSession.getScreenShotUri());
            }
            String b = WorkoutDao_Impl.this.__workoutTypeConverter.b(workoutSession.getStates());
            if (b == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, b);
            }
            if (workoutSession.getId() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, workoutSession.getId());
            }
            String a2 = WorkoutDao_Impl.this.__dateShortStringConverter.a(workoutSession.getDate());
            if (a2 == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a2);
            }
            String a3 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getStartTime());
            if (a3 == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, a3);
            }
            String a4 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getEndTime());
            if (a4 == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, a4);
            }
            if (workoutSession.getDeviceSerialNumber() == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, workoutSession.getDeviceSerialNumber());
            }
            String a5 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getStep());
            if (a5 == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindString(9, a5);
            }
            String a6 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getCalorie());
            if (a6 == null) {
                ajVar.bindNull(10);
            } else {
                ajVar.bindString(10, a6);
            }
            String a7 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getDistance());
            if (a7 == null) {
                ajVar.bindNull(11);
            } else {
                ajVar.bindString(11, a7);
            }
            String a8 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getHeartRate());
            if (a8 == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, a8);
            }
            String a9 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getSourceType());
            if (a9 == null) {
                ajVar.bindNull(13);
            } else {
                ajVar.bindString(13, a9);
            }
            String a10 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getWorkoutType());
            if (a10 == null) {
                ajVar.bindNull(14);
            } else {
                ajVar.bindString(14, a10);
            }
            ajVar.bindLong(15, (long) workoutSession.getTimezoneOffsetInSecond());
            ajVar.bindLong(16, (long) workoutSession.getDuration());
            ajVar.bindLong(17, workoutSession.getCreatedAt());
            ajVar.bindLong(18, workoutSession.getUpdatedAt());
            String a11 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getWorkoutGpsPoints());
            if (a11 == null) {
                ajVar.bindNull(19);
            } else {
                ajVar.bindString(19, a11);
            }
            if (workoutSession.getGpsDataPoints() == null) {
                ajVar.bindNull(20);
            } else {
                ajVar.bindString(20, workoutSession.getGpsDataPoints());
            }
            String a12 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getMode());
            if (a12 == null) {
                ajVar.bindNull(21);
            } else {
                ajVar.bindString(21, a12);
            }
            String a13 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getPace());
            if (a13 == null) {
                ajVar.bindNull(22);
            } else {
                ajVar.bindString(22, a13);
            }
            String a14 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getCadence());
            if (a14 == null) {
                ajVar.bindNull(23);
            } else {
                ajVar.bindString(23, a14);
            }
            String a15 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getEditedStartTime());
            if (a15 == null) {
                ajVar.bindNull(24);
            } else {
                ajVar.bindString(24, a15);
            }
            String a16 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getEditedEndTime());
            if (a16 == null) {
                ajVar.bindNull(25);
            } else {
                ajVar.bindString(25, a16);
            }
            String a17 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getEditedType());
            if (a17 == null) {
                ajVar.bindNull(26);
            } else {
                ajVar.bindString(26, a17);
            }
            String a18 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getEditedMode());
            if (a18 == null) {
                ajVar.bindNull(27);
            } else {
                ajVar.bindString(27, a18);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM workout_session WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends ji {
        @DexIgnore
        public Anon4(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM workout_session";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 implements Callable<List<WorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon5(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WorkoutSession> call() throws Exception {
            Cursor a = pi.a(WorkoutDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, PlaceManager.PARAM_SPEED);
                int b2 = oi.b(a, "screenShotUri");
                int b3 = oi.b(a, "states");
                int b4 = oi.b(a, "id");
                int b5 = oi.b(a, "date");
                int b6 = oi.b(a, SampleRaw.COLUMN_START_TIME);
                int b7 = oi.b(a, SampleRaw.COLUMN_END_TIME);
                int b8 = oi.b(a, MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
                int b9 = oi.b(a, "step");
                int b10 = oi.b(a, "calorie");
                int b11 = oi.b(a, "distance");
                int b12 = oi.b(a, "heartRate");
                int b13 = oi.b(a, "sourceType");
                int b14 = oi.b(a, "workoutType");
                int i = b3;
                int b15 = oi.b(a, "timezoneOffset");
                int i2 = b2;
                int b16 = oi.b(a, "duration");
                int i3 = b;
                int b17 = oi.b(a, "createdAt");
                int b18 = oi.b(a, "updatedAt");
                int b19 = oi.b(a, "workoutGpsPoints");
                int b20 = oi.b(a, "gpsDataPoints");
                int b21 = oi.b(a, "mode");
                int b22 = oi.b(a, "pace");
                int b23 = oi.b(a, "cadence");
                int b24 = oi.b(a, MFSleepSession.COLUMN_EDITED_START_TIME);
                int b25 = oi.b(a, MFSleepSession.COLUMN_EDITED_END_TIME);
                int b26 = oi.b(a, "editedType");
                int b27 = oi.b(a, "editedMode");
                int i4 = b16;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    String string = a.getString(b4);
                    Date a2 = WorkoutDao_Impl.this.__dateShortStringConverter.a(a.getString(b5));
                    DateTime a3 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b6));
                    DateTime a4 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b7));
                    String string2 = a.getString(b8);
                    WorkoutStep k = WorkoutDao_Impl.this.__workoutTypeConverter.k(a.getString(b9));
                    WorkoutCalorie c = WorkoutDao_Impl.this.__workoutTypeConverter.c(a.getString(b10));
                    WorkoutDistance d = WorkoutDao_Impl.this.__workoutTypeConverter.d(a.getString(b11));
                    WorkoutHeartRate e = WorkoutDao_Impl.this.__workoutTypeConverter.e(a.getString(b12));
                    xb5 h = WorkoutDao_Impl.this.__workoutTypeConverter.h(a.getString(b13));
                    ac5 l = WorkoutDao_Impl.this.__workoutTypeConverter.l(a.getString(b14));
                    int i5 = a.getInt(b15);
                    int i6 = a.getInt(i4);
                    long j = a.getLong(b17);
                    i4 = i4;
                    long j2 = a.getLong(b18);
                    b18 = b18;
                    String string3 = a.getString(b19);
                    b19 = b19;
                    List<WorkoutGpsPoint> a5 = WorkoutDao_Impl.this.__workoutTypeConverter.a(string3);
                    String string4 = a.getString(b20);
                    b20 = b20;
                    String string5 = a.getString(b21);
                    b21 = b21;
                    ub5 f = WorkoutDao_Impl.this.__workoutTypeConverter.f(string5);
                    String string6 = a.getString(b22);
                    b22 = b22;
                    WorkoutPace g = WorkoutDao_Impl.this.__workoutTypeConverter.g(string6);
                    String string7 = a.getString(b23);
                    b23 = b23;
                    WorkoutCadence b28 = WorkoutDao_Impl.this.__workoutTypeConverter.b(string7);
                    String string8 = a.getString(b24);
                    b24 = b24;
                    DateTime a6 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(string8);
                    String string9 = a.getString(b25);
                    b25 = b25;
                    DateTime a7 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(string9);
                    String string10 = a.getString(b26);
                    b26 = b26;
                    ac5 l2 = WorkoutDao_Impl.this.__workoutTypeConverter.l(string10);
                    String string11 = a.getString(b27);
                    b27 = b27;
                    WorkoutSession workoutSession = new WorkoutSession(string, a2, a3, a4, string2, k, c, d, e, h, l, i5, i6, j, j2, a5, string4, f, g, b28, a6, a7, l2, WorkoutDao_Impl.this.__workoutTypeConverter.f(string11));
                    workoutSession.setSpeed(WorkoutDao_Impl.this.__workoutTypeConverter.i(a.getString(i3)));
                    workoutSession.setScreenShotUri(a.getString(i2));
                    i2 = i2;
                    String string12 = a.getString(i);
                    i = i;
                    workoutSession.setStates(WorkoutDao_Impl.this.__workoutTypeConverter.j(string12));
                    arrayList.add(workoutSession);
                    b15 = b15;
                    b4 = b4;
                    b17 = b17;
                    i3 = i3;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 implements Callable<List<WorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon6(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WorkoutSession> call() throws Exception {
            Cursor a = pi.a(WorkoutDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, PlaceManager.PARAM_SPEED);
                int b2 = oi.b(a, "screenShotUri");
                int b3 = oi.b(a, "states");
                int b4 = oi.b(a, "id");
                int b5 = oi.b(a, "date");
                int b6 = oi.b(a, SampleRaw.COLUMN_START_TIME);
                int b7 = oi.b(a, SampleRaw.COLUMN_END_TIME);
                int b8 = oi.b(a, MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
                int b9 = oi.b(a, "step");
                int b10 = oi.b(a, "calorie");
                int b11 = oi.b(a, "distance");
                int b12 = oi.b(a, "heartRate");
                int b13 = oi.b(a, "sourceType");
                int b14 = oi.b(a, "workoutType");
                int i = b3;
                int b15 = oi.b(a, "timezoneOffset");
                int i2 = b2;
                int b16 = oi.b(a, "duration");
                int i3 = b;
                int b17 = oi.b(a, "createdAt");
                int b18 = oi.b(a, "updatedAt");
                int b19 = oi.b(a, "workoutGpsPoints");
                int b20 = oi.b(a, "gpsDataPoints");
                int b21 = oi.b(a, "mode");
                int b22 = oi.b(a, "pace");
                int b23 = oi.b(a, "cadence");
                int b24 = oi.b(a, MFSleepSession.COLUMN_EDITED_START_TIME);
                int b25 = oi.b(a, MFSleepSession.COLUMN_EDITED_END_TIME);
                int b26 = oi.b(a, "editedType");
                int b27 = oi.b(a, "editedMode");
                int i4 = b16;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    String string = a.getString(b4);
                    Date a2 = WorkoutDao_Impl.this.__dateShortStringConverter.a(a.getString(b5));
                    DateTime a3 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b6));
                    DateTime a4 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b7));
                    String string2 = a.getString(b8);
                    WorkoutStep k = WorkoutDao_Impl.this.__workoutTypeConverter.k(a.getString(b9));
                    WorkoutCalorie c = WorkoutDao_Impl.this.__workoutTypeConverter.c(a.getString(b10));
                    WorkoutDistance d = WorkoutDao_Impl.this.__workoutTypeConverter.d(a.getString(b11));
                    WorkoutHeartRate e = WorkoutDao_Impl.this.__workoutTypeConverter.e(a.getString(b12));
                    xb5 h = WorkoutDao_Impl.this.__workoutTypeConverter.h(a.getString(b13));
                    ac5 l = WorkoutDao_Impl.this.__workoutTypeConverter.l(a.getString(b14));
                    int i5 = a.getInt(b15);
                    int i6 = a.getInt(i4);
                    long j = a.getLong(b17);
                    i4 = i4;
                    long j2 = a.getLong(b18);
                    b18 = b18;
                    String string3 = a.getString(b19);
                    b19 = b19;
                    List<WorkoutGpsPoint> a5 = WorkoutDao_Impl.this.__workoutTypeConverter.a(string3);
                    String string4 = a.getString(b20);
                    b20 = b20;
                    String string5 = a.getString(b21);
                    b21 = b21;
                    ub5 f = WorkoutDao_Impl.this.__workoutTypeConverter.f(string5);
                    String string6 = a.getString(b22);
                    b22 = b22;
                    WorkoutPace g = WorkoutDao_Impl.this.__workoutTypeConverter.g(string6);
                    String string7 = a.getString(b23);
                    b23 = b23;
                    WorkoutCadence b28 = WorkoutDao_Impl.this.__workoutTypeConverter.b(string7);
                    String string8 = a.getString(b24);
                    b24 = b24;
                    DateTime a6 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(string8);
                    String string9 = a.getString(b25);
                    b25 = b25;
                    DateTime a7 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(string9);
                    String string10 = a.getString(b26);
                    b26 = b26;
                    ac5 l2 = WorkoutDao_Impl.this.__workoutTypeConverter.l(string10);
                    String string11 = a.getString(b27);
                    b27 = b27;
                    WorkoutSession workoutSession = new WorkoutSession(string, a2, a3, a4, string2, k, c, d, e, h, l, i5, i6, j, j2, a5, string4, f, g, b28, a6, a7, l2, WorkoutDao_Impl.this.__workoutTypeConverter.f(string11));
                    workoutSession.setSpeed(WorkoutDao_Impl.this.__workoutTypeConverter.i(a.getString(i3)));
                    workoutSession.setScreenShotUri(a.getString(i2));
                    i2 = i2;
                    String string12 = a.getString(i);
                    i = i;
                    workoutSession.setStates(WorkoutDao_Impl.this.__workoutTypeConverter.j(string12));
                    arrayList.add(workoutSession);
                    b15 = b15;
                    b4 = b4;
                    b17 = b17;
                    i3 = i3;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public WorkoutDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfWorkoutSession = new Anon1(ciVar);
        this.__insertionAdapterOfWorkoutSession_1 = new Anon2(ciVar);
        this.__preparedStmtOfDeleteWorkoutSessionById = new Anon3(ciVar);
        this.__preparedStmtOfDeleteAllWorkoutSession = new Anon4(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public void deleteAllWorkoutSession() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteAllWorkoutSession.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllWorkoutSession.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public void deleteWorkoutSessionById(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteWorkoutSessionById.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWorkoutSessionById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public WorkoutSession getWorkoutSessionById(String str) {
        fi fiVar;
        WorkoutSession workoutSession;
        fi b = fi.b("SELECT * FROM workout_session WHERE id = ?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, PlaceManager.PARAM_SPEED);
            int b3 = oi.b(a, "screenShotUri");
            int b4 = oi.b(a, "states");
            int b5 = oi.b(a, "id");
            int b6 = oi.b(a, "date");
            int b7 = oi.b(a, SampleRaw.COLUMN_START_TIME);
            int b8 = oi.b(a, SampleRaw.COLUMN_END_TIME);
            int b9 = oi.b(a, MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b10 = oi.b(a, "step");
            int b11 = oi.b(a, "calorie");
            int b12 = oi.b(a, "distance");
            int b13 = oi.b(a, "heartRate");
            int b14 = oi.b(a, "sourceType");
            fiVar = b;
            try {
                int b15 = oi.b(a, "workoutType");
                int b16 = oi.b(a, "timezoneOffset");
                int b17 = oi.b(a, "duration");
                int b18 = oi.b(a, "createdAt");
                int b19 = oi.b(a, "updatedAt");
                int b20 = oi.b(a, "workoutGpsPoints");
                int b21 = oi.b(a, "gpsDataPoints");
                int b22 = oi.b(a, "mode");
                int b23 = oi.b(a, "pace");
                int b24 = oi.b(a, "cadence");
                int b25 = oi.b(a, MFSleepSession.COLUMN_EDITED_START_TIME);
                int b26 = oi.b(a, MFSleepSession.COLUMN_EDITED_END_TIME);
                int b27 = oi.b(a, "editedType");
                int b28 = oi.b(a, "editedMode");
                if (a.moveToFirst()) {
                    workoutSession = new WorkoutSession(a.getString(b5), this.__dateShortStringConverter.a(a.getString(b6)), this.__dateTimeISOStringConverter.a(a.getString(b7)), this.__dateTimeISOStringConverter.a(a.getString(b8)), a.getString(b9), this.__workoutTypeConverter.k(a.getString(b10)), this.__workoutTypeConverter.c(a.getString(b11)), this.__workoutTypeConverter.d(a.getString(b12)), this.__workoutTypeConverter.e(a.getString(b13)), this.__workoutTypeConverter.h(a.getString(b14)), this.__workoutTypeConverter.l(a.getString(b15)), a.getInt(b16), a.getInt(b17), a.getLong(b18), a.getLong(b19), this.__workoutTypeConverter.a(a.getString(b20)), a.getString(b21), this.__workoutTypeConverter.f(a.getString(b22)), this.__workoutTypeConverter.g(a.getString(b23)), this.__workoutTypeConverter.b(a.getString(b24)), this.__dateTimeISOStringConverter.a(a.getString(b25)), this.__dateTimeISOStringConverter.a(a.getString(b26)), this.__workoutTypeConverter.l(a.getString(b27)), this.__workoutTypeConverter.f(a.getString(b28)));
                    workoutSession.setSpeed(this.__workoutTypeConverter.i(a.getString(b2)));
                    workoutSession.setScreenShotUri(a.getString(b3));
                    workoutSession.setStates(this.__workoutTypeConverter.j(a.getString(b4)));
                } else {
                    workoutSession = null;
                }
                a.close();
                fiVar.c();
                return workoutSession;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public LiveData<List<WorkoutSession>> getWorkoutSessions(Date date, Date date2) {
        fi b = fi.b("SELECT * FROM workout_session WHERE date >= ? AND date <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"workout_session"}, false, (Callable) new Anon5(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public LiveData<List<WorkoutSession>> getWorkoutSessionsDesc(Date date, Date date2) {
        fi b = fi.b("SELECT * FROM workout_session WHERE date >= ? AND date <= ? ORDER BY startTime DESC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"workout_session"}, false, (Callable) new Anon6(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public List<WorkoutSession> getWorkoutSessionsInDate(Date date) {
        fi fiVar;
        fi b = fi.b("SELECT * FROM workout_session WHERE date = ? ORDER BY startTime ASC", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a2, PlaceManager.PARAM_SPEED);
            int b3 = oi.b(a2, "screenShotUri");
            int b4 = oi.b(a2, "states");
            int b5 = oi.b(a2, "id");
            int b6 = oi.b(a2, "date");
            int b7 = oi.b(a2, SampleRaw.COLUMN_START_TIME);
            int b8 = oi.b(a2, SampleRaw.COLUMN_END_TIME);
            int b9 = oi.b(a2, MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b10 = oi.b(a2, "step");
            int b11 = oi.b(a2, "calorie");
            int b12 = oi.b(a2, "distance");
            int b13 = oi.b(a2, "heartRate");
            int b14 = oi.b(a2, "sourceType");
            fiVar = b;
            try {
                int b15 = oi.b(a2, "workoutType");
                int i = b4;
                int b16 = oi.b(a2, "timezoneOffset");
                int i2 = b3;
                int b17 = oi.b(a2, "duration");
                int i3 = b2;
                int b18 = oi.b(a2, "createdAt");
                int b19 = oi.b(a2, "updatedAt");
                int b20 = oi.b(a2, "workoutGpsPoints");
                int b21 = oi.b(a2, "gpsDataPoints");
                int b22 = oi.b(a2, "mode");
                int b23 = oi.b(a2, "pace");
                int b24 = oi.b(a2, "cadence");
                int b25 = oi.b(a2, MFSleepSession.COLUMN_EDITED_START_TIME);
                int b26 = oi.b(a2, MFSleepSession.COLUMN_EDITED_END_TIME);
                int b27 = oi.b(a2, "editedType");
                int b28 = oi.b(a2, "editedMode");
                int i4 = b17;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b5);
                    Date a3 = this.__dateShortStringConverter.a(a2.getString(b6));
                    DateTime a4 = this.__dateTimeISOStringConverter.a(a2.getString(b7));
                    DateTime a5 = this.__dateTimeISOStringConverter.a(a2.getString(b8));
                    String string2 = a2.getString(b9);
                    WorkoutStep k = this.__workoutTypeConverter.k(a2.getString(b10));
                    WorkoutCalorie c = this.__workoutTypeConverter.c(a2.getString(b11));
                    WorkoutDistance d = this.__workoutTypeConverter.d(a2.getString(b12));
                    WorkoutHeartRate e = this.__workoutTypeConverter.e(a2.getString(b13));
                    xb5 h = this.__workoutTypeConverter.h(a2.getString(b14));
                    ac5 l = this.__workoutTypeConverter.l(a2.getString(b15));
                    int i5 = a2.getInt(b16);
                    int i6 = a2.getInt(i4);
                    long j = a2.getLong(b18);
                    long j2 = a2.getLong(b19);
                    b19 = b19;
                    i4 = i4;
                    String string3 = a2.getString(b20);
                    b20 = b20;
                    List<WorkoutGpsPoint> a6 = this.__workoutTypeConverter.a(string3);
                    String string4 = a2.getString(b21);
                    b21 = b21;
                    String string5 = a2.getString(b22);
                    b22 = b22;
                    ub5 f = this.__workoutTypeConverter.f(string5);
                    String string6 = a2.getString(b23);
                    b23 = b23;
                    WorkoutPace g = this.__workoutTypeConverter.g(string6);
                    String string7 = a2.getString(b24);
                    b24 = b24;
                    WorkoutCadence b29 = this.__workoutTypeConverter.b(string7);
                    String string8 = a2.getString(b25);
                    b25 = b25;
                    DateTime a7 = this.__dateTimeISOStringConverter.a(string8);
                    String string9 = a2.getString(b26);
                    b26 = b26;
                    DateTime a8 = this.__dateTimeISOStringConverter.a(string9);
                    String string10 = a2.getString(b27);
                    b27 = b27;
                    ac5 l2 = this.__workoutTypeConverter.l(string10);
                    String string11 = a2.getString(b28);
                    b28 = b28;
                    WorkoutSession workoutSession = new WorkoutSession(string, a3, a4, a5, string2, k, c, d, e, h, l, i5, i6, j, j2, a6, string4, f, g, b29, a7, a8, l2, this.__workoutTypeConverter.f(string11));
                    workoutSession.setSpeed(this.__workoutTypeConverter.i(a2.getString(i3)));
                    workoutSession.setScreenShotUri(a2.getString(i2));
                    i2 = i2;
                    String string12 = a2.getString(i);
                    i = i;
                    workoutSession.setStates(this.__workoutTypeConverter.j(string12));
                    arrayList.add(workoutSession);
                    b16 = b16;
                    b15 = b15;
                    i3 = i3;
                    b18 = b18;
                    b5 = b5;
                }
                a2.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a2.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public List<WorkoutSession> getWorkoutSessionsInDateAfterDesc(Date date, long j, int i) {
        fi fiVar;
        fi b = fi.b("SELECT * FROM workout_session WHERE date = ? AND createdAt < ? ORDER BY startTime DESC limit ?", 3);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        b.bindLong(2, j);
        b.bindLong(3, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a2, PlaceManager.PARAM_SPEED);
            int b3 = oi.b(a2, "screenShotUri");
            int b4 = oi.b(a2, "states");
            int b5 = oi.b(a2, "id");
            int b6 = oi.b(a2, "date");
            int b7 = oi.b(a2, SampleRaw.COLUMN_START_TIME);
            int b8 = oi.b(a2, SampleRaw.COLUMN_END_TIME);
            int b9 = oi.b(a2, MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b10 = oi.b(a2, "step");
            int b11 = oi.b(a2, "calorie");
            int b12 = oi.b(a2, "distance");
            int b13 = oi.b(a2, "heartRate");
            int b14 = oi.b(a2, "sourceType");
            fiVar = b;
            try {
                int b15 = oi.b(a2, "workoutType");
                int i2 = b4;
                int b16 = oi.b(a2, "timezoneOffset");
                int i3 = b3;
                int b17 = oi.b(a2, "duration");
                int i4 = b2;
                int b18 = oi.b(a2, "createdAt");
                int b19 = oi.b(a2, "updatedAt");
                int b20 = oi.b(a2, "workoutGpsPoints");
                int b21 = oi.b(a2, "gpsDataPoints");
                int b22 = oi.b(a2, "mode");
                int b23 = oi.b(a2, "pace");
                int b24 = oi.b(a2, "cadence");
                int b25 = oi.b(a2, MFSleepSession.COLUMN_EDITED_START_TIME);
                int b26 = oi.b(a2, MFSleepSession.COLUMN_EDITED_END_TIME);
                int b27 = oi.b(a2, "editedType");
                int b28 = oi.b(a2, "editedMode");
                int i5 = b17;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b5);
                    Date a3 = this.__dateShortStringConverter.a(a2.getString(b6));
                    DateTime a4 = this.__dateTimeISOStringConverter.a(a2.getString(b7));
                    DateTime a5 = this.__dateTimeISOStringConverter.a(a2.getString(b8));
                    String string2 = a2.getString(b9);
                    WorkoutStep k = this.__workoutTypeConverter.k(a2.getString(b10));
                    WorkoutCalorie c = this.__workoutTypeConverter.c(a2.getString(b11));
                    WorkoutDistance d = this.__workoutTypeConverter.d(a2.getString(b12));
                    WorkoutHeartRate e = this.__workoutTypeConverter.e(a2.getString(b13));
                    xb5 h = this.__workoutTypeConverter.h(a2.getString(b14));
                    ac5 l = this.__workoutTypeConverter.l(a2.getString(b15));
                    int i6 = a2.getInt(b16);
                    int i7 = a2.getInt(i5);
                    long j2 = a2.getLong(b18);
                    long j3 = a2.getLong(b19);
                    b19 = b19;
                    i5 = i5;
                    String string3 = a2.getString(b20);
                    b20 = b20;
                    List<WorkoutGpsPoint> a6 = this.__workoutTypeConverter.a(string3);
                    String string4 = a2.getString(b21);
                    b21 = b21;
                    String string5 = a2.getString(b22);
                    b22 = b22;
                    ub5 f = this.__workoutTypeConverter.f(string5);
                    String string6 = a2.getString(b23);
                    b23 = b23;
                    WorkoutPace g = this.__workoutTypeConverter.g(string6);
                    String string7 = a2.getString(b24);
                    b24 = b24;
                    WorkoutCadence b29 = this.__workoutTypeConverter.b(string7);
                    String string8 = a2.getString(b25);
                    b25 = b25;
                    DateTime a7 = this.__dateTimeISOStringConverter.a(string8);
                    String string9 = a2.getString(b26);
                    b26 = b26;
                    DateTime a8 = this.__dateTimeISOStringConverter.a(string9);
                    String string10 = a2.getString(b27);
                    b27 = b27;
                    ac5 l2 = this.__workoutTypeConverter.l(string10);
                    String string11 = a2.getString(b28);
                    b28 = b28;
                    WorkoutSession workoutSession = new WorkoutSession(string, a3, a4, a5, string2, k, c, d, e, h, l, i6, i7, j2, j3, a6, string4, f, g, b29, a7, a8, l2, this.__workoutTypeConverter.f(string11));
                    workoutSession.setSpeed(this.__workoutTypeConverter.i(a2.getString(i4)));
                    workoutSession.setScreenShotUri(a2.getString(i3));
                    i3 = i3;
                    String string12 = a2.getString(i2);
                    i2 = i2;
                    workoutSession.setStates(this.__workoutTypeConverter.j(string12));
                    arrayList.add(workoutSession);
                    b16 = b16;
                    b15 = b15;
                    b18 = b18;
                    i4 = i4;
                    b5 = b5;
                }
                a2.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a2.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public List<WorkoutSession> getWorkoutSessionsInDateDesc(Date date, int i) {
        fi fiVar;
        fi b = fi.b("SELECT * FROM workout_session WHERE date = ? ORDER BY startTime DESC limit ?", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        b.bindLong(2, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a2, PlaceManager.PARAM_SPEED);
            int b3 = oi.b(a2, "screenShotUri");
            int b4 = oi.b(a2, "states");
            int b5 = oi.b(a2, "id");
            int b6 = oi.b(a2, "date");
            int b7 = oi.b(a2, SampleRaw.COLUMN_START_TIME);
            int b8 = oi.b(a2, SampleRaw.COLUMN_END_TIME);
            int b9 = oi.b(a2, MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b10 = oi.b(a2, "step");
            int b11 = oi.b(a2, "calorie");
            int b12 = oi.b(a2, "distance");
            int b13 = oi.b(a2, "heartRate");
            int b14 = oi.b(a2, "sourceType");
            fiVar = b;
            try {
                int b15 = oi.b(a2, "workoutType");
                int i2 = b4;
                int b16 = oi.b(a2, "timezoneOffset");
                int i3 = b3;
                int b17 = oi.b(a2, "duration");
                int i4 = b2;
                int b18 = oi.b(a2, "createdAt");
                int b19 = oi.b(a2, "updatedAt");
                int b20 = oi.b(a2, "workoutGpsPoints");
                int b21 = oi.b(a2, "gpsDataPoints");
                int b22 = oi.b(a2, "mode");
                int b23 = oi.b(a2, "pace");
                int b24 = oi.b(a2, "cadence");
                int b25 = oi.b(a2, MFSleepSession.COLUMN_EDITED_START_TIME);
                int b26 = oi.b(a2, MFSleepSession.COLUMN_EDITED_END_TIME);
                int b27 = oi.b(a2, "editedType");
                int b28 = oi.b(a2, "editedMode");
                int i5 = b17;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b5);
                    Date a3 = this.__dateShortStringConverter.a(a2.getString(b6));
                    DateTime a4 = this.__dateTimeISOStringConverter.a(a2.getString(b7));
                    DateTime a5 = this.__dateTimeISOStringConverter.a(a2.getString(b8));
                    String string2 = a2.getString(b9);
                    WorkoutStep k = this.__workoutTypeConverter.k(a2.getString(b10));
                    WorkoutCalorie c = this.__workoutTypeConverter.c(a2.getString(b11));
                    WorkoutDistance d = this.__workoutTypeConverter.d(a2.getString(b12));
                    WorkoutHeartRate e = this.__workoutTypeConverter.e(a2.getString(b13));
                    xb5 h = this.__workoutTypeConverter.h(a2.getString(b14));
                    ac5 l = this.__workoutTypeConverter.l(a2.getString(b15));
                    int i6 = a2.getInt(b16);
                    int i7 = a2.getInt(i5);
                    long j = a2.getLong(b18);
                    long j2 = a2.getLong(b19);
                    b19 = b19;
                    i5 = i5;
                    String string3 = a2.getString(b20);
                    b20 = b20;
                    List<WorkoutGpsPoint> a6 = this.__workoutTypeConverter.a(string3);
                    String string4 = a2.getString(b21);
                    b21 = b21;
                    String string5 = a2.getString(b22);
                    b22 = b22;
                    ub5 f = this.__workoutTypeConverter.f(string5);
                    String string6 = a2.getString(b23);
                    b23 = b23;
                    WorkoutPace g = this.__workoutTypeConverter.g(string6);
                    String string7 = a2.getString(b24);
                    b24 = b24;
                    WorkoutCadence b29 = this.__workoutTypeConverter.b(string7);
                    String string8 = a2.getString(b25);
                    b25 = b25;
                    DateTime a7 = this.__dateTimeISOStringConverter.a(string8);
                    String string9 = a2.getString(b26);
                    b26 = b26;
                    DateTime a8 = this.__dateTimeISOStringConverter.a(string9);
                    String string10 = a2.getString(b27);
                    b27 = b27;
                    ac5 l2 = this.__workoutTypeConverter.l(string10);
                    String string11 = a2.getString(b28);
                    b28 = b28;
                    WorkoutSession workoutSession = new WorkoutSession(string, a3, a4, a5, string2, k, c, d, e, h, l, i6, i7, j, j2, a6, string4, f, g, b29, a7, a8, l2, this.__workoutTypeConverter.f(string11));
                    workoutSession.setSpeed(this.__workoutTypeConverter.i(a2.getString(i4)));
                    workoutSession.setScreenShotUri(a2.getString(i3));
                    i3 = i3;
                    String string12 = a2.getString(i2);
                    i2 = i2;
                    workoutSession.setStates(this.__workoutTypeConverter.j(string12));
                    arrayList.add(workoutSession);
                    b16 = b16;
                    b15 = b15;
                    i4 = i4;
                    b18 = b18;
                    b5 = b5;
                }
                a2.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a2.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public List<WorkoutSession> getWorkoutSessionsRaw(Date date, Date date2) {
        fi fiVar;
        fi b = fi.b("SELECT * FROM workout_session WHERE date >= ? AND date <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a3, PlaceManager.PARAM_SPEED);
            int b3 = oi.b(a3, "screenShotUri");
            int b4 = oi.b(a3, "states");
            int b5 = oi.b(a3, "id");
            int b6 = oi.b(a3, "date");
            int b7 = oi.b(a3, SampleRaw.COLUMN_START_TIME);
            int b8 = oi.b(a3, SampleRaw.COLUMN_END_TIME);
            int b9 = oi.b(a3, MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b10 = oi.b(a3, "step");
            int b11 = oi.b(a3, "calorie");
            int b12 = oi.b(a3, "distance");
            int b13 = oi.b(a3, "heartRate");
            int b14 = oi.b(a3, "sourceType");
            fiVar = b;
            try {
                int b15 = oi.b(a3, "workoutType");
                int i = b4;
                int b16 = oi.b(a3, "timezoneOffset");
                int i2 = b3;
                int b17 = oi.b(a3, "duration");
                int i3 = b2;
                int b18 = oi.b(a3, "createdAt");
                int b19 = oi.b(a3, "updatedAt");
                int b20 = oi.b(a3, "workoutGpsPoints");
                int b21 = oi.b(a3, "gpsDataPoints");
                int b22 = oi.b(a3, "mode");
                int b23 = oi.b(a3, "pace");
                int b24 = oi.b(a3, "cadence");
                int b25 = oi.b(a3, MFSleepSession.COLUMN_EDITED_START_TIME);
                int b26 = oi.b(a3, MFSleepSession.COLUMN_EDITED_END_TIME);
                int b27 = oi.b(a3, "editedType");
                int b28 = oi.b(a3, "editedMode");
                int i4 = b17;
                ArrayList arrayList = new ArrayList(a3.getCount());
                while (a3.moveToNext()) {
                    String string = a3.getString(b5);
                    Date a4 = this.__dateShortStringConverter.a(a3.getString(b6));
                    DateTime a5 = this.__dateTimeISOStringConverter.a(a3.getString(b7));
                    DateTime a6 = this.__dateTimeISOStringConverter.a(a3.getString(b8));
                    String string2 = a3.getString(b9);
                    WorkoutStep k = this.__workoutTypeConverter.k(a3.getString(b10));
                    WorkoutCalorie c = this.__workoutTypeConverter.c(a3.getString(b11));
                    WorkoutDistance d = this.__workoutTypeConverter.d(a3.getString(b12));
                    WorkoutHeartRate e = this.__workoutTypeConverter.e(a3.getString(b13));
                    xb5 h = this.__workoutTypeConverter.h(a3.getString(b14));
                    ac5 l = this.__workoutTypeConverter.l(a3.getString(b15));
                    int i5 = a3.getInt(b16);
                    int i6 = a3.getInt(i4);
                    long j = a3.getLong(b18);
                    long j2 = a3.getLong(b19);
                    b19 = b19;
                    i4 = i4;
                    String string3 = a3.getString(b20);
                    b20 = b20;
                    List<WorkoutGpsPoint> a7 = this.__workoutTypeConverter.a(string3);
                    String string4 = a3.getString(b21);
                    b21 = b21;
                    String string5 = a3.getString(b22);
                    b22 = b22;
                    ub5 f = this.__workoutTypeConverter.f(string5);
                    String string6 = a3.getString(b23);
                    b23 = b23;
                    WorkoutPace g = this.__workoutTypeConverter.g(string6);
                    String string7 = a3.getString(b24);
                    b24 = b24;
                    WorkoutCadence b29 = this.__workoutTypeConverter.b(string7);
                    String string8 = a3.getString(b25);
                    b25 = b25;
                    DateTime a8 = this.__dateTimeISOStringConverter.a(string8);
                    String string9 = a3.getString(b26);
                    b26 = b26;
                    DateTime a9 = this.__dateTimeISOStringConverter.a(string9);
                    String string10 = a3.getString(b27);
                    b27 = b27;
                    ac5 l2 = this.__workoutTypeConverter.l(string10);
                    String string11 = a3.getString(b28);
                    b28 = b28;
                    WorkoutSession workoutSession = new WorkoutSession(string, a4, a5, a6, string2, k, c, d, e, h, l, i5, i6, j, j2, a7, string4, f, g, b29, a8, a9, l2, this.__workoutTypeConverter.f(string11));
                    workoutSession.setSpeed(this.__workoutTypeConverter.i(a3.getString(i3)));
                    workoutSession.setScreenShotUri(a3.getString(i2));
                    i2 = i2;
                    String string12 = a3.getString(i);
                    i = i;
                    workoutSession.setStates(this.__workoutTypeConverter.j(string12));
                    arrayList.add(workoutSession);
                    b16 = b16;
                    b15 = b15;
                    i3 = i3;
                    b18 = b18;
                    b5 = b5;
                }
                a3.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a3.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a3.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public void insertWorkoutSession(WorkoutSession workoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkoutSession.insert(workoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public void upsertListWorkoutSession(List<WorkoutSession> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkoutSession_1.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.workout.WorkoutDao
    public void upsertWorkoutSession(WorkoutSession workoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkoutSession_1.insert(workoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
