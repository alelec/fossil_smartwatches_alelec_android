package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.SleepStatistic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatisticAwait$2", f = "SleepSummariesRepository.kt", l = {375, 379, 382}, m = "invokeSuspend")
public final class SleepSummariesRepository$getSleepStatisticAwait$Anon2 extends zb7 implements kd7<yi7, fb7<? super SleepStatistic>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepStatisticAwait$Anon2(SleepSummariesRepository sleepSummariesRepository, fb7 fb7) {
        super(2, fb7);
        this.this$0 = sleepSummariesRepository;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSummariesRepository$getSleepStatisticAwait$Anon2 sleepSummariesRepository$getSleepStatisticAwait$Anon2 = new SleepSummariesRepository$getSleepStatisticAwait$Anon2(this.this$0, fb7);
        sleepSummariesRepository$getSleepStatisticAwait$Anon2.p$ = (yi7) obj;
        return sleepSummariesRepository$getSleepStatisticAwait$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super SleepStatistic> fb7) {
        return ((SleepSummariesRepository$getSleepStatisticAwait$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b6  */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
        /*
            r9 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r9.label
            r2 = 3
            r3 = 2
            r4 = 1
            r5 = 0
            if (r1 == 0) goto L_0x003f
            if (r1 == r4) goto L_0x0037
            if (r1 == r3) goto L_0x002b
            if (r1 != r2) goto L_0x0023
            java.lang.Object r0 = r9.L$2
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            java.lang.Object r1 = r9.L$1
            com.portfolio.platform.data.SleepStatistic r1 = (com.portfolio.platform.data.SleepStatistic) r1
            java.lang.Object r1 = r9.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r10)
            goto L_0x00a0
        L_0x0023:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r0)
            throw r10
        L_0x002b:
            java.lang.Object r1 = r9.L$1
            com.portfolio.platform.data.SleepStatistic r1 = (com.portfolio.platform.data.SleepStatistic) r1
            java.lang.Object r3 = r9.L$0
            com.fossil.yi7 r3 = (com.fossil.yi7) r3
            com.fossil.t87.a(r10)
            goto L_0x007e
        L_0x0037:
            java.lang.Object r1 = r9.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r10)
            goto L_0x0063
        L_0x003f:
            com.fossil.t87.a(r10)
            com.fossil.yi7 r10 = r9.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r6 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.String r7 = "getSleepStatisticAwait"
            r1.d(r6, r7)
            com.portfolio.platform.data.source.SleepSummariesRepository r1 = r9.this$0
            r9.L$0 = r10
            r9.label = r4
            java.lang.Object r1 = r1.getSleepStatisticDB(r9)
            if (r1 != r0) goto L_0x0060
            return r0
        L_0x0060:
            r8 = r1
            r1 = r10
            r10 = r8
        L_0x0063:
            com.portfolio.platform.data.SleepStatistic r10 = (com.portfolio.platform.data.SleepStatistic) r10
            if (r10 == 0) goto L_0x0068
            return r10
        L_0x0068:
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatisticAwait$Anon2$response$Anon1_Level2 r4 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatisticAwait$Anon2$response$Anon1_Level2
            r4.<init>(r9, r5)
            r9.L$0 = r1
            r9.L$1 = r10
            r9.label = r3
            java.lang.Object r3 = com.fossil.aj5.a(r4, r9)
            if (r3 != r0) goto L_0x007a
            return r0
        L_0x007a:
            r8 = r1
            r1 = r10
            r10 = r3
            r3 = r8
        L_0x007e:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r4 = r10 instanceof com.fossil.bj5
            if (r4 == 0) goto L_0x00b6
            r4 = r10
            com.fossil.bj5 r4 = (com.fossil.bj5) r4
            java.lang.Object r4 = r4.a()
            if (r4 == 0) goto L_0x00f2
            com.fossil.pg5 r4 = com.fossil.pg5.i
            r9.L$0 = r3
            r9.L$1 = r1
            r9.L$2 = r10
            r9.label = r2
            java.lang.Object r1 = r4.d(r9)
            if (r1 != r0) goto L_0x009e
            return r0
        L_0x009e:
            r0 = r10
            r10 = r1
        L_0x00a0:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r10 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r10
            com.portfolio.platform.data.source.local.sleep.SleepDao r10 = r10.sleepDao()
            com.fossil.bj5 r0 = (com.fossil.bj5) r0
            java.lang.Object r1 = r0.a()
            com.portfolio.platform.data.SleepStatistic r1 = (com.portfolio.platform.data.SleepStatistic) r1
            r10.upsertSleepStatistic(r1)
            java.lang.Object r10 = r0.a()
            return r10
        L_0x00b6:
            boolean r0 = r10 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x00f2
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "getSleepStatisticAwait - Failure -- code="
            r2.append(r3)
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r3 = r10.a()
            r2.append(r3)
            java.lang.String r3 = ", message="
            r2.append(r3)
            com.portfolio.platform.data.model.ServerError r10 = r10.c()
            if (r10 == 0) goto L_0x00e7
            java.lang.String r10 = r10.getMessage()
            goto L_0x00e8
        L_0x00e7:
            r10 = r5
        L_0x00e8:
            r2.append(r10)
            java.lang.String r10 = r2.toString()
            r0.e(r1, r10)
        L_0x00f2:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatisticAwait$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
