package com.portfolio.platform.data.source;

import com.fossil.bj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.x97;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.FitnessDataRepository$pushPendingFitnessData$2", f = "FitnessDataRepository.kt", l = {30, 35}, m = "invokeSuspend")
public final class FitnessDataRepository$pushPendingFitnessData$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<List<FitnessDataWrapper>>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ FitnessDataRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FitnessDataRepository$pushPendingFitnessData$Anon2(FitnessDataRepository fitnessDataRepository, fb7 fb7) {
        super(2, fb7);
        this.this$0 = fitnessDataRepository;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        FitnessDataRepository$pushPendingFitnessData$Anon2 fitnessDataRepository$pushPendingFitnessData$Anon2 = new FitnessDataRepository$pushPendingFitnessData$Anon2(this.this$0, fb7);
        fitnessDataRepository$pushPendingFitnessData$Anon2.p$ = (yi7) obj;
        return fitnessDataRepository$pushPendingFitnessData$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<List<FitnessDataWrapper>>> fb7) {
        return ((FitnessDataRepository$pushPendingFitnessData$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.label = 1;
            obj = pg5.b(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            List list = (List) this.L$2;
            List list2 = (List) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            return (zi5) obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List<FitnessDataWrapper> pendingFitnessData = ((FitnessDatabase) obj).getFitnessDataDao().getPendingFitnessData();
        if (pendingFitnessData.size() <= 0) {
            return new bj5(new ArrayList(), false, 2, null);
        }
        ArrayList arrayList = new ArrayList(x97.a(pendingFitnessData, 10));
        Iterator<T> it = pendingFitnessData.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().getStartTime());
        }
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.PUSH_FITNESS_FILE;
        String access$getTAG$cp = FitnessDataRepository.TAG;
        remote.i(component, session, "", access$getTAG$cp, "[Push Pending Fitness File] Pending fitness file size " + pendingFitnessData.size() + " startTimestamp " + arrayList);
        FitnessDataRepository fitnessDataRepository = this.this$0;
        this.L$0 = yi7;
        this.L$1 = pendingFitnessData;
        this.L$2 = arrayList;
        this.label = 2;
        obj = fitnessDataRepository.pushFitnessData(pendingFitnessData, this);
        if (obj == a) {
            return a;
        }
        return (zi5) obj;
    }
}
