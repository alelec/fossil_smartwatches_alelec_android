package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.AlarmsRepository$deleteAlarm$2", f = "AlarmsRepository.kt", l = {162, 165, 171}, m = "invokeSuspend")
public final class AlarmsRepository$deleteAlarm$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<? extends Void>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Alarm $alarm;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.data.source.AlarmsRepository$deleteAlarm$2$1", f = "AlarmsRepository.kt", l = {173}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super Long>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmsRepository$deleteAlarm$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(AlarmsRepository$deleteAlarm$Anon2 alarmsRepository$deleteAlarm$Anon2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = alarmsRepository$deleteAlarm$Anon2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, fb7);
            anon1_Level2.p$ = (yi7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Long> fb7) {
            return ((Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.$alarm.setPinType(3);
                pg5 pg5 = pg5.i;
                this.L$0 = yi7;
                this.label = 1;
                obj = pg5.a(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return pb7.a(((AlarmDatabase) obj).alarmDao().insertAlarm(this.this$0.$alarm));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$deleteAlarm$Anon2(AlarmsRepository alarmsRepository, Alarm alarm, fb7 fb7) {
        super(2, fb7);
        this.this$0 = alarmsRepository;
        this.$alarm = alarm;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        AlarmsRepository$deleteAlarm$Anon2 alarmsRepository$deleteAlarm$Anon2 = new AlarmsRepository$deleteAlarm$Anon2(this.this$0, this.$alarm, fb7);
        alarmsRepository$deleteAlarm$Anon2.p$ = (yi7) obj;
        return alarmsRepository$deleteAlarm$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<? extends Void>> fb7) {
        return ((AlarmsRepository$deleteAlarm$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00d4  */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r13) {
        /*
            r12 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r12.label
            r2 = 0
            r3 = 3
            r4 = 1
            r5 = 2
            r6 = 0
            if (r1 == 0) goto L_0x0039
            if (r1 == r4) goto L_0x0031
            if (r1 == r5) goto L_0x0028
            if (r1 != r3) goto L_0x0020
            java.lang.Object r0 = r12.L$1
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            java.lang.Object r1 = r12.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r13)
            goto L_0x00ef
        L_0x0020:
            java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r13.<init>(r0)
            throw r13
        L_0x0028:
            java.lang.Object r1 = r12.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r13)
            goto L_0x00c8
        L_0x0031:
            java.lang.Object r1 = r12.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r13)
            goto L_0x008c
        L_0x0039:
            com.fossil.t87.a(r13)
            com.fossil.yi7 r13 = r12.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r7 = com.portfolio.platform.data.source.AlarmsRepository.TAG
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "deleteAlarm - alarm= "
            r8.append(r9)
            com.portfolio.platform.data.source.local.alarm.Alarm r9 = r12.$alarm
            java.lang.String r9 = r9.getTitle()
            r8.append(r9)
            java.lang.String r9 = " - "
            r8.append(r9)
            com.portfolio.platform.data.source.local.alarm.Alarm r10 = r12.$alarm
            java.lang.String r10 = r10.getId()
            r8.append(r10)
            r8.append(r9)
            com.portfolio.platform.data.source.local.alarm.Alarm r9 = r12.$alarm
            int r9 = r9.getPinType()
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r1.d(r7, r8)
            com.fossil.pg5 r1 = com.fossil.pg5.i
            r12.L$0 = r13
            r12.label = r4
            java.lang.Object r1 = r1.a(r12)
            if (r1 != r0) goto L_0x0089
            return r0
        L_0x0089:
            r11 = r1
            r1 = r13
            r13 = r11
        L_0x008c:
            com.portfolio.platform.data.source.local.alarm.AlarmDatabase r13 = (com.portfolio.platform.data.source.local.alarm.AlarmDatabase) r13
            com.portfolio.platform.data.source.local.alarm.AlarmDao r13 = r13.alarmDao()
            com.portfolio.platform.data.source.local.alarm.Alarm r7 = r12.$alarm
            java.lang.String r7 = r7.getUri()
            r13.removeAlarm(r7)
            com.portfolio.platform.data.source.local.alarm.Alarm r13 = r12.$alarm
            int r13 = r13.getPinType()
            if (r13 == r4) goto L_0x0113
            com.portfolio.platform.data.source.local.alarm.Alarm r13 = r12.$alarm
            java.lang.String r13 = r13.getId()
            boolean r13 = android.text.TextUtils.isEmpty(r13)
            if (r13 != 0) goto L_0x0113
            com.portfolio.platform.data.source.AlarmsRepository r13 = r12.this$0
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource r13 = r13.mAlarmRemoteDataSource
            com.portfolio.platform.data.source.local.alarm.Alarm r4 = r12.$alarm
            java.lang.String r4 = r4.getId()
            if (r4 == 0) goto L_0x010f
            r12.L$0 = r1
            r12.label = r5
            java.lang.Object r13 = r13.deleteAlarm(r4, r12)
            if (r13 != r0) goto L_0x00c8
            return r0
        L_0x00c8:
            com.fossil.zi5 r13 = (com.fossil.zi5) r13
            boolean r4 = r13 instanceof com.fossil.bj5
            if (r4 == 0) goto L_0x00d4
            com.fossil.bj5 r13 = new com.fossil.bj5
            r13.<init>(r6, r2, r5, r6)
            goto L_0x0108
        L_0x00d4:
            boolean r2 = r13 instanceof com.fossil.yi5
            if (r2 == 0) goto L_0x0109
            com.fossil.ti7 r2 = com.fossil.qj7.b()
            com.portfolio.platform.data.source.AlarmsRepository$deleteAlarm$Anon2$Anon1_Level2 r4 = new com.portfolio.platform.data.source.AlarmsRepository$deleteAlarm$Anon2$Anon1_Level2
            r4.<init>(r12, r6)
            r12.L$0 = r1
            r12.L$1 = r13
            r12.label = r3
            java.lang.Object r1 = com.fossil.vh7.a(r2, r4, r12)
            if (r1 != r0) goto L_0x00ee
            return r0
        L_0x00ee:
            r0 = r13
        L_0x00ef:
            com.fossil.yi5 r13 = new com.fossil.yi5
            com.fossil.yi5 r0 = (com.fossil.yi5) r0
            int r2 = r0.a()
            com.portfolio.platform.data.model.ServerError r3 = r0.c()
            java.lang.Throwable r4 = r0.d()
            r5 = 0
            r6 = 0
            r7 = 24
            r8 = 0
            r1 = r13
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
        L_0x0108:
            return r13
        L_0x0109:
            com.fossil.p87 r13 = new com.fossil.p87
            r13.<init>()
            throw r13
        L_0x010f:
            com.fossil.ee7.a()
            throw r6
        L_0x0113:
            com.fossil.bj5 r13 = new com.fossil.bj5
            r13.<init>(r6, r2, r5, r6)
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.AlarmsRepository$deleteAlarm$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
