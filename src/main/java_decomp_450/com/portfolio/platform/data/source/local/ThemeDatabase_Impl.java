package com.portfolio.platform.data.source.local;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeDatabase_Impl extends ThemeDatabase {
    @DexIgnore
    public volatile ThemeDao _themeDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `theme` (`isCurrentTheme` INTEGER NOT NULL, `type` TEXT NOT NULL, `id` TEXT NOT NULL, `name` TEXT NOT NULL, `styles` TEXT NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '3bbfe40ee4044998d625f23932959275')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `theme`");
            if (((ci) ThemeDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) ThemeDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) ThemeDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) ThemeDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) ThemeDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) ThemeDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) ThemeDatabase_Impl.this).mDatabase = wiVar;
            ThemeDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) ThemeDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) ThemeDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) ThemeDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(5);
            hashMap.put("isCurrentTheme", new ti.a("isCurrentTheme", "INTEGER", true, 0, null, 1));
            hashMap.put("type", new ti.a("type", "TEXT", true, 0, null, 1));
            hashMap.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap.put("name", new ti.a("name", "TEXT", true, 0, null, 1));
            hashMap.put("styles", new ti.a("styles", "TEXT", true, 0, null, 1));
            ti tiVar = new ti("theme", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "theme");
            if (tiVar.equals(a)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "theme(com.portfolio.platform.data.model.Theme).\n Expected:\n" + tiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `theme`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "theme");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(1), "3bbfe40ee4044998d625f23932959275", "68ccf1dee92254a5e6e37fa35b38bb4e");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDatabase
    public ThemeDao themeDao() {
        ThemeDao themeDao;
        if (this._themeDao != null) {
            return this._themeDao;
        }
        synchronized (this) {
            if (this._themeDao == null) {
                this._themeDao = new ThemeDao_Impl(this);
            }
            themeDao = this._themeDao;
        }
        return themeDao;
    }
}
