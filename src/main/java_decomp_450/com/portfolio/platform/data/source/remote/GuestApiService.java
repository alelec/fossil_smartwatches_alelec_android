package com.portfolio.platform.data.source.remote;

import com.fossil.bw7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ie4;
import com.fossil.nw7;
import com.portfolio.platform.data.model.Firmware;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface GuestApiService {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class DefaultImpls {
        @DexIgnore
        public static /* synthetic */ Object getFirmwares$default(GuestApiService guestApiService, String str, String str2, String str3, boolean z, fb7 fb7, int i, Object obj) {
            if (obj == null) {
                if ((i & 4) != 0) {
                    str3 = "android";
                }
                return guestApiService.getFirmwares(str, str2, str3, (i & 8) != 0 ? true : z, fb7);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getFirmwares");
        }
    }

    @DexIgnore
    @bw7("firmwares")
    Object getFirmwares(@nw7("appVersion") String str, @nw7("deviceModel") String str2, @nw7("os") String str3, @nw7("includesInactive") boolean z, fb7<? super fv7<ApiResponse<Firmware>>> fb7);

    @DexIgnore
    @bw7("assets/app-localizations")
    Object getLocalizations(@nw7("metadata.appVersion") String str, @nw7("metadata.platform") String str2, fb7<? super fv7<ApiResponse<ie4>>> fb7);
}
