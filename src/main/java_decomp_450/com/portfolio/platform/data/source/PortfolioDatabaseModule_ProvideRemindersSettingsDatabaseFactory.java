package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory implements Factory<RemindersSettingsDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static RemindersSettingsDatabase provideRemindersSettingsDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        RemindersSettingsDatabase provideRemindersSettingsDatabase = portfolioDatabaseModule.provideRemindersSettingsDatabase(portfolioApp);
        c87.a(provideRemindersSettingsDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideRemindersSettingsDatabase;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public RemindersSettingsDatabase get() {
        return provideRemindersSettingsDatabase(this.module, this.appProvider.get());
    }
}
