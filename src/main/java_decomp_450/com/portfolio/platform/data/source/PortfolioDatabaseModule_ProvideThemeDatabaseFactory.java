package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.ThemeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideThemeDatabaseFactory implements Factory<ThemeDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideThemeDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideThemeDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideThemeDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static ThemeDatabase provideThemeDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        ThemeDatabase provideThemeDatabase = portfolioDatabaseModule.provideThemeDatabase(portfolioApp);
        c87.a(provideThemeDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideThemeDatabase;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ThemeDatabase get() {
        return provideThemeDatabase(this.module, this.appProvider.get());
    }
}
