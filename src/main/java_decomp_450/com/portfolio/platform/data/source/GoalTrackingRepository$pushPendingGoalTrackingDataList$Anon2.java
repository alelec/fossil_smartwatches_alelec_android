package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$pushPendingGoalTrackingDataList$2", f = "GoalTrackingRepository.kt", l = {624, 627}, m = "invokeSuspend")
public final class GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository.PushPendingGoalTrackingDataListCallback $pushPendingGoalTrackingDataListCallback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon2(GoalTrackingRepository goalTrackingRepository, GoalTrackingRepository.PushPendingGoalTrackingDataListCallback pushPendingGoalTrackingDataListCallback, fb7 fb7) {
        super(2, fb7);
        this.this$0 = goalTrackingRepository;
        this.$pushPendingGoalTrackingDataListCallback = pushPendingGoalTrackingDataListCallback;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon2 goalTrackingRepository$pushPendingGoalTrackingDataList$Anon2 = new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon2(this.this$0, this.$pushPendingGoalTrackingDataListCallback, fb7);
        goalTrackingRepository$pushPendingGoalTrackingDataList$Anon2.p$ = (yi7) obj;
        return goalTrackingRepository$pushPendingGoalTrackingDataList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.label = 1;
            obj = pg5.c(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            List list = (List) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            return i97.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List<GoalTrackingData> pendingGoalTrackingDataList = ((GoalTrackingDatabase) obj).getGoalTrackingDao().getPendingGoalTrackingDataList();
        if (pendingGoalTrackingDataList.size() > 0) {
            GoalTrackingRepository goalTrackingRepository = this.this$0;
            GoalTrackingRepository.PushPendingGoalTrackingDataListCallback pushPendingGoalTrackingDataListCallback = this.$pushPendingGoalTrackingDataListCallback;
            this.L$0 = yi7;
            this.L$1 = pendingGoalTrackingDataList;
            this.label = 2;
            if (goalTrackingRepository.saveGoalTrackingDataListToServer(pendingGoalTrackingDataList, pushPendingGoalTrackingDataListCallback, this) == a) {
                return a;
            }
            return i97.a;
        }
        GoalTrackingRepository.PushPendingGoalTrackingDataListCallback pushPendingGoalTrackingDataListCallback2 = this.$pushPendingGoalTrackingDataListCallback;
        if (pushPendingGoalTrackingDataListCallback2 == null) {
            return null;
        }
        pushPendingGoalTrackingDataListCallback2.onFail(404);
        return i97.a;
    }
}
