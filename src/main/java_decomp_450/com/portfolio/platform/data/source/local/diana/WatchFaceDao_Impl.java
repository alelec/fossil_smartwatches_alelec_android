package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.lv4;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceDao_Impl implements WatchFaceDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<WatchFace> __insertionAdapterOfWatchFace;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteAll;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteAllBackgroundWatchface;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteWatchFace;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteWatchFacesWithSerial;
    @DexIgnore
    public /* final */ lv4 __watchFaceTypeConverter; // = new lv4();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<WatchFace> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watch_face` (`id`,`name`,`ringStyleItems`,`background`,`previewUrl`,`serial`,`watchFaceType`) VALUES (?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, WatchFace watchFace) {
            if (watchFace.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, watchFace.getId());
            }
            if (watchFace.getName() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, watchFace.getName());
            }
            String a = WatchFaceDao_Impl.this.__watchFaceTypeConverter.a(watchFace.getRingStyleItems());
            if (a == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, a);
            }
            String a2 = WatchFaceDao_Impl.this.__watchFaceTypeConverter.a(watchFace.getBackground());
            if (a2 == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, a2);
            }
            if (watchFace.getPreviewUrl() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, watchFace.getPreviewUrl());
            }
            if (watchFace.getSerial() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, watchFace.getSerial());
            }
            ajVar.bindLong(7, (long) watchFace.getWatchFaceType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM watch_face WHERE serial = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM watch_face WHERE id = ? and watchFaceType = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends ji {
        @DexIgnore
        public Anon4(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM watch_face WHERE watchFaceType = 0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends ji {
        @DexIgnore
        public Anon5(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM watch_face";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 implements Callable<List<WatchFace>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon6(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WatchFace> call() throws Exception {
            Cursor a = pi.a(WatchFaceDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "id");
                int b2 = oi.b(a, "name");
                int b3 = oi.b(a, "ringStyleItems");
                int b4 = oi.b(a, Explore.COLUMN_BACKGROUND);
                int b5 = oi.b(a, "previewUrl");
                int b6 = oi.b(a, "serial");
                int b7 = oi.b(a, "watchFaceType");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new WatchFace(a.getString(b), a.getString(b2), WatchFaceDao_Impl.this.__watchFaceTypeConverter.b(a.getString(b3)), WatchFaceDao_Impl.this.__watchFaceTypeConverter.a(a.getString(b4)), a.getString(b5), a.getString(b6), a.getInt(b7)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements Callable<List<WatchFace>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon7(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WatchFace> call() throws Exception {
            Cursor a = pi.a(WatchFaceDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "id");
                int b2 = oi.b(a, "name");
                int b3 = oi.b(a, "ringStyleItems");
                int b4 = oi.b(a, Explore.COLUMN_BACKGROUND);
                int b5 = oi.b(a, "previewUrl");
                int b6 = oi.b(a, "serial");
                int b7 = oi.b(a, "watchFaceType");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new WatchFace(a.getString(b), a.getString(b2), WatchFaceDao_Impl.this.__watchFaceTypeConverter.b(a.getString(b3)), WatchFaceDao_Impl.this.__watchFaceTypeConverter.a(a.getString(b4)), a.getString(b5), a.getString(b6), a.getInt(b7)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public WatchFaceDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfWatchFace = new Anon1(ciVar);
        this.__preparedStmtOfDeleteWatchFacesWithSerial = new Anon2(ciVar);
        this.__preparedStmtOfDeleteWatchFace = new Anon3(ciVar);
        this.__preparedStmtOfDeleteAllBackgroundWatchface = new Anon4(ciVar);
        this.__preparedStmtOfDeleteAll = new Anon5(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void deleteAll() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void deleteAllBackgroundWatchface() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteAllBackgroundWatchface.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllBackgroundWatchface.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void deleteWatchFace(String str, int i) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteWatchFace.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        acquire.bindLong(2, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchFace.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void deleteWatchFacesWithSerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteWatchFacesWithSerial.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchFacesWithSerial.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public List<WatchFace> getAllWatchFaces() {
        fi b = fi.b("SELECT * FROM watch_face", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "name");
            int b4 = oi.b(a, "ringStyleItems");
            int b5 = oi.b(a, Explore.COLUMN_BACKGROUND);
            int b6 = oi.b(a, "previewUrl");
            int b7 = oi.b(a, "serial");
            int b8 = oi.b(a, "watchFaceType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchFace(a.getString(b2), a.getString(b3), this.__watchFaceTypeConverter.b(a.getString(b4)), this.__watchFaceTypeConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getInt(b8)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public String getLatestWatchFaceName(int i) {
        fi b = fi.b("SELECT name FROM watch_face WHERE watchFaceType = ? ORDER BY id DESC LIMIT 1", 1);
        b.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        String str = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            if (a.moveToFirst()) {
                str = a.getString(0);
            }
            return str;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public WatchFace getWatchFaceWithId(String str) {
        fi b = fi.b("SELECT * FROM watch_face WHERE id = ?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        WatchFace watchFace = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "name");
            int b4 = oi.b(a, "ringStyleItems");
            int b5 = oi.b(a, Explore.COLUMN_BACKGROUND);
            int b6 = oi.b(a, "previewUrl");
            int b7 = oi.b(a, "serial");
            int b8 = oi.b(a, "watchFaceType");
            if (a.moveToFirst()) {
                watchFace = new WatchFace(a.getString(b2), a.getString(b3), this.__watchFaceTypeConverter.b(a.getString(b4)), this.__watchFaceTypeConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getInt(b8));
            }
            return watchFace;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public LiveData<List<WatchFace>> getWatchFacesLiveData(String str) {
        fi b = fi.b("SELECT * FROM watch_face WHERE serial = ? ", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"watch_face"}, false, (Callable) new Anon6(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public LiveData<List<WatchFace>> getWatchFacesLiveDataWithType(String str, int i) {
        fi b = fi.b("SELECT * FROM watch_face WHERE serial = ? and watchFaceType = ? ", 2);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        b.bindLong(2, (long) i);
        return this.__db.getInvalidationTracker().a(new String[]{"watch_face"}, false, (Callable) new Anon7(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public List<WatchFace> getWatchFacesWithSerial(String str) {
        fi b = fi.b("SELECT * FROM watch_face WHERE serial = ?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "name");
            int b4 = oi.b(a, "ringStyleItems");
            int b5 = oi.b(a, Explore.COLUMN_BACKGROUND);
            int b6 = oi.b(a, "previewUrl");
            int b7 = oi.b(a, "serial");
            int b8 = oi.b(a, "watchFaceType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchFace(a.getString(b2), a.getString(b3), this.__watchFaceTypeConverter.b(a.getString(b4)), this.__watchFaceTypeConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getInt(b8)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public List<WatchFace> getWatchFacesWithType(String str, int i) {
        fi b = fi.b("SELECT * FROM watch_face WHERE  serial = ? and watchFaceType = ?", 2);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        b.bindLong(2, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "name");
            int b4 = oi.b(a, "ringStyleItems");
            int b5 = oi.b(a, Explore.COLUMN_BACKGROUND);
            int b6 = oi.b(a, "previewUrl");
            int b7 = oi.b(a, "serial");
            int b8 = oi.b(a, "watchFaceType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchFace(a.getString(b2), a.getString(b3), this.__watchFaceTypeConverter.b(a.getString(b4)), this.__watchFaceTypeConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getInt(b8)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void insertAllWatchFaces(List<WatchFace> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchFace.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void insertWatchFace(WatchFace watchFace) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchFace.insert(watchFace);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
