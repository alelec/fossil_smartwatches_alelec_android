package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.zb7;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$fetchLastSleepGoal$2$response$1", f = "SleepSummariesRepository.kt", l = {122}, m = "invokeSuspend")
public final class SleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2 extends zb7 implements gd7<fb7<? super fv7<MFSleepSettings>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository$fetchLastSleepGoal$Anon2 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2(SleepSummariesRepository$fetchLastSleepGoal$Anon2 sleepSummariesRepository$fetchLastSleepGoal$Anon2, fb7 fb7) {
        super(1, fb7);
        this.this$0 = sleepSummariesRepository$fetchLastSleepGoal$Anon2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(fb7<?> fb7) {
        ee7.b(fb7, "completion");
        return new SleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2(this.this$0, fb7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public final Object invoke(fb7<? super fv7<MFSleepSettings>> fb7) {
        return ((SleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2) create(fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
            this.label = 1;
            obj = access$getMApiService$p.getSleepSetting(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
