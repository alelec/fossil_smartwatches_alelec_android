package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory implements Factory<DianaPresetRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiServiceV2Provider;
    @DexIgnore
    public /* final */ RepositoriesModule module;

    @DexIgnore
    public RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        this.module = repositoriesModule;
        this.apiServiceV2Provider = provider;
    }

    @DexIgnore
    public static RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory create(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        return new RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory(repositoriesModule, provider);
    }

    @DexIgnore
    public static DianaPresetRemoteDataSource provideDianaPresetRemoteDataSource(RepositoriesModule repositoriesModule, ApiServiceV2 apiServiceV2) {
        DianaPresetRemoteDataSource provideDianaPresetRemoteDataSource = repositoriesModule.provideDianaPresetRemoteDataSource(apiServiceV2);
        c87.a(provideDianaPresetRemoteDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideDianaPresetRemoteDataSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public DianaPresetRemoteDataSource get() {
        return provideDianaPresetRemoteDataSource(this.module, this.apiServiceV2Provider.get());
    }
}
