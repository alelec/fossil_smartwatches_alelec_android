package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$loadGoalTrackingDataList$2", f = "GoalTrackingRepository.kt", l = {MFNetworkReturnCode.WRONG_PASSWORD, 410, 413}, m = "invokeSuspend")
public final class GoalTrackingRepository$loadGoalTrackingDataList$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<ApiResponse<GoalEvent>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$loadGoalTrackingDataList$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, int i, int i2, fb7 fb7) {
        super(2, fb7);
        this.this$0 = goalTrackingRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        GoalTrackingRepository$loadGoalTrackingDataList$Anon2 goalTrackingRepository$loadGoalTrackingDataList$Anon2 = new GoalTrackingRepository$loadGoalTrackingDataList$Anon2(this.this$0, this.$startDate, this.$endDate, this.$offset, this.$limit, fb7);
        goalTrackingRepository$loadGoalTrackingDataList$Anon2.p$ = (yi7) obj;
        return goalTrackingRepository$loadGoalTrackingDataList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<ApiResponse<GoalEvent>>> fb7) {
        return ((GoalTrackingRepository$loadGoalTrackingDataList$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:43:0x010f  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:75:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r15) {
        /*
            r14 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r14.label
            r2 = 3
            r3 = 2
            r4 = 0
            r5 = 1
            if (r1 == 0) goto L_0x004c
            if (r1 == r5) goto L_0x0043
            if (r1 == r3) goto L_0x002e
            if (r1 != r2) goto L_0x0026
            java.lang.Object r0 = r14.L$2
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r14.L$1
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            java.lang.Object r1 = r14.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r15)     // Catch:{ Exception -> 0x0023 }
            goto L_0x015b
        L_0x0023:
            r15 = move-exception
            goto L_0x0168
        L_0x0026:
            java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r15.<init>(r0)
            throw r15
        L_0x002e:
            java.lang.Object r1 = r14.L$2
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r3 = r14.L$1
            com.fossil.zi5 r3 = (com.fossil.zi5) r3
            java.lang.Object r6 = r14.L$0
            com.fossil.yi7 r6 = (com.fossil.yi7) r6
            com.fossil.t87.a(r15)     // Catch:{ Exception -> 0x003f }
            goto L_0x00ee
        L_0x003f:
            r15 = move-exception
            r0 = r3
            goto L_0x0168
        L_0x0043:
            java.lang.Object r1 = r14.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r15)
            r6 = r1
            goto L_0x008f
        L_0x004c:
            com.fossil.t87.a(r15)
            com.fossil.yi7 r15 = r14.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            com.portfolio.platform.data.source.GoalTrackingRepository$Companion r6 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
            java.lang.String r6 = r6.getTAG()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "loadGoalTrackingDataList startDate="
            r7.append(r8)
            java.util.Date r8 = r14.$startDate
            r7.append(r8)
            java.lang.String r8 = ", endDate="
            r7.append(r8)
            java.util.Date r8 = r14.$endDate
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r1.d(r6, r7)
            com.portfolio.platform.data.source.GoalTrackingRepository$loadGoalTrackingDataList$Anon2$response$Anon1_Level2 r1 = new com.portfolio.platform.data.source.GoalTrackingRepository$loadGoalTrackingDataList$Anon2$response$Anon1_Level2
            r1.<init>(r14, r4)
            r14.L$0 = r15
            r14.label = r5
            java.lang.Object r1 = com.fossil.aj5.a(r1, r14)
            if (r1 != r0) goto L_0x008d
            return r0
        L_0x008d:
            r6 = r15
            r15 = r1
        L_0x008f:
            com.fossil.zi5 r15 = (com.fossil.zi5) r15
            boolean r1 = r15 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x018d
            r1 = r15
            com.fossil.bj5 r1 = (com.fossil.bj5) r1
            java.lang.Object r7 = r1.a()
            if (r7 == 0) goto L_0x01dd
            boolean r1 = r1.b()
            if (r1 != 0) goto L_0x01dd
            r1 = r15
            com.fossil.bj5 r1 = (com.fossil.bj5) r1     // Catch:{ Exception -> 0x0164 }
            java.lang.Object r1 = r1.a()     // Catch:{ Exception -> 0x0164 }
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1     // Catch:{ Exception -> 0x0164 }
            java.util.List r1 = r1.get_items()     // Catch:{ Exception -> 0x0164 }
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ Exception -> 0x0164 }
            r8 = 10
            int r8 = com.fossil.x97.a(r1, r8)     // Catch:{ Exception -> 0x0164 }
            r7.<init>(r8)     // Catch:{ Exception -> 0x0164 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ Exception -> 0x0164 }
        L_0x00c0:
            boolean r8 = r1.hasNext()     // Catch:{ Exception -> 0x0164 }
            if (r8 == 0) goto L_0x00da
            java.lang.Object r8 = r1.next()     // Catch:{ Exception -> 0x0164 }
            com.portfolio.platform.data.model.goaltracking.response.GoalEvent r8 = (com.portfolio.platform.data.model.goaltracking.response.GoalEvent) r8     // Catch:{ Exception -> 0x0164 }
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r8 = r8.toGoalTrackingData()     // Catch:{ Exception -> 0x0164 }
            if (r8 == 0) goto L_0x00d6
            r7.add(r8)     // Catch:{ Exception -> 0x0164 }
            goto L_0x00c0
        L_0x00d6:
            com.fossil.ee7.a()     // Catch:{ Exception -> 0x0164 }
            throw r4
        L_0x00da:
            com.fossil.pg5 r1 = com.fossil.pg5.i
            r14.L$0 = r6
            r14.L$1 = r15
            r14.L$2 = r7
            r14.label = r3
            java.lang.Object r1 = r1.c(r14)
            if (r1 != r0) goto L_0x00eb
            return r0
        L_0x00eb:
            r3 = r15
            r15 = r1
            r1 = r7
        L_0x00ee:
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r15 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r15
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r15 = r15.getGoalTrackingDao()
            java.util.List r7 = com.fossil.ea7.d(r1)
            r15.upsertGoalTrackingDataList(r7)
            r15 = r3
            com.fossil.bj5 r15 = (com.fossil.bj5) r15
            java.lang.Object r15 = r15.a()
            com.portfolio.platform.data.source.remote.ApiResponse r15 = (com.portfolio.platform.data.source.remote.ApiResponse) r15
            java.util.List r15 = r15.get_items()
            boolean r15 = r15.isEmpty()
            r15 = r15 ^ r5
            if (r15 == 0) goto L_0x0118
            com.portfolio.platform.data.source.GoalTrackingRepository r15 = r14.this$0
            com.fossil.ch5 r15 = r15.mSharedPreferencesManager
            r15.j(r5)
        L_0x0118:
            r15 = r3
            com.fossil.bj5 r15 = (com.fossil.bj5) r15
            java.lang.Object r15 = r15.a()
            com.portfolio.platform.data.source.remote.ApiResponse r15 = (com.portfolio.platform.data.source.remote.ApiResponse) r15
            com.portfolio.platform.data.model.Range r15 = r15.get_range()
            if (r15 == 0) goto L_0x0163
            r15 = r3
            com.fossil.bj5 r15 = (com.fossil.bj5) r15
            java.lang.Object r15 = r15.a()
            com.portfolio.platform.data.source.remote.ApiResponse r15 = (com.portfolio.platform.data.source.remote.ApiResponse) r15
            com.portfolio.platform.data.model.Range r15 = r15.get_range()
            if (r15 == 0) goto L_0x015f
            boolean r15 = r15.isHasNext()
            if (r15 == 0) goto L_0x0163
            com.portfolio.platform.data.source.GoalTrackingRepository r7 = r14.this$0
            java.util.Date r8 = r14.$startDate
            java.util.Date r9 = r14.$endDate
            int r15 = r14.$offset
            int r4 = r14.$limit
            int r10 = r15 + r4
            int r11 = r14.$limit
            r14.L$0 = r6
            r14.L$1 = r3
            r14.L$2 = r1
            r14.label = r2
            r12 = r14
            java.lang.Object r15 = r7.loadGoalTrackingDataList(r8, r9, r10, r11, r12)
            if (r15 != r0) goto L_0x015a
            return r0
        L_0x015a:
            r0 = r3
        L_0x015b:
            r3 = r15
            com.fossil.zi5 r3 = (com.fossil.zi5) r3
            goto L_0x0163
        L_0x015f:
            com.fossil.ee7.a()
            throw r4
        L_0x0163:
            return r3
        L_0x0164:
            r0 = move-exception
            r13 = r0
            r0 = r15
            r15 = r13
        L_0x0168:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            com.portfolio.platform.data.source.GoalTrackingRepository$Companion r2 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
            java.lang.String r2 = r2.getTAG()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "loadGoalTrackingDataList exception="
            r3.append(r4)
            r3.append(r15)
            java.lang.String r3 = r3.toString()
            r1.e(r2, r3)
            r15.printStackTrace()
            r15 = r0
            goto L_0x01dd
        L_0x018d:
            boolean r0 = r15 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x01dd
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            com.portfolio.platform.data.source.GoalTrackingRepository$Companion r1 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
            java.lang.String r1 = r1.getTAG()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "loadGoalTrackingDataList Failure code="
            r2.append(r3)
            r3 = r15
            com.fossil.yi5 r3 = (com.fossil.yi5) r3
            int r5 = r3.a()
            r2.append(r5)
            java.lang.String r5 = " message="
            r2.append(r5)
            com.portfolio.platform.data.model.ServerError r5 = r3.c()
            if (r5 == 0) goto L_0x01c4
            java.lang.String r5 = r5.getMessage()
            if (r5 == 0) goto L_0x01c4
            r4 = r5
            goto L_0x01ce
        L_0x01c4:
            com.portfolio.platform.data.model.ServerError r3 = r3.c()
            if (r3 == 0) goto L_0x01ce
            java.lang.String r4 = r3.getUserMessage()
        L_0x01ce:
            if (r4 == 0) goto L_0x01d1
            goto L_0x01d3
        L_0x01d1:
            java.lang.String r4 = ""
        L_0x01d3:
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
        L_0x01dd:
            return r15
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository$loadGoalTrackingDataList$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
