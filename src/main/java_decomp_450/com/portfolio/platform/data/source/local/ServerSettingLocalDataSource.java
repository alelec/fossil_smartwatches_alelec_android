package com.portfolio.platform.data.source.local;

import com.fossil.ah5;
import com.fossil.ee7;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerSettingLocalDataSource implements ServerSettingDataSource {
    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void addOrUpdateServerSetting(ServerSetting serverSetting) {
        ah5.p.a().k().addOrUpdateServerSetting(serverSetting);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void addOrUpdateServerSettingList(List<ServerSetting> list) {
        ah5.p.a().k().a(list);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void clearData() {
        ah5.p.a().k().a();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public ServerSetting getServerSettingByKey(String str) {
        ee7.b(str, "key");
        return ah5.p.a().k().getServerSettingByKey(str);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void getServerSettingList(ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        ee7.b(onGetServerSettingList, Constants.CALLBACK);
    }
}
