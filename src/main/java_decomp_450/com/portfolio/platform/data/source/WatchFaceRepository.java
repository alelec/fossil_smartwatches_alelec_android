package com.portfolio.platform.data.source;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.jr5;
import com.fossil.ps7;
import com.fossil.qb5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.Background;
import com.portfolio.platform.data.model.diana.preset.Data;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.source.local.diana.WatchFaceDao;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceRepository {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public /* final */ FileRepository fileRepository;
    @DexIgnore
    public /* final */ WatchFaceDao watchFaceDao;
    @DexIgnore
    public /* final */ WatchFaceRemoteDataSource watchFaceRemoteDataSource;

    @DexIgnore
    public WatchFaceRepository(Context context2, WatchFaceDao watchFaceDao2, WatchFaceRemoteDataSource watchFaceRemoteDataSource2, FileRepository fileRepository2) {
        ee7.b(context2, "context");
        ee7.b(watchFaceDao2, "watchFaceDao");
        ee7.b(watchFaceRemoteDataSource2, "watchFaceRemoteDataSource");
        ee7.b(fileRepository2, "fileRepository");
        this.context = context2;
        this.watchFaceDao = watchFaceDao2;
        this.watchFaceRemoteDataSource = watchFaceRemoteDataSource2;
        this.fileRepository = fileRepository2;
        String simpleName = WatchFaceRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "WatchFaceRepository::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(this.TAG, "cleanUp()");
        this.fileRepository.deletedFilesByType(FileType.WATCH_FACE);
        this.watchFaceDao.deleteAll();
    }

    @DexIgnore
    public final void deleteBackgroundPhoto(String str, String str2) {
        ee7.b(str, "imageName");
        ee7.b(str2, "binaryName");
        FLogger.INSTANCE.getLocal().d(this.TAG, "deleteBackgroundPhoto()");
        this.fileRepository.deleteFileByName(str, FileType.WATCH_FACE);
        this.fileRepository.deleteFileByName(str2, FileType.WATCH_FACE);
    }

    @DexIgnore
    public final void deleteWatchFace(String str, qb5 qb5) {
        ee7.b(str, "id");
        ee7.b(qb5, "type");
        this.watchFaceDao.deleteWatchFace(str, qb5.getValue());
    }

    @DexIgnore
    public final void deleteWatchFacesWithSerial(String str) {
        ee7.b(str, "serial");
        this.watchFaceDao.deleteWatchFacesWithSerial(str);
    }

    @DexIgnore
    public final Context getContext() {
        return this.context;
    }

    @DexIgnore
    public final WatchFace getWatchFaceWithId(String str) {
        ee7.b(str, "id");
        return this.watchFaceDao.getWatchFaceWithId(str);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01e2  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWatchFacesFromServer(java.lang.String r21, com.fossil.fb7<? super com.fossil.i97> r22) {
        /*
            r20 = this;
            r0 = r20
            r1 = r21
            r2 = r22
            boolean r3 = r2 instanceof com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesFromServer$Anon1
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesFromServer$Anon1 r3 = (com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesFromServer$Anon1) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesFromServer$Anon1 r3 = new com.portfolio.platform.data.source.WatchFaceRepository$getWatchFacesFromServer$Anon1
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 2
            r7 = 1
            if (r5 == 0) goto L_0x005f
            if (r5 == r7) goto L_0x0053
            if (r5 != r6) goto L_0x004b
            java.lang.Object r1 = r3.L$5
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r1 = r3.L$4
            java.util.ArrayList r1 = (java.util.ArrayList) r1
            java.lang.Object r1 = r3.L$3
            java.util.ArrayList r1 = (java.util.ArrayList) r1
            java.lang.Object r4 = r3.L$2
            com.fossil.zi5 r4 = (com.fossil.zi5) r4
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r3 = r3.L$0
            com.portfolio.platform.data.source.WatchFaceRepository r3 = (com.portfolio.platform.data.source.WatchFaceRepository) r3
            com.fossil.t87.a(r2)
            goto L_0x013f
        L_0x004b:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0053:
            java.lang.Object r1 = r3.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.data.source.WatchFaceRepository r5 = (com.portfolio.platform.data.source.WatchFaceRepository) r5
            com.fossil.t87.a(r2)
            goto L_0x0072
        L_0x005f:
            com.fossil.t87.a(r2)
            com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource r2 = r0.watchFaceRemoteDataSource
            r3.L$0 = r0
            r3.L$1 = r1
            r3.label = r7
            java.lang.Object r2 = r2.getWatchFacesFromServer(r1, r3)
            if (r2 != r4) goto L_0x0071
            return r4
        L_0x0071:
            r5 = r0
        L_0x0072:
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            boolean r7 = r2 instanceof com.fossil.bj5
            if (r7 == 0) goto L_0x01e2
            r7 = r2
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            java.lang.Object r8 = r7.a()
            java.util.ArrayList r8 = (java.util.ArrayList) r8
            if (r8 == 0) goto L_0x01ef
            com.fossil.qb5 r9 = com.fossil.qb5.BACKGROUND
            java.util.List r9 = r5.getWatchFacesWithType(r1, r9)
            java.util.ArrayList r10 = new java.util.ArrayList
            r11 = 10
            int r11 = com.fossil.x97.a(r8, r11)
            r10.<init>(r11)
            java.util.Iterator r11 = r8.iterator()
        L_0x0098:
            boolean r12 = r11.hasNext()
            if (r12 == 0) goto L_0x00ad
            java.lang.Object r12 = r11.next()
            com.portfolio.platform.data.model.diana.preset.WatchFace r12 = (com.portfolio.platform.data.model.diana.preset.WatchFace) r12
            r12.setSerial(r1)
            com.fossil.i97 r12 = com.fossil.i97.a
            r10.add(r12)
            goto L_0x0098
        L_0x00ad:
            java.util.Iterator r10 = r8.iterator()
        L_0x00b1:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x00c7
            java.lang.Object r11 = r10.next()
            com.portfolio.platform.data.model.diana.preset.WatchFace r11 = (com.portfolio.platform.data.model.diana.preset.WatchFace) r11
            com.fossil.qb5 r12 = com.fossil.qb5.BACKGROUND
            int r12 = r12.getValue()
            r11.setWatchFaceType(r12)
            goto L_0x00b1
        L_0x00c7:
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = r5.TAG
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "getWatchFacesFromServer isFromCache "
            r12.append(r13)
            boolean r13 = r7.b()
            r12.append(r13)
            java.lang.String r13 = " localSize "
            r12.append(r13)
            int r13 = r9.size()
            r12.append(r13)
            java.lang.String r13 = " serverSize "
            r12.append(r13)
            int r13 = r8.size()
            r12.append(r13)
            java.lang.String r12 = r12.toString()
            r10.d(r11, r12)
            boolean r7 = r7.b()
            if (r7 == 0) goto L_0x010f
            int r7 = r9.size()
            int r10 = r8.size()
            if (r7 == r10) goto L_0x0141
        L_0x010f:
            com.portfolio.platform.data.source.FileRepository r7 = r5.fileRepository
            r7.deleteBackgroundFiles()
            com.portfolio.platform.data.source.local.diana.WatchFaceDao r7 = r5.watchFaceDao
            r7.deleteAllBackgroundWatchface()
            com.portfolio.platform.data.source.local.diana.WatchFaceDao r7 = r5.watchFaceDao
            r7.insertAllWatchFaces(r8)
            com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r7 = r7.c()
            com.fossil.nh5 r7 = r7.i()
            r3.L$0 = r5
            r3.L$1 = r1
            r3.L$2 = r2
            r3.L$3 = r8
            r3.L$4 = r8
            r3.L$5 = r9
            r3.label = r6
            java.lang.Object r1 = r7.a(r1, r3)
            if (r1 != r4) goto L_0x013d
            return r4
        L_0x013d:
            r3 = r5
            r1 = r8
        L_0x013f:
            r8 = r1
            r5 = r3
        L_0x0141:
            android.content.Context r1 = r5.context
            java.io.File r1 = r1.getFilesDir()
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "context.filesDir.toString()"
            com.fossil.ee7.a(r1, r2)
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Iterator r1 = r8.iterator()
        L_0x0159:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x01ef
            java.lang.Object r2 = r1.next()
            com.portfolio.platform.data.model.diana.preset.WatchFace r2 = (com.portfolio.platform.data.model.diana.preset.WatchFace) r2
            java.util.ArrayList r3 = r2.component3()
            com.portfolio.platform.data.model.diana.preset.Background r4 = r2.component4()
            java.lang.String r7 = r2.component5()
            com.portfolio.platform.data.source.FileRepository r8 = r5.fileRepository
            com.portfolio.platform.data.model.diana.preset.Data r2 = r4.getData()
            java.lang.String r9 = r2.getUrl()
            com.misfit.frameworks.buttonservice.model.FileType r10 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            r11 = 0
            r12 = 4
            r13 = 0
            com.portfolio.platform.data.source.FileRepository.asyncDownloadFromUrl$default(r8, r9, r10, r11, r12, r13)
            com.portfolio.platform.data.source.FileRepository r14 = r5.fileRepository
            com.portfolio.platform.data.model.diana.preset.Data r2 = r4.getData()
            java.lang.String r15 = r2.getPreviewUrl()
            com.misfit.frameworks.buttonservice.model.FileType r16 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            r17 = 0
            r18 = 4
            r19 = 0
            com.portfolio.platform.data.source.FileRepository.asyncDownloadFromUrl$default(r14, r15, r16, r17, r18, r19)
            com.portfolio.platform.data.source.FileRepository r6 = r5.fileRepository
            com.misfit.frameworks.buttonservice.model.FileType r8 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            r9 = 0
            r10 = 4
            com.portfolio.platform.data.source.FileRepository.asyncDownloadFromUrl$default(r6, r7, r8, r9, r10, r11)
            if (r3 == 0) goto L_0x0159
            java.util.Iterator r2 = r3.iterator()
        L_0x01a7:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0159
            java.lang.Object r3 = r2.next()
            com.portfolio.platform.data.model.diana.preset.RingStyleItem r3 = (com.portfolio.platform.data.model.diana.preset.RingStyleItem) r3
            com.portfolio.platform.data.source.FileRepository r6 = r5.fileRepository
            com.portfolio.platform.data.model.diana.preset.RingStyle r4 = r3.getRingStyle()
            com.portfolio.platform.data.model.diana.preset.Data r4 = r4.getData()
            java.lang.String r7 = r4.getUrl()
            com.misfit.frameworks.buttonservice.model.FileType r8 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            r9 = 0
            r10 = 4
            r11 = 0
            com.portfolio.platform.data.source.FileRepository.asyncDownloadFromUrl$default(r6, r7, r8, r9, r10, r11)
            com.portfolio.platform.data.source.FileRepository r12 = r5.fileRepository
            com.portfolio.platform.data.model.diana.preset.RingStyle r3 = r3.getRingStyle()
            com.portfolio.platform.data.model.diana.preset.Data r3 = r3.getData()
            java.lang.String r13 = r3.getPreviewUrl()
            com.misfit.frameworks.buttonservice.model.FileType r14 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            r15 = 0
            r16 = 4
            r17 = 0
            com.portfolio.platform.data.source.FileRepository.asyncDownloadFromUrl$default(r12, r13, r14, r15, r16, r17)
            goto L_0x01a7
        L_0x01e2:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = r5.TAG
            java.lang.String r3 = "Failed"
            r1.e(r2, r3)
        L_0x01ef:
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchFaceRepository.getWatchFacesFromServer(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final LiveData<List<WatchFace>> getWatchFacesLiveDataWithSerial(String str) {
        ee7.b(str, "serial");
        return this.watchFaceDao.getWatchFacesLiveData(str);
    }

    @DexIgnore
    public final LiveData<List<WatchFace>> getWatchFacesLiveDataWithType(String str, qb5 qb5) {
        ee7.b(str, "serial");
        ee7.b(qb5, "type");
        return this.watchFaceDao.getWatchFacesLiveDataWithType(str, qb5.getValue());
    }

    @DexIgnore
    public final List<WatchFace> getWatchFacesWithSerial(String str) {
        ee7.b(str, "serial");
        return this.watchFaceDao.getWatchFacesWithSerial(str);
    }

    @DexIgnore
    public final List<WatchFace> getWatchFacesWithType(String str, qb5 qb5) {
        ee7.b(str, "serial");
        ee7.b(qb5, "type");
        return this.watchFaceDao.getWatchFacesWithType(str, qb5.getValue());
    }

    @DexIgnore
    public final void insertWatchFace(String str, String str2, String str3, ArrayList<RingStyleItem> arrayList) {
        ee7.b(str, "directory");
        ee7.b(str2, "fileName");
        ee7.b(str3, "binaryName");
        FLogger.INSTANCE.getLocal().d(this.TAG, "insertWatchFace()");
        String str4 = str + File.separator + str2;
        this.watchFaceDao.insertWatchFace(new WatchFace(str2, "", arrayList, new Background(str2, new Data(str4, str + File.separator + str3)), str4, PortfolioApp.g0.c().c(), qb5.PHOTO.getValue()));
    }

    @DexIgnore
    public final void saveBackgroundPhoto(Context context2, String str, String str2, Bitmap bitmap, String str3, byte[] bArr) {
        ee7.b(context2, "context");
        ee7.b(str, "directory");
        ee7.b(str2, "fileName");
        ee7.b(bitmap, "bitmap");
        ee7.b(str3, "dataName");
        ee7.b(bArr, "data");
        FLogger.INSTANCE.getLocal().d(this.TAG, "saveBackgroundPhoto()");
        if (new File(str).exists()) {
            ps7.a(new File(str + str3), bArr);
            jr5.a(context2, str, str2, bitmap);
        }
    }
}
