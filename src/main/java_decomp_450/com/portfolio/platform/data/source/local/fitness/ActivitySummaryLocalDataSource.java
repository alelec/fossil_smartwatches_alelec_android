package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.facebook.share.internal.VideoUploader;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.ge5;
import com.fossil.ik7;
import com.fossil.of;
import com.fossil.pj4;
import com.fossil.qj7;
import com.fossil.r87;
import com.fossil.te5;
import com.fossil.ue5;
import com.fossil.w97;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.fossil.xh7;
import com.fossil.xi;
import com.fossil.zd5;
import com.fossil.zd7;
import com.fossil.zh;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryLocalDataSource extends of<Date, ActivitySummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ te5.a listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public /* final */ ge5 mFitnessHelper;
    @DexIgnore
    public te5 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ zh.c mObserver;
    @DexIgnore
    public List<r87<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public Date mStartDate; // = new Date();
    @DexIgnore
    public /* final */ SummariesRepository mSummariesRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends zh.c {
        @DexIgnore
        public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = activitySummaryLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.zh.c
        public void onInvalidated(Set<String> set) {
            ee7.b(set, "tables");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ActivitySummaryLocalDataSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "XXX-invalidate " + set);
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            ee7.b(date, "date");
            ee7.b(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(getTAG$app_fossilRelease(), "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar c = zd5.c(instance);
            if (zd5.b(date2, c.getTime())) {
                c.setTime(date2);
            }
            Date time = c.getTime();
            ee7.a((Object) time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ActivitySummaryLocalDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = ActivitySummaryLocalDataSource.class.getSimpleName();
        ee7.a((Object) simpleName, "ActivitySummaryLocalData\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ActivitySummaryLocalDataSource(SummariesRepository summariesRepository, ge5 ge5, FitnessDataRepository fitnessDataRepository, FitnessDatabase fitnessDatabase, Date date, pj4 pj4, te5.a aVar, Calendar calendar) {
        ee7.b(summariesRepository, "mSummariesRepository");
        ee7.b(ge5, "mFitnessHelper");
        ee7.b(fitnessDataRepository, "mFitnessDataRepository");
        ee7.b(fitnessDatabase, "mFitnessDatabase");
        ee7.b(date, "mCreatedDate");
        ee7.b(pj4, "appExecutors");
        ee7.b(aVar, "listener");
        ee7.b(calendar, "key");
        this.mSummariesRepository = summariesRepository;
        this.mFitnessHelper = ge5;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mFitnessDatabase = fitnessDatabase;
        this.mCreatedDate = date;
        this.listener = aVar;
        this.key = calendar;
        te5 te5 = new te5(pj4.a());
        this.mHelper = te5;
        this.mNetworkState = ue5.a(te5);
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, SampleDay.TABLE_NAME, new String[0]);
        this.mFitnessDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar c = zd5.c(instance);
        c.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + c.getTime());
        Date time = c.getTime();
        ee7.a((Object) time, "calendar.time");
        this.mStartDate = time;
        if (zd5.b(date, time)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final void calculateSummaries(List<ActivitySummary> list) {
        int i;
        int i2;
        int i3;
        int i4;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "endCalendar");
            instance.setTime(((ActivitySummary) ea7.f((List) list)).getDate());
            if (instance.get(7) != 1) {
                Calendar s = zd5.s(instance.getTime());
                ee7.a((Object) s, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                ActivitySummaryDao activitySummaryDao = this.mFitnessDatabase.activitySummaryDao();
                Date time = s.getTime();
                ee7.a((Object) time, "startCalendar.time");
                Date time2 = instance.getTime();
                ee7.a((Object) time2, "endCalendar.time");
                ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = activitySummaryDao.getTotalValuesOfWeek(time, time2);
                i2 = (int) totalValuesOfWeek.getTotalStepsOfWeek();
                i = (int) totalValuesOfWeek.getTotalCaloriesOfWeek();
                i3 = totalValuesOfWeek.getTotalActiveTimeOfWeek();
            } else {
                i3 = 0;
                i2 = 0;
                i = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            ee7.a((Object) instance2, "calendar");
            instance2.setTime(((ActivitySummary) ea7.d((List) list)).getDate());
            Calendar s2 = zd5.s(instance2.getTime());
            ee7.a((Object) s2, "DateHelper.getStartOfWeek(calendar.time)");
            s2.add(5, -1);
            int i5 = 0;
            int i6 = 0;
            double d = 0.0d;
            double d2 = 0.0d;
            int i7 = 0;
            for (T t : list) {
                int i8 = i6 + 1;
                if (i6 >= 0) {
                    T t2 = t;
                    if (zd5.d(t2.getDate(), s2.getTime())) {
                        i4 = i6;
                        list.get(i5).setTotalValuesOfWeek(new ActivitySummary.TotalValuesOfWeek(d, d2, i7));
                        s2.add(5, -7);
                        i5 = i4;
                        d = 0.0d;
                        d2 = 0.0d;
                        i7 = 0;
                    } else {
                        i4 = i6;
                    }
                    d += t2.getSteps();
                    d2 += t2.getCalories();
                    i7 += t2.getActiveTime();
                    if (i4 == list.size() - 1) {
                        d += (double) i2;
                        d2 += (double) i;
                        i7 += i3;
                    }
                    i6 = i8;
                } else {
                    w97.c();
                    throw null;
                }
            }
            list.get(i5).setTotalValuesOfWeek(new ActivitySummary.TotalValuesOfWeek(d, d2, i7));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateSummaries summaries.size=" + list.size());
    }

    @DexIgnore
    private final ActivitySummary dummySummary(ActivitySummary activitySummary, Date date) {
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "calendar");
        instance.setTime(date);
        ActivitySummary activitySummary2 = new ActivitySummary(instance.get(1), instance.get(2) + 1, instance.get(5), activitySummary.getTimezoneName(), activitySummary.getDstOffset(), 0.0d, 0.0d, 0.0d, w97.d(0, 0, 0), 0, 0, 0, 0, 7680, null);
        activitySummary2.setCreatedAt(DateTime.now());
        activitySummary2.setUpdatedAt(DateTime.now());
        return activitySummary2;
    }

    @DexIgnore
    private final List<ActivitySummary> getDataInDatabase(Date date, Date date2) {
        Object obj;
        Date date3 = date;
        Date date4 = date2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("getDataInDatabase - startDate=");
        sb.append(date3);
        sb.append(", endDate=");
        sb.append(date4);
        sb.append(" DBNAME=");
        xi openHelper = this.mFitnessDatabase.getOpenHelper();
        ee7.a((Object) openHelper, "mFitnessDatabase.openHelper");
        sb.append(openHelper.getDatabaseName());
        local.d(str, sb.toString());
        List<ActivitySummary> activitySummariesDesc = this.mFitnessDatabase.activitySummaryDao().getActivitySummariesDesc(zd5.b(date, date2) ? date4 : date3, date4);
        if (!activitySummariesDesc.isEmpty()) {
            Boolean w = zd5.w(((ActivitySummary) ea7.d((List) activitySummariesDesc)).getDate());
            ee7.a((Object) w, "DateHelper.isToday(summaries.first().getDate())");
            if (w.booleanValue()) {
                ((ActivitySummary) ea7.d((List) activitySummariesDesc)).setSteps(Math.max((double) this.mFitnessHelper.a(new Date()), ((ActivitySummary) ea7.d((List) activitySummariesDesc)).getSteps()));
            }
        }
        calculateSummaries(activitySummariesDesc);
        ArrayList arrayList = new ArrayList();
        Date lastDate = this.mFitnessDatabase.activitySummaryDao().getLastDate();
        if (lastDate == null) {
            lastDate = date3;
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(activitySummariesDesc);
        ActivitySummary activitySummary = this.mFitnessDatabase.activitySummaryDao().getActivitySummary(date4);
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "endCalendar");
        instance.setTime(date4);
        if (activitySummary == null) {
            activitySummary = new ActivitySummary(instance.get(1), instance.get(2) + 1, instance.get(5), "", 0, 0.0d, 0.0d, 0.0d, w97.d(0, 0, 0), 0, 0, 0, 0, 7680, null);
            activitySummary.setActiveTimeGoal(30);
            activitySummary.setStepGoal(VideoUploader.RETRY_DELAY_UNIT_MS);
            activitySummary.setCaloriesGoal(ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
        }
        if (!zd5.b(date3, lastDate)) {
            date3 = lastDate;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "getDataInDatabase - summaries.size=" + activitySummariesDesc.size() + ", summaryParent=" + activitySummary + ", " + "lastDate=" + lastDate + ", startDateToFill=" + date3);
        while (zd5.c(date4, date3)) {
            Iterator it = arrayList2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (zd5.d(((ActivitySummary) obj).getDate(), date4)) {
                    break;
                }
            }
            ActivitySummary activitySummary2 = (ActivitySummary) obj;
            if (activitySummary2 == null) {
                arrayList.add(dummySummary(activitySummary, date4));
            } else {
                arrayList.add(activitySummary2);
                arrayList2.remove(activitySummary2);
            }
            date4 = zd5.p(date4);
            ee7.a((Object) date4, "DateHelper.getPrevDate(endDateToFill)");
        }
        if (!arrayList.isEmpty()) {
            ActivitySummary activitySummary3 = (ActivitySummary) ea7.d((List) arrayList);
            Boolean w2 = zd5.w(activitySummary3.getDate());
            ee7.a((Object) w2, "DateHelper.isToday(todaySummary.getDate())");
            if (w2.booleanValue()) {
                arrayList.add(0, new ActivitySummary(activitySummary3));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final void loadData(te5.d dVar, Date date, Date date2, te5.b.a aVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadData start=" + date + ", end=" + date2);
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new ActivitySummaryLocalDataSource$loadData$Anon1(this, date, date2, dVar, aVar, null), 3, null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final te5 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    @Override // com.fossil.lf
    public boolean isInvalid() {
        this.mFitnessDatabase.getInvalidationTracker().b();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadAfter(of.f<Date> fVar, of.a<Date, ActivitySummary> aVar) {
        ee7.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Object) fVar.a));
        if (zd5.b(fVar.a, this.mCreatedDate)) {
            Key key2 = fVar.a;
            ee7.a((Object) key2, "params.key");
            Key key3 = key2;
            Companion companion = Companion;
            Key key4 = fVar.a;
            ee7.a((Object) key4, "params.key");
            Date calculateNextKey = companion.calculateNextKey(key4, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date o = zd5.d(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : zd5.o(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + o + ", endQueryDate=" + ((Object) key3));
            ee7.a((Object) o, "startQueryDate");
            aVar.a(getDataInDatabase(o, key3), calculateNextKey);
            if (zd5.b(this.mStartDate, (Date) key3)) {
                this.mEndDate = key3;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.d(str3, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new r87<>(this.mStartDate, this.mEndDate));
                this.mHelper.a(te5.d.AFTER, new ActivitySummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadBefore(of.f<Date> fVar, of.a<Date, ActivitySummary> aVar) {
        ee7.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadInitial(of.e<Date> eVar, of.c<Date, ActivitySummary> cVar) {
        ee7.b(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date o = zd5.d(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : zd5.o(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + o + ", endQueryDate=" + date);
        ee7.a((Object) o, "startQueryDate");
        cVar.a(getDataInDatabase(o, date), null, this.key.getTime());
        this.mHelper.a(te5.d.INITIAL, new ActivitySummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        ee7.b(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(te5 te5) {
        ee7.b(te5, "<set-?>");
        this.mHelper = te5;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        ee7.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        ee7.b(date, "<set-?>");
        this.mStartDate = date;
    }
}
