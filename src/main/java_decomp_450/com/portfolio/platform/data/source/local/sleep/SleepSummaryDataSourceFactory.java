package com.portfolio.platform.data.source.local.sleep;

import androidx.lifecycle.MutableLiveData;
import com.fossil.ee7;
import com.fossil.lf;
import com.fossil.pj4;
import com.fossil.te5;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummaryDataSourceFactory extends lf.b<Date, SleepSummary> {
    @DexIgnore
    public SleepSummaryLocalDataSource localSource;
    @DexIgnore
    public /* final */ pj4 mAppExecutors;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ te5.a mListener;
    @DexIgnore
    public /* final */ SleepDatabase mSleepDatabase;
    @DexIgnore
    public /* final */ SleepSessionsRepository mSleepSessionsRepository;
    @DexIgnore
    public /* final */ SleepSummariesRepository mSleepSummariesRepository;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<SleepSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public SleepSummaryDataSourceFactory(SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDatabase sleepDatabase, Date date, pj4 pj4, te5.a aVar, Calendar calendar) {
        ee7.b(sleepSummariesRepository, "mSleepSummariesRepository");
        ee7.b(sleepSessionsRepository, "mSleepSessionsRepository");
        ee7.b(fitnessDataRepository, "mFitnessDataRepository");
        ee7.b(sleepDatabase, "mSleepDatabase");
        ee7.b(date, "mCreatedDate");
        ee7.b(pj4, "mAppExecutors");
        ee7.b(aVar, "mListener");
        ee7.b(calendar, "mStartCalendar");
        this.mSleepSummariesRepository = sleepSummariesRepository;
        this.mSleepSessionsRepository = sleepSessionsRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mSleepDatabase = sleepDatabase;
        this.mCreatedDate = date;
        this.mAppExecutors = pj4;
        this.mListener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    @Override // com.fossil.lf.b
    public lf<Date, SleepSummary> create() {
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource = new SleepSummaryLocalDataSource(this.mSleepSummariesRepository, this.mSleepSessionsRepository, this.mFitnessDataRepository, this.mSleepDatabase, this.mCreatedDate, this.mAppExecutors, this.mListener, this.mStartCalendar);
        this.localSource = sleepSummaryLocalDataSource;
        this.sourceLiveData.a(sleepSummaryLocalDataSource);
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource2 = this.localSource;
        if (sleepSummaryLocalDataSource2 != null) {
            return sleepSummaryLocalDataSource2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final SleepSummaryLocalDataSource getLocalSource() {
        return this.localSource;
    }

    @DexIgnore
    public final MutableLiveData<SleepSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalSource(SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        this.localSource = sleepSummaryLocalDataSource;
    }
}
