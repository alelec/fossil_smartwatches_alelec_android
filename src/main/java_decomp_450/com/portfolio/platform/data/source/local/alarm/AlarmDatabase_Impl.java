package com.portfolio.platform.data.source.local.alarm;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDatabase_Impl extends AlarmDatabase {
    @DexIgnore
    public volatile AlarmDao _alarmDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `alarm` (`id` TEXT, `uri` TEXT NOT NULL, `title` TEXT NOT NULL, `message` TEXT NOT NULL, `hour` INTEGER NOT NULL, `minute` INTEGER NOT NULL, `days` TEXT, `isActive` INTEGER NOT NULL, `isRepeated` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`uri`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '0b8d5ee1081d415aedfc0c7985d749f7')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `alarm`");
            if (((ci) AlarmDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) AlarmDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) AlarmDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) AlarmDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) AlarmDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) AlarmDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) AlarmDatabase_Impl.this).mDatabase = wiVar;
            AlarmDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) AlarmDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) AlarmDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) AlarmDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(12);
            hashMap.put("id", new ti.a("id", "TEXT", false, 0, null, 1));
            hashMap.put("uri", new ti.a("uri", "TEXT", true, 1, null, 1));
            hashMap.put("title", new ti.a("title", "TEXT", true, 0, null, 1));
            hashMap.put("message", new ti.a("message", "TEXT", true, 0, null, 1));
            hashMap.put(AppFilter.COLUMN_HOUR, new ti.a(AppFilter.COLUMN_HOUR, "INTEGER", true, 0, null, 1));
            hashMap.put(MFSleepGoal.COLUMN_MINUTE, new ti.a(MFSleepGoal.COLUMN_MINUTE, "INTEGER", true, 0, null, 1));
            hashMap.put(Alarm.COLUMN_DAYS, new ti.a(Alarm.COLUMN_DAYS, "TEXT", false, 0, null, 1));
            hashMap.put("isActive", new ti.a("isActive", "INTEGER", true, 0, null, 1));
            hashMap.put("isRepeated", new ti.a("isRepeated", "INTEGER", true, 0, null, 1));
            hashMap.put("createdAt", new ti.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap.put("updatedAt", new ti.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap.put("pinType", new ti.a("pinType", "INTEGER", true, 0, null, 1));
            ti tiVar = new ti(Alarm.TABLE_NAME, hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, Alarm.TABLE_NAME);
            if (tiVar.equals(a)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "alarm(com.portfolio.platform.data.source.local.alarm.Alarm).\n Expected:\n" + tiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDatabase
    public AlarmDao alarmDao() {
        AlarmDao alarmDao;
        if (this._alarmDao != null) {
            return this._alarmDao;
        }
        synchronized (this) {
            if (this._alarmDao == null) {
                this._alarmDao = new AlarmDao_Impl(this);
            }
            alarmDao = this._alarmDao;
        }
        return alarmDao;
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `alarm`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), Alarm.TABLE_NAME);
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(6), "0b8d5ee1081d415aedfc0c7985d749f7", "7e35df0dd907bb841b2567561f725af0");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }
}
