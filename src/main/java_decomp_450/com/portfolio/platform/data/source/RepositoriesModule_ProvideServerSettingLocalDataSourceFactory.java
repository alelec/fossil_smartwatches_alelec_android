package com.portfolio.platform.data.source;

import com.fossil.c87;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RepositoriesModule_ProvideServerSettingLocalDataSourceFactory implements Factory<ServerSettingDataSource> {
    @DexIgnore
    public /* final */ RepositoriesModule module;

    @DexIgnore
    public RepositoriesModule_ProvideServerSettingLocalDataSourceFactory(RepositoriesModule repositoriesModule) {
        this.module = repositoriesModule;
    }

    @DexIgnore
    public static RepositoriesModule_ProvideServerSettingLocalDataSourceFactory create(RepositoriesModule repositoriesModule) {
        return new RepositoriesModule_ProvideServerSettingLocalDataSourceFactory(repositoriesModule);
    }

    @DexIgnore
    public static ServerSettingDataSource provideServerSettingLocalDataSource(RepositoriesModule repositoriesModule) {
        ServerSettingDataSource provideServerSettingLocalDataSource = repositoriesModule.provideServerSettingLocalDataSource();
        c87.a(provideServerSettingLocalDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideServerSettingLocalDataSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ServerSettingDataSource get() {
        return provideServerSettingLocalDataSource(this.module);
    }
}
