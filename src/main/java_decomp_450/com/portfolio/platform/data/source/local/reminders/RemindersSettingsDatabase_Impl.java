package com.portfolio.platform.data.source.local.reminders;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemindersSettingsDatabase_Impl extends RemindersSettingsDatabase {
    @DexIgnore
    public volatile InactivityNudgeTimeDao _inactivityNudgeTimeDao;
    @DexIgnore
    public volatile RemindTimeDao _remindTimeDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `inactivityNudgeTimeModel` (`nudgeTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, `nudgeTimeType` INTEGER NOT NULL, PRIMARY KEY(`nudgeTimeName`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `remindTimeModel` (`remindTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, PRIMARY KEY(`remindTimeName`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '0ee434ba350bbb753b81bda456b5c107')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `inactivityNudgeTimeModel`");
            wiVar.execSQL("DROP TABLE IF EXISTS `remindTimeModel`");
            if (((ci) RemindersSettingsDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) RemindersSettingsDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) RemindersSettingsDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) RemindersSettingsDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) RemindersSettingsDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) RemindersSettingsDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) RemindersSettingsDatabase_Impl.this).mDatabase = wiVar;
            RemindersSettingsDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) RemindersSettingsDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) RemindersSettingsDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) RemindersSettingsDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("nudgeTimeName", new ti.a("nudgeTimeName", "TEXT", true, 1, null, 1));
            hashMap.put("minutes", new ti.a("minutes", "INTEGER", true, 0, null, 1));
            hashMap.put("nudgeTimeType", new ti.a("nudgeTimeType", "INTEGER", true, 0, null, 1));
            ti tiVar = new ti("inactivityNudgeTimeModel", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "inactivityNudgeTimeModel");
            if (!tiVar.equals(a)) {
                return new ei.b(false, "inactivityNudgeTimeModel(com.portfolio.platform.data.InactivityNudgeTimeModel).\n Expected:\n" + tiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(2);
            hashMap2.put("remindTimeName", new ti.a("remindTimeName", "TEXT", true, 1, null, 1));
            hashMap2.put("minutes", new ti.a("minutes", "INTEGER", true, 0, null, 1));
            ti tiVar2 = new ti("remindTimeModel", hashMap2, new HashSet(0), new HashSet(0));
            ti a2 = ti.a(wiVar, "remindTimeModel");
            if (tiVar2.equals(a2)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "remindTimeModel(com.portfolio.platform.data.RemindTimeModel).\n Expected:\n" + tiVar2 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `inactivityNudgeTimeModel`");
            writableDatabase.execSQL("DELETE FROM `remindTimeModel`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "inactivityNudgeTimeModel", "remindTimeModel");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(1), "0ee434ba350bbb753b81bda456b5c107", "7e66671f5f316f81e6558b840c854b68");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase
    public InactivityNudgeTimeDao getInactivityNudgeTimeDao() {
        InactivityNudgeTimeDao inactivityNudgeTimeDao;
        if (this._inactivityNudgeTimeDao != null) {
            return this._inactivityNudgeTimeDao;
        }
        synchronized (this) {
            if (this._inactivityNudgeTimeDao == null) {
                this._inactivityNudgeTimeDao = new InactivityNudgeTimeDao_Impl(this);
            }
            inactivityNudgeTimeDao = this._inactivityNudgeTimeDao;
        }
        return inactivityNudgeTimeDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase
    public RemindTimeDao getRemindTimeDao() {
        RemindTimeDao remindTimeDao;
        if (this._remindTimeDao != null) {
            return this._remindTimeDao;
        }
        synchronized (this) {
            if (this._remindTimeDao == null) {
                this._remindTimeDao = new RemindTimeDao_Impl(this);
            }
            remindTimeDao = this._remindTimeDao;
        }
        return remindTimeDao;
    }
}
