package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.bv4;
import com.fossil.ci;
import com.fossil.cv4;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetDao_Impl implements DianaPresetDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<DianaPreset> __insertionAdapterOfDianaPreset;
    @DexIgnore
    public /* final */ vh<DianaRecommendPreset> __insertionAdapterOfDianaRecommendPreset;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAllPresetBySerial;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearDianaPresetTable;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearDianaRecommendPresetTable;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeletePreset;
    @DexIgnore
    public /* final */ ji __preparedStmtOfRemoveAllDeletePinTypePreset;
    @DexIgnore
    public /* final */ bv4 __presetComplicationSettingTypeConverter; // = new bv4();
    @DexIgnore
    public /* final */ cv4 __presetWatchAppSettingTypeConverter; // = new cv4();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<DianaRecommendPreset> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaRecommendPreset` (`serialNumber`,`id`,`name`,`isDefault`,`complications`,`watchapps`,`watchFaceId`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, DianaRecommendPreset dianaRecommendPreset) {
            if (dianaRecommendPreset.getSerialNumber() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, dianaRecommendPreset.getSerialNumber());
            }
            if (dianaRecommendPreset.getId() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, dianaRecommendPreset.getId());
            }
            if (dianaRecommendPreset.getName() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, dianaRecommendPreset.getName());
            }
            ajVar.bindLong(4, dianaRecommendPreset.isDefault() ? 1 : 0);
            String a = DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(dianaRecommendPreset.getComplications());
            if (a == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a);
            }
            String a2 = DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(dianaRecommendPreset.getWatchapps());
            if (a2 == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, a2);
            }
            if (dianaRecommendPreset.getWatchFaceId() == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, dianaRecommendPreset.getWatchFaceId());
            }
            if (dianaRecommendPreset.getCreatedAt() == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, dianaRecommendPreset.getCreatedAt());
            }
            if (dianaRecommendPreset.getUpdatedAt() == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindString(9, dianaRecommendPreset.getUpdatedAt());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh<DianaPreset> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaPreset` (`createdAt`,`updatedAt`,`pinType`,`id`,`serialNumber`,`name`,`isActive`,`complications`,`watchapps`,`watchFaceId`) VALUES (?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, DianaPreset dianaPreset) {
            if (dianaPreset.getCreatedAt() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, dianaPreset.getCreatedAt());
            }
            if (dianaPreset.getUpdatedAt() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, dianaPreset.getUpdatedAt());
            }
            ajVar.bindLong(3, (long) dianaPreset.getPinType());
            if (dianaPreset.getId() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, dianaPreset.getId());
            }
            if (dianaPreset.getSerialNumber() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, dianaPreset.getSerialNumber());
            }
            if (dianaPreset.getName() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, dianaPreset.getName());
            }
            ajVar.bindLong(7, dianaPreset.isActive() ? 1 : 0);
            String a = DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(dianaPreset.getComplications());
            if (a == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, a);
            }
            String a2 = DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(dianaPreset.getWatchapps());
            if (a2 == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindString(9, a2);
            }
            if (dianaPreset.getWatchFaceId() == null) {
                ajVar.bindNull(10);
            } else {
                ajVar.bindString(10, dianaPreset.getWatchFaceId());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM dianaPreset WHERE serialNumber=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends ji {
        @DexIgnore
        public Anon4(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM dianaPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends ji {
        @DexIgnore
        public Anon5(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM dianaRecommendPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends ji {
        @DexIgnore
        public Anon6(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM dianaPreset WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends ji {
        @DexIgnore
        public Anon7(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM dianaPreset WHERE pinType = 3";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon8(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public DianaPreset call() throws Exception {
            DianaPreset dianaPreset = null;
            Cursor a = pi.a(DianaPresetDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "createdAt");
                int b2 = oi.b(a, "updatedAt");
                int b3 = oi.b(a, "pinType");
                int b4 = oi.b(a, "id");
                int b5 = oi.b(a, "serialNumber");
                int b6 = oi.b(a, "name");
                int b7 = oi.b(a, "isActive");
                int b8 = oi.b(a, "complications");
                int b9 = oi.b(a, "watchapps");
                int b10 = oi.b(a, "watchFaceId");
                if (a.moveToFirst()) {
                    dianaPreset = new DianaPreset(a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7) != 0, DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(a.getString(b8)), DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(a.getString(b9)), a.getString(b10));
                    dianaPreset.setCreatedAt(a.getString(b));
                    dianaPreset.setUpdatedAt(a.getString(b2));
                    dianaPreset.setPinType(a.getInt(b3));
                }
                return dianaPreset;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 implements Callable<List<DianaPreset>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon9(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DianaPreset> call() throws Exception {
            Cursor a = pi.a(DianaPresetDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "createdAt");
                int b2 = oi.b(a, "updatedAt");
                int b3 = oi.b(a, "pinType");
                int b4 = oi.b(a, "id");
                int b5 = oi.b(a, "serialNumber");
                int b6 = oi.b(a, "name");
                int b7 = oi.b(a, "isActive");
                int b8 = oi.b(a, "complications");
                int b9 = oi.b(a, "watchapps");
                int b10 = oi.b(a, "watchFaceId");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    DianaPreset dianaPreset = new DianaPreset(a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7) != 0, DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(a.getString(b8)), DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(a.getString(b9)), a.getString(b10));
                    dianaPreset.setCreatedAt(a.getString(b));
                    dianaPreset.setUpdatedAt(a.getString(b2));
                    dianaPreset.setPinType(a.getInt(b3));
                    arrayList.add(dianaPreset);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public DianaPresetDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfDianaRecommendPreset = new Anon1(ciVar);
        this.__insertionAdapterOfDianaPreset = new Anon2(ciVar);
        this.__preparedStmtOfClearAllPresetBySerial = new Anon3(ciVar);
        this.__preparedStmtOfClearDianaPresetTable = new Anon4(ciVar);
        this.__preparedStmtOfClearDianaRecommendPresetTable = new Anon5(ciVar);
        this.__preparedStmtOfDeletePreset = new Anon6(ciVar);
        this.__preparedStmtOfRemoveAllDeletePinTypePreset = new Anon7(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void clearAllPresetBySerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAllPresetBySerial.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllPresetBySerial.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void clearDianaPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearDianaPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearDianaPresetTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void clearDianaRecommendPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearDianaRecommendPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearDianaRecommendPresetTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void deletePreset(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeletePreset.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeletePreset.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public DianaPreset getActivePresetBySerial(String str) {
        fi b = fi.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        DianaPreset dianaPreset = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "createdAt");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, "pinType");
            int b5 = oi.b(a, "id");
            int b6 = oi.b(a, "serialNumber");
            int b7 = oi.b(a, "name");
            int b8 = oi.b(a, "isActive");
            int b9 = oi.b(a, "complications");
            int b10 = oi.b(a, "watchapps");
            int b11 = oi.b(a, "watchFaceId");
            if (a.moveToFirst()) {
                dianaPreset = new DianaPreset(a.getString(b5), a.getString(b6), a.getString(b7), a.getInt(b8) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b9)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b10)), a.getString(b11));
                dianaPreset.setCreatedAt(a.getString(b2));
                dianaPreset.setUpdatedAt(a.getString(b3));
                dianaPreset.setPinType(a.getInt(b4));
            }
            return dianaPreset;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public LiveData<DianaPreset> getActivePresetBySerialLiveData(String str) {
        fi b = fi.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"dianaPreset"}, false, (Callable) new Anon8(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public List<DianaPreset> getAllPendingPreset(String str) {
        fi b = fi.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 0", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "createdAt");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, "pinType");
            int b5 = oi.b(a, "id");
            int b6 = oi.b(a, "serialNumber");
            int b7 = oi.b(a, "name");
            int b8 = oi.b(a, "isActive");
            int b9 = oi.b(a, "complications");
            int b10 = oi.b(a, "watchapps");
            int b11 = oi.b(a, "watchFaceId");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(a.getString(b5), a.getString(b6), a.getString(b7), a.getInt(b8) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b9)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b10)), a.getString(b11));
                dianaPreset.setCreatedAt(a.getString(b2));
                dianaPreset.setUpdatedAt(a.getString(b3));
                dianaPreset.setPinType(a.getInt(b4));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public List<DianaPreset> getAllPreset(String str) {
        fi b = fi.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC ", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "createdAt");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, "pinType");
            int b5 = oi.b(a, "id");
            int b6 = oi.b(a, "serialNumber");
            int b7 = oi.b(a, "name");
            int b8 = oi.b(a, "isActive");
            int b9 = oi.b(a, "complications");
            int b10 = oi.b(a, "watchapps");
            int b11 = oi.b(a, "watchFaceId");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(a.getString(b5), a.getString(b6), a.getString(b7), a.getInt(b8) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b9)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b10)), a.getString(b11));
                dianaPreset.setCreatedAt(a.getString(b2));
                dianaPreset.setUpdatedAt(a.getString(b3));
                dianaPreset.setPinType(a.getInt(b4));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public LiveData<List<DianaPreset>> getAllPresetAsLiveData(String str) {
        fi b = fi.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"dianaPreset"}, false, (Callable) new Anon9(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public List<DianaRecommendPreset> getDianaRecommendPresetList(String str) {
        fi b = fi.b("SELECT * FROM dianaRecommendPreset WHERE serialNumber=?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "serialNumber");
            int b3 = oi.b(a, "id");
            int b4 = oi.b(a, "name");
            int b5 = oi.b(a, "isDefault");
            int b6 = oi.b(a, "complications");
            int b7 = oi.b(a, "watchapps");
            int b8 = oi.b(a, "watchFaceId");
            int b9 = oi.b(a, "createdAt");
            int b10 = oi.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new DianaRecommendPreset(a.getString(b2), a.getString(b3), a.getString(b4), a.getInt(b5) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b6)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b7)), a.getString(b8), a.getString(b9), a.getString(b10)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public DianaPreset getPresetById(String str) {
        fi b = fi.b("SELECT * FROM dianaPreset WHERE id=? AND pinType != 3", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        DianaPreset dianaPreset = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "createdAt");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, "pinType");
            int b5 = oi.b(a, "id");
            int b6 = oi.b(a, "serialNumber");
            int b7 = oi.b(a, "name");
            int b8 = oi.b(a, "isActive");
            int b9 = oi.b(a, "complications");
            int b10 = oi.b(a, "watchapps");
            int b11 = oi.b(a, "watchFaceId");
            if (a.moveToFirst()) {
                dianaPreset = new DianaPreset(a.getString(b5), a.getString(b6), a.getString(b7), a.getInt(b8) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b9)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b10)), a.getString(b11));
                dianaPreset.setCreatedAt(a.getString(b2));
                dianaPreset.setUpdatedAt(a.getString(b3));
                dianaPreset.setPinType(a.getInt(b4));
            }
            return dianaPreset;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public List<DianaPreset> getPresetListByWatchFaceId(String str) {
        fi b = fi.b("SELECT * FROM dianaPreset WHERE watchFaceId=? AND pinType != 3", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "createdAt");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, "pinType");
            int b5 = oi.b(a, "id");
            int b6 = oi.b(a, "serialNumber");
            int b7 = oi.b(a, "name");
            int b8 = oi.b(a, "isActive");
            int b9 = oi.b(a, "complications");
            int b10 = oi.b(a, "watchapps");
            int b11 = oi.b(a, "watchFaceId");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(a.getString(b5), a.getString(b6), a.getString(b7), a.getInt(b8) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b9)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b10)), a.getString(b11));
                dianaPreset.setCreatedAt(a.getString(b2));
                dianaPreset.setUpdatedAt(a.getString(b3));
                dianaPreset.setPinType(a.getInt(b4));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void removeAllDeletePinTypePreset() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfRemoveAllDeletePinTypePreset.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAllDeletePinTypePreset.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void upsertDianaRecommendPresetList(List<DianaRecommendPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaRecommendPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void upsertPreset(DianaPreset dianaPreset) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaPreset.insert(dianaPreset);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void upsertPresetList(List<DianaPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
