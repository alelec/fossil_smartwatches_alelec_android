package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.lx6;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.qx6;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.ti7;
import com.fossil.vh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getLastGoalSettingLiveData$2", f = "GoalTrackingRepository.kt", l = {87}, m = "invokeSuspend")
public final class GoalTrackingRepository$getLastGoalSettingLiveData$Anon2 extends zb7 implements kd7<yi7, fb7<? super LiveData<qx6<? extends Integer>>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends lx6<Integer, GoalSetting> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDatabase $fitnessDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingRepository$getLastGoalSettingLiveData$Anon2 this$0;

        @DexIgnore
        public Anon1_Level2(GoalTrackingRepository$getLastGoalSettingLiveData$Anon2 goalTrackingRepository$getLastGoalSettingLiveData$Anon2, GoalTrackingDatabase goalTrackingDatabase) {
            this.this$0 = goalTrackingRepository$getLastGoalSettingLiveData$Anon2;
            this.$fitnessDatabase = goalTrackingDatabase;
        }

        @DexIgnore
        @Override // com.fossil.lx6
        public Object createCall(fb7<? super fv7<GoalSetting>> fb7) {
            return this.this$0.this$0.mApiServiceV2.getGoalSetting(fb7);
        }

        @DexIgnore
        @Override // com.fossil.lx6
        public Object loadFromDb(fb7<? super LiveData<Integer>> fb7) {
            return this.$fitnessDatabase.getGoalTrackingDao().getLastGoalSettingLiveData();
        }

        @DexIgnore
        @Override // com.fossil.lx6
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "getLastGoalSettingLiveData onFetchFailed");
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
        @Override // com.fossil.lx6
        public /* bridge */ /* synthetic */ Object saveCallResult(GoalSetting goalSetting, fb7 fb7) {
            return saveCallResult(goalSetting, (fb7<? super i97>) fb7);
        }

        @DexIgnore
        public boolean shouldFetch(Integer num) {
            return true;
        }

        @DexIgnore
        public Object saveCallResult(GoalSetting goalSetting, fb7<? super i97> fb7) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = GoalTrackingRepository.Companion.getTAG();
            local.d(tag, "getLastGoalSettingLiveData saveCallResult goal: " + goalSetting);
            Object saveSettingToDB = this.this$0.this$0.saveSettingToDB(goalSetting, fb7);
            if (saveSettingToDB == nb7.a()) {
                return saveSettingToDB;
            }
            return i97.a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getLastGoalSettingLiveData$Anon2(GoalTrackingRepository goalTrackingRepository, fb7 fb7) {
        super(2, fb7);
        this.this$0 = goalTrackingRepository;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        GoalTrackingRepository$getLastGoalSettingLiveData$Anon2 goalTrackingRepository$getLastGoalSettingLiveData$Anon2 = new GoalTrackingRepository$getLastGoalSettingLiveData$Anon2(this.this$0, fb7);
        goalTrackingRepository$getLastGoalSettingLiveData$Anon2.p$ = (yi7) obj;
        return goalTrackingRepository$getLastGoalSettingLiveData$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LiveData<qx6<? extends Integer>>> fb7) {
        return ((GoalTrackingRepository$getLastGoalSettingLiveData$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            ti7 b = qj7.b();
            GoalTrackingRepository$getLastGoalSettingLiveData$Anon2$fitnessDatabase$Anon1_Level2 goalTrackingRepository$getLastGoalSettingLiveData$Anon2$fitnessDatabase$Anon1_Level2 = new GoalTrackingRepository$getLastGoalSettingLiveData$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = yi7;
            this.label = 1;
            obj = vh7.a(b, goalTrackingRepository$getLastGoalSettingLiveData$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return new Anon1_Level2(this, (GoalTrackingDatabase) obj).asLiveData();
    }
}
