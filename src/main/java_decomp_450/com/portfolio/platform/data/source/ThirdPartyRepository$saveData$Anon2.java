package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$saveData$2", f = "ThirdPartyRepository.kt", l = {49}, m = "invokeSuspend")
public final class ThirdPartyRepository$saveData$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GFitActiveTime $gFitActiveTime;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitHeartRate;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitSample;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitWorkoutSession;
    @DexIgnore
    public /* final */ /* synthetic */ List $listMFSleepSession;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyDatabase $thirdPartyDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$saveData$Anon2 this$0;

        @DexIgnore
        public Anon1_Level2(ThirdPartyRepository$saveData$Anon2 thirdPartyRepository$saveData$Anon2, ThirdPartyDatabase thirdPartyDatabase) {
            this.this$0 = thirdPartyRepository$saveData$Anon2;
            this.$thirdPartyDatabase = thirdPartyDatabase;
        }

        @DexIgnore
        public final void run() {
            if (!this.this$0.$listGFitSample.isEmpty()) {
                this.$thirdPartyDatabase.getGFitSampleDao().insertListGFitSample(this.this$0.$listGFitSample);
            }
            if (this.this$0.$gFitActiveTime != null) {
                this.$thirdPartyDatabase.getGFitActiveTimeDao().insertGFitActiveTime(this.this$0.$gFitActiveTime);
            }
            if (!this.this$0.$listGFitHeartRate.isEmpty()) {
                this.$thirdPartyDatabase.getGFitHeartRateDao().insertListGFitHeartRate(this.this$0.$listGFitHeartRate);
            }
            if (!this.this$0.$listGFitWorkoutSession.isEmpty()) {
                this.$thirdPartyDatabase.getGFitWorkoutSessionDao().insertListGFitWorkoutSession(this.this$0.$listGFitWorkoutSession);
            }
            if (!this.this$0.$listMFSleepSession.isEmpty()) {
                ThirdPartyRepository$saveData$Anon2 thirdPartyRepository$saveData$Anon2 = this.this$0;
                this.$thirdPartyDatabase.getGFitSleepDao().insertListGFitSleep(thirdPartyRepository$saveData$Anon2.this$0.convertListMFSleepSessionToListGFitSleep(thirdPartyRepository$saveData$Anon2.$listMFSleepSession));
                FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "listMFSleepSession.isNotEmpty");
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThirdPartyRepository$saveData$Anon2(ThirdPartyRepository thirdPartyRepository, List list, GFitActiveTime gFitActiveTime, List list2, List list3, List list4, fb7 fb7) {
        super(2, fb7);
        this.this$0 = thirdPartyRepository;
        this.$listGFitSample = list;
        this.$gFitActiveTime = gFitActiveTime;
        this.$listGFitHeartRate = list2;
        this.$listGFitWorkoutSession = list3;
        this.$listMFSleepSession = list4;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        ThirdPartyRepository$saveData$Anon2 thirdPartyRepository$saveData$Anon2 = new ThirdPartyRepository$saveData$Anon2(this.this$0, this.$listGFitSample, this.$gFitActiveTime, this.$listGFitHeartRate, this.$listGFitWorkoutSession, this.$listMFSleepSession, fb7);
        thirdPartyRepository$saveData$Anon2.p$ = (yi7) obj;
        return thirdPartyRepository$saveData$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((ThirdPartyRepository$saveData$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.label = 1;
            obj = pg5.e(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ThirdPartyDatabase thirdPartyDatabase = (ThirdPartyDatabase) obj;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(ThirdPartyRepository.TAG, "saveData = listGFitSample=" + this.$listGFitSample + ", gFitActiveTime=" + this.$gFitActiveTime + ", listGFitHeartRate=" + this.$listGFitHeartRate + ", listGFitWorkoutSession=" + this.$listGFitWorkoutSession + ", listMFSleepSession= " + this.$listMFSleepSession);
        thirdPartyDatabase.runInTransaction(new Anon1_Level2(this, thirdPartyDatabase));
        return i97.a;
    }
}
