package com.portfolio.platform.data.source;

import com.fossil.ik7;
import com.fossil.qj7;
import com.fossil.xh7;
import com.fossil.zi7;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerSettingRepository$getServerSettingList$Anon1 implements ServerSettingDataSource.OnGetServerSettingList {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingDataSource.OnGetServerSettingList $callback;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRepository this$0;

    @DexIgnore
    public ServerSettingRepository$getServerSettingList$Anon1(ServerSettingRepository serverSettingRepository, ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        this.this$0 = serverSettingRepository;
        this.$callback = onGetServerSettingList;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
    public void onFailed(int i) {
        this.$callback.onFailed(i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
    public void onSuccess(ServerSettingList serverSettingList) {
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2(this, serverSettingList, null), 3, null);
        this.$callback.onSuccess(serverSettingList);
    }
}
