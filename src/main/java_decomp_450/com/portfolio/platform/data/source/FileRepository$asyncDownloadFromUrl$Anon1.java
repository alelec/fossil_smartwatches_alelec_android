package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.qg5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.LocalFile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileRepository$asyncDownloadFromUrl$Anon1 implements qg5.c {
    @DexIgnore
    public /* final */ /* synthetic */ String $remoteUrl;
    @DexIgnore
    public /* final */ /* synthetic */ FileRepository this$0;

    @DexIgnore
    public FileRepository$asyncDownloadFromUrl$Anon1(FileRepository fileRepository, String str) {
        this.this$0 = fileRepository;
        this.$remoteUrl = str;
    }

    @DexIgnore
    @Override // com.fossil.qg5.c
    public void onComplete(boolean z, LocalFile localFile) {
        ee7.b(localFile, "file");
        if (z) {
            this.this$0.mFileDao.upsertLocalFile(localFile);
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = FileRepository.Companion.getTAG();
        local.d(tag, "downloadFromURL with remoteUrl " + this.$remoteUrl + " false");
    }
}
