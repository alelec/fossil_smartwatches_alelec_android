package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.AlarmsRepository$insertAlarm$2", f = "AlarmsRepository.kt", l = {124, 126}, m = "invokeSuspend")
public final class AlarmsRepository$insertAlarm$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<Alarm>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Alarm $alarm;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$insertAlarm$Anon2(AlarmsRepository alarmsRepository, Alarm alarm, fb7 fb7) {
        super(2, fb7);
        this.this$0 = alarmsRepository;
        this.$alarm = alarm;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        AlarmsRepository$insertAlarm$Anon2 alarmsRepository$insertAlarm$Anon2 = new AlarmsRepository$insertAlarm$Anon2(this.this$0, this.$alarm, fb7);
        alarmsRepository$insertAlarm$Anon2.p$ = (yi7) obj;
        return alarmsRepository$insertAlarm$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<Alarm>> fb7) {
        return ((AlarmsRepository$insertAlarm$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = AlarmsRepository.TAG;
            local.d(access$getTAG$cp, "insertAlarm - alarmId=" + this.$alarm.getId());
            this.$alarm.setPinType(1);
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.label = 1;
            obj = pg5.a(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ((AlarmDatabase) obj).alarmDao().insertAlarm(this.$alarm);
        AlarmsRepository alarmsRepository = this.this$0;
        Alarm alarm = this.$alarm;
        this.L$0 = yi7;
        this.label = 2;
        obj = alarmsRepository.upsertAlarm(alarm, this);
        return obj == a ? a : obj;
    }
}
