package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.ActivitiesRepository$insert$2", f = "ActivitiesRepository.kt", l = {278, 297}, m = "invokeSuspend")
public final class ActivitiesRepository$insert$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<List<ActivitySample>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $activityList;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$insert$Anon2(ActivitiesRepository activitiesRepository, List list, fb7 fb7) {
        super(2, fb7);
        this.this$0 = activitiesRepository;
        this.$activityList = list;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        ActivitiesRepository$insert$Anon2 activitiesRepository$insert$Anon2 = new ActivitiesRepository$insert$Anon2(this.this$0, this.$activityList, fb7);
        activitiesRepository$insert$Anon2.p$ = (yi7) obj;
        return activitiesRepository$insert$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<List<ActivitySample>>> fb7) {
        return ((ActivitiesRepository$insert$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0168  */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r14) {
        /*
            r13 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r13.label
            r2 = 1
            r3 = 2
            r4 = 0
            if (r1 == 0) goto L_0x0038
            if (r1 == r2) goto L_0x0030
            if (r1 != r3) goto L_0x0028
            java.lang.Object r0 = r13.L$4
            com.fossil.ie4 r0 = (com.fossil.ie4) r0
            java.lang.Object r0 = r13.L$3
            com.google.gson.Gson r0 = (com.google.gson.Gson) r0
            java.lang.Object r0 = r13.L$2
            com.fossil.de4 r0 = (com.fossil.de4) r0
            java.lang.Object r0 = r13.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r13.L$0
            com.fossil.yi7 r0 = (com.fossil.yi7) r0
            com.fossil.t87.a(r14)
            goto L_0x010e
        L_0x0028:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r14.<init>(r0)
            throw r14
        L_0x0030:
            java.lang.Object r1 = r13.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r14)
            goto L_0x0074
        L_0x0038:
            com.fossil.t87.a(r14)
            com.fossil.yi7 r1 = r13.p$
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            com.portfolio.platform.data.source.ActivitiesRepository$Companion r5 = com.portfolio.platform.data.source.ActivitiesRepository.Companion
            java.lang.String r5 = r5.getTAG$app_fossilRelease()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "insertActivities: sampleRawList ="
            r6.append(r7)
            java.util.List r7 = r13.$activityList
            int r7 = r7.size()
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r14.d(r5, r6)
            com.portfolio.platform.data.source.ActivitiesRepository r14 = r13.this$0
            com.portfolio.platform.data.source.UserRepository r14 = r14.mUserRepository
            r13.L$0 = r1
            r13.label = r2
            java.lang.Object r14 = r14.getCurrentUser(r13)
            if (r14 != r0) goto L_0x0074
            return r0
        L_0x0074:
            com.portfolio.platform.data.model.MFUser r14 = (com.portfolio.platform.data.model.MFUser) r14
            if (r14 == 0) goto L_0x007d
            java.lang.String r14 = r14.getUserId()
            goto L_0x007e
        L_0x007d:
            r14 = r4
        L_0x007e:
            boolean r2 = android.text.TextUtils.isEmpty(r14)
            if (r2 != 0) goto L_0x01c6
            com.fossil.de4 r2 = new com.fossil.de4
            r2.<init>()
            com.portfolio.platform.data.Activity$Companion r5 = com.portfolio.platform.data.Activity.Companion
            com.google.gson.Gson r5 = r5.gsonConverter()
            java.util.List r6 = r13.$activityList
            java.util.Iterator r6 = r6.iterator()
        L_0x0095:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x00ec
            java.lang.Object r7 = r6.next()
            com.portfolio.platform.data.model.room.fitness.SampleRaw r7 = (com.portfolio.platform.data.model.room.fitness.SampleRaw) r7
            com.portfolio.platform.data.Activity$Companion r8 = com.portfolio.platform.data.Activity.Companion
            if (r14 == 0) goto L_0x00e8
            com.portfolio.platform.data.model.room.fitness.ActivitySample r7 = r7.toActivitySample()
            com.portfolio.platform.data.Activity r7 = r8.toActivity(r14, r7)
            com.google.gson.JsonElement r8 = r5.b(r7)
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            com.portfolio.platform.data.source.ActivitiesRepository$Companion r10 = com.portfolio.platform.data.source.ActivitiesRepository.Companion
            java.lang.String r10 = r10.getTAG$app_fossilRelease()
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "activity "
            r11.append(r12)
            r11.append(r7)
            java.lang.String r7 = " json "
            r11.append(r7)
            r11.append(r8)
            java.lang.String r7 = r11.toString()
            r9.d(r10, r7)
            java.lang.String r7 = "jsonTree"
            com.fossil.ee7.a(r8, r7)
            boolean r7 = r8.h()
            if (r7 != 0) goto L_0x0095
            r2.a(r8)
            goto L_0x0095
        L_0x00e8:
            com.fossil.ee7.a()
            throw r4
        L_0x00ec:
            com.fossil.ie4 r6 = new com.fossil.ie4
            r6.<init>()
            java.lang.String r7 = "_items"
            r6.a(r7, r2)
            com.portfolio.platform.data.source.ActivitiesRepository$insert$Anon2$repoResponse$Anon1_Level2 r7 = new com.portfolio.platform.data.source.ActivitiesRepository$insert$Anon2$repoResponse$Anon1_Level2
            r7.<init>(r13, r6, r4)
            r13.L$0 = r1
            r13.L$1 = r14
            r13.L$2 = r2
            r13.L$3 = r5
            r13.L$4 = r6
            r13.label = r3
            java.lang.Object r14 = com.fossil.aj5.a(r7, r13)
            if (r14 != r0) goto L_0x010e
            return r0
        L_0x010e:
            com.fossil.zi5 r14 = (com.fossil.zi5) r14
            boolean r0 = r14 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x0168
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            com.portfolio.platform.data.source.ActivitiesRepository$Companion r1 = com.portfolio.platform.data.source.ActivitiesRepository.Companion
            java.lang.String r1 = r1.getTAG$app_fossilRelease()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r5 = "insertActivity onResponse: response = "
            r2.append(r5)
            r2.append(r14)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            com.fossil.bj5 r14 = (com.fossil.bj5) r14
            java.lang.Object r14 = r14.a()
            com.portfolio.platform.data.source.remote.ApiResponse r14 = (com.portfolio.platform.data.source.remote.ApiResponse) r14
            if (r14 == 0) goto L_0x0161
            java.util.List r14 = r14.get_items()
            if (r14 == 0) goto L_0x0161
            java.util.Iterator r14 = r14.iterator()
        L_0x014d:
            boolean r1 = r14.hasNext()
            if (r1 == 0) goto L_0x0161
            java.lang.Object r1 = r14.next()
            com.portfolio.platform.data.Activity r1 = (com.portfolio.platform.data.Activity) r1
            com.portfolio.platform.data.model.room.fitness.ActivitySample r1 = r1.toActivitySample()
            r0.add(r1)
            goto L_0x014d
        L_0x0161:
            com.fossil.bj5 r14 = new com.fossil.bj5
            r1 = 0
            r14.<init>(r0, r1, r3, r4)
            goto L_0x01bf
        L_0x0168:
            boolean r0 = r14 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x01c0
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            com.portfolio.platform.data.source.ActivitiesRepository$Companion r1 = com.portfolio.platform.data.source.ActivitiesRepository.Companion
            java.lang.String r1 = r1.getTAG$app_fossilRelease()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "insertActivity Failure code="
            r2.append(r3)
            com.fossil.yi5 r14 = (com.fossil.yi5) r14
            int r3 = r14.a()
            r2.append(r3)
            java.lang.String r3 = " message="
            r2.append(r3)
            com.portfolio.platform.data.model.ServerError r3 = r14.c()
            if (r3 == 0) goto L_0x019a
            java.lang.String r4 = r3.getMessage()
        L_0x019a:
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            com.fossil.yi5 r0 = new com.fossil.yi5
            int r4 = r14.a()
            com.portfolio.platform.data.model.ServerError r5 = r14.c()
            java.lang.Throwable r6 = r14.d()
            java.lang.String r7 = r14.b()
            r8 = 0
            r9 = 16
            r10 = 0
            r3 = r0
            r3.<init>(r4, r5, r6, r7, r8, r9, r10)
            r14 = r0
        L_0x01bf:
            return r14
        L_0x01c0:
            com.fossil.p87 r14 = new com.fossil.p87
            r14.<init>()
            throw r14
        L_0x01c6:
            com.fossil.yi5 r14 = new com.fossil.yi5
            r1 = 600(0x258, float:8.41E-43)
            com.portfolio.platform.data.model.ServerError r2 = new com.portfolio.platform.data.model.ServerError
            r0 = 600(0x258, float:8.41E-43)
            java.lang.String r3 = ""
            r2.<init>(r0, r3)
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r14
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ActivitiesRepository$insert$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
