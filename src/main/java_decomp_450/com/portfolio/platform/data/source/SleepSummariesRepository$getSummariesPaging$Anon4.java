package com.portfolio.platform.data.source;

import com.fossil.fe7;
import com.fossil.i97;
import com.fossil.te5;
import com.fossil.vc7;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository$getSummariesPaging$Anon4 extends fe7 implements vc7<i97> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSummariesPaging$Anon4(SleepSummaryDataSourceFactory sleepSummaryDataSourceFactory) {
        super(0);
        this.$sourceFactory = sleepSummaryDataSourceFactory;
    }

    @DexIgnore
    @Override // com.fossil.vc7
    public final void invoke() {
        te5 mHelper;
        SleepSummaryLocalDataSource a = this.$sourceFactory.getSourceLiveData().a();
        if (a != null && (mHelper = a.getMHelper()) != null) {
            mHelper.b();
        }
    }
}
