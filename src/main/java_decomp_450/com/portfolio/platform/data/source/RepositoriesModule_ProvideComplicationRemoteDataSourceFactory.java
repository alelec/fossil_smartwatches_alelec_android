package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RepositoriesModule_ProvideComplicationRemoteDataSourceFactory implements Factory<ComplicationRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiServiceV2Provider;
    @DexIgnore
    public /* final */ RepositoriesModule module;

    @DexIgnore
    public RepositoriesModule_ProvideComplicationRemoteDataSourceFactory(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        this.module = repositoriesModule;
        this.apiServiceV2Provider = provider;
    }

    @DexIgnore
    public static RepositoriesModule_ProvideComplicationRemoteDataSourceFactory create(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        return new RepositoriesModule_ProvideComplicationRemoteDataSourceFactory(repositoriesModule, provider);
    }

    @DexIgnore
    public static ComplicationRemoteDataSource provideComplicationRemoteDataSource(RepositoriesModule repositoriesModule, ApiServiceV2 apiServiceV2) {
        ComplicationRemoteDataSource provideComplicationRemoteDataSource = repositoriesModule.provideComplicationRemoteDataSource(apiServiceV2);
        c87.a(provideComplicationRemoteDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideComplicationRemoteDataSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ComplicationRemoteDataSource get() {
        return provideComplicationRemoteDataSource(this.module, this.apiServiceV2Provider.get());
    }
}
