package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.fossil.vn4;
import com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesSocialFriendDaoFactory implements Factory<vn4> {
    @DexIgnore
    public /* final */ Provider<BuddyChallengeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesSocialFriendDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<BuddyChallengeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesSocialFriendDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<BuddyChallengeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvidesSocialFriendDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static vn4 providesSocialFriendDao(PortfolioDatabaseModule portfolioDatabaseModule, BuddyChallengeDatabase buddyChallengeDatabase) {
        vn4 providesSocialFriendDao = portfolioDatabaseModule.providesSocialFriendDao(buddyChallengeDatabase);
        c87.a(providesSocialFriendDao, "Cannot return null from a non-@Nullable @Provides method");
        return providesSocialFriendDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public vn4 get() {
        return providesSocialFriendDao(this.module, this.dbProvider.get());
    }
}
