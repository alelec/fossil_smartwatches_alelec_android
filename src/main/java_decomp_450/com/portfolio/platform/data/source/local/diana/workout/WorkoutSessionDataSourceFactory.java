package com.portfolio.platform.data.source.local.diana.workout;

import androidx.lifecycle.MutableLiveData;
import com.fossil.ee7;
import com.fossil.lf;
import com.fossil.pj4;
import com.fossil.te5;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionDataSourceFactory extends lf.b<Long, WorkoutSession> {
    @DexIgnore
    public /* final */ pj4 appExecutors;
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public /* final */ te5.a listener;
    @DexIgnore
    public WorkoutSessionLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ MutableLiveData<WorkoutSessionLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ FitnessDatabase workoutDatabase;
    @DexIgnore
    public /* final */ WorkoutSessionRepository workoutSessionRepository;

    @DexIgnore
    public WorkoutSessionDataSourceFactory(WorkoutSessionRepository workoutSessionRepository2, FitnessDatabase fitnessDatabase, Date date, pj4 pj4, te5.a aVar) {
        ee7.b(workoutSessionRepository2, "workoutSessionRepository");
        ee7.b(fitnessDatabase, "workoutDatabase");
        ee7.b(date, "currentDate");
        ee7.b(pj4, "appExecutors");
        ee7.b(aVar, "listener");
        this.workoutSessionRepository = workoutSessionRepository2;
        this.workoutDatabase = fitnessDatabase;
        this.currentDate = date;
        this.appExecutors = pj4;
        this.listener = aVar;
    }

    @DexIgnore
    @Override // com.fossil.lf.b
    public lf<Long, WorkoutSession> create() {
        WorkoutSessionLocalDataSource workoutSessionLocalDataSource = new WorkoutSessionLocalDataSource(this.workoutSessionRepository, this.workoutDatabase, this.currentDate, this.appExecutors, this.listener);
        this.localDataSource = workoutSessionLocalDataSource;
        this.sourceLiveData.a(workoutSessionLocalDataSource);
        WorkoutSessionLocalDataSource workoutSessionLocalDataSource2 = this.localDataSource;
        if (workoutSessionLocalDataSource2 != null) {
            return workoutSessionLocalDataSource2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final WorkoutSessionLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<WorkoutSessionLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(WorkoutSessionLocalDataSource workoutSessionLocalDataSource) {
        this.localDataSource = workoutSessionLocalDataSource;
    }
}
