package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.qx6;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.ti7;
import com.fossil.vh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SummariesRepository$getSummary$2", f = "SummariesRepository.kt", l = {160}, m = "invokeSuspend")
public final class SummariesRepository$getSummary$Anon2 extends zb7 implements kd7<yi7, fb7<? super LiveData<qx6<? extends ActivitySummary>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummary$Anon2(SummariesRepository summariesRepository, Date date, fb7 fb7) {
        super(2, fb7);
        this.this$0 = summariesRepository;
        this.$date = date;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SummariesRepository$getSummary$Anon2 summariesRepository$getSummary$Anon2 = new SummariesRepository$getSummary$Anon2(this.this$0, this.$date, fb7);
        summariesRepository$getSummary$Anon2.p$ = (yi7) obj;
        return summariesRepository$getSummary$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LiveData<qx6<? extends ActivitySummary>>> fb7) {
        return ((SummariesRepository$getSummary$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "getSummary - date=" + this.$date);
            ti7 b = qj7.b();
            SummariesRepository$getSummary$Anon2$fitnessDatabase$Anon1_Level2 summariesRepository$getSummary$Anon2$fitnessDatabase$Anon1_Level2 = new SummariesRepository$getSummary$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = yi7;
            this.label = 1;
            obj = vh7.a(b, summariesRepository$getSummary$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) obj;
        FitnessDataDao fitnessDataDao = fitnessDatabase.getFitnessDataDao();
        Date date = this.$date;
        LiveData b2 = ge.b(fitnessDataDao.getFitnessDataLiveData(date, date), new SummariesRepository$getSummary$Anon2$result$Anon1_Level2(this, fitnessDatabase));
        ee7.a((Object) b2, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(SummariesRepository.TAG, "XXX- summaryLiveObj " + b2);
        return b2;
    }
}
