package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SummariesRepository$getSummary$4", f = "SummariesRepository.kt", l = {382}, m = "invokeSuspend")
public final class SummariesRepository$getSummary$Anon4 extends zb7 implements kd7<yi7, fb7<? super ActivitySummary>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Calendar $date;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummary$Anon4(Calendar calendar, fb7 fb7) {
        super(2, fb7);
        this.$date = calendar;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SummariesRepository$getSummary$Anon4 summariesRepository$getSummary$Anon4 = new SummariesRepository$getSummary$Anon4(this.$date, fb7);
        summariesRepository$getSummary$Anon4.p$ = (yi7) obj;
        return summariesRepository$getSummary$Anon4;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super ActivitySummary> fb7) {
        return ((SummariesRepository$getSummary$Anon4) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.label = 1;
            obj = pg5.b(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ActivitySummaryDao activitySummaryDao = ((FitnessDatabase) obj).activitySummaryDao();
        Date time = this.$date.getTime();
        ee7.a((Object) time, "date.time");
        return activitySummaryDao.getActivitySummary(time);
    }
}
