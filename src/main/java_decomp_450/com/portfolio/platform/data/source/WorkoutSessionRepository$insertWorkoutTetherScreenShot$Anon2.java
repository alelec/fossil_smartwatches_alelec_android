package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.WorkoutSessionRepository$insertWorkoutTetherScreenShot$2", f = "WorkoutSessionRepository.kt", l = {ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 143}, m = "invokeSuspend")
public final class WorkoutSessionRepository$insertWorkoutTetherScreenShot$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $screenShotUri;
    @DexIgnore
    public /* final */ /* synthetic */ String $workoutId;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$insertWorkoutTetherScreenShot$Anon2(String str, String str2, fb7 fb7) {
        super(2, fb7);
        this.$workoutId = str;
        this.$screenShotUri = str2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        WorkoutSessionRepository$insertWorkoutTetherScreenShot$Anon2 workoutSessionRepository$insertWorkoutTetherScreenShot$Anon2 = new WorkoutSessionRepository$insertWorkoutTetherScreenShot$Anon2(this.$workoutId, this.$screenShotUri, fb7);
        workoutSessionRepository$insertWorkoutTetherScreenShot$Anon2.p$ = (yi7) obj;
        return workoutSessionRepository$insertWorkoutTetherScreenShot$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((WorkoutSessionRepository$insertWorkoutTetherScreenShot$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        WorkoutSession workoutSession;
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "insertWorkoutTetherScreenShot: workoutId = " + this.$workoutId + " screenshotUri " + this.$screenShotUri);
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.label = 1;
            obj = pg5.b(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            WorkoutSession workoutSession2 = (WorkoutSession) this.L$2;
            workoutSession = (WorkoutSession) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            ((FitnessDatabase) obj).getWorkoutDao().upsertWorkoutSession(workoutSession);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease2 = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
            local2.d(tAG$app_fossilRelease2, "insertWorkoutTetherScreenShot: workoutId = " + this.$workoutId + " DONE!!!");
            return i97.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        WorkoutSession workoutSessionById = ((FitnessDatabase) obj).getWorkoutDao().getWorkoutSessionById(this.$workoutId);
        if (workoutSessionById == null) {
            return null;
        }
        workoutSessionById.setScreenShotUri(this.$screenShotUri);
        pg5 pg52 = pg5.i;
        this.L$0 = yi7;
        this.L$1 = workoutSessionById;
        this.L$2 = workoutSessionById;
        this.label = 2;
        Object b = pg52.b(this);
        if (b == a) {
            return a;
        }
        workoutSession = workoutSessionById;
        obj = b;
        ((FitnessDatabase) obj).getWorkoutDao().upsertWorkoutSession(workoutSession);
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        String tAG$app_fossilRelease22 = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
        local22.d(tAG$app_fossilRelease22, "insertWorkoutTetherScreenShot: workoutId = " + this.$workoutId + " DONE!!!");
        return i97.a;
    }
}
