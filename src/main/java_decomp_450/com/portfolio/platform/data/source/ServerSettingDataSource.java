package com.portfolio.platform.data.source;

import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.ServerSettingList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ServerSettingDataSource {

    @DexIgnore
    public interface OnGetServerSettingList {
        @DexIgnore
        void onFailed(int i);

        @DexIgnore
        void onSuccess(ServerSettingList serverSettingList);
    }

    @DexIgnore
    void addOrUpdateServerSetting(ServerSetting serverSetting);

    @DexIgnore
    void addOrUpdateServerSettingList(List<ServerSetting> list);

    @DexIgnore
    void clearData();

    @DexIgnore
    ServerSetting getServerSettingByKey(String str);

    @DexIgnore
    void getServerSettingList(OnGetServerSettingList onGetServerSettingList);
}
