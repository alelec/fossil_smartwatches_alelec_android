package com.portfolio.platform.data.source.local.diana;

import com.portfolio.platform.data.model.diana.WatchAppData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface WatchAppDataDao {
    @DexIgnore
    void clearAll();

    @DexIgnore
    void deleteWatchAppDataById(String str);

    @DexIgnore
    List<WatchAppData> getAllWatchAppData();

    @DexIgnore
    void upsertWatchAppData(WatchAppData watchAppData);

    @DexIgnore
    void upsertWatchAppDataList(List<WatchAppData> list);
}
