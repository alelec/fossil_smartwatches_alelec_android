package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.zb7;
import com.portfolio.platform.data.model.WatchParameterResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getLatestWatchParamFromServer$response$1", f = "DeviceRemoteDataSource.kt", l = {142}, m = "invokeSuspend")
public final class DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1 extends zb7 implements gd7<fb7<? super fv7<ApiResponse<WatchParameterResponse>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $majorVersion;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1(DeviceRemoteDataSource deviceRemoteDataSource, String str, int i, fb7 fb7) {
        super(1, fb7);
        this.this$0 = deviceRemoteDataSource;
        this.$serial = str;
        this.$majorVersion = i;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(fb7<?> fb7) {
        ee7.b(fb7, "completion");
        return new DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1(this.this$0, this.$serial, this.$majorVersion, fb7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public final Object invoke(fb7<? super fv7<ApiResponse<WatchParameterResponse>>> fb7) {
        return ((DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1) create(fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            ApiServiceV2 access$getMApiService$p = this.this$0.mApiService;
            String str = this.$serial;
            int i2 = this.$majorVersion;
            this.label = 1;
            obj = access$getMApiService$p.getLatestWatchParams(str, i2, "-metadata.version.minor", 0, 1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
