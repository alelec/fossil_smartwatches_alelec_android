package com.portfolio.platform.data.source.local;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.ee7;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.zd5;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class FitnessDataDao {
    @DexIgnore
    public abstract void deleteAllFitnessData();

    @DexIgnore
    public abstract void deleteFitnessData(List<FitnessDataWrapper> list);

    @DexIgnore
    public final List<FitnessDataWrapper> getFitnessData(Date date, Date date2) {
        ee7.b(date, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        ee7.b(date2, "end");
        Date q = zd5.q(date);
        Date l = zd5.l(date2);
        ee7.a((Object) q, SampleRaw.COLUMN_START_TIME);
        DateTime dateTime = new DateTime(q.getTime());
        ee7.a((Object) l, SampleRaw.COLUMN_END_TIME);
        return getListFitnessData(dateTime, new DateTime(l.getTime()));
    }

    @DexIgnore
    public final LiveData<List<FitnessDataWrapper>> getFitnessDataLiveData(Date date, Date date2) {
        ee7.b(date, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        ee7.b(date2, "end");
        Date q = zd5.q(date);
        Date l = zd5.l(date2);
        ee7.a((Object) q, SampleRaw.COLUMN_START_TIME);
        DateTime dateTime = new DateTime(q.getTime());
        ee7.a((Object) l, SampleRaw.COLUMN_END_TIME);
        return getListFitnessDataLiveData(dateTime, new DateTime(l.getTime()));
    }

    @DexIgnore
    public abstract List<FitnessDataWrapper> getListFitnessData(DateTime dateTime, DateTime dateTime2);

    @DexIgnore
    public abstract LiveData<List<FitnessDataWrapper>> getListFitnessDataLiveData(DateTime dateTime, DateTime dateTime2);

    @DexIgnore
    public abstract List<FitnessDataWrapper> getPendingFitnessData();

    @DexIgnore
    public abstract void insertFitnessDataList(List<FitnessDataWrapper> list);
}
