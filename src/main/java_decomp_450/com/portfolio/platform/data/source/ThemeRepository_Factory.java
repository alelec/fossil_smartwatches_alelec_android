package com.portfolio.platform.data.source;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.ThemeDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeRepository_Factory implements Factory<ThemeRepository> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> mAppProvider;
    @DexIgnore
    public /* final */ Provider<ThemeDao> mThemeDaoProvider;

    @DexIgnore
    public ThemeRepository_Factory(Provider<ThemeDao> provider, Provider<PortfolioApp> provider2) {
        this.mThemeDaoProvider = provider;
        this.mAppProvider = provider2;
    }

    @DexIgnore
    public static ThemeRepository_Factory create(Provider<ThemeDao> provider, Provider<PortfolioApp> provider2) {
        return new ThemeRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static ThemeRepository newInstance(ThemeDao themeDao, PortfolioApp portfolioApp) {
        return new ThemeRepository(themeDao, portfolioApp);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ThemeRepository get() {
        return newInstance(this.mThemeDaoProvider.get(), this.mAppProvider.get());
    }
}
