package com.portfolio.platform.data.source.local.reminders;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InactivityNudgeTimeDao_Impl implements InactivityNudgeTimeDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<InactivityNudgeTimeModel> __insertionAdapterOfInactivityNudgeTimeModel;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<InactivityNudgeTimeModel> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `inactivityNudgeTimeModel` (`nudgeTimeName`,`minutes`,`nudgeTimeType`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, InactivityNudgeTimeModel inactivityNudgeTimeModel) {
            if (inactivityNudgeTimeModel.getNudgeTimeName() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, inactivityNudgeTimeModel.getNudgeTimeName());
            }
            ajVar.bindLong(2, (long) inactivityNudgeTimeModel.getMinutes());
            ajVar.bindLong(3, (long) inactivityNudgeTimeModel.getNudgeTimeType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM inactivityNudgeTimeModel";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<InactivityNudgeTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon3(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<InactivityNudgeTimeModel> call() throws Exception {
            Cursor a = pi.a(InactivityNudgeTimeDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "nudgeTimeName");
                int b2 = oi.b(a, "minutes");
                int b3 = oi.b(a, "nudgeTimeType");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new InactivityNudgeTimeModel(a.getString(b), a.getInt(b2), a.getInt(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<InactivityNudgeTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon4(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public InactivityNudgeTimeModel call() throws Exception {
            InactivityNudgeTimeModel inactivityNudgeTimeModel = null;
            Cursor a = pi.a(InactivityNudgeTimeDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "nudgeTimeName");
                int b2 = oi.b(a, "minutes");
                int b3 = oi.b(a, "nudgeTimeType");
                if (a.moveToFirst()) {
                    inactivityNudgeTimeModel = new InactivityNudgeTimeModel(a.getString(b), a.getInt(b2), a.getInt(b3));
                }
                return inactivityNudgeTimeModel;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public InactivityNudgeTimeDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfInactivityNudgeTimeModel = new Anon1(ciVar);
        this.__preparedStmtOfDelete = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao
    public InactivityNudgeTimeModel getInactivityNudgeTimeModelWithFieldNudgeTimeType(int i) {
        fi b = fi.b("SELECT * FROM inactivityNudgeTimeModel WHERE nudgeTimeType = ?", 1);
        b.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        InactivityNudgeTimeModel inactivityNudgeTimeModel = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "nudgeTimeName");
            int b3 = oi.b(a, "minutes");
            int b4 = oi.b(a, "nudgeTimeType");
            if (a.moveToFirst()) {
                inactivityNudgeTimeModel = new InactivityNudgeTimeModel(a.getString(b2), a.getInt(b3), a.getInt(b4));
            }
            return inactivityNudgeTimeModel;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao
    public LiveData<InactivityNudgeTimeModel> getInactivityNudgeTimeWithFieldNudgeTimeType(int i) {
        fi b = fi.b("SELECT * FROM inactivityNudgeTimeModel WHERE nudgeTimeType = ?", 1);
        b.bindLong(1, (long) i);
        return this.__db.getInvalidationTracker().a(new String[]{"inactivityNudgeTimeModel"}, false, (Callable) new Anon4(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao
    public LiveData<List<InactivityNudgeTimeModel>> getListInactivityNudgeTime() {
        return this.__db.getInvalidationTracker().a(new String[]{"inactivityNudgeTimeModel"}, false, (Callable) new Anon3(fi.b("SELECT * FROM inactivityNudgeTimeModel", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao
    public List<InactivityNudgeTimeModel> getListInactivityNudgeTimeModel() {
        fi b = fi.b("SELECT * FROM inactivityNudgeTimeModel", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "nudgeTimeName");
            int b3 = oi.b(a, "minutes");
            int b4 = oi.b(a, "nudgeTimeType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new InactivityNudgeTimeModel(a.getString(b2), a.getInt(b3), a.getInt(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao
    public void upsertInactivityNudgeTime(InactivityNudgeTimeModel inactivityNudgeTimeModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfInactivityNudgeTimeModel.insert(inactivityNudgeTimeModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao
    public void upsertListInactivityNudgeTime(List<InactivityNudgeTimeModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfInactivityNudgeTimeModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
