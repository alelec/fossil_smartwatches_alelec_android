package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.lf;
import com.fossil.mi;
import com.fossil.mu4;
import com.fossil.oi;
import com.fossil.ou4;
import com.fossil.pi;
import com.fossil.vh;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDao_Impl extends GoalTrackingDao {
    @DexIgnore
    public /* final */ mu4 __dateShortStringConverter; // = new mu4();
    @DexIgnore
    public /* final */ ou4 __dateTimeISOStringConverter; // = new ou4();
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<GoalSetting> __insertionAdapterOfGoalSetting;
    @DexIgnore
    public /* final */ vh<GoalTrackingData> __insertionAdapterOfGoalTrackingData;
    @DexIgnore
    public /* final */ vh<GoalTrackingSummary> __insertionAdapterOfGoalTrackingSummary;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteAllGoalTrackingData;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteAllGoalTrackingSummaries;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteGoalSetting;
    @DexIgnore
    public /* final */ ji __preparedStmtOfRemoveDeletedGoalTrackingData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<GoalSetting> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalSetting` (`id`,`currentTarget`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, GoalSetting goalSetting) {
            ajVar.bindLong(1, (long) goalSetting.getId());
            ajVar.bindLong(2, (long) goalSetting.getCurrentTarget());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<List<GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon10(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<GoalTrackingSummary> call() throws Exception {
            Cursor a = pi.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "pinType");
                int b2 = oi.b(a, "date");
                int b3 = oi.b(a, "totalTracked");
                int b4 = oi.b(a, "goalTarget");
                int b5 = oi.b(a, "createdAt");
                int b6 = oi.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b2)), a.getInt(b3), a.getInt(b4), a.getLong(b5), a.getLong(b6));
                    goalTrackingSummary.setPinType(a.getInt(b));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon11 implements Callable<GoalTrackingSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon11(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public GoalTrackingSummary call() throws Exception {
            GoalTrackingSummary goalTrackingSummary = null;
            Cursor a = pi.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "pinType");
                int b2 = oi.b(a, "date");
                int b3 = oi.b(a, "totalTracked");
                int b4 = oi.b(a, "goalTarget");
                int b5 = oi.b(a, "createdAt");
                int b6 = oi.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b2)), a.getInt(b3), a.getInt(b4), a.getLong(b5), a.getLong(b6));
                    goalTrackingSummary.setPinType(a.getInt(b));
                }
                return goalTrackingSummary;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon12 implements Callable<List<GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon12(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<GoalTrackingSummary> call() throws Exception {
            Cursor a = pi.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "pinType");
                int b2 = oi.b(a, "date");
                int b3 = oi.b(a, "totalTracked");
                int b4 = oi.b(a, "goalTarget");
                int b5 = oi.b(a, "createdAt");
                int b6 = oi.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b2)), a.getInt(b3), a.getInt(b4), a.getLong(b5), a.getLong(b6));
                    goalTrackingSummary.setPinType(a.getInt(b));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon13 implements Callable<List<GoalTrackingData>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon13(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<GoalTrackingData> call() throws Exception {
            Cursor a = pi.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "pinType");
                int b2 = oi.b(a, "id");
                int b3 = oi.b(a, GoalTrackingEvent.COLUMN_TRACKED_AT);
                int b4 = oi.b(a, "timezoneOffsetInSecond");
                int b5 = oi.b(a, "date");
                int b6 = oi.b(a, "createdAt");
                int b7 = oi.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    GoalTrackingData goalTrackingData = new GoalTrackingData(a.getString(b2), GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b3)), a.getInt(b4), GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b5)), a.getLong(b6), a.getLong(b7));
                    goalTrackingData.setPinType(a.getInt(b));
                    arrayList.add(goalTrackingData);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon14 implements Callable<List<GoalTrackingData>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon14(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<GoalTrackingData> call() throws Exception {
            Cursor a = pi.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "pinType");
                int b2 = oi.b(a, "id");
                int b3 = oi.b(a, GoalTrackingEvent.COLUMN_TRACKED_AT);
                int b4 = oi.b(a, "timezoneOffsetInSecond");
                int b5 = oi.b(a, "date");
                int b6 = oi.b(a, "createdAt");
                int b7 = oi.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    GoalTrackingData goalTrackingData = new GoalTrackingData(a.getString(b2), GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b3)), a.getInt(b4), GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b5)), a.getLong(b6), a.getLong(b7));
                    goalTrackingData.setPinType(a.getInt(b));
                    arrayList.add(goalTrackingData);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh<GoalTrackingSummary> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalTrackingDay` (`pinType`,`date`,`totalTracked`,`goalTarget`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, GoalTrackingSummary goalTrackingSummary) {
            ajVar.bindLong(1, (long) goalTrackingSummary.getPinType());
            String a = GoalTrackingDao_Impl.this.__dateShortStringConverter.a(goalTrackingSummary.getDate());
            if (a == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, a);
            }
            ajVar.bindLong(3, (long) goalTrackingSummary.getTotalTracked());
            ajVar.bindLong(4, (long) goalTrackingSummary.getGoalTarget());
            ajVar.bindLong(5, goalTrackingSummary.getCreatedAt());
            ajVar.bindLong(6, goalTrackingSummary.getUpdatedAt());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh<GoalTrackingData> {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalTrackingRaw` (`pinType`,`id`,`trackedAt`,`timezoneOffsetInSecond`,`date`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, GoalTrackingData goalTrackingData) {
            ajVar.bindLong(1, (long) goalTrackingData.getPinType());
            if (goalTrackingData.getId() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, goalTrackingData.getId());
            }
            String a = GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.a(goalTrackingData.getTrackedAt());
            if (a == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, a);
            }
            ajVar.bindLong(4, (long) goalTrackingData.getTimezoneOffsetInSecond());
            String a2 = GoalTrackingDao_Impl.this.__dateShortStringConverter.a(goalTrackingData.getDate());
            if (a2 == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a2);
            }
            ajVar.bindLong(6, goalTrackingData.getCreatedAt());
            ajVar.bindLong(7, goalTrackingData.getUpdatedAt());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends ji {
        @DexIgnore
        public Anon4(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM goalSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends ji {
        @DexIgnore
        public Anon5(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM goalTrackingDay";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends ji {
        @DexIgnore
        public Anon6(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM goalTrackingRaw WHERE  id == ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends ji {
        @DexIgnore
        public Anon7(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM goalTrackingRaw";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon8(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Integer call() throws Exception {
            Integer num = null;
            Cursor a = pi.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                if (a.moveToFirst()) {
                    if (!a.isNull(0)) {
                        num = Integer.valueOf(a.getInt(0));
                    }
                }
                return num;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 extends lf.b<Integer, GoalTrackingSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 extends mi<GoalTrackingSummary> {
            @DexIgnore
            public Anon1_Level2(ci ciVar, fi fiVar, boolean z, String... strArr) {
                super(ciVar, fiVar, z, strArr);
            }

            @DexIgnore
            @Override // com.fossil.mi
            public List<GoalTrackingSummary> convertRows(Cursor cursor) {
                int b = oi.b(cursor, "pinType");
                int b2 = oi.b(cursor, "date");
                int b3 = oi.b(cursor, "totalTracked");
                int b4 = oi.b(cursor, "goalTarget");
                int b5 = oi.b(cursor, "createdAt");
                int b6 = oi.b(cursor, "updatedAt");
                ArrayList arrayList = new ArrayList(cursor.getCount());
                while (cursor.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.a(cursor.getString(b2)), cursor.getInt(b3), cursor.getInt(b4), cursor.getLong(b5), cursor.getLong(b6));
                    goalTrackingSummary.setPinType(cursor.getInt(b));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            }
        }

        @DexIgnore
        public Anon9(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.mi<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>' to match base method */
        @Override // com.fossil.lf.b
        public lf<Integer, GoalTrackingSummary> create() {
            return new Anon1_Level2(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, "goalTrackingDay");
        }
    }

    @DexIgnore
    public GoalTrackingDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfGoalSetting = new Anon1(ciVar);
        this.__insertionAdapterOfGoalTrackingSummary = new Anon2(ciVar);
        this.__insertionAdapterOfGoalTrackingData = new Anon3(ciVar);
        this.__preparedStmtOfDeleteGoalSetting = new Anon4(ciVar);
        this.__preparedStmtOfDeleteAllGoalTrackingSummaries = new Anon5(ciVar);
        this.__preparedStmtOfRemoveDeletedGoalTrackingData = new Anon6(ciVar);
        this.__preparedStmtOfDeleteAllGoalTrackingData = new Anon7(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void deleteAllGoalTrackingData() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteAllGoalTrackingData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllGoalTrackingData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void deleteAllGoalTrackingSummaries() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteAllGoalTrackingSummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllGoalTrackingSummaries.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void deleteGoalSetting() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteGoalSetting.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteGoalSetting.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getGoalTrackingDataInDate(Date date) {
        fi b = fi.b("SELECT * FROM goalTrackingRaw WHERE date== ?", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a2, "pinType");
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int b5 = oi.b(a2, "timezoneOffsetInSecond");
            int b6 = oi.b(a2, "date");
            int b7 = oi.b(a2, "createdAt");
            int b8 = oi.b(a2, "updatedAt");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a2.getString(b3), this.__dateTimeISOStringConverter.a(a2.getString(b4)), a2.getInt(b5), this.__dateShortStringConverter.a(a2.getString(b6)), a2.getLong(b7), a2.getLong(b8));
                goalTrackingData.setPinType(a2.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getGoalTrackingDataList(Date date, Date date2) {
        fi b = fi.b("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 3 ORDER BY trackedAt ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a3, "pinType");
            int b3 = oi.b(a3, "id");
            int b4 = oi.b(a3, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int b5 = oi.b(a3, "timezoneOffsetInSecond");
            int b6 = oi.b(a3, "date");
            int b7 = oi.b(a3, "createdAt");
            int b8 = oi.b(a3, "updatedAt");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a3.getString(b3), this.__dateTimeISOStringConverter.a(a3.getString(b4)), a3.getInt(b5), this.__dateShortStringConverter.a(a3.getString(b6)), a3.getLong(b7), a3.getLong(b8));
                goalTrackingData.setPinType(a3.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getGoalTrackingDataListAfterInDate(Date date, DateTime dateTime, long j, int i) {
        fi b = fi.b("SELECT * FROM goalTrackingRaw WHERE date == ? AND updatedAt < ? AND trackedAt < ? AND pinType <> 3 ORDER BY trackedAt DESC limit ?", 4);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        b.bindLong(2, j);
        String a2 = this.__dateTimeISOStringConverter.a(dateTime);
        if (a2 == null) {
            b.bindNull(3);
        } else {
            b.bindString(3, a2);
        }
        b.bindLong(4, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a3, "pinType");
            int b3 = oi.b(a3, "id");
            int b4 = oi.b(a3, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int b5 = oi.b(a3, "timezoneOffsetInSecond");
            int b6 = oi.b(a3, "date");
            int b7 = oi.b(a3, "createdAt");
            int b8 = oi.b(a3, "updatedAt");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a3.getString(b3), this.__dateTimeISOStringConverter.a(a3.getString(b4)), a3.getInt(b5), this.__dateShortStringConverter.a(a3.getString(b6)), a3.getLong(b7), a3.getLong(b8));
                goalTrackingData.setPinType(a3.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getGoalTrackingDataListInitInDate(Date date, int i) {
        fi b = fi.b("SELECT * FROM goalTrackingRaw WHERE date == ? AND pinType <> 3 ORDER BY trackedAt DESC limit ?", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        b.bindLong(2, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a2, "pinType");
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int b5 = oi.b(a2, "timezoneOffsetInSecond");
            int b6 = oi.b(a2, "date");
            int b7 = oi.b(a2, "createdAt");
            int b8 = oi.b(a2, "updatedAt");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a2.getString(b3), this.__dateTimeISOStringConverter.a(a2.getString(b4)), a2.getInt(b5), this.__dateShortStringConverter.a(a2.getString(b6)), a2.getLong(b7), a2.getLong(b8));
                goalTrackingData.setPinType(a2.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<List<GoalTrackingData>> getGoalTrackingDataListLiveData(Date date, Date date2) {
        fi b = fi.b("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 3 ORDER BY trackedAt ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingRaw"}, false, (Callable) new Anon13(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingSummary> getGoalTrackingSummaries(Date date, Date date2) {
        fi b = fi.b("SELECT * FROM goalTrackingDay WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a3, "pinType");
            int b3 = oi.b(a3, "date");
            int b4 = oi.b(a3, "totalTracked");
            int b5 = oi.b(a3, "goalTarget");
            int b6 = oi.b(a3, "createdAt");
            int b7 = oi.b(a3, "updatedAt");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.a(a3.getString(b3)), a3.getInt(b4), a3.getInt(b5), a3.getLong(b6), a3.getLong(b7));
                goalTrackingSummary.setPinType(a3.getInt(b2));
                arrayList.add(goalTrackingSummary);
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<List<GoalTrackingSummary>> getGoalTrackingSummariesLiveData(Date date, Date date2) {
        fi b = fi.b("SELECT * FROM goalTrackingDay WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingDay"}, false, (Callable) new Anon12(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public GoalTrackingSummary getGoalTrackingSummary(Date date) {
        fi b = fi.b("SELECT * FROM goalTrackingDay WHERE date == ?", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        this.__db.assertNotSuspendingTransaction();
        GoalTrackingSummary goalTrackingSummary = null;
        Cursor a2 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a2, "pinType");
            int b3 = oi.b(a2, "date");
            int b4 = oi.b(a2, "totalTracked");
            int b5 = oi.b(a2, "goalTarget");
            int b6 = oi.b(a2, "createdAt");
            int b7 = oi.b(a2, "updatedAt");
            if (a2.moveToFirst()) {
                goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.a(a2.getString(b3)), a2.getInt(b4), a2.getInt(b5), a2.getLong(b6), a2.getLong(b7));
                goalTrackingSummary.setPinType(a2.getInt(b2));
            }
            return goalTrackingSummary;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingSummary> getGoalTrackingSummaryList() {
        fi b = fi.b("SELECT * FROM goalTrackingDay", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "date");
            int b4 = oi.b(a, "totalTracked");
            int b5 = oi.b(a, "goalTarget");
            int b6 = oi.b(a, "createdAt");
            int b7 = oi.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.a(a.getString(b3)), a.getInt(b4), a.getInt(b5), a.getLong(b6), a.getLong(b7));
                goalTrackingSummary.setPinType(a.getInt(b2));
                arrayList.add(goalTrackingSummary);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<List<GoalTrackingSummary>> getGoalTrackingSummaryListLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingDay"}, false, (Callable) new Anon10(fi.b("SELECT * FROM goalTrackingDay", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<GoalTrackingSummary> getGoalTrackingSummaryLiveData(Date date) {
        fi b = fi.b("SELECT * FROM goalTrackingDay WHERE date == ?", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingDay"}, false, (Callable) new Anon11(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public GoalTrackingDao.TotalSummary getGoalTrackingValueAndTarget(Date date, Date date2) {
        fi b = fi.b("SELECT SUM(totalTracked) as total_values, SUM(goalTarget) as total_targets FROM goalTrackingDay\n        WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        GoalTrackingDao.TotalSummary totalSummary = null;
        Cursor a3 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a3, "total_values");
            int b3 = oi.b(a3, "total_targets");
            if (a3.moveToFirst()) {
                totalSummary = new GoalTrackingDao.TotalSummary();
                totalSummary.setValues(a3.getInt(b2));
                totalSummary.setTargets(a3.getInt(b3));
            }
            return totalSummary;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public Integer getLastGoalSetting() {
        fi b = fi.b("SELECT currentTarget FROM goalSetting LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Integer num = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            if (a.moveToFirst()) {
                if (!a.isNull(0)) {
                    num = Integer.valueOf(a.getInt(0));
                }
            }
            return num;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<Integer> getLastGoalSettingLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"goalSetting"}, false, (Callable) new Anon8(fi.b("SELECT currentTarget FROM goalSetting LIMIT 1", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public GoalTrackingSummary getNearestGoalTrackingFromDate(Date date) {
        fi b = fi.b("SELECT * FROM goalTrackingDay WHERE date <= ? ORDER BY date DESC LIMIT 1", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        this.__db.assertNotSuspendingTransaction();
        GoalTrackingSummary goalTrackingSummary = null;
        Cursor a2 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a2, "pinType");
            int b3 = oi.b(a2, "date");
            int b4 = oi.b(a2, "totalTracked");
            int b5 = oi.b(a2, "goalTarget");
            int b6 = oi.b(a2, "createdAt");
            int b7 = oi.b(a2, "updatedAt");
            if (a2.moveToFirst()) {
                goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.a(a2.getString(b3)), a2.getInt(b4), a2.getInt(b5), a2.getLong(b6), a2.getLong(b7));
                goalTrackingSummary.setPinType(a2.getInt(b2));
            }
            return goalTrackingSummary;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getPendingGoalTrackingDataList(Date date, Date date2) {
        fi b = fi.b("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 0", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a3, "pinType");
            int b3 = oi.b(a3, "id");
            int b4 = oi.b(a3, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int b5 = oi.b(a3, "timezoneOffsetInSecond");
            int b6 = oi.b(a3, "date");
            int b7 = oi.b(a3, "createdAt");
            int b8 = oi.b(a3, "updatedAt");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a3.getString(b3), this.__dateTimeISOStringConverter.a(a3.getString(b4)), a3.getInt(b5), this.__dateShortStringConverter.a(a3.getString(b6)), a3.getLong(b7), a3.getLong(b8));
                goalTrackingData.setPinType(a3.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<List<GoalTrackingData>> getPendingGoalTrackingDataListLiveData(Date date, Date date2) {
        fi b = fi.b("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 0", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingRaw"}, false, (Callable) new Anon14(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public lf.b<Integer, GoalTrackingSummary> getSummariesDataSource() {
        return new Anon9(fi.b("SELECT * FROM goalTrackingDay ORDER BY date DESC", 0));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void removeDeletedGoalTrackingData(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfRemoveDeletedGoalTrackingData.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveDeletedGoalTrackingData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalSettings(GoalSetting goalSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalSetting.insert(goalSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalTrackingData(GoalTrackingData goalTrackingData) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert(goalTrackingData);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalTrackingDataList(List<GoalTrackingData> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalTrackingSummaries(List<GoalTrackingSummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingSummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalTrackingSummary(GoalTrackingSummary goalTrackingSummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingSummary.insert(goalTrackingSummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertListGoalTrackingData(List<GoalTrackingData> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getPendingGoalTrackingDataList() {
        fi b = fi.b("SELECT * FROM goalTrackingRaw WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "id");
            int b4 = oi.b(a, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int b5 = oi.b(a, "timezoneOffsetInSecond");
            int b6 = oi.b(a, "date");
            int b7 = oi.b(a, "createdAt");
            int b8 = oi.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a.getString(b3), this.__dateTimeISOStringConverter.a(a.getString(b4)), a.getInt(b5), this.__dateShortStringConverter.a(a.getString(b6)), a.getLong(b7), a.getLong(b8));
                goalTrackingData.setPinType(a.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }
}
