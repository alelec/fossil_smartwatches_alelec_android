package com.portfolio.platform.data.source;

import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.ig5;
import com.fossil.nh7;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.source.local.diana.WatchAppDao;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "WatchAppRepository";
    @DexIgnore
    public /* final */ PortfolioApp mPortfolioApp;
    @DexIgnore
    public /* final */ WatchAppDao mWatchAppDao;
    @DexIgnore
    public /* final */ WatchAppRemoteDataSource mWatchAppRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public WatchAppRepository(WatchAppDao watchAppDao, WatchAppRemoteDataSource watchAppRemoteDataSource, PortfolioApp portfolioApp) {
        ee7.b(watchAppDao, "mWatchAppDao");
        ee7.b(watchAppRemoteDataSource, "mWatchAppRemoteDataSource");
        ee7.b(portfolioApp, "mPortfolioApp");
        this.mWatchAppDao = watchAppDao;
        this.mWatchAppRemoteDataSource = watchAppRemoteDataSource;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mWatchAppDao.clearAll();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadWatchApp(java.lang.String r7, com.fossil.fb7<? super com.fossil.i97> r8) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.WatchAppRepository$downloadWatchApp$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.portfolio.platform.data.source.WatchAppRepository$downloadWatchApp$Anon1 r0 = (com.portfolio.platform.data.source.WatchAppRepository$downloadWatchApp$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.WatchAppRepository$downloadWatchApp$Anon1 r0 = new com.portfolio.platform.data.source.WatchAppRepository$downloadWatchApp$Anon1
            r0.<init>(r6, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = "downloadWatchApp of "
            r4 = 1
            java.lang.String r5 = "WatchAppRepository"
            if (r2 == 0) goto L_0x003d
            if (r2 != r4) goto L_0x0035
            java.lang.Object r7 = r0.L$1
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.WatchAppRepository r0 = (com.portfolio.platform.data.source.WatchAppRepository) r0
            com.fossil.t87.a(r8)
            goto L_0x0068
        L_0x0035:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L_0x003d:
            com.fossil.t87.a(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r3)
            r2.append(r7)
            java.lang.String r2 = r2.toString()
            r8.d(r5, r2)
            com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource r8 = r6.mWatchAppRemoteDataSource
            r0.L$0 = r6
            r0.L$1 = r7
            r0.label = r4
            java.lang.Object r8 = r8.getAllWatchApp(r7, r0)
            if (r8 != r1) goto L_0x0067
            return r1
        L_0x0067:
            r0 = r6
        L_0x0068:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r1 = r8 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x00cd
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r3)
            r2.append(r7)
            java.lang.String r7 = " success isFromCache "
            r2.append(r7)
            com.fossil.bj5 r8 = (com.fossil.bj5) r8
            boolean r7 = r8.b()
            r2.append(r7)
            java.lang.String r7 = " response "
            r2.append(r7)
            java.lang.Object r7 = r8.a()
            java.util.List r7 = (java.util.List) r7
            r2.append(r7)
            java.lang.String r7 = r2.toString()
            r1.d(r5, r7)
            com.portfolio.platform.data.source.local.diana.WatchAppDao r7 = r0.mWatchAppDao
            java.util.List r7 = r7.getAllWatchApp()
            java.lang.Object r8 = r8.a()
            java.util.List r8 = (java.util.List) r8
            if (r8 == 0) goto L_0x010b
            boolean r7 = com.fossil.ee7.a(r8, r7)
            r7 = r7 ^ r4
            if (r7 == 0) goto L_0x010b
            com.portfolio.platform.data.source.local.diana.WatchAppDao r7 = r0.mWatchAppDao
            r7.clearAll()
            com.portfolio.platform.data.source.local.diana.WatchAppDao r7 = r0.mWatchAppDao
            r7.upsertWatchAppList(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r8 = "downloadWatchApp - insert to database success"
            r7.d(r5, r8)
            goto L_0x010b
        L_0x00cd:
            boolean r0 = r8 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x010b
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r3)
            r1.append(r7)
            java.lang.String r7 = " fail!!! error="
            r1.append(r7)
            com.fossil.yi5 r8 = (com.fossil.yi5) r8
            int r7 = r8.a()
            r1.append(r7)
            java.lang.String r7 = " serverErrorCode="
            r1.append(r7)
            com.portfolio.platform.data.model.ServerError r7 = r8.c()
            if (r7 == 0) goto L_0x0100
            java.lang.Integer r7 = r7.getCode()
            goto L_0x0101
        L_0x0100:
            r7 = 0
        L_0x0101:
            r1.append(r7)
            java.lang.String r7 = r1.toString()
            r0.d(r5, r7)
        L_0x010b:
            com.fossil.i97 r7 = com.fossil.i97.a
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchAppRepository.downloadWatchApp(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final List<WatchApp> getAllWatchAppRaw() {
        return this.mWatchAppDao.getAllWatchApp();
    }

    @DexIgnore
    public final List<WatchApp> getWatchAppByIds(List<String> list) {
        ee7.b(list, "ids");
        if (!list.isEmpty()) {
            return ea7.a((Iterable) this.mWatchAppDao.getWatchAppByIds(list), (Comparator) new WatchAppRepository$getWatchAppByIds$$inlined$sortedBy$Anon1(list));
        }
        return new ArrayList();
    }

    @DexIgnore
    public final List<WatchApp> queryWatchAppByName(String str) {
        ee7.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        ArrayList arrayList = new ArrayList();
        for (WatchApp watchApp : this.mWatchAppDao.getAllWatchApp()) {
            String normalize = Normalizer.normalize(ig5.a(this.mPortfolioApp, watchApp.getNameKey(), watchApp.getName()), Normalizer.Form.NFC);
            String normalize2 = Normalizer.normalize(str, Normalizer.Form.NFC);
            ee7.a((Object) normalize, "name");
            ee7.a((Object) normalize2, "searchQuery");
            if (nh7.a((CharSequence) normalize, (CharSequence) normalize2, true)) {
                arrayList.add(watchApp);
            }
        }
        return arrayList;
    }
}
