package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppLastSettingRepository {
    @DexIgnore
    public /* final */ MicroAppLastSettingDao mMicroAppLastSettingDao;

    @DexIgnore
    public MicroAppLastSettingRepository(MicroAppLastSettingDao microAppLastSettingDao) {
        ee7.b(microAppLastSettingDao, "mMicroAppLastSettingDao");
        this.mMicroAppLastSettingDao = microAppLastSettingDao;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mMicroAppLastSettingDao.cleanUp();
    }

    @DexIgnore
    public final MicroAppLastSetting getMicroAppLastSetting(String str) {
        ee7.b(str, "id");
        return this.mMicroAppLastSettingDao.getMicroAppLastSetting(str);
    }

    @DexIgnore
    public final void upsertMicroAppLastSetting(MicroAppLastSetting microAppLastSetting) {
        ee7.b(microAppLastSetting, "MicroAppLastSetting");
        this.mMicroAppLastSettingDao.upsertMicroAppLastSetting(microAppLastSetting);
    }

    @DexIgnore
    public final void upsertMicroAppLastSettingList(List<MicroAppLastSetting> list) {
        ee7.b(list, "microAppLastSettingList");
        this.mMicroAppLastSettingDao.upsertMicroAppLastSettingList(list);
    }
}
