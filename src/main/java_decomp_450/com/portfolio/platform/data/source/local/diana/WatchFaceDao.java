package com.portfolio.platform.data.source.local.diana;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface WatchFaceDao {
    @DexIgnore
    void deleteAll();

    @DexIgnore
    void deleteAllBackgroundWatchface();

    @DexIgnore
    void deleteWatchFace(String str, int i);

    @DexIgnore
    void deleteWatchFacesWithSerial(String str);

    @DexIgnore
    List<WatchFace> getAllWatchFaces();

    @DexIgnore
    String getLatestWatchFaceName(int i);

    @DexIgnore
    WatchFace getWatchFaceWithId(String str);

    @DexIgnore
    LiveData<List<WatchFace>> getWatchFacesLiveData(String str);

    @DexIgnore
    LiveData<List<WatchFace>> getWatchFacesLiveDataWithType(String str, int i);

    @DexIgnore
    List<WatchFace> getWatchFacesWithSerial(String str);

    @DexIgnore
    List<WatchFace> getWatchFacesWithType(String str, int i);

    @DexIgnore
    void insertAllWatchFaces(List<WatchFace> list);

    @DexIgnore
    void insertWatchFace(WatchFace watchFace);
}
