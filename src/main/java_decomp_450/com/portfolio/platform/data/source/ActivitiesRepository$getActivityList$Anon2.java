package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.lx6;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.qe7;
import com.fossil.qj7;
import com.fossil.qx6;
import com.fossil.r87;
import com.fossil.t3;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.ti7;
import com.fossil.vh7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$2", f = "ActivitiesRepository.kt", l = {62}, m = "invokeSuspend")
public final class ActivitiesRepository$getActivityList$Anon2 extends zb7 implements kd7<yi7, fb7<? super LiveData<qx6<? extends List<ActivitySample>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ FitnessDatabase $fitnessDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startDate;
        @DexIgnore
        public /* final */ /* synthetic */ ActivitiesRepository$getActivityList$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends lx6<List<ActivitySample>, ApiResponse<Activity>> {
            @DexIgnore
            public /* final */ /* synthetic */ r87 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ int $limit;
            @DexIgnore
            public /* final */ /* synthetic */ qe7 $offset;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, qe7 qe7, int i, r87 r87) {
                this.this$0 = anon1_Level2;
                this.$offset = qe7;
                this.$limit = i;
                this.$downloadingDate = r87;
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object createCall(fb7<? super fv7<ApiResponse<Activity>>> fb7) {
                Date date;
                Date date2;
                ApiServiceV2 access$getMApiService$p = this.this$0.this$0.this$0.mApiService;
                r87 r87 = this.$downloadingDate;
                if (r87 == null || (date = (Date) r87.getFirst()) == null) {
                    date = this.this$0.$startDate;
                }
                String g = zd5.g(date);
                ee7.a((Object) g, "DateHelper.formatShortDa\u2026            ?: startDate)");
                r87 r872 = this.$downloadingDate;
                if (r872 == null || (date2 = (Date) r872.getSecond()) == null) {
                    date2 = this.this$0.$endDate;
                }
                String g2 = zd5.g(date2);
                ee7.a((Object) g2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return access$getMApiService$p.getActivities(g, g2, this.$offset.element, this.$limit, fb7);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
            /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
            @Override // com.fossil.lx6
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object loadFromDb(com.fossil.fb7<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>>> r7) {
                /*
                    r6 = this;
                    boolean r0 = r7 instanceof com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    if (r0 == 0) goto L_0x0013
                    r0 = r7
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = (com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = -2147483648(0xffffffff80000000, float:-0.0)
                    r3 = r1 & r2
                    if (r3 == 0) goto L_0x0013
                    int r1 = r1 - r2
                    r0.label = r1
                    goto L_0x0018
                L_0x0013:
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = new com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    r0.<init>(r6, r7)
                L_0x0018:
                    java.lang.Object r7 = r0.result
                    java.lang.Object r1 = com.fossil.nb7.a()
                    int r2 = r0.label
                    r3 = 1
                    if (r2 == 0) goto L_0x0035
                    if (r2 != r3) goto L_0x002d
                    java.lang.Object r0 = r0.L$0
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3 r0 = (com.portfolio.platform.data.source.ActivitiesRepository.getActivityList.Anon2.Anon1_Level2.Anon1_Level3) r0
                    com.fossil.t87.a(r7)
                    goto L_0x0081
                L_0x002d:
                    java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r7.<init>(r0)
                    throw r7
                L_0x0035:
                    com.fossil.t87.a(r7)
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2 r7 = r6.this$0
                    java.util.Date r7 = r7.$endDate
                    java.lang.Boolean r7 = com.fossil.zd5.w(r7)
                    boolean r7 = r7.booleanValue()
                    if (r7 != 0) goto L_0x0084
                    com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                    com.portfolio.platform.data.source.ActivitiesRepository$Companion r2 = com.portfolio.platform.data.source.ActivitiesRepository.Companion
                    java.lang.String r2 = r2.getTAG$app_fossilRelease()
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "getActivityList loadFromDb isNotToday day = "
                    r4.append(r5)
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2 r5 = r6.this$0
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2 r5 = r5.this$0
                    java.util.Date r5 = r5.$end
                    r4.append(r5)
                    java.lang.String r4 = r4.toString()
                    r7.d(r2, r4)
                    com.fossil.ti7 r7 = com.fossil.qj7.b()
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4 r2 = new com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4
                    r4 = 0
                    r2.<init>(r6, r4)
                    r0.L$0 = r6
                    r0.label = r3
                    java.lang.Object r7 = com.fossil.vh7.a(r7, r2, r0)
                    if (r7 != r1) goto L_0x0081
                    return r1
                L_0x0081:
                    androidx.lifecycle.LiveData r7 = (androidx.lifecycle.LiveData) r7
                    goto L_0x00c5
                L_0x0084:
                    com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                    com.portfolio.platform.data.source.ActivitiesRepository$Companion r0 = com.portfolio.platform.data.source.ActivitiesRepository.Companion
                    java.lang.String r0 = r0.getTAG$app_fossilRelease()
                    java.lang.String r1 = "getActivityList loadFromDb: isToday"
                    r7.d(r0, r1)
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2 r7 = r6.this$0
                    com.portfolio.platform.data.source.local.fitness.FitnessDatabase r7 = r7.$fitnessDatabase
                    com.portfolio.platform.data.source.local.fitness.ActivitySampleDao r7 = r7.activitySampleDao()
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2 r0 = r6.this$0
                    java.util.Date r0 = r0.$startDate
                    java.lang.String r1 = "startDate"
                    com.fossil.ee7.a(r0, r1)
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2 r1 = r6.this$0
                    java.util.Date r1 = r1.$endDate
                    java.util.Date r1 = com.fossil.zd5.p(r1)
                    java.lang.String r2 = "DateHelper.getPrevDate(endDate)"
                    com.fossil.ee7.a(r1, r2)
                    androidx.lifecycle.LiveData r7 = r7.getActivitySamplesLiveData(r0, r1)
                    com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon3_Level4 r0 = new com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon3_Level4
                    r0.<init>(r6)
                    androidx.lifecycle.LiveData r7 = com.fossil.ge.b(r7, r0)
                    java.lang.String r0 = "Transformations.switchMa\u2026                        }"
                    com.fossil.ee7.a(r7, r0)
                L_0x00c5:
                    return r7
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ActivitiesRepository.getActivityList.Anon2.Anon1_Level2.Anon1_Level3.loadFromDb(com.fossil.fb7):java.lang.Object");
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(ActivitiesRepository.Companion.getTAG$app_fossilRelease(), "getActivityList onFetchFailed");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object processContinueFetching(ApiResponse<Activity> apiResponse, fb7 fb7) {
                return processContinueFetching(apiResponse, (fb7<? super Boolean>) fb7);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<Activity> apiResponse, fb7 fb7) {
                return saveCallResult(apiResponse, (fb7<? super i97>) fb7);
            }

            @DexIgnore
            public Object processContinueFetching(ApiResponse<Activity> apiResponse, fb7<? super Boolean> fb7) {
                Boolean a;
                Range range = apiResponse.get_range();
                if (range == null || (a = pb7.a(range.isHasNext())) == null || !a.booleanValue()) {
                    return pb7.a(false);
                }
                FLogger.INSTANCE.getLocal().d(ActivitiesRepository.Companion.getTAG$app_fossilRelease(), "getActivityList processContinueFetching hasNext");
                this.$offset.element += this.$limit;
                return pb7.a(true);
            }

            @DexIgnore
            public Object saveCallResult(ApiResponse<Activity> apiResponse, fb7<? super i97> fb7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = ActivitiesRepository.Companion.getTAG$app_fossilRelease();
                StringBuilder sb = new StringBuilder();
                sb.append("getActivityList saveCallResult onResponse: response = ");
                sb.append(apiResponse.get_items().size());
                sb.append(" hasNext=");
                Range range = apiResponse.get_range();
                sb.append(range != null ? pb7.a(range.isHasNext()) : null);
                local.d(tAG$app_fossilRelease, sb.toString());
                ArrayList arrayList = new ArrayList();
                Iterator<T> it = apiResponse.get_items().iterator();
                while (it.hasNext()) {
                    arrayList.add(it.next().toActivitySample());
                }
                Object a = vh7.a(qj7.b(), new ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon3_Level4(this, arrayList, null), fb7);
                if (a == nb7.a()) {
                    return a;
                }
                return i97.a;
            }

            @DexIgnore
            public boolean shouldFetch(List<ActivitySample> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(ActivitiesRepository$getActivityList$Anon2 activitiesRepository$getActivityList$Anon2, Date date, Date date2, FitnessDatabase fitnessDatabase) {
            this.this$0 = activitiesRepository$getActivityList$Anon2;
            this.$startDate = date;
            this.$endDate = date2;
            this.$fitnessDatabase = fitnessDatabase;
        }

        @DexIgnore
        public final LiveData<qx6<List<ActivitySample>>> apply(List<FitnessDataWrapper> list) {
            qe7 qe7 = new qe7();
            qe7.element = 0;
            ee7.a((Object) list, "fitnessDataList");
            Date date = this.$startDate;
            ee7.a((Object) date, GoalPhase.COLUMN_START_DATE);
            Date date2 = this.$endDate;
            ee7.a((Object) date2, GoalPhase.COLUMN_END_DATE);
            return new Anon1_Level3(this, qe7, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS, FitnessDataWrapperKt.calculateRangeDownload(list, date, date2)).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$getActivityList$Anon2(ActivitiesRepository activitiesRepository, Date date, Date date2, boolean z, fb7 fb7) {
        super(2, fb7);
        this.this$0 = activitiesRepository;
        this.$start = date;
        this.$end = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        ActivitiesRepository$getActivityList$Anon2 activitiesRepository$getActivityList$Anon2 = new ActivitiesRepository$getActivityList$Anon2(this.this$0, this.$start, this.$end, this.$shouldFetch, fb7);
        activitiesRepository$getActivityList$Anon2.p$ = (yi7) obj;
        return activitiesRepository$getActivityList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LiveData<qx6<? extends List<ActivitySample>>>> fb7) {
        return ((ActivitiesRepository$getActivityList$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Date date;
        Date date2;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ActivitiesRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getActivityList: start = " + this.$start + ", end = " + this.$end + " shouldFetch=" + this.$shouldFetch);
            date = zd5.q(this.$start);
            Date l = zd5.l(this.$end);
            ti7 b = qj7.b();
            ActivitiesRepository$getActivityList$Anon2$fitnessDatabase$Anon1_Level2 activitiesRepository$getActivityList$Anon2$fitnessDatabase$Anon1_Level2 = new ActivitiesRepository$getActivityList$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = yi7;
            this.L$1 = date;
            this.L$2 = l;
            this.label = 1;
            obj = vh7.a(b, activitiesRepository$getActivityList$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
            date2 = l;
        } else if (i == 1) {
            date2 = (Date) this.L$2;
            date = (Date) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) obj;
        FitnessDataDao fitnessDataDao = fitnessDatabase.getFitnessDataDao();
        ee7.a((Object) date, GoalPhase.COLUMN_START_DATE);
        ee7.a((Object) date2, GoalPhase.COLUMN_END_DATE);
        return ge.b(fitnessDataDao.getFitnessDataLiveData(date, date2), new Anon1_Level2(this, date, date2, fitnessDatabase));
    }
}
