package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "MicroAppRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public MicroAppRemoteDataSource(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getAllMicroApp(java.lang.String r18, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.model.room.microapp.MicroApp>>> r19) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            boolean r3 = r2 instanceof com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroApp$Anon1
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroApp$Anon1 r3 = (com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroApp$Anon1) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroApp$Anon1 r3 = new com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroApp$Anon1
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 0
            r7 = 1
            java.lang.String r8 = "MicroAppRemoteDataSource"
            if (r5 == 0) goto L_0x0042
            if (r5 != r7) goto L_0x003a
            java.lang.Object r1 = r3.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r3 = r3.L$0
            com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource r3 = (com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource) r3
            com.fossil.t87.a(r2)
            goto L_0x0071
        L_0x003a:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0042:
            com.fossil.t87.a(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r9 = "getAllMicroApp "
            r5.append(r9)
            r5.append(r1)
            java.lang.String r5 = r5.toString()
            r2.d(r8, r5)
            com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroApp$response$Anon1 r2 = new com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroApp$response$Anon1
            r2.<init>(r0, r1, r6)
            r3.L$0 = r0
            r3.L$1 = r1
            r3.label = r7
            java.lang.Object r2 = com.fossil.aj5.a(r2, r3)
            if (r2 != r4) goto L_0x0071
            return r4
        L_0x0071:
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            boolean r3 = r2 instanceof com.fossil.bj5
            if (r3 == 0) goto L_0x00cd
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            com.fossil.bj5 r2 = (com.fossil.bj5) r2
            java.lang.Object r4 = r2.a()
            if (r4 == 0) goto L_0x00c9
            com.portfolio.platform.data.source.remote.ApiResponse r4 = (com.portfolio.platform.data.source.remote.ApiResponse) r4
            java.util.List r4 = r4.get_items()
            java.util.Iterator r4 = r4.iterator()
        L_0x008e:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x00a1
            java.lang.Object r5 = r4.next()
            com.portfolio.platform.data.model.room.microapp.MicroApp r5 = (com.portfolio.platform.data.model.room.microapp.MicroApp) r5
            r5.setSerialNumber(r1)
            r3.add(r5)
            goto L_0x008e
        L_0x00a1:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "getAllMicroApp success isFromCache "
            r4.append(r5)
            boolean r5 = r2.b()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.d(r8, r4)
            com.fossil.bj5 r1 = new com.fossil.bj5
            boolean r2 = r2.b()
            r1.<init>(r3, r2)
            goto L_0x0115
        L_0x00c9:
            com.fossil.ee7.a()
            throw r6
        L_0x00cd:
            boolean r1 = r2 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x0116
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "getAllMicroApp fail code "
            r3.append(r4)
            com.fossil.yi5 r2 = (com.fossil.yi5) r2
            int r4 = r2.a()
            r3.append(r4)
            java.lang.String r4 = " serverError "
            r3.append(r4)
            com.portfolio.platform.data.model.ServerError r4 = r2.c()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.d(r8, r3)
            com.fossil.yi5 r1 = new com.fossil.yi5
            int r10 = r2.a()
            com.portfolio.platform.data.model.ServerError r11 = r2.c()
            java.lang.Throwable r12 = r2.d()
            r13 = 0
            r14 = 0
            r15 = 24
            r16 = 0
            r9 = r1
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
        L_0x0115:
            return r1
        L_0x0116:
            com.fossil.p87 r1 = new com.fossil.p87
            r1.<init>()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource.getAllMicroApp(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getAllMicroAppVariant(java.lang.String r23, java.lang.String r24, java.lang.String r25, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.model.room.microapp.MicroAppVariant>>> r26) {
        /*
            r22 = this;
            r6 = r22
            r7 = r23
            r8 = r24
            r9 = r25
            r0 = r26
            boolean r1 = r0 instanceof com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1
            if (r1 == 0) goto L_0x001d
            r1 = r0
            com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1 r1 = (com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1) r1
            int r2 = r1.label
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r2 & r3
            if (r4 == 0) goto L_0x001d
            int r2 = r2 - r3
            r1.label = r2
            goto L_0x0022
        L_0x001d:
            com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1 r1 = new com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1
            r1.<init>(r6, r0)
        L_0x0022:
            r10 = r1
            java.lang.Object r0 = r10.result
            java.lang.Object r11 = com.fossil.nb7.a()
            int r1 = r10.label
            r12 = 1
            java.lang.String r13 = "MicroAppRemoteDataSource"
            if (r1 == 0) goto L_0x004e
            if (r1 != r12) goto L_0x0046
            java.lang.Object r1 = r10.L$3
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r10.L$2
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r10.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r10.L$0
            com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource r2 = (com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource) r2
            com.fossil.t87.a(r0)
            goto L_0x009c
        L_0x0046:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004e:
            com.fossil.t87.a(r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "getAllMicroAppVariant "
            r1.append(r2)
            r1.append(r7)
            java.lang.String r2 = " major "
            r1.append(r2)
            r1.append(r8)
            java.lang.String r2 = " minor "
            r1.append(r2)
            r1.append(r9)
            java.lang.String r1 = r1.toString()
            r0.d(r13, r1)
            com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1 r14 = new com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1
            r5 = 0
            r0 = r14
            r1 = r22
            r2 = r23
            r3 = r24
            r4 = r25
            r0.<init>(r1, r2, r3, r4, r5)
            r10.L$0 = r6
            r10.L$1 = r7
            r10.L$2 = r8
            r10.L$3 = r9
            r10.label = r12
            java.lang.Object r0 = com.fossil.aj5.a(r14, r10)
            if (r0 != r11) goto L_0x009b
            return r11
        L_0x009b:
            r1 = r7
        L_0x009c:
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            boolean r2 = r0 instanceof com.fossil.bj5
            if (r2 == 0) goto L_0x011f
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            com.fossil.bj5 r0 = (com.fossil.bj5) r0
            java.lang.Object r3 = r0.a()
            if (r3 == 0) goto L_0x011a
            com.portfolio.platform.data.source.remote.ApiResponse r3 = (com.portfolio.platform.data.source.remote.ApiResponse) r3
            java.util.List r3 = r3.get_items()
            java.util.Iterator r3 = r3.iterator()
        L_0x00b9:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x00f2
            java.lang.Object r4 = r3.next()
            com.portfolio.platform.data.model.room.microapp.MicroAppVariant r4 = (com.portfolio.platform.data.model.room.microapp.MicroAppVariant) r4
            r4.setSerialNumber(r1)
            java.util.ArrayList r5 = r4.getDeclarationFileList()
            java.util.Iterator r5 = r5.iterator()
        L_0x00d0:
            boolean r7 = r5.hasNext()
            if (r7 == 0) goto L_0x00ee
            java.lang.Object r7 = r5.next()
            com.portfolio.platform.data.model.room.microapp.DeclarationFile r7 = (com.portfolio.platform.data.model.room.microapp.DeclarationFile) r7
            java.lang.String r8 = r4.getAppId()
            r7.setAppId(r8)
            r7.setSerialNumber(r1)
            java.lang.String r8 = r4.getName()
            r7.setVariantName(r8)
            goto L_0x00d0
        L_0x00ee:
            r2.add(r4)
            goto L_0x00b9
        L_0x00f2:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "getAllMicroApp success isFromCache "
            r3.append(r4)
            boolean r4 = r0.b()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.d(r13, r3)
            com.fossil.bj5 r1 = new com.fossil.bj5
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            goto L_0x0169
        L_0x011a:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x011f:
            boolean r1 = r0 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x016a
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "getAllMicroApp fail code "
            r2.append(r3)
            com.fossil.yi5 r0 = (com.fossil.yi5) r0
            int r3 = r0.a()
            r2.append(r3)
            java.lang.String r3 = " serverError "
            r2.append(r3)
            com.portfolio.platform.data.model.ServerError r3 = r0.c()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.d(r13, r2)
            com.fossil.yi5 r1 = new com.fossil.yi5
            int r15 = r0.a()
            com.portfolio.platform.data.model.ServerError r16 = r0.c()
            java.lang.Throwable r17 = r0.d()
            r18 = 0
            r19 = 0
            r20 = 24
            r21 = 0
            r14 = r1
            r14.<init>(r15, r16, r17, r18, r19, r20, r21)
        L_0x0169:
            return r1
        L_0x016a:
            com.fossil.p87 r0 = new com.fossil.p87
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource.getAllMicroAppVariant(java.lang.String, java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }
}
