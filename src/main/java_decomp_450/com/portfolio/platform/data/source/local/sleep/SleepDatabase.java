package com.portfolio.platform.data.source.local.sleep;

import com.fossil.ci;
import com.fossil.li;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class SleepDatabase extends ci {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ li MIGRATION_FROM_2_TO_5; // = new SleepDatabase$Companion$MIGRATION_FROM_2_TO_5$Anon1(2, 5);
    @DexIgnore
    public static /* final */ li MIGRATION_FROM_3_TO_9; // = new SleepDatabase$Companion$MIGRATION_FROM_3_TO_9$Anon1(3, 9);
    @DexIgnore
    public static /* final */ String TAG; // = "SleepDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_2_TO_5$annotations() {
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_3_TO_9$annotations() {
        }

        @DexIgnore
        public final li getMIGRATION_FROM_2_TO_5() {
            return SleepDatabase.MIGRATION_FROM_2_TO_5;
        }

        @DexIgnore
        public final li getMIGRATION_FROM_3_TO_9() {
            return SleepDatabase.MIGRATION_FROM_3_TO_9;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public abstract SleepDao sleepDao();
}
