package com.portfolio.platform.data.source.local.sleep;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.r87;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1$sleepSessionsDeferred$1", f = "SleepSummaryLocalDataSource.kt", l = {176}, m = "invokeSuspend")
public final class SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super zi5<ie4>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ r87 $downloadingRange;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource$loadData$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2(SleepSummaryLocalDataSource$loadData$Anon1 sleepSummaryLocalDataSource$loadData$Anon1, r87 r87, fb7 fb7) {
        super(2, fb7);
        this.this$0 = sleepSummaryLocalDataSource$loadData$Anon1;
        this.$downloadingRange = r87;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2 sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2 = new SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2(this.this$0, this.$downloadingRange, fb7);
        sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2.p$ = (yi7) obj;
        return sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<ie4>> fb7) {
        return ((SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            this.L$0 = this.p$;
            this.label = 1;
            obj = SleepSessionsRepository.fetchSleepSessions$default(this.this$0.this$0.mSleepSessionsRepository, (Date) this.$downloadingRange.getFirst(), (Date) this.$downloadingRange.getSecond(), 0, 0, this, 12, null);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
