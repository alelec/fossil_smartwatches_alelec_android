package com.portfolio.platform.data.source.local.label;

import com.fossil.ee7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Label {
    @DexIgnore
    public /* final */ String category;
    @DexIgnore
    public /* final */ String createdAt;
    @DexIgnore
    public /* final */ Data data;
    @DexIgnore
    public /* final */ String id;
    @DexIgnore
    public /* final */ Metadata metadata;
    @DexIgnore
    public /* final */ String name;
    @DexIgnore
    public /* final */ String updatedAt;

    @DexIgnore
    public Label(String str, String str2, String str3, Data data2, Metadata metadata2, String str4, String str5) {
        ee7.b(data2, "data");
        this.id = str;
        this.category = str2;
        this.name = str3;
        this.data = data2;
        this.metadata = metadata2;
        this.createdAt = str4;
        this.updatedAt = str5;
    }

    @DexIgnore
    public static /* synthetic */ Label copy$default(Label label, String str, String str2, String str3, Data data2, Metadata metadata2, String str4, String str5, int i, Object obj) {
        if ((i & 1) != 0) {
            str = label.id;
        }
        if ((i & 2) != 0) {
            str2 = label.category;
        }
        if ((i & 4) != 0) {
            str3 = label.name;
        }
        if ((i & 8) != 0) {
            data2 = label.data;
        }
        if ((i & 16) != 0) {
            metadata2 = label.metadata;
        }
        if ((i & 32) != 0) {
            str4 = label.createdAt;
        }
        if ((i & 64) != 0) {
            str5 = label.updatedAt;
        }
        return label.copy(str, str2, str3, data2, metadata2, str4, str5);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.category;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final Data component4() {
        return this.data;
    }

    @DexIgnore
    public final Metadata component5() {
        return this.metadata;
    }

    @DexIgnore
    public final String component6() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component7() {
        return this.updatedAt;
    }

    @DexIgnore
    public final Label copy(String str, String str2, String str3, Data data2, Metadata metadata2, String str4, String str5) {
        ee7.b(data2, "data");
        return new Label(str, str2, str3, data2, metadata2, str4, str5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Label)) {
            return false;
        }
        Label label = (Label) obj;
        return ee7.a(this.id, label.id) && ee7.a(this.category, label.category) && ee7.a(this.name, label.name) && ee7.a(this.data, label.data) && ee7.a(this.metadata, label.metadata) && ee7.a(this.createdAt, label.createdAt) && ee7.a(this.updatedAt, label.updatedAt);
    }

    @DexIgnore
    public final String getCategory() {
        return this.category;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Data getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final Metadata getMetadata() {
        return this.metadata;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.category;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Data data2 = this.data;
        int hashCode4 = (hashCode3 + (data2 != null ? data2.hashCode() : 0)) * 31;
        Metadata metadata2 = this.metadata;
        int hashCode5 = (hashCode4 + (metadata2 != null ? metadata2.hashCode() : 0)) * 31;
        String str4 = this.createdAt;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.updatedAt;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode6 + i;
    }

    @DexIgnore
    public String toString() {
        return "Label(id=" + this.id + ", category=" + this.category + ", name=" + this.name + ", data=" + this.data + ", metadata=" + this.metadata + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
