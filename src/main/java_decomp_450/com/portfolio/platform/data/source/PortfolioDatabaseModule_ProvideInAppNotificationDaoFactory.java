package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory implements Factory<InAppNotificationDao> {
    @DexIgnore
    public /* final */ Provider<InAppNotificationDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<InAppNotificationDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<InAppNotificationDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static InAppNotificationDao provideInAppNotificationDao(PortfolioDatabaseModule portfolioDatabaseModule, InAppNotificationDatabase inAppNotificationDatabase) {
        InAppNotificationDao provideInAppNotificationDao = portfolioDatabaseModule.provideInAppNotificationDao(inAppNotificationDatabase);
        c87.a(provideInAppNotificationDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideInAppNotificationDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public InAppNotificationDao get() {
        return provideInAppNotificationDao(this.module, this.dbProvider.get());
    }
}
