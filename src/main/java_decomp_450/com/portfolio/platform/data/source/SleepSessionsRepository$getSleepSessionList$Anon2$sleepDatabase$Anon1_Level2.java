package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$2$sleepDatabase$1", f = "SleepSessionsRepository.kt", l = {69}, m = "invokeSuspend")
public final class SleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super SleepDatabase>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;

    @DexIgnore
    public SleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2(fb7 fb7) {
        super(2, fb7);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2 sleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2 = new SleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2(fb7);
        sleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2.p$ = (yi7) obj;
        return sleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super SleepDatabase> fb7) {
        return ((SleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.label = 1;
            obj = pg5.d(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
