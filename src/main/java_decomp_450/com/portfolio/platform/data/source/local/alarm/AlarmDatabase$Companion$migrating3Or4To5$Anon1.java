package com.portfolio.platform.data.source.local.alarm;

import android.database.Cursor;
import com.fossil.ee7;
import com.fossil.li;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.fossil.wi;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDatabase$Companion$migrating3Or4To5$Anon1 extends li {
    @DexIgnore
    public /* final */ /* synthetic */ int $previous;
    @DexIgnore
    public /* final */ /* synthetic */ String $userId;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmDatabase$Companion$migrating3Or4To5$Anon1(int i, String str, int i2, int i3) {
        super(i2, i3);
        this.$previous = i;
        this.$userId = str;
    }

    @DexIgnore
    @Override // com.fossil.li
    public void migrate(wi wiVar) {
        AlarmDatabase$Companion$migrating3Or4To5$Anon1 alarmDatabase$Companion$migrating3Or4To5$Anon1 = this;
        ee7.b(wiVar, "database");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$cp = AlarmDatabase.TAG;
        local.d(access$getTAG$cp, "migrating3Or4To5 - previous: " + alarmDatabase$Companion$migrating3Or4To5$Anon1.$previous);
        wiVar.beginTransaction();
        try {
            wiVar.execSQL("CREATE TABLE alarm_temp (id TEXT, uri TEXT PRIMARY KEY NOT NULL, title TEXT NOT NULL, hour INTEGER NOT NULL, minute INTEGER NOT NULL, days TEXT, isActive INTEGER NOT NULL, isRepeated INTEGER NOT NULL, createdAt TEXT, updatedAt TEXT NOT NULL, pinType INTEGER NOT NULL DEFAULT 1)");
            if (alarmDatabase$Companion$migrating3Or4To5$Anon1.$previous == 4) {
                wiVar.execSQL("INSERT INTO alarm_temp (id, uri, title, hour, minute, days, isActive, isRepeated, createdAt, updatedAt, pinType) SELECT objectId, uri, alarmTitle, 0, alarmMinute, days, isActiveAlarm, isRepeat, createdAt, updatedAt, pinType FROM alarm");
            } else {
                wiVar.execSQL("INSERT INTO alarm_temp (id, uri, title, hour, minute, days, isActive, isRepeated, createdAt, updatedAt, pinType) SELECT objectId, uri, alarmTitle, 0, alarmMinute, days, isActiveAlarm, isRepeat, createdAt, updatedAt FROM alarm");
            }
            wiVar.execSQL("UPDATE alarm_temp SET hour = minute/60, minute = minute%60");
            wiVar.execSQL("DROP TABLE alarm");
            wiVar.execSQL("ALTER TABLE alarm_temp RENAME TO alarm");
            Cursor query = wiVar.query("SELECT*FROM alarm WHERE instr(uri, 'uri:') > 0 or instr(uri, ':') = 0");
            query.moveToFirst();
            while (true) {
                ee7.a((Object) query, "cursor");
                if (query.isAfterLast()) {
                    break;
                }
                StringBuilder sb = new StringBuilder();
                sb.append(alarmDatabase$Companion$migrating3Or4To5$Anon1.$userId);
                sb.append(':');
                Calendar instance = Calendar.getInstance();
                ee7.a((Object) instance, "Calendar.getInstance()");
                sb.append(instance.getTimeInMillis());
                String sb2 = sb.toString();
                String string = query.getString(query.getColumnIndex("title"));
                int i = query.getInt(query.getColumnIndex(AppFilter.COLUMN_HOUR));
                int i2 = query.getInt(query.getColumnIndex(MFSleepGoal.COLUMN_MINUTE));
                String string2 = query.getString(query.getColumnIndex(Alarm.COLUMN_DAYS));
                int i3 = query.getInt(query.getColumnIndex("isActive"));
                int i4 = query.getInt(query.getColumnIndex("isRepeated"));
                String string3 = query.getString(query.getColumnIndex("createdAt"));
                String string4 = query.getString(query.getColumnIndex("updatedAt"));
                wiVar.execSQL("INSERT INTO alarm(id, uri, title, hour, minute, days, isActive, isRepeated, createdAt, updatedAt, pinType) VALUES (null, '" + sb2 + "', '" + string + "', " + i + ", " + i2 + ", '" + string2 + "', " + i3 + ", " + i4 + ", '" + string3 + "', '" + string4 + "', 1)");
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp2 = AlarmDatabase.TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("migrating3Or4To5 uri: ");
                sb3.append(sb2);
                sb3.append(" - title: ");
                sb3.append(string);
                local2.d(access$getTAG$cp2, sb3.toString());
                query.moveToNext();
                alarmDatabase$Companion$migrating3Or4To5$Anon1 = this;
                query = query;
            }
            query.close();
            wiVar.execSQL("UPDATE alarm SET pinType = 3 WHERE instr(uri, 'uri:') > 0 or instr(uri, ':') = 0");
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().e(AlarmDatabase.TAG, "migration is failed!");
            wiVar.execSQL("DROP TABLE IF EXISTS alarm_temp");
            wiVar.execSQL("DROP TABLE IF EXISTS alarm");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS  alarm (id TEXT, uri TEXT PRIMARY KEY NOT NULL, title TEXT NOT NULL, hour INTEGER NOT NULL, minute INTEGER NOT NULL, days TEXT, isActive INTEGER NOT NULL, isRepeated INTEGER NOT NULL, createdAt TEXT, updatedAt TEXT NOT NULL, pinType INTEGER NOT NULL DEFAULT 1)");
        }
        wiVar.setTransactionSuccessful();
        wiVar.endTransaction();
    }
}
