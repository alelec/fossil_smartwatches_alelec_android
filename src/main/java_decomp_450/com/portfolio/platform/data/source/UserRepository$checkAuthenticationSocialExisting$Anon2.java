package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.UserRepository$checkAuthenticationSocialExisting$2", f = "UserRepository.kt", l = {273}, m = "invokeSuspend")
public final class UserRepository$checkAuthenticationSocialExisting$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<Boolean>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $service;
    @DexIgnore
    public /* final */ /* synthetic */ String $token;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRepository$checkAuthenticationSocialExisting$Anon2(UserRepository userRepository, String str, String str2, fb7 fb7) {
        super(2, fb7);
        this.this$0 = userRepository;
        this.$service = str;
        this.$token = str2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        UserRepository$checkAuthenticationSocialExisting$Anon2 userRepository$checkAuthenticationSocialExisting$Anon2 = new UserRepository$checkAuthenticationSocialExisting$Anon2(this.this$0, this.$service, this.$token, fb7);
        userRepository$checkAuthenticationSocialExisting$Anon2.p$ = (yi7) obj;
        return userRepository$checkAuthenticationSocialExisting$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<Boolean>> fb7) {
        return ((UserRepository$checkAuthenticationSocialExisting$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            UserRemoteDataSource access$getMUserRemoteDataSource$p = this.this$0.mUserRemoteDataSource;
            String str = this.$service;
            String str2 = this.$token;
            this.L$0 = yi7;
            this.label = 1;
            obj = access$getMUserRemoteDataSource$p.checkAuthenticationSocialExisting(str, str2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
