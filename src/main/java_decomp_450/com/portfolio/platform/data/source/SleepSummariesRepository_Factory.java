package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository_Factory implements Factory<SleepSummariesRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;

    @DexIgnore
    public SleepSummariesRepository_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceProvider = provider;
    }

    @DexIgnore
    public static SleepSummariesRepository_Factory create(Provider<ApiServiceV2> provider) {
        return new SleepSummariesRepository_Factory(provider);
    }

    @DexIgnore
    public static SleepSummariesRepository newInstance(ApiServiceV2 apiServiceV2) {
        return new SleepSummariesRepository(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public SleepSummariesRepository get() {
        return newInstance(this.mApiServiceProvider.get());
    }
}
