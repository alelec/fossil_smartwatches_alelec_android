package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.ThemeRepository$getCurrentThemeId$2", f = "ThemeRepository.kt", l = {}, m = "invokeSuspend")
public final class ThemeRepository$getCurrentThemeId$Anon2 extends zb7 implements kd7<yi7, fb7<? super String>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThemeRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThemeRepository$getCurrentThemeId$Anon2(ThemeRepository themeRepository, fb7 fb7) {
        super(2, fb7);
        this.this$0 = themeRepository;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        ThemeRepository$getCurrentThemeId$Anon2 themeRepository$getCurrentThemeId$Anon2 = new ThemeRepository$getCurrentThemeId$Anon2(this.this$0, fb7);
        themeRepository$getCurrentThemeId$Anon2.p$ = (yi7) obj;
        return themeRepository$getCurrentThemeId$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super String> fb7) {
        return ((ThemeRepository$getCurrentThemeId$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            return this.this$0.mThemeDao.getCurrentThemeId();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
