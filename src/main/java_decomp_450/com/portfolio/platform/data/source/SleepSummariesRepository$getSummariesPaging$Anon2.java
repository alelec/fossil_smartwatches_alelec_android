package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.t3;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository$getSummariesPaging$Anon2<I, O> implements t3<X, LiveData<Y>> {
    @DexIgnore
    public static /* final */ SleepSummariesRepository$getSummariesPaging$Anon2 INSTANCE; // = new SleepSummariesRepository$getSummariesPaging$Anon2();

    @DexIgnore
    public final LiveData<NetworkState> apply(SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        return sleepSummaryLocalDataSource.getMNetworkState();
    }
}
