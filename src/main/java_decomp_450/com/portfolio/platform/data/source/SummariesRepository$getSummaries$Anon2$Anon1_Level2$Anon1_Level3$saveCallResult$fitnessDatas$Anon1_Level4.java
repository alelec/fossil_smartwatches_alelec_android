package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.r87;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SummariesRepository$getSummaries$2$1$1$saveCallResult$fitnessDatas$1", f = "SummariesRepository.kt", l = {}, m = "invokeSuspend")
public final class SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4 extends zb7 implements kd7<yi7, fb7<? super List<FitnessDataWrapper>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon2.Anon1_Level2.Anon1_Level3 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4(SummariesRepository$getSummaries$Anon2.Anon1_Level2.Anon1_Level3 anon1_Level3, fb7 fb7) {
        super(2, fb7);
        this.this$0 = anon1_Level3;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4 summariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4 = new SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4(this.this$0, fb7);
        summariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4.p$ = (yi7) obj;
        return summariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super List<FitnessDataWrapper>> fb7) {
        return ((SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Date date;
        Date date2;
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            FitnessDataDao fitnessDataDao = this.this$0.this$0.$fitnessDatabase.getFitnessDataDao();
            r87 r87 = this.this$0.$downloadingDate;
            if (r87 == null || (date = (Date) r87.getFirst()) == null) {
                date = this.this$0.this$0.this$0.$startDate;
            }
            r87 r872 = this.this$0.$downloadingDate;
            if (r872 == null || (date2 = (Date) r872.getSecond()) == null) {
                date2 = this.this$0.this$0.this$0.$endDate;
            }
            return fitnessDataDao.getFitnessData(date, date2);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
