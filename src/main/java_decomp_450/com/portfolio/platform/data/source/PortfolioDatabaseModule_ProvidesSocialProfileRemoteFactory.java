package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.fossil.ep4;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesSocialProfileRemoteFactory implements Factory<ep4> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesSocialProfileRemoteFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        this.module = portfolioDatabaseModule;
        this.apiProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesSocialProfileRemoteFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        return new PortfolioDatabaseModule_ProvidesSocialProfileRemoteFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static ep4 providesSocialProfileRemote(PortfolioDatabaseModule portfolioDatabaseModule, ApiServiceV2 apiServiceV2) {
        ep4 providesSocialProfileRemote = portfolioDatabaseModule.providesSocialProfileRemote(apiServiceV2);
        c87.a(providesSocialProfileRemote, "Cannot return null from a non-@Nullable @Provides method");
        return providesSocialProfileRemote;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ep4 get() {
        return providesSocialProfileRemote(this.module, this.apiProvider.get());
    }
}
