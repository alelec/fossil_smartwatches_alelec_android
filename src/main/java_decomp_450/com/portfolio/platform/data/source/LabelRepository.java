package com.portfolio.platform.data.source;

import android.content.Context;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.zd7;
import com.fossil.zi5;
import com.portfolio.platform.data.source.local.label.Label;
import com.portfolio.platform.data.source.remote.LabelRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LabelRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "LabelRepository";
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public /* final */ LabelRemoteDataSource mRemoteSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public LabelRepository(Context context2, LabelRemoteDataSource labelRemoteDataSource) {
        ee7.b(context2, "context");
        ee7.b(labelRemoteDataSource, "mRemoteSource");
        this.context = context2;
        this.mRemoteSource = labelRemoteDataSource;
    }

    @DexIgnore
    public final Object downloadLabel(String str, fb7<? super zi5<Label>> fb7) {
        return this.mRemoteSource.getLabel(str, fb7);
    }

    @DexIgnore
    public final Context getContext() {
        return this.context;
    }
}
