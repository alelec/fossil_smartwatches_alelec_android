package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.zb7;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$upsertHybridPresetList$response$1", f = "HybridPresetRemoteDataSource.kt", l = {56}, m = "invokeSuspend")
public final class HybridPresetRemoteDataSource$upsertHybridPresetList$response$Anon1 extends zb7 implements gd7<fb7<? super fv7<ApiResponse<HybridPreset>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ie4 $jsonObject;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ HybridPresetRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridPresetRemoteDataSource$upsertHybridPresetList$response$Anon1(HybridPresetRemoteDataSource hybridPresetRemoteDataSource, ie4 ie4, fb7 fb7) {
        super(1, fb7);
        this.this$0 = hybridPresetRemoteDataSource;
        this.$jsonObject = ie4;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(fb7<?> fb7) {
        ee7.b(fb7, "completion");
        return new HybridPresetRemoteDataSource$upsertHybridPresetList$response$Anon1(this.this$0, this.$jsonObject, fb7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public final Object invoke(fb7<? super fv7<ApiResponse<HybridPreset>>> fb7) {
        return ((HybridPresetRemoteDataSource$upsertHybridPresetList$response$Anon1) create(fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            ApiServiceV2 access$getMApiServiceV2$p = this.this$0.mApiServiceV2;
            ie4 ie4 = this.$jsonObject;
            this.label = 1;
            obj = access$getMApiServiceV2$p.upsertHybridPresetList(ie4, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
