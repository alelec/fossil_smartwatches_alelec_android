package com.portfolio.platform.data.source;

import android.graphics.Bitmap;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.fossil.bi7;
import com.fossil.ee5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.mb7;
import com.fossil.mg5;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.qg5;
import com.fossil.rc7;
import com.fossil.s87;
import com.fossil.sw6;
import com.fossil.vb7;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.LocalFile;
import com.portfolio.platform.data.source.local.FileDao;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ PortfolioApp mApp;
    @DexIgnore
    public /* final */ FileDao mFileDao;
    @DexIgnore
    public /* final */ qg5 mFileDownloadManager;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return FileRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = FileRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "FileRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public FileRepository(FileDao fileDao, qg5 qg5, PortfolioApp portfolioApp) {
        ee7.b(fileDao, "mFileDao");
        ee7.b(qg5, "mFileDownloadManager");
        ee7.b(portfolioApp, "mApp");
        this.mFileDao = fileDao;
        this.mFileDownloadManager = qg5;
        this.mApp = portfolioApp;
    }

    @DexIgnore
    public static /* synthetic */ void asyncDownloadFromUrl$default(FileRepository fileRepository, String str, FileType fileType, String str2, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = null;
        }
        fileRepository.asyncDownloadFromUrl(str, fileType, str2);
    }

    @DexIgnore
    private final void deleteFileRecursively(String str) {
        File file = new File(str);
        if (file.exists()) {
            rc7.c(file);
        }
    }

    @DexIgnore
    private final void deletedFile(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "deletedFile() - filePath = " + str);
        File file = new File(str);
        if (file.exists()) {
            file.delete();
        }
    }

    @DexIgnore
    public static /* synthetic */ Object downloadFromUrl$default(FileRepository fileRepository, String str, FileType fileType, String str2, fb7 fb7, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = null;
        }
        return fileRepository.downloadFromUrl(str, fileType, str2, fb7);
    }

    @DexIgnore
    private final void upsertLocalFile(LocalFile localFile) {
        this.mFileDao.upsertLocalFile(localFile);
    }

    @DexIgnore
    public final void asyncDownloadFromUrl(String str, FileType fileType, String str2) {
        ee7.b(fileType, "type");
        if (!TextUtils.isEmpty(str)) {
            LocalFile localFileByRemoteUrl = this.mFileDao.getLocalFileByRemoteUrl(str);
            if (localFileByRemoteUrl == null) {
                String a = mg5.a(str);
                ee7.a((Object) a, "fileName");
                if (str != null) {
                    LocalFile localFile = new LocalFile(a, "", str, str2);
                    localFile.setType(fileType);
                    this.mFileDao.upsertLocalFile(localFile);
                    localFileByRemoteUrl = localFile;
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (localFileByRemoteUrl.getPinType() == 0) {
                return;
            }
            this.mFileDownloadManager.b(new qg5.b(localFileByRemoteUrl, new FileRepository$asyncDownloadFromUrl$Anon1(this, str)));
        }
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().e(TAG, "cleanUp()");
        String downloadedPath = FileUtils.getDownloadedPath(this.mApp.getApplicationContext());
        ee7.a((Object) downloadedPath, "directory");
        deleteFileRecursively(downloadedPath);
        this.mFileDao.clearLocalFileTable();
    }

    @DexIgnore
    public final void deleteBackgroundFiles() {
        File[] listFiles;
        FLogger.INSTANCE.getLocal().d(TAG, "deleteBackgroundFiles()");
        File file = new File(FileUtils.getDirectory(this.mApp.getApplicationContext(), FileType.WATCH_FACE));
        if (file.exists() && (listFiles = file.listFiles(FileRepository$deleteBackgroundFiles$Anon1.INSTANCE)) != null) {
            for (File file2 : listFiles) {
                ee7.a((Object) file2, "it");
                String path = file2.getPath();
                ee7.a((Object) path, "it.path");
                deletedFile(path);
                FileDao fileDao = this.mFileDao;
                String path2 = file2.getPath();
                ee7.a((Object) path2, "it.path");
                fileDao.deleteLocalFileByUri(path2);
            }
        }
    }

    @DexIgnore
    public final void deleteFileByName(String str, FileType fileType) {
        ee7.b(str, "fileName");
        ee7.b(fileType, "fileType");
        FLogger.INSTANCE.getLocal().e(TAG, "deleteFile() - fileName: " + str);
        String str2 = FileUtils.getDirectory(this.mApp.getApplicationContext(), fileType) + File.separator;
        if (new File(str2).exists()) {
            String str3 = str2 + str;
            deletedFile(str3);
            this.mFileDao.deleteLocalFileByUri(str3);
        }
    }

    @DexIgnore
    public final void deletedFilesByType(FileType fileType) {
        ee7.b(fileType, "fileType");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.e(str, "deletedFiles() - fileType: " + fileType);
        String directory = FileUtils.getDirectory(this.mApp.getApplicationContext(), fileType);
        ee7.a((Object) directory, "directory");
        deleteFileRecursively(directory);
        this.mFileDao.deleteLocalFileByType(fileType.getMValue());
    }

    @DexIgnore
    public final Object downloadFromUrl(String str, FileType fileType, String str2, fb7<? super Boolean> fb7) {
        LocalFile localFile;
        bi7 bi7 = new bi7(mb7.a(fb7), 1);
        if (TextUtils.isEmpty(str)) {
            s87.a aVar = s87.Companion;
            bi7.resumeWith(s87.m60constructorimpl(pb7.a(true)));
        } else {
            LocalFile localFileByRemoteUrl = this.mFileDao.getLocalFileByRemoteUrl(str);
            if (localFileByRemoteUrl == null) {
                String a = mg5.a(str);
                ee7.a((Object) a, "fileName");
                if (str != null) {
                    LocalFile localFile2 = new LocalFile(a, "", str, str2);
                    localFile2.setType(fileType);
                    this.mFileDao.upsertLocalFile(localFile2);
                    localFile = localFile2;
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (localFileByRemoteUrl.getPinType() == 0) {
                s87.a aVar2 = s87.Companion;
                bi7.resumeWith(s87.m60constructorimpl(pb7.a(true)));
            } else {
                localFile = localFileByRemoteUrl;
            }
            this.mFileDownloadManager.b(new qg5.b(localFile, new FileRepository$downloadFromUrl$$inlined$suspendCancellableCoroutine$lambda$Anon1(bi7, this, str, str2, fileType)));
        }
        Object g = bi7.g();
        if (g == nb7.a()) {
            vb7.c(fb7);
        }
        return g;
    }

    @DexIgnore
    public final void downloadPendingFile() {
        List<LocalFile> listPendingFile = this.mFileDao.getListPendingFile();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "downloadPendingFile with size " + listPendingFile.size());
        for (T t : listPendingFile) {
            this.mFileDownloadManager.b(new qg5.b(t, new FileRepository$downloadPendingFile$$inlined$forEach$lambda$Anon1(t, this)));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0027, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0028, code lost:
        com.fossil.hc7.a(r0, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        throw r4;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getBinaryString(java.lang.String r3, com.fossil.fb7<? super java.lang.String> r4) {
        /*
            r2 = this;
            java.io.File r3 = r2.getFileByRemoteUrl(r3)
            r4 = 0
            if (r3 == 0) goto L_0x000d
            java.io.FileInputStream r0 = new java.io.FileInputStream
            r0.<init>(r3)
            goto L_0x000e
        L_0x000d:
            r0 = r4
        L_0x000e:
            byte[] r3 = com.fossil.rs7.b(r0)     // Catch:{ all -> 0x0025 }
            r1 = 0
            java.lang.String r3 = android.util.Base64.encodeToString(r3, r1)     // Catch:{ all -> 0x0025 }
            java.lang.String r1 = "Base64.encodeToString(IO\u2026rray(it), Base64.DEFAULT)"
            com.fossil.ee7.a(r3, r1)     // Catch:{ all -> 0x0025 }
            com.fossil.hc7.a(r0, r4)
            java.lang.String r4 = "file?.inputStream().use \u2026Base64.DEFAULT)\n        }"
            com.fossil.ee7.a(r3, r4)
            return r3
        L_0x0025:
            r3 = move-exception
            throw r3     // Catch:{ all -> 0x0027 }
        L_0x0027:
            r4 = move-exception
            com.fossil.hc7.a(r0, r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.FileRepository.getBinaryString(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final File getFileByRemoteUrl(String str) {
        LocalFile localFileByRemoteUrl = this.mFileDao.getLocalFileByRemoteUrl(str);
        String localUri = localFileByRemoteUrl != null ? localFileByRemoteUrl.getLocalUri() : null;
        boolean a = ee5.a.a(localUri);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "getFileLocalUrl with remoteUrl " + str + " isFileExist " + a);
        if (TextUtils.isEmpty(localUri) || !a) {
            return null;
        }
        if (localUri != null) {
            return new File(localUri);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final LiveData<List<LocalFile>> getPendingFilesAsLiveData() {
        return this.mFileDao.getListPendingFileAsLiveData();
    }

    @DexIgnore
    public final String saveLocalDataFile(String str, String str2, String str3) {
        ee7.b(str2, "fileName");
        ee7.b(str3, "filePath");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = TAG;
        local.d(str4, "Start saving " + str2 + " to " + str3);
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        if (str != null) {
            Bitmap a = sw6.a(str);
            ee5 ee5 = ee5.a;
            ee7.a((Object) a, "bitmap");
            ee5.a(a, str3);
            LocalFile localFile = new LocalFile(str2, str3, "", "");
            localFile.setPinType(0);
            localFile.setType(FileType.WORKOUT_SCREEN_SHOT);
            this.mFileDao.upsertLocalFile(localFile);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str5 = TAG;
            local2.d(str5, "Done saving " + str2 + " to " + str3 + ", localUri " + localFile.getLocalUri());
            return localFile.getLocalUri();
        }
        ee7.a();
        throw null;
    }
}
