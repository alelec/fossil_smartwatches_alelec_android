package com.portfolio.platform.data.source;

import com.fossil.ai7;
import com.fossil.dc2;
import com.fossil.ee7;
import com.fossil.io3;
import com.fossil.qe7;
import com.fossil.s87;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2 implements io3 {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ai7 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ qe7 $countSizeOfLists$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $gFitSampleList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ dc2 $historyClient$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfDataSetList;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfListsOfGFitSampleList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore
    public ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(int i, dc2 dc2, qe7 qe7, ai7 ai7, int i2, ThirdPartyRepository thirdPartyRepository, List list, String str) {
        this.$sizeOfDataSetList = i;
        this.$historyClient$inlined = dc2;
        this.$countSizeOfLists$inlined = qe7;
        this.$continuation$inlined = ai7;
        this.$sizeOfListsOfGFitSampleList$inlined = i2;
        this.this$0 = thirdPartyRepository;
        this.$gFitSampleList$inlined = list;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    @Override // com.fossil.io3
    public final void onFailure(Exception exc) {
        ee7.b(exc, "it");
        FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "saveGFitSampleToGoogleFit - Fail");
        qe7 qe7 = this.$countSizeOfLists$inlined;
        int i = qe7.element + 1;
        qe7.element = i;
        if (i >= this.$sizeOfListsOfGFitSampleList$inlined * this.$sizeOfDataSetList && this.$continuation$inlined.isActive()) {
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveGFitSampleToGoogleFit - Fail");
            ai7 ai7 = this.$continuation$inlined;
            s87.a aVar = s87.Companion;
            ai7.resumeWith(s87.m60constructorimpl(null));
        }
    }
}
