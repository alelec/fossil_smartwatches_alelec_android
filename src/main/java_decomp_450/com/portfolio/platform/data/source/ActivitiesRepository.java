package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.ge5;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.vh7;
import com.fossil.x87;
import com.fossil.zd5;
import com.fossil.zd7;
import com.fossil.zi5;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.source.local.fitness.ActivitySampleDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitiesRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ ge5 mFitnessHelper;
    @DexIgnore
    public /* final */ UserRepository mUserRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ActivitiesRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingActivitiesCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(List<ActivitySample> list);
    }

    /*
    static {
        String simpleName = ActivitiesRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "ActivitiesRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ActivitiesRepository(ApiServiceV2 apiServiceV2, UserRepository userRepository, ge5 ge5) {
        ee7.b(apiServiceV2, "mApiService");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(ge5, "mFitnessHelper");
        this.mApiService = apiServiceV2;
        this.mUserRepository = userRepository;
        this.mFitnessHelper = ge5;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchActivitySamples$default(ActivitiesRepository activitiesRepository, Date date, Date date2, int i, int i2, fb7 fb7, int i3, Object obj) {
        return activitiesRepository.fetchActivitySamples(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, fb7);
    }

    @DexIgnore
    private final LiveData<List<ActivitySample>> getActivitySamplesInDate(Date date, ActivitySampleDao activitySampleDao) {
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "calendarStart");
        instance.setTime(date);
        zd5.d(instance);
        Object clone = instance.clone();
        if (clone != null) {
            Calendar calendar = (Calendar) clone;
            zd5.a(calendar);
            ee7.a((Object) calendar, "DateHelper.getEndOfDay(calendarEnd)");
            Date time = instance.getTime();
            ee7.a((Object) time, "calendarStart.time");
            Date time2 = calendar.getTime();
            ee7.a((Object) time2, "calendarEnd.time");
            return activitySampleDao.getActivitySamplesLiveData(time, time2);
        }
        throw new x87("null cannot be cast to non-null type java.util.Calendar");
    }

    @DexIgnore
    public final Object cleanUp(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new ActivitiesRepository$cleanUp$Anon2(null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object fetchActivitySamples(Date date, Date date2, int i, int i2, fb7<? super zi5<ApiResponse<Activity>>> fb7) {
        return vh7.a(qj7.b(), new ActivitiesRepository$fetchActivitySamples$Anon2(this, date, date2, i, i2, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getActivityList(java.util.Date r11, java.util.Date r12, boolean r13, com.fossil.fb7<? super androidx.lifecycle.LiveData<com.fossil.qx6<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>>>> r14) {
        /*
            r10 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon1 r0 = (com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon1 r0 = new com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon1
            r0.<init>(r10, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            boolean r11 = r0.Z$0
            java.lang.Object r11 = r0.L$2
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$1
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.ActivitiesRepository r11 = (com.portfolio.platform.data.source.ActivitiesRepository) r11
            com.fossil.t87.a(r14)
            goto L_0x0062
        L_0x0037:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003f:
            com.fossil.t87.a(r14)
            com.fossil.tk7 r14 = com.fossil.qj7.c()
            com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2 r2 = new com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2
            r9 = 0
            r4 = r2
            r5 = r10
            r6 = r11
            r7 = r12
            r8 = r13
            r4.<init>(r5, r6, r7, r8, r9)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.Z$0 = r13
            r0.label = r3
            java.lang.Object r14 = com.fossil.vh7.a(r14, r2, r0)
            if (r14 != r1) goto L_0x0062
            return r1
        L_0x0062:
            java.lang.String r11 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.ee7.a(r14, r11)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ActivitiesRepository.getActivityList(java.util.Date, java.util.Date, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object getPendingActivities(Date date, Date date2, fb7<? super List<SampleRaw>> fb7) {
        return vh7.a(qj7.b(), new ActivitiesRepository$getPendingActivities$Anon2(date, date2, null), fb7);
    }

    @DexIgnore
    public final Object insert(List<SampleRaw> list, fb7<? super zi5<List<ActivitySample>>> fb7) {
        return vh7.a(qj7.b(), new ActivitiesRepository$insert$Anon2(this, list, null), fb7);
    }

    @DexIgnore
    public final Object insertFromDevice(List<ActivitySample> list, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new ActivitiesRepository$insertFromDevice$Anon2(list, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object pushPendingActivities(PushPendingActivitiesCallback pushPendingActivitiesCallback, fb7<? super i97> fb7) {
        return vh7.a(qj7.b(), new ActivitiesRepository$pushPendingActivities$Anon2(this, pushPendingActivitiesCallback, null), fb7);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0231, code lost:
        if (r13.intValue() != 409000) goto L_0x0233;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0247, code lost:
        if (r9.intValue() != 409001) goto L_0x0249;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0185  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x018a  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0197  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x02b0 A[LOOP:1: B:82:0x02aa->B:84:0x02b0, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x02c7  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x02d3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveActivitiesToServer(java.util.List<com.portfolio.platform.data.model.room.fitness.SampleRaw> r18, com.portfolio.platform.data.source.ActivitiesRepository.PushPendingActivitiesCallback r19, com.fossil.fb7<? super com.fossil.i97> r20) {
        /*
            r17 = this;
            r1 = r17
            r0 = r20
            boolean r2 = r0 instanceof com.portfolio.platform.data.source.ActivitiesRepository$saveActivitiesToServer$Anon1
            if (r2 == 0) goto L_0x0017
            r2 = r0
            com.portfolio.platform.data.source.ActivitiesRepository$saveActivitiesToServer$Anon1 r2 = (com.portfolio.platform.data.source.ActivitiesRepository$saveActivitiesToServer$Anon1) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.portfolio.platform.data.source.ActivitiesRepository$saveActivitiesToServer$Anon1 r2 = new com.portfolio.platform.data.source.ActivitiesRepository$saveActivitiesToServer$Anon1
            r2.<init>(r1, r0)
        L_0x001c:
            java.lang.Object r0 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            r5 = 3
            r6 = 2
            java.lang.String r7 = " endIndex="
            r8 = 0
            r9 = 1
            if (r4 == 0) goto L_0x00a0
            if (r4 == r9) goto L_0x0085
            if (r4 == r6) goto L_0x0064
            if (r4 != r5) goto L_0x005c
            java.lang.Object r4 = r2.L$7
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r10 = r2.L$6
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            java.lang.Object r10 = r2.L$5
            java.util.List r10 = (java.util.List) r10
            int r10 = r2.I$1
            java.lang.Object r10 = r2.L$4
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r10 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r10
            java.lang.Object r11 = r2.L$3
            java.util.List r11 = (java.util.List) r11
            int r12 = r2.I$0
            java.lang.Object r13 = r2.L$2
            com.portfolio.platform.data.source.ActivitiesRepository$PushPendingActivitiesCallback r13 = (com.portfolio.platform.data.source.ActivitiesRepository.PushPendingActivitiesCallback) r13
            java.lang.Object r14 = r2.L$1
            java.util.List r14 = (java.util.List) r14
            java.lang.Object r15 = r2.L$0
            com.portfolio.platform.data.source.ActivitiesRepository r15 = (com.portfolio.platform.data.source.ActivitiesRepository) r15
            com.fossil.t87.a(r0)
            r6 = 3
            goto L_0x017a
        L_0x005c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x0064:
            java.lang.Object r4 = r2.L$5
            java.util.List r4 = (java.util.List) r4
            int r10 = r2.I$1
            java.lang.Object r11 = r2.L$4
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r11 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r11
            java.lang.Object r12 = r2.L$3
            java.util.List r12 = (java.util.List) r12
            int r13 = r2.I$0
            java.lang.Object r14 = r2.L$2
            com.portfolio.platform.data.source.ActivitiesRepository$PushPendingActivitiesCallback r14 = (com.portfolio.platform.data.source.ActivitiesRepository.PushPendingActivitiesCallback) r14
            java.lang.Object r15 = r2.L$1
            java.util.List r15 = (java.util.List) r15
            java.lang.Object r5 = r2.L$0
            com.portfolio.platform.data.source.ActivitiesRepository r5 = (com.portfolio.platform.data.source.ActivitiesRepository) r5
            com.fossil.t87.a(r0)
            goto L_0x011f
        L_0x0085:
            java.lang.Object r4 = r2.L$3
            java.util.List r4 = (java.util.List) r4
            int r5 = r2.I$0
            java.lang.Object r10 = r2.L$2
            com.portfolio.platform.data.source.ActivitiesRepository$PushPendingActivitiesCallback r10 = (com.portfolio.platform.data.source.ActivitiesRepository.PushPendingActivitiesCallback) r10
            java.lang.Object r11 = r2.L$1
            java.util.List r11 = (java.util.List) r11
            java.lang.Object r12 = r2.L$0
            com.portfolio.platform.data.source.ActivitiesRepository r12 = (com.portfolio.platform.data.source.ActivitiesRepository) r12
            com.fossil.t87.a(r0)
            r16 = r11
            r11 = r5
            r5 = r16
            goto L_0x00c3
        L_0x00a0:
            com.fossil.t87.a(r0)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            com.fossil.pg5 r0 = com.fossil.pg5.i
            r2.L$0 = r1
            r5 = r18
            r2.L$1 = r5
            r10 = r19
            r2.L$2 = r10
            r2.I$0 = r8
            r2.L$3 = r4
            r2.label = r9
            java.lang.Object r0 = r0.b(r2)
            if (r0 != r3) goto L_0x00c1
            return r3
        L_0x00c1:
            r12 = r1
            r11 = 0
        L_0x00c3:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            r13 = r11
        L_0x00c6:
            int r11 = r5.size()
            if (r13 >= r11) goto L_0x02ee
            int r11 = r13 + 100
            int r14 = r5.size()
            if (r11 <= r14) goto L_0x00d8
            int r11 = r5.size()
        L_0x00d8:
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r15 = com.portfolio.platform.data.source.ActivitiesRepository.TAG
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r8 = "saveActivitiesToServer startIndex="
            r9.append(r8)
            r9.append(r13)
            r9.append(r7)
            r9.append(r11)
            java.lang.String r8 = r9.toString()
            r14.d(r15, r8)
            java.util.List r8 = r5.subList(r13, r11)
            r2.L$0 = r12
            r2.L$1 = r5
            r2.L$2 = r10
            r2.I$0 = r13
            r2.L$3 = r4
            r2.L$4 = r0
            r2.I$1 = r11
            r2.L$5 = r8
            r2.label = r6
            java.lang.Object r9 = r12.insert(r8, r2)
            if (r9 != r3) goto L_0x0117
            return r3
        L_0x0117:
            r15 = r5
            r14 = r10
            r10 = r11
            r5 = r12
            r11 = r0
            r12 = r4
            r4 = r8
            r0 = r9
        L_0x011f:
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            int r8 = r13 + 100
            boolean r9 = r0 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x0197
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r13 = com.portfolio.platform.data.source.ActivitiesRepository.TAG
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r1 = "saveActivitiesToServer success, bravo!!! startIndex="
            r6.append(r1)
            r6.append(r8)
            r6.append(r7)
            r6.append(r10)
            java.lang.String r1 = r6.toString()
            r9.d(r13, r1)
            r1 = r0
            com.fossil.bj5 r1 = (com.fossil.bj5) r1
            java.lang.Object r1 = r1.a()
            if (r1 == 0) goto L_0x0192
            java.util.List r1 = (java.util.List) r1
            r2.L$0 = r5
            r2.L$1 = r15
            r2.L$2 = r14
            r2.I$0 = r8
            r2.L$3 = r12
            r2.L$4 = r11
            r2.I$1 = r10
            r2.L$5 = r4
            r2.L$6 = r0
            r2.L$7 = r1
            r6 = 3
            r2.label = r6
            r9 = 0
            java.lang.Object r0 = r5.updateActivityPinType(r4, r9, r2)
            if (r0 != r3) goto L_0x0173
            return r3
        L_0x0173:
            r4 = r1
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r15
            r15 = r5
            r12 = r8
        L_0x017a:
            r11.addAll(r4)
            int r0 = r14.size()
            if (r12 < r0) goto L_0x018a
            if (r13 == 0) goto L_0x02ee
            r13.onSuccess(r11)
            goto L_0x02ee
        L_0x018a:
            r0 = r10
            r4 = r11
            r10 = r13
            r5 = r14
            r13 = r12
            r12 = r15
            goto L_0x02e7
        L_0x0192:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x0197:
            r6 = 3
            boolean r1 = r0 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x02dd
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r9 = com.portfolio.platform.data.source.ActivitiesRepository.TAG
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r6 = "saveActivitiesToServer failed, errorCode="
            r13.append(r6)
            r6 = r0
            com.fossil.yi5 r6 = (com.fossil.yi5) r6
            r18 = r2
            int r2 = r6.a()
            r13.append(r2)
            r2 = 32
            r13.append(r2)
            java.lang.String r2 = "startIndex="
            r13.append(r2)
            r13.append(r8)
            r13.append(r7)
            r13.append(r10)
            java.lang.String r2 = r13.toString()
            r1.d(r9, r2)
            int r1 = r6.a()
            r2 = 422(0x1a6, float:5.91E-43)
            if (r1 != r2) goto L_0x02cd
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.lang.String r2 = r6.b()
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x02cd
            com.portfolio.platform.data.source.ActivitiesRepository$saveActivitiesToServer$type$Anon1 r2 = new com.portfolio.platform.data.source.ActivitiesRepository$saveActivitiesToServer$type$Anon1
            r2.<init>()
            java.lang.reflect.Type r2 = r2.getType()
            com.google.gson.Gson r9 = new com.google.gson.Gson     // Catch:{ Exception -> 0x0271 }
            r9.<init>()     // Catch:{ Exception -> 0x0271 }
            com.fossil.yi5 r0 = (com.fossil.yi5) r0     // Catch:{ Exception -> 0x0271 }
            java.lang.String r0 = r0.b()     // Catch:{ Exception -> 0x0271 }
            java.lang.Object r0 = r9.a(r0, r2)     // Catch:{ Exception -> 0x0271 }
            java.lang.String r2 = "Gson().fromJson(repoResponse.errorItems, type)"
            com.fossil.ee7.a(r0, r2)     // Catch:{ Exception -> 0x0271 }
            com.portfolio.platform.data.source.remote.UpsertApiResponse r0 = (com.portfolio.platform.data.source.remote.UpsertApiResponse) r0     // Catch:{ Exception -> 0x0271 }
            java.util.List r0 = r0.get_items()     // Catch:{ Exception -> 0x0271 }
            boolean r2 = r0.isEmpty()     // Catch:{ Exception -> 0x0271 }
            r9 = 1
            r2 = r2 ^ r9
            if (r2 == 0) goto L_0x026f
            int r2 = r0.size()     // Catch:{ Exception -> 0x0271 }
            r10 = 0
        L_0x021b:
            if (r10 >= r2) goto L_0x026f
            java.lang.Object r13 = r0.get(r10)     // Catch:{ Exception -> 0x0271 }
            com.portfolio.platform.data.Activity r13 = (com.portfolio.platform.data.Activity) r13     // Catch:{ Exception -> 0x0271 }
            java.lang.Integer r13 = r13.getCode()     // Catch:{ Exception -> 0x0271 }
            r9 = 409000(0x63da8, float:5.73131E-40)
            if (r13 != 0) goto L_0x022d
            goto L_0x0233
        L_0x022d:
            int r13 = r13.intValue()     // Catch:{ Exception -> 0x0271 }
            if (r13 == r9) goto L_0x025c
        L_0x0233:
            java.lang.Object r9 = r0.get(r10)     // Catch:{ Exception -> 0x0271 }
            com.portfolio.platform.data.Activity r9 = (com.portfolio.platform.data.Activity) r9     // Catch:{ Exception -> 0x0271 }
            java.lang.Integer r9 = r9.getCode()     // Catch:{ Exception -> 0x0271 }
            r13 = 409001(0x63da9, float:5.73132E-40)
            if (r9 != 0) goto L_0x0243
            goto L_0x0249
        L_0x0243:
            int r9 = r9.intValue()     // Catch:{ Exception -> 0x0271 }
            if (r9 == r13) goto L_0x025c
        L_0x0249:
            java.lang.Object r9 = r0.get(r10)     // Catch:{ Exception -> 0x0271 }
            com.portfolio.platform.data.Activity r9 = (com.portfolio.platform.data.Activity) r9     // Catch:{ Exception -> 0x0271 }
            java.lang.String r9 = r9.getId()     // Catch:{ Exception -> 0x0271 }
            boolean r9 = android.text.TextUtils.isEmpty(r9)     // Catch:{ Exception -> 0x0271 }
            if (r9 != 0) goto L_0x025a
            goto L_0x025c
        L_0x025a:
            r13 = 0
            goto L_0x0269
        L_0x025c:
            java.lang.Object r9 = r4.get(r10)     // Catch:{ Exception -> 0x0271 }
            com.portfolio.platform.data.model.room.fitness.SampleRaw r9 = (com.portfolio.platform.data.model.room.fitness.SampleRaw) r9     // Catch:{ Exception -> 0x0271 }
            r13 = 0
            r9.setPinType(r13)     // Catch:{ Exception -> 0x026d }
            r1.add(r9)     // Catch:{ Exception -> 0x026d }
        L_0x0269:
            int r10 = r10 + 1
            r9 = 1
            goto L_0x021b
        L_0x026d:
            r0 = move-exception
            goto L_0x0273
        L_0x026f:
            r13 = 0
            goto L_0x0294
        L_0x0271:
            r0 = move-exception
            r13 = 0
        L_0x0273:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r9 = com.portfolio.platform.data.source.ActivitiesRepository.TAG
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r13 = "saveActivitiesToServer ex="
            r10.append(r13)
            r0.printStackTrace()
            com.fossil.i97 r0 = com.fossil.i97.a
            r10.append(r0)
            java.lang.String r0 = r10.toString()
            r2.d(r9, r0)
        L_0x0294:
            com.portfolio.platform.data.source.local.fitness.SampleRawDao r0 = r11.sampleRawDao()
            r0.upsertListActivitySample(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r1 = 10
            int r1 = com.fossil.x97.a(r4, r1)
            r0.<init>(r1)
            java.util.Iterator r1 = r4.iterator()
        L_0x02aa:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x02be
            java.lang.Object r2 = r1.next()
            com.portfolio.platform.data.model.room.fitness.SampleRaw r2 = (com.portfolio.platform.data.model.room.fitness.SampleRaw) r2
            com.portfolio.platform.data.model.room.fitness.ActivitySample r2 = r2.toActivitySample()
            r0.add(r2)
            goto L_0x02aa
        L_0x02be:
            r12.addAll(r0)
            int r0 = r15.size()
            if (r8 < r0) goto L_0x02cd
            if (r14 == 0) goto L_0x02ee
            r14.onSuccess(r12)
            goto L_0x02ee
        L_0x02cd:
            int r0 = r15.size()
            if (r8 < r0) goto L_0x02df
            if (r14 == 0) goto L_0x02ee
            int r0 = r6.a()
            r14.onFail(r0)
            goto L_0x02ee
        L_0x02dd:
            r18 = r2
        L_0x02df:
            r2 = r18
            r13 = r8
            r0 = r11
            r4 = r12
            r10 = r14
            r12 = r5
            r5 = r15
        L_0x02e7:
            r6 = 2
            r8 = 0
            r9 = 1
            r1 = r17
            goto L_0x00c6
        L_0x02ee:
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ActivitiesRepository.saveActivitiesToServer(java.util.List, com.portfolio.platform.data.source.ActivitiesRepository$PushPendingActivitiesCallback, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object updateActivityPinType(List<SampleRaw> list, int i, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new ActivitiesRepository$updateActivityPinType$Anon2(list, i, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }
}
