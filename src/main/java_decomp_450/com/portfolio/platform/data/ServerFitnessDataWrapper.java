package com.portfolio.platform.data;

import com.fossil.ee7;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.model.fitnessdata.ActiveMinuteWrapper;
import com.portfolio.platform.data.model.fitnessdata.CalorieWrapper;
import com.portfolio.platform.data.model.fitnessdata.DistanceWrapper;
import com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper;
import com.portfolio.platform.data.model.fitnessdata.RestingWrapper;
import com.portfolio.platform.data.model.fitnessdata.SleepSessionWrapper;
import com.portfolio.platform.data.model.fitnessdata.StepWrapper;
import com.portfolio.platform.data.model.fitnessdata.StressWrapper;
import com.portfolio.platform.service.syncmodel.WrapperTapEventSummary;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerFitnessDataWrapper {
    @DexIgnore
    public /* final */ ActiveMinuteWrapper activeMinute;
    @DexIgnore
    public /* final */ CalorieWrapper calorie;
    @DexIgnore
    public /* final */ DistanceWrapper distance;
    @DexIgnore
    public /* final */ DateTime endTime;
    @DexIgnore
    public /* final */ List<WrapperTapEventSummary> goalTrackings;
    @DexIgnore
    public /* final */ HeartRateWrapper heartRate;
    @DexIgnore
    public /* final */ List<RestingWrapper> restings;
    @DexIgnore
    public /* final */ String serialNumber;
    @DexIgnore
    public /* final */ List<SleepSessionWrapper> sleeps;
    @DexIgnore
    public /* final */ DateTime startTime;
    @DexIgnore
    public /* final */ StepWrapper step;
    @DexIgnore
    public /* final */ StressWrapper stress;
    @DexIgnore
    public /* final */ DateTime syncTime;
    @DexIgnore
    public /* final */ int timezoneOffsetInSecond;
    @DexIgnore
    public /* final */ List<ServerWorkoutSessionWrapper> workouts;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r21v0, resolved type: java.util.List<? extends com.portfolio.platform.service.syncmodel.WrapperTapEventSummary> */
    /* JADX WARN: Multi-variable type inference failed */
    public ServerFitnessDataWrapper(DateTime dateTime, DateTime dateTime2, DateTime dateTime3, int i, String str, StepWrapper stepWrapper, ActiveMinuteWrapper activeMinuteWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, StressWrapper stressWrapper, List<RestingWrapper> list, HeartRateWrapper heartRateWrapper, List<SleepSessionWrapper> list2, List<ServerWorkoutSessionWrapper> list3, List<? extends WrapperTapEventSummary> list4) {
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        ee7.b(dateTime3, "syncTime");
        ee7.b(str, "serialNumber");
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.syncTime = dateTime3;
        this.timezoneOffsetInSecond = i;
        this.serialNumber = str;
        this.step = stepWrapper;
        this.activeMinute = activeMinuteWrapper;
        this.calorie = calorieWrapper;
        this.distance = distanceWrapper;
        this.stress = stressWrapper;
        this.restings = list;
        this.heartRate = heartRateWrapper;
        this.sleeps = list2;
        this.workouts = list3;
        this.goalTrackings = list4;
    }

    @DexIgnore
    public static /* synthetic */ ServerFitnessDataWrapper copy$default(ServerFitnessDataWrapper serverFitnessDataWrapper, DateTime dateTime, DateTime dateTime2, DateTime dateTime3, int i, String str, StepWrapper stepWrapper, ActiveMinuteWrapper activeMinuteWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, StressWrapper stressWrapper, List list, HeartRateWrapper heartRateWrapper, List list2, List list3, List list4, int i2, Object obj) {
        return serverFitnessDataWrapper.copy((i2 & 1) != 0 ? serverFitnessDataWrapper.startTime : dateTime, (i2 & 2) != 0 ? serverFitnessDataWrapper.endTime : dateTime2, (i2 & 4) != 0 ? serverFitnessDataWrapper.syncTime : dateTime3, (i2 & 8) != 0 ? serverFitnessDataWrapper.timezoneOffsetInSecond : i, (i2 & 16) != 0 ? serverFitnessDataWrapper.serialNumber : str, (i2 & 32) != 0 ? serverFitnessDataWrapper.step : stepWrapper, (i2 & 64) != 0 ? serverFitnessDataWrapper.activeMinute : activeMinuteWrapper, (i2 & 128) != 0 ? serverFitnessDataWrapper.calorie : calorieWrapper, (i2 & 256) != 0 ? serverFitnessDataWrapper.distance : distanceWrapper, (i2 & 512) != 0 ? serverFitnessDataWrapper.stress : stressWrapper, (i2 & 1024) != 0 ? serverFitnessDataWrapper.restings : list, (i2 & 2048) != 0 ? serverFitnessDataWrapper.heartRate : heartRateWrapper, (i2 & 4096) != 0 ? serverFitnessDataWrapper.sleeps : list2, (i2 & 8192) != 0 ? serverFitnessDataWrapper.workouts : list3, (i2 & 16384) != 0 ? serverFitnessDataWrapper.goalTrackings : list4);
    }

    @DexIgnore
    public final DateTime component1() {
        return this.startTime;
    }

    @DexIgnore
    public final StressWrapper component10() {
        return this.stress;
    }

    @DexIgnore
    public final List<RestingWrapper> component11() {
        return this.restings;
    }

    @DexIgnore
    public final HeartRateWrapper component12() {
        return this.heartRate;
    }

    @DexIgnore
    public final List<SleepSessionWrapper> component13() {
        return this.sleeps;
    }

    @DexIgnore
    public final List<ServerWorkoutSessionWrapper> component14() {
        return this.workouts;
    }

    @DexIgnore
    public final List<WrapperTapEventSummary> component15() {
        return this.goalTrackings;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.endTime;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.syncTime;
    }

    @DexIgnore
    public final int component4() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final String component5() {
        return this.serialNumber;
    }

    @DexIgnore
    public final StepWrapper component6() {
        return this.step;
    }

    @DexIgnore
    public final ActiveMinuteWrapper component7() {
        return this.activeMinute;
    }

    @DexIgnore
    public final CalorieWrapper component8() {
        return this.calorie;
    }

    @DexIgnore
    public final DistanceWrapper component9() {
        return this.distance;
    }

    @DexIgnore
    public final ServerFitnessDataWrapper copy(DateTime dateTime, DateTime dateTime2, DateTime dateTime3, int i, String str, StepWrapper stepWrapper, ActiveMinuteWrapper activeMinuteWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, StressWrapper stressWrapper, List<RestingWrapper> list, HeartRateWrapper heartRateWrapper, List<SleepSessionWrapper> list2, List<ServerWorkoutSessionWrapper> list3, List<? extends WrapperTapEventSummary> list4) {
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        ee7.b(dateTime3, "syncTime");
        ee7.b(str, "serialNumber");
        return new ServerFitnessDataWrapper(dateTime, dateTime2, dateTime3, i, str, stepWrapper, activeMinuteWrapper, calorieWrapper, distanceWrapper, stressWrapper, list, heartRateWrapper, list2, list3, list4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ServerFitnessDataWrapper)) {
            return false;
        }
        ServerFitnessDataWrapper serverFitnessDataWrapper = (ServerFitnessDataWrapper) obj;
        return ee7.a(this.startTime, serverFitnessDataWrapper.startTime) && ee7.a(this.endTime, serverFitnessDataWrapper.endTime) && ee7.a(this.syncTime, serverFitnessDataWrapper.syncTime) && this.timezoneOffsetInSecond == serverFitnessDataWrapper.timezoneOffsetInSecond && ee7.a(this.serialNumber, serverFitnessDataWrapper.serialNumber) && ee7.a(this.step, serverFitnessDataWrapper.step) && ee7.a(this.activeMinute, serverFitnessDataWrapper.activeMinute) && ee7.a(this.calorie, serverFitnessDataWrapper.calorie) && ee7.a(this.distance, serverFitnessDataWrapper.distance) && ee7.a(this.stress, serverFitnessDataWrapper.stress) && ee7.a(this.restings, serverFitnessDataWrapper.restings) && ee7.a(this.heartRate, serverFitnessDataWrapper.heartRate) && ee7.a(this.sleeps, serverFitnessDataWrapper.sleeps) && ee7.a(this.workouts, serverFitnessDataWrapper.workouts) && ee7.a(this.goalTrackings, serverFitnessDataWrapper.goalTrackings);
    }

    @DexIgnore
    public final ActiveMinuteWrapper getActiveMinute() {
        return this.activeMinute;
    }

    @DexIgnore
    public final CalorieWrapper getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public final DistanceWrapper getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final List<WrapperTapEventSummary> getGoalTrackings() {
        return this.goalTrackings;
    }

    @DexIgnore
    public final HeartRateWrapper getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final List<RestingWrapper> getRestings() {
        return this.restings;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final List<SleepSessionWrapper> getSleeps() {
        return this.sleeps;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final StepWrapper getStep() {
        return this.step;
    }

    @DexIgnore
    public final StressWrapper getStress() {
        return this.stress;
    }

    @DexIgnore
    public final DateTime getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final List<ServerWorkoutSessionWrapper> getWorkouts() {
        return this.workouts;
    }

    @DexIgnore
    public int hashCode() {
        DateTime dateTime = this.startTime;
        int i = 0;
        int hashCode = (dateTime != null ? dateTime.hashCode() : 0) * 31;
        DateTime dateTime2 = this.endTime;
        int hashCode2 = (hashCode + (dateTime2 != null ? dateTime2.hashCode() : 0)) * 31;
        DateTime dateTime3 = this.syncTime;
        int hashCode3 = (((hashCode2 + (dateTime3 != null ? dateTime3.hashCode() : 0)) * 31) + this.timezoneOffsetInSecond) * 31;
        String str = this.serialNumber;
        int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
        StepWrapper stepWrapper = this.step;
        int hashCode5 = (hashCode4 + (stepWrapper != null ? stepWrapper.hashCode() : 0)) * 31;
        ActiveMinuteWrapper activeMinuteWrapper = this.activeMinute;
        int hashCode6 = (hashCode5 + (activeMinuteWrapper != null ? activeMinuteWrapper.hashCode() : 0)) * 31;
        CalorieWrapper calorieWrapper = this.calorie;
        int hashCode7 = (hashCode6 + (calorieWrapper != null ? calorieWrapper.hashCode() : 0)) * 31;
        DistanceWrapper distanceWrapper = this.distance;
        int hashCode8 = (hashCode7 + (distanceWrapper != null ? distanceWrapper.hashCode() : 0)) * 31;
        StressWrapper stressWrapper = this.stress;
        int hashCode9 = (hashCode8 + (stressWrapper != null ? stressWrapper.hashCode() : 0)) * 31;
        List<RestingWrapper> list = this.restings;
        int hashCode10 = (hashCode9 + (list != null ? list.hashCode() : 0)) * 31;
        HeartRateWrapper heartRateWrapper = this.heartRate;
        int hashCode11 = (hashCode10 + (heartRateWrapper != null ? heartRateWrapper.hashCode() : 0)) * 31;
        List<SleepSessionWrapper> list2 = this.sleeps;
        int hashCode12 = (hashCode11 + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<ServerWorkoutSessionWrapper> list3 = this.workouts;
        int hashCode13 = (hashCode12 + (list3 != null ? list3.hashCode() : 0)) * 31;
        List<WrapperTapEventSummary> list4 = this.goalTrackings;
        if (list4 != null) {
            i = list4.hashCode();
        }
        return hashCode13 + i;
    }

    @DexIgnore
    public String toString() {
        return "ServerFitnessDataWrapper(startTime=" + this.startTime + ", endTime=" + this.endTime + ", syncTime=" + this.syncTime + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", serialNumber=" + this.serialNumber + ", step=" + this.step + ", activeMinute=" + this.activeMinute + ", calorie=" + this.calorie + ", distance=" + this.distance + ", stress=" + this.stress + ", restings=" + this.restings + ", heartRate=" + this.heartRate + ", sleeps=" + this.sleeps + ", workouts=" + this.workouts + ", goalTrackings=" + this.goalTrackings + ")";
    }
}
