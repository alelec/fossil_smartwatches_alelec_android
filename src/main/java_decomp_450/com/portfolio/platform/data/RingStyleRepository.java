package com.portfolio.platform.data;

import com.fossil.ee7;
import com.fossil.zd7;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.local.RingStyleDao;
import com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyleRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "RingStyleRepository";
    @DexIgnore
    public /* final */ FileRepository mFileRepository;
    @DexIgnore
    public /* final */ RingStyleDao mRingStyleDao;
    @DexIgnore
    public /* final */ RingStyleRemoteDataSource mRingStyleRemoveSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public RingStyleRepository(RingStyleDao ringStyleDao, RingStyleRemoteDataSource ringStyleRemoteDataSource, FileRepository fileRepository) {
        ee7.b(ringStyleDao, "mRingStyleDao");
        ee7.b(ringStyleRemoteDataSource, "mRingStyleRemoveSource");
        ee7.b(fileRepository, "mFileRepository");
        this.mRingStyleDao = ringStyleDao;
        this.mRingStyleRemoveSource = ringStyleRemoteDataSource;
        this.mFileRepository = fileRepository;
    }

    @DexIgnore
    private final void saveRingStyle(DianaComplicationRingStyle dianaComplicationRingStyle) {
        this.mRingStyleDao.insertRingStyle(dianaComplicationRingStyle);
    }

    @DexIgnore
    public final void cleanUp() {
        this.mRingStyleDao.clearAllData();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0131, code lost:
        if (r9 == null) goto L_0x0135;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01d3  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadRingStyleList(java.lang.String r20, boolean r21, com.fossil.fb7<? super com.fossil.i97> r22) {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r2 = r21
            r3 = r22
            boolean r4 = r3 instanceof com.portfolio.platform.data.RingStyleRepository$downloadRingStyleList$Anon1
            if (r4 == 0) goto L_0x001b
            r4 = r3
            com.portfolio.platform.data.RingStyleRepository$downloadRingStyleList$Anon1 r4 = (com.portfolio.platform.data.RingStyleRepository$downloadRingStyleList$Anon1) r4
            int r5 = r4.label
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = r5 & r6
            if (r7 == 0) goto L_0x001b
            int r5 = r5 - r6
            r4.label = r5
            goto L_0x0020
        L_0x001b:
            com.portfolio.platform.data.RingStyleRepository$downloadRingStyleList$Anon1 r4 = new com.portfolio.platform.data.RingStyleRepository$downloadRingStyleList$Anon1
            r4.<init>(r0, r3)
        L_0x0020:
            java.lang.Object r3 = r4.result
            java.lang.Object r5 = com.fossil.nb7.a()
            int r6 = r4.label
            r7 = 1
            if (r6 == 0) goto L_0x0043
            if (r6 != r7) goto L_0x003b
            boolean r1 = r4.Z$0
            java.lang.Object r1 = r4.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r4.L$0
            com.portfolio.platform.data.RingStyleRepository r1 = (com.portfolio.platform.data.RingStyleRepository) r1
            com.fossil.t87.a(r3)
            goto L_0x0058
        L_0x003b:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0043:
            com.fossil.t87.a(r3)
            com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource r3 = r0.mRingStyleRemoveSource
            r4.L$0 = r0
            r4.L$1 = r1
            r4.Z$0 = r2
            r4.label = r7
            java.lang.Object r3 = r3.getRingStyleList(r1, r2, r4)
            if (r3 != r5) goto L_0x0057
            return r5
        L_0x0057:
            r1 = r0
        L_0x0058:
            com.fossil.zi5 r3 = (com.fossil.zi5) r3
            boolean r2 = r3 instanceof com.fossil.bj5
            java.lang.String r4 = "RingStyleRepository"
            if (r2 == 0) goto L_0x01d3
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            java.lang.String r2 = r2.c()
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r8 = "downloadRingStyleList() success activeSerial "
            r6.append(r8)
            r6.append(r2)
            java.lang.String r6 = r6.toString()
            r5.d(r4, r6)
            boolean r5 = com.fossil.mh7.a(r2)
            if (r5 == 0) goto L_0x008d
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x008d:
            com.fossil.bj5 r3 = (com.fossil.bj5) r3
            java.lang.Object r3 = r3.a()
            java.util.ArrayList r3 = (java.util.ArrayList) r3
            com.portfolio.platform.data.source.local.RingStyleDao r5 = r1.mRingStyleDao
            com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r5 = r5.getRingStylesBySerial(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "downloadRingStyleList() success serverRingstyle "
            r8.append(r9)
            r8.append(r3)
            java.lang.String r9 = " localRingstyle "
            r8.append(r9)
            r8.append(r5)
            java.lang.String r8 = r8.toString()
            r6.d(r4, r8)
            r6 = 0
            if (r5 != 0) goto L_0x00c2
            goto L_0x0135
        L_0x00c2:
            if (r3 == 0) goto L_0x0134
            boolean r8 = r3.isEmpty()
            r8 = r8 ^ r7
            if (r8 == 0) goto L_0x0134
            java.util.Iterator r8 = r3.iterator()
        L_0x00cf:
            boolean r9 = r8.hasNext()
            if (r9 == 0) goto L_0x0114
            java.lang.Object r9 = r8.next()
            r10 = r9
            com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r10 = (com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle) r10
            java.lang.String r11 = r10.getCategory()
            java.lang.String r12 = r5.getCategory()
            boolean r11 = com.fossil.ee7.a(r11, r12)
            if (r11 == 0) goto L_0x0108
            com.portfolio.platform.data.model.diana.preset.Data r11 = r10.getData()
            com.portfolio.platform.data.model.diana.preset.Data r12 = r5.getData()
            boolean r11 = com.fossil.ee7.a(r11, r12)
            if (r11 == 0) goto L_0x0108
            com.portfolio.platform.data.model.diana.preset.MetaData r10 = r10.getMetaData()
            com.portfolio.platform.data.model.diana.preset.MetaData r11 = r5.getMetaData()
            boolean r10 = com.fossil.ee7.a(r10, r11)
            if (r10 == 0) goto L_0x0108
            r10 = 1
            goto L_0x0109
        L_0x0108:
            r10 = 0
        L_0x0109:
            java.lang.Boolean r10 = com.fossil.pb7.a(r10)
            boolean r10 = r10.booleanValue()
            if (r10 == 0) goto L_0x00cf
            goto L_0x0115
        L_0x0114:
            r9 = 0
        L_0x0115:
            com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r9 = (com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle) r9
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r10 = "downloadRingStyleList() success matchItem "
            r8.append(r10)
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r5.d(r4, r8)
            if (r9 != 0) goto L_0x0134
            goto L_0x0135
        L_0x0134:
            r7 = 0
        L_0x0135:
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r8 = "downloadRingStyleList() success should insert "
            r6.append(r8)
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r5.d(r4, r6)
            if (r7 != 0) goto L_0x0154
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x0154:
            if (r3 == 0) goto L_0x01f7
            java.util.Iterator r3 = r3.iterator()
        L_0x015a:
            boolean r5 = r3.hasNext()
            if (r5 == 0) goto L_0x01f7
            java.lang.Object r5 = r3.next()
            com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r5 = (com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle) r5
            r5.setSerial(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "saveRingStyle "
            r7.append(r8)
            r7.append(r5)
            java.lang.String r8 = " url "
            r7.append(r8)
            com.portfolio.platform.data.model.diana.preset.Data r8 = r5.getData()
            java.lang.String r8 = r8.getUrl()
            r7.append(r8)
            java.lang.String r8 = " previewUrl "
            r7.append(r8)
            com.portfolio.platform.data.model.diana.preset.Data r8 = r5.getData()
            java.lang.String r8 = r8.getPreviewUrl()
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.d(r4, r7)
            java.lang.String r6 = "item"
            com.fossil.ee7.a(r5, r6)
            r1.saveRingStyle(r5)
            com.portfolio.platform.data.source.FileRepository r7 = r1.mFileRepository
            com.portfolio.platform.data.model.diana.preset.Data r6 = r5.getData()
            java.lang.String r8 = r6.getUrl()
            com.misfit.frameworks.buttonservice.model.FileType r9 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            r10 = 0
            r11 = 4
            r12 = 0
            com.portfolio.platform.data.source.FileRepository.asyncDownloadFromUrl$default(r7, r8, r9, r10, r11, r12)
            com.portfolio.platform.data.source.FileRepository r13 = r1.mFileRepository
            com.portfolio.platform.data.model.diana.preset.Data r5 = r5.getData()
            java.lang.String r14 = r5.getPreviewUrl()
            com.misfit.frameworks.buttonservice.model.FileType r15 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            r16 = 0
            r17 = 4
            r18 = 0
            com.portfolio.platform.data.source.FileRepository.asyncDownloadFromUrl$default(r13, r14, r15, r16, r17, r18)
            goto L_0x015a
        L_0x01d3:
            boolean r1 = r3 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x01f7
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r5 = "downloadRingStyleList() Failed, serverError = "
            r2.append(r5)
            com.fossil.yi5 r3 = (com.fossil.yi5) r3
            com.portfolio.platform.data.model.ServerError r3 = r3.c()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.e(r4, r2)
        L_0x01f7:
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.RingStyleRepository.downloadRingStyleList(java.lang.String, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final List<DianaComplicationRingStyle> getRingStyles() {
        return this.mRingStyleDao.getRingStyles();
    }

    @DexIgnore
    public final DianaComplicationRingStyle getRingStylesBySerial(String str) {
        ee7.b(str, "serialNumber");
        return this.mRingStyleDao.getRingStylesBySerial(str);
    }
}
