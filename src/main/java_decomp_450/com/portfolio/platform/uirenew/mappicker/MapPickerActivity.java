package com.portfolio.platform.uirenew.mappicker;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.ro6;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MapPickerActivity extends cl5 {
    @DexIgnore
    public static /* final */ a y; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Fragment fragment, double d, double d2, String str, int i, Object obj) {
            double d3 = 0.0d;
            double d4 = (i & 2) != 0 ? 0.0d : d;
            if ((i & 4) == 0) {
                d3 = d2;
            }
            aVar.a(fragment, d4, d3, (i & 8) != 0 ? "" : str);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, double d, double d2, String str) {
            ee7.b(fragment, "context");
            ee7.b(str, "address");
            Intent intent = new Intent(fragment.getContext(), MapPickerActivity.class);
            intent.putExtra("latitude", d);
            intent.putExtra("longitude", d2);
            intent.putExtra("address", str);
            fragment.startActivityForResult(intent, 100);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Fragment b = getSupportFragmentManager().b(2131362149);
        if (b != null) {
            b.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        String str;
        double d;
        double d2;
        String stringExtra;
        super.onCreate(bundle);
        setContentView(2131558439);
        if (((ro6) getSupportFragmentManager().b(2131362149)) == null) {
            Intent intent = getIntent();
            String str2 = "";
            double d3 = 0.0d;
            if (intent != null) {
                double doubleExtra = intent.hasExtra("latitude") ? intent.getDoubleExtra("latitude", 0.0d) : 0.0d;
                if (intent.hasExtra("longitude")) {
                    d3 = intent.getDoubleExtra("longitude", 0.0d);
                }
                if (intent.hasExtra("address") && (stringExtra = intent.getStringExtra("address")) != null) {
                    str2 = stringExtra;
                }
                str = str2;
                d = d3;
                d2 = doubleExtra;
            } else {
                str = str2;
                d2 = 0.0d;
                d = 0.0d;
            }
            a(ro6.q.a(d2, d, str), "MapPickerFragment", 2131362149);
        }
    }
}
