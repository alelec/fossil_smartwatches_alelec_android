package com.portfolio.platform.uirenew.pairing.scanning;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.rx6;
import com.fossil.vr6;
import com.fossil.xr6;
import com.fossil.zd7;
import com.fossil.zq6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingActivity extends cl5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);
    @DexIgnore
    public static /* final */ String z;
    @DexIgnore
    public xr6 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return PairingActivity.z;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, boolean z) {
            ee7.b(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = a();
            local.d(a, "start isOnboarding=" + z);
            Intent intent = new Intent(context, PairingActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = PairingActivity.class.getSimpleName();
        ee7.a((Object) simpleName, "PairingActivity::class.java.simpleName");
        z = simpleName;
    }
    */

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Fragment b = getSupportFragmentManager().b(2131362149);
        if (b != null) {
            b.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.cl5
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        zq6 zq6 = (zq6) getSupportFragmentManager().b(2131362149);
        Intent intent = getIntent();
        boolean z2 = false;
        if (intent != null) {
            z2 = intent.getBooleanExtra("IS_ONBOARDING_FLOW", false);
        }
        if (zq6 == null) {
            zq6 = zq6.j.a(z2);
            a(zq6, 2131362149);
        }
        PortfolioApp.g0.c().f().a(new vr6(zq6)).a(this);
        if (bundle != null) {
            xr6 xr6 = this.y;
            if (xr6 != null) {
                xr6.d(bundle.getBoolean("KEY_IS_PAIR_DEVICE_FAIL_POPUP"));
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        xr6 xr6 = this.y;
        if (xr6 != null) {
            bundle.putBoolean("KEY_IS_PAIR_DEVICE_FAIL_POPUP", xr6.v());
            super.onSaveInstanceState(bundle);
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onStart() {
        super.onStart();
        q();
    }

    @DexIgnore
    public final void q() {
        if (TextUtils.isEmpty(PortfolioApp.g0.c().c())) {
            FLogger.INSTANCE.getLocal().d(e(), "Service Tracking - startForegroundService in PairingActivity");
            rx6.a.a(this, MFDeviceService.class, Constants.START_FOREGROUND_ACTION);
            rx6.a.a(this, ButtonService.class, Constants.START_FOREGROUND_ACTION);
            return;
        }
        FLogger.INSTANCE.getLocal().d(e(), "Skip start service");
    }
}
