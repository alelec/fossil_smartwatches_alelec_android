package com.portfolio.platform.uirenew.splash;

import android.os.Bundle;
import com.fossil.cl5;
import com.fossil.mv6;
import com.fossil.ut6;
import com.fossil.wt6;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SplashScreenActivity extends cl5 {
    @DexIgnore
    public wt6 y;

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.cl5
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        mv6 mv6 = (mv6) getSupportFragmentManager().b(2131362149);
        if (mv6 == null) {
            mv6 = mv6.h.a();
            a(mv6, 2131362149);
        }
        PortfolioApp.g0.c().f().a(new ut6(mv6)).a(this);
    }
}
