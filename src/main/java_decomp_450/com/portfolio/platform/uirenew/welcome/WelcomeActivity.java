package com.portfolio.platform.uirenew.welcome;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.rv6;
import com.fossil.rx6;
import com.fossil.sv6;
import com.fossil.tv6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WelcomeActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public sv6 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            ee7.b(context, "context");
            context.startActivity(new Intent(context, WelcomeActivity.class));
        }

        @DexIgnore
        public final void b(Context context) {
            ee7.b(context, "context");
            Intent intent = new Intent(context, WelcomeActivity.class);
            intent.addFlags(268468224);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        rv6 rv6 = (rv6) getSupportFragmentManager().b(2131362149);
        if (rv6 == null) {
            rv6 = new rv6();
            a(rv6, "WelcomeFragment", 2131362149);
        }
        PortfolioApp.g0.c().f().a(new tv6(rv6)).a(this);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onStart() {
        super.onStart();
        FLogger.INSTANCE.getLocal().e(e(), "Service Tracking - startForegroundService in WelcomeActivity");
        rx6.a.a(this, MFDeviceService.class, Constants.STOP_FOREGROUND_ACTION);
        rx6.a.a(this, ButtonService.class, Constants.STOP_FOREGROUND_ACTION);
    }
}
