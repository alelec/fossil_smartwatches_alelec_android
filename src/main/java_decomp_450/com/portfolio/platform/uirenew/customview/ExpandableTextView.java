package com.portfolio.platform.uirenew.customview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import androidx.appcompat.widget.AppCompatTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ExpandableTextView extends AppCompatTextView {
    @DexIgnore
    public /* final */ List<c> e;
    @DexIgnore
    public TimeInterpolator f;
    @DexIgnore
    public TimeInterpolator g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public int p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            ExpandableTextView.this.setHeight(((Integer) valueAnimator.getAnimatedValue()).intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends AnimatorListenerAdapter {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ExpandableTextView.this.g();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(ExpandableTextView expandableTextView);
    }

    @DexIgnore
    public ExpandableTextView(Context context) {
        this(context, null);
    }

    @DexIgnore
    public boolean e() {
        if (this.j || this.i || this.h < 0) {
            return false;
        }
        f();
        measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
        this.p = getMeasuredHeight();
        this.i = true;
        setMaxLines(Integer.MAX_VALUE);
        measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
        ValueAnimator ofInt = ValueAnimator.ofInt(this.p, getMeasuredHeight());
        ofInt.addUpdateListener(new a());
        ofInt.addListener(new b());
        ofInt.setInterpolator(this.f);
        ofInt.setDuration(750L).start();
        return true;
    }

    @DexIgnore
    public final void f() {
        for (c cVar : this.e) {
            cVar.a(this);
        }
    }

    @DexIgnore
    public void g() {
        setMaxHeight(Integer.MAX_VALUE);
        setMinHeight(0);
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.height = -2;
        setLayoutParams(layoutParams);
        this.j = true;
        this.i = false;
    }

    @DexIgnore
    public TimeInterpolator getCollapseInterpolator() {
        return this.g;
    }

    @DexIgnore
    public TimeInterpolator getExpandInterpolator() {
        return this.f;
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatTextView
    public void onMeasure(int i2, int i3) {
        if (this.h == 0 && !this.j && !this.i) {
            i3 = View.MeasureSpec.makeMeasureSpec(0, 1073741824);
        }
        super.onMeasure(i2, i3);
    }

    @DexIgnore
    public void setCollapseInterpolator(TimeInterpolator timeInterpolator) {
        this.g = timeInterpolator;
    }

    @DexIgnore
    public void setExpandInterpolator(TimeInterpolator timeInterpolator) {
        this.f = timeInterpolator;
    }

    @DexIgnore
    public void setInterpolator(TimeInterpolator timeInterpolator) {
        this.f = timeInterpolator;
        this.g = timeInterpolator;
    }

    @DexIgnore
    public ExpandableTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public ExpandableTextView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.h = getMaxLines();
        this.e = new ArrayList();
        this.f = new AccelerateDecelerateInterpolator();
        this.g = new AccelerateDecelerateInterpolator();
    }
}
