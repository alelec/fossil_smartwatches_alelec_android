package com.portfolio.platform.uirenew.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ee7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecyclerViewEmptySupport extends RecyclerView {
    @DexIgnore
    public View a;
    @DexIgnore
    public /* final */ a b; // = new a(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.AdapterDataObserver {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewEmptySupport a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(RecyclerViewEmptySupport recyclerViewEmptySupport) {
            this.a = recyclerViewEmptySupport;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void a() {
            if (this.a.getAdapter() != null && this.a.getEmptyView$app_fossilRelease() != null) {
                RecyclerView.g adapter = this.a.getAdapter();
                if (adapter != null) {
                    ee7.a((Object) adapter, "adapter!!");
                    if (adapter.getItemCount() == 0) {
                        View emptyView$app_fossilRelease = this.a.getEmptyView$app_fossilRelease();
                        if (emptyView$app_fossilRelease != null) {
                            emptyView$app_fossilRelease.setVisibility(0);
                            this.a.setVisibility(8);
                            return;
                        }
                        ee7.a();
                        throw null;
                    }
                    View emptyView$app_fossilRelease2 = this.a.getEmptyView$app_fossilRelease();
                    if (emptyView$app_fossilRelease2 != null) {
                        emptyView$app_fossilRelease2.setVisibility(8);
                        this.a.setVisibility(0);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewEmptySupport(Context context) {
        super(context);
        ee7.b(context, "context");
    }

    @DexIgnore
    public final View getEmptyView$app_fossilRelease() {
        return this.a;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView
    public void setAdapter(RecyclerView.g<?> gVar) {
        super.setAdapter(gVar);
        if (gVar != null) {
            gVar.registerAdapterDataObserver(this.b);
        }
        this.b.a();
    }

    @DexIgnore
    public final void setEmptyView(View view) {
        ee7.b(view, "emptyView");
        this.a = view;
    }

    @DexIgnore
    public final void setEmptyView$app_fossilRelease(View view) {
        this.a = view;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewEmptySupport(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewEmptySupport(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
    }
}
