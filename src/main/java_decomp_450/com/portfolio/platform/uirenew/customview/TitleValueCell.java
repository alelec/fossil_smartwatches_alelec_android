package com.portfolio.platform.uirenew.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.ee7;
import com.fossil.pl4;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TitleValueCell extends ConstraintLayout {
    @DexIgnore
    public FlexibleTextView v;
    @DexIgnore
    public FlexibleTextView w;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TitleValueCell(Context context) {
        super(context);
        ee7.b(context, "context");
        a(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        View inflate = ViewGroup.inflate(context, 2131558825, this);
        View findViewById = inflate.findViewById(2131362517);
        ee7.a((Object) findViewById, "view.findViewById(R.id.ftv_title)");
        this.v = (FlexibleTextView) findViewById;
        View findViewById2 = inflate.findViewById(2131362525);
        ee7.a((Object) findViewById2, "view.findViewById(R.id.ftv_value)");
        this.w = (FlexibleTextView) findViewById2;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.TitleValueCell);
        String string = obtainStyledAttributes.getString(0);
        String string2 = obtainStyledAttributes.getString(1);
        obtainStyledAttributes.recycle();
        FlexibleTextView flexibleTextView = this.v;
        if (flexibleTextView != null) {
            if (string == null) {
                string = "";
            }
            flexibleTextView.setText(string);
            FlexibleTextView flexibleTextView2 = this.w;
            if (flexibleTextView2 != null) {
                if (string2 == null) {
                    string2 = "";
                }
                flexibleTextView2.setText(string2);
                return;
            }
            ee7.d("ftvValue");
            throw null;
        }
        ee7.d("ftvTitle");
        throw null;
    }

    @DexIgnore
    public final void setTitle(String str) {
        ee7.b(str, "name");
        FlexibleTextView flexibleTextView = this.v;
        if (flexibleTextView != null) {
            flexibleTextView.setText(str);
        } else {
            ee7.d("ftvTitle");
            throw null;
        }
    }

    @DexIgnore
    public final void setValue(String str) {
        ee7.b(str, "value");
        FlexibleTextView flexibleTextView = this.w;
        if (flexibleTextView != null) {
            flexibleTextView.setText(str);
        } else {
            ee7.d("ftvValue");
            throw null;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TitleValueCell(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attributeSet");
        a(context, attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TitleValueCell(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attributeSet");
        a(context, attributeSet);
    }
}
