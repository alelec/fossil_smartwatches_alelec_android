package com.portfolio.platform.uirenew.onboarding.profilesetup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.iq6;
import com.fossil.kq6;
import com.fossil.oo5;
import com.fossil.tj4;
import com.fossil.x87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileSetupActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public kq6 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, SignUpEmailAuth signUpEmailAuth) {
            ee7.b(context, "context");
            ee7.b(signUpEmailAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ProfileSetupActivity", "start with auth=" + signUpEmailAuth);
            Intent intent = new Intent(context, ProfileSetupActivity.class);
            intent.putExtra("EMAIL_AUTH", signUpEmailAuth);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, SignUpSocialAuth signUpSocialAuth) {
            ee7.b(context, "context");
            ee7.b(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ProfileSetupActivity", "startSetUpSocial with " + signUpSocialAuth);
            Intent intent = new Intent(context, ProfileSetupActivity.class);
            intent.putExtra("SOCIAL_AUTH", signUpSocialAuth);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.cl5
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        oo5 oo5 = (oo5) getSupportFragmentManager().b(2131362149);
        if (oo5 == null) {
            oo5 = oo5.s.a();
            a(oo5, "ProfileSetupFragment", 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (oo5 != null) {
            f.a(new iq6(oo5)).a(this);
            Intent intent = getIntent();
            if (intent != null) {
                if (intent.hasExtra("SOCIAL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(e(), "Retrieve from intent socialAuth");
                    kq6 kq6 = this.y;
                    if (kq6 != null) {
                        Parcelable parcelableExtra = intent.getParcelableExtra("SOCIAL_AUTH");
                        ee7.a((Object) parcelableExtra, "it.getParcelableExtra(co\u2026ms.Constants.SOCIAL_AUTH)");
                        kq6.a((SignUpSocialAuth) parcelableExtra);
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                }
                if (intent.hasExtra("EMAIL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(e(), "Retrieve from intent emailAuth");
                    kq6 kq62 = this.y;
                    if (kq62 != null) {
                        Parcelable parcelableExtra2 = intent.getParcelableExtra("EMAIL_AUTH");
                        ee7.a((Object) parcelableExtra2, "it.getParcelableExtra(co\u2026ums.Constants.EMAIL_AUTH)");
                        kq62.a((SignUpEmailAuth) parcelableExtra2);
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                }
                if (intent.hasExtra("IS_UPDATE_PROFILE_PROCESS")) {
                    boolean booleanExtra = intent.getBooleanExtra("IS_UPDATE_PROFILE_PROCESS", false);
                    kq6 kq63 = this.y;
                    if (kq63 != null) {
                        kq63.f(booleanExtra);
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                }
            }
            if (bundle != null) {
                if (bundle.containsKey("SOCIAL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(e(), "Retrieve from savedInstanceState socialAuth");
                    SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) bundle.getParcelable("SOCIAL_AUTH");
                    if (signUpSocialAuth != null) {
                        kq6 kq64 = this.y;
                        if (kq64 != null) {
                            kq64.a(signUpSocialAuth);
                        } else {
                            ee7.d("mPresenter");
                            throw null;
                        }
                    }
                }
                if (bundle.containsKey("EMAIL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(e(), "Retrieve from savedInstanceState emailAuth");
                    SignUpEmailAuth signUpEmailAuth = (SignUpEmailAuth) bundle.getParcelable("EMAIL_AUTH");
                    if (signUpEmailAuth != null) {
                        kq6 kq65 = this.y;
                        if (kq65 != null) {
                            kq65.a(signUpEmailAuth);
                        } else {
                            ee7.d("mPresenter");
                            throw null;
                        }
                    }
                }
                if (bundle.containsKey("IS_UPDATE_PROFILE_PROCESS")) {
                    boolean z2 = bundle.getBoolean("IS_UPDATE_PROFILE_PROCESS");
                    kq6 kq66 = this.y;
                    if (kq66 != null) {
                        kq66.f(z2);
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                }
            }
        } else {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupContract.View");
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        kq6 kq6 = this.y;
        if (kq6 != null) {
            SignUpEmailAuth l = kq6.l();
            if (l != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String e = e();
                local.d(e, "onSaveInstanceState put emailAuth " + l);
                bundle.putParcelable("EMAIL_AUTH", l);
            }
            kq6 kq62 = this.y;
            if (kq62 != null) {
                SignUpSocialAuth s = kq62.s();
                if (s != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String e2 = e();
                    local2.d(e2, "onSaveInstanceState put socialAuth " + s);
                    bundle.putParcelable("SOCIAL_AUTH", s);
                }
                kq6 kq63 = this.y;
                if (kq63 != null) {
                    bundle.putBoolean("IS_UPDATE_PROFILE_PROCESS", kq63.z());
                    super.onSaveInstanceState(bundle);
                    return;
                }
                ee7.d("mPresenter");
                throw null;
            }
            ee7.d("mPresenter");
            throw null;
        }
        ee7.d("mPresenter");
        throw null;
    }
}
