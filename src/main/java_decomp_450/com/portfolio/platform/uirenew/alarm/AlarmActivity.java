package com.portfolio.platform.uirenew.alarm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.hq5;
import com.fossil.iq5;
import com.fossil.mq5;
import com.fossil.tj4;
import com.fossil.x87;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public mq5 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str, ArrayList<Alarm> arrayList, Alarm alarm) {
            ee7.b(context, "context");
            ee7.b(str, "deviceId");
            ee7.b(arrayList, "currentAlarms");
            Intent intent = new Intent(context, AlarmActivity.class);
            intent.putExtra("EXTRA_DEVICE_ID", str);
            intent.putExtra("EXTRA_ALARM", alarm);
            intent.putParcelableArrayListExtra("EXTRA_ALARMS", arrayList);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.cl5
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        String stringExtra = getIntent().getStringExtra("EXTRA_DEVICE_ID");
        Alarm alarm = (Alarm) getIntent().getParcelableExtra("EXTRA_ALARM");
        ArrayList parcelableArrayListExtra = getIntent().getParcelableArrayListExtra("EXTRA_ALARMS");
        hq5 hq5 = (hq5) getSupportFragmentManager().b(2131362149);
        if (hq5 == null) {
            hq5 = hq5.r.b();
            a(hq5, hq5.r.a(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (hq5 != null) {
            ee7.a((Object) stringExtra, "deviceId");
            ee7.a((Object) parcelableArrayListExtra, "alarms");
            f.a(new iq5(hq5, stringExtra, parcelableArrayListExtra, alarm)).a(this);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.alarm.AlarmContract.View");
    }
}
