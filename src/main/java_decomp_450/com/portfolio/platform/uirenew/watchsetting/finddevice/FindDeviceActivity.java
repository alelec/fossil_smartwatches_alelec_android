package com.portfolio.platform.uirenew.watchsetting.finddevice;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.gv6;
import com.fossil.iv6;
import com.fossil.lu6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FindDeviceActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public iv6 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str) {
            ee7.b(context, "context");
            ee7.b(str, "serial");
            Intent intent = new Intent(context, FindDeviceActivity.class);
            intent.putExtra("SERIAL", str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDeviceActivity", "start with " + str);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        lu6 lu6 = (lu6) getSupportFragmentManager().b(2131362149);
        if (lu6 == null) {
            lu6 = lu6.v.a();
            a(lu6, "FindDeviceFragment", 2131362149);
        }
        PortfolioApp.g0.c().f().a(new gv6(lu6)).a(this);
        if (bundle != null && bundle.containsKey("SERIAL")) {
            String string = bundle.getString("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = e();
            local.d(e, "retrieve serial from savedInstanceState " + string);
            if (string != null) {
                iv6 iv6 = this.y;
                if (iv6 != null) {
                    iv6.b(string);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        }
        if (getIntent() != null) {
            String stringExtra = getIntent().getStringExtra("SERIAL");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String e2 = e();
            local2.d(e2, "retrieve serial from intent " + stringExtra);
            iv6 iv62 = this.y;
            if (iv62 != null) {
                ee7.a((Object) stringExtra, "serial");
                iv62.b(stringExtra);
                return;
            }
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        iv6 iv6 = this.y;
        if (iv6 != null) {
            bundle.putString("SERIAL", iv6.k());
            super.onSaveInstanceState(bundle);
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }
}
