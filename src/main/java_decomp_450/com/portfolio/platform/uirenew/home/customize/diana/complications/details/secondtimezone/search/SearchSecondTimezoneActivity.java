package com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.b26;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.s06;
import com.fossil.tj4;
import com.fossil.x87;
import com.fossil.z16;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchSecondTimezoneActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public b26 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            ee7.b(fragment, "fragment");
            ee7.b(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), SearchSecondTimezoneActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 100);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        String str;
        FLogger.INSTANCE.getLocal().d(e(), "onCreate");
        super.onCreate(bundle);
        setContentView(2131558428);
        s06 s06 = (s06) getSupportFragmentManager().b(2131362149);
        if (s06 == null) {
            s06 = s06.p.a();
            a(s06, s06.p.b(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (s06 != null) {
            f.a(new z16(s06)).a(this);
            Intent intent = getIntent();
            if (intent != null) {
                str = intent.getStringExtra(Constants.USER_SETTING);
                ee7.a((Object) str, "it.getStringExtra(Constants.JSON_KEY_SETTINGS)");
            } else {
                str = "";
            }
            b26 b26 = this.y;
            if (b26 != null) {
                b26.a(str);
            } else {
                ee7.d("mSearchSecondTimezonePresenter");
                throw null;
            }
        } else {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneContract.View");
        }
    }
}
