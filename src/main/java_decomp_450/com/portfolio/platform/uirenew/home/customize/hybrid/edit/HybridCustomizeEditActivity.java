package com.portfolio.platform.uirenew.home.customize.hybrid.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.a9;
import com.fossil.bl5;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.g6;
import com.fossil.h6;
import com.fossil.j56;
import com.fossil.rj4;
import com.fossil.v56;
import com.fossil.x87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridCustomizeEditActivity extends cl5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public j56 A;
    @DexIgnore
    public v56 y;
    @DexIgnore
    public rj4 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            ee7.b(context, "context");
            ee7.b(str, "presetId");
            ee7.b(str2, "microAppPos");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditActivity", "start - presetId=" + str + ", microAppPos=" + str2);
            Intent intent = new Intent(context, HybridCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str2);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(FragmentActivity fragmentActivity, String str, ArrayList<a9<View, String>> arrayList, List<? extends a9<CustomizeWidget, String>> list, String str2) {
            ee7.b(fragmentActivity, "context");
            ee7.b(str, "presetId");
            ee7.b(arrayList, "views");
            ee7.b(list, "customizeWidgetViews");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditActivity", "startForResultAnimation() - presetId=" + str + ", microAppPos=" + str2);
            Intent intent = new Intent(fragmentActivity, HybridCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str2);
            Iterator<? extends a9<CustomizeWidget, String>> it = list.iterator();
            while (it.hasNext()) {
                a9 a9Var = (a9) it.next();
                arrayList.add(new a9<>(a9Var.a, a9Var.b));
                Bundle bundle = new Bundle();
                bl5.a aVar = bl5.b;
                F f = a9Var.a;
                if (f != null) {
                    ee7.a((Object) f, "wcPair.first!!");
                    aVar.a(f, bundle);
                    F f2 = a9Var.a;
                    if (f2 != null) {
                        ee7.a((Object) f2, "wcPair.first!!");
                        intent.putExtra(f2.getTransitionName(), bundle);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            Object[] array = arrayList.toArray(new a9[0]);
            if (array != null) {
                a9[] a9VarArr = (a9[]) array;
                h6 a = h6.a(fragmentActivity, (a9[]) Arrays.copyOf(a9VarArr, a9VarArr.length));
                ee7.a((Object) a, "ActivityOptionsCompat.ma\u2026context, *viewsTypeArray)");
                g6.a(fragmentActivity, intent, 100, a.a());
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Fragment b = getSupportFragmentManager().b(2131362149);
        if (b != null) {
            b.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.cl5
    public void onBackPressed() {
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0122  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x012e  */
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r14) {
        /*
            r13 = this;
            java.lang.String r0 = "KEY_ORIGINAL_PRESET"
            java.lang.String r1 = "KEY_CURRENT_PRESET"
            java.lang.Class<com.portfolio.platform.data.model.room.microapp.HybridPreset> r2 = com.portfolio.platform.data.model.room.microapp.HybridPreset.class
            super.onCreate(r14)
            r3 = 2131558428(0x7f0d001c, float:1.8742172E38)
            r13.setContentView(r3)
            android.content.Intent r3 = r13.getIntent()
            java.lang.String r4 = "KEY_PRESET_ID"
            java.lang.String r3 = r3.getStringExtra(r4)
            android.content.Intent r4 = r13.getIntent()
            java.lang.String r5 = "KEY_PRESET_WATCH_APP_POS_SELECTED"
            java.lang.String r4 = r4.getStringExtra(r5)
            androidx.fragment.app.FragmentManager r6 = r13.getSupportFragmentManager()
            r7 = 2131362149(0x7f0a0165, float:1.834407E38)
            androidx.fragment.app.Fragment r6 = r6.b(r7)
            com.fossil.r56 r6 = (com.fossil.r56) r6
            if (r6 != 0) goto L_0x003c
            com.fossil.r56 r6 = new com.fossil.r56
            r6.<init>()
            java.lang.String r8 = "DianaCustomizeEditFragment"
            r13.a(r6, r8, r7)
        L_0x003c:
            com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r7 = r7.c()
            com.fossil.tj4 r7 = r7.f()
            com.fossil.t56 r8 = new com.fossil.t56
            java.lang.String r9 = "presetId"
            com.fossil.ee7.a(r3, r9)
            java.lang.String r9 = "microAppPos"
            com.fossil.ee7.a(r4, r9)
            r8.<init>(r6, r3, r4)
            com.fossil.o56 r6 = r7.a(r8)
            r6.a(r13)
            com.fossil.rj4 r6 = r13.z
            r7 = 0
            if (r6 == 0) goto L_0x0132
            androidx.lifecycle.ViewModelProvider r6 = com.fossil.je.a(r13, r6)
            java.lang.Class<com.fossil.j56> r8 = com.fossil.j56.class
            com.fossil.he r6 = r6.a(r8)
            java.lang.String r8 = "ViewModelProviders.of(th\u2026izeViewModel::class.java)"
            com.fossil.ee7.a(r6, r8)
            com.fossil.j56 r6 = (com.fossil.j56) r6
            r13.A = r6
            java.lang.String r6 = "mHybridCustomizeViewModel"
            if (r14 != 0) goto L_0x0094
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r0 = r13.e()
            java.lang.String r1 = "init from initialize state"
            r14.d(r0, r1)
            com.fossil.j56 r14 = r13.A
            if (r14 == 0) goto L_0x0090
            r14.a(r3, r4)
            goto L_0x012d
        L_0x0090:
            com.fossil.ee7.d(r6)
            throw r7
        L_0x0094:
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r9 = r13.e()
            java.lang.String r10 = "init from savedInstanceState"
            r8.d(r9, r10)
            com.fossil.be4 r8 = new com.fossil.be4
            r8.<init>()
            com.portfolio.platform.gson.HybridPresetDeserializer r9 = new com.portfolio.platform.gson.HybridPresetDeserializer
            r9.<init>()
            r8.a(r2, r9)
            com.google.gson.Gson r8 = r8.a()
            boolean r9 = r14.containsKey(r1)     // Catch:{ Exception -> 0x00fb }
            if (r9 == 0) goto L_0x00e7
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x00fb }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()     // Catch:{ Exception -> 0x00fb }
            java.lang.String r10 = r13.e()     // Catch:{ Exception -> 0x00fb }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fb }
            r11.<init>()     // Catch:{ Exception -> 0x00fb }
            java.lang.String r12 = "parse gson "
            r11.append(r12)     // Catch:{ Exception -> 0x00fb }
            java.lang.String r12 = r14.getString(r1)     // Catch:{ Exception -> 0x00fb }
            r11.append(r12)     // Catch:{ Exception -> 0x00fb }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x00fb }
            r9.d(r10, r11)     // Catch:{ Exception -> 0x00fb }
            java.lang.String r1 = r14.getString(r1)     // Catch:{ Exception -> 0x00fb }
            java.lang.Object r1 = r8.a(r1, r2)     // Catch:{ Exception -> 0x00fb }
            com.portfolio.platform.data.model.room.microapp.HybridPreset r1 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r1     // Catch:{ Exception -> 0x00fb }
            goto L_0x00e8
        L_0x00e7:
            r1 = r7
        L_0x00e8:
            boolean r9 = r14.containsKey(r0)     // Catch:{ Exception -> 0x00f9 }
            if (r9 == 0) goto L_0x011b
            java.lang.String r0 = r14.getString(r0)     // Catch:{ Exception -> 0x00f9 }
            java.lang.Object r0 = r8.a(r0, r2)     // Catch:{ Exception -> 0x00f9 }
            com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r0     // Catch:{ Exception -> 0x00f9 }
            goto L_0x011c
        L_0x00f9:
            r0 = move-exception
            goto L_0x00fd
        L_0x00fb:
            r0 = move-exception
            r1 = r7
        L_0x00fd:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r8 = r13.e()
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "exception when parse GSON when retrieve from saveInstanceState "
            r9.append(r10)
            r9.append(r0)
            java.lang.String r0 = r9.toString()
            r2.d(r8, r0)
        L_0x011b:
            r0 = r7
        L_0x011c:
            boolean r2 = r14.containsKey(r5)
            if (r2 == 0) goto L_0x0126
            java.lang.String r4 = r14.getString(r5)
        L_0x0126:
            com.fossil.j56 r14 = r13.A
            if (r14 == 0) goto L_0x012e
            r14.a(r3, r0, r1, r4)
        L_0x012d:
            return
        L_0x012e:
            com.fossil.ee7.d(r6)
            throw r7
        L_0x0132:
            java.lang.String r14 = "viewModelFactory"
            com.fossil.ee7.d(r14)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity.onCreate(android.os.Bundle):void");
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        v56 v56 = this.y;
        if (v56 != null) {
            v56.a(bundle);
            if (bundle != null) {
                super.onSaveInstanceState(bundle);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }
}
