package com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.p16;
import com.fossil.q16;
import com.fossil.s16;
import com.fossil.tj4;
import com.fossil.x87;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Ringtone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchRingPhoneActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public s16 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            ee7.b(fragment, "context");
            ee7.b(str, "selectedRingtone");
            Intent intent = new Intent(fragment.getContext(), SearchRingPhoneActivity.class);
            intent.putExtra("KEY_SELECTED_RINGPHONE", str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 104);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        Ringtone ringtone;
        super.onCreate(bundle);
        setContentView(2131558439);
        p16 p16 = (p16) getSupportFragmentManager().b(2131362149);
        if (p16 == null) {
            p16 = p16.p.b();
            a(p16, p16.p.a(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (p16 != null) {
            f.a(new q16(p16)).a(this);
            Intent intent = getIntent();
            if (intent != null) {
                s16 s16 = this.y;
                if (s16 != null) {
                    String stringExtra = intent.getStringExtra("KEY_SELECTED_RINGPHONE");
                    ee7.a((Object) stringExtra, "it.getStringExtra(KEY_SELECTED_RINGPHONE)");
                    s16.a(stringExtra);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
            if (bundle != null && (ringtone = (Ringtone) bundle.getParcelable("KEY_SELECTED_RINGPHONE")) != null) {
                s16 s162 = this.y;
                if (s162 != null) {
                    s162.b(ringtone);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneContract.View");
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        s16 s16 = this.y;
        if (s16 != null) {
            bundle.putParcelable("KEY_SELECTED_RINGPHONE", s16.h());
            super.onSaveInstanceState(bundle);
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }
}
