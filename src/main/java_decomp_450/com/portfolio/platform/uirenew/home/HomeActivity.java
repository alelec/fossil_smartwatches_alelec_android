package com.portfolio.platform.uirenew.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.bs5;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.wr5;
import com.fossil.zd7;
import com.fossil.zr5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public bs5 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Context context, Integer num, int i, Object obj) {
            if ((i & 2) != 0) {
                num = 0;
            }
            aVar.a(context, num);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, Integer num) {
            ee7.b(context, "context");
            Intent intent = new Intent(context, HomeActivity.class);
            intent.addFlags(268468224);
            intent.putExtra("OUT_STATE_DASHBOARD_CURRENT_TAB", num);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        bs5 bs5 = this.y;
        if (bs5 != null) {
            bs5.a(i, i2, intent);
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        wr5 wr5 = (wr5) getSupportFragmentManager().b(2131362149);
        if (wr5 == null) {
            wr5 = wr5.y.a();
            a(wr5, 2131362149);
        }
        PortfolioApp.g0.c().f().a(new zr5(wr5)).a(this);
        if (bundle != null) {
            bs5 bs5 = this.y;
            if (bs5 != null) {
                bs5.a(bundle.getInt("OUT_STATE_DASHBOARD_CURRENT_TAB", 0));
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        } else {
            Intent intent = getIntent();
            if (intent != null && intent.getExtras() != null) {
                Intent intent2 = getIntent();
                if (intent2 != null) {
                    Bundle extras = intent2.getExtras();
                    if (extras == null) {
                        ee7.a();
                        throw null;
                    } else if (extras.containsKey("OUT_STATE_DASHBOARD_CURRENT_TAB")) {
                        bs5 bs52 = this.y;
                        if (bs52 != null) {
                            Intent intent3 = getIntent();
                            if (intent3 != null) {
                                Bundle extras2 = intent3.getExtras();
                                if (extras2 != null) {
                                    bs52.a(extras2.getInt("OUT_STATE_DASHBOARD_CURRENT_TAB", 0));
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.d("mPresenter");
                            throw null;
                        }
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        bs5 bs5 = this.y;
        if (bs5 != null) {
            bundle.putInt("OUT_STATE_DASHBOARD_CURRENT_TAB", bs5.h());
            super.onSaveInstanceState(bundle);
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onStart() {
        super.onStart();
        a(MFDeviceService.class, ButtonService.class);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onStop() {
        super.onStop();
        b(MFDeviceService.class, ButtonService.class);
    }
}
