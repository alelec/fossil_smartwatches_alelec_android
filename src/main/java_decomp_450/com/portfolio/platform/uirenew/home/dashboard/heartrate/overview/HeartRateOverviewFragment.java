package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ah;
import com.fossil.da;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.gc6;
import com.fossil.go5;
import com.fossil.hc6;
import com.fossil.lc6;
import com.fossil.qb;
import com.fossil.qw6;
import com.fossil.qz6;
import com.fossil.rc6;
import com.fossil.sc6;
import com.fossil.tj4;
import com.fossil.w15;
import com.fossil.xc6;
import com.fossil.yc6;
import com.fossil.zd7;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateOverviewFragment extends go5 {
    @DexIgnore
    public qw6<w15> f;
    @DexIgnore
    public hc6 g;
    @DexIgnore
    public yc6 h;
    @DexIgnore
    public sc6 i;
    @DexIgnore
    public gc6 j;
    @DexIgnore
    public xc6 p;
    @DexIgnore
    public rc6 q;
    @DexIgnore
    public int r; // = 7;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewFragment a;

        @DexIgnore
        public b(HeartRateOverviewFragment heartRateOverviewFragment) {
            this.a = heartRateOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HeartRateOverviewFragment heartRateOverviewFragment = this.a;
            qw6 a2 = heartRateOverviewFragment.f;
            heartRateOverviewFragment.a(7, a2 != null ? (w15) a2.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewFragment a;

        @DexIgnore
        public c(HeartRateOverviewFragment heartRateOverviewFragment) {
            this.a = heartRateOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HeartRateOverviewFragment heartRateOverviewFragment = this.a;
            qw6 a2 = heartRateOverviewFragment.f;
            heartRateOverviewFragment.a(4, a2 != null ? (w15) a2.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewFragment a;

        @DexIgnore
        public d(HeartRateOverviewFragment heartRateOverviewFragment) {
            this.a = heartRateOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HeartRateOverviewFragment heartRateOverviewFragment = this.a;
            qw6 a2 = heartRateOverviewFragment.f;
            heartRateOverviewFragment.a(2, a2 != null ? (w15) a2.a() : null);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "HeartRateOverviewFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        MFLogger.d("HeartRateOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void f1() {
        qw6<w15> qw6;
        w15 a2;
        qw6<w15> qw62;
        w15 a3;
        ConstraintLayout constraintLayout;
        qw6<w15> qw63;
        w15 a4;
        FlexibleTextView flexibleTextView;
        qw6<w15> qw64;
        w15 a5;
        String b2 = eh5.l.a().b("dianaHeartRateTab");
        String b3 = eh5.l.a().b("onDianaHeartRateTab");
        if (!(b2 == null || (qw64 = this.f) == null || (a5 = qw64.a()) == null)) {
            a5.x.setBackgroundColor(Color.parseColor(b2));
            a5.y.setBackgroundColor(Color.parseColor(b2));
        }
        if (!(b3 == null || (qw63 = this.f) == null || (a4 = qw63.a()) == null || (flexibleTextView = a4.t) == null)) {
            flexibleTextView.setTextColor(Color.parseColor(b3));
        }
        this.s = eh5.l.a().b("onHybridInactiveTab");
        String b4 = eh5.l.a().b("nonBrandSurface");
        this.t = eh5.l.a().b("primaryText");
        if (!(b4 == null || (qw62 = this.f) == null || (a3 = qw62.a()) == null || (constraintLayout = a3.q) == null)) {
            constraintLayout.setBackgroundColor(Color.parseColor(b4));
        }
        if (!TextUtils.isEmpty(this.s) && !TextUtils.isEmpty(this.t) && (qw6 = this.f) != null && (a2 = qw6.a()) != null) {
            FlexibleTextView flexibleTextView2 = a2.u;
            ee7.a((Object) flexibleTextView2, "it.ftvToday");
            if (flexibleTextView2.isSelected()) {
                a2.u.setTextColor(Color.parseColor(this.t));
            } else {
                a2.u.setTextColor(Color.parseColor(this.s));
            }
            FlexibleTextView flexibleTextView3 = a2.r;
            ee7.a((Object) flexibleTextView3, "it.ftv7Days");
            if (flexibleTextView3.isSelected()) {
                a2.r.setTextColor(Color.parseColor(this.t));
            } else {
                a2.r.setTextColor(Color.parseColor(this.s));
            }
            FlexibleTextView flexibleTextView4 = a2.s;
            ee7.a((Object) flexibleTextView4, "it.ftvMonth");
            if (flexibleTextView4.isSelected()) {
                a2.s.setTextColor(Color.parseColor(this.t));
            } else {
                a2.s.setTextColor(Color.parseColor(this.s));
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        w15 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewFragment", "onCreateView");
        w15 w15 = (w15) qb.a(layoutInflater, 2131558563, viewGroup, false, a1());
        da.d((View) w15.w, false);
        if (bundle != null) {
            this.r = bundle.getInt("CURRENT_TAB", 7);
        }
        ee7.a((Object) w15, "binding");
        a(w15);
        this.f = new qw6<>(this, w15);
        f1();
        qw6<w15> qw6 = this.f;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.r);
    }

    @DexIgnore
    public final void a(w15 w15) {
        MFLogger.d("HeartRateOverviewFragment", "initUI");
        this.j = (gc6) getChildFragmentManager().b("HeartRateOverviewDayFragment");
        this.p = (xc6) getChildFragmentManager().b("HeartRateOverviewWeekFragment");
        this.q = (rc6) getChildFragmentManager().b("HeartRateOverviewMonthFragment");
        if (this.j == null) {
            this.j = new gc6();
        }
        if (this.p == null) {
            this.p = new xc6();
        }
        if (this.q == null) {
            this.q = new rc6();
        }
        ArrayList arrayList = new ArrayList();
        gc6 gc6 = this.j;
        if (gc6 != null) {
            arrayList.add(gc6);
            xc6 xc6 = this.p;
            if (xc6 != null) {
                arrayList.add(xc6);
                rc6 rc6 = this.q;
                if (rc6 != null) {
                    arrayList.add(rc6);
                    RecyclerView recyclerView = w15.w;
                    ee7.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new qz6(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new HeartRateOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new ah().a(recyclerView);
                    a(this.r, w15);
                    tj4 f2 = PortfolioApp.g0.c().f();
                    gc6 gc62 = this.j;
                    if (gc62 != null) {
                        xc6 xc62 = this.p;
                        if (xc62 != null) {
                            rc6 rc62 = this.q;
                            if (rc62 != null) {
                                f2.a(new lc6(gc62, xc62, rc62)).a(this);
                                w15.u.setOnClickListener(new b(this));
                                w15.r.setOnClickListener(new c(this));
                                w15.s.setOnClickListener(new d(this));
                                return;
                            }
                            ee7.a();
                            throw null;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i2, w15 w15) {
        qw6<w15> qw6;
        w15 a2;
        w15 a3;
        RecyclerView recyclerView;
        w15 a4;
        RecyclerView recyclerView2;
        w15 a5;
        RecyclerView recyclerView3;
        w15 a6;
        RecyclerView recyclerView4;
        if (w15 != null) {
            FlexibleTextView flexibleTextView = w15.u;
            ee7.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = w15.r;
            ee7.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = w15.s;
            ee7.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = w15.u;
            ee7.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = w15.r;
            ee7.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = w15.s;
            ee7.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i2 == 2) {
                FlexibleTextView flexibleTextView7 = w15.s;
                ee7.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = w15.s;
                ee7.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = w15.r;
                ee7.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                qw6<w15> qw62 = this.f;
                if (!(qw62 == null || (a3 = qw62.a()) == null || (recyclerView = a3.w) == null)) {
                    recyclerView.scrollToPosition(2);
                }
            } else if (i2 == 4) {
                FlexibleTextView flexibleTextView10 = w15.r;
                ee7.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = w15.r;
                ee7.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = w15.r;
                ee7.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                qw6<w15> qw63 = this.f;
                if (!(qw63 == null || (a4 = qw63.a()) == null || (recyclerView2 = a4.w) == null)) {
                    recyclerView2.scrollToPosition(1);
                }
            } else if (i2 != 7) {
                FlexibleTextView flexibleTextView13 = w15.u;
                ee7.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = w15.u;
                ee7.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = w15.r;
                ee7.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                qw6<w15> qw64 = this.f;
                if (!(qw64 == null || (a6 = qw64.a()) == null || (recyclerView4 = a6.w) == null)) {
                    recyclerView4.scrollToPosition(0);
                }
            } else {
                FlexibleTextView flexibleTextView16 = w15.u;
                ee7.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = w15.u;
                ee7.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = w15.r;
                ee7.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                qw6<w15> qw65 = this.f;
                if (!(qw65 == null || (a5 = qw65.a()) == null || (recyclerView3 = a5.w) == null)) {
                    recyclerView3.scrollToPosition(0);
                }
            }
            if (!TextUtils.isEmpty(this.s) && !TextUtils.isEmpty(this.t) && (qw6 = this.f) != null && (a2 = qw6.a()) != null) {
                FlexibleTextView flexibleTextView19 = a2.u;
                ee7.a((Object) flexibleTextView19, "it.ftvToday");
                if (flexibleTextView19.isSelected()) {
                    a2.u.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.u.setTextColor(Color.parseColor(this.s));
                }
                FlexibleTextView flexibleTextView20 = a2.r;
                ee7.a((Object) flexibleTextView20, "it.ftv7Days");
                if (flexibleTextView20.isSelected()) {
                    a2.r.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.r.setTextColor(Color.parseColor(this.s));
                }
                FlexibleTextView flexibleTextView21 = a2.s;
                ee7.a((Object) flexibleTextView21, "it.ftvMonth");
                if (flexibleTextView21.isSelected()) {
                    a2.s.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.s.setTextColor(Color.parseColor(this.s));
                }
            }
        }
    }
}
