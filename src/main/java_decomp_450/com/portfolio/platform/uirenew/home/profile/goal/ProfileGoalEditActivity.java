package com.portfolio.platform.uirenew.home.profile.goal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.ri6;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileGoalEditActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public ri6 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            ee7.b(context, "context");
            Intent intent = new Intent(context, ProfileGoalEditActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        ri6 ri6 = this.y;
        if (ri6 != null) {
            ri6.onActivityResult(i, i2, intent);
        }
        super.onActivityResult(i, i2, intent);
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.cl5
    public void onBackPressed() {
        ri6 ri6 = this.y;
        if (ri6 != null) {
            ri6.e1();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        ri6 ri6 = (ri6) getSupportFragmentManager().b(2131362149);
        this.y = ri6;
        if (ri6 == null) {
            ri6 b = ri6.u.b();
            this.y = b;
            if (b != null) {
                a(b, ri6.u.a(), 2131362149);
            } else {
                ee7.a();
                throw null;
            }
        }
    }
}
