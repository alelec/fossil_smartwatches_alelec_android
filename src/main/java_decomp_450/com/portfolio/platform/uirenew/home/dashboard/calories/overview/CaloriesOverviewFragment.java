package com.portfolio.platform.uirenew.home.dashboard.calories.overview;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ah;
import com.fossil.be5;
import com.fossil.da;
import com.fossil.da6;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.go5;
import com.fossil.ja6;
import com.fossil.ka6;
import com.fossil.pa6;
import com.fossil.qa6;
import com.fossil.qb;
import com.fossil.qw6;
import com.fossil.qz6;
import com.fossil.sx4;
import com.fossil.tj4;
import com.fossil.y96;
import com.fossil.z96;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CaloriesOverviewFragment extends go5 {
    @DexIgnore
    public qw6<sx4> f;
    @DexIgnore
    public z96 g;
    @DexIgnore
    public qa6 h;
    @DexIgnore
    public ka6 i;
    @DexIgnore
    public y96 j;
    @DexIgnore
    public pa6 p;
    @DexIgnore
    public ja6 q;
    @DexIgnore
    public int r; // = 7;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewFragment a;

        @DexIgnore
        public b(CaloriesOverviewFragment caloriesOverviewFragment) {
            this.a = caloriesOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CaloriesOverviewFragment caloriesOverviewFragment = this.a;
            qw6 a2 = caloriesOverviewFragment.f;
            caloriesOverviewFragment.a(7, a2 != null ? (sx4) a2.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewFragment a;

        @DexIgnore
        public c(CaloriesOverviewFragment caloriesOverviewFragment) {
            this.a = caloriesOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CaloriesOverviewFragment caloriesOverviewFragment = this.a;
            qw6 a2 = caloriesOverviewFragment.f;
            caloriesOverviewFragment.a(4, a2 != null ? (sx4) a2.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewFragment a;

        @DexIgnore
        public d(CaloriesOverviewFragment caloriesOverviewFragment) {
            this.a = caloriesOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CaloriesOverviewFragment caloriesOverviewFragment = this.a;
            qw6 a2 = caloriesOverviewFragment.f;
            caloriesOverviewFragment.a(2, a2 != null ? (sx4) a2.a() : null);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "CaloriesOverviewFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void f1() {
        String str;
        String str2;
        String str3;
        qw6<sx4> qw6;
        sx4 a2;
        qw6<sx4> qw62;
        sx4 a3;
        ConstraintLayout constraintLayout;
        qw6<sx4> qw63;
        sx4 a4;
        FlexibleTextView flexibleTextView;
        qw6<sx4> qw64;
        sx4 a5;
        be5.a aVar = be5.o;
        z96 z96 = this.g;
        if (z96 != null) {
            if (aVar.a(z96.h())) {
                str = eh5.l.a().b("dianaActiveCaloriesTab");
            } else {
                str = eh5.l.a().b("hybridActiveCaloriesTab");
            }
            be5.a aVar2 = be5.o;
            z96 z962 = this.g;
            if (z962 != null) {
                if (aVar2.a(z962.h())) {
                    str2 = eh5.l.a().b("onDianaActiveCaloriesTab");
                } else {
                    str2 = eh5.l.a().b("onHybridActiveCaloriesTab");
                }
                if (!(str == null || (qw64 = this.f) == null || (a5 = qw64.a()) == null)) {
                    a5.x.setBackgroundColor(Color.parseColor(str));
                    a5.y.setBackgroundColor(Color.parseColor(str));
                }
                if (!(str2 == null || (qw63 = this.f) == null || (a4 = qw63.a()) == null || (flexibleTextView = a4.t) == null)) {
                    flexibleTextView.setTextColor(Color.parseColor(str2));
                }
                be5.a aVar3 = be5.o;
                z96 z963 = this.g;
                if (z963 != null) {
                    if (aVar3.a(z963.h())) {
                        str3 = eh5.l.a().b("onDianaInactiveTab");
                    } else {
                        str3 = eh5.l.a().b("onHybridInactiveTab");
                    }
                    this.s = str3;
                    this.t = eh5.l.a().b("primaryText");
                    String b2 = eh5.l.a().b("nonBrandSurface");
                    if (!(b2 == null || (qw62 = this.f) == null || (a3 = qw62.a()) == null || (constraintLayout = a3.q) == null)) {
                        constraintLayout.setBackgroundColor(Color.parseColor(b2));
                    }
                    if (!TextUtils.isEmpty(this.s) && !TextUtils.isEmpty(this.t) && (qw6 = this.f) != null && (a2 = qw6.a()) != null) {
                        FlexibleTextView flexibleTextView2 = a2.u;
                        ee7.a((Object) flexibleTextView2, "it.ftvToday");
                        if (flexibleTextView2.isSelected()) {
                            a2.u.setTextColor(Color.parseColor(this.t));
                        } else {
                            a2.u.setTextColor(Color.parseColor(this.s));
                        }
                        FlexibleTextView flexibleTextView3 = a2.r;
                        ee7.a((Object) flexibleTextView3, "it.ftv7Days");
                        if (flexibleTextView3.isSelected()) {
                            a2.r.setTextColor(Color.parseColor(this.t));
                        } else {
                            a2.r.setTextColor(Color.parseColor(this.s));
                        }
                        FlexibleTextView flexibleTextView4 = a2.s;
                        ee7.a((Object) flexibleTextView4, "it.ftvMonth");
                        if (flexibleTextView4.isSelected()) {
                            a2.s.setTextColor(Color.parseColor(this.t));
                        } else {
                            a2.s.setTextColor(Color.parseColor(this.s));
                        }
                    }
                } else {
                    ee7.d("mCaloriesOverviewDayPresenter");
                    throw null;
                }
            } else {
                ee7.d("mCaloriesOverviewDayPresenter");
                throw null;
            }
        } else {
            ee7.d("mCaloriesOverviewDayPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        sx4 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewFragment", "onCreateView");
        sx4 sx4 = (sx4) qb.a(layoutInflater, 2131558507, viewGroup, false, a1());
        da.d((View) sx4.w, false);
        if (bundle != null) {
            this.r = bundle.getInt("CURRENT_TAB", 7);
        }
        ee7.a((Object) sx4, "binding");
        a(sx4);
        this.f = new qw6<>(this, sx4);
        f1();
        qw6<sx4> qw6 = this.f;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.r);
    }

    @DexIgnore
    public final void a(sx4 sx4) {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewFragment", "initUI");
        this.j = (y96) getChildFragmentManager().b("CaloriesOverviewDayFragment");
        this.p = (pa6) getChildFragmentManager().b("CaloriesOverviewWeekFragment");
        this.q = (ja6) getChildFragmentManager().b("CaloriesOverviewMonthFragment");
        if (this.j == null) {
            this.j = new y96();
        }
        if (this.p == null) {
            this.p = new pa6();
        }
        if (this.q == null) {
            this.q = new ja6();
        }
        ArrayList arrayList = new ArrayList();
        y96 y96 = this.j;
        if (y96 != null) {
            arrayList.add(y96);
            pa6 pa6 = this.p;
            if (pa6 != null) {
                arrayList.add(pa6);
                ja6 ja6 = this.q;
                if (ja6 != null) {
                    arrayList.add(ja6);
                    RecyclerView recyclerView = sx4.w;
                    ee7.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new qz6(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new CaloriesOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new ah().a(recyclerView);
                    a(this.r, sx4);
                    tj4 f2 = PortfolioApp.g0.c().f();
                    y96 y962 = this.j;
                    if (y962 != null) {
                        pa6 pa62 = this.p;
                        if (pa62 != null) {
                            ja6 ja62 = this.q;
                            if (ja62 != null) {
                                f2.a(new da6(y962, pa62, ja62)).a(this);
                                sx4.u.setOnClickListener(new b(this));
                                sx4.r.setOnClickListener(new c(this));
                                sx4.s.setOnClickListener(new d(this));
                                return;
                            }
                            ee7.a();
                            throw null;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i2, sx4 sx4) {
        qw6<sx4> qw6;
        sx4 a2;
        sx4 a3;
        RecyclerView recyclerView;
        sx4 a4;
        RecyclerView recyclerView2;
        sx4 a5;
        RecyclerView recyclerView3;
        sx4 a6;
        RecyclerView recyclerView4;
        if (sx4 != null) {
            FlexibleTextView flexibleTextView = sx4.u;
            ee7.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = sx4.r;
            ee7.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = sx4.s;
            ee7.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = sx4.u;
            ee7.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = sx4.r;
            ee7.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = sx4.s;
            ee7.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i2 == 2) {
                FlexibleTextView flexibleTextView7 = sx4.s;
                ee7.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = sx4.s;
                ee7.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = sx4.r;
                ee7.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                qw6<sx4> qw62 = this.f;
                if (!(qw62 == null || (a3 = qw62.a()) == null || (recyclerView = a3.w) == null)) {
                    recyclerView.scrollToPosition(2);
                }
            } else if (i2 == 4) {
                FlexibleTextView flexibleTextView10 = sx4.r;
                ee7.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = sx4.r;
                ee7.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = sx4.r;
                ee7.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                qw6<sx4> qw63 = this.f;
                if (!(qw63 == null || (a4 = qw63.a()) == null || (recyclerView2 = a4.w) == null)) {
                    recyclerView2.scrollToPosition(1);
                }
            } else if (i2 != 7) {
                FlexibleTextView flexibleTextView13 = sx4.u;
                ee7.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = sx4.u;
                ee7.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = sx4.r;
                ee7.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                qw6<sx4> qw64 = this.f;
                if (!(qw64 == null || (a6 = qw64.a()) == null || (recyclerView4 = a6.w) == null)) {
                    recyclerView4.scrollToPosition(0);
                }
            } else {
                FlexibleTextView flexibleTextView16 = sx4.u;
                ee7.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = sx4.u;
                ee7.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = sx4.r;
                ee7.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                qw6<sx4> qw65 = this.f;
                if (!(qw65 == null || (a5 = qw65.a()) == null || (recyclerView3 = a5.w) == null)) {
                    recyclerView3.scrollToPosition(0);
                }
            }
            if (!TextUtils.isEmpty(this.s) && !TextUtils.isEmpty(this.t) && (qw6 = this.f) != null && (a2 = qw6.a()) != null) {
                FlexibleTextView flexibleTextView19 = a2.u;
                ee7.a((Object) flexibleTextView19, "it.ftvToday");
                if (flexibleTextView19.isSelected()) {
                    a2.u.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.u.setTextColor(Color.parseColor(this.s));
                }
                FlexibleTextView flexibleTextView20 = a2.r;
                ee7.a((Object) flexibleTextView20, "it.ftv7Days");
                if (flexibleTextView20.isSelected()) {
                    a2.r.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.r.setTextColor(Color.parseColor(this.s));
                }
                FlexibleTextView flexibleTextView21 = a2.s;
                ee7.a((Object) flexibleTextView21, "it.ftvMonth");
                if (flexibleTextView21.isSelected()) {
                    a2.s.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.s.setTextColor(Color.parseColor(this.s));
                }
            }
        }
    }
}
