package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ah;
import com.fossil.be5;
import com.fossil.da;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.f96;
import com.fossil.g96;
import com.fossil.go5;
import com.fossil.l96;
import com.fossil.m96;
import com.fossil.qb;
import com.fossil.qw6;
import com.fossil.qz6;
import com.fossil.t86;
import com.fossil.tj4;
import com.fossil.v86;
import com.fossil.yw4;
import com.fossil.z86;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityOverviewFragment extends go5 {
    @DexIgnore
    public qw6<yw4> f;
    @DexIgnore
    public v86 g;
    @DexIgnore
    public m96 h;
    @DexIgnore
    public g96 i;
    @DexIgnore
    public t86 j;
    @DexIgnore
    public l96 p;
    @DexIgnore
    public f96 q;
    @DexIgnore
    public int r; // = 7;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewFragment a;

        @DexIgnore
        public b(ActivityOverviewFragment activityOverviewFragment) {
            this.a = activityOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActivityOverviewFragment activityOverviewFragment = this.a;
            qw6 a2 = activityOverviewFragment.f;
            activityOverviewFragment.a(7, a2 != null ? (yw4) a2.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewFragment a;

        @DexIgnore
        public c(ActivityOverviewFragment activityOverviewFragment) {
            this.a = activityOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActivityOverviewFragment activityOverviewFragment = this.a;
            qw6 a2 = activityOverviewFragment.f;
            activityOverviewFragment.a(4, a2 != null ? (yw4) a2.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewFragment a;

        @DexIgnore
        public d(ActivityOverviewFragment activityOverviewFragment) {
            this.a = activityOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActivityOverviewFragment activityOverviewFragment = this.a;
            qw6 a2 = activityOverviewFragment.f;
            activityOverviewFragment.a(2, a2 != null ? (yw4) a2.a() : null);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "ActivityOverviewFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void f1() {
        String str;
        String str2;
        String str3;
        qw6<yw4> qw6;
        yw4 a2;
        qw6<yw4> qw62;
        yw4 a3;
        ConstraintLayout constraintLayout;
        qw6<yw4> qw63;
        yw4 a4;
        FlexibleTextView flexibleTextView;
        qw6<yw4> qw64;
        yw4 a5;
        be5.a aVar = be5.o;
        v86 v86 = this.g;
        if (v86 != null) {
            if (aVar.a(v86.h())) {
                str = eh5.l.a().b("dianaStepsTab");
            } else {
                str = eh5.l.a().b("hybridStepsTab");
            }
            be5.a aVar2 = be5.o;
            v86 v862 = this.g;
            if (v862 != null) {
                if (aVar2.a(v862.h())) {
                    str2 = eh5.l.a().b("onDianaStepsTab");
                } else {
                    str2 = eh5.l.a().b("onHybridStepsTab");
                }
                if (!(str == null || (qw64 = this.f) == null || (a5 = qw64.a()) == null)) {
                    a5.x.setBackgroundColor(Color.parseColor(str));
                    a5.y.setBackgroundColor(Color.parseColor(str));
                }
                if (!(str2 == null || (qw63 = this.f) == null || (a4 = qw63.a()) == null || (flexibleTextView = a4.t) == null)) {
                    flexibleTextView.setTextColor(Color.parseColor(str2));
                }
                be5.a aVar3 = be5.o;
                v86 v863 = this.g;
                if (v863 != null) {
                    if (aVar3.a(v863.h())) {
                        str3 = eh5.l.a().b("onDianaInactiveTab");
                    } else {
                        str3 = eh5.l.a().b("onHybridInactiveTab");
                    }
                    this.s = str3;
                    String b2 = eh5.l.a().b("nonBrandSurface");
                    this.t = eh5.l.a().b("primaryText");
                    if (!(b2 == null || (qw62 = this.f) == null || (a3 = qw62.a()) == null || (constraintLayout = a3.q) == null)) {
                        constraintLayout.setBackgroundColor(Color.parseColor(b2));
                    }
                    if (!TextUtils.isEmpty(this.s) && !TextUtils.isEmpty(this.t) && (qw6 = this.f) != null && (a2 = qw6.a()) != null) {
                        FlexibleTextView flexibleTextView2 = a2.u;
                        ee7.a((Object) flexibleTextView2, "it.ftvToday");
                        if (flexibleTextView2.isSelected()) {
                            a2.u.setTextColor(Color.parseColor(this.t));
                        } else {
                            a2.u.setTextColor(Color.parseColor(this.s));
                        }
                        FlexibleTextView flexibleTextView3 = a2.r;
                        ee7.a((Object) flexibleTextView3, "it.ftv7Days");
                        if (flexibleTextView3.isSelected()) {
                            a2.r.setTextColor(Color.parseColor(this.t));
                        } else {
                            a2.r.setTextColor(Color.parseColor(this.s));
                        }
                        FlexibleTextView flexibleTextView4 = a2.s;
                        ee7.a((Object) flexibleTextView4, "it.ftvMonth");
                        if (flexibleTextView4.isSelected()) {
                            a2.s.setTextColor(Color.parseColor(this.t));
                        } else {
                            a2.s.setTextColor(Color.parseColor(this.s));
                        }
                    }
                } else {
                    ee7.d("mActivityOverviewDayPresenter");
                    throw null;
                }
            } else {
                ee7.d("mActivityOverviewDayPresenter");
                throw null;
            }
        } else {
            ee7.d("mActivityOverviewDayPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        yw4 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewFragment", "onCreateView");
        yw4 yw4 = (yw4) qb.a(layoutInflater, 2131558497, viewGroup, false, a1());
        da.d((View) yw4.w, false);
        if (bundle != null) {
            this.r = bundle.getInt("CURRENT_TAB", 7);
        }
        ee7.a((Object) yw4, "binding");
        a(yw4);
        this.f = new qw6<>(this, yw4);
        f1();
        qw6<yw4> qw6 = this.f;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.r);
    }

    @DexIgnore
    public final void a(yw4 yw4) {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewFragment", "initUI");
        this.j = (t86) getChildFragmentManager().b("ActivityOverviewDayFragment");
        this.p = (l96) getChildFragmentManager().b("ActivityOverviewWeekFragment");
        this.q = (f96) getChildFragmentManager().b("ActivityOverviewMonthFragment");
        if (this.j == null) {
            this.j = new t86();
        }
        if (this.p == null) {
            this.p = new l96();
        }
        if (this.q == null) {
            this.q = new f96();
        }
        ArrayList arrayList = new ArrayList();
        t86 t86 = this.j;
        if (t86 != null) {
            arrayList.add(t86);
            l96 l96 = this.p;
            if (l96 != null) {
                arrayList.add(l96);
                f96 f96 = this.q;
                if (f96 != null) {
                    arrayList.add(f96);
                    RecyclerView recyclerView = yw4.w;
                    ee7.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new qz6(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new ActivityOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new ah().a(recyclerView);
                    a(this.r, yw4);
                    tj4 f2 = PortfolioApp.g0.c().f();
                    t86 t862 = this.j;
                    if (t862 != null) {
                        l96 l962 = this.p;
                        if (l962 != null) {
                            f96 f962 = this.q;
                            if (f962 != null) {
                                f2.a(new z86(t862, l962, f962)).a(this);
                                yw4.u.setOnClickListener(new b(this));
                                yw4.r.setOnClickListener(new c(this));
                                yw4.s.setOnClickListener(new d(this));
                                return;
                            }
                            ee7.a();
                            throw null;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i2, yw4 yw4) {
        qw6<yw4> qw6;
        yw4 a2;
        yw4 a3;
        RecyclerView recyclerView;
        yw4 a4;
        RecyclerView recyclerView2;
        yw4 a5;
        RecyclerView recyclerView3;
        yw4 a6;
        RecyclerView recyclerView4;
        if (yw4 != null) {
            FlexibleTextView flexibleTextView = yw4.u;
            ee7.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = yw4.r;
            ee7.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = yw4.s;
            ee7.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = yw4.u;
            ee7.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = yw4.r;
            ee7.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = yw4.s;
            ee7.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i2 == 2) {
                FlexibleTextView flexibleTextView7 = yw4.s;
                ee7.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = yw4.s;
                ee7.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = yw4.r;
                ee7.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                qw6<yw4> qw62 = this.f;
                if (!(qw62 == null || (a3 = qw62.a()) == null || (recyclerView = a3.w) == null)) {
                    recyclerView.scrollToPosition(2);
                }
            } else if (i2 == 4) {
                FlexibleTextView flexibleTextView10 = yw4.r;
                ee7.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = yw4.r;
                ee7.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = yw4.r;
                ee7.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                qw6<yw4> qw63 = this.f;
                if (!(qw63 == null || (a4 = qw63.a()) == null || (recyclerView2 = a4.w) == null)) {
                    recyclerView2.scrollToPosition(1);
                }
            } else if (i2 != 7) {
                FlexibleTextView flexibleTextView13 = yw4.u;
                ee7.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = yw4.u;
                ee7.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = yw4.r;
                ee7.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                qw6<yw4> qw64 = this.f;
                if (!(qw64 == null || (a6 = qw64.a()) == null || (recyclerView4 = a6.w) == null)) {
                    recyclerView4.scrollToPosition(0);
                }
            } else {
                FlexibleTextView flexibleTextView16 = yw4.u;
                ee7.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = yw4.u;
                ee7.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = yw4.r;
                ee7.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                qw6<yw4> qw65 = this.f;
                if (!(qw65 == null || (a5 = qw65.a()) == null || (recyclerView3 = a5.w) == null)) {
                    recyclerView3.scrollToPosition(0);
                }
            }
            if (!TextUtils.isEmpty(this.s) && !TextUtils.isEmpty(this.t) && (qw6 = this.f) != null && (a2 = qw6.a()) != null) {
                FlexibleTextView flexibleTextView19 = a2.u;
                ee7.a((Object) flexibleTextView19, "it.ftvToday");
                if (flexibleTextView19.isSelected()) {
                    a2.u.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.u.setTextColor(Color.parseColor(this.s));
                }
                FlexibleTextView flexibleTextView20 = a2.r;
                ee7.a((Object) flexibleTextView20, "it.ftv7Days");
                if (flexibleTextView20.isSelected()) {
                    a2.r.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.r.setTextColor(Color.parseColor(this.s));
                }
                FlexibleTextView flexibleTextView21 = a2.s;
                ee7.a((Object) flexibleTextView21, "it.ftvMonth");
                if (flexibleTextView21.isSelected()) {
                    a2.s.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.s.setTextColor(Color.parseColor(this.s));
                }
            }
        }
    }
}
