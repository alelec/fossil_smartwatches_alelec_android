package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.cl5;
import com.fossil.cn6;
import com.fossil.ee7;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserCustomizeThemeActivity extends cl5 {
    @DexIgnore
    public static /* final */ a y; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str, boolean z) {
            ee7.b(context, "context");
            ee7.b(str, "id");
            Intent intent = new Intent(context, UserCustomizeThemeActivity.class);
            intent.setFlags(536870912);
            Bundle bundle = new Bundle();
            bundle.putString("THEME_ID", str);
            bundle.putBoolean("THEME_MODE_EDIT", z);
            intent.addFlags(32768);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        cn6 cn6 = (cn6) getSupportFragmentManager().b(2131362149);
        if (cn6 == null) {
            cn6 = cn6.q.c();
            a(cn6, 2131362149);
        }
        Intent intent = getIntent();
        ee7.a((Object) intent, "intent");
        Bundle extras = intent.getExtras();
        if (extras != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("THEME_ID", extras.getString("THEME_ID"));
            bundle2.putBoolean("THEME_MODE_EDIT", extras.getBoolean("THEME_MODE_EDIT"));
            cn6.setArguments(bundle2);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onResume() {
        super.onResume();
        a(false);
    }
}
