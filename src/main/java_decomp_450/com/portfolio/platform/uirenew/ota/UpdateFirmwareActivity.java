package com.portfolio.platform.uirenew.ota;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.bq6;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.wq6;
import com.fossil.zd7;
import com.fossil.zp6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UpdateFirmwareActivity extends cl5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);
    @DexIgnore
    public static /* final */ String z;
    @DexIgnore
    public bq6 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return UpdateFirmwareActivity.z;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str, boolean z) {
            ee7.b(context, "context");
            ee7.b(str, "serial");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = a();
            local.d(a, "start isOnboarding=" + z + " serial " + str);
            Intent intent = new Intent(context, UpdateFirmwareActivity.class);
            intent.putExtra("IS_ONBOARDING_FLOW", z);
            intent.putExtra("SERIAL", str);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = UpdateFirmwareActivity.class.getSimpleName();
        if (simpleName != null) {
            ee7.a((Object) simpleName, "UpdateFirmwareActivity::class.java.simpleName!!");
            z = simpleName;
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.cl5
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        wq6 wq6 = (wq6) getSupportFragmentManager().b(2131362149);
        Intent intent = getIntent();
        String str = "";
        boolean z2 = false;
        if (intent != null) {
            z2 = intent.getBooleanExtra("IS_ONBOARDING_FLOW", false);
            String stringExtra = intent.getStringExtra("SERIAL");
            if (stringExtra != null) {
                str = stringExtra;
            }
        }
        if (wq6 == null) {
            wq6 = wq6.u.a(z2, str);
            a(wq6, 2131362149);
        }
        PortfolioApp.g0.c().f().a(new zp6(wq6)).a(this);
    }
}
