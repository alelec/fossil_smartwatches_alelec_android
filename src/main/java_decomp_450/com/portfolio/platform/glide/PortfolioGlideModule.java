package com.portfolio.platform.glide;

import android.content.Context;
import com.fossil.aw;
import com.fossil.dd5;
import com.fossil.ed5;
import com.fossil.ee7;
import com.fossil.fd5;
import com.fossil.gd5;
import com.fossil.gw;
import com.fossil.ld5;
import com.fossil.md5;
import com.fossil.y30;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioGlideModule extends y30 {
    @DexIgnore
    @Override // com.fossil.b40, com.fossil.d40
    public void a(Context context, aw awVar, gw gwVar) {
        ee7.b(context, "context");
        ee7.b(awVar, "glide");
        ee7.b(gwVar, "registry");
        super.a(context, awVar, gwVar);
        gwVar.a(ed5.class, InputStream.class, new dd5.b());
        gwVar.a(gd5.class, InputStream.class, new fd5.b());
        gwVar.a(md5.class, InputStream.class, new ld5.b());
    }

    @DexIgnore
    @Override // com.fossil.y30
    public boolean a() {
        return false;
    }
}
