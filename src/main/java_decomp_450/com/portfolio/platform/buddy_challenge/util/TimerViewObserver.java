package com.portfolio.platform.buddy_challenge.util;

import android.util.SparseArray;
import androidx.lifecycle.Lifecycle;
import com.fossil.ae;
import com.fossil.ee7;
import com.fossil.rd;
import com.fossil.zt4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TimerViewObserver implements rd {
    @DexIgnore
    public SparseArray<zt4> a; // = new SparseArray<>();

    @DexIgnore
    public final void a(zt4 zt4, int i) {
        ee7.b(zt4, "aware");
        this.a.put(i, zt4);
    }

    @DexIgnore
    @ae(Lifecycle.a.ON_PAUSE)
    public final void onPause() {
        if (this.a.size() != 0) {
            SparseArray<zt4> sparseArray = this.a;
            int size = sparseArray.size();
            for (int i = 0; i < size; i++) {
                sparseArray.keyAt(i);
                sparseArray.valueAt(i).onPause();
            }
        }
    }

    @DexIgnore
    @ae(Lifecycle.a.ON_RESUME)
    public final void onResume() {
        if (this.a.size() != 0) {
            SparseArray<zt4> sparseArray = this.a;
            int size = sparseArray.size();
            for (int i = 0; i < size; i++) {
                sparseArray.keyAt(i);
                sparseArray.valueAt(i).onResume();
            }
        }
    }

    @DexIgnore
    public final void a(int i) {
        this.a.remove(i);
    }

    @DexIgnore
    public final void a() {
        this.a.clear();
    }
}
