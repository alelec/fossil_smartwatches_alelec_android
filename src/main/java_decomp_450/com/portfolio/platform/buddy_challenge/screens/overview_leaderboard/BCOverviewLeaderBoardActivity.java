package com.portfolio.platform.buddy_challenge.screens.overview_leaderboard;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.cl5;
import com.fossil.dr4;
import com.fossil.ee7;
import com.fossil.mn4;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCOverviewLeaderBoardActivity extends cl5 {
    @DexIgnore
    public static /* final */ a y; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, mn4 mn4, String str) {
            ee7.b(fragment, "fragment");
            ee7.b(mn4, "challenge");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("BCOverviewLeaderBoardActivity", "startActivity - challenge: " + mn4 + " - historyId: " + str);
            Intent intent = new Intent(fragment.getContext(), BCOverviewLeaderBoardActivity.class);
            intent.putExtra("challenge_extra", mn4);
            intent.putExtra("challenge_history_id_extra", str);
            fragment.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        Fragment b = getSupportFragmentManager().b(2131362149);
        if (!(b instanceof dr4)) {
            b = null;
        }
        if (((dr4) b) == null) {
            mn4 mn4 = (mn4) getIntent().getParcelableExtra("challenge_extra");
            String stringExtra = getIntent().getStringExtra("challenge_history_id_extra");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = e();
            local.e(e, "onCreate - challenge: " + mn4 + " - historyId: " + stringExtra);
            a(dr4.s.a(mn4, stringExtra), dr4.s.a());
        }
    }
}
