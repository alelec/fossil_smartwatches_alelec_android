package com.portfolio.platform.buddy_challenge.screens.leaderboard;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.eq4;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCLeaderBoardActivity extends cl5 {
    @DexIgnore
    public static /* final */ a y; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, long j, int i, String str2) {
            ee7.b(fragment, "fragment");
            ee7.b(str, "challengeId");
            Intent intent = new Intent(fragment.getContext(), BCLeaderBoardActivity.class);
            intent.putExtra("challenge_id_extra", str);
            intent.putExtra("challenge_start_time_extra", j);
            intent.putExtra("challenge_target_extra", i);
            intent.putExtra("challenge_status_extra", str2);
            fragment.startActivityForResult(intent, 15);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((eq4) getSupportFragmentManager().b(2131362149)) == null) {
            a(eq4.u.a(getIntent().getStringExtra("challenge_id_extra"), getIntent().getLongExtra("challenge_start_time_extra", 0), getIntent().getIntExtra("challenge_target_extra", -1), getIntent().getStringExtra("challenge_status_extra")), eq4.u.a(), 2131362149);
        }
    }
}
