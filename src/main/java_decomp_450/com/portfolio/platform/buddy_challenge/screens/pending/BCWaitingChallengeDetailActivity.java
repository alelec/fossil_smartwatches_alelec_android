package com.portfolio.platform.buddy_challenge.screens.pending;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.jr4;
import com.fossil.mn4;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCWaitingChallengeDetailActivity extends cl5 {
    @DexIgnore
    public static /* final */ String y;
    @DexIgnore
    public static /* final */ a z; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, mn4 mn4, String str, int i, boolean z) {
            ee7.b(fragment, "fragment");
            ee7.b(str, "category");
            Intent intent = new Intent(fragment.getContext(), BCWaitingChallengeDetailActivity.class);
            intent.putExtra("challenge_extra", mn4);
            intent.putExtra("category_extra", str);
            intent.putExtra("index_extra", i);
            intent.putExtra("about_extra", z);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String q = BCWaitingChallengeDetailActivity.y;
            local.e(q, "startActivityForResult - challenge: " + mn4 + " - category: " + str + " - index: " + i);
            fragment.startActivityForResult(intent, 13);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = BCWaitingChallengeDetailActivity.class.getSimpleName();
        ee7.a((Object) simpleName, "BCWaitingChallengeDetail\u2026ty::class.java.simpleName");
        y = simpleName;
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        Fragment b = getSupportFragmentManager().b(2131362149);
        if (!(b instanceof jr4)) {
            b = null;
        }
        jr4 jr4 = (jr4) b;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String e = e();
        local.e(e, "onCreate - fragment: " + jr4);
        if (jr4 == null) {
            mn4 mn4 = (mn4) getIntent().getParcelableExtra("challenge_extra");
            String stringExtra = getIntent().getStringExtra("category_extra");
            int intExtra = getIntent().getIntExtra("index_extra", -1);
            String stringExtra2 = getIntent().getStringExtra("challenge_id_extra");
            boolean booleanExtra = getIntent().getBooleanExtra("about_extra", false);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str = y;
            local2.e(str, "onCreate - challenge: " + mn4 + " - category: " + stringExtra + " - index: " + intExtra + " - visitId: " + stringExtra2 + " - about: " + booleanExtra);
            a(jr4.C.a(mn4, stringExtra, intExtra, stringExtra2, booleanExtra), jr4.C.a(), 2131362149);
        }
    }
}
