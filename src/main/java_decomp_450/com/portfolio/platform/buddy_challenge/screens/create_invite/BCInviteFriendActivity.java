package com.portfolio.platform.buddy_challenge.screens.create_invite;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.qn4;
import com.fossil.tp4;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCInviteFriendActivity extends cl5 {
    @DexIgnore
    public static /* final */ a y; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, qn4 qn4) {
            ee7.b(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), BCInviteFriendActivity.class);
            intent.putExtra("challenge_draft_extra", qn4);
            fragment.startActivity(intent);
        }

        @DexIgnore
        public final void b(Fragment fragment, qn4 qn4) {
            ee7.b(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), BCInviteFriendActivity.class);
            intent.putExtra("challenge_draft_extra", qn4);
            fragment.startActivityForResult(intent, 11);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((tp4) getSupportFragmentManager().b(2131362149)) == null) {
            a(tp4.t.a((qn4) getIntent().getParcelableExtra("challenge_draft_extra")), tp4.t.a(), 2131362149);
        }
    }
}
