package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.c7;
import com.fossil.eh5;
import com.fossil.ny6;
import com.fossil.pl4;
import java.io.Serializable;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NumberPickerLarge extends LinearLayout {
    @DexIgnore
    public static /* final */ l p0; // = new l();
    @DexIgnore
    public /* final */ SparseArray<String> A;
    @DexIgnore
    public /* final */ int[] B;
    @DexIgnore
    public /* final */ Paint C;
    @DexIgnore
    public /* final */ Paint D;
    @DexIgnore
    public /* final */ Drawable E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public int H;
    @DexIgnore
    public /* final */ ny6 I;
    @DexIgnore
    public /* final */ ny6 J;
    @DexIgnore
    public int K;
    @DexIgnore
    public j L;
    @DexIgnore
    public e M;
    @DexIgnore
    public d N;
    @DexIgnore
    public float O;
    @DexIgnore
    public float P;
    @DexIgnore
    public VelocityTracker Q;
    @DexIgnore
    public int R;
    @DexIgnore
    public int S;
    @DexIgnore
    public int T;
    @DexIgnore
    public boolean U;
    @DexIgnore
    public /* final */ int V;
    @DexIgnore
    public int W;
    @DexIgnore
    public String a;
    @DexIgnore
    public /* final */ boolean a0;
    @DexIgnore
    public String b;
    @DexIgnore
    public /* final */ Drawable b0;
    @DexIgnore
    public String c;
    @DexIgnore
    public /* final */ int c0;
    @DexIgnore
    public /* final */ ImageButton d;
    @DexIgnore
    public int d0;
    @DexIgnore
    public /* final */ ImageButton e;
    @DexIgnore
    public boolean e0;
    @DexIgnore
    public /* final */ AppCompatEditText f;
    @DexIgnore
    public boolean f0;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public int g0;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public int h0;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public int i0;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public boolean j0;
    @DexIgnore
    public boolean k0;
    @DexIgnore
    public k l0;
    @DexIgnore
    public /* final */ i m0;
    @DexIgnore
    public int n0;
    @DexIgnore
    public Typeface o0;
    @DexIgnore
    public int p;
    @DexIgnore
    public /* final */ boolean q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public String[] t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public h x;
    @DexIgnore
    public g y;
    @DexIgnore
    public f z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            NumberPickerLarge.this.b();
            NumberPickerLarge.this.f.clearFocus();
            if (view.getId() == 2131362849) {
                NumberPickerLarge.this.a(true);
            } else {
                NumberPickerLarge.this.a(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnLongClickListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            NumberPickerLarge.this.b();
            NumberPickerLarge.this.f.clearFocus();
            if (view.getId() == 2131362849) {
                NumberPickerLarge.this.a(true, 0L);
            } else {
                NumberPickerLarge.this.a(false, 0L);
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            NumberPickerLarge.this.j();
            NumberPickerLarge.this.e0 = true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public boolean a;

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public void run() {
            NumberPickerLarge.this.a(this.a);
            NumberPickerLarge.this.postDelayed(this, 300);
        }
    }

    @DexIgnore
    public interface f extends Serializable {
        @DexIgnore
        String format(int i);
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(NumberPickerLarge numberPickerLarge, int i);
    }

    @DexIgnore
    public interface h {
        @DexIgnore
        void a(NumberPickerLarge numberPickerLarge, int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements Runnable {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k {
        @DexIgnore
        public c a;

        @DexIgnore
        public k() {
            this.a = new c();
        }

        @DexIgnore
        public boolean a(int i, int i2, Bundle bundle) {
            c cVar = this.a;
            return cVar != null && cVar.performAction(i, i2, bundle);
        }

        @DexIgnore
        public void a(int i, int i2) {
            c cVar = this.a;
            if (cVar != null) {
                cVar.a(i, i2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class l implements f {
        @DexIgnore
        public /* final */ Object[] mArgs; // = new Object[1];
        @DexIgnore
        public /* final */ StringBuilder mBuilder; // = new StringBuilder();
        @DexIgnore
        public transient Formatter mFmt;
        @DexIgnore
        public char mZeroDigit;

        @DexIgnore
        public l() {
            b(Locale.getDefault());
        }

        @DexIgnore
        public static char c(Locale locale) {
            return new DecimalFormatSymbols(locale).getZeroDigit();
        }

        @DexIgnore
        public final Formatter a(Locale locale) {
            return new Formatter(this.mBuilder, locale);
        }

        @DexIgnore
        public final void b(Locale locale) {
            this.mFmt = a(locale);
            this.mZeroDigit = c(locale);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPickerLarge.f
        public String format(int i) {
            Locale locale = Locale.getDefault();
            if (this.mZeroDigit != c(locale)) {
                b(locale);
            }
            this.mArgs[0] = Integer.valueOf(i);
            StringBuilder sb = this.mBuilder;
            sb.delete(0, sb.length());
            this.mFmt.format("%02d", this.mArgs);
            return this.mFmt.toString();
        }
    }

    @DexIgnore
    public NumberPickerLarge(Context context) {
        this(context, null);
    }

    @DexIgnore
    private k getSupportAccessibilityNodeProvider() {
        return new k();
    }

    @DexIgnore
    public static f getTwoDigitFormatter() {
        return p0;
    }

    @DexIgnore
    public static int resolveSizeAndState(int i2, int i3, int i4) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode != Integer.MIN_VALUE) {
            if (mode == 1073741824) {
                i2 = size;
            }
        } else if (size < i2) {
            i2 = 16777216 | size;
        }
        return i2 | (-16777216 & i4);
    }

    @DexIgnore
    private void setWrapSelectorWheel(boolean z2) {
        boolean z3 = this.v - this.u >= this.B.length;
        if ((!z2 || z3) && z2 != this.U) {
            this.U = z2;
        }
    }

    @DexIgnore
    public final boolean a(ny6 ny6) {
        ny6.a(true);
        int d2 = ny6.d() - ny6.c();
        int i2 = this.G - ((this.H + d2) % this.F);
        if (i2 == 0) {
            return false;
        }
        int abs = Math.abs(i2);
        int i3 = this.F;
        if (abs > i3 / 2) {
            i2 = i2 > 0 ? i2 - i3 : i2 + i3;
        }
        scrollBy(0, d2 + i2);
        return true;
    }

    @DexIgnore
    public void b() {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null && inputMethodManager.isActive(this.f)) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            if (this.a0) {
                this.f.setVisibility(4);
            }
        }
    }

    @DexIgnore
    public final void c() {
        setVerticalFadingEdgeEnabled(true);
        setFadingEdgeLength(((getBottom() - getTop()) - this.r) / 2);
    }

    @DexIgnore
    public void computeScroll() {
        ny6 ny6 = this.I;
        if (ny6.f()) {
            ny6 = this.J;
            if (ny6.f()) {
                return;
            }
        }
        ny6.a();
        int c2 = ny6.c();
        if (this.K == 0) {
            this.K = ny6.e();
        }
        scrollBy(0, c2 - this.K);
        this.K = c2;
        if (ny6.f()) {
            b(ny6);
        } else {
            invalidate();
        }
    }

    @DexIgnore
    public final void d() {
        e();
        int[] iArr = this.B;
        int bottom = (int) ((((float) ((getBottom() - getTop()) - (iArr.length * this.r))) / ((float) iArr.length)) + 0.5f);
        this.s = bottom;
        this.F = this.r + bottom;
        int baseline = (this.f.getBaseline() + this.f.getTop()) - (this.F * 2);
        this.G = baseline;
        this.H = baseline;
        l();
    }

    @DexIgnore
    public boolean dispatchHoverEvent(MotionEvent motionEvent) {
        int i2;
        if (!this.a0) {
            return super.dispatchHoverEvent(motionEvent);
        }
        if (!((AccessibilityManager) getContext().getSystemService("accessibility")).isEnabled()) {
            return false;
        }
        int y2 = (int) motionEvent.getY();
        if (y2 < this.g0) {
            i2 = 3;
        } else {
            i2 = y2 > this.h0 ? 1 : 2;
        }
        int action = motionEvent.getAction() & 255;
        k supportAccessibilityNodeProvider = getSupportAccessibilityNodeProvider();
        if (action == 7) {
            int i3 = this.i0;
            if (i3 == i2 || i3 == -1) {
                return false;
            }
            supportAccessibilityNodeProvider.a(i3, 256);
            supportAccessibilityNodeProvider.a(i2, 128);
            this.i0 = i2;
            supportAccessibilityNodeProvider.a(i2, 64, null);
            return false;
        } else if (action == 9) {
            supportAccessibilityNodeProvider.a(i2, 128);
            this.i0 = i2;
            supportAccessibilityNodeProvider.a(i2, 64, null);
            return false;
        } else if (action != 10) {
            return false;
        } else {
            supportAccessibilityNodeProvider.a(i2, 256);
            this.i0 = -1;
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
        requestFocus();
        r5.n0 = r0;
        g();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
        if (r5.I.f() == false) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0054, code lost:
        if (r0 != 20) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0056, code lost:
        r6 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0058, code lost:
        r6 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0059, code lost:
        a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005c, code lost:
        return true;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean dispatchKeyEvent(android.view.KeyEvent r6) {
        /*
            r5 = this;
            int r0 = r6.getKeyCode()
            r1 = 19
            r2 = 20
            if (r0 == r1) goto L_0x0019
            if (r0 == r2) goto L_0x0019
            r1 = 23
            if (r0 == r1) goto L_0x0015
            r1 = 66
            if (r0 == r1) goto L_0x0015
            goto L_0x005d
        L_0x0015:
            r5.g()
            goto L_0x005d
        L_0x0019:
            boolean r1 = r5.a0
            if (r1 != 0) goto L_0x001e
            goto L_0x005d
        L_0x001e:
            int r1 = r6.getAction()
            r3 = 1
            if (r1 == 0) goto L_0x0030
            if (r1 == r3) goto L_0x0028
            goto L_0x005d
        L_0x0028:
            int r1 = r5.n0
            if (r1 != r0) goto L_0x005d
            r6 = -1
            r5.n0 = r6
            return r3
        L_0x0030:
            boolean r1 = r5.U
            if (r1 != 0) goto L_0x003e
            if (r0 != r2) goto L_0x0037
            goto L_0x003e
        L_0x0037:
            int r1 = r5.w
            int r4 = r5.u
            if (r1 <= r4) goto L_0x005d
            goto L_0x0044
        L_0x003e:
            int r1 = r5.w
            int r4 = r5.v
            if (r1 >= r4) goto L_0x005d
        L_0x0044:
            r5.requestFocus()
            r5.n0 = r0
            r5.g()
            com.fossil.ny6 r6 = r5.I
            boolean r6 = r6.f()
            if (r6 == 0) goto L_0x005c
            if (r0 != r2) goto L_0x0058
            r6 = 1
            goto L_0x0059
        L_0x0058:
            r6 = 0
        L_0x0059:
            r5.a(r6)
        L_0x005c:
            return r3
        L_0x005d:
            boolean r6 = super.dispatchKeyEvent(r6)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.NumberPickerLarge.dispatchKeyEvent(android.view.KeyEvent):boolean");
    }

    @DexIgnore
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 3) {
            g();
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    @DexIgnore
    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 3) {
            g();
        }
        return super.dispatchTrackballEvent(motionEvent);
    }

    @DexIgnore
    public final void e() {
        this.A.clear();
        int[] iArr = this.B;
        int i2 = this.w;
        for (int i3 = 0; i3 < this.B.length; i3++) {
            int i4 = (i3 - 2) + i2;
            if (this.U) {
                i4 = d(i4);
            }
            iArr[i3] = i4;
            a(iArr[i3]);
        }
    }

    @DexIgnore
    public final void f(int i2) {
        if (this.d0 != i2) {
            this.d0 = i2;
            g gVar = this.y;
            if (gVar != null) {
                gVar.a(this, i2);
            }
        }
    }

    @DexIgnore
    public final void g() {
        e eVar = this.M;
        if (eVar != null) {
            removeCallbacks(eVar);
        }
        j jVar = this.L;
        if (jVar != null) {
            removeCallbacks(jVar);
        }
        d dVar = this.N;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
        this.m0.a();
    }

    @DexIgnore
    public AccessibilityNodeProvider getAccessibilityNodeProvider() {
        if (!this.a0) {
            return super.getAccessibilityNodeProvider();
        }
        if (this.l0 == null) {
            this.l0 = new k();
        }
        return this.l0.a;
    }

    @DexIgnore
    public float getBottomFadingEdgeStrength() {
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public int getMaxValue() {
        return this.v;
    }

    @DexIgnore
    public int getMinValue() {
        return this.u;
    }

    @DexIgnore
    public int getSolidColor() {
        return this.V;
    }

    @DexIgnore
    public float getTopFadingEdgeStrength() {
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public int getValue() {
        return this.w;
    }

    @DexIgnore
    public boolean getWrapSelectorWheel() {
        return this.U;
    }

    @DexIgnore
    public final void h() {
        d dVar = this.N;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
    }

    @DexIgnore
    public final void i() {
        e eVar = this.M;
        if (eVar != null) {
            removeCallbacks(eVar);
        }
    }

    @DexIgnore
    public void j() {
    }

    @DexIgnore
    public final void k() {
        int i2;
        if (this.q) {
            String[] strArr = this.t;
            int i3 = 0;
            if (strArr == null) {
                float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                for (int i4 = 0; i4 <= 9; i4++) {
                    float measureText = this.C.measureText(g(i4));
                    if (measureText > f2) {
                        f2 = measureText;
                    }
                }
                for (int i5 = this.v; i5 > 0; i5 /= 10) {
                    i3++;
                }
                i2 = (int) (((float) i3) * f2);
            } else {
                int length = strArr.length;
                int i6 = 0;
                while (i3 < length) {
                    float measureText2 = this.C.measureText(strArr[i3]);
                    if (measureText2 > ((float) i6)) {
                        i6 = (int) measureText2;
                    }
                    i3++;
                }
                i2 = i6;
            }
            int paddingLeft = i2 + this.f.getPaddingLeft() + this.f.getPaddingRight();
            if (this.p != paddingLeft) {
                int i7 = this.j;
                if (paddingLeft > i7) {
                    this.p = paddingLeft;
                } else {
                    this.p = i7;
                }
                invalidate();
            }
        }
    }

    @DexIgnore
    public final boolean l() {
        String[] strArr = this.t;
        String c2 = strArr == null ? c(this.w) : strArr[this.w - this.u];
        if (TextUtils.isEmpty(c2) || c2.equals(this.f.getText().toString())) {
            return false;
        }
        this.f.setText(c2);
        return true;
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        g();
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (!this.a0) {
            super.onDraw(canvas);
            return;
        }
        float right = (float) ((getRight() - getLeft()) / 2);
        float f2 = (float) this.H;
        Drawable drawable = this.E;
        if (drawable != null && this.d0 == 0) {
            if (this.k0) {
                drawable.setState(LinearLayout.PRESSED_ENABLED_STATE_SET);
                this.E.setBounds(0, 0, getRight(), this.g0);
                this.E.draw(canvas);
            }
            if (this.j0) {
                this.E.setState(LinearLayout.PRESSED_ENABLED_STATE_SET);
                this.E.setBounds(0, this.h0, getRight(), getBottom());
                this.E.draw(canvas);
            }
        }
        int[] iArr = this.B;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            String str = this.A.get(iArr[i2]);
            if (i2 != 2) {
                canvas.drawText(str, right, f2, this.C);
            } else if (this.f.getVisibility() != 0) {
                canvas.drawText(str, right, f2, this.D);
            }
            f2 += (float) this.F;
        }
        Drawable drawable2 = this.b0;
        if (drawable2 != null) {
            int i3 = this.g0;
            drawable2.setBounds(0, i3, getRight(), this.c0 + i3);
            this.b0.draw(canvas);
            int i4 = this.h0;
            this.b0.setBounds(0, i4 - this.c0, getRight(), i4);
            this.b0.draw(canvas);
        }
    }

    @DexIgnore
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(NumberPickerLarge.class.getName());
        accessibilityEvent.setScrollable(true);
        accessibilityEvent.setScrollY((this.u + this.w) * this.F);
        accessibilityEvent.setMaxScrollY((this.v - this.u) * this.F);
    }

    @DexIgnore
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.a0 || !isEnabled() || (motionEvent.getAction() & 255) != 0) {
            return false;
        }
        g();
        this.f.setVisibility(4);
        float y2 = motionEvent.getY();
        this.O = y2;
        this.P = y2;
        motionEvent.getEventTime();
        this.e0 = false;
        this.f0 = false;
        float f2 = this.O;
        if (f2 < ((float) this.g0)) {
            if (this.d0 == 0) {
                this.m0.a(2);
            }
        } else if (f2 > ((float) this.h0) && this.d0 == 0) {
            this.m0.a(1);
        }
        getParent().requestDisallowInterceptTouchEvent(true);
        if (!this.I.f()) {
            this.I.a(true);
            this.J.a(true);
            f(0);
        } else if (!this.J.f()) {
            this.I.a(true);
            this.J.a(true);
        } else {
            float f3 = this.O;
            if (f3 < ((float) this.g0)) {
                b();
                a(false, (long) ViewConfiguration.getLongPressTimeout());
            } else if (f3 > ((float) this.h0)) {
                b();
                a(true, (long) ViewConfiguration.getLongPressTimeout());
            } else {
                this.f0 = true;
                f();
            }
        }
        return true;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        if (!this.a0) {
            super.onLayout(z2, i2, i3, i4, i5);
            return;
        }
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        int measuredWidth2 = this.f.getMeasuredWidth();
        int measuredHeight2 = this.f.getMeasuredHeight();
        int i6 = (measuredWidth - measuredWidth2) / 2;
        int i7 = (measuredHeight - measuredHeight2) / 2;
        this.f.layout(i6, i7, measuredWidth2 + i6, measuredHeight2 + i7);
        if (z2) {
            d();
            c();
            int height = getHeight();
            int i8 = this.g;
            int i9 = this.c0;
            int i10 = ((height - i8) / 2) - i9;
            this.g0 = i10;
            this.h0 = i10 + (i9 * 2) + i8;
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (!this.a0) {
            super.onMeasure(i2, i3);
            return;
        }
        super.onMeasure(a(i2, this.p), a(i3, this.i));
        setMeasuredDimension(a(this.j, getMeasuredWidth(), i2), a(this.h, getMeasuredHeight(), i3));
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!isEnabled() || !this.a0) {
            return false;
        }
        if (this.Q == null) {
            this.Q = VelocityTracker.obtain();
        }
        this.Q.addMovement(motionEvent);
        int action = motionEvent.getAction() & 255;
        if (action == 1) {
            h();
            i();
            this.m0.a();
            VelocityTracker velocityTracker = this.Q;
            velocityTracker.computeCurrentVelocity(1000, (float) this.T);
            int yVelocity = (int) velocityTracker.getYVelocity();
            if (Math.abs(yVelocity) > this.S) {
                b(yVelocity);
                f(2);
            } else {
                int y2 = (int) motionEvent.getY();
                if (((int) Math.abs(((float) y2) - this.O)) > this.R) {
                    a();
                } else if (this.f0) {
                    this.f0 = false;
                    j();
                } else {
                    int i2 = (y2 / this.F) - 2;
                    if (i2 > 0) {
                        a(true);
                        this.m0.b(1);
                    } else if (i2 < 0) {
                        a(false);
                        this.m0.b(2);
                    }
                }
                f(0);
            }
            this.Q.recycle();
            this.Q = null;
        } else if (action == 2 && !this.e0) {
            float y3 = motionEvent.getY();
            if (this.d0 == 1) {
                scrollBy(0, (int) (y3 - this.P));
                invalidate();
            } else if (((int) Math.abs(y3 - this.O)) > this.R) {
                g();
                f(1);
            }
            this.P = y3;
        }
        return true;
    }

    @DexIgnore
    public void scrollBy(int i2, int i3) {
        int[] iArr = this.B;
        if (!this.U && i3 > 0 && iArr[2] <= this.u) {
            this.H = this.G;
        } else if (this.U || i3 >= 0 || iArr[2] < this.v) {
            this.H += i3;
            while (true) {
                int i4 = this.H;
                if (i4 - this.G <= this.s) {
                    break;
                }
                this.H = i4 - this.F;
                a(iArr);
                a(iArr[2], true);
                if (!this.U && iArr[2] <= this.u) {
                    this.H = this.G;
                }
            }
            while (true) {
                int i5 = this.H;
                if (i5 - this.G < (-this.s)) {
                    this.H = i5 + this.F;
                    b(iArr);
                    a(iArr[2], true);
                    if (!this.U && iArr[2] >= this.v) {
                        this.H = this.G;
                    }
                } else {
                    return;
                }
            }
        } else {
            this.H = this.G;
        }
    }

    @DexIgnore
    public void setDisplayedValues(String[] strArr) {
        if (!Arrays.equals(this.t, strArr)) {
            this.t = strArr;
            if (strArr != null) {
                this.f.setRawInputType(524289);
            } else {
                this.f.setRawInputType(2);
            }
            l();
            e();
            k();
        }
    }

    @DexIgnore
    public void setEnabled(boolean z2) {
        ImageButton imageButton;
        ImageButton imageButton2;
        super.setEnabled(z2);
        if (!this.a0 && (imageButton2 = this.d) != null) {
            imageButton2.setEnabled(z2);
        }
        if (!this.a0 && (imageButton = this.e) != null) {
            imageButton.setEnabled(z2);
        }
        this.f.setEnabled(z2);
    }

    @DexIgnore
    public void setFormatter(f fVar) {
        if (!Objects.equals(fVar, this.z)) {
            this.z = fVar;
            e();
            l();
        }
    }

    @DexIgnore
    public void setMaxValue(int i2) {
        if (this.v != i2) {
            if (i2 >= 0) {
                this.v = i2;
                if (i2 < this.w) {
                    this.w = i2;
                }
                setWrapSelectorWheel(this.v - this.u > this.B.length);
                e();
                l();
                k();
                invalidate();
                return;
            }
            throw new IllegalArgumentException("maxValue must be >= 0");
        }
    }

    @DexIgnore
    public void setMinValue(int i2) {
        if (this.u != i2) {
            if (i2 >= 0) {
                this.u = i2;
                if (i2 > this.w) {
                    this.w = i2;
                }
                setWrapSelectorWheel(this.v - this.u > this.B.length);
                e();
                l();
                k();
                invalidate();
                return;
            }
            throw new IllegalArgumentException("minValue must be >= 0");
        }
    }

    @DexIgnore
    public void setOnValueChangedListener(h hVar) {
        this.x = hVar;
    }

    @DexIgnore
    public void setValue(int i2) {
        a(i2, false);
    }

    @DexIgnore
    public NumberPickerLarge(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 2130969482);
    }

    @DexIgnore
    public NumberPickerLarge(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        this.A = new SparseArray<>();
        this.B = new int[5];
        this.G = RecyclerView.UNDEFINED_DURATION;
        boolean z2 = false;
        this.d0 = 0;
        this.n0 = -1;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.NumberPicker, i2, 0);
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        this.a0 = resourceId != 0;
        this.V = obtainStyledAttributes.getColor(15, 0);
        this.W = obtainStyledAttributes.getColor(10, 0);
        obtainStyledAttributes.getColor(16, 0);
        this.b0 = obtainStyledAttributes.getDrawable(12);
        this.a = obtainStyledAttributes.getString(7);
        this.c = obtainStyledAttributes.getString(6);
        this.b = obtainStyledAttributes.getString(8);
        this.c0 = obtainStyledAttributes.getDimensionPixelSize(13, (int) TypedValue.applyDimension(1, 2.0f, getResources().getDisplayMetrics()));
        this.g = obtainStyledAttributes.getDimensionPixelSize(14, (int) TypedValue.applyDimension(1, 48.0f, getResources().getDisplayMetrics()));
        this.h = obtainStyledAttributes.getDimensionPixelSize(3, -1);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(1, -1);
        this.i = dimensionPixelSize;
        int i3 = this.h;
        if (i3 == -1 || dimensionPixelSize == -1 || i3 <= dimensionPixelSize) {
            this.j = obtainStyledAttributes.getDimensionPixelSize(4, -1);
            int dimensionPixelSize2 = obtainStyledAttributes.getDimensionPixelSize(2, -1);
            this.p = dimensionPixelSize2;
            int i4 = this.j;
            if (i4 == -1 || dimensionPixelSize2 == -1 || i4 <= dimensionPixelSize2) {
                this.q = this.p == -1 ? true : z2;
                this.E = obtainStyledAttributes.getDrawable(17);
                String string = obtainStyledAttributes.getString(9);
                if (obtainStyledAttributes.hasValue(5)) {
                    this.o0 = c7.a(getContext(), obtainStyledAttributes.getResourceId(5, -1));
                }
                obtainStyledAttributes.recycle();
                this.m0 = new i();
                setWillNotDraw(!this.a0);
                LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
                if (layoutInflater != null) {
                    layoutInflater.inflate(resourceId, (ViewGroup) this, true);
                }
                a aVar = new a();
                b bVar = new b();
                if (!this.a0) {
                    ImageButton imageButton = (ImageButton) findViewById(2131362849);
                    this.d = imageButton;
                    imageButton.setOnClickListener(aVar);
                    this.d.setOnLongClickListener(bVar);
                } else {
                    this.d = null;
                }
                if (!this.a0) {
                    ImageButton imageButton2 = (ImageButton) findViewById(2131362848);
                    this.e = imageButton2;
                    imageButton2.setOnClickListener(aVar);
                    this.e.setOnLongClickListener(bVar);
                } else {
                    this.e = null;
                }
                this.f = (AppCompatEditText) findViewById(2131362850);
                if (!TextUtils.isEmpty(this.a)) {
                    int parseColor = Color.parseColor(eh5.l.a().b(this.a));
                    this.f.setTextColor(parseColor);
                    this.W = parseColor;
                    this.b0.setColorFilter(parseColor, PorterDuff.Mode.SRC_ATOP);
                }
                if (!TextUtils.isEmpty(this.c)) {
                    this.W = Color.parseColor(eh5.l.a().b(this.c));
                }
                if (!TextUtils.isEmpty(this.b)) {
                    Typeface c2 = eh5.l.a().c(this.b);
                    this.f.setTypeface(c2);
                    this.o0 = c2;
                }
                ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
                this.R = viewConfiguration.getScaledTouchSlop();
                this.S = viewConfiguration.getScaledMinimumFlingVelocity();
                this.T = viewConfiguration.getScaledMaximumFlingVelocity() / 8;
                this.r = (int) this.f.getTextSize();
                Paint paint = new Paint(1);
                paint.setAntiAlias(true);
                paint.setTextAlign(Paint.Align.CENTER);
                paint.setTextSize((float) this.r);
                Typeface typeface = this.o0;
                if (typeface != null) {
                    paint.setTypeface(typeface);
                } else {
                    paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), string));
                }
                paint.setColor(this.f.getTextColors().getColorForState(LinearLayout.ENABLED_STATE_SET, -1));
                this.C = paint;
                Paint paint2 = new Paint(1);
                paint2.setAntiAlias(true);
                paint2.setTextAlign(Paint.Align.CENTER);
                paint2.setTextSize((float) this.r);
                Typeface typeface2 = this.o0;
                if (typeface2 != null) {
                    paint2.setTypeface(typeface2);
                } else {
                    paint2.setTypeface(Typeface.createFromAsset(getResources().getAssets(), string));
                }
                paint2.setColor(this.W);
                this.D = paint2;
                this.I = new ny6(getContext(), null, true);
                this.J = new ny6(getContext(), new DecelerateInterpolator(2.5f));
                l();
                if (getImportantForAccessibility() == 0) {
                    setImportantForAccessibility(1);
                    return;
                }
                return;
            }
            throw new IllegalArgumentException("minWidth > maxWidth");
        }
        throw new IllegalArgumentException("minHeight > maxHeight");
    }

    @DexIgnore
    public String c(int i2) {
        f fVar = this.z;
        return fVar != null ? fVar.format(i2) : g(i2);
    }

    @DexIgnore
    public final void f() {
        d dVar = this.N;
        if (dVar == null) {
            this.N = new d();
        } else {
            removeCallbacks(dVar);
        }
        postDelayed(this.N, (long) ViewConfiguration.getLongPressTimeout());
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements Runnable {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;

        @DexIgnore
        public i() {
        }

        @DexIgnore
        public void a() {
            this.b = 0;
            this.a = 0;
            NumberPickerLarge.this.removeCallbacks(this);
            NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
            if (numberPickerLarge.j0) {
                numberPickerLarge.j0 = false;
                numberPickerLarge.invalidate(0, numberPickerLarge.h0, numberPickerLarge.getRight(), NumberPickerLarge.this.getBottom());
            }
        }

        @DexIgnore
        public void b(int i) {
            a();
            this.b = 2;
            this.a = i;
            NumberPickerLarge.this.post(this);
        }

        @DexIgnore
        public void run() {
            int i = this.b;
            if (i == 1) {
                int i2 = this.a;
                if (i2 == 1) {
                    NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                    numberPickerLarge.j0 = true;
                    numberPickerLarge.invalidate(0, numberPickerLarge.h0, numberPickerLarge.getRight(), NumberPickerLarge.this.getBottom());
                } else if (i2 == 2) {
                    NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
                    numberPickerLarge2.k0 = true;
                    numberPickerLarge2.invalidate(0, 0, numberPickerLarge2.getRight(), NumberPickerLarge.this.g0);
                }
            } else if (i == 2) {
                int i3 = this.a;
                if (i3 == 1) {
                    NumberPickerLarge numberPickerLarge3 = NumberPickerLarge.this;
                    if (!numberPickerLarge3.j0) {
                        numberPickerLarge3.postDelayed(this, (long) ViewConfiguration.getPressedStateDuration());
                    }
                    NumberPickerLarge numberPickerLarge4 = NumberPickerLarge.this;
                    numberPickerLarge4.j0 = !numberPickerLarge4.j0;
                    numberPickerLarge4.invalidate(0, numberPickerLarge4.h0, numberPickerLarge4.getRight(), NumberPickerLarge.this.getBottom());
                } else if (i3 == 2) {
                    NumberPickerLarge numberPickerLarge5 = NumberPickerLarge.this;
                    if (!numberPickerLarge5.k0) {
                        numberPickerLarge5.postDelayed(this, (long) ViewConfiguration.getPressedStateDuration());
                    }
                    NumberPickerLarge numberPickerLarge6 = NumberPickerLarge.this;
                    numberPickerLarge6.k0 = !numberPickerLarge6.k0;
                    numberPickerLarge6.invalidate(0, 0, numberPickerLarge6.getRight(), NumberPickerLarge.this.g0);
                }
            }
        }

        @DexIgnore
        public void a(int i) {
            a();
            this.b = 1;
            this.a = i;
            NumberPickerLarge.this.postDelayed(this, (long) ViewConfiguration.getTapTimeout());
        }
    }

    @DexIgnore
    public final void b(ny6 ny6) {
        if (Objects.equals(ny6, this.I)) {
            if (!a()) {
                l();
            }
            f(0);
        } else if (this.d0 != 1) {
            l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends AccessibilityNodeProvider {
        @DexIgnore
        public /* final */ Rect a; // = new Rect();
        @DexIgnore
        public /* final */ int[] b; // = new int[2];
        @DexIgnore
        public int c; // = RecyclerView.UNDEFINED_DURATION;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(int i, int i2) {
            if (i != 1) {
                if (i == 2) {
                    a(i2);
                } else if (i == 3 && d()) {
                    a(i, i2, b());
                }
            } else if (e()) {
                a(i, i2, c());
            }
        }

        @DexIgnore
        public final String b() {
            NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
            int i = numberPickerLarge.w - 1;
            if (numberPickerLarge.U) {
                i = numberPickerLarge.d(i);
            }
            NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
            int i2 = numberPickerLarge2.u;
            if (i < i2) {
                return null;
            }
            String[] strArr = numberPickerLarge2.t;
            return strArr == null ? numberPickerLarge2.c(i) : strArr[i - i2];
        }

        @DexIgnore
        public final String c() {
            NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
            int i = numberPickerLarge.w + 1;
            if (numberPickerLarge.U) {
                i = numberPickerLarge.d(i);
            }
            NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
            if (i > numberPickerLarge2.v) {
                return null;
            }
            String[] strArr = numberPickerLarge2.t;
            return strArr == null ? numberPickerLarge2.c(i) : strArr[i - numberPickerLarge2.u];
        }

        @DexIgnore
        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            if (i == -1) {
                return a(NumberPickerLarge.this.getScrollX(), NumberPickerLarge.this.getScrollY(), NumberPickerLarge.this.getScrollX() + (NumberPickerLarge.this.getRight() - NumberPickerLarge.this.getLeft()), NumberPickerLarge.this.getScrollY() + (NumberPickerLarge.this.getBottom() - NumberPickerLarge.this.getTop()));
            }
            if (i == 1) {
                String c2 = c();
                int scrollX = NumberPickerLarge.this.getScrollX();
                NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                return a(1, c2, scrollX, numberPickerLarge.h0 - numberPickerLarge.c0, numberPickerLarge.getScrollX() + (NumberPickerLarge.this.getRight() - NumberPickerLarge.this.getLeft()), NumberPickerLarge.this.getScrollY() + (NumberPickerLarge.this.getBottom() - NumberPickerLarge.this.getTop()));
            } else if (i == 2) {
                return a();
            } else {
                if (i != 3) {
                    return super.createAccessibilityNodeInfo(i);
                }
                String b2 = b();
                int scrollX2 = NumberPickerLarge.this.getScrollX();
                int scrollY = NumberPickerLarge.this.getScrollY();
                int scrollX3 = NumberPickerLarge.this.getScrollX() + (NumberPickerLarge.this.getRight() - NumberPickerLarge.this.getLeft());
                NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
                return a(3, b2, scrollX2, scrollY, scrollX3, numberPickerLarge2.g0 + numberPickerLarge2.c0);
            }
        }

        @DexIgnore
        public final boolean d() {
            return NumberPickerLarge.this.getWrapSelectorWheel() || NumberPickerLarge.this.getValue() > NumberPickerLarge.this.getMinValue();
        }

        @DexIgnore
        public final boolean e() {
            return NumberPickerLarge.this.getWrapSelectorWheel() || NumberPickerLarge.this.getValue() < NumberPickerLarge.this.getMaxValue();
        }

        @DexIgnore
        @Override // android.view.accessibility.AccessibilityNodeProvider
        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            if (TextUtils.isEmpty(str)) {
                return Collections.emptyList();
            }
            String lowerCase = str.toLowerCase();
            ArrayList arrayList = new ArrayList();
            if (i == -1) {
                a(lowerCase, 3, arrayList);
                a(lowerCase, 2, arrayList);
                a(lowerCase, 1, arrayList);
                return arrayList;
            } else if (i != 1 && i != 2 && i != 3) {
                return super.findAccessibilityNodeInfosByText(str, i);
            } else {
                a(lowerCase, i, arrayList);
                return arrayList;
            }
        }

        @DexIgnore
        public boolean performAction(int i, int i2, Bundle bundle) {
            if (i != -1) {
                if (i != 1) {
                    if (i != 2) {
                        if (i == 3) {
                            if (i2 != 16) {
                                if (i2 != 64) {
                                    if (i2 != 128 || this.c != i) {
                                        return false;
                                    }
                                    this.c = RecyclerView.UNDEFINED_DURATION;
                                    a(i, 65536);
                                    NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                                    numberPickerLarge.invalidate(0, 0, numberPickerLarge.getRight(), NumberPickerLarge.this.g0);
                                    return true;
                                } else if (this.c == i) {
                                    return false;
                                } else {
                                    this.c = i;
                                    a(i, 32768);
                                    NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
                                    numberPickerLarge2.invalidate(0, 0, numberPickerLarge2.getRight(), NumberPickerLarge.this.g0);
                                    return true;
                                }
                            } else if (!NumberPickerLarge.this.isEnabled()) {
                                return false;
                            } else {
                                NumberPickerLarge.this.a(false);
                                a(i, 1);
                                return true;
                            }
                        }
                    } else if (i2 != 1) {
                        if (i2 != 2) {
                            if (i2 != 16) {
                                if (i2 != 64) {
                                    if (i2 != 128) {
                                        return NumberPickerLarge.this.f.performAccessibilityAction(i2, bundle);
                                    }
                                    if (this.c != i) {
                                        return false;
                                    }
                                    this.c = RecyclerView.UNDEFINED_DURATION;
                                    a(i, 65536);
                                    NumberPickerLarge.this.f.invalidate();
                                    return true;
                                } else if (this.c == i) {
                                    return false;
                                } else {
                                    this.c = i;
                                    a(i, 32768);
                                    NumberPickerLarge.this.f.invalidate();
                                    return true;
                                }
                            } else if (!NumberPickerLarge.this.isEnabled()) {
                                return false;
                            } else {
                                NumberPickerLarge.this.j();
                                return true;
                            }
                        } else if (!NumberPickerLarge.this.isEnabled() || !NumberPickerLarge.this.f.isFocused()) {
                            return false;
                        } else {
                            NumberPickerLarge.this.f.clearFocus();
                            return true;
                        }
                    } else if (!NumberPickerLarge.this.isEnabled() || NumberPickerLarge.this.f.isFocused()) {
                        return false;
                    } else {
                        return NumberPickerLarge.this.f.requestFocus();
                    }
                } else if (i2 != 16) {
                    if (i2 != 64) {
                        if (i2 != 128 || this.c != i) {
                            return false;
                        }
                        this.c = RecyclerView.UNDEFINED_DURATION;
                        a(i, 65536);
                        NumberPickerLarge numberPickerLarge3 = NumberPickerLarge.this;
                        numberPickerLarge3.invalidate(0, numberPickerLarge3.h0, numberPickerLarge3.getRight(), NumberPickerLarge.this.getBottom());
                        return true;
                    } else if (this.c == i) {
                        return false;
                    } else {
                        this.c = i;
                        a(i, 32768);
                        NumberPickerLarge numberPickerLarge4 = NumberPickerLarge.this;
                        numberPickerLarge4.invalidate(0, numberPickerLarge4.h0, numberPickerLarge4.getRight(), NumberPickerLarge.this.getBottom());
                        return true;
                    }
                } else if (!NumberPickerLarge.this.isEnabled()) {
                    return false;
                } else {
                    NumberPickerLarge.this.a(true);
                    a(i, 1);
                    return true;
                }
            } else if (i2 != 64) {
                if (i2 != 128) {
                    if (i2 != 4096) {
                        if (i2 == 8192) {
                            if (!NumberPickerLarge.this.isEnabled() || (!NumberPickerLarge.this.getWrapSelectorWheel() && NumberPickerLarge.this.getValue() <= NumberPickerLarge.this.getMinValue())) {
                                return false;
                            }
                            NumberPickerLarge.this.a(false);
                            return true;
                        }
                    } else if (!NumberPickerLarge.this.isEnabled() || (!NumberPickerLarge.this.getWrapSelectorWheel() && NumberPickerLarge.this.getValue() >= NumberPickerLarge.this.getMaxValue())) {
                        return false;
                    } else {
                        NumberPickerLarge.this.a(true);
                        return true;
                    }
                } else if (this.c != i) {
                    return false;
                } else {
                    this.c = RecyclerView.UNDEFINED_DURATION;
                    NumberPickerLarge.this.performAccessibilityAction(128, null);
                    return true;
                }
            } else if (this.c == i) {
                return false;
            } else {
                this.c = i;
                NumberPickerLarge.this.performAccessibilityAction(64, null);
                return true;
            }
            return super.performAction(i, i2, bundle);
        }

        @DexIgnore
        public final void a(int i) {
            if (((AccessibilityManager) NumberPickerLarge.this.getContext().getSystemService("accessibility")).isEnabled()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i);
                NumberPickerLarge.this.f.onInitializeAccessibilityEvent(obtain);
                NumberPickerLarge.this.f.onPopulateAccessibilityEvent(obtain);
                obtain.setSource(NumberPickerLarge.this, 2);
                NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                numberPickerLarge.requestSendAccessibilityEvent(numberPickerLarge, obtain);
            }
        }

        @DexIgnore
        public final void a(int i, int i2, String str) {
            if (((AccessibilityManager) NumberPickerLarge.this.getContext().getSystemService("accessibility")).isEnabled()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
                obtain.setClassName(Button.class.getName());
                obtain.setPackageName(NumberPickerLarge.this.getContext().getPackageName());
                obtain.getText().add(str);
                obtain.setEnabled(NumberPickerLarge.this.isEnabled());
                obtain.setSource(NumberPickerLarge.this, i);
                NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                numberPickerLarge.requestSendAccessibilityEvent(numberPickerLarge, obtain);
            }
        }

        @DexIgnore
        public final void a(String str, int i, List<AccessibilityNodeInfo> list) {
            if (i == 1) {
                String c2 = c();
                if (!TextUtils.isEmpty(c2) && c2.toLowerCase().contains(str)) {
                    list.add(createAccessibilityNodeInfo(1));
                }
            } else if (i == 2) {
                Editable text = NumberPickerLarge.this.f.getText();
                if (TextUtils.isEmpty(text) || !text.toString().toLowerCase().contains(str)) {
                    Editable text2 = NumberPickerLarge.this.f.getText();
                    if (!TextUtils.isEmpty(text2) && text2.toString().toLowerCase().contains(str)) {
                        list.add(createAccessibilityNodeInfo(2));
                        return;
                    }
                    return;
                }
                list.add(createAccessibilityNodeInfo(2));
            } else if (i == 3) {
                String b2 = b();
                if (!TextUtils.isEmpty(b2) && b2.toLowerCase().contains(str)) {
                    list.add(createAccessibilityNodeInfo(3));
                }
            }
        }

        @DexIgnore
        public final AccessibilityNodeInfo a() {
            AccessibilityNodeInfo createAccessibilityNodeInfo = NumberPickerLarge.this.f.createAccessibilityNodeInfo();
            createAccessibilityNodeInfo.setSource(NumberPickerLarge.this, 2);
            if (this.c != 2) {
                createAccessibilityNodeInfo.addAction(64);
            }
            if (this.c == 2) {
                createAccessibilityNodeInfo.addAction(128);
            }
            return createAccessibilityNodeInfo;
        }

        @DexIgnore
        public final AccessibilityNodeInfo a(int i, String str, int i2, int i3, int i4, int i5) {
            AccessibilityNodeInfo obtain = AccessibilityNodeInfo.obtain();
            obtain.setClassName(Button.class.getName());
            obtain.setPackageName(NumberPickerLarge.this.getContext().getPackageName());
            obtain.setSource(NumberPickerLarge.this, i);
            obtain.setParent(NumberPickerLarge.this);
            obtain.setText(str);
            obtain.setClickable(true);
            obtain.setLongClickable(true);
            obtain.setEnabled(NumberPickerLarge.this.isEnabled());
            Rect rect = this.a;
            rect.set(i2, i3, i4, i5);
            obtain.setBoundsInParent(rect);
            int[] iArr = this.b;
            NumberPickerLarge.this.getLocationOnScreen(iArr);
            rect.offset(iArr[0], iArr[1]);
            obtain.setBoundsInScreen(rect);
            if (this.c != i) {
                obtain.addAction(64);
            }
            if (this.c == i) {
                obtain.addAction(128);
            }
            if (NumberPickerLarge.this.isEnabled()) {
                obtain.addAction(16);
            }
            return obtain;
        }

        @DexIgnore
        public final AccessibilityNodeInfo a(int i, int i2, int i3, int i4) {
            AccessibilityNodeInfo obtain = AccessibilityNodeInfo.obtain();
            obtain.setClassName(NumberPickerLarge.class.getName());
            obtain.setPackageName(NumberPickerLarge.this.getContext().getPackageName());
            obtain.setSource(NumberPickerLarge.this);
            if (d()) {
                obtain.addChild(NumberPickerLarge.this, 3);
            }
            obtain.addChild(NumberPickerLarge.this, 2);
            if (e()) {
                obtain.addChild(NumberPickerLarge.this, 1);
            }
            obtain.setParent((View) NumberPickerLarge.this.getParentForAccessibility());
            obtain.setEnabled(NumberPickerLarge.this.isEnabled());
            obtain.setScrollable(true);
            if (this.c != -1) {
                obtain.addAction(64);
            }
            if (this.c == -1) {
                obtain.addAction(128);
            }
            if (NumberPickerLarge.this.isEnabled()) {
                if (NumberPickerLarge.this.getWrapSelectorWheel() || NumberPickerLarge.this.getValue() < NumberPickerLarge.this.getMaxValue()) {
                    obtain.addAction(4096);
                }
                if (NumberPickerLarge.this.getWrapSelectorWheel() || NumberPickerLarge.this.getValue() > NumberPickerLarge.this.getMinValue()) {
                    obtain.addAction(8192);
                }
            }
            return obtain;
        }
    }

    @DexIgnore
    public final int a(int i2, int i3) {
        if (i3 == -1) {
            return i2;
        }
        int size = View.MeasureSpec.getSize(i2);
        int mode = View.MeasureSpec.getMode(i2);
        if (mode == Integer.MIN_VALUE) {
            return View.MeasureSpec.makeMeasureSpec(Math.min(size, i3), 1073741824);
        }
        if (mode == 0) {
            return View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
        }
        if (mode == 1073741824) {
            return i2;
        }
        throw new IllegalArgumentException("Unknown measure mode: " + mode);
    }

    @DexIgnore
    public static String g(int i2) {
        return String.format(Locale.getDefault(), "%d", Integer.valueOf(i2));
    }

    @DexIgnore
    public final void e(int i2) {
        h hVar = this.x;
        if (hVar != null) {
            hVar.a(this, i2, this.w);
        }
    }

    @DexIgnore
    public final int a(int i2, int i3, int i4) {
        return i2 != -1 ? resolveSizeAndState(Math.max(i2, i3), i4, 0) : i3;
    }

    @DexIgnore
    public final void b(int i2) {
        this.K = 0;
        if (i2 > 0) {
            this.I.a(0, 0, 0, i2, 0, 0, 0, Integer.MAX_VALUE);
        } else {
            this.I.a(0, Integer.MAX_VALUE, 0, i2, 0, 0, 0, Integer.MAX_VALUE);
        }
        invalidate();
    }

    @DexIgnore
    public int d(int i2) {
        int i3 = this.v;
        if (i2 > i3) {
            int i4 = this.u;
            return (i4 + ((i2 - i3) % (i3 - i4))) - 1;
        }
        int i5 = this.u;
        return i2 < i5 ? (i3 - ((i5 - i2) % (i3 - i5))) + 1 : i2;
    }

    @DexIgnore
    public final void a(int i2, boolean z2) {
        int i3;
        if (this.w != i2) {
            if (this.U) {
                i3 = d(i2);
            } else {
                i3 = Math.min(Math.max(i2, this.u), this.v);
            }
            int i4 = this.w;
            this.w = i3;
            l();
            if (z2) {
                e(i4);
            }
            e();
            invalidate();
        }
    }

    @DexIgnore
    public final void b(int[] iArr) {
        System.arraycopy(iArr, 1, iArr, 0, iArr.length - 1);
        int i2 = iArr[iArr.length - 2] + 1;
        if (this.U && i2 > this.v) {
            i2 = this.u;
        }
        iArr[iArr.length - 1] = i2;
        a(i2);
    }

    @DexIgnore
    public void a(boolean z2) {
        if (this.a0) {
            this.f.setVisibility(4);
            if (!a(this.I)) {
                a(this.J);
            }
            this.K = 0;
            if (z2) {
                this.I.a(0, 0, 0, -this.F, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS);
            } else {
                this.I.a(0, 0, 0, this.F, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS);
            }
            invalidate();
        } else if (z2) {
            a(this.w + 1, true);
        } else {
            a(this.w - 1, true);
        }
    }

    @DexIgnore
    public final void a(int[] iArr) {
        System.arraycopy(iArr, 0, iArr, 1, iArr.length - 1);
        int i2 = iArr[1] - 1;
        if (this.U && i2 < this.u) {
            i2 = this.v;
        }
        iArr[0] = i2;
        a(i2);
    }

    @DexIgnore
    public final void a(int i2) {
        String str;
        SparseArray<String> sparseArray = this.A;
        if (sparseArray.get(i2) == null) {
            int i3 = this.u;
            if (i2 < i3 || i2 > this.v) {
                str = "";
            } else {
                String[] strArr = this.t;
                str = strArr != null ? strArr[i2 - i3] : c(i2);
            }
            sparseArray.put(i2, str);
        }
    }

    @DexIgnore
    public void a(boolean z2, long j2) {
        e eVar = this.M;
        if (eVar == null) {
            this.M = new e();
        } else {
            removeCallbacks(eVar);
        }
        this.M.a(z2);
        postDelayed(this.M, j2);
    }

    @DexIgnore
    public final boolean a() {
        int i2 = this.G - this.H;
        if (i2 == 0) {
            return false;
        }
        this.K = 0;
        int abs = Math.abs(i2);
        int i3 = this.F;
        if (abs > i3 / 2) {
            if (i2 > 0) {
                i3 = -i3;
            }
            i2 += i3;
        }
        this.J.a(0, 0, 0, i2, 800);
        invalidate();
        return true;
    }
}
