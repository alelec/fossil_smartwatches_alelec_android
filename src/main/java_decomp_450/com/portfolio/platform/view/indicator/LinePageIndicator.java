package com.portfolio.platform.view.indicator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.fossil.mz6;
import com.fossil.pl4;
import com.fossil.v6;
import com.fossil.we5;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LinePageIndicator extends View implements mz6 {
    @DexIgnore
    public /* final */ Paint a;
    @DexIgnore
    public /* final */ Paint b;
    @DexIgnore
    public /* final */ Paint c;
    @DexIgnore
    public RecyclerView d;
    @DexIgnore
    public ViewPager e;
    @DexIgnore
    public ViewPager.i f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public int p;
    @DexIgnore
    public float q;
    @DexIgnore
    public int r;
    @DexIgnore
    public boolean s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends View.BaseSavedState {
        @DexIgnore
        public static /* final */ Parcelable.Creator<a> CREATOR; // = new C0313a();
        @DexIgnore
        public int a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.indicator.LinePageIndicator$a$a")
        /* renamed from: com.portfolio.platform.view.indicator.LinePageIndicator$a$a  reason: collision with other inner class name */
        public static class C0313a implements Parcelable.Creator<a> {
            @DexIgnore
            @Override // android.os.Parcelable.Creator
            public a createFromParcel(Parcel parcel) {
                return new a(parcel);
            }

            @DexIgnore
            @Override // android.os.Parcelable.Creator
            public a[] newArray(int i) {
                return new a[i];
            }
        }

        @DexIgnore
        public a(Parcelable parcelable) {
            super(parcelable);
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
        }

        @DexIgnore
        public a(Parcel parcel) {
            super(parcel);
            this.a = parcel.readInt();
        }
    }

    @DexIgnore
    public LinePageIndicator(Context context) {
        this(context, null);
    }

    @DexIgnore
    private void setStrokeWidth(float f2) {
        this.b.setStrokeWidth(f2);
        this.a.setStrokeWidth(f2);
        invalidate();
    }

    @DexIgnore
    private void setUnselectedBorderWidth(float f2) {
        this.c.setStyle(Paint.Style.STROKE);
        this.c.setStrokeWidth(f2);
    }

    @DexIgnore
    public void a(ViewPager viewPager, int i2) {
        setViewPager(viewPager);
        setCurrentItem(i2);
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void b(int i2) {
        this.g = we5.a(getContext()) ? (this.e.getAdapter().a() - i2) - 1 : i2;
        invalidate();
        ViewPager.i iVar = this.f;
        if (iVar != null) {
            iVar.b(i2);
        }
    }

    @DexIgnore
    public final int c(int i2) {
        float f2;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            f2 = (float) size;
        } else {
            float strokeWidth = this.b.getStrokeWidth() + ((float) getPaddingTop()) + ((float) getPaddingBottom());
            f2 = mode == Integer.MIN_VALUE ? Math.min(strokeWidth, (float) size) : strokeWidth;
        }
        return (int) Math.ceil((double) f2);
    }

    @DexIgnore
    public final int d(int i2) {
        float f2;
        ViewPager viewPager;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824 || (viewPager = this.e) == null) {
            f2 = (float) size;
        } else {
            int a2 = viewPager.getAdapter().a();
            f2 = ((float) (getPaddingLeft() + getPaddingRight())) + (((float) a2) * this.i) + (((float) (a2 - 1)) * this.j);
            if (mode == Integer.MIN_VALUE) {
                f2 = Math.min(f2, (float) size);
            }
        }
        return (int) Math.ceil((double) f2);
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        int i2;
        super.onDraw(canvas);
        ViewPager viewPager = this.e;
        int i3 = 0;
        if (viewPager == null || viewPager.getAdapter() == null) {
            RecyclerView recyclerView = this.d;
            i2 = (recyclerView == null || recyclerView.getAdapter() == null) ? 0 : this.d.getAdapter().getItemCount();
        } else {
            i2 = this.e.getAdapter().a();
        }
        if (i2 != 0) {
            if (this.g >= i2) {
                setCurrentItem(i2 - 1);
                return;
            }
            float f2 = this.i;
            float f3 = this.j;
            float f4 = f2 + f3;
            float f5 = (((float) i2) * f4) - f3;
            float paddingTop = (float) getPaddingTop();
            float paddingLeft = (float) getPaddingLeft();
            float paddingRight = (float) getPaddingRight();
            float height = paddingTop + (((((float) getHeight()) - paddingTop) - ((float) getPaddingBottom())) / 2.0f);
            if (this.h) {
                paddingLeft += (((((float) getWidth()) - paddingLeft) - paddingRight) / 2.0f) - (f5 / 2.0f);
            }
            float strokeWidth = this.b.getStrokeWidth();
            float strokeWidth2 = this.c.getStrokeWidth();
            while (i3 < i2) {
                float f6 = (((float) i3) * f4) + paddingLeft;
                float f7 = f6 + this.i;
                canvas.drawLine(f6, height, f7, height, i3 == this.g ? this.b : this.a);
                if (i3 != this.g) {
                    float f8 = strokeWidth2 / 2.0f;
                    canvas.drawRect(f6 + f8, height + ((strokeWidth2 - strokeWidth) / 2.0f), f7 - f8, height + ((strokeWidth - strokeWidth2) / 2.0f), this.c);
                }
                i3++;
            }
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        setMeasuredDimension(d(i2), c(i3));
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        a aVar = (a) parcelable;
        super.onRestoreInstanceState(aVar.getSuperState());
        this.g = aVar.a;
        requestLayout();
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        a aVar = new a(super.onSaveInstanceState());
        aVar.a = this.g;
        return aVar;
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        ViewPager viewPager = this.e;
        int i2 = 0;
        if (viewPager == null || viewPager.getAdapter() == null || this.e.getAdapter().a() == 0) {
            return false;
        }
        int action = motionEvent.getAction() & 255;
        if (action != 0) {
            if (action != 1) {
                if (action == 2) {
                    float x = motionEvent.getX(motionEvent.findPointerIndex(this.r));
                    float f2 = x - this.q;
                    if (!this.s && Math.abs(f2) > ((float) this.p)) {
                        this.s = true;
                    }
                    if (this.s) {
                        this.q = x;
                        if (this.e.g() || this.e.a()) {
                            this.e.b(f2);
                        }
                    }
                } else if (action != 3) {
                    if (action == 5) {
                        int actionIndex = motionEvent.getActionIndex();
                        this.q = motionEvent.getX(actionIndex);
                        this.r = motionEvent.getPointerId(actionIndex);
                    } else if (action == 6) {
                        int actionIndex2 = motionEvent.getActionIndex();
                        if (motionEvent.getPointerId(actionIndex2) == this.r) {
                            if (actionIndex2 == 0) {
                                i2 = 1;
                            }
                            this.r = motionEvent.getPointerId(i2);
                        }
                        this.q = motionEvent.getX(motionEvent.findPointerIndex(this.r));
                    }
                }
            }
            if (!this.s) {
                int a2 = this.e.getAdapter().a();
                float width = (float) getWidth();
                float f3 = width / 2.0f;
                float f4 = width / 6.0f;
                if (this.g > 0 && motionEvent.getX() < f3 - f4) {
                    if (action != 3) {
                        this.e.setCurrentItem(this.g - 1);
                    }
                    return true;
                } else if (this.g < a2 - 1 && motionEvent.getX() > f3 + f4) {
                    if (action != 3) {
                        this.e.setCurrentItem(this.g + 1);
                    }
                    return true;
                }
            }
            this.s = false;
            this.r = -1;
            if (this.e.g()) {
                this.e.d();
            }
        } else {
            this.r = motionEvent.getPointerId(0);
            this.q = motionEvent.getX();
        }
        return true;
    }

    @DexIgnore
    public void setCurrentItem(int i2) {
        if (this.e == null && this.d == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        ViewPager viewPager = this.e;
        if (viewPager != null) {
            viewPager.setCurrentItem(i2);
            if (we5.a(getContext())) {
                i2 = (this.e.getAdapter().a() - i2) - 1;
            }
            this.g = i2;
        } else {
            if (we5.a(getContext())) {
                i2 = (this.d.getAdapter().getItemCount() - i2) - 1;
            }
            this.g = i2;
        }
        invalidate();
    }

    @DexIgnore
    public void setOnPageChangeListener(ViewPager.i iVar) {
        this.f = iVar;
    }

    @DexIgnore
    public void setViewPager(ViewPager viewPager) {
        if (!Objects.equals(this.e, viewPager)) {
            if (viewPager.getAdapter() != null) {
                this.e = viewPager;
                viewPager.a((ViewPager.i) this);
                invalidate();
                setCurrentItem(0);
                return;
            }
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
    }

    @DexIgnore
    public LinePageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 2130969881);
    }

    @DexIgnore
    public LinePageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.a = new Paint(1);
        this.b = new Paint(1);
        this.c = new Paint(1);
        this.q = -1.0f;
        this.r = -1;
        if (!isInEditMode()) {
            Resources resources = getResources();
            int a2 = v6.a(getContext(), 2131099829);
            int a3 = v6.a(getContext(), 2131099764);
            float dimension = resources.getDimension(2131165319);
            float dimension2 = resources.getDimension(2131165318);
            float dimension3 = resources.getDimension(2131165320);
            float dimension4 = resources.getDimension(2131165320);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.LinePageIndicator, i2, 0);
            this.h = obtainStyledAttributes.getBoolean(2, true);
            this.i = obtainStyledAttributes.getDimension(8, dimension);
            this.j = obtainStyledAttributes.getDimension(1, dimension2);
            setStrokeWidth(obtainStyledAttributes.getDimension(4, dimension3));
            setUnselectedBorderWidth(obtainStyledAttributes.getDimension(6, dimension4));
            this.a.setColor(obtainStyledAttributes.getColor(7, a3));
            this.b.setColor(obtainStyledAttributes.getColor(3, a2));
            this.c.setColor(obtainStyledAttributes.getColor(5, a3));
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            if (drawable != null) {
                setBackground(drawable);
            }
            obtainStyledAttributes.recycle();
            this.p = ViewConfiguration.get(context).getScaledPagingTouchSlop();
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void a(int i2) {
        ViewPager.i iVar = this.f;
        if (iVar != null) {
            iVar.a(i2);
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void a(int i2, float f2, int i3) {
        ViewPager.i iVar = this.f;
        if (iVar != null) {
            iVar.a(i2, f2, i3);
        }
    }
}
