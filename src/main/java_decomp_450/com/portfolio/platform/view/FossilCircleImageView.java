package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.c50;
import com.fossil.ex;
import com.fossil.gd5;
import com.fossil.hd5;
import com.fossil.iw;
import com.fossil.kd5;
import com.fossil.pl4;
import com.fossil.py;
import com.fossil.q40;
import com.fossil.r40;
import com.fossil.sw;
import com.fossil.sw6;
import com.fossil.vd5;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.ref.WeakReference;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FossilCircleImageView extends AppCompatImageView {
    @DexIgnore
    public static /* final */ String w; // = FossilCircleImageView.class.getSimpleName();
    @DexIgnore
    public static /* final */ ImageView.ScaleType x; // = ImageView.ScaleType.CENTER_CROP;
    @DexIgnore
    public /* final */ RectF c;
    @DexIgnore
    public /* final */ RectF d;
    @DexIgnore
    public /* final */ Paint e;
    @DexIgnore
    public /* final */ Paint f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public String j;
    @DexIgnore
    public float p;
    @DexIgnore
    public float q;
    @DexIgnore
    public ColorFilter r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements q40<Drawable> {
        @DexIgnore
        public /* final */ /* synthetic */ kd5 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.FossilCircleImageView$a$a")
        /* renamed from: com.portfolio.platform.view.FossilCircleImageView$a$a  reason: collision with other inner class name */
        public class RunnableC0310a implements Runnable {
            @DexIgnore
            public RunnableC0310a() {
            }

            @DexIgnore
            public void run() {
                a.this.a.a((Object) new gd5("", a.this.b)).a(new r40().a((ex<Bitmap>) new vd5())).a((ImageView) FossilCircleImageView.this);
            }
        }

        @DexIgnore
        public a(kd5 kd5, String str) {
            this.a = kd5;
            this.b = str;
        }

        @DexIgnore
        public boolean a(Drawable drawable, Object obj, c50<Drawable> c50, sw swVar, boolean z) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.q40
        public boolean a(py pyVar, Object obj, c50<Drawable> c50, boolean z) {
            new Handler(Looper.getMainLooper()).post(new RunnableC0310a());
            return true;
        }
    }

    @DexIgnore
    public FossilCircleImageView(Context context) {
        super(context);
        this.c = new RectF();
        this.d = new RectF();
        this.e = new Paint();
        this.f = new Paint();
        this.g = -16777216;
        this.h = 0;
        this.i = 0;
        e();
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2) {
        new b(new WeakReference(this)).execute(bitmap, bitmap2);
    }

    @DexIgnore
    public final void c() {
    }

    @DexIgnore
    public final RectF d() {
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
        int min = Math.min(width, height);
        float paddingLeft = ((float) getPaddingLeft()) + (((float) (width - min)) / 2.0f);
        float paddingTop = ((float) getPaddingTop()) + (((float) (height - min)) / 2.0f);
        float f2 = (float) min;
        return new RectF(paddingLeft, paddingTop, paddingLeft + f2, f2 + paddingTop);
    }

    @DexIgnore
    public final void e() {
        super.setScaleType(x);
        this.s = true;
        if (this.t) {
            f();
            this.t = false;
        }
        FLogger.INSTANCE.getLocal().d(w, "init()");
    }

    @DexIgnore
    public final void f() {
        int i2;
        if (!this.s) {
            this.t = true;
        } else if (getWidth() != 0 || getHeight() != 0) {
            this.e.setStyle(Paint.Style.STROKE);
            this.e.setAntiAlias(true);
            this.e.setColor(this.g);
            this.e.setStrokeWidth((float) this.h);
            this.f.setStyle(Paint.Style.FILL);
            this.f.setAntiAlias(true);
            this.f.setColor(this.i);
            this.f.setFilterBitmap(true);
            this.f.setDither(true);
            this.d.set(d());
            this.q = Math.min((this.d.height() - ((float) this.h)) / 2.0f, (this.d.width() - ((float) this.h)) / 2.0f);
            this.c.set(this.d);
            if (!this.u && (i2 = this.h) > 0) {
                this.c.inset(((float) i2) - 1.0f, ((float) i2) - 1.0f);
            }
            this.p = Math.min(this.c.height() / 2.0f, this.c.width() / 2.0f);
            c();
            g();
            invalidate();
        }
    }

    @DexIgnore
    public final void g() {
    }

    @DexIgnore
    public int getBorderColor() {
        return this.g;
    }

    @DexIgnore
    public int getBorderWidth() {
        return this.h;
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        return this.r;
    }

    @DexIgnore
    @Deprecated
    public int getFillColor() {
        return this.i;
    }

    @DexIgnore
    public String getHandNumber() {
        return this.j;
    }

    @DexIgnore
    public ImageView.ScaleType getScaleType() {
        return x;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.i != 0) {
            canvas.drawCircle(this.c.centerX(), this.c.centerY(), this.p, this.f);
        }
        if (this.h > 0) {
            canvas.drawCircle(this.d.centerX(), this.d.centerY(), this.q, this.e);
        }
        if (this.v) {
            this.e.setAntiAlias(true);
            this.e.setFilterBitmap(true);
            this.e.setDither(true);
            this.e.setColor(Color.parseColor("#EEEEEE"));
            canvas.drawCircle(this.d.centerX(), this.d.centerY(), this.q, this.e);
        }
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        f();
    }

    @DexIgnore
    public void setAdjustViewBounds(boolean z) {
        if (z) {
            throw new IllegalArgumentException("adjustViewBounds not supported.");
        }
    }

    @DexIgnore
    public void setBorderColor(int i2) {
        if (i2 != this.g) {
            this.g = i2;
            this.e.setColor(i2);
            invalidate();
        }
    }

    @DexIgnore
    @Deprecated
    public void setBorderColorResource(int i2) {
        setBorderColor(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    public void setBorderWidth(int i2) {
        if (i2 != this.h) {
            this.h = i2;
            f();
        }
    }

    @DexIgnore
    @Override // android.widget.ImageView
    public void setColorFilter(ColorFilter colorFilter) {
        if (!Objects.equals(colorFilter, this.r)) {
            this.r = colorFilter;
            c();
            invalidate();
        }
    }

    @DexIgnore
    public void setDefault(boolean z) {
        this.v = z;
    }

    @DexIgnore
    @Deprecated
    public void setFillColor(int i2) {
        if (i2 != this.i) {
            this.i = i2;
            this.f.setColor(i2);
            invalidate();
        }
    }

    @DexIgnore
    @Deprecated
    public void setFillColorResource(int i2) {
        setColorFilter(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    public void setHandNumber(String str) {
        this.j = str;
    }

    @DexIgnore
    public void setPadding(int i2, int i3, int i4, int i5) {
        super.setPadding(i2, i3, i4, i5);
        f();
    }

    @DexIgnore
    public void setPaddingRelative(int i2, int i3, int i4, int i5) {
        super.setPaddingRelative(i2, i3, i4, i5);
        f();
    }

    @DexIgnore
    public void setScaleType(ImageView.ScaleType scaleType) {
        if (scaleType != x) {
            throw new IllegalArgumentException(String.format("ScaleType %s not supported.", scaleType));
        }
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3) {
        new b(new WeakReference(this)).execute(bitmap, bitmap2, bitmap3);
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, Bitmap bitmap4) {
        new b(new WeakReference(this)).execute(bitmap, bitmap2, bitmap3, bitmap4);
    }

    @DexIgnore
    public void a(kd5 kd5, String str, String str2) {
        kd5.a(str).c(3000).a(new r40().a((ex<Bitmap>) new vd5())).b((q40<Drawable>) new a(kd5, str2)).a((ImageView) this);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends AsyncTask<Bitmap, Void, Bitmap> {
        @DexIgnore
        public WeakReference<FossilCircleImageView> a;

        @DexIgnore
        public b(WeakReference<FossilCircleImageView> weakReference) {
            this.a = weakReference;
        }

        @DexIgnore
        /* renamed from: a */
        public Bitmap doInBackground(Bitmap... bitmapArr) {
            int length = bitmapArr.length;
            if (length == 1) {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.w, "doInBackground bitmap list = 0");
                return bitmapArr[0];
            } else if (length == 2) {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.w, "doInBackground bitmap list = 2");
                return sw6.a(bitmapArr[0], bitmapArr[1], ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            } else if (length == 3) {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.w, "doInBackground bitmap list = 3");
                return sw6.a(bitmapArr[0], bitmapArr[1], bitmapArr[2], ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            } else if (length != 4) {
                return null;
            } else {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.w, "doInBackground bitmap list = 4");
                return sw6.a(bitmapArr[0], bitmapArr[1], bitmapArr[2], bitmapArr[3], ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            }
        }

        @DexIgnore
        public void onPreExecute() {
            super.onPreExecute();
        }

        @DexIgnore
        /* renamed from: a */
        public void onPostExecute(Bitmap bitmap) {
            FLogger.INSTANCE.getLocal().d(FossilCircleImageView.w, "onPostExecute");
            if (this.a.get() != null) {
                this.a.get().setImageBitmap(bitmap);
            }
        }
    }

    @DexIgnore
    public void a(String str, iw iwVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = w;
        local.d(str2, "setImageUrl without defaultName url=" + str);
        iwVar.a(str).a(new r40().a((ex<Bitmap>) new vd5())).a((ImageView) this);
    }

    @DexIgnore
    public FossilCircleImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public FossilCircleImageView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.c = new RectF();
        this.d = new RectF();
        this.e = new Paint();
        this.f = new Paint();
        this.g = -16777216;
        this.h = 0;
        this.i = 0;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.FossilCircleImageView, i2, 0);
        this.h = obtainStyledAttributes.getDimensionPixelSize(2, 0);
        this.g = obtainStyledAttributes.getColor(0, -16777216);
        this.u = obtainStyledAttributes.getBoolean(1, false);
        this.i = obtainStyledAttributes.getColor(3, 0);
        String string = obtainStyledAttributes.getString(4);
        this.j = string;
        if (string == null) {
            this.j = "";
        }
        obtainStyledAttributes.recycle();
        FLogger.INSTANCE.getLocal().d(w, "FossilCircleImageView constructor");
        e();
    }

    @DexIgnore
    public void a(String str, String str2, iw iwVar) {
        hd5.a(this).a((Object) new gd5(str, str2)).a(new r40().a((ex<Bitmap>) new vd5())).a((ImageView) this);
    }

    @DexIgnore
    public void a(int i2, iw iwVar) {
        iwVar.a(Integer.valueOf(i2)).a(new r40().a((ex<Bitmap>) new vd5())).a((ImageView) this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "setImageResource resId = " + i2);
    }
}
