package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.fossil.c7;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.ez6;
import com.fossil.ig5;
import com.fossil.pl4;
import com.fossil.v6;
import com.fossil.yx6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BaseChart;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSleepSessionChart extends BaseChart {
    @DexIgnore
    public String A;
    @DexIgnore
    public float B;
    @DexIgnore
    public Integer C;
    @DexIgnore
    public float D;
    @DexIgnore
    public float E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public int O;
    @DexIgnore
    public short P;
    @DexIgnore
    public short Q;
    @DexIgnore
    public Paint R;
    @DexIgnore
    public Paint S;
    @DexIgnore
    public Paint T;
    @DexIgnore
    public Paint U;
    @DexIgnore
    public ArrayList<ez6> V;
    @DexIgnore
    public float W;
    @DexIgnore
    public boolean a0; // = true;
    @DexIgnore
    public String v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public String z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ Path a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(Path path, int i) {
            ee7.b(path, "path");
            this.a = path;
            this.b = i;
        }

        @DexIgnore
        public final Path a() {
            return this.a;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return ee7.a(this.a, bVar.a) && this.b == bVar.b;
        }

        @DexIgnore
        public int hashCode() {
            Path path = this.a;
            return ((path != null ? path.hashCode() : 0) * 31) + this.b;
        }

        @DexIgnore
        public String toString() {
            return "PathDist(path=" + this.a + ", sleepState=" + this.b + ")";
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSleepSessionChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Typeface c;
        String b2;
        String b3;
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        String str = "";
        this.v = str;
        this.z = str;
        this.A = str;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.HeartRateSleepSessionChart);
        String string = obtainStyledAttributes.getString(1);
        this.v = string == null ? str : string;
        this.w = obtainStyledAttributes.getColor(0, v6.a(context, (int) R.color.activeTime));
        this.x = obtainStyledAttributes.getColor(5, v6.a(context, 2131099834));
        this.y = obtainStyledAttributes.getColor(2, v6.a(context, 2131099834));
        String string2 = obtainStyledAttributes.getString(6);
        this.z = string2 == null ? str : string2;
        String string3 = obtainStyledAttributes.getString(4);
        this.A = string3 != null ? string3 : str;
        this.B = obtainStyledAttributes.getDimension(3, yx6.c(13.0f));
        this.C = Integer.valueOf(obtainStyledAttributes.getResourceId(8, -1));
        this.D = obtainStyledAttributes.getDimension(7, yx6.c(13.0f));
        obtainStyledAttributes.recycle();
        this.R = new Paint();
        if (!TextUtils.isEmpty(this.v) && (b3 = eh5.l.a().b(this.v)) != null) {
            this.R.setColor(Color.parseColor(b3));
        }
        this.R.setStrokeWidth(2.0f);
        this.R.setStyle(Paint.Style.STROKE);
        this.S = new Paint(1);
        if (!TextUtils.isEmpty(this.z) && (b2 = eh5.l.a().b(this.z)) != null) {
            this.S.setColor(Color.parseColor(b2));
        }
        this.S.setStyle(Paint.Style.FILL);
        this.S.setTextSize(this.B);
        if (!TextUtils.isEmpty(this.A) && (c = eh5.l.a().c(this.A)) != null) {
            this.S.setTypeface(c);
        }
        Paint paint = new Paint(1);
        this.T = paint;
        paint.setColor(this.S.getColor());
        this.T.setStyle(Paint.Style.FILL);
        this.T.setTextSize(this.D);
        Integer num = this.C;
        if (num == null || num.intValue() != -1) {
            Paint paint2 = this.T;
            Integer num2 = this.C;
            if (num2 != null) {
                paint2.setTypeface(c7.a(context, num2.intValue()));
            } else {
                ee7.a();
                throw null;
            }
        }
        Paint paint3 = new Paint(1);
        this.U = paint3;
        paint3.setStrokeWidth(yx6.a(1.0f));
        this.U.setStyle(Paint.Style.STROKE);
        Rect rect = new Rect();
        this.S.getTextBounds("2222", 0, 4, rect);
        this.L = (float) rect.width();
        this.M = (float) rect.height();
        Rect rect2 = new Rect();
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131887176);
        this.T.getTextBounds(a2, 0, a2.length(), rect2);
        this.N = (float) rect2.height();
        this.V = new ArrayList<>();
        a();
    }

    @DexIgnore
    public final void a(int i, int i2, int i3) {
        this.w = i3;
        this.x = i2;
        this.y = i;
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void b(Canvas canvas) {
        ee7.b(canvas, "canvas");
        super.b(canvas);
        float f = (float) 2;
        this.E = ((float) canvas.getHeight()) - (this.N * f);
        this.F = 25.0f;
        this.G = ((float) canvas.getWidth()) + 30.0f;
        this.H = this.D;
        this.I = this.F + yx6.a(5.0f);
        this.K = this.E - (this.D * f);
        float a2 = (this.G - this.L) + yx6.a(10.0f);
        this.J = a2;
        this.W = (a2 - this.I) / ((float) this.O);
        f(canvas);
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        canvas.drawLine(this.F, this.K, (float) canvas.getWidth(), this.K, this.R);
        canvas.drawLine(this.F, this.H, (float) canvas.getWidth(), this.H, this.R);
        if (this.a0) {
            canvas.drawText(String.valueOf((int) this.P), this.J + ((((float) canvas.getWidth()) - this.J) - this.S.measureText(String.valueOf((int) this.P))), this.K + (this.M * 1.5f), this.S);
            canvas.drawText(String.valueOf((int) this.Q), this.J + ((((float) canvas.getWidth()) - this.J) - this.S.measureText(String.valueOf((int) this.Q))), this.H - (this.M * 0.5f), this.S);
        }
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        e(canvas);
        g(canvas);
    }

    @DexIgnore
    public final void g(Canvas canvas) {
        int i;
        ArrayList<b> arrayList = new ArrayList();
        ez6 ez6 = (ez6) ea7.e((List) this.V);
        int d = ez6 != null ? ez6.d() : -1;
        Iterator<T> it = this.V.iterator();
        Path path = null;
        boolean z2 = false;
        int i2 = 0;
        while (true) {
            int i3 = 1;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            int a2 = next.a();
            int b2 = next.b();
            int c = next.c();
            float f = this.I + (((float) b2) * this.W);
            float f2 = this.H;
            float f3 = this.K;
            short s = this.Q;
            float f4 = f2 + (((f3 - f2) / ((float) (s - this.P))) * ((float) (s - a2)));
            if (!z2 || f4 > f3 || d != c) {
                if (!(f4 > this.K || d == c || path == null)) {
                    path.lineTo(f, f4);
                }
                if (path != null) {
                    if (i2 > 1) {
                        arrayList.add(new b(path, d));
                    }
                    path = null;
                }
                if (f4 <= this.K) {
                    Path path2 = new Path();
                    path2.moveTo(f, f4);
                    path = path2;
                    z2 = true;
                } else {
                    z2 = false;
                    i3 = 0;
                }
                d = c;
                i2 = i3;
            } else {
                if (path != null) {
                    path.lineTo(f, f4);
                }
                i2++;
            }
        }
        if (path != null && i2 > 1) {
            arrayList.add(new b(path, d));
        }
        for (b bVar : arrayList) {
            Path a3 = bVar.a();
            int b3 = bVar.b();
            Paint paint = this.U;
            if (b3 == 0) {
                i = this.w;
            } else if (b3 != 1) {
                i = this.y;
            } else {
                i = this.x;
            }
            paint.setColor(i);
            canvas.drawPath(a3, this.U);
        }
    }

    @DexIgnore
    public final int getMDuration() {
        return this.O;
    }

    @DexIgnore
    public final short getMMaxHRValue() {
        return this.Q;
    }

    @DexIgnore
    public final short getMMinHRValue() {
        return this.P;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        getMLegend().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), (i2 - getMLegendHeight()) - getMBottomPadding());
    }

    @DexIgnore
    public final void setMDuration(int i) {
        this.O = i;
    }

    @DexIgnore
    public final void setMMaxHRValue(short s) {
        if (s == ((short) 0)) {
            s = (short) 100;
        }
        this.Q = s;
    }

    @DexIgnore
    public final void setMMinHRValue(short s) {
        this.P = s;
    }

    @DexIgnore
    public final void a(ArrayList<ez6> arrayList) {
        ee7.b(arrayList, "heartRatePointList");
        this.V.clear();
        this.V.addAll(arrayList);
        this.a0 = !this.V.isEmpty();
        getMGraph().invalidate();
    }
}
