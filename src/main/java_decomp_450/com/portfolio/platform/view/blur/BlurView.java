package com.portfolio.platform.view.blur;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import androidx.renderscript.RenderScript;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.hh;
import com.fossil.jh;
import com.fossil.mh7;
import com.fossil.oh;
import com.fossil.pl4;
import com.fossil.rh;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BlurView extends View {
    @DexIgnore
    public static int w;
    @DexIgnore
    public static /* final */ b x; // = new b();
    @DexIgnore
    public static Boolean y; // = null;
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public /* final */ float a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public Bitmap e;
    @DexIgnore
    public Bitmap f;
    @DexIgnore
    public Canvas g;
    @DexIgnore
    public RenderScript h;
    @DexIgnore
    public rh i;
    @DexIgnore
    public hh j;
    @DexIgnore
    public hh p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public /* final */ Rect r; // = new Rect();
    @DexIgnore
    public /* final */ Rect s; // = new Rect();
    @DexIgnore
    public View t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public /* final */ ViewTreeObserver.OnPreDrawListener v; // = new c(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final Boolean a() {
            return BlurView.y;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(Boolean bool) {
            BlurView.y = bool;
        }

        @DexIgnore
        public final boolean a(Context context) {
            if (a() == null && context != null) {
                a(Boolean.valueOf((context.getApplicationInfo().flags & 2) != 0));
            }
            if (a() == Boolean.TRUE) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RuntimeException {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ViewTreeObserver.OnPreDrawListener {
        @DexIgnore
        public /* final */ /* synthetic */ BlurView a;

        @DexIgnore
        public c(BlurView blurView) {
            this.a = blurView;
        }

        @DexIgnore
        public final boolean onPreDraw() {
            Canvas canvas;
            int[] iArr = new int[2];
            Bitmap b = this.a.f;
            View d = this.a.t;
            if (d != null && this.a.isShown() && this.a.a()) {
                boolean z = !ee7.a(this.a.f, b);
                d.getLocationOnScreen(iArr);
                this.a.getLocationOnScreen(iArr);
                int i = (-iArr[0]) + iArr[0];
                int i2 = (-iArr[1]) + iArr[1];
                Bitmap a2 = this.a.e;
                if (a2 != null) {
                    a2.eraseColor(this.a.b & 16777215);
                    Canvas c = this.a.g;
                    if (c != null) {
                        int save = c.save();
                        this.a.q = true;
                        BlurView.w = BlurView.w + 1;
                        try {
                            Canvas c2 = this.a.g;
                            if (c2 != null) {
                                Bitmap a3 = this.a.e;
                                if (a3 != null) {
                                    float width = (((float) a3.getWidth()) * 1.0f) / ((float) this.a.getWidth());
                                    Bitmap a4 = this.a.e;
                                    if (a4 != null) {
                                        c2.scale(width, (1.0f * ((float) a4.getHeight())) / ((float) this.a.getHeight()));
                                        Canvas c3 = this.a.g;
                                        if (c3 != null) {
                                            c3.translate((float) (-i), (float) (-i2));
                                            if (d.getBackground() != null) {
                                                Drawable background = d.getBackground();
                                                Canvas c4 = this.a.g;
                                                if (c4 != null) {
                                                    background.draw(c4);
                                                } else {
                                                    ee7.a();
                                                    throw null;
                                                }
                                            }
                                            d.draw(this.a.g);
                                            this.a.q = false;
                                            BlurView.w = BlurView.w - 1;
                                            canvas = this.a.g;
                                            if (canvas == null) {
                                                ee7.a();
                                                throw null;
                                            }
                                            canvas.restoreToCount(save);
                                            BlurView blurView = this.a;
                                            Bitmap a5 = blurView.e;
                                            if (a5 != null) {
                                                Bitmap b2 = this.a.f;
                                                if (b2 != null) {
                                                    blurView.a(a5, b2);
                                                    if (z || this.a.u) {
                                                        this.a.invalidate();
                                                    }
                                                } else {
                                                    ee7.a();
                                                    throw null;
                                                }
                                            } else {
                                                ee7.a();
                                                throw null;
                                            }
                                        } else {
                                            ee7.a();
                                            throw null;
                                        }
                                    } else {
                                        ee7.a();
                                        throw null;
                                    }
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } catch (b unused) {
                            this.a.q = false;
                            BlurView.w = BlurView.w - 1;
                            canvas = this.a.g;
                            if (canvas == null) {
                                ee7.a();
                                throw null;
                            }
                        } catch (Throwable th) {
                            this.a.q = false;
                            BlurView.w = BlurView.w - 1;
                            Canvas c5 = this.a.g;
                            if (c5 == null) {
                                ee7.a();
                                throw null;
                            }
                            c5.restoreToCount(save);
                            throw th;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            return true;
        }
    }

    /*
    static {
        try {
            ClassLoader classLoader = BlurView.class.getClassLoader();
            if (classLoader != null) {
                classLoader.loadClass("android.support.v8.renderscript.RenderScript");
                return;
            }
            ee7.a();
            throw null;
        } catch (ClassNotFoundException unused) {
            throw new RuntimeException("RenderScript support not enabled. Add \"android { defaultConfig { renderscriptSupportModeEnabled true }}\" in your build.gradle");
        }
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BlurView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.BlurView);
        Resources resources = context.getResources();
        ee7.a((Object) resources, "context.resources");
        this.c = obtainStyledAttributes.getDimension(2, TypedValue.applyDimension(1, 10.0f, resources.getDisplayMetrics()));
        this.a = obtainStyledAttributes.getFloat(0, 4.0f);
        this.b = obtainStyledAttributes.getColor(1, 0);
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    private final View getActivityDecorView() {
        Context context = getContext();
        for (int i2 = 0; i2 < 4 && context != null && !(context instanceof Activity) && (context instanceof ContextWrapper); i2++) {
            context = ((ContextWrapper) context).getBaseContext();
        }
        if (!(context instanceof Activity)) {
            return null;
        }
        Window window = ((Activity) context).getWindow();
        ee7.a((Object) window, "ctx.window");
        return window.getDecorView();
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        ee7.b(canvas, "canvas");
        if (this.q) {
            throw x;
        } else if (w <= 0) {
            super.draw(canvas);
        }
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        View activityDecorView = getActivityDecorView();
        this.t = activityDecorView;
        boolean z2 = false;
        if (activityDecorView == null) {
            this.u = false;
        } else if (activityDecorView != null) {
            activityDecorView.getViewTreeObserver().addOnPreDrawListener(this.v);
            View view = this.t;
            if (view != null) {
                if (view.getRootView() != getRootView()) {
                    z2 = true;
                }
                this.u = z2;
                if (z2) {
                    View view2 = this.t;
                    if (view2 != null) {
                        view2.postInvalidate();
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        View view = this.t;
        if (view != null) {
            if (view != null) {
                view.getViewTreeObserver().removeOnPreDrawListener(this.v);
            } else {
                ee7.a();
                throw null;
            }
        }
        b();
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        ee7.b(canvas, "canvas");
        super.onDraw(canvas);
        a(canvas, this.f, this.b);
    }

    @DexIgnore
    public final void b() {
        c();
        d();
    }

    @DexIgnore
    public final void c() {
        hh hhVar = this.j;
        if (hhVar != null) {
            if (hhVar != null) {
                hhVar.b();
                this.j = null;
            } else {
                ee7.a();
                throw null;
            }
        }
        hh hhVar2 = this.p;
        if (hhVar2 != null) {
            if (hhVar2 != null) {
                hhVar2.b();
                this.p = null;
            } else {
                ee7.a();
                throw null;
            }
        }
        Bitmap bitmap = this.e;
        if (bitmap != null) {
            if (bitmap != null) {
                bitmap.recycle();
                this.e = null;
            } else {
                ee7.a();
                throw null;
            }
        }
        Bitmap bitmap2 = this.f;
        if (bitmap2 == null) {
            return;
        }
        if (bitmap2 != null) {
            bitmap2.recycle();
            this.f = null;
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void d() {
        RenderScript renderScript = this.h;
        if (renderScript != null) {
            if (renderScript != null) {
                renderScript.a();
                this.h = null;
            } else {
                ee7.a();
                throw null;
            }
        }
        rh rhVar = this.i;
        if (rhVar == null) {
            return;
        }
        if (rhVar != null) {
            rhVar.b();
            this.i = null;
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final boolean a() {
        Bitmap bitmap;
        if (this.c == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            b();
            return false;
        }
        float f2 = this.a;
        if (this.d || this.h == null) {
            if (this.h == null) {
                try {
                    RenderScript a2 = RenderScript.a(getContext());
                    this.h = a2;
                    if (a2 == null) {
                        ee7.a();
                        throw null;
                    } else if (a2 != null) {
                        this.i = rh.a(a2, jh.h(a2));
                    } else {
                        ee7.a();
                        throw null;
                    }
                } catch (oh e2) {
                    if (z.a(getContext())) {
                        if (e2.getMessage() != null) {
                            String message = e2.getMessage();
                            if (message == null) {
                                ee7.a();
                                throw null;
                            } else if (mh7.c(message, "Error networkLoading RS jni library: java.lang.UnsatisfiedLinkError:", false, 2, null)) {
                                throw new RuntimeException("Error networkLoading RS jni library, Upgrade buildToolsVersion=\"24.0.2\" or higher may solve this issue");
                            }
                        }
                        throw e2;
                    }
                    d();
                    return false;
                }
            }
            this.d = false;
            float f3 = this.c / f2;
            if (f3 > 25.0f) {
                f2 = (f2 * f3) / 25.0f;
                f3 = 25.0f;
            }
            rh rhVar = this.i;
            if (rhVar != null) {
                rhVar.a(f3);
            } else {
                ee7.a();
                throw null;
            }
        }
        int width = getWidth();
        int height = getHeight();
        int max = Math.max(1, (int) (((float) width) / f2));
        int max2 = Math.max(1, (int) (((float) height) / f2));
        if (!(this.g == null || (bitmap = this.f) == null)) {
            if (bitmap == null) {
                ee7.a();
                throw null;
            } else if (bitmap.getWidth() == max) {
                Bitmap bitmap2 = this.f;
                if (bitmap2 == null) {
                    ee7.a();
                    throw null;
                } else if (bitmap2.getHeight() == max2) {
                    return true;
                }
            }
        }
        c();
        try {
            Bitmap createBitmap = Bitmap.createBitmap(max, max2, Bitmap.Config.ARGB_8888);
            this.e = createBitmap;
            if (createBitmap == null) {
                c();
                return false;
            }
            Bitmap bitmap3 = this.e;
            if (bitmap3 != null) {
                this.g = new Canvas(bitmap3);
                RenderScript renderScript = this.h;
                if (renderScript != null) {
                    Bitmap bitmap4 = this.e;
                    if (bitmap4 != null) {
                        hh a3 = hh.a(renderScript, bitmap4, hh.b.MIPMAP_NONE, 1);
                        this.j = a3;
                        RenderScript renderScript2 = this.h;
                        if (renderScript2 == null) {
                            ee7.a();
                            throw null;
                        } else if (a3 != null) {
                            this.p = hh.a(renderScript2, a3.e());
                            Bitmap createBitmap2 = Bitmap.createBitmap(max, max2, Bitmap.Config.ARGB_8888);
                            this.f = createBitmap2;
                            if (createBitmap2 != null) {
                                return true;
                            }
                            c();
                            return false;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } catch (OutOfMemoryError unused) {
            c();
            return false;
        } catch (Throwable unused2) {
            c();
            return false;
        }
    }

    @DexIgnore
    public final void a(Bitmap bitmap, Bitmap bitmap2) {
        hh hhVar = this.j;
        if (hhVar != null) {
            hhVar.a(bitmap);
            rh rhVar = this.i;
            if (rhVar != null) {
                rhVar.c(this.j);
                rh rhVar2 = this.i;
                if (rhVar2 != null) {
                    rhVar2.b(this.p);
                    hh hhVar2 = this.p;
                    if (hhVar2 != null) {
                        hhVar2.b(bitmap2);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, Bitmap bitmap, int i2) {
        if (bitmap != null) {
            this.r.right = bitmap.getWidth();
            this.r.bottom = bitmap.getHeight();
            this.s.right = getWidth();
            this.s.bottom = getHeight();
            canvas.drawBitmap(bitmap, this.r, this.s, (Paint) null);
        }
        canvas.drawColor(i2);
    }
}
