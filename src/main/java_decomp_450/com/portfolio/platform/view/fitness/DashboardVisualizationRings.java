package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.pl4;
import com.fossil.v6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardVisualizationRings extends View implements ViewTreeObserver.OnGlobalLayoutListener {
    @DexIgnore
    public static /* final */ String T;
    @DexIgnore
    public /* final */ Paint A;
    @DexIgnore
    public /* final */ Paint B;
    @DexIgnore
    public /* final */ Paint C;
    @DexIgnore
    public /* final */ Paint D;
    @DexIgnore
    public /* final */ int E;
    @DexIgnore
    public /* final */ int F;
    @DexIgnore
    public /* final */ int G;
    @DexIgnore
    public Bitmap H;
    @DexIgnore
    public Bitmap I;
    @DexIgnore
    public Bitmap J;
    @DexIgnore
    public Bitmap K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public float O;
    @DexIgnore
    public float P;
    @DexIgnore
    public float Q;
    @DexIgnore
    public float R;
    @DexIgnore
    public float S;
    @DexIgnore
    public TypedArray a;
    @DexIgnore
    public /* final */ RectF b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ Paint v;
    @DexIgnore
    public /* final */ Paint w;
    @DexIgnore
    public /* final */ Paint x;
    @DexIgnore
    public /* final */ Paint y;
    @DexIgnore
    public /* final */ Paint z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
        String simpleName = DashboardVisualizationRings.class.getSimpleName();
        ee7.a((Object) simpleName, "DashboardVisualizationRings::class.java.simpleName");
        T = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardVisualizationRings(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        this.b = new RectF();
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = new Paint(1);
        this.w = new Paint(1);
        this.x = new Paint(1);
        this.y = new Paint(1);
        this.z = new Paint(1);
        this.A = new Paint(1);
        this.B = new Paint(1);
        this.C = new Paint(1);
        this.D = new Paint(1);
        this.E = context.getResources().getDimensionPixelSize(2131165412);
        this.F = context.getResources().getDimensionPixelSize(2131165420);
        this.G = context.getResources().getDimensionPixelSize(2131165395);
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            this.a = context.getTheme().obtainStyledAttributes(attributeSet, pl4.DashboardVisualizationRings, 0, 0);
        }
        int a2 = v6.a(context, 2131099833);
        this.s = v6.a(context, 2131099703);
        TypedArray typedArray = this.a;
        this.r = typedArray != null ? typedArray.getColor(2, a2) : a2;
        TypedArray typedArray2 = this.a;
        this.c = typedArray2 != null ? typedArray2.getColor(9, a2) : a2;
        TypedArray typedArray3 = this.a;
        this.d = typedArray3 != null ? typedArray3.getColor(9, a2) : a2;
        TypedArray typedArray4 = this.a;
        this.e = typedArray4 != null ? typedArray4.getColor(0, a2) : a2;
        TypedArray typedArray5 = this.a;
        this.f = typedArray5 != null ? typedArray5.getColor(0, a2) : a2;
        TypedArray typedArray6 = this.a;
        this.g = typedArray6 != null ? typedArray6.getColor(3, a2) : a2;
        TypedArray typedArray7 = this.a;
        this.h = typedArray7 != null ? typedArray7.getColor(3, a2) : a2;
        TypedArray typedArray8 = this.a;
        this.j = typedArray8 != null ? typedArray8.getColor(6, a2) : a2;
        TypedArray typedArray9 = this.a;
        this.p = typedArray9 != null ? typedArray9.getColor(7, a2) : a2;
        TypedArray typedArray10 = this.a;
        this.q = typedArray10 != null ? typedArray10.getColor(8, a2) : a2;
        TypedArray typedArray11 = this.a;
        this.i = typedArray11 != null ? typedArray11.getColor(5, a2) : a2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0046, code lost:
        if (r0.isRecycled() != false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007c, code lost:
        if (r0.isRecycled() != false) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0010, code lost:
        if (r0.isRecycled() != false) goto L_0x0017;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r6 = this;
            android.graphics.Bitmap r0 = r6.H
            java.lang.String r1 = "mStepsBitmap"
            java.lang.String r2 = "Bitmap.createScaledBitma\u2026Px, mIconSizeInPx, false)"
            r3 = 0
            r4 = 0
            if (r0 == 0) goto L_0x0017
            if (r0 == 0) goto L_0x0013
            boolean r0 = r0.isRecycled()
            if (r0 == 0) goto L_0x003a
            goto L_0x0017
        L_0x0013:
            com.fossil.ee7.d(r1)
            throw r4
        L_0x0017:
            android.content.res.Resources r0 = r6.getResources()
            r5 = 2131231189(0x7f0801d5, float:1.8078452E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r5)
            int r5 = r6.G
            android.graphics.Bitmap r5 = android.graphics.Bitmap.createScaledBitmap(r0, r5, r5, r3)
            com.fossil.ee7.a(r5, r2)
            r6.H = r5
            if (r5 == 0) goto L_0x00e9
            boolean r1 = com.fossil.ee7.a(r0, r5)
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x003a
            r0.recycle()
        L_0x003a:
            android.graphics.Bitmap r0 = r6.I
            java.lang.String r1 = "mActiveMinutesBitmap"
            if (r0 == 0) goto L_0x004d
            if (r0 == 0) goto L_0x0049
            boolean r0 = r0.isRecycled()
            if (r0 == 0) goto L_0x0070
            goto L_0x004d
        L_0x0049:
            com.fossil.ee7.d(r1)
            throw r4
        L_0x004d:
            android.content.res.Resources r0 = r6.getResources()
            r5 = 2131231185(0x7f0801d1, float:1.8078444E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r5)
            int r5 = r6.G
            android.graphics.Bitmap r5 = android.graphics.Bitmap.createScaledBitmap(r0, r5, r5, r3)
            com.fossil.ee7.a(r5, r2)
            r6.I = r5
            if (r5 == 0) goto L_0x00e5
            boolean r1 = com.fossil.ee7.a(r0, r5)
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x0070
            r0.recycle()
        L_0x0070:
            android.graphics.Bitmap r0 = r6.J
            java.lang.String r1 = "mCaloriesBitmap"
            if (r0 == 0) goto L_0x0083
            if (r0 == 0) goto L_0x007f
            boolean r0 = r0.isRecycled()
            if (r0 == 0) goto L_0x00a6
            goto L_0x0083
        L_0x007f:
            com.fossil.ee7.d(r1)
            throw r4
        L_0x0083:
            android.content.res.Resources r0 = r6.getResources()
            r5 = 2131231186(0x7f0801d2, float:1.8078446E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r5)
            int r5 = r6.G
            android.graphics.Bitmap r5 = android.graphics.Bitmap.createScaledBitmap(r0, r5, r5, r3)
            com.fossil.ee7.a(r5, r2)
            r6.J = r5
            if (r5 == 0) goto L_0x00e1
            boolean r1 = com.fossil.ee7.a(r0, r5)
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x00a6
            r0.recycle()
        L_0x00a6:
            android.graphics.Bitmap r0 = r6.K
            java.lang.String r1 = "mSleepBitmap"
            if (r0 == 0) goto L_0x00b9
            if (r0 == 0) goto L_0x00b5
            boolean r0 = r0.isRecycled()
            if (r0 == 0) goto L_0x00dc
            goto L_0x00b9
        L_0x00b5:
            com.fossil.ee7.d(r1)
            throw r4
        L_0x00b9:
            android.content.res.Resources r0 = r6.getResources()
            r5 = 2131231188(0x7f0801d4, float:1.807845E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r5)
            int r5 = r6.G
            android.graphics.Bitmap r3 = android.graphics.Bitmap.createScaledBitmap(r0, r5, r5, r3)
            com.fossil.ee7.a(r3, r2)
            r6.K = r3
            if (r3 == 0) goto L_0x00dd
            boolean r1 = com.fossil.ee7.a(r0, r3)
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x00dc
            r0.recycle()
        L_0x00dc:
            return
        L_0x00dd:
            com.fossil.ee7.d(r1)
            throw r4
        L_0x00e1:
            com.fossil.ee7.d(r1)
            throw r4
        L_0x00e5:
            com.fossil.ee7.d(r1)
            throw r4
        L_0x00e9:
            com.fossil.ee7.d(r1)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.fitness.DashboardVisualizationRings.a():void");
    }

    @DexIgnore
    public final void b() {
        Bitmap bitmap = this.H;
        if (bitmap != null) {
            if (bitmap != null) {
                bitmap.recycle();
            } else {
                ee7.d("mStepsBitmap");
                throw null;
            }
        }
        Bitmap bitmap2 = this.J;
        if (bitmap2 != null) {
            if (bitmap2 != null) {
                bitmap2.recycle();
            } else {
                ee7.d("mCaloriesBitmap");
                throw null;
            }
        }
        Bitmap bitmap3 = this.I;
        if (bitmap3 != null) {
            if (bitmap3 != null) {
                bitmap3.recycle();
            } else {
                ee7.d("mActiveMinutesBitmap");
                throw null;
            }
        }
        Bitmap bitmap4 = this.K;
        if (bitmap4 == null) {
            return;
        }
        if (bitmap4 != null) {
            bitmap4.recycle();
        } else {
            ee7.d("mSleepBitmap");
            throw null;
        }
    }

    @DexIgnore
    public final void c(Canvas canvas, float f2, float f3, Bitmap bitmap) {
        a(canvas, a(this.b, ((float) this.F) * 1.5f), f2, f3, bitmap, this.B, this.C);
    }

    @DexIgnore
    public final void d(Canvas canvas, float f2, float f3, Bitmap bitmap) {
        RectF rectF = this.b;
        Paint paint = this.v;
        a(canvas, rectF, f2, f3, bitmap, paint, paint);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(this.r);
            float f2 = this.L;
            float f3 = this.M;
            Bitmap bitmap = this.H;
            if (bitmap != null) {
                d(canvas, f2, f3, bitmap);
                float f4 = this.N;
                float f5 = this.O;
                Bitmap bitmap2 = this.J;
                if (bitmap2 != null) {
                    b(canvas, f4, f5, bitmap2);
                    float f6 = this.P;
                    float f7 = this.Q;
                    Bitmap bitmap3 = this.I;
                    if (bitmap3 != null) {
                        a(canvas, f6, f7, bitmap3);
                        float f8 = this.R;
                        float f9 = this.S;
                        Bitmap bitmap4 = this.K;
                        if (bitmap4 != null) {
                            c(canvas, f8, f9, bitmap4);
                        } else {
                            ee7.d("mSleepBitmap");
                            throw null;
                        }
                    } else {
                        ee7.d("mActiveMinutesBitmap");
                        throw null;
                    }
                } else {
                    ee7.d("mCaloriesBitmap");
                    throw null;
                }
            } else {
                ee7.d("mStepsBitmap");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onAttachedToWindow() {
        FLogger.INSTANCE.getLocal().d(T, "onAttachedToWindow, initIcons");
        super.onAttachedToWindow();
        a();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        FLogger.INSTANCE.getLocal().d(T, "onDetachedFromWindow, recycleIcons");
        super.onDetachedFromWindow();
        b();
    }

    @DexIgnore
    public void onGlobalLayout() {
        float width = (float) getWidth();
        float height = (float) getHeight();
        if (width > height) {
            float f2 = (width - height) / ((float) 2);
            RectF rectF = this.b;
            rectF.left = f2;
            rectF.right = f2 + height;
            rectF.top = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            rectF.bottom = height;
        } else if (height > width) {
            float f3 = (height - width) / ((float) 2);
            RectF rectF2 = this.b;
            rectF2.left = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            rectF2.right = width;
            rectF2.top = f3;
            rectF2.bottom = f3 + width;
        } else {
            RectF rectF3 = this.b;
            rectF3.left = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            rectF3.right = width;
            rectF3.top = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            rectF3.bottom = height;
        }
        float f4 = (float) this.E;
        this.t.setColor(this.i);
        this.t.setDither(true);
        this.t.setStyle(Paint.Style.STROKE);
        this.t.setStrokeWidth(f4);
        this.u.setColor(this.s);
        this.u.setStrokeWidth(f4);
        a(this.v, this.c, f4);
        a(this.w, this.d, f4);
        a(this.z, this.g, f4);
        a(this.A, this.h, f4);
        a(this.x, this.e, f4);
        a(this.y, this.f, f4);
        a(this.B, this.j, f4);
        a(this.C, this.p, f4);
        a(this.D, this.q, f4);
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void b(Canvas canvas, float f2, float f3, Bitmap bitmap) {
        RectF a2 = a(this.b, ((float) this.F) / ((float) 2));
        Paint paint = this.z;
        a(canvas, a2, f2, f3, bitmap, paint, paint);
    }

    @DexIgnore
    public final void a(Paint paint, int i2, float f2) {
        paint.setColor(i2);
        paint.setDither(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(f2);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeJoin(Paint.Join.ROUND);
    }

    @DexIgnore
    public final void a(Canvas canvas, float f2, float f3, Bitmap bitmap) {
        RectF a2 = a(this.b, (float) this.F);
        Paint paint = this.x;
        a(canvas, a2, f2, f3, bitmap, paint, paint);
    }

    @DexIgnore
    public final void a(Canvas canvas, RectF rectF, float f2, float f3, Bitmap bitmap, Paint paint, Paint paint2) {
        RectF rectF2 = rectF;
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        float strokeWidth = paint.getStrokeWidth();
        float f4 = rectF2.top;
        float f5 = (rectF2.left + rectF2.right) - ((float) width);
        float f6 = (float) 2;
        float f7 = f5 / f6;
        float f8 = (float) height;
        if (f8 >= strokeWidth) {
            rectF2 = a(rectF2, (f8 - strokeWidth) / f6);
        }
        if (f3 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            a(canvas, rectF2, true, 270.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, paint);
            a(canvas, bitmap, f4, f7);
            return;
        }
        if (f2 > f3) {
            a(canvas, rectF2, false, 270.0f, 360.0f, paint);
            a(canvas, a(rectF2, strokeWidth + ((float) 1)), false, 270.0f, Math.min(Math.min((f2 - f3) / f3, 1.0f) * 360.0f, 360.0f), paint2);
        } else {
            float min = Math.min((f2 / f3) * 360.0f, 360.0f);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = T;
            local.d(str, "sweepAngle: " + min);
            a(canvas, rectF2, true, 270.0f, min, paint);
        }
        a(canvas, bitmap, f4, f7);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DashboardVisualizationRings(Context context) {
        this(context, null, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DashboardVisualizationRings(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, RectF rectF, boolean z2, float f2, float f3, Paint paint) {
        if (z2) {
            float centerX = rectF.centerX();
            float centerY = rectF.centerY();
            float width = (rectF.width() - ((float) this.E)) / ((float) 2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = T;
            local.d(str, "centerX: " + centerX + ", centerY: " + centerY + ", radius: " + width);
            canvas.drawCircle(centerX, centerY, width, this.t);
        }
        canvas.drawArc(a(rectF, paint.getStrokeWidth() / ((float) 2)), f2, f3, false, paint);
    }

    @DexIgnore
    public final void a(Canvas canvas, Bitmap bitmap, float f2, float f3) {
        canvas.drawBitmap(bitmap, f3, f2, this.u);
    }

    @DexIgnore
    public final RectF a(RectF rectF, float f2) {
        return new RectF(rectF.left + f2, rectF.top + f2, rectF.right - f2, rectF.bottom - f2);
    }
}
