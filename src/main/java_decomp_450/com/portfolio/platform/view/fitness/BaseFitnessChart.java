package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import com.fossil.ee7;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseFitnessChart extends View implements ViewTreeObserver.OnGlobalLayoutListener {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public TypedArray a;
    @DexIgnore
    public Bitmap b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
        String simpleName = BaseFitnessChart.class.getSimpleName();
        ee7.a((Object) simpleName, "BaseFitnessChart::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseFitnessChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
    }

    @DexIgnore
    public final void a() {
        Bitmap bitmap = this.b;
        if (bitmap != null) {
            if (bitmap == null) {
                ee7.d("mStarBitmap");
                throw null;
            } else if (!bitmap.isRecycled()) {
                return;
            }
        }
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), getStarIconResId());
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeResource, getStarSizeInPx(), getStarSizeInPx(), false);
        ee7.a((Object) createScaledBitmap, "Bitmap.createScaledBitma\u2026nPx, starSizeInPx, false)");
        this.b = createScaledBitmap;
        if (createScaledBitmap == null) {
            ee7.d("mStarBitmap");
            throw null;
        } else if (!ee7.a(createScaledBitmap, decodeResource)) {
            decodeResource.recycle();
        }
    }

    @DexIgnore
    public final void b() {
        Bitmap bitmap = this.b;
        if (bitmap == null) {
            return;
        }
        if (bitmap != null) {
            bitmap.recycle();
        } else {
            ee7.d("mStarBitmap");
            throw null;
        }
    }

    @DexIgnore
    public final TypedArray getMTypedArray() {
        return this.a;
    }

    @DexIgnore
    public abstract int getStarIconResId();

    @DexIgnore
    public abstract int getStarSizeInPx();

    @DexIgnore
    public final Bitmap getStartBitmap() {
        if (this.b == null) {
            a();
        }
        Bitmap bitmap = this.b;
        if (bitmap != null) {
            return bitmap;
        }
        ee7.d("mStarBitmap");
        throw null;
    }

    @DexIgnore
    public void onAttachedToWindow() {
        FLogger.INSTANCE.getLocal().d(c, "onAttachedToWindow, initStartBitmap");
        super.onAttachedToWindow();
        a();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        FLogger.INSTANCE.getLocal().d(c, "onDetachedFromWindow, recycleStarBitmap");
        super.onDetachedFromWindow();
        b();
    }

    @DexIgnore
    public final void setMTypedArray(TypedArray typedArray) {
        this.a = typedArray;
    }
}
