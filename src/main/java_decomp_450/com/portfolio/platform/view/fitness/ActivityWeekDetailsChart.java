package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ae7;
import com.fossil.c7;
import com.fossil.ee7;
import com.fossil.fe7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nh7;
import com.fossil.pl4;
import com.fossil.qe7;
import com.fossil.r87;
import com.fossil.se7;
import com.fossil.tw6;
import com.fossil.v6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityWeekDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String E;
    @DexIgnore
    public int A;
    @DexIgnore
    public /* final */ ArrayList<r87<Float, Float>> B;
    @DexIgnore
    public float C;
    @DexIgnore
    public float D;
    @DexIgnore
    public /* final */ String[] d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ Paint q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements kd7<Integer, List<? extends Integer>, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ qe7 $bottomTextHeight;
        @DexIgnore
        public /* final */ /* synthetic */ se7 $charactersCenterXList;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(qe7 qe7, se7 se7) {
            super(2);
            this.$bottomTextHeight = qe7;
            this.$charactersCenterXList = se7;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public /* bridge */ /* synthetic */ i97 invoke(Integer num, List<? extends Integer> list) {
            invoke(num.intValue(), (List<Integer>) list);
            return i97.a;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<java.lang.Integer> */
        /* JADX WARN: Multi-variable type inference failed */
        public final void invoke(int i, List<Integer> list) {
            ee7.b(list, "list");
            this.$bottomTextHeight.element = i;
            this.$charactersCenterXList.element = list;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = ActivityWeekDetailsChart.class.getSimpleName();
        ee7.a((Object) simpleName, "ActivityWeekDetailsChart::class.java.simpleName");
        E = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityWeekDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        String[] stringArray = context.getResources().getStringArray(2130903040);
        ee7.a((Object) stringArray, "context.resources.getStr\u2026ay.days_of_week_alphabet)");
        this.d = stringArray;
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = context.getResources().getDimensionPixelSize(2131165384);
        this.w = context.getResources().getDimensionPixelSize(2131165384);
        this.x = context.getResources().getDimensionPixelSize(2131165372);
        this.y = context.getResources().getDimensionPixelSize(2131165372);
        this.z = context.getResources().getDimensionPixelSize(2131165419);
        this.A = 4;
        this.B = new ArrayList<>();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, pl4.ActivityWeekDetailsChart, 0, 0));
        }
        int a2 = v6.a(context, 2131099831);
        TypedArray mTypedArray = getMTypedArray();
        this.e = mTypedArray != null ? mTypedArray.getColor(3, a2) : a2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.f = mTypedArray2 != null ? mTypedArray2.getColor(2, a2) : a2;
        TypedArray mTypedArray3 = getMTypedArray();
        this.g = mTypedArray3 != null ? mTypedArray3.getColor(1, a2) : a2;
        TypedArray mTypedArray4 = getMTypedArray();
        this.h = mTypedArray4 != null ? mTypedArray4.getColor(0, a2) : a2;
        TypedArray mTypedArray5 = getMTypedArray();
        this.i = mTypedArray5 != null ? mTypedArray5.getColor(4, a2) : a2;
        TypedArray mTypedArray6 = getMTypedArray();
        this.p = mTypedArray6 != null ? mTypedArray6.getDimensionPixelSize(6, 40) : 40;
        TypedArray mTypedArray7 = getMTypedArray();
        this.j = mTypedArray7 != null ? mTypedArray7.getResourceId(5, 2131296261) : 2131296261;
        TypedArray mTypedArray8 = getMTypedArray();
        if (mTypedArray8 != null) {
            mTypedArray8.recycle();
        }
    }

    @DexIgnore
    private final float getMChartMax() {
        return Math.max(getMMaxGoal(), getMMaxSleepMinutes());
    }

    @DexIgnore
    private final float getMMaxGoal() {
        T t2;
        Float f2;
        Iterator<T> it = this.B.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (!it.hasNext()) {
                t2 = next;
            } else {
                float floatValue = ((Number) next.getSecond()).floatValue();
                do {
                    T next2 = it.next();
                    float floatValue2 = ((Number) next2.getSecond()).floatValue();
                    if (Float.compare(floatValue, floatValue2) < 0) {
                        next = next2;
                        floatValue = floatValue2;
                    }
                } while (it.hasNext());
            }
            t2 = next;
        }
        T t3 = t2;
        if (t3 == null || (f2 = (Float) t3.getSecond()) == null) {
            return this.C;
        }
        return f2.floatValue();
    }

    @DexIgnore
    private final float getMMaxSleepMinutes() {
        T t2;
        Float f2;
        Iterator<T> it = this.B.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (!it.hasNext()) {
                t2 = next;
            } else {
                float floatValue = ((Number) next.getFirst()).floatValue();
                do {
                    T next2 = it.next();
                    float floatValue2 = ((Number) next2.getFirst()).floatValue();
                    if (Float.compare(floatValue, floatValue2) < 0) {
                        next = next2;
                        floatValue = floatValue2;
                    }
                } while (it.hasNext());
            }
            t2 = next;
        }
        T t3 = t2;
        if (t3 == null || (f2 = (Float) t3.getFirst()) == null) {
            return this.D;
        }
        return f2.floatValue();
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, kd7<? super Integer, ? super List<Integer>, i97> kd7) {
        int length = this.d.length;
        Rect rect = new Rect();
        String str = this.d[0];
        ae7 ae7 = ae7.a;
        Paint paint = this.s;
        ee7.a((Object) str, "sundayCharacter");
        paint.getTextBounds(str, 0, nh7.c(str) > 0 ? nh7.c((CharSequence) str) : 1, rect);
        float measureText = this.s.measureText(str);
        float height = (float) rect.height();
        float f2 = (((float) (i2 - (this.w * 2))) - (((float) length) * measureText)) / ((float) (length - 1));
        float f3 = (float) 2;
        float height2 = ((float) getHeight()) - (height / f3);
        float f4 = (float) this.w;
        LinkedList linkedList = new LinkedList();
        for (String str2 : this.d) {
            canvas.drawText(str2, f4, height2, this.s);
            linkedList.add(Integer.valueOf((int) ((measureText / f3) + f4)));
            f4 += measureText + f2;
        }
        kd7.invoke(Integer.valueOf((int) height), linkedList);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(-1);
            int width = (getWidth() - getStartBitmap().getWidth()) - (this.z * 2);
            qe7 qe7 = new qe7();
            qe7.element = 0;
            se7 se7 = new se7();
            se7.element = null;
            a(canvas, width, new b(qe7, se7));
            int height = (getHeight() - qe7.element) - this.x;
            T t2 = se7.element;
            if (t2 != null) {
                a(canvas, t2, height);
                a(canvas, t2, width, height, height - 4);
            }
            a(canvas, 0, height);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.v;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.q.setColor(this.e);
        float f2 = (float) 4;
        this.q.setStrokeWidth(f2);
        this.s.setColor(this.i);
        this.s.setStyle(Paint.Style.FILL);
        this.s.setTextSize((float) this.p);
        this.s.setTypeface(c7.a(getContext(), this.j));
        this.r.setColor(this.f);
        this.r.setStyle(Paint.Style.STROKE);
        this.r.setStrokeWidth(f2);
        this.r.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.t.setColor(this.g);
        this.t.setStrokeWidth((float) this.y);
        this.t.setStyle(Paint.Style.FILL);
        this.u.setColor(this.h);
        this.u.setStrokeWidth((float) this.y);
        this.u.setStyle(Paint.Style.FILL);
        this.A = getStartBitmap().getHeight() / 2;
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.q);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.q);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3, int i4) {
        if ((!this.B.isEmpty()) && this.B.size() > 1) {
            float mChartMax = getMChartMax();
            if (mChartMax > ((float) 0)) {
                Path path = new Path();
                float height = (float) getStartBitmap().getHeight();
                int size = list.size();
                int i5 = 1;
                while (i5 < size) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = E;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Previous goal: ");
                    int i6 = i5 - 1;
                    sb.append(this.B.get(i6).getSecond().floatValue());
                    sb.append(", current goal: ");
                    sb.append(this.B.get(i5).getSecond().floatValue());
                    sb.append(", chart max: ");
                    sb.append(mChartMax);
                    local.d(str, sb.toString());
                    float intValue = (float) list.get(i5).intValue();
                    float f2 = (float) i3;
                    float f3 = (float) i4;
                    float max = Math.max(Math.min((1.0f - (this.B.get(i5).getSecond().floatValue() / mChartMax)) * f2, f3), (float) this.A);
                    float intValue2 = (float) list.get(i6).intValue();
                    float max2 = Math.max(Math.min((1.0f - (this.B.get(i6).getSecond().floatValue() / mChartMax)) * f2, f3), (float) this.A);
                    if (max == max2) {
                        path.moveTo(intValue2, max2);
                        if (i5 == list.size() - 1) {
                            path.lineTo((float) (i2 + this.z), max);
                        } else {
                            path.lineTo(intValue, max);
                        }
                        canvas.drawPath(path, this.r);
                    } else {
                        path.moveTo(intValue2, max2);
                        path.lineTo(intValue, max2);
                        canvas.drawPath(path, this.r);
                        path.moveTo(intValue, max2);
                        path.lineTo(intValue, max);
                        canvas.drawPath(path, this.r);
                        if (i5 == list.size() - 1) {
                            path.moveTo(intValue, max);
                            path.lineTo((float) (i2 + this.z), max);
                            canvas.drawPath(path, this.r);
                        }
                    }
                    i5++;
                    height = max;
                }
                canvas.drawBitmap(getStartBitmap(), (float) (i2 + this.z), height - ((float) (getStartBitmap().getHeight() / 2)), this.s);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityWeekDetailsChart(Context context) {
        this(context, null, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityWeekDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = E;
        local.d(str, "drawBars: " + list.size());
        if (!this.B.isEmpty()) {
            float mChartMax = getMChartMax();
            int size = list.size();
            for (int i3 = 0; i3 < size; i3++) {
                int intValue = list.get(i3).intValue();
                r87<Float, Float> r87 = this.B.get(i3);
                ee7.a((Object) r87, "mDataList[index]");
                float floatValue = r87.getFirst().floatValue() / mChartMax;
                float f2 = (float) i2;
                int i4 = intValue - (this.y / 2);
                tw6.c(canvas, new RectF((float) i4, Math.max(f2 - (floatValue * f2), (float) this.A), (float) (i4 + this.y), f2), floatValue < 1.0f ? this.t : this.u, ((float) this.y) / ((float) 3));
            }
        }
    }
}
