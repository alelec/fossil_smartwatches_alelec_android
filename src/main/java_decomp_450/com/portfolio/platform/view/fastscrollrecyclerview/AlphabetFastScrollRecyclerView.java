package com.portfolio.platform.view.fastscrollrecyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.hz6;
import com.fossil.pl4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class AlphabetFastScrollRecyclerView extends RecyclerView {
    @DexIgnore
    public hz6 a; // = null;
    @DexIgnore
    public GestureDetector b; // = null;
    @DexIgnore
    public boolean c; // = true;
    @DexIgnore
    public int d; // = 12;
    @DexIgnore
    public float e; // = 20.0f;
    @DexIgnore
    public float f; // = 5.0f;
    @DexIgnore
    public int g; // = 5;
    @DexIgnore
    public int h; // = 5;
    @DexIgnore
    public float i; // = 0.6f;
    @DexIgnore
    public int j; // = -16777216;
    @DexIgnore
    public int p; // = -1;
    @DexIgnore
    public int q; // = -16777216;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            return super.onFling(motionEvent, motionEvent2, f, f2);
        }
    }

    @DexIgnore
    public AlphabetFastScrollRecyclerView(Context context) {
        super(context);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes;
        if (!(attributeSet == null || (obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.AlphabetFastScrollRecyclerView, 0, 0)) == null)) {
            try {
                this.d = obtainStyledAttributes.getInt(8, this.d);
                this.e = obtainStyledAttributes.getFloat(10, this.e);
                this.f = obtainStyledAttributes.getFloat(9, this.f);
                this.g = obtainStyledAttributes.getInt(11, this.g);
                this.h = obtainStyledAttributes.getInt(2, this.h);
                this.i = obtainStyledAttributes.getFloat(7, this.i);
                if (obtainStyledAttributes.hasValue(0)) {
                    this.j = Color.parseColor(obtainStyledAttributes.getString(0));
                }
                if (obtainStyledAttributes.hasValue(5)) {
                    this.p = Color.parseColor(obtainStyledAttributes.getString(5));
                }
                if (obtainStyledAttributes.hasValue(3)) {
                    this.q = Color.parseColor(obtainStyledAttributes.getString(3));
                }
                if (obtainStyledAttributes.hasValue(1)) {
                    this.j = obtainStyledAttributes.getColor(1, this.j);
                }
                if (obtainStyledAttributes.hasValue(6)) {
                    this.p = obtainStyledAttributes.getColor(6, this.p);
                }
                if (obtainStyledAttributes.hasValue(4)) {
                    this.q = obtainStyledAttributes.getColor(3, this.q);
                }
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        this.a = new hz6(context, this);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView
    public void draw(Canvas canvas) {
        super.draw(canvas);
        hz6 hz6 = this.a;
        if (hz6 != null) {
            hz6.a(canvas);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        hz6 hz6;
        return (this.c && (hz6 = this.a) != null && hz6.a(motionEvent.getX(), motionEvent.getY())) || super.onInterceptTouchEvent(motionEvent);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        hz6 hz6 = this.a;
        if (hz6 != null) {
            hz6.a(i2, i3, i4, i5);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.c) {
            hz6 hz6 = this.a;
            if (hz6 != null && hz6.a(motionEvent)) {
                return true;
            }
            if (this.b == null) {
                this.b = new GestureDetector(getContext(), new a());
            }
            this.b.onTouchEvent(motionEvent);
        }
        return super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView
    public void setAdapter(RecyclerView.g gVar) {
        super.setAdapter(gVar);
        hz6 hz6 = this.a;
        if (hz6 != null) {
            hz6.a(gVar);
        }
    }

    @DexIgnore
    public void setIndexBarColor(String str) {
        this.a.a(Color.parseColor(str));
    }

    @DexIgnore
    public void setIndexBarCornerRadius(int i2) {
        this.a.b(i2);
    }

    @DexIgnore
    public void setIndexBarHighLateTextVisibility(boolean z) {
        this.a.a(z);
    }

    @DexIgnore
    public void setIndexBarTextColor(String str) {
        this.a.d(Color.parseColor(str));
    }

    @DexIgnore
    public void setIndexBarTransparentValue(float f2) {
        this.a.c(f2);
    }

    @DexIgnore
    public void setIndexBarVisibility(boolean z) {
        this.a.b(z);
        this.c = z;
    }

    @DexIgnore
    public void setIndexTextSize(int i2) {
        this.a.e(i2);
    }

    @DexIgnore
    public void setIndexbarHighLateTextColor(String str) {
        this.a.c(Color.parseColor(str));
    }

    @DexIgnore
    public void setIndexbarMargin(float f2) {
        this.a.d(f2);
    }

    @DexIgnore
    public void setIndexbarWidth(float f2) {
        this.a.e(f2);
    }

    @DexIgnore
    public void setPreviewPadding(int i2) {
        this.a.f(i2);
    }

    @DexIgnore
    public void setPreviewVisibility(boolean z) {
        this.a.c(z);
    }

    @DexIgnore
    public void setTypeface(Typeface typeface) {
        this.a.a(typeface);
    }

    @DexIgnore
    public void setIndexBarColor(int i2) {
        this.a.a(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    public void setIndexBarTextColor(int i2) {
        this.a.d(i2);
    }

    @DexIgnore
    public void setIndexbarHighLateTextColor(int i2) {
        this.a.c(i2);
    }

    @DexIgnore
    public AlphabetFastScrollRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    @DexIgnore
    public AlphabetFastScrollRecyclerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context, attributeSet);
    }
}
