package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageButton;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.pl4;
import com.fossil.x87;
import com.portfolio.platform.data.model.Explore;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleImageButton extends AppCompatImageButton {
    @DexIgnore
    public String c; // = "#FFFFFF";
    @DexIgnore
    public String d; // = "#FFFFFF";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleImageButton(Context context) {
        super(context);
        ee7.b(context, "context");
        a(null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, pl4.FlexibleImageButton);
            String string = obtainStyledAttributes.getString(0);
            if (string == null) {
                string = Explore.COLUMN_BACKGROUND;
            }
            ee7.a((Object) string, "styledAttrs.getString(R.\u2026          ?: \"background\"");
            String string2 = obtainStyledAttributes.getString(1);
            if (string2 == null) {
                string2 = "primaryText";
            }
            ee7.a((Object) string2, "styledAttrs.getString(R.\u2026         ?: \"primaryText\"");
            String b = eh5.l.a().b(string);
            String str = "#FFFFFF";
            if (b == null) {
                b = str;
            }
            this.c = b;
            String b2 = eh5.l.a().b(string2);
            if (b2 != null) {
                str = b2;
            }
            this.d = str;
            a();
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatImageButton
    public void setImageDrawable(Drawable drawable) {
        if (drawable != null) {
            drawable.setAutoMirrored(true);
            super.setImageDrawable(drawable);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleImageButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleImageButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    public final void a() {
        if (this.c != null) {
            Drawable background = getBackground();
            if (background instanceof ShapeDrawable) {
                Drawable background2 = getBackground();
                if (background2 != null) {
                    Paint paint = ((ShapeDrawable) background2).getPaint();
                    ee7.a((Object) paint, "shapeDrawable.paint");
                    paint.setColor(Color.parseColor(this.c));
                } else {
                    throw new x87("null cannot be cast to non-null type android.graphics.drawable.ShapeDrawable");
                }
            } else if (background instanceof GradientDrawable) {
                Drawable background3 = getBackground();
                if (background3 != null) {
                    ((GradientDrawable) background3).setColor(Color.parseColor(this.c));
                } else {
                    throw new x87("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
                }
            } else if (background instanceof ColorDrawable) {
                Drawable background4 = getBackground();
                if (background4 != null) {
                    ((ColorDrawable) background4).setColor(Color.parseColor(this.c));
                } else {
                    throw new x87("null cannot be cast to non-null type android.graphics.drawable.ColorDrawable");
                }
            } else if (background instanceof StateListDrawable) {
                Drawable background5 = getBackground();
                if (background5 != null) {
                    ((StateListDrawable) background5).setColorFilter(Color.parseColor(this.c), PorterDuff.Mode.SRC_ATOP);
                } else {
                    throw new x87("null cannot be cast to non-null type android.graphics.drawable.StateListDrawable");
                }
            }
        }
        String str = this.d;
        if (str != null) {
            getDrawable().setColorFilter(Color.parseColor(str), PorterDuff.Mode.SRC_ATOP);
        }
    }
}
