package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ky6;
import com.fossil.pl4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ColorPickerView extends View {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public String B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public Rect F;
    @DexIgnore
    public Rect G;
    @DexIgnore
    public Rect H;
    @DexIgnore
    public Rect I;
    @DexIgnore
    public Point J;
    @DexIgnore
    public c K;
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Paint g;
    @DexIgnore
    public Paint h;
    @DexIgnore
    public Paint i;
    @DexIgnore
    public Paint j;
    @DexIgnore
    public Paint p;
    @DexIgnore
    public Paint q;
    @DexIgnore
    public Shader r;
    @DexIgnore
    public Shader s;
    @DexIgnore
    public Shader t;
    @DexIgnore
    public b u;
    @DexIgnore
    public b v;
    @DexIgnore
    public int w;
    @DexIgnore
    public float x;
    @DexIgnore
    public float y;
    @DexIgnore
    public float z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public Canvas a;
        @DexIgnore
        public Bitmap b;
        @DexIgnore
        public float c;

        @DexIgnore
        public b() {
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void m(int i);
    }

    @DexIgnore
    public ColorPickerView(Context context) {
        this(context, null);
    }

    @DexIgnore
    private int getPreferredHeight() {
        int a2 = ky6.a(getContext(), 200.0f);
        return this.A ? a2 + this.c + this.b : a2;
    }

    @DexIgnore
    private int getPreferredWidth() {
        return ky6.a(getContext(), 200.0f) + this.a + this.c;
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, pl4.ColorPickerView);
        this.A = obtainStyledAttributes.getBoolean(1, false);
        this.C = obtainStyledAttributes.getColor(3, -4342339);
        this.D = obtainStyledAttributes.getColor(2, -9539986);
        obtainStyledAttributes.recycle();
        a(context);
        this.a = ky6.a(getContext(), 30.0f);
        this.b = ky6.a(getContext(), 20.0f);
        this.c = ky6.a(getContext(), 10.0f);
        this.d = ky6.a(getContext(), 5.0f);
        this.f = ky6.a(getContext(), 4.0f);
        this.e = ky6.a(getContext(), 2.0f);
        this.E = getResources().getDimensionPixelSize(2131165316);
        a();
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        Rect rect = this.H;
        this.q.setColor(this.D);
        canvas.drawRect((float) (rect.left - 1), (float) (rect.top - 1), (float) (rect.right + 1), (float) (rect.bottom + 1), this.q);
        if (this.v == null) {
            b bVar = new b();
            this.v = bVar;
            bVar.b = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            this.v.a = new Canvas(this.v.b);
            int height = (int) (((float) rect.height()) + 0.5f);
            int[] iArr = new int[height];
            float f2 = 360.0f;
            for (int i2 = 0; i2 < height; i2++) {
                iArr[i2] = Color.HSVToColor(new float[]{f2, 1.0f, 1.0f});
                f2 -= 360.0f / ((float) height);
            }
            Paint paint = new Paint();
            paint.setStrokeWidth(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            for (int i3 = 0; i3 < height; i3++) {
                paint.setColor(iArr[i3]);
                b bVar2 = this.v;
                float f3 = (float) i3;
                bVar2.a.drawLine(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3, (float) bVar2.b.getWidth(), f3, paint);
            }
        }
        canvas.drawBitmap(this.v.b, (Rect) null, rect, (Paint) null);
        Point a2 = a(this.x);
        RectF rectF = new RectF();
        int i4 = rect.left;
        int i5 = this.e;
        rectF.left = (float) (i4 - i5);
        rectF.right = (float) (rect.right + i5);
        int i6 = a2.y;
        int i7 = this.f;
        rectF.top = (float) (i6 - (i7 / 2));
        rectF.bottom = (float) (i6 + (i7 / 2));
        canvas.drawRoundRect(rectF, 2.0f, 2.0f, this.p);
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        Rect rect = this.G;
        this.q.setColor(this.D);
        Rect rect2 = this.F;
        canvas.drawRect((float) rect2.left, (float) rect2.top, (float) (rect.right + 1), (float) (rect.bottom + 1), this.q);
        if (this.r == null) {
            int i2 = rect.left;
            this.r = new LinearGradient((float) i2, (float) rect.top, (float) i2, (float) rect.bottom, -1, -16777216, Shader.TileMode.CLAMP);
        }
        b bVar = this.u;
        if (bVar == null || bVar.c != this.x) {
            if (this.u == null) {
                this.u = new b();
            }
            b bVar2 = this.u;
            if (bVar2.b == null) {
                bVar2.b = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            }
            b bVar3 = this.u;
            if (bVar3.a == null) {
                bVar3.a = new Canvas(this.u.b);
            }
            int HSVToColor = Color.HSVToColor(new float[]{this.x, 1.0f, 1.0f});
            float f2 = (float) rect.left;
            int i3 = rect.top;
            this.s = new LinearGradient(f2, (float) i3, (float) rect.right, (float) i3, -1, HSVToColor, Shader.TileMode.CLAMP);
            this.g.setShader(new ComposeShader(this.r, this.s, PorterDuff.Mode.MULTIPLY));
            b bVar4 = this.u;
            bVar4.a.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) bVar4.b.getWidth(), (float) this.u.b.getHeight(), this.g);
            this.u.c = this.x;
        }
        canvas.drawBitmap(this.u.b, (Rect) null, rect, (Paint) null);
        Point b2 = b(this.y, this.z);
        this.h.setColor(-16777216);
        canvas.drawCircle((float) b2.x, (float) b2.y, (float) (this.d - ky6.a(getContext(), 1.0f)), this.h);
        this.h.setColor(-2236963);
        canvas.drawCircle((float) b2.x, (float) b2.y, (float) this.d, this.h);
    }

    @DexIgnore
    public final void d() {
        Rect rect = this.F;
        int i2 = rect.left + 1;
        int i3 = rect.top + 1;
        int i4 = rect.bottom - 1;
        int i5 = this.c;
        int i6 = ((rect.right - 1) - i5) - this.a;
        if (this.A) {
            i4 -= this.b + i5;
        }
        this.G = new Rect(i2, i3, i6, i4);
    }

    @DexIgnore
    public String getAlphaSliderText() {
        return this.B;
    }

    @DexIgnore
    public int getBorderColor() {
        return this.D;
    }

    @DexIgnore
    public int getColor() {
        return Color.HSVToColor(this.w, new float[]{this.x, this.y, this.z});
    }

    @DexIgnore
    public int getPaddingBottom() {
        return Math.max(super.getPaddingBottom(), this.E);
    }

    @DexIgnore
    public int getPaddingLeft() {
        return Math.max(super.getPaddingLeft(), this.E);
    }

    @DexIgnore
    public int getPaddingRight() {
        return Math.max(super.getPaddingRight(), this.E);
    }

    @DexIgnore
    public int getPaddingTop() {
        return Math.max(super.getPaddingTop(), this.E);
    }

    @DexIgnore
    public int getSliderTrackerColor() {
        return this.C;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (this.F.width() > 0 && this.F.height() > 0) {
            c(canvas);
            b(canvas);
            a(canvas);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0058, code lost:
        if (r0 != false) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0087, code lost:
        if (r1 > r6) goto L_0x0089;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r6, int r7) {
        /*
            r5 = this;
            int r0 = android.view.View.MeasureSpec.getMode(r6)
            int r1 = android.view.View.MeasureSpec.getMode(r7)
            int r6 = android.view.View.MeasureSpec.getSize(r6)
            int r2 = r5.getPaddingLeft()
            int r6 = r6 - r2
            int r2 = r5.getPaddingRight()
            int r6 = r6 - r2
            int r7 = android.view.View.MeasureSpec.getSize(r7)
            int r2 = r5.getPaddingBottom()
            int r7 = r7 - r2
            int r2 = r5.getPaddingTop()
            int r7 = r7 - r2
            r2 = 1073741824(0x40000000, float:2.0)
            if (r0 == r2) goto L_0x005c
            if (r1 != r2) goto L_0x002b
            goto L_0x005c
        L_0x002b:
            int r0 = r5.c
            int r1 = r7 + r0
            int r2 = r5.a
            int r1 = r1 + r2
            int r3 = r6 - r0
            int r3 = r3 - r2
            boolean r2 = r5.A
            if (r2 == 0) goto L_0x0040
            int r2 = r5.b
            int r4 = r0 + r2
            int r1 = r1 - r4
            int r0 = r0 + r2
            int r3 = r3 + r0
        L_0x0040:
            r0 = 1
            r2 = 0
            if (r1 > r6) goto L_0x0046
            r4 = 1
            goto L_0x0047
        L_0x0046:
            r4 = 0
        L_0x0047:
            if (r3 > r7) goto L_0x004a
            goto L_0x004b
        L_0x004a:
            r0 = 0
        L_0x004b:
            if (r4 == 0) goto L_0x0050
            if (r0 == 0) goto L_0x0050
            goto L_0x005a
        L_0x0050:
            if (r0 != 0) goto L_0x0056
            if (r4 == 0) goto L_0x0056
        L_0x0054:
            r6 = r1
            goto L_0x0089
        L_0x0056:
            if (r4 != 0) goto L_0x0089
            if (r0 == 0) goto L_0x0089
        L_0x005a:
            r7 = r3
            goto L_0x0089
        L_0x005c:
            if (r0 != r2) goto L_0x0074
            if (r1 == r2) goto L_0x0074
            int r0 = r5.c
            int r1 = r6 - r0
            int r2 = r5.a
            int r1 = r1 - r2
            boolean r2 = r5.A
            if (r2 == 0) goto L_0x006f
            int r2 = r5.b
            int r0 = r0 + r2
            int r1 = r1 + r0
        L_0x006f:
            if (r1 <= r7) goto L_0x0072
            goto L_0x0089
        L_0x0072:
            r7 = r1
            goto L_0x0089
        L_0x0074:
            if (r1 != r2) goto L_0x0089
            if (r0 == r2) goto L_0x0089
            int r0 = r5.c
            int r1 = r7 + r0
            int r2 = r5.a
            int r1 = r1 + r2
            boolean r2 = r5.A
            if (r2 == 0) goto L_0x0087
            int r2 = r5.b
            int r0 = r0 + r2
            int r1 = r1 - r0
        L_0x0087:
            if (r1 <= r6) goto L_0x0054
        L_0x0089:
            int r0 = r5.getPaddingLeft()
            int r6 = r6 + r0
            int r0 = r5.getPaddingRight()
            int r6 = r6 + r0
            int r0 = r5.getPaddingTop()
            int r7 = r7 + r0
            int r0 = r5.getPaddingBottom()
            int r7 = r7 + r0
            r5.setMeasuredDimension(r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.ColorPickerView.onMeasure(int, int):void");
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.w = bundle.getInt("alpha");
            this.x = bundle.getFloat("hue");
            this.y = bundle.getFloat("sat");
            this.z = bundle.getFloat("val");
            this.A = bundle.getBoolean("show_alpha");
            this.B = bundle.getString("alpha_text");
            parcelable = bundle.getParcelable("instanceState");
        }
        super.onRestoreInstanceState(parcelable);
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putInt("alpha", this.w);
        bundle.putFloat("hue", this.x);
        bundle.putFloat("sat", this.y);
        bundle.putFloat("val", this.z);
        bundle.putBoolean("show_alpha", this.A);
        bundle.putString("alpha_text", this.B);
        return bundle;
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        Rect rect = new Rect();
        this.F = rect;
        rect.left = getPaddingLeft();
        this.F.right = i2 - getPaddingRight();
        this.F.top = getPaddingTop();
        this.F.bottom = i3 - getPaddingBottom();
        this.r = null;
        this.s = null;
        this.t = null;
        this.u = null;
        this.v = null;
        d();
        c();
        b();
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        int action = motionEvent.getAction();
        if (action == 0) {
            this.J = new Point((int) motionEvent.getX(), (int) motionEvent.getY());
            z2 = a(motionEvent);
        } else if (action == 1) {
            this.J = null;
            z2 = a(motionEvent);
        } else if (action != 2) {
            z2 = false;
        } else {
            z2 = a(motionEvent);
        }
        if (!z2) {
            return super.onTouchEvent(motionEvent);
        }
        c cVar = this.K;
        if (cVar != null) {
            cVar.m(Color.HSVToColor(this.w, new float[]{this.x, this.y, this.z}));
        }
        invalidate();
        return true;
    }

    @DexIgnore
    public void setAlphaSliderText(int i2) {
        setAlphaSliderText(getContext().getString(i2));
    }

    @DexIgnore
    public void setAlphaSliderVisible(boolean z2) {
        if (this.A != z2) {
            this.A = z2;
            this.r = null;
            this.s = null;
            this.t = null;
            this.v = null;
            this.u = null;
            requestLayout();
        }
    }

    @DexIgnore
    public void setBorderColor(int i2) {
        this.D = i2;
        invalidate();
    }

    @DexIgnore
    public void setColor(int i2) {
        a(i2, false);
    }

    @DexIgnore
    public void setOnColorChangedListener(c cVar) {
        this.K = cVar;
    }

    @DexIgnore
    public void setSliderTrackerColor(int i2) {
        this.C = i2;
        this.p.setColor(i2);
        invalidate();
    }

    @DexIgnore
    public ColorPickerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public ColorPickerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.w = 255;
        this.x = 360.0f;
        this.y = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.z = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.A = false;
        this.B = null;
        this.C = -4342339;
        this.D = -9539986;
        this.J = null;
        a(context, attributeSet);
    }

    @DexIgnore
    public void setAlphaSliderText(String str) {
        this.B = str;
        invalidate();
    }

    @DexIgnore
    public final void a(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new TypedValue().data, new int[]{16842808});
        if (this.D == -9539986) {
            this.D = obtainStyledAttributes.getColor(0, -9539986);
        }
        if (this.C == -4342339) {
            this.C = obtainStyledAttributes.getColor(0, -4342339);
        }
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public final Point b(float f2, float f3) {
        Rect rect = this.G;
        Point point = new Point();
        point.x = (int) ((f2 * ((float) rect.width())) + ((float) rect.left));
        point.y = (int) (((1.0f - f3) * ((float) rect.height())) + ((float) rect.top));
        return point;
    }

    @DexIgnore
    public final void a() {
        this.g = new Paint();
        this.h = new Paint();
        this.p = new Paint();
        this.i = new Paint();
        this.j = new Paint();
        this.q = new Paint();
        this.h.setStyle(Paint.Style.STROKE);
        this.h.setStrokeWidth((float) ky6.a(getContext(), 2.0f));
        this.h.setAntiAlias(true);
        this.p.setColor(this.C);
        this.p.setStyle(Paint.Style.STROKE);
        this.p.setStrokeWidth((float) ky6.a(getContext(), 2.0f));
        this.p.setAntiAlias(true);
        this.j.setColor(-14935012);
        this.j.setTextSize((float) ky6.a(getContext(), 14.0f));
        this.j.setAntiAlias(true);
        this.j.setTextAlign(Paint.Align.CENTER);
        this.j.setFakeBoldText(true);
    }

    @DexIgnore
    public final void c() {
        Rect rect = this.F;
        this.H = new Rect((rect.right - this.a) + 1, rect.top + 1, rect.right - 1, (rect.bottom - 1) - (this.A ? this.c + this.b : 0));
    }

    @DexIgnore
    public final float b(float f2) {
        float f3;
        Rect rect = this.H;
        float height = (float) rect.height();
        int i2 = rect.top;
        if (f2 < ((float) i2)) {
            f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        } else {
            f3 = f2 > ((float) rect.bottom) ? height : f2 - ((float) i2);
        }
        return 360.0f - ((f3 * 360.0f) / height);
    }

    @DexIgnore
    public final int b(int i2) {
        int i3;
        Rect rect = this.I;
        int width = rect.width();
        int i4 = rect.left;
        if (i2 < i4) {
            i3 = 0;
        } else {
            i3 = i2 > rect.right ? width : i2 - i4;
        }
        return 255 - ((i3 * 255) / width);
    }

    @DexIgnore
    public final void b() {
        if (this.A) {
            Rect rect = this.F;
            int i2 = rect.bottom;
            this.I = new Rect(rect.left + 1, (i2 - this.b) + 1, rect.right - 1, i2 - 1);
        }
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        Rect rect;
        if (this.A && (rect = this.I) != null) {
            this.q.setColor(this.D);
            canvas.drawRect((float) (rect.left - 1), (float) (rect.top - 1), (float) (rect.right + 1), (float) (rect.bottom + 1), this.q);
            float[] fArr = {this.x, this.y, this.z};
            int HSVToColor = Color.HSVToColor(fArr);
            int HSVToColor2 = Color.HSVToColor(0, fArr);
            float f2 = (float) rect.left;
            int i2 = rect.top;
            LinearGradient linearGradient = new LinearGradient(f2, (float) i2, (float) rect.right, (float) i2, HSVToColor, HSVToColor2, Shader.TileMode.CLAMP);
            this.t = linearGradient;
            this.i.setShader(linearGradient);
            canvas.drawRect(rect, this.i);
            String str = this.B;
            if (str != null && !str.equals("")) {
                canvas.drawText(this.B, (float) rect.centerX(), (float) (rect.centerY() + ky6.a(getContext(), 4.0f)), this.j);
            }
            Point a2 = a(this.w);
            RectF rectF = new RectF();
            int i3 = a2.x;
            int i4 = this.f;
            rectF.left = (float) (i3 - (i4 / 2));
            rectF.right = (float) (i3 + (i4 / 2));
            int i5 = rect.top;
            int i6 = this.e;
            rectF.top = (float) (i5 - i6);
            rectF.bottom = (float) (rect.bottom + i6);
            canvas.drawRoundRect(rectF, 2.0f, 2.0f, this.p);
        }
    }

    @DexIgnore
    public final Point a(float f2) {
        Rect rect = this.H;
        float height = (float) rect.height();
        Point point = new Point();
        point.y = (int) ((height - ((f2 * height) / 360.0f)) + ((float) rect.top));
        point.x = rect.left;
        return point;
    }

    @DexIgnore
    public final Point a(int i2) {
        Rect rect = this.I;
        float width = (float) rect.width();
        Point point = new Point();
        point.x = (int) ((width - ((((float) i2) * width) / 255.0f)) + ((float) rect.left));
        point.y = rect.top;
        return point;
    }

    @DexIgnore
    public final float[] a(float f2, float f3) {
        float f4;
        Rect rect = this.G;
        float[] fArr = new float[2];
        float width = (float) rect.width();
        float height = (float) rect.height();
        int i2 = rect.left;
        float f5 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (f2 < ((float) i2)) {
            f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        } else {
            f4 = f2 > ((float) rect.right) ? width : f2 - ((float) i2);
        }
        int i3 = rect.top;
        if (f3 >= ((float) i3)) {
            f5 = f3 > ((float) rect.bottom) ? height : f3 - ((float) i3);
        }
        fArr[0] = (1.0f / width) * f4;
        fArr[1] = 1.0f - ((1.0f / height) * f5);
        return fArr;
    }

    @DexIgnore
    public final boolean a(MotionEvent motionEvent) {
        Point point = this.J;
        if (point == null) {
            return false;
        }
        int i2 = point.x;
        int i3 = point.y;
        if (this.H.contains(i2, i3)) {
            this.x = b(motionEvent.getY());
        } else if (this.G.contains(i2, i3)) {
            float[] a2 = a(motionEvent.getX(), motionEvent.getY());
            this.y = a2[0];
            this.z = a2[1];
        } else {
            Rect rect = this.I;
            if (rect == null || !rect.contains(i2, i3)) {
                return false;
            }
            this.w = b((int) motionEvent.getX());
        }
        return true;
    }

    @DexIgnore
    public void a(int i2, boolean z2) {
        c cVar;
        int alpha = Color.alpha(i2);
        float[] fArr = new float[3];
        Color.RGBToHSV(Color.red(i2), Color.green(i2), Color.blue(i2), fArr);
        this.w = alpha;
        float f2 = fArr[0];
        this.x = f2;
        float f3 = fArr[1];
        this.y = f3;
        float f4 = fArr[2];
        this.z = f4;
        if (z2 && (cVar = this.K) != null) {
            cVar.m(Color.HSVToColor(alpha, new float[]{f2, f3, f4}));
        }
        invalidate();
    }
}
