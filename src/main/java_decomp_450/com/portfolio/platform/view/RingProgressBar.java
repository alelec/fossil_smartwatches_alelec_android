package com.portfolio.platform.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.pl4;
import com.fossil.v6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingProgressBar extends View {
    @DexIgnore
    public int A;
    @DexIgnore
    public float B;
    @DexIgnore
    public int C;
    @DexIgnore
    public ObjectAnimator D;
    @DexIgnore
    public int E;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ RectF b;
    @DexIgnore
    public /* final */ RectF c;
    @DexIgnore
    public /* final */ RectF d;
    @DexIgnore
    public int e;
    @DexIgnore
    public float f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int p;
    @DexIgnore
    public Paint q;
    @DexIgnore
    public Paint r;
    @DexIgnore
    public Paint s;
    @DexIgnore
    public Paint t;
    @DexIgnore
    public Paint u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public enum b {
        ACTIVE_TIME,
        STEPS,
        CALORIES,
        SLEEP,
        GOAL
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;

        @DexIgnore
        public c(RingProgressBar ringProgressBar) {
            this.a = ringProgressBar;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.a.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public d(RingProgressBar ringProgressBar, int i) {
            this.a = ringProgressBar;
            this.b = i;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            ee7.b(animator, "animator");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ee7.b(animator, "animator");
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            ee7.b(animator, "animator");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            ee7.b(animator, "animator");
            this.a.setMReachAnimateState$app_fossilRelease(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;

        @DexIgnore
        public e(RingProgressBar ringProgressBar) {
            this.a = ringProgressBar;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            RingProgressBar ringProgressBar = this.a;
            float f = (float) 2;
            ringProgressBar.invalidate((ringProgressBar.getWidth() / 2) - ((int) ((((float) this.a.y) * 1.15f) / f)), (int) ((this.a.f / 2.0f) + (this.a.f * 0.14999998f)), (this.a.getWidth() / 2) + ((int) ((((float) this.a.y) * 1.15f) / f)), (int) ((this.a.f / 2.0f) + (this.a.f * 0.14999998f) + (((float) this.a.y) * 1.15f)));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(RingProgressBar ringProgressBar) {
            this.a = ringProgressBar;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            ee7.b(animator, "animator");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ee7.b(animator, "animator");
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            ee7.b(animator, "animator");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            ee7.b(animator, "animator");
            this.a.setMReachAnimateState$app_fossilRelease(1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public g(RingProgressBar ringProgressBar, boolean z) {
            this.a = ringProgressBar;
            this.b = z;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            if (this.a.getMProgress$app_fossilRelease() >= ((float) 1) && this.a.getMReachAnimateState$app_fossilRelease() == 0) {
                if (this.b) {
                    RingProgressBar ringProgressBar = this.a;
                    ringProgressBar.b(ringProgressBar.getMProgressIndex$app_fossilRelease());
                    return;
                }
                this.a.b(0);
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public RingProgressBar(Context context) {
        this(context, null);
        ee7.b(context, "context");
    }

    @DexIgnore
    private final void setIconSize(int i2) {
        this.y = i2;
        invalidate();
    }

    @DexIgnore
    private final void setMaxProgress(float f2) {
        this.B = f2;
        invalidate();
    }

    @DexIgnore
    private final void setProgressBackgroundColor(int i2) {
        this.j = i2;
        b();
    }

    @DexIgnore
    private final void setSpaceWidth(int i2) {
        b();
        c();
    }

    @DexIgnore
    public final void c(int i2, int i3) {
        this.z = i2;
        this.A = i3;
        invalidate();
    }

    @DexIgnore
    public final float getMProgress$app_fossilRelease() {
        return this.h;
    }

    @DexIgnore
    public final int getMProgressIndex$app_fossilRelease() {
        return this.C;
    }

    @DexIgnore
    public final int getMReachAnimateState$app_fossilRelease() {
        return this.E;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        ee7.b(canvas, "canvas");
        canvas.translate(this.v, this.w);
        float f2 = this.h * ((float) 360);
        canvas.drawArc(this.b, 270.0f, -(360.0f - f2), false, this.r);
        canvas.drawArc(this.b, 270.0f, this.h > 1.0f ? 360.0f : f2, false, this.q);
        float f3 = this.h;
        if (f3 > 1.0f) {
            canvas.drawArc(this.c, 270.0f, f3 > 2.0f ? 360.0f : f2 - 360.0f, false, this.q);
        }
        a(canvas, f2);
        canvas.translate(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, -this.w);
        a(canvas);
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (i2 <= i3) {
            i2 = i3;
        }
        int defaultSize = View.getDefaultSize(getSuggestedMinimumWidth() + getPaddingLeft() + getPaddingRight(), i2);
        setMeasuredDimension(defaultSize, defaultSize);
        float f2 = ((float) defaultSize) / 2.0f;
        int i4 = this.e;
        float f3 = (float) (i4 + 2);
        this.f = f3;
        float f4 = (f2 - ((float) i4)) - ((((float) this.y) * 1.15f) / ((float) 2));
        float f5 = f4 - f3;
        float f6 = f5 - f3;
        float f7 = -f4;
        this.b.set(f7, f7, f4, f4);
        float f8 = -f5;
        this.c.set(f8, f8, f5, f5);
        float f9 = -f6;
        this.d.set(f9, f9, f6, f6);
        this.v = f2;
        this.w = f2;
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        ee7.b(parcelable, "state");
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            setProgress(bundle.getFloat("mProgress"));
            int i2 = bundle.getInt("progress_color");
            if (i2 != this.p) {
                this.p = i2;
                c();
            }
            int i3 = bundle.getInt("progress_background_color");
            if (i3 != this.j) {
                this.j = i3;
                b();
            }
            super.onRestoreInstanceState(bundle.getParcelable("saved_state"));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("saved_state", super.onSaveInstanceState());
        bundle.putFloat("mProgress", this.h);
        bundle.putInt("progress_color", this.p);
        bundle.putInt("progress_background_color", this.j);
        return bundle;
    }

    @DexIgnore
    public final void setIconSource(int i2) {
        this.x = i2;
        invalidate();
    }

    @DexIgnore
    public final void setMProgress$app_fossilRelease(float f2) {
        this.h = f2;
    }

    @DexIgnore
    public final void setMProgressIndex$app_fossilRelease(int i2) {
        this.C = i2;
    }

    @DexIgnore
    public final void setMReachAnimateState$app_fossilRelease(int i2) {
        this.E = i2;
    }

    @DexIgnore
    @SuppressLint({"AnimatorKeep"})
    public final void setProgress(float f2) {
        if (f2 != this.h) {
            float f3 = this.B;
            if (f2 >= f3) {
                f2 = f3;
            }
            this.h = f2;
            if (!this.g) {
                invalidate();
            }
        }
    }

    @DexIgnore
    public final void setProgressColor(int i2) {
        this.p = i2;
        c();
    }

    @DexIgnore
    public final void setProgressRingColorPreview(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "setProgressRingColorPreview color=" + i2);
        setProgressColor(i2);
    }

    @DexIgnore
    public final void setStrokeWidth(int i2) {
        this.e = i2;
        b();
        c();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public RingProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    public final void a(String str, String str2) {
        ee7.b(str, "progressColorValue");
        ee7.b(str2, "unfilledColorValue");
        String b2 = eh5.l.a().b(str);
        String b3 = eh5.l.a().b(str2);
        if (b2 != null) {
            setProgressColor(Color.parseColor(b2));
        }
        if (b3 != null) {
            setProgressBackgroundColor(Color.parseColor(b3));
        }
    }

    @DexIgnore
    public final int b(int i2, int i3) {
        return Color.argb(i3, Color.red(i2), Color.green(i2), Color.blue(i2));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingProgressBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        this.a = "RingProgressBar";
        this.b = new RectF();
        this.c = new RectF();
        this.d = new RectF();
        this.g = true;
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.RingProgressBar, i2, 0);
        if (obtainStyledAttributes != null) {
            try {
                setProgressColor(obtainStyledAttributes.getColor(7, v6.a(PortfolioApp.g0.c(), 2131099951)));
                setProgressBackgroundColor(obtainStyledAttributes.getColor(6, -16711936));
                setProgress(obtainStyledAttributes.getFloat(5, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                setStrokeWidth((int) obtainStyledAttributes.getDimension(10, (float) 10));
                setSpaceWidth((int) obtainStyledAttributes.getDimension(9, (float) 2));
                setIconSize((int) obtainStyledAttributes.getDimension(2, (float) 60));
                setMaxProgress(obtainStyledAttributes.getFloat(4, 2.0f));
                setIconSource(obtainStyledAttributes.getResourceId(3, 0));
                c(v6.a(context, obtainStyledAttributes.getResourceId(1, 2131099811)), v6.a(context, obtainStyledAttributes.getResourceId(0, 2131099946)));
                this.C = obtainStyledAttributes.getInteger(8, 0);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        b();
        c();
        this.g = false;
    }

    @DexIgnore
    public final void c() {
        a(this.q, this.p, (float) this.e);
        a(this.s, b(this.p, 30), ((float) this.e) * 1.9f);
        a(this.t, b(this.p, 30), ((float) this.e) * 2.7f);
        a(this.u, b(this.p, 30), ((float) this.e) * 3.5f);
        invalidate();
    }

    @DexIgnore
    public final void a(Paint paint, int i2, float f2) {
        paint.setColor(i2);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(f2);
    }

    @DexIgnore
    public final void b() {
        a(this.r, this.j, (float) this.e);
        invalidate();
    }

    @DexIgnore
    public final void b(int i2) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playSequentially(a(170), a(60, 2), a(60, 3), a(120, 4), a(60, 3), a(60, 2), a(60, 0));
        animatorSet.setStartDelay(((long) i2) * ((long) 170));
        animatorSet.start();
    }

    @DexIgnore
    public final void a(Canvas canvas, float f2) {
        Paint[] paintArr;
        int i2 = this.E;
        if (i2 == 2) {
            paintArr = new Paint[]{this.s};
        } else if (i2 != 3) {
            paintArr = i2 != 4 ? new Paint[0] : new Paint[]{this.u, this.t, this.s};
        } else {
            paintArr = new Paint[]{this.t, this.s};
        }
        for (Paint paint : paintArr) {
            canvas.drawArc(this.b, 270.0f, this.h > 1.0f ? 360.0f : f2, false, paint);
        }
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), this.x);
        if (decodeResource != null) {
            int i2 = this.y;
            float f2 = this.f;
            float f3 = (((float) i2) * 0.14999998f) + (f2 / 2.0f);
            if (this.E == 1) {
                i2 = (int) (((float) i2) * 1.15f);
                f3 = (f2 / 2.0f) + (f2 * 0.14999998f);
            }
            Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeResource, i2, i2, false);
            Paint paint = new Paint(1);
            paint.setColorFilter(new PorterDuffColorFilter(this.A, PorterDuff.Mode.SRC_OVER));
            int i3 = this.y;
            float f4 = this.f;
            canvas.drawRect((((float) (-i3)) * 1.15f) / 2.0f, (f4 / 2.0f) + (f4 * 0.14999998f) + 10.0f, (((float) i3) * 1.15f) / 2.0f, ((float) i3) * 2.0f, paint);
            paint.setColorFilter(new PorterDuffColorFilter(this.z, PorterDuff.Mode.SRC_ATOP));
            if (createScaledBitmap != null) {
                canvas.drawBitmap(createScaledBitmap, ((float) (-createScaledBitmap.getWidth())) / 2.0f, f3, paint);
                createScaledBitmap.recycle();
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(float f2, boolean z2) {
        if (f2 != this.i) {
            a();
            a(f2, 1000, z2);
            this.i = f2;
        }
    }

    @DexIgnore
    public final void a(float f2, int i2, boolean z2) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "progress", this.h, f2);
        this.D = ofFloat;
        if (ofFloat != null) {
            ofFloat.setDuration((long) i2);
            ObjectAnimator objectAnimator = this.D;
            if (objectAnimator != null) {
                objectAnimator.reverse();
                ObjectAnimator objectAnimator2 = this.D;
                if (objectAnimator2 != null) {
                    objectAnimator2.addListener(new g(this, z2));
                    ObjectAnimator objectAnimator3 = this.D;
                    if (objectAnimator3 != null) {
                        objectAnimator3.start();
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a() {
        ObjectAnimator objectAnimator = this.D;
        if (objectAnimator == null) {
            return;
        }
        if (objectAnimator != null) {
            objectAnimator.cancel();
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @SuppressLint({"ObjectAnimatorBinding"})
    public final ObjectAnimator a(int i2) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "", 1.0f);
        ee7.a((Object) ofFloat, "scaleAnimator");
        ofFloat.setDuration((long) i2);
        ofFloat.addUpdateListener(new e(this));
        ofFloat.addListener(new f(this));
        return ofFloat;
    }

    @DexIgnore
    @SuppressLint({"ObjectAnimatorBinding"})
    public final ObjectAnimator a(int i2, int i3) {
        ObjectAnimator ofInt = ObjectAnimator.ofInt(this, "", 0);
        ee7.a((Object) ofInt, "gradientAnimator");
        ofInt.setDuration((long) i2);
        ofInt.setStartDelay((long) 33);
        ofInt.addUpdateListener(new c(this));
        ofInt.addListener(new d(this, i3));
        return ofInt;
    }
}
