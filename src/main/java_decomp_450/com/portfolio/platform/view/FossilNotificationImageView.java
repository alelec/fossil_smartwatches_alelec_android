package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import com.fossil.iw;
import com.fossil.yx6;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FossilNotificationImageView extends ViewGroup {
    @DexIgnore
    public static /* final */ String f; // = FossilNotificationImageView.class.getSimpleName();
    @DexIgnore
    public /* final */ Paint a; // = new Paint();
    @DexIgnore
    public /* final */ Paint b; // = new Paint();
    @DexIgnore
    public /* final */ Rect c; // = new Rect();
    @DexIgnore
    public FossilCircleImageView d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public FossilNotificationImageView(Context context) {
        super(context);
        a(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            this.d = new FossilCircleImageView(context, attributeSet);
        } else {
            this.d = new FossilCircleImageView(context);
        }
        addView(this.d);
    }

    @DexIgnore
    public void b() {
        this.d.setBorderColor(PortfolioApp.c0.getResources().getColor(R.color.transparent));
        this.d.setBorderWidth(3);
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        int i;
        super.dispatchDraw(canvas);
        if (!TextUtils.isEmpty(this.d.getHandNumber())) {
            if (this.e) {
                i = getWidth() / 6;
            } else {
                i = getWidth() / 4;
            }
            this.b.setAntiAlias(true);
            this.b.setFilterBitmap(true);
            this.b.setDither(true);
            this.b.setColor(-65536);
            this.b.setStyle(Paint.Style.FILL);
            if (this.e) {
                canvas.drawCircle((float) (getWidth() - (getWidth() / 6)), (float) (getHeight() / 6), (float) (getWidth() / 6), this.b);
            } else {
                canvas.drawCircle((float) (getWidth() / 2), ((float) (getHeight() - i)) - yx6.a(5.0f), (float) i, this.b);
            }
            this.a.setAntiAlias(true);
            this.a.setColor(-1);
            this.a.setStrokeWidth(4.0f);
            this.a.setStyle(Paint.Style.STROKE);
            if (this.e) {
                canvas.drawCircle((float) (getWidth() - (getWidth() / 6)), (float) (getHeight() / 6), (float) (getWidth() / 6), this.a);
            } else {
                canvas.drawCircle((float) (getWidth() / 2), ((float) (getHeight() - i)) - yx6.a(5.0f), (float) i, this.a);
            }
            this.b.setColor(-1);
            this.b.setTextSize((float) i);
            this.b.getTextBounds(this.d.getHandNumber(), 0, this.d.getHandNumber().length(), this.c);
            float measureText = this.b.measureText(this.d.getHandNumber());
            int height = this.c.height();
            if (this.e) {
                canvas.drawText(this.d.getHandNumber(), ((float) (getWidth() - (getWidth() / 6))) - (measureText / 2.0f), (float) ((getHeight() / 6) + (height / 2)), this.b);
            } else {
                canvas.drawText(this.d.getHandNumber(), ((float) (getWidth() / 2)) - (measureText / 2.0f), (((float) (getHeight() - i)) - yx6.a(5.0f)) + ((float) (height / 2)), this.b);
            }
        }
    }

    @DexIgnore
    public FossilCircleImageView getFossilCircleImageView() {
        return this.d;
    }

    @DexIgnore
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        this.d.layout(0, 0, i5, i5);
    }

    @DexIgnore
    public void setDefault(boolean z) {
        this.d.setDefault(z);
    }

    @DexIgnore
    public void setHandNumber(String str) {
        this.d.setHandNumber(str);
    }

    @DexIgnore
    public void setImageDrawable(Drawable drawable) {
        this.d.setImageDrawable(drawable);
    }

    @DexIgnore
    public void setTopRightBadge(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public void a(String str, iw iwVar) {
        this.d.a(str, iwVar);
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2) {
        if (bitmap == null || bitmap2 == null) {
            FLogger.INSTANCE.getLocal().d(f, "Inside. setImageBitmap 2 bitmap, bitmap == null");
        } else {
            this.d.a(bitmap, bitmap2);
        }
    }

    @DexIgnore
    public FossilNotificationImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3) {
        if (bitmap == null || bitmap2 == null || bitmap3 == null) {
            FLogger.INSTANCE.getLocal().d(f, "Inside. setImageBitmap 3 bitmap, bitmap == null");
        } else {
            this.d.a(bitmap, bitmap2, bitmap3);
        }
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, Bitmap bitmap4) {
        if (bitmap == null || bitmap2 == null || bitmap3 == null || bitmap4 == null) {
            FLogger.INSTANCE.getLocal().d(f, "Inside. setImageBitmap 4 bitmap, bitmap == null");
        } else {
            this.d.a(bitmap, bitmap2, bitmap3, bitmap4);
        }
    }

    @DexIgnore
    public void a(int i, iw iwVar) {
        this.d.a(i, iwVar);
    }

    @DexIgnore
    public void a() {
        this.d.setBorderColor(PortfolioApp.c0.getResources().getColor(2131099827));
        this.d.setBorderWidth(3);
    }
}
