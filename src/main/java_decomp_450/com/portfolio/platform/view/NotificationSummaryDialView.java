package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.c50;
import com.fossil.ee7;
import com.fossil.ex;
import com.fossil.hd5;
import com.fossil.iy;
import com.fossil.k40;
import com.fossil.kd5;
import com.fossil.md5;
import com.fossil.pl4;
import com.fossil.py;
import com.fossil.q40;
import com.fossil.r40;
import com.fossil.sw;
import com.fossil.v6;
import com.fossil.vd5;
import com.fossil.wa;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.x87;
import com.fossil.yx6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationSummaryDialView extends ViewGroup {
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public Paint f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int p;
    @DexIgnore
    public ViewGroup.LayoutParams q;
    @DexIgnore
    public SparseArray<Bitmap> r; // = new SparseArray<>();
    @DexIgnore
    public /* final */ Handler s; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public Bitmap t;
    @DexIgnore
    public SparseArray<List<BaseFeatureModel>> u; // = new SparseArray<>();
    @DexIgnore
    public SparseArray<List<BaseFeatureModel>> v; // = new SparseArray<>();
    @DexIgnore
    public b w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSummaryDialView a;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;

        @DexIgnore
        public c(NotificationSummaryDialView notificationSummaryDialView, ArrayList arrayList) {
            this.a = notificationSummaryDialView;
            this.b = arrayList;
        }

        @DexIgnore
        public final void run() {
            this.a.a();
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                AppCompatImageView appCompatImageView = (AppCompatImageView) it.next();
                if (appCompatImageView != null) {
                    try {
                        this.a.addView(appCompatImageView);
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String e2 = NotificationSummaryDialView.x;
                        local.d(e2, "onLoadBitmapAsyncComplete exception " + e);
                    }
                }
            }
            this.a.requestLayout();
            this.a.v.clear();
            if (this.a.u.size() > 0) {
                FLogger.INSTANCE.getLocal().d(NotificationSummaryDialView.x, "onLoadBitmapAsyncComplete pendingList exists, start set again");
                NotificationSummaryDialView notificationSummaryDialView = this.a;
                SparseArray<List<BaseFeatureModel>> clone = notificationSummaryDialView.u.clone();
                ee7.a((Object) clone, "mPendingData.clone()");
                notificationSummaryDialView.setDataAsync(clone);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSummaryDialView a;

        @DexIgnore
        public d(NotificationSummaryDialView notificationSummaryDialView) {
            this.a = notificationSummaryDialView;
        }

        @DexIgnore
        public final void onClick(View view) {
            b c = this.a.w;
            if (c != null) {
                Object tag = view.getTag(123456789);
                if (tag != null) {
                    c.a(((Integer) tag).intValue());
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Int");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements q40<Bitmap> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSummaryDialView a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ AppCompatImageView c;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList d;

        @DexIgnore
        public e(NotificationSummaryDialView notificationSummaryDialView, int i, AppCompatImageView appCompatImageView, ArrayList arrayList) {
            this.a = notificationSummaryDialView;
            this.b = i;
            this.c = appCompatImageView;
            this.d = arrayList;
        }

        @DexIgnore
        @Override // com.fossil.q40
        public boolean a(py pyVar, Object obj, c50<Bitmap> c50, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = NotificationSummaryDialView.x;
            local.d(e, "setDataAsync onLoadFailed " + obj);
            if (this.a.r.get(this.b) != null) {
                this.c.setImageBitmap((Bitmap) this.a.r.get(this.b));
            }
            this.d.add(this.c);
            this.a.a(this.d);
            return false;
        }

        @DexIgnore
        public boolean a(Bitmap bitmap, Object obj, c50<Bitmap> c50, sw swVar, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = NotificationSummaryDialView.x;
            local.d(e, "setDataAsync onResourceReady " + obj);
            if (bitmap != null) {
                this.a.r.put(this.b, bitmap);
                this.c.setImageBitmap(bitmap);
            }
            this.d.add(this.c);
            this.a.a(this.d);
            return false;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = NotificationSummaryDialView.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationSummaryDialView::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationSummaryDialView(Context context) {
        super(context);
        ee7.b(context, "context");
        d();
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        ee7.b(canvas, "canvas");
        FLogger.INSTANCE.getLocal().d(x, "dispatchDraw");
        a(canvas);
        super.dispatchDraw(canvas);
        c();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        this.s.removeCallbacksAndMessages(null);
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        this.p = (Math.min((i4 - i2) - 100, i5 - i3) / 2) - (this.j / 2);
        destroyDrawingCache();
        buildDrawingCache();
        int i6 = 0;
        while (i6 < childCount) {
            View childAt = getChildAt(i6);
            if (childAt != null) {
                AppCompatImageView appCompatImageView = (AppCompatImageView) childAt;
                appCompatImageView.setLayoutParams(this.q);
                Object tag = appCompatImageView.getTag(123456789);
                if (tag != null) {
                    a(appCompatImageView, ((Integer) tag).intValue());
                    i6++;
                } else {
                    throw new x87("null cannot be cast to non-null type kotlin.Int");
                }
            } else {
                throw new x87("null cannot be cast to non-null type androidx.appcompat.widget.AppCompatImageView");
            }
        }
    }

    @DexIgnore
    public final void setDataAsync(SparseArray<List<BaseFeatureModel>> sparseArray) {
        ee7.b(sparseArray, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "setDataAsync data size " + sparseArray.size());
        int size = this.v.size();
        this.u.clear();
        if (size > 0) {
            SparseArray<List<BaseFeatureModel>> clone = sparseArray.clone();
            ee7.a((Object) clone, "data.clone()");
            this.u = clone;
            FLogger.INSTANCE.getLocal().d(x, "setDataAsync executing in progress, put new list in pending list");
            return;
        }
        SparseArray<List<BaseFeatureModel>> clone2 = sparseArray.clone();
        ee7.a((Object) clone2, "data.clone()");
        this.v = clone2;
        ArrayList<AppCompatImageView> arrayList = new ArrayList<>();
        kd5 a2 = hd5.a(getContext());
        ee7.a((Object) a2, "GlideApp.with(context)");
        TypedValue typedValue = new TypedValue();
        ColorStateList valueOf = ColorStateList.valueOf(this.b);
        ee7.a((Object) valueOf, "ColorStateList.valueOf(mDialItemTintColor)");
        k40<?> a3 = ((r40) ((r40) new r40().a((ex<Bitmap>) new vd5())).a(iy.a)).a(true);
        ee7.a((Object) a3, "RequestOptions()\n       \u2026   .skipMemoryCache(true)");
        r40 r40 = (r40) a3;
        Context context = getContext();
        ee7.a((Object) context, "context");
        context.getTheme().resolveAttribute(16843534, typedValue, true);
        for (int i2 = 1; i2 <= 12; i2++) {
            AppCompatImageView appCompatImageView = new AppCompatImageView(getContext());
            appCompatImageView.setLayoutParams(this.q);
            appCompatImageView.setTag(123456789, Integer.valueOf(i2));
            appCompatImageView.setOnClickListener(new d(this));
            if (sparseArray.get(i2) != null) {
                ee7.a((Object) a2.b().a((Object) new md5(sparseArray.get(i2))).b((q40<Bitmap>) new e(this, i2, appCompatImageView, arrayList)).a((k40<?>) r40).a(iy.a).O(), "glideApp.asBitmap()\n    \u2026                .submit()");
            } else {
                appCompatImageView.setImageResource(this.a);
                int i3 = this.d;
                appCompatImageView.setPadding(i3, i3, i3, i3);
                appCompatImageView.setElevation(50.0f);
                int i4 = this.c;
                if (i4 != -1) {
                    appCompatImageView.setBackgroundResource(i4);
                } else {
                    appCompatImageView.setBackgroundResource(typedValue.resourceId);
                }
                wa.a(appCompatImageView, valueOf);
                arrayList.add(appCompatImageView);
            }
        }
        a(arrayList);
    }

    @DexIgnore
    public final void setOnItemClickListener(b bVar) {
        ee7.b(bVar, "listener");
        this.w = bVar;
    }

    @DexIgnore
    public final void b() {
        setDataAsync(new SparseArray<>());
    }

    @DexIgnore
    public final void c() {
        FLogger.INSTANCE.getLocal().d(x, "initParams");
        this.g = getMeasuredWidth();
        this.h = getMeasuredHeight();
        ViewGroup.LayoutParams layoutParams = this.q;
        if (layoutParams != null) {
            layoutParams.width = this.j;
        }
        ViewGroup.LayoutParams layoutParams2 = this.q;
        if (layoutParams2 != null) {
            layoutParams2.height = this.j;
        }
        int min = Math.min(this.g, this.h) / 2;
        this.i = min;
        this.j = (int) (((float) min) / 3.5f);
    }

    @DexIgnore
    public final void d() {
        Paint paint = new Paint(1);
        this.f = paint;
        if (paint != null) {
            paint.setStyle(Paint.Style.FILL);
            this.q = new ViewGroup.LayoutParams(-1, -1);
            return;
        }
        ee7.d("mBitmapPaint");
        throw null;
    }

    @DexIgnore
    public final void a(ArrayList<AppCompatImageView> arrayList) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "onLoadBitmapAsyncComplete size " + arrayList.size());
        if (arrayList.size() == 12) {
            this.s.post(new c(this, arrayList));
        }
    }

    @DexIgnore
    public final void a() {
        removeAllViews();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationSummaryDialView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.NotificationSummaryDialView);
        this.a = obtainStyledAttributes.getResourceId(3, 2131231133);
        this.b = obtainStyledAttributes.getColor(4, v6.a(context, 2131099971));
        this.c = obtainStyledAttributes.getResourceId(1, -1);
        this.d = (int) obtainStyledAttributes.getDimension(2, yx6.a(15.0f));
        this.e = obtainStyledAttributes.getResourceId(0, R.drawable.ic_launcher_transparent);
        this.t = BitmapFactory.decodeResource(getResources(), this.e);
        obtainStyledAttributes.recycle();
        d();
        b();
    }

    @DexIgnore
    public final void a(AppCompatImageView appCompatImageView, int i2) {
        double d2 = ((double) (i2 - 3)) * 0.5235987755982988d;
        int i3 = this.j;
        int width = (int) (((double) ((getWidth() / 2) - (i3 / 2))) + (((double) this.p) * Math.cos(d2)));
        int height = (int) (((double) ((getHeight() / 2) - (i3 / 2))) + (((double) this.p) * Math.sin(d2)));
        appCompatImageView.layout(width, height, width + i3, i3 + height);
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        FLogger.INSTANCE.getLocal().d(x, "initClockImage");
        int i2 = this.j;
        float f2 = ((float) i2) * 1.5f;
        float f3 = ((float) this.g) - (((float) i2) * 1.5f);
        float f4 = f3 - f2;
        float f5 = (((float) this.h) - f4) / ((float) 2);
        float f6 = f4 + f5;
        Bitmap bitmap = this.t;
        if (bitmap != null) {
            Rect rect = new Rect((int) f2, (int) f5, (int) f3, (int) f6);
            Paint paint = this.f;
            if (paint != null) {
                canvas.drawBitmap(bitmap, (Rect) null, rect, paint);
            } else {
                ee7.d("mBitmapPaint");
                throw null;
            }
        }
    }
}
