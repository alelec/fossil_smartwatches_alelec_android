package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.ig5;
import com.fossil.pl4;
import com.fossil.qy6;
import com.fossil.ux6;
import com.fossil.v6;
import com.fossil.w97;
import com.fossil.x87;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FlexibleTextView extends AppCompatTextView {
    @DexIgnore
    public static /* final */ ArrayList<String> q; // = w97.a((Object[]) new String[]{"nonBrandSurface", "primaryColor"});
    @DexIgnore
    public static /* final */ ArrayList<String> r; // = w97.a((Object[]) new String[]{"primaryText", "nonBrandSwitchDisabledGray"});
    @DexIgnore
    public static /* final */ int s; // = Color.parseColor("#FFFF00");
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public String p; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a() {
            return FlexibleTextView.s;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextView(Context context) {
        super(context);
        ee7.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    @SuppressLint({"ResourceType"})
    public final void a(AttributeSet attributeSet) {
        this.i = 0;
        CharSequence text = getText();
        CharSequence hint = getHint();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, pl4.FlexibleTextView);
            this.i = obtainStyledAttributes.getInt(5, 0);
            this.j = obtainStyledAttributes.getInt(2, 0);
            String string = obtainStyledAttributes.getString(6);
            String str = "";
            if (string == null) {
                string = str;
            }
            this.p = string;
            String string2 = obtainStyledAttributes.getString(3);
            if (string2 == null) {
                string2 = str;
            }
            this.e = string2;
            String string3 = obtainStyledAttributes.getString(4);
            if (string3 == null) {
                string3 = str;
            }
            this.f = string3;
            String string4 = obtainStyledAttributes.getString(0);
            if (string4 != null) {
                str = string4;
            }
            this.g = str;
            this.h = obtainStyledAttributes.getInt(1, 0);
            obtainStyledAttributes.recycle();
            TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(attributeSet, new int[]{16843087, 16843088}, 0, 0);
            int resourceId = obtainStyledAttributes2.getResourceId(0, -1);
            if (resourceId != -1) {
                text = a(resourceId);
            }
            int resourceId2 = obtainStyledAttributes2.getResourceId(1, -1);
            if (resourceId2 != -1) {
                hint = a(resourceId2);
            }
            obtainStyledAttributes2.recycle();
        }
        if (!TextUtils.isEmpty(text)) {
            setText(text);
        }
        if (!TextUtils.isEmpty(hint)) {
            ee7.a((Object) hint, "hint");
            setHint(a(hint, this.j));
        }
        if (TextUtils.isEmpty(this.p)) {
            this.p = "primaryColor";
            String b = eh5.l.a().b(this.p);
            if (b != null) {
                qy6.a(this, Color.parseColor(b));
            }
        }
        if (TextUtils.isEmpty(this.e)) {
            this.e = "primaryText";
        }
        if (TextUtils.isEmpty(this.f)) {
            this.f = "nonBrandTextStyle2";
        }
        a(this.e, this.f, this.g, this.h);
    }

    @DexIgnore
    @Override // android.widget.TextView
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        ee7.b(bufferType, "type");
        if (charSequence == null) {
            charSequence = "";
        }
        super.setText(a(charSequence), bufferType);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "state");
        int hashCode = str.hashCode();
        if (hashCode != -227352252) {
            if (hashCode == 636108957 && str.equals("flexible_tv_unselected")) {
                String str2 = r.get(0);
                ee7.a((Object) str2, "UNSELECTED_TV_STYLES[0]");
                String str3 = this.f;
                String str4 = r.get(1);
                ee7.a((Object) str4, "UNSELECTED_TV_STYLES[1]");
                a(str2, str3, str4, this.h);
            }
        } else if (str.equals("flexible_tv_selected")) {
            String str5 = q.get(0);
            ee7.a((Object) str5, "SELECTED_TV_STYLES[0]");
            String str6 = this.f;
            String str7 = q.get(1);
            ee7.a((Object) str7, "SELECTED_TV_STYLES[1]");
            a(str5, str6, str7, this.h);
        }
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, int i2) {
        LayerDrawable layerDrawable;
        GradientDrawable gradientDrawable;
        LayerDrawable layerDrawable2;
        String b = eh5.l.a().b(str);
        Typeface c = eh5.l.a().c(str2);
        String b2 = eh5.l.a().b(str3);
        if (b != null) {
            setTextColor(Color.parseColor(b));
        }
        if (c != null) {
            setTypeface(c);
        }
        if (b2 != null) {
            GradientDrawable gradientDrawable2 = null;
            if (i2 == 1) {
                Drawable c2 = v6.c(getContext(), 2131230845);
                if (c2 != null) {
                    layerDrawable2 = (LayerDrawable) c2;
                    gradientDrawable = (GradientDrawable) layerDrawable2.findDrawableByLayerId(2131363060);
                } else {
                    throw new x87("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
                }
            } else if (i2 != 2) {
                layerDrawable = null;
                if (gradientDrawable2 != null && layerDrawable != null) {
                    gradientDrawable2.setColor(Color.parseColor(b2));
                    gradientDrawable2.setStroke(1, Color.parseColor(b2));
                    setBackground(layerDrawable);
                    return;
                }
            } else {
                Drawable c3 = v6.c(getContext(), 2131230861);
                if (c3 != null) {
                    layerDrawable2 = (LayerDrawable) c3;
                    gradientDrawable = (GradientDrawable) layerDrawable2.findDrawableByLayerId(2131363060);
                } else {
                    throw new x87("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
                }
            }
            gradientDrawable2 = gradientDrawable;
            layerDrawable = layerDrawable2;
            if (gradientDrawable2 != null) {
            }
        }
    }

    @DexIgnore
    public final CharSequence a(CharSequence charSequence) {
        return a(charSequence, this.i);
    }

    @DexIgnore
    public final CharSequence a(CharSequence charSequence, int i2) {
        if (i2 == 1) {
            return ux6.a(charSequence);
        }
        if (i2 == 2) {
            return ux6.b(charSequence);
        }
        if (i2 == 3) {
            return ux6.d(charSequence);
        }
        if (i2 == 4) {
            return ux6.e(charSequence);
        }
        if (i2 != 5) {
            return charSequence;
        }
        return ux6.c(charSequence);
    }

    @DexIgnore
    public final String a(int i2) {
        String a2 = ig5.a(PortfolioApp.g0.c(), i2);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026ioApp.instance, stringId)");
        return a2;
    }
}
