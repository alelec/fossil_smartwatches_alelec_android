package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.FrameLayout;
import com.fossil.pl4;
import java.lang.ref.WeakReference;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FossilPopupWindow {
    @DexIgnore
    public Drawable A;
    @DexIgnore
    public Drawable B;
    @DexIgnore
    public Drawable C;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public WeakReference<View> H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public int J;
    @DexIgnore
    public int K;
    @DexIgnore
    public int L;
    @DexIgnore
    public boolean M;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public /* final */ int[] a;
    @DexIgnore
    public /* final */ int[] b;
    @DexIgnore
    public Context c;
    @DexIgnore
    public WindowManager d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public c g;
    @DexIgnore
    public View h;
    @DexIgnore
    public View i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public int k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public int o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnAttachStateChangeListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            FossilPopupWindow.this.I = false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ViewTreeObserver.OnScrollChangedListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onScrollChanged() {
            c cVar;
            WeakReference<View> weakReference = FossilPopupWindow.this.H;
            View view = weakReference != null ? weakReference.get() : null;
            if (view != null && (cVar = FossilPopupWindow.this.g) != null) {
                WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) cVar.getLayoutParams();
                FossilPopupWindow fossilPopupWindow = FossilPopupWindow.this;
                fossilPopupWindow.b(fossilPopupWindow.a(view, layoutParams, fossilPopupWindow.J, fossilPopupWindow.K, layoutParams.width, layoutParams.height, fossilPopupWindow.L));
                FossilPopupWindow.this.a(layoutParams.x, layoutParams.y, -1, -1, true);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends FrameLayout {
    }

    @DexIgnore
    public FossilPopupWindow(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public void a(View view) {
        if (!this.e) {
            this.i = view;
            if (this.c == null && view != null) {
                this.c = view.getContext();
            }
            if (this.d == null && this.i != null) {
                this.d = (WindowManager) this.c.getSystemService("window");
            }
            Context context = this.c;
            if (context != null && !this.v) {
                a(context.getApplicationInfo().targetSdkVersion >= 22);
            }
        }
    }

    @DexIgnore
    public void b(boolean z2) {
        View view;
        if (z2 != this.D) {
            this.D = z2;
            if (this.A != null && (view = this.h) != null) {
                Drawable drawable = this.B;
                if (drawable == null) {
                    view.refreshDrawableState();
                } else if (z2) {
                    view.setBackground(drawable);
                } else {
                    view.setBackground(this.C);
                }
            }
        }
    }

    @DexIgnore
    public boolean c() {
        Context context;
        if (this.o >= 0 || (context = this.c) == null) {
            if (this.o == 1) {
                return true;
            }
            return false;
        } else if (context.getApplicationInfo().targetSdkVersion >= 11) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public final void d() {
        View view;
        c cVar;
        WeakReference<View> weakReference = this.H;
        if (weakReference != null && (view = weakReference.get()) != null && this.N && (cVar = this.g) != null) {
            cVar.setLayoutDirection(view.getLayoutDirection());
        }
    }

    @DexIgnore
    public FossilPopupWindow(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, 0);
    }

    @DexIgnore
    public FossilPopupWindow(Context context, AttributeSet attributeSet, int i2, int i3) {
        this.a = new int[2];
        this.b = new int[2];
        new Rect();
        this.k = 0;
        this.l = true;
        this.n = true;
        this.o = -1;
        this.r = true;
        this.u = true;
        this.E = false;
        this.F = -1;
        this.G = 0;
        new a();
        new b();
        this.c = context;
        this.d = (WindowManager) context.getSystemService("window");
        context.obtainStyledAttributes(attributeSet, pl4.PopupWindow, i2, i3).recycle();
    }

    @DexIgnore
    public final int b() {
        int i2 = this.G;
        if (i2 == 0) {
            i2 = 8388659;
        }
        if (this.f) {
            return (this.q || this.n) ? i2 | SQLiteDatabase.CREATE_IF_NECESSARY : i2;
        }
        return i2;
    }

    @DexIgnore
    public void a(boolean z2) {
        this.u = z2;
        this.v = true;
    }

    @DexIgnore
    public final boolean b(WindowManager.LayoutParams layoutParams, int i2, int i3, int i4, int i5, int i6, int i7, int i8, boolean z2) {
        int i9 = layoutParams.y + (i6 - i5);
        int i10 = i8 - i9;
        if (i9 >= 0 && i3 <= i10) {
            return true;
        }
        if (i3 > (i9 - i4) - i7) {
            return b(layoutParams, i3, i5, i6, i7, i8, z2);
        }
        layoutParams.y = (i5 - i3) + (this.M ? i2 + i4 : i2);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        if (r3.k == 1) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
        if (r3.k == 2) goto L_0x001f;
     */
    @DexIgnore
    @android.annotation.TargetApi(22)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(int r4) {
        /*
            r3 = this;
            r0 = -8815129(0xffffffffff797de7, float:-3.316315E38)
            r4 = r4 & r0
            boolean r0 = r3.E
            if (r0 == 0) goto L_0x000c
            r0 = 32768(0x8000, float:4.5918E-41)
            r4 = r4 | r0
        L_0x000c:
            boolean r0 = r3.j
            r1 = 131072(0x20000, float:1.83671E-40)
            if (r0 != 0) goto L_0x001a
            r4 = r4 | 8
            int r0 = r3.k
            r2 = 1
            if (r0 != r2) goto L_0x0020
            goto L_0x001f
        L_0x001a:
            int r0 = r3.k
            r2 = 2
            if (r0 != r2) goto L_0x0020
        L_0x001f:
            r4 = r4 | r1
        L_0x0020:
            boolean r0 = r3.l
            if (r0 != 0) goto L_0x0026
            r4 = r4 | 16
        L_0x0026:
            boolean r0 = r3.m
            if (r0 == 0) goto L_0x002d
            r0 = 262144(0x40000, float:3.67342E-40)
            r4 = r4 | r0
        L_0x002d:
            boolean r0 = r3.n
            if (r0 == 0) goto L_0x0035
            boolean r0 = r3.q
            if (r0 == 0) goto L_0x0037
        L_0x0035:
            r4 = r4 | 512(0x200, float:7.175E-43)
        L_0x0037:
            boolean r0 = r3.c()
            if (r0 == 0) goto L_0x0040
            r0 = 8388608(0x800000, float:1.17549435E-38)
            r4 = r4 | r0
        L_0x0040:
            boolean r0 = r3.p
            if (r0 == 0) goto L_0x0046
            r4 = r4 | 256(0x100, float:3.59E-43)
        L_0x0046:
            boolean r0 = r3.s
            if (r0 == 0) goto L_0x004d
            r0 = 65536(0x10000, float:9.18355E-41)
            r4 = r4 | r0
        L_0x004d:
            boolean r0 = r3.t
            if (r0 == 0) goto L_0x0053
            r4 = r4 | 32
        L_0x0053:
            boolean r0 = r3.u
            if (r0 == 0) goto L_0x005a
            r0 = 1073741824(0x40000000, float:2.0)
            r4 = r4 | r0
        L_0x005a:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.FossilPopupWindow.a(int):int");
    }

    @DexIgnore
    public final boolean b(WindowManager.LayoutParams layoutParams, int i2, int i3, int i4, int i5, int i6, boolean z2) {
        boolean z3;
        int i7 = i4 - i3;
        int i8 = layoutParams.y + i7;
        layoutParams.y = i8;
        layoutParams.height = i2;
        int i9 = i8 + i2;
        if (i9 > i6) {
            layoutParams.y = i8 - (i9 - i6);
        }
        if (layoutParams.y < i5) {
            layoutParams.y = i5;
            int i10 = i6 - i5;
            if (!z2 || i2 <= i10) {
                z3 = false;
                layoutParams.y -= i7;
                return z3;
            }
            layoutParams.height = i10;
        }
        z3 = true;
        layoutParams.y -= i7;
        return z3;
    }

    @DexIgnore
    public FossilPopupWindow() {
        this((View) null, 0, 0);
    }

    @DexIgnore
    public FossilPopupWindow(View view, int i2, int i3) {
        this(view, i2, i3, false);
    }

    @DexIgnore
    public final int a() {
        return this.F;
    }

    @DexIgnore
    public FossilPopupWindow(View view, int i2, int i3, boolean z2) {
        this.a = new int[2];
        this.b = new int[2];
        new Rect();
        this.k = 0;
        this.l = true;
        this.n = true;
        this.o = -1;
        this.r = true;
        this.u = true;
        this.E = false;
        this.F = -1;
        this.G = 0;
        new a();
        new b();
        if (view != null) {
            Context context = view.getContext();
            this.c = context;
            this.d = (WindowManager) context.getSystemService("window");
        }
        a(view);
        this.j = z2;
    }

    @DexIgnore
    public boolean a(View view, WindowManager.LayoutParams layoutParams, int i2, int i3, int i4, int i5, int i6) {
        char c2;
        WindowManager.LayoutParams layoutParams2;
        int height = view.getHeight();
        int width = view.getWidth();
        int i7 = this.M ? i3 - height : i3;
        int[] iArr = this.a;
        view.getLocationInWindow(iArr);
        layoutParams.x = iArr[0] + i2;
        layoutParams.y = iArr[1] + height + i7;
        Rect rect = new Rect();
        view.getWindowVisibleDisplayFrame(rect);
        int i8 = i4;
        if (i8 == -1) {
            i8 = rect.right - rect.left;
        }
        int i9 = i5 == -1 ? rect.bottom - rect.top : i5;
        layoutParams.gravity = b();
        layoutParams.width = i8;
        layoutParams.height = i9;
        int absoluteGravity = Gravity.getAbsoluteGravity(i6, view.getLayoutDirection()) & 7;
        if (absoluteGravity == 8388613) {
            layoutParams.x -= i8 - width;
        }
        int[] iArr2 = this.b;
        view.getLocationOnScreen(iArr2);
        boolean b2 = b(layoutParams, i7, i9, height, iArr[1], iArr2[1], rect.top, rect.bottom, false);
        boolean a2 = a(layoutParams, i2, i8, width, iArr[0], iArr2[0], rect.left, rect.right, false);
        if (!b2 || !a2) {
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            Rect rect2 = new Rect(scrollX, scrollY, scrollX + i8 + i2, scrollY + i9 + height + i7);
            if (!this.r || !view.requestRectangleOnScreen(rect2, true)) {
                layoutParams2 = layoutParams;
                c2 = 1;
            } else {
                view.getLocationInWindow(iArr);
                int i10 = iArr[0] + i2;
                layoutParams2 = layoutParams;
                c2 = 1;
                layoutParams2.x = i10;
                layoutParams2.y = iArr[1] + height + i7;
                if (absoluteGravity == 8388613) {
                    layoutParams2.x = i10 - (i8 - width);
                }
            }
            b(layoutParams, i7, i9, height, iArr[c2], iArr2[c2], rect.top, rect.bottom, this.q);
            a(layoutParams, i2, i8, width, iArr[0], iArr2[0], rect.left, rect.right, this.q);
        } else {
            layoutParams2 = layoutParams;
            c2 = 1;
        }
        if (layoutParams2.y < iArr[c2]) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000b, code lost:
        if (r11 > r0) goto L_0x000f;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.view.WindowManager.LayoutParams r9, int r10, int r11, int r12, int r13, int r14, int r15, int r16, boolean r17) {
        /*
            r8 = this;
            int r0 = r14 - r13
            r1 = r9
            int r2 = r1.x
            int r2 = r2 + r0
            int r0 = r16 - r2
            if (r2 < 0) goto L_0x000e
            r2 = r11
            if (r2 <= r0) goto L_0x001f
            goto L_0x000f
        L_0x000e:
            r2 = r11
        L_0x000f:
            r0 = r8
            r1 = r9
            r2 = r11
            r3 = r13
            r4 = r14
            r5 = r15
            r6 = r16
            r7 = r17
            boolean r0 = r0.a(r1, r2, r3, r4, r5, r6, r7)
            if (r0 == 0) goto L_0x0021
        L_0x001f:
            r0 = 1
            goto L_0x0022
        L_0x0021:
            r0 = 0
        L_0x0022:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.FossilPopupWindow.a(android.view.WindowManager$LayoutParams, int, int, int, int, int, int, int, boolean):boolean");
    }

    @DexIgnore
    public final boolean a(WindowManager.LayoutParams layoutParams, int i2, int i3, int i4, int i5, int i6, boolean z2) {
        boolean z3;
        int i7 = i4 - i3;
        int i8 = layoutParams.x + i7;
        layoutParams.x = i8;
        int i9 = i8 + i2;
        if (i9 > i6) {
            layoutParams.x = i8 - (i9 - i6);
        }
        if (layoutParams.x < i5) {
            layoutParams.x = i5;
            int i10 = i6 - i5;
            if (!z2 || i2 <= i10) {
                z3 = false;
                layoutParams.x -= i7;
                return z3;
            }
            layoutParams.width = i10;
        }
        z3 = true;
        layoutParams.x -= i7;
        return z3;
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5, boolean z2) {
        c cVar;
        if (i4 >= 0) {
            this.x = i4;
        }
        if (i5 >= 0) {
            this.z = i5;
        }
        if (this.e && this.i != null && (cVar = this.g) != null) {
            WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) cVar.getLayoutParams();
            int i6 = this.w;
            if (i6 >= 0) {
                i6 = this.x;
            }
            boolean z3 = true;
            if (!(i4 == -1 || layoutParams.width == i6)) {
                this.x = i6;
                layoutParams.width = i6;
                z2 = true;
            }
            int i7 = this.y;
            if (i7 >= 0) {
                i7 = this.z;
            }
            if (!(i5 == -1 || layoutParams.height == i7)) {
                this.z = i7;
                layoutParams.height = i7;
                z2 = true;
            }
            if (layoutParams.x != i2) {
                layoutParams.x = i2;
                z2 = true;
            }
            if (layoutParams.y != i3) {
                layoutParams.y = i3;
                z2 = true;
            }
            int a2 = a();
            if (a2 != layoutParams.windowAnimations) {
                layoutParams.windowAnimations = a2;
                z2 = true;
            }
            int a3 = a(layoutParams.flags);
            if (a3 != layoutParams.flags) {
                layoutParams.flags = a3;
                z2 = true;
            }
            int b2 = b();
            if (b2 != layoutParams.gravity) {
                layoutParams.gravity = b2;
            } else {
                z3 = z2;
            }
            if (z3) {
                d();
                this.d.updateViewLayout(this.g, layoutParams);
            }
        }
    }
}
