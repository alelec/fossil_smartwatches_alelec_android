package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ee7;
import com.fossil.xw6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecyclerViewHeartRateCalendar$init$Anon1 extends GridLayoutManager {
    @DexIgnore
    public /* final */ /* synthetic */ Context R;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewHeartRateCalendar$init$Anon1(Context context, Context context2, int i, int i2, boolean z) {
        super(context2, i, i2, z);
        this.R = context;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.m, androidx.recyclerview.widget.LinearLayoutManager
    public boolean a() {
        return false;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.LinearLayoutManager
    public int k(RecyclerView.State state) {
        ee7.b(state, "state");
        return xw6.a(this.R);
    }
}
