package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Scroller;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.eh5;
import com.fossil.pl4;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NumberPicker extends LinearLayout {
    @DexIgnore
    public /* final */ Paint A;
    @DexIgnore
    public /* final */ Drawable B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public /* final */ Scroller F;
    @DexIgnore
    public /* final */ Scroller G;
    @DexIgnore
    public int H;
    @DexIgnore
    public e I;
    @DexIgnore
    public d J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public VelocityTracker M;
    @DexIgnore
    public int N;
    @DexIgnore
    public int O;
    @DexIgnore
    public int P;
    @DexIgnore
    public boolean Q;
    @DexIgnore
    public /* final */ int R;
    @DexIgnore
    public /* final */ boolean S;
    @DexIgnore
    public /* final */ Drawable T;
    @DexIgnore
    public /* final */ int U;
    @DexIgnore
    public int V;
    @DexIgnore
    public boolean W;
    @DexIgnore
    public String a;
    @DexIgnore
    public boolean a0;
    @DexIgnore
    public String b;
    @DexIgnore
    public int b0;
    @DexIgnore
    public /* final */ ImageButton c;
    @DexIgnore
    public int c0;
    @DexIgnore
    public /* final */ ImageButton d;
    @DexIgnore
    public int d0;
    @DexIgnore
    public /* final */ AppCompatEditText e;
    @DexIgnore
    public boolean e0;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public boolean f0;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public i g0;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ h h0;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public int i0;
    @DexIgnore
    public int j;
    @DexIgnore
    public /* final */ boolean p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public String[] s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public g w;
    @DexIgnore
    public f x;
    @DexIgnore
    public /* final */ SparseArray<String> y;
    @DexIgnore
    public /* final */ int[] z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class CustomEditText extends AppCompatEditText {
        @DexIgnore
        public CustomEditText(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        @DexIgnore
        public void onEditorAction(int i) {
            super.onEditorAction(i);
            if (i == 6) {
                clearFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            NumberPicker.this.b();
            NumberPicker.this.e.clearFocus();
            if (view.getId() == 2131362849) {
                NumberPicker.this.a(true);
            } else {
                NumberPicker.this.a(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnLongClickListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            NumberPicker.this.b();
            NumberPicker.this.e.clearFocus();
            if (view.getId() == 2131362849) {
                NumberPicker.this.a(true, 0L);
            } else {
                NumberPicker.this.a(false, 0L);
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            NumberPicker.this.j();
            NumberPicker.this.W = true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public boolean a;

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public void run() {
            NumberPicker.this.a(this.a);
            NumberPicker.this.postDelayed(this, 300);
        }
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        String format(int i);
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(NumberPicker numberPicker, int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i {
        @DexIgnore
        public c a;

        @DexIgnore
        public i() {
            this.a = new c();
        }

        @DexIgnore
        public boolean a(int i, int i2, Bundle bundle) {
            c cVar = this.a;
            return cVar != null && cVar.performAction(i, i2, bundle);
        }

        @DexIgnore
        public void a(int i, int i2) {
            c cVar = this.a;
            if (cVar != null) {
                cVar.a(i, i2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class j implements f {
        @DexIgnore
        public /* final */ StringBuilder a; // = new StringBuilder();
        @DexIgnore
        public char b;
        @DexIgnore
        public transient Formatter c;
        @DexIgnore
        public /* final */ Object[] d; // = new Object[1];

        @DexIgnore
        public j() {
            b(Locale.getDefault());
        }

        @DexIgnore
        public static char c(Locale locale) {
            return new DecimalFormatSymbols(locale).getZeroDigit();
        }

        @DexIgnore
        public final Formatter a(Locale locale) {
            return new Formatter(this.a, locale);
        }

        @DexIgnore
        public final void b(Locale locale) {
            this.c = a(locale);
            this.b = c(locale);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.f
        public String format(int i) {
            Locale locale = Locale.getDefault();
            if (this.b != c(locale)) {
                b(locale);
            }
            this.d[0] = Integer.valueOf(i);
            StringBuilder sb = this.a;
            sb.delete(0, sb.length());
            this.c.format("%02d", this.d);
            return this.c.toString();
        }
    }

    /*
    static {
        new j();
    }
    */

    @DexIgnore
    public NumberPicker(Context context) {
        this(context, null);
    }

    @DexIgnore
    private i getSupportAccessibilityNodeProvider() {
        return new i();
    }

    @DexIgnore
    public static int resolveSizeAndState(int i2, int i3, int i4) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode != Integer.MIN_VALUE) {
            if (mode == 1073741824) {
                i2 = size;
            }
        } else if (size < i2) {
            i2 = 16777216 | size;
        }
        return i2 | (-16777216 & i4);
    }

    @DexIgnore
    public final boolean a(Scroller scroller) {
        scroller.forceFinished(true);
        int finalY = scroller.getFinalY() - scroller.getCurrY();
        int i2 = this.D - ((this.E + finalY) % this.C);
        if (i2 == 0) {
            return false;
        }
        int abs = Math.abs(i2);
        int i3 = this.C;
        if (abs > i3 / 2) {
            i2 = i2 > 0 ? i2 - i3 : i2 + i3;
        }
        scrollBy(0, finalY + i2);
        return true;
    }

    @DexIgnore
    public void b() {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null && inputMethodManager.isActive(this.e)) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            if (this.S) {
                this.e.setVisibility(4);
            }
        }
    }

    @DexIgnore
    public final void c() {
        setVerticalFadingEdgeEnabled(true);
        setFadingEdgeLength(((getBottom() - getTop()) - this.q) / 2);
    }

    @DexIgnore
    public void computeScroll() {
        Scroller scroller = this.F;
        if (scroller.isFinished()) {
            scroller = this.G;
            if (scroller.isFinished()) {
                return;
            }
        }
        scroller.computeScrollOffset();
        int currY = scroller.getCurrY();
        if (this.H == 0) {
            this.H = scroller.getStartY();
        }
        scrollBy(0, currY - this.H);
        this.H = currY;
        if (scroller.isFinished()) {
            b(scroller);
        } else {
            invalidate();
        }
    }

    @DexIgnore
    public final void d() {
        e();
        int[] iArr = this.z;
        int bottom = (int) ((((float) ((getBottom() - getTop()) - (iArr.length * this.q))) / ((float) iArr.length)) + 0.5f);
        this.r = bottom;
        this.C = this.q + bottom;
        int baseline = (this.e.getBaseline() + this.e.getTop()) - (this.C * 1);
        this.D = baseline;
        this.E = baseline;
        l();
    }

    @DexIgnore
    public boolean dispatchHoverEvent(MotionEvent motionEvent) {
        int i2;
        if (!this.S) {
            return super.dispatchHoverEvent(motionEvent);
        }
        if (!((AccessibilityManager) getContext().getSystemService("accessibility")).isEnabled()) {
            return false;
        }
        int y2 = (int) motionEvent.getY();
        if (y2 < this.b0) {
            i2 = 3;
        } else {
            i2 = y2 > this.c0 ? 1 : 2;
        }
        int action = motionEvent.getAction() & 255;
        i supportAccessibilityNodeProvider = getSupportAccessibilityNodeProvider();
        if (action == 7) {
            int i3 = this.d0;
            if (i3 == i2 || i3 == -1) {
                return false;
            }
            supportAccessibilityNodeProvider.a(i3, 256);
            supportAccessibilityNodeProvider.a(i2, 128);
            this.d0 = i2;
            supportAccessibilityNodeProvider.a(i2, 64, null);
            return false;
        } else if (action == 9) {
            supportAccessibilityNodeProvider.a(i2, 128);
            this.d0 = i2;
            supportAccessibilityNodeProvider.a(i2, 64, null);
            return false;
        } else if (action != 10) {
            return false;
        } else {
            supportAccessibilityNodeProvider.a(i2, 256);
            this.d0 = -1;
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
        requestFocus();
        r5.i0 = r0;
        g();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
        if (r5.F.isFinished() == false) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0054, code lost:
        if (r0 != 20) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0056, code lost:
        r6 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0058, code lost:
        r6 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0059, code lost:
        a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005c, code lost:
        return true;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean dispatchKeyEvent(android.view.KeyEvent r6) {
        /*
            r5 = this;
            int r0 = r6.getKeyCode()
            r1 = 19
            r2 = 20
            if (r0 == r1) goto L_0x0019
            if (r0 == r2) goto L_0x0019
            r1 = 23
            if (r0 == r1) goto L_0x0015
            r1 = 66
            if (r0 == r1) goto L_0x0015
            goto L_0x005d
        L_0x0015:
            r5.g()
            goto L_0x005d
        L_0x0019:
            boolean r1 = r5.S
            if (r1 != 0) goto L_0x001e
            goto L_0x005d
        L_0x001e:
            int r1 = r6.getAction()
            r3 = 1
            if (r1 == 0) goto L_0x0030
            if (r1 == r3) goto L_0x0028
            goto L_0x005d
        L_0x0028:
            int r1 = r5.i0
            if (r1 != r0) goto L_0x005d
            r6 = -1
            r5.i0 = r6
            return r3
        L_0x0030:
            boolean r1 = r5.Q
            if (r1 != 0) goto L_0x003e
            if (r0 != r2) goto L_0x0037
            goto L_0x003e
        L_0x0037:
            int r1 = r5.v
            int r4 = r5.t
            if (r1 <= r4) goto L_0x005d
            goto L_0x0044
        L_0x003e:
            int r1 = r5.v
            int r4 = r5.u
            if (r1 >= r4) goto L_0x005d
        L_0x0044:
            r5.requestFocus()
            r5.i0 = r0
            r5.g()
            android.widget.Scroller r6 = r5.F
            boolean r6 = r6.isFinished()
            if (r6 == 0) goto L_0x005c
            if (r0 != r2) goto L_0x0058
            r6 = 1
            goto L_0x0059
        L_0x0058:
            r6 = 0
        L_0x0059:
            r5.a(r6)
        L_0x005c:
            return r3
        L_0x005d:
            boolean r6 = super.dispatchKeyEvent(r6)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.NumberPicker.dispatchKeyEvent(android.view.KeyEvent):boolean");
    }

    @DexIgnore
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 3) {
            g();
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    @DexIgnore
    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 3) {
            g();
        }
        return super.dispatchTrackballEvent(motionEvent);
    }

    @DexIgnore
    public final void e() {
        this.y.clear();
        int[] iArr = this.z;
        int i2 = this.v;
        for (int i3 = 0; i3 < this.z.length; i3++) {
            int i4 = (i3 - 1) + i2;
            if (this.Q) {
                i4 = d(i4);
            }
            iArr[i3] = i4;
            a(iArr[i3]);
        }
    }

    @DexIgnore
    public final void f(int i2) {
        if (this.V != i2) {
            this.V = i2;
        }
    }

    @DexIgnore
    public final void g() {
        e eVar = this.I;
        if (eVar != null) {
            removeCallbacks(eVar);
        }
        d dVar = this.J;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
        this.h0.a();
    }

    @DexIgnore
    public AccessibilityNodeProvider getAccessibilityNodeProvider() {
        if (!this.S) {
            return super.getAccessibilityNodeProvider();
        }
        if (this.g0 == null) {
            this.g0 = new i();
        }
        return this.g0.a;
    }

    @DexIgnore
    public float getBottomFadingEdgeStrength() {
        return 0.9f;
    }

    @DexIgnore
    public int getMaxValue() {
        return this.u;
    }

    @DexIgnore
    public int getMinValue() {
        return this.t;
    }

    @DexIgnore
    public int getSolidColor() {
        return this.R;
    }

    @DexIgnore
    public float getTopFadingEdgeStrength() {
        return 0.9f;
    }

    @DexIgnore
    public int getValue() {
        return this.v;
    }

    @DexIgnore
    public boolean getWrapSelectorWheel() {
        return this.Q;
    }

    @DexIgnore
    public final void h() {
        d dVar = this.J;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
    }

    @DexIgnore
    public final void i() {
        e eVar = this.I;
        if (eVar != null) {
            removeCallbacks(eVar);
        }
    }

    @DexIgnore
    public void j() {
    }

    @DexIgnore
    public final void k() {
        int i2;
        if (this.p) {
            String[] strArr = this.s;
            int i3 = 0;
            if (strArr == null) {
                float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                for (int i4 = 0; i4 <= 9; i4++) {
                    float measureText = this.A.measureText(g(i4));
                    if (measureText > f2) {
                        f2 = measureText;
                    }
                }
                for (int i5 = this.u; i5 > 0; i5 /= 10) {
                    i3++;
                }
                i2 = (int) (((float) i3) * f2);
            } else {
                int length = strArr.length;
                int i6 = 0;
                while (i3 < length) {
                    float measureText2 = this.A.measureText(strArr[i3]);
                    if (measureText2 > ((float) i6)) {
                        i6 = (int) measureText2;
                    }
                    i3++;
                }
                i2 = i6;
            }
            int paddingLeft = i2 + this.e.getPaddingLeft() + this.e.getPaddingRight();
            if (this.j != paddingLeft) {
                int i7 = this.i;
                if (paddingLeft > i7) {
                    this.j = paddingLeft;
                } else {
                    this.j = i7;
                }
                invalidate();
            }
        }
    }

    @DexIgnore
    public final boolean l() {
        String[] strArr = this.s;
        String c2 = strArr == null ? c(this.v) : strArr[this.v - this.t];
        if (TextUtils.isEmpty(c2) || c2.equals(this.e.getText().toString())) {
            return false;
        }
        this.e.setText(c2);
        return true;
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        g();
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (!this.S) {
            super.onDraw(canvas);
            return;
        }
        float right = (float) ((getRight() - getLeft()) / 2);
        float f2 = (float) this.E;
        Drawable drawable = this.B;
        if (drawable != null && this.V == 0) {
            if (this.f0) {
                drawable.setState(LinearLayout.PRESSED_ENABLED_STATE_SET);
                this.B.setBounds(0, 0, getRight(), this.b0);
                this.B.draw(canvas);
            }
            if (this.e0) {
                this.B.setState(LinearLayout.PRESSED_ENABLED_STATE_SET);
                this.B.setBounds(0, this.c0, getRight(), getBottom());
                this.B.draw(canvas);
            }
        }
        int[] iArr = this.z;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            String str = this.y.get(iArr[i2]);
            if (i2 != 1 || this.e.getVisibility() != 0) {
                a(this.A, (float) getWidth(), str);
                canvas.drawText(str, right, f2, this.A);
            }
            f2 += (float) this.C;
        }
        Drawable drawable2 = this.T;
        if (drawable2 != null) {
            int i3 = this.b0;
            drawable2.setBounds(0, i3, getRight(), this.U + i3);
            this.T.draw(canvas);
            int i4 = this.c0;
            this.T.setBounds(0, i4 - this.U, getRight(), i4);
            this.T.draw(canvas);
        }
    }

    @DexIgnore
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(NumberPicker.class.getName());
        accessibilityEvent.setScrollable(true);
        accessibilityEvent.setScrollY((this.t + this.v) * this.C);
        accessibilityEvent.setMaxScrollY((this.u - this.t) * this.C);
    }

    @DexIgnore
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.S || !isEnabled() || (motionEvent.getAction() & 255) != 0) {
            return false;
        }
        g();
        this.e.setVisibility(4);
        float y2 = motionEvent.getY();
        this.K = y2;
        this.L = y2;
        this.W = false;
        this.a0 = false;
        if (y2 < ((float) this.b0)) {
            if (this.V == 0) {
                this.h0.a(2);
            }
        } else if (y2 > ((float) this.c0) && this.V == 0) {
            this.h0.a(1);
        }
        getParent().requestDisallowInterceptTouchEvent(true);
        if (!this.F.isFinished()) {
            this.F.forceFinished(true);
            this.G.forceFinished(true);
            f(0);
        } else if (!this.G.isFinished()) {
            this.F.forceFinished(true);
            this.G.forceFinished(true);
        } else {
            float f2 = this.K;
            if (f2 < ((float) this.b0)) {
                b();
                a(false, (long) ViewConfiguration.getLongPressTimeout());
            } else if (f2 > ((float) this.c0)) {
                b();
                a(true, (long) ViewConfiguration.getLongPressTimeout());
            } else {
                this.a0 = true;
                f();
            }
        }
        return true;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        if (!this.S) {
            super.onLayout(z2, i2, i3, i4, i5);
            return;
        }
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        int measuredWidth2 = this.e.getMeasuredWidth();
        int measuredHeight2 = this.e.getMeasuredHeight();
        int i6 = (measuredWidth - measuredWidth2) / 2;
        int i7 = (measuredHeight - measuredHeight2) / 2;
        this.e.layout(i6, i7, measuredWidth2 + i6, measuredHeight2 + i7);
        if (z2) {
            d();
            c();
            int height = getHeight();
            int i8 = this.f;
            int i9 = this.U;
            int i10 = ((height - i8) / 2) - i9;
            this.b0 = i10;
            this.c0 = i10 + (i9 * 2) + i8;
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (!this.S) {
            super.onMeasure(i2, i3);
            return;
        }
        super.onMeasure(a(i2, this.j), a(i3, this.h));
        setMeasuredDimension(a(this.i, getMeasuredWidth(), i2), a(this.g, getMeasuredHeight(), i3));
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!isEnabled() || !this.S) {
            return false;
        }
        if (this.M == null) {
            this.M = VelocityTracker.obtain();
        }
        this.M.addMovement(motionEvent);
        int action = motionEvent.getAction() & 255;
        if (action == 1) {
            h();
            i();
            this.h0.a();
            VelocityTracker velocityTracker = this.M;
            velocityTracker.computeCurrentVelocity(1000, (float) this.P);
            int yVelocity = (int) velocityTracker.getYVelocity();
            if (Math.abs(yVelocity) > this.O) {
                b(yVelocity);
                f(2);
            } else {
                int y2 = (int) motionEvent.getY();
                if (((int) Math.abs(((float) y2) - this.K)) > this.N) {
                    a();
                } else if (this.a0) {
                    this.a0 = false;
                    j();
                } else {
                    int i2 = (y2 / this.C) - 1;
                    if (i2 > 0) {
                        a(true);
                        this.h0.b(1);
                    } else if (i2 < 0) {
                        a(false);
                        this.h0.b(2);
                    }
                }
                f(0);
            }
            this.M.recycle();
            this.M = null;
        } else if (action == 2 && !this.W) {
            float y3 = motionEvent.getY();
            if (this.V == 1) {
                scrollBy(0, (int) (y3 - this.L));
                invalidate();
            } else if (((int) Math.abs(y3 - this.K)) > this.N) {
                g();
                f(1);
            }
            this.L = y3;
        }
        return true;
    }

    @DexIgnore
    public void scrollBy(int i2, int i3) {
        int[] iArr = this.z;
        if (!this.Q && i3 > 0 && iArr[1] <= this.t) {
            this.E = this.D;
        } else if (this.Q || i3 >= 0 || iArr[1] < this.u) {
            this.E += i3;
            while (true) {
                int i4 = this.E;
                if (i4 - this.D <= this.r) {
                    break;
                }
                this.E = i4 - this.C;
                a(iArr);
                a(iArr[1], true);
                if (!this.Q && iArr[1] <= this.t) {
                    this.E = this.D;
                }
            }
            while (true) {
                int i5 = this.E;
                if (i5 - this.D < (-this.r)) {
                    this.E = i5 + this.C;
                    b(iArr);
                    a(iArr[1], true);
                    if (!this.Q && iArr[1] >= this.u) {
                        this.E = this.D;
                    }
                } else {
                    return;
                }
            }
        } else {
            this.E = this.D;
        }
    }

    @DexIgnore
    public void setDisplayedValues(String[] strArr) {
        if (!Arrays.equals(this.s, strArr)) {
            this.s = strArr;
            if (strArr != null) {
                this.e.setRawInputType(524289);
            } else {
                this.e.setRawInputType(2);
            }
            l();
            e();
            k();
        }
    }

    @DexIgnore
    public void setEnabled(boolean z2) {
        ImageButton imageButton;
        ImageButton imageButton2;
        super.setEnabled(z2);
        if (!this.S && (imageButton2 = this.c) != null) {
            imageButton2.setEnabled(z2);
        }
        if (!this.S && (imageButton = this.d) != null) {
            imageButton.setEnabled(z2);
        }
        this.e.setEnabled(z2);
    }

    @DexIgnore
    public void setMaxValue(int i2) {
        if (this.u != i2) {
            if (i2 >= 0) {
                this.u = i2;
                if (i2 < this.v) {
                    this.v = i2;
                }
                setWrapSelectorWheel(this.u - this.t > this.z.length);
                e();
                l();
                k();
                invalidate();
                return;
            }
            throw new IllegalArgumentException("maxValue must be >= 0");
        }
    }

    @DexIgnore
    public void setMinValue(int i2) {
        if (this.t != i2) {
            if (i2 >= 0) {
                this.t = i2;
                if (i2 > this.v) {
                    this.v = i2;
                }
                setWrapSelectorWheel(this.u - this.t > this.z.length);
                e();
                l();
                k();
                invalidate();
                return;
            }
            throw new IllegalArgumentException("minValue must be >= 0");
        }
    }

    @DexIgnore
    public void setOnValueChangedListener(g gVar) {
        this.w = gVar;
    }

    @DexIgnore
    public void setValue(int i2) {
        a(i2, false);
    }

    @DexIgnore
    public void setWrapSelectorWheel(boolean z2) {
        boolean z3 = this.u - this.t >= this.z.length;
        if ((!z2 || z3) && z2 != this.Q) {
            this.Q = z2;
        }
    }

    @DexIgnore
    public NumberPicker(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 2130969482);
    }

    @DexIgnore
    public NumberPicker(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        Typeface c2;
        String b2;
        this.y = new SparseArray<>();
        this.z = new int[3];
        this.D = RecyclerView.UNDEFINED_DURATION;
        boolean z2 = false;
        this.V = 0;
        this.i0 = -1;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.NumberPicker, i2, 0);
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        this.S = resourceId != 0;
        obtainStyledAttributes.getColor(11, 0);
        this.R = obtainStyledAttributes.getColor(15, 0);
        this.T = obtainStyledAttributes.getDrawable(12);
        this.a = obtainStyledAttributes.getString(7);
        this.b = obtainStyledAttributes.getString(8);
        this.U = obtainStyledAttributes.getDimensionPixelSize(13, (int) TypedValue.applyDimension(1, 2.0f, getResources().getDisplayMetrics()));
        this.f = obtainStyledAttributes.getDimensionPixelSize(14, (int) TypedValue.applyDimension(1, 48.0f, getResources().getDisplayMetrics()));
        this.g = obtainStyledAttributes.getDimensionPixelSize(3, -1);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(1, -1);
        this.h = dimensionPixelSize;
        int i3 = this.g;
        if (i3 == -1 || dimensionPixelSize == -1 || i3 <= dimensionPixelSize) {
            this.i = obtainStyledAttributes.getDimensionPixelSize(4, -1);
            int dimensionPixelSize2 = obtainStyledAttributes.getDimensionPixelSize(2, -1);
            this.j = dimensionPixelSize2;
            int i4 = this.i;
            if (i4 == -1 || dimensionPixelSize2 == -1 || i4 <= dimensionPixelSize2) {
                this.p = this.j == -1 ? true : z2;
                this.B = obtainStyledAttributes.getDrawable(17);
                obtainStyledAttributes.recycle();
                this.h0 = new h();
                setWillNotDraw(!this.S);
                LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
                if (layoutInflater != null) {
                    layoutInflater.inflate(resourceId, (ViewGroup) this, true);
                }
                a aVar = new a();
                b bVar = new b();
                if (!this.S) {
                    ImageButton imageButton = (ImageButton) findViewById(2131362849);
                    this.c = imageButton;
                    imageButton.setOnClickListener(aVar);
                    this.c.setOnLongClickListener(bVar);
                } else {
                    this.c = null;
                }
                if (!this.S) {
                    ImageButton imageButton2 = (ImageButton) findViewById(2131362848);
                    this.d = imageButton2;
                    imageButton2.setOnClickListener(aVar);
                    this.d.setOnLongClickListener(bVar);
                } else {
                    this.d = null;
                }
                this.e = (AppCompatEditText) findViewById(2131362850);
                if (!TextUtils.isEmpty(this.a) && (b2 = eh5.l.a().b(this.a)) != null) {
                    this.e.setTextColor(Color.parseColor(b2));
                }
                if (!TextUtils.isEmpty(this.b) && (c2 = eh5.l.a().c(this.b)) != null) {
                    this.e.setTypeface(c2);
                }
                ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
                this.N = viewConfiguration.getScaledTouchSlop();
                this.O = viewConfiguration.getScaledMinimumFlingVelocity();
                this.P = viewConfiguration.getScaledMaximumFlingVelocity() / 8;
                this.q = (int) this.e.getTextSize();
                Paint paint = new Paint();
                paint.setAntiAlias(true);
                paint.setTextAlign(Paint.Align.CENTER);
                paint.setTextSize((float) this.q);
                paint.setTypeface(this.e.getTypeface());
                paint.setColor(this.e.getTextColors().getColorForState(LinearLayout.ENABLED_STATE_SET, -1));
                this.A = paint;
                this.F = new Scroller(getContext(), null, true);
                this.G = new Scroller(getContext(), new DecelerateInterpolator(2.5f));
                l();
                if (getImportantForAccessibility() == 0) {
                    setImportantForAccessibility(1);
                    return;
                }
                return;
            }
            throw new IllegalArgumentException("minWidth > maxWidth");
        }
        throw new IllegalArgumentException("minHeight > maxHeight");
    }

    @DexIgnore
    public String c(int i2) {
        f fVar = this.x;
        return fVar != null ? fVar.format(i2) : g(i2);
    }

    @DexIgnore
    public final void f() {
        d dVar = this.J;
        if (dVar == null) {
            this.J = new d();
        } else {
            removeCallbacks(dVar);
        }
        postDelayed(this.J, (long) ViewConfiguration.getLongPressTimeout());
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements Runnable {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;

        @DexIgnore
        public h() {
        }

        @DexIgnore
        public void a() {
            this.b = 0;
            this.a = 0;
            NumberPicker.this.removeCallbacks(this);
            NumberPicker numberPicker = NumberPicker.this;
            if (numberPicker.e0) {
                numberPicker.e0 = false;
                numberPicker.invalidate(0, numberPicker.c0, numberPicker.getRight(), NumberPicker.this.getBottom());
            }
        }

        @DexIgnore
        public void b(int i) {
            a();
            this.b = 2;
            this.a = i;
            NumberPicker.this.post(this);
        }

        @DexIgnore
        public void run() {
            int i = this.b;
            if (i == 1) {
                int i2 = this.a;
                if (i2 == 1) {
                    NumberPicker numberPicker = NumberPicker.this;
                    numberPicker.e0 = true;
                    numberPicker.invalidate(0, numberPicker.c0, numberPicker.getRight(), NumberPicker.this.getBottom());
                } else if (i2 == 2) {
                    NumberPicker numberPicker2 = NumberPicker.this;
                    numberPicker2.f0 = true;
                    numberPicker2.invalidate(0, 0, numberPicker2.getRight(), NumberPicker.this.b0);
                }
            } else if (i == 2) {
                int i3 = this.a;
                if (i3 == 1) {
                    NumberPicker numberPicker3 = NumberPicker.this;
                    if (!numberPicker3.e0) {
                        numberPicker3.postDelayed(this, (long) ViewConfiguration.getPressedStateDuration());
                    }
                    NumberPicker numberPicker4 = NumberPicker.this;
                    numberPicker4.e0 = !numberPicker4.e0;
                    numberPicker4.invalidate(0, numberPicker4.c0, numberPicker4.getRight(), NumberPicker.this.getBottom());
                } else if (i3 == 2) {
                    NumberPicker numberPicker5 = NumberPicker.this;
                    if (!numberPicker5.f0) {
                        numberPicker5.postDelayed(this, (long) ViewConfiguration.getPressedStateDuration());
                    }
                    NumberPicker numberPicker6 = NumberPicker.this;
                    numberPicker6.f0 = !numberPicker6.f0;
                    numberPicker6.invalidate(0, 0, numberPicker6.getRight(), NumberPicker.this.b0);
                }
            }
        }

        @DexIgnore
        public void a(int i) {
            a();
            this.b = 1;
            this.a = i;
            NumberPicker.this.postDelayed(this, (long) ViewConfiguration.getTapTimeout());
        }
    }

    @DexIgnore
    public static String g(int i2) {
        return String.format(Locale.getDefault(), "%d", Integer.valueOf(i2));
    }

    @DexIgnore
    public final void b(Scroller scroller) {
        if (Objects.equals(scroller, this.F)) {
            if (!a()) {
                l();
            }
            f(0);
        } else if (this.V != 1) {
            l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends AccessibilityNodeProvider {
        @DexIgnore
        public /* final */ Rect a; // = new Rect();
        @DexIgnore
        public /* final */ int[] b; // = new int[2];
        @DexIgnore
        public int c; // = RecyclerView.UNDEFINED_DURATION;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(int i, int i2) {
            if (i != 1) {
                if (i == 2) {
                    a(i2);
                } else if (i == 3 && d()) {
                    a(i, i2, b());
                }
            } else if (e()) {
                a(i, i2, c());
            }
        }

        @DexIgnore
        public final String b() {
            NumberPicker numberPicker = NumberPicker.this;
            int i = numberPicker.v - 1;
            if (numberPicker.Q) {
                i = numberPicker.d(i);
            }
            NumberPicker numberPicker2 = NumberPicker.this;
            int i2 = numberPicker2.t;
            if (i < i2) {
                return null;
            }
            String[] strArr = numberPicker2.s;
            return strArr == null ? numberPicker2.c(i) : strArr[i - i2];
        }

        @DexIgnore
        public final String c() {
            NumberPicker numberPicker = NumberPicker.this;
            int i = numberPicker.v + 1;
            if (numberPicker.Q) {
                i = numberPicker.d(i);
            }
            NumberPicker numberPicker2 = NumberPicker.this;
            if (i > numberPicker2.u) {
                return null;
            }
            String[] strArr = numberPicker2.s;
            return strArr == null ? numberPicker2.c(i) : strArr[i - numberPicker2.t];
        }

        @DexIgnore
        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            if (i == -1) {
                return a(NumberPicker.this.getScrollX(), NumberPicker.this.getScrollY(), NumberPicker.this.getScrollX() + (NumberPicker.this.getRight() - NumberPicker.this.getLeft()), NumberPicker.this.getScrollY() + (NumberPicker.this.getBottom() - NumberPicker.this.getTop()));
            }
            if (i == 1) {
                String c2 = c();
                int scrollX = NumberPicker.this.getScrollX();
                NumberPicker numberPicker = NumberPicker.this;
                return a(1, c2, scrollX, numberPicker.c0 - numberPicker.U, numberPicker.getScrollX() + (NumberPicker.this.getRight() - NumberPicker.this.getLeft()), NumberPicker.this.getScrollY() + (NumberPicker.this.getBottom() - NumberPicker.this.getTop()));
            } else if (i == 2) {
                return a();
            } else {
                if (i != 3) {
                    return super.createAccessibilityNodeInfo(i);
                }
                String b2 = b();
                int scrollX2 = NumberPicker.this.getScrollX();
                int scrollY = NumberPicker.this.getScrollY();
                int scrollX3 = NumberPicker.this.getScrollX() + (NumberPicker.this.getRight() - NumberPicker.this.getLeft());
                NumberPicker numberPicker2 = NumberPicker.this;
                return a(3, b2, scrollX2, scrollY, scrollX3, numberPicker2.b0 + numberPicker2.U);
            }
        }

        @DexIgnore
        public final boolean d() {
            return NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() > NumberPicker.this.getMinValue();
        }

        @DexIgnore
        public final boolean e() {
            return NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() < NumberPicker.this.getMaxValue();
        }

        @DexIgnore
        @Override // android.view.accessibility.AccessibilityNodeProvider
        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            if (TextUtils.isEmpty(str)) {
                return Collections.emptyList();
            }
            String lowerCase = str.toLowerCase();
            ArrayList arrayList = new ArrayList();
            if (i == -1) {
                a(lowerCase, 3, arrayList);
                a(lowerCase, 2, arrayList);
                a(lowerCase, 1, arrayList);
                return arrayList;
            } else if (i != 1 && i != 2 && i != 3) {
                return super.findAccessibilityNodeInfosByText(str, i);
            } else {
                a(lowerCase, i, arrayList);
                return arrayList;
            }
        }

        @DexIgnore
        public boolean performAction(int i, int i2, Bundle bundle) {
            if (i != -1) {
                if (i != 1) {
                    if (i != 2) {
                        if (i == 3) {
                            if (i2 != 16) {
                                if (i2 != 64) {
                                    if (i2 != 128 || this.c != i) {
                                        return false;
                                    }
                                    this.c = RecyclerView.UNDEFINED_DURATION;
                                    a(i, 65536);
                                    NumberPicker numberPicker = NumberPicker.this;
                                    numberPicker.invalidate(0, 0, numberPicker.getRight(), NumberPicker.this.b0);
                                    return true;
                                } else if (this.c == i) {
                                    return false;
                                } else {
                                    this.c = i;
                                    a(i, 32768);
                                    NumberPicker numberPicker2 = NumberPicker.this;
                                    numberPicker2.invalidate(0, 0, numberPicker2.getRight(), NumberPicker.this.b0);
                                    return true;
                                }
                            } else if (!NumberPicker.this.isEnabled()) {
                                return false;
                            } else {
                                NumberPicker.this.a(false);
                                a(i, 1);
                                return true;
                            }
                        }
                    } else if (i2 != 1) {
                        if (i2 != 2) {
                            if (i2 != 16) {
                                if (i2 != 64) {
                                    if (i2 != 128) {
                                        return NumberPicker.this.e.performAccessibilityAction(i2, bundle);
                                    }
                                    if (this.c != i) {
                                        return false;
                                    }
                                    this.c = RecyclerView.UNDEFINED_DURATION;
                                    a(i, 65536);
                                    NumberPicker.this.e.invalidate();
                                    return true;
                                } else if (this.c == i) {
                                    return false;
                                } else {
                                    this.c = i;
                                    a(i, 32768);
                                    NumberPicker.this.e.invalidate();
                                    return true;
                                }
                            } else if (!NumberPicker.this.isEnabled()) {
                                return false;
                            } else {
                                NumberPicker.this.j();
                                return true;
                            }
                        } else if (!NumberPicker.this.isEnabled() || !NumberPicker.this.e.isFocused()) {
                            return false;
                        } else {
                            NumberPicker.this.e.clearFocus();
                            return true;
                        }
                    } else if (!NumberPicker.this.isEnabled() || NumberPicker.this.e.isFocused()) {
                        return false;
                    } else {
                        return NumberPicker.this.e.requestFocus();
                    }
                } else if (i2 != 16) {
                    if (i2 != 64) {
                        if (i2 != 128 || this.c != i) {
                            return false;
                        }
                        this.c = RecyclerView.UNDEFINED_DURATION;
                        a(i, 65536);
                        NumberPicker numberPicker3 = NumberPicker.this;
                        numberPicker3.invalidate(0, numberPicker3.c0, numberPicker3.getRight(), NumberPicker.this.getBottom());
                        return true;
                    } else if (this.c == i) {
                        return false;
                    } else {
                        this.c = i;
                        a(i, 32768);
                        NumberPicker numberPicker4 = NumberPicker.this;
                        numberPicker4.invalidate(0, numberPicker4.c0, numberPicker4.getRight(), NumberPicker.this.getBottom());
                        return true;
                    }
                } else if (!NumberPicker.this.isEnabled()) {
                    return false;
                } else {
                    NumberPicker.this.a(true);
                    a(i, 1);
                    return true;
                }
            } else if (i2 != 64) {
                if (i2 != 128) {
                    if (i2 != 4096) {
                        if (i2 == 8192) {
                            if (!NumberPicker.this.isEnabled() || (!NumberPicker.this.getWrapSelectorWheel() && NumberPicker.this.getValue() <= NumberPicker.this.getMinValue())) {
                                return false;
                            }
                            NumberPicker.this.a(false);
                            return true;
                        }
                    } else if (!NumberPicker.this.isEnabled() || (!NumberPicker.this.getWrapSelectorWheel() && NumberPicker.this.getValue() >= NumberPicker.this.getMaxValue())) {
                        return false;
                    } else {
                        NumberPicker.this.a(true);
                        return true;
                    }
                } else if (this.c != i) {
                    return false;
                } else {
                    this.c = RecyclerView.UNDEFINED_DURATION;
                    NumberPicker.this.performAccessibilityAction(128, null);
                    return true;
                }
            } else if (this.c == i) {
                return false;
            } else {
                this.c = i;
                NumberPicker.this.performAccessibilityAction(64, null);
                return true;
            }
            return super.performAction(i, i2, bundle);
        }

        @DexIgnore
        public final void a(int i) {
            if (((AccessibilityManager) NumberPicker.this.getContext().getSystemService("accessibility")).isEnabled()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i);
                NumberPicker.this.e.onInitializeAccessibilityEvent(obtain);
                NumberPicker.this.e.onPopulateAccessibilityEvent(obtain);
                obtain.setSource(NumberPicker.this, 2);
                NumberPicker numberPicker = NumberPicker.this;
                numberPicker.requestSendAccessibilityEvent(numberPicker, obtain);
            }
        }

        @DexIgnore
        public final void a(int i, int i2, String str) {
            if (((AccessibilityManager) NumberPicker.this.getContext().getSystemService("accessibility")).isEnabled()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
                obtain.setClassName(Button.class.getName());
                obtain.setPackageName(NumberPicker.this.getContext().getPackageName());
                obtain.getText().add(str);
                obtain.setEnabled(NumberPicker.this.isEnabled());
                obtain.setSource(NumberPicker.this, i);
                NumberPicker numberPicker = NumberPicker.this;
                numberPicker.requestSendAccessibilityEvent(numberPicker, obtain);
            }
        }

        @DexIgnore
        public final void a(String str, int i, List<AccessibilityNodeInfo> list) {
            if (i == 1) {
                String c2 = c();
                if (!TextUtils.isEmpty(c2) && c2.toLowerCase().contains(str)) {
                    list.add(createAccessibilityNodeInfo(1));
                }
            } else if (i == 2) {
                Editable text = NumberPicker.this.e.getText();
                if (TextUtils.isEmpty(text) || !text.toString().toLowerCase().contains(str)) {
                    Editable text2 = NumberPicker.this.e.getText();
                    if (!TextUtils.isEmpty(text2) && text2.toString().toLowerCase().contains(str)) {
                        list.add(createAccessibilityNodeInfo(2));
                        return;
                    }
                    return;
                }
                list.add(createAccessibilityNodeInfo(2));
            } else if (i == 3) {
                String b2 = b();
                if (!TextUtils.isEmpty(b2) && b2.toLowerCase().contains(str)) {
                    list.add(createAccessibilityNodeInfo(3));
                }
            }
        }

        @DexIgnore
        public final AccessibilityNodeInfo a() {
            AccessibilityNodeInfo createAccessibilityNodeInfo = NumberPicker.this.e.createAccessibilityNodeInfo();
            createAccessibilityNodeInfo.setSource(NumberPicker.this, 2);
            if (this.c != 2) {
                createAccessibilityNodeInfo.addAction(64);
            }
            if (this.c == 2) {
                createAccessibilityNodeInfo.addAction(128);
            }
            return createAccessibilityNodeInfo;
        }

        @DexIgnore
        public final AccessibilityNodeInfo a(int i, String str, int i2, int i3, int i4, int i5) {
            AccessibilityNodeInfo obtain = AccessibilityNodeInfo.obtain();
            obtain.setClassName(Button.class.getName());
            obtain.setPackageName(NumberPicker.this.getContext().getPackageName());
            obtain.setSource(NumberPicker.this, i);
            obtain.setParent(NumberPicker.this);
            obtain.setText(str);
            obtain.setClickable(true);
            obtain.setLongClickable(true);
            obtain.setEnabled(NumberPicker.this.isEnabled());
            Rect rect = this.a;
            rect.set(i2, i3, i4, i5);
            obtain.setBoundsInParent(rect);
            int[] iArr = this.b;
            NumberPicker.this.getLocationOnScreen(iArr);
            rect.offset(iArr[0], iArr[1]);
            obtain.setBoundsInScreen(rect);
            if (this.c != i) {
                obtain.addAction(64);
            }
            if (this.c == i) {
                obtain.addAction(128);
            }
            if (NumberPicker.this.isEnabled()) {
                obtain.addAction(16);
            }
            return obtain;
        }

        @DexIgnore
        public final AccessibilityNodeInfo a(int i, int i2, int i3, int i4) {
            AccessibilityNodeInfo obtain = AccessibilityNodeInfo.obtain();
            obtain.setClassName(NumberPicker.class.getName());
            obtain.setPackageName(NumberPicker.this.getContext().getPackageName());
            obtain.setSource(NumberPicker.this);
            if (d()) {
                obtain.addChild(NumberPicker.this, 3);
            }
            obtain.addChild(NumberPicker.this, 2);
            if (e()) {
                obtain.addChild(NumberPicker.this, 1);
            }
            obtain.setParent((View) NumberPicker.this.getParentForAccessibility());
            obtain.setEnabled(NumberPicker.this.isEnabled());
            obtain.setScrollable(true);
            if (this.c != -1) {
                obtain.addAction(64);
            }
            if (this.c == -1) {
                obtain.addAction(128);
            }
            if (NumberPicker.this.isEnabled()) {
                if (NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() < NumberPicker.this.getMaxValue()) {
                    obtain.addAction(4096);
                }
                if (NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() > NumberPicker.this.getMinValue()) {
                    obtain.addAction(8192);
                }
            }
            return obtain;
        }
    }

    @DexIgnore
    public final int a(int i2, int i3) {
        if (i3 == -1) {
            return i2;
        }
        int size = View.MeasureSpec.getSize(i2);
        int mode = View.MeasureSpec.getMode(i2);
        if (mode == Integer.MIN_VALUE) {
            return View.MeasureSpec.makeMeasureSpec(Math.min(size, i3), 1073741824);
        }
        if (mode == 0) {
            return View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
        }
        if (mode == 1073741824) {
            return i2;
        }
        throw new IllegalArgumentException("Unknown measure mode: " + mode);
    }

    @DexIgnore
    public final void e(int i2) {
        g gVar = this.w;
        if (gVar != null) {
            gVar.a(this, i2, this.v);
        }
    }

    @DexIgnore
    public final int a(int i2, int i3, int i4) {
        return i2 != -1 ? resolveSizeAndState(Math.max(i2, i3), i4, 0) : i3;
    }

    @DexIgnore
    public final void b(int i2) {
        this.H = 0;
        if (i2 > 0) {
            this.F.fling(0, 0, 0, i2, 0, 0, 0, Integer.MAX_VALUE);
        } else {
            this.F.fling(0, Integer.MAX_VALUE, 0, i2, 0, 0, 0, Integer.MAX_VALUE);
        }
        invalidate();
    }

    @DexIgnore
    public int d(int i2) {
        int i3 = this.u;
        if (i2 > i3) {
            int i4 = this.t;
            return (i4 + ((i2 - i3) % (i3 - i4))) - 1;
        }
        int i5 = this.t;
        return i2 < i5 ? (i3 - ((i5 - i2) % (i3 - i5))) + 1 : i2;
    }

    @DexIgnore
    public final void a(int i2, boolean z2) {
        int i3;
        if (this.v != i2) {
            if (this.Q) {
                i3 = d(i2);
            } else {
                i3 = Math.min(Math.max(i2, this.t), this.u);
            }
            int i4 = this.v;
            this.v = i3;
            l();
            if (z2) {
                e(i4);
            }
            e();
            invalidate();
        }
    }

    @DexIgnore
    public final void b(int[] iArr) {
        System.arraycopy(iArr, 1, iArr, 0, iArr.length - 1);
        int i2 = iArr[iArr.length - 2] + 1;
        if (this.Q && i2 > this.u) {
            i2 = this.t;
        }
        iArr[iArr.length - 1] = i2;
        a(i2);
    }

    @DexIgnore
    public void a(boolean z2) {
        if (this.S) {
            this.e.setVisibility(4);
            if (!a(this.F)) {
                a(this.G);
            }
            this.H = 0;
            if (z2) {
                this.F.startScroll(0, 0, 0, -this.C, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS);
            } else {
                this.F.startScroll(0, 0, 0, this.C, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS);
            }
            invalidate();
        } else if (z2) {
            a(this.v + 1, true);
        } else {
            a(this.v - 1, true);
        }
    }

    @DexIgnore
    public final void a(int[] iArr) {
        System.arraycopy(iArr, 0, iArr, 1, iArr.length - 1);
        int i2 = iArr[1] - 1;
        if (this.Q && i2 < this.t) {
            i2 = this.u;
        }
        iArr[0] = i2;
        a(i2);
    }

    @DexIgnore
    public final void a(int i2) {
        String str;
        SparseArray<String> sparseArray = this.y;
        if (sparseArray.get(i2) == null) {
            int i3 = this.t;
            if (i2 < i3 || i2 > this.u) {
                str = "";
            } else {
                String[] strArr = this.s;
                str = strArr != null ? strArr[i2 - i3] : c(i2);
            }
            sparseArray.put(i2, str);
        }
    }

    @DexIgnore
    public void a(boolean z2, long j2) {
        e eVar = this.I;
        if (eVar == null) {
            this.I = new e();
        } else {
            removeCallbacks(eVar);
        }
        this.I.a(z2);
        postDelayed(this.I, j2);
    }

    @DexIgnore
    public final boolean a() {
        int i2 = this.D - this.E;
        if (i2 == 0) {
            return false;
        }
        this.H = 0;
        int abs = Math.abs(i2);
        int i3 = this.C;
        if (abs > i3 / 2) {
            if (i2 > 0) {
                i3 = -i3;
            }
            i2 += i3;
        }
        this.G.startScroll(0, 0, 0, i2, 800);
        invalidate();
        return true;
    }

    @DexIgnore
    public final void a(Paint paint, float f2, String str) {
        paint.setTextSize((float) this.q);
        Rect rect = new Rect();
        paint.getTextBounds(str, 0, str.length(), rect);
        if (((float) rect.width()) >= f2) {
            paint.setTextSize((paint.getTextSize() * f2) / ((float) rect.width()));
        }
    }
}
