package com.portfolio.platform.helper;

import com.fossil.ee4;
import com.fossil.ee7;
import com.fossil.fe4;
import com.fossil.i97;
import com.fossil.le4;
import com.fossil.me4;
import com.fossil.ne4;
import com.fossil.zd5;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonISOConvertDateTime implements fe4<Date>, ne4<Date> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(Date date, Type type, me4 me4) {
        String str;
        if (date == null) {
            str = "";
        } else {
            SimpleDateFormat simpleDateFormat = zd5.o.get();
            if (simpleDateFormat != null) {
                str = simpleDateFormat.format(date);
                ee7.a((Object) str, "DateHelper.SERVER_DATE_I\u2026ORMAT.get()!!.format(src)");
            } else {
                ee7.a();
                throw null;
            }
        }
        return new le4(str);
    }

    @DexIgnore
    @Override // com.fossil.fe4
    public Date deserialize(JsonElement jsonElement, Type type, ee4 ee4) {
        String f;
        if (!(jsonElement == null || (f = jsonElement.f()) == null)) {
            try {
                SimpleDateFormat simpleDateFormat = zd5.o.get();
                if (simpleDateFormat != null) {
                    Date parse = simpleDateFormat.parse(f);
                    SimpleDateFormat simpleDateFormat2 = zd5.m.get();
                    if (simpleDateFormat2 != null) {
                        SimpleDateFormat simpleDateFormat3 = simpleDateFormat2;
                        SimpleDateFormat simpleDateFormat4 = zd5.m.get();
                        if (simpleDateFormat4 != null) {
                            Date parse2 = simpleDateFormat3.parse(simpleDateFormat4.format(parse));
                            ee7.a((Object) parse2, "DateHelper.LOCAL_DATE_SP\u2026MAT.get()!!.format(date))");
                            return parse2;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            } catch (ParseException e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("deserialize - json=");
                sb.append(jsonElement.f());
                sb.append(", e=");
                e.printStackTrace();
                sb.append(i97.a);
                local.e("GsonISOConvertDateTime", sb.toString());
            }
        }
        return new Date(0);
    }
}
