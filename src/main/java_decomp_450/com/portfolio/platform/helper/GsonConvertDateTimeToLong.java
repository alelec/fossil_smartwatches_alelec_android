package com.portfolio.platform.helper;

import com.fossil.fe4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConvertDateTimeToLong implements fe4<Long> {
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:8:0x0020 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v0, types: [com.google.gson.JsonElement, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r5v3, types: [com.google.gson.JsonElement] */
    /* JADX WARN: Type inference failed for: r5v9, types: [long] */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:8|9|10|11|14|15|16) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0040, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0041, code lost:
        r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r2 = new java.lang.StringBuilder();
        r2.append("deserialize - json=");
        r2.append(r5.f());
        r2.append(", e=");
        r6.printStackTrace();
        r2.append(com.fossil.i97.a);
        r7.e("GsonConvertDateTimeToLong", r2.toString());
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x002e */
    @Override // com.fossil.fe4
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Long deserialize(com.google.gson.JsonElement r5, java.lang.reflect.Type r6, com.fossil.ee4 r7) {
        /*
            r4 = this;
            java.lang.String r6 = "json"
            com.fossil.ee7.b(r5, r6)
            java.lang.String r6 = r5.f()
            java.lang.String r7 = "dateAsString"
            com.fossil.ee7.a(r6, r7)
            int r7 = r6.length()
            if (r7 != 0) goto L_0x0016
            r7 = 1
            goto L_0x0017
        L_0x0016:
            r7 = 0
        L_0x0017:
            r0 = 0
            if (r7 == 0) goto L_0x0020
            java.lang.Long r5 = java.lang.Long.valueOf(r0)
            return r5
        L_0x0020:
            org.joda.time.DateTime r6 = com.fossil.zd5.c(r6)     // Catch:{ Exception -> 0x002e }
            java.lang.String r7 = "DateHelper.getServerDate\u2026ffsetParsed(dateAsString)"
            com.fossil.ee7.a(r6, r7)     // Catch:{ Exception -> 0x002e }
            long r5 = r6.getMillis()     // Catch:{ Exception -> 0x002e }
            goto L_0x006f
        L_0x002e:
            java.lang.String r6 = r5.f()     // Catch:{ Exception -> 0x0040 }
            org.joda.time.DateTime r6 = com.fossil.zd5.b(r6)     // Catch:{ Exception -> 0x0040 }
            java.lang.String r7 = "DateHelper.getLocalDateT\u2026DateFormat(json.asString)"
            com.fossil.ee7.a(r6, r7)     // Catch:{ Exception -> 0x0040 }
            long r0 = r6.getMillis()     // Catch:{ Exception -> 0x0040 }
            goto L_0x006e
        L_0x0040:
            r6 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "deserialize - json="
            r2.append(r3)
            java.lang.String r5 = r5.f()
            r2.append(r5)
            java.lang.String r5 = ", e="
            r2.append(r5)
            r6.printStackTrace()
            com.fossil.i97 r5 = com.fossil.i97.a
            r2.append(r5)
            java.lang.String r5 = r2.toString()
            java.lang.String r6 = "GsonConvertDateTimeToLong"
            r7.e(r6, r5)
        L_0x006e:
            r5 = r0
        L_0x006f:
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.helper.GsonConvertDateTimeToLong.deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.fossil.ee4):java.lang.Long");
    }
}
