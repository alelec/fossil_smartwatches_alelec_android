package com.portfolio.platform.gson;

import android.text.TextUtils;
import com.fossil.de4;
import com.fossil.ee7;
import com.fossil.he4;
import com.fossil.ie4;
import com.fossil.ke4;
import com.fossil.me4;
import com.fossil.ne4;
import com.fossil.x87;
import com.google.gson.JsonElement;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import java.lang.reflect.Type;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetWatchAppSettingSerializer implements ne4<List<? extends DianaPresetWatchAppSetting>> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(List<DianaPresetWatchAppSetting> list, Type type, me4 me4) {
        ee7.b(list, "src");
        de4 de4 = new de4();
        ke4 ke4 = new ke4();
        for (DianaPresetWatchAppSetting dianaPresetWatchAppSetting : list) {
            String component1 = dianaPresetWatchAppSetting.component1();
            String component2 = dianaPresetWatchAppSetting.component2();
            String component3 = dianaPresetWatchAppSetting.component3();
            String component4 = dianaPresetWatchAppSetting.component4();
            ie4 ie4 = new ie4();
            ie4.a("buttonPosition", component1);
            ie4.a("appId", component2);
            ie4.a("localUpdatedAt", component3);
            if (!TextUtils.isEmpty(component4)) {
                try {
                    JsonElement a = ke4.a(component4);
                    if (a != null) {
                        ie4.a(Constants.USER_SETTING, (ie4) a);
                    } else {
                        throw new x87("null cannot be cast to non-null type com.google.gson.JsonObject");
                    }
                } catch (Exception unused) {
                    ie4.a(Constants.USER_SETTING, new he4());
                }
            } else {
                ie4.a(Constants.USER_SETTING, new he4());
            }
            de4.a(ie4);
        }
        return de4;
    }
}
