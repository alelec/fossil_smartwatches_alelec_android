package com.portfolio.platform.gson;

import com.fossil.ee4;
import com.fossil.ee7;
import com.fossil.fe4;
import com.fossil.ie4;
import com.fossil.zd7;
import com.google.gson.JsonElement;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeDeserializer implements fe4<Theme> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.fe4
    public Theme deserialize(JsonElement jsonElement, Type type, ee4 ee4) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        ie4 d = jsonElement != null ? jsonElement.d() : null;
        if (d == null) {
            return null;
        }
        if (d.d("id")) {
            JsonElement a2 = d.a("id");
            ee7.a((Object) a2, "jsonObject.get(Constants.JSON_KEY_ID)");
            str = a2.f();
        } else {
            str = "";
        }
        if (d.d("name")) {
            JsonElement a3 = d.a("name");
            ee7.a((Object) a3, "jsonObject.get(Constants.JSON_KEY_NAME)");
            str2 = a3.f();
        } else {
            str2 = "";
        }
        ArrayList arrayList = new ArrayList();
        if (ee4 != null && d.d("styles")) {
            Iterator<JsonElement> it = d.b("styles").iterator();
            while (it.hasNext()) {
                JsonElement next = it.next();
                ee7.a((Object) next, "item");
                ie4 d2 = next.d();
                if (d2.d("key")) {
                    JsonElement a4 = d2.a("key");
                    ee7.a((Object) a4, "itemJsonObject.get(Constants.JSON_KEY_KEY)");
                    str3 = a4.f();
                } else {
                    str3 = "";
                }
                if (d2.d("type")) {
                    JsonElement a5 = d2.a("type");
                    ee7.a((Object) a5, "itemJsonObject.get(Constants.JSON_KEY_TYPE)");
                    str4 = a5.f();
                } else {
                    str4 = "";
                }
                if (d2.d("value")) {
                    JsonElement a6 = d2.a("value");
                    if (a6 instanceof ie4) {
                        ie4 ie4 = (ie4) a6;
                        if (ie4.d("fileName")) {
                            JsonElement a7 = ie4.a("fileName");
                            ee7.a((Object) a7, "valueJsonElement.get(Constants.JSON_KEY_FILE_NAME)");
                            str5 = a7.f();
                            ee7.a((Object) str5, "valueJsonElement.get(Con\u2026N_KEY_FILE_NAME).asString");
                        }
                    } else {
                        ee7.a((Object) a6, "valueJsonElement");
                        str5 = a6.f();
                        ee7.a((Object) str5, "valueJsonElement.asString");
                    }
                    ee7.a((Object) str3, "key");
                    ee7.a((Object) str4, "type");
                    arrayList.add(new Style(str3, str4, str5));
                }
                str5 = "";
                ee7.a((Object) str3, "key");
                ee7.a((Object) str4, "type");
                arrayList.add(new Style(str3, str4, str5));
            }
        }
        ee7.a((Object) str, "id");
        ee7.a((Object) str2, "name");
        return new Theme(str, str2, arrayList);
    }
}
