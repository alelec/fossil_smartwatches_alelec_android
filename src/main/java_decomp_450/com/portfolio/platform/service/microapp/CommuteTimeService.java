package com.portfolio.platform.service.microapp;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import com.facebook.applinks.AppLinkData;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.VideoUploader;
import com.fossil.b53;
import com.fossil.bk5;
import com.fossil.c53;
import com.fossil.d53;
import com.fossil.dk5;
import com.fossil.e53;
import com.fossil.ee7;
import com.fossil.f53;
import com.fossil.fb7;
import com.fossil.g53;
import com.fossil.gn5;
import com.fossil.ho3;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.io3;
import com.fossil.jo3;
import com.fossil.k53;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.no3;
import com.fossil.qj7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.v6;
import com.fossil.w02;
import com.fossil.x87;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.gson.Gson;
import com.google.maps.model.TravelMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.Mapping;
import com.misfit.frameworks.buttonservice.model.microapp.CommuteTimeETAMicroAppResponse;
import com.misfit.frameworks.buttonservice.model.microapp.CommuteTimeTravelMicroAppResponse;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeService extends bk5 implements d53, LocationListener {
    @DexIgnore
    public static /* final */ float A; // = 50.0f;
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ int y; // = 120000;
    @DexIgnore
    public static /* final */ int z; // = z;
    @DexIgnore
    public Location d;
    @DexIgnore
    public Location e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public String g;
    @DexIgnore
    public long h;
    @DexIgnore
    public CommuteTimeSetting i;
    @DexIgnore
    public LocationManager j;
    @DexIgnore
    public String p;
    @DexIgnore
    public Handler q;
    @DexIgnore
    public b53 r;
    @DexIgnore
    public k53 s;
    @DexIgnore
    public LocationRequest t;
    @DexIgnore
    public f53 u;
    @DexIgnore
    public c53 v;
    @DexIgnore
    public gn5 w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return CommuteTimeService.x;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, Bundle bundle, dk5 dk5) {
            ee7.b(context, "context");
            ee7.b(bundle, Mapping.COLUMN_EXTRA_INFO);
            ee7.b(dk5, "listener");
            Intent intent = new Intent(context, CommuteTimeService.class);
            intent.putExtras(bundle);
            context.startService(intent);
            bk5.a(dk5);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends c53 {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        @Override // com.fossil.c53
        public void onLocationResult(LocationResult locationResult) {
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.B.a(), "onLocationResult");
            super.onLocationResult(locationResult);
            CommuteTimeService commuteTimeService = this.a;
            if (locationResult != null) {
                commuteTimeService.b(locationResult.e());
                if (this.a.i() != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = CommuteTimeService.B.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("onLocationResult lastLocation=");
                    Location i = this.a.i();
                    if (i != null) {
                        sb.append(i);
                        local.d(a2, sb.toString());
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.B.a(), "onLocationResult lastLocation is null");
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.microapp.CommuteTimeService$getDurationTime$1", f = "CommuteTimeService.kt", l = {}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Location $location;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(CommuteTimeService commuteTimeService, Location location, fb7 fb7) {
            super(2, fb7);
            this.this$0 = commuteTimeService;
            this.$location = location;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$location, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                if (this.this$0.i != null) {
                    CommuteTimeSetting a = this.this$0.i;
                    if (a != null) {
                        if (!(a.getAddress().length() == 0)) {
                            gn5 h = this.this$0.h();
                            if (h != null) {
                                CommuteTimeSetting a2 = this.this$0.i;
                                if (a2 != null) {
                                    String address = a2.getAddress();
                                    TravelMode travelMode = TravelMode.DRIVING;
                                    CommuteTimeSetting a3 = this.this$0.i;
                                    if (a3 != null) {
                                        long a4 = h.a(address, travelMode, a3.getAvoidTolls(), this.$location.getLatitude(), this.$location.getLongitude());
                                        if (a4 != -1) {
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String a5 = CommuteTimeService.B.a();
                                            local.d(a5, "getDurationTime duration " + a4);
                                            this.this$0.h = a4;
                                            if (this.this$0.h < ((long) 60)) {
                                                this.this$0.h = 60;
                                            }
                                            this.this$0.k();
                                        } else {
                                            this.this$0.a();
                                        }
                                    } else {
                                        ee7.a();
                                        throw null;
                                    }
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<TResult> implements ho3<Location> {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ho3
            public final void onComplete(no3<Location> no3) {
                ee7.b(no3, "task");
                if (!no3.e() || no3.b() == null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = CommuteTimeService.B.a();
                    local.d(a2, "getLastLocation:exception" + no3.a());
                    this.a.a.a();
                    return;
                }
                CommuteTimeService commuteTimeService = this.a.a;
                Location i = commuteTimeService.i();
                if (i != null) {
                    commuteTimeService.b(commuteTimeService.a(i, no3.b()));
                    long currentTimeMillis = System.currentTimeMillis();
                    Location i2 = this.a.a.i();
                    if (i2 == null) {
                        ee7.a();
                        throw null;
                    } else if (currentTimeMillis - i2.getTime() > ((long) CommuteTimeService.z)) {
                        FLogger.INSTANCE.getLocal().d(CommuteTimeService.B.a(), "Runnable after 5s over 5 mins not trust");
                        this.a.a.a();
                    } else {
                        CommuteTimeService commuteTimeService2 = this.a.a;
                        Location i3 = commuteTimeService2.i();
                        if (i3 != null) {
                            commuteTimeService2.a(i3);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public d(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        public final void run() {
            no3<Location> i;
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.B.a(), "Runnable after 5s");
            if (this.a.d == null && this.a.j != null) {
                LocationManager f = this.a.j;
                if (f != null) {
                    f.removeUpdates(this.a);
                    if (v6.a(PortfolioApp.g0.c(), "android.permission.ACCESS_FINE_LOCATION") == 0 && v6.a(PortfolioApp.g0.c(), "android.permission.ACCESS_COARSE_LOCATION") == 0 && LocationUtils.isLocationEnable(PortfolioApp.g0.c())) {
                        CommuteTimeService commuteTimeService = this.a;
                        LocationManager f2 = commuteTimeService.j;
                        if (f2 != null) {
                            commuteTimeService.b(f2.getLastKnownLocation(this.a.p));
                            if (this.a.i() != null) {
                                b53 c = this.a.r;
                                if (c != null && (i = c.i()) != null) {
                                    i.a(new a(this));
                                    return;
                                }
                                return;
                            }
                            this.a.a();
                            return;
                        }
                        ee7.a();
                        throw null;
                    }
                    FLogger.INSTANCE.getLocal().d(CommuteTimeService.B.a(), "Runnable after 5s permission not granted");
                    this.a.a();
                    return;
                }
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<TResult> implements jo3<g53> {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        public e(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onSuccess(g53 g53) {
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.B.a(), "All location settings are satisfied");
            if (v6.a(this.a, "android.permission.ACCESS_FINE_LOCATION") == 0 || v6.a(this.a, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
                b53 c = this.a.r;
                if (c != null) {
                    c.a(this.a.t, this.a.v, Looper.myLooper());
                    return;
                }
                return;
            }
            this.a.a();
            this.a.a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements io3 {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        public f(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        @Override // com.fossil.io3
        public final void onFailure(Exception exc) {
            ee7.b(exc, "exception");
            int statusCode = ((w02) exc).getStatusCode();
            if (statusCode == 6) {
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.B.a(), "Location settings are not satisfied. Attempting to upgrade location settings ");
                this.a.a();
            } else if (statusCode == 8502) {
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.B.a(), "Location settings are inadequate, and cannot be fixed here. Fix in Settings.");
                this.a.a(false);
                this.a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<TResult> implements ho3<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        public g(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        @Override // com.fossil.ho3
        public final void onComplete(no3<Void> no3) {
            ee7.b(no3, "it");
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.B.a(), "stopLocationUpdates success");
            this.a.a(false);
        }
    }

    /*
    static {
        String simpleName = CommuteTimeService.class.getSimpleName();
        ee7.a((Object) simpleName, "CommuteTimeService::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    public final void j() {
        FLogger.INSTANCE.getLocal().d(x, "initLocationManager");
        if (v6.a(PortfolioApp.g0.c(), "android.permission.ACCESS_FINE_LOCATION") == 0 || v6.a(PortfolioApp.g0.c(), "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            Criteria criteria = new Criteria();
            criteria.setPowerRequirement(1);
            Object systemService = getSystemService(PlaceFields.LOCATION);
            if (systemService != null) {
                LocationManager locationManager = (LocationManager) systemService;
                this.j = locationManager;
                boolean z2 = false;
                if (locationManager != null) {
                    if (locationManager != null) {
                        z2 = locationManager.isProviderEnabled("network");
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                if (z2) {
                    criteria.setAccuracy(2);
                } else {
                    criteria.setAccuracy(1);
                }
                LocationManager locationManager2 = this.j;
                if (locationManager2 != null) {
                    String bestProvider = locationManager2.getBestProvider(criteria, true);
                    this.p = bestProvider;
                    if (this.j != null && bestProvider != null) {
                        FLogger.INSTANCE.getLocal().d(x, "mLocationManager");
                        LocationManager locationManager3 = this.j;
                        if (locationManager3 != null) {
                            locationManager3.requestLocationUpdates(this.p, 0, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                throw new x87("null cannot be cast to non-null type android.location.LocationManager");
            }
        } else {
            a();
        }
    }

    @DexIgnore
    public final void k() {
        FLogger.INSTANCE.getLocal().d(x, "playHands");
        CommuteTimeSetting commuteTimeSetting = this.i;
        if (commuteTimeSetting == null) {
            ee7.a();
            throw null;
        } else if (ee7.a((Object) commuteTimeSetting.getFormat(), (Object) "travel")) {
            m();
        } else {
            l();
        }
    }

    @DexIgnore
    public final void l() {
        FLogger.INSTANCE.getLocal().d(x, "playHandsETA");
        long currentTimeMillis = System.currentTimeMillis() + (this.h * ((long) 1000));
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "calendar");
        instance.setTimeInMillis(currentTimeMillis);
        int i2 = instance.get(11) % 12;
        int i3 = instance.get(12);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "playHandsETA - duration=" + this.h + ", hour=" + i2 + ", minute=" + i3);
        try {
            PortfolioApp c2 = PortfolioApp.g0.c();
            String str2 = this.g;
            if (str2 != null) {
                c2.a(str2, new CommuteTimeETAMicroAppResponse(i2, i3));
                a();
                return;
            }
            ee7.a();
            throw null;
        } catch (IllegalArgumentException e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = x;
            local2.d(str3, "playHandsETA exception exception=" + e2.getMessage());
        }
    }

    @DexIgnore
    public final void m() {
        FLogger.INSTANCE.getLocal().d(x, "playHandsMinute");
        int round = Math.round(((float) this.h) / 60.0f);
        try {
            PortfolioApp c2 = PortfolioApp.g0.c();
            String str = this.g;
            if (str != null) {
                c2.a(str, new CommuteTimeTravelMicroAppResponse(round));
                a();
                return;
            }
            ee7.a();
            throw null;
        } catch (IllegalArgumentException e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = x;
            local.d(str2, "playHandsMinute exception exception=" + e2.getMessage());
        }
    }

    @DexIgnore
    public final void n() {
        FLogger.INSTANCE.getLocal().d(x, "startLocationUpdates");
        if (v6.a(PortfolioApp.g0.c(), "android.permission.ACCESS_FINE_LOCATION") == 0 && v6.a(PortfolioApp.g0.c(), "android.permission.ACCESS_COARSE_LOCATION") == 0 && LocationUtils.isLocationEnable(PortfolioApp.g0.c())) {
            this.f = true;
            k53 k53 = this.s;
            if (k53 != null) {
                no3<g53> a2 = k53.a(this.u);
                a2.a(new e(this));
                a2.a(new f(this));
                return;
            }
            ee7.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(x, "startLocationUpdates permission not granted");
        a();
        this.f = false;
    }

    @DexIgnore
    public final void o() {
        no3<Void> a2;
        if (!this.f) {
            FLogger.INSTANCE.getLocal().d(x, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }
        b53 b53 = this.r;
        if (b53 != null && (a2 = b53.a(this.v)) != null) {
            a2.a(new g(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.bk5
    public IBinder onBind(Intent intent) {
        ee7.b(intent, "intent");
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        FLogger.INSTANCE.getLocal().d(x, "onCreate");
        PortfolioApp.g0.c().f().a(this);
        this.q = new Handler();
        this.g = PortfolioApp.g0.c().c();
        this.r = e53.a(this);
        this.s = e53.b(this);
        f();
        g();
        e();
        j();
        n();
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(x, "onDestroy");
        super.onDestroy();
        o();
        Handler handler = this.q;
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            LocationManager locationManager = this.j;
            if (locationManager == null) {
                return;
            }
            if (locationManager != null) {
                locationManager.removeUpdates(this);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.d53
    public void onLocationChanged(Location location) {
        FLogger.INSTANCE.getLocal().d(x, "onLocationChanged");
        if (!(this.i == null || location == null || this.d != null)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = x;
            local.d(str, "onLocationChanged location=lat: " + location.getLatitude() + " long: " + location.getLongitude());
            this.d = location;
            if (location != null) {
                a(location);
            } else {
                ee7.a();
                throw null;
            }
        }
        LocationManager locationManager = this.j;
        if (locationManager == null) {
            return;
        }
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public void onProviderDisabled(String str) {
        ee7.b(str, "provider");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        local.d(str2, "onProviderDisabled provider=" + str);
    }

    @DexIgnore
    public void onProviderEnabled(String str) {
        ee7.b(str, "provider");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        local.d(str2, "onProviderEnabled provider=" + str);
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        Bundle extras;
        ee7.b(intent, "intent");
        FLogger.INSTANCE.getLocal().d(x, "onStartCommand");
        ((bk5) this).a = Action.MicroAppAction.SHOW_COMMUTE;
        super.d();
        if (this.i != null || (extras = intent.getExtras()) == null) {
            return 2;
        }
        String string = extras.getString(Constants.EXTRA_INFO);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        StringBuilder sb = new StringBuilder();
        sb.append("onStartCommand json=");
        if (string != null) {
            sb.append(string);
            local.d(str, sb.toString());
            this.i = (CommuteTimeSetting) new Gson().a(string, CommuteTimeSetting.class);
            Handler handler = this.q;
            if (handler != null) {
                handler.postDelayed(new d(this), 5000);
                return 2;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public void onStatusChanged(String str, int i2, Bundle bundle) {
        ee7.b(str, "provider");
        ee7.b(bundle, AppLinkData.ARGUMENTS_EXTRAS_KEY);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        local.d(str2, "onStatusChanged status=" + i2);
    }

    @DexIgnore
    public final void b(Location location) {
        this.e = location;
    }

    @DexIgnore
    public final void e() {
        f53.a aVar = new f53.a();
        LocationRequest locationRequest = this.t;
        if (locationRequest != null) {
            if (locationRequest != null) {
                aVar.a(locationRequest);
            } else {
                ee7.a();
                throw null;
            }
        }
        this.u = aVar.a();
    }

    @DexIgnore
    public final void f() {
        this.v = new b(this);
    }

    @DexIgnore
    public final void g() {
        FLogger.INSTANCE.getLocal().d(x, "createLocationRequest");
        LocationRequest locationRequest = new LocationRequest();
        this.t = locationRequest;
        if (locationRequest != null) {
            locationRequest.l(1000);
            LocationRequest locationRequest2 = this.t;
            if (locationRequest2 != null) {
                locationRequest2.k(1000);
                LocationRequest locationRequest3 = this.t;
                if (locationRequest3 != null) {
                    locationRequest3.b(100);
                    LocationRequest locationRequest4 = this.t;
                    if (locationRequest4 != null) {
                        locationRequest4.a(A);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final gn5 h() {
        gn5 gn5 = this.w;
        if (gn5 != null) {
            return gn5;
        }
        ee7.d("mDurationUtils");
        throw null;
    }

    @DexIgnore
    public final Location i() {
        return this.e;
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.f = z2;
    }

    @DexIgnore
    @Override // com.fossil.bk5
    public void b() {
        FLogger.INSTANCE.getLocal().d(x, "forceStop");
        a();
    }

    @DexIgnore
    public final void a(Location location) {
        ee7.b(location, PlaceFields.LOCATION);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "getDurationTime location long=" + location.getLongitude() + " lat=" + location.getLatitude());
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new c(this, location, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.bk5
    public void a() {
        FLogger.INSTANCE.getLocal().d(x, VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        super.a();
        stopSelf();
    }

    @DexIgnore
    public final Location a(Location location, Location location2) {
        ee7.b(location, PlaceFields.LOCATION);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "isBetterLocation location long=" + location.getLongitude() + " lat=" + location.getLatitude() + " time=" + location.getTime());
        if (location2 == null) {
            FLogger.INSTANCE.getLocal().d(x, "isBetterLocation currentBestLocation null");
            return location;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = x;
        local2.d(str2, "isBetterLocation currentBestLocation long=" + location2.getLongitude() + " lat=" + location2.getLatitude() + " time=" + location2.getTime());
        long time = location.getTime() - location2.getTime();
        boolean z2 = true;
        boolean z3 = time > ((long) y);
        boolean z4 = time < ((long) (-y));
        boolean z5 = time > 0;
        if (z3) {
            FLogger.INSTANCE.getLocal().d(x, "isBetterLocation isSignificantlyNewer");
            return location;
        } else if (z4) {
            FLogger.INSTANCE.getLocal().d(x, "isBetterLocation isSignificantlyOlder");
            return location2;
        } else {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = x;
            local3.d(str3, "isBetterLocation accuracy location=" + location.getAccuracy() + " currentBestLocation=" + location2.getAccuracy());
            int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
            boolean z6 = accuracy > 0;
            boolean z7 = accuracy < 0;
            if (accuracy <= 200) {
                z2 = false;
            }
            boolean a2 = a(location.getProvider(), location2.getProvider());
            if (z7) {
                FLogger.INSTANCE.getLocal().d(x, "isBetterLocation isMoreAccurate");
                return location;
            } else if (z5 && !z6) {
                FLogger.INSTANCE.getLocal().d(x, "isBetterLocation isNewer && isLessAccurate=false");
                return location;
            } else if (!z5 || z2 || !a2) {
                return location2;
            } else {
                FLogger.INSTANCE.getLocal().d(x, "isBetterLocation isNewer && isSignificantlyLessAccurate=false && isFromSameProvider");
                return location;
            }
        }
    }

    @DexIgnore
    public final boolean a(String str, String str2) {
        FLogger.INSTANCE.getLocal().d(x, "isSameProvider");
        if (str == null) {
            return str2 == null;
        }
        return ee7.a((Object) str, (Object) str2);
    }
}
