package com.portfolio.platform.service.workout;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Lifecycle;
import com.fossil.ae;
import com.fossil.dl7;
import com.fossil.ee7;
import com.fossil.ex6;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.kd7;
import com.fossil.ki7;
import com.fossil.kn7;
import com.fossil.mn7;
import com.fossil.nb7;
import com.fossil.nc;
import com.fossil.qj7;
import com.fossil.rd;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.we7;
import com.fossil.x87;
import com.fossil.x97;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.yx6;
import com.fossil.zb7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint;
import com.portfolio.platform.data.model.diana.workout.WorkoutRouterGpsWrapper;
import com.portfolio.platform.data.model.diana.workout.WorkoutScreenshotWrapper;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import io.flutter.embedding.android.FlutterFragment;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.dart.DartExecutor;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.JSONMethodCodec;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.platform.PlatformViewsController;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutTetherScreenShotManager implements rd {
    @DexIgnore
    public static /* final */ String u; // = (PortfolioApp.g0.c().getFilesDir() + File.separator + "%s");
    @DexIgnore
    public static /* final */ a v; // = new a(null);
    @DexIgnore
    public kn7 a; // = mn7.a(false, 1, null);
    @DexIgnore
    public /* final */ LinkedBlockingDeque<WorkoutScreenshotWrapper> b; // = new LinkedBlockingDeque<>();
    @DexIgnore
    public String c;
    @DexIgnore
    public /* final */ ki7 d;
    @DexIgnore
    public /* final */ yi7 e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public FlutterEngine g;
    @DexIgnore
    public MethodChannel h;
    @DexIgnore
    public FragmentActivity i;
    @DexIgnore
    public FrameLayout j;
    @DexIgnore
    public FlutterFragment p;
    @DexIgnore
    public int q;
    @DexIgnore
    public /* final */ Gson r;
    @DexIgnore
    public /* final */ WorkoutSessionRepository s;
    @DexIgnore
    public /* final */ FileRepository t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return WorkoutTetherScreenShotManager.u;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.workout.WorkoutTetherScreenShotManager$enqueue$1", f = "WorkoutTetherScreenShotManager.kt", l = {206}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutScreenshotWrapper $job;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutTetherScreenShotManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WorkoutTetherScreenShotManager workoutTetherScreenShotManager, WorkoutScreenshotWrapper workoutScreenshotWrapper, fb7 fb7) {
            super(2, fb7);
            this.this$0 = workoutTetherScreenShotManager;
            this.$job = workoutScreenshotWrapper;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$job, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            kn7 kn7;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.b.add(this.$job);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", this.$job.getWorkoutId() + " added to mQueue");
                kn7 i2 = this.this$0.a;
                this.L$0 = yi7;
                this.L$1 = i2;
                this.label = 1;
                if (i2.a(null, this) == a) {
                    return a;
                }
                kn7 = i2;
            } else if (i == 1) {
                kn7 = (kn7) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            try {
                this.this$0.b();
                i97 i97 = i97.a;
                kn7.a(null);
                return i97.a;
            } catch (Throwable th) {
                kn7.a(null);
                throw th;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutScreenshotWrapper $job$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ JSONObject $mock$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutTetherScreenShotManager this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements MethodChannel.Result {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.workout.WorkoutTetherScreenShotManager$c$a$a")
            /* renamed from: com.portfolio.platform.service.workout.WorkoutTetherScreenShotManager$c$a$a  reason: collision with other inner class name */
            public static final class C0305a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0305a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0305a aVar = new C0305a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0305a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        WorkoutTetherScreenShotManager workoutTetherScreenShotManager = this.this$0.a.this$0;
                        workoutTetherScreenShotManager.q = workoutTetherScreenShotManager.q - 1;
                        this.this$0.a.this$0.c = (String) null;
                        this.this$0.a.this$0.f = false;
                        this.this$0.a.this$0.b();
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Object $result;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, Object obj, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$result = obj;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    b bVar = new b(this.this$0, this.$result, fb7);
                    bVar.p$ = (yi7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        Object obj2 = this.$result;
                        if (obj2 != null) {
                            String string = ((JSONObject) obj2).getString("imageData");
                            FileRepository f = this.this$0.a.this$0.t;
                            String workoutId = this.this$0.a.$job$inlined.getWorkoutId();
                            we7 we7 = we7.a;
                            String format = String.format(WorkoutTetherScreenShotManager.v.a(), Arrays.copyOf(new Object[]{this.this$0.a.$job$inlined.getWorkoutId()}, 1));
                            ee7.a((Object) format, "java.lang.String.format(format, *args)");
                            String saveLocalDataFile = f.saveLocalDataFile(string, workoutId, format);
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.this$0.a.$job$inlined.getWorkoutId() + " success insert to DB, uri " + saveLocalDataFile);
                            WorkoutSessionRepository k = this.this$0.a.this$0.s;
                            String workoutId2 = this.this$0.a.$job$inlined.getWorkoutId();
                            this.L$0 = yi7;
                            this.L$1 = string;
                            this.L$2 = saveLocalDataFile;
                            this.label = 1;
                            if (k.insertWorkoutTetherScreenShot(workoutId2, saveLocalDataFile, this) == a) {
                                return a;
                            }
                        } else {
                            throw new x87("null cannot be cast to non-null type org.json.JSONObject");
                        }
                    } else if (i == 1) {
                        String str = (String) this.L$2;
                        String str2 = (String) this.L$1;
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    WorkoutTetherScreenShotManager workoutTetherScreenShotManager = this.this$0.a.this$0;
                    workoutTetherScreenShotManager.q = workoutTetherScreenShotManager.q - 1;
                    this.this$0.a.this$0.c = (String) null;
                    this.this$0.a.this$0.f = false;
                    this.this$0.a.this$0.b();
                    return i97.a;
                }
            }

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void error(String str, String str2, Object obj) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.a.$job$inlined.getWorkoutId() + " error " + str2 + ' ');
                ik7 unused = xh7.b(this.a.this$0.e, null, null, new C0305a(this, null), 3, null);
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void notImplemented() {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.a.$job$inlined.getWorkoutId() + " notImplemented ");
                this.a.this$0.c = (String) null;
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void success(Object obj) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.a.$job$inlined.getWorkoutId() + " success");
                ik7 unused = xh7.b(this.a.this$0.e, null, null, new b(this, obj, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(fb7 fb7, WorkoutTetherScreenShotManager workoutTetherScreenShotManager, WorkoutScreenshotWrapper workoutScreenshotWrapper, JSONObject jSONObject) {
            super(2, fb7);
            this.this$0 = workoutTetherScreenShotManager;
            this.$job$inlined = workoutScreenshotWrapper;
            this.$mock$inlined = jSONObject;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(fb7, this.this$0, this.$job$inlined, this.$mock$inlined);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                if (this.this$0.p == null || this.this$0.j == null) {
                    this.this$0.a();
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.$job$inlined.getWorkoutId() + " invoke flutter module to take screenshot");
                MethodChannel d = this.this$0.h;
                if (d != null) {
                    d.invokeMethod("takeSnapShot", this.$mock$inlined, new a(this));
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public WorkoutTetherScreenShotManager(WorkoutSessionRepository workoutSessionRepository, FileRepository fileRepository) {
        DartExecutor dartExecutor;
        ee7.b(workoutSessionRepository, "mWorkoutSessionRepository");
        ee7.b(fileRepository, "mFileRepository");
        this.s = workoutSessionRepository;
        this.t = fileRepository;
        BinaryMessenger binaryMessenger = null;
        ki7 a2 = dl7.a(null, 1, null);
        this.d = a2;
        this.e = zi7.a(a2.plus(qj7.b()));
        this.f = true;
        FlutterEngine b2 = ex6.c.b();
        this.g = b2;
        if (!(b2 == null || (dartExecutor = b2.getDartExecutor()) == null)) {
            binaryMessenger = dartExecutor.getBinaryMessenger();
        }
        this.h = new MethodChannel(binaryMessenger, "detailTracking/screenShotMap", JSONMethodCodec.INSTANCE);
        this.r = new Gson();
    }

    @DexIgnore
    @ae(Lifecycle.a.ON_STOP)
    public final void cancel() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherScreenShotManager", "onActivity onStop currentTaskId " + this.c);
        this.b.clear();
        this.c = null;
        this.q = 0;
        c();
    }

    @DexIgnore
    public final synchronized void b() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherScreenShotManager", "dequeue queueSize " + this.b.size() + " currentJobRunning " + this.q);
        if (this.b.size() > 0 && this.q == 0) {
            WorkoutScreenshotWrapper pop = this.b.pop();
            pop.setResetState(this.f);
            ee7.a((Object) pop, "jobToRun");
            b(pop);
        }
    }

    @DexIgnore
    public final void c() {
        PlatformViewsController platformViewsController;
        FLogger.INSTANCE.getLocal().d("WorkoutTetherScreenShotManager", "detachPlaceHolderView");
        FragmentActivity fragmentActivity = this.i;
        if (fragmentActivity != null) {
            if (this.p != null) {
                nc b2 = fragmentActivity.getSupportFragmentManager().b();
                FlutterFragment flutterFragment = this.p;
                if (flutterFragment != null) {
                    b2.d(flutterFragment);
                    b2.a();
                } else {
                    throw new x87("null cannot be cast to non-null type androidx.fragment.app.Fragment");
                }
            }
            if (this.j != null) {
                View findViewById = fragmentActivity.findViewById(2131362149);
                if (findViewById != null) {
                    ((ViewGroup) findViewById).removeView(this.j);
                } else {
                    throw new x87("null cannot be cast to non-null type android.view.ViewGroup");
                }
            }
            FlutterFragment flutterFragment2 = this.p;
            if (flutterFragment2 != null) {
                flutterFragment2.onDestroyView();
            }
            FlutterEngine flutterEngine = this.g;
            if (!(flutterEngine == null || (platformViewsController = flutterEngine.getPlatformViewsController()) == null)) {
                platformViewsController.detachFromView();
            }
        }
        this.i = null;
        this.p = null;
        this.j = null;
    }

    @DexIgnore
    public final void a(FragmentActivity fragmentActivity) {
        ee7.b(fragmentActivity, Constants.ACTIVITY);
        this.i = fragmentActivity;
    }

    @DexIgnore
    public final synchronized void a(WorkoutScreenshotWrapper workoutScreenshotWrapper) {
        ee7.b(workoutScreenshotWrapper, "job");
        if (!ee7.a((Object) this.c, (Object) workoutScreenshotWrapper.getWorkoutId())) {
            if (!this.b.contains(workoutScreenshotWrapper)) {
                ik7 unused = xh7.b(this.e, null, null, new b(this, workoutScreenshotWrapper, null), 3, null);
                return;
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherScreenShotManager", workoutScreenshotWrapper.getWorkoutId() + " is already added");
    }

    @DexIgnore
    public final void b(WorkoutScreenshotWrapper workoutScreenshotWrapper) {
        JSONObject jSONObject = new JSONObject(this.r.a(workoutScreenshotWrapper));
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + workoutScreenshotWrapper.getWorkoutId() + " resetState " + workoutScreenshotWrapper.getResetState());
        this.q = this.q + 1;
        this.c = workoutScreenshotWrapper.getWorkoutId();
        if (this.i != null) {
            ik7 unused = xh7.b(zi7.a(qj7.c()), null, null, new c(null, this, workoutScreenshotWrapper, jSONObject), 3, null);
        }
    }

    @DexIgnore
    public final String a(String str) {
        ee7.b(str, "workoutId");
        we7 we7 = we7.a;
        String format = String.format(u, Arrays.copyOf(new Object[]{str}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final void a() {
        FLogger.INSTANCE.getLocal().d("WorkoutTetherScreenShotManager", "attachPlaceHolderView");
        FragmentActivity fragmentActivity = this.i;
        if (fragmentActivity != null) {
            FlutterFragment flutterFragment = (FlutterFragment) fragmentActivity.getSupportFragmentManager().b("TAG_FLUTTER_FRAGMENT");
            this.p = flutterFragment;
            if (flutterFragment == null || this.j == null) {
                FrameLayout frameLayout = new FrameLayout(fragmentActivity);
                this.j = frameLayout;
                if (frameLayout != null) {
                    frameLayout.setId(View.generateViewId());
                    ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams((int) yx6.a(350.0f), (int) yx6.a(200.0f));
                    marginLayoutParams.setMargins(0, Constants.BOLT_RSSI_ERROR, 0, 0);
                    FrameLayout frameLayout2 = this.j;
                    if (frameLayout2 != null) {
                        frameLayout2.setLayoutParams(new FrameLayout.LayoutParams(marginLayoutParams));
                        View findViewById = fragmentActivity.findViewById(2131362149);
                        if (findViewById != null) {
                            ((ViewGroup) findViewById).addView(this.j);
                            this.p = FlutterFragment.withCachedEngine("screenshot_engine_id").build();
                            nc b2 = fragmentActivity.getSupportFragmentManager().b();
                            FrameLayout frameLayout3 = this.j;
                            if (frameLayout3 != null) {
                                int id = frameLayout3.getId();
                                FlutterFragment flutterFragment2 = this.p;
                                if (flutterFragment2 != null) {
                                    b2.a(id, flutterFragment2, "TAG_FLUTTER_FRAGMENT");
                                    b2.a();
                                    return;
                                }
                                throw new x87("null cannot be cast to non-null type androidx.fragment.app.Fragment");
                            }
                            ee7.a();
                            throw null;
                        }
                        throw new x87("null cannot be cast to non-null type android.view.ViewGroup");
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final WorkoutScreenshotWrapper a(WorkoutSession workoutSession) {
        ArrayList arrayList;
        ee7.b(workoutSession, "workoutSession");
        String id = workoutSession.getId();
        int a2 = (int) yx6.a(350.0f);
        int a3 = (int) yx6.a(200.0f);
        List<WorkoutGpsPoint> workoutGpsPoints = workoutSession.getWorkoutGpsPoints();
        if (workoutGpsPoints != null) {
            ArrayList arrayList2 = new ArrayList(x97.a(workoutGpsPoints, 10));
            Iterator<T> it = workoutGpsPoints.iterator();
            while (it.hasNext()) {
                arrayList2.add(new WorkoutRouterGpsWrapper(it.next()));
            }
            arrayList = arrayList2;
        } else {
            arrayList = null;
        }
        return new WorkoutScreenshotWrapper(id, a2, a3, true, arrayList);
    }
}
