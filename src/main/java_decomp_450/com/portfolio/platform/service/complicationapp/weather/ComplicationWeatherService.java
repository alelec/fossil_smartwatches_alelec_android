package com.portfolio.platform.service.complicationapp.weather;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.VideoUploader;
import com.fossil.bk5;
import com.fossil.ch5;
import com.fossil.dl7;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.gf5;
import com.fossil.gw6;
import com.fossil.i97;
import com.fossil.if5;
import com.fossil.ik7;
import com.fossil.kd7;
import com.fossil.mb5;
import com.fossil.nb7;
import com.fossil.qd5;
import com.fossil.qj7;
import com.fossil.ql4;
import com.fossil.rb7;
import com.fossil.rl4;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.we7;
import com.fossil.wj5;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.yj5;
import com.fossil.zb7;
import com.fossil.zi7;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationWeatherService extends wj5 {
    @DexIgnore
    public /* final */ int g; // = 1800000;
    @DexIgnore
    public gw6 h;
    @DexIgnore
    public rl4 i;
    @DexIgnore
    public ch5 j;
    @DexIgnore
    public PortfolioApp p;
    @DexIgnore
    public CustomizeRealDataRepository q;
    @DexIgnore
    public UserRepository r;
    @DexIgnore
    public /* final */ yi7 s; // = zi7.a(dl7.a(null, 1, null).plus(qj7.b()));
    @DexIgnore
    public String t;
    @DexIgnore
    public WeatherSettings u;
    @DexIgnore
    public Weather v;
    @DexIgnore
    public a<String> w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ql4.d<gw6.d, gw6.b> {
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationWeatherService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$getWeather$1$onSuccess$1", f = "ComplicationWeatherService.kt", l = {147}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    this.this$0.a.i().u(new Gson().a(this.this$0.a.k(), WeatherSettings.class));
                    ComplicationWeatherService complicationWeatherService = this.this$0.a;
                    WeatherSettings k = complicationWeatherService.k();
                    if (k != null) {
                        this.L$0 = yi7;
                        this.label = 1;
                        if (ComplicationWeatherService.a(complicationWeatherService, k, false, this, 2, null) == a) {
                            return a;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.a.a();
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(ComplicationWeatherService complicationWeatherService) {
            this.a = complicationWeatherService;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(gw6.d dVar) {
            ee7.b(dVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(wj5.f.a(), "getWeather - onSuccess");
            this.a.a(dVar.a());
            if (this.a.j() != null) {
                ik7 unused = xh7.b(this.a.s, null, null, new a(this, null), 3, null);
            }
        }

        @DexIgnore
        public void a(gw6.b bVar) {
            ee7.b(bVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(wj5.f.a(), "getWeather - onError");
            this.a.a(bVar.a());
            this.a.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$getWeatherBaseOnLocation$1", f = "ComplicationWeatherService.kt", l = {118, 121}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationWeatherService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ComplicationWeatherService complicationWeatherService, fb7 fb7) {
            super(2, fb7);
            this.this$0 = complicationWeatherService;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:34:0x00e3  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x010f  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
                r14 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r14.label
                r2 = 2
                r3 = 1
                r4 = 0
                if (r1 == 0) goto L_0x0029
                if (r1 == r3) goto L_0x0020
                if (r1 != r2) goto L_0x0018
                java.lang.Object r0 = r14.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r15)
                goto L_0x00d9
            L_0x0018:
                java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r15.<init>(r0)
                throw r15
            L_0x0020:
                java.lang.Object r0 = r14.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r15)
                goto L_0x00af
            L_0x0029:
                com.fossil.t87.a(r15)
                com.fossil.yi7 r15 = r14.p$
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r1 = r14.this$0
                com.portfolio.platform.data.model.microapp.weather.WeatherSettings r1 = r1.k()
                if (r1 == 0) goto L_0x0144
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.fossil.wj5$a r5 = com.fossil.wj5.f
                java.lang.String r5 = r5.a()
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r7 = "getWeatherBaseOnLocation - location="
                r6.append(r7)
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r7 = r14.this$0
                com.portfolio.platform.data.model.microapp.weather.WeatherSettings r7 = r7.k()
                if (r7 == 0) goto L_0x0140
                java.lang.String r7 = r7.getLocation()
                r6.append(r7)
                java.lang.String r6 = r6.toString()
                r1.d(r5, r6)
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r1 = r14.this$0
                com.portfolio.platform.data.model.microapp.weather.WeatherSettings r1 = r1.k()
                if (r1 == 0) goto L_0x013c
                boolean r1 = r1.isUseCurrentLocation()
                if (r1 != 0) goto L_0x0077
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r15 = r14.this$0
                r15.l()
                goto L_0x0144
            L_0x0077:
                java.util.Calendar r1 = java.util.Calendar.getInstance()
                java.lang.String r5 = "Calendar.getInstance()"
                com.fossil.ee7.a(r1, r5)
                long r5 = r1.getTimeInMillis()
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r1 = r14.this$0
                com.portfolio.platform.data.model.microapp.weather.WeatherSettings r1 = r1.k()
                if (r1 == 0) goto L_0x0138
                long r7 = r1.getUpdatedAt()
                long r5 = r5 - r7
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r1 = r14.this$0
                int r1 = r1.g
                long r7 = (long) r1
                int r1 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
                if (r1 >= 0) goto L_0x00ba
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r1 = r14.this$0
                com.portfolio.platform.data.model.microapp.weather.WeatherSettings r2 = r1.k()
                if (r2 == 0) goto L_0x00b6
                r14.L$0 = r15
                r14.label = r3
                java.lang.Object r15 = r1.a(r2, r3, r14)
                if (r15 != r0) goto L_0x00af
                return r0
            L_0x00af:
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r15 = r14.this$0
                r15.a()
                goto L_0x0144
            L_0x00b6:
                com.fossil.ee7.a()
                throw r4
            L_0x00ba:
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r1 = r14.this$0
                com.portfolio.platform.data.LocationSource r5 = r1.e()
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r1 = r14.this$0
                com.portfolio.platform.PortfolioApp r6 = r1.h()
                r7 = 0
                r8 = 0
                r9 = 0
                r10 = 0
                r12 = 28
                r13 = 0
                r14.L$0 = r15
                r14.label = r2
                r11 = r14
                java.lang.Object r15 = com.portfolio.platform.data.LocationSource.getLocation$default(r5, r6, r7, r8, r9, r10, r11, r12, r13)
                if (r15 != r0) goto L_0x00d9
                return r0
            L_0x00d9:
                com.portfolio.platform.data.LocationSource$Result r15 = (com.portfolio.platform.data.LocationSource.Result) r15
                com.portfolio.platform.data.LocationSource$ErrorState r0 = r15.getErrorState()
                com.portfolio.platform.data.LocationSource$ErrorState r1 = com.portfolio.platform.data.LocationSource.ErrorState.SUCCESS
                if (r0 != r1) goto L_0x010f
                android.location.Location r15 = r15.getLocation()
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r0 = r14.this$0
                com.portfolio.platform.data.model.microapp.weather.WeatherSettings r0 = r0.k()
                if (r0 == 0) goto L_0x010b
                com.google.android.gms.maps.model.LatLng r1 = new com.google.android.gms.maps.model.LatLng
                if (r15 == 0) goto L_0x0107
                double r2 = r15.getLatitude()
                double r4 = r15.getLongitude()
                r1.<init>(r2, r4)
                r0.setLatLng(r1)
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r15 = r14.this$0
                r15.l()
                goto L_0x0144
            L_0x0107:
                com.fossil.ee7.a()
                throw r4
            L_0x010b:
                com.fossil.ee7.a()
                throw r4
            L_0x010f:
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r0 = r14.this$0
                com.portfolio.platform.data.LocationSource$ErrorState r1 = r15.getErrorState()
                r0.a(r1)
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r0 = r14.this$0
                com.portfolio.platform.data.LocationSource$ErrorState r15 = r15.getErrorState()
                r0.c(r15)
                com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
                com.fossil.wj5$a r0 = com.fossil.wj5.f
                java.lang.String r0 = r0.a()
                java.lang.String r1 = "getWeatherBaseOnLocation - using current location but location is null, stop service."
                r15.d(r0, r1)
                com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r15 = r14.this$0
                r15.a()
                goto L_0x0144
            L_0x0138:
                com.fossil.ee7.a()
                throw r4
            L_0x013c:
                com.fossil.ee7.a()
                throw r4
            L_0x0140:
                com.fossil.ee7.a()
                throw r4
            L_0x0144:
                com.fossil.i97 r15 = com.fossil.i97.a
                return r15
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService", f = "ComplicationWeatherService.kt", l = {167}, m = "sendComplicationAppResponse")
    public static final class d extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationWeatherService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ComplicationWeatherService complicationWeatherService, fb7 fb7) {
            super(fb7);
            this.this$0 = complicationWeatherService;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$showChanceOfRain$1", f = "ComplicationWeatherService.kt", l = {}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $rainProbabilityPercent;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationWeatherService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ComplicationWeatherService complicationWeatherService, int i, fb7 fb7) {
            super(2, fb7);
            this.this$0 = complicationWeatherService;
            this.$rainProbabilityPercent = i;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$rainProbabilityPercent, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                this.this$0.g().upsertCustomizeRealData(new CustomizeRealData("chance_of_rain", String.valueOf(this.$rainProbabilityPercent)));
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService", f = "ComplicationWeatherService.kt", l = {177}, m = "showTemperature")
    public static final class f extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationWeatherService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ComplicationWeatherService complicationWeatherService, fb7 fb7) {
            super(fb7);
            this.this$0 = complicationWeatherService;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, false, this);
        }
    }

    @DexIgnore
    public final CustomizeRealDataRepository g() {
        CustomizeRealDataRepository customizeRealDataRepository = this.q;
        if (customizeRealDataRepository != null) {
            return customizeRealDataRepository;
        }
        ee7.d("mCustomizeRealDataRepository");
        throw null;
    }

    @DexIgnore
    public final PortfolioApp h() {
        PortfolioApp portfolioApp = this.p;
        if (portfolioApp != null) {
            return portfolioApp;
        }
        ee7.d("mPortfolioApp");
        throw null;
    }

    @DexIgnore
    public final ch5 i() {
        ch5 ch5 = this.j;
        if (ch5 != null) {
            return ch5;
        }
        ee7.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final Weather j() {
        return this.v;
    }

    @DexIgnore
    public final WeatherSettings k() {
        return this.u;
    }

    @DexIgnore
    public final void l() {
        rl4 rl4 = this.i;
        if (rl4 != null) {
            gw6 gw6 = this.h;
            if (gw6 != null) {
                WeatherSettings weatherSettings = this.u;
                if (weatherSettings != null) {
                    LatLng latLng = weatherSettings.getLatLng();
                    WeatherSettings weatherSettings2 = this.u;
                    if (weatherSettings2 != null) {
                        rl4.a(gw6, new gw6.c(latLng, weatherSettings2.getTempUnit()), new b(this));
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.d("mGetWeather");
                throw null;
            }
        } else {
            ee7.d("mUseCaseHandler");
            throw null;
        }
    }

    @DexIgnore
    public final ik7 m() {
        return xh7.b(zi7.a(qj7.a()), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.bk5
    public IBinder onBind(Intent intent) {
        ee7.b(intent, "intent");
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        FLogger.INSTANCE.getLocal().d(wj5.f.a(), "onCreate");
        PortfolioApp.g0.c().f().a(this);
        this.t = PortfolioApp.g0.c().c();
        this.w = new a<>();
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(wj5.f.a(), "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        String string;
        ee7.b(intent, "intent");
        FLogger.INSTANCE.getLocal().d(wj5.f.a(), "onStartCommand");
        ((bk5) this).a = 3001;
        super.d();
        Bundle extras = intent.getExtras();
        if (extras == null || (string = extras.getString("action")) == null) {
            return 2;
        }
        a<String> aVar = this.w;
        if (aVar == null) {
            ee7.d("mWeatherTasks");
            throw null;
        } else if (aVar.a()) {
            a<String> aVar2 = this.w;
            if (aVar2 != null) {
                aVar2.a(string);
                ch5 ch5 = this.j;
                if (ch5 != null) {
                    String o = ch5.o();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = wj5.f.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("Inside SteamingAction .run - WEATHER_ACTION json ");
                    if (o != null) {
                        sb.append(o);
                        local.d(a2, sb.toString());
                        this.u = (WeatherSettings) new Gson().a(o, WeatherSettings.class);
                        m();
                        return 2;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.d("mSharedPreferencesManager");
                throw null;
            }
            ee7.d("mWeatherTasks");
            throw null;
        } else {
            a<String> aVar3 = this.w;
            if (aVar3 != null) {
                aVar3.a(string);
                return 2;
            }
            ee7.d("mWeatherTasks");
            throw null;
        }
    }

    @DexIgnore
    public final void c(LocationSource.ErrorState errorState) {
        a<String> aVar = this.w;
        if (aVar != null) {
            String b2 = aVar.b();
            if (b2 == null) {
                return;
            }
            if (ee7.a((Object) b2, (Object) "TEMPERATURE")) {
                if5 c2 = qd5.f.c("weather");
                if (c2 != null) {
                    we7 we7 = we7.a;
                    String format = String.format("update_%s_optional_error", Arrays.copyOf(new Object[]{"weather"}, 1));
                    ee7.a((Object) format, "java.lang.String.format(format, *args)");
                    gf5 a2 = qd5.f.a(format);
                    a2.a("error_code", b(errorState));
                    c2.a(a2);
                    return;
                }
                return;
            }
            if5 c3 = qd5.f.c("chance-of-rain");
            if (c3 != null) {
                we7 we72 = we7.a;
                String format2 = String.format("update_%s_optional_error", Arrays.copyOf(new Object[]{qd5.f.d("chance-of-rain")}, 1));
                ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                gf5 a3 = qd5.f.a(format2);
                a3.a("error_code", b(errorState));
                c3.a(a3);
                return;
            }
            return;
        }
        ee7.d("mWeatherTasks");
        throw null;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {
        @DexIgnore
        public /* final */ HashSet<T> a; // = new HashSet<>();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final void a(T t) {
            synchronized (this.b) {
                this.a.add(t);
            }
        }

        @DexIgnore
        public final void b(T t) {
            synchronized (this.b) {
                this.a.remove(t);
            }
        }

        @DexIgnore
        public final boolean a() {
            boolean isEmpty;
            synchronized (this.b) {
                isEmpty = this.a.isEmpty();
            }
            return isEmpty;
        }

        @DexIgnore
        public final T b() {
            T next;
            synchronized (this.b) {
                Iterator<T> it = this.a.iterator();
                ee7.a((Object) it, "hashSet.iterator()");
                next = it.hasNext() ? it.next() : null;
            }
            return next;
        }
    }

    @DexIgnore
    public final void a(Weather weather) {
        this.v = weather;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0196  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object b(com.portfolio.platform.data.model.microapp.weather.WeatherSettings r12, boolean r13, com.fossil.fb7<? super com.fossil.i97> r14) {
        /*
            r11 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService.f
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$f r0 = (com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService.f) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$f r0 = new com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$f
            r0.<init>(r11, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003c
            if (r2 != r3) goto L_0x0034
            boolean r13 = r0.Z$0
            java.lang.Object r12 = r0.L$1
            com.portfolio.platform.data.model.microapp.weather.WeatherSettings r12 = (com.portfolio.platform.data.model.microapp.weather.WeatherSettings) r12
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r0 = (com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService) r0
            com.fossil.t87.a(r14)
            goto L_0x0053
        L_0x0034:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r13 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r13)
            throw r12
        L_0x003c:
            com.fossil.t87.a(r14)
            com.portfolio.platform.data.source.UserRepository r14 = r11.r
            if (r14 == 0) goto L_0x01aa
            r0.L$0 = r11
            r0.L$1 = r12
            r0.Z$0 = r13
            r0.label = r3
            java.lang.Object r14 = r14.getCurrentUser(r0)
            if (r14 != r1) goto L_0x0052
            return r1
        L_0x0052:
            r0 = r11
        L_0x0053:
            com.portfolio.platform.data.model.MFUser r14 = (com.portfolio.platform.data.model.MFUser) r14
            if (r14 == 0) goto L_0x0196
            com.portfolio.platform.data.model.MFUser$UnitGroup r1 = r14.getUnitGroup()
            if (r1 == 0) goto L_0x0062
            java.lang.String r1 = r1.getTemperature()
            goto L_0x0063
        L_0x0062:
            r1 = r4
        L_0x0063:
            com.fossil.ob5 r2 = com.fossil.ob5.METRIC
            java.lang.String r2 = r2.getValue()
            boolean r2 = com.fossil.ee7.a(r1, r2)
            if (r2 == 0) goto L_0x007f
            com.fossil.r87 r14 = new com.fossil.r87
            com.portfolio.platform.data.model.microapp.weather.WeatherSettings$TEMP_UNIT r1 = com.portfolio.platform.data.model.microapp.weather.WeatherSettings.TEMP_UNIT.CELSIUS
            float r2 = r12.getTemperatureIn(r1)
            java.lang.Float r2 = com.fossil.pb7.a(r2)
            r14.<init>(r1, r2)
            goto L_0x00d8
        L_0x007f:
            com.fossil.ob5 r2 = com.fossil.ob5.IMPERIAL
            java.lang.String r2 = r2.getValue()
            boolean r1 = com.fossil.ee7.a(r1, r2)
            if (r1 == 0) goto L_0x009b
            com.fossil.r87 r14 = new com.fossil.r87
            com.portfolio.platform.data.model.microapp.weather.WeatherSettings$TEMP_UNIT r1 = com.portfolio.platform.data.model.microapp.weather.WeatherSettings.TEMP_UNIT.FAHRENHEIT
            float r2 = r12.getTemperatureIn(r1)
            java.lang.Float r2 = com.fossil.pb7.a(r2)
            r14.<init>(r1, r2)
            goto L_0x00d8
        L_0x009b:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            com.fossil.wj5$a r2 = com.fossil.wj5.f
            java.lang.String r2 = r2.a()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "showTemperature - temperature unit is not found, "
            r5.append(r6)
            com.portfolio.platform.data.model.MFUser$UnitGroup r14 = r14.getUnitGroup()
            if (r14 == 0) goto L_0x00bc
            java.lang.String r14 = r14.getTemperature()
            goto L_0x00bd
        L_0x00bc:
            r14 = r4
        L_0x00bd:
            r5.append(r14)
            java.lang.String r14 = r5.toString()
            r1.e(r2, r14)
            com.fossil.r87 r14 = new com.fossil.r87
            com.portfolio.platform.data.model.microapp.weather.WeatherSettings$TEMP_UNIT r1 = r12.getTempUnit()
            float r2 = r12.getTemperature()
            java.lang.Float r2 = com.fossil.pb7.a(r2)
            r14.<init>(r1, r2)
        L_0x00d8:
            java.lang.Object r1 = r14.component1()
            com.portfolio.platform.data.model.microapp.weather.WeatherSettings$TEMP_UNIT r1 = (com.portfolio.platform.data.model.microapp.weather.WeatherSettings.TEMP_UNIT) r1
            java.lang.Object r14 = r14.component2()
            java.lang.Number r14 = (java.lang.Number) r14
            float r6 = r14.floatValue()
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            com.fossil.wj5$a r2 = com.fossil.wj5.f
            java.lang.String r2 = r2.a()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = "showTemperature - forecast="
            r5.append(r7)
            java.lang.String r7 = r12.getForecast()
            r5.append(r7)
            java.lang.String r7 = ", temperature="
            r5.append(r7)
            r5.append(r6)
            java.lang.String r7 = ", temperatureUnit="
            r5.append(r7)
            r5.append(r1)
            java.lang.String r5 = r5.toString()
            r14.d(r2, r5)
            com.fossil.rb5$a r14 = com.fossil.rb5.Companion
            java.lang.String r2 = r12.getForecast()
            com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo$WeatherCondition r8 = r14.a(r2)
            int[] r14 = com.fossil.yj5.a
            int r1 = r1.ordinal()
            r14 = r14[r1]
            if (r14 == r3) goto L_0x013c
            r1 = 2
            if (r14 != r1) goto L_0x0136
            com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo$TemperatureUnit r14 = com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo.TemperatureUnit.F
            goto L_0x013e
        L_0x0136:
            com.fossil.p87 r12 = new com.fossil.p87
            r12.<init>()
            throw r12
        L_0x013c:
            com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo$TemperatureUnit r14 = com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo.TemperatureUnit.C
        L_0x013e:
            r7 = r14
            com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo r14 = new com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo
            r9 = 3600(0xe10, double:1.7786E-320)
            r5 = r14
            r5.<init>(r6, r7, r8, r9)
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            java.lang.String r2 = r0.t
            java.lang.String r3 = "mSerial"
            if (r2 == 0) goto L_0x0192
            r1.a(r14, r2)
            com.fossil.qd5$a r14 = com.fossil.qd5.f
            java.lang.String r1 = "weather"
            com.fossil.if5 r14 = r14.c(r1)
            if (r14 == 0) goto L_0x016e
            java.lang.String r2 = r0.t
            if (r2 == 0) goto L_0x016a
            java.lang.String r3 = ""
            r14.a(r2, r13, r3)
            goto L_0x016e
        L_0x016a:
            com.fossil.ee7.d(r3)
            throw r4
        L_0x016e:
            com.fossil.qd5$a r13 = com.fossil.qd5.f
            r13.e(r1)
            com.portfolio.platform.data.model.CustomizeRealData r13 = new com.portfolio.platform.data.model.CustomizeRealData
            com.portfolio.platform.data.model.microapp.weather.WeatherSettings$TEMP_UNIT r14 = com.portfolio.platform.data.model.microapp.weather.WeatherSettings.TEMP_UNIT.CELSIUS
            float r12 = r12.getTemperatureIn(r14)
            java.lang.String r12 = java.lang.String.valueOf(r12)
            java.lang.String r14 = "temperature"
            r13.<init>(r14, r12)
            com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository r12 = r0.q
            if (r12 == 0) goto L_0x018c
            r12.upsertCustomizeRealData(r13)
            goto L_0x01a7
        L_0x018c:
            java.lang.String r12 = "mCustomizeRealDataRepository"
            com.fossil.ee7.d(r12)
            throw r4
        L_0x0192:
            com.fossil.ee7.d(r3)
            throw r4
        L_0x0196:
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            com.fossil.wj5$a r13 = com.fossil.wj5.f
            java.lang.String r13 = r13.a()
            java.lang.String r14 = "showTemperature - do not send temperature, no user info"
            r12.e(r13, r14)
        L_0x01a7:
            com.fossil.i97 r12 = com.fossil.i97.a
            return r12
        L_0x01aa:
            java.lang.String r12 = "mUserRepository"
            com.fossil.ee7.d(r12)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService.b(com.portfolio.platform.data.model.microapp.weather.WeatherSettings, boolean, com.fossil.fb7):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0066, code lost:
        if (com.fossil.ee7.a((java.lang.Object) r9, (java.lang.Object) "TEMPERATURE") == false) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0068, code lost:
        r0.L$0 = r5;
        r0.L$1 = r7;
        r0.Z$0 = r8;
        r0.L$2 = r9;
        r0.L$3 = r9;
        r0.label = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0078, code lost:
        if (r5.b(r7, r8, r0) != r1) goto L_0x007c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007b, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007c, code lost:
        r2 = r7;
        r7 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0087, code lost:
        if (com.fossil.ee7.a((java.lang.Object) r9, (java.lang.Object) "RAIN_CHANCE") == false) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0089, code lost:
        r5.a(r7.getRainProbability(), r8);
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00a8 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized /* synthetic */ java.lang.Object a(com.portfolio.platform.data.model.microapp.weather.WeatherSettings r7, boolean r8, com.fossil.fb7<? super com.fossil.i97> r9) {
        /*
            r6 = this;
            monitor-enter(r6)
            boolean r0 = r9 instanceof com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService.d     // Catch:{ all -> 0x00ae }
            if (r0 == 0) goto L_0x0015
            r0 = r9
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$d r0 = (com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService.d) r0     // Catch:{ all -> 0x00ae }
            int r1 = r0.label     // Catch:{ all -> 0x00ae }
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = r1 & r2
            if (r1 == 0) goto L_0x0015
            int r9 = r0.label     // Catch:{ all -> 0x00ae }
            int r9 = r9 - r2
            r0.label = r9     // Catch:{ all -> 0x00ae }
            goto L_0x001a
        L_0x0015:
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$d r0 = new com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$d     // Catch:{ all -> 0x00ae }
            r0.<init>(r6, r9)     // Catch:{ all -> 0x00ae }
        L_0x001a:
            java.lang.Object r9 = r0.result     // Catch:{ all -> 0x00ae }
            java.lang.Object r1 = com.fossil.nb7.a()     // Catch:{ all -> 0x00ae }
            int r2 = r0.label     // Catch:{ all -> 0x00ae }
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0046
            if (r2 != r3) goto L_0x003e
            java.lang.Object r7 = r0.L$3     // Catch:{ all -> 0x00ae }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x00ae }
            java.lang.Object r8 = r0.L$2     // Catch:{ all -> 0x00ae }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ all -> 0x00ae }
            boolean r8 = r0.Z$0     // Catch:{ all -> 0x00ae }
            java.lang.Object r2 = r0.L$1     // Catch:{ all -> 0x00ae }
            com.portfolio.platform.data.model.microapp.weather.WeatherSettings r2 = (com.portfolio.platform.data.model.microapp.weather.WeatherSettings) r2     // Catch:{ all -> 0x00ae }
            java.lang.Object r5 = r0.L$0     // Catch:{ all -> 0x00ae }
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService r5 = (com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService) r5     // Catch:{ all -> 0x00ae }
            com.fossil.t87.a(r9)     // Catch:{ all -> 0x00ae }
            goto L_0x007e
        L_0x003e:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException     // Catch:{ all -> 0x00ae }
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)     // Catch:{ all -> 0x00ae }
            throw r7     // Catch:{ all -> 0x00ae }
        L_0x0046:
            com.fossil.t87.a(r9)     // Catch:{ all -> 0x00ae }
            r5 = r6
        L_0x004a:
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$a<java.lang.String> r9 = r5.w     // Catch:{ all -> 0x00ae }
            if (r9 == 0) goto L_0x00a8
            boolean r9 = r9.a()     // Catch:{ all -> 0x00ae }
            if (r9 != 0) goto L_0x00a4
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$a<java.lang.String> r9 = r5.w     // Catch:{ all -> 0x00ae }
            if (r9 == 0) goto L_0x009e
            java.lang.Object r9 = r9.b()     // Catch:{ all -> 0x00ae }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x00ae }
            if (r9 == 0) goto L_0x004a
            java.lang.String r2 = "TEMPERATURE"
            boolean r2 = com.fossil.ee7.a(r9, r2)     // Catch:{ all -> 0x00ae }
            if (r2 == 0) goto L_0x0081
            r0.L$0 = r5     // Catch:{ all -> 0x00ae }
            r0.L$1 = r7     // Catch:{ all -> 0x00ae }
            r0.Z$0 = r8     // Catch:{ all -> 0x00ae }
            r0.L$2 = r9     // Catch:{ all -> 0x00ae }
            r0.L$3 = r9     // Catch:{ all -> 0x00ae }
            r0.label = r3     // Catch:{ all -> 0x00ae }
            java.lang.Object r2 = r5.b(r7, r8, r0)     // Catch:{ all -> 0x00ae }
            if (r2 != r1) goto L_0x007c
            monitor-exit(r6)
            return r1
        L_0x007c:
            r2 = r7
            r7 = r9
        L_0x007e:
            r9 = r7
            r7 = r2
            goto L_0x0090
        L_0x0081:
            java.lang.String r2 = "RAIN_CHANCE"
            boolean r2 = com.fossil.ee7.a(r9, r2)
            if (r2 == 0) goto L_0x0090
            float r2 = r7.getRainProbability()
            r5.a(r2, r8)
        L_0x0090:
            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$a<java.lang.String> r2 = r5.w
            if (r2 == 0) goto L_0x0098
            r2.b(r9)
            goto L_0x004a
        L_0x0098:
            java.lang.String r7 = "mWeatherTasks"
            com.fossil.ee7.d(r7)
            throw r4
        L_0x009e:
            java.lang.String r7 = "mWeatherTasks"
            com.fossil.ee7.d(r7)
            throw r4
        L_0x00a4:
            com.fossil.i97 r7 = com.fossil.i97.a
            monitor-exit(r6)
            return r7
        L_0x00a8:
            java.lang.String r7 = "mWeatherTasks"
            com.fossil.ee7.d(r7)
            throw r4
        L_0x00ae:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService.a(com.portfolio.platform.data.model.microapp.weather.WeatherSettings, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public static /* synthetic */ Object a(ComplicationWeatherService complicationWeatherService, WeatherSettings weatherSettings, boolean z, fb7 fb7, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        return complicationWeatherService.a(weatherSettings, z, fb7);
    }

    @DexIgnore
    public final void a(float f2, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = wj5.f.a();
        local.d(a2, "showChanceOfRain - probability=" + f2);
        int i2 = (int) (f2 * ((float) 100));
        ChanceOfRainComplicationAppInfo chanceOfRainComplicationAppInfo = new ChanceOfRainComplicationAppInfo(i2, 3600);
        PortfolioApp c2 = PortfolioApp.g0.c();
        String str = this.t;
        if (str != null) {
            c2.a(chanceOfRainComplicationAppInfo, str);
            if5 c3 = qd5.f.c("chance-of-rain");
            if (c3 != null) {
                String str2 = this.t;
                if (str2 != null) {
                    c3.a(str2, z, "");
                } else {
                    ee7.d("mSerial");
                    throw null;
                }
            }
            qd5.f.e("chance-of-rain");
            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new e(this, i2, null), 3, null);
            return;
        }
        ee7.d("mSerial");
        throw null;
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "errorMessage");
        a<String> aVar = this.w;
        if (aVar != null) {
            String b2 = aVar.b();
            if (b2 == null) {
                return;
            }
            if (ee7.a((Object) b2, (Object) "TEMPERATURE")) {
                if5 c2 = qd5.f.c("weather");
                if (c2 != null) {
                    we7 we7 = we7.a;
                    String format = String.format("update_%s_optional_error", Arrays.copyOf(new Object[]{"weather"}, 1));
                    ee7.a((Object) format, "java.lang.String.format(format, *args)");
                    gf5 a2 = qd5.f.a(format);
                    a2.a("error_code", str);
                    c2.a(a2);
                    return;
                }
                return;
            }
            if5 c3 = qd5.f.c("chance-of-rain");
            if (c3 != null) {
                we7 we72 = we7.a;
                String format2 = String.format("update_%s_optional_error", Arrays.copyOf(new Object[]{qd5.f.d("chance-of-rain")}, 1));
                ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                gf5 a3 = qd5.f.a(format2);
                a3.a("error_code", str);
                c3.a(a3);
                return;
            }
            return;
        }
        ee7.d("mWeatherTasks");
        throw null;
    }

    @DexIgnore
    public final String b(LocationSource.ErrorState errorState) {
        int i2 = yj5.b[errorState.ordinal()];
        if (i2 == 1) {
            return mb5.LOCATION_ACCESS_DISABLED.getCode();
        }
        if (i2 == 2) {
            return mb5.BACKGROUND_LOCATION_ACCESS_DISABLED.getCode();
        }
        if (i2 != 3) {
            return mb5.UNKNOWN.getCode();
        }
        return mb5.LOCATION_SERVICE_DISABLED.getCode();
    }

    @DexIgnore
    @Override // com.fossil.bk5
    public void a() {
        FLogger.INSTANCE.getLocal().d(wj5.f.a(), VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        super.a();
        stopSelf();
    }

    @DexIgnore
    @Override // com.fossil.bk5
    public void b() {
        FLogger.INSTANCE.getLocal().d(wj5.f.a(), "forceStop");
    }
}
