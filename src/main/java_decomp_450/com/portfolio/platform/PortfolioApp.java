package com.portfolio.platform;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.webkit.MimeTypeMap;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.facebook.places.model.PlaceFields;
import com.facebook.stetho.Stetho;
import com.fossil.ad5;
import com.fossil.ah7;
import com.fossil.aj5;
import com.fossil.aw6;
import com.fossil.be;
import com.fossil.be5;
import com.fossil.bj5;
import com.fossil.ch5;
import com.fossil.dl7;
import com.fossil.dw6;
import com.fossil.e07;
import com.fossil.e8;
import com.fossil.ea7;
import com.fossil.eb5;
import com.fossil.ee7;
import com.fossil.ef;
import com.fossil.fb7;
import com.fossil.ff;
import com.fossil.fl4;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.ge5;
import com.fossil.gf5;
import com.fossil.hl4;
import com.fossil.i97;
import com.fossil.if5;
import com.fossil.ig5;
import com.fossil.ik7;
import com.fossil.jf5;
import com.fossil.jg5;
import com.fossil.jl4;
import com.fossil.jm5;
import com.fossil.kd7;
import com.fossil.km5;
import com.fossil.l07;
import com.fossil.li5;
import com.fossil.lm5;
import com.fossil.mh7;
import com.fossil.nb7;
import com.fossil.nh5;
import com.fossil.nh7;
import com.fossil.ol4;
import com.fossil.pb7;
import com.fossil.pd5;
import com.fossil.pj4;
import com.fossil.px6;
import com.fossil.qc5;
import com.fossil.qd5;
import com.fossil.qe;
import com.fossil.qe5;
import com.fossil.qh5;
import com.fossil.qj7;
import com.fossil.qn5;
import com.fossil.r60;
import com.fossil.rb7;
import com.fossil.rd5;
import com.fossil.ri5;
import com.fossil.rl4;
import com.fossil.rm;
import com.fossil.rx6;
import com.fossil.sj5;
import com.fossil.t87;
import com.fossil.t97;
import com.fossil.tb7;
import com.fossil.ti7;
import com.fossil.tj4;
import com.fossil.ui5;
import com.fossil.uw6;
import com.fossil.vh7;
import com.fossil.vm4;
import com.fossil.we7;
import com.fossil.wj4;
import com.fossil.wk5;
import com.fossil.x24;
import com.fossil.x87;
import com.fossil.x97;
import com.fossil.xh7;
import com.fossil.xi5;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.fossil.zd7;
import com.fossil.ze5;
import com.fossil.zi5;
import com.fossil.zi7;
import com.fossil.zl;
import com.fossil.zz6;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMapping;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.cloudimage.ResolutionHelper;
import com.portfolio.platform.data.model.Installation;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppDataRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.uirenew.splash.SplashScreenActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.workers.PushPendingDataWorker;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.FlutterEngineCache;
import io.flutter.embedding.engine.dart.DartExecutor;
import io.flutter.view.FlutterMain;
import java.io.File;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import net.sqlcipher.database.SQLiteDatabase;
import retrofit.Endpoints;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioApp extends ff implements Application.ActivityLifecycleCallbacks {
    @DexIgnore
    public static /* final */ String a0;
    @DexIgnore
    public static boolean b0;
    @DexIgnore
    public static PortfolioApp c0;
    @DexIgnore
    public static boolean d0;
    @DexIgnore
    public static e07 e0;
    @DexIgnore
    public static IButtonConnectivity f0;
    @DexIgnore
    public static /* final */ a g0; // = new a(null);
    @DexIgnore
    public UserRepository A;
    @DexIgnore
    public WatchFaceRepository B;
    @DexIgnore
    public QuickResponseRepository C;
    @DexIgnore
    public WorkoutSettingRepository D;
    @DexIgnore
    public WatchAppDataRepository E;
    @DexIgnore
    public wk5 F;
    @DexIgnore
    public MutableLiveData<String> G; // = new MutableLiveData<>();
    @DexIgnore
    public boolean H;
    @DexIgnore
    public boolean I; // = true;
    @DexIgnore
    public /* final */ Handler J; // = new Handler();
    @DexIgnore
    public Runnable K;
    @DexIgnore
    public tj4 L;
    @DexIgnore
    public Thread.UncaughtExceptionHandler M;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public ServerError O;
    @DexIgnore
    public if5 P;
    @DexIgnore
    public jf5 Q;
    @DexIgnore
    public NetworkChangedReceiver R;
    @DexIgnore
    public /* final */ xi5 S; // = new xi5();
    @DexIgnore
    public ri5 T;
    @DexIgnore
    public ui5 U;
    @DexIgnore
    public ThemeRepository V;
    @DexIgnore
    public aw6 W;
    @DexIgnore
    public dw6 X;
    @DexIgnore
    public vm4 Y;
    @DexIgnore
    public /* final */ yi7 Z; // = zi7.a(dl7.a(null, 1, null).plus(qj7.b()));
    @DexIgnore
    public jg5 a;
    @DexIgnore
    public li5 b;
    @DexIgnore
    public ch5 c;
    @DexIgnore
    public SummariesRepository d;
    @DexIgnore
    public SleepSummariesRepository e;
    @DexIgnore
    public pd5 f;
    @DexIgnore
    public GuestApiService g;
    @DexIgnore
    public pj4 h;
    @DexIgnore
    public ApiServiceV2 i;
    @DexIgnore
    public uw6 j;
    @DexIgnore
    public rl4 p;
    @DexIgnore
    public qn5 q;
    @DexIgnore
    public qd5 r;
    @DexIgnore
    public ApplicationEventListener s;
    @DexIgnore
    public DeviceRepository t;
    @DexIgnore
    public ge5 u;
    @DexIgnore
    public sj5 v;
    @DexIgnore
    public DianaPresetRepository w;
    @DexIgnore
    public zz6 x;
    @DexIgnore
    public WatchLocalizationRepository y;
    @DexIgnore
    public nh5 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.PortfolioApp$a$a")
        @tb7(c = "com.portfolio.platform.PortfolioApp$Companion", f = "PortfolioApp.kt", l = {2448}, m = "updateButtonService")
        /* renamed from: com.portfolio.platform.PortfolioApp$a$a  reason: collision with other inner class name */
        public static final class C0300a extends rb7 {
            @DexIgnore
            public char C$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public int label;
            @DexIgnore
            public /* synthetic */ Object result;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0300a(a aVar, fb7 fb7) {
                super(fb7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                this.result = obj;
                this.label |= RecyclerView.UNDEFINED_DURATION;
                return this.this$0.a(null, this);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(boolean z) {
            PortfolioApp.d(z);
        }

        @DexIgnore
        public final IButtonConnectivity b() {
            return PortfolioApp.f0;
        }

        @DexIgnore
        public final PortfolioApp c() {
            PortfolioApp U = PortfolioApp.c0;
            if (U != null) {
                return U;
            }
            ee7.d("instance");
            throw null;
        }

        @DexIgnore
        public final String d() {
            return PortfolioApp.a0;
        }

        @DexIgnore
        public final boolean e() {
            return PortfolioApp.d0;
        }

        @DexIgnore
        public final boolean f() {
            return PortfolioApp.b0;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final e07 a() {
            return PortfolioApp.e0;
        }

        @DexIgnore
        public final void b(MFDeviceService.b bVar) {
            ee7.b(bVar, Constants.SERVICE);
            a(bVar);
        }

        @DexIgnore
        public final void c(Object obj) {
            ee7.b(obj, "o");
            try {
                e07 a = a();
                if (a != null) {
                    a.c(obj);
                } else {
                    ee7.a();
                    throw null;
                }
            } catch (Exception unused) {
            }
        }

        @DexIgnore
        public final void a(IButtonConnectivity iButtonConnectivity) {
            PortfolioApp.f0 = iButtonConnectivity;
        }

        @DexIgnore
        public final void b(Object obj) {
            ee7.b(obj, "o");
            try {
                e07 a = a();
                if (a != null) {
                    a.b(obj);
                } else {
                    ee7.a();
                    throw null;
                }
            } catch (Exception unused) {
            }
        }

        @DexIgnore
        public final void a(MFDeviceService.b bVar) {
            PortfolioApp.a(bVar);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0067  */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object a(com.misfit.frameworks.buttonservice.IButtonConnectivity r21, com.fossil.fb7<? super com.fossil.i97> r22) {
            /*
                r20 = this;
                r1 = r20
                r0 = r21
                r2 = r22
                java.lang.String r3 = "release"
                boolean r4 = r2 instanceof com.portfolio.platform.PortfolioApp.a.C0300a
                if (r4 == 0) goto L_0x001b
                r4 = r2
                com.portfolio.platform.PortfolioApp$a$a r4 = (com.portfolio.platform.PortfolioApp.a.C0300a) r4
                int r5 = r4.label
                r6 = -2147483648(0xffffffff80000000, float:-0.0)
                r7 = r5 & r6
                if (r7 == 0) goto L_0x001b
                int r5 = r5 - r6
                r4.label = r5
                goto L_0x0020
            L_0x001b:
                com.portfolio.platform.PortfolioApp$a$a r4 = new com.portfolio.platform.PortfolioApp$a$a
                r4.<init>(r1, r2)
            L_0x0020:
                java.lang.Object r2 = r4.result
                java.lang.Object r5 = com.fossil.nb7.a()
                int r6 = r4.label
                r7 = 1
                if (r6 == 0) goto L_0x0067
                if (r6 != r7) goto L_0x005f
                char r0 = r4.C$0
                java.lang.Object r3 = r4.L$7
                java.lang.String r3 = (java.lang.String) r3
                java.lang.Object r5 = r4.L$6
                java.lang.String r5 = (java.lang.String) r5
                java.lang.Object r6 = r4.L$5
                java.lang.String r6 = (java.lang.String) r6
                java.lang.Object r7 = r4.L$4
                com.misfit.frameworks.buttonservice.IButtonConnectivity r7 = (com.misfit.frameworks.buttonservice.IButtonConnectivity) r7
                java.lang.Object r8 = r4.L$3
                com.misfit.frameworks.buttonservice.log.model.CloudLogConfig r8 = (com.misfit.frameworks.buttonservice.log.model.CloudLogConfig) r8
                java.lang.Object r9 = r4.L$2
                com.portfolio.platform.data.Access r9 = (com.portfolio.platform.data.Access) r9
                java.lang.Object r9 = r4.L$1
                com.misfit.frameworks.buttonservice.IButtonConnectivity r9 = (com.misfit.frameworks.buttonservice.IButtonConnectivity) r9
                java.lang.Object r4 = r4.L$0
                com.portfolio.platform.PortfolioApp$a r4 = (com.portfolio.platform.PortfolioApp.a) r4
                com.fossil.t87.a(r2)     // Catch:{ Exception -> 0x005c }
                r16 = r0
                r15 = r3
                r14 = r5
                r13 = r6
                r12 = r7
                r19 = r8
                goto L_0x011d
            L_0x005c:
                r0 = move-exception
                goto L_0x0136
            L_0x005f:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r2)
                throw r0
            L_0x0067:
                com.fossil.t87.a(r2)
                r20.a(r21)
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                java.lang.String r6 = r20.d()
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                java.lang.String r9 = "updateButtonService "
                r8.append(r9)
                r8.append(r0)
                java.lang.String r8 = r8.toString()
                r2.d(r6, r8)
                com.fossil.ng5 r2 = com.fossil.ng5.a()     // Catch:{ Exception -> 0x0134 }
                com.portfolio.platform.PortfolioApp r6 = r20.c()     // Catch:{ Exception -> 0x0134 }
                com.portfolio.platform.data.Access r2 = r2.a(r6)     // Catch:{ Exception -> 0x0134 }
                com.misfit.frameworks.buttonservice.log.model.CloudLogConfig r8 = new com.misfit.frameworks.buttonservice.log.model.CloudLogConfig     // Catch:{ Exception -> 0x0134 }
                com.fossil.ol4$a r6 = com.fossil.ol4.A     // Catch:{ Exception -> 0x0134 }
                java.lang.String r6 = r6.h()     // Catch:{ Exception -> 0x0134 }
                com.fossil.ol4$a r9 = com.fossil.ol4.A     // Catch:{ Exception -> 0x0134 }
                java.lang.String r9 = r9.e()     // Catch:{ Exception -> 0x0134 }
                java.lang.String r10 = ""
                if (r2 == 0) goto L_0x00b0
                java.lang.String r11 = r2.getI()
                if (r11 == 0) goto L_0x00b0
                goto L_0x00b1
            L_0x00b0:
                r11 = r10
            L_0x00b1:
                if (r2 == 0) goto L_0x00ba
                java.lang.String r12 = r2.getK()
                if (r12 == 0) goto L_0x00ba
                goto L_0x00bb
            L_0x00ba:
                r12 = r10
            L_0x00bb:
                r8.<init>(r6, r9, r11, r12)
                com.misfit.frameworks.buttonservice.IButtonConnectivity r6 = r20.b()
                if (r6 == 0) goto L_0x012f
                com.fossil.ol4$a r9 = com.fossil.ol4.A
                java.lang.String r9 = r9.s()
                if (r2 == 0) goto L_0x00d3
                java.lang.String r11 = r2.getL()
                if (r11 == 0) goto L_0x00d3
                goto L_0x00d4
            L_0x00d3:
                r11 = r10
            L_0x00d4:
                if (r2 == 0) goto L_0x00dd
                java.lang.String r12 = r2.getM()
                if (r12 == 0) goto L_0x00dd
                r10 = r12
            L_0x00dd:
                boolean r3 = com.fossil.ee7.a(r3, r3)
                if (r3 == 0) goto L_0x00f0
                com.portfolio.platform.PortfolioApp r3 = r20.c()
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$BRAND r3 = r3.k()
            L_0x00eb:
                char r3 = r3.getPrefix()
                goto L_0x00f3
            L_0x00f0:
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$BRAND r3 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.BRAND.UNKNOWN
                goto L_0x00eb
            L_0x00f3:
                com.fossil.rd5$a r12 = com.fossil.rd5.g
                com.fossil.rd5 r12 = r12.b()
                r4.L$0 = r1
                r4.L$1 = r0
                r4.L$2 = r2
                r4.L$3 = r8
                r4.L$4 = r6
                r4.L$5 = r9
                r4.L$6 = r11
                r4.L$7 = r10
                r4.C$0 = r3
                r4.label = r7
                java.lang.Object r2 = r12.a(r4)
                if (r2 != r5) goto L_0x0114
                return r5
            L_0x0114:
                r4 = r1
                r16 = r3
                r12 = r6
                r19 = r8
                r13 = r9
                r15 = r10
                r14 = r11
            L_0x011d:
                r17 = r2
                com.misfit.frameworks.buttonservice.log.model.AppLogInfo r17 = (com.misfit.frameworks.buttonservice.log.model.AppLogInfo) r17
                com.fossil.rd5$a r0 = com.fossil.rd5.g
                com.fossil.rd5 r0 = r0.b()
                com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo r18 = r0.a()
                r12.init(r13, r14, r15, r16, r17, r18, r19)
                goto L_0x0154
            L_0x012f:
                com.fossil.ee7.a()
                r0 = 0
                throw r0
            L_0x0134:
                r0 = move-exception
                r4 = r1
            L_0x0136:
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                java.lang.String r3 = r4.d()
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = ".updateButtonService(), ex="
                r4.append(r5)
                r4.append(r0)
                java.lang.String r0 = r4.toString()
                r2.e(r3, r0)
            L_0x0154:
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.a.a(com.misfit.frameworks.buttonservice.IButtonConnectivity, com.fossil.fb7):java.lang.Object");
        }

        @DexIgnore
        public final void a(Object obj) {
            ee7.b(obj, Constants.EVENT);
            e07 a = a();
            if (a != null) {
                a.a(obj);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public enum b {
        CREATE,
        START,
        RESUME,
        PAUSE,
        STOP,
        DESTROY
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp$configureWorkManager$2", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(PortfolioApp portfolioApp, fb7 fb7) {
            super(2, fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                zl.a aVar = new zl.a();
                aVar.a(this.this$0.o());
                zl a = aVar.a();
                ee7.a((Object) a, "Configuration.Builder()\n\u2026\n                .build()");
                rm.a(this.this$0, a);
                PushPendingDataWorker.G.a();
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp$deleteInstanceId$2", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore
        public d(fb7 fb7) {
            super(2, fb7);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                try {
                    FirebaseInstanceId.p().b();
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String d = PortfolioApp.g0.d();
                    local.d(d, "deleteInstanceId ex-" + e);
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {326}, m = "downloadWatchAppData")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(PortfolioApp portfolioApp, fb7 fb7) {
            super(fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp$executeSetLocalization$1", f = "PortfolioApp.kt", l = {FailureCode.FAILED_TO_START_STREAMING}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(PortfolioApp portfolioApp, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = portfolioApp;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, this.$serial, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            IButtonConnectivity b;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                WatchLocalizationRepository t = this.this$0.t();
                this.L$0 = yi7;
                this.label = 1;
                obj = t.getWatchLocalizationFromServer(true, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str = (String) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d = PortfolioApp.g0.d();
            local.d(d, "path of localization file: " + str);
            if (!(str == null || (b = PortfolioApp.g0.b()) == null)) {
                b.setLocalizationData(new LocalizationData(str, null, 2, null), this.$serial);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements fl4.e<qn5.d, qn5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g(PortfolioApp portfolioApp) {
            this.a = portfolioApp;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(qn5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(PortfolioApp.g0.d(), "logout successfully - start welcome activity");
            Intent intent = new Intent(this.a, WelcomeActivity.class);
            intent.addFlags(268468224);
            this.a.startActivity(intent);
            this.a.b((ServerError) null);
            this.a.c(false);
        }

        @DexIgnore
        public void a(qn5.c cVar) {
            ee7.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(PortfolioApp.g0.d(), "logout unsuccessfully");
            this.a.b((ServerError) null);
            this.a.c(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp$forceReconnectDevice$2", f = "PortfolioApp.kt", l = {FailureCode.USER_KILL_APP_FROM_TASK_MANAGER}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(PortfolioApp portfolioApp, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = portfolioApp;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, this.$serial, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Object> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            IButtonConnectivity iButtonConnectivity;
            String str;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = PortfolioApp.g0.d();
                local.d(d, "Inside " + PortfolioApp.g0.d() + ".forceReconnectDevice");
                iButtonConnectivity = PortfolioApp.g0.b();
                if (iButtonConnectivity != null) {
                    String str2 = this.$serial;
                    PortfolioApp portfolioApp = this.this$0;
                    this.L$0 = yi7;
                    this.L$1 = iButtonConnectivity;
                    this.L$2 = str2;
                    this.label = 1;
                    obj = portfolioApp.d(this);
                    if (obj == a) {
                        return a;
                    }
                    str = str2;
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (i == 1) {
                str = (String) this.L$2;
                iButtonConnectivity = (IButtonConnectivity) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                try {
                    t87.a(obj);
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String d2 = PortfolioApp.g0.d();
                    local2.e(d2, "Error inside " + PortfolioApp.g0.d() + ".deviceReconnect - e=" + e);
                    return i97.a;
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return pb7.a(iButtonConnectivity.deviceForceReconnect(str, (UserProfile) obj));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {367, 379, 380, 381, 382, 419, 420}, m = "getCurrentUserProfile")
    public static final class i extends rb7 {
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public float F$0;
        @DexIgnore
        public float F$1;
        @DexIgnore
        public float F$2;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public int I$2;
        @DexIgnore
        public int I$3;
        @DexIgnore
        public int I$4;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(PortfolioApp portfolioApp, fb7 fb7) {
            super(fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {547, 554}, m = "getPassphrase")
    public static final class j extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(PortfolioApp portfolioApp, fb7 fb7) {
            super(fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {345}, m = "getUserRegisterDate")
    public static final class k extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(PortfolioApp portfolioApp, fb7 fb7) {
            super(fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {438}, m = "getWorkoutDetectionSetting")
    public static final class l extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(PortfolioApp portfolioApp, fb7 fb7) {
            super(fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp$onActiveDeviceStealed$2", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class m extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(PortfolioApp portfolioApp, fb7 fb7) {
            super(2, fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            m mVar = new m(this.this$0, fb7);
            mVar.p$ = (yi7) obj;
            return mVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((m) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = PortfolioApp.g0.d();
                local.d(d, "activeDevice=" + this.this$0.c() + ", was stealed remove it!!!");
                if (be5.o.f(this.this$0.c())) {
                    this.this$0.y().deleteWatchFacesWithSerial(this.this$0.c());
                }
                PortfolioApp portfolioApp = this.this$0;
                portfolioApp.r(portfolioApp.c());
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp a;

        @DexIgnore
        public n(PortfolioApp portfolioApp) {
            this.a = portfolioApp;
        }

        @DexIgnore
        public final void run() {
            if (!this.a.C() || !this.a.I) {
                PortfolioApp.g0.a(false);
                FLogger.INSTANCE.getLocal().d(PortfolioApp.g0.d(), "still foreground");
                return;
            }
            this.a.b(false);
            PortfolioApp.g0.a(true);
            FLogger.INSTANCE.getLocal().d(PortfolioApp.g0.d(), "went background");
            this.a.H();
            if5 a2 = this.a.P;
            if (a2 != null) {
                a2.a("");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp$onCreate$1", f = "PortfolioApp.kt", l = {596, 597, 598, 604, 619, 630}, m = "invokeSuspend")
    public static final class o extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(PortfolioApp portfolioApp, fb7 fb7) {
            super(2, fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            o oVar = new o(this.this$0, fb7);
            oVar.p$ = (yi7) obj;
            return oVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((o) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x008f A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x00a1 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x0112 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x0113  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x014d  */
        /* JADX WARNING: Removed duplicated region for block: B:51:0x0158  */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x017b A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:60:0x017c  */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x01bd A[RETURN] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
                r14 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r14.label
                java.lang.String r2 = ""
                switch(r1) {
                    case 0: goto L_0x006f;
                    case 1: goto L_0x0067;
                    case 2: goto L_0x005f;
                    case 3: goto L_0x0057;
                    case 4: goto L_0x0039;
                    case 5: goto L_0x0028;
                    case 6: goto L_0x0013;
                    default: goto L_0x000b;
                }
            L_0x000b:
                java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r15.<init>(r0)
                throw r15
            L_0x0013:
                java.lang.Object r0 = r14.L$3
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                java.lang.Object r0 = r14.L$2
                com.misfit.frameworks.buttonservice.log.model.CloudLogConfig r0 = (com.misfit.frameworks.buttonservice.log.model.CloudLogConfig) r0
                java.lang.Object r0 = r14.L$1
                com.portfolio.platform.data.Access r0 = (com.portfolio.platform.data.Access) r0
                java.lang.Object r0 = r14.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r15)
                goto L_0x01be
            L_0x0028:
                java.lang.Object r1 = r14.L$2
                com.misfit.frameworks.buttonservice.log.model.CloudLogConfig r1 = (com.misfit.frameworks.buttonservice.log.model.CloudLogConfig) r1
                java.lang.Object r3 = r14.L$1
                com.portfolio.platform.data.Access r3 = (com.portfolio.platform.data.Access) r3
                java.lang.Object r4 = r14.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r15)
                goto L_0x017e
            L_0x0039:
                java.lang.Object r1 = r14.L$4
                java.lang.String r1 = (java.lang.String) r1
                java.lang.Object r3 = r14.L$3
                com.misfit.frameworks.buttonservice.log.FLogger r3 = (com.misfit.frameworks.buttonservice.log.FLogger) r3
                java.lang.Object r4 = r14.L$2
                com.misfit.frameworks.buttonservice.log.model.CloudLogConfig r4 = (com.misfit.frameworks.buttonservice.log.model.CloudLogConfig) r4
                java.lang.Object r5 = r14.L$1
                com.portfolio.platform.data.Access r5 = (com.portfolio.platform.data.Access) r5
                java.lang.Object r6 = r14.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
                com.fossil.t87.a(r15)
                r11 = r5
                r12 = r6
                r13 = r4
                r4 = r1
                r1 = r13
                goto L_0x0119
            L_0x0057:
                java.lang.Object r1 = r14.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r15)
                goto L_0x00a2
            L_0x005f:
                java.lang.Object r1 = r14.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r15)
                goto L_0x0090
            L_0x0067:
                java.lang.Object r1 = r14.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r15)
                goto L_0x0082
            L_0x006f:
                com.fossil.t87.a(r15)
                com.fossil.yi7 r1 = r14.p$
                com.portfolio.platform.PortfolioApp r15 = r14.this$0
                r14.L$0 = r1
                r3 = 1
                r14.label = r3
                java.lang.Object r15 = r15.a(r14)
                if (r15 != r0) goto L_0x0082
                return r0
            L_0x0082:
                com.portfolio.platform.PortfolioApp r15 = r14.this$0
                r14.L$0 = r1
                r3 = 2
                r14.label = r3
                java.lang.Object r15 = r15.i(r14)
                if (r15 != r0) goto L_0x0090
                return r0
            L_0x0090:
                com.portfolio.platform.PortfolioApp r15 = r14.this$0
                com.portfolio.platform.data.source.ThemeRepository r15 = r15.r()
                r14.L$0 = r1
                r3 = 3
                r14.label = r3
                java.lang.Object r15 = r15.initializeLocalTheme(r14)
                if (r15 != r0) goto L_0x00a2
                return r0
            L_0x00a2:
                com.fossil.ng5 r15 = com.fossil.ng5.a()
                com.portfolio.platform.PortfolioApp r3 = r14.this$0
                com.portfolio.platform.data.Access r15 = r15.a(r3)
                com.fossil.zs1 r3 = com.fossil.zs1.b
                com.fossil.dh0 r4 = new com.fossil.dh0
                com.fossil.ol4$a r5 = com.fossil.ol4.A
                java.lang.String r5 = r5.s()
                if (r15 == 0) goto L_0x00bf
                java.lang.String r6 = r15.getL()
                if (r6 == 0) goto L_0x00bf
                goto L_0x00c0
            L_0x00bf:
                r6 = r2
            L_0x00c0:
                if (r15 == 0) goto L_0x00c9
                java.lang.String r7 = r15.getM()
                if (r7 == 0) goto L_0x00c9
                goto L_0x00ca
            L_0x00c9:
                r7 = r2
            L_0x00ca:
                r4.<init>(r5, r6, r7)
                r3.a(r4)
                com.misfit.frameworks.buttonservice.log.model.CloudLogConfig r3 = new com.misfit.frameworks.buttonservice.log.model.CloudLogConfig
                com.fossil.ol4$a r4 = com.fossil.ol4.A
                java.lang.String r4 = r4.h()
                com.fossil.ol4$a r5 = com.fossil.ol4.A
                java.lang.String r5 = r5.e()
                if (r15 == 0) goto L_0x00e7
                java.lang.String r6 = r15.getI()
                if (r6 == 0) goto L_0x00e7
                goto L_0x00e8
            L_0x00e7:
                r6 = r2
            L_0x00e8:
                if (r15 == 0) goto L_0x00f1
                java.lang.String r7 = r15.getK()
                if (r7 == 0) goto L_0x00f1
                goto L_0x00f2
            L_0x00f1:
                r7 = r2
            L_0x00f2:
                r3.<init>(r4, r5, r6, r7)
                com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                java.lang.String r5 = "App"
                com.fossil.rd5$a r6 = com.fossil.rd5.g
                com.fossil.rd5 r6 = r6.b()
                r14.L$0 = r1
                r14.L$1 = r15
                r14.L$2 = r3
                r14.L$3 = r4
                r14.L$4 = r5
                r7 = 4
                r14.label = r7
                java.lang.Object r6 = r6.a(r14)
                if (r6 != r0) goto L_0x0113
                return r0
            L_0x0113:
                r11 = r15
                r12 = r1
                r1 = r3
                r3 = r4
                r4 = r5
                r15 = r6
            L_0x0119:
                r5 = r15
                com.misfit.frameworks.buttonservice.log.model.AppLogInfo r5 = (com.misfit.frameworks.buttonservice.log.model.AppLogInfo) r5
                com.fossil.rd5$a r15 = com.fossil.rd5.g
                com.fossil.rd5 r15 = r15.b()
                com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo r6 = r15.a()
                com.portfolio.platform.PortfolioApp r8 = r14.this$0
                r9 = 1
                java.lang.String r10 = "App"
                r7 = r1
                r3.init(r4, r5, r6, r7, r8, r9, r10)
                com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.IRemoteFLogger r15 = r15.getRemote()
                r15.flush()
                java.lang.String r15 = "0000000000000"
                if (r11 == 0) goto L_0x0143
                java.lang.String r3 = r11.getO()
                if (r3 == 0) goto L_0x0143
                goto L_0x0144
            L_0x0143:
                r3 = r15
            L_0x0144:
                com.facebook.FacebookSdk.setApplicationId(r3)
                boolean r3 = com.facebook.FacebookSdk.isInitialized()
                if (r3 != 0) goto L_0x0152
                com.portfolio.platform.PortfolioApp r3 = r14.this$0
                com.facebook.FacebookSdk.sdkInitialize(r3)
            L_0x0152:
                boolean r3 = com.google.android.libraries.places.api.Places.isInitialized()
                if (r3 != 0) goto L_0x0166
                com.portfolio.platform.PortfolioApp r3 = r14.this$0
                if (r11 == 0) goto L_0x0163
                java.lang.String r4 = r11.getN()
                if (r4 == 0) goto L_0x0163
                r15 = r4
            L_0x0163:
                com.google.android.libraries.places.api.Places.initialize(r3, r15)
            L_0x0166:
                com.portfolio.platform.PortfolioApp r15 = r14.this$0
                com.portfolio.platform.data.source.UserRepository r15 = r15.s()
                r14.L$0 = r12
                r14.L$1 = r11
                r14.L$2 = r1
                r3 = 5
                r14.label = r3
                java.lang.Object r15 = r15.getCurrentUser(r14)
                if (r15 != r0) goto L_0x017c
                return r0
            L_0x017c:
                r3 = r11
                r4 = r12
            L_0x017e:
                com.portfolio.platform.data.model.MFUser r15 = (com.portfolio.platform.data.model.MFUser) r15
                com.portfolio.platform.PortfolioApp r5 = r14.this$0
                r5.a(r15)
                com.fossil.rd5$a r5 = com.fossil.rd5.g
                r5.d()
                com.zendesk.sdk.network.impl.ZendeskConfig r5 = com.zendesk.sdk.network.impl.ZendeskConfig.INSTANCE
                com.portfolio.platform.PortfolioApp r6 = r14.this$0
                com.fossil.ol4$a r7 = com.fossil.ol4.A
                java.lang.String r7 = r7.y()
                if (r3 == 0) goto L_0x019d
                java.lang.String r8 = r3.getG()
                if (r8 == 0) goto L_0x019d
                goto L_0x019e
            L_0x019d:
                r8 = r2
            L_0x019e:
                if (r3 == 0) goto L_0x01a7
                java.lang.String r9 = r3.getH()
                if (r9 == 0) goto L_0x01a7
                r2 = r9
            L_0x01a7:
                r5.init(r6, r7, r8, r2)
                com.portfolio.platform.PortfolioApp r2 = r14.this$0
                r14.L$0 = r4
                r14.L$1 = r3
                r14.L$2 = r1
                r14.L$3 = r15
                r15 = 6
                r14.label = r15
                java.lang.Object r15 = r2.k(r14)
                if (r15 != r0) goto L_0x01be
                return r0
            L_0x01be:
                com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.IRemoteFLogger r0 = r15.getRemote()
                com.misfit.frameworks.buttonservice.log.FLogger$Component r1 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
                com.misfit.frameworks.buttonservice.log.FLogger$Session r2 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER
                com.portfolio.platform.PortfolioApp$a r15 = com.portfolio.platform.PortfolioApp.g0
                java.lang.String r4 = r15.d()
                java.lang.StringBuilder r15 = new java.lang.StringBuilder
                r15.<init>()
                java.lang.String r3 = "[App] onAppCreated at "
                r15.append(r3)
                long r5 = java.lang.System.currentTimeMillis()
                r15.append(r5)
                java.lang.String r5 = r15.toString()
                java.lang.String r3 = ""
                r0.i(r1, r2, r3, r4, r5)
                com.fossil.i97 r15 = com.fossil.i97.a
                return r15
                switch-data {0->0x006f, 1->0x0067, 2->0x005f, 3->0x0057, 4->0x0039, 5->0x0028, 6->0x0013, }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.o.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements Thread.UncaughtExceptionHandler {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp a;

        @DexIgnore
        public p(PortfolioApp portfolioApp) {
            this.a = portfolioApp;
        }

        @DexIgnore
        public final void uncaughtException(Thread thread, Throwable th) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d = PortfolioApp.g0.d();
            local.e(d, "uncaughtException - ex=" + th);
            th.printStackTrace();
            ee7.a((Object) th, "e");
            StackTraceElement[] stackTrace = th.getStackTrace();
            int length = stackTrace != null ? stackTrace.length : 0;
            if (length > 0) {
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (stackTrace != null) {
                        StackTraceElement stackTraceElement = stackTrace[i];
                        ee7.a((Object) stackTraceElement, "element");
                        String className = stackTraceElement.getClassName();
                        ee7.a((Object) className, "element.className");
                        String simpleName = ButtonService.class.getSimpleName();
                        ee7.a((Object) simpleName, "ButtonService::class.java.simpleName");
                        if (nh7.a((CharSequence) className, (CharSequence) simpleName, false, 2, (Object) null)) {
                            FLogger.INSTANCE.getLocal().e(PortfolioApp.g0.d(), "uncaughtException - stopLogService");
                            this.a.b((int) FailureCode.APP_CRASH_FROM_BUTTON_SERVICE);
                            break;
                        }
                        i++;
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
            long currentTimeMillis = System.currentTimeMillis();
            this.a.v().c(currentTimeMillis);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String d2 = PortfolioApp.g0.d();
            local2.d(d2, "Inside .uncaughtException - currentTime = " + currentTimeMillis);
            Thread.UncaughtExceptionHandler b = this.a.M;
            if (b != null) {
                b.uncaughtException(thread, th);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp$pairDevice$2", f = "PortfolioApp.kt", l = {1795}, m = "invokeSuspend")
    public static final class q extends zb7 implements kd7<yi7, fb7<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $macAddress;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(PortfolioApp portfolioApp, String str, String str2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = portfolioApp;
            this.$serial = str;
            this.$macAddress = str2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            q qVar = new q(this.this$0, this.$serial, this.$macAddress, fb7);
            qVar.p$ = (yi7) obj;
            return qVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Object> fb7) {
            return ((q) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            IButtonConnectivity iButtonConnectivity;
            String str;
            String str2;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = PortfolioApp.g0.d();
                local.d(d, "Inside " + PortfolioApp.g0.d() + ".pairDevice");
                IButtonConnectivity b = PortfolioApp.g0.b();
                if (b != null) {
                    String str3 = this.$serial;
                    String str4 = this.$macAddress;
                    PortfolioApp portfolioApp = this.this$0;
                    this.L$0 = yi7;
                    this.L$1 = b;
                    this.L$2 = str3;
                    this.L$3 = str4;
                    this.label = 1;
                    obj = portfolioApp.d(this);
                    if (obj == a) {
                        return a;
                    }
                    iButtonConnectivity = b;
                    str = str3;
                    str2 = str4;
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (i == 1) {
                str2 = (String) this.L$3;
                str = (String) this.L$2;
                iButtonConnectivity = (IButtonConnectivity) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                try {
                    t87.a(obj);
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String d2 = PortfolioApp.g0.d();
                    local2.e(d2, "Error inside " + PortfolioApp.g0.d() + ".pairDevice - e=" + e);
                    return i97.a;
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return pb7.a(iButtonConnectivity.pairDevice(str, str2, (UserProfile) obj));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {782}, m = "registerContactObserver")
    public static final class r extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public r(PortfolioApp portfolioApp, fb7 fb7) {
            super(fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp$saveInstallation$2", f = "PortfolioApp.kt", l = {1128, 1171}, m = "invokeSuspend")
    public static final class s extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.PortfolioApp$saveInstallation$2$1", f = "PortfolioApp.kt", l = {1172}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Installation $installation;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ s this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.PortfolioApp$s$a$a")
            @tb7(c = "com.portfolio.platform.PortfolioApp$saveInstallation$2$1$response$1", f = "PortfolioApp.kt", l = {1172}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.PortfolioApp$s$a$a  reason: collision with other inner class name */
            public static final class C0301a extends zb7 implements gd7<fb7<? super fv7<Installation>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0301a(a aVar, fb7 fb7) {
                    super(1, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    return new C0301a(this.this$0, fb7);
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // com.fossil.gd7
                public final Object invoke(fb7<? super fv7<Installation>> fb7) {
                    return ((C0301a) create(fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        ApiServiceV2 n = this.this$0.this$0.this$0.n();
                        Installation installation = this.this$0.$installation;
                        this.label = 1;
                        obj = n.insertInstallation(installation, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(s sVar, Installation installation, fb7 fb7) {
                super(2, fb7);
                this.this$0 = sVar;
                this.$installation = installation;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$installation, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    C0301a aVar = new C0301a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = aj5.a(aVar, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                zi5 zi5 = (zi5) obj;
                if (zi5 instanceof bj5) {
                    bj5 bj5 = (bj5) zi5;
                    if (bj5.a() != null) {
                        this.this$0.this$0.v().p(((Installation) bj5.a()).getInstallationId());
                    }
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public s(PortfolioApp portfolioApp, fb7 fb7) {
            super(2, fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            s sVar = new s(this.this$0, fb7);
            sVar.p$ = (yi7) obj;
            return sVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((s) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                UserRepository s = this.this$0.s();
                this.L$0 = yi7;
                this.label = 1;
                obj = s.getCurrentUser(this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                String str = (String) this.L$3;
                Installation installation = (Installation) this.L$2;
                MFUser mFUser = (MFUser) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser2 = (MFUser) obj;
            if (mFUser2 != null) {
                Installation installation2 = new Installation();
                installation2.setAppMarketingVersion("4.5.0");
                installation2.setAppBuildNumber(26467);
                installation2.setModel(Build.MODEL);
                installation2.setOsVersion(Build.VERSION.RELEASE);
                installation2.setInstallationId(rd5.g.a(mFUser2.getUserId()));
                installation2.setAppPermissions(px6.a.a());
                installation2.setDeviceToken(this.this$0.v().j());
                TimeZone timeZone = TimeZone.getDefault();
                ee7.a((Object) timeZone, "TimeZone.getDefault()");
                String id = timeZone.getID();
                ee7.a((Object) id, "zone");
                if (nh7.a((CharSequence) id, '/', 0, false, 6, (Object) null) > 0) {
                    installation2.setTimeZone(id);
                }
                try {
                    String packageName = this.this$0.getPackageName();
                    PackageManager packageManager = this.this$0.getPackageManager();
                    String str2 = packageManager.getPackageInfo(packageName, 0).versionName;
                    String obj2 = packageManager.getApplicationLabel(packageManager.getApplicationInfo(packageName, 0)).toString();
                    if (!TextUtils.isEmpty(obj2)) {
                        installation2.setAppName(obj2);
                    }
                    if (!TextUtils.isEmpty(str2)) {
                        installation2.setAppVersion(str2);
                    }
                } catch (Exception unused) {
                    FLogger.INSTANCE.getLocal().e(PortfolioApp.g0.d(), "Cannot load package info; will not be saved to installation");
                }
                installation2.setDeviceType("android");
                this.this$0.a(installation2);
                ti7 b = qj7.b();
                a aVar = new a(this, installation2, null);
                this.L$0 = yi7;
                this.L$1 = mFUser2;
                this.L$2 = installation2;
                this.L$3 = id;
                this.label = 2;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2149}, m = "setImplicitDeviceConfig")
    public static final class t extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public t(PortfolioApp portfolioApp, fb7 fb7) {
            super(fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c((String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {1041}, m = "switchActiveDevice")
    public static final class u extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public u(PortfolioApp portfolioApp, fb7 fb7) {
            super(fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d((String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp$updateActiveDeviceInfoLog$1", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class v extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore
        public v(fb7 fb7) {
            super(2, fb7);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            v vVar = new v(fb7);
            vVar.p$ = (yi7) obj;
            return vVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((v) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                ActiveDeviceInfo a = rd5.g.b().a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = PortfolioApp.g0.d();
                local.d(d, ".updateActiveDeviceInfoLog(), " + new Gson().a(a));
                FLogger.INSTANCE.getRemote().updateActiveDeviceInfo(a);
                try {
                    IButtonConnectivity b = PortfolioApp.g0.b();
                    if (b != null) {
                        b.updateActiveDeviceInfoLog(a);
                    }
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String d2 = PortfolioApp.g0.d();
                    local2.e(d2, ".updateActiveDeviceInfoToButtonService(), error=" + e);
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2396}, m = "updateAppLogInfo")
    public static final class w extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public w(PortfolioApp portfolioApp, fb7 fb7) {
            super(fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {766}, m = "updateCrashlyticsUserInformation")
    public static final class x extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public x(PortfolioApp portfolioApp, fb7 fb7) {
            super(fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.n(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {1375, 1378}, m = "updatePercentageGoalProgress")
    public static final class y extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public y(PortfolioApp portfolioApp, fb7 fb7) {
            super(fb7);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(false, (fb7<? super i97>) this);
        }
    }

    /*
    static {
        String simpleName = PortfolioApp.class.getSimpleName();
        ee7.a((Object) simpleName, "PortfolioApp::class.java.simpleName");
        a0 = simpleName;
    }
    */

    @DexIgnore
    public static final IButtonConnectivity Y() {
        return f0;
    }

    @DexIgnore
    public static final /* synthetic */ void a(MFDeviceService.b bVar) {
    }

    @DexIgnore
    public static final /* synthetic */ void d(boolean z2) {
    }

    @DexIgnore
    public final boolean A() {
        wk5 wk5 = this.F;
        if (wk5 != null) {
            return wk5.c();
        }
        ee7.d("mWorkoutGpsManager");
        throw null;
    }

    @DexIgnore
    public final void B() {
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.interruptCurrentSession(c());
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.e(str, "Error while interruptCurrentSession - e=" + e2);
        }
    }

    @DexIgnore
    public final boolean C() {
        return this.H;
    }

    @DexIgnore
    public final boolean D() {
        Boolean bool;
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            int[] listActiveCommunicator = iButtonConnectivity != null ? iButtonConnectivity.getListActiveCommunicator() : null;
            List<Integer> e2 = listActiveCommunicator != null ? t97.e(listActiveCommunicator) : null;
            if (e2 != null) {
                bool = Boolean.valueOf(!e2.isEmpty());
            } else {
                bool = null;
            }
            if (bool == null) {
                ee7.a();
                throw null;
            } else if (!bool.booleanValue() || !e2.contains(Integer.valueOf(CommunicateMode.OTA.getValue()))) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e3) {
            e3.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    public final boolean E() {
        return ee7.a((Object) getPackageName(), (Object) q());
    }

    @DexIgnore
    public final boolean F() {
        String string = Settings.Secure.getString(getContentResolver(), "enabled_notification_listeners");
        String str = getPackageName() + "/" + FossilNotificationListenerService.class.getCanonicalName();
        FLogger.INSTANCE.getLocal().d(a0, "isNotificationListenerEnabled() - notificationServicePath = " + str);
        if (!TextUtils.isEmpty(string)) {
            FLogger.INSTANCE.getLocal().d(a0, "isNotificationListenerEnabled() enabledNotificationListeners = " + string);
        }
        if (TextUtils.isEmpty(string)) {
            return false;
        }
        ee7.a((Object) string, "enabledNotificationListeners");
        return nh7.a(string, str, false, 2, null);
    }

    @DexIgnore
    public final boolean G() {
        return mh7.b("release", "release", true);
    }

    @DexIgnore
    public final void H() {
        if5 c2 = qd5.f.c("ota_session");
        if5 c3 = qd5.f.c("sync_session");
        if5 c4 = qd5.f.c("setup_device_session");
        if (c2 != null && c2.b()) {
            c2.a(qd5.f.a("ota_session_go_to_background"));
        } else if (c3 != null && c3.b()) {
            c3.a(qd5.f.a("sync_session_go_to_background"));
        } else if (c4 != null && c4.b()) {
            c4.a(qd5.f.a("setup_device_session_go_to_background"));
        }
    }

    @DexIgnore
    public final void I() {
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.logOut();
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final long J() {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceReadRealTimeStep(c());
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void K() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
        intentFilter.addDataScheme(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY);
        registerReceiver(this.b, intentFilter);
    }

    @DexIgnore
    public final void L() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.provider.Telephony.WAP_PUSH_RECEIVED");
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        ui5 ui5 = this.U;
        if (ui5 != null) {
            registerReceiver(ui5, intentFilter);
        } else {
            ee7.d("mMessageReceiver");
            throw null;
        }
    }

    @DexIgnore
    public final void M() {
        try {
            ri5 ri5 = this.T;
            if (ri5 != null) {
                registerReceiver(ri5, new IntentFilter("android.intent.action.PHONE_STATE"));
                L();
                return;
            }
            ee7.d("mPhoneCallReceiver");
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.e(str, "registerTelephonyReceiver throw exception: " + e2.getMessage());
        }
    }

    @DexIgnore
    public final void N() {
        pd5 pd5 = this.f;
        if (pd5 != null) {
            pd5.a(this);
            pd5 pd52 = this.f;
            if (pd52 != null) {
                pd52.d(this);
            } else {
                ee7.d("mAlarmHelper");
                throw null;
            }
        } else {
            ee7.d("mAlarmHelper");
            throw null;
        }
    }

    @DexIgnore
    public final void O() {
        String c2 = c();
        try {
            FLogger.INSTANCE.getLocal().d(a0, "Inside resetDeviceSettingToDefault");
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.resetDeviceSettingToDefault(c2);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.e(str, "Error inside " + a0 + ".resetDeviceSettingToDefault - e=" + e2);
        }
    }

    @DexIgnore
    public final void P() {
        FLogger.INSTANCE.getLocal().d(a0, "unregisterContactObserver");
        try {
            uw6 uw6 = this.j;
            if (uw6 != null) {
                uw6.c();
                ContentResolver contentResolver = getContentResolver();
                uw6 uw62 = this.j;
                if (uw62 != null) {
                    contentResolver.unregisterContentObserver(uw62);
                    ch5 ch5 = this.c;
                    if (ch5 != null) {
                        ch5.n(false);
                    } else {
                        ee7.d("sharedPreferencesManager");
                        throw null;
                    }
                } else {
                    ee7.d("mContactObserver");
                    throw null;
                }
            } else {
                ee7.d("mContactObserver");
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.d(str, "unregisterContactObserver e=" + e2);
        }
    }

    @DexIgnore
    public final void Q() {
        try {
            ri5 ri5 = this.T;
            if (ri5 != null) {
                unregisterReceiver(ri5);
                ui5 ui5 = this.U;
                if (ui5 != null) {
                    unregisterReceiver(ui5);
                } else {
                    ee7.d("mMessageReceiver");
                    throw null;
                }
            } else {
                ee7.d("mPhoneCallReceiver");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.e(str, "unregisterTelephonyReceiver throw exception: " + e2.getMessage());
        }
    }

    @DexIgnore
    public final ik7 R() {
        return xh7.b(zi7.a(qj7.b()), null, null, new v(null), 3, null);
    }

    @DexIgnore
    public final void a(int i2) {
    }

    @DexIgnore
    @Override // com.fossil.ff
    public void attachBaseContext(Context context) {
        ee7.b(context, "base");
        super.attachBaseContext(context);
        ef.d(this);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00d1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.fossil.fb7<? super char[]> r11) {
        /*
            r10 = this;
            boolean r0 = r11 instanceof com.portfolio.platform.PortfolioApp.j
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.portfolio.platform.PortfolioApp$j r0 = (com.portfolio.platform.PortfolioApp.j) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.PortfolioApp$j r0 = new com.portfolio.platform.PortfolioApp$j
            r0.<init>(r10, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = "null cannot be cast to non-null type java.lang.String"
            java.lang.String r4 = "(this as java.lang.String).toCharArray()"
            r5 = 2
            r6 = 0
            r7 = 1
            if (r2 == 0) goto L_0x0052
            if (r2 == r7) goto L_0x0046
            if (r2 != r5) goto L_0x003e
            java.lang.Object r1 = r0.L$2
            com.fossil.fl4$c r1 = (com.fossil.fl4.c) r1
            java.lang.Object r1 = r0.L$1
            char[] r1 = (char[]) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.t87.a(r11)
            goto L_0x00b3
        L_0x003e:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r0)
            throw r11
        L_0x0046:
            java.lang.Object r2 = r0.L$1
            char[] r2 = (char[]) r2
            java.lang.Object r8 = r0.L$0
            com.portfolio.platform.PortfolioApp r8 = (com.portfolio.platform.PortfolioApp) r8
            com.fossil.t87.a(r11)
            goto L_0x0074
        L_0x0052:
            com.fossil.t87.a(r11)
            com.fossil.aw6 r11 = r10.W
            if (r11 == 0) goto L_0x00d7
            com.fossil.aw6$b r2 = new com.fossil.aw6$b
            com.fossil.ya5 r8 = new com.fossil.ya5
            r8.<init>()
            java.lang.String r9 = "Passphrase"
            r2.<init>(r9, r8)
            r0.L$0 = r10
            r0.L$1 = r6
            r0.label = r7
            java.lang.Object r11 = com.fossil.gl4.a(r11, r2, r0)
            if (r11 != r1) goto L_0x0072
            return r1
        L_0x0072:
            r8 = r10
            r2 = r6
        L_0x0074:
            com.fossil.fl4$c r11 = (com.fossil.fl4.c) r11
            boolean r9 = r11 instanceof com.fossil.aw6.d
            if (r9 == 0) goto L_0x0091
            r2 = r11
            com.fossil.aw6$d r2 = (com.fossil.aw6.d) r2
            java.lang.String r2 = r2.a()
            if (r2 == 0) goto L_0x008b
            char[] r2 = r2.toCharArray()
            com.fossil.ee7.a(r2, r4)
            goto L_0x0091
        L_0x008b:
            com.fossil.x87 r11 = new com.fossil.x87
            r11.<init>(r3)
            throw r11
        L_0x0091:
            if (r2 == 0) goto L_0x009a
            int r9 = r2.length
            if (r9 != 0) goto L_0x0097
            goto L_0x0098
        L_0x0097:
            r7 = 0
        L_0x0098:
            if (r7 == 0) goto L_0x00d0
        L_0x009a:
            com.fossil.dw6 r7 = r8.X
            if (r7 == 0) goto L_0x00d1
            com.fossil.dw6$b r6 = new com.fossil.dw6$b
            r6.<init>()
            r0.L$0 = r8
            r0.L$1 = r2
            r0.L$2 = r11
            r0.label = r5
            java.lang.Object r11 = com.fossil.gl4.a(r7, r6, r0)
            if (r11 != r1) goto L_0x00b2
            return r1
        L_0x00b2:
            r1 = r2
        L_0x00b3:
            com.fossil.fl4$c r11 = (com.fossil.fl4.c) r11
            boolean r0 = r11 instanceof com.fossil.dw6.c
            if (r0 == 0) goto L_0x00cf
            com.fossil.dw6$c r11 = (com.fossil.dw6.c) r11
            java.lang.String r11 = r11.a()
            if (r11 == 0) goto L_0x00c9
            char[] r2 = r11.toCharArray()
            com.fossil.ee7.a(r2, r4)
            goto L_0x00d0
        L_0x00c9:
            com.fossil.x87 r11 = new com.fossil.x87
            r11.<init>(r3)
            throw r11
        L_0x00cf:
            r2 = r1
        L_0x00d0:
            return r2
        L_0x00d1:
            java.lang.String r11 = "mGeneratePassphrase"
            com.fossil.ee7.d(r11)
            throw r6
        L_0x00d7:
            java.lang.String r11 = "mDecryptValueKeyStoreUseCase"
            com.fossil.ee7.d(r11)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.e(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final tj4 f() {
        tj4 tj4 = this.L;
        if (tj4 != null) {
            return tj4;
        }
        ee7.d("applicationComponent");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g(com.fossil.fb7<? super java.util.Date> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof com.portfolio.platform.PortfolioApp.k
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.portfolio.platform.PortfolioApp$k r0 = (com.portfolio.platform.PortfolioApp.k) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.PortfolioApp$k r0 = new com.portfolio.platform.PortfolioApp$k
            r0.<init>(r5, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0036
            if (r2 != r4) goto L_0x002e
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.t87.a(r6)
            goto L_0x0048
        L_0x002e:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L_0x0036:
            com.fossil.t87.a(r6)
            com.portfolio.platform.data.source.UserRepository r6 = r5.A
            if (r6 == 0) goto L_0x007d
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r6 = r6.getCurrentUser(r0)
            if (r6 != r1) goto L_0x0048
            return r1
        L_0x0048:
            com.portfolio.platform.data.model.MFUser r6 = (com.portfolio.platform.data.model.MFUser) r6
            if (r6 == 0) goto L_0x006a
            java.lang.String r6 = r6.getCreatedAt()
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 != 0) goto L_0x006a
            if (r6 == 0) goto L_0x0066
            java.util.Date r6 = com.fossil.zd5.d(r6)
            java.util.Date r6 = com.fossil.ye5.a(r6)
            java.lang.String r0 = "TimeHelper.getStartOfDay(registeredDate)"
            com.fossil.ee7.a(r6, r0)
            return r6
        L_0x0066:
            com.fossil.ee7.a()
            throw r3
        L_0x006a:
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r0 = com.portfolio.platform.PortfolioApp.a0
            java.lang.String r1 = "Fail to getCurrentUserRegisteringDate, return new Date(1, 1, 1)"
            r6.d(r0, r1)
            java.util.Date r6 = new java.util.Date
            r6.<init>(r4, r4, r4)
            return r6
        L_0x007d:
            java.lang.String r6 = "mUserRepository"
            com.fossil.ee7.d(r6)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.g(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object h(com.fossil.fb7<? super com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof com.portfolio.platform.PortfolioApp.l
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.portfolio.platform.PortfolioApp$l r0 = (com.portfolio.platform.PortfolioApp.l) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.PortfolioApp$l r0 = new com.portfolio.platform.PortfolioApp$l
            r0.<init>(r5, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.t87.a(r6)
            goto L_0x0054
        L_0x002d:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L_0x0035:
            com.fossil.t87.a(r6)
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.a0
            java.lang.String r4 = "getWorkoutDetectionSetting()"
            r6.d(r2, r4)
            com.portfolio.platform.data.source.WorkoutSettingRepository r6 = r5.D
            if (r6 == 0) goto L_0x005b
            r0.L$0 = r5
            r0.label = r3
            java.lang.Object r6 = r6.getWorkoutSettingList(r0)
            if (r6 != r1) goto L_0x0054
            return r1
        L_0x0054:
            java.util.List r6 = (java.util.List) r6
            com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting r6 = com.fossil.ay6.a(r6)
            return r6
        L_0x005b:
            java.lang.String r6 = "mWorkoutSettingRepository"
            com.fossil.ee7.d(r6)
            r6 = 0
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.h(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object i(fb7<? super i97> fb7) {
        FLogger.INSTANCE.getLocal().d(a0, "initCrashlytics");
        x24.a().a("SDK Version V2", ButtonService.Companion.getSdkVersionV2());
        x24.a().a("Locale", Locale.getDefault().toString());
        Object n2 = n(fb7);
        if (n2 == nb7.a()) {
            return n2;
        }
        return i97.a;
    }

    @DexIgnore
    public final String j() {
        String h2 = h();
        if (ee7.a((Object) h2, (Object) eb5.CITIZEN.getName())) {
            return "citizen";
        }
        return ee7.a(h2, eb5.UNIVERSAL.getName()) ? "universal" : Endpoints.DEFAULT_NAME;
    }

    @DexIgnore
    public final FossilDeviceSerialPatternUtil.BRAND k() {
        String h2 = h();
        if (ee7.a((Object) h2, (Object) eb5.CHAPS.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.CHAPS;
        }
        if (ee7.a((Object) h2, (Object) eb5.DIESEL.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.DIESEL;
        }
        if (ee7.a((Object) h2, (Object) eb5.EA.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.EA;
        }
        if (ee7.a((Object) h2, (Object) eb5.KATESPADE.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.KATE_SPADE;
        }
        if (ee7.a((Object) h2, (Object) eb5.MICHAELKORS.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.MICHAEL_KORS;
        }
        if (ee7.a((Object) h2, (Object) eb5.SKAGEN.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.SKAGEN;
        }
        if (ee7.a((Object) h2, (Object) eb5.AX.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.ARMANI_EXCHANGE;
        }
        if (ee7.a((Object) h2, (Object) eb5.RELIC.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.RELIC;
        }
        if (ee7.a((Object) h2, (Object) eb5.MJ.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.MARC_JACOBS;
        }
        if (ee7.a((Object) h2, (Object) eb5.FOSSIL.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.FOSSIL;
        }
        if (ee7.a((Object) h2, (Object) eb5.UNIVERSAL.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.UNIVERSAL;
        }
        if (ee7.a((Object) h2, (Object) eb5.CITIZEN.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.CITIZEN;
        }
        return FossilDeviceSerialPatternUtil.BRAND.UNKNOWN;
    }

    @DexIgnore
    public final String l() {
        Locale locale = Locale.getDefault();
        ee7.a((Object) locale, "locale");
        String language = locale.getLanguage();
        String country = locale.getCountry();
        if (TextUtils.isEmpty(language)) {
            return "";
        }
        if (ee7.a((Object) language, (Object) "iw")) {
            language = "he";
        }
        if (ee7.a((Object) language, (Object) "in")) {
            language = "id";
        }
        if (ee7.a((Object) language, (Object) "ji")) {
            language = "yi";
        }
        if (!TextUtils.isEmpty(country)) {
            we7 we7 = we7.a;
            Locale locale2 = Locale.US;
            ee7.a((Object) locale2, "Locale.US");
            language = String.format(locale2, "%s-%s", Arrays.copyOf(new Object[]{language, country}, 2));
            ee7.a((Object) language, "java.lang.String.format(locale, format, *args)");
        }
        ee7.a((Object) language, "localeString");
        return language;
    }

    @DexIgnore
    public final String m() {
        Resources resources = getResources();
        ee7.a((Object) resources, "resources");
        String languageTag = e8.a(resources.getConfiguration()).a(0).toLanguageTag();
        ee7.a((Object) languageTag, "ConfigurationCompat.getL\u2026ation)[0].toLanguageTag()");
        return languageTag;
    }

    @DexIgnore
    public final ApiServiceV2 n() {
        ApiServiceV2 apiServiceV2 = this.i;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        ee7.d("mApiService");
        throw null;
    }

    @DexIgnore
    public final zz6 o() {
        zz6 zz6 = this.x;
        if (zz6 != null) {
            return zz6;
        }
        ee7.d("mDaggerAwareWorkerFactory");
        throw null;
    }

    @DexIgnore
    public void onActivityCreated(Activity activity, Bundle bundle) {
        ee7.b(activity, Constants.ACTIVITY);
        b bVar = b.CREATE;
    }

    @DexIgnore
    public void onActivityDestroyed(Activity activity) {
        ee7.b(activity, Constants.ACTIVITY);
        b bVar = b.DESTROY;
    }

    @DexIgnore
    public void onActivityPaused(Activity activity) {
        ee7.b(activity, Constants.ACTIVITY);
        b bVar = b.PAUSE;
        if (d0) {
            sj5 sj5 = this.v;
            if (sj5 != null) {
                sj5.e();
            } else {
                ee7.d("mShakeFeedbackService");
                throw null;
            }
        }
        this.I = true;
        Runnable runnable = this.K;
        if (runnable != null) {
            this.J.removeCallbacks(runnable);
        }
        this.J.postDelayed(new n(this), 500);
    }

    @DexIgnore
    public void onActivityResumed(Activity activity) {
        ee7.b(activity, Constants.ACTIVITY);
        if (d0) {
            sj5 sj5 = this.v;
            if (sj5 != null) {
                sj5.a(activity);
            } else {
                ee7.d("mShakeFeedbackService");
                throw null;
            }
        }
        b bVar = b.RESUME;
        this.I = false;
        boolean z2 = !this.H;
        this.H = true;
        Runnable runnable = this.K;
        if (runnable != null) {
            this.J.removeCallbacks(runnable);
        }
        if (z2) {
            b0 = true;
            FLogger.INSTANCE.getLocal().d(a0, "from background");
            if5 if5 = this.P;
            if (if5 != null) {
                if5.d();
            }
        } else {
            b0 = false;
            FLogger.INSTANCE.getLocal().d(a0, "still foreground");
        }
        if (this.N) {
            a(this.O);
        }
    }

    @DexIgnore
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        ee7.b(activity, Constants.ACTIVITY);
    }

    @DexIgnore
    public void onActivityStarted(Activity activity) {
        ee7.b(activity, Constants.ACTIVITY);
        b bVar = b.START;
    }

    @DexIgnore
    public void onActivityStopped(Activity activity) {
        ee7.b(activity, Constants.ACTIVITY);
        b bVar = b.STOP;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        c0 = this;
        d0 = (getApplicationContext().getApplicationInfo().flags & 2) != 0;
        if (E()) {
            wj4 wj4 = new wj4(this);
            hl4.z L0 = hl4.L0();
            L0.a(wj4);
            tj4 a2 = L0.a();
            ee7.a((Object) a2, "DaggerApplicationCompone\u2026\n                .build()");
            this.L = a2;
            if (a2 != null) {
                a2.a(this);
                vm4 vm4 = this.Y;
                if (vm4 != null) {
                    vm4.a(this);
                    System.loadLibrary("FitnessAlgorithm");
                    LifecycleOwner g2 = be.g();
                    ee7.a((Object) g2, "ProcessLifecycleOwner.get()");
                    Lifecycle lifecycle = g2.getLifecycle();
                    ApplicationEventListener applicationEventListener = this.s;
                    if (applicationEventListener != null) {
                        lifecycle.a(applicationEventListener);
                        MicroAppEventLogger.initialize(this);
                        if (!G()) {
                            Stetho.initializeWithDefaults(this);
                        }
                        ik7 unused = xh7.b(this.Z, null, null, new o(this, null), 3, null);
                        FLogger.INSTANCE.getLocal().d(a0, "On app create");
                        e0 = new qc5(l07.a);
                        MutableLiveData<String> mutableLiveData = this.G;
                        ch5 ch5 = this.c;
                        if (ch5 != null) {
                            mutableLiveData.b(ch5.e());
                            registerActivityLifecycleCallbacks(this);
                            String str = "4.5.0";
                            if (d0) {
                                Object[] array = new ah7(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).split(str, 2).toArray(new String[0]);
                                if (array != null) {
                                    str = ((String[]) array)[0];
                                } else {
                                    throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
                                }
                            }
                            GuestApiService guestApiService = this.g;
                            if (guestApiService != null) {
                                this.a = new jg5(this, str, guestApiService);
                                this.b = new li5();
                                jl4.a();
                                registerReceiver(this.a, new IntentFilter("android.intent.action.LOCALE_CHANGED"));
                                K();
                                jg5 jg5 = this.a;
                                if (jg5 != null) {
                                    registerActivityLifecycleCallbacks(jg5.a());
                                    jg5 jg52 = this.a;
                                    if (jg52 != null) {
                                        String simpleName = SplashScreenActivity.class.getSimpleName();
                                        ee7.a((Object) simpleName, "SplashScreenActivity::class.java.simpleName");
                                        jg52.a(simpleName);
                                        jg5 jg53 = this.a;
                                        if (jg53 != null) {
                                            jg53.i();
                                            ResolutionHelper.INSTANCE.initDeviceDensity(this);
                                            try {
                                                if (Build.VERSION.SDK_INT >= 24) {
                                                    NetworkChangedReceiver networkChangedReceiver = new NetworkChangedReceiver();
                                                    this.R = networkChangedReceiver;
                                                    registerReceiver(networkChangedReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                                                }
                                            } catch (Exception e2) {
                                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                                String str2 = a0;
                                                local.e(str2, "onCreate - jobScheduler - ex=" + e2);
                                            }
                                            IntentFilter intentFilter = new IntentFilter();
                                            intentFilter.addAction("android.intent.action.TIME_TICK");
                                            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
                                            registerReceiver(this.S, intentFilter);
                                            M();
                                            this.M = Thread.getDefaultUncaughtExceptionHandler();
                                            Thread.setDefaultUncaughtExceptionHandler(new p(this));
                                            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                                            StrictMode.setVmPolicy(builder.build());
                                            builder.detectFileUriExposure();
                                            pd5 pd5 = this.f;
                                            if (pd5 != null) {
                                                pd5.c(this);
                                                pd5 pd52 = this.f;
                                                if (pd52 != null) {
                                                    pd52.d(this);
                                                    this.P = qd5.f.b("app_appearance");
                                                    qd5 qd5 = this.r;
                                                    if (qd5 != null) {
                                                        qd5.b("diana_sdk_version", ButtonService.Companion.getSdkVersionV2());
                                                        if (!TextUtils.isEmpty(c())) {
                                                            FLogger.INSTANCE.getLocal().d(a0, "Service Tracking - startForegroundService in PortfolioApp onCreate");
                                                            rx6.a.a(rx6.a, this, MFDeviceService.class, null, 4, null);
                                                            rx6.a.a(rx6.a, this, ButtonService.class, null, 4, null);
                                                        }
                                                        FlutterEngine flutterEngine = new FlutterEngine(this);
                                                        flutterEngine.getDartExecutor().executeDartEntrypoint(new DartExecutor.DartEntrypoint(FlutterMain.findAppBundlePath(), Constants.MAP));
                                                        FlutterEngineCache.getInstance().put("map_engine_id", flutterEngine);
                                                        FlutterEngine flutterEngine2 = new FlutterEngine(this);
                                                        flutterEngine2.getDartExecutor().executeDartEntrypoint(new DartExecutor.DartEntrypoint(FlutterMain.findAppBundlePath(), "screenShotMap"));
                                                        FlutterEngineCache.getInstance().put("screenshot_engine_id", flutterEngine2);
                                                        return;
                                                    }
                                                    ee7.d("mAnalyticsHelper");
                                                    throw null;
                                                }
                                                ee7.d("mAlarmHelper");
                                                throw null;
                                            }
                                            ee7.d("mAlarmHelper");
                                            throw null;
                                        }
                                        ee7.a();
                                        throw null;
                                    }
                                    ee7.a();
                                    throw null;
                                }
                                ee7.a();
                                throw null;
                            }
                            ee7.d("mGuestApiService");
                            throw null;
                        }
                        ee7.d("sharedPreferencesManager");
                        throw null;
                    }
                    ee7.d("mAppEventManager");
                    throw null;
                }
                ee7.d("bcAnalytic");
                throw null;
            }
            ee7.d("applicationComponent");
            throw null;
        }
    }

    @DexIgnore
    public void onLowMemory() {
        super.onLowMemory();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.d(str, "Inside " + a0 + ".onLowMemory");
        System.runFinalization();
    }

    @DexIgnore
    public void onTerminate() {
        super.onTerminate();
        FLogger.INSTANCE.getLocal().d(a0, "---Inside .onTerminate of Application");
        Q();
        unregisterReceiver(this.a);
        P();
        jg5 jg5 = this.a;
        unregisterActivityLifecycleCallbacks(jg5 != null ? jg5.a() : null);
        if (Build.VERSION.SDK_INT >= 24) {
            FLogger.INSTANCE.getLocal().d(a0, "unregister NetworkChangedReceiver");
            unregisterReceiver(this.R);
        }
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.d(str, "Inside " + a0 + ".onTrimMemory");
        super.onTrimMemory(i2);
        System.runFinalization();
    }

    @DexIgnore
    public final jg5 p() {
        return this.a;
    }

    @DexIgnore
    public final String q() {
        try {
            int myPid = Process.myPid();
            Object systemService = getSystemService(Constants.ACTIVITY);
            if (systemService != null) {
                for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : ((ActivityManager) systemService).getRunningAppProcesses()) {
                    if (runningAppProcessInfo.pid == myPid) {
                        return runningAppProcessInfo.processName;
                    }
                }
                return null;
            }
            throw new x87("null cannot be cast to non-null type android.app.ActivityManager");
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d(a0, "exception when get process name");
            return null;
        }
    }

    @DexIgnore
    public final ThemeRepository r() {
        ThemeRepository themeRepository = this.V;
        if (themeRepository != null) {
            return themeRepository;
        }
        ee7.d("mThemeRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository s() {
        UserRepository userRepository = this.A;
        if (userRepository != null) {
            return userRepository;
        }
        ee7.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final WatchLocalizationRepository t() {
        WatchLocalizationRepository watchLocalizationRepository = this.y;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        ee7.d("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public final wk5 u() {
        wk5 wk5 = this.F;
        if (wk5 != null) {
            return wk5;
        }
        ee7.d("mWorkoutGpsManager");
        throw null;
    }

    @DexIgnore
    public final ch5 v() {
        ch5 ch5 = this.c;
        if (ch5 != null) {
            return ch5;
        }
        ee7.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final String w() {
        ch5 ch5 = this.c;
        if (ch5 != null) {
            String A2 = ch5.A();
            return A2 != null ? A2 : "";
        }
        ee7.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final String x() {
        MisfitDeviceProfile a2;
        r60 uiPackageOSVersion;
        if (!(!mh7.a((CharSequence) c())) || (a2 = be5.o.e().a(c())) == null || (uiPackageOSVersion = a2.getUiPackageOSVersion()) == null) {
            return null;
        }
        byte major = uiPackageOSVersion.getMajor();
        byte minor = uiPackageOSVersion.getMinor();
        if (major == 0 && minor == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append((int) major);
        sb.append('.');
        sb.append((int) minor);
        return sb.toString();
    }

    @DexIgnore
    public final WatchFaceRepository y() {
        WatchFaceRepository watchFaceRepository = this.B;
        if (watchFaceRepository != null) {
            return watchFaceRepository;
        }
        ee7.d("watchFaceRepository");
        throw null;
    }

    @DexIgnore
    public final boolean z() {
        Object systemService = getSystemService("connectivity");
        if (systemService != null) {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) systemService).getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
        }
        throw new x87("null cannot be cast to non-null type android.net.ConnectivityManager");
    }

    @DexIgnore
    public final void b(boolean z2) {
        this.H = z2;
    }

    @DexIgnore
    public final void c(boolean z2) {
        this.N = z2;
    }

    @DexIgnore
    public final MutableLiveData<String> d() {
        ch5 ch5 = this.c;
        if (ch5 != null) {
            String e2 = ch5.e();
            if (!ee7.a((Object) e2, (Object) this.G.a())) {
                this.G.a(e2);
            }
            return this.G;
        }
        ee7.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final /* synthetic */ Object f(fb7<? super ReplyMessageMappingGroup> fb7) {
        QuickResponseRepository quickResponseRepository = this.C;
        if (quickResponseRepository != null) {
            List<QuickResponseMessage> allQuickResponse = quickResponseRepository.getAllQuickResponse();
            ArrayList<QuickResponseMessage> arrayList = new ArrayList();
            for (T t2 : allQuickResponse) {
                if (!pb7.a(t2.getResponse().length() == 0).booleanValue()) {
                    arrayList.add(t2);
                }
            }
            ArrayList arrayList2 = new ArrayList(x97.a(arrayList, 10));
            for (QuickResponseMessage quickResponseMessage : arrayList) {
                arrayList2.add(new ReplyMessageMapping(String.valueOf(quickResponseMessage.getId()), quickResponseMessage.getResponse()));
            }
            return new ReplyMessageMappingGroup(ea7.d((Collection) arrayList2), "icMessage.icon");
        }
        ee7.d("mQuickResponseRepository");
        throw null;
    }

    @DexIgnore
    public final void m(String str) {
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, ".removePairedPreferenceSerial - serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.removePairedSerial(str);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "removePairedPreferenceSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object n(com.fossil.fb7<? super com.fossil.i97> r5) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof com.portfolio.platform.PortfolioApp.x
            if (r0 == 0) goto L_0x0013
            r0 = r5
            com.portfolio.platform.PortfolioApp$x r0 = (com.portfolio.platform.PortfolioApp.x) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.PortfolioApp$x r0 = new com.portfolio.platform.PortfolioApp$x
            r0.<init>(r4, r5)
        L_0x0018:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.t87.a(r5)
            goto L_0x0047
        L_0x002d:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r0)
            throw r5
        L_0x0035:
            com.fossil.t87.a(r5)
            com.portfolio.platform.data.source.UserRepository r5 = r4.A
            if (r5 == 0) goto L_0x0085
            r0.L$0 = r4
            r0.label = r3
            java.lang.Object r5 = r5.getCurrentUser(r0)
            if (r5 != r1) goto L_0x0047
            return r1
        L_0x0047:
            com.portfolio.platform.data.model.MFUser r5 = (com.portfolio.platform.data.model.MFUser) r5
            if (r5 == 0) goto L_0x0082
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.PortfolioApp.a0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "updateCrashlyticsUserInformation currentUser="
            r2.append(r3)
            r2.append(r5)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            com.fossil.x24 r0 = com.fossil.x24.a()
            java.lang.String r1 = r5.getUserId()
            r0.a(r1)
            com.fossil.x24 r0 = com.fossil.x24.a()
            java.lang.String r5 = r5.getUserId()
            java.lang.String r1 = "UserId"
            r0.a(r1, r5)
            com.fossil.i97 r5 = com.fossil.i97.a
            return r5
        L_0x0082:
            com.fossil.i97 r5 = com.fossil.i97.a
            return r5
        L_0x0085:
            java.lang.String r5 = "mUserRepository"
            com.fossil.ee7.d(r5)
            r5 = 0
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.n(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void o(String str) {
        String str2;
        int i2;
        if (str == null) {
            i2 = 1024;
            str2 = "";
        } else {
            try {
                i2 = ze5.a(str);
                str2 = str;
            } catch (Exception e2) {
                e2.printStackTrace();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = a0;
                local.e(str3, "Inside " + a0 + ".setAutoSecondTimezone - ex=" + e2);
                return;
            }
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str4 = a0;
        local2.d(str4, "Inside " + a0 + ".setAutoSecondTimezone - offsetMinutes=" + i2 + ", mSecondTimezoneId=" + str2);
        IButtonConnectivity iButtonConnectivity = f0;
        if (iButtonConnectivity != null) {
            if (str == null) {
                str = "";
            }
            iButtonConnectivity.deviceSetAutoSecondTimezone(str);
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void p(String str) {
        MisfitDeviceProfile deviceProfile;
        MisfitDeviceProfile deviceProfile2;
        ee7.b(str, "serial");
        try {
            String m2 = m();
            IButtonConnectivity iButtonConnectivity = f0;
            String locale = (iButtonConnectivity == null || (deviceProfile2 = iButtonConnectivity.getDeviceProfile(this.G.a())) == null) ? null : deviceProfile2.getLocale();
            ch5 ch5 = this.c;
            if (ch5 != null) {
                String f2 = ch5.f(m2);
                IButtonConnectivity iButtonConnectivity2 = f0;
                String localeVersion = (iButtonConnectivity2 == null || (deviceProfile = iButtonConnectivity2.getDeviceProfile(this.G.a())) == null) ? null : deviceProfile.getLocaleVersion();
                ch5 ch52 = this.c;
                if (ch52 != null) {
                    String s2 = ch52.s();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = a0;
                    local.d(str2, "phoneLocale " + m2 + " - watchLocale: " + locale + " - standardLocale: " + f2 + "\nwatchLocaleVersion: " + localeVersion + " - standardLocaleVersion: " + s2);
                    if (!ee7.a((Object) f2, (Object) locale)) {
                        a(m2, str);
                    } else if (!ee7.a((Object) localeVersion, (Object) s2)) {
                        a(m2, str);
                    } else {
                        FLogger.INSTANCE.getLocal().d(a0, "don't need to set to watch");
                    }
                } else {
                    ee7.d("sharedPreferencesManager");
                    throw null;
                }
            } else {
                ee7.d("sharedPreferencesManager");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void r(String str) {
        ee7.b(str, "serial");
        if (!TextUtils.isEmpty(str)) {
            try {
                if (mh7.b(str, c(), true)) {
                    ch5 ch5 = this.c;
                    if (ch5 != null) {
                        ch5.o("");
                        this.G.a("");
                    } else {
                        ee7.d("sharedPreferencesManager");
                        throw null;
                    }
                }
                IButtonConnectivity iButtonConnectivity = f0;
                if (iButtonConnectivity != null) {
                    iButtonConnectivity.deviceUnlink(str);
                    ch5 ch52 = this.c;
                    if (ch52 != null) {
                        ch52.a(str, 0L, false);
                    } else {
                        ee7.d("sharedPreferencesManager");
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = a0;
                local.e(str2, "Inside " + a0 + ".unlinkDevice - serial=" + str + ", ex=" + e2);
            }
        }
    }

    @DexIgnore
    public final long s(String str) {
        ee7.b(str, "serial");
        FLogger.INSTANCE.getLocal().d(a0, "verifySecretKey()");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.verifySecretKeySession(str);
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, ".verifySecretKey(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void b(ServerError serverError) {
        this.O = serverError;
    }

    @DexIgnore
    public final String c() {
        ch5 ch5 = this.c;
        if (ch5 != null) {
            String e2 = ch5.e();
            return e2 != null ? e2 : "";
        }
        ee7.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final void b(String str, boolean z2) {
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "notifyWatchAppDataReady(), serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.notifyWatchAppFilesReady(str, z2);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "notifyWatchAppDataReady() - e=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.fossil.fb7<? super com.fossil.i97> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof com.portfolio.platform.PortfolioApp.e
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.portfolio.platform.PortfolioApp$e r0 = (com.portfolio.platform.PortfolioApp.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.PortfolioApp$e r0 = new com.portfolio.platform.PortfolioApp$e
            r0.<init>(r5, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r1 = r0.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.t87.a(r6)
            goto L_0x0064
        L_0x0031:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L_0x0039:
            com.fossil.t87.a(r6)
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.a0
            java.lang.String r4 = "downloadWatchAppData()"
            r6.d(r2, r4)
            java.lang.String r6 = r5.x()
            if (r6 == 0) goto L_0x0075
            com.portfolio.platform.data.source.WatchAppDataRepository r2 = r5.E
            if (r2 == 0) goto L_0x006e
            java.lang.String r4 = r5.g()
            r0.L$0 = r5
            r0.L$1 = r6
            r0.label = r3
            java.lang.Object r6 = r2.downloadWatchAppData(r4, r6, r0)
            if (r6 != r1) goto L_0x0064
            return r1
        L_0x0064:
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            boolean r6 = r6.booleanValue()
            com.fossil.pb7.a(r6)
            goto L_0x0075
        L_0x006e:
            java.lang.String r6 = "mWatchAppDataRepository"
            com.fossil.ee7.d(r6)
            r6 = 0
            throw r6
        L_0x0075:
            com.fossil.i97 r6 = com.fossil.i97.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.c(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object j(fb7<? super i97> fb7) {
        Object a2 = vh7.a(qj7.b(), new m(this, null), fb7);
        return a2 == nb7.a() ? a2 : i97.a;
    }

    @DexIgnore
    public final Object a(String str, fb7<? super Boolean> fb7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "downloadWatchAppData(), uiVersion = " + str);
        WatchAppDataRepository watchAppDataRepository = this.E;
        if (watchAppDataRepository != null) {
            return watchAppDataRepository.downloadWatchAppData(g(), str, fb7);
        }
        ee7.d("mWatchAppDataRepository");
        throw null;
    }

    @DexIgnore
    public final long j(String str) {
        ee7.b(str, "serial");
        return a(str, 1, 1, true);
    }

    @DexIgnore
    public final void i(String str) {
        ee7.b(str, "newActiveDeviceSerial");
        String c2 = c();
        ch5 ch5 = this.c;
        if (ch5 != null) {
            ch5.o(str);
            this.G.a(str);
            qh5.c.a(this);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "onSwitchActiveDeviceSuccess - current=" + c2 + ", new=" + str);
            return;
        }
        ee7.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0054 A[Catch:{ Exception -> 0x0058 }] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object m(com.fossil.fb7<? super com.fossil.i97> r5) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof com.portfolio.platform.PortfolioApp.w
            if (r0 == 0) goto L_0x0013
            r0 = r5
            com.portfolio.platform.PortfolioApp$w r0 = (com.portfolio.platform.PortfolioApp.w) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.PortfolioApp$w r0 = new com.portfolio.platform.PortfolioApp$w
            r0.<init>(r4, r5)
        L_0x0018:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.t87.a(r5)
            goto L_0x0049
        L_0x002d:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r0)
            throw r5
        L_0x0035:
            com.fossil.t87.a(r5)
            com.fossil.rd5$a r5 = com.fossil.rd5.g
            com.fossil.rd5 r5 = r5.b()
            r0.L$0 = r4
            r0.label = r3
            java.lang.Object r5 = r5.a(r0)
            if (r5 != r1) goto L_0x0049
            return r1
        L_0x0049:
            com.misfit.frameworks.buttonservice.log.model.AppLogInfo r5 = (com.misfit.frameworks.buttonservice.log.model.AppLogInfo) r5
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            r0.updateAppLogInfo(r5)
            com.misfit.frameworks.buttonservice.IButtonConnectivity r0 = com.portfolio.platform.PortfolioApp.f0     // Catch:{ Exception -> 0x0058 }
            if (r0 == 0) goto L_0x0075
            r0.updateAppLogInfo(r5)     // Catch:{ Exception -> 0x0058 }
            goto L_0x0075
        L_0x0058:
            r5 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.PortfolioApp.a0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ".updateAppLogInfo(), error="
            r2.append(r3)
            r2.append(r5)
            java.lang.String r5 = r2.toString()
            r0.e(r1, r5)
        L_0x0075:
            com.fossil.i97 r5 = com.fossil.i97.a
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.m(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(jf5 jf5) {
        if5 if5;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.d(str, "Active view tracer old: " + this.Q + " \n new: " + jf5);
        if (jf5 != null && ee7.a((Object) jf5.a(), (Object) "view_appearance")) {
            jf5 jf52 = this.Q;
            if (!(jf52 == null || jf52 == null || jf52.a(jf5) || (if5 = this.P) == null)) {
                gf5 a2 = qd5.f.a("app_appearance_view_navigate");
                jf5 jf53 = this.Q;
                if (jf53 != null) {
                    String e2 = jf53.e();
                    if (e2 != null) {
                        a2.a("prev_view", e2);
                        String e3 = jf5.e();
                        if (e3 != null) {
                            a2.a("next_view", e3);
                            if5.a(a2);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            this.Q = jf5;
        }
    }

    @DexIgnore
    public final Object b(fb7<? super i97> fb7) {
        Object a2 = vh7.a(qj7.b(), new d(null), fb7);
        return a2 == nb7.a() ? a2 : i97.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x04c3 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x04c4  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0533 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0534  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x05a2  */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x05a9  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x05b0  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x013f  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x05b7  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x05c1  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x05ce A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0171  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x019b  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x01bc  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x01d8  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x01e1  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x01f8  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0257  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x02cc  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0305  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0341  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x03a2  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x03be  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x03c6  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x040e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0411  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x042a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.fossil.fb7<? super com.misfit.frameworks.buttonservice.model.UserProfile> r45) {
        /*
            r44 = this;
            r0 = r44
            r1 = r45
            boolean r2 = r1 instanceof com.portfolio.platform.PortfolioApp.i
            if (r2 == 0) goto L_0x0017
            r2 = r1
            com.portfolio.platform.PortfolioApp$i r2 = (com.portfolio.platform.PortfolioApp.i) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.portfolio.platform.PortfolioApp$i r2 = new com.portfolio.platform.PortfolioApp$i
            r2.<init>(r0, r1)
        L_0x001c:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            java.lang.String r5 = "mSummariesRepository"
            java.lang.String r6 = "Calendar.getInstance()"
            r7 = 1
            switch(r4) {
                case 0: goto L_0x01e1;
                case 1: goto L_0x01d8;
                case 2: goto L_0x01bc;
                case 3: goto L_0x019b;
                case 4: goto L_0x0171;
                case 5: goto L_0x013f;
                case 6: goto L_0x00b0;
                case 7: goto L_0x0034;
                default: goto L_0x002c;
            }
        L_0x002c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0034:
            java.lang.Object r3 = r2.L$12
            com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup r3 = (com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup) r3
            java.lang.Object r4 = r2.L$11
            com.misfit.frameworks.buttonservice.model.UserBiometricData$BiometricWearingPosition r4 = (com.misfit.frameworks.buttonservice.model.UserBiometricData.BiometricWearingPosition) r4
            java.lang.Object r5 = r2.L$10
            com.misfit.frameworks.buttonservice.model.UserBiometricData$BiometricGender r5 = (com.misfit.frameworks.buttonservice.model.UserBiometricData.BiometricGender) r5
            java.lang.Object r6 = r2.L$9
            com.fossil.qe7 r6 = (com.fossil.qe7) r6
            java.lang.Object r7 = r2.L$8
            com.fossil.qe7 r7 = (com.fossil.qe7) r7
            java.lang.Object r8 = r2.L$7
            com.fossil.qe7 r8 = (com.fossil.qe7) r8
            java.lang.Object r9 = r2.L$6
            com.fossil.qe7 r9 = (com.fossil.qe7) r9
            double r10 = r2.D$1
            double r12 = r2.D$0
            int r14 = r2.I$4
            float r15 = r2.F$2
            r45 = r3
            java.lang.Object r3 = r2.L$5
            com.portfolio.platform.data.model.room.sleep.MFSleepDay r3 = (com.portfolio.platform.data.model.room.sleep.MFSleepDay) r3
            java.lang.Object r3 = r2.L$4
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r3 = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) r3
            int r3 = r2.I$3
            r16 = r3
            int r3 = r2.I$2
            r17 = r3
            int r3 = r2.I$1
            r18 = r3
            float r3 = r2.F$1
            r19 = r3
            float r3 = r2.F$0
            r20 = r3
            java.lang.Object r3 = r2.L$3
            java.lang.String[] r3 = (java.lang.String[]) r3
            java.lang.Object r3 = r2.L$2
            java.lang.String r3 = (java.lang.String) r3
            int r3 = r2.I$0
            r21 = r3
            java.lang.Object r3 = r2.L$1
            com.portfolio.platform.data.model.MFUser r3 = (com.portfolio.platform.data.model.MFUser) r3
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.PortfolioApp r2 = (com.portfolio.platform.PortfolioApp) r2
            com.fossil.t87.a(r1)
            r38 = r45
            r45 = r3
            r36 = r14
            r2 = r15
            r0 = r17
            r3 = r21
            r15 = r7
            r14 = r9
            r17 = r16
            r7 = r4
            r4 = r5
            r5 = r19
            r41 = r18
            r18 = r6
            r6 = r20
            r42 = r12
            r13 = r8
            r11 = r10
            r8 = r42
            r10 = r41
            goto L_0x0550
        L_0x00b0:
            java.lang.Object r4 = r2.L$11
            com.misfit.frameworks.buttonservice.model.UserBiometricData$BiometricWearingPosition r4 = (com.misfit.frameworks.buttonservice.model.UserBiometricData.BiometricWearingPosition) r4
            java.lang.Object r5 = r2.L$10
            com.misfit.frameworks.buttonservice.model.UserBiometricData$BiometricGender r5 = (com.misfit.frameworks.buttonservice.model.UserBiometricData.BiometricGender) r5
            java.lang.Object r6 = r2.L$9
            com.fossil.qe7 r6 = (com.fossil.qe7) r6
            java.lang.Object r7 = r2.L$8
            com.fossil.qe7 r7 = (com.fossil.qe7) r7
            java.lang.Object r8 = r2.L$7
            com.fossil.qe7 r8 = (com.fossil.qe7) r8
            java.lang.Object r9 = r2.L$6
            com.fossil.qe7 r9 = (com.fossil.qe7) r9
            double r10 = r2.D$1
            double r12 = r2.D$0
            int r14 = r2.I$4
            float r15 = r2.F$2
            r45 = r4
            java.lang.Object r4 = r2.L$5
            com.portfolio.platform.data.model.room.sleep.MFSleepDay r4 = (com.portfolio.platform.data.model.room.sleep.MFSleepDay) r4
            r16 = r4
            java.lang.Object r4 = r2.L$4
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r4 = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) r4
            r17 = r4
            int r4 = r2.I$3
            r18 = r4
            int r4 = r2.I$2
            r19 = r4
            int r4 = r2.I$1
            r20 = r4
            float r4 = r2.F$1
            r21 = r4
            float r4 = r2.F$0
            r22 = r4
            java.lang.Object r4 = r2.L$3
            java.lang.String[] r4 = (java.lang.String[]) r4
            r23 = r4
            java.lang.Object r4 = r2.L$2
            java.lang.String r4 = (java.lang.String) r4
            r24 = r4
            int r4 = r2.I$0
            r25 = r4
            java.lang.Object r4 = r2.L$1
            com.portfolio.platform.data.model.MFUser r4 = (com.portfolio.platform.data.model.MFUser) r4
            r26 = r4
            java.lang.Object r4 = r2.L$0
            com.portfolio.platform.PortfolioApp r4 = (com.portfolio.platform.PortfolioApp) r4
            com.fossil.t87.a(r1)
            r0 = r1
            r31 = r10
            r33 = r12
            r1 = r15
            r10 = r20
            r12 = r22
            r11 = r23
            r13 = r25
            r15 = r4
            r20 = r7
            r22 = r9
            r7 = r16
            r4 = r19
            r9 = r21
            r16 = r45
            r19 = r6
            r21 = r8
            r8 = r14
            r6 = r17
            r14 = r24
            r17 = r3
            r3 = r26
            r41 = r18
            r18 = r5
            r5 = r41
            goto L_0x04e2
        L_0x013f:
            java.lang.Object r4 = r2.L$5
            com.portfolio.platform.data.model.room.sleep.MFSleepDay r4 = (com.portfolio.platform.data.model.room.sleep.MFSleepDay) r4
            java.lang.Object r5 = r2.L$4
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r5 = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) r5
            int r6 = r2.I$3
            int r7 = r2.I$2
            int r10 = r2.I$1
            float r11 = r2.F$1
            float r12 = r2.F$0
            java.lang.Object r13 = r2.L$3
            java.lang.String[] r13 = (java.lang.String[]) r13
            java.lang.Object r14 = r2.L$2
            java.lang.String r14 = (java.lang.String) r14
            int r15 = r2.I$0
            java.lang.Object r8 = r2.L$1
            com.portfolio.platform.data.model.MFUser r8 = (com.portfolio.platform.data.model.MFUser) r8
            java.lang.Object r9 = r2.L$0
            com.portfolio.platform.PortfolioApp r9 = (com.portfolio.platform.PortfolioApp) r9
            com.fossil.t87.a(r1)
            r0 = r4
            r4 = r5
            r41 = r15
            r15 = r9
            r9 = r11
            r11 = r13
            r13 = r41
            goto L_0x0373
        L_0x0171:
            java.lang.Object r4 = r2.L$4
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r4 = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) r4
            int r5 = r2.I$3
            int r6 = r2.I$2
            int r8 = r2.I$1
            float r9 = r2.F$1
            float r10 = r2.F$0
            java.lang.Object r11 = r2.L$3
            java.lang.String[] r11 = (java.lang.String[]) r11
            java.lang.Object r12 = r2.L$2
            java.lang.String r12 = (java.lang.String) r12
            int r13 = r2.I$0
            java.lang.Object r14 = r2.L$1
            com.portfolio.platform.data.model.MFUser r14 = (com.portfolio.platform.data.model.MFUser) r14
            java.lang.Object r15 = r2.L$0
            com.portfolio.platform.PortfolioApp r15 = (com.portfolio.platform.PortfolioApp) r15
            com.fossil.t87.a(r1)
            r7 = r6
            r0 = r10
            r6 = r5
        L_0x0197:
            r10 = r8
            r8 = r14
            goto L_0x033b
        L_0x019b:
            int r4 = r2.I$3
            int r5 = r2.I$2
            int r8 = r2.I$1
            float r9 = r2.F$1
            float r10 = r2.F$0
            java.lang.Object r11 = r2.L$3
            java.lang.String[] r11 = (java.lang.String[]) r11
            java.lang.Object r12 = r2.L$2
            java.lang.String r12 = (java.lang.String) r12
            int r13 = r2.I$0
            java.lang.Object r14 = r2.L$1
            com.portfolio.platform.data.model.MFUser r14 = (com.portfolio.platform.data.model.MFUser) r14
            java.lang.Object r15 = r2.L$0
            com.portfolio.platform.PortfolioApp r15 = (com.portfolio.platform.PortfolioApp) r15
            com.fossil.t87.a(r1)
            goto L_0x02fe
        L_0x01bc:
            float r4 = r2.F$1
            float r8 = r2.F$0
            java.lang.Object r9 = r2.L$3
            java.lang.String[] r9 = (java.lang.String[]) r9
            java.lang.Object r10 = r2.L$2
            java.lang.String r10 = (java.lang.String) r10
            int r11 = r2.I$0
            java.lang.Object r12 = r2.L$1
            com.portfolio.platform.data.model.MFUser r12 = (com.portfolio.platform.data.model.MFUser) r12
            java.lang.Object r13 = r2.L$0
            com.portfolio.platform.PortfolioApp r13 = (com.portfolio.platform.PortfolioApp) r13
            com.fossil.t87.a(r1)
            r14 = r12
            goto L_0x02ba
        L_0x01d8:
            java.lang.Object r4 = r2.L$0
            com.portfolio.platform.PortfolioApp r4 = (com.portfolio.platform.PortfolioApp) r4
            com.fossil.t87.a(r1)
            r13 = r4
            goto L_0x01f4
        L_0x01e1:
            com.fossil.t87.a(r1)
            com.portfolio.platform.data.source.UserRepository r1 = r0.A
            if (r1 == 0) goto L_0x05d0
            r2.L$0 = r0
            r2.label = r7
            java.lang.Object r1 = r1.getCurrentUser(r2)
            if (r1 != r3) goto L_0x01f3
            return r3
        L_0x01f3:
            r13 = r0
        L_0x01f4:
            com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
            if (r1 == 0) goto L_0x05ce
            java.lang.String r4 = r1.getBirthday()
            boolean r8 = com.fossil.mh7.a(r4)
            if (r8 == 0) goto L_0x0204
            java.lang.String r4 = "1970-01-01"
        L_0x0204:
            r10 = r4
            if (r10 == 0) goto L_0x05c9
            com.fossil.ah7 r4 = new com.fossil.ah7
            java.lang.String r8 = "-"
            r4.<init>(r8)
            r8 = 0
            java.util.List r4 = r4.split(r10, r8)
            boolean r8 = r4.isEmpty()
            if (r8 != 0) goto L_0x024a
            int r8 = r4.size()
            java.util.ListIterator r8 = r4.listIterator(r8)
        L_0x0221:
            boolean r9 = r8.hasPrevious()
            if (r9 == 0) goto L_0x024a
            java.lang.Object r9 = r8.previous()
            java.lang.String r9 = (java.lang.String) r9
            int r9 = r9.length()
            if (r9 != 0) goto L_0x0235
            r9 = 1
            goto L_0x0236
        L_0x0235:
            r9 = 0
        L_0x0236:
            java.lang.Boolean r9 = com.fossil.pb7.a(r9)
            boolean r9 = r9.booleanValue()
            if (r9 != 0) goto L_0x0221
            int r8 = r8.nextIndex()
            int r8 = r8 + r7
            java.util.List r4 = com.fossil.ea7.d(r4, r8)
            goto L_0x024e
        L_0x024a:
            java.util.List r4 = com.fossil.w97.a()
        L_0x024e:
            r8 = 0
            java.lang.String[] r9 = new java.lang.String[r8]
            java.lang.Object[] r4 = r4.toArray(r9)
            if (r4 == 0) goto L_0x05c1
            r9 = r4
            java.lang.String[] r9 = (java.lang.String[]) r9
            java.util.Calendar r4 = java.util.Calendar.getInstance()
            int r4 = r4.get(r7)
            r11 = r9[r8]
            java.lang.Integer r8 = java.lang.Integer.valueOf(r11)
            java.lang.String r11 = "Integer.valueOf(s[0])"
            com.fossil.ee7.a(r8, r11)
            int r8 = r8.intValue()
            int r4 = r4 - r8
            int r8 = r1.getWeightInGrams()
            if (r8 <= 0) goto L_0x027d
            int r8 = r1.getWeightInGrams()
            goto L_0x0280
        L_0x027d:
            r8 = 68039(0x109c7, float:9.5343E-41)
        L_0x0280:
            float r8 = (float) r8
            float r8 = com.fossil.xd5.e(r8)
            int r11 = r1.getHeightInCentimeters()
            if (r11 <= 0) goto L_0x0290
            int r11 = r1.getHeightInCentimeters()
            goto L_0x0292
        L_0x0290:
            r11 = 170(0xaa, float:2.38E-43)
        L_0x0292:
            float r11 = (float) r11
            float r11 = com.fossil.xd5.d(r11)
            com.portfolio.platform.data.source.SummariesRepository r12 = r13.d
            if (r12 == 0) goto L_0x05bc
            r2.L$0 = r13
            r2.L$1 = r1
            r2.I$0 = r4
            r2.L$2 = r10
            r2.L$3 = r9
            r2.F$0 = r8
            r2.F$1 = r11
            r14 = 2
            r2.label = r14
            java.lang.Object r12 = r12.getCurrentActivitySettings(r2)
            if (r12 != r3) goto L_0x02b3
            return r3
        L_0x02b3:
            r14 = r1
            r1 = r12
            r41 = r11
            r11 = r4
            r4 = r41
        L_0x02ba:
            com.portfolio.platform.data.model.room.fitness.ActivitySettings r1 = (com.portfolio.platform.data.model.room.fitness.ActivitySettings) r1
            int r12 = r1.component1()
            int r15 = r1.component2()
            int r1 = r1.component3()
            com.portfolio.platform.data.source.SummariesRepository r7 = r13.d
            if (r7 == 0) goto L_0x05b7
            java.util.Calendar r5 = java.util.Calendar.getInstance()
            com.fossil.ee7.a(r5, r6)
            r2.L$0 = r13
            r2.L$1 = r14
            r2.I$0 = r11
            r2.L$2 = r10
            r2.L$3 = r9
            r2.F$0 = r8
            r2.F$1 = r4
            r2.I$1 = r12
            r2.I$2 = r15
            r2.I$3 = r1
            r0 = 3
            r2.label = r0
            java.lang.Object r0 = r7.getSummary(r5, r2)
            if (r0 != r3) goto L_0x02f1
            return r3
        L_0x02f1:
            r5 = r15
            r15 = r13
            r13 = r11
            r11 = r9
            r9 = r4
            r4 = r1
            r1 = r0
            r41 = r10
            r10 = r8
            r8 = r12
            r12 = r41
        L_0x02fe:
            r0 = r1
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r0 = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) r0
            com.portfolio.platform.data.source.SleepSummariesRepository r1 = r15.e
            if (r1 == 0) goto L_0x05b0
            java.util.Calendar r7 = java.util.Calendar.getInstance()
            com.fossil.ee7.a(r7, r6)
            java.util.Date r6 = r7.getTime()
            java.lang.String r7 = "Calendar.getInstance().time"
            com.fossil.ee7.a(r6, r7)
            r2.L$0 = r15
            r2.L$1 = r14
            r2.I$0 = r13
            r2.L$2 = r12
            r2.L$3 = r11
            r2.F$0 = r10
            r2.F$1 = r9
            r2.I$1 = r8
            r2.I$2 = r5
            r2.I$3 = r4
            r2.L$4 = r0
            r7 = 4
            r2.label = r7
            java.lang.Object r1 = r1.getSleepSummaryFromDb(r6, r2)
            if (r1 != r3) goto L_0x0335
            return r3
        L_0x0335:
            r6 = r4
            r7 = r5
            r4 = r0
            r0 = r10
            goto L_0x0197
        L_0x033b:
            com.portfolio.platform.data.model.room.sleep.MFSleepDay r1 = (com.portfolio.platform.data.model.room.sleep.MFSleepDay) r1
            com.fossil.ge5 r5 = r15.u
            if (r5 == 0) goto L_0x05a9
            java.util.Date r14 = new java.util.Date
            r14.<init>()
            r2.L$0 = r15
            r2.L$1 = r8
            r2.I$0 = r13
            r2.L$2 = r12
            r2.L$3 = r11
            r2.F$0 = r0
            r2.F$1 = r9
            r2.I$1 = r10
            r2.I$2 = r7
            r2.I$3 = r6
            r2.L$4 = r4
            r2.L$5 = r1
            r18 = r0
            r0 = 5
            r2.label = r0
            r0 = 1
            java.lang.Object r0 = r5.a(r14, r0, r2)
            if (r0 != r3) goto L_0x036b
            return r3
        L_0x036b:
            r14 = r12
            r12 = r18
            r41 = r1
            r1 = r0
            r0 = r41
        L_0x0373:
            java.lang.Number r1 = (java.lang.Number) r1
            float r1 = r1.floatValue()
            com.fossil.qe7 r5 = new com.fossil.qe7
            r5.<init>()
            r17 = r3
            r3 = 0
            r5.element = r3
            r16 = r1
            com.fossil.qe7 r1 = new com.fossil.qe7
            r1.<init>()
            r1.element = r3
            r18 = r6
            com.fossil.qe7 r6 = new com.fossil.qe7
            r6.<init>()
            r6.element = r3
            r19 = r7
            com.fossil.qe7 r7 = new com.fossil.qe7
            r7.<init>()
            r7.element = r3
            r20 = 0
            if (r4 == 0) goto L_0x03be
            int r3 = r4.getActiveTime()
            double r20 = r4.getCalories()
            double r22 = r4.getDistance()
            r24 = r3
            r3 = 100
            r25 = r4
            double r3 = (double) r3
            double r3 = r3 * r22
            r29 = r3
            r27 = r20
            r3 = r24
            goto L_0x03c4
        L_0x03be:
            r25 = r4
            r27 = r20
            r29 = r27
        L_0x03c4:
            if (r0 == 0) goto L_0x03f8
            int r4 = r5.element
            int r20 = r0.getSleepMinutes()
            int r4 = r4 + r20
            r5.element = r4
            com.portfolio.platform.data.model.room.sleep.SleepDistribution r4 = r0.getSleepStateDistInMinute()
            if (r4 == 0) goto L_0x03f8
            int r20 = r4.component1()
            int r21 = r4.component2()
            int r4 = r4.component3()
            r22 = r5
            int r5 = r1.element
            int r5 = r5 + r20
            r1.element = r5
            int r5 = r6.element
            int r5 = r5 + r21
            r6.element = r5
            int r5 = r7.element
            int r5 = r5 + r4
            r7.element = r5
            com.fossil.i97 r4 = com.fossil.i97.a
            goto L_0x03fa
        L_0x03f8:
            r22 = r5
        L_0x03fa:
            com.misfit.frameworks.buttonservice.model.UserBiometricData$BiometricGender r4 = com.misfit.frameworks.buttonservice.model.UserBiometricData.BiometricGender.UNSPECIFIED
            java.lang.String r5 = r8.getGender()
            com.fossil.fb5 r20 = com.fossil.fb5.MALE
            r21 = r4
            java.lang.String r4 = r20.getValue()
            boolean r4 = com.fossil.ee7.a(r5, r4)
            if (r4 == 0) goto L_0x0411
            com.misfit.frameworks.buttonservice.model.UserBiometricData$BiometricGender r4 = com.misfit.frameworks.buttonservice.model.UserBiometricData.BiometricGender.MALE
            goto L_0x0426
        L_0x0411:
            java.lang.String r4 = r8.getGender()
            com.fossil.fb5 r5 = com.fossil.fb5.FEMALE
            java.lang.String r5 = r5.getValue()
            boolean r4 = com.fossil.ee7.a(r4, r5)
            if (r4 == 0) goto L_0x0424
            com.misfit.frameworks.buttonservice.model.UserBiometricData$BiometricGender r4 = com.misfit.frameworks.buttonservice.model.UserBiometricData.BiometricGender.FEMALE
            goto L_0x0426
        L_0x0424:
            r4 = r21
        L_0x0426:
            com.fossil.ch5 r5 = r15.c
            if (r5 == 0) goto L_0x05a2
            java.lang.String r5 = r5.n()
            r20 = r4
            if (r5 != 0) goto L_0x0435
            r21 = r7
            goto L_0x046c
        L_0x0435:
            int r4 = r5.hashCode()
            r21 = r7
            r7 = -1867896629(0xffffffff90aa28cb, float:-6.711603E-29)
            if (r4 == r7) goto L_0x0461
            r7 = 647131558(0x269271a6, float:1.016159E-15)
            if (r4 == r7) goto L_0x0456
            r7 = 660843126(0x2763aa76, float:3.1594985E-15)
            if (r4 == r7) goto L_0x044b
            goto L_0x046c
        L_0x044b:
            java.lang.String r4 = "left wrist"
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x046c
            com.misfit.frameworks.buttonservice.model.UserBiometricData$BiometricWearingPosition r4 = com.misfit.frameworks.buttonservice.model.UserBiometricData.BiometricWearingPosition.LEFT_WRIST
            goto L_0x046e
        L_0x0456:
            java.lang.String r4 = "unspecified wrist"
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x046c
            com.misfit.frameworks.buttonservice.model.UserBiometricData$BiometricWearingPosition r4 = com.misfit.frameworks.buttonservice.model.UserBiometricData.BiometricWearingPosition.UNSPECIFIED_WRIST
            goto L_0x046e
        L_0x0461:
            java.lang.String r4 = "right wrist"
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x046c
            com.misfit.frameworks.buttonservice.model.UserBiometricData$BiometricWearingPosition r4 = com.misfit.frameworks.buttonservice.model.UserBiometricData.BiometricWearingPosition.RIGHT_WRIST
            goto L_0x046e
        L_0x046c:
            com.misfit.frameworks.buttonservice.model.UserBiometricData$BiometricWearingPosition r4 = com.misfit.frameworks.buttonservice.model.UserBiometricData.BiometricWearingPosition.UNSPECIFIED
        L_0x046e:
            r2.L$0 = r15
            r2.L$1 = r8
            r2.I$0 = r13
            r2.L$2 = r14
            r2.L$3 = r11
            r2.F$0 = r12
            r2.F$1 = r9
            r2.I$1 = r10
            r7 = r19
            r2.I$2 = r7
            r5 = r18
            r2.I$3 = r5
            r5 = r25
            r2.L$4 = r5
            r2.L$5 = r0
            r19 = r0
            r0 = r16
            r2.F$2 = r0
            r2.I$4 = r3
            r23 = r7
            r16 = r8
            r7 = r27
            r2.D$0 = r7
            r24 = r7
            r7 = r29
            r2.D$1 = r7
            r26 = r0
            r0 = r22
            r2.L$6 = r0
            r2.L$7 = r1
            r2.L$8 = r6
            r0 = r21
            r2.L$9 = r0
            r0 = r20
            r2.L$10 = r0
            r2.L$11 = r4
            r0 = 6
            r2.label = r0
            java.lang.Object r0 = r15.f(r2)
            r27 = r1
            r1 = r17
            if (r0 != r1) goto L_0x04c4
            return r1
        L_0x04c4:
            r17 = r1
            r31 = r7
            r7 = r19
            r19 = r21
            r33 = r24
            r1 = r26
            r21 = r27
            r8 = r3
            r3 = r16
            r16 = r4
            r4 = r23
            r41 = r6
            r6 = r5
            r5 = r18
            r18 = r20
            r20 = r41
        L_0x04e2:
            com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup r0 = (com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup) r0
            r2.L$0 = r15
            r2.L$1 = r3
            r2.I$0 = r13
            r2.L$2 = r14
            r2.L$3 = r11
            r2.F$0 = r12
            r2.F$1 = r9
            r2.I$1 = r10
            r2.I$2 = r4
            r2.I$3 = r5
            r2.L$4 = r6
            r2.L$5 = r7
            r2.F$2 = r1
            r2.I$4 = r8
            r6 = r33
            r2.D$0 = r6
            r11 = r3
            r14 = r4
            r3 = r31
            r2.D$1 = r3
            r23 = r1
            r1 = r22
            r2.L$6 = r1
            r1 = r21
            r2.L$7 = r1
            r1 = r20
            r2.L$8 = r1
            r1 = r19
            r2.L$9 = r1
            r1 = r18
            r2.L$10 = r1
            r1 = r16
            r2.L$11 = r1
            r2.L$12 = r0
            r45 = r0
            r0 = 7
            r2.label = r0
            java.lang.Object r0 = r15.h(r2)
            r2 = r17
            if (r0 != r2) goto L_0x0534
            return r2
        L_0x0534:
            r38 = r45
            r17 = r5
            r36 = r8
            r5 = r9
            r45 = r11
            r15 = r20
            r2 = r23
            r8 = r6
            r6 = r12
            r7 = r1
            r11 = r3
            r3 = r13
            r4 = r18
            r18 = r19
            r13 = r21
            r1 = r0
            r0 = r14
            r14 = r22
        L_0x0550:
            r37 = r1
            com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting r37 = (com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting) r37
            com.misfit.frameworks.buttonservice.model.UserProfile r1 = new com.misfit.frameworks.buttonservice.model.UserProfile
            r39 = r11
            r11 = r1
            com.misfit.frameworks.buttonservice.model.UserBiometricData r16 = new com.misfit.frameworks.buttonservice.model.UserBiometricData
            r12 = r16
            r19 = r15
            r15 = r2
            r2 = r16
            r2.<init>(r3, r4, r5, r6, r7)
            long r2 = (long) r10
            r5 = r13
            r4 = r14
            r13 = r2
            long r2 = (long) r15
            r7 = r19
            r15 = r2
            long r2 = (long) r0
            r19 = r2
            long r2 = (long) r8
            r21 = r2
            r2 = r39
            long r2 = (long) r2
            r23 = r2
            com.misfit.frameworks.buttonservice.model.UserDisplayUnit r25 = com.fossil.vc5.a(r45)
            int r0 = r4.element
            r26 = r0
            int r0 = r5.element
            r27 = r0
            int r0 = r7.element
            r28 = r0
            r6 = r18
            int r0 = r6.element
            r29 = r0
            r30 = 0
            r31 = 0
            r32 = -1
            r33 = -1
            long r34 = java.lang.System.currentTimeMillis()
            r18 = r36
            r36 = r38
            r11.<init>(r12, r13, r15, r17, r18, r19, r21, r23, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r36, r37)
            return r1
        L_0x05a2:
            java.lang.String r0 = "sharedPreferencesManager"
            com.fossil.ee7.d(r0)
            r0 = 0
            throw r0
        L_0x05a9:
            r0 = 0
            java.lang.String r1 = "mFitnessHelper"
            com.fossil.ee7.d(r1)
            throw r0
        L_0x05b0:
            r0 = 0
            java.lang.String r1 = "mSleepSummariesRepository"
            com.fossil.ee7.d(r1)
            throw r0
        L_0x05b7:
            r0 = 0
            com.fossil.ee7.d(r5)
            throw r0
        L_0x05bc:
            r0 = 0
            com.fossil.ee7.d(r5)
            throw r0
        L_0x05c1:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Array<T>"
            r0.<init>(r1)
            throw r0
        L_0x05c9:
            r0 = 0
            com.fossil.ee7.a()
            throw r0
        L_0x05ce:
            r0 = 0
            return r0
        L_0x05d0:
            r0 = 0
            java.lang.String r1 = "mUserRepository"
            com.fossil.ee7.d(r1)
            throw r0
            switch-data {0->0x01e1, 1->0x01d8, 2->0x01bc, 3->0x019b, 4->0x0171, 5->0x013f, 6->0x00b0, 7->0x0034, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.d(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void b(String str, NotificationBaseObj notificationBaseObj) {
        ee7.b(str, "serial");
        ee7.b(notificationBaseObj, "notification");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "sendNotificationToDevice packageName " + notificationBaseObj);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSendNotification(str, notificationBaseObj);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".sendDianaNotification() - e=" + e2);
        }
    }

    @DexIgnore
    public final String h() {
        String name = eb5.fromInt(Integer.parseInt(ol4.A.d())).getName();
        ee7.a((Object) name, "FossilBrand.fromInt(Inte\u2026onfig.brandId)).getName()");
        return name;
    }

    @DexIgnore
    public final void q(String str) {
        ee7.b(str, ButtonService.USER_ID);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.updateUserId(str);
            }
        } catch (Exception unused) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "exception when set userId to SDK " + str);
        }
    }

    @DexIgnore
    public final void h(String str) {
        ee7.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, ".onPing(), serial=" + str);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.onPing(str);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".onPing() - e=" + e2);
        }
    }

    @DexIgnore
    public final String b() {
        String str;
        String c2 = c();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "generateDeviceNotificationContent activeSerial=" + c2);
        if (TextUtils.isEmpty(c2)) {
            PortfolioApp portfolioApp = c0;
            if (portfolioApp != null) {
                String a2 = ig5.a(portfolioApp, 2131887005);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026Dashboard_CTA__PairWatch)");
                return a2;
            }
            ee7.d("instance");
            throw null;
        }
        String a3 = ig5.a(this, 2131887120);
        DeviceRepository deviceRepository = this.t;
        if (deviceRepository != null) {
            String deviceNameBySerial = deviceRepository.getDeviceNameBySerial(c2);
            try {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = a0;
                StringBuilder sb = new StringBuilder();
                sb.append("generateDeviceNotificationContent gattState=");
                IButtonConnectivity iButtonConnectivity = f0;
                if (iButtonConnectivity != null) {
                    sb.append(iButtonConnectivity.getGattState(c2));
                    local2.d(str3, sb.toString());
                    IButtonConnectivity iButtonConnectivity2 = f0;
                    if (iButtonConnectivity2 != null) {
                        if (iButtonConnectivity2.getGattState(c2) == 2) {
                            PortfolioApp portfolioApp2 = c0;
                            if (portfolioApp2 == null) {
                                ee7.d("instance");
                                throw null;
                            } else if (portfolioApp2.g(c2)) {
                                PortfolioApp portfolioApp3 = c0;
                                if (portfolioApp3 != null) {
                                    a3 = ig5.a(portfolioApp3, 2131886733);
                                } else {
                                    ee7.d("instance");
                                    throw null;
                                }
                            } else {
                                ch5 ch5 = this.c;
                                if (ch5 != null) {
                                    long e2 = ch5.e(c2);
                                    if (e2 == 0) {
                                        str = "";
                                    } else if (System.currentTimeMillis() - e2 < 60000) {
                                        we7 we7 = we7.a;
                                        PortfolioApp portfolioApp4 = c0;
                                        if (portfolioApp4 != null) {
                                            String a4 = ig5.a(portfolioApp4, 2131887136);
                                            ee7.a((Object) a4, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                                            str = String.format(a4, Arrays.copyOf(new Object[]{1}, 1));
                                            ee7.a((Object) str, "java.lang.String.format(format, *args)");
                                        } else {
                                            ee7.d("instance");
                                            throw null;
                                        }
                                    } else {
                                        str = zd5.a(e2);
                                    }
                                    we7 we72 = we7.a;
                                    PortfolioApp portfolioApp5 = c0;
                                    if (portfolioApp5 != null) {
                                        String a5 = ig5.a(portfolioApp5, 2131887126);
                                        ee7.a((Object) a5, "LanguageHelper.getString\u2026_Text__LastSyncedDayTime)");
                                        String format = String.format(a5, Arrays.copyOf(new Object[]{str}, 1));
                                        ee7.a((Object) format, "java.lang.String.format(format, *args)");
                                        a3 = format;
                                    } else {
                                        ee7.d("instance");
                                        throw null;
                                    }
                                } else {
                                    ee7.d("sharedPreferencesManager");
                                    throw null;
                                }
                            }
                        }
                        return deviceNameBySerial + " : " + a3;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            } catch (Exception e3) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str4 = a0;
                local3.d(str4, "generateDeviceNotificationContent e=" + e3);
            }
        } else {
            ee7.d("mDeviceRepository");
            throw null;
        }
    }

    @DexIgnore
    public final void c(String str, String str2) {
        ee7.b(str, "serial");
        if (TextUtils.isEmpty(str) || be5.o.e(str)) {
            String c2 = c();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local.d(str3, "Inside " + a0 + ".setActiveDeviceSerial - current=" + c2 + ", new=" + str + ", newDevice mac address=" + str2);
            if (str2 == null) {
                str2 = "";
            }
            ch5 ch5 = this.c;
            if (ch5 != null) {
                ch5.o(str);
                if (c2 != str) {
                    this.G.a(str);
                }
                R();
                try {
                    IButtonConnectivity iButtonConnectivity = f0;
                    if (iButtonConnectivity != null) {
                        iButtonConnectivity.setActiveSerial(str, str2);
                        qh5.c.a(this);
                        return;
                    }
                    ee7.a();
                    throw null;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                ee7.d("sharedPreferencesManager");
                throw null;
            }
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.d(str4, "Ignore legacy device serial=" + str);
        }
    }

    @DexIgnore
    public final int f(String str) {
        ee7.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.getGattState(str);
            }
            return 0;
        } catch (Exception unused) {
            return 0;
        }
    }

    @DexIgnore
    public final Object l(fb7<? super i97> fb7) {
        Object a2 = vh7.a(qj7.b(), new s(this, null), fb7);
        return a2 == nb7.a() ? a2 : i97.a;
    }

    @DexIgnore
    public final long n(String str) {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        if (str == null) {
            FLogger.INSTANCE.getLocal().e(a0, "sendingEncryptedDataSession - encryptedData is null");
            ch5 ch5 = this.c;
            if (ch5 != null) {
                ch5.a((Boolean) true);
                return time_stamp_for_non_executable_method;
            }
            ee7.d("sharedPreferencesManager");
            throw null;
        }
        try {
            byte[] decode = Base64.decode(str, 0);
            ch5 ch52 = this.c;
            if (ch52 != null) {
                Boolean a2 = ch52.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = a0;
                local.e(str2, "sendingEncryptedDataSession - encryptedData: " + str + " - latestBCStatus: " + a2);
                IButtonConnectivity iButtonConnectivity = f0;
                if (iButtonConnectivity != null) {
                    String c2 = c();
                    ee7.a((Object) a2, "latestBCStatus");
                    return iButtonConnectivity.sendingEncryptedDataSession(decode, c2, a2.booleanValue());
                }
                ee7.a();
                throw null;
            }
            ee7.d("sharedPreferencesManager");
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".sendingEncryptedDataSession - e=" + e2);
            e2.printStackTrace();
            ch5 ch53 = this.c;
            if (ch53 != null) {
                ch53.a((Boolean) true);
                return time_stamp_for_non_executable_method;
            }
            ee7.d("sharedPreferencesManager");
            throw null;
        }
    }

    @DexIgnore
    public final nh5 i() {
        nh5 nh5 = this.z;
        if (nh5 != null) {
            return nh5;
        }
        ee7.d("mDataValidationManager");
        throw null;
    }

    @DexIgnore
    public final void l(String str) {
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, ".removeActivePreferenceSerial - serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.removeActiveSerial(str);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "removeActivePreferenceSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String g() {
        /*
            r6 = this;
            boolean r0 = com.portfolio.platform.PortfolioApp.d0
            java.lang.String r1 = "4.5.0"
            if (r0 == 0) goto L_0x0064
            com.fossil.ah7 r0 = new com.fossil.ah7
            java.lang.String r2 = "-"
            r0.<init>(r2)
            r2 = 0
            java.util.List r0 = r0.split(r1, r2)
            boolean r3 = r0.isEmpty()
            r4 = 1
            if (r3 != 0) goto L_0x0042
            int r3 = r0.size()
            java.util.ListIterator r3 = r0.listIterator(r3)
        L_0x0021:
            boolean r5 = r3.hasPrevious()
            if (r5 == 0) goto L_0x0042
            java.lang.Object r5 = r3.previous()
            java.lang.String r5 = (java.lang.String) r5
            int r5 = r5.length()
            if (r5 != 0) goto L_0x0035
            r5 = 1
            goto L_0x0036
        L_0x0035:
            r5 = 0
        L_0x0036:
            if (r5 != 0) goto L_0x0021
            int r3 = r3.nextIndex()
            int r3 = r3 + r4
            java.util.List r0 = com.fossil.ea7.d(r0, r3)
            goto L_0x0046
        L_0x0042:
            java.util.List r0 = com.fossil.w97.a()
        L_0x0046:
            java.lang.String[] r3 = new java.lang.String[r2]
            java.lang.Object[] r0 = r0.toArray(r3)
            if (r0 == 0) goto L_0x005c
            java.lang.String[] r0 = (java.lang.String[]) r0
            int r3 = r0.length
            if (r3 != 0) goto L_0x0055
            r3 = 1
            goto L_0x0056
        L_0x0055:
            r3 = 0
        L_0x0056:
            r3 = r3 ^ r4
            if (r3 == 0) goto L_0x0064
            r1 = r0[r2]
            goto L_0x0064
        L_0x005c:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Array<T>"
            r0.<init>(r1)
            throw r0
        L_0x0064:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.g():java.lang.String");
    }

    @DexIgnore
    public final int e(String str) {
        ee7.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.getCommunicatorModeBySerial(str);
            }
            ee7.a();
            throw null;
        } catch (Exception unused) {
            return -1;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object k(com.fossil.fb7<? super com.fossil.i97> r8) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof com.portfolio.platform.PortfolioApp.r
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.portfolio.platform.PortfolioApp$r r0 = (com.portfolio.platform.PortfolioApp.r) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.PortfolioApp$r r0 = new com.portfolio.platform.PortfolioApp$r
            r0.<init>(r7, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0036
            if (r2 != r4) goto L_0x002e
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.t87.a(r8)
            goto L_0x0049
        L_0x002e:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r0)
            throw r8
        L_0x0036:
            com.fossil.t87.a(r8)
            com.portfolio.platform.data.source.UserRepository r8 = r7.A
            if (r8 == 0) goto L_0x00cc
            r0.L$0 = r7
            r0.label = r4
            java.lang.Object r8 = r8.getCurrentUser(r0)
            if (r8 != r1) goto L_0x0048
            return r1
        L_0x0048:
            r0 = r7
        L_0x0049:
            com.portfolio.platform.data.model.MFUser r8 = (com.portfolio.platform.data.model.MFUser) r8
            if (r8 == 0) goto L_0x00c9
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.a0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "registerContactObserver currentUser="
            r5.append(r6)
            r5.append(r8)
            java.lang.String r8 = r5.toString()
            r1.d(r2, r8)
            com.fossil.px6$a r8 = com.fossil.px6.a
            boolean r8 = r8.e(r0)
            if (r8 == 0) goto L_0x00aa
            com.fossil.uw6 r8 = r0.j
            java.lang.String r1 = "mContactObserver"
            if (r8 == 0) goto L_0x00a6
            r8.b()
            android.content.ContentResolver r8 = r0.getContentResolver()
            android.net.Uri r2 = android.provider.ContactsContract.Contacts.CONTENT_URI
            com.fossil.uw6 r5 = r0.j
            if (r5 == 0) goto L_0x00a2
            r8.registerContentObserver(r2, r4, r5)
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r1 = com.portfolio.platform.PortfolioApp.a0
            java.lang.String r2 = "registerContactObserver success"
            r8.d(r1, r2)
            com.fossil.ch5 r8 = r0.c
            if (r8 == 0) goto L_0x009c
            r8.n(r4)
            goto L_0x00c6
        L_0x009c:
            java.lang.String r8 = "sharedPreferencesManager"
            com.fossil.ee7.d(r8)
            throw r3
        L_0x00a2:
            com.fossil.ee7.d(r1)
            throw r3
        L_0x00a6:
            com.fossil.ee7.d(r1)
            throw r3
        L_0x00aa:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.PortfolioApp.a0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "registerContactObserver fail due to enough Permission ="
            r2.append(r3)
            r2.append(r8)
            java.lang.String r8 = r2.toString()
            r0.d(r1, r8)
        L_0x00c6:
            com.fossil.i97 r8 = com.fossil.i97.a
            return r8
        L_0x00c9:
            com.fossil.i97 r8 = com.fossil.i97.a
            return r8
        L_0x00cc:
            java.lang.String r8 = "mUserRepository"
            com.fossil.ee7.d(r8)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.k(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void e(String str, String str2) {
        ee7.b(str2, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local.d(str3, ".setSecretKeyToDevice(), secretKey=" + str);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setSecretKey(str2, str);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, ".setSecretKeyToDevice() - e=" + e2);
        }
    }

    @DexIgnore
    public final boolean a(String str, UserProfile userProfile) {
        ee7.b(str, "serial");
        ee7.b(userProfile, "userProfile");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        boolean z2 = false;
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "startDeviceSyncInButtonService - serial=" + str);
            if (!TextUtils.isEmpty(str)) {
                IButtonConnectivity iButtonConnectivity = f0;
                if (iButtonConnectivity != null) {
                    time_stamp_for_non_executable_method = iButtonConnectivity.deviceStartSync(str, userProfile);
                    z2 = true;
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = a0;
                local2.e(str3, "startDeviceSyncInButtonService - serial " + str);
            }
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local3.e(str4, "startDeviceSyncInButtonService - serial " + str + " exception " + e2);
        }
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str5 = a0;
        local4.d(str5, "put timestamp " + time_stamp_for_non_executable_method + " for sync session");
        return z2;
    }

    @DexIgnore
    public final boolean e() {
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            return iButtonConnectivity != null && iButtonConnectivity.getGattState(c()) == 2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.d(str, "exception when get gatt state " + e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean c(String str) {
        ee7.b(str, "newActiveDeviceSerial");
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                boolean forceSwitchDeviceWithoutErase = iButtonConnectivity.forceSwitchDeviceWithoutErase(str);
                if (!forceSwitchDeviceWithoutErase) {
                    return forceSwitchDeviceWithoutErase;
                }
                i(str);
                return forceSwitchDeviceWithoutErase;
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    public final void a(ad5 ad5, boolean z2, int i2) {
        ee7.b(ad5, "factory");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.d(str, "startDeviceSync, isNewDevice=" + z2 + ", syncMode=" + i2);
        qe5.c.b();
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", 0);
        intent.putExtra("SERIAL", c());
        qe.a(this).a(intent);
        String c2 = c();
        if (g(c2)) {
            FLogger.INSTANCE.getLocal().d(a0, "Device is syncing, Skip this sync.");
        } else {
            ad5.b(c2).a(new km5(i2, c2, z2), (fl4.e<? super lm5, ? super jm5>) null);
        }
    }

    @DexIgnore
    public final void c(String str, boolean z2) {
        ee7.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, ".onUpdateSecretKeyToServerResponse(), isSuccess=" + z2);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendPushSecretKeyResponse(str, z2);
                i97 i97 = i97.a;
                return;
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".onUpdateSecretKeyToServerResponse() - e=" + e2);
            i97 i972 = i97.a;
        }
    }

    @DexIgnore
    public final boolean g(String str) {
        ee7.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.isSyncing(str);
            }
            ee7.a();
            throw null;
        } catch (Exception unused) {
            return false;
        }
    }

    @DexIgnore
    public final long c(String str, List<? extends MicroAppMapping> list) {
        ee7.b(str, "serial");
        ee7.b(list, "mappings");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            for (MicroAppMapping microAppMapping : list) {
                if (microAppMapping != null) {
                    StringBuilder sb = new StringBuilder("Inside .setMappings set mapping of deviceId=" + str + ", gesture=" + microAppMapping.getGesture() + ", appID=" + microAppMapping.getMicroAppId() + ", declarationFiles=");
                    String[] declarationFiles = microAppMapping.getDeclarationFiles();
                    for (String str2 : declarationFiles) {
                        sb.append("||");
                        sb.append(str2);
                    }
                    FLogger.INSTANCE.getLocal().d(a0, sb.toString());
                    FLogger.INSTANCE.getLocal().d(a0, "Inside .setMappings set mapping of deviceId=" + str + ", gesture=" + microAppMapping.getGesture() + ", appID=" + microAppMapping.getMicroAppId() + ", extraInfo=" + Arrays.toString(microAppMapping.getDeclarationFiles()));
                }
            }
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetMapping(str, MicroAppMapping.convertToBLEMapping(list));
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.d(str, "stopLogService - failureCode=" + i2);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.stopLogService(i2);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local2.e(str2, "stopLogService - ex=" + e2);
        }
    }

    @DexIgnore
    public final long k(String str) {
        ee7.b(str, "serial");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.readCurrentWorkoutSession(str);
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, ".readCurrentWorkoutSession - e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void a(String str, boolean z2) {
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "confirmStopWorkout -serial =" + str + ", stopWorkout=" + z2);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.confirmStopWorkout(str, z2);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "confirmStopWorkout - serial " + str + " exception " + e2);
        }
    }

    @DexIgnore
    public final boolean b(String str, String str2, int i2) {
        ee7.b(str, "serial");
        ee7.b(str2, "serverSecretKey");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local.d(str3, ".sendServerSecretKeyToDevice(), serverSecretKey=" + str2);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendServerSecretKey(str, str2, i2);
                return true;
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, ".sendServerSecretKeyToDevice() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final void a(String str, boolean z2, WatchParamsFileMapping watchParamsFileMapping) {
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "onGetWatchParamResponse -serial =" + str + ", content=" + watchParamsFileMapping);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.onSetWatchParamResponse(str, z2, watchParamsFileMapping);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "onSetWatchParamResponse - serial " + str + " exception " + e2);
        }
    }

    @DexIgnore
    public final boolean b(String str, String str2) {
        ee7.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local.d(str3, ".sendCurrentSecretKeyToDevice(), secretKey=" + str2);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendCurrentSecretKey(str, str2);
                return true;
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, ".sendCurrentSecretKeyToDevice() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean a(String str, boolean z2, int i2) {
        ee7.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, ".switchDeviceResponse(), isSuccess=" + z2 + ", failureCode=" + i2);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.switchDeviceResponse(str, z2, i2);
                return true;
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".pairDeviceResponse() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final Object b(String str, fb7<Object> fb7) {
        return vh7.a(qj7.b(), new h(this, str, null), fb7);
    }

    @DexIgnore
    public final void a(Installation installation) {
        installation.setLocaleIdentifier(l());
    }

    @DexIgnore
    public final void b(String str, List<? extends BLEMapping> list) {
        ee7.b(str, "serial");
        ee7.b(list, "mappings");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setAutoMapping serial=" + str + "mappingsSize=" + list.size());
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoMapping(str, list);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(java.lang.String r5, com.fossil.fb7<? super com.fossil.i97> r6) {
        /*
            r4 = this;
            boolean r0 = r6 instanceof com.portfolio.platform.PortfolioApp.t
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.portfolio.platform.PortfolioApp$t r0 = (com.portfolio.platform.PortfolioApp.t) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.PortfolioApp$t r0 = new com.portfolio.platform.PortfolioApp$t
            r0.<init>(r4, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r5 = r0.L$1
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.t87.a(r6)
            goto L_0x0049
        L_0x0031:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L_0x0039:
            com.fossil.t87.a(r6)
            r0.L$0 = r4
            r0.L$1 = r5
            r0.label = r3
            java.lang.Object r6 = r4.d(r0)
            if (r6 != r1) goto L_0x0049
            return r1
        L_0x0049:
            com.misfit.frameworks.buttonservice.model.UserProfile r6 = (com.misfit.frameworks.buttonservice.model.UserProfile) r6
            if (r6 == 0) goto L_0x0097
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.PortfolioApp.a0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "setImplicitDeviceConfig - currentUserProfile="
            r2.append(r3)
            r2.append(r6)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            com.misfit.frameworks.buttonservice.IButtonConnectivity r0 = com.portfolio.platform.PortfolioApp.f0     // Catch:{ Exception -> 0x0076 }
            if (r0 == 0) goto L_0x0071
            r0.setImplicitDeviceConfig(r6, r5)     // Catch:{ Exception -> 0x0076 }
            goto L_0x00a4
        L_0x0071:
            com.fossil.ee7.a()     // Catch:{ Exception -> 0x0076 }
            r5 = 0
            throw r5
        L_0x0076:
            r5 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r0 = com.portfolio.platform.PortfolioApp.a0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = ".setImplicitDisplayUnitSettings(), e="
            r1.append(r2)
            r1.append(r5)
            java.lang.String r1 = r1.toString()
            r6.e(r0, r1)
            r5.printStackTrace()
            goto L_0x00a4
        L_0x0097:
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = com.portfolio.platform.PortfolioApp.a0
            java.lang.String r0 = "setImplicitDeviceConfig - currentUserProfile is NULL"
            r5.e(r6, r0)
        L_0x00a4:
            com.fossil.i97 r5 = com.fossil.i97.a
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.c(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final long a(String str, int i2, int i3, int i4, boolean z2) {
        ee7.b(str, "serial");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "Inside " + a0 + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2);
            if (!TextUtils.isEmpty(str)) {
                IButtonConnectivity iButtonConnectivity = f0;
                if (iButtonConnectivity != null) {
                    return iButtonConnectivity.deviceUpdateActivityGoals(str, i2, i3, i4, z2);
                }
                ee7.a();
                throw null;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "Error Inside " + a0 + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2 + ", caloriesGoal=" + i3 + ", activeTimeGoal=" + i4 + ", goalRingEnabled=" + z2);
            return time_stamp_for_non_executable_method;
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local3.e(str4, "Error Inside " + a0 + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2 + ", ex=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long b(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        ee7.b(appNotificationFilterSettings, "notificationFilterSettings");
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setNotificationFilterSettings - notificationFilterSettings=" + appNotificationFilterSettings);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setNotificationFilterSettings(appNotificationFilterSettings, str);
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".setNotificationFilterSettings(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(String str, int i2, int i3, boolean z2) {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "Inside " + a0 + ".playVibeNotification");
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.playVibration(str, i2, i3, true);
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "Error inside " + a0 + ".playVibeNotification - e=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deleteDataFiles(str);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, "Error while deleting data file - e=" + e2);
        }
    }

    @DexIgnore
    public final long a(String str, List<? extends Alarm> list) {
        ee7.b(str, "serial");
        ee7.b(list, "alarms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setMultipleAlarms - alarms size=" + list.size());
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetListAlarm(str, list);
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "setMultipleAlarms - ex=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void a(List<? extends Alarm> list) {
        ee7.b(list, "alarms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.e(str, "setAutoAlarms - alarms=" + list);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSetAutoListAlarm(list);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local2.e(str2, "setAutoAlarms - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        String str3;
        String str4 = "localization_" + str + '.';
        File[] listFiles = new File(getFilesDir() + "/localization").listFiles();
        int length = listFiles.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                str3 = "";
                break;
            }
            File file = listFiles[i2];
            ee7.a((Object) file, "file");
            String path = file.getPath();
            ee7.a((Object) path, "file.path");
            if (nh7.a((CharSequence) path, (CharSequence) str4, false, 2, (Object) null)) {
                str3 = file.getPath();
                ee7.a((Object) str3, "file.path");
                break;
            }
            i2++;
        }
        FLogger.INSTANCE.getLocal().d(a0, "setLocalization - neededLocalizationFilePath: " + str3);
        if (TextUtils.isEmpty(str3)) {
            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new f(this, str2, null), 3, null);
            return;
        }
        IButtonConnectivity iButtonConnectivity = f0;
        if (iButtonConnectivity != null) {
            iButtonConnectivity.setLocalizationData(new LocalizationData(str3, null, 2, null), str2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(boolean r8, com.fossil.fb7<? super com.fossil.i97> r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof com.portfolio.platform.PortfolioApp.y
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.portfolio.platform.PortfolioApp$y r0 = (com.portfolio.platform.PortfolioApp.y) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.PortfolioApp$y r0 = new com.portfolio.platform.PortfolioApp$y
            r0.<init>(r7, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x0061
            if (r2 == r4) goto L_0x0049
            if (r2 != r3) goto L_0x0041
            boolean r4 = r0.Z$1
            java.lang.Object r8 = r0.L$3
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r1 = r0.L$2
            com.misfit.frameworks.buttonservice.IButtonConnectivity r1 = (com.misfit.frameworks.buttonservice.IButtonConnectivity) r1
            java.lang.Object r2 = r0.L$1
            java.lang.String r2 = (java.lang.String) r2
            boolean r2 = r0.Z$0
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.t87.a(r9)     // Catch:{ Exception -> 0x00ba }
            goto L_0x00b0
        L_0x0041:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L_0x0049:
            boolean r8 = r0.Z$1
            java.lang.Object r1 = r0.L$3
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r0.L$2
            com.misfit.frameworks.buttonservice.IButtonConnectivity r2 = (com.misfit.frameworks.buttonservice.IButtonConnectivity) r2
            java.lang.Object r3 = r0.L$1
            java.lang.String r3 = (java.lang.String) r3
            boolean r3 = r0.Z$0
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.t87.a(r9)
            goto L_0x0089
        L_0x0061:
            com.fossil.t87.a(r9)
            java.lang.String r9 = r7.c()
            r2 = 0
            if (r8 != 0) goto L_0x0093
            com.misfit.frameworks.buttonservice.IButtonConnectivity r3 = com.portfolio.platform.PortfolioApp.f0
            if (r3 == 0) goto L_0x008f
            r2 = 0
            r0.L$0 = r7
            r0.Z$0 = r8
            r0.L$1 = r9
            r0.L$2 = r3
            r0.L$3 = r9
            r0.Z$1 = r2
            r0.label = r4
            java.lang.Object r8 = r7.d(r0)
            if (r8 != r1) goto L_0x0085
            return r1
        L_0x0085:
            r1 = r9
            r2 = r3
            r9 = r8
            r8 = 0
        L_0x0089:
            com.misfit.frameworks.buttonservice.model.UserProfile r9 = (com.misfit.frameworks.buttonservice.model.UserProfile) r9
            r2.updatePercentageGoalProgress(r1, r8, r9)
            goto L_0x00be
        L_0x008f:
            com.fossil.ee7.a()
            throw r2
        L_0x0093:
            com.misfit.frameworks.buttonservice.IButtonConnectivity r5 = com.portfolio.platform.PortfolioApp.f0
            if (r5 == 0) goto L_0x00b6
            r0.L$0 = r7
            r0.Z$0 = r8
            r0.L$1 = r9
            r0.L$2 = r5
            r0.L$3 = r9
            r0.Z$1 = r4
            r0.label = r3
            java.lang.Object r8 = r7.d(r0)
            if (r8 != r1) goto L_0x00ac
            return r1
        L_0x00ac:
            r1 = r5
            r6 = r9
            r9 = r8
            r8 = r6
        L_0x00b0:
            com.misfit.frameworks.buttonservice.model.UserProfile r9 = (com.misfit.frameworks.buttonservice.model.UserProfile) r9
            r1.updatePercentageGoalProgress(r8, r4, r9)
            goto L_0x00be
        L_0x00b6:
            com.fossil.ee7.a()
            throw r2
        L_0x00ba:
            r8 = move-exception
            r8.printStackTrace()
        L_0x00be:
            com.fossil.i97 r8 = com.fossil.i97.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.a(boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007d A[SYNTHETIC, Splitter:B:20:0x007d] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(java.lang.String r6, com.fossil.fb7<? super java.lang.Boolean> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.portfolio.platform.PortfolioApp.u
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.portfolio.platform.PortfolioApp$u r0 = (com.portfolio.platform.PortfolioApp.u) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.PortfolioApp$u r0 = new com.portfolio.platform.PortfolioApp$u
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x003c
            if (r2 != r4) goto L_0x0034
            boolean r6 = r0.Z$0
            java.lang.Object r1 = r0.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            com.fossil.t87.a(r7)
            goto L_0x0051
        L_0x0034:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x003c:
            com.fossil.t87.a(r7)
            r0.L$0 = r5
            r0.L$1 = r6
            r0.Z$0 = r3
            r0.label = r4
            java.lang.Object r7 = r5.d(r0)
            if (r7 != r1) goto L_0x004e
            return r1
        L_0x004e:
            r0 = r5
            r1 = r6
            r6 = 0
        L_0x0051:
            com.misfit.frameworks.buttonservice.model.UserProfile r7 = (com.misfit.frameworks.buttonservice.model.UserProfile) r7
            if (r7 != 0) goto L_0x007d
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r0 = com.portfolio.platform.PortfolioApp.a0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Error inside "
            r1.append(r2)
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.a0
            r1.append(r2)
            java.lang.String r2 = ".switchActiveDevice - user is null"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r7.e(r0, r1)
            java.lang.Boolean r6 = com.fossil.pb7.a(r6)
            return r6
        L_0x007d:
            com.misfit.frameworks.buttonservice.IButtonConnectivity r6 = com.portfolio.platform.PortfolioApp.f0     // Catch:{ Exception -> 0x009b }
            if (r6 == 0) goto L_0x0096
            boolean r6 = r6.switchActiveDevice(r1, r7)     // Catch:{ Exception -> 0x009b }
            if (r6 == 0) goto L_0x0094
            int r7 = r1.length()     // Catch:{ Exception -> 0x009b }
            if (r7 != 0) goto L_0x008e
            goto L_0x008f
        L_0x008e:
            r4 = 0
        L_0x008f:
            if (r4 == 0) goto L_0x0094
            r0.i(r1)     // Catch:{ Exception -> 0x009b }
        L_0x0094:
            r3 = r6
            goto L_0x009f
        L_0x0096:
            com.fossil.ee7.a()     // Catch:{ Exception -> 0x009b }
            r6 = 0
            throw r6
        L_0x009b:
            r6 = move-exception
            r6.printStackTrace()
        L_0x009f:
            java.lang.Boolean r6 = com.fossil.pb7.a(r3)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.d(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a() {
        ch5 ch5 = this.c;
        if (ch5 != null) {
            ch5.a(53);
            File cacheDir = getCacheDir();
            ee7.a((Object) cacheDir, "cacheDirectory");
            File file = new File(cacheDir.getParent());
            if (file.exists()) {
                String[] list = file.list();
                for (String str : list) {
                    if (!ee7.a((Object) str, (Object) "lib")) {
                        a(new File(file, str));
                    }
                }
            }
            PendingIntent activity = PendingIntent.getActivity(this, 123456, new Intent(this, SplashScreenActivity.class), SQLiteDatabase.CREATE_IF_NECESSARY);
            Object systemService = getSystemService(Alarm.TABLE_NAME);
            if (systemService != null) {
                ((AlarmManager) systemService).set(1, System.currentTimeMillis() + ((long) 1000), activity);
                if (19 <= Build.VERSION.SDK_INT) {
                    Object systemService2 = getSystemService(Constants.ACTIVITY);
                    if (systemService2 != null) {
                        ((ActivityManager) systemService2).clearApplicationUserData();
                        return;
                    }
                    throw new x87("null cannot be cast to non-null type android.app.ActivityManager");
                }
                try {
                    Runtime.getRuntime().exec("pm clear " + getPackageName());
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                throw new x87("null cannot be cast to non-null type android.app.AlarmManager");
            }
        } else {
            ee7.d("sharedPreferencesManager");
            throw null;
        }
    }

    @DexIgnore
    public final long d(String str, boolean z2) {
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setFrontLightEnable() - serial=" + str + ", isFrontLightEnable=" + z2);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setFrontLightEnable(str, z2);
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void d(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = a0;
        local.d(str3, ".setPairedSerial - serial=" + str + ", macaddress=" + str2);
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setPairedSerial(str, str2);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, "setPairedSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    public final String d(String str) {
        ee7.b(str, "packageName");
        String str2 = (String) ea7.f(nh7.a((CharSequence) str, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0, 6, (Object) null));
        PortfolioApp portfolioApp = c0;
        if (portfolioApp != null) {
            PackageManager packageManager = portfolioApp.getPackageManager();
            try {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 0);
                return applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo).toString() : str2;
            } catch (PackageManager.NameNotFoundException unused) {
                return str2;
            }
        } else {
            ee7.d("instance");
            throw null;
        }
    }

    @DexIgnore
    public final boolean a(File file) {
        if (file == null) {
            return true;
        }
        if (!file.isDirectory()) {
            return file.delete();
        }
        String[] list = file.list();
        int length = list.length;
        boolean z2 = true;
        for (int i2 = 0; i2 < length; i2++) {
            z2 = a(new File(file, list[i2])) && z2;
        }
        return z2;
    }

    @DexIgnore
    public final void a(CommunicateMode communicateMode, String str, String str2) {
        ee7.b(communicateMode, "communicateMode");
        ee7.b(str, "serial");
        ee7.b(str2, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = a0;
        local.d(str3, "addLog - communicateMode=" + communicateMode + ", serial=" + str + ", message=" + str2);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.addLog(communicateMode.ordinal(), str, str2);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, "addLog - ex=" + e2);
        }
    }

    @DexIgnore
    public final void a(CommunicateMode communicateMode, String str, CommunicateMode communicateMode2, String str2) {
        ee7.b(communicateMode, "curCommunicateMode");
        ee7.b(str, "curSerial");
        ee7.b(communicateMode2, "newCommunicateMode");
        ee7.b(str2, "newSerial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = a0;
        local.d(str3, "changePendingLogKey - curCommunicateMode=" + communicateMode + ", curSerial=" + str + ", newCommunicateMode=" + communicateMode2 + ", newSerial=" + str2);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.changePendingLogKey(communicateMode.ordinal(), str, communicateMode2.ordinal(), str2);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, "changePendingLogKey - ex=" + e2);
        }
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, int i5) {
        String c2 = c();
        int min = Math.min(i2, 65535);
        int min2 = Math.min(i3, 65535);
        int min3 = Math.min(i4, 65535);
        int min4 = Math.min(i5, 4294967);
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.d(str, "Inside simulateDisconnection - delay=" + min + ", duration=" + min2);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.simulateDisconnection(c2, min, min2, min3, min4);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local2.e(str2, "Error inside " + a0 + ".simulateDisconnection - e=" + e2);
        }
    }

    @DexIgnore
    public final long a(String str, CustomRequest customRequest) {
        ee7.b(str, "serial");
        ee7.b(customRequest, Constants.COMMAND);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "sendCustomCommand() - serial=" + str + ", command=" + customRequest);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendCustomCommand(str, customRequest);
                return time_stamp_for_non_executable_method;
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x005a, code lost:
        if (com.fossil.nh7.a((java.lang.CharSequence) r6, (java.lang.CharSequence) "image", false, 2, (java.lang.Object) null) == false) goto L_0x0061;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.content.Intent r6, android.net.Uri r7) {
        /*
            r5 = this;
            java.lang.String r0 = "fileUri"
            com.fossil.ee7.b(r7, r0)
            java.lang.String r0 = r5.a(r7)
            if (r6 == 0) goto L_0x0010
            java.lang.String r6 = r6.getType()
            goto L_0x0012
        L_0x0010:
            java.lang.String r6 = ""
        L_0x0012:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.PortfolioApp.a0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "isImageFile intentMimeType="
            r3.append(r4)
            r3.append(r6)
            java.lang.String r4 = ", uriMimeType="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            java.lang.String r7 = r7.getPath()
            r1 = 0
            if (r7 == 0) goto L_0x0076
            java.lang.String r2 = "fileUri.path!!"
            com.fossil.ee7.a(r7, r2)
            r2 = 2
            r3 = 0
            java.lang.String r4 = "pickerImage"
            boolean r7 = com.fossil.nh7.a(r7, r4, r3, r2, r1)
            if (r7 != 0) goto L_0x0074
            boolean r7 = android.text.TextUtils.isEmpty(r6)
            java.lang.String r4 = "image"
            if (r7 != 0) goto L_0x0061
            if (r6 == 0) goto L_0x005d
            boolean r6 = com.fossil.nh7.a(r6, r4, r3, r2, r1)
            if (r6 != 0) goto L_0x0074
            goto L_0x0061
        L_0x005d:
            com.fossil.ee7.a()
            throw r1
        L_0x0061:
            boolean r6 = android.text.TextUtils.isEmpty(r0)
            if (r6 != 0) goto L_0x0075
            if (r0 == 0) goto L_0x0070
            boolean r6 = com.fossil.nh7.a(r0, r4, r3, r2, r1)
            if (r6 == 0) goto L_0x0075
            goto L_0x0074
        L_0x0070:
            com.fossil.ee7.a()
            throw r1
        L_0x0074:
            r3 = 1
        L_0x0075:
            return r3
        L_0x0076:
            com.fossil.ee7.a()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.PortfolioApp.a(android.content.Intent, android.net.Uri):boolean");
    }

    @DexIgnore
    public final String a(Uri uri) {
        if (ee7.a((Object) uri.getScheme(), (Object) "content")) {
            return getContentResolver().getType(uri);
        }
        String path = uri.getPath();
        if (path != null) {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(path)).toString()));
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final Object a(String str, String str2, fb7<Object> fb7) {
        return vh7.a(qj7.b(), new q(this, str, str2, null), fb7);
    }

    @DexIgnore
    public final boolean a(String str, PairingResponse pairingResponse) {
        ee7.b(str, "serial");
        ee7.b(pairingResponse, "response");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, ".pairDeviceResponse(), response=" + pairingResponse);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.pairDeviceResponse(str, pairingResponse);
                return true;
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".pairDeviceResponse() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, int i2) {
        ee7.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local.d(str3, ".sendRandomKeyToDevice(), randomKey=" + str2);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendRandomKey(str, str2, i2);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, ".sendRandomKeyToDevice() - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.cancelPairDevice(str);
                i97 i97 = i97.a;
                return;
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, ".cancelPairDevice() - e=" + e2);
            i97 i972 = i97.a;
        }
    }

    @DexIgnore
    public final void a(String str, VibrationStrengthObj vibrationStrengthObj) {
        ee7.b(str, "serial");
        ee7.b(vibrationStrengthObj, "vibrationStrengthObj");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "Inside " + a0 + ".setVibrationStrength");
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSetVibrationStrength(str, vibrationStrengthObj);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "Error inside " + a0 + ".setVibrationStrength - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(String str, FirmwareData firmwareData, UserProfile userProfile) {
        ee7.b(str, "serial");
        ee7.b(firmwareData, "firmwareData");
        ee7.b(userProfile, "userProfile");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "Inside " + a0 + ".otaDevice");
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceOta(str, firmwareData, userProfile);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "Error inside " + a0 + ".otaDevice - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(ComplicationAppMappingSettings complicationAppMappingSettings, String str) {
        ee7.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setComplicationApps - complicationAppsSettings=" + complicationAppMappingSettings);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoComplicationAppSettings(complicationAppMappingSettings, str);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.e(str, "confirmBcStatusToButtonService - bcStatus: " + z2);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.confirmBCStatus(z2);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(DeviceAppResponse deviceAppResponse, String str) {
        ee7.b(deviceAppResponse, "deviceAppResponse");
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "sendComplicationAppInfo - deviceAppResponse=" + deviceAppResponse);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendDeviceAppResponse(deviceAppResponse, str);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(MusicResponse musicResponse, String str) {
        ee7.b(musicResponse, "musicAppResponse");
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "sendMusicAppResponse - musicAppResponse=" + musicResponse);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendMusicAppResponse(musicResponse, str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final long a(WatchAppMappingSettings watchAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings, BackgroundConfig backgroundConfig, String str) {
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setPresetApps - watchAppMappingSettings=" + watchAppMappingSettings + ", complicationAppsSettings=" + complicationAppMappingSettings + ", backgroundConfig=" + backgroundConfig);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setPresetApps(watchAppMappingSettings, complicationAppMappingSettings, backgroundConfig, str);
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(ReplyMessageMappingGroup replyMessageMappingGroup, String str) {
        ee7.b(replyMessageMappingGroup, "replyMessageGroup");
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setReplyMessageMapping - replyMessageGroup=" + replyMessageMappingGroup);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setReplyMessageMappingSetting(replyMessageMappingGroup, str);
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".setReplyMessageMappingSetting(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(String str, NotificationBaseObj notificationBaseObj) {
        ee7.b(str, "serial");
        ee7.b(notificationBaseObj, "notification");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "notifyNotificationControlEvent - notification=" + notificationBaseObj);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.notifyNotificationEvent(notificationBaseObj, str);
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".notifyNotificationControlEvent(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(String str, WorkoutDetectionSetting workoutDetectionSetting) {
        ee7.b(str, "serial");
        ee7.b(workoutDetectionSetting, "workoutDetectionSetting");
        FLogger.INSTANCE.getLocal().d(a0, "setWorkoutDetectionSetting()");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setWorkoutDetectionSetting(workoutDetectionSetting, str);
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, "setWorkoutDetectionSetting(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void a(WatchAppMappingSettings watchAppMappingSettings, String str) {
        ee7.b(watchAppMappingSettings, "watchAppMappingSettings");
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setAutoWatchAppSettings - watchAppMappingSettings=" + watchAppMappingSettings);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoWatchAppSettings(watchAppMappingSettings, str);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(BackgroundConfig backgroundConfig, String str) {
        ee7.b(backgroundConfig, "backgroundConfig");
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setAutoBackgroundImageConfig - backgroundConfig=" + backgroundConfig);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoBackgroundImageConfig(backgroundConfig, str);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        ee7.b(appNotificationFilterSettings, "notificationFilterSettings");
        ee7.b(str, "serial");
        Iterator<T> it = appNotificationFilterSettings.getNotificationFilters().iterator();
        while (it.hasNext()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "setAutoNotificationFilterSettings " + ((Object) it.next()));
        }
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoNotificationFilterSettings(appNotificationFilterSettings, str);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".setAutoNotificationFilterSettings(), e=" + e2);
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(UserDisplayUnit userDisplayUnit, String str) {
        ee7.b(userDisplayUnit, "userDisplayUnit");
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setImplicitDisplayUnitSettings - userDisplayUnit=" + userDisplayUnit);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setImplicitDisplayUnitSettings(userDisplayUnit, str);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".setImplicitDisplayUnitSettings(), e=" + e2);
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(UserProfile userProfile) {
        ee7.b(userProfile, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.d(str, "setAutoUserBiometricData - userProfile=" + userProfile);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoUserBiometricData(userProfile);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(String str, DeviceAppResponse deviceAppResponse) {
        ee7.b(str, "serial");
        ee7.b(deviceAppResponse, "deviceAppResponse");
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendMicroAppRemoteActivity(str, deviceAppResponse);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, "Error inside " + a0 + ".sendMicroAppRemoteActivity - e=" + e2);
        }
    }

    @DexIgnore
    public final long a(String str, Location location) {
        ee7.b(str, "serial");
        ee7.b(location, PlaceFields.LOCATION);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setWorkoutGPSDataSession(str, location);
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, ".setWorkoutGPSDataSession - e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void a(WorkoutConfigData workoutConfigData, String str) {
        ee7.b(workoutConfigData, "workoutConfigData");
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setWorkoutConfig - workoutConfigData=" + workoutConfigData);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setWorkoutConfig(workoutConfigData, str);
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final long a(String str, InactiveNudgeData inactiveNudgeData) {
        ee7.b(str, "serial");
        ee7.b(inactiveNudgeData, "inactiveNudgeData");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetInactiveNudgeConfig(str, inactiveNudgeData);
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, ".setInactiveNudgeConfig - e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void a(ServerError serverError) {
        FLogger.INSTANCE.getLocal().d(a0, "forceLogout");
        if (this.H) {
            qn5 qn5 = this.q;
            if (qn5 != null) {
                qn5.a(new qn5.b(2, null), new g(this));
            } else {
                ee7.d("mDeleteLogoutUserUseCase");
                throw null;
            }
        } else {
            this.O = serverError;
            this.N = true;
        }
    }

    @DexIgnore
    public final void a(MFUser mFUser) {
        if (mFUser == null) {
            qd5.f.c().a(false);
            return;
        }
        qd5.f.c().b(mFUser.getUserId());
        qd5.f.c().a(mFUser.getDiagnosticEnabled());
    }

    @DexIgnore
    public final /* synthetic */ Object a(fb7<? super i97> fb7) {
        Object a2 = vh7.a(qj7.b(), new c(this, null), fb7);
        return a2 == nb7.a() ? a2 : i97.a;
    }
}
