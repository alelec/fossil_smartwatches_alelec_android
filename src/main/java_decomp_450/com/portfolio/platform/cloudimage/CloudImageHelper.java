package com.portfolio.platform.cloudimage;

import android.widget.ImageView;
import com.fossil.ee7;
import com.fossil.ik7;
import com.fossil.n87;
import com.fossil.o87;
import com.fossil.pj4;
import com.fossil.qj7;
import com.fossil.xh7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.Constants;
import java.io.File;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CloudImageHelper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ n87 instance$delegate; // = o87.a(CloudImageHelper$Companion$instance$Anon2.INSTANCE);
    @DexIgnore
    public PortfolioApp mApp;
    @DexIgnore
    public pj4 mAppExecutors;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public static /* synthetic */ void instance$annotations() {
        }

        @DexIgnore
        public final CloudImageHelper getInstance() {
            n87 access$getInstance$cp = CloudImageHelper.instance$delegate;
            Companion companion = CloudImageHelper.Companion;
            return (CloudImageHelper) access$getInstance$cp.getValue();
        }

        @DexIgnore
        public final String getTAG() {
            return CloudImageHelper.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Holder {
        @DexIgnore
        public static /* final */ Holder INSTANCE; // = new Holder();

        @DexIgnore
        /* renamed from: INSTANCE  reason: collision with other field name */
        public static /* final */ CloudImageHelper f1INSTANCE; // = new CloudImageHelper();

        @DexIgnore
        public final CloudImageHelper getINSTANCE() {
            return f1INSTANCE;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ItemImage {
        @DexIgnore
        public Constants.CalibrationType mCalibrationType; // = Constants.CalibrationType.NONE;
        @DexIgnore
        public Constants.DeviceType mDeviceType; // = Constants.DeviceType.NONE;
        @DexIgnore
        public String mFastPairId;
        @DexIgnore
        public File mFile;
        @DexIgnore
        public OnImageCallbackListener mListener;
        @DexIgnore
        public Integer mResourceId; // = -1;
        @DexIgnore
        public String mSerialNumber;
        @DexIgnore
        public String mSerialPrefix;
        @DexIgnore
        public WeakReference<ImageView> mWeakReferenceImageView;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ItemImage() {
        }

        @DexIgnore
        public final void download() {
            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new CloudImageHelper$ItemImage$download$Anon1(this, null), 3, null);
        }

        @DexIgnore
        public final void downloadForCalibration() {
            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new CloudImageHelper$ItemImage$downloadForCalibration$Anon1(this, null), 3, null);
        }

        @DexIgnore
        public final void downloadForWearOS() {
            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new CloudImageHelper$ItemImage$downloadForWearOS$Anon1(this, null), 3, null);
        }

        @DexIgnore
        public final File getFile() {
            return this.mFile;
        }

        @DexIgnore
        public final ItemImage setFastPairId(String str) {
            ee7.b(str, "fastPairId");
            this.mFastPairId = str;
            return this;
        }

        @DexIgnore
        public final ItemImage setFile(File file) {
            this.mFile = file;
            return this;
        }

        @DexIgnore
        public final ItemImage setImageCallback(OnImageCallbackListener onImageCallbackListener) {
            this.mListener = onImageCallbackListener;
            return this;
        }

        @DexIgnore
        public final ItemImage setPlaceHolder(ImageView imageView, int i) {
            ee7.b(imageView, "imageView");
            this.mWeakReferenceImageView = new WeakReference<>(imageView);
            this.mResourceId = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        public final ItemImage setSerialNumber(String str) {
            ee7.b(str, "serialNumber");
            this.mSerialNumber = str;
            return this;
        }

        @DexIgnore
        public final ItemImage setSerialPrefix(String str) {
            ee7.b(str, "serialPrefix");
            this.mSerialPrefix = str;
            return this;
        }

        @DexIgnore
        public final ItemImage setType(Constants.DeviceType deviceType) {
            ee7.b(deviceType, "deviceType");
            this.mDeviceType = deviceType;
            return this;
        }

        @DexIgnore
        public final ItemImage setType(Constants.CalibrationType calibrationType) {
            ee7.b(calibrationType, "calibrationType");
            this.mCalibrationType = calibrationType;
            return this;
        }
    }

    @DexIgnore
    public interface OnForceDownloadCallbackListener {
        @DexIgnore
        void onDownloadCallback(boolean z);
    }

    @DexIgnore
    public interface OnImageCallbackListener {
        @DexIgnore
        void onImageCallback(String str, String str2);
    }

    /*
    static {
        String simpleName = CloudImageHelper$Companion$TAG$Anon1.INSTANCE.getClass().getSimpleName();
        ee7.a((Object) simpleName, "CloudImageHelper::javaClass.javaClass.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public CloudImageHelper() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public static final CloudImageHelper getInstance() {
        return Companion.getInstance();
    }

    @DexIgnore
    public final PortfolioApp getMApp() {
        PortfolioApp portfolioApp = this.mApp;
        if (portfolioApp != null) {
            return portfolioApp;
        }
        ee7.d("mApp");
        throw null;
    }

    @DexIgnore
    public final pj4 getMAppExecutors() {
        pj4 pj4 = this.mAppExecutors;
        if (pj4 != null) {
            return pj4;
        }
        ee7.d("mAppExecutors");
        throw null;
    }

    @DexIgnore
    public final void setMApp(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "<set-?>");
        this.mApp = portfolioApp;
    }

    @DexIgnore
    public final void setMAppExecutors(pj4 pj4) {
        ee7.b(pj4, "<set-?>");
        this.mAppExecutors = pj4;
    }

    @DexIgnore
    public final ItemImage with() {
        ItemImage itemImage = new ItemImage();
        if (itemImage.getFile() == null) {
            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new CloudImageHelper$with$Anon1(this, itemImage, null), 3, null);
        }
        return itemImage;
    }
}
