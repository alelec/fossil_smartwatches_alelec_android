package com.portfolio.platform.cloudimage;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fb7;
import com.fossil.rb7;
import com.fossil.tb7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.cloudimage.URLRequestTaskHelper", f = "URLRequestTaskHelper.kt", l = {60, 62, 71}, m = "execute")
public final class URLRequestTaskHelper$execute$Anon1 extends rb7 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ URLRequestTaskHelper this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public URLRequestTaskHelper$execute$Anon1(URLRequestTaskHelper uRLRequestTaskHelper, fb7 fb7) {
        super(fb7);
        this.this$0 = uRLRequestTaskHelper;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.execute(this);
    }
}
