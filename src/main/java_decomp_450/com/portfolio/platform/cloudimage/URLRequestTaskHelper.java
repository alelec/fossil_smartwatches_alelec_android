package com.portfolio.platform.cloudimage;

import com.fossil.ee7;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class URLRequestTaskHelper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = (Constants.MAIN_TAG + URLRequestTaskHelper.class.getSimpleName());
    @DexIgnore
    public String destinationUnzipPath;
    @DexIgnore
    public String fastPairId;
    @DexIgnore
    public String feature;
    @DexIgnore
    public OnNextTaskListener listener;
    @DexIgnore
    public ApiServiceV2 mApiService;
    @DexIgnore
    public String resolution;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public String zipFilePath;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return URLRequestTaskHelper.TAG;
        }

        @DexIgnore
        public final URLRequestTaskHelper newInstance() {
            return new URLRequestTaskHelper();
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface OnNextTaskListener {
        @DexIgnore
        void downloadFile(String str, String str2, AssetsDeviceResponse assetsDeviceResponse);

        @DexIgnore
        void onGetDeviceAssetFailed();
    }

    @DexIgnore
    public URLRequestTaskHelper() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public static /* synthetic */ void init$default(URLRequestTaskHelper uRLRequestTaskHelper, String str, String str2, String str3, String str4, String str5, String str6, int i, Object obj) {
        if ((i & 32) != 0) {
            str6 = "";
        }
        uRLRequestTaskHelper.init(str, str2, str3, str4, str5, str6);
    }

    @DexIgnore
    public static final URLRequestTaskHelper newInstance() {
        return Companion.newInstance();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0184  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object execute(com.fossil.fb7<? super com.fossil.i97> r13) {
        /*
            r12 = this;
            boolean r0 = r13 instanceof com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r13
            com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$Anon1 r0 = (com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$Anon1 r0 = new com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$Anon1
            r0.<init>(r12, r13)
        L_0x0018:
            java.lang.Object r13 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            java.lang.String r4 = "], fastPairId = ["
            java.lang.String r5 = "], feature = ["
            r6 = 3
            r7 = 2
            r8 = 1
            r9 = 0
            if (r2 == 0) goto L_0x005b
            if (r2 == r8) goto L_0x0053
            if (r2 == r7) goto L_0x004a
            if (r2 != r6) goto L_0x0042
            java.lang.Object r1 = r0.L$2
            com.portfolio.platform.cloudimage.AssetsDeviceResponse r1 = (com.portfolio.platform.cloudimage.AssetsDeviceResponse) r1
            java.lang.Object r1 = r0.L$1
            com.fossil.zi5 r1 = (com.fossil.zi5) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.cloudimage.URLRequestTaskHelper r0 = (com.portfolio.platform.cloudimage.URLRequestTaskHelper) r0
            com.fossil.t87.a(r13)
            goto L_0x01d6
        L_0x0042:
            java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r13.<init>(r0)
            throw r13
        L_0x004a:
            java.lang.Object r2 = r0.L$0
            com.portfolio.platform.cloudimage.URLRequestTaskHelper r2 = (com.portfolio.platform.cloudimage.URLRequestTaskHelper) r2
            com.fossil.t87.a(r13)
            goto L_0x00c6
        L_0x0053:
            java.lang.Object r2 = r0.L$0
            com.portfolio.platform.cloudimage.URLRequestTaskHelper r2 = (com.portfolio.platform.cloudimage.URLRequestTaskHelper) r2
            com.fossil.t87.a(r13)
            goto L_0x00b2
        L_0x005b:
            com.fossil.t87.a(r13)
            com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
            java.lang.String r2 = com.portfolio.platform.cloudimage.URLRequestTaskHelper.TAG
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "execute() called with serialNumber = ["
            r10.append(r11)
            java.lang.String r11 = r12.serialNumber
            r10.append(r11)
            r10.append(r5)
            java.lang.String r11 = r12.feature
            r10.append(r11)
            r10.append(r4)
            java.lang.String r11 = r12.fastPairId
            r10.append(r11)
            r11 = 93
            r10.append(r11)
            java.lang.String r10 = r10.toString()
            r13.d(r2, r10)
            java.lang.String r13 = r12.serialNumber
            if (r13 == 0) goto L_0x009e
            int r13 = r13.length()
            if (r13 != 0) goto L_0x009c
            goto L_0x009e
        L_0x009c:
            r13 = 0
            goto L_0x009f
        L_0x009e:
            r13 = 1
        L_0x009f:
            if (r13 == 0) goto L_0x00b5
            com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$response$Anon1 r13 = new com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$response$Anon1
            r13.<init>(r12, r9)
            r0.L$0 = r12
            r0.label = r8
            java.lang.Object r13 = com.fossil.aj5.a(r13, r0)
            if (r13 != r1) goto L_0x00b1
            return r1
        L_0x00b1:
            r2 = r12
        L_0x00b2:
            com.fossil.zi5 r13 = (com.fossil.zi5) r13
            goto L_0x00c8
        L_0x00b5:
            com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$response$Anon2 r13 = new com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$response$Anon2
            r13.<init>(r12, r9)
            r0.L$0 = r12
            r0.label = r7
            java.lang.Object r13 = com.fossil.aj5.a(r13, r0)
            if (r13 != r1) goto L_0x00c5
            return r1
        L_0x00c5:
            r2 = r12
        L_0x00c6:
            com.fossil.zi5 r13 = (com.fossil.zi5) r13
        L_0x00c8:
            boolean r7 = r13 instanceof com.fossil.bj5
            if (r7 == 0) goto L_0x0184
            r4 = r13
            com.fossil.bj5 r4 = (com.fossil.bj5) r4
            java.lang.Object r5 = r4.a()
            if (r5 == 0) goto L_0x0180
            com.portfolio.platform.data.source.remote.ApiResponse r5 = (com.portfolio.platform.data.source.remote.ApiResponse) r5
            java.util.List r5 = r5.get_items()
            boolean r5 = r5.isEmpty()
            r5 = r5 ^ r8
            if (r5 == 0) goto L_0x01d6
            com.google.gson.Gson r5 = new com.google.gson.Gson
            r5.<init>()
            java.lang.Object r4 = r4.a()
            com.portfolio.platform.data.source.remote.ApiResponse r4 = (com.portfolio.platform.data.source.remote.ApiResponse) r4
            java.util.List r4 = r4.get_items()
            java.lang.Object r3 = r4.get(r3)
            com.google.gson.JsonElement r3 = (com.google.gson.JsonElement) r3
            java.lang.Class<com.portfolio.platform.cloudimage.AssetsDeviceResponse> r4 = com.portfolio.platform.cloudimage.AssetsDeviceResponse.class
            java.lang.Object r3 = r5.a(r3, r4)
            com.portfolio.platform.cloudimage.AssetsDeviceResponse r3 = (com.portfolio.platform.cloudimage.AssetsDeviceResponse) r3
            com.portfolio.platform.cloudimage.AssetUtil r4 = com.portfolio.platform.cloudimage.AssetUtil.INSTANCE
            java.lang.String r5 = r2.zipFilePath
            if (r5 == 0) goto L_0x017c
            boolean r4 = r4.checkFileExist(r5)
            if (r4 == 0) goto L_0x0157
            com.portfolio.platform.cloudimage.ChecksumUtil r4 = com.portfolio.platform.cloudimage.ChecksumUtil.INSTANCE
            java.lang.String r5 = r2.zipFilePath
            if (r5 == 0) goto L_0x0153
            com.portfolio.platform.cloudimage.Metadata r7 = r3.getMetadata()
            if (r7 == 0) goto L_0x011c
            java.lang.String r7 = r7.getChecksum()
            goto L_0x011d
        L_0x011c:
            r7 = r9
        L_0x011d:
            boolean r4 = r4.verifyDownloadFile(r5, r7)
            if (r4 != 0) goto L_0x0124
            goto L_0x0157
        L_0x0124:
            com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
            java.lang.String r0 = com.portfolio.platform.cloudimage.URLRequestTaskHelper.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "onSuccess: The assets with serialNumber = ["
            r1.append(r3)
            java.lang.String r3 = r2.serialNumber
            r1.append(r3)
            java.lang.String r3 = "] for feature =["
            r1.append(r3)
            java.lang.String r2 = r2.feature
            r1.append(r2)
            java.lang.String r2 = "] is the latest, no need to download!"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r13.d(r0, r1)
            goto L_0x01d6
        L_0x0153:
            com.fossil.ee7.a()
            throw r9
        L_0x0157:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.portfolio.platform.cloudimage.URLRequestTaskHelper.TAG
            java.lang.String r7 = "onSuccess: need to download new asset for this"
            r4.d(r5, r7)
            com.fossil.tk7 r4 = com.fossil.qj7.c()
            com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$Anon2 r5 = new com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$Anon2
            r5.<init>(r2, r3, r9)
            r0.L$0 = r2
            r0.L$1 = r13
            r0.L$2 = r3
            r0.label = r6
            java.lang.Object r13 = com.fossil.vh7.a(r4, r5, r0)
            if (r13 != r1) goto L_0x01d6
            return r1
        L_0x017c:
            com.fossil.ee7.a()
            throw r9
        L_0x0180:
            com.fossil.ee7.a()
            throw r9
        L_0x0184:
            boolean r0 = r13 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x01d6
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.cloudimage.URLRequestTaskHelper.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r6 = "onFail: serialNumber = ["
            r3.append(r6)
            java.lang.String r6 = r2.serialNumber
            r3.append(r6)
            r3.append(r5)
            java.lang.String r5 = r2.feature
            r3.append(r5)
            r3.append(r4)
            java.lang.String r4 = r2.fastPairId
            r3.append(r4)
            java.lang.String r4 = "], error = ["
            r3.append(r4)
            com.fossil.yi5 r13 = (com.fossil.yi5) r13
            java.lang.Throwable r13 = r13.d()
            if (r13 == 0) goto L_0x01c0
            java.lang.String r9 = r13.getMessage()
        L_0x01c0:
            r3.append(r9)
            java.lang.String r13 = "]"
            r3.append(r13)
            java.lang.String r13 = r3.toString()
            r0.e(r1, r13)
            com.portfolio.platform.cloudimage.URLRequestTaskHelper$OnNextTaskListener r13 = r2.listener
            if (r13 == 0) goto L_0x01d6
            r13.onGetDeviceAssetFailed()
        L_0x01d6:
            com.fossil.i97 r13 = com.fossil.i97.a
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.cloudimage.URLRequestTaskHelper.execute(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final String getDestinationUnzipPath$app_fossilRelease() {
        return this.destinationUnzipPath;
    }

    @DexIgnore
    public final String getFastPairId$app_fossilRelease() {
        return this.fastPairId;
    }

    @DexIgnore
    public final String getFeature$app_fossilRelease() {
        return this.feature;
    }

    @DexIgnore
    public final OnNextTaskListener getListener$app_fossilRelease() {
        return this.listener;
    }

    @DexIgnore
    public final ApiServiceV2 getMApiService() {
        ApiServiceV2 apiServiceV2 = this.mApiService;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        ee7.d("mApiService");
        throw null;
    }

    @DexIgnore
    public final String getSerialNumber$app_fossilRelease() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getZipFilePath$app_fossilRelease() {
        return this.zipFilePath;
    }

    @DexIgnore
    public final void init(String str, String str2, String str3, String str4, String str5, String str6) {
        ee7.b(str, "zipFilePath");
        ee7.b(str2, "destinationUnzipPath");
        ee7.b(str3, "serialNumber");
        ee7.b(str4, "feature");
        ee7.b(str5, "resolution");
        ee7.b(str6, "fastPairId");
        this.zipFilePath = str;
        this.destinationUnzipPath = str2;
        this.serialNumber = str3;
        this.feature = str4;
        this.resolution = str5;
        this.fastPairId = str6;
    }

    @DexIgnore
    public final void setDestinationUnzipPath$app_fossilRelease(String str) {
        this.destinationUnzipPath = str;
    }

    @DexIgnore
    public final void setFastPairId$app_fossilRelease(String str) {
        this.fastPairId = str;
    }

    @DexIgnore
    public final void setFeature$app_fossilRelease(String str) {
        this.feature = str;
    }

    @DexIgnore
    public final void setListener$app_fossilRelease(OnNextTaskListener onNextTaskListener) {
        this.listener = onNextTaskListener;
    }

    @DexIgnore
    public final void setMApiService(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "<set-?>");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public final void setOnNextTaskListener(OnNextTaskListener onNextTaskListener) {
        ee7.b(onNextTaskListener, "listener");
        this.listener = onNextTaskListener;
    }

    @DexIgnore
    public final void setSerialNumber$app_fossilRelease(String str) {
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setZipFilePath$app_fossilRelease(String str) {
        this.zipFilePath = str;
    }
}
