package com.portfolio.platform.cloudimage;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.cloudimage.CloudImageHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$2", f = "AssetUtil.kt", l = {}, m = "invokeSuspend")
public final class AssetUtil$checkWearOSAssetExist$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $fastPairId;
    @DexIgnore
    public /* final */ /* synthetic */ String $filePath1;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageHelper.OnImageCallbackListener $listener;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AssetUtil$checkWearOSAssetExist$Anon2(CloudImageHelper.OnImageCallbackListener onImageCallbackListener, String str, String str2, fb7 fb7) {
        super(2, fb7);
        this.$listener = onImageCallbackListener;
        this.$fastPairId = str;
        this.$filePath1 = str2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        AssetUtil$checkWearOSAssetExist$Anon2 assetUtil$checkWearOSAssetExist$Anon2 = new AssetUtil$checkWearOSAssetExist$Anon2(this.$listener, this.$fastPairId, this.$filePath1, fb7);
        assetUtil$checkWearOSAssetExist$Anon2.p$ = (yi7) obj;
        return assetUtil$checkWearOSAssetExist$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((AssetUtil$checkWearOSAssetExist$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            CloudImageHelper.OnImageCallbackListener onImageCallbackListener = this.$listener;
            if (onImageCallbackListener == null) {
                return null;
            }
            onImageCallbackListener.onImageCallback(this.$fastPairId, this.$filePath1);
            return i97.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
