package com.portfolio.platform.cloudimage;

import android.widget.ImageView;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.tk7;
import com.fossil.vh7;
import com.fossil.x87;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import java.io.File;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$download$1", f = "CloudImageHelper.kt", l = {91, 96}, m = "invokeSuspend")
public final class CloudImageHelper$ItemImage$download$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageHelper.ItemImage this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$download$1$1", f = "CloudImageHelper.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageHelper$ItemImage$download$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(CloudImageHelper$ItemImage$download$Anon1 cloudImageHelper$ItemImage$download$Anon1, fb7 fb7) {
            super(2, fb7);
            this.this$0 = cloudImageHelper$ItemImage$download$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, fb7);
            anon1_Level2.p$ = (yi7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = CloudImageHelper.Companion.getTAG();
                local.d(tag, "withContext, mSerialNumber=" + this.this$0.this$0.mSerialNumber);
                if (this.this$0.this$0.mWeakReferenceImageView != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String tag2 = CloudImageHelper.Companion.getTAG();
                    local2.d(tag2, "download setDefaultImage first, resourceId=" + this.this$0.this$0.mResourceId);
                    WeakReference access$getMWeakReferenceImageView$p = this.this$0.this$0.mWeakReferenceImageView;
                    if (access$getMWeakReferenceImageView$p != null) {
                        ImageView imageView = (ImageView) access$getMWeakReferenceImageView$p.get();
                        if (imageView != null) {
                            Integer access$getMResourceId$p = this.this$0.this$0.mResourceId;
                            if (access$getMResourceId$p != null) {
                                imageView.setImageResource(access$getMResourceId$p.intValue());
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                    } else {
                        throw new x87("null cannot be cast to non-null type java.lang.ref.WeakReference<android.widget.ImageView>");
                    }
                }
                File access$getMFile$p = this.this$0.this$0.mFile;
                if (access$getMFile$p != null) {
                    String access$getMSerialNumber$p = this.this$0.this$0.mSerialNumber;
                    if (access$getMSerialNumber$p != null) {
                        String access$getMSerialPrefix$p = this.this$0.this$0.mSerialPrefix;
                        if (access$getMSerialPrefix$p != null) {
                            CloudImageHelper.this.getMAppExecutors().b().execute(new CloudImageRunnable(access$getMFile$p, access$getMSerialNumber$p, access$getMSerialPrefix$p, ResolutionHelper.INSTANCE.getResolutionFromDevice().getResolution(), Constants.DownloadAssetType.DEVICE, this.this$0.this$0.mDeviceType.getType(), this.this$0.this$0.mListener));
                            return i97.a;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageHelper$ItemImage$download$Anon1(CloudImageHelper.ItemImage itemImage, fb7 fb7) {
        super(2, fb7);
        this.this$0 = itemImage;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        CloudImageHelper$ItemImage$download$Anon1 cloudImageHelper$ItemImage$download$Anon1 = new CloudImageHelper$ItemImage$download$Anon1(this.this$0, fb7);
        cloudImageHelper$ItemImage$download$Anon1.p$ = (yi7) obj;
        return cloudImageHelper$ItemImage$download$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((CloudImageHelper$ItemImage$download$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        yi7 yi7;
        Object obj2;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            if (this.this$0.mFile == null) {
                CloudImageHelper.ItemImage itemImage = this.this$0;
                itemImage.mFile = CloudImageHelper.this.getMApp().getFilesDir();
            }
            AssetUtil assetUtil = AssetUtil.INSTANCE;
            File access$getMFile$p = this.this$0.mFile;
            if (access$getMFile$p != null) {
                String access$getMSerialNumber$p = this.this$0.mSerialNumber;
                if (access$getMSerialNumber$p != null) {
                    String access$getMSerialPrefix$p = this.this$0.mSerialPrefix;
                    if (access$getMSerialPrefix$p != null) {
                        String resolution = ResolutionHelper.INSTANCE.getResolutionFromDevice().getResolution();
                        String feature = Constants.Feature.DEVICE.getFeature();
                        String type = this.this$0.mDeviceType.getType();
                        CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$0.mListener;
                        this.L$0 = yi7;
                        this.label = 1;
                        obj2 = assetUtil.checkAssetExist(access$getMFile$p, access$getMSerialNumber$p, access$getMSerialPrefix$p, resolution, feature, type, access$getMListener$p, this);
                        if (obj2 == a) {
                            return a;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else if (i == 1) {
            t87.a(obj);
            yi7 = (yi7) this.L$0;
            obj2 = obj;
        } else if (i == 2) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            return i97.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (((Boolean) obj2).booleanValue()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = CloudImageHelper.Companion.getTAG();
            local.d(tag, "file is exist, mSerialNumber=" + this.this$0.mSerialNumber);
            return i97.a;
        }
        tk7 c = qj7.c();
        Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, null);
        this.L$0 = yi7;
        this.label = 2;
        if (vh7.a(c, anon1_Level2, this) == a) {
            return a;
        }
        return i97.a;
    }
}
