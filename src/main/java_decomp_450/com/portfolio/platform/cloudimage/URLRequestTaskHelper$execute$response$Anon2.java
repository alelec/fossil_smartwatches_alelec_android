package com.portfolio.platform.cloudimage;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.zb7;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$response$2", f = "URLRequestTaskHelper.kt", l = {62}, m = "invokeSuspend")
public final class URLRequestTaskHelper$execute$response$Anon2 extends zb7 implements gd7<fb7<? super fv7<ApiResponse<ie4>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ URLRequestTaskHelper this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public URLRequestTaskHelper$execute$response$Anon2(URLRequestTaskHelper uRLRequestTaskHelper, fb7 fb7) {
        super(1, fb7);
        this.this$0 = uRLRequestTaskHelper;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(fb7<?> fb7) {
        ee7.b(fb7, "completion");
        return new URLRequestTaskHelper$execute$response$Anon2(this.this$0, fb7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public final Object invoke(fb7<? super fv7<ApiResponse<ie4>>> fb7) {
        return ((URLRequestTaskHelper$execute$response$Anon2) create(fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            ApiServiceV2 mApiService = this.this$0.getMApiService();
            String serialNumber$app_fossilRelease = this.this$0.getSerialNumber$app_fossilRelease();
            if (serialNumber$app_fossilRelease != null) {
                String feature$app_fossilRelease = this.this$0.getFeature$app_fossilRelease();
                if (feature$app_fossilRelease != null) {
                    String access$getResolution$p = this.this$0.resolution;
                    if (access$getResolution$p != null) {
                        this.label = 1;
                        obj = ApiServiceV2.DefaultImpls.getDeviceAssets$default(mApiService, 20, 0, serialNumber$app_fossilRelease, feature$app_fossilRelease, access$getResolution$p, "ANDROID", null, this, 64, null);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else if (i == 1) {
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
