package com.portfolio.platform.manager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import java.nio.charset.StandardCharsets;
import java.util.Random;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"UnknownNullness"})
public class SoLibraryLoader {
    @DexIgnore
    public static SoLibraryLoader b;
    @DexIgnore
    public char[][] a; // = {new char[]{'A', 1, '1', 'D', 'E', 'F'}, new char[]{'F', 2, 'H', 'L', 'K', '~'}, new char[]{'#', 'O', 'P', 'Q', 'R', 'S'}, new char[]{'T', '1', 'V', 'W', ')', 'Y'}, new char[]{'$', 'I', 'D', 'O', '0', 'E'}, new char[]{'X', 'V', '%', '^', 'G', '3'}, new char[]{'a', 'b', 'c', '&', 'e', 'f'}, new char[]{'g', 'h', 'i', 'j', 'l', 'k'}, new char[]{'k', 'm', 'n', 'o', 'p', 'q'}, new char[]{'1', '2', '\b', '4', '5', '6'}, new char[]{'7', '(', '9', '0', '+', '-'}, new char[]{',', '.', '!', '@', '#', '$'}, new char[]{'^', '&', '|', ']', '[', ';'}};

    @DexIgnore
    public String a(String str, String str2) throws Exception {
        String[] split = str.split("]");
        if (split.length == 3) {
            byte[] decode = Base64.decode(split[0], 0);
            byte[] decode2 = Base64.decode(split[1], 0);
            byte[] decode3 = Base64.decode(split[2], 0);
            SecretKeySpec secretKeySpec = new SecretKeySpec(SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1", "BC").generateSecret(new PBEKeySpec(str2.toCharArray(), decode, 1000, 128)).getEncoded(), "AES");
            Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
            instance.init(2, secretKeySpec, new GCMParameterSpec(128, decode2));
            return new String(instance.doFinal(decode3), StandardCharsets.UTF_8);
        }
        throw new IllegalArgumentException("Invalid encypted text format");
    }

    @DexIgnore
    public native byte[] find(Context context, int i);

    @DexIgnore
    public native byte[] getData(int i, int i2);

    @DexIgnore
    public String a(String str, int i) {
        char[][] cArr = this.a;
        char[] cArr2 = {cArr[1][5], cArr[0][1], cArr[1][1], cArr[2][0], cArr[4][0], cArr[5][2], cArr[5][3], cArr[6][3], cArr[9][2], cArr[10][1], cArr[3][4], 0};
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < str.length(); i2++) {
            sb.append((char) (str.charAt(i2) ^ cArr2[(i2 + i) % 12]));
        }
        return sb.toString();
    }

    @DexIgnore
    public static SoLibraryLoader a() {
        if (b == null) {
            System.loadLibrary("res-c");
            synchronized (SoLibraryLoader.class) {
                if (b == null) {
                    b = new SoLibraryLoader();
                }
            }
        }
        return b;
    }

    @DexIgnore
    public boolean a(int i) {
        int i2 = 3;
        if (i > 3 && i % 2 != 0) {
            while (((double) i2) <= Math.sqrt((double) i) && i % i2 != 0) {
                i2 += 2;
            }
            if (i % i2 != 0) {
                return true;
            }
            return false;
        } else if (i == 2 || i == 3) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public Boolean a(Context context, int i) throws Exception {
        Random random = new Random();
        int nextInt = random.nextInt(1000);
        while (true) {
            int i2 = nextInt + 1;
            if (!a(i2)) {
                nextInt = random.nextInt(1000);
            } else {
                String str = new String(getData(i2, i), StandardCharsets.UTF_8);
                return Boolean.valueOf(!TextUtils.isEmpty(a(a(str, i2), a(new String(find(context, i2), StandardCharsets.UTF_8), i2).trim())));
            }
        }
    }
}
