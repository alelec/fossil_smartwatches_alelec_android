package com.portfolio.platform;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.databinding.ViewDataBinding;
import com.fossil.cl5;
import com.fossil.cw4;
import com.fossil.ee7;
import com.fossil.mh7;
import com.fossil.qb;
import com.fossil.t6;
import com.fossil.x87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"Registered"})
public class BaseWebViewActivity extends cl5 {
    @DexIgnore
    public static /* final */ String A;
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public cw4 y;
    @DexIgnore
    public String z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return BaseWebViewActivity.A;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            ee7.b(context, "context");
            ee7.b(str, "title");
            ee7.b(str2, "url");
            Intent intent = new Intent(context, BaseWebViewActivity.class);
            intent.putExtra("urlToLoad", str2);
            intent.putExtra("title", str);
            context.startActivity(intent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends WebViewClient {
        @DexIgnore
        public /* final */ /* synthetic */ BaseWebViewActivity a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(BaseWebViewActivity baseWebViewActivity) {
            this.a = baseWebViewActivity;
        }

        @DexIgnore
        @Override // android.webkit.WebViewClient
        @TargetApi(21)
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = BaseWebViewActivity.B.a();
            StringBuilder sb = new StringBuilder();
            sb.append("Should override ");
            Uri uri = null;
            sb.append(webResourceRequest != null ? webResourceRequest.getUrl() : null);
            local.d(a2, sb.toString());
            if (!mh7.c(String.valueOf(webResourceRequest != null ? webResourceRequest.getUrl() : null), "mailto", false, 2, null)) {
                return super.shouldOverrideUrlLoading(webView, webResourceRequest);
            }
            FLogger.INSTANCE.getLocal().d(BaseWebViewActivity.B.a(), "We are overriding the mailto urlToLoad");
            if (webResourceRequest != null) {
                uri = webResourceRequest.getUrl();
            }
            String valueOf = String.valueOf(uri);
            if (valueOf != null) {
                String substring = valueOf.substring(7);
                ee7.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                t6 a3 = t6.a(this.a);
                ee7.a((Object) a3, "ShareCompat.IntentBuilde\u2026this@BaseWebViewActivity)");
                a3.b("message/rfc822");
                a3.a(substring);
                a3.c();
                return true;
            }
            throw new x87("null cannot be cast to non-null type java.lang.String");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ BaseWebViewActivity a;

        @DexIgnore
        public c(BaseWebViewActivity baseWebViewActivity) {
            this.a = baseWebViewActivity;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.finish();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends WebChromeClient {
        @DexIgnore
        public /* final */ /* synthetic */ BaseWebViewActivity a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(BaseWebViewActivity baseWebViewActivity) {
            this.a = baseWebViewActivity;
        }

        @DexIgnore
        public void onProgressChanged(WebView webView, int i) {
            ee7.b(webView, "view");
            if (i == 100) {
                this.a.g();
            }
        }
    }

    /*
    static {
        String simpleName = BaseWebViewActivity.class.getSimpleName();
        ee7.a((Object) simpleName, "BaseWebViewActivity::class.java.simpleName");
        A = simpleName;
    }
    */

    @DexIgnore
    public void a(cw4 cw4) {
        ee7.b(cw4, "<set-?>");
        this.y = cw4;
    }

    @DexIgnore
    public void c(String str) {
        ee7.b(str, "<set-?>");
        this.z = str;
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        if (getIntent().getStringExtra("urlToLoad") == null) {
            str = "";
        } else {
            str = getIntent().getStringExtra("urlToLoad");
            ee7.a((Object) str, "intent.getStringExtra(KEY_URL)");
        }
        c(str);
        ViewDataBinding a2 = qb.a(this, 2131558438);
        ee7.a((Object) a2, "DataBindingUtil.setConte\u2026.layout.activity_webview)");
        a((cw4) a2);
        FlexibleTextView flexibleTextView = s().r;
        ee7.a((Object) flexibleTextView, "binding.ftvTitle");
        flexibleTextView.setText(getIntent().getStringExtra("title"));
        s().q.setOnClickListener(new c(this));
        cl5.a(this, true, null, 2, null);
        r();
        u();
    }

    @DexIgnore
    public WebViewClient q() {
        FLogger.INSTANCE.getLocal().d(A, "Building default web client");
        return new b(this);
    }

    @DexIgnore
    public final void r() {
        if (Build.VERSION.SDK_INT >= 22) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
            return;
        }
        CookieSyncManager createInstance = CookieSyncManager.createInstance(getBaseContext());
        createInstance.startSync();
        CookieManager instance = CookieManager.getInstance();
        instance.removeAllCookie();
        instance.removeSessionCookie();
        createInstance.stopSync();
        createInstance.sync();
    }

    @DexIgnore
    public cw4 s() {
        cw4 cw4 = this.y;
        if (cw4 != null) {
            return cw4;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public String t() {
        String str = this.z;
        if (str != null) {
            return str;
        }
        ee7.d("urlToLoad");
        throw null;
    }

    @DexIgnore
    @SuppressLint({"SetJavaScriptEnabled"})
    public void u() {
        WebView webView = s().s;
        ee7.a((Object) webView, "binding.webView");
        WebSettings settings = webView.getSettings();
        ee7.a((Object) settings, "ws");
        settings.setSaveFormData(false);
        settings.setSavePassword(false);
        s().s.clearCache(true);
        WebView webView2 = s().s;
        ee7.a((Object) webView2, "binding.webView");
        webView2.setWebViewClient(q());
        WebView webView3 = s().s;
        ee7.a((Object) webView3, "binding.webView");
        WebSettings settings2 = webView3.getSettings();
        ee7.a((Object) settings2, Constants.USER_SETTING);
        settings2.setJavaScriptEnabled(true);
        settings2.setDomStorageEnabled(true);
        settings2.setAllowFileAccess(true);
        settings2.setAllowFileAccessFromFileURLs(true);
        settings2.setAllowUniversalAccessFromFileURLs(true);
        settings2.setUserAgentString("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36");
        WebView webView4 = s().s;
        ee7.a((Object) webView4, "binding.webView");
        webView4.setWebChromeClient(new d(this));
        if (PortfolioApp.g0.e() && TextUtils.isEmpty(t())) {
            FLogger.INSTANCE.getLocal().e(A, "You must create a urlToLoad to load before the webview is created");
        }
        s().s.loadUrl(t());
    }
}
