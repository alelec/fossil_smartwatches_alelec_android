package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LocaleChangedReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String a; // = LocaleChangedReceiver.class.getSimpleName();

    @DexIgnore
    public LocaleChangedReceiver() {
        PortfolioApp.c0.f().a(this);
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        FLogger.INSTANCE.getLocal().d(a, "Inside .onLocaledChangedReceiver, startReloadData");
    }
}
