package com.portfolio.platform.ui.view.chart.overview;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.pl4;
import com.fossil.tw6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OverviewSleepDaySummary extends View {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float p;
    @DexIgnore
    public RectF q;
    @DexIgnore
    public RectF r;
    @DexIgnore
    public RectF s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public RectF u;
    @DexIgnore
    public /* final */ PorterDuffXfermode v;
    @DexIgnore
    public /* final */ PorterDuffXfermode w;

    @DexIgnore
    public OverviewSleepDaySummary(Context context) {
        this(context, null);
    }

    @DexIgnore
    public final void a(float f2, float f3, float f4, int i2, int i3, int i4) {
        this.d = f2;
        this.e = f3;
        this.f = f4;
        this.g = i2;
        this.h = i3;
        this.i = i4;
        invalidate();
    }

    @DexIgnore
    public final String getTAG() {
        return this.a;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (canvas != null) {
            this.t.setColor(0);
            this.t.setXfermode(this.v);
            RectF rectF = this.u;
            float f2 = rectF.left;
            float f3 = rectF.top;
            float f4 = rectF.right;
            float f5 = rectF.bottom;
            float f6 = this.j;
            tw6.a(canvas, f2, f3, f4, f5, f6, f6, true, true, true, true, this.t);
            a(canvas, this.q, this.g);
            a(canvas, this.r, this.h);
            a(canvas, this.s, this.i);
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int size = View.MeasureSpec.getSize(i2);
        int size2 = View.MeasureSpec.getSize(i3);
        setMeasuredDimension(size, size2);
        float f2 = (float) size;
        float f3 = this.p;
        float f4 = f2 - (((float) 2) * f3);
        this.b = this.d * f4;
        this.c = this.e * f4;
        float f5 = (float) size2;
        this.u.set(f3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2 - f3, f5);
        RectF rectF = this.q;
        float f6 = this.p;
        rectF.set(f6, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.b + f6, f5);
        RectF rectF2 = this.r;
        float f7 = this.p;
        float f8 = this.b;
        rectF2.set(f7 + f8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f7 + f8 + this.c, f5);
        RectF rectF3 = this.s;
        float f9 = this.p;
        rectF3.set(this.b + f9 + this.c, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2 - f9, f5);
    }

    @DexIgnore
    public OverviewSleepDaySummary(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewSleepDaySummary(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, 0);
    }

    @DexIgnore
    public OverviewSleepDaySummary(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        Resources.Theme theme;
        TypedArray obtainStyledAttributes;
        this.a = "OverviewSleepDaySummary";
        this.q = new RectF();
        this.r = new RectF();
        this.s = new RectF();
        this.t = new Paint(1);
        this.u = new RectF();
        this.v = new PorterDuffXfermode(PorterDuff.Mode.CLEAR);
        this.w = new PorterDuffXfermode(PorterDuff.Mode.DST_OVER);
        if (!(attributeSet == null || context == null || (theme = context.getTheme()) == null || (obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, pl4.OverviewSleepDaySummary, 0, 0)) == null)) {
            try {
                this.g = obtainStyledAttributes.getColor(3, 0);
                this.h = obtainStyledAttributes.getColor(4, 0);
                this.i = obtainStyledAttributes.getColor(0, 0);
                this.j = (float) obtainStyledAttributes.getDimensionPixelSize(2, 5);
                this.p = (float) obtainStyledAttributes.getDimensionPixelSize(1, 30);
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.a;
                local.d(str, "constructor - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        a();
    }

    @DexIgnore
    public final void a() {
        this.t.setAntiAlias(true);
        this.t.setStyle(Paint.Style.FILL);
    }

    @DexIgnore
    public final void a(Canvas canvas, RectF rectF, int i2) {
        this.t.setColor(i2);
        this.t.setXfermode(this.w);
        canvas.drawRect(rectF, this.t);
    }
}
