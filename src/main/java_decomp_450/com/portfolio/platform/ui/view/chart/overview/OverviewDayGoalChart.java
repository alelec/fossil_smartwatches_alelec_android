package com.portfolio.platform.ui.view.chart.overview;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import com.fossil.bb7;
import com.fossil.ea7;
import com.fossil.ee7;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OverviewDayGoalChart extends OverviewDayChart {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(t.c(), t2.c());
        }
    }

    @DexIgnore
    public OverviewDayGoalChart(Context context) {
        this(context, null);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v9 */
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart, com.portfolio.platform.ui.view.chart.overview.OverviewDayChart
    public void e(Canvas canvas) {
        BarChart.b bVar;
        ee7.b(canvas, "canvas");
        getMGraphPaint().setColor(getMDefaultColor());
        Iterator<BarChart.a> it = getMChartModel().a().iterator();
        while (it.hasNext()) {
            ArrayList<BarChart.b> arrayList = it.next().c().get(0);
            ee7.a((Object) arrayList, "item.mListOfBarPoints[0]");
            Iterator it2 = ea7.a((Iterable) arrayList, (Comparator) new a()).iterator();
            if (!it2.hasNext()) {
                bVar = null;
            } else {
                Object next = it2.next();
                if (!it2.hasNext()) {
                    bVar = next;
                } else {
                    int e = ((BarChart.b) next).e();
                    do {
                        Object next2 = it2.next();
                        int e2 = ((BarChart.b) next2).e();
                        if (e < e2) {
                            next = next2;
                            e = e2;
                        }
                    } while (it2.hasNext());
                }
                bVar = next;
            }
            BarChart.b bVar2 = bVar;
            if (bVar2 != null) {
                canvas.drawRoundRect(bVar2.a(), getMBarRadius(), getMBarRadius(), getMGraphPaint());
            }
        }
    }

    @DexIgnore
    public OverviewDayGoalChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewDayGoalChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewDayGoalChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }
}
