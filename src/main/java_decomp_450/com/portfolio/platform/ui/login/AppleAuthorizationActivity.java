package com.portfolio.platform.ui.login;

import android.content.Intent;
import android.webkit.WebViewClient;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ee7;
import com.fossil.zd7;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.BaseWebViewActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppleAuthorizationActivity extends BaseWebViewActivity {
    @DexIgnore
    public static /* final */ a C; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(FragmentActivity fragmentActivity, String str) {
            ee7.b(fragmentActivity, Constants.ACTIVITY);
            ee7.b(str, "authorizationUrl");
            Intent intent = new Intent(fragmentActivity, AppleAuthorizationActivity.class);
            intent.putExtra("urlToLoad", str);
            fragmentActivity.startActivityForResult(intent, 3535);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends WebViewClient {
        @DexIgnore
        public /* final */ /* synthetic */ AppleAuthorizationActivity a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(AppleAuthorizationActivity appleAuthorizationActivity) {
            this.a = appleAuthorizationActivity;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0040 A[Catch:{ Exception -> 0x00c7 }] */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00b6 A[Catch:{ Exception -> 0x00c7 }] */
        @Override // android.webkit.WebViewClient
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean shouldOverrideUrlLoading(android.webkit.WebView r7, android.webkit.WebResourceRequest r8) {
            /*
                r6 = this;
                r0 = 0
                r1 = 1
                if (r8 == 0) goto L_0x00ed
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x00c7 }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ Exception -> 0x00c7 }
                com.portfolio.platform.ui.login.AppleAuthorizationActivity r3 = r6.a     // Catch:{ Exception -> 0x00c7 }
                java.lang.String r3 = r3.e()     // Catch:{ Exception -> 0x00c7 }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c7 }
                r4.<init>()     // Catch:{ Exception -> 0x00c7 }
                java.lang.String r5 = "request = "
                r4.append(r5)     // Catch:{ Exception -> 0x00c7 }
                android.net.Uri r5 = r8.getUrl()     // Catch:{ Exception -> 0x00c7 }
                r4.append(r5)     // Catch:{ Exception -> 0x00c7 }
                java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00c7 }
                r2.d(r3, r4)     // Catch:{ Exception -> 0x00c7 }
                android.net.Uri r2 = r8.getUrl()     // Catch:{ Exception -> 0x00c7 }
                java.lang.String r3 = "id_token"
                java.lang.String r2 = r2.getQueryParameter(r3)     // Catch:{ Exception -> 0x00c7 }
                if (r2 == 0) goto L_0x003d
                boolean r3 = com.fossil.mh7.a(r2)     // Catch:{ Exception -> 0x00c7 }
                if (r3 == 0) goto L_0x003b
                goto L_0x003d
            L_0x003b:
                r3 = 0
                goto L_0x003e
            L_0x003d:
                r3 = 1
            L_0x003e:
                if (r3 != 0) goto L_0x00b6
                com.portfolio.platform.ui.login.AppleAuthorizationActivity r3 = r6.a     // Catch:{ Exception -> 0x00c7 }
                android.content.Intent r3 = r3.getIntent()     // Catch:{ Exception -> 0x00c7 }
                if (r3 != 0) goto L_0x0052
                com.portfolio.platform.ui.login.AppleAuthorizationActivity r3 = r6.a     // Catch:{ Exception -> 0x00c7 }
                android.content.Intent r4 = new android.content.Intent     // Catch:{ Exception -> 0x00c7 }
                r4.<init>()     // Catch:{ Exception -> 0x00c7 }
                r3.setIntent(r4)     // Catch:{ Exception -> 0x00c7 }
            L_0x0052:
                com.portfolio.platform.data.SignUpSocialAuth r3 = new com.portfolio.platform.data.SignUpSocialAuth     // Catch:{ Exception -> 0x00c7 }
                r3.<init>()     // Catch:{ Exception -> 0x00c7 }
                java.lang.String r4 = "apple"
                r3.setService(r4)     // Catch:{ Exception -> 0x00c7 }
                r3.setToken(r2)     // Catch:{ Exception -> 0x00c7 }
                android.net.Uri r2 = r8.getUrl()     // Catch:{ Exception -> 0x00c7 }
                java.lang.String r4 = "user"
                java.lang.String r2 = r2.getQueryParameter(r4)     // Catch:{ Exception -> 0x00c7 }
                if (r2 == 0) goto L_0x0071
                boolean r4 = com.fossil.mh7.a(r2)     // Catch:{ Exception -> 0x00c7 }
                if (r4 == 0) goto L_0x0072
            L_0x0071:
                r0 = 1
            L_0x0072:
                if (r0 != 0) goto L_0x009e
                com.google.gson.Gson r0 = new com.google.gson.Gson     // Catch:{ Exception -> 0x00c7 }
                r0.<init>()     // Catch:{ Exception -> 0x00c7 }
                java.lang.Class<com.portfolio.platform.data.AppleAuth> r4 = com.portfolio.platform.data.AppleAuth.class
                java.lang.Object r0 = r0.a(r2, r4)     // Catch:{ Exception -> 0x00c7 }
                com.portfolio.platform.data.AppleAuth r0 = (com.portfolio.platform.data.AppleAuth) r0     // Catch:{ Exception -> 0x00c7 }
                java.lang.String r2 = r0.getEmail()     // Catch:{ Exception -> 0x00c7 }
                r3.setEmail(r2)     // Catch:{ Exception -> 0x00c7 }
                com.portfolio.platform.data.AppleAuth$Name r2 = r0.getName()     // Catch:{ Exception -> 0x00c7 }
                java.lang.String r2 = r2.getLastName()     // Catch:{ Exception -> 0x00c7 }
                r3.setLastName(r2)     // Catch:{ Exception -> 0x00c7 }
                com.portfolio.platform.data.AppleAuth$Name r0 = r0.getName()     // Catch:{ Exception -> 0x00c7 }
                java.lang.String r0 = r0.getFirstName()     // Catch:{ Exception -> 0x00c7 }
                r3.setFirstName(r0)     // Catch:{ Exception -> 0x00c7 }
            L_0x009e:
                com.portfolio.platform.ui.login.AppleAuthorizationActivity r0 = r6.a     // Catch:{ Exception -> 0x00c7 }
                android.content.Intent r0 = r0.getIntent()     // Catch:{ Exception -> 0x00c7 }
                java.lang.String r2 = "USER_INFO_EXTRA"
                r0.putExtra(r2, r3)     // Catch:{ Exception -> 0x00c7 }
                com.portfolio.platform.ui.login.AppleAuthorizationActivity r0 = r6.a     // Catch:{ Exception -> 0x00c7 }
                r2 = -1
                com.portfolio.platform.ui.login.AppleAuthorizationActivity r3 = r6.a     // Catch:{ Exception -> 0x00c7 }
                android.content.Intent r3 = r3.getIntent()     // Catch:{ Exception -> 0x00c7 }
                r0.setResult(r2, r3)     // Catch:{ Exception -> 0x00c7 }
                goto L_0x00ec
            L_0x00b6:
                android.net.Uri r0 = r8.getUrl()     // Catch:{ Exception -> 0x00c7 }
                java.lang.String r2 = "error"
                java.lang.String r0 = r0.getQueryParameter(r2)     // Catch:{ Exception -> 0x00c7 }
                java.lang.String r2 = "user_cancelled_authorize"
                boolean r0 = com.fossil.ee7.a(r0, r2)     // Catch:{ Exception -> 0x00c7 }
                goto L_0x00ed
            L_0x00c7:
                r0 = move-exception
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                com.portfolio.platform.ui.login.AppleAuthorizationActivity r3 = r6.a
                java.lang.String r3 = r3.e()
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "Get authorization info with error: "
                r4.append(r5)
                java.lang.String r0 = r0.getMessage()
                r4.append(r0)
                java.lang.String r0 = r4.toString()
                r2.d(r3, r0)
            L_0x00ec:
                r0 = 1
            L_0x00ed:
                if (r0 == 0) goto L_0x00f5
                com.portfolio.platform.ui.login.AppleAuthorizationActivity r7 = r6.a
                r7.finish()
                goto L_0x00f9
            L_0x00f5:
                boolean r1 = super.shouldOverrideUrlLoading(r7, r8)
            L_0x00f9:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.login.AppleAuthorizationActivity.b.shouldOverrideUrlLoading(android.webkit.WebView, android.webkit.WebResourceRequest):boolean");
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.BaseWebViewActivity
    public WebViewClient q() {
        return new b(this);
    }
}
