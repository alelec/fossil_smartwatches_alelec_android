package com.fossil;

import com.fossil.pf;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jf<Key, Value> extends lf<Key, Value> {
    @DexIgnore
    public abstract void dispatchLoadAfter(int i, Value value, int i2, Executor executor, pf.a<Value> aVar);

    @DexIgnore
    public abstract void dispatchLoadBefore(int i, Value value, int i2, Executor executor, pf.a<Value> aVar);

    @DexIgnore
    public abstract void dispatchLoadInitial(Key key, int i, int i2, boolean z, Executor executor, pf.a<Value> aVar);

    @DexIgnore
    public abstract Key getKey(int i, Value value);

    @DexIgnore
    @Override // com.fossil.lf
    public boolean isContiguous() {
        return true;
    }

    @DexIgnore
    public boolean supportsPageDropping() {
        return true;
    }
}
