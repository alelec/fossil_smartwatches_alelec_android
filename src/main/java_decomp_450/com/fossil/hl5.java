package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hl5 {
    @DexIgnore
    public Object a;
    @DexIgnore
    public String b;

    @DexIgnore
    public hl5(String str) {
        ee7.b(str, "tagName");
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final Object b() {
        return this.a;
    }

    @DexIgnore
    public final void a(Object obj) {
        this.a = obj;
    }
}
