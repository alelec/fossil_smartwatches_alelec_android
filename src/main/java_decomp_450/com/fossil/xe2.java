package com.fossil;

import android.os.Binder;
import android.os.Process;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xe2 extends Binder {
    @DexIgnore
    public /* final */ te2 a;

    @DexIgnore
    public xe2(te2 te2) {
        this.a = te2;
    }

    @DexIgnore
    public final void a(ve2 ve2) {
        if (Binder.getCallingUid() == Process.myUid()) {
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "service received new intent via bind strategy");
            }
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "intent being queued for bg execution");
            }
            this.a.a.execute(new ye2(this, ve2));
            return;
        }
        throw new SecurityException("Binding only allowed within app");
    }
}
