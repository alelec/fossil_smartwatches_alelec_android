package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ka3 implements Parcelable.Creator<GoogleMapOptions> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ GoogleMapOptions createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        CameraPosition cameraPosition = null;
        Float f = null;
        Float f2 = null;
        LatLngBounds latLngBounds = null;
        byte b2 = -1;
        byte b3 = -1;
        int i = 0;
        byte b4 = -1;
        byte b5 = -1;
        byte b6 = -1;
        byte b7 = -1;
        byte b8 = -1;
        byte b9 = -1;
        byte b10 = -1;
        byte b11 = -1;
        byte b12 = -1;
        byte b13 = -1;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 2:
                    b2 = j72.k(parcel, a);
                    break;
                case 3:
                    b3 = j72.k(parcel, a);
                    break;
                case 4:
                    i = j72.q(parcel, a);
                    break;
                case 5:
                    cameraPosition = (CameraPosition) j72.a(parcel, a, CameraPosition.CREATOR);
                    break;
                case 6:
                    b4 = j72.k(parcel, a);
                    break;
                case 7:
                    b5 = j72.k(parcel, a);
                    break;
                case 8:
                    b6 = j72.k(parcel, a);
                    break;
                case 9:
                    b7 = j72.k(parcel, a);
                    break;
                case 10:
                    b8 = j72.k(parcel, a);
                    break;
                case 11:
                    b9 = j72.k(parcel, a);
                    break;
                case 12:
                    b10 = j72.k(parcel, a);
                    break;
                case 13:
                default:
                    j72.v(parcel, a);
                    break;
                case 14:
                    b11 = j72.k(parcel, a);
                    break;
                case 15:
                    b12 = j72.k(parcel, a);
                    break;
                case 16:
                    f = j72.o(parcel, a);
                    break;
                case 17:
                    f2 = j72.o(parcel, a);
                    break;
                case 18:
                    latLngBounds = (LatLngBounds) j72.a(parcel, a, LatLngBounds.CREATOR);
                    break;
                case 19:
                    b13 = j72.k(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new GoogleMapOptions(b2, b3, i, cameraPosition, b4, b5, b6, b7, b8, b9, b10, b11, b12, f, f2, latLngBounds, b13);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ GoogleMapOptions[] newArray(int i) {
        return new GoogleMapOptions[i];
    }
}
