package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f2 extends DataSetObservable {
    @DexIgnore
    public static /* final */ String n; // = f2.class.getSimpleName();
    @DexIgnore
    public static /* final */ Object o; // = new Object();
    @DexIgnore
    public static /* final */ Map<String, f2> p; // = new HashMap();
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ List<a> b; // = new ArrayList();
    @DexIgnore
    public /* final */ List<d> c; // = new ArrayList();
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public Intent f;
    @DexIgnore
    public b g; // = new c();
    @DexIgnore
    public int h; // = 50;
    @DexIgnore
    public boolean i; // = true;
    @DexIgnore
    public boolean j; // = false;
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public boolean l; // = false;
    @DexIgnore
    public e m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Comparable<a> {
        @DexIgnore
        public /* final */ ResolveInfo a;
        @DexIgnore
        public float b;

        @DexIgnore
        public a(ResolveInfo resolveInfo) {
            this.a = resolveInfo;
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(a aVar) {
            return Float.floatToIntBits(aVar.b) - Float.floatToIntBits(this.b);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return obj != null && a.class == obj.getClass() && Float.floatToIntBits(this.b) == Float.floatToIntBits(((a) obj).b);
        }

        @DexIgnore
        public int hashCode() {
            return Float.floatToIntBits(this.b) + 31;
        }

        @DexIgnore
        public String toString() {
            return "[" + "resolveInfo:" + this.a.toString() + "; weight:" + new BigDecimal((double) this.b) + "]";
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Intent intent, List<a> list, List<d> list2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements b {
        @DexIgnore
        public /* final */ Map<ComponentName, a> a; // = new HashMap();

        @DexIgnore
        @Override // com.fossil.f2.b
        public void a(Intent intent, List<a> list, List<d> list2) {
            Map<ComponentName, a> map = this.a;
            map.clear();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                a aVar = list.get(i);
                aVar.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                ActivityInfo activityInfo = aVar.a.activityInfo;
                map.put(new ComponentName(activityInfo.packageName, activityInfo.name), aVar);
            }
            float f = 1.0f;
            for (int size2 = list2.size() - 1; size2 >= 0; size2--) {
                d dVar = list2.get(size2);
                a aVar2 = map.get(dVar.a);
                if (aVar2 != null) {
                    aVar2.b += dVar.c * f;
                    f *= 0.95f;
                }
            }
            Collections.sort(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* final */ ComponentName a;
        @DexIgnore
        public /* final */ long b;
        @DexIgnore
        public /* final */ float c;

        @DexIgnore
        public d(String str, long j, float f) {
            this(ComponentName.unflattenFromString(str), j, f);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || d.class != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            ComponentName componentName = this.a;
            if (componentName == null) {
                if (dVar.a != null) {
                    return false;
                }
            } else if (!componentName.equals(dVar.a)) {
                return false;
            }
            return this.b == dVar.b && Float.floatToIntBits(this.c) == Float.floatToIntBits(dVar.c);
        }

        @DexIgnore
        public int hashCode() {
            ComponentName componentName = this.a;
            int hashCode = componentName == null ? 0 : componentName.hashCode();
            long j = this.b;
            return ((((hashCode + 31) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + Float.floatToIntBits(this.c);
        }

        @DexIgnore
        public String toString() {
            return "[" + "; activity:" + this.a + "; time:" + this.b + "; weight:" + new BigDecimal((double) this.c) + "]";
        }

        @DexIgnore
        public d(ComponentName componentName, long j, float f) {
            this.a = componentName;
            this.b = j;
            this.c = f;
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        boolean a(f2 f2Var, Intent intent);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f extends AsyncTask<Object, Void, Void> {
        @DexIgnore
        public f() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x006d, code lost:
            if (r15 != null) goto L_0x006f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
            r15.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0092, code lost:
            if (r15 == null) goto L_0x00d5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x00b2, code lost:
            if (r15 == null) goto L_0x00d5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x00d2, code lost:
            if (r15 == null) goto L_0x00d5;
         */
        @DexIgnore
        @Override // android.os.AsyncTask
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Void doInBackground(java.lang.Object... r15) {
            /*
                r14 = this;
                java.lang.String r0 = "historical-record"
                java.lang.String r1 = "historical-records"
                java.lang.String r2 = "Error writing historical record file: "
                r3 = 0
                r4 = r15[r3]
                java.util.List r4 = (java.util.List) r4
                r5 = 1
                r15 = r15[r5]
                java.lang.String r15 = (java.lang.String) r15
                r6 = 0
                com.fossil.f2 r7 = com.fossil.f2.this     // Catch:{ FileNotFoundException -> 0x00e0 }
                android.content.Context r7 = r7.d     // Catch:{ FileNotFoundException -> 0x00e0 }
                java.io.FileOutputStream r15 = r7.openFileOutput(r15, r3)     // Catch:{ FileNotFoundException -> 0x00e0 }
                org.xmlpull.v1.XmlSerializer r7 = android.util.Xml.newSerializer()
                r7.setOutput(r15, r6)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                java.lang.String r8 = "UTF-8"
                java.lang.Boolean r9 = java.lang.Boolean.valueOf(r5)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.startDocument(r8, r9)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.startTag(r6, r1)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                int r8 = r4.size()     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r9 = 0
            L_0x0031:
                if (r9 >= r8) goto L_0x0063
                java.lang.Object r10 = r4.remove(r3)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                com.fossil.f2$d r10 = (com.fossil.f2.d) r10     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.startTag(r6, r0)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                java.lang.String r11 = "activity"
                android.content.ComponentName r12 = r10.a     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                java.lang.String r12 = r12.flattenToString()     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.attribute(r6, r11, r12)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                java.lang.String r11 = "time"
                long r12 = r10.b     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                java.lang.String r12 = java.lang.String.valueOf(r12)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.attribute(r6, r11, r12)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                java.lang.String r11 = "weight"
                float r10 = r10.c     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.attribute(r6, r11, r10)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.endTag(r6, r0)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                int r9 = r9 + 1
                goto L_0x0031
            L_0x0063:
                r7.endTag(r6, r1)     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                r7.endDocument()     // Catch:{ IllegalArgumentException -> 0x00b5, IllegalStateException -> 0x0095, IOException -> 0x0075 }
                com.fossil.f2 r0 = com.fossil.f2.this
                r0.i = r5
                if (r15 == 0) goto L_0x00d5
            L_0x006f:
                r15.close()     // Catch:{ IOException -> 0x00d5 }
                goto L_0x00d5
            L_0x0073:
                r0 = move-exception
                goto L_0x00d6
            L_0x0075:
                r0 = move-exception
                java.lang.String r1 = com.fossil.f2.n     // Catch:{ all -> 0x0073 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0073 }
                r3.<init>()     // Catch:{ all -> 0x0073 }
                r3.append(r2)     // Catch:{ all -> 0x0073 }
                com.fossil.f2 r2 = com.fossil.f2.this     // Catch:{ all -> 0x0073 }
                java.lang.String r2 = r2.e     // Catch:{ all -> 0x0073 }
                r3.append(r2)     // Catch:{ all -> 0x0073 }
                java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x0073 }
                android.util.Log.e(r1, r2, r0)     // Catch:{ all -> 0x0073 }
                com.fossil.f2 r0 = com.fossil.f2.this
                r0.i = r5
                if (r15 == 0) goto L_0x00d5
                goto L_0x006f
            L_0x0095:
                r0 = move-exception
                java.lang.String r1 = com.fossil.f2.n
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                r3.append(r2)
                com.fossil.f2 r2 = com.fossil.f2.this
                java.lang.String r2 = r2.e
                r3.append(r2)
                java.lang.String r2 = r3.toString()
                android.util.Log.e(r1, r2, r0)
                com.fossil.f2 r0 = com.fossil.f2.this
                r0.i = r5
                if (r15 == 0) goto L_0x00d5
                goto L_0x006f
            L_0x00b5:
                r0 = move-exception
                java.lang.String r1 = com.fossil.f2.n
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                r3.append(r2)
                com.fossil.f2 r2 = com.fossil.f2.this
                java.lang.String r2 = r2.e
                r3.append(r2)
                java.lang.String r2 = r3.toString()
                android.util.Log.e(r1, r2, r0)
                com.fossil.f2 r0 = com.fossil.f2.this
                r0.i = r5
                if (r15 == 0) goto L_0x00d5
                goto L_0x006f
            L_0x00d5:
                return r6
            L_0x00d6:
                com.fossil.f2 r1 = com.fossil.f2.this
                r1.i = r5
                if (r15 == 0) goto L_0x00df
                r15.close()     // Catch:{ IOException -> 0x00df }
            L_0x00df:
                throw r0
            L_0x00e0:
                r0 = move-exception
                java.lang.String r1 = com.fossil.f2.n
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                r3.append(r2)
                r3.append(r15)
                java.lang.String r15 = r3.toString()
                android.util.Log.e(r1, r15, r0)
                return r6
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.f2.f.doInBackground(java.lang.Object[]):java.lang.Void");
        }
    }

    @DexIgnore
    public f2(Context context, String str) {
        this.d = context.getApplicationContext();
        if (TextUtils.isEmpty(str) || str.endsWith(".xml")) {
            this.e = str;
            return;
        }
        this.e = str + ".xml";
    }

    @DexIgnore
    public static f2 a(Context context, String str) {
        f2 f2Var;
        synchronized (o) {
            f2Var = p.get(str);
            if (f2Var == null) {
                f2Var = new f2(context, str);
                p.put(str, f2Var);
            }
        }
        return f2Var;
    }

    @DexIgnore
    public int b() {
        int size;
        synchronized (this.a) {
            a();
            size = this.b.size();
        }
        return size;
    }

    @DexIgnore
    public ResolveInfo c() {
        synchronized (this.a) {
            a();
            if (this.b.isEmpty()) {
                return null;
            }
            return this.b.get(0).a;
        }
    }

    @DexIgnore
    public int d() {
        int size;
        synchronized (this.a) {
            a();
            size = this.c.size();
        }
        return size;
    }

    @DexIgnore
    public final boolean e() {
        if (!this.l || this.f == null) {
            return false;
        }
        this.l = false;
        this.b.clear();
        List<ResolveInfo> queryIntentActivities = this.d.getPackageManager().queryIntentActivities(this.f, 0);
        int size = queryIntentActivities.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.b.add(new a(queryIntentActivities.get(i2)));
        }
        return true;
    }

    @DexIgnore
    public final void f() {
        if (!this.j) {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        } else if (this.k) {
            this.k = false;
            if (!TextUtils.isEmpty(this.e)) {
                new f().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new ArrayList(this.c), this.e);
            }
        }
    }

    @DexIgnore
    public final void g() {
        int size = this.c.size() - this.h;
        if (size > 0) {
            this.k = true;
            for (int i2 = 0; i2 < size; i2++) {
                this.c.remove(0);
            }
        }
    }

    @DexIgnore
    public final boolean h() {
        if (!this.i || !this.k || TextUtils.isEmpty(this.e)) {
            return false;
        }
        this.i = false;
        this.j = true;
        i();
        return true;
    }

    @DexIgnore
    public final void i() {
        try {
            FileInputStream openFileInput = this.d.openFileInput(this.e);
            try {
                XmlPullParser newPullParser = Xml.newPullParser();
                newPullParser.setInput(openFileInput, "UTF-8");
                int i2 = 0;
                while (i2 != 1 && i2 != 2) {
                    i2 = newPullParser.next();
                }
                if ("historical-records".equals(newPullParser.getName())) {
                    List<d> list = this.c;
                    list.clear();
                    while (true) {
                        int next = newPullParser.next();
                        if (next == 1) {
                            if (openFileInput == null) {
                                return;
                            }
                        } else if (!(next == 3 || next == 4)) {
                            if ("historical-record".equals(newPullParser.getName())) {
                                list.add(new d(newPullParser.getAttributeValue(null, Constants.ACTIVITY), Long.parseLong(newPullParser.getAttributeValue(null, LogBuilder.KEY_TIME)), Float.parseFloat(newPullParser.getAttributeValue(null, Constants.PROFILE_KEY_UNITS_WEIGHT))));
                            } else {
                                throw new XmlPullParserException("Share records file not well-formed.");
                            }
                        }
                    }
                    try {
                        openFileInput.close();
                    } catch (IOException unused) {
                    }
                } else {
                    throw new XmlPullParserException("Share records file does not start with historical-records tag.");
                }
            } catch (XmlPullParserException e2) {
                String str = n;
                Log.e(str, "Error reading historical recrod file: " + this.e, e2);
                if (openFileInput == null) {
                }
            } catch (IOException e3) {
                String str2 = n;
                Log.e(str2, "Error reading historical recrod file: " + this.e, e3);
                if (openFileInput == null) {
                }
            } catch (Throwable th) {
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException unused2) {
                    }
                }
                throw th;
            }
        } catch (FileNotFoundException unused3) {
        }
    }

    @DexIgnore
    public final boolean j() {
        if (this.g == null || this.f == null || this.b.isEmpty() || this.c.isEmpty()) {
            return false;
        }
        this.g.a(this.f, this.b, Collections.unmodifiableList(this.c));
        return true;
    }

    @DexIgnore
    public ResolveInfo b(int i2) {
        ResolveInfo resolveInfo;
        synchronized (this.a) {
            a();
            resolveInfo = this.b.get(i2).a;
        }
        return resolveInfo;
    }

    @DexIgnore
    public void c(int i2) {
        synchronized (this.a) {
            a();
            a aVar = this.b.get(i2);
            a aVar2 = this.b.get(0);
            a(new d(new ComponentName(aVar.a.activityInfo.packageName, aVar.a.activityInfo.name), System.currentTimeMillis(), aVar2 != null ? (aVar2.b - aVar.b) + 5.0f : 1.0f));
        }
    }

    @DexIgnore
    public void a(Intent intent) {
        synchronized (this.a) {
            if (this.f != intent) {
                this.f = intent;
                this.l = true;
                a();
            }
        }
    }

    @DexIgnore
    public int a(ResolveInfo resolveInfo) {
        synchronized (this.a) {
            a();
            List<a> list = this.b;
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (list.get(i2).a == resolveInfo) {
                    return i2;
                }
            }
            return -1;
        }
    }

    @DexIgnore
    public Intent a(int i2) {
        synchronized (this.a) {
            if (this.f == null) {
                return null;
            }
            a();
            a aVar = this.b.get(i2);
            ComponentName componentName = new ComponentName(aVar.a.activityInfo.packageName, aVar.a.activityInfo.name);
            Intent intent = new Intent(this.f);
            intent.setComponent(componentName);
            if (this.m != null) {
                if (this.m.a(this, new Intent(intent))) {
                    return null;
                }
            }
            a(new d(componentName, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }

    @DexIgnore
    public void a(e eVar) {
        synchronized (this.a) {
            this.m = eVar;
        }
    }

    @DexIgnore
    public final void a() {
        boolean e2 = e() | h();
        g();
        if (e2) {
            j();
            notifyChanged();
        }
    }

    @DexIgnore
    public final boolean a(d dVar) {
        boolean add = this.c.add(dVar);
        if (add) {
            this.k = true;
            g();
            f();
            j();
            notifyChanged();
        }
        return add;
    }
}
