package com.fossil;

import android.database.Cursor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class so implements ro {
    @DexIgnore
    public /* final */ ci a;
    @DexIgnore
    public /* final */ vh<qo> b;
    @DexIgnore
    public /* final */ ji c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends vh<qo> {
        @DexIgnore
        public a(so soVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, qo qoVar) {
            String str = qoVar.a;
            if (str == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, str);
            }
            ajVar.bindLong(2, (long) qoVar.b);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `SystemIdInfo` (`work_spec_id`,`system_id`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ji {
        @DexIgnore
        public b(so soVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM SystemIdInfo where work_spec_id=?";
        }
    }

    @DexIgnore
    public so(ci ciVar) {
        this.a = ciVar;
        this.b = new a(this, ciVar);
        this.c = new b(this, ciVar);
    }

    @DexIgnore
    @Override // com.fossil.ro
    public void a(qo qoVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(qoVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.ro
    public void b(String str) {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.c.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.ro
    public qo a(String str) {
        fi b2 = fi.b("SELECT `SystemIdInfo`.`work_spec_id` AS `work_spec_id`, `SystemIdInfo`.`system_id` AS `system_id` FROM SystemIdInfo WHERE work_spec_id=?", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        qo qoVar = null;
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "work_spec_id");
            int b4 = oi.b(a2, "system_id");
            if (a2.moveToFirst()) {
                qoVar = new qo(a2.getString(b3), a2.getInt(b4));
            }
            return qoVar;
        } finally {
            a2.close();
            b2.c();
        }
    }
}
