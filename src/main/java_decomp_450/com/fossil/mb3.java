package com.fossil;

import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mb3 {
    @DexIgnore
    public static volatile Handler d;
    @DexIgnore
    public /* final */ ki3 a;
    @DexIgnore
    public /* final */ Runnable b;
    @DexIgnore
    public volatile long c;

    @DexIgnore
    public mb3(ki3 ki3) {
        a72.a(ki3);
        this.a = ki3;
        this.b = new pb3(this, ki3);
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void a(long j) {
        c();
        if (j >= 0) {
            this.c = this.a.zzm().b();
            if (!d().postDelayed(this.b, j)) {
                this.a.e().t().a("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.c != 0;
    }

    @DexIgnore
    public final void c() {
        this.c = 0;
        d().removeCallbacks(this.b);
    }

    @DexIgnore
    public final Handler d() {
        Handler handler;
        if (d != null) {
            return d;
        }
        synchronized (mb3.class) {
            if (d == null) {
                d = new b43(this.a.f().getMainLooper());
            }
            handler = d;
        }
        return handler;
    }
}
