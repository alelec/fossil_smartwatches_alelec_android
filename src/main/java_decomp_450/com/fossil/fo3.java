package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface fo3<TResult, TContinuationResult> {
    @DexIgnore
    TContinuationResult then(no3<TResult> no3) throws Exception;
}
