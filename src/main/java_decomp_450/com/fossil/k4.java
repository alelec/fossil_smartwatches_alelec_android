package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface k4 {
    @DexIgnore
    float a(j4 j4Var);

    @DexIgnore
    void a();

    @DexIgnore
    void a(j4 j4Var, float f);

    @DexIgnore
    void a(j4 j4Var, Context context, ColorStateList colorStateList, float f, float f2, float f3);

    @DexIgnore
    void a(j4 j4Var, ColorStateList colorStateList);

    @DexIgnore
    float b(j4 j4Var);

    @DexIgnore
    void b(j4 j4Var, float f);

    @DexIgnore
    void c(j4 j4Var);

    @DexIgnore
    void c(j4 j4Var, float f);

    @DexIgnore
    float d(j4 j4Var);

    @DexIgnore
    ColorStateList e(j4 j4Var);

    @DexIgnore
    void f(j4 j4Var);

    @DexIgnore
    float g(j4 j4Var);

    @DexIgnore
    float h(j4 j4Var);

    @DexIgnore
    void i(j4 j4Var);
}
