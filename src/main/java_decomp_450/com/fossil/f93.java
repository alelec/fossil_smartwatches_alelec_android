package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f93 extends l93 {
    @DexIgnore
    public /* final */ float c;

    @DexIgnore
    public f93(float f) {
        super(0, Float.valueOf(Math.max(f, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
        this.c = Math.max(f, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    @Override // com.fossil.l93
    public final String toString() {
        float f = this.c;
        StringBuilder sb = new StringBuilder(30);
        sb.append("[Dash: length=");
        sb.append(f);
        sb.append("]");
        return sb.toString();
    }
}
