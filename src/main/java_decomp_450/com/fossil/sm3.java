package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sm3 extends vm3 {
    @DexIgnore
    public ap2 g;
    @DexIgnore
    public /* final */ /* synthetic */ om3 h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sm3(om3 om3, String str, int i, ap2 ap2) {
        super(str, i);
        this.h = om3;
        this.g = ap2;
    }

    @DexIgnore
    @Override // com.fossil.vm3
    public final int a() {
        return this.g.p();
    }

    @DexIgnore
    @Override // com.fossil.vm3
    public final boolean b() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.vm3
    public final boolean c() {
        return this.g.t();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x010f, code lost:
        if (r6.booleanValue() == false) goto L_0x03a3;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:122:0x03af  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x03b2  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x03ba A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x03bb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.lang.Long r18, java.lang.Long r19, com.fossil.rp2 r20, long r21, com.fossil.qb3 r23, boolean r24) {
        /*
            r17 = this;
            r0 = r17
            boolean r1 = com.fossil.b13.a()
            r2 = 1
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r2)
            r4 = 0
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r4)
            if (r1 == 0) goto L_0x0024
            com.fossil.om3 r1 = r0.h
            com.fossil.ym3 r1 = r1.l()
            java.lang.String r6 = r0.a
            com.fossil.yf3<java.lang.Boolean> r7 = com.fossil.wb3.g0
            boolean r1 = r1.d(r6, r7)
            if (r1 == 0) goto L_0x0024
            r1 = 1
            goto L_0x0025
        L_0x0024:
            r1 = 0
        L_0x0025:
            com.fossil.ap2 r6 = r0.g
            boolean r6 = r6.y()
            if (r6 == 0) goto L_0x0032
            r6 = r23
            long r6 = r6.e
            goto L_0x0034
        L_0x0032:
            r6 = r21
        L_0x0034:
            com.fossil.om3 r8 = r0.h
            com.fossil.jg3 r8 = r8.e()
            r9 = 2
            boolean r8 = r8.a(r9)
            r9 = 0
            if (r8 == 0) goto L_0x0096
            com.fossil.om3 r8 = r0.h
            com.fossil.jg3 r8 = r8.e()
            com.fossil.mg3 r8 = r8.B()
            int r10 = r0.b
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)
            com.fossil.ap2 r11 = r0.g
            boolean r11 = r11.zza()
            if (r11 == 0) goto L_0x0065
            com.fossil.ap2 r11 = r0.g
            int r11 = r11.p()
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)
            goto L_0x0066
        L_0x0065:
            r11 = r9
        L_0x0066:
            com.fossil.om3 r12 = r0.h
            com.fossil.hg3 r12 = r12.i()
            com.fossil.ap2 r13 = r0.g
            java.lang.String r13 = r13.q()
            java.lang.String r12 = r12.a(r13)
            java.lang.String r13 = "Evaluating filter. audience, filter, event"
            r8.a(r13, r10, r11, r12)
            com.fossil.om3 r8 = r0.h
            com.fossil.jg3 r8 = r8.e()
            com.fossil.mg3 r8 = r8.B()
            com.fossil.om3 r10 = r0.h
            com.fossil.fm3 r10 = r10.m()
            com.fossil.ap2 r11 = r0.g
            java.lang.String r10 = r10.a(r11)
            java.lang.String r11 = "Filter definition"
            r8.a(r11, r10)
        L_0x0096:
            com.fossil.ap2 r8 = r0.g
            boolean r8 = r8.zza()
            if (r8 == 0) goto L_0x03fc
            com.fossil.ap2 r8 = r0.g
            int r8 = r8.p()
            r10 = 256(0x100, float:3.59E-43)
            if (r8 <= r10) goto L_0x00aa
            goto L_0x03fc
        L_0x00aa:
            com.fossil.ap2 r8 = r0.g
            boolean r8 = r8.v()
            com.fossil.ap2 r10 = r0.g
            boolean r10 = r10.w()
            com.fossil.ap2 r11 = r0.g
            boolean r11 = r11.y()
            if (r8 != 0) goto L_0x00c5
            if (r10 != 0) goto L_0x00c5
            if (r11 == 0) goto L_0x00c3
            goto L_0x00c5
        L_0x00c3:
            r8 = 0
            goto L_0x00c6
        L_0x00c5:
            r8 = 1
        L_0x00c6:
            if (r24 == 0) goto L_0x00f2
            if (r8 != 0) goto L_0x00f2
            com.fossil.om3 r1 = r0.h
            com.fossil.jg3 r1 = r1.e()
            com.fossil.mg3 r1 = r1.B()
            int r3 = r0.b
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            com.fossil.ap2 r4 = r0.g
            boolean r4 = r4.zza()
            if (r4 == 0) goto L_0x00ec
            com.fossil.ap2 r4 = r0.g
            int r4 = r4.p()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r4)
        L_0x00ec:
            java.lang.String r4 = "Event filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID"
            r1.a(r4, r3, r9)
            return r2
        L_0x00f2:
            com.fossil.ap2 r10 = r0.g
            java.lang.String r11 = r20.q()
            boolean r12 = r10.t()
            if (r12 == 0) goto L_0x0113
            com.fossil.cp2 r12 = r10.u()
            java.lang.Boolean r6 = com.fossil.vm3.a(r6, r12)
            if (r6 != 0) goto L_0x010b
        L_0x0108:
            r5 = r9
            goto L_0x03a3
        L_0x010b:
            boolean r6 = r6.booleanValue()
            if (r6 != 0) goto L_0x0113
            goto L_0x03a3
        L_0x0113:
            java.util.HashSet r6 = new java.util.HashSet
            r6.<init>()
            java.util.List r7 = r10.r()
            java.util.Iterator r7 = r7.iterator()
        L_0x0120:
            boolean r12 = r7.hasNext()
            if (r12 == 0) goto L_0x0158
            java.lang.Object r12 = r7.next()
            com.fossil.bp2 r12 = (com.fossil.bp2) r12
            java.lang.String r13 = r12.v()
            boolean r13 = r13.isEmpty()
            if (r13 == 0) goto L_0x0150
            com.fossil.om3 r5 = r0.h
            com.fossil.jg3 r5 = r5.e()
            com.fossil.mg3 r5 = r5.w()
            com.fossil.om3 r6 = r0.h
            com.fossil.hg3 r6 = r6.i()
            java.lang.String r6 = r6.a(r11)
            java.lang.String r7 = "null or empty param name in filter. event"
            r5.a(r7, r6)
            goto L_0x0108
        L_0x0150:
            java.lang.String r12 = r12.v()
            r6.add(r12)
            goto L_0x0120
        L_0x0158:
            com.fossil.n4 r7 = new com.fossil.n4
            r7.<init>()
            java.util.List r12 = r20.zza()
            java.util.Iterator r12 = r12.iterator()
        L_0x0165:
            boolean r13 = r12.hasNext()
            if (r13 == 0) goto L_0x01f2
            java.lang.Object r13 = r12.next()
            com.fossil.tp2 r13 = (com.fossil.tp2) r13
            java.lang.String r14 = r13.p()
            boolean r14 = r6.contains(r14)
            if (r14 == 0) goto L_0x0165
            boolean r14 = r13.s()
            if (r14 == 0) goto L_0x0199
            java.lang.String r14 = r13.p()
            boolean r15 = r13.s()
            if (r15 == 0) goto L_0x0194
            long r15 = r13.t()
            java.lang.Long r13 = java.lang.Long.valueOf(r15)
            goto L_0x0195
        L_0x0194:
            r13 = r9
        L_0x0195:
            r7.put(r14, r13)
            goto L_0x0165
        L_0x0199:
            boolean r14 = r13.w()
            if (r14 == 0) goto L_0x01b7
            java.lang.String r14 = r13.p()
            boolean r15 = r13.w()
            if (r15 == 0) goto L_0x01b2
            double r15 = r13.x()
            java.lang.Double r13 = java.lang.Double.valueOf(r15)
            goto L_0x01b3
        L_0x01b2:
            r13 = r9
        L_0x01b3:
            r7.put(r14, r13)
            goto L_0x0165
        L_0x01b7:
            boolean r14 = r13.q()
            if (r14 == 0) goto L_0x01c9
            java.lang.String r14 = r13.p()
            java.lang.String r13 = r13.r()
            r7.put(r14, r13)
            goto L_0x0165
        L_0x01c9:
            com.fossil.om3 r5 = r0.h
            com.fossil.jg3 r5 = r5.e()
            com.fossil.mg3 r5 = r5.w()
            com.fossil.om3 r6 = r0.h
            com.fossil.hg3 r6 = r6.i()
            java.lang.String r6 = r6.a(r11)
            com.fossil.om3 r7 = r0.h
            com.fossil.hg3 r7 = r7.i()
            java.lang.String r10 = r13.p()
            java.lang.String r7 = r7.b(r10)
            java.lang.String r10 = "Unknown value for param. event, param"
            r5.a(r10, r6, r7)
            goto L_0x0108
        L_0x01f2:
            java.util.List r6 = r10.r()
            java.util.Iterator r6 = r6.iterator()
        L_0x01fa:
            boolean r10 = r6.hasNext()
            if (r10 == 0) goto L_0x03a2
            java.lang.Object r10 = r6.next()
            com.fossil.bp2 r10 = (com.fossil.bp2) r10
            boolean r12 = r10.s()
            if (r12 == 0) goto L_0x0214
            boolean r12 = r10.t()
            if (r12 == 0) goto L_0x0214
            r12 = 1
            goto L_0x0215
        L_0x0214:
            r12 = 0
        L_0x0215:
            java.lang.String r13 = r10.v()
            boolean r14 = r13.isEmpty()
            if (r14 == 0) goto L_0x023a
            com.fossil.om3 r5 = r0.h
            com.fossil.jg3 r5 = r5.e()
            com.fossil.mg3 r5 = r5.w()
            com.fossil.om3 r6 = r0.h
            com.fossil.hg3 r6 = r6.i()
            java.lang.String r6 = r6.a(r11)
            java.lang.String r7 = "Event has empty param name. event"
            r5.a(r7, r6)
            goto L_0x0108
        L_0x023a:
            java.lang.Object r14 = r7.get(r13)
            boolean r15 = r14 instanceof java.lang.Long
            if (r15 == 0) goto L_0x0287
            boolean r15 = r10.q()
            if (r15 != 0) goto L_0x026d
            com.fossil.om3 r5 = r0.h
            com.fossil.jg3 r5 = r5.e()
            com.fossil.mg3 r5 = r5.w()
            com.fossil.om3 r6 = r0.h
            com.fossil.hg3 r6 = r6.i()
            java.lang.String r6 = r6.a(r11)
            com.fossil.om3 r7 = r0.h
            com.fossil.hg3 r7 = r7.i()
            java.lang.String r7 = r7.b(r13)
            java.lang.String r10 = "No number filter for long param. event, param"
            r5.a(r10, r6, r7)
            goto L_0x0108
        L_0x026d:
            java.lang.Long r14 = (java.lang.Long) r14
            long r13 = r14.longValue()
            com.fossil.cp2 r10 = r10.r()
            java.lang.Boolean r10 = com.fossil.vm3.a(r13, r10)
            if (r10 != 0) goto L_0x027f
            goto L_0x0108
        L_0x027f:
            boolean r10 = r10.booleanValue()
            if (r10 != r12) goto L_0x01fa
            goto L_0x03a3
        L_0x0287:
            boolean r15 = r14 instanceof java.lang.Double
            if (r15 == 0) goto L_0x02d0
            boolean r15 = r10.q()
            if (r15 != 0) goto L_0x02b6
            com.fossil.om3 r5 = r0.h
            com.fossil.jg3 r5 = r5.e()
            com.fossil.mg3 r5 = r5.w()
            com.fossil.om3 r6 = r0.h
            com.fossil.hg3 r6 = r6.i()
            java.lang.String r6 = r6.a(r11)
            com.fossil.om3 r7 = r0.h
            com.fossil.hg3 r7 = r7.i()
            java.lang.String r7 = r7.b(r13)
            java.lang.String r10 = "No number filter for double param. event, param"
            r5.a(r10, r6, r7)
            goto L_0x0108
        L_0x02b6:
            java.lang.Double r14 = (java.lang.Double) r14
            double r13 = r14.doubleValue()
            com.fossil.cp2 r10 = r10.r()
            java.lang.Boolean r10 = com.fossil.vm3.a(r13, r10)
            if (r10 != 0) goto L_0x02c8
            goto L_0x0108
        L_0x02c8:
            boolean r10 = r10.booleanValue()
            if (r10 != r12) goto L_0x01fa
            goto L_0x03a3
        L_0x02d0:
            boolean r15 = r14 instanceof java.lang.String
            if (r15 == 0) goto L_0x0357
            boolean r15 = r10.zza()
            if (r15 == 0) goto L_0x02eb
            java.lang.String r14 = (java.lang.String) r14
            com.fossil.ep2 r10 = r10.p()
            com.fossil.om3 r13 = r0.h
            com.fossil.jg3 r13 = r13.e()
            java.lang.Boolean r10 = com.fossil.vm3.a(r14, r10, r13)
            goto L_0x0301
        L_0x02eb:
            boolean r15 = r10.q()
            if (r15 == 0) goto L_0x0332
            java.lang.String r14 = (java.lang.String) r14
            boolean r15 = com.fossil.fm3.a(r14)
            if (r15 == 0) goto L_0x030d
            com.fossil.cp2 r10 = r10.r()
            java.lang.Boolean r10 = com.fossil.vm3.a(r14, r10)
        L_0x0301:
            if (r10 != 0) goto L_0x0305
            goto L_0x0108
        L_0x0305:
            boolean r10 = r10.booleanValue()
            if (r10 != r12) goto L_0x01fa
            goto L_0x03a3
        L_0x030d:
            com.fossil.om3 r5 = r0.h
            com.fossil.jg3 r5 = r5.e()
            com.fossil.mg3 r5 = r5.w()
            com.fossil.om3 r6 = r0.h
            com.fossil.hg3 r6 = r6.i()
            java.lang.String r6 = r6.a(r11)
            com.fossil.om3 r7 = r0.h
            com.fossil.hg3 r7 = r7.i()
            java.lang.String r7 = r7.b(r13)
            java.lang.String r10 = "Invalid param value for number filter. event, param"
            r5.a(r10, r6, r7)
            goto L_0x0108
        L_0x0332:
            com.fossil.om3 r5 = r0.h
            com.fossil.jg3 r5 = r5.e()
            com.fossil.mg3 r5 = r5.w()
            com.fossil.om3 r6 = r0.h
            com.fossil.hg3 r6 = r6.i()
            java.lang.String r6 = r6.a(r11)
            com.fossil.om3 r7 = r0.h
            com.fossil.hg3 r7 = r7.i()
            java.lang.String r7 = r7.b(r13)
            java.lang.String r10 = "No filter for String param. event, param"
            r5.a(r10, r6, r7)
            goto L_0x0108
        L_0x0357:
            if (r14 != 0) goto L_0x037d
            com.fossil.om3 r6 = r0.h
            com.fossil.jg3 r6 = r6.e()
            com.fossil.mg3 r6 = r6.B()
            com.fossil.om3 r7 = r0.h
            com.fossil.hg3 r7 = r7.i()
            java.lang.String r7 = r7.a(r11)
            com.fossil.om3 r9 = r0.h
            com.fossil.hg3 r9 = r9.i()
            java.lang.String r9 = r9.b(r13)
            java.lang.String r10 = "Missing param for filter. event, param"
            r6.a(r10, r7, r9)
            goto L_0x03a3
        L_0x037d:
            com.fossil.om3 r5 = r0.h
            com.fossil.jg3 r5 = r5.e()
            com.fossil.mg3 r5 = r5.w()
            com.fossil.om3 r6 = r0.h
            com.fossil.hg3 r6 = r6.i()
            java.lang.String r6 = r6.a(r11)
            com.fossil.om3 r7 = r0.h
            com.fossil.hg3 r7 = r7.i()
            java.lang.String r7 = r7.b(r13)
            java.lang.String r10 = "Unknown param type. event, param"
            r5.a(r10, r6, r7)
            goto L_0x0108
        L_0x03a2:
            r5 = r3
        L_0x03a3:
            com.fossil.om3 r6 = r0.h
            com.fossil.jg3 r6 = r6.e()
            com.fossil.mg3 r6 = r6.B()
            if (r5 != 0) goto L_0x03b2
            java.lang.String r7 = "null"
            goto L_0x03b3
        L_0x03b2:
            r7 = r5
        L_0x03b3:
            java.lang.String r9 = "Event filter result"
            r6.a(r9, r7)
            if (r5 != 0) goto L_0x03bb
            return r4
        L_0x03bb:
            r0.c = r3
            boolean r4 = r5.booleanValue()
            if (r4 != 0) goto L_0x03c4
            return r2
        L_0x03c4:
            r0.d = r3
            if (r8 == 0) goto L_0x03fb
            boolean r3 = r20.r()
            if (r3 == 0) goto L_0x03fb
            long r3 = r20.s()
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            com.fossil.ap2 r4 = r0.g
            boolean r4 = r4.w()
            if (r4 == 0) goto L_0x03ed
            if (r1 == 0) goto L_0x03ea
            com.fossil.ap2 r1 = r0.g
            boolean r1 = r1.t()
            if (r1 == 0) goto L_0x03ea
            r3 = r18
        L_0x03ea:
            r0.f = r3
            goto L_0x03fb
        L_0x03ed:
            if (r1 == 0) goto L_0x03f9
            com.fossil.ap2 r1 = r0.g
            boolean r1 = r1.t()
            if (r1 == 0) goto L_0x03f9
            r3 = r19
        L_0x03f9:
            r0.e = r3
        L_0x03fb:
            return r2
        L_0x03fc:
            com.fossil.om3 r1 = r0.h
            com.fossil.jg3 r1 = r1.e()
            com.fossil.mg3 r1 = r1.w()
            java.lang.String r3 = r0.a
            java.lang.Object r3 = com.fossil.jg3.a(r3)
            com.fossil.ap2 r5 = r0.g
            boolean r5 = r5.zza()
            if (r5 == 0) goto L_0x041e
            com.fossil.ap2 r5 = r0.g
            int r5 = r5.p()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r5)
        L_0x041e:
            java.lang.String r5 = java.lang.String.valueOf(r9)
            java.lang.String r6 = "Invalid event filter ID. appId, id"
            r1.a(r6, r3, r5)
            com.fossil.om3 r1 = r0.h
            com.fossil.ym3 r1 = r1.l()
            java.lang.String r3 = r0.a
            com.fossil.yf3<java.lang.Boolean> r5 = com.fossil.wb3.d0
            boolean r1 = r1.d(r3, r5)
            if (r1 == 0) goto L_0x0438
            return r4
        L_0x0438:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sm3.a(java.lang.Long, java.lang.Long, com.fossil.rp2, long, com.fossil.qb3, boolean):boolean");
    }
}
