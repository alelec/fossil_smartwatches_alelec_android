package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hm6 implements Factory<gm6> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public hm6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static hm6 a(Provider<ThemeRepository> provider) {
        return new hm6(provider);
    }

    @DexIgnore
    public static gm6 a(ThemeRepository themeRepository) {
        return new gm6(themeRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public gm6 get() {
        return a(this.a.get());
    }
}
