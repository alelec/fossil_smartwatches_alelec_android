package com.fossil;

import android.os.Parcelable;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j56 extends he {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public HybridPreset a;
    @DexIgnore
    public MutableLiveData<HybridPreset> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> d; // = new ArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<MicroApp> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Gson h; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<String> i;
    @DexIgnore
    public /* final */ LiveData<MicroApp> j;
    @DexIgnore
    public /* final */ LiveData<Boolean> k;
    @DexIgnore
    public /* final */ zd<HybridPreset> l;
    @DexIgnore
    public /* final */ HybridPresetRepository m;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository n;
    @DexIgnore
    public /* final */ MicroAppRepository o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return j56.p;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<HybridPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ j56 a;

        @DexIgnore
        public b(j56 j56) {
            this.a = j56;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r7v6, resolved type: androidx.lifecycle.MutableLiveData */
        /* JADX WARN: Multi-variable type inference failed */
        /* renamed from: a */
        public final void onChanged(HybridPreset hybridPreset) {
            T t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = j56.q.a();
            local.d(a2, "current preset change=" + hybridPreset);
            if (hybridPreset != null) {
                String str = (String) this.a.e.a();
                String str2 = (String) this.a.f.a();
                Iterator<T> it = hybridPreset.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    T t2 = t;
                    boolean z = true;
                    if (!ee7.a((Object) t2.getPosition(), (Object) str) || mh7.b(t2.getAppId(), str2, true)) {
                        z = false;
                        continue;
                    }
                    if (z) {
                        break;
                    }
                }
                T t3 = t;
                if (t3 != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = j56.q.a();
                    local2.d(a3, "Update new microapp id=" + t3.getAppId() + " at position=" + str);
                    this.a.f.a((Object) t3.getAppId());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$init$1", f = "HybridCustomizeViewModel.kt", l = {102, 107}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $microAppPos;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ j56 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$init$1$1", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.b.a(this.this$0.this$0.l);
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(j56 j56, String str, String str2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = j56;
            this.$presetId = str;
            this.$microAppPos = str2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$presetId, this.$microAppPos, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = j56.q.a();
                local.d(a3, "init presetId=" + this.$presetId + " originalPreset=" + this.this$0.a + " microAppPos=" + this.$microAppPos + ' ');
                if (this.this$0.a == null) {
                    j56 j56 = this.this$0;
                    String str = this.$presetId;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (j56.a(str, this) == a2) {
                        return a2;
                    }
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.e.a((Object) this.$microAppPos);
            tk7 c = qj7.c();
            a aVar = new a(this, null);
            this.L$0 = yi7;
            this.label = 2;
            if (vh7.a(c, aVar, this) == a2) {
                return a2;
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initFromSaveInstanceState$1", f = "HybridCustomizeViewModel.kt", l = {118, 125}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $microAppPos;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $savedCurrentPreset;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $savedOriginalPreset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ j56 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initFromSaveInstanceState$1$1", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.b.a(this.this$0.this$0.l);
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(j56 j56, String str, HybridPreset hybridPreset, HybridPreset hybridPreset2, String str2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = j56;
            this.$presetId = str;
            this.$savedOriginalPreset = hybridPreset;
            this.$savedCurrentPreset = hybridPreset2;
            this.$microAppPos = str2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$presetId, this.$savedOriginalPreset, this.$savedCurrentPreset, this.$microAppPos, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x00b9  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r7.label
                r2 = 2
                r3 = 1
                if (r1 == 0) goto L_0x0027
                if (r1 == r3) goto L_0x001f
                if (r1 != r2) goto L_0x0017
                java.lang.Object r0 = r7.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r8)
                goto L_0x00b5
            L_0x0017:
                java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r8.<init>(r0)
                throw r8
            L_0x001f:
                java.lang.Object r1 = r7.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r8)
                goto L_0x0095
            L_0x0027:
                com.fossil.t87.a(r8)
                com.fossil.yi7 r1 = r7.p$
                com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                com.fossil.j56$a r4 = com.fossil.j56.q
                java.lang.String r4 = r4.a()
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "initFromSaveInstanceState presetId "
                r5.append(r6)
                java.lang.String r6 = r7.$presetId
                r5.append(r6)
                java.lang.String r6 = " savedOriginalPreset="
                r5.append(r6)
                com.portfolio.platform.data.model.room.microapp.HybridPreset r6 = r7.$savedOriginalPreset
                r5.append(r6)
                r6 = 44
                r5.append(r6)
                java.lang.String r6 = " savedCurrentPreset="
                r5.append(r6)
                com.portfolio.platform.data.model.room.microapp.HybridPreset r6 = r7.$savedCurrentPreset
                r5.append(r6)
                java.lang.String r6 = " microAppPos="
                r5.append(r6)
                java.lang.String r6 = r7.$microAppPos
                r5.append(r6)
                java.lang.String r6 = " currentPreset"
                r5.append(r6)
                java.lang.String r5 = r5.toString()
                r8.d(r4, r5)
                com.portfolio.platform.data.model.room.microapp.HybridPreset r8 = r7.$savedOriginalPreset
                if (r8 != 0) goto L_0x0089
                com.fossil.j56 r8 = r7.this$0
                java.lang.String r4 = r7.$presetId
                r7.L$0 = r1
                r7.label = r3
                java.lang.Object r8 = r8.a(r4, r7)
                if (r8 != r0) goto L_0x0095
                return r0
            L_0x0089:
                com.fossil.j56 r8 = r7.this$0
                r8.f()
                com.fossil.j56 r8 = r7.this$0
                com.portfolio.platform.data.model.room.microapp.HybridPreset r3 = r7.$savedOriginalPreset
                r8.a = r3
            L_0x0095:
                com.fossil.j56 r8 = r7.this$0
                androidx.lifecycle.MutableLiveData r8 = r8.e
                java.lang.String r3 = r7.$microAppPos
                r8.a(r3)
                com.fossil.tk7 r8 = com.fossil.qj7.c()
                com.fossil.j56$d$a r3 = new com.fossil.j56$d$a
                r4 = 0
                r3.<init>(r7, r4)
                r7.L$0 = r1
                r7.label = r2
                java.lang.Object r8 = com.fossil.vh7.a(r8, r3, r7)
                if (r8 != r0) goto L_0x00b5
                return r0
            L_0x00b5:
                com.portfolio.platform.data.model.room.microapp.HybridPreset r8 = r7.$savedCurrentPreset
                if (r8 == 0) goto L_0x00c4
                com.fossil.j56 r8 = r7.this$0
                androidx.lifecycle.MutableLiveData r8 = r8.b
                com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = r7.$savedCurrentPreset
                r8.a(r0)
            L_0x00c4:
                com.fossil.i97 r8 = com.fossil.i97.a
                return r8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.j56.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel", f = "HybridCustomizeViewModel.kt", l = {263, 264}, m = "initializePreset")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ j56 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(j56 j56, fb7 fb7) {
            super(fb7);
            this.this$0 = j56;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initializePreset$2", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ j56 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(j56 j56, fb7 fb7) {
            super(2, fb7);
            this.this$0 = j56;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                this.this$0.f();
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initializePreset$preset$1", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super HybridPreset>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ j56 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(j56 j56, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = j56;
            this.$presetId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, this.$presetId, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super HybridPreset> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                return this.this$0.m.getPresetById(this.$presetId);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ j56 a;

        @DexIgnore
        public h(j56 j56) {
            this.a = j56;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<MicroApp> apply(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = j56.q.a();
            local.d(a2, "transformWatchAppIdToModel watchAppId=" + str);
            j56 j56 = this.a;
            ee7.a((Object) str, "id");
            this.a.g.a(j56.b(str));
            return this.a.g;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ j56 a;

        @DexIgnore
        public i(j56 j56) {
            this.a = j56;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(String str) {
            T t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = j56.q.a();
            local.d(a2, "transformMicroAppPosToId pos=" + str);
            HybridPreset hybridPreset = (HybridPreset) this.a.b.a();
            if (hybridPreset != null) {
                Iterator<T> it = hybridPreset.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ee7.a((Object) t.getPosition(), (Object) str)) {
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    this.a.f.b(t2.getAppId());
                }
            }
            return this.a.f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ j56 a;

        @DexIgnore
        public j(j56 j56) {
            this.a = j56;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<Boolean> apply(HybridPreset hybridPreset) {
            ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
            Gson c = this.a.h;
            HybridPreset e = this.a.a;
            if (e != null) {
                boolean b = xc5.b(buttons, c, e.getButtons());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = a06.H.a();
                local.d(a2, "isTheSameHybridAppSetting " + b);
                if (b) {
                    this.a.c.a((Object) false);
                } else {
                    this.a.c.a((Object) true);
                }
                return this.a.c;
            }
            ee7.a();
            throw null;
        }
    }

    /*
    static {
        String simpleName = j56.class.getSimpleName();
        ee7.a((Object) simpleName, "HybridCustomizeViewModel::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public j56(HybridPresetRepository hybridPresetRepository, MicroAppLastSettingRepository microAppLastSettingRepository, MicroAppRepository microAppRepository) {
        ee7.b(hybridPresetRepository, "mHybridPresetRepository");
        ee7.b(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        ee7.b(microAppRepository, "mMicroAppRepository");
        this.m = hybridPresetRepository;
        this.n = microAppLastSettingRepository;
        this.o = microAppRepository;
        LiveData<String> b2 = ge.b(this.e, new i(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026dMicroAppIdLiveData\n    }");
        this.i = b2;
        LiveData<MicroApp> b3 = ge.b(b2, new h(this));
        ee7.a((Object) b3, "Transformations.switchMa\u2026tedMicroAppLiveData\n    }");
        this.j = b3;
        LiveData<Boolean> b4 = ge.b(this.b, new j(this));
        ee7.a((Object) b4, "Transformations.switchMa\u2026    isPresetChanged\n    }");
        this.k = b4;
        this.l = new b(this);
    }

    @DexIgnore
    @Override // com.fossil.he
    public void onCleared() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onCleared originalPreset=" + this.a + " currentPreset=" + this.b.a());
        this.b.b(this.l);
        super.onCleared();
    }

    @DexIgnore
    public final LiveData<Boolean> b() {
        return this.k;
    }

    @DexIgnore
    public final HybridPreset c() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<MicroApp> d() {
        LiveData<MicroApp> liveData = this.j;
        if (liveData != null) {
            return liveData;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final MutableLiveData<String> e() {
        return this.e;
    }

    @DexIgnore
    public final void f() {
        this.d.clear();
        this.d.addAll(this.o.getAllMicroApp(PortfolioApp.g0.c().c()));
    }

    @DexIgnore
    public final boolean g() {
        Boolean a2 = this.c.a();
        if (a2 != null) {
            return a2.booleanValue();
        }
        return false;
    }

    @DexIgnore
    public final ik7 a(String str, String str2) {
        ee7.b(str, "presetId");
        return xh7.b(zi7.a(qj7.a()), null, null, new c(this, str, str2, null), 3, null);
    }

    @DexIgnore
    public final MicroApp b(String str) {
        T t;
        ee7.b(str, "microAppId");
        Iterator<T> it = this.d.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (ee7.a((Object) str, (Object) t.getId())) {
                break;
            }
        }
        return t;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r7v9, resolved type: com.portfolio.platform.data.model.setting.SecondTimezoneSetting */
    /* JADX DEBUG: Multi-variable search result rejected for r7v10, resolved type: com.portfolio.platform.data.model.Ringtone */
    /* JADX DEBUG: Multi-variable search result rejected for r7v13, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeSetting */
    /* JADX DEBUG: Multi-variable search result rejected for r7v14, resolved type: com.portfolio.platform.data.model.Ringtone */
    /* JADX DEBUG: Multi-variable search result rejected for r7v15, resolved type: com.portfolio.platform.data.model.Ringtone */
    /* JADX DEBUG: Multi-variable search result rejected for r7v16, resolved type: com.portfolio.platform.data.model.Ringtone */
    /* JADX WARN: Multi-variable type inference failed */
    public final Parcelable c(String str) {
        Ringtone ringtone;
        T t;
        T t2;
        String settings;
        ee7.b(str, "appId");
        if (!pe5.c.d(str)) {
            return null;
        }
        HybridPreset a2 = a().a();
        String str2 = "";
        if (a2 != null) {
            Iterator<T> it = a2.getButtons().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                if (ee7.a((Object) t2.getAppId(), (Object) str)) {
                    break;
                }
            }
            T t3 = t2;
            if (!(t3 == null || (settings = t3.getSettings()) == null)) {
                str2 = settings;
            }
        }
        if (str2.length() == 0) {
            MicroAppLastSetting microAppLastSetting = this.n.getMicroAppLastSetting(str);
            if (microAppLastSetting != null) {
                str2 = microAppLastSetting.getSetting();
            }
            if (!TextUtils.isEmpty(str2)) {
                if (a2 != null) {
                    HybridPreset clone = a2.clone();
                    Iterator<T> it2 = clone.getButtons().iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it2.next();
                        if (ee7.a((Object) t.getAppId(), (Object) str)) {
                            break;
                        }
                    }
                    T t4 = t;
                    if (t4 != null) {
                        t4.setSettings(str2);
                        a(clone);
                    }
                }
            } else if (ee7.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                List<Ringtone> d2 = rd5.g.d();
                if (!d2.isEmpty()) {
                    str2 = wc5.a(d2.get(0));
                }
            }
        }
        if (sc5.a(str2)) {
            return null;
        }
        try {
            if (ee7.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                CommuteTimeSetting commuteTimeSetting = (CommuteTimeSetting) this.h.a(str2, CommuteTimeSetting.class);
                boolean isEmpty = TextUtils.isEmpty(commuteTimeSetting.getAddress());
                ringtone = commuteTimeSetting;
                if (isEmpty) {
                    return null;
                }
            } else if (ee7.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) this.h.a(str2, SecondTimezoneSetting.class);
                if (TextUtils.isEmpty(secondTimezoneSetting.getTimeZoneId())) {
                    return null;
                }
                ringtone = secondTimezoneSetting;
            } else if (!ee7.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                return null;
            } else {
                Ringtone ringtone2 = (Ringtone) this.h.a(str2, Ringtone.class);
                boolean isEmpty2 = TextUtils.isEmpty(ringtone2.getRingtoneName());
                ringtone = ringtone2;
                if (isEmpty2) {
                    return null;
                }
            }
            return ringtone;
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d(a06.H.a(), "exception when parse setting from json " + e2);
            return null;
        }
    }

    @DexIgnore
    public final boolean d(String str) {
        ee7.b(str, "microAppId");
        HybridPreset a2 = this.b.a();
        if (a2 == null) {
            return false;
        }
        Iterator<HybridPresetAppSetting> it = a2.getButtons().iterator();
        while (it.hasNext()) {
            if (ee7.a((Object) it.next().getAppId(), (Object) str)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void e(String str) {
        ee7.b(str, "watchAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = p;
        local.d(str2, "setSelectedMicroApp watchAppPos=" + str);
        this.e.a(str);
    }

    @DexIgnore
    public final ik7 a(String str, HybridPreset hybridPreset, HybridPreset hybridPreset2, String str2) {
        ee7.b(str, "presetId");
        return xh7.b(zi7.a(qj7.a()), null, null, new d(this, str, hybridPreset, hybridPreset2, str2, null), 3, null);
    }

    @DexIgnore
    public final MutableLiveData<HybridPreset> a() {
        return this.b;
    }

    @DexIgnore
    public final void a(HybridPreset hybridPreset) {
        ee7.b(hybridPreset, "preset");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "savePreset newPreset=" + hybridPreset);
        this.b.a(hybridPreset.clone());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.lang.String r9, com.fossil.fb7<? super com.fossil.i97> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.j56.e
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.j56$e r0 = (com.fossil.j56.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.j56$e r0 = new com.fossil.j56$e
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 2
            r5 = 1
            if (r2 == 0) goto L_0x004d
            if (r2 == r5) goto L_0x0041
            if (r2 != r4) goto L_0x0039
            java.lang.Object r9 = r0.L$2
            com.portfolio.platform.data.model.room.microapp.HybridPreset r9 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r9
            java.lang.Object r1 = r0.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.j56 r0 = (com.fossil.j56) r0
            com.fossil.t87.a(r10)
            goto L_0x009f
        L_0x0039:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0041:
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r2 = r0.L$0
            com.fossil.j56 r2 = (com.fossil.j56) r2
            com.fossil.t87.a(r10)
            goto L_0x0083
        L_0x004d:
            com.fossil.t87.a(r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r2 = com.fossil.j56.p
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "initializePreset presetId="
            r6.append(r7)
            r6.append(r9)
            java.lang.String r6 = r6.toString()
            r10.d(r2, r6)
            com.fossil.ti7 r10 = com.fossil.qj7.b()
            com.fossil.j56$g r2 = new com.fossil.j56$g
            r2.<init>(r8, r9, r3)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r5
            java.lang.Object r10 = com.fossil.vh7.a(r10, r2, r0)
            if (r10 != r1) goto L_0x0082
            return r1
        L_0x0082:
            r2 = r8
        L_0x0083:
            com.portfolio.platform.data.model.room.microapp.HybridPreset r10 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r10
            com.fossil.ti7 r5 = com.fossil.qj7.a()
            com.fossil.j56$f r6 = new com.fossil.j56$f
            r6.<init>(r2, r3)
            r0.L$0 = r2
            r0.L$1 = r9
            r0.L$2 = r10
            r0.label = r4
            java.lang.Object r9 = com.fossil.vh7.a(r5, r6, r0)
            if (r9 != r1) goto L_0x009d
            return r1
        L_0x009d:
            r9 = r10
            r0 = r2
        L_0x009f:
            if (r9 == 0) goto L_0x00b0
            com.portfolio.platform.data.model.room.microapp.HybridPreset r10 = r9.clone()
            r0.a = r10
            androidx.lifecycle.MutableLiveData<com.portfolio.platform.data.model.room.microapp.HybridPreset> r10 = r0.b
            com.portfolio.platform.data.model.room.microapp.HybridPreset r9 = r9.clone()
            r10.a(r9)
        L_0x00b0:
            com.fossil.i97 r9 = com.fossil.i97.a
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.j56.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final List<MicroApp> a(String str) {
        ee7.b(str, "category");
        ArrayList<MicroApp> arrayList = this.d;
        ArrayList arrayList2 = new ArrayList();
        for (T t : arrayList) {
            if (t.getCategories().contains(str)) {
                arrayList2.add(t);
            }
        }
        return arrayList2;
    }
}
