package com.fossil;

import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface yi extends Closeable {
    @DexIgnore
    void bindBlob(int i, byte[] bArr);

    @DexIgnore
    void bindDouble(int i, double d);

    @DexIgnore
    void bindLong(int i, long j);

    @DexIgnore
    void bindNull(int i);

    @DexIgnore
    void bindString(int i, String str);
}
