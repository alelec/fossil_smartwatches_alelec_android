package com.fossil;

import android.content.Context;
import java.io.File;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x44 {
    @DexIgnore
    public static /* final */ c d; // = new c();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ b b;
    @DexIgnore
    public w44 c;

    @DexIgnore
    public interface b {
        @DexIgnore
        File a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements w44 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.w44
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.w44
        public void a(long j, String str) {
        }

        @DexIgnore
        @Override // com.fossil.w44
        public String b() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.w44
        public byte[] c() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.w44
        public void d() {
        }
    }

    @DexIgnore
    public x44(Context context, b bVar) {
        this(context, bVar, null);
    }

    @DexIgnore
    public void a(long j, String str) {
        this.c.a(j, str);
    }

    @DexIgnore
    public final void b(String str) {
        this.c.a();
        this.c = d;
        if (str != null) {
            if (!t34.a(this.a, "com.crashlytics.CollectCustomLogs", true)) {
                z24.a().a("Preferences requested no custom logs. Aborting log file creation.");
            } else {
                a(a(str), 65536);
            }
        }
    }

    @DexIgnore
    public String c() {
        return this.c.b();
    }

    @DexIgnore
    public x44(Context context, b bVar, String str) {
        this.a = context;
        this.b = bVar;
        this.c = d;
        b(str);
    }

    @DexIgnore
    public void a() {
        this.c.d();
    }

    @DexIgnore
    public void a(Set<String> set) {
        File[] listFiles = this.b.a().listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (!set.contains(a(file))) {
                    file.delete();
                }
            }
        }
    }

    @DexIgnore
    public void a(File file, int i) {
        this.c = new z44(file, i);
    }

    @DexIgnore
    public byte[] b() {
        return this.c.c();
    }

    @DexIgnore
    public final File a(String str) {
        return new File(this.b.a(), "crashlytics-userlog-" + str + ".temp");
    }

    @DexIgnore
    public final String a(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".temp");
        if (lastIndexOf == -1) {
            return name;
        }
        return name.substring(20, lastIndexOf);
    }
}
