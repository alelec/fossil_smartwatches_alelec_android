package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b85 extends a85 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i w;
    @DexIgnore
    public static /* final */ SparseIntArray x;
    @DexIgnore
    public /* final */ LinearLayout u;
    @DexIgnore
    public long v;

    /*
    static {
        ViewDataBinding.i iVar = new ViewDataBinding.i(5);
        w = iVar;
        iVar.a(0, new String[]{"item_activity_day"}, new int[]{1}, new int[]{2131558651});
        SparseIntArray sparseIntArray = new SparseIntArray();
        x = sparseIntArray;
        sparseIntArray.put(2131362119, 2);
        x.put(2131362528, 3);
        x.put(2131362529, 4);
    }
    */

    @DexIgnore
    public b85(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 5, w, x));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
        ViewDataBinding.d(((a85) this).r);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (((com.fossil.a85) r6).r.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean e() {
        /*
            r6 = this;
            monitor-enter(r6)
            long r0 = r6.v     // Catch:{ all -> 0x0018 }
            r2 = 0
            r4 = 1
            int r5 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r5 == 0) goto L_0x000c
            monitor-exit(r6)     // Catch:{ all -> 0x0018 }
            return r4
        L_0x000c:
            monitor-exit(r6)     // Catch:{ all -> 0x0018 }
            com.fossil.y75 r0 = r6.r
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x0016
            return r4
        L_0x0016:
            r0 = 0
            return r0
        L_0x0018:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.b85.e():boolean");
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.v = 2;
        }
        ((a85) this).r.f();
        g();
    }

    @DexIgnore
    public b85(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 1, (ConstraintLayout) objArr[2], (y75) objArr[1], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[4]);
        this.v = -1;
        LinearLayout linearLayout = (LinearLayout) objArr[0];
        this.u = linearLayout;
        linearLayout.setTag(null);
        a(view);
        f();
    }
}
