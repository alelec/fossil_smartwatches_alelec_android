package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.a12;
import com.fossil.my1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lf2 extends n62<mf2> {
    @DexIgnore
    public /* final */ my1.a E;

    @DexIgnore
    public lf2(Context context, Looper looper, j62 j62, my1.a aVar, a12.b bVar, a12.c cVar) {
        super(context, looper, 68, j62, bVar, cVar);
        this.E = aVar;
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.credentials.internal.ICredentialsService");
        if (queryLocalInterface instanceof mf2) {
            return (mf2) queryLocalInterface;
        }
        return new nf2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final String i() {
        return "com.google.android.gms.auth.api.credentials.internal.ICredentialsService";
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.h62, com.fossil.n62
    public final int k() {
        return 12800000;
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final String p() {
        return "com.google.android.gms.auth.api.credentials.service.START";
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final Bundle x() {
        my1.a aVar = this.E;
        return aVar == null ? new Bundle() : aVar.a();
    }
}
