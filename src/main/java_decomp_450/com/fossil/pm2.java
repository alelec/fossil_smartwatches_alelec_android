package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pm2 implements Parcelable.Creator<om2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ om2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        mm2 mm2 = null;
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        int i = 1;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                i = j72.q(parcel, a);
            } else if (a2 == 2) {
                mm2 = (mm2) j72.a(parcel, a, mm2.CREATOR);
            } else if (a2 == 3) {
                iBinder = j72.p(parcel, a);
            } else if (a2 != 4) {
                j72.v(parcel, a);
            } else {
                iBinder2 = j72.p(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new om2(i, mm2, iBinder, iBinder2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ om2[] newArray(int i) {
        return new om2[i];
    }
}
