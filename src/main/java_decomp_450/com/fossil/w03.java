package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w03 implements tr2<z03> {
    @DexIgnore
    public static w03 b; // = new w03();
    @DexIgnore
    public /* final */ tr2<z03> a;

    @DexIgnore
    public w03(tr2<z03> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((z03) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((z03) b.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ z03 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public w03() {
        this(sr2.a(new y03()));
    }
}
