package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import com.fossil.g17;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h17 {
    @DexIgnore
    public static /* final */ AtomicInteger m; // = new AtomicInteger();
    @DexIgnore
    public /* final */ Picasso a;
    @DexIgnore
    public /* final */ g17.b b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e; // = true;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public Drawable j;
    @DexIgnore
    public Drawable k;
    @DexIgnore
    public Object l;

    @DexIgnore
    public h17(Picasso picasso, Uri uri, int i2) {
        if (!picasso.o) {
            this.a = picasso;
            this.b = new g17.b(uri, i2, picasso.l);
            return;
        }
        throw new IllegalStateException("Picasso instance already shut down. Cannot submit new requests.");
    }

    @DexIgnore
    public h17 a(int i2) {
        if (i2 == 0) {
            throw new IllegalArgumentException("Error image resource invalid.");
        } else if (this.k == null) {
            this.g = i2;
            return this;
        } else {
            throw new IllegalStateException("Error image already set.");
        }
    }

    @DexIgnore
    public h17 b(int i2) {
        if (!this.e) {
            throw new IllegalStateException("Already explicitly declared as no placeholder.");
        } else if (i2 == 0) {
            throw new IllegalArgumentException("Placeholder image resource invalid.");
        } else if (this.j == null) {
            this.f = i2;
            return this;
        } else {
            throw new IllegalStateException("Placeholder image already set.");
        }
    }

    @DexIgnore
    public h17 c() {
        this.d = false;
        return this;
    }

    @DexIgnore
    public h17 a(int i2, int i3) {
        this.b.a(i2, i3);
        return this;
    }

    @DexIgnore
    public h17 a(Transformation transformation) {
        this.b.a(transformation);
        return this;
    }

    @DexIgnore
    public void a(Target target) {
        Bitmap b2;
        long nanoTime = System.nanoTime();
        o17.a();
        if (target == null) {
            throw new IllegalArgumentException("Target must not be null.");
        } else if (!this.d) {
            Drawable drawable = null;
            if (!this.b.b()) {
                this.a.a(target);
                if (this.e) {
                    drawable = a();
                }
                target.onPrepareLoad(drawable);
                return;
            }
            g17 a2 = a(nanoTime);
            String a3 = o17.a(a2);
            if (!a17.shouldReadFromMemoryCache(this.h) || (b2 = this.a.b(a3)) == null) {
                if (this.e) {
                    drawable = a();
                }
                target.onPrepareLoad(drawable);
                this.a.a((m07) new m17(this.a, target, a2, this.h, this.i, this.k, a3, this.l, this.g));
                return;
            }
            this.a.a(target);
            target.onBitmapLoaded(b2, Picasso.LoadedFrom.MEMORY);
        } else {
            throw new IllegalStateException("Fit cannot be used with a Target.");
        }
    }

    @DexIgnore
    public h17 b() {
        this.c = true;
        return this;
    }

    @DexIgnore
    public void a(ImageView imageView) {
        a(imageView, (q07) null);
    }

    @DexIgnore
    public void a(ImageView imageView, q07 q07) {
        Bitmap b2;
        long nanoTime = System.nanoTime();
        o17.a();
        if (imageView == null) {
            throw new IllegalArgumentException("Target must not be null.");
        } else if (!this.b.b()) {
            this.a.a(imageView);
            if (this.e) {
                e17.a(imageView, a());
            }
        } else {
            if (this.d) {
                if (!this.b.c()) {
                    int width = imageView.getWidth();
                    int height = imageView.getHeight();
                    if (width == 0 || height == 0) {
                        if (this.e) {
                            e17.a(imageView, a());
                        }
                        this.a.a(imageView, new t07(this, imageView, q07));
                        return;
                    }
                    this.b.a(width, height);
                } else {
                    throw new IllegalStateException("Fit cannot be used with resize.");
                }
            }
            g17 a2 = a(nanoTime);
            String a3 = o17.a(a2);
            if (!a17.shouldReadFromMemoryCache(this.h) || (b2 = this.a.b(a3)) == null) {
                if (this.e) {
                    e17.a(imageView, a());
                }
                this.a.a((m07) new w07(this.a, imageView, a2, this.h, this.i, this.g, this.k, a3, this.l, q07, this.c));
                return;
            }
            this.a.a(imageView);
            Picasso picasso = this.a;
            e17.a(imageView, picasso.e, b2, Picasso.LoadedFrom.MEMORY, this.c, picasso.m);
            if (this.a.n) {
                String g2 = a2.g();
                o17.a("Main", "completed", g2, "from " + Picasso.LoadedFrom.MEMORY);
            }
            if (q07 != null) {
                q07.onSuccess();
            }
        }
    }

    @DexIgnore
    public final Drawable a() {
        if (this.f != 0) {
            return this.a.e.getResources().getDrawable(this.f);
        }
        return this.j;
    }

    @DexIgnore
    public final g17 a(long j2) {
        int andIncrement = m.getAndIncrement();
        g17 a2 = this.b.a();
        a2.a = andIncrement;
        a2.b = j2;
        boolean z = this.a.n;
        if (z) {
            o17.a("Main", "created", a2.g(), a2.toString());
        }
        this.a.a(a2);
        if (a2 != a2) {
            a2.a = andIncrement;
            a2.b = j2;
            if (z) {
                String d2 = a2.d();
                o17.a("Main", "changed", d2, "into " + a2);
            }
        }
        return a2;
    }
}
