package com.fossil;

import android.os.Bundle;
import com.fossil.a12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f52 implements a12.b, a12.c {
    @DexIgnore
    public /* final */ v02<?> a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public e52 c;

    @DexIgnore
    public f52(v02<?> v02, boolean z) {
        this.a = v02;
        this.b = z;
    }

    @DexIgnore
    @Override // com.fossil.t12
    public final void a(int i) {
        a();
        this.c.a(i);
    }

    @DexIgnore
    @Override // com.fossil.t12
    public final void b(Bundle bundle) {
        a();
        this.c.b(bundle);
    }

    @DexIgnore
    @Override // com.fossil.a22
    public final void a(i02 i02) {
        a();
        this.c.a(i02, this.a, this.b);
    }

    @DexIgnore
    public final void a(e52 e52) {
        this.c = e52;
    }

    @DexIgnore
    public final void a() {
        a72.a(this.c, "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
    }
}
