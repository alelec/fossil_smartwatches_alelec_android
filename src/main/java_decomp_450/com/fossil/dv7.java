package com.fossil;

import com.fossil.do7;
import com.fossil.fo7;
import com.fossil.go7;
import com.fossil.io7;
import com.fossil.lo7;
import java.io.IOException;
import java.util.regex.Pattern;
import okhttp3.RequestBody;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dv7 {
    @DexIgnore
    public static /* final */ char[] l; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    @DexIgnore
    public static /* final */ Pattern m; // = Pattern.compile("(.*/)?(\\.|%2e|%2E){1,2}(/.*)?");
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ go7 b;
    @DexIgnore
    public String c;
    @DexIgnore
    public go7.a d;
    @DexIgnore
    public /* final */ lo7.a e; // = new lo7.a();
    @DexIgnore
    public /* final */ fo7.a f;
    @DexIgnore
    public ho7 g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public io7.a i;
    @DexIgnore
    public do7.a j;
    @DexIgnore
    public RequestBody k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends RequestBody {
        @DexIgnore
        public /* final */ RequestBody a;
        @DexIgnore
        public /* final */ ho7 b;

        @DexIgnore
        public a(RequestBody requestBody, ho7 ho7) {
            this.a = requestBody;
            this.b = ho7;
        }

        @DexIgnore
        @Override // okhttp3.RequestBody
        public long a() throws IOException {
            return this.a.a();
        }

        @DexIgnore
        @Override // okhttp3.RequestBody
        public ho7 b() {
            return this.b;
        }

        @DexIgnore
        @Override // okhttp3.RequestBody
        public void a(zq7 zq7) throws IOException {
            this.a.a(zq7);
        }
    }

    @DexIgnore
    public dv7(String str, go7 go7, String str2, fo7 fo7, ho7 ho7, boolean z, boolean z2, boolean z3) {
        this.a = str;
        this.b = go7;
        this.c = str2;
        this.g = ho7;
        this.h = z;
        if (fo7 != null) {
            this.f = fo7.a();
        } else {
            this.f = new fo7.a();
        }
        if (z2) {
            this.j = new do7.a();
        } else if (z3) {
            io7.a aVar = new io7.a();
            this.i = aVar;
            aVar.a(io7.f);
        }
    }

    @DexIgnore
    public void a(Object obj) {
        this.c = obj.toString();
    }

    @DexIgnore
    public void b(String str, String str2, boolean z) {
        if (this.c != null) {
            String a2 = a(str2, z);
            String str3 = this.c;
            String replace = str3.replace("{" + str + "}", a2);
            if (!m.matcher(replace).matches()) {
                this.c = replace;
                return;
            }
            throw new IllegalArgumentException("@Path parameters shouldn't perform path traversal ('.' or '..'): " + str2);
        }
        throw new AssertionError();
    }

    @DexIgnore
    public void c(String str, String str2, boolean z) {
        String str3 = this.c;
        if (str3 != null) {
            go7.a a2 = this.b.a(str3);
            this.d = a2;
            if (a2 != null) {
                this.c = null;
            } else {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.b + ", Relative: " + this.c);
            }
        }
        if (z) {
            this.d.a(str, str2);
        } else {
            this.d.b(str, str2);
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        if ("Content-Type".equalsIgnoreCase(str)) {
            try {
                this.g = ho7.a(str2);
            } catch (IllegalArgumentException e2) {
                throw new IllegalArgumentException("Malformed content type: " + str2, e2);
            }
        } else {
            this.f.a(str, str2);
        }
    }

    @DexIgnore
    public static String a(String str, boolean z) {
        int length = str.length();
        int i2 = 0;
        while (i2 < length) {
            int codePointAt = str.codePointAt(i2);
            if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                yq7 yq7 = new yq7();
                yq7.a(str, 0, i2);
                a(yq7, str, i2, length, z);
                return yq7.v();
            }
            i2 += Character.charCount(codePointAt);
        }
        return str;
    }

    @DexIgnore
    public static void a(yq7 yq7, String str, int i2, int i3, boolean z) {
        yq7 yq72 = null;
        while (i2 < i3) {
            int codePointAt = str.codePointAt(i2);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                    if (yq72 == null) {
                        yq72 = new yq7();
                    }
                    yq72.c(codePointAt);
                    while (!yq72.h()) {
                        byte readByte = yq72.readByte() & 255;
                        yq7.writeByte(37);
                        yq7.writeByte((int) l[(readByte >> 4) & 15]);
                        yq7.writeByte((int) l[readByte & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY]);
                    }
                } else {
                    yq7.c(codePointAt);
                }
            }
            i2 += Character.charCount(codePointAt);
        }
    }

    @DexIgnore
    public void a(String str, String str2, boolean z) {
        if (z) {
            this.j.b(str, str2);
        } else {
            this.j.a(str, str2);
        }
    }

    @DexIgnore
    public void a(fo7 fo7, RequestBody requestBody) {
        this.i.a(fo7, requestBody);
    }

    @DexIgnore
    public void a(io7.b bVar) {
        this.i.a(bVar);
    }

    @DexIgnore
    public void a(RequestBody requestBody) {
        this.k = requestBody;
    }

    @DexIgnore
    public lo7.a a() {
        go7 go7;
        go7.a aVar = this.d;
        if (aVar != null) {
            go7 = aVar.a();
        } else {
            go7 = this.b.b(this.c);
            if (go7 == null) {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.b + ", Relative: " + this.c);
            }
        }
        a aVar2 = this.k;
        if (aVar2 == null) {
            do7.a aVar3 = this.j;
            if (aVar3 != null) {
                aVar2 = aVar3.a();
            } else {
                io7.a aVar4 = this.i;
                if (aVar4 != null) {
                    aVar2 = aVar4.a();
                } else if (this.h) {
                    aVar2 = RequestBody.a((ho7) null, new byte[0]);
                }
            }
        }
        ho7 ho7 = this.g;
        if (ho7 != null) {
            if (aVar2 != null) {
                aVar2 = new a(aVar2, ho7);
            } else {
                this.f.a("Content-Type", ho7.toString());
            }
        }
        lo7.a aVar5 = this.e;
        aVar5.a(go7);
        aVar5.a(this.f.a());
        aVar5.a(this.a, aVar2);
        return aVar5;
    }
}
