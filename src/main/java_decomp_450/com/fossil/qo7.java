package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qo7 implements Runnable {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public qo7(String str, Object... objArr) {
        this.a = ro7.a(str, objArr);
    }

    @DexIgnore
    public abstract void b();

    @DexIgnore
    public final void run() {
        String name = Thread.currentThread().getName();
        Thread.currentThread().setName(this.a);
        try {
            b();
        } finally {
            Thread.currentThread().setName(name);
        }
    }
}
