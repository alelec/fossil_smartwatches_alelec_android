package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b80 extends n80 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ short b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ sf0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<b80> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final b80 a(byte[] bArr) throws IllegalArgumentException {
            sf0 sf0;
            if (bArr.length >= 3) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                short s = order.getShort(0);
                byte b = order.get(2);
                if (bArr.length > 3) {
                    sf0 = sf0.c.a(order.get(3));
                    if (sf0 == null) {
                        StringBuilder b2 = yh0.b("Invalid state: ");
                        b2.append((int) order.get(3));
                        throw new IllegalArgumentException(b2.toString());
                    }
                } else {
                    sf0 = sf0.NORMAL;
                }
                return new b80(s, b, sf0);
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", require as ", "least: 3"));
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public b80 createFromParcel(Parcel parcel) {
            return new b80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public b80[] newArray(int i) {
            return new b80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public b80 m3createFromParcel(Parcel parcel) {
            return new b80(parcel, null);
        }
    }

    @DexIgnore
    public b80(short s, byte b2, sf0 sf0) throws IllegalArgumentException {
        super(o80.BATTERY);
        this.b = s;
        this.c = b2;
        this.d = sf0;
        e();
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).putShort(this.b).put(this.c).put(this.d.a()).array();
        ee7.a((Object) array, "ByteBuffer.allocate(MIN_\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        short s = this.b;
        boolean z = true;
        if (s >= 0 && 4400 >= s) {
            byte b2 = this.c;
            if (b2 < 0 || 100 < b2) {
                z = false;
            }
            if (!z) {
                throw new IllegalArgumentException(yh0.a(yh0.b("percentage("), this.c, ") is out of range ", "[0, 100]."));
            }
            return;
        }
        throw new IllegalArgumentException(yh0.a(yh0.b("voltage("), this.b, ") is out of range ", "[0, 4400]."));
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(b80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            b80 b80 = (b80) obj;
            return this.b == b80.b && this.c == b80.c && this.d == b80.d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BatteryConfig");
    }

    @DexIgnore
    public final byte getPercentage() {
        return this.c;
    }

    @DexIgnore
    public final sf0 getState() {
        return this.d;
    }

    @DexIgnore
    public final short getVoltage() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        int hashCode = Byte.valueOf(this.c).hashCode();
        return this.d.hashCode() + ((hashCode + (this.b * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(yz0.b(this.b));
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeByte(this.d.a());
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(jSONObject, r51.b5, Short.valueOf(this.b));
            yz0.a(jSONObject, r51.a5, Byte.valueOf(this.c));
            yz0.a(jSONObject, r51.l1, this.d);
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ b80(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = (short) parcel.readInt();
        this.c = parcel.readByte();
        sf0 a2 = sf0.c.a(parcel.readByte());
        if (a2 != null) {
            this.d = a2;
            e();
            return;
        }
        ee7.a();
        throw null;
    }
}
