package com.fossil;

import android.net.Uri;
import java.nio.charset.Charset;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gd5 implements yw {
    @DexIgnore
    public String b;
    @DexIgnore
    public Uri c;
    @DexIgnore
    public String d;

    @DexIgnore
    public gd5(String str, String str2) {
        this.b = str;
        this.d = str2;
    }

    @DexIgnore
    public final String a() {
        return this.d;
    }

    @DexIgnore
    public final Uri b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public void a(MessageDigest messageDigest) {
        ee7.b(messageDigest, "messageDigest");
        String str = this.b + this.c + this.d;
        Charset charset = yw.a;
        ee7.a((Object) charset, "Key.CHARSET");
        if (str != null) {
            byte[] bytes = str.getBytes(charset);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            messageDigest.update(bytes);
            return;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public gd5(Uri uri, String str) {
        this.c = uri;
        this.d = str;
    }
}
