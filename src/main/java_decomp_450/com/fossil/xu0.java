package com.fossil;

import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xu0 extends i60 {
    @DexIgnore
    public /* final */ n60[] a;

    @DexIgnore
    public xu0(n60[] n60Arr) {
        this.a = n60Arr;
    }

    @DexIgnore
    @Override // com.fossil.i60
    public boolean a(km1 km1) {
        if (this.a.length == 0) {
            return true;
        }
        for (n60 n60 : this.a) {
            if (n60 == km1.t.getDeviceType()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject a2 = yz0.a(new JSONObject(), r51.F1, "device_type");
        r51 r51 = r51.H1;
        n60[] n60Arr = this.a;
        JSONArray jSONArray = new JSONArray();
        for (n60 n60 : n60Arr) {
            jSONArray.put(yz0.a(n60));
        }
        return yz0.a(a2, r51, jSONArray);
    }
}
