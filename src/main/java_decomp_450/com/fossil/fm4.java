package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fm4 extends RecyclerView.g<c> {
    @DexIgnore
    public cc5 a;
    @DexIgnore
    public ArrayList<cc5> b;
    @DexIgnore
    public b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(cc5 cc5);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public CustomizeWidget a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public cc5 d;
        @DexIgnore
        public /* final */ /* synthetic */ fm4 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b c;
                if (this.a.e.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && (c = this.a.e.c()) != null) {
                    Object obj = this.a.e.b.get(this.a.getAdapterPosition());
                    ee7.a(obj, "mData[adapterPosition]");
                    c.a((cc5) obj);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(fm4 fm4, View view) {
            super(view);
            ee7.b(view, "view");
            this.e = fm4;
            View findViewById = view.findViewById(2131363465);
            ee7.a((Object) findViewById, "view.findViewById(R.id.wc_workout_type)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363361);
            ee7.a((Object) findViewById2, "view.findViewById(R.id.tv_workout_type_name)");
            this.b = (TextView) findViewById2;
            this.c = view.findViewById(2131362698);
            this.a.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void a(cc5 cc5) {
            ee7.b(cc5, "workoutWrapperType");
            this.d = cc5;
            if (cc5 != null) {
                r87<Integer, Integer> a2 = cc5.Companion.a(cc5);
                this.a.c(a2.getFirst().intValue());
                this.b.setText(ig5.a(PortfolioApp.g0.c(), a2.getSecond().intValue()));
            }
            this.a.setSelectedWc(cc5.ordinal() == this.e.a.ordinal());
            if (cc5.ordinal() == this.e.a.ordinal()) {
                View view = this.c;
                ee7.a((Object) view, "ivIndicator");
                view.setBackground(v6.c(PortfolioApp.g0.c(), 2131230953));
                return;
            }
            View view2 = this.c;
            ee7.a((Object) view2, "ivIndicator");
            view2.setBackground(v6.c(PortfolioApp.g0.c(), 2131230954));
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ fm4(ArrayList arrayList, b bVar, int i, zd7 zd7) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : bVar);
    }

    @DexIgnore
    public final b c() {
        return this.c;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public final void a(List<? extends cc5> list) {
        ee7.b(list, "data");
        this.b.clear();
        this.b.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void b(cc5 cc5) {
        ee7.b(cc5, "workoutWrapperType");
        this.a = cc5;
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558723, viewGroup, false);
        ee7.a((Object) inflate, "LayoutInflater.from(pare\u2026kout_type, parent, false)");
        return new c(this, inflate);
    }

    @DexIgnore
    public fm4(ArrayList<cc5> arrayList, b bVar) {
        ee7.b(arrayList, "mData");
        this.b = arrayList;
        this.c = bVar;
        this.a = cc5.WORKOUT;
    }

    @DexIgnore
    public final int a(cc5 cc5) {
        T t;
        boolean z;
        ee7.b(cc5, "workoutWrapperType");
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (t == cc5) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return this.b.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        ee7.b(cVar, "holder");
        if (getItemCount() > i && i != -1) {
            cc5 cc5 = this.b.get(i);
            ee7.a((Object) cc5, "mData[position]");
            cVar.a(cc5);
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "listener");
        this.c = bVar;
    }
}
