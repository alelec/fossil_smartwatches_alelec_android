package com.fossil;

import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppDataRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GuestApiService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nl4 implements MembersInjector<PortfolioApp> {
    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ch5 ch5) {
        portfolioApp.c = ch5;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, SummariesRepository summariesRepository) {
        portfolioApp.d = summariesRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, SleepSummariesRepository sleepSummariesRepository) {
        portfolioApp.e = sleepSummariesRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, pd5 pd5) {
        portfolioApp.f = pd5;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, GuestApiService guestApiService) {
        portfolioApp.g = guestApiService;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, pj4 pj4) {
        portfolioApp.h = pj4;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ApiServiceV2 apiServiceV2) {
        portfolioApp.i = apiServiceV2;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, uw6 uw6) {
        portfolioApp.j = uw6;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, rl4 rl4) {
        portfolioApp.p = rl4;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, qn5 qn5) {
        portfolioApp.q = qn5;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, qd5 qd5) {
        portfolioApp.r = qd5;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ApplicationEventListener applicationEventListener) {
        portfolioApp.s = applicationEventListener;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, DeviceRepository deviceRepository) {
        portfolioApp.t = deviceRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ge5 ge5) {
        portfolioApp.u = ge5;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, sj5 sj5) {
        portfolioApp.v = sj5;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, DianaPresetRepository dianaPresetRepository) {
        portfolioApp.w = dianaPresetRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, zz6 zz6) {
        portfolioApp.x = zz6;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, WatchLocalizationRepository watchLocalizationRepository) {
        portfolioApp.y = watchLocalizationRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, nh5 nh5) {
        portfolioApp.z = nh5;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, UserRepository userRepository) {
        portfolioApp.A = userRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, WatchFaceRepository watchFaceRepository) {
        portfolioApp.B = watchFaceRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, QuickResponseRepository quickResponseRepository) {
        portfolioApp.C = quickResponseRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, WorkoutSettingRepository workoutSettingRepository) {
        portfolioApp.D = workoutSettingRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, WatchAppDataRepository watchAppDataRepository) {
        portfolioApp.E = watchAppDataRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, wk5 wk5) {
        portfolioApp.F = wk5;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ri5 ri5) {
        portfolioApp.T = ri5;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ui5 ui5) {
        portfolioApp.U = ui5;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ThemeRepository themeRepository) {
        portfolioApp.V = themeRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, aw6 aw6) {
        portfolioApp.W = aw6;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, dw6 dw6) {
        portfolioApp.X = dw6;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, vm4 vm4) {
        portfolioApp.Y = vm4;
    }
}
