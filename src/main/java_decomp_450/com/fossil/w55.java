package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class w55 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView A;
    @DexIgnore
    public /* final */ ImageView B;
    @DexIgnore
    public /* final */ ConstraintLayout C;
    @DexIgnore
    public /* final */ ConstraintLayout D;
    @DexIgnore
    public /* final */ ConstraintLayout E;
    @DexIgnore
    public /* final */ ConstraintLayout F;
    @DexIgnore
    public /* final */ ScrollView G;
    @DexIgnore
    public /* final */ FlexibleTextView H;
    @DexIgnore
    public /* final */ FlexibleTextView I;
    @DexIgnore
    public /* final */ FlexibleTextView J;
    @DexIgnore
    public /* final */ FlexibleTextView K;
    @DexIgnore
    public /* final */ FlexibleButton L;
    @DexIgnore
    public /* final */ FlexibleTextView M;
    @DexIgnore
    public /* final */ FlexibleTextView N;
    @DexIgnore
    public /* final */ Barrier q;
    @DexIgnore
    public /* final */ Barrier r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText t;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText u;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout v;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout w;
    @DexIgnore
    public /* final */ FloatingActionButton x;
    @DexIgnore
    public /* final */ RTLImageView y;
    @DexIgnore
    public /* final */ ImageView z;

    @DexIgnore
    public w55(Object obj, View view, int i, Barrier barrier, Barrier barrier2, FlexibleButton flexibleButton, FlexibleTextInputEditText flexibleTextInputEditText, FlexibleTextInputEditText flexibleTextInputEditText2, FlexibleTextInputLayout flexibleTextInputLayout, FlexibleTextInputLayout flexibleTextInputLayout2, FloatingActionButton floatingActionButton, RTLImageView rTLImageView, ImageView imageView, ImageView imageView2, ImageView imageView3, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ScrollView scrollView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6) {
        super(obj, view, i);
        this.q = barrier;
        this.r = barrier2;
        this.s = flexibleButton;
        this.t = flexibleTextInputEditText;
        this.u = flexibleTextInputEditText2;
        this.v = flexibleTextInputLayout;
        this.w = flexibleTextInputLayout2;
        this.x = floatingActionButton;
        this.y = rTLImageView;
        this.z = imageView;
        this.A = imageView2;
        this.B = imageView3;
        this.C = constraintLayout;
        this.D = constraintLayout2;
        this.E = constraintLayout3;
        this.F = constraintLayout4;
        this.G = scrollView;
        this.H = flexibleTextView;
        this.I = flexibleTextView2;
        this.J = flexibleTextView3;
        this.K = flexibleTextView4;
        this.L = flexibleButton2;
        this.M = flexibleTextView5;
        this.N = flexibleTextView6;
    }
}
