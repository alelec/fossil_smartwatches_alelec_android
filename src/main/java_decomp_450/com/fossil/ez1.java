package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.SignInConfiguration;
import com.google.android.gms.auth.api.signin.internal.SignInHubActivity;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ez1 {
    @DexIgnore
    public static b92 a; // = new b92("GoogleSignInCommon", new String[0]);

    @DexIgnore
    public static Intent a(Context context, GoogleSignInOptions googleSignInOptions) {
        a.a("getSignInIntent()", new Object[0]);
        SignInConfiguration signInConfiguration = new SignInConfiguration(context.getPackageName(), googleSignInOptions);
        Intent intent = new Intent("com.google.android.gms.auth.GOOGLE_SIGN_IN");
        intent.setPackage(context.getPackageName());
        intent.setClass(context, SignInHubActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("config", signInConfiguration);
        intent.putExtra("config", bundle);
        return intent;
    }

    @DexIgnore
    public static Intent b(Context context, GoogleSignInOptions googleSignInOptions) {
        a.a("getFallbackSignInIntent()", new Object[0]);
        Intent a2 = a(context, googleSignInOptions);
        a2.setAction("com.google.android.gms.auth.APPAUTH_SIGN_IN");
        return a2;
    }

    @DexIgnore
    public static Intent c(Context context, GoogleSignInOptions googleSignInOptions) {
        a.a("getNoImplementationSignInIntent()", new Object[0]);
        Intent a2 = a(context, googleSignInOptions);
        a2.setAction("com.google.android.gms.auth.NO_IMPL");
        return a2;
    }

    @DexIgnore
    public static c12<Status> b(a12 a12, Context context, boolean z) {
        a.a("Revoking access", new Object[0]);
        String d = xy1.a(context).d();
        a(context);
        if (z) {
            return az1.a(d);
        }
        return a12.b(new hz1(a12));
    }

    @DexIgnore
    public static c12<Status> a(a12 a12, Context context, boolean z) {
        a.a("Signing out", new Object[0]);
        a(context);
        if (z) {
            return d12.a(Status.e, a12);
        }
        return a12.b(new fz1(a12));
    }

    @DexIgnore
    public static void a(Context context) {
        kz1.a(context).a();
        for (a12 a12 : a12.i()) {
            a12.h();
        }
        u12.d();
    }

    @DexIgnore
    public static uy1 a(Intent intent) {
        if (intent == null) {
            return null;
        }
        if (!intent.hasExtra("googleSignInStatus") && !intent.hasExtra("googleSignInAccount")) {
            return null;
        }
        GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) intent.getParcelableExtra("googleSignInAccount");
        Status status = (Status) intent.getParcelableExtra("googleSignInStatus");
        if (googleSignInAccount != null) {
            status = Status.e;
        }
        return new uy1(googleSignInAccount, status);
    }
}
