package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilNotificationImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d95 extends c95 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x;
    @DexIgnore
    public long v;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        x = sparseIntArray;
        sparseIntArray.put(2131362686, 1);
        x.put(2131362405, 2);
        x.put(2131362566, 3);
        x.put(2131362751, 4);
    }
    */

    @DexIgnore
    public d95(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 5, w, x));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public d95(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[2], (ImageButton) objArr[3], (FossilNotificationImageView) objArr[1], (View) objArr[4]);
        this.v = -1;
        ((c95) this).q.setTag(null);
        a(view);
        f();
    }
}
