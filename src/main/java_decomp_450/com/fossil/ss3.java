package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Build;
import android.view.View;
import android.view.ViewAnimationUtils;
import com.fossil.us3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ss3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ us3 a;

        @DexIgnore
        public a(us3 us3) {
            this.a = us3;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.a.b();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            this.a.a();
        }
    }

    @DexIgnore
    public static Animator a(us3 us3, float f, float f2, float f3) {
        ObjectAnimator ofObject = ObjectAnimator.ofObject(us3, us3.c.a, us3.b.b, new us3.e(f, f2, f3));
        if (Build.VERSION.SDK_INT < 21) {
            return ofObject;
        }
        us3.e revealInfo = us3.getRevealInfo();
        if (revealInfo != null) {
            Animator createCircularReveal = ViewAnimationUtils.createCircularReveal((View) us3, (int) f, (int) f2, revealInfo.c, f3);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(ofObject, createCircularReveal);
            return animatorSet;
        }
        throw new IllegalStateException("Caller must set a non-null RevealInfo before calling this.");
    }

    @DexIgnore
    public static Animator.AnimatorListener a(us3 us3) {
        return new a(us3);
    }
}
