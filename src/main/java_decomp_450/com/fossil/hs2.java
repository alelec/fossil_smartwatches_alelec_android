package com.fossil;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hs2 extends AbstractSet<Map.Entry<K, V>> {
    @DexIgnore
    public /* final */ /* synthetic */ ds2 a;

    @DexIgnore
    public hs2(ds2 ds2) {
        this.a = ds2;
    }

    @DexIgnore
    public final void clear() {
        this.a.clear();
    }

    @DexIgnore
    public final boolean contains(@NullableDecl Object obj) {
        Map zzb = this.a.zzb();
        if (zzb != null) {
            return zzb.entrySet().contains(obj);
        }
        if (obj instanceof Map.Entry) {
            Map.Entry entry = (Map.Entry) obj;
            int zzb2 = this.a.b(entry.getKey());
            if (zzb2 == -1 || !mr2.a(this.a.zzc[zzb2], entry.getValue())) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public final Iterator<Map.Entry<K, V>> iterator() {
        return this.a.zzf();
    }

    @DexIgnore
    public final boolean remove(@NullableDecl Object obj) {
        Map zzb = this.a.zzb();
        if (zzb != null) {
            return zzb.entrySet().remove(obj);
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        if (this.a.zza()) {
            return false;
        }
        int zzb2 = this.a.a();
        Object key = entry.getKey();
        Object value = entry.getValue();
        Object zzc = this.a.a;
        ds2 ds2 = this.a;
        int a2 = ks2.a(key, value, zzb2, zzc, ds2.zza, ds2.zzb, ds2.zzc);
        if (a2 == -1) {
            return false;
        }
        this.a.zza(a2, zzb2);
        ds2.zzd(this.a);
        this.a.zzc();
        return true;
    }

    @DexIgnore
    public final int size() {
        return this.a.size();
    }
}
