package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vg0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ba0 a;
    @DexIgnore
    public /* final */ ug0 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<vg0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public vg0 createFromParcel(Parcel parcel) {
            ba0 ba0 = ba0.values()[parcel.readInt()];
            Parcelable readParcelable = parcel.readParcelable(ug0.class.getClassLoader());
            if (readParcelable != null) {
                return new vg0(ba0, (ug0) readParcelable);
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public vg0[] newArray(int i) {
            return new vg0[i];
        }
    }

    @DexIgnore
    public vg0(ba0 ba0, ug0 ug0) {
        this.a = ba0;
        this.b = ug0;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(new JSONObject(), r51.d, yz0.a(this.a)), r51.g5, this.b.a());
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(vg0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            vg0 vg0 = (vg0) obj;
            return this.a == vg0.a && !(ee7.a(this.b, vg0.b) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.notification.reply_message.AppNotificationReplyMessageMapping");
    }

    @DexIgnore
    public final ba0 getNotificationType() {
        return this.a;
    }

    @DexIgnore
    public final ug0 getReplyMessageGroup() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + (this.a.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a.ordinal());
        parcel.writeParcelable(this.b, i);
    }
}
