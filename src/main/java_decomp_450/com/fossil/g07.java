package com.fossil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g07 {
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ Method b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public boolean d; // = true;

    @DexIgnore
    public g07(Object obj, Method method) {
        if (obj == null) {
            throw new NullPointerException("EventHandler target cannot be null.");
        } else if (method != null) {
            this.a = obj;
            this.b = method;
            method.setAccessible(true);
            this.c = ((method.hashCode() + 31) * 31) + obj.hashCode();
        } else {
            throw new NullPointerException("EventHandler method cannot be null.");
        }
    }

    @DexIgnore
    public void a() {
        this.d = false;
    }

    @DexIgnore
    public boolean b() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || g07.class != obj.getClass()) {
            return false;
        }
        g07 g07 = (g07) obj;
        if (!this.b.equals(g07.b) || this.a != g07.a) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return this.c;
    }

    @DexIgnore
    public String toString() {
        return "[EventHandler " + this.b + "]";
    }

    @DexIgnore
    public void a(Object obj) throws InvocationTargetException {
        if (this.d) {
            try {
                this.b.invoke(this.a, obj);
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            } catch (InvocationTargetException e2) {
                if (e2.getCause() instanceof Error) {
                    throw ((Error) e2.getCause());
                }
                throw e2;
            }
        } else {
            throw new IllegalStateException(toString() + " has been invalidated and can no longer handle events.");
        }
    }
}
