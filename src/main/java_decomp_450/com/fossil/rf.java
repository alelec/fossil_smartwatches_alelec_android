package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.Cif;
import com.fossil.ng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rf<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> {
    @DexIgnore
    public /* final */ Cif<T> a;
    @DexIgnore
    public /* final */ Cif.c<T> b; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Cif.c<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.Cif.c
        public void a(qf<T> qfVar, qf<T> qfVar2) {
            rf.this.a(qfVar2);
            rf.this.a(qfVar, qfVar2);
        }
    }

    @DexIgnore
    public rf(ng.d<T> dVar) {
        Cif<T> ifVar = new Cif<>(this, dVar);
        this.a = ifVar;
        ifVar.a(this.b);
    }

    @DexIgnore
    @Deprecated
    public void a(qf<T> qfVar) {
    }

    @DexIgnore
    public void a(qf<T> qfVar, qf<T> qfVar2) {
    }

    @DexIgnore
    public void b(qf<T> qfVar) {
        this.a.a(qfVar);
    }

    @DexIgnore
    public T getItem(int i) {
        return this.a.a(i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.a();
    }
}
