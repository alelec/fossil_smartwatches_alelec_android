package com.fossil;

import com.fossil.l60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ef1 extends fe7 implements gd7<je0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ ah1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ef1(ah1 ah1) {
        super(1);
        this.a = ah1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(je0 je0) {
        ke0 errorCode = je0.getErrorCode();
        if (errorCode == p60.SECRET_KEY_IS_REQUIRED || errorCode == p60.AUTHENTICATION_FAILED) {
            ah1 ah1 = this.a;
            km1 km1 = ah1.a;
            gb0 gb0 = new gb0(ah1.b.b);
            l60.b bVar = km1.v;
            if (bVar != null) {
                bVar.onEventReceived(km1, gb0);
            }
        }
        return i97.a;
    }
}
