package com.fossil;

import java.nio.charset.Charset;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oh0 {
    @DexIgnore
    public /* synthetic */ oh0(zd7 zd7) {
    }

    @DexIgnore
    public final byte[] a(JSONObject jSONObject) {
        String jSONObject2 = jSONObject.toString();
        ee7.a((Object) jSONObject2, "jsonFileContent.toString()");
        Charset c = b21.x.c();
        if (jSONObject2 != null) {
            byte[] bytes = jSONObject2.getBytes(c);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }
}
