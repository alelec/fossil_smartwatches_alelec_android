package com.fossil;

import com.fossil.ix;
import com.fossil.m00;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a00<Data> implements m00<byte[], Data> {
    @DexIgnore
    public /* final */ b<Data> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements n00<byte[], ByteBuffer> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.a00$a$a")
        /* renamed from: com.fossil.a00$a$a  reason: collision with other inner class name */
        public class C0006a implements b<ByteBuffer> {
            @DexIgnore
            public C0006a(a aVar) {
            }

            @DexIgnore
            @Override // com.fossil.a00.b
            public Class<ByteBuffer> getDataClass() {
                return ByteBuffer.class;
            }

            @DexIgnore
            @Override // com.fossil.a00.b
            public ByteBuffer a(byte[] bArr) {
                return ByteBuffer.wrap(bArr);
            }
        }

        @DexIgnore
        @Override // com.fossil.n00
        public m00<byte[], ByteBuffer> a(q00 q00) {
            return new a00(new C0006a(this));
        }
    }

    @DexIgnore
    public interface b<Data> {
        @DexIgnore
        Data a(byte[] bArr);

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<Data> implements ix<Data> {
        @DexIgnore
        public /* final */ byte[] a;
        @DexIgnore
        public /* final */ b<Data> b;

        @DexIgnore
        public c(byte[] bArr, b<Data> bVar) {
            this.a = bArr;
            this.b = bVar;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a(ew ewVar, ix.a<? super Data> aVar) {
            aVar.a((Object) this.b.a(this.a));
        }

        @DexIgnore
        @Override // com.fossil.ix
        public sw b() {
            return sw.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.ix
        public Class<Data> getDataClass() {
            return this.b.getDataClass();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements n00<byte[], InputStream> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements b<InputStream> {
            @DexIgnore
            public a(d dVar) {
            }

            @DexIgnore
            @Override // com.fossil.a00.b
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }

            @DexIgnore
            @Override // com.fossil.a00.b
            public InputStream a(byte[] bArr) {
                return new ByteArrayInputStream(bArr);
            }
        }

        @DexIgnore
        @Override // com.fossil.n00
        public m00<byte[], InputStream> a(q00 q00) {
            return new a00(new a(this));
        }
    }

    @DexIgnore
    public a00(b<Data> bVar) {
        this.a = bVar;
    }

    @DexIgnore
    public boolean a(byte[] bArr) {
        return true;
    }

    @DexIgnore
    public m00.a<Data> a(byte[] bArr, int i, int i2, ax axVar) {
        return new m00.a<>(new k50(bArr), new c(bArr, this.a));
    }
}
