package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOHeartRate;
import java.lang.reflect.Type;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vu4 {
    @DexIgnore
    public /* final */ Gson a; // = new Gson();
    @DexIgnore
    public /* final */ Type b; // = new b().getType();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<List<? extends GFitWOHeartRate>> {
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public final String a(List<GFitWOHeartRate> list) {
        ee7.b(list, "heartRates");
        if (list.isEmpty()) {
            return "";
        }
        try {
            String a2 = this.a.a(list, this.b);
            ee7.a((Object) a2, "mGson.toJson(heartRates, mType)");
            return a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toString: ");
            e.printStackTrace();
            sb.append(i97.a);
            local.e("GFitWOHeartRatesConverter", sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final List<GFitWOHeartRate> a(String str) {
        ee7.b(str, "data");
        if (str.length() == 0) {
            return w97.a();
        }
        try {
            Object a2 = this.a.a(str, this.b);
            ee7.a(a2, "mGson.fromJson(data, mType)");
            return (List) a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toListGFitWOHeartRate: ");
            e.printStackTrace();
            sb.append(i97.a);
            local.e("GFitWOHeartRatesConverter", sb.toString());
            return w97.a();
        }
    }
}
