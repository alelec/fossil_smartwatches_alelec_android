package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jy4 extends iy4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i N;
    @DexIgnore
    public static /* final */ SparseIntArray O;
    @DexIgnore
    public /* final */ ConstraintLayout L;
    @DexIgnore
    public long M;

    /*
    static {
        ViewDataBinding.i iVar = new ViewDataBinding.i(22);
        N = iVar;
        iVar.a(1, new String[]{"item_default_place_commute_time", "item_default_place_commute_time"}, new int[]{2, 3}, new int[]{2131558678, 2131558678});
        SparseIntArray sparseIntArray = new SparseIntArray();
        O = sparseIntArray;
        sparseIntArray.put(2131362563, 4);
        O.put(2131362517, 5);
        O.put(2131362045, 6);
        O.put(2131362730, 7);
        O.put(2131362632, 8);
        O.put(2131363365, 9);
        O.put(2131361902, 10);
        O.put(2131362743, 11);
        O.put(2131362125, 12);
        O.put(2131362755, 13);
        O.put(2131362711, 14);
        O.put(2131362751, 15);
        O.put(2131362333, 16);
        O.put(2131362785, 17);
        O.put(2131362999, 18);
        O.put(2131361917, 19);
        O.put(2131362769, 20);
        O.put(2131363018, 21);
    }
    */

    @DexIgnore
    public jy4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 22, N, O));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.M = 0;
        }
        ViewDataBinding.d(((iy4) this).x);
        ViewDataBinding.d(((iy4) this).y);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (((com.fossil.iy4) r6).y.e() == false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (((com.fossil.iy4) r6).x.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean e() {
        /*
            r6 = this;
            monitor-enter(r6)
            long r0 = r6.M     // Catch:{ all -> 0x0021 }
            r2 = 0
            r4 = 1
            int r5 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r5 == 0) goto L_0x000c
            monitor-exit(r6)     // Catch:{ all -> 0x0021 }
            return r4
        L_0x000c:
            monitor-exit(r6)     // Catch:{ all -> 0x0021 }
            com.fossil.a95 r0 = r6.x
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x0016
            return r4
        L_0x0016:
            com.fossil.a95 r0 = r6.y
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x001f
            return r4
        L_0x001f:
            r0 = 0
            return r0
        L_0x0021:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jy4.e():boolean");
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.M = 4;
        }
        ((iy4) this).x.f();
        ((iy4) this).y.f();
        g();
    }

    @DexIgnore
    public jy4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 2, (FlexibleAutoCompleteTextView) objArr[10], (View) objArr[19], (ConstraintLayout) objArr[6], (RTLImageView) objArr[12], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[5], (RTLImageView) objArr[4], (a95) objArr[2], (a95) objArr[3], (RTLImageView) objArr[8], (RTLImageView) objArr[14], (RTLImageView) objArr[7], (ConstraintLayout) objArr[11], (View) objArr[15], (ImageView) objArr[13], (LinearLayout) objArr[20], (LinearLayout) objArr[17], (ConstraintLayout) objArr[0], (RecyclerView) objArr[18], (FlexibleSwitchCompat) objArr[21], (View) objArr[9]);
        this.M = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[1];
        this.L = constraintLayout;
        constraintLayout.setTag(null);
        ((iy4) this).H.setTag(null);
        a(view);
        f();
    }
}
