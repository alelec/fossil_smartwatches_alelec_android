package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fw5 {
    @DexIgnore
    public /* final */ iv5 a;
    @DexIgnore
    public /* final */ zv5 b;

    @DexIgnore
    public fw5(iv5 iv5, zv5 zv5) {
        ee7.b(iv5, "mInActivityNudgeTimeContractView");
        ee7.b(zv5, "mRemindTimeContractView");
        this.a = iv5;
        this.b = zv5;
    }

    @DexIgnore
    public final iv5 a() {
        return this.a;
    }

    @DexIgnore
    public final zv5 b() {
        return this.b;
    }
}
