package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ni2 extends gi2 implements oi2 {
    @DexIgnore
    public ni2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitConfigApi");
    }

    @DexIgnore
    @Override // com.fossil.oi2
    public final void a(kd2 kd2) throws RemoteException {
        Parcel zza = zza();
        ej2.a(zza, kd2);
        a(22, zza);
    }
}
