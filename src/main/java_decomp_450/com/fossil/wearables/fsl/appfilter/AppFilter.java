package com.fossil.wearables.fsl.appfilter;

import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class AppFilter extends BaseFeatureModel {
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_FAMILY; // = "deviceFamily";
    @DexIgnore
    public static /* final */ String COLUMN_HOUR; // = "hour";
    @DexIgnore
    public static /* final */ String COLUMN_IS_VIBRATION_ONLY; // = "isVibrationOnly";
    @DexIgnore
    public static /* final */ String COMLUMN_TYPE; // = "type";
    @DexIgnore
    @DatabaseField
    public int deviceFamily;
    @DexIgnore
    @DatabaseField
    public String type;

    @DexIgnore
    public AppFilter() {
    }

    @DexIgnore
    public int getDeviceFamily() {
        return this.deviceFamily;
    }

    @DexIgnore
    public String getType() {
        return this.type;
    }

    @DexIgnore
    public void setDeviceFamily(int i) {
        this.deviceFamily = i;
    }

    @DexIgnore
    public void setType(String str) {
        this.type = str;
    }

    @DexIgnore
    public AppFilter(String str) {
        this.type = str;
    }
}
