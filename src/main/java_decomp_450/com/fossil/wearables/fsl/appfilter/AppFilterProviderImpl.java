package com.fossil.wearables.fsl.appfilter;

import android.content.Context;
import android.util.Log;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class AppFilterProviderImpl extends BaseDbProvider implements AppFilterProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "appfilter.db";

    @DexIgnore
    public AppFilterProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    public void clearAppFilterTable() {
        try {
            TableUtils.clearTable(getAppFilterDao().getConnectionSource(), AppFilter.class);
        } catch (Exception e) {
            String str = ((BaseDbProvider) this).TAG;
            Log.e(str, "clearAppFilterTable Exception=" + e);
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.appfilter.AppFilterProvider
    public List<AppFilter> getAllAppFilterVibration(int i) {
        try {
            QueryBuilder<AppFilter, Integer> queryBuilder = getAppFilterDao().queryBuilder();
            queryBuilder.where().eq(AppFilter.COLUMN_IS_VIBRATION_ONLY, true).and().eq("deviceFamily", Integer.valueOf(i));
            return queryBuilder.query();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList();
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.appfilter.AppFilterProvider
    public List<AppFilter> getAllAppFilters() {
        ArrayList arrayList = new ArrayList();
        try {
            return getAppFilterDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.appfilter.AppFilterProvider
    public List<AppFilter> getAllAppFiltersWithHour(int i, int i2) {
        try {
            QueryBuilder<AppFilter, Integer> queryBuilder = getAppFilterDao().queryBuilder();
            queryBuilder.where().eq(AppFilter.COLUMN_HOUR, Integer.valueOf(i)).and().eq("deviceFamily", Integer.valueOf(i2));
            return queryBuilder.query();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList();
        }
    }

    @DexIgnore
    public AppFilter getAppFilter(int i) {
        try {
            return getAppFilterDao().queryForId(Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public Dao<AppFilter, Integer> getAppFilterDao() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(AppFilter.class);
    }

    @DexIgnore
    public AppFilter getAppFilterMatchingType(String str) {
        try {
            QueryBuilder<AppFilter, Integer> queryBuilder = getAppFilterDao().queryBuilder();
            queryBuilder.where().eq("type", str).and().eq("enabled", true);
            List<AppFilter> query = getAppFilterDao().query(queryBuilder.prepare());
            if (query == null || query.size() <= 0) {
                return null;
            }
            return query.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{AppFilter.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    public String getDbPath() {
        return ((BaseDbProvider) this).databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 4;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.appfilter.AppFilterProvider
    public void removeAllAppFilters() {
        for (AppFilter appFilter : getAllAppFilters()) {
            removeAppFilter(appFilter);
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.appfilter.AppFilterProvider
    public void removeAppFilter(AppFilter appFilter) {
        if (appFilter != null) {
            try {
                getAppFilterDao().delete(appFilter);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.appfilter.AppFilterProvider
    public void saveAppFilter(AppFilter appFilter) {
        AppFilter appFilter2;
        if (appFilter != null) {
            try {
                QueryBuilder<AppFilter, Integer> queryBuilder = getAppFilterDao().queryBuilder();
                queryBuilder.where().eq("type", appFilter.getType()).and().eq("deviceFamily", Integer.valueOf(appFilter.getDeviceFamily()));
                List<AppFilter> query = getAppFilterDao().query(queryBuilder.prepare());
                if (!(query == null || query.size() <= 0 || (appFilter2 = query.get(0)) == null)) {
                    appFilter.setDbRowId(appFilter2.getDbRowId());
                }
                getAppFilterDao().createOrUpdate(appFilter);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.appfilter.AppFilterProvider
    public List<AppFilter> getAllAppFilters(int i) {
        ArrayList arrayList = new ArrayList();
        try {
            return getAppFilterDao().queryForEq("deviceFamily", Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.appfilter.AppFilterProvider
    public AppFilter getAppFilterMatchingType(String str, int i) {
        try {
            QueryBuilder<AppFilter, Integer> queryBuilder = getAppFilterDao().queryBuilder();
            queryBuilder.where().eq("deviceFamily", Integer.valueOf(i)).and().eq("type", str).and().eq("enabled", true);
            List<AppFilter> query = getAppFilterDao().query(queryBuilder.prepare());
            if (query == null || query.size() <= 0) {
                return null;
            }
            return query.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
