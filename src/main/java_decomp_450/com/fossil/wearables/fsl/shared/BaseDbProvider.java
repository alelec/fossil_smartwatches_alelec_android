package com.fossil.wearables.fsl.shared;

import android.content.Context;
import android.util.Log;
import com.baseflow.geolocator.utils.LocaleConverter;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseDbProvider {
    @DexIgnore
    public /* final */ String TAG; // = getClass().getCanonicalName();
    @DexIgnore
    public Context context;
    @DexIgnore
    public DatabaseHelper databaseHelper;
    @DexIgnore
    public boolean isCacheDirty; // = true;

    @DexIgnore
    public BaseDbProvider(Context context2, String str) {
        this.context = context2;
        this.databaseHelper = new DatabaseHelper(context2, str, getDbVersion(), getDbEntities(), getDbUpgrades());
    }

    @DexIgnore
    public abstract Class<?>[] getDbEntities();

    @DexIgnore
    public abstract Map<Integer, UpgradeCommand> getDbUpgrades();

    @DexIgnore
    public abstract int getDbVersion();

    @DexIgnore
    public void setCacheToDirty() {
        Log.d(this.TAG, "Setting cache to dirty");
        this.isCacheDirty = true;
    }

    @DexIgnore
    public BaseDbProvider(Context context2, String str, String str2) {
        this.databaseHelper = new DatabaseHelper(context2, str + LocaleConverter.LOCALE_DELIMITER + str2, getDbVersion(), getDbEntities(), getDbUpgrades());
    }
}
