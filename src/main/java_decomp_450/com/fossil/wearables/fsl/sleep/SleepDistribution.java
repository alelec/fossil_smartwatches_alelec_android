package com.fossil.wearables.fsl.sleep;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SleepDistribution {
    @DexIgnore
    public int awake;
    @DexIgnore
    public int deep;
    @DexIgnore
    public int light;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 extends TypeToken<SleepDistribution> {
    }

    @DexIgnore
    public SleepDistribution(int i, int i2, int i3) {
        this.awake = i;
        this.deep = i3;
        this.light = i2;
    }

    @DexIgnore
    public static SleepDistribution getSleepDistributionByString(String str) {
        return (SleepDistribution) new Gson().a(str, new Anon1().getType());
    }

    @DexIgnore
    public static String getStringSleepDistributionByArrayInt(int[] iArr) {
        if (iArr != null && iArr.length > 2) {
            return new SleepDistribution(iArr[0], iArr[1], iArr[2]).getSleepDistribution();
        }
        return "";
    }

    @DexIgnore
    public int[] getArrayDistribution() {
        return new int[]{this.awake, this.light, this.deep};
    }

    @DexIgnore
    public int getAwake() {
        return this.awake;
    }

    @DexIgnore
    public int getDeep() {
        return this.deep;
    }

    @DexIgnore
    public int getLight() {
        return this.light;
    }

    @DexIgnore
    public String getSleepDistribution() {
        return new Gson().a(this);
    }

    @DexIgnore
    public int getTotalMinuteBySleepDistribution() {
        return this.awake + this.light + this.deep;
    }

    @DexIgnore
    public void setAwake(int i) {
        this.awake = i;
    }

    @DexIgnore
    public void setDeep(int i) {
        this.deep = i;
    }

    @DexIgnore
    public void setLight(int i) {
        this.light = i;
    }

    @DexIgnore
    public static int getTotalMinuteBySleepDistribution(SleepDistribution sleepDistribution) {
        if (sleepDistribution == null) {
            return 0;
        }
        return sleepDistribution.getAwake() + sleepDistribution.getLight() + sleepDistribution.getDeep();
    }
}
