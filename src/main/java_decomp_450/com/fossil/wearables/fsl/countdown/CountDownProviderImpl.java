package com.fossil.wearables.fsl.countdown;

import android.content.Context;
import android.util.Log;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CountDownProviderImpl extends BaseDbProvider implements CountDownProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "countdown.db";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {
        @DexIgnore
        public Anon1() {
        }
    }

    @DexIgnore
    public CountDownProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<CountDown, Long> getCountDownDao() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(CountDown.class);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
        return;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x000c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void clearData() {
        /*
            r2 = this;
            java.lang.Class<com.fossil.wearables.fsl.countdown.CountDown> r0 = com.fossil.wearables.fsl.countdown.CountDown.class
            com.fossil.wearables.fsl.shared.DatabaseHelper r1 = r2.databaseHelper     // Catch:{ Exception -> 0x000c }
            com.j256.ormlite.support.ConnectionSource r1 = r1.getConnectionSource()     // Catch:{ Exception -> 0x000c }
            com.j256.ormlite.table.TableUtils.clearTable(r1, r0)     // Catch:{ Exception -> 0x000c }
            goto L_0x001c
        L_0x000c:
            com.fossil.wearables.fsl.shared.DatabaseHelper r1 = r2.databaseHelper     // Catch:{ Exception -> 0x0015 }
            com.j256.ormlite.support.ConnectionSource r1 = r1.getConnectionSource()     // Catch:{ Exception -> 0x0015 }
            com.j256.ormlite.table.TableUtils.createTableIfNotExists(r1, r0)     // Catch:{ Exception -> 0x0015 }
        L_0x0015:
            java.lang.String r0 = r2.TAG
            java.lang.String r1 = "Failed to clear data"
            android.util.Log.e(r0, r1)
        L_0x001c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wearables.fsl.countdown.CountDownProviderImpl.clearData():void");
    }

    @DexIgnore
    public void deleteCountDown(String str) {
        try {
            DeleteBuilder<CountDown, Long> deleteBuilder = getCountDownDao().deleteBuilder();
            deleteBuilder.where().eq("uri", str);
            getCountDownDao().delete(deleteBuilder.prepare());
        } catch (Exception e) {
            String str2 = ((BaseDbProvider) this).TAG;
            Log.e(str2, "Error inside " + ((BaseDbProvider) this).TAG + ".deleteCountDown - e=" + e);
        }
    }

    @DexIgnore
    public void dropThenCreateTables() {
        try {
            ((BaseDbProvider) this).databaseHelper.dropTables(getDbEntities());
            TableUtils.createTableIfNotExists(((BaseDbProvider) this).databaseHelper.getConnectionSource(), CountDown.class);
        } catch (Exception unused) {
            Log.e(((BaseDbProvider) this).TAG, "Failed to drop tables");
        }
    }

    @DexIgnore
    public CountDown getActiveCountDown() {
        try {
            QueryBuilder<CountDown, Long> queryBuilder = getCountDownDao().queryBuilder();
            queryBuilder.where().eq("status", Integer.valueOf(CountDownStatus.ACTIVE.getValue()));
            return getCountDownDao().queryForFirst(queryBuilder.prepare());
        } catch (Exception e) {
            String str = ((BaseDbProvider) this).TAG;
            Log.e(str, "Error inside " + ((BaseDbProvider) this).TAG + ".getActiveCountDown - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public List<CountDown> getAllCountDown() {
        try {
            return getCountDownDao().queryForAll();
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public CountDown getCountDown(String str, long j) {
        try {
            QueryBuilder<CountDown, Long> queryBuilder = getCountDownDao().queryBuilder();
            queryBuilder.where().eq("name", str).and().eq(CountDown.COLUMN_ENDED_AT, Long.valueOf(j));
            return getCountDownDao().queryForFirst(queryBuilder.prepare());
        } catch (Exception e) {
            String str2 = ((BaseDbProvider) this).TAG;
            Log.e(str2, "Error inside " + ((BaseDbProvider) this).TAG + ".getCountDown - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public CountDown getCountDownByClientId(String str) {
        try {
            QueryBuilder<CountDown, Long> queryBuilder = getCountDownDao().queryBuilder();
            queryBuilder.where().eq("uri", str);
            return getCountDownDao().queryForFirst(queryBuilder.prepare());
        } catch (Exception e) {
            String str2 = ((BaseDbProvider) this).TAG;
            Log.e(str2, "Error inside " + ((BaseDbProvider) this).TAG + ".getCountDownByClientId - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public CountDown getCountDownById(long j) {
        try {
            QueryBuilder<CountDown, Long> queryBuilder = getCountDownDao().queryBuilder();
            queryBuilder.where().eq("id", Long.valueOf(j));
            return getCountDownDao().queryForFirst(queryBuilder.prepare());
        } catch (Exception e) {
            String str = ((BaseDbProvider) this).TAG;
            Log.e(str, "Error inside " + ((BaseDbProvider) this).TAG + ".getCountDownByClientId - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public CountDown getCountDownByServerId(String str) {
        try {
            QueryBuilder<CountDown, Long> queryBuilder = getCountDownDao().queryBuilder();
            queryBuilder.where().eq("serverId", str);
            return getCountDownDao().queryForFirst(queryBuilder.prepare());
        } catch (Exception e) {
            String str2 = ((BaseDbProvider) this).TAG;
            Log.e(str2, "Error inside " + ((BaseDbProvider) this).TAG + ".getCountDown - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public List<CountDown> getCountDownsByStatus(int i) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<CountDown, Long> queryBuilder = getCountDownDao().queryBuilder();
            queryBuilder.orderBy("updatedAt", false);
            queryBuilder.orderBy("createdAt", false);
            queryBuilder.where().eq("status", Integer.valueOf(i));
            List<CountDown> query = getCountDownDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            String str = ((BaseDbProvider) this).TAG;
            Log.e(str, "Error inside " + ((BaseDbProvider) this).TAG + ".getPastCountDownsPaging - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{CountDown.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    public List<CountDown> getPastCountDownsPaging(long j, long j2) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<CountDown, Long> queryBuilder = getCountDownDao().queryBuilder();
            queryBuilder.where().eq("status", Integer.valueOf(CountDownStatus.COMPLETED.getValue())).or().eq("status", Integer.valueOf(CountDownStatus.INACTIVE.getValue()));
            queryBuilder.orderBy("updatedAt", false);
            queryBuilder.orderBy("createdAt", false);
            queryBuilder.offset(Long.valueOf(j));
            queryBuilder.limit(Long.valueOf(j2));
            List<CountDown> query = getCountDownDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            String str = ((BaseDbProvider) this).TAG;
            Log.e(str, "Error inside " + ((BaseDbProvider) this).TAG + ".getPastCountDownsPaging - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    public void saveCountDown(CountDown countDown) {
        try {
            CountDown countDownByClientId = getCountDownByClientId(countDown.getUri());
            if (countDownByClientId != null) {
                countDown.setUpdatedAt(System.currentTimeMillis());
                countDown.setId(countDownByClientId.getId());
            } else {
                countDown.setCreatedAt(System.currentTimeMillis());
            }
            getCountDownDao().createOrUpdate(countDown);
        } catch (Exception e) {
            String str = ((BaseDbProvider) this).TAG;
            Log.e(str, "Error inside " + ((BaseDbProvider) this).TAG + ".saveCountDown - e=" + e);
        }
    }

    @DexIgnore
    public void setCountDownStatus(String str, CountDownStatus countDownStatus) {
        try {
            CountDown countDownByClientId = getCountDownByClientId(str);
            if (countDownByClientId != null) {
                countDownByClientId.setStatus(countDownStatus);
                saveCountDown(countDownByClientId);
            }
        } catch (Exception e) {
            String str2 = ((BaseDbProvider) this).TAG;
            Log.e(str2, "Error inside " + ((BaseDbProvider) this).TAG + ".setCountDownStatus - e=" + e);
        }
    }

    @DexIgnore
    public void deleteCountDown(long j) {
        try {
            DeleteBuilder<CountDown, Long> deleteBuilder = getCountDownDao().deleteBuilder();
            deleteBuilder.where().eq("id", Long.valueOf(j));
            getCountDownDao().delete(deleteBuilder.prepare());
        } catch (Exception e) {
            String str = ((BaseDbProvider) this).TAG;
            Log.e(str, "Error inside " + ((BaseDbProvider) this).TAG + ".deleteCountDown - e=" + e);
        }
    }
}
