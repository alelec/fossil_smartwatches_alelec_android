package com.fossil.wearables.fsl.countdown;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum CountDownStatus {
    REMOVED(-1),
    INACTIVE(0),
    ACTIVE(1),
    COMPLETED(2);
    
    @DexIgnore
    public int value;

    @DexIgnore
    public CountDownStatus(int i) {
        this.value = i;
    }

    @DexIgnore
    public static CountDownStatus fromInt(int i) {
        CountDownStatus[] values = values();
        for (CountDownStatus countDownStatus : values) {
            if (countDownStatus.getValue() == i) {
                return countDownStatus;
            }
        }
        return ACTIVE;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
