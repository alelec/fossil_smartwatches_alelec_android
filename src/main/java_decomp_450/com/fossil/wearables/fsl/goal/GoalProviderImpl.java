package com.fossil.wearables.fsl.goal;

import android.content.Context;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class GoalProviderImpl extends BaseDbProvider implements GoalProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "goal.db";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {
        @DexIgnore
        public Anon1() {
        }
    }

    @DexIgnore
    public GoalProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<Goal, Integer> getGoalDao() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(Goal.class);
    }

    @DexIgnore
    private Dao<GoalInterval, Integer> getGoalIntervalDao() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(GoalInterval.class);
    }

    @DexIgnore
    public List<Goal> getAllGoals() {
        ArrayList arrayList = new ArrayList();
        try {
            return getGoalDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{Goal.class, GoalInterval.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    public String getDbPath() {
        return ((BaseDbProvider) this).databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    public Goal getGoal(int i) {
        try {
            return getGoalDao().queryForId(Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public void removeGoal(Goal goal) {
        if (goal != null) {
            try {
                for (GoalInterval goalInterval : goal.getIntervals()) {
                    removeGoalInterval(goalInterval);
                }
                getGoalDao().delete(goal);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void removeGoalInterval(GoalInterval goalInterval) {
        try {
            getGoalIntervalDao().delete(goalInterval);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    public void saveGoal(Goal goal) {
        if (goal != null) {
            try {
                Goal queryForSameId = getGoalDao().queryForSameId(goal);
                if (queryForSameId != null) {
                    goal.setDbRowId(queryForSameId.getDbRowId());
                }
                getGoalDao().createOrUpdate(goal);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void saveGoalInterval(GoalInterval goalInterval) {
        if (goalInterval != null) {
            try {
                GoalInterval queryForSameId = getGoalIntervalDao().queryForSameId(goalInterval);
                if (queryForSameId != null) {
                    goalInterval.setDbRowId(queryForSameId.getDbRowId());
                }
                getGoalIntervalDao().createOrUpdate(goalInterval);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public List<Goal> getAllGoals(long j) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<Goal, Integer> queryBuilder = getGoalDao().queryBuilder();
            queryBuilder.where().le(GoalPhase.COLUMN_START_DATE, Long.valueOf(j)).and().ge(GoalPhase.COLUMN_END_DATE, Long.valueOf(j));
            return getGoalDao().query(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }
}
