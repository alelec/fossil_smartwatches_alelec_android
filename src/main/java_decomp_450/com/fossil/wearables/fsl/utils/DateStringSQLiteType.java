package com.fossil.wearables.fsl.utils;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.BaseDateType;
import com.j256.ormlite.field.types.DateStringType;
import com.j256.ormlite.misc.SqlExceptionUtil;
import com.j256.ormlite.support.DatabaseResults;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DateStringSQLiteType extends DateStringType {
    @DexIgnore
    public static /* final */ ExtendDateStringFormatConfig dateFormatConfig; // = new ExtendDateStringFormatConfig("yyyy-MM-dd HH:mm:ss.SSSSS");
    @DexIgnore
    public static /* final */ DateStringSQLiteType singleTon; // = new DateStringSQLiteType();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class ExtendDateStringFormatConfig extends BaseDateType.DateStringFormatConfig {
        @DexIgnore
        public /* final */ String exDateFormatStr;
        @DexIgnore
        public /* final */ ThreadLocal<DateFormat> exThreadLocal; // = new Anon1();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1 extends ThreadLocal<DateFormat> {
            @DexIgnore
            public Anon1() {
            }

            @DexIgnore
            @Override // java.lang.ThreadLocal
            public DateFormat initialValue() {
                return new SimpleDateFormat(ExtendDateStringFormatConfig.this.exDateFormatStr, Locale.US);
            }
        }

        @DexIgnore
        public ExtendDateStringFormatConfig(String str) {
            super(str);
            this.exDateFormatStr = str;
        }

        @DexIgnore
        @Override // com.j256.ormlite.field.types.BaseDateType.DateStringFormatConfig
        public DateFormat getDateFormat() {
            return this.exThreadLocal.get();
        }

        @DexIgnore
        @Override // com.j256.ormlite.field.types.BaseDateType.DateStringFormatConfig
        public String toString() {
            return this.exDateFormatStr;
        }
    }

    @DexIgnore
    public DateStringSQLiteType() {
        super(SqlType.STRING, new Class[0]);
    }

    @DexIgnore
    @Override // com.j256.ormlite.field.types.DateStringType
    public static DateStringSQLiteType getSingleton() {
        return singleTon;
    }

    @DexIgnore
    @Override // com.j256.ormlite.field.types.DateStringType, com.j256.ormlite.field.BaseFieldConverter, com.j256.ormlite.field.FieldConverter
    public Object javaToSqlArg(FieldType fieldType, Object obj) {
        DateFormat dateFormat = BaseDateType.convertDateStringConfig(fieldType, dateFormatConfig).getDateFormat();
        dateFormat.setTimeZone(TimeZone.getDefault());
        return dateFormat.format((Date) obj);
    }

    @DexIgnore
    @Override // com.j256.ormlite.field.types.DateStringType, com.j256.ormlite.field.types.BaseDataType, com.j256.ormlite.field.DataPersister
    public Object makeConfigObject(FieldType fieldType) {
        String format = fieldType.getFormat();
        if (format != null) {
            return new ExtendDateStringFormatConfig(format);
        }
        dateFormatConfig.getDateFormat().setTimeZone(TimeZone.getDefault());
        return dateFormatConfig;
    }

    @DexIgnore
    @Override // com.j256.ormlite.field.types.DateStringType, com.j256.ormlite.field.FieldConverter
    public Object parseDefaultString(FieldType fieldType, String str) throws SQLException {
        BaseDateType.DateStringFormatConfig convertDateStringConfig = BaseDateType.convertDateStringConfig(fieldType, dateFormatConfig);
        try {
            convertDateStringConfig.getDateFormat().setTimeZone(TimeZone.getDefault());
            return BaseDateType.normalizeDateString(convertDateStringConfig, str);
        } catch (ParseException e) {
            throw SqlExceptionUtil.create("Problems with field " + fieldType + " parsing default date-string '" + str + "' using '" + convertDateStringConfig + "'", e);
        }
    }

    @DexIgnore
    @Override // com.j256.ormlite.field.types.DateStringType, com.j256.ormlite.field.FieldConverter
    public Object resultToSqlArg(FieldType fieldType, DatabaseResults databaseResults, int i) throws SQLException {
        return databaseResults.getString(i);
    }

    @DexIgnore
    @Override // com.j256.ormlite.field.types.DateStringType, com.j256.ormlite.field.BaseFieldConverter
    public Object sqlArgToJava(FieldType fieldType, Object obj, int i) throws SQLException {
        String str = (String) obj;
        BaseDateType.DateStringFormatConfig convertDateStringConfig = BaseDateType.convertDateStringConfig(fieldType, dateFormatConfig);
        try {
            convertDateStringConfig.getDateFormat().setTimeZone(TimeZone.getDefault());
            return BaseDateType.parseDateString(convertDateStringConfig, str);
        } catch (ParseException e) {
            throw SqlExceptionUtil.create("Problems with column " + i + " parsing date-string '" + str + "' using '" + convertDateStringConfig + "'", e);
        }
    }
}
