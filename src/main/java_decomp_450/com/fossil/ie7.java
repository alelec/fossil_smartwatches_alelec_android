package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ie7 extends he7 {
    @DexIgnore
    public /* final */ String name;
    @DexIgnore
    public /* final */ uf7 owner;
    @DexIgnore
    public /* final */ String signature;

    @DexIgnore
    public ie7(uf7 uf7, String str, String str2) {
        this.owner = uf7;
        this.name = str;
        this.signature = str2;
    }

    @DexIgnore
    @Override // com.fossil.bg7
    public Object get(Object obj) {
        return getGetter().call(obj);
    }

    @DexIgnore
    @Override // com.fossil.sf7, com.fossil.vd7
    public String getName() {
        return this.name;
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public uf7 getOwner() {
        return this.owner;
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public String getSignature() {
        return this.signature;
    }

    @DexIgnore
    public void set(Object obj, Object obj2) {
        getSetter().call(obj, obj2);
    }
}
