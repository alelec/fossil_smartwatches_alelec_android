package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.fossil.c17;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u07 {
    @DexIgnore
    public /* final */ b a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ ExecutorService c;
    @DexIgnore
    public /* final */ Downloader d;
    @DexIgnore
    public /* final */ Map<String, o07> e; // = new LinkedHashMap();
    @DexIgnore
    public /* final */ Map<Object, m07> f; // = new WeakHashMap();
    @DexIgnore
    public /* final */ Map<Object, m07> g; // = new WeakHashMap();
    @DexIgnore
    public /* final */ Set<Object> h; // = new HashSet();
    @DexIgnore
    public /* final */ Handler i; // = new a(this.a.getLooper(), this);
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public /* final */ p07 k;
    @DexIgnore
    public /* final */ k17 l;
    @DexIgnore
    public /* final */ List<o07> m;
    @DexIgnore
    public /* final */ c n;
    @DexIgnore
    public /* final */ boolean o;
    @DexIgnore
    public boolean p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Handler {
        @DexIgnore
        public /* final */ u07 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.u07$a$a")
        /* renamed from: com.fossil.u07$a$a  reason: collision with other inner class name */
        public class RunnableC0191a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Message a;

            @DexIgnore
            public RunnableC0191a(a aVar, Message message) {
                this.a = message;
            }

            @DexIgnore
            public void run() {
                throw new AssertionError("Unknown handler message received: " + this.a.what);
            }
        }

        @DexIgnore
        public a(Looper looper, u07 u07) {
            super(looper);
            this.a = u07;
        }

        @DexIgnore
        public void handleMessage(Message message) {
            boolean z = false;
            switch (message.what) {
                case 1:
                    this.a.e((m07) message.obj);
                    return;
                case 2:
                    this.a.d((m07) message.obj);
                    return;
                case 3:
                case 8:
                default:
                    Picasso.p.post(new RunnableC0191a(this, message));
                    return;
                case 4:
                    this.a.f((o07) message.obj);
                    return;
                case 5:
                    this.a.g((o07) message.obj);
                    return;
                case 6:
                    this.a.a((o07) message.obj, false);
                    return;
                case 7:
                    this.a.b();
                    return;
                case 9:
                    this.a.b((NetworkInfo) message.obj);
                    return;
                case 10:
                    u07 u07 = this.a;
                    if (message.arg1 == 1) {
                        z = true;
                    }
                    u07.b(z);
                    return;
                case 11:
                    this.a.a(message.obj);
                    return;
                case 12:
                    this.a.b(message.obj);
                    return;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends HandlerThread {
        @DexIgnore
        public b() {
            super("Picasso-Dispatcher", 10);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ u07 a;

        @DexIgnore
        public c(u07 u07) {
            this.a = u07;
        }

        @DexIgnore
        public void a() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.AIRPLANE_MODE");
            if (this.a.o) {
                intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            }
            this.a.b.registerReceiver(this, intentFilter);
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if ("android.intent.action.AIRPLANE_MODE".equals(action)) {
                    if (intent.hasExtra("state")) {
                        this.a.a(intent.getBooleanExtra("state", false));
                    }
                } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
                    this.a.a(((ConnectivityManager) o17.a(context, "connectivity")).getActiveNetworkInfo());
                }
            }
        }
    }

    @DexIgnore
    public u07(Context context, ExecutorService executorService, Handler handler, Downloader downloader, p07 p07, k17 k17) {
        b bVar = new b();
        this.a = bVar;
        bVar.start();
        o17.a(this.a.getLooper());
        this.b = context;
        this.c = executorService;
        this.d = downloader;
        this.j = handler;
        this.k = p07;
        this.l = k17;
        this.m = new ArrayList(4);
        this.p = o17.d(this.b);
        this.o = o17.b(context, "android.permission.ACCESS_NETWORK_STATE");
        c cVar = new c(this);
        this.n = cVar;
        cVar.a();
    }

    @DexIgnore
    public void a(m07 m07) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(2, m07));
    }

    @DexIgnore
    public void b(m07 m07) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(1, m07));
    }

    @DexIgnore
    public void c(o07 o07) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(6, o07));
    }

    @DexIgnore
    public void d(o07 o07) {
        Handler handler = this.i;
        handler.sendMessageDelayed(handler.obtainMessage(5, o07), 500);
    }

    @DexIgnore
    public void e(m07 m07) {
        a(m07, true);
    }

    @DexIgnore
    public void f(o07 o07) {
        if (a17.shouldWriteToMemoryCache(o07.i())) {
            this.k.a(o07.g(), o07.l());
        }
        this.e.remove(o07.g());
        a(o07);
        if (o07.j().n) {
            o17.a("Dispatcher", "batched", o17.a(o07), "for completion");
        }
    }

    @DexIgnore
    public void g(o07 o07) {
        if (!o07.n()) {
            boolean z = false;
            if (this.c.isShutdown()) {
                a(o07, false);
                return;
            }
            NetworkInfo networkInfo = null;
            if (this.o) {
                networkInfo = ((ConnectivityManager) o17.a(this.b, "connectivity")).getActiveNetworkInfo();
            }
            boolean z2 = networkInfo != null && networkInfo.isConnected();
            boolean a2 = o07.a(this.p, networkInfo);
            boolean o2 = o07.o();
            if (!a2) {
                if (this.o && o2) {
                    z = true;
                }
                a(o07, z);
                if (z) {
                    e(o07);
                }
            } else if (!this.o || z2) {
                if (o07.j().n) {
                    o17.a("Dispatcher", "retrying", o17.a(o07));
                }
                if (o07.f() instanceof c17.a) {
                    o07.i |= b17.NO_CACHE.index;
                }
                o07.s = this.c.submit(o07);
            } else {
                a(o07, o2);
                if (o2) {
                    e(o07);
                }
            }
        }
    }

    @DexIgnore
    public void a(NetworkInfo networkInfo) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(9, networkInfo));
    }

    @DexIgnore
    public void b(o07 o07) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(4, o07));
    }

    @DexIgnore
    public final void c(m07 m07) {
        Object j2 = m07.j();
        if (j2 != null) {
            m07.k = true;
            this.f.put(j2, m07);
        }
    }

    @DexIgnore
    public void d(m07 m07) {
        String c2 = m07.c();
        o07 o07 = this.e.get(c2);
        if (o07 != null) {
            o07.b(m07);
            if (o07.a()) {
                this.e.remove(c2);
                if (m07.f().n) {
                    o17.a("Dispatcher", "canceled", m07.h().d());
                }
            }
        }
        if (this.h.contains(m07.i())) {
            this.g.remove(m07.j());
            if (m07.f().n) {
                o17.a("Dispatcher", "canceled", m07.h().d(), "because paused request got canceled");
            }
        }
        m07 remove = this.f.remove(m07.j());
        if (remove != null && remove.f().n) {
            o17.a("Dispatcher", "canceled", remove.h().d(), "from replaying");
        }
    }

    @DexIgnore
    public final void e(o07 o07) {
        m07 c2 = o07.c();
        if (c2 != null) {
            c(c2);
        }
        List<m07> d2 = o07.d();
        if (d2 != null) {
            int size = d2.size();
            for (int i2 = 0; i2 < size; i2++) {
                c(d2.get(i2));
            }
        }
    }

    @DexIgnore
    public void a(boolean z) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(10, z ? 1 : 0, 0));
    }

    @DexIgnore
    public void b(Object obj) {
        if (this.h.remove(obj)) {
            ArrayList arrayList = null;
            Iterator<m07> it = this.g.values().iterator();
            while (it.hasNext()) {
                m07 next = it.next();
                if (next.i().equals(obj)) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(next);
                    it.remove();
                }
            }
            if (arrayList != null) {
                Handler handler = this.j;
                handler.sendMessage(handler.obtainMessage(13, arrayList));
            }
        }
    }

    @DexIgnore
    public void a(m07 m07, boolean z) {
        if (this.h.contains(m07.i())) {
            this.g.put(m07.j(), m07);
            if (m07.f().n) {
                String d2 = m07.b.d();
                o17.a("Dispatcher", "paused", d2, "because tag '" + m07.i() + "' is paused");
                return;
            }
            return;
        }
        o07 o07 = this.e.get(m07.c());
        if (o07 != null) {
            o07.a(m07);
        } else if (!this.c.isShutdown()) {
            o07 a2 = o07.a(m07.f(), this, this.k, this.l, m07);
            a2.s = this.c.submit(a2);
            this.e.put(m07.c(), a2);
            if (z) {
                this.f.remove(m07.j());
            }
            if (m07.f().n) {
                o17.a("Dispatcher", "enqueued", m07.b.d());
            }
        } else if (m07.f().n) {
            o17.a("Dispatcher", "ignored", m07.b.d(), "because shut down");
        }
    }

    @DexIgnore
    public void b() {
        ArrayList arrayList = new ArrayList(this.m);
        this.m.clear();
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(8, arrayList));
        a((List<o07>) arrayList);
    }

    @DexIgnore
    public void b(boolean z) {
        this.p = z;
    }

    @DexIgnore
    public void b(NetworkInfo networkInfo) {
        ExecutorService executorService = this.c;
        if (executorService instanceof f17) {
            ((f17) executorService).a(networkInfo);
        }
        if (networkInfo != null && networkInfo.isConnected()) {
            a();
        }
    }

    @DexIgnore
    public void a(Object obj) {
        if (this.h.add(obj)) {
            Iterator<o07> it = this.e.values().iterator();
            while (it.hasNext()) {
                o07 next = it.next();
                boolean z = next.j().n;
                m07 c2 = next.c();
                List<m07> d2 = next.d();
                boolean z2 = d2 != null && !d2.isEmpty();
                if (c2 != null || z2) {
                    if (c2 != null && c2.i().equals(obj)) {
                        next.b(c2);
                        this.g.put(c2.j(), c2);
                        if (z) {
                            o17.a("Dispatcher", "paused", c2.b.d(), "because tag '" + obj + "' was paused");
                        }
                    }
                    if (z2) {
                        for (int size = d2.size() - 1; size >= 0; size--) {
                            m07 m07 = d2.get(size);
                            if (m07.i().equals(obj)) {
                                next.b(m07);
                                this.g.put(m07.j(), m07);
                                if (z) {
                                    o17.a("Dispatcher", "paused", m07.b.d(), "because tag '" + obj + "' was paused");
                                }
                            }
                        }
                    }
                    if (next.a()) {
                        it.remove();
                        if (z) {
                            o17.a("Dispatcher", "canceled", o17.a(next), "all actions paused");
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    public void a(o07 o07, boolean z) {
        if (o07.j().n) {
            String a2 = o17.a(o07);
            StringBuilder sb = new StringBuilder();
            sb.append("for error");
            sb.append(z ? " (will replay)" : "");
            o17.a("Dispatcher", "batched", a2, sb.toString());
        }
        this.e.remove(o07.g());
        a(o07);
    }

    @DexIgnore
    public final void a() {
        if (!this.f.isEmpty()) {
            Iterator<m07> it = this.f.values().iterator();
            while (it.hasNext()) {
                m07 next = it.next();
                it.remove();
                if (next.f().n) {
                    o17.a("Dispatcher", "replaying", next.h().d());
                }
                a(next, false);
            }
        }
    }

    @DexIgnore
    public final void a(o07 o07) {
        if (!o07.n()) {
            this.m.add(o07);
            if (!this.i.hasMessages(7)) {
                this.i.sendEmptyMessageDelayed(7, 200);
            }
        }
    }

    @DexIgnore
    public final void a(List<o07> list) {
        if (list != null && !list.isEmpty() && list.get(0).j().n) {
            StringBuilder sb = new StringBuilder();
            for (o07 o07 : list) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(o17.a(o07));
            }
            o17.a("Dispatcher", "delivered", sb.toString());
        }
    }
}
