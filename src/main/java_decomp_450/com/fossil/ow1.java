package com.fossil;

import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ow1 implements Factory<nw1> {
    @DexIgnore
    public /* final */ Provider<Executor> a;
    @DexIgnore
    public /* final */ Provider<sw1> b;
    @DexIgnore
    public /* final */ Provider<pw1> c;
    @DexIgnore
    public /* final */ Provider<ay1> d;

    @DexIgnore
    public ow1(Provider<Executor> provider, Provider<sw1> provider2, Provider<pw1> provider3, Provider<ay1> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static ow1 a(Provider<Executor> provider, Provider<sw1> provider2, Provider<pw1> provider3, Provider<ay1> provider4) {
        return new ow1(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public nw1 get() {
        return new nw1(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
