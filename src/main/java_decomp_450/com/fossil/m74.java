package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class m74 implements ht1 {
    @DexIgnore
    public /* final */ oo3 a;
    @DexIgnore
    public /* final */ b44 b;

    @DexIgnore
    public m74(oo3 oo3, b44 b44) {
        this.a = oo3;
        this.b = b44;
    }

    @DexIgnore
    public static ht1 a(oo3 oo3, b44 b44) {
        return new m74(oo3, b44);
    }

    @DexIgnore
    @Override // com.fossil.ht1
    public void a(Exception exc) {
        o74.a(this.a, this.b, exc);
    }
}
