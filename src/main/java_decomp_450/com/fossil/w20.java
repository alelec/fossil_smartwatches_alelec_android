package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w20 implements ex<t20> {
    @DexIgnore
    public /* final */ ex<Bitmap> b;

    @DexIgnore
    public w20(ex<Bitmap> exVar) {
        u50.a(exVar);
        this.b = exVar;
    }

    @DexIgnore
    @Override // com.fossil.ex
    public uy<t20> a(Context context, uy<t20> uyVar, int i, int i2) {
        t20 t20 = uyVar.get();
        uy<Bitmap> k10 = new k10(t20.e(), aw.a(context).c());
        uy<Bitmap> a = this.b.a(context, k10, i, i2);
        if (!k10.equals(a)) {
            k10.b();
        }
        t20.a(this.b, a.get());
        return uyVar;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public boolean equals(Object obj) {
        if (obj instanceof w20) {
            return this.b.equals(((w20) obj).b);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.yw
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }
}
