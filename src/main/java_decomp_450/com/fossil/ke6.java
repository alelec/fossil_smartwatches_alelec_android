package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.af6;
import com.fossil.be5;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ke6 extends go5 implements je6, View.OnClickListener, af6.b {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public /* final */ Calendar f; // = Calendar.getInstance();
    @DexIgnore
    public af6 g;
    @DexIgnore
    public Date h; // = new Date();
    @DexIgnore
    public qw6<mw4> i;
    @DexIgnore
    public ie6 j;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public HashMap y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ke6 a(Date date) {
            ee7.b(date, "date");
            ke6 ke6 = new ke6();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            ke6.setArguments(bundle);
            return ke6;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends AppBarLayout.Behavior.a {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ qf b;

        @DexIgnore
        public b(ke6 ke6, boolean z, qf qfVar, ob5 ob5) {
            this.a = z;
            this.b = qfVar;
        }

        @DexIgnore
        @Override // com.google.android.material.appbar.AppBarLayout.BaseBehavior.b
        public boolean a(AppBarLayout appBarLayout) {
            ee7.b(appBarLayout, "appBarLayout");
            return this.a && (this.b.isEmpty() ^ true);
        }
    }

    @DexIgnore
    public ke6() {
        String b2 = eh5.l.a().b("nonBrandSurface");
        String str = "#FFFFFF";
        this.s = Color.parseColor(b2 == null ? str : b2);
        String b3 = eh5.l.a().b("backgroundDashboard");
        this.t = Color.parseColor(b3 == null ? str : b3);
        String b4 = eh5.l.a().b("secondaryText");
        this.u = Color.parseColor(b4 == null ? str : b4);
        String b5 = eh5.l.a().b("primaryText");
        this.v = Color.parseColor(b5 == null ? str : b5);
        String b6 = eh5.l.a().b("nonBrandDisableCalendarDay");
        this.w = Color.parseColor(b6 == null ? str : b6);
        String b7 = eh5.l.a().b("nonBrandNonReachGoal");
        this.x = Color.parseColor(b7 != null ? b7 : str);
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.af6.b
    public void b(WorkoutSession workoutSession) {
        ee7.b(workoutSession, "workoutSession");
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "ActiveTimeDetailFragment";
    }

    @DexIgnore
    public final void f1() {
        mw4 a2;
        OverviewDayChart overviewDayChart;
        qw6<mw4> qw6 = this.i;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewDayChart = a2.v) != null) {
            be5.a aVar = be5.o;
            ie6 ie6 = this.j;
            if (aVar.a(ie6 != null ? ie6.h() : null)) {
                overviewDayChart.a("dianaActiveMinutesTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.a("hybridActiveMinutesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("ActiveTimeDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131362636:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362637:
                    ie6 ie6 = this.j;
                    if (ie6 != null) {
                        ie6.m();
                        return;
                    }
                    return;
                case 2131362705:
                    ie6 ie62 = this.j;
                    if (ie62 != null) {
                        ie62.l();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        mw4 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        mw4 mw4 = (mw4) qb.a(layoutInflater, 2131558491, viewGroup, false, a1());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        this.h = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.h = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        ee7.a((Object) mw4, "binding");
        a(mw4);
        ie6 ie6 = this.j;
        if (ie6 != null) {
            ie6.a(this.h);
        }
        this.i = new qw6<>(this, mw4);
        f1();
        qw6<mw4> qw6 = this.i;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        ie6 ie6 = this.j;
        if (ie6 != null) {
            ie6.k();
        }
        Lifecycle lifecycle = getLifecycle();
        ie6 ie62 = this.j;
        if (ie62 != null) {
            lifecycle.b(ie62.j());
            super.onDestroyView();
            Z0();
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        ie6 ie6 = this.j;
        if (ie6 != null) {
            ie6.g();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        f1();
        ie6 ie6 = this.j;
        if (ie6 != null) {
            ie6.b(this.h);
        }
        ie6 ie62 = this.j;
        if (ie62 != null) {
            ie62.f();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        ie6 ie6 = this.j;
        if (ie6 != null) {
            ie6.a(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public final void a(mw4 mw4) {
        String str;
        mw4.E.setOnClickListener(this);
        mw4.F.setOnClickListener(this);
        mw4.G.setOnClickListener(this);
        be5.a aVar = be5.o;
        ie6 ie6 = this.j;
        if (aVar.a(ie6 != null ? ie6.h() : null)) {
            str = eh5.l.a().b("dianaActiveMinutesTab");
        } else {
            str = eh5.l.a().b("hybridActiveMinutesTab");
        }
        this.p = str;
        this.q = eh5.l.a().b("nonBrandActivityDetailBackground");
        this.r = eh5.l.a().b("onDianaActiveMinutesTab");
        af6 af6 = new af6(af6.d.ACTIVE_TIME, ob5.IMPERIAL, new WorkoutSessionDifference(), this.p);
        this.g = af6;
        if (af6 != null) {
            af6.a(this);
        }
        mw4.I.setBackgroundColor(this.t);
        mw4.r.setBackgroundColor(this.s);
        RecyclerView recyclerView = mw4.L;
        ee7.a((Object) recyclerView, "it");
        recyclerView.setAdapter(this.g);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable c = v6.c(recyclerView.getContext(), 2131230856);
        if (c != null) {
            fe6 fe6 = new fe6(linearLayoutManager.Q(), false, false, 6, null);
            ee7.a((Object) c, ResourceManager.DRAWABLE);
            fe6.a(c);
            recyclerView.addItemDecoration(fe6);
        }
    }

    @DexIgnore
    public void a(ie6 ie6) {
        ee7.b(ie6, "presenter");
        this.j = ie6;
        Lifecycle lifecycle = getLifecycle();
        ie6 ie62 = this.j;
        if (ie62 != null) {
            lifecycle.a(ie62.j());
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.je6
    public void a(Date date, boolean z2, boolean z3, boolean z4) {
        mw4 a2;
        ee7.b(date, "date");
        this.h = date;
        Calendar calendar = this.f;
        ee7.a((Object) calendar, "calendar");
        calendar.setTime(date);
        int i2 = this.f.get(7);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z2 + " - isToday - " + z3 + " - isDateAfter: " + z4 + " - calendar: " + this.f);
        qw6<mw4> qw6 = this.i;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            a2.q.a(true, true);
            FlexibleTextView flexibleTextView = a2.y;
            ee7.a((Object) flexibleTextView, "binding.ftvDayOfMonth");
            flexibleTextView.setText(String.valueOf(this.f.get(5)));
            if (z2) {
                RTLImageView rTLImageView = a2.F;
                ee7.a((Object) rTLImageView, "binding.ivBackDate");
                rTLImageView.setVisibility(4);
            } else {
                RTLImageView rTLImageView2 = a2.F;
                ee7.a((Object) rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setVisibility(0);
            }
            if (z3 || z4) {
                RTLImageView rTLImageView3 = a2.G;
                ee7.a((Object) rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setVisibility(8);
                if (z3) {
                    FlexibleTextView flexibleTextView2 = a2.z;
                    ee7.a((Object) flexibleTextView2, "binding.ftvDayOfWeek");
                    flexibleTextView2.setText(ig5.a(getContext(), 2131886616));
                    return;
                }
                FlexibleTextView flexibleTextView3 = a2.z;
                ee7.a((Object) flexibleTextView3, "binding.ftvDayOfWeek");
                flexibleTextView3.setText(xe5.b.b(i2));
                return;
            }
            RTLImageView rTLImageView4 = a2.G;
            ee7.a((Object) rTLImageView4, "binding.ivNextDate");
            rTLImageView4.setVisibility(0);
            FlexibleTextView flexibleTextView4 = a2.z;
            ee7.a((Object) flexibleTextView4, "binding.ftvDayOfWeek");
            flexibleTextView4.setText(xe5.b.b(i2));
        }
    }

    @DexIgnore
    @Override // com.fossil.je6
    public void a(ob5 ob5, ActivitySummary activitySummary) {
        mw4 a2;
        int i2;
        int i3;
        ee7.b(ob5, "distanceUnit");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeDetailFragment", "showDayDetail - distanceUnit=" + ob5 + ", activitySummary=" + activitySummary);
        qw6<mw4> qw6 = this.i;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            ee7.a((Object) a2, "binding");
            View d = a2.d();
            ee7.a((Object) d, "binding.root");
            Context context = d.getContext();
            if (activitySummary != null) {
                i2 = activitySummary.getActiveTime();
                i3 = activitySummary.getActiveTimeGoal();
            } else {
                i3 = 0;
                i2 = 0;
            }
            if (i2 > 0) {
                FlexibleTextView flexibleTextView = a2.x;
                ee7.a((Object) flexibleTextView, "binding.ftvDailyValue");
                flexibleTextView.setText(af5.a.a(Integer.valueOf(i2)));
                FlexibleTextView flexibleTextView2 = a2.w;
                ee7.a((Object) flexibleTextView2, "binding.ftvDailyUnit");
                String a3 = ig5.a(context, 2131886563);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026s_DetailPage_Label__Mins)");
                if (a3 != null) {
                    String lowerCase = a3.toLowerCase();
                    ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    flexibleTextView2.setText(lowerCase);
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                FlexibleTextView flexibleTextView3 = a2.x;
                ee7.a((Object) flexibleTextView3, "binding.ftvDailyValue");
                flexibleTextView3.setText("");
                FlexibleTextView flexibleTextView4 = a2.w;
                ee7.a((Object) flexibleTextView4, "binding.ftvDailyUnit");
                String a4 = ig5.a(context, 2131886562);
                ee7.a((Object) a4, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
                if (a4 != null) {
                    String upperCase = a4.toUpperCase();
                    ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                    flexibleTextView4.setText(upperCase);
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            }
            int i4 = i3 > 0 ? (i2 * 100) / i3 : -1;
            if (i2 >= i3 && i3 > 0) {
                a2.z.setTextColor(this.s);
                a2.y.setTextColor(this.s);
                a2.w.setTextColor(this.s);
                a2.x.setTextColor(this.s);
                a2.A.setTextColor(this.s);
                RTLImageView rTLImageView = a2.G;
                ee7.a((Object) rTLImageView, "binding.ivNextDate");
                rTLImageView.setSelected(true);
                RTLImageView rTLImageView2 = a2.F;
                ee7.a((Object) rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setSelected(true);
                ConstraintLayout constraintLayout = a2.s;
                ee7.a((Object) constraintLayout, "binding.clOverviewDay");
                constraintLayout.setSelected(true);
                FlexibleTextView flexibleTextView5 = a2.z;
                ee7.a((Object) flexibleTextView5, "binding.ftvDayOfWeek");
                flexibleTextView5.setSelected(true);
                FlexibleTextView flexibleTextView6 = a2.y;
                ee7.a((Object) flexibleTextView6, "binding.ftvDayOfMonth");
                flexibleTextView6.setSelected(true);
                View view = a2.H;
                ee7.a((Object) view, "binding.line");
                view.setSelected(true);
                FlexibleTextView flexibleTextView7 = a2.x;
                ee7.a((Object) flexibleTextView7, "binding.ftvDailyValue");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = a2.w;
                ee7.a((Object) flexibleTextView8, "binding.ftvDailyUnit");
                flexibleTextView8.setSelected(true);
                FlexibleTextView flexibleTextView9 = a2.A;
                ee7.a((Object) flexibleTextView9, "binding.ftvEst");
                flexibleTextView9.setSelected(true);
                String str = this.r;
                if (str != null) {
                    a2.z.setTextColor(Color.parseColor(str));
                    a2.y.setTextColor(Color.parseColor(str));
                    a2.x.setTextColor(Color.parseColor(str));
                    a2.w.setTextColor(Color.parseColor(str));
                    a2.A.setTextColor(Color.parseColor(str));
                    a2.H.setBackgroundColor(Color.parseColor(str));
                    a2.G.setColorFilter(Color.parseColor(str));
                    a2.F.setColorFilter(Color.parseColor(str));
                }
                String str2 = this.p;
                if (str2 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str2));
                }
            } else if (i2 > 0) {
                a2.y.setTextColor(this.v);
                a2.z.setTextColor(this.u);
                a2.w.setTextColor(this.x);
                a2.x.setTextColor(this.v);
                a2.A.setTextColor(this.v);
                a2.s.setBackgroundColor(Color.parseColor(this.q));
                View view2 = a2.H;
                ee7.a((Object) view2, "binding.line");
                view2.setSelected(false);
                RTLImageView rTLImageView3 = a2.G;
                ee7.a((Object) rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setSelected(false);
                RTLImageView rTLImageView4 = a2.F;
                ee7.a((Object) rTLImageView4, "binding.ivBackDate");
                rTLImageView4.setSelected(false);
                int i5 = this.x;
                a2.H.setBackgroundColor(i5);
                a2.G.setColorFilter(i5);
                a2.F.setColorFilter(i5);
                String str3 = this.q;
                if (str3 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str3));
                }
            } else {
                a2.y.setTextColor(this.v);
                a2.z.setTextColor(this.u);
                a2.x.setTextColor(this.w);
                a2.w.setTextColor(this.w);
                RTLImageView rTLImageView5 = a2.G;
                ee7.a((Object) rTLImageView5, "binding.ivNextDate");
                rTLImageView5.setSelected(false);
                RTLImageView rTLImageView6 = a2.F;
                ee7.a((Object) rTLImageView6, "binding.ivBackDate");
                rTLImageView6.setSelected(false);
                ConstraintLayout constraintLayout2 = a2.s;
                ee7.a((Object) constraintLayout2, "binding.clOverviewDay");
                constraintLayout2.setSelected(false);
                FlexibleTextView flexibleTextView10 = a2.z;
                ee7.a((Object) flexibleTextView10, "binding.ftvDayOfWeek");
                flexibleTextView10.setSelected(false);
                FlexibleTextView flexibleTextView11 = a2.y;
                ee7.a((Object) flexibleTextView11, "binding.ftvDayOfMonth");
                flexibleTextView11.setSelected(false);
                View view3 = a2.H;
                ee7.a((Object) view3, "binding.line");
                view3.setSelected(false);
                FlexibleTextView flexibleTextView12 = a2.x;
                ee7.a((Object) flexibleTextView12, "binding.ftvDailyValue");
                flexibleTextView12.setSelected(false);
                FlexibleTextView flexibleTextView13 = a2.w;
                ee7.a((Object) flexibleTextView13, "binding.ftvDailyUnit");
                flexibleTextView13.setSelected(false);
                FlexibleTextView flexibleTextView14 = a2.A;
                ee7.a((Object) flexibleTextView14, "binding.ftvEst");
                flexibleTextView14.setSelected(false);
                int i6 = this.x;
                a2.H.setBackgroundColor(i6);
                a2.G.setColorFilter(i6);
                a2.F.setColorFilter(i6);
                String str4 = this.q;
                if (str4 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str4));
                }
            }
            if (i4 == -1) {
                FlexibleProgressBar flexibleProgressBar = a2.J;
                ee7.a((Object) flexibleProgressBar, "binding.pbGoal");
                flexibleProgressBar.setProgress(0);
                FlexibleTextView flexibleTextView15 = a2.D;
                ee7.a((Object) flexibleTextView15, "binding.ftvProgressValue");
                flexibleTextView15.setText(ig5.a(context, 2131887288));
            } else {
                FlexibleProgressBar flexibleProgressBar2 = a2.J;
                ee7.a((Object) flexibleProgressBar2, "binding.pbGoal");
                flexibleProgressBar2.setProgress(i4);
                FlexibleTextView flexibleTextView16 = a2.D;
                ee7.a((Object) flexibleTextView16, "binding.ftvProgressValue");
                flexibleTextView16.setText(i4 + "%");
            }
            FlexibleTextView flexibleTextView17 = a2.B;
            ee7.a((Object) flexibleTextView17, "binding.ftvGoalValue");
            we7 we7 = we7.a;
            String a5 = ig5.a(context, 2131886568);
            ee7.a((Object) a5, "LanguageHelper.getString\u2026Page_Title__OfNumberMins)");
            String format = String.format(a5, Arrays.copyOf(new Object[]{af5.a.a(Integer.valueOf(i3))}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            flexibleTextView17.setText(format);
        }
    }

    @DexIgnore
    @Override // com.fossil.je6
    public void a(do5 do5, ArrayList<String> arrayList) {
        mw4 a2;
        OverviewDayChart overviewDayChart;
        ee7.b(do5, "baseModel");
        ee7.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeDetailFragment", "showDayDetailChart - baseModel=" + do5);
        qw6<mw4> qw6 = this.i;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewDayChart = a2.v) != null) {
            BarChart.c cVar = (BarChart.c) do5;
            cVar.b(do5.a.a(cVar.c()));
            if (!arrayList.isEmpty()) {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
            } else {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) xe5.b.b(), false, 2, (Object) null);
            }
            overviewDayChart.a(do5);
        }
    }

    @DexIgnore
    @Override // com.fossil.je6
    public void a(boolean z2, ob5 ob5, qf<WorkoutSession> qfVar) {
        mw4 a2;
        ee7.b(ob5, "distanceUnit");
        ee7.b(qfVar, "workoutSessions");
        qw6<mw4> qw6 = this.i;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            if (z2) {
                LinearLayout linearLayout = a2.I;
                ee7.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                if (!qfVar.isEmpty()) {
                    FlexibleTextView flexibleTextView = a2.C;
                    ee7.a((Object) flexibleTextView, "it.ftvNoWorkoutRecorded");
                    flexibleTextView.setVisibility(8);
                    RecyclerView recyclerView = a2.L;
                    ee7.a((Object) recyclerView, "it.rvWorkout");
                    recyclerView.setVisibility(0);
                    af6 af6 = this.g;
                    if (af6 != null) {
                        af6.a(ob5, qfVar);
                    }
                } else {
                    FlexibleTextView flexibleTextView2 = a2.C;
                    ee7.a((Object) flexibleTextView2, "it.ftvNoWorkoutRecorded");
                    flexibleTextView2.setVisibility(0);
                    RecyclerView recyclerView2 = a2.L;
                    ee7.a((Object) recyclerView2, "it.rvWorkout");
                    recyclerView2.setVisibility(8);
                    af6 af62 = this.g;
                    if (af62 != null) {
                        af62.a(ob5, qfVar);
                    }
                }
            } else {
                LinearLayout linearLayout2 = a2.I;
                ee7.a((Object) linearLayout2, "it.llWorkout");
                linearLayout2.setVisibility(8);
            }
            AppBarLayout appBarLayout = a2.q;
            ee7.a((Object) appBarLayout, "it.appBarLayout");
            ViewGroup.LayoutParams layoutParams = appBarLayout.getLayoutParams();
            if (layoutParams != null) {
                CoordinatorLayout.e eVar = (CoordinatorLayout.e) layoutParams;
                AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) eVar.d();
                if (behavior == null) {
                    behavior = new AppBarLayout.Behavior();
                }
                behavior.setDragCallback(new b(this, z2, qfVar, ob5));
                eVar.a(behavior);
                return;
            }
            throw new x87("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
        }
    }

    @DexIgnore
    @Override // com.fossil.af6.b
    public void a(WorkoutSession workoutSession) {
        String str;
        ob5 i2;
        ee7.b(workoutSession, "workoutSession");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ie6 ie6 = this.j;
            if (ie6 == null || (i2 = ie6.i()) == null || (str = i2.name()) == null) {
                str = ob5.METRIC.name();
            }
            WorkoutDetailActivity.a aVar = WorkoutDetailActivity.e;
            ee7.a((Object) activity, "it");
            aVar.a(activity, workoutSession.getId(), str);
        }
    }
}
