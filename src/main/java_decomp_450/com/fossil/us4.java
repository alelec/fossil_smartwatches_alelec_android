package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class us4 extends he {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<jo4, Integer>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Object>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, ServerError>> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Integer> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ ro4 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel$initLoadRecommendedChallenge$1", f = "BCRecommendationViewModel.kt", l = {51}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ us4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(us4 us4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = us4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.this$0, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.b.a(pb7.a(true));
                us4 us4 = this.this$0;
                this.L$0 = yi7;
                this.label = 1;
                if (us4.a(this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.a(pb7.a(false));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel", f = "BCRecommendationViewModel.kt", l = {69, 86}, m = "loadRecommended")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ us4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(us4 us4, fb7 fb7) {
            super(fb7);
            this.this$0 = us4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel$loadRecommended$data$1", f = "BCRecommendationViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super List<? extends Object>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $recommended;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(List list, fb7 fb7) {
            super(2, fb7);
            this.$recommended = list;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.$recommended, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super List<? extends Object>> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                return nt4.c(this.$recommended);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel$loadRecommended$result$1", f = "BCRecommendationViewModel.kt", l = {69}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends jo4>>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ us4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(us4 us4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = us4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends jo4>>> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ro4 a2 = this.this$0.h;
                this.L$0 = yi7;
                this.label = 1;
                obj = a2.a(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel$refresh$1", f = "BCRecommendationViewModel.kt", l = {58}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ us4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(us4 us4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = us4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                us4 us4 = this.this$0;
                this.L$0 = yi7;
                this.label = 1;
                if (us4.a(this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.a(pb7.a(false));
            return i97.a;
        }
    }

    @DexIgnore
    public us4(ro4 ro4) {
        ee7.b(ro4, "challengeRepository");
        this.h = ro4;
        String simpleName = us4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCRecommendationViewModel::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public final LiveData<Boolean> c() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<List<Object>> d() {
        return au4.a(this.e);
    }

    @DexIgnore
    public final LiveData<r87<jo4, Integer>> e() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Integer> f() {
        return this.g;
    }

    @DexIgnore
    public final void g() {
        ik7 unused = xh7.b(ie.a(this), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    public final void h() {
        ik7 unused = xh7.b(ie.a(this), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final LiveData<Boolean> a() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<r87<Boolean, ServerError>> b() {
        return this.f;
    }

    @DexIgnore
    public final void a(int i) {
        this.g.a(Integer.valueOf(i));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r9) {
        /*
            r8 = this;
            boolean r0 = r9 instanceof com.fossil.us4.b
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.us4$b r0 = (com.fossil.us4.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.us4$b r0 = new com.fossil.us4$b
            r0.<init>(r8, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 0
            r5 = 1
            if (r2 == 0) goto L_0x004a
            if (r2 == r5) goto L_0x0042
            if (r2 != r3) goto L_0x003a
            java.lang.Object r1 = r0.L$2
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r1 = r0.L$1
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.us4 r0 = (com.fossil.us4) r0
            com.fossil.t87.a(r9)
            goto L_0x00fc
        L_0x003a:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r0)
            throw r9
        L_0x0042:
            java.lang.Object r2 = r0.L$0
            com.fossil.us4 r2 = (com.fossil.us4) r2
            com.fossil.t87.a(r9)
            goto L_0x0062
        L_0x004a:
            com.fossil.t87.a(r9)
            com.fossil.ti7 r9 = com.fossil.qj7.b()
            com.fossil.us4$d r2 = new com.fossil.us4$d
            r2.<init>(r8, r4)
            r0.L$0 = r8
            r0.label = r5
            java.lang.Object r9 = com.fossil.vh7.a(r9, r2, r0)
            if (r9 != r1) goto L_0x0061
            return r1
        L_0x0061:
            r2 = r8
        L_0x0062:
            com.fossil.ko4 r9 = (com.fossil.ko4) r9
            java.lang.Object r6 = r9.c()
            java.util.List r6 = (java.util.List) r6
            if (r6 != 0) goto L_0x00e3
            com.portfolio.platform.data.model.ServerError r9 = r9.a()
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r2.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r6 = "loadRecommended - error: "
            r3.append(r6)
            r3.append(r9)
            java.lang.String r3 = r3.toString()
            r0.e(r1, r3)
            r0 = 404(0x194, float:5.66E-43)
            if (r9 == 0) goto L_0x0094
            java.lang.Integer r4 = r9.getCode()
        L_0x0094:
            if (r4 != 0) goto L_0x0097
            goto L_0x00b0
        L_0x0097:
            int r1 = r4.intValue()
            if (r0 != r1) goto L_0x00b0
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r9 = r2.c
            java.lang.Boolean r0 = com.fossil.pb7.a(r5)
            r9.a(r0)
            androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Object>> r9 = r2.e
            java.util.List r0 = com.fossil.w97.a()
            r9.a(r0)
            goto L_0x0110
        L_0x00b0:
            androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Object>> r0 = r2.e
            java.lang.Object r0 = r0.a()
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = 0
            if (r0 == 0) goto L_0x00c4
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x00c2
            goto L_0x00c4
        L_0x00c2:
            r0 = 0
            goto L_0x00c5
        L_0x00c4:
            r0 = 1
        L_0x00c5:
            if (r0 == 0) goto L_0x00d5
            androidx.lifecycle.MutableLiveData<com.fossil.r87<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r0 = r2.f
            java.lang.Boolean r1 = com.fossil.pb7.a(r5)
            com.fossil.r87 r9 = com.fossil.w87.a(r1, r9)
            r0.a(r9)
            goto L_0x0110
        L_0x00d5:
            androidx.lifecycle.MutableLiveData<com.fossil.r87<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r0 = r2.f
            java.lang.Boolean r1 = com.fossil.pb7.a(r1)
            com.fossil.r87 r9 = com.fossil.w87.a(r1, r9)
            r0.a(r9)
            goto L_0x0110
        L_0x00e3:
            com.fossil.ti7 r5 = com.fossil.qj7.a()
            com.fossil.us4$c r7 = new com.fossil.us4$c
            r7.<init>(r6, r4)
            r0.L$0 = r2
            r0.L$1 = r9
            r0.L$2 = r6
            r0.label = r3
            java.lang.Object r9 = com.fossil.vh7.a(r5, r7, r0)
            if (r9 != r1) goto L_0x00fb
            return r1
        L_0x00fb:
            r0 = r2
        L_0x00fc:
            java.util.List r9 = (java.util.List) r9
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r1 = r0.c
            boolean r2 = r9.isEmpty()
            java.lang.Boolean r2 = com.fossil.pb7.a(r2)
            r1.a(r2)
            androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Object>> r0 = r0.e
            r0.a(r9)
        L_0x0110:
            com.fossil.i97 r9 = com.fossil.i97.a
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.us4.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(jo4 jo4, int i) {
        ee7.b(jo4, "recommended");
        this.d.a(w87.a(jo4, Integer.valueOf(i)));
    }
}
