package com.fossil;

import com.fossil.lf;
import com.fossil.pf;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mf<Key, Value> extends jf<Key, Value> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<Value> {
        @DexIgnore
        public abstract void a(List<Value> list);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Value> extends a<Value> {
        @DexIgnore
        public /* final */ lf.d<Value> a;

        @DexIgnore
        public b(mf mfVar, int i, Executor executor, pf.a<Value> aVar) {
            this.a = new lf.d<>(mfVar, i, executor, aVar);
        }

        @DexIgnore
        @Override // com.fossil.mf.a
        public void a(List<Value> list) {
            if (!this.a.a()) {
                this.a.a(new pf<>(list, 0, 0, 0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<Value> extends a<Value> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<Value> extends c<Value> {
        @DexIgnore
        public /* final */ lf.d<Value> a;

        @DexIgnore
        public d(mf mfVar, boolean z, pf.a<Value> aVar) {
            this.a = new lf.d<>(mfVar, 0, null, aVar);
        }

        @DexIgnore
        @Override // com.fossil.mf.a
        public void a(List<Value> list) {
            if (!this.a.a()) {
                this.a.a(new pf<>(list, 0, 0, 0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<Key> {
        @DexIgnore
        public /* final */ Key a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public e(Key key, int i, boolean z) {
            this.a = key;
            this.b = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f<Key> {
        @DexIgnore
        public /* final */ Key a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public f(Key key, int i) {
            this.a = key;
            this.b = i;
        }
    }

    @DexIgnore
    @Override // com.fossil.jf
    public final void dispatchLoadAfter(int i, Value value, int i2, Executor executor, pf.a<Value> aVar) {
        loadAfter(new f<>(getKey(value), i2), new b(this, 1, executor, aVar));
    }

    @DexIgnore
    @Override // com.fossil.jf
    public final void dispatchLoadBefore(int i, Value value, int i2, Executor executor, pf.a<Value> aVar) {
        loadBefore(new f<>(getKey(value), i2), new b(this, 2, executor, aVar));
    }

    @DexIgnore
    @Override // com.fossil.jf
    public final void dispatchLoadInitial(Key key, int i, int i2, boolean z, Executor executor, pf.a<Value> aVar) {
        d dVar = new d(this, z, aVar);
        loadInitial(new e(key, i, z), dVar);
        dVar.a.a(executor);
    }

    @DexIgnore
    @Override // com.fossil.jf
    public final Key getKey(int i, Value value) {
        if (value == null) {
            return null;
        }
        return getKey(value);
    }

    @DexIgnore
    public abstract Key getKey(Value value);

    @DexIgnore
    public abstract void loadAfter(f<Key> fVar, a<Value> aVar);

    @DexIgnore
    public abstract void loadBefore(f<Key> fVar, a<Value> aVar);

    @DexIgnore
    public abstract void loadInitial(e<Key> eVar, c<Value> cVar);

    @DexIgnore
    @Override // com.fossil.lf
    public final <ToValue> mf<Key, ToValue> map(t3<Value, ToValue> t3Var) {
        return mapByPage((t3) lf.createListFunction(t3Var));
    }

    @DexIgnore
    @Override // com.fossil.lf
    public final <ToValue> mf<Key, ToValue> mapByPage(t3<List<Value>, List<ToValue>> t3Var) {
        return new xf(this, t3Var);
    }
}
