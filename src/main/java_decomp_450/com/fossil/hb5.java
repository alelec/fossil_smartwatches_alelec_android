package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum hb5 {
    Jawbone("jawbone"),
    UnderArmour("underarmour"),
    HealthKit("healthkit"),
    GoogleFit("googlefit");
    
    @DexIgnore
    public /* final */ String name;

    @DexIgnore
    public hb5(String str) {
        this.name = str;
    }

    @DexIgnore
    public static hb5 fromName(String str) {
        hb5[] values = values();
        for (hb5 hb5 : values) {
            if (hb5.name.equalsIgnoreCase(str)) {
                return hb5;
            }
        }
        return null;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }
}
