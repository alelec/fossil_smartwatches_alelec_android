package com.fossil;

import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleImageButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ox4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ DashBar A;
    @DexIgnore
    public /* final */ ConstraintLayout B;
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ AppCompatImageView r;
    @DexIgnore
    public /* final */ FlexibleImageButton s;
    @DexIgnore
    public /* final */ FlexibleImageButton t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ ConstraintLayout w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleButton y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public ox4(Object obj, View view, int i, RTLImageView rTLImageView, AppCompatImageView appCompatImageView, FlexibleImageButton flexibleImageButton, FlexibleImageButton flexibleImageButton2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, FlexibleTextView flexibleTextView, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView2, DashBar dashBar, ConstraintLayout constraintLayout4) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = appCompatImageView;
        this.s = flexibleImageButton;
        this.t = flexibleImageButton2;
        this.u = constraintLayout;
        this.v = constraintLayout2;
        this.w = constraintLayout3;
        this.x = flexibleTextView;
        this.y = flexibleButton;
        this.z = flexibleTextView2;
        this.A = dashBar;
        this.B = constraintLayout4;
    }
}
