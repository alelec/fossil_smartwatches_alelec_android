package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dx4 extends cx4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i t; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray u;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public long s;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        u = sparseIntArray;
        sparseIntArray.put(2131361980, 1);
    }
    */

    @DexIgnore
    public dx4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 2, t, u));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.s = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.s != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.s = 1;
        }
        g();
    }

    @DexIgnore
    public dx4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (RecyclerViewCalendar) objArr[1]);
        this.s = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.r = constraintLayout;
        constraintLayout.setTag(null);
        a(view);
        f();
    }
}
