package com.fossil;

import com.fossil.bw2;
import com.fossil.xp2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pp2 extends bw2<pp2, a> implements lx2 {
    @DexIgnore
    public static /* final */ pp2 zzh;
    @DexIgnore
    public static volatile wx2<pp2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public xp2 zze;
    @DexIgnore
    public xp2 zzf;
    @DexIgnore
    public boolean zzg;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<pp2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(pp2.zzh);
        }

        @DexIgnore
        public final a a(int i) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((pp2) ((bw2.a) this).b).b(i);
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(op2 op2) {
            this();
        }

        @DexIgnore
        public final a a(xp2.a aVar) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((pp2) ((bw2.a) this).b).a((xp2) ((bw2) aVar.g()));
            return this;
        }

        @DexIgnore
        public final a a(xp2 xp2) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((pp2) ((bw2.a) this).b).b(xp2);
            return this;
        }

        @DexIgnore
        public final a a(boolean z) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((pp2) ((bw2.a) this).b).a(z);
            return this;
        }
    }

    /*
    static {
        pp2 pp2 = new pp2();
        zzh = pp2;
        bw2.a(pp2.class, pp2);
    }
    */

    @DexIgnore
    public static a v() {
        return (a) zzh.e();
    }

    @DexIgnore
    public final void a(xp2 xp2) {
        xp2.getClass();
        this.zze = xp2;
        this.zzc |= 2;
    }

    @DexIgnore
    public final void b(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    @DexIgnore
    public final int p() {
        return this.zzd;
    }

    @DexIgnore
    public final xp2 q() {
        xp2 xp2 = this.zze;
        return xp2 == null ? xp2.B() : xp2;
    }

    @DexIgnore
    public final boolean r() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final xp2 s() {
        xp2 xp2 = this.zzf;
        return xp2 == null ? xp2.B() : xp2;
    }

    @DexIgnore
    public final boolean t() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final boolean u() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean zza() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final void b(xp2 xp2) {
        xp2.getClass();
        this.zzf = xp2;
        this.zzc |= 4;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.zzc |= 8;
        this.zzg = z;
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (op2.a[i - 1]) {
            case 1:
                return new pp2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u1004\u0000\u0002\u1009\u0001\u0003\u1009\u0002\u0004\u1007\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                wx2<pp2> wx2 = zzi;
                if (wx2 == null) {
                    synchronized (pp2.class) {
                        wx2 = zzi;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzh);
                            zzi = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
