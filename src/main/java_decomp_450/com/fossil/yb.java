package com.fossil;

import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import com.fossil.nc;
import java.io.PrintWriter;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yb extends nc implements FragmentManager.h {
    @DexIgnore
    public /* final */ FragmentManager r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public int t;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public yb(androidx.fragment.app.FragmentManager r3) {
        /*
            r2 = this;
            com.fossil.dc r0 = r3.u()
            androidx.fragment.app.FragmentHostCallback<?> r1 = r3.o
            if (r1 == 0) goto L_0x0011
            android.content.Context r1 = r1.c()
            java.lang.ClassLoader r1 = r1.getClassLoader()
            goto L_0x0012
        L_0x0011:
            r1 = 0
        L_0x0012:
            r2.<init>(r0, r1)
            r0 = -1
            r2.t = r0
            r2.r = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yb.<init>(androidx.fragment.app.FragmentManager):void");
    }

    @DexIgnore
    public void a(String str, PrintWriter printWriter) {
        a(str, printWriter, true);
    }

    @DexIgnore
    @Override // com.fossil.nc
    public nc b(Fragment fragment) {
        FragmentManager fragmentManager = fragment.mFragmentManager;
        if (fragmentManager == null || fragmentManager == this.r) {
            super.b(fragment);
            return this;
        }
        throw new IllegalStateException("Cannot detach Fragment attached to a different FragmentManager. Fragment " + fragment.toString() + " is already attached to a FragmentManager.");
    }

    @DexIgnore
    @Override // com.fossil.nc
    public nc c(Fragment fragment) {
        FragmentManager fragmentManager = fragment.mFragmentManager;
        if (fragmentManager == null || fragmentManager == this.r) {
            super.c(fragment);
            return this;
        }
        throw new IllegalStateException("Cannot hide Fragment attached to a different FragmentManager. Fragment " + fragment.toString() + " is already attached to a FragmentManager.");
    }

    @DexIgnore
    @Override // com.fossil.nc
    public nc d(Fragment fragment) {
        FragmentManager fragmentManager = fragment.mFragmentManager;
        if (fragmentManager == null || fragmentManager == this.r) {
            super.d(fragment);
            return this;
        }
        throw new IllegalStateException("Cannot remove Fragment attached to a different FragmentManager. Fragment " + fragment.toString() + " is already attached to a FragmentManager.");
    }

    @DexIgnore
    @Override // com.fossil.nc
    public nc e(Fragment fragment) {
        FragmentManager fragmentManager = fragment.mFragmentManager;
        if (fragmentManager == null || fragmentManager == this.r) {
            super.e(fragment);
            return this;
        }
        throw new IllegalStateException("Cannot show Fragment attached to a different FragmentManager. Fragment " + fragment.toString() + " is already attached to a FragmentManager.");
    }

    @DexIgnore
    @Override // com.fossil.nc
    public boolean f() {
        return ((nc) this).a.isEmpty();
    }

    @DexIgnore
    public void g() {
        int size = ((nc) this).a.size();
        for (int i = 0; i < size; i++) {
            nc.a aVar = ((nc) this).a.get(i);
            Fragment fragment = aVar.b;
            if (fragment != null) {
                fragment.setNextTransition(((nc) this).f);
            }
            switch (aVar.a) {
                case 1:
                    fragment.setNextAnim(aVar.c);
                    this.r.a(fragment, false);
                    this.r.a(fragment);
                    break;
                case 2:
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + aVar.a);
                case 3:
                    fragment.setNextAnim(aVar.d);
                    this.r.t(fragment);
                    break;
                case 4:
                    fragment.setNextAnim(aVar.d);
                    this.r.l(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(aVar.c);
                    this.r.a(fragment, false);
                    this.r.y(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(aVar.d);
                    this.r.g(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(aVar.c);
                    this.r.a(fragment, false);
                    this.r.c(fragment);
                    break;
                case 8:
                    this.r.w(fragment);
                    break;
                case 9:
                    this.r.w(null);
                    break;
                case 10:
                    this.r.a(fragment, aVar.h);
                    break;
            }
            if (!(((nc) this).p || aVar.a == 1 || fragment == null)) {
                this.r.q(fragment);
            }
        }
        if (!((nc) this).p) {
            FragmentManager fragmentManager = this.r;
            fragmentManager.a(fragmentManager.n, true);
        }
    }

    @DexIgnore
    public String h() {
        return ((nc) this).i;
    }

    @DexIgnore
    public boolean i() {
        for (int i = 0; i < ((nc) this).a.size(); i++) {
            if (b(((nc) this).a.get(i))) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void j() {
        if (((nc) this).q != null) {
            for (int i = 0; i < ((nc) this).q.size(); i++) {
                ((nc) this).q.get(i).run();
            }
            ((nc) this).q = null;
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.t >= 0) {
            sb.append(" #");
            sb.append(this.t);
        }
        if (((nc) this).i != null) {
            sb.append(" ");
            sb.append(((nc) this).i);
        }
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public void a(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(((nc) this).i);
            printWriter.print(" mIndex=");
            printWriter.print(this.t);
            printWriter.print(" mCommitted=");
            printWriter.println(this.s);
            if (((nc) this).f != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(((nc) this).f));
            }
            if (!(((nc) this).b == 0 && ((nc) this).c == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(((nc) this).b));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(((nc) this).c));
            }
            if (!(((nc) this).d == 0 && ((nc) this).e == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(((nc) this).d));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(((nc) this).e));
            }
            if (!(((nc) this).j == 0 && ((nc) this).k == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(((nc) this).j));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(((nc) this).k);
            }
            if (!(((nc) this).l == 0 && ((nc) this).m == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(((nc) this).l));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(((nc) this).m);
            }
        }
        if (!((nc) this).a.isEmpty()) {
            printWriter.print(str);
            printWriter.println("Operations:");
            int size = ((nc) this).a.size();
            for (int i = 0; i < size; i++) {
                nc.a aVar = ((nc) this).a.get(i);
                switch (aVar.a) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    case 8:
                        str2 = "SET_PRIMARY_NAV";
                        break;
                    case 9:
                        str2 = "UNSET_PRIMARY_NAV";
                        break;
                    case 10:
                        str2 = "OP_SET_MAX_LIFECYCLE";
                        break;
                    default:
                        str2 = "cmd=" + aVar.a;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(aVar.b);
                if (z) {
                    if (!(aVar.c == 0 && aVar.d == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.c));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.d));
                    }
                    if (aVar.e != 0 || aVar.f != 0) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.e));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.f));
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.nc
    public int b() {
        return b(true);
    }

    @DexIgnore
    @Override // com.fossil.nc
    public void c() {
        e();
        this.r.b((FragmentManager.h) this, false);
    }

    @DexIgnore
    @Override // com.fossil.nc
    public void d() {
        e();
        this.r.b((FragmentManager.h) this, true);
    }

    @DexIgnore
    public int b(boolean z) {
        if (!this.s) {
            if (FragmentManager.d(2)) {
                Log.v("FragmentManager", "Commit: " + this);
                PrintWriter printWriter = new PrintWriter(new y8("FragmentManager"));
                a("  ", printWriter);
                printWriter.close();
            }
            this.s = true;
            if (((nc) this).g) {
                this.t = this.r.a();
            } else {
                this.t = -1;
            }
            this.r.a(this, z);
            return this.t;
        }
        throw new IllegalStateException("commit already called");
    }

    @DexIgnore
    public void c(boolean z) {
        for (int size = ((nc) this).a.size() - 1; size >= 0; size--) {
            nc.a aVar = ((nc) this).a.get(size);
            Fragment fragment = aVar.b;
            if (fragment != null) {
                fragment.setNextTransition(FragmentManager.e(((nc) this).f));
            }
            switch (aVar.a) {
                case 1:
                    fragment.setNextAnim(aVar.f);
                    this.r.a(fragment, true);
                    this.r.t(fragment);
                    break;
                case 2:
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + aVar.a);
                case 3:
                    fragment.setNextAnim(aVar.e);
                    this.r.a(fragment);
                    break;
                case 4:
                    fragment.setNextAnim(aVar.e);
                    this.r.y(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(aVar.f);
                    this.r.a(fragment, true);
                    this.r.l(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(aVar.e);
                    this.r.c(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(aVar.f);
                    this.r.a(fragment, true);
                    this.r.g(fragment);
                    break;
                case 8:
                    this.r.w(null);
                    break;
                case 9:
                    this.r.w(fragment);
                    break;
                case 10:
                    this.r.a(fragment, aVar.g);
                    break;
            }
            if (!(((nc) this).p || aVar.a == 3 || fragment == null)) {
                this.r.q(fragment);
            }
        }
        if (!((nc) this).p && z) {
            FragmentManager fragmentManager = this.r;
            fragmentManager.a(fragmentManager.n, true);
        }
    }

    @DexIgnore
    public boolean b(int i) {
        int size = ((nc) this).a.size();
        for (int i2 = 0; i2 < size; i2++) {
            Fragment fragment = ((nc) this).a.get(i2).b;
            int i3 = fragment != null ? fragment.mContainerId : 0;
            if (i3 != 0 && i3 == i) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public Fragment b(ArrayList<Fragment> arrayList, Fragment fragment) {
        for (int size = ((nc) this).a.size() - 1; size >= 0; size--) {
            nc.a aVar = ((nc) this).a.get(size);
            int i = aVar.a;
            if (i != 1) {
                if (i != 3) {
                    switch (i) {
                        case 8:
                            fragment = null;
                            break;
                        case 9:
                            fragment = aVar.b;
                            break;
                        case 10:
                            aVar.h = aVar.g;
                            break;
                    }
                }
                arrayList.add(aVar.b);
            }
            arrayList.remove(aVar.b);
        }
        return fragment;
    }

    @DexIgnore
    public static boolean b(nc.a aVar) {
        Fragment fragment = aVar.b;
        return fragment != null && fragment.mAdded && fragment.mView != null && !fragment.mDetached && !fragment.mHidden && fragment.isPostponed();
    }

    @DexIgnore
    @Override // com.fossil.nc
    public void a(int i, Fragment fragment, String str, int i2) {
        super.a(i, fragment, str, i2);
        fragment.mFragmentManager = this.r;
    }

    @DexIgnore
    @Override // com.fossil.nc
    public nc a(Fragment fragment, Lifecycle.State state) {
        if (fragment.mFragmentManager != this.r) {
            throw new IllegalArgumentException("Cannot setMaxLifecycle for Fragment not attached to FragmentManager " + this.r);
        } else if (state.isAtLeast(Lifecycle.State.CREATED)) {
            super.a(fragment, state);
            return this;
        } else {
            throw new IllegalArgumentException("Cannot set maximum Lifecycle below " + Lifecycle.State.CREATED);
        }
    }

    @DexIgnore
    public void a(int i) {
        if (((nc) this).g) {
            if (FragmentManager.d(2)) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i);
            }
            int size = ((nc) this).a.size();
            for (int i2 = 0; i2 < size; i2++) {
                nc.a aVar = ((nc) this).a.get(i2);
                Fragment fragment = aVar.b;
                if (fragment != null) {
                    fragment.mBackStackNesting += i;
                    if (FragmentManager.d(2)) {
                        Log.v("FragmentManager", "Bump nesting of " + aVar.b + " to " + aVar.b.mBackStackNesting);
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.nc
    public int a() {
        return b(false);
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentManager.h
    public boolean a(ArrayList<yb> arrayList, ArrayList<Boolean> arrayList2) {
        if (FragmentManager.d(2)) {
            Log.v("FragmentManager", "Run: " + this);
        }
        arrayList.add(this);
        arrayList2.add(false);
        if (!((nc) this).g) {
            return true;
        }
        this.r.a(this);
        return true;
    }

    @DexIgnore
    public boolean a(ArrayList<yb> arrayList, int i, int i2) {
        if (i2 == i) {
            return false;
        }
        int size = ((nc) this).a.size();
        int i3 = -1;
        for (int i4 = 0; i4 < size; i4++) {
            Fragment fragment = ((nc) this).a.get(i4).b;
            int i5 = fragment != null ? fragment.mContainerId : 0;
            if (!(i5 == 0 || i5 == i3)) {
                for (int i6 = i; i6 < i2; i6++) {
                    yb ybVar = arrayList.get(i6);
                    int size2 = ((nc) ybVar).a.size();
                    for (int i7 = 0; i7 < size2; i7++) {
                        Fragment fragment2 = ((nc) ybVar).a.get(i7).b;
                        if ((fragment2 != null ? fragment2.mContainerId : 0) == i5) {
                            return true;
                        }
                    }
                }
                i3 = i5;
            }
        }
        return false;
    }

    @DexIgnore
    public Fragment a(ArrayList<Fragment> arrayList, Fragment fragment) {
        Fragment fragment2 = fragment;
        int i = 0;
        while (i < ((nc) this).a.size()) {
            nc.a aVar = ((nc) this).a.get(i);
            int i2 = aVar.a;
            if (i2 != 1) {
                if (i2 == 2) {
                    Fragment fragment3 = aVar.b;
                    int i3 = fragment3.mContainerId;
                    boolean z = false;
                    for (int size = arrayList.size() - 1; size >= 0; size--) {
                        Fragment fragment4 = arrayList.get(size);
                        if (fragment4.mContainerId == i3) {
                            if (fragment4 == fragment3) {
                                z = true;
                            } else {
                                if (fragment4 == fragment2) {
                                    ((nc) this).a.add(i, new nc.a(9, fragment4));
                                    i++;
                                    fragment2 = null;
                                }
                                nc.a aVar2 = new nc.a(3, fragment4);
                                aVar2.c = aVar.c;
                                aVar2.e = aVar.e;
                                aVar2.d = aVar.d;
                                aVar2.f = aVar.f;
                                ((nc) this).a.add(i, aVar2);
                                arrayList.remove(fragment4);
                                i++;
                            }
                        }
                    }
                    if (z) {
                        ((nc) this).a.remove(i);
                        i--;
                    } else {
                        aVar.a = 1;
                        arrayList.add(fragment3);
                    }
                } else if (i2 == 3 || i2 == 6) {
                    arrayList.remove(aVar.b);
                    Fragment fragment5 = aVar.b;
                    if (fragment5 == fragment2) {
                        ((nc) this).a.add(i, new nc.a(9, fragment5));
                        i++;
                        fragment2 = null;
                    }
                } else if (i2 != 7) {
                    if (i2 == 8) {
                        ((nc) this).a.add(i, new nc.a(9, fragment2));
                        i++;
                        fragment2 = aVar.b;
                    }
                }
                i++;
            }
            arrayList.add(aVar.b);
            i++;
        }
        return fragment2;
    }

    @DexIgnore
    public void a(Fragment.OnStartEnterTransitionListener onStartEnterTransitionListener) {
        for (int i = 0; i < ((nc) this).a.size(); i++) {
            nc.a aVar = ((nc) this).a.get(i);
            if (b(aVar)) {
                aVar.b.setOnStartEnterTransitionListener(onStartEnterTransitionListener);
            }
        }
    }
}
