package com.fossil;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.fossil.zs;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"MissingPermission"})
public final class at implements zs {
    @DexIgnore
    public static /* final */ IntentFilter e; // = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
    @DexIgnore
    public /* final */ b b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ ConnectivityManager d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ at a;
        @DexIgnore
        public /* final */ /* synthetic */ zs.b b;

        @DexIgnore
        public b(at atVar, zs.b bVar) {
            this.a = atVar;
            this.b = bVar;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            if (ee7.a((Object) (intent != null ? intent.getAction() : null), (Object) "android.net.conn.CONNECTIVITY_CHANGE")) {
                this.b.a(this.a.a());
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public at(Context context, ConnectivityManager connectivityManager, zs.b bVar) {
        ee7.b(context, "context");
        ee7.b(connectivityManager, "connectivityManager");
        ee7.b(bVar, "listener");
        this.c = context;
        this.d = connectivityManager;
        this.b = new b(this, bVar);
    }

    @DexIgnore
    @Override // com.fossil.zs
    public boolean a() {
        NetworkInfo activeNetworkInfo = this.d.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    @DexIgnore
    @Override // com.fossil.zs
    public void start() {
        this.c.registerReceiver(this.b, e);
    }

    @DexIgnore
    @Override // com.fossil.zs
    public void stop() {
        this.c.unregisterReceiver(this.b);
    }
}
