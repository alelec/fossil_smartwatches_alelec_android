package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.s87;
import java.lang.reflect.Method;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yu7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fe7 implements gd7<Throwable, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ Call $this_await$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Call call) {
            super(1);
            this.$this_await$inlined = call;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(Throwable th) {
            invoke(th);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(Throwable th) {
            this.$this_await$inlined.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<Throwable, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ Call $this_await$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Call call) {
            super(1);
            this.$this_await$inlined = call;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(Throwable th) {
            invoke(th);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(Throwable th) {
            this.$this_await$inlined.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ru7<T> {
        @DexIgnore
        public /* final */ /* synthetic */ ai7 a;

        @DexIgnore
        public c(ai7 ai7) {
            this.a = ai7;
        }

        @DexIgnore
        @Override // com.fossil.ru7
        public void onFailure(Call<T> call, Throwable th) {
            ee7.b(call, "call");
            ee7.b(th, "t");
            ai7 ai7 = this.a;
            s87.a aVar = s87.Companion;
            ai7.resumeWith(s87.m60constructorimpl(t87.a(th)));
        }

        @DexIgnore
        @Override // com.fossil.ru7
        public void onResponse(Call<T> call, fv7<T> fv7) {
            ee7.b(call, "call");
            ee7.b(fv7, "response");
            if (fv7.d()) {
                T a2 = fv7.a();
                if (a2 == null) {
                    Object a3 = call.c().a(xu7.class);
                    if (a3 != null) {
                        ee7.a(a3, "call.request().tag(Invocation::class.java)!!");
                        Method a4 = ((xu7) a3).a();
                        StringBuilder sb = new StringBuilder();
                        sb.append("Response from ");
                        ee7.a((Object) a4, "method");
                        Class<?> declaringClass = a4.getDeclaringClass();
                        ee7.a((Object) declaringClass, "method.declaringClass");
                        sb.append(declaringClass.getName());
                        sb.append('.');
                        sb.append(a4.getName());
                        sb.append(" was null but response body type was declared as non-null");
                        l87 l87 = new l87(sb.toString());
                        ai7 ai7 = this.a;
                        s87.a aVar = s87.Companion;
                        ai7.resumeWith(s87.m60constructorimpl(t87.a((Throwable) l87)));
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ai7 ai72 = this.a;
                s87.a aVar2 = s87.Companion;
                ai72.resumeWith(s87.m60constructorimpl(a2));
                return;
            }
            ai7 ai73 = this.a;
            vu7 vu7 = new vu7(fv7);
            s87.a aVar3 = s87.Companion;
            ai73.resumeWith(s87.m60constructorimpl(t87.a((Throwable) vu7)));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ru7<T> {
        @DexIgnore
        public /* final */ /* synthetic */ ai7 a;

        @DexIgnore
        public d(ai7 ai7) {
            this.a = ai7;
        }

        @DexIgnore
        @Override // com.fossil.ru7
        public void onFailure(Call<T> call, Throwable th) {
            ee7.b(call, "call");
            ee7.b(th, "t");
            ai7 ai7 = this.a;
            s87.a aVar = s87.Companion;
            ai7.resumeWith(s87.m60constructorimpl(t87.a(th)));
        }

        @DexIgnore
        @Override // com.fossil.ru7
        public void onResponse(Call<T> call, fv7<T> fv7) {
            ee7.b(call, "call");
            ee7.b(fv7, "response");
            if (fv7.d()) {
                ai7 ai7 = this.a;
                T a2 = fv7.a();
                s87.a aVar = s87.Companion;
                ai7.resumeWith(s87.m60constructorimpl(a2));
                return;
            }
            ai7 ai72 = this.a;
            vu7 vu7 = new vu7(fv7);
            s87.a aVar2 = s87.Companion;
            ai72.resumeWith(s87.m60constructorimpl(t87.a((Throwable) vu7)));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends fe7 implements gd7<Throwable, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ Call $this_awaitResponse$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(Call call) {
            super(1);
            this.$this_awaitResponse$inlined = call;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(Throwable th) {
            invoke(th);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(Throwable th) {
            this.$this_awaitResponse$inlined.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ru7<T> {
        @DexIgnore
        public /* final */ /* synthetic */ ai7 a;

        @DexIgnore
        public f(ai7 ai7) {
            this.a = ai7;
        }

        @DexIgnore
        @Override // com.fossil.ru7
        public void onFailure(Call<T> call, Throwable th) {
            ee7.b(call, "call");
            ee7.b(th, "t");
            ai7 ai7 = this.a;
            s87.a aVar = s87.Companion;
            ai7.resumeWith(s87.m60constructorimpl(t87.a(th)));
        }

        @DexIgnore
        @Override // com.fossil.ru7
        public void onResponse(Call<T> call, fv7<T> fv7) {
            ee7.b(call, "call");
            ee7.b(fv7, "response");
            ai7 ai7 = this.a;
            s87.a aVar = s87.Companion;
            ai7.resumeWith(s87.m60constructorimpl(fv7));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "retrofit2/KotlinExtensions", f = "KotlinExtensions.kt", l = {100, 102}, m = "yieldAndThrow")
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;

        @DexIgnore
        public g(fb7 fb7) {
            super(fb7);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return yu7.a((Exception) null, this);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Object a(java.lang.Exception r4, com.fossil.fb7<?> r5) {
        /*
            boolean r0 = r5 instanceof com.fossil.yu7.g
            if (r0 == 0) goto L_0x0013
            r0 = r5
            com.fossil.yu7$g r0 = (com.fossil.yu7.g) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.yu7$g r0 = new com.fossil.yu7$g
            r0.<init>(r5)
        L_0x0018:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003a
            if (r2 != r3) goto L_0x0032
            java.lang.Object r4 = r0.L$0
            java.lang.Exception r4 = (java.lang.Exception) r4
            boolean r0 = r5 instanceof com.fossil.s87.b
            if (r0 == 0) goto L_0x0049
            com.fossil.s87$b r5 = (com.fossil.s87.b) r5
            java.lang.Throwable r4 = r5.exception
            throw r4
        L_0x0032:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            java.lang.String r5 = "call to 'resume' before 'invoke' with coroutine"
            r4.<init>(r5)
            throw r4
        L_0x003a:
            boolean r2 = r5 instanceof com.fossil.s87.b
            if (r2 != 0) goto L_0x004a
            r0.L$0 = r4
            r0.label = r3
            java.lang.Object r5 = com.fossil.ol7.a(r0)
            if (r5 != r1) goto L_0x0049
            return r1
        L_0x0049:
            throw r4
        L_0x004a:
            com.fossil.s87$b r5 = (com.fossil.s87.b) r5
            java.lang.Throwable r4 = r5.exception
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yu7.a(java.lang.Exception, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public static final <T> Object b(Call<T> call, fb7<? super T> fb7) {
        bi7 bi7 = new bi7(mb7.a(fb7), 1);
        bi7.a((gd7<? super Throwable, i97>) new b(call));
        call.a(new d(bi7));
        Object g2 = bi7.g();
        if (g2 == nb7.a()) {
            vb7.c(fb7);
        }
        return g2;
    }

    @DexIgnore
    public static final <T> Object c(Call<T> call, fb7<? super fv7<T>> fb7) {
        bi7 bi7 = new bi7(mb7.a(fb7), 1);
        bi7.a((gd7<? super Throwable, i97>) new e(call));
        call.a(new f(bi7));
        Object g2 = bi7.g();
        if (g2 == nb7.a()) {
            vb7.c(fb7);
        }
        return g2;
    }

    @DexIgnore
    public static final <T> Object a(Call<T> call, fb7<? super T> fb7) {
        bi7 bi7 = new bi7(mb7.a(fb7), 1);
        bi7.a((gd7<? super Throwable, i97>) new a(call));
        call.a(new c(bi7));
        Object g2 = bi7.g();
        if (g2 == nb7.a()) {
            vb7.c(fb7);
        }
        return g2;
    }
}
