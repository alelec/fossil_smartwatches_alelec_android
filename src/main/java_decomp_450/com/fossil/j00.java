package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.fossil.ix;
import com.fossil.m00;
import java.io.File;
import java.io.FileNotFoundException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j00 implements m00<Uri, File> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements n00<Uri, File> {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public a(Context context) {
            this.a = context;
        }

        @DexIgnore
        @Override // com.fossil.n00
        public m00<Uri, File> a(q00 q00) {
            return new j00(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements ix<File> {
        @DexIgnore
        public static /* final */ String[] c; // = {"_data"};
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ Uri b;

        @DexIgnore
        public b(Context context, Uri uri) {
            this.a = context;
            this.b = uri;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a(ew ewVar, ix.a<? super File> aVar) {
            Cursor query = this.a.getContentResolver().query(this.b, c, null, null, null);
            String str = null;
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        str = query.getString(query.getColumnIndexOrThrow("_data"));
                    }
                } finally {
                    query.close();
                }
            }
            if (TextUtils.isEmpty(str)) {
                aVar.a((Exception) new FileNotFoundException("Failed to find file path for: " + this.b));
                return;
            }
            aVar.a(new File(str));
        }

        @DexIgnore
        @Override // com.fossil.ix
        public sw b() {
            return sw.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.ix
        public Class<File> getDataClass() {
            return File.class;
        }
    }

    @DexIgnore
    public j00(Context context) {
        this.a = context;
    }

    @DexIgnore
    public m00.a<File> a(Uri uri, int i, int i2, ax axVar) {
        return new m00.a<>(new k50(uri), new b(this.a, uri));
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return vx.b(uri);
    }
}
