package com.fossil;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.SystemClock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q50 {
    @DexIgnore
    public static /* final */ double a;

    /*
    static {
        double d = 1.0d;
        if (Build.VERSION.SDK_INT >= 17) {
            d = 1.0d / Math.pow(10.0d, 6.0d);
        }
        a = d;
    }
    */

    @DexIgnore
    @TargetApi(17)
    public static long a() {
        if (Build.VERSION.SDK_INT >= 17) {
            return SystemClock.elapsedRealtimeNanos();
        }
        return SystemClock.uptimeMillis();
    }

    @DexIgnore
    public static double a(long j) {
        return ((double) (a() - j)) * a;
    }
}
