package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum yr0 {
    GET((byte) 0),
    SET((byte) 1);
    
    @DexIgnore
    public static /* final */ cq0 e; // = new cq0(null);
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public yr0(byte b) {
        this.a = b;
    }
}
