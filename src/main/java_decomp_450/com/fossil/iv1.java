package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iv1 implements Factory<hv1> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<fv1> b;

    @DexIgnore
    public iv1(Provider<Context> provider, Provider<fv1> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static iv1 a(Provider<Context> provider, Provider<fv1> provider2) {
        return new iv1(provider, provider2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public hv1 get() {
        return new hv1(this.a.get(), this.b.get());
    }
}
