package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface sb7 {
    @DexIgnore
    sb7 getCallerFrame();

    @DexIgnore
    StackTraceElement getStackTraceElement();
}
