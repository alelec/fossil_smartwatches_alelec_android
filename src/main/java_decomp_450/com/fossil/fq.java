package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fq<TResult> {
    @DexIgnore
    public /* final */ eq<TResult> a; // = new eq<>();

    @DexIgnore
    public eq<TResult> a() {
        return this.a;
    }

    @DexIgnore
    public boolean b(TResult tresult) {
        return this.a.a((Object) tresult);
    }

    @DexIgnore
    public boolean c() {
        return this.a.g();
    }

    @DexIgnore
    public void a(TResult tresult) {
        if (!b((Object) tresult)) {
            throw new IllegalStateException("Cannot set the result of a completed task.");
        }
    }

    @DexIgnore
    public boolean b(Exception exc) {
        return this.a.a(exc);
    }

    @DexIgnore
    public void b() {
        if (!c()) {
            throw new IllegalStateException("Cannot cancel a completed task.");
        }
    }

    @DexIgnore
    public void a(Exception exc) {
        if (!b(exc)) {
            throw new IllegalStateException("Cannot set the error on a completed task.");
        }
    }
}
