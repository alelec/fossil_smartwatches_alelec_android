package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum eh4 {
    Cp437(new int[]{0, 2}, new String[0]),
    ISO8859_1(new int[]{1, 3}, "ISO-8859-1"),
    ISO8859_2(4, "ISO-8859-2"),
    ISO8859_3(5, "ISO-8859-3"),
    ISO8859_4(6, "ISO-8859-4"),
    ISO8859_5(7, "ISO-8859-5"),
    ISO8859_6(8, "ISO-8859-6"),
    ISO8859_7(9, "ISO-8859-7"),
    ISO8859_8(10, "ISO-8859-8"),
    ISO8859_9(11, "ISO-8859-9"),
    ISO8859_10(12, "ISO-8859-10"),
    ISO8859_11(13, "ISO-8859-11"),
    ISO8859_13(15, "ISO-8859-13"),
    ISO8859_14(16, "ISO-8859-14"),
    ISO8859_15(17, "ISO-8859-15"),
    ISO8859_16(18, "ISO-8859-16"),
    SJIS(20, "Shift_JIS"),
    Cp1250(21, "windows-1250"),
    Cp1251(22, "windows-1251"),
    Cp1252(23, "windows-1252"),
    Cp1256(24, "windows-1256"),
    UnicodeBigUnmarked(25, "UTF-16BE", "UnicodeBig"),
    UTF8(26, "UTF-8"),
    ASCII(new int[]{27, 170}, "US-ASCII"),
    Big5(28),
    GB18030(29, "GB2312", "EUC_CN", "GBK"),
    EUC_KR(30, "EUC-KR");
    
    @DexIgnore
    public static /* final */ Map<Integer, eh4> a; // = new HashMap();
    @DexIgnore
    public static /* final */ Map<String, eh4> b; // = new HashMap();
    @DexIgnore
    public /* final */ String[] otherEncodingNames;
    @DexIgnore
    public /* final */ int[] values;

    /*
    static {
        eh4[] values2 = values();
        for (eh4 eh4 : values2) {
            for (int i : eh4.values) {
                a.put(Integer.valueOf(i), eh4);
            }
            b.put(eh4.name(), eh4);
            for (String str : eh4.otherEncodingNames) {
                b.put(str, eh4);
            }
        }
    }
    */

    @DexIgnore
    public eh4(int i) {
        this(new int[]{i}, new String[0]);
    }

    @DexIgnore
    public static eh4 getCharacterSetECIByName(String str) {
        return b.get(str);
    }

    @DexIgnore
    public static eh4 getCharacterSetECIByValue(int i) throws pg4 {
        if (i >= 0 && i < 900) {
            return a.get(Integer.valueOf(i));
        }
        throw pg4.getFormatInstance();
    }

    @DexIgnore
    public int getValue() {
        return this.values[0];
    }

    @DexIgnore
    public eh4(int i, String... strArr) {
        this.values = new int[]{i};
        this.otherEncodingNames = strArr;
    }

    @DexIgnore
    public eh4(int[] iArr, String... strArr) {
        this.values = iArr;
        this.otherEncodingNames = strArr;
    }
}
