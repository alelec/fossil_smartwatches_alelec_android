package com.fossil;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ch7 {
    @DexIgnore
    public static final yg7 b(Matcher matcher, int i, CharSequence charSequence) {
        if (!matcher.find(i)) {
            return null;
        }
        return new zg7(matcher, charSequence);
    }

    @DexIgnore
    public static final yg7 b(Matcher matcher, CharSequence charSequence) {
        if (!matcher.matches()) {
            return null;
        }
        return new zg7(matcher, charSequence);
    }

    @DexIgnore
    public static final lf7 b(MatchResult matchResult) {
        return qf7.d(matchResult.start(), matchResult.end());
    }

    @DexIgnore
    public static final lf7 b(MatchResult matchResult, int i) {
        return qf7.d(matchResult.start(i), matchResult.end(i));
    }

    @DexIgnore
    public static final int b(Iterable<? extends ug7> iterable) {
        int i = 0;
        for (ug7 ug7 : iterable) {
            i |= ug7.getValue();
        }
        return i;
    }
}
