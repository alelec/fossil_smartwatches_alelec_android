package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class to5 extends RecyclerView.g<a> {
    @DexIgnore
    public List<AddressWrapper> a;
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;
        @DexIgnore
        public /* final */ TextView b;
        @DexIgnore
        public /* final */ ImageView c;
        @DexIgnore
        public /* final */ /* synthetic */ to5 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.to5$a$a")
        /* renamed from: com.fossil.to5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0186a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public View$OnClickListenerC0186a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b b;
                int adapterPosition = this.a.getAdapterPosition();
                if (this.a.getAdapterPosition() != -1 && (b = this.a.d.b) != null) {
                    List a2 = this.a.d.a;
                    if (a2 != null) {
                        b.a((AddressWrapper) a2.get(adapterPosition));
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(to5 to5, View view) {
            super(view);
            ee7.b(view, "view");
            this.d = to5;
            view.setOnClickListener(new View$OnClickListenerC0186a(this));
            View findViewById = view.findViewById(2131362517);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131362360);
                if (findViewById2 != null) {
                    this.b = (TextView) findViewById2;
                    View findViewById3 = view.findViewById(2131362696);
                    if (findViewById3 != null) {
                        this.c = (ImageView) findViewById3;
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.b;
        }

        @DexIgnore
        public final ImageView b() {
            return this.c;
        }

        @DexIgnore
        public final TextView c() {
            return this.a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(AddressWrapper addressWrapper);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<AddressWrapper> list = this.a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        ee7.b(aVar, "holder");
        List<AddressWrapper> list = this.a;
        if (list != null) {
            AddressWrapper addressWrapper = list.get(i);
            aVar.c().setText(addressWrapper.getName());
            aVar.a().setText(addressWrapper.getAddress());
            int i2 = uo5.a[addressWrapper.getType().ordinal()];
            if (i2 == 1) {
                aVar.b().setImageDrawable(v6.c(PortfolioApp.g0.c(), 2131230984));
                aVar.c().setText(ig5.a(PortfolioApp.g0.c(), 2131886348));
            } else if (i2 == 2) {
                aVar.b().setImageDrawable(v6.c(PortfolioApp.g0.c(), 2131230986));
                aVar.c().setText(ig5.a(PortfolioApp.g0.c(), 2131886350));
            } else if (i2 == 3) {
                aVar.b().setImageDrawable(v6.c(PortfolioApp.g0.c(), 2131230985));
                aVar.c().setText(addressWrapper.getName());
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558654, viewGroup, false);
        ee7.a((Object) inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void a(List<AddressWrapper> list) {
        ee7.b(list, "addressList");
        this.a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final AddressWrapper a(int i) {
        if (i != -1) {
            List<AddressWrapper> list = this.a;
            if (list == null) {
                ee7.a();
                throw null;
            } else if (i <= list.size()) {
                List<AddressWrapper> list2 = this.a;
                if (list2 != null) {
                    return list2.get(i);
                }
                ee7.a();
                throw null;
            }
        }
        return null;
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "listener");
        this.b = bVar;
    }
}
