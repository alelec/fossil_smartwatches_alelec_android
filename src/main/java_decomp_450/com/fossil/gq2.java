package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gq2 extends cr2 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ tr2<pr2<pq2>> b;

    @DexIgnore
    public gq2(Context context, tr2<pr2<pq2>> tr2) {
        if (context != null) {
            this.a = context;
            this.b = tr2;
            return;
        }
        throw new NullPointerException("Null context");
    }

    @DexIgnore
    @Override // com.fossil.cr2
    public final Context a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.cr2
    public final tr2<pr2<pq2>> b() {
        return this.b;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        tr2<pr2<pq2>> tr2;
        if (obj == this) {
            return true;
        }
        if (obj instanceof cr2) {
            cr2 cr2 = (cr2) obj;
            return this.a.equals(cr2.a()) && ((tr2 = this.b) != null ? tr2.equals(cr2.b()) : cr2.b() == null);
        }
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        tr2<pr2<pq2>> tr2 = this.b;
        return hashCode ^ (tr2 == null ? 0 : tr2.hashCode());
    }

    @DexIgnore
    public final String toString() {
        String valueOf = String.valueOf(this.a);
        String valueOf2 = String.valueOf(this.b);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 46 + String.valueOf(valueOf2).length());
        sb.append("FlagsContext{context=");
        sb.append(valueOf);
        sb.append(", hermeticFileOverrides=");
        sb.append(valueOf2);
        sb.append("}");
        return sb.toString();
    }
}
