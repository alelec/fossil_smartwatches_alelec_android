package com.fossil;

import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.zendesk.sdk.model.request.CustomField;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fn5 extends fl4<c, d, b> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ long h; // = (ee7.a("release", "release") ? 25044663 : 360000146526L);
    @DexIgnore
    public static /* final */ long i; // = (ee7.a("release", "release") ? 23451689 : 24435966);
    @DexIgnore
    public static /* final */ long j; // = (ee7.a("release", "release") ? 360000024083L : 360000148503L);
    @DexIgnore
    public static /* final */ long k; // = (ee7.a("release", "release") ? 24416029 : 24436006);
    @DexIgnore
    public static /* final */ long l; // = (ee7.a("release", "release") ? 24504683 : 24881463);
    @DexIgnore
    public static /* final */ long m; // = (ee7.a("release", "release") ? 23777683 : 24435986);
    @DexIgnore
    public static /* final */ long n; // = (ee7.a("release", "release") ? 24545186 : 24881503);
    @DexIgnore
    public static /* final */ long o; // = (ee7.a("release", "release") ? 24545246 : 24945726);
    @DexIgnore
    public static /* final */ long p; // = (ee7.a("release", "release") ? 23780847 : 24436086);
    @DexIgnore
    public static /* final */ long q; // = (ee7.a("release", "release") ? 24935086 : 24881543);
    @DexIgnore
    public static /* final */ long r; // = (ee7.a("release", "release") ? 24506223 : 24881523);
    @DexIgnore
    public static /* final */ long s; // = (ee7.a("release", "release") ? 360000156783L : 360000156723L);
    @DexIgnore
    public static /* final */ long t; // = (ee7.a("release", "release") ? 360000156803L : 360000156743L);
    @DexIgnore
    public static /* final */ long u; // = (ee7.a("release", "release") ? 360000156823L : 360000156763L);
    @DexIgnore
    public static /* final */ long v; // = (ee7.a("release", "release") ? 60000157103L : 360000148563L);
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ DeviceRepository e;
    @DexIgnore
    public /* final */ ch5 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public c(String str) {
            ee7.b(str, "subject");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public /* final */ List<CustomField> a;
        @DexIgnore
        public /* final */ List<String> b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ String d;
        @DexIgnore
        public /* final */ String e;
        @DexIgnore
        public /* final */ String f;

        @DexIgnore
        public d(List<CustomField> list, List<String> list2, String str, String str2, String str3, String str4) {
            ee7.b(list, "customFieldList");
            ee7.b(list2, "tags");
            ee7.b(str, Constants.EMAIL);
            ee7.b(str2, "userName");
            ee7.b(str3, "subject");
            ee7.b(str4, "additionalInfo");
            this.a = list;
            this.b = list2;
            this.c = str;
            this.d = str2;
            this.e = str3;
            this.f = str4;
        }

        @DexIgnore
        public final String a() {
            return this.f;
        }

        @DexIgnore
        public final List<CustomField> b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.e;
        }

        @DexIgnore
        public final List<String> e() {
            return this.b;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.profile.usecase.GetZendeskInformation", f = "GetZendeskInformation.kt", l = {25}, m = "run")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ fn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(fn5 fn5, fb7 fb7) {
            super(fb7);
            this.this$0 = fn5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((c) null, (fb7<? super i97>) this);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = fn5.class.getSimpleName();
        ee7.a((Object) simpleName, "GetZendeskInformation::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public fn5(UserRepository userRepository, DeviceRepository deviceRepository, ch5 ch5) {
        ee7.b(userRepository, "mUserRepository");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.d = userRepository;
        this.e = deviceRepository;
        this.f = ch5;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(c cVar, fb7 fb7) {
        return a(cVar, (fb7<? super i97>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:109:0x02d7  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00e1 A[SYNTHETIC, Splitter:B:46:0x00e1] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0108 A[SYNTHETIC, Splitter:B:58:0x0108] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x014d  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x017f  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x018a  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x019e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.fn5.c r31, com.fossil.fb7<? super com.fossil.i97> r32) {
        /*
            r30 = this;
            r0 = r30
            r1 = r32
            boolean r2 = r1 instanceof com.fossil.fn5.e
            if (r2 == 0) goto L_0x0017
            r2 = r1
            com.fossil.fn5$e r2 = (com.fossil.fn5.e) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.fossil.fn5$e r2 = new com.fossil.fn5$e
            r2.<init>(r0, r1)
        L_0x001c:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            r5 = 1
            if (r4 == 0) goto L_0x003d
            if (r4 != r5) goto L_0x0035
            java.lang.Object r3 = r2.L$1
            com.fossil.fn5$c r3 = (com.fossil.fn5.c) r3
            java.lang.Object r2 = r2.L$0
            com.fossil.fn5 r2 = (com.fossil.fn5) r2
            com.fossil.t87.a(r1)
            goto L_0x0053
        L_0x0035:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x003d:
            com.fossil.t87.a(r1)
            com.portfolio.platform.data.source.UserRepository r1 = r0.d
            r2.L$0 = r0
            r4 = r31
            r2.L$1 = r4
            r2.label = r5
            java.lang.Object r1 = r1.getCurrentUser(r2)
            if (r1 != r3) goto L_0x0051
            return r3
        L_0x0051:
            r2 = r0
            r3 = r4
        L_0x0053:
            com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
            if (r1 != 0) goto L_0x0062
            com.fossil.fn5$b r1 = new com.fossil.fn5$b
            r1.<init>()
            r2.a(r1)
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x0062:
            com.portfolio.platform.data.source.DeviceRepository r4 = r2.e
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            java.lang.String r6 = r6.c()
            com.portfolio.platform.data.model.Device r4 = r4.getDeviceBySerial(r6)
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            com.zendesk.sdk.model.request.CustomField r7 = new com.zendesk.sdk.model.request.CustomField
            long r8 = com.fossil.fn5.h
            java.lang.Long r8 = com.fossil.pb7.a(r8)
            java.lang.String r13 = ""
            r7.<init>(r8, r13)
            com.zendesk.sdk.model.request.CustomField r8 = new com.zendesk.sdk.model.request.CustomField
            long r9 = com.fossil.fn5.i
            java.lang.Long r9 = com.fossil.pb7.a(r9)
            java.lang.String r10 = r1.getEmail()
            r8.<init>(r9, r10)
            java.lang.String r9 = "android"
            java.lang.String r10 = "app_feedback"
            java.lang.String[] r10 = new java.lang.String[]{r9, r10}
            java.util.List r15 = com.fossil.w97.d(r10)
            r10 = 0
            java.lang.String r12 = r6.getPackageName()     // Catch:{ Exception -> 0x0115 }
            android.content.pm.PackageManager r6 = r6.getPackageManager()     // Catch:{ Exception -> 0x0115 }
            if (r6 == 0) goto L_0x00b7
            android.content.pm.PackageInfo r14 = r6.getPackageInfo(r12, r10)     // Catch:{ Exception -> 0x00b0 }
            goto L_0x00b8
        L_0x00b0:
            r6 = r13
            r14 = r6
            r5 = 0
        L_0x00b3:
            r11 = 0
            r12 = 0
            goto L_0x011a
        L_0x00b7:
            r14 = 0
        L_0x00b8:
            if (r14 == 0) goto L_0x00bf
            java.lang.String r14 = r14.versionName     // Catch:{ Exception -> 0x00b0 }
            if (r14 == 0) goto L_0x00bf
            goto L_0x00c0
        L_0x00bf:
            r14 = r13
        L_0x00c0:
            com.zendesk.sdk.model.request.CustomField r5 = new com.zendesk.sdk.model.request.CustomField     // Catch:{ Exception -> 0x0110 }
            long r16 = com.fossil.fn5.k     // Catch:{ Exception -> 0x0110 }
            java.lang.Long r11 = com.fossil.pb7.a(r16)     // Catch:{ Exception -> 0x0110 }
            r5.<init>(r11, r14)     // Catch:{ Exception -> 0x0110 }
            if (r6 == 0) goto L_0x00de
            android.content.pm.ApplicationInfo r11 = r6.getApplicationInfo(r12, r10)     // Catch:{ Exception -> 0x00dc }
            java.lang.CharSequence r6 = r6.getApplicationLabel(r11)     // Catch:{ Exception -> 0x00dc }
            if (r6 == 0) goto L_0x00de
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00dc }
            goto L_0x00df
        L_0x00dc:
            r6 = r13
            goto L_0x00b3
        L_0x00de:
            r6 = 0
        L_0x00df:
            if (r6 == 0) goto L_0x0108
            com.zendesk.sdk.model.request.CustomField r11 = new com.zendesk.sdk.model.request.CustomField     // Catch:{ Exception -> 0x00b3 }
            long r16 = com.fossil.fn5.j     // Catch:{ Exception -> 0x00b3 }
            java.lang.Long r12 = com.fossil.pb7.a(r16)     // Catch:{ Exception -> 0x00b3 }
            r11.<init>(r12, r6)     // Catch:{ Exception -> 0x00b3 }
            r17 = 32
            r18 = 95
            r19 = 0
            r20 = 4
            r21 = 0
            r16 = r6
            java.lang.String r12 = com.fossil.mh7.a(r16, r17, r18, r19, r20, r21)     // Catch:{ Exception -> 0x0105 }
            if (r12 == 0) goto L_0x00ff
            goto L_0x0100
        L_0x00ff:
            r12 = r13
        L_0x0100:
            r15.add(r12)     // Catch:{ Exception -> 0x0105 }
            r0 = r6
            goto L_0x0129
        L_0x0105:
            r12 = r11
            r11 = 0
            goto L_0x011a
        L_0x0108:
            com.fossil.ee7.a()     // Catch:{ Exception -> 0x010d }
            r11 = 0
            throw r11
        L_0x010d:
            r11 = 0
            r12 = r11
            goto L_0x0113
        L_0x0110:
            r11 = 0
            r5 = r11
            r12 = r5
        L_0x0113:
            r6 = r13
            goto L_0x011a
        L_0x0115:
            r11 = 0
            r5 = r11
            r12 = r5
            r6 = r13
            r14 = r6
        L_0x011a:
            com.misfit.frameworks.buttonservice.log.FLogger r16 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r16.getLocal()
            java.lang.String r10 = com.fossil.fn5.g
            java.lang.String r0 = "Cannot load package info; will not be saved to installation"
            r11.e(r10, r0)
            r0 = r6
            r11 = r12
        L_0x0129:
            com.zendesk.sdk.model.request.CustomField r6 = new com.zendesk.sdk.model.request.CustomField
            long r17 = com.fossil.fn5.l
            java.lang.Long r10 = com.fossil.pb7.a(r17)
            r6.<init>(r10, r9)
            com.zendesk.sdk.model.request.CustomField r9 = new com.zendesk.sdk.model.request.CustomField
            long r17 = com.fossil.fn5.m
            java.lang.Long r10 = com.fossil.pb7.a(r17)
            java.lang.String r12 = android.os.Build.VERSION.RELEASE
            r9.<init>(r10, r12)
            com.zendesk.sdk.model.request.CustomField r10 = new com.zendesk.sdk.model.request.CustomField
            long r17 = com.fossil.fn5.n
            java.lang.Long r12 = com.fossil.pb7.a(r17)
            java.lang.String r17 = android.os.Build.MANUFACTURER
            if (r17 == 0) goto L_0x0154
            r29 = r17
            r17 = r15
            r15 = r29
            goto L_0x0157
        L_0x0154:
            r17 = r15
            r15 = r13
        L_0x0157:
            r10.<init>(r12, r15)
            com.zendesk.sdk.model.request.CustomField r12 = new com.zendesk.sdk.model.request.CustomField
            long r18 = com.fossil.fn5.o
            java.lang.Long r15 = com.fossil.pb7.a(r18)
            r18 = r1
            java.lang.String r1 = android.os.Build.MODEL
            r12.<init>(r15, r1)
            java.lang.String r1 = android.os.Build.MANUFACTURER
            if (r1 == 0) goto L_0x016e
            goto L_0x016f
        L_0x016e:
            r1 = r13
        L_0x016f:
            java.lang.String r15 = android.os.Build.MODEL
            r19 = r3
            com.zendesk.sdk.model.request.CustomField r3 = new com.zendesk.sdk.model.request.CustomField
            long r20 = com.fossil.fn5.p
            r22 = r15
            java.lang.Long r15 = com.fossil.pb7.a(r20)
            if (r4 == 0) goto L_0x018a
            java.lang.String r20 = r4.getDeviceId()
            r29 = r20
            r20 = r1
            r1 = r29
            goto L_0x018d
        L_0x018a:
            r20 = r1
            r1 = 0
        L_0x018d:
            r3.<init>(r15, r1)
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            java.lang.String r15 = "phone"
            java.lang.Object r1 = r1.getSystemService(r15)
            if (r1 == 0) goto L_0x02d7
            android.telephony.TelephonyManager r1 = (android.telephony.TelephonyManager) r1
            com.zendesk.sdk.model.request.CustomField r15 = new com.zendesk.sdk.model.request.CustomField
            long r23 = com.fossil.fn5.q
            r21 = r14
            java.lang.Long r14 = com.fossil.pb7.a(r23)
            r23 = r0
            java.lang.String r0 = r1.getNetworkOperatorName()
            r15.<init>(r14, r0)
            com.zendesk.sdk.model.request.CustomField r0 = new com.zendesk.sdk.model.request.CustomField
            long r24 = com.fossil.fn5.r
            java.lang.Long r14 = com.fossil.pb7.a(r24)
            if (r4 == 0) goto L_0x01c8
            java.lang.String r24 = r4.getFirmwareRevision()
            r29 = r24
            r24 = r2
            r2 = r29
            goto L_0x01cb
        L_0x01c8:
            r24 = r2
            r2 = 0
        L_0x01cb:
            r0.<init>(r14, r2)
            java.util.Locale r2 = java.util.Locale.getDefault()
            if (r4 == 0) goto L_0x01d9
            java.lang.String r4 = r4.getDeviceId()
            goto L_0x01da
        L_0x01d9:
            r4 = 0
        L_0x01da:
            java.lang.String r1 = r1.getNetworkOperatorName()
            com.zendesk.sdk.model.request.CustomField r14 = new com.zendesk.sdk.model.request.CustomField
            long r25 = com.fossil.fn5.s
            r31 = r1
            java.lang.Long r1 = com.fossil.pb7.a(r25)
            r25 = r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r26 = r11
            java.lang.String r11 = "currentLocale"
            com.fossil.ee7.a(r2, r11)
            java.lang.String r11 = r2.getLanguage()
            r4.append(r11)
            r11 = 45
            r4.append(r11)
            java.lang.String r11 = r2.getScript()
            r4.append(r11)
            java.lang.String r4 = r4.toString()
            r14.<init>(r1, r4)
            com.zendesk.sdk.model.request.CustomField r1 = new com.zendesk.sdk.model.request.CustomField
            long r27 = com.fossil.fn5.t
            java.lang.Long r4 = com.fossil.pb7.a(r27)
            java.lang.String r2 = r2.getCountry()
            r1.<init>(r4, r2)
            com.zendesk.sdk.model.request.CustomField r2 = new com.zendesk.sdk.model.request.CustomField
            long r27 = com.fossil.fn5.u
            java.lang.Long r4 = com.fossil.pb7.a(r27)
            r2.<init>(r4, r13)
            com.zendesk.sdk.model.request.CustomField r4 = new com.zendesk.sdk.model.request.CustomField
            long r27 = com.fossil.fn5.v
            java.lang.Long r11 = com.fossil.pb7.a(r27)
            com.misfit.frameworks.buttonservice.ButtonService$Companion r27 = com.misfit.frameworks.buttonservice.ButtonService.Companion
            r28 = r13
            java.lang.String r13 = r27.getSDKVersion()
            r4.<init>(r11, r13)
            r11 = 13
            com.zendesk.sdk.model.request.CustomField[] r11 = new com.zendesk.sdk.model.request.CustomField[r11]
            r13 = 0
            r11[r13] = r7
            r7 = 1
            r11[r7] = r8
            r7 = 2
            r11[r7] = r6
            r6 = 3
            r11[r6] = r9
            r6 = 4
            r11[r6] = r10
            r6 = 5
            r11[r6] = r12
            r6 = 6
            r11[r6] = r3
            r3 = 7
            r11[r3] = r15
            r3 = 8
            r11[r3] = r0
            r0 = 9
            r11[r0] = r14
            r0 = 10
            r11[r0] = r1
            r0 = 11
            r11[r0] = r2
            r0 = 12
            r11[r0] = r4
            java.util.List r15 = com.fossil.w97.d(r11)
            if (r5 == 0) goto L_0x027a
            boolean r0 = r15.add(r5)
            com.fossil.pb7.a(r0)
        L_0x027a:
            if (r26 == 0) goto L_0x0285
            r11 = r26
            boolean r0 = r15.add(r11)
            com.fossil.pb7.a(r0)
        L_0x0285:
            r6 = r24
            r7 = r23
            r8 = r21
            r9 = r25
            r10 = r20
            r11 = r22
            r12 = r31
            java.lang.StringBuilder r0 = r6.a(r7, r8, r9, r10, r11, r12)
            if (r19 == 0) goto L_0x02a0
            java.lang.String r1 = r19.a()
            if (r1 == 0) goto L_0x02a0
            goto L_0x02a2
        L_0x02a0:
            java.lang.String r1 = "Feedback - From app [Fossil] - [Android]"
        L_0x02a2:
            r19 = r1
            com.fossil.fn5$d r1 = new com.fossil.fn5$d
            java.lang.String r2 = r18.getEmail()
            if (r2 == 0) goto L_0x02ad
            goto L_0x02af
        L_0x02ad:
            r2 = r28
        L_0x02af:
            java.lang.String r3 = r18.getUsername()
            if (r3 == 0) goto L_0x02b8
            r18 = r3
            goto L_0x02ba
        L_0x02b8:
            r18 = r28
        L_0x02ba:
            java.lang.String r0 = r0.toString()
            java.lang.String r3 = "additionalInfo.toString()"
            com.fossil.ee7.a(r0, r3)
            r14 = r1
            r3 = r17
            r16 = r3
            r17 = r2
            r20 = r0
            r14.<init>(r15, r16, r17, r18, r19, r20)
            r0 = r24
            r0.a(r1)
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
        L_0x02d7:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r1 = "null cannot be cast to non-null type android.telephony.TelephonyManager"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fn5.a(com.fossil.fn5$c, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final StringBuilder a(String str, String str2, String str3, String str4, String str5, String str6) {
        StringBuilder sb = new StringBuilder();
        String str7 = Build.DEVICE + " - " + Build.MODEL;
        String str8 = Build.VERSION.RELEASE;
        StringBuilder sb2 = new StringBuilder();
        Locale locale = Locale.getDefault();
        ee7.a((Object) locale, "Locale.getDefault()");
        sb2.append(locale.getLanguage());
        sb2.append("_t");
        String sb3 = sb2.toString();
        String str9 = "";
        String str10 = str9;
        String str11 = str10;
        for (T t2 : this.e.getAllDevice()) {
            str10 = str10 + " " + t2.getDeviceId();
            if (t2.isActive()) {
                long e2 = this.f.e(t2.getDeviceId());
                StringBuilder sb4 = new StringBuilder();
                sb4.append(str11);
                we7 we7 = we7.a;
                String format = String.format(" %d (%s)", Arrays.copyOf(new Object[]{Long.valueOf(e2 / ((long) 1000)), zd5.e(new Date(e2))}, 2));
                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                sb4.append(format);
                str11 = sb4.toString();
            }
        }
        sb.append("App Name: " + str);
        ee7.a((Object) sb, "append(value)");
        ih7.a(sb);
        sb.append("App Version: " + str2);
        ee7.a((Object) sb, "append(value)");
        ih7.a(sb);
        sb.append("Build number: " + "26467-2020-08-13");
        ee7.a((Object) sb, "append(value)");
        ih7.a(sb);
        sb.append("Phone Info: " + str7);
        ee7.a((Object) sb, "append(value)");
        ih7.a(sb);
        sb.append("System version: " + str8);
        ee7.a((Object) sb, "append(value)");
        ih7.a(sb);
        sb.append("Host Maker: " + str4);
        ee7.a((Object) sb, "append(value)");
        ih7.a(sb);
        sb.append("Host Model: " + str5);
        ee7.a((Object) sb, "append(value)");
        ih7.a(sb);
        sb.append("Carrier: " + str6);
        ee7.a((Object) sb, "append(value)");
        ih7.a(sb);
        sb.append("Language code: " + sb3);
        ee7.a((Object) sb, "append(value)");
        ih7.a(sb);
        sb.append("_______________");
        ee7.a((Object) sb, "append(value)");
        ih7.a(sb);
        StringBuilder sb5 = new StringBuilder();
        sb5.append("Serial ");
        if (str3 != null) {
            str9 = str3;
        }
        sb5.append(str9);
        sb.append(sb5.toString());
        ee7.a((Object) sb, "append(value)");
        ih7.a(sb);
        sb.append("Last successful sync: " + str11);
        ee7.a((Object) sb, "append(value)");
        ih7.a(sb);
        sb.append("List of paired device: " + str10);
        ee7.a((Object) sb, "append(value)");
        ih7.a(sb);
        return sb;
    }
}
