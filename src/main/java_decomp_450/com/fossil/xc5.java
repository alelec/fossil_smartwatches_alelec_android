package com.fossil;

import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ActiveMinutesComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.BatteryComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.CaloriesComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ChanceOfRainComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.DateComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.HeartRateComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.NoneComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.SecondTimezoneComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.StepsComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.WeatherComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLEGoalTrackingCustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLENonCustomization;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.BuddyChallengeMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.CommuteTimeWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.DiagnosticsWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.MusicWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.NoneWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.NotificationPanelWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.StopWatchWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.TimerWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WeatherWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WellnessWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WorkoutWatchAppMapping;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Gesture;
import com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xc5 {
    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static final ComplicationAppMappingSettings a(List<DianaPresetComplicationSetting> list, Gson gson) {
        ComplicationAppMapping complicationAppMapping;
        SecondTimezoneSetting secondTimezoneSetting;
        ee7.b(list, "$this$toComplicationSetting");
        ee7.b(gson, "mGson");
        FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Convert DianaPresetSetting to complication mapping");
        ComplicationAppMapping complicationAppMapping2 = null;
        ComplicationAppMapping complicationAppMapping3 = null;
        ComplicationAppMapping complicationAppMapping4 = null;
        ComplicationAppMapping complicationAppMapping5 = null;
        for (DianaPresetComplicationSetting dianaPresetComplicationSetting : list) {
            String component1 = dianaPresetComplicationSetting.component1();
            String component2 = dianaPresetComplicationSetting.component2();
            String component4 = dianaPresetComplicationSetting.component4();
            switch (component2.hashCode()) {
                case -331239923:
                    if (component2.equals(Constants.BATTERY)) {
                        complicationAppMapping = new BatteryComplicationAppMapping();
                        break;
                    }
                    complicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case -168965370:
                    if (component2.equals("calories")) {
                        complicationAppMapping = new CaloriesComplicationAppMapping();
                        break;
                    }
                    complicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case -85386984:
                    if (component2.equals("active-minutes")) {
                        complicationAppMapping = new ActiveMinutesComplicationAppMapping();
                        break;
                    }
                    complicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case -48173007:
                    if (component2.equals("chance-of-rain")) {
                        complicationAppMapping = new ChanceOfRainComplicationAppMapping();
                        break;
                    }
                    complicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case 3076014:
                    if (component2.equals("date")) {
                        complicationAppMapping = new DateComplicationAppMapping();
                        break;
                    }
                    complicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case 96634189:
                    if (component2.equals("empty")) {
                        complicationAppMapping = new NoneComplicationAppMapping();
                        break;
                    }
                    complicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case 109761319:
                    if (component2.equals("steps")) {
                        complicationAppMapping = new StepsComplicationAppMapping();
                        break;
                    }
                    complicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case 134170930:
                    if (component2.equals("second-timezone")) {
                        if (component4 != null) {
                            secondTimezoneSetting = (SecondTimezoneSetting) gson.a(component4, SecondTimezoneSetting.class);
                            if (secondTimezoneSetting == null) {
                                secondTimezoneSetting = new SecondTimezoneSetting(null, null, 0, null, 15, null);
                            }
                        } else {
                            secondTimezoneSetting = new SecondTimezoneSetting(null, null, 0, null, 15, null);
                        }
                        secondTimezoneSetting.setTimezoneOffset(ConversionUtils.INSTANCE.getTimezoneRawOffsetById(secondTimezoneSetting.getTimeZoneId()));
                        complicationAppMapping = new SecondTimezoneComplicationAppMapping(secondTimezoneSetting.getCityCode(), secondTimezoneSetting.getTimezoneOffset());
                        break;
                    }
                    complicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case 1223440372:
                    if (component2.equals("weather")) {
                        complicationAppMapping = new WeatherComplicationAppMapping();
                        break;
                    }
                    complicationAppMapping = new NoneComplicationAppMapping();
                    break;
                case 1884273159:
                    if (component2.equals("heart-rate")) {
                        complicationAppMapping = new HeartRateComplicationAppMapping();
                        break;
                    }
                    complicationAppMapping = new NoneComplicationAppMapping();
                    break;
                default:
                    complicationAppMapping = new NoneComplicationAppMapping();
                    break;
            }
            switch (component1.hashCode()) {
                case -1383228885:
                    if (!component1.equals("bottom")) {
                        break;
                    } else {
                        complicationAppMapping5 = complicationAppMapping;
                        break;
                    }
                case 115029:
                    if (!component1.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        break;
                    } else {
                        complicationAppMapping2 = complicationAppMapping;
                        break;
                    }
                case 3317767:
                    if (!component1.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                        break;
                    } else {
                        complicationAppMapping4 = complicationAppMapping;
                        break;
                    }
                case 108511772:
                    if (!component1.equals("right")) {
                        break;
                    } else {
                        complicationAppMapping3 = complicationAppMapping;
                        break;
                    }
            }
        }
        if (complicationAppMapping2 == null) {
            complicationAppMapping2 = new NoneComplicationAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Top complication empty");
        }
        if (complicationAppMapping3 == null) {
            complicationAppMapping3 = new NoneComplicationAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Right complication empty");
        }
        if (complicationAppMapping4 == null) {
            complicationAppMapping4 = new NoneComplicationAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Left complication empty");
        }
        if (complicationAppMapping5 == null) {
            complicationAppMapping5 = new NoneComplicationAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Bottom complication empty");
        }
        return new ComplicationAppMappingSettings(complicationAppMapping2, complicationAppMapping5, complicationAppMapping4, complicationAppMapping3, System.currentTimeMillis());
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static final WatchAppMappingSettings b(List<DianaPresetWatchAppSetting> list, Gson gson) {
        WatchAppMapping watchAppMapping;
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        ee7.b(list, "$this$toWatchAppMappingSettings");
        ee7.b(gson, "mGson");
        FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Convert DianaPresetSetting to watch app mapping");
        WatchAppMapping watchAppMapping2 = null;
        WatchAppMapping watchAppMapping3 = null;
        WatchAppMapping watchAppMapping4 = null;
        for (DianaPresetWatchAppSetting dianaPresetWatchAppSetting : list) {
            String component1 = dianaPresetWatchAppSetting.component1();
            String component2 = dianaPresetWatchAppSetting.component2();
            String component4 = dianaPresetWatchAppSetting.component4();
            switch (component2.hashCode()) {
                case -829740640:
                    if (component2.equals("commute-time")) {
                        if (component4 != null) {
                            commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) gson.a(component4, CommuteTimeWatchAppSetting.class);
                            if (commuteTimeWatchAppSetting == null) {
                                commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting(null, 1, null);
                            }
                        } else {
                            commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting(null, 1, null);
                        }
                        watchAppMapping = new CommuteTimeWatchAppMapping(commuteTimeWatchAppSetting.getListAddressNameExceptOf(null));
                        break;
                    }
                    watchAppMapping = new NoneWatchAppMapping();
                    break;
                case -740386388:
                    if (component2.equals("diagnostics")) {
                        watchAppMapping = new DiagnosticsWatchAppMapping();
                        break;
                    }
                    watchAppMapping = new NoneWatchAppMapping();
                    break;
                case -420342747:
                    if (component2.equals("wellness")) {
                        watchAppMapping = new WellnessWatchAppMapping();
                        break;
                    }
                    watchAppMapping = new NoneWatchAppMapping();
                    break;
                case 96634189:
                    if (component2.equals("empty")) {
                        watchAppMapping = new NoneWatchAppMapping();
                        break;
                    }
                    watchAppMapping = new NoneWatchAppMapping();
                    break;
                case 104263205:
                    if (component2.equals(Constants.MUSIC)) {
                        watchAppMapping = new MusicWatchAppMapping();
                        break;
                    }
                    watchAppMapping = new NoneWatchAppMapping();
                    break;
                case 110364485:
                    if (component2.equals("timer")) {
                        watchAppMapping = new TimerWatchAppMapping();
                        break;
                    }
                    watchAppMapping = new NoneWatchAppMapping();
                    break;
                case 1223440372:
                    if (component2.equals("weather")) {
                        watchAppMapping = new WeatherWatchAppMapping();
                        break;
                    }
                    watchAppMapping = new NoneWatchAppMapping();
                    break;
                case 1374620322:
                    if (component2.equals("notification-panel")) {
                        watchAppMapping = new NotificationPanelWatchAppMapping();
                        break;
                    }
                    watchAppMapping = new NoneWatchAppMapping();
                    break;
                case 1525170845:
                    if (component2.equals("workout")) {
                        watchAppMapping = new WorkoutWatchAppMapping();
                        break;
                    }
                    watchAppMapping = new NoneWatchAppMapping();
                    break;
                case 1860261700:
                    if (component2.equals("stop-watch")) {
                        watchAppMapping = new StopWatchWatchAppMapping();
                        break;
                    }
                    watchAppMapping = new NoneWatchAppMapping();
                    break;
                case 1904923164:
                    if (component2.equals("buddy-challenge")) {
                        watchAppMapping = new BuddyChallengeMapping();
                        break;
                    }
                    watchAppMapping = new NoneWatchAppMapping();
                    break;
                default:
                    watchAppMapping = new NoneWatchAppMapping();
                    break;
            }
            int hashCode = component1.hashCode();
            if (hashCode != -1383228885) {
                if (hashCode != -1074341483) {
                    if (hashCode == 115029 && component1.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        watchAppMapping2 = watchAppMapping;
                    }
                } else if (component1.equals("middle")) {
                    watchAppMapping3 = watchAppMapping;
                }
            } else if (component1.equals("bottom")) {
                watchAppMapping4 = watchAppMapping;
            }
        }
        if (watchAppMapping2 == null) {
            watchAppMapping2 = new NoneWatchAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "top watch app empty");
        }
        if (watchAppMapping3 == null) {
            watchAppMapping3 = new NoneWatchAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "middle watch app empty");
        }
        if (watchAppMapping4 == null) {
            watchAppMapping4 = new NoneWatchAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "bottom watch app empty");
        }
        return new WatchAppMappingSettings(watchAppMapping2, watchAppMapping3, watchAppMapping4, System.currentTimeMillis());
    }

    @DexIgnore
    public static final ArrayList<DianaPresetWatchAppSetting> c(List<DianaPresetWatchAppSetting> list) {
        ee7.b(list, "$this$deepCopyPresetWatchAppSetting");
        ArrayList<DianaPresetWatchAppSetting> arrayList = new ArrayList<>();
        for (DianaPresetWatchAppSetting dianaPresetWatchAppSetting : list) {
            arrayList.add(new DianaPresetWatchAppSetting(dianaPresetWatchAppSetting.component1(), dianaPresetWatchAppSetting.component2(), dianaPresetWatchAppSetting.component3(), dianaPresetWatchAppSetting.component4()));
        }
        return arrayList;
    }

    @DexIgnore
    public static final boolean c(List<DianaPresetWatchAppSetting> list, Gson gson, List<DianaPresetWatchAppSetting> list2) {
        T t;
        ee7.b(list, "$this$isWatchAppSettingListEquals");
        ee7.b(gson, "gson");
        ee7.b(list2, FacebookRequestErrorClassification.KEY_OTHER);
        if (list2.size() != list.size()) {
            return false;
        }
        boolean z = true;
        for (DianaPresetWatchAppSetting dianaPresetWatchAppSetting : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (ee7.a((Object) dianaPresetWatchAppSetting.getPosition(), (Object) t.getPosition())) {
                    break;
                }
            }
            T t2 = t;
            if (t2 != null && !a(t2, gson, dianaPresetWatchAppSetting)) {
                z = false;
            }
        }
        return z;
    }

    @DexIgnore
    public static final ArrayList<DianaPresetComplicationSetting> b(List<DianaPresetComplicationSetting> list) {
        ee7.b(list, "$this$deepCopyPresetComplicationSetting");
        ArrayList<DianaPresetComplicationSetting> arrayList = new ArrayList<>();
        for (DianaPresetComplicationSetting dianaPresetComplicationSetting : list) {
            arrayList.add(new DianaPresetComplicationSetting(dianaPresetComplicationSetting.component1(), dianaPresetComplicationSetting.component2(), dianaPresetComplicationSetting.component3(), dianaPresetComplicationSetting.component4()));
        }
        return arrayList;
    }

    @DexIgnore
    public static final boolean b(List<HybridPresetAppSetting> list, Gson gson, List<HybridPresetAppSetting> list2) {
        T t;
        ee7.b(list, "$this$isHybridAppSettingListEquals");
        ee7.b(gson, "gson");
        ee7.b(list2, FacebookRequestErrorClassification.KEY_OTHER);
        if (list2.size() != list.size()) {
            return false;
        }
        boolean z = true;
        for (HybridPresetAppSetting hybridPresetAppSetting : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (ee7.a((Object) hybridPresetAppSetting.getPosition(), (Object) t.getPosition())) {
                    break;
                }
            }
            T t2 = t;
            if (t2 != null && !a(hybridPresetAppSetting, gson, t2)) {
                z = false;
            }
        }
        return z;
    }

    @DexIgnore
    public static final List<MicroAppMapping> a(HybridPreset hybridPreset, String str, DeviceRepository deviceRepository, MicroAppRepository microAppRepository) {
        ee7.b(hybridPreset, "$this$toMicroAppMappingBLE");
        ee7.b(str, "serialNumber");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(microAppRepository, "mMicroAppRepository");
        Device deviceBySerial = deviceRepository.getDeviceBySerial(str);
        if (deviceBySerial == null) {
            return new ArrayList();
        }
        if (deviceBySerial.getMajor() == 255 || deviceBySerial.getMinor() == 255) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("toMicroAppMappingBLE", "buttons " + hybridPreset.getButtons());
        long currentTimeMillis = System.currentTimeMillis();
        Iterator<HybridPresetAppSetting> it = hybridPreset.getButtons().iterator();
        while (it.hasNext()) {
            HybridPresetAppSetting next = it.next();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("toMicroAppMappingBLE", "setting for " + next + " is " + next.getSettings());
            ee7.a((Object) next, "buttonMapping");
            MicroAppVariant a = a(next, str, deviceBySerial.getMajor(), microAppRepository, next.getSettings());
            if (a == null) {
                return new ArrayList();
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("toMicroAppMappingBLE", "variant for " + next + " is " + a);
            MicroAppMapping a2 = a(next, currentTimeMillis, a, next.getSettings());
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("toMicroAppMappingBLE", "bleMicroAppMapping for " + next + " is " + a2);
            arrayList.add(a2);
        }
        return arrayList;
    }

    @DexIgnore
    public static final MicroAppVariant a(HybridPresetAppSetting hybridPresetAppSetting, String str, int i, MicroAppRepository microAppRepository, String str2) {
        String str3;
        CommuteTimeSetting commuteTimeSetting;
        ee7.b(hybridPresetAppSetting, "$this$getVariant");
        ee7.b(str, "serialNumber");
        ee7.b(microAppRepository, "mMicroAppRepository");
        MicroAppInstruction.MicroAppID microAppId = MicroAppInstruction.MicroAppID.Companion.getMicroAppId(hybridPresetAppSetting.getAppId());
        if (microAppId != MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME || str2 == null || TextUtils.isEmpty(str2) || (commuteTimeSetting = (CommuteTimeSetting) new Gson().a(str2, CommuteTimeSetting.class)) == null) {
            str3 = "";
        } else {
            str3 = commuteTimeSetting.getFormat();
            if (TextUtils.isEmpty(str3)) {
                str3 = "travel";
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("getVariant", "variantName " + str3 + " microAppId " + hybridPresetAppSetting.getAppId() + ' ' + str + " major " + i + " bleAppEnum " + microAppId + " setting " + str2);
        if (str3.length() == 0) {
            return microAppRepository.getMicroAppVariant(str, hybridPresetAppSetting.getAppId(), i);
        }
        String appId = hybridPresetAppSetting.getAppId();
        if (str3 != null) {
            String lowerCase = str3.toLowerCase();
            ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            return microAppRepository.getMicroAppVariant(str, appId, lowerCase, i);
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final MicroAppMapping a(HybridPresetAppSetting hybridPresetAppSetting, long j, MicroAppVariant microAppVariant, String str) {
        ee7.b(hybridPresetAppSetting, "$this$toBleMicroAppMapping");
        Gesture a = a(hybridPresetAppSetting);
        MicroAppInstruction.MicroAppID microAppId = MicroAppInstruction.MicroAppID.Companion.getMicroAppId(hybridPresetAppSetting.getAppId());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("bleGesture ");
        sb.append(a);
        sb.append(" bleAppEnum ");
        sb.append(microAppId);
        sb.append(' ');
        sb.append("setting ");
        sb.append(str);
        sb.append(" variant ");
        sb.append(microAppVariant);
        sb.append(" declarationsFile ");
        sb.append(microAppVariant != null ? microAppVariant.getDeclarationFileList() : null);
        local.d("toBleMicroAppMapping", sb.toString());
        if (microAppVariant == null || !(!microAppVariant.getDeclarationFileList().isEmpty())) {
            return null;
        }
        String[] strArr = new String[microAppVariant.getDeclarationFileList().size()];
        int size = microAppVariant.getDeclarationFileList().size();
        for (int i = 0; i < size; i++) {
            strArr[i] = microAppVariant.getDeclarationFileList().get(i).getContent();
        }
        BLECustomization bLENonCustomization = new BLENonCustomization();
        if (microAppId == MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID) {
            bLENonCustomization = new BLEGoalTrackingCustomization(0);
        }
        return new MicroAppMapping(a, microAppId.getValue(), strArr, bLENonCustomization, Long.valueOf(j));
    }

    @DexIgnore
    public static final Gesture a(HybridPresetAppSetting hybridPresetAppSetting) {
        ee7.b(hybridPresetAppSetting, "$this$getBleGesture");
        String position = hybridPresetAppSetting.getPosition();
        if (ee7.a((Object) position, (Object) PusherConfiguration.Pusher.TOP_PUSHER.getValue())) {
            return Gesture.SAM_BT1_SINGLE_PRESS;
        }
        if (ee7.a((Object) position, (Object) PusherConfiguration.Pusher.MID_PUSHER.getValue())) {
            return Gesture.SAM_BT2_SINGLE_PRESS;
        }
        return Gesture.SAM_BT3_SINGLE_PRESS;
    }

    @DexIgnore
    public static final ArrayList<HybridPresetAppSetting> a(List<HybridPresetAppSetting> list) {
        ee7.b(list, "$this$deepCopyAppSetting");
        ArrayList<HybridPresetAppSetting> arrayList = new ArrayList<>();
        for (T t : list) {
            arrayList.add(new HybridPresetAppSetting(t.getPosition(), t.getAppId(), t.getLocalUpdateAt(), t.getSettings()));
        }
        return arrayList;
    }

    @DexIgnore
    public static final boolean a(DianaPresetWatchAppSetting dianaPresetWatchAppSetting, Gson gson, DianaPresetWatchAppSetting dianaPresetWatchAppSetting2) {
        ee7.b(dianaPresetWatchAppSetting, "$this$isWatchAppSettingEquals");
        ee7.b(gson, "gson");
        ee7.b(dianaPresetWatchAppSetting2, FacebookRequestErrorClassification.KEY_OTHER);
        if ((!ee7.a((Object) dianaPresetWatchAppSetting.getPosition(), (Object) dianaPresetWatchAppSetting2.getPosition())) || (!ee7.a((Object) dianaPresetWatchAppSetting.getId(), (Object) dianaPresetWatchAppSetting2.getId()))) {
            return false;
        }
        if (TextUtils.isEmpty(dianaPresetWatchAppSetting.getSettings()) && TextUtils.isEmpty(dianaPresetWatchAppSetting2.getSettings())) {
            return true;
        }
        String id = dianaPresetWatchAppSetting.getId();
        int hashCode = id.hashCode();
        if (hashCode != -829740640) {
            if (hashCode == 1223440372 && id.equals("weather")) {
                return sc5.e(dianaPresetWatchAppSetting.getSettings(), gson, dianaPresetWatchAppSetting2.getSettings());
            }
            return true;
        } else if (id.equals("commute-time")) {
            return sc5.b(dianaPresetWatchAppSetting.getSettings(), gson, dianaPresetWatchAppSetting2.getSettings());
        } else {
            return true;
        }
    }

    @DexIgnore
    public static final boolean a(List<DianaPresetComplicationSetting> list, Gson gson, List<DianaPresetComplicationSetting> list2) {
        T t;
        ee7.b(list, "$this$isComplicationListSettingEquals");
        ee7.b(gson, "gson");
        ee7.b(list2, FacebookRequestErrorClassification.KEY_OTHER);
        if (list2.size() != list.size()) {
            return false;
        }
        boolean z = true;
        for (DianaPresetComplicationSetting dianaPresetComplicationSetting : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (ee7.a((Object) dianaPresetComplicationSetting.getPosition(), (Object) t.getPosition())) {
                    break;
                }
            }
            T t2 = t;
            if (t2 != null && !a(dianaPresetComplicationSetting, gson, t2)) {
                z = false;
            }
        }
        return z;
    }

    @DexIgnore
    public static final boolean a(DianaPresetComplicationSetting dianaPresetComplicationSetting, Gson gson, DianaPresetComplicationSetting dianaPresetComplicationSetting2) {
        ee7.b(dianaPresetComplicationSetting, "$this$isComplicationSettingEquals");
        ee7.b(gson, "gson");
        ee7.b(dianaPresetComplicationSetting2, FacebookRequestErrorClassification.KEY_OTHER);
        if ((!ee7.a((Object) dianaPresetComplicationSetting.getPosition(), (Object) dianaPresetComplicationSetting2.getPosition())) || (!ee7.a((Object) dianaPresetComplicationSetting.getId(), (Object) dianaPresetComplicationSetting2.getId()))) {
            return false;
        }
        if (TextUtils.isEmpty(dianaPresetComplicationSetting.getSettings()) && TextUtils.isEmpty(dianaPresetComplicationSetting2.getSettings())) {
            return true;
        }
        String id = dianaPresetComplicationSetting.getId();
        if (id.hashCode() == 134170930 && id.equals("second-timezone")) {
            return sc5.d(dianaPresetComplicationSetting.getSettings(), gson, dianaPresetComplicationSetting2.getSettings());
        }
        return true;
    }

    @DexIgnore
    public static final boolean a(HybridPresetAppSetting hybridPresetAppSetting, Gson gson, HybridPresetAppSetting hybridPresetAppSetting2) {
        ee7.b(hybridPresetAppSetting, "$this$isHybridAppSettingEquals");
        ee7.b(gson, "gson");
        ee7.b(hybridPresetAppSetting2, FacebookRequestErrorClassification.KEY_OTHER);
        if ((!ee7.a((Object) hybridPresetAppSetting.getPosition(), (Object) hybridPresetAppSetting2.getPosition())) || (!ee7.a((Object) hybridPresetAppSetting.getAppId(), (Object) hybridPresetAppSetting2.getAppId()))) {
            return false;
        }
        if (TextUtils.isEmpty(hybridPresetAppSetting.getSettings()) && TextUtils.isEmpty(hybridPresetAppSetting2.getSettings())) {
            return true;
        }
        String appId = hybridPresetAppSetting.getAppId();
        if (ee7.a((Object) appId, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
            return sc5.a(hybridPresetAppSetting.getSettings(), gson, hybridPresetAppSetting2.getSettings());
        }
        if (ee7.a((Object) appId, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
            return sc5.d(hybridPresetAppSetting.getSettings(), gson, hybridPresetAppSetting2.getSettings());
        }
        if (ee7.a((Object) appId, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
            return sc5.c(hybridPresetAppSetting.getSettings(), gson, hybridPresetAppSetting2.getSettings());
        }
        return true;
    }
}
