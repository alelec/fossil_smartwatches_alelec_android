package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r03 implements o03 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a;
    @DexIgnore
    public static /* final */ tq2<Boolean> b;
    @DexIgnore
    public static /* final */ tq2<Boolean> c;
    @DexIgnore
    public static /* final */ tq2<Boolean> d;
    @DexIgnore
    public static /* final */ tq2<Boolean> e;
    @DexIgnore
    public static /* final */ tq2<Boolean> f;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        a = dr2.a("measurement.gold.enhanced_ecommerce.format_logs", true);
        b = dr2.a("measurement.gold.enhanced_ecommerce.log_nested_complex_events", true);
        c = dr2.a("measurement.gold.enhanced_ecommerce.nested_param_daily_event_count", true);
        d = dr2.a("measurement.gold.enhanced_ecommerce.updated_schema.client", true);
        e = dr2.a("measurement.gold.enhanced_ecommerce.updated_schema.service", true);
        f = dr2.a("measurement.gold.enhanced_ecommerce.upload_nested_complex_events", true);
    }
    */

    @DexIgnore
    @Override // com.fossil.o03
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.o03
    public final boolean zzb() {
        return a.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.o03
    public final boolean zzc() {
        return b.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.o03
    public final boolean zzd() {
        return c.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.o03
    public final boolean zze() {
        return d.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.o03
    public final boolean zzf() {
        return e.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.o03
    public final boolean zzg() {
        return f.b().booleanValue();
    }
}
