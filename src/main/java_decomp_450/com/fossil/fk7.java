package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fk7 extends yh7 {
    @DexIgnore
    public /* final */ gd7<Throwable, i97> a;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.gd7<? super java.lang.Throwable, com.fossil.i97> */
    /* JADX WARN: Multi-variable type inference failed */
    public fk7(gd7<? super Throwable, i97> gd7) {
        this.a = gd7;
    }

    @DexIgnore
    @Override // com.fossil.zh7
    public void a(Throwable th) {
        this.a.invoke(th);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(Throwable th) {
        a(th);
        return i97.a;
    }

    @DexIgnore
    public String toString() {
        return "InvokeOnCancel[" + ej7.a(this.a) + '@' + ej7.b(this) + ']';
    }
}
