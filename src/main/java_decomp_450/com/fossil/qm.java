package com.fossil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qm {
    @DexIgnore
    public UUID a;
    @DexIgnore
    public a b;
    @DexIgnore
    public cm c;
    @DexIgnore
    public Set<String> d;
    @DexIgnore
    public cm e;
    @DexIgnore
    public int f;

    @DexIgnore
    public enum a {
        ENQUEUED,
        RUNNING,
        SUCCEEDED,
        FAILED,
        BLOCKED,
        CANCELLED;

        @DexIgnore
        public boolean isFinished() {
            return this == SUCCEEDED || this == FAILED || this == CANCELLED;
        }
    }

    @DexIgnore
    public qm(UUID uuid, a aVar, cm cmVar, List<String> list, cm cmVar2, int i) {
        this.a = uuid;
        this.b = aVar;
        this.c = cmVar;
        this.d = new HashSet(list);
        this.e = cmVar2;
        this.f = i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || qm.class != obj.getClass()) {
            return false;
        }
        qm qmVar = (qm) obj;
        if (this.f == qmVar.f && this.a.equals(qmVar.a) && this.b == qmVar.b && this.c.equals(qmVar.c) && this.d.equals(qmVar.d)) {
            return this.e.equals(qmVar.e);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode()) * 31) + this.f;
    }

    @DexIgnore
    public String toString() {
        return "WorkInfo{mId='" + this.a + '\'' + ", mState=" + this.b + ", mOutputData=" + this.c + ", mTags=" + this.d + ", mProgress=" + this.e + '}';
    }
}
