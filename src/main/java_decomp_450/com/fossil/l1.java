package com.fossil;

import android.content.Context;
import android.view.MenuItem;
import android.view.SubMenu;
import androidx.collection.SimpleArrayMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class l1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public SimpleArrayMap<w7, MenuItem> b;
    @DexIgnore
    public SimpleArrayMap<x7, SubMenu> c;

    @DexIgnore
    public l1(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final MenuItem a(MenuItem menuItem) {
        if (!(menuItem instanceof w7)) {
            return menuItem;
        }
        w7 w7Var = (w7) menuItem;
        if (this.b == null) {
            this.b = new SimpleArrayMap<>();
        }
        MenuItem menuItem2 = this.b.get(menuItem);
        if (menuItem2 != null) {
            return menuItem2;
        }
        s1 s1Var = new s1(this.a, w7Var);
        this.b.put(w7Var, s1Var);
        return s1Var;
    }

    @DexIgnore
    public final void b() {
        SimpleArrayMap<w7, MenuItem> simpleArrayMap = this.b;
        if (simpleArrayMap != null) {
            simpleArrayMap.clear();
        }
        SimpleArrayMap<x7, SubMenu> simpleArrayMap2 = this.c;
        if (simpleArrayMap2 != null) {
            simpleArrayMap2.clear();
        }
    }

    @DexIgnore
    public final void b(int i) {
        if (this.b != null) {
            for (int i2 = 0; i2 < this.b.size(); i2++) {
                if (this.b.c(i2).getItemId() == i) {
                    this.b.d(i2);
                    return;
                }
            }
        }
    }

    @DexIgnore
    public final SubMenu a(SubMenu subMenu) {
        if (!(subMenu instanceof x7)) {
            return subMenu;
        }
        x7 x7Var = (x7) subMenu;
        if (this.c == null) {
            this.c = new SimpleArrayMap<>();
        }
        SubMenu subMenu2 = this.c.get(x7Var);
        if (subMenu2 != null) {
            return subMenu2;
        }
        b2 b2Var = new b2(this.a, x7Var);
        this.c.put(x7Var, b2Var);
        return b2Var;
    }

    @DexIgnore
    public final void a(int i) {
        if (this.b != null) {
            int i2 = 0;
            while (i2 < this.b.size()) {
                if (this.b.c(i2).getGroupId() == i) {
                    this.b.d(i2);
                    i2--;
                }
                i2++;
            }
        }
    }
}
