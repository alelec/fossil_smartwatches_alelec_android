package com.fossil;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ep7 {
    @DexIgnore
    public /* final */ nn7 a;
    @DexIgnore
    public /* final */ cp7 b;
    @DexIgnore
    public /* final */ qn7 c;
    @DexIgnore
    public /* final */ co7 d;
    @DexIgnore
    public List<Proxy> e; // = Collections.emptyList();
    @DexIgnore
    public int f;
    @DexIgnore
    public List<InetSocketAddress> g; // = Collections.emptyList();
    @DexIgnore
    public /* final */ List<no7> h; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ List<no7> a;
        @DexIgnore
        public int b; // = 0;

        @DexIgnore
        public a(List<no7> list) {
            this.a = list;
        }

        @DexIgnore
        public List<no7> a() {
            return new ArrayList(this.a);
        }

        @DexIgnore
        public boolean b() {
            return this.b < this.a.size();
        }

        @DexIgnore
        public no7 c() {
            if (b()) {
                List<no7> list = this.a;
                int i = this.b;
                this.b = i + 1;
                return list.get(i);
            }
            throw new NoSuchElementException();
        }
    }

    @DexIgnore
    public ep7(nn7 nn7, cp7 cp7, qn7 qn7, co7 co7) {
        this.a = nn7;
        this.b = cp7;
        this.c = qn7;
        this.d = co7;
        a(nn7.k(), nn7.f());
    }

    @DexIgnore
    public boolean a() {
        return b() || !this.h.isEmpty();
    }

    @DexIgnore
    public final boolean b() {
        return this.f < this.e.size();
    }

    @DexIgnore
    public a c() throws IOException {
        if (a()) {
            ArrayList arrayList = new ArrayList();
            while (b()) {
                Proxy d2 = d();
                int size = this.g.size();
                for (int i = 0; i < size; i++) {
                    no7 no7 = new no7(this.a, d2, this.g.get(i));
                    if (this.b.c(no7)) {
                        this.h.add(no7);
                    } else {
                        arrayList.add(no7);
                    }
                }
                if (!arrayList.isEmpty()) {
                    break;
                }
            }
            if (arrayList.isEmpty()) {
                arrayList.addAll(this.h);
                this.h.clear();
            }
            return new a(arrayList);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public final Proxy d() throws IOException {
        if (b()) {
            List<Proxy> list = this.e;
            int i = this.f;
            this.f = i + 1;
            Proxy proxy = list.get(i);
            a(proxy);
            return proxy;
        }
        throw new SocketException("No route to " + this.a.k().g() + "; exhausted proxy configurations: " + this.e);
    }

    @DexIgnore
    public void a(no7 no7, IOException iOException) {
        if (!(no7.b().type() == Proxy.Type.DIRECT || this.a.h() == null)) {
            this.a.h().connectFailed(this.a.k().o(), no7.b().address(), iOException);
        }
        this.b.b(no7);
    }

    @DexIgnore
    public final void a(go7 go7, Proxy proxy) {
        List<Proxy> list;
        if (proxy != null) {
            this.e = Collections.singletonList(proxy);
        } else {
            List<Proxy> select = this.a.h().select(go7.o());
            if (select == null || select.isEmpty()) {
                list = ro7.a(Proxy.NO_PROXY);
            } else {
                list = ro7.a(select);
            }
            this.e = list;
        }
        this.f = 0;
    }

    @DexIgnore
    public final void a(Proxy proxy) throws IOException {
        String str;
        int i;
        this.g = new ArrayList();
        if (proxy.type() == Proxy.Type.DIRECT || proxy.type() == Proxy.Type.SOCKS) {
            str = this.a.k().g();
            i = this.a.k().k();
        } else {
            SocketAddress address = proxy.address();
            if (address instanceof InetSocketAddress) {
                InetSocketAddress inetSocketAddress = (InetSocketAddress) address;
                str = a(inetSocketAddress);
                i = inetSocketAddress.getPort();
            } else {
                throw new IllegalArgumentException("Proxy.address() is not an InetSocketAddress: " + address.getClass());
            }
        }
        if (i < 1 || i > 65535) {
            throw new SocketException("No route to " + str + ":" + i + "; port is out of range");
        } else if (proxy.type() == Proxy.Type.SOCKS) {
            this.g.add(InetSocketAddress.createUnresolved(str, i));
        } else {
            this.d.a(this.c, str);
            List<InetAddress> a2 = this.a.c().a(str);
            if (!a2.isEmpty()) {
                this.d.a(this.c, str, a2);
                int size = a2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    this.g.add(new InetSocketAddress(a2.get(i2), i));
                }
                return;
            }
            throw new UnknownHostException(this.a.c() + " returned no addresses for " + str);
        }
    }

    @DexIgnore
    public static String a(InetSocketAddress inetSocketAddress) {
        InetAddress address = inetSocketAddress.getAddress();
        if (address == null) {
            return inetSocketAddress.getHostName();
        }
        return address.getHostAddress();
    }
}
