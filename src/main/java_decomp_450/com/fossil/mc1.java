package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class mc1 extends Enum<mc1> implements mw0 {
    @DexIgnore
    public static /* final */ mc1 c;
    @DexIgnore
    public static /* final */ mc1 d;
    @DexIgnore
    public static /* final */ /* synthetic */ mc1[] e;
    @DexIgnore
    public static /* final */ sa1 f; // = new sa1(null);
    @DexIgnore
    public /* final */ String a; // = yz0.a(this);
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        mc1 mc1 = new mc1("SUCCESS", 0, (byte) 0);
        c = mc1;
        mc1 mc12 = new mc1("UNKNOWN", 17, (byte) 255);
        d = mc12;
        e = new mc1[]{mc1, new mc1("WRONG_LENGTH", 1, (byte) 1), new mc1("FAIL_TO_GENERATE_RANDOM_NUMBER", 2, (byte) 2), new mc1("FAIL_TO_GET_KEY", 3, (byte) 3), new mc1("FAIL_TO_ENCRYPT_FRAME", 4, (byte) 4), new mc1("FAIL_TO_DECRYPT_FRAME", 5, (byte) 5), new mc1("INVALID_KEY_TYPE", 6, (byte) 6), new mc1("WRONG_CONFIRM_RANDOM_NUMBER", 7, (byte) 7), new mc1("WRONG_STATE", 8, (byte) 8), new mc1("FAIL_TO_STORE_KEY", 9, (byte) 9), new mc1("WRONG_KEY_OPERATION", 10, (byte) 10), new mc1("NOT_SUPPORTED", 11, (byte) 11), new mc1("FAIL_TO_GENERATE_PUBLIC_KEY", 12, (byte) 12), new mc1("FAIL_TO_GENERATE_SECRET_KEY", 13, (byte) 13), new mc1("FAIL_BECAUSE_OF_INVALID_INPUT", 14, DateTimeFieldType.HOUR_OF_HALFDAY), new mc1("FAIL_BECAUSE_OF_NOT_SET_KEY", 15, DateTimeFieldType.CLOCKHOUR_OF_HALFDAY), new mc1("FAIL_TO_SEND_REQUEST_AUTHENTICATE_THROUGH_ASYNC", 16, DateTimeFieldType.CLOCKHOUR_OF_DAY), mc12};
    }
    */

    @DexIgnore
    public mc1(String str, int i, byte b2) {
        this.b = b2;
    }

    @DexIgnore
    public static mc1 valueOf(String str) {
        return (mc1) Enum.valueOf(mc1.class, str);
    }

    @DexIgnore
    public static mc1[] values() {
        return (mc1[]) e.clone();
    }

    @DexIgnore
    @Override // com.fossil.mw0
    public boolean a() {
        return this == c;
    }

    @DexIgnore
    @Override // com.fossil.mw0
    public String getLogName() {
        return this.a;
    }
}
