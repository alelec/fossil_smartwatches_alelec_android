package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fp4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ dp4 b;
    @DexIgnore
    public /* final */ ep4 c;
    @DexIgnore
    public /* final */ ch5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRepository", f = "ProfileRepository.kt", l = {49}, m = "changeSocialId")
    public static final class a extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ fp4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(fp4 fp4, fb7 fb7) {
            super(fb7);
            this.this$0 = fp4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRepository", f = "ProfileRepository.kt", l = {22}, m = "fetchSocialProfile")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ fp4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(fp4 fp4, fb7 fb7) {
            super(fb7);
            this.this$0 = fp4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore
    public fp4(dp4 dp4, ep4 ep4, ch5 ch5) {
        ee7.b(dp4, "local");
        ee7.b(ep4, "remote");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.b = dp4;
        this.c = ep4;
        this.d = ch5;
        String name = fp4.class.getName();
        ee7.a((Object) name, "ProfileRepository::class.java.name");
        this.a = name;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.fossil.ko4<com.fossil.fo4>> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof com.fossil.fp4.b
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.fossil.fp4$b r0 = (com.fossil.fp4.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.fp4$b r0 = new com.fossil.fp4$b
            r0.<init>(r5, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.fossil.fp4 r0 = (com.fossil.fp4) r0
            com.fossil.t87.a(r6)
            goto L_0x0046
        L_0x002d:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L_0x0035:
            com.fossil.t87.a(r6)
            com.fossil.ep4 r6 = r5.c
            r0.L$0 = r5
            r0.label = r3
            java.lang.Object r6 = r6.a(r0)
            if (r6 != r1) goto L_0x0045
            return r1
        L_0x0045:
            r0 = r5
        L_0x0046:
            com.fossil.zi5 r6 = (com.fossil.zi5) r6
            boolean r1 = r6 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x0079
            com.fossil.bj5 r6 = (com.fossil.bj5) r6
            java.lang.Object r6 = r6.a()
            com.fossil.fo4 r6 = (com.fossil.fo4) r6
            if (r6 != 0) goto L_0x0064
            com.fossil.ko4 r6 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            r1 = 600(0x258, float:8.41E-43)
            r0.<init>(r1, r2)
            r6.<init>(r0)
            goto L_0x00ae
        L_0x0064:
            com.fossil.ch5 r1 = r0.d
            java.lang.String r3 = r6.c()
            r1.q(r3)
            com.fossil.dp4 r0 = r0.b
            r0.a(r6)
            com.fossil.ko4 r0 = new com.fossil.ko4
            r1 = 2
            r0.<init>(r6, r2, r1, r2)
            goto L_0x00ad
        L_0x0079:
            boolean r1 = r6 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x00af
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r0 = r0.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "downloadProfile - Failure - "
            r3.append(r4)
            com.fossil.yi5 r6 = (com.fossil.yi5) r6
            int r4 = r6.a()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.e(r0, r3)
            com.fossil.ko4 r0 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            int r6 = r6.a()
            r1.<init>(r6, r2)
            r0.<init>(r1)
        L_0x00ad:
            r6 = r0
        L_0x00ae:
            return r6
        L_0x00af:
            com.fossil.p87 r6 = new com.fossil.p87
            r6.<init>()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fp4.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final fo4 b() {
        return this.b.b();
    }

    @DexIgnore
    public final LiveData<fo4> c() {
        return this.b.c();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r5, com.fossil.fb7<? super com.fossil.ko4<com.fossil.fo4>> r6) {
        /*
            r4 = this;
            boolean r0 = r6 instanceof com.fossil.fp4.a
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.fossil.fp4$a r0 = (com.fossil.fp4.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.fp4$a r0 = new com.fossil.fp4$a
            r0.<init>(r4, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r5 = r0.L$1
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r5 = r0.L$0
            com.fossil.fp4 r5 = (com.fossil.fp4) r5
            com.fossil.t87.a(r6)
            goto L_0x004c
        L_0x0031:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L_0x0039:
            com.fossil.t87.a(r6)
            com.fossil.ep4 r6 = r4.c
            r0.L$0 = r4
            r0.L$1 = r5
            r0.label = r3
            java.lang.Object r6 = r6.a(r5, r0)
            if (r6 != r1) goto L_0x004b
            return r1
        L_0x004b:
            r5 = r4
        L_0x004c:
            com.fossil.zi5 r6 = (com.fossil.zi5) r6
            boolean r0 = r6 instanceof com.fossil.bj5
            r1 = 0
            if (r0 == 0) goto L_0x009b
            com.fossil.bj5 r6 = (com.fossil.bj5) r6
            java.lang.Object r6 = r6.a()
            com.fossil.fo4 r6 = (com.fossil.fo4) r6
            if (r6 != 0) goto L_0x0086
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r5 = r5.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "generateSocialProfile - Success - "
            r2.append(r3)
            r2.append(r6)
            java.lang.String r6 = r2.toString()
            r0.e(r5, r6)
            com.fossil.ko4 r5 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r6 = new com.portfolio.platform.data.model.ServerError
            r0 = 600(0x258, float:8.41E-43)
            r6.<init>(r0, r1)
            r5.<init>(r6)
            goto L_0x00cf
        L_0x0086:
            com.fossil.ch5 r0 = r5.d
            java.lang.String r2 = r6.c()
            r0.q(r2)
            com.fossil.dp4 r5 = r5.b
            r5.a(r6)
            com.fossil.ko4 r5 = new com.fossil.ko4
            r0 = 2
            r5.<init>(r6, r1, r0, r1)
            goto L_0x00cf
        L_0x009b:
            boolean r0 = r6 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x00d0
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r5 = r5.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "changeSocialId - Failure - "
            r2.append(r3)
            com.fossil.yi5 r6 = (com.fossil.yi5) r6
            int r3 = r6.a()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.e(r5, r2)
            com.fossil.ko4 r5 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            int r6 = r6.a()
            r0.<init>(r6, r1)
            r5.<init>(r0)
        L_0x00cf:
            return r5
        L_0x00d0:
            com.fossil.p87 r5 = new com.fossil.p87
            r5.<init>()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fp4.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a() {
        this.b.a();
    }
}
