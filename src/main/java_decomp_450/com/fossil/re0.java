package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class re0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ kr0 a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<re0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public re0 createFromParcel(Parcel parcel) {
            kr0 kr0 = kr0.values()[parcel.readInt()];
            parcel.setDataPosition(0);
            int i = op0.a[kr0.ordinal()];
            if (i == 1) {
                return av0.CREATOR.createFromParcel(parcel);
            }
            if (i == 2) {
                return te0.CREATOR.createFromParcel(parcel);
            }
            if (i == 3) {
                return se0.CREATOR.createFromParcel(parcel);
            }
            throw new p87();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public re0[] newArray(int i) {
            return new re0[i];
        }
    }

    @DexIgnore
    public re0(kr0 kr0, boolean z) {
        this.a = kr0;
        this.b = z;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject put = new JSONObject().put("enable_goal_ring", this.b);
        ee7.a((Object) put, "JSONObject()\n           \u2026 mEnablePercentageCircle)");
        return put;
    }

    @DexIgnore
    public boolean b() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            re0 re0 = (re0) obj;
            return this.a == re0.a && this.b == re0.b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.ComplicationDataConfig");
    }

    @DexIgnore
    public int hashCode() {
        return Boolean.valueOf(this.b).hashCode() + this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a.ordinal());
        }
        if (parcel != null) {
            parcel.writeInt(this.b ? 1 : 0);
        }
    }

    @DexIgnore
    public /* synthetic */ re0(kr0 kr0, boolean z, int i) {
        z = (i & 2) != 0 ? false : z;
        this.a = kr0;
        this.b = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public re0(Parcel parcel) {
        this(kr0.values()[parcel.readInt()], parcel.readInt() != 1 ? false : true);
    }
}
