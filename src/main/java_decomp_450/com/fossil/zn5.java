package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zn5 extends fl4<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ AuthApiGuestService d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            ee7.b(str, Constants.EMAIL);
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase", f = "ResetPasswordUseCase.kt", l = {27}, m = "run")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(zn5 zn5, fb7 fb7) {
            super(fb7);
            this.this$0 = zn5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<Object>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase$run$response$1", f = "ResetPasswordUseCase.kt", l = {27}, m = "invokeSuspend")
    public static final class f extends zb7 implements gd7<fb7<? super fv7<Void>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ zn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(zn5 zn5, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = zn5;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new f(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<Void>> fb7) {
            return ((f) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                AuthApiGuestService a2 = this.this$0.d;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.passwordRequestReset(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = zn5.class.getSimpleName();
        ee7.a((Object) simpleName, "ResetPasswordUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public zn5(AuthApiGuestService authApiGuestService) {
        ee7.b(authApiGuestService, "mApiGuestService");
        this.d = authApiGuestService;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return e;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.zn5.b r9, com.fossil.fb7<java.lang.Object> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.zn5.e
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.zn5$e r0 = (com.fossil.zn5.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.zn5$e r0 = new com.fossil.zn5$e
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = ""
            r4 = 1
            r5 = 0
            if (r2 == 0) goto L_0x0040
            if (r2 != r4) goto L_0x0038
            java.lang.Object r9 = r0.L$2
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$1
            com.fossil.zn5$b r9 = (com.fossil.zn5.b) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.zn5 r9 = (com.fossil.zn5) r9
            com.fossil.t87.a(r10)
            goto L_0x0093
        L_0x0038:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0040:
            com.fossil.t87.a(r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r2 = com.fossil.zn5.e
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "running UseCase with email="
            r6.append(r7)
            if (r9 == 0) goto L_0x005c
            java.lang.String r7 = r9.a()
            goto L_0x005d
        L_0x005c:
            r7 = r5
        L_0x005d:
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r10.d(r2, r6)
            if (r9 != 0) goto L_0x0071
            com.fossil.zn5$c r9 = new com.fossil.zn5$c
            r10 = 600(0x258, float:8.41E-43)
            r9.<init>(r10, r3)
            return r9
        L_0x0071:
            com.fossil.ie4 r10 = new com.fossil.ie4
            r10.<init>()
            java.lang.String r2 = r9.a()
            java.lang.String r6 = "email"
            r10.a(r6, r2)
            com.fossil.zn5$f r2 = new com.fossil.zn5$f
            r2.<init>(r8, r10, r5)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.label = r4
            java.lang.Object r10 = com.fossil.aj5.a(r2, r0)
            if (r10 != r1) goto L_0x0093
            return r1
        L_0x0093:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x009f
            com.fossil.zn5$d r9 = new com.fossil.zn5$d
            r9.<init>()
            goto L_0x00f2
        L_0x009f:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00f3
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r0 = com.fossil.zn5.e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Inside .run failed with error="
            r1.append(r2)
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            if (r2 == 0) goto L_0x00c1
            java.lang.Integer r5 = r2.getCode()
        L_0x00c1:
            r1.append(r5)
            java.lang.String r1 = r1.toString()
            r9.d(r0, r1)
            com.fossil.zn5$c r9 = new com.fossil.zn5$c
            com.portfolio.platform.data.model.ServerError r0 = r10.c()
            if (r0 == 0) goto L_0x00de
            java.lang.Integer r0 = r0.getCode()
            if (r0 == 0) goto L_0x00de
            int r0 = r0.intValue()
            goto L_0x00e2
        L_0x00de:
            int r0 = r10.a()
        L_0x00e2:
            com.portfolio.platform.data.model.ServerError r10 = r10.c()
            if (r10 == 0) goto L_0x00ef
            java.lang.String r10 = r10.getMessage()
            if (r10 == 0) goto L_0x00ef
            r3 = r10
        L_0x00ef:
            r9.<init>(r0, r3)
        L_0x00f2:
            return r9
        L_0x00f3:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zn5.a(com.fossil.zn5$b, com.fossil.fb7):java.lang.Object");
    }
}
