package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class er {
    /*
    static {
        new er();
        br7.Companion.c("GIF");
        br7.Companion.c("RIFF");
        br7.Companion.c("WEBP");
        br7.Companion.c("VP8X");
    }
    */

    @DexIgnore
    public static final int a(int i, int i2, int i3, int i4, qt qtVar) {
        ee7.b(qtVar, "scale");
        int a = qf7.a(Integer.highestOneBit(i / i3), 1);
        int a2 = qf7.a(Integer.highestOneBit(i2 / i4), 1);
        int i5 = dr.a[qtVar.ordinal()];
        if (i5 == 1) {
            return Math.min(a, a2);
        }
        if (i5 == 2) {
            return Math.max(a, a2);
        }
        throw new p87();
    }

    @DexIgnore
    public static final double b(int i, int i2, int i3, int i4, qt qtVar) {
        ee7.b(qtVar, "scale");
        double d = ((double) i3) / ((double) i);
        double d2 = ((double) i4) / ((double) i2);
        int i5 = dr.b[qtVar.ordinal()];
        if (i5 == 1) {
            return Math.max(d, d2);
        }
        if (i5 == 2) {
            return Math.min(d, d2);
        }
        throw new p87();
    }

    @DexIgnore
    public static final double a(double d, double d2, double d3, double d4, qt qtVar) {
        ee7.b(qtVar, "scale");
        double d5 = d3 / d;
        double d6 = d4 / d2;
        int i = dr.d[qtVar.ordinal()];
        if (i == 1) {
            return Math.max(d5, d6);
        }
        if (i == 2) {
            return Math.min(d5, d6);
        }
        throw new p87();
    }
}
