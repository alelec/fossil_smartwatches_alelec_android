package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class oi5 extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String e; // = oi5.class.getSimpleName();
    @DexIgnore
    public int a; // = 0;
    @DexIgnore
    public Date b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public String d;

    @DexIgnore
    public final void a(Context context, int i, String str) {
        int i2 = this.a;
        if (i2 != i) {
            if (i != 0) {
                if (i == 1) {
                    this.c = true;
                    Date date = new Date();
                    this.b = date;
                    this.d = str;
                    b(context, str, date);
                } else if (i == 2) {
                    if (i2 != 1) {
                        this.c = false;
                        Date date2 = new Date();
                        this.b = date2;
                        d(context, this.d, date2);
                    } else {
                        a(context, this.d, this.b);
                    }
                }
            } else if (i2 == 1) {
                c(context, this.d, this.b);
            } else if (this.c) {
                a(context, this.d, this.b, new Date());
            } else {
                b(context, this.d, this.b, new Date());
            }
            this.a = i;
        }
    }

    @DexIgnore
    public abstract void a(Context context, String str, Date date);

    @DexIgnore
    public abstract void a(Context context, String str, Date date, Date date2);

    @DexIgnore
    public abstract void b(Context context, String str, Date date);

    @DexIgnore
    public abstract void b(Context context, String str, Date date, Date date2);

    @DexIgnore
    public abstract void c(Context context, String str, Date date);

    @DexIgnore
    public abstract void d(Context context, String str, Date date);

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.NEW_OUTGOING_CALL".equalsIgnoreCase(intent.getAction())) {
            this.d = intent.getStringExtra("android.intent.extra.PHONE_NUMBER");
            return;
        }
        String stringExtra = intent.getStringExtra("state");
        String stringExtra2 = intent.getStringExtra("incoming_number");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, "onReceive() - number = " + stringExtra2 + " stateStr = " + stringExtra);
        if (stringExtra != null && !TextUtils.isEmpty(stringExtra2)) {
            int i = 0;
            if (!stringExtra.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                if (stringExtra.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    i = 2;
                } else if (stringExtra.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    i = 1;
                }
            }
            a(context, i, stringExtra2);
        }
    }
}
