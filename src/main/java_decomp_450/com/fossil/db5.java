package com.fossil;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum db5 {
    Device("Device"),
    User("User"),
    Mock("Mock");
    
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    public db5(String str) {
        this.value = str;
    }

    @DexIgnore
    public static db5 fromString(String str) {
        if (!TextUtils.isEmpty(str)) {
            db5[] values = values();
            for (db5 db5 : values) {
                if (str.equalsIgnoreCase(db5.value)) {
                    return db5;
                }
            }
        }
        return Device;
    }

    @DexIgnore
    public String getName() {
        return this.value;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }
}
