package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xx5 {
    @DexIgnore
    public /* final */ vx5 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<String> c;

    @DexIgnore
    public xx5(vx5 vx5, int i, ArrayList<String> arrayList) {
        ee7.b(vx5, "mView");
        this.a = vx5;
        this.b = i;
        this.c = arrayList;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final ArrayList<String> b() {
        ArrayList<String> arrayList = this.c;
        return arrayList == null ? new ArrayList<>() : arrayList;
    }

    @DexIgnore
    public final vx5 c() {
        return this.a;
    }
}
