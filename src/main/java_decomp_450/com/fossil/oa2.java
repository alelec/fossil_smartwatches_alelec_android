package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class oa2 implements Callable {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ na2 c;

    @DexIgnore
    public oa2(boolean z, String str, na2 na2) {
        this.a = z;
        this.b = str;
        this.c = na2;
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return ma2.a(this.a, this.b, this.c);
    }
}
