package com.fossil;

import android.os.Handler;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wt6 extends st6 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public /* final */ Handler e; // = new Handler();
    @DexIgnore
    public /* final */ Runnable f; // = new c(this);
    @DexIgnore
    public /* final */ tt6 g;
    @DexIgnore
    public /* final */ UserRepository h;
    @DexIgnore
    public /* final */ ph5 i;
    @DexIgnore
    public /* final */ ThemeRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return wt6.k;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1", f = "SplashPresenter.kt", l = {52, 58, 63}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wt6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1$isAA$1", f = "SplashPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return pb7.a(ng5.a().a(PortfolioApp.g0.c()) != null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.wt6$b$b")
        @tb7(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1$mCurrentUser$1", f = "SplashPresenter.kt", l = {58}, m = "invokeSuspend")
        /* renamed from: com.fossil.wt6$b$b  reason: collision with other inner class name */
        public static final class C0235b extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0235b(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0235b bVar = new C0235b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((C0235b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository f = this.this$0.this$0.h;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = f.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(wt6 wt6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = wt6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00d1  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00db  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00fd  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0107  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r12.label
                r2 = 0
                r3 = 3
                r4 = 2
                r5 = 1
                if (r1 == 0) goto L_0x0049
                if (r1 == r5) goto L_0x003a
                if (r1 == r4) goto L_0x002b
                if (r1 != r3) goto L_0x0023
                java.lang.Object r0 = r12.L$2
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                java.lang.Object r0 = r12.L$1
                java.lang.String r0 = (java.lang.String) r0
                java.lang.Object r0 = r12.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r13)
                goto L_0x00f5
            L_0x0023:
                java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r13.<init>(r0)
                throw r13
            L_0x002b:
                boolean r1 = r12.Z$0
                java.lang.Object r4 = r12.L$1
                java.lang.String r4 = (java.lang.String) r4
                java.lang.Object r5 = r12.L$0
                com.fossil.yi7 r5 = (com.fossil.yi7) r5
                com.fossil.t87.a(r13)
                goto L_0x00cd
            L_0x003a:
                boolean r1 = r12.Z$0
                java.lang.Object r5 = r12.L$1
                java.lang.String r5 = (java.lang.String) r5
                java.lang.Object r6 = r12.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
                com.fossil.t87.a(r13)
                r13 = r6
                goto L_0x0099
            L_0x0049:
                com.fossil.t87.a(r13)
                com.fossil.yi7 r13 = r12.p$
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                java.lang.String r1 = r1.g()
                com.fossil.wt6 r6 = r12.this$0
                com.fossil.ph5 r6 = r6.i
                boolean r6 = r6.a(r1)
                com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                com.fossil.wt6$a r8 = com.fossil.wt6.l
                java.lang.String r8 = r8.a()
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                java.lang.String r10 = "checkToGoToNextStep isMigrationComplete "
                r9.append(r10)
                r9.append(r6)
                java.lang.String r9 = r9.toString()
                r7.d(r8, r9)
                com.fossil.wt6 r7 = r12.this$0
                com.portfolio.platform.data.source.ThemeRepository r7 = r7.j
                r12.L$0 = r13
                r12.L$1 = r1
                r12.Z$0 = r6
                r12.label = r5
                java.lang.Object r5 = r7.initializeLocalTheme(r12)
                if (r5 != r0) goto L_0x0097
                return r0
            L_0x0097:
                r5 = r1
                r1 = r6
            L_0x0099:
                if (r1 != 0) goto L_0x00af
                com.fossil.wt6 r13 = r12.this$0
                android.os.Handler r13 = r13.e
                com.fossil.wt6 r0 = r12.this$0
                java.lang.Runnable r0 = r0.f
                r1 = 500(0x1f4, double:2.47E-321)
                r13.postDelayed(r0, r1)
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            L_0x00af:
                com.fossil.wt6 r6 = r12.this$0
                com.fossil.ti7 r6 = r6.b()
                com.fossil.wt6$b$b r7 = new com.fossil.wt6$b$b
                r7.<init>(r12, r2)
                r12.L$0 = r13
                r12.L$1 = r5
                r12.Z$0 = r1
                r12.label = r4
                java.lang.Object r4 = com.fossil.vh7.a(r6, r7, r12)
                if (r4 != r0) goto L_0x00c9
                return r0
            L_0x00c9:
                r11 = r5
                r5 = r13
                r13 = r4
                r4 = r11
            L_0x00cd:
                com.portfolio.platform.data.model.MFUser r13 = (com.portfolio.platform.data.model.MFUser) r13
                if (r13 != 0) goto L_0x00db
                com.fossil.wt6 r13 = r12.this$0
                com.fossil.tt6 r13 = r13.g
                r13.n0()
                goto L_0x0134
            L_0x00db:
                com.fossil.ti7 r6 = com.fossil.qj7.b()
                com.fossil.wt6$b$a r7 = new com.fossil.wt6$b$a
                r7.<init>(r2)
                r12.L$0 = r5
                r12.L$1 = r4
                r12.Z$0 = r1
                r12.L$2 = r13
                r12.label = r3
                java.lang.Object r13 = com.fossil.vh7.a(r6, r7, r12)
                if (r13 != r0) goto L_0x00f5
                return r0
            L_0x00f5:
                java.lang.Boolean r13 = (java.lang.Boolean) r13
                boolean r13 = r13.booleanValue()
                if (r13 == 0) goto L_0x0107
                com.fossil.wt6 r13 = r12.this$0
                com.fossil.tt6 r13 = r13.g
                r13.i()
                goto L_0x0134
            L_0x0107:
                com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.IRemoteFLogger r0 = r13.getRemote()
                com.misfit.frameworks.buttonservice.log.FLogger$Component r1 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
                com.misfit.frameworks.buttonservice.log.FLogger$Session r2 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER
                com.portfolio.platform.PortfolioApp$a r13 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r13 = r13.c()
                java.lang.String r3 = r13.c()
                com.portfolio.platform.ApplicationEventListener$a r13 = com.portfolio.platform.ApplicationEventListener.F
                java.lang.String r4 = r13.a()
                java.lang.String r5 = "[App Authentication] Unauthorized"
                r0.i(r1, r2, r3, r4, r5)
                com.portfolio.platform.PortfolioApp$a r13 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r13 = r13.c()
                com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
                r0.<init>()
                r13.a(r0)
            L_0x0134:
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.wt6.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ wt6 a;

        @DexIgnore
        public c(wt6 wt6) {
            this.a = wt6;
        }

        @DexIgnore
        public final void run() {
            FLogger.INSTANCE.getLocal().d(wt6.l.a(), "runnable");
            this.a.h();
        }
    }

    /*
    static {
        String simpleName = wt6.class.getSimpleName();
        ee7.a((Object) simpleName, "SplashPresenter::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public wt6(tt6 tt6, UserRepository userRepository, ph5 ph5, ThemeRepository themeRepository) {
        ee7.b(tt6, "mView");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(ph5, "mMigrationHelper");
        ee7.b(themeRepository, "mThemeRepository");
        this.g = tt6;
        this.h = userRepository;
        this.i = ph5;
        this.j = themeRepository;
    }

    @DexIgnore
    public void i() {
        this.g.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        String g2 = PortfolioApp.g0.c().g();
        boolean a2 = this.i.a(g2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "currentAppVersion " + g2 + " complete " + a2);
        if (!a2) {
            this.i.b(g2);
        }
        this.e.postDelayed(this.f, 1000);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        this.e.removeCallbacksAndMessages(null);
    }

    @DexIgnore
    public final void h() {
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }
}
