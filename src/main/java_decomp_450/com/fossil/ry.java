package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ry {
    @DexIgnore
    public /* final */ Map<yw, ky<?>> a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<yw, ky<?>> b; // = new HashMap();

    @DexIgnore
    public ky<?> a(yw ywVar, boolean z) {
        return a(z).get(ywVar);
    }

    @DexIgnore
    public void b(yw ywVar, ky<?> kyVar) {
        Map<yw, ky<?>> a2 = a(kyVar.h());
        if (kyVar.equals(a2.get(ywVar))) {
            a2.remove(ywVar);
        }
    }

    @DexIgnore
    public void a(yw ywVar, ky<?> kyVar) {
        a(kyVar.h()).put(ywVar, kyVar);
    }

    @DexIgnore
    public final Map<yw, ky<?>> a(boolean z) {
        return z ? this.b : this.a;
    }
}
