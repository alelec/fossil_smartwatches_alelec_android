package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bn5 extends fl4<b, fl4.d, fl4.a> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public /* final */ HeartRateSampleRepository d;
    @DexIgnore
    public /* final */ FitnessDataRepository e;
    @DexIgnore
    public /* final */ UserRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public b(Date date) {
            ee7.b(date, "date");
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples", f = "FetchHeartRateSamples.kt", l = {27, 45, 46}, m = "run")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(bn5 bn5, fb7 fb7) {
            super(fb7);
            this.this$0 = bn5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<? super i97>) this);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = bn5.class.getSimpleName();
        ee7.a((Object) simpleName, "FetchHeartRateSamples::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public bn5(HeartRateSampleRepository heartRateSampleRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository) {
        ee7.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        ee7.b(fitnessDataRepository, "mFitnessDataRepository");
        ee7.b(userRepository, "mUserRepository");
        this.d = heartRateSampleRepository;
        this.e = fitnessDataRepository;
        this.f = userRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<? super i97>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0163  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.bn5.b r20, com.fossil.fb7<? super com.fossil.i97> r21) {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r2 = r21
            boolean r3 = r2 instanceof com.fossil.bn5.c
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.fossil.bn5$c r3 = (com.fossil.bn5.c) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.fossil.bn5$c r3 = new com.fossil.bn5$c
            r3.<init>(r0, r2)
        L_0x001e:
            r9 = r3
            java.lang.Object r2 = r9.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r9.label
            java.lang.String r5 = "startDate"
            r6 = 3
            r7 = 2
            r8 = 1
            if (r4 == 0) goto L_0x0091
            if (r4 == r8) goto L_0x007a
            if (r4 == r7) goto L_0x005d
            if (r4 != r6) goto L_0x0055
            java.lang.Object r1 = r9.L$6
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r1 = r9.L$5
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r1 = r9.L$4
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r1 = r9.L$3
            com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
            java.lang.Object r1 = r9.L$2
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r1 = r9.L$1
            com.fossil.bn5$b r1 = (com.fossil.bn5.b) r1
            java.lang.Object r1 = r9.L$0
            com.fossil.bn5 r1 = (com.fossil.bn5) r1
            com.fossil.t87.a(r2)
            goto L_0x018d
        L_0x0055:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x005d:
            java.lang.Object r1 = r9.L$5
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r4 = r9.L$4
            java.util.Date r4 = (java.util.Date) r4
            java.lang.Object r7 = r9.L$3
            com.portfolio.platform.data.model.MFUser r7 = (com.portfolio.platform.data.model.MFUser) r7
            java.lang.Object r8 = r9.L$2
            java.util.Date r8 = (java.util.Date) r8
            java.lang.Object r10 = r9.L$1
            com.fossil.bn5$b r10 = (com.fossil.bn5.b) r10
            java.lang.Object r11 = r9.L$0
            com.fossil.bn5 r11 = (com.fossil.bn5) r11
            com.fossil.t87.a(r2)
            goto L_0x015b
        L_0x007a:
            java.lang.Object r1 = r9.L$2
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r4 = r9.L$1
            com.fossil.bn5$b r4 = (com.fossil.bn5.b) r4
            java.lang.Object r8 = r9.L$0
            com.fossil.bn5 r8 = (com.fossil.bn5) r8
            com.fossil.t87.a(r2)
            r11 = r8
            r17 = r2
            r2 = r1
            r1 = r4
            r4 = r17
            goto L_0x00cf
        L_0x0091:
            com.fossil.t87.a(r2)
            if (r1 != 0) goto L_0x0099
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x0099:
            java.util.Date r2 = r20.a()
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r10 = com.fossil.bn5.g
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "executeUseCase - date="
            r11.append(r12)
            java.lang.String r12 = com.fossil.qy6.a(r2)
            r11.append(r12)
            java.lang.String r11 = r11.toString()
            r4.d(r10, r11)
            com.portfolio.platform.data.source.UserRepository r4 = r0.f
            r9.L$0 = r0
            r9.L$1 = r1
            r9.L$2 = r2
            r9.label = r8
            java.lang.Object r4 = r4.getCurrentUser(r9)
            if (r4 != r3) goto L_0x00ce
            return r3
        L_0x00ce:
            r11 = r0
        L_0x00cf:
            com.portfolio.platform.data.model.MFUser r4 = (com.portfolio.platform.data.model.MFUser) r4
            if (r4 == 0) goto L_0x0198
            java.lang.String r8 = r4.getCreatedAt()
            boolean r8 = android.text.TextUtils.isEmpty(r8)
            if (r8 == 0) goto L_0x00df
            goto L_0x0198
        L_0x00df:
            java.lang.String r8 = r4.getCreatedAt()
            if (r8 == 0) goto L_0x0193
            java.util.Date r8 = com.fossil.zd5.d(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r12 = com.fossil.bn5.g
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "executeUseCase - createdDate="
            r13.append(r14)
            java.lang.String r14 = "createdDate"
            com.fossil.ee7.a(r8, r14)
            java.lang.String r14 = com.fossil.qy6.a(r8)
            r13.append(r14)
            java.lang.String r13 = r13.toString()
            r10.d(r12, r13)
            boolean r10 = com.fossil.zd5.b(r8, r2)
            if (r10 != 0) goto L_0x0190
            java.util.Date r10 = new java.util.Date
            r10.<init>()
            boolean r10 = com.fossil.zd5.b(r2, r10)
            if (r10 == 0) goto L_0x0121
            goto L_0x0190
        L_0x0121:
            java.util.Calendar r10 = com.fossil.zd5.s(r2)
            java.lang.String r12 = "DateHelper.getStartOfWeek(date)"
            com.fossil.ee7.a(r10, r12)
            java.util.Date r10 = r10.getTime()
            boolean r12 = com.fossil.zd5.c(r8, r10)
            if (r12 == 0) goto L_0x0135
            r10 = r8
        L_0x0135:
            com.portfolio.platform.data.source.FitnessDataRepository r12 = r11.e
            com.fossil.ee7.a(r10, r5)
            r9.L$0 = r11
            r9.L$1 = r1
            r9.L$2 = r2
            r9.L$3 = r4
            r9.L$4 = r8
            r9.L$5 = r10
            r9.label = r7
            java.lang.Object r7 = r12.getFitnessData(r10, r2, r9)
            if (r7 != r3) goto L_0x014f
            return r3
        L_0x014f:
            r17 = r10
            r10 = r1
            r1 = r17
            r18 = r8
            r8 = r2
            r2 = r7
            r7 = r4
            r4 = r18
        L_0x015b:
            java.util.List r2 = (java.util.List) r2
            boolean r12 = r2.isEmpty()
            if (r12 == 0) goto L_0x018d
            com.portfolio.platform.data.source.HeartRateSampleRepository r12 = r11.d
            com.fossil.ee7.a(r1, r5)
            r13 = 0
            r14 = 0
            r15 = 12
            r16 = 0
            r9.L$0 = r11
            r9.L$1 = r10
            r9.L$2 = r8
            r9.L$3 = r7
            r9.L$4 = r4
            r9.L$5 = r1
            r9.L$6 = r2
            r9.label = r6
            r4 = r12
            r5 = r1
            r6 = r8
            r7 = r13
            r8 = r14
            r10 = r15
            r11 = r16
            java.lang.Object r1 = com.portfolio.platform.data.source.HeartRateSampleRepository.fetchHeartRateSamples$default(r4, r5, r6, r7, r8, r9, r10, r11)
            if (r1 != r3) goto L_0x018d
            return r3
        L_0x018d:
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x0190:
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x0193:
            com.fossil.ee7.a()
            r1 = 0
            throw r1
        L_0x0198:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.bn5.g
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "executeUseCase - FAILED!!! with user="
            r3.append(r5)
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bn5.a(com.fossil.bn5$b, com.fossil.fb7):java.lang.Object");
    }
}
