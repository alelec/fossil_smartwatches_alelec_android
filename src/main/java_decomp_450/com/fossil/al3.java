package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class al3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ bg3 a;
    @DexIgnore
    public /* final */ /* synthetic */ zk3 b;

    @DexIgnore
    public al3(zk3 zk3, bg3 bg3) {
        this.b = zk3;
        this.a = bg3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.b) {
            boolean unused = this.b.a = false;
            if (!this.b.c.A()) {
                this.b.c.e().A().a("Connected to remote service");
                this.b.c.a(this.a);
            }
        }
    }
}
