package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hz5 {
    @DexIgnore
    public /* final */ j06 a;
    @DexIgnore
    public /* final */ n36 b;
    @DexIgnore
    public /* final */ r26 c;

    @DexIgnore
    public hz5(j06 j06, n36 n36, r26 r26) {
        ee7.b(j06, "mComplicationsContractView");
        ee7.b(n36, "mWatchAppsContractView");
        ee7.b(r26, "mCustomizeThemeContractView");
        this.a = j06;
        this.b = n36;
        this.c = r26;
    }

    @DexIgnore
    public final j06 a() {
        return this.a;
    }

    @DexIgnore
    public final r26 b() {
        return this.c;
    }

    @DexIgnore
    public final n36 c() {
        return this.b;
    }
}
