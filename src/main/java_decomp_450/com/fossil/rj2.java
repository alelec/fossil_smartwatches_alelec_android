package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rj2 implements Parcelable.Creator<pj2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ pj2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        ArrayList arrayList = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            if (j72.a(a) != 1) {
                j72.v(parcel, a);
            } else {
                arrayList = j72.c(parcel, a, DataType.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new pj2(arrayList);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ pj2[] newArray(int i) {
        return new pj2[i];
    }
}
