package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b16 {
    @DexIgnore
    public /* final */ a16 a;

    @DexIgnore
    public b16(a16 a16) {
        ee7.b(a16, "mCommuteTimeSettingsDefaultAddressContractView");
        this.a = a16;
    }

    @DexIgnore
    public final a16 a() {
        return this.a;
    }
}
