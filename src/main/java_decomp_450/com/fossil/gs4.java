package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.fossil.cy6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.create_intro.BCCreateChallengeIntroActivity;
import com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsActivity;
import com.portfolio.platform.view.FlexibleButton;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gs4 extends go5 implements cy6.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a p; // = new a(null);
    @DexIgnore
    public qw6<az4> f;
    @DexIgnore
    public js4 g;
    @DexIgnore
    public rj4 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return gs4.j;
        }

        @DexIgnore
        public final gs4 b() {
            return new gs4();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<View, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ gs4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(gs4 gs4) {
            super(1);
            this.this$0 = gs4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(View view) {
            invoke(view);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            gs4.a(this.this$0).c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<gu4> {
        @DexIgnore
        public /* final */ /* synthetic */ gs4 a;

        @DexIgnore
        public c(gs4 gs4) {
            this.a = gs4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(gu4 gu4) {
            if (gu4 != null) {
                int i = hs4.a[gu4.ordinal()];
                boolean z = true;
                if (i == 1) {
                    bx6 bx6 = bx6.c;
                    FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                    ee7.a((Object) childFragmentManager, "childFragmentManager");
                    String a2 = ig5.a(PortfolioApp.g0.c(), 2131886231);
                    ee7.a((Object) a2, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    String a3 = ig5.a(PortfolioApp.g0.c(), 2131886229);
                    ee7.a((Object) a3, "LanguageHelper.getString\u2026uCanOnlyJoinOneChallenge)");
                    bx6.a(childFragmentManager, a2, a3);
                } else if (i == 2) {
                    if (PortfolioApp.g0.c().c().length() <= 0) {
                        z = false;
                    }
                    if (z) {
                        bx6 bx62 = bx6.c;
                        FragmentManager childFragmentManager2 = this.a.getChildFragmentManager();
                        ee7.a((Object) childFragmentManager2, "childFragmentManager");
                        String a4 = ig5.a(PortfolioApp.g0.c(), 2131886231);
                        ee7.a((Object) a4, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                        String a5 = ig5.a(PortfolioApp.g0.c(), 2131886230);
                        ee7.a((Object) a5, "LanguageHelper.getString\u2026IsDisconnectedPleaseSync)");
                        bx62.a(childFragmentManager2, a4, a5);
                        return;
                    }
                    bx6 bx63 = bx6.c;
                    FragmentManager childFragmentManager3 = this.a.getChildFragmentManager();
                    ee7.a((Object) childFragmentManager3, "childFragmentManager");
                    String a6 = ig5.a(PortfolioApp.g0.c(), 2131886231);
                    ee7.a((Object) a6, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    String a7 = ig5.a(PortfolioApp.g0.c(), 2131886223);
                    ee7.a((Object) a7, "LanguageHelper.getString\u2026ctivateYourDeviceToJoinA)");
                    bx63.a(childFragmentManager3, a6, a7);
                } else if (i == 3) {
                    bx6 bx64 = bx6.c;
                    FragmentManager childFragmentManager4 = this.a.getChildFragmentManager();
                    ee7.a((Object) childFragmentManager4, "childFragmentManager");
                    bx64.X(childFragmentManager4);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gs4 a;

        @DexIgnore
        public d(gs4 gs4) {
            this.a = gs4;
        }

        @DexIgnore
        @Override // com.fossil.zd
        public final void onChanged(Object obj) {
            this.a.h1();
        }
    }

    /*
    static {
        String name = gs4.class.getName();
        ee7.a((Object) name, "BCCreateSubTabFragment::class.java.name");
        j = name;
    }
    */

    @DexIgnore
    public static final /* synthetic */ js4 a(gs4 gs4) {
        js4 js4 = gs4.g;
        if (js4 != null) {
            return js4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void f1() {
        FlexibleButton flexibleButton;
        qw6<az4> qw6 = this.f;
        if (qw6 != null) {
            az4 a2 = qw6.a();
            if (a2 != null && (flexibleButton = a2.q) != null) {
                du4.a(flexibleButton, new b(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        js4 js4 = this.g;
        if (js4 != null) {
            js4.b().a(getViewLifecycleOwner(), new c(this));
            js4 js42 = this.g;
            if (js42 != null) {
                js42.a().a(getViewLifecycleOwner(), new d(this));
            } else {
                ee7.d("viewModel");
                throw null;
            }
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void h1() {
        BCCreateChallengeIntroActivity.a aVar = BCCreateChallengeIntroActivity.y;
        Fragment requireParentFragment = requireParentFragment();
        ee7.a((Object) requireParentFragment, "requireParentFragment()");
        aVar.a(requireParentFragment);
    }

    @DexIgnore
    public final void i1() {
        BCFindFriendsActivity.y.a(this, null);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        f1();
        g1();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        az4 az4 = (az4) qb.a(layoutInflater, 2131558526, viewGroup, false, a1());
        this.f = new qw6<>(this, az4);
        PortfolioApp.g0.c().f().v().a(this);
        rj4 rj4 = this.h;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(js4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026TabViewModel::class.java)");
            this.g = (js4) a2;
            ee7.a((Object) az4, "binding");
            return az4.d();
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        if (str.hashCode() != 1839815732 || !str.equals("FINDING_FRIEND")) {
            return;
        }
        if (i2 == 2131363229) {
            i1();
        } else if (i2 == 2131363307) {
            h1();
        }
    }
}
