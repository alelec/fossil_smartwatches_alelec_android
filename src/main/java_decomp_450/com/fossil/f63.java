package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface f63 extends IInterface {
    @DexIgnore
    void a(LocationAvailability locationAvailability) throws RemoteException;

    @DexIgnore
    void a(LocationResult locationResult) throws RemoteException;
}
