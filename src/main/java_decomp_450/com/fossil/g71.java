package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import java.io.File;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g71 {
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public int c;
    @DexIgnore
    public long d;
    @DexIgnore
    public /* final */ Handler e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public g71(String str, int i, long j, Handler handler, String str2, String str3) {
        this.b = str;
        this.c = i;
        this.d = j;
        this.e = handler;
        this.f = str2;
        this.g = str3;
        Context a2 = u31.g.a();
        int i2 = 0;
        SharedPreferences sharedPreferences = a2 != null ? a2.getSharedPreferences(str, 0) : null;
        this.a = sharedPreferences != null ? sharedPreferences.getInt("file_count", 0) : i2;
    }

    @DexIgnore
    public final String a(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.b);
        sb.append(File.separatorChar);
        we7 we7 = we7.a;
        String format = String.format("%s_%010d.%s", Arrays.copyOf(new Object[]{this.f, Integer.valueOf(i), this.g}, 3));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        sb.append(format);
        return sb.toString();
    }

    @DexIgnore
    public final File b(int i) {
        File d2 = d();
        if (d2 == null) {
            return null;
        }
        if (!d2.exists()) {
            d2.mkdirs();
        }
        File c2 = c();
        if (c2 == null) {
            return null;
        }
        if (c2.length() <= 0 || c2.length() + ((long) i) < ((long) this.c)) {
            return c2;
        }
        f();
        a();
        return b(i);
    }

    @DexIgnore
    public final File c() {
        File file;
        synchronized (Integer.valueOf(this.a)) {
            String a2 = a(this.a);
            Context a3 = u31.g.a();
            file = a3 != null ? new File(a3.getFilesDir(), a2) : null;
        }
        return file;
    }

    @DexIgnore
    public final File d() {
        String str = this.b;
        Context a2 = u31.g.a();
        if (a2 != null) {
            return new File(a2.getFilesDir(), str);
        }
        return null;
    }

    @DexIgnore
    public final long e() {
        long j = 0;
        try {
            for (File file : b()) {
                j += file.length();
            }
        } catch (Exception unused) {
        }
        return j;
    }

    @DexIgnore
    public final void f() {
        SharedPreferences.Editor edit;
        SharedPreferences.Editor putInt;
        synchronized (Integer.valueOf(this.a)) {
            long j = (long) Integer.MAX_VALUE;
            this.a = (int) (((((long) this.a) + 1) + j) % j);
            String str = this.b;
            Context a2 = u31.g.a();
            SharedPreferences sharedPreferences = a2 != null ? a2.getSharedPreferences(str, 0) : null;
            if (!(sharedPreferences == null || (edit = sharedPreferences.edit()) == null || (putInt = edit.putInt("file_count", this.a)) == null)) {
                putInt.apply();
                i97 i97 = i97.a;
            }
        }
    }

    @DexIgnore
    public final boolean a(String str) {
        ee7.a((Object) str.getBytes(sg7.a), "(this as java.lang.String).getBytes(charset)");
        File b2 = b(str.length());
        if (b2 != null) {
            return this.e.post(new m31(b2, this, str));
        }
        return false;
    }

    @DexIgnore
    public final void a() {
        List c2 = t97.c(b(), new j51());
        int i = 0;
        long j = 0;
        while (i < c2.size() && j < this.d) {
            File file = (File) ea7.a(c2, i);
            j += file != null ? file.length() : 0;
            i++;
        }
        int size = c2.size();
        while (i < size) {
            File file2 = (File) ea7.a(c2, i);
            if (file2 != null) {
                file2.delete();
            }
            i++;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0054, code lost:
        if (com.fossil.ee7.a((java.lang.Object) com.fossil.rc7.d(r6), (java.lang.Object) r12.g) != false) goto L_0x0058;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.io.File[] b() {
        /*
            r12 = this;
            java.io.File r0 = r12.d()
            r1 = 0
            if (r0 == 0) goto L_0x0073
            boolean r2 = r0.exists()
            if (r2 != 0) goto L_0x000e
            goto L_0x0073
        L_0x000e:
            r12.f()
            java.io.File[] r0 = r0.listFiles()
            if (r0 == 0) goto L_0x0018
            goto L_0x001a
        L_0x0018:
            java.io.File[] r0 = new java.io.File[r1]
        L_0x001a:
            java.io.File r2 = r12.c()
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            int r4 = r0.length
            r5 = 0
        L_0x0025:
            if (r5 >= r4) goto L_0x0060
            r6 = r0[r5]
            boolean r7 = com.fossil.ee7.a(r6, r2)
            r8 = 1
            r7 = r7 ^ r8
            if (r7 == 0) goto L_0x0057
            boolean r7 = r6.isFile()
            if (r7 == 0) goto L_0x0057
            java.lang.String r7 = r6.getName()
            java.lang.String r9 = "it.name"
            com.fossil.ee7.a(r7, r9)
            java.lang.String r9 = r12.f
            r10 = 2
            r11 = 0
            boolean r7 = com.fossil.mh7.c(r7, r9, r1, r10, r11)
            if (r7 == 0) goto L_0x0057
            java.lang.String r7 = com.fossil.rc7.d(r6)
            java.lang.String r9 = r12.g
            boolean r7 = com.fossil.ee7.a(r7, r9)
            if (r7 == 0) goto L_0x0057
            goto L_0x0058
        L_0x0057:
            r8 = 0
        L_0x0058:
            if (r8 == 0) goto L_0x005d
            r3.add(r6)
        L_0x005d:
            int r5 = r5 + 1
            goto L_0x0025
        L_0x0060:
            java.io.File[] r0 = new java.io.File[r1]
            java.lang.Object[] r0 = r3.toArray(r0)
            if (r0 == 0) goto L_0x006b
            java.io.File[] r0 = (java.io.File[]) r0
            return r0
        L_0x006b:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Array<T>"
            r0.<init>(r1)
            throw r0
        L_0x0073:
            com.fossil.t11 r0 = com.fossil.t11.a
            java.io.File[] r0 = new java.io.File[r1]
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.g71.b():java.io.File[]");
    }
}
