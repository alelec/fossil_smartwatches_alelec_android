package com.fossil;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vo4 {
    @DexIgnore
    public /* final */ vn4 a;

    @DexIgnore
    public vo4(vn4 vn4) {
        ee7.b(vn4, "dao");
        this.a = vn4;
    }

    @DexIgnore
    public final long a(un4 un4) {
        ee7.b(un4, "friend");
        return this.a.a(un4);
    }

    @DexIgnore
    public final un4 b(String str) {
        ee7.b(str, "id");
        return this.a.b(str);
    }

    @DexIgnore
    public final void c() {
        this.a.i();
    }

    @DexIgnore
    public final void d() {
        this.a.f();
    }

    @DexIgnore
    public final void e() {
        this.a.g();
    }

    @DexIgnore
    public final List<un4> f() {
        return this.a.l();
    }

    @DexIgnore
    public final List<un4> g() {
        return this.a.j();
    }

    @DexIgnore
    public final List<un4> h() {
        return this.a.k();
    }

    @DexIgnore
    public final LiveData<List<un4>> i() {
        return this.a.b();
    }

    @DexIgnore
    public final List<un4> j() {
        return this.a.h();
    }

    @DexIgnore
    public final List<un4> k() {
        return this.a.e();
    }

    @DexIgnore
    public final List<un4> l() {
        return this.a.c();
    }

    @DexIgnore
    public final Long[] a(List<un4> list) {
        ee7.b(list, NativeProtocol.AUDIENCE_FRIENDS);
        return this.a.a(list);
    }

    @DexIgnore
    public final int b() {
        return this.a.d();
    }

    @DexIgnore
    public final int a(String str) {
        ee7.b(str, "profileId");
        return this.a.a(str);
    }

    @DexIgnore
    public final int a(String[] strArr) {
        ee7.b(strArr, "ids");
        return this.a.a(strArr);
    }

    @DexIgnore
    public final void a() {
        this.a.a();
    }
}
