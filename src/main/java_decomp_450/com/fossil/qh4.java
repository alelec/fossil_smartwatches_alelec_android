package com.fossil;

import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qh4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public uh4 b;
    @DexIgnore
    public ng4 c;
    @DexIgnore
    public ng4 d;
    @DexIgnore
    public /* final */ StringBuilder e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public th4 h;
    @DexIgnore
    public int i;

    @DexIgnore
    public qh4(String str) {
        byte[] bytes = str.getBytes(Charset.forName("ISO-8859-1"));
        StringBuilder sb = new StringBuilder(bytes.length);
        int length = bytes.length;
        int i2 = 0;
        while (i2 < length) {
            char c2 = (char) (bytes[i2] & 255);
            if (c2 != '?' || str.charAt(i2) == '?') {
                sb.append(c2);
                i2++;
            } else {
                throw new IllegalArgumentException("Message contains characters outside ISO-8859-1 encoding.");
            }
        }
        this.a = sb.toString();
        this.b = uh4.FORCE_NONE;
        this.e = new StringBuilder(str.length());
        this.g = -1;
    }

    @DexIgnore
    public void a(uh4 uh4) {
        this.b = uh4;
    }

    @DexIgnore
    public StringBuilder b() {
        return this.e;
    }

    @DexIgnore
    public char c() {
        return this.a.charAt(this.f);
    }

    @DexIgnore
    public String d() {
        return this.a;
    }

    @DexIgnore
    public int e() {
        return this.g;
    }

    @DexIgnore
    public int f() {
        return h() - this.f;
    }

    @DexIgnore
    public th4 g() {
        return this.h;
    }

    @DexIgnore
    public final int h() {
        return this.a.length() - this.i;
    }

    @DexIgnore
    public boolean i() {
        return this.f < h();
    }

    @DexIgnore
    public void j() {
        this.g = -1;
    }

    @DexIgnore
    public void k() {
        this.h = null;
    }

    @DexIgnore
    public void l() {
        c(a());
    }

    @DexIgnore
    public void a(ng4 ng4, ng4 ng42) {
        this.c = ng4;
        this.d = ng42;
    }

    @DexIgnore
    public void b(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public void c(int i2) {
        th4 th4 = this.h;
        if (th4 == null || i2 > th4.a()) {
            this.h = th4.a(i2, this.b, this.c, this.d, true);
        }
    }

    @DexIgnore
    public void a(int i2) {
        this.i = i2;
    }

    @DexIgnore
    public void a(String str) {
        this.e.append(str);
    }

    @DexIgnore
    public void a(char c2) {
        this.e.append(c2);
    }

    @DexIgnore
    public int a() {
        return this.e.length();
    }
}
