package com.fossil;

import android.bluetooth.BluetoothDevice;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w51 {
    @DexIgnore
    public static /* final */ Hashtable<String, ri1> a; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ w51 b; // = new w51();

    @DexIgnore
    public final ri1 a(BluetoothDevice bluetoothDevice) {
        ri1 ri1;
        t11 t11 = t11.a;
        bluetoothDevice.getAddress();
        synchronized (a) {
            ri1 = a.get(bluetoothDevice.getAddress());
            if (ri1 == null) {
                ri1 = new ri1(bluetoothDevice, null);
                a.put(ri1.x.getAddress(), ri1);
            }
        }
        return ri1;
    }
}
