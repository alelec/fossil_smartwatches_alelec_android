package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ha0 extends na0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ha0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ha0 createFromParcel(Parcel parcel) {
            return new ha0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ha0[] newArray(int i) {
            return new ha0[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public ha0 m24createFromParcel(Parcel parcel) {
            return new ha0(parcel, null);
        }
    }

    @DexIgnore
    public ha0() {
        super(pa0.DIAGNOSTICS, null, null, 6);
    }

    @DexIgnore
    public ha0(zg0 zg0) {
        super(pa0.DIAGNOSTICS, zg0, null, 4);
    }

    @DexIgnore
    public /* synthetic */ ha0(Parcel parcel, zd7 zd7) {
        super(parcel);
    }
}
