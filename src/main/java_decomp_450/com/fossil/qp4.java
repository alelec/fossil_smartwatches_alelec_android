package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qp4 implements Factory<pp4> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ qp4 a; // = new qp4();
    }

    @DexIgnore
    public static qp4 a() {
        return a.a;
    }

    @DexIgnore
    public static pp4 b() {
        return new pp4();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public pp4 get() {
        return b();
    }
}
