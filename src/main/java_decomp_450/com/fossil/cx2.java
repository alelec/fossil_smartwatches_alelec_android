package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cx2 implements kx2 {
    @DexIgnore
    public kx2[] a;

    @DexIgnore
    public cx2(kx2... kx2Arr) {
        this.a = kx2Arr;
    }

    @DexIgnore
    @Override // com.fossil.kx2
    public final boolean zza(Class<?> cls) {
        for (kx2 kx2 : this.a) {
            if (kx2.zza(cls)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.kx2
    public final hx2 zzb(Class<?> cls) {
        kx2[] kx2Arr = this.a;
        for (kx2 kx2 : kx2Arr) {
            if (kx2.zza(cls)) {
                return kx2.zzb(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }
}
