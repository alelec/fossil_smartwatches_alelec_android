package com.fossil;

import android.content.Context;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f10<T> implements ex<T> {
    @DexIgnore
    public static /* final */ ex<?> b; // = new f10();

    @DexIgnore
    public static <T> f10<T> a() {
        return (f10) b;
    }

    @DexIgnore
    @Override // com.fossil.ex
    public uy<T> a(Context context, uy<T> uyVar, int i, int i2) {
        return uyVar;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public void a(MessageDigest messageDigest) {
    }
}
