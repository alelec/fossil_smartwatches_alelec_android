package com.fossil;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentHostCallback;
import androidx.fragment.app.FragmentManager;
import com.facebook.applinks.FacebookAppLinkResolver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ec implements LayoutInflater.Factory2 {
    @DexIgnore
    public /* final */ FragmentManager a;

    @DexIgnore
    public ec(FragmentManager fragmentManager) {
        this.a = fragmentManager;
    }

    @DexIgnore
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }

    @DexIgnore
    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        if (FragmentContainerView.class.getName().equals(str)) {
            return new FragmentContainerView(context, attributeSet, this.a);
        }
        Fragment fragment = null;
        if (!"fragment".equals(str)) {
            return null;
        }
        String attributeValue = attributeSet.getAttributeValue(null, FacebookAppLinkResolver.APP_LINK_TARGET_CLASS_KEY);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, xb.Fragment);
        if (attributeValue == null) {
            attributeValue = obtainStyledAttributes.getString(xb.Fragment_android_name);
        }
        int resourceId = obtainStyledAttributes.getResourceId(xb.Fragment_android_id, -1);
        String string = obtainStyledAttributes.getString(xb.Fragment_android_tag);
        obtainStyledAttributes.recycle();
        if (attributeValue == null || !dc.isFragmentClass(context.getClassLoader(), attributeValue)) {
            return null;
        }
        int id = view != null ? view.getId() : 0;
        if (id == -1 && resourceId == -1 && string == null) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + attributeValue);
        }
        if (resourceId != -1) {
            fragment = this.a.b(resourceId);
        }
        if (fragment == null && string != null) {
            fragment = this.a.b(string);
        }
        if (fragment == null && id != -1) {
            fragment = this.a.b(id);
        }
        if (FragmentManager.d(2)) {
            Log.v("FragmentManager", "onCreateView: id=0x" + Integer.toHexString(resourceId) + " fname=" + attributeValue + " existing=" + fragment);
        }
        if (fragment == null) {
            fragment = this.a.u().instantiate(context.getClassLoader(), attributeValue);
            fragment.mFromLayout = true;
            fragment.mFragmentId = resourceId != 0 ? resourceId : id;
            fragment.mContainerId = id;
            fragment.mTag = string;
            fragment.mInLayout = true;
            FragmentManager fragmentManager = this.a;
            fragment.mFragmentManager = fragmentManager;
            FragmentHostCallback<?> fragmentHostCallback = fragmentManager.o;
            fragment.mHost = fragmentHostCallback;
            fragment.onInflate(fragmentHostCallback.c(), attributeSet, fragment.mSavedFragmentState);
            this.a.a(fragment);
            this.a.r(fragment);
        } else if (!fragment.mInLayout) {
            fragment.mInLayout = true;
            FragmentHostCallback<?> fragmentHostCallback2 = this.a.o;
            fragment.mHost = fragmentHostCallback2;
            fragment.onInflate(fragmentHostCallback2.c(), attributeSet, fragment.mSavedFragmentState);
        } else {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(resourceId) + ", tag " + string + ", or parent id 0x" + Integer.toHexString(id) + " with another fragment for " + attributeValue);
        }
        FragmentManager fragmentManager2 = this.a;
        if (fragmentManager2.n >= 1 || !fragment.mFromLayout) {
            this.a.r(fragment);
        } else {
            fragmentManager2.a(fragment, 1);
        }
        View view2 = fragment.mView;
        if (view2 != null) {
            if (resourceId != 0) {
                view2.setId(resourceId);
            }
            if (fragment.mView.getTag() == null) {
                fragment.mView.setTag(string);
            }
            return fragment.mView;
        }
        throw new IllegalStateException("Fragment " + attributeValue + " did not create a view.");
    }
}
