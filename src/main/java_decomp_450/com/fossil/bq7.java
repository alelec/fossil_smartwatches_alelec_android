package com.fossil;

import com.fossil.vp7;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq7 implements Closeable {
    @DexIgnore
    public static /* final */ Logger g; // = Logger.getLogger(wp7.class.getName());
    @DexIgnore
    public /* final */ zq7 a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ yq7 c;
    @DexIgnore
    public int d; // = 16384;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ vp7.b f;

    @DexIgnore
    public bq7(zq7 zq7, boolean z) {
        this.a = zq7;
        this.b = z;
        yq7 yq7 = new yq7();
        this.c = yq7;
        this.f = new vp7.b(yq7);
    }

    @DexIgnore
    public synchronized void a() throws IOException {
        if (this.e) {
            throw new IOException("closed");
        } else if (this.b) {
            if (g.isLoggable(Level.FINE)) {
                g.fine(ro7.a(">> CONNECTION %s", wp7.a.hex()));
            }
            this.a.write(wp7.a.toByteArray());
            this.a.flush();
        }
    }

    @DexIgnore
    public int b() {
        return this.d;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() throws IOException {
        this.e = true;
        this.a.close();
    }

    @DexIgnore
    public synchronized void flush() throws IOException {
        if (!this.e) {
            this.a.flush();
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void b(eq7 eq7) throws IOException {
        if (!this.e) {
            int i = 0;
            a(0, eq7.d() * 6, (byte) 4, (byte) 0);
            while (i < 10) {
                if (eq7.d(i)) {
                    this.a.writeShort(i == 4 ? 3 : i == 7 ? 4 : i);
                    this.a.writeInt(eq7.a(i));
                }
                i++;
            }
            this.a.flush();
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void a(eq7 eq7) throws IOException {
        if (!this.e) {
            this.d = eq7.c(this.d);
            if (eq7.b() != -1) {
                this.f.b(eq7.b());
            }
            a(0, 0, (byte) 4, (byte) 1);
            this.a.flush();
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public final void b(int i, long j) throws IOException {
        while (j > 0) {
            int min = (int) Math.min((long) this.d, j);
            long j2 = (long) min;
            j -= j2;
            a(i, min, (byte) 9, j == 0 ? (byte) 4 : 0);
            this.a.a(this.c, j2);
        }
    }

    @DexIgnore
    public synchronized void a(int i, int i2, List<up7> list) throws IOException {
        if (!this.e) {
            this.f.a(list);
            long x = this.c.x();
            int min = (int) Math.min((long) (this.d - 4), x);
            long j = (long) min;
            int i3 = (x > j ? 1 : (x == j ? 0 : -1));
            a(i, min + 4, (byte) 5, i3 == 0 ? (byte) 4 : 0);
            this.a.writeInt(i2 & Integer.MAX_VALUE);
            this.a.a(this.c, j);
            if (i3 > 0) {
                b(i, x - j);
            }
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void a(boolean z, int i, int i2, List<up7> list) throws IOException {
        if (!this.e) {
            a(z, i, list);
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void a(int i, tp7 tp7) throws IOException {
        if (this.e) {
            throw new IOException("closed");
        } else if (tp7.httpCode != -1) {
            a(i, 4, (byte) 3, (byte) 0);
            this.a.writeInt(tp7.httpCode);
            this.a.flush();
        } else {
            throw new IllegalArgumentException();
        }
    }

    @DexIgnore
    public synchronized void a(boolean z, int i, yq7 yq7, int i2) throws IOException {
        if (!this.e) {
            byte b2 = 0;
            if (z) {
                b2 = (byte) 1;
            }
            a(i, b2, yq7, i2);
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public void a(int i, byte b2, yq7 yq7, int i2) throws IOException {
        a(i, i2, (byte) 0, b2);
        if (i2 > 0) {
            this.a.a(yq7, (long) i2);
        }
    }

    @DexIgnore
    public synchronized void a(boolean z, int i, int i2) throws IOException {
        if (!this.e) {
            a(0, 8, (byte) 6, z ? (byte) 1 : 0);
            this.a.writeInt(i);
            this.a.writeInt(i2);
            this.a.flush();
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void a(int i, tp7 tp7, byte[] bArr) throws IOException {
        if (this.e) {
            throw new IOException("closed");
        } else if (tp7.httpCode != -1) {
            a(0, bArr.length + 8, (byte) 7, (byte) 0);
            this.a.writeInt(i);
            this.a.writeInt(tp7.httpCode);
            if (bArr.length > 0) {
                this.a.write(bArr);
            }
            this.a.flush();
        } else {
            wp7.a("errorCode.httpCode == -1", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public synchronized void a(int i, long j) throws IOException {
        if (this.e) {
            throw new IOException("closed");
        } else if (j == 0 || j > 2147483647L) {
            wp7.a("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", Long.valueOf(j));
            throw null;
        } else {
            a(i, 4, (byte) 8, (byte) 0);
            this.a.writeInt((int) j);
            this.a.flush();
        }
    }

    @DexIgnore
    public void a(int i, int i2, byte b2, byte b3) throws IOException {
        if (g.isLoggable(Level.FINE)) {
            g.fine(wp7.a(false, i, i2, b2, b3));
        }
        int i3 = this.d;
        if (i2 > i3) {
            wp7.a("FRAME_SIZE_ERROR length > %d: %d", Integer.valueOf(i3), Integer.valueOf(i2));
            throw null;
        } else if ((Integer.MIN_VALUE & i) == 0) {
            a(this.a, i2);
            this.a.writeByte(b2 & 255);
            this.a.writeByte(b3 & 255);
            this.a.writeInt(i & Integer.MAX_VALUE);
        } else {
            wp7.a("reserved bit set: %s", Integer.valueOf(i));
            throw null;
        }
    }

    @DexIgnore
    public static void a(zq7 zq7, int i) throws IOException {
        zq7.writeByte((i >>> 16) & 255);
        zq7.writeByte((i >>> 8) & 255);
        zq7.writeByte(i & 255);
    }

    @DexIgnore
    public void a(boolean z, int i, List<up7> list) throws IOException {
        if (!this.e) {
            this.f.a(list);
            long x = this.c.x();
            int min = (int) Math.min((long) this.d, x);
            long j = (long) min;
            int i2 = (x > j ? 1 : (x == j ? 0 : -1));
            byte b2 = i2 == 0 ? (byte) 4 : 0;
            if (z) {
                b2 = (byte) (b2 | 1);
            }
            a(i, min, (byte) 1, b2);
            this.a.a(this.c, j);
            if (i2 > 0) {
                b(i, x - j);
                return;
            }
            return;
        }
        throw new IOException("closed");
    }
}
