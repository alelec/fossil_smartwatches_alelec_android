package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cy6;
import com.fossil.yp5;
import com.portfolio.platform.data.model.InstalledApp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wx5 extends go5 implements vx5, cy6.g {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public ux5 f;
    @DexIgnore
    public yp5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return wx5.i;
        }

        @DexIgnore
        public final wx5 b() {
            return new wx5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements yp5.a {
        @DexIgnore
        public /* final */ /* synthetic */ wx5 a;

        @DexIgnore
        public b(wx5 wx5) {
            this.a = wx5;
        }

        @DexIgnore
        @Override // com.fossil.yp5.a
        public void a(at5 at5, boolean z) {
            ee7.b(at5, "appWrapper");
            if (at5.getCurrentHandGroup() == 0 || at5.getCurrentHandGroup() == wx5.b(this.a).h()) {
                wx5.b(this.a).a(at5, z);
                return;
            }
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            InstalledApp installedApp = at5.getInstalledApp();
            String title = installedApp != null ? installedApp.getTitle() : null;
            if (title != null) {
                bx6.a(childFragmentManager, title, at5.getCurrentHandGroup(), wx5.b(this.a).h(), at5);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wx5 a;

        @DexIgnore
        public c(wx5 wx5) {
            this.a = wx5;
        }

        @DexIgnore
        public final void onClick(View view) {
            wx5.b(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ wx5 a;
        @DexIgnore
        public /* final */ /* synthetic */ a45 b;

        @DexIgnore
        public d(wx5 wx5, a45 a45) {
            this.a = wx5;
            this.b = a45;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.b.t;
            ee7.a((Object) imageView, "binding.ivClear");
            imageView.setVisibility(i3 == 0 ? 4 : 0);
            wx5.a(this.a).getFilter().filter(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ a45 a;

        @DexIgnore
        public e(a45 a45) {
            this.a = a45;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                a45 a45 = this.a;
                ee7.a((Object) a45, "binding");
                a45.d().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ a45 a;

        @DexIgnore
        public f(a45 a45) {
            this.a = a45;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.q.setText("");
        }
    }

    /*
    static {
        String simpleName = wx5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationHybridAppFra\u2026nt::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ yp5 a(wx5 wx5) {
        yp5 yp5 = wx5.g;
        if (yp5 != null) {
            return yp5;
        }
        ee7.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ux5 b(wx5 wx5) {
        ux5 ux5 = wx5.f;
        if (ux5 != null) {
            return ux5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        ux5 ux5 = this.f;
        if (ux5 != null) {
            ux5.i();
            return true;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.vx5
    public void h() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.vx5
    public void j() {
        b();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        a45 a45 = (a45) qb.a(layoutInflater, 2131558592, viewGroup, false, a1());
        a45.s.setOnClickListener(new c(this));
        a45.q.addTextChangedListener(new d(this, a45));
        a45.q.setOnFocusChangeListener(new e(a45));
        a45.t.setOnClickListener(new f(a45));
        yp5 yp5 = new yp5();
        yp5.a(new b(this));
        this.g = yp5;
        RecyclerView recyclerView = a45.w;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        yp5 yp52 = this.g;
        if (yp52 != null) {
            recyclerView.setAdapter(yp52);
            String b2 = eh5.l.a().b("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(b2)) {
                a45.u.setBackgroundColor(Color.parseColor(b2));
            }
            new qw6(this, a45);
            ee7.a((Object) a45, "binding");
            return a45.d();
        }
        ee7.d("mAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        ux5 ux5 = this.f;
        if (ux5 != null) {
            ux5.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ux5 ux5 = this.f;
        if (ux5 != null) {
            ux5.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ux5 ux5) {
        ee7.b(ux5, "presenter");
        this.f = ux5;
    }

    @DexIgnore
    @Override // com.fossil.vx5
    public void a(List<at5> list, int i2) {
        ee7.b(list, "listAppWrapper");
        yp5 yp5 = this.g;
        if (yp5 != null) {
            yp5.a(list, i2);
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.vx5
    public void a(ArrayList<String> arrayList) {
        ee7.b(arrayList, "uriAppsSelected");
        Intent intent = new Intent();
        intent.putStringArrayListExtra("APP_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        at5 at5;
        at5 at52;
        ee7.b(str, "tag");
        if (str.hashCode() != -1984760733 || !str.equals("CONFIRM_REASSIGN_APP")) {
            return;
        }
        if (i2 != 2131363307) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (at52 = (at5) extras.getSerializable("CONFIRM_REASSIGN_APPWRAPPER")) != null) {
                ux5 ux5 = this.f;
                if (ux5 != null) {
                    ux5.a(at52, false);
                    yp5 yp5 = this.g;
                    if (yp5 != null) {
                        yp5.notifyDataSetChanged();
                    } else {
                        ee7.d("mAdapter");
                        throw null;
                    }
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else {
            Bundle extras2 = intent != null ? intent.getExtras() : null;
            if (extras2 != null && (at5 = (at5) extras2.getSerializable("CONFIRM_REASSIGN_APPWRAPPER")) != null) {
                ux5 ux52 = this.f;
                if (ux52 != null) {
                    ux52.a(at5, true);
                    yp5 yp52 = this.g;
                    if (yp52 != null) {
                        yp52.notifyDataSetChanged();
                    } else {
                        ee7.d("mAdapter");
                        throw null;
                    }
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        }
    }
}
