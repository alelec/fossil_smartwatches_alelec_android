package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleFitnessTab;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.RingProgressBar;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r25 extends q25 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i h0; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray i0;
    @DexIgnore
    public long g0;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        i0 = sparseIntArray;
        sparseIntArray.put(2131362728, 1);
        i0.put(2131362707, 2);
        i0.put(2131362112, 3);
        i0.put(2131362888, 4);
        i0.put(2131363348, 5);
        i0.put(2131363347, 6);
        i0.put(2131363089, 7);
        i0.put(2131362089, 8);
        i0.put(2131363382, 9);
        i0.put(2131363115, 10);
        i0.put(2131362004, 11);
        i0.put(2131361813, 12);
        i0.put(2131362129, 13);
        i0.put(2131362117, 14);
        i0.put(2131362969, 15);
        i0.put(2131362968, 16);
        i0.put(2131362970, 17);
        i0.put(2131362971, 18);
        i0.put(2131362081, 19);
        i0.put(2131363383, 20);
        i0.put(2131362378, 21);
        i0.put(2131362273, 22);
        i0.put(2131362080, 23);
        i0.put(2131363344, 24);
        i0.put(2131362860, 25);
        i0.put(2131362074, 26);
        i0.put(2131362451, 27);
        i0.put(2131362383, 28);
        i0.put(2131361978, 29);
        i0.put(2131362296, 30);
        i0.put(2131362295, 31);
        i0.put(2131362297, 32);
        i0.put(2131362299, 33);
        i0.put(2131362300, 34);
        i0.put(2131362298, 35);
        i0.put(2131363446, 36);
        i0.put(2131363005, 37);
        i0.put(2131362062, 38);
        i0.put(2131363342, 39);
        i0.put(2131361949, 40);
        i0.put(2131361941, 41);
    }
    */

    @DexIgnore
    public r25(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 42, h0, i0));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.g0 = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.g0 != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.g0 = 1;
        }
        g();
    }

    @DexIgnore
    public r25(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (AppBarLayout) objArr[12], (FlexibleTextView) objArr[41], (FlexibleTextView) objArr[40], (ConstraintLayout) objArr[29], (CoordinatorLayout) objArr[11], (ConstraintLayout) objArr[38], (ConstraintLayout) objArr[26], (ConstraintLayout) objArr[23], (ConstraintLayout) objArr[19], (ConstraintLayout) objArr[8], (ConstraintLayout) objArr[3], (ConstraintLayout) objArr[14], (CollapsingToolbarLayout) objArr[13], (FlexibleButton) objArr[22], (FlexibleFitnessTab) objArr[31], (FlexibleFitnessTab) objArr[30], (FlexibleFitnessTab) objArr[32], (FlexibleFitnessTab) objArr[35], (FlexibleFitnessTab) objArr[33], (FlexibleFitnessTab) objArr[34], (FlexibleTextView) objArr[21], (FlexibleTextView) objArr[28], (FlexibleTextView) objArr[27], (ImageView) objArr[2], (RTLImageView) objArr[1], (NestedScrollView) objArr[25], (FlexibleProgressBar) objArr[4], (ConstraintLayout) objArr[0], (RingProgressBar) objArr[16], (RingProgressBar) objArr[15], (RingProgressBar) objArr[17], (RingProgressBar) objArr[18], (ViewPager2) objArr[37], (CustomSwipeRefreshLayout) objArr[7], (FlexibleProgressBar) objArr[10], (FlexibleTextView) objArr[39], (FlexibleTextView) objArr[24], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[5], (View) objArr[9], (View) objArr[20], (View) objArr[36]);
        this.g0 = -1;
        ((q25) this).R.setTag(null);
        a(view);
        f();
    }
}
