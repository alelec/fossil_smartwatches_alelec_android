package com.fossil;

import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gy2 extends dy2<FieldDescriptorType, Object> {
    @DexIgnore
    public gy2(int i) {
        super(i, null);
    }

    @DexIgnore
    @Override // com.fossil.dy2
    public final void a() {
        if (!b()) {
            for (int i = 0; i < c(); i++) {
                Map.Entry a = a(i);
                if (((sv2) a.getKey()).zzd()) {
                    a.setValue(Collections.unmodifiableList((List) a.getValue()));
                }
            }
            for (Map.Entry entry : d()) {
                if (((sv2) entry.getKey()).zzd()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.a();
    }
}
