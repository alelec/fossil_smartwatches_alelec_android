package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xt4 {
    @DexIgnore
    public /* final */ int a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ m95 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(m95 m95) {
            super(m95.d());
            ee7.b(m95, "binding");
            this.a = m95;
        }

        @DexIgnore
        public final void a(String str) {
            ee7.b(str, "name");
            FlexibleTextView flexibleTextView = this.a.q;
            ee7.a((Object) flexibleTextView, "binding.ftvTitle");
            flexibleTextView.setText(str);
            String b = eh5.l.a().b("primaryText");
            if (b != null) {
                this.a.q.setBackgroundColor(Color.parseColor(b));
            }
        }
    }

    @DexIgnore
    public xt4(int i) {
        this.a = i;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public boolean a(List<? extends Object> list, int i) {
        ee7.b(list, "items");
        return list.get(i) instanceof String;
    }

    @DexIgnore
    public RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        ee7.b(viewGroup, "parent");
        m95 a2 = m95.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemHeaderMemberBinding.\u2026(inflater, parent, false)");
        return new a(a2);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r1v3, resolved type: java.lang.String */
    /* JADX WARN: Multi-variable type inference failed */
    public void a(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        ee7.b(list, "items");
        ee7.b(viewHolder, "holder");
        String str = null;
        if (!(viewHolder instanceof a)) {
            viewHolder = null;
        }
        a aVar = (a) viewHolder;
        Object obj = list.get(i);
        if (obj instanceof String) {
            str = obj;
        }
        String str2 = str;
        if (aVar != null && str2 != null) {
            aVar.a(str2);
        }
    }
}
