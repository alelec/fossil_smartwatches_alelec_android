package com.fossil;

import com.fossil.ql4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cz5 extends ql4<b, c, ql4.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ql4.b {
        @DexIgnore
        public /* final */ List<bt5> a;
        @DexIgnore
        public /* final */ List<at5> b;

        @DexIgnore
        public b(List<bt5> list, List<at5> list2) {
            ee7.b(list, "contactWrapperList");
            ee7.b(list2, "appWrapperList");
            this.a = list;
            this.b = list2;
        }

        @DexIgnore
        public final List<at5> a() {
            return this.b;
        }

        @DexIgnore
        public final List<bt5> b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ql4.c {
        @DexIgnore
        public c(boolean z) {
        }
    }

    /*
    static {
        new a(null);
        String simpleName = cz5.class.getSimpleName();
        ee7.a((Object) simpleName, "SaveAllHybridNotification::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public cz5(NotificationsRepository notificationsRepository) {
        ee7.b(notificationsRepository, "mNotificationsRepository");
        this.d = notificationsRepository;
    }

    @DexIgnore
    public final void b(List<bt5> list) {
        if (!list.isEmpty()) {
            ContactProvider b2 = ah5.p.a().b();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            for (bt5 bt5 : list) {
                Contact contact = bt5.getContact();
                if (contact != null) {
                    Contact contact2 = b2.getContact(contact.getDbRowId());
                    if (contact2 != null) {
                        arrayList2.add(contact2);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = e;
                        local.d(str, "Removed contact=" + contact2.getFirstName() + ", rowId=" + contact2.getDbRowId());
                        List<ContactGroup> allContactGroups = b2.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
                        ArrayList arrayList4 = new ArrayList();
                        for (ContactGroup contactGroup : allContactGroups) {
                            ee7.a((Object) contactGroup, "contactGroupItem");
                            Iterator<Contact> it = contactGroup.getContacts().iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                Contact next = it.next();
                                int contactId = contact2.getContactId();
                                ee7.a((Object) next, "contactItem");
                                if (contactId == next.getContactId()) {
                                    contact2.setDbRowId(next.getDbRowId());
                                    arrayList.add(contactGroup);
                                    arrayList4.addAll(next.getPhoneNumbers());
                                    break;
                                }
                            }
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = e;
                        local2.d(str2, "Save phone numbers " + arrayList4 + " of contact " + contact2.getDisplayName());
                        Iterator it2 = arrayList4.iterator();
                        while (it2.hasNext()) {
                            PhoneNumber phoneNumber = (PhoneNumber) it2.next();
                            ee7.a((Object) phoneNumber, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
                            arrayList3.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
                        }
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            this.d.removeListContact(arrayList2);
            this.d.removeContactGroupList(arrayList);
            c(arrayList3);
        }
    }

    @DexIgnore
    public final void c(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact phoneFavoritesContact : list) {
            this.d.removePhoneFavoritesContact(phoneFavoritesContact);
        }
    }

    @DexIgnore
    public final void d(List<at5> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (at5 at5 : list) {
                AppFilter appFilter = new AppFilter();
                appFilter.setHour(at5.getCurrentHandGroup());
                InstalledApp installedApp = at5.getInstalledApp();
                String str = null;
                appFilter.setType(installedApp != null ? installedApp.getIdentifier() : null);
                appFilter.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = e;
                StringBuilder sb = new StringBuilder();
                sb.append("Saved App: name=");
                InstalledApp installedApp2 = at5.getInstalledApp();
                if (installedApp2 != null) {
                    str = installedApp2.getIdentifier();
                }
                sb.append(str);
                sb.append(", hour=");
                sb.append(at5.getCurrentHandGroup());
                local.d(str2, sb.toString());
                arrayList.add(appFilter);
            }
            this.d.saveListAppFilters(arrayList);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:106:0x028d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x017d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void e(java.util.List<com.fossil.bt5> r18) {
        /*
            r17 = this;
            r1 = r17
            boolean r0 = r18.isEmpty()
            if (r0 == 0) goto L_0x0009
            return
        L_0x0009:
            com.portfolio.platform.data.source.NotificationsRepository r0 = r1.d
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r2 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_SAM
            int r2 = r2.getValue()
            java.util.List r2 = r0.getAllContactGroups(r2)
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            com.fossil.ah5$a r0 = com.fossil.ah5.p
            com.fossil.ah5 r0 = r0.a()
            com.fossil.wearables.fsl.contact.ContactProvider r7 = r0.b()
            java.util.Iterator r8 = r18.iterator()
        L_0x0037:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x0292
            java.lang.Object r0 = r8.next()
            com.fossil.bt5 r0 = (com.fossil.bt5) r0
            if (r2 == 0) goto L_0x015d
            java.util.Iterator r11 = r2.iterator()
            r12 = 0
        L_0x004a:
            boolean r13 = r11.hasNext()
            if (r13 == 0) goto L_0x015d
            java.lang.Object r13 = r11.next()
            com.fossil.wearables.fsl.contact.ContactGroup r13 = (com.fossil.wearables.fsl.contact.ContactGroup) r13
            java.lang.String r14 = "contactGroup"
            com.fossil.ee7.a(r13, r14)
            java.util.List r14 = r13.getContacts()
            java.util.Iterator r14 = r14.iterator()
        L_0x0063:
            boolean r15 = r14.hasNext()
            if (r15 == 0) goto L_0x0154
            java.lang.Object r15 = r14.next()
            com.fossil.wearables.fsl.contact.Contact r15 = (com.fossil.wearables.fsl.contact.Contact) r15
            com.fossil.wearables.fsl.contact.Contact r16 = r0.getContact()
            if (r16 == 0) goto L_0x014e
            if (r15 == 0) goto L_0x014e
            int r9 = r15.getContactId()
            com.fossil.wearables.fsl.contact.Contact r16 = r0.getContact()
            if (r16 == 0) goto L_0x014e
            int r10 = r16.getContactId()
            if (r9 != r10) goto L_0x014e
            com.fossil.wearables.fsl.contact.Contact r9 = r0.getContact()
            if (r9 == 0) goto L_0x0096
            boolean r9 = r9.isUseCall()
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)
            goto L_0x0097
        L_0x0096:
            r9 = 0
        L_0x0097:
            if (r9 == 0) goto L_0x0149
            boolean r9 = r9.booleanValue()
            r15.setUseCall(r9)
            com.fossil.wearables.fsl.contact.Contact r9 = r0.getContact()
            if (r9 == 0) goto L_0x00af
            boolean r9 = r9.isUseSms()
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)
            goto L_0x00b0
        L_0x00af:
            r9 = 0
        L_0x00b0:
            if (r9 == 0) goto L_0x0144
            boolean r9 = r9.booleanValue()
            r15.setUseSms(r9)
            com.fossil.wearables.fsl.contact.Contact r9 = r0.getContact()
            if (r9 == 0) goto L_0x00c4
            java.lang.String r9 = r9.getFirstName()
            goto L_0x00c5
        L_0x00c4:
            r9 = 0
        L_0x00c5:
            r15.setFirstName(r9)
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = com.fossil.cz5.e
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r14 = "Contact Id = "
            r12.append(r14)
            com.fossil.wearables.fsl.contact.Contact r14 = r0.getContact()
            if (r14 == 0) goto L_0x00e9
            int r14 = r14.getContactId()
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
            goto L_0x00ea
        L_0x00e9:
            r14 = 0
        L_0x00ea:
            r12.append(r14)
            java.lang.String r14 = ", "
            r12.append(r14)
            r16 = r8
            java.lang.String r8 = "Contact name = "
            r12.append(r8)
            com.fossil.wearables.fsl.contact.Contact r8 = r0.getContact()
            if (r8 == 0) goto L_0x0104
            java.lang.String r8 = r8.getFirstName()
            goto L_0x0105
        L_0x0104:
            r8 = 0
        L_0x0105:
            r12.append(r8)
            r12.append(r14)
            java.lang.String r8 = "Contact db row = "
            r12.append(r8)
            com.fossil.wearables.fsl.contact.Contact r8 = r0.getContact()
            if (r8 == 0) goto L_0x011f
            int r8 = r8.getDbRowId()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            goto L_0x0120
        L_0x011f:
            r8 = 0
        L_0x0120:
            r12.append(r8)
            r12.append(r14)
            java.lang.String r8 = "Contact phone = "
            r12.append(r8)
            java.lang.String r8 = r0.getPhoneNumber()
            r12.append(r8)
            java.lang.String r8 = r12.toString()
            r9.d(r10, r8)
            r0.setContact(r15)
            r7.removeContactGroup(r13)
            r2.remove(r13)
            r12 = 1
            goto L_0x0156
        L_0x0144:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x0149:
            r0 = 0
            com.fossil.ee7.a()
            throw r0
        L_0x014e:
            r16 = r8
            r8 = r16
            goto L_0x0063
        L_0x0154:
            r16 = r8
        L_0x0156:
            if (r12 == 0) goto L_0x0159
            goto L_0x015f
        L_0x0159:
            r8 = r16
            goto L_0x004a
        L_0x015d:
            r16 = r8
        L_0x015f:
            com.fossil.wearables.fsl.contact.ContactGroup r8 = new com.fossil.wearables.fsl.contact.ContactGroup
            r8.<init>()
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r9 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_SAM
            int r9 = r9.getValue()
            r8.setDeviceFamily(r9)
            int r9 = r0.getCurrentHandGroup()
            r8.setHour(r9)
            r4.add(r8)
            com.fossil.wearables.fsl.contact.Contact r9 = r0.getContact()
            if (r9 == 0) goto L_0x028d
            r9.setContactGroup(r8)
            com.fossil.wearables.fsl.contact.Contact r8 = r0.getContact()
            if (r8 == 0) goto L_0x018f
            boolean r8 = r8.isUseCall()
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)
            goto L_0x0190
        L_0x018f:
            r8 = 0
        L_0x0190:
            if (r8 == 0) goto L_0x0288
            boolean r8 = r8.booleanValue()
            r9.setUseCall(r8)
            com.fossil.wearables.fsl.contact.Contact r8 = r0.getContact()
            if (r8 == 0) goto L_0x01a8
            boolean r8 = r8.isUseSms()
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)
            goto L_0x01a9
        L_0x01a8:
            r8 = 0
        L_0x01a9:
            if (r8 == 0) goto L_0x0283
            boolean r8 = r8.booleanValue()
            r9.setUseSms(r8)
            r8 = 0
            r9.setUseEmail(r8)
            r3.add(r9)
            com.portfolio.platform.PortfolioApp$a r8 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r8 = r8.c()
            android.content.ContentResolver r10 = r8.getContentResolver()
            boolean r8 = r0.hasPhoneNumber()
            if (r8 == 0) goto L_0x027f
            android.net.Uri r11 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI
            java.lang.String r8 = "data1"
            java.lang.String[] r12 = new java.lang.String[]{r8}
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "contact_id="
            r13.append(r14)
            int r14 = r9.getContactId()
            r13.append(r14)
            java.lang.String r13 = r13.toString()
            r14 = 0
            r15 = 0
            android.database.Cursor r10 = r10.query(r11, r12, r13, r14, r15)
            if (r10 == 0) goto L_0x024f
        L_0x01ee:
            boolean r11 = r10.moveToNext()     // Catch:{ Exception -> 0x0223 }
            if (r11 == 0) goto L_0x021d
            com.fossil.wearables.fsl.contact.PhoneNumber r11 = new com.fossil.wearables.fsl.contact.PhoneNumber     // Catch:{ Exception -> 0x0223 }
            r11.<init>()     // Catch:{ Exception -> 0x0223 }
            int r12 = r10.getColumnIndex(r8)     // Catch:{ Exception -> 0x0223 }
            java.lang.String r12 = r10.getString(r12)     // Catch:{ Exception -> 0x0223 }
            r11.setNumber(r12)     // Catch:{ Exception -> 0x0223 }
            r11.setContact(r9)     // Catch:{ Exception -> 0x0223 }
            r5.add(r11)     // Catch:{ Exception -> 0x0223 }
            boolean r12 = r0.isFavorites()     // Catch:{ Exception -> 0x0223 }
            if (r12 == 0) goto L_0x01ee
            com.portfolio.platform.data.model.PhoneFavoritesContact r12 = new com.portfolio.platform.data.model.PhoneFavoritesContact     // Catch:{ Exception -> 0x0223 }
            java.lang.String r11 = r11.getNumber()     // Catch:{ Exception -> 0x0223 }
            r12.<init>(r11)     // Catch:{ Exception -> 0x0223 }
            r6.add(r12)     // Catch:{ Exception -> 0x0223 }
            goto L_0x01ee
        L_0x021d:
            r10.close()
            goto L_0x024f
        L_0x0221:
            r0 = move-exception
            goto L_0x024b
        L_0x0223:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0221 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()     // Catch:{ all -> 0x0221 }
            java.lang.String r11 = com.fossil.cz5.e     // Catch:{ all -> 0x0221 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x0221 }
            r12.<init>()     // Catch:{ all -> 0x0221 }
            java.lang.String r13 = "Error Inside "
            r12.append(r13)     // Catch:{ all -> 0x0221 }
            java.lang.String r13 = com.fossil.cz5.e     // Catch:{ all -> 0x0221 }
            r12.append(r13)     // Catch:{ all -> 0x0221 }
            java.lang.String r13 = ".saveContactToFSL - ex="
            r12.append(r13)     // Catch:{ all -> 0x0221 }
            r12.append(r0)     // Catch:{ all -> 0x0221 }
            java.lang.String r0 = r12.toString()     // Catch:{ all -> 0x0221 }
            r8.e(r11, r0)     // Catch:{ all -> 0x0221 }
            goto L_0x021d
        L_0x024b:
            r10.close()
            throw r0
        L_0x024f:
            int r0 = r9.getContactId()
            r8 = -100
            if (r0 != r8) goto L_0x0267
            com.fossil.wearables.fsl.contact.PhoneNumber r0 = new com.fossil.wearables.fsl.contact.PhoneNumber
            r0.<init>()
            java.lang.String r8 = "-1234"
            r0.setNumber(r8)
            r0.setContact(r9)
            r5.add(r0)
        L_0x0267:
            int r0 = r9.getContactId()
            r8 = -200(0xffffffffffffff38, float:NaN)
            if (r0 != r8) goto L_0x027f
            com.fossil.wearables.fsl.contact.PhoneNumber r0 = new com.fossil.wearables.fsl.contact.PhoneNumber
            r0.<init>()
            java.lang.String r8 = "-5678"
            r0.setNumber(r8)
            r0.setContact(r9)
            r5.add(r0)
        L_0x027f:
            r8 = r16
            goto L_0x0037
        L_0x0283:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x0288:
            r0 = 0
            com.fossil.ee7.a()
            throw r0
        L_0x028d:
            r0 = 0
            com.fossil.ee7.a()
            throw r0
        L_0x0292:
            com.portfolio.platform.data.source.NotificationsRepository r0 = r1.d
            r0.saveContactGroupList(r4)
            com.portfolio.platform.data.source.NotificationsRepository r0 = r1.d
            r0.saveListContact(r3)
            com.portfolio.platform.data.source.NotificationsRepository r0 = r1.d
            r0.saveListPhoneNumber(r5)
            r1.f(r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cz5.e(java.util.List):void");
    }

    @DexIgnore
    public final void f(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact phoneFavoritesContact : list) {
            this.d.savePhoneFavoritesContact(phoneFavoritesContact);
        }
    }

    @DexIgnore
    public void a(b bVar) {
        ee7.b(bVar, "requestValues");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        for (bt5 bt5 : bVar.b()) {
            if (!bt5.isAdded()) {
                arrayList2.add(bt5);
            } else {
                arrayList.add(bt5);
            }
        }
        b(arrayList2);
        e(arrayList);
        for (at5 at5 : bVar.a()) {
            InstalledApp installedApp = at5.getInstalledApp();
            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
            if (isSelected == null) {
                ee7.a();
                throw null;
            } else if (isSelected.booleanValue()) {
                arrayList3.add(at5);
            } else {
                arrayList4.add(at5);
            }
        }
        a(arrayList4);
        d(arrayList3);
        FLogger.INSTANCE.getLocal().d(e, "Inside .SaveAllHybridNotification done");
        a().onSuccess(new c(true));
    }

    @DexIgnore
    public final void a(List<at5> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (at5 at5 : list) {
                AppFilter appFilter = new AppFilter();
                InstalledApp installedApp = at5.getInstalledApp();
                Integer num = null;
                Integer valueOf = installedApp != null ? Integer.valueOf(installedApp.getDbRowId()) : null;
                if (valueOf != null) {
                    appFilter.setDbRowId(valueOf.intValue());
                    appFilter.setHour(at5.getCurrentHandGroup());
                    InstalledApp installedApp2 = at5.getInstalledApp();
                    appFilter.setType(installedApp2 != null ? installedApp2.getIdentifier() : null);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = e;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Removed App: name=");
                    InstalledApp installedApp3 = at5.getInstalledApp();
                    sb.append(installedApp3 != null ? installedApp3.getIdentifier() : null);
                    sb.append(", rowId=");
                    InstalledApp installedApp4 = at5.getInstalledApp();
                    if (installedApp4 != null) {
                        num = Integer.valueOf(installedApp4.getDbRowId());
                    }
                    sb.append(num);
                    sb.append(", hour=");
                    sb.append(at5.getCurrentHandGroup());
                    local.d(str, sb.toString());
                    arrayList.add(appFilter);
                } else {
                    ee7.a();
                    throw null;
                }
            }
            this.d.removeListAppFilter(arrayList);
        }
    }
}
