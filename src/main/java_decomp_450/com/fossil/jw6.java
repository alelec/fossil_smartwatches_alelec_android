package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jw6 extends fl4<b, d, c> {
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            ee7.b(str, Constants.EMAIL);
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(int i, String str) {
            ee7.b(str, "errorMesagge");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.usecase.RequestEmailOtp", f = "RequestEmailOtp.kt", l = {21}, m = "run")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ jw6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(jw6 jw6, fb7 fb7) {
            super(fb7);
            this.this$0 = jw6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public jw6(UserRepository userRepository) {
        ee7.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "RequestEmailOtp";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.jw6.b r7, com.fossil.fb7<java.lang.Object> r8) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof com.fossil.jw6.e
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.jw6$e r0 = (com.fossil.jw6.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.jw6$e r0 = new com.fossil.jw6$e
            r0.<init>(r6, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 600(0x258, float:8.41E-43)
            r4 = 1
            java.lang.String r5 = ""
            if (r2 == 0) goto L_0x003d
            if (r2 != r4) goto L_0x0035
            java.lang.Object r7 = r0.L$1
            com.fossil.jw6$b r7 = (com.fossil.jw6.b) r7
            java.lang.Object r7 = r0.L$0
            com.fossil.jw6 r7 = (com.fossil.jw6) r7
            com.fossil.t87.a(r8)
            goto L_0x005b
        L_0x0035:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L_0x003d:
            com.fossil.t87.a(r8)
            if (r7 != 0) goto L_0x0048
            com.fossil.jw6$c r7 = new com.fossil.jw6$c
            r7.<init>(r3, r5)
            return r7
        L_0x0048:
            com.portfolio.platform.data.source.UserRepository r8 = r6.d
            java.lang.String r2 = r7.a()
            r0.L$0 = r6
            r0.L$1 = r7
            r0.label = r4
            java.lang.Object r8 = r8.requestEmailOtp(r2, r0)
            if (r8 != r1) goto L_0x005b
            return r1
        L_0x005b:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r7 = r8 instanceof com.fossil.bj5
            java.lang.String r0 = "RequestEmailOtp"
            if (r7 == 0) goto L_0x0074
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r8 = "request OTP success"
            r7.d(r0, r8)
            com.fossil.jw6$d r7 = new com.fossil.jw6$d
            r7.<init>()
            goto L_0x00a7
        L_0x0074:
            boolean r7 = r8 instanceof com.fossil.yi5
            if (r7 == 0) goto L_0x00a2
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "request OTP failed "
            r1.append(r2)
            com.fossil.yi5 r8 = (com.fossil.yi5) r8
            com.portfolio.platform.data.model.ServerError r2 = r8.c()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r7.d(r0, r1)
            com.fossil.jw6$c r7 = new com.fossil.jw6$c
            int r8 = r8.a()
            r7.<init>(r8, r5)
            goto L_0x00a7
        L_0x00a2:
            com.fossil.jw6$c r7 = new com.fossil.jw6$c
            r7.<init>(r3, r5)
        L_0x00a7:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jw6.a(com.fossil.jw6$b, com.fossil.fb7):java.lang.Object");
    }
}
