package com.fossil;

import android.app.Activity;
import android.app.ActivityOptions;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends h6 {
        @DexIgnore
        public /* final */ ActivityOptions a;

        @DexIgnore
        public a(ActivityOptions activityOptions) {
            this.a = activityOptions;
        }

        @DexIgnore
        @Override // com.fossil.h6
        public Bundle a() {
            return this.a.toBundle();
        }
    }

    @DexIgnore
    public static h6 a(Activity activity, a9<View, String>... a9VarArr) {
        if (Build.VERSION.SDK_INT < 21) {
            return new h6();
        }
        Pair[] pairArr = null;
        if (a9VarArr != null) {
            pairArr = new Pair[a9VarArr.length];
            for (int i = 0; i < a9VarArr.length; i++) {
                pairArr[i] = Pair.create(a9VarArr[i].a, a9VarArr[i].b);
            }
        }
        return new a(ActivityOptions.makeSceneTransitionAnimation(activity, pairArr));
    }

    @DexIgnore
    public Bundle a() {
        return null;
    }
}
