package com.fossil;

import com.fossil.s87;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mi7 {
    @DexIgnore
    public static final <T> Object a(Object obj) {
        Throwable r0 = s87.m63exceptionOrNullimpl(obj);
        return r0 == null ? obj : new li7(r0, false, 2, null);
    }

    @DexIgnore
    public static final <T> Object a(Object obj, ai7<?> ai7) {
        Throwable r0 = s87.m63exceptionOrNullimpl(obj);
        if (r0 == null) {
            return obj;
        }
        if (dj7.d() && (ai7 instanceof sb7)) {
            r0 = km7.b(r0, (sb7) ai7);
        }
        return new li7(r0, false, 2, null);
    }

    @DexIgnore
    public static final <T> Object a(Object obj, fb7<? super T> fb7) {
        if (obj instanceof li7) {
            s87.a aVar = s87.Companion;
            Throwable th = ((li7) obj).a;
            if (dj7.d() && (fb7 instanceof sb7)) {
                th = km7.b(th, (sb7) fb7);
            }
            return s87.m60constructorimpl(t87.a(th));
        }
        s87.a aVar2 = s87.Companion;
        return s87.m60constructorimpl(obj);
    }
}
