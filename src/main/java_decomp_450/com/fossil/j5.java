package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.h5;
import com.fossil.i5;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j5 extends t5 {
    @DexIgnore
    public int A0; // = 0;
    @DexIgnore
    public int B0; // = 0;
    @DexIgnore
    public int C0; // = 7;
    @DexIgnore
    public boolean D0; // = false;
    @DexIgnore
    public boolean E0; // = false;
    @DexIgnore
    public boolean F0; // = false;
    @DexIgnore
    public boolean l0; // = false;
    @DexIgnore
    public y4 m0; // = new y4();
    @DexIgnore
    public s5 n0;
    @DexIgnore
    public int o0;
    @DexIgnore
    public int p0;
    @DexIgnore
    public int q0;
    @DexIgnore
    public int r0;
    @DexIgnore
    public int s0; // = 0;
    @DexIgnore
    public int t0; // = 0;
    @DexIgnore
    public g5[] u0; // = new g5[4];
    @DexIgnore
    public g5[] v0; // = new g5[4];
    @DexIgnore
    public List<k5> w0; // = new ArrayList();
    @DexIgnore
    public boolean x0; // = false;
    @DexIgnore
    public boolean y0; // = false;
    @DexIgnore
    public boolean z0; // = false;

    @DexIgnore
    @Override // com.fossil.t5, com.fossil.i5
    public void E() {
        this.m0.i();
        this.o0 = 0;
        this.q0 = 0;
        this.p0 = 0;
        this.r0 = 0;
        this.w0.clear();
        this.D0 = false;
        super.E();
    }

    @DexIgnore
    /* JADX WARN: Type inference failed for: r8v17, types: [boolean] */
    /* JADX WARN: Type inference failed for: r8v21 */
    /* JADX WARN: Type inference failed for: r8v22 */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0263  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0280  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x028d  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0292  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0186  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x018f  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01da  */
    @Override // com.fossil.t5
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void L() {
        /*
            r21 = this;
            r1 = r21
            int r2 = r1.I
            int r3 = r1.J
            int r0 = r21.t()
            r4 = 0
            int r5 = java.lang.Math.max(r4, r0)
            int r0 = r21.j()
            int r6 = java.lang.Math.max(r4, r0)
            r1.E0 = r4
            r1.F0 = r4
            com.fossil.i5 r0 = r1.D
            if (r0 == 0) goto L_0x0046
            com.fossil.s5 r0 = r1.n0
            if (r0 != 0) goto L_0x002a
            com.fossil.s5 r0 = new com.fossil.s5
            r0.<init>(r1)
            r1.n0 = r0
        L_0x002a:
            com.fossil.s5 r0 = r1.n0
            r0.b(r1)
            int r0 = r1.o0
            r1.s(r0)
            int r0 = r1.p0
            r1.t(r0)
            r21.F()
            com.fossil.y4 r0 = r1.m0
            com.fossil.w4 r0 = r0.e()
            r1.a(r0)
            goto L_0x004a
        L_0x0046:
            r1.I = r4
            r1.J = r4
        L_0x004a:
            int r0 = r1.C0
            r7 = 32
            r8 = 8
            r9 = 1
            if (r0 == 0) goto L_0x006a
            boolean r0 = r1.u(r8)
            if (r0 != 0) goto L_0x005c
            r21.T()
        L_0x005c:
            boolean r0 = r1.u(r7)
            if (r0 != 0) goto L_0x0065
            r21.S()
        L_0x0065:
            com.fossil.y4 r0 = r1.m0
            r0.g = r9
            goto L_0x006e
        L_0x006a:
            com.fossil.y4 r0 = r1.m0
            r0.g = r4
        L_0x006e:
            com.fossil.i5$b[] r0 = r1.C
            r10 = r0[r9]
            r11 = r0[r4]
            r21.V()
            java.util.List<com.fossil.k5> r0 = r1.w0
            int r0 = r0.size()
            if (r0 != 0) goto L_0x0090
            java.util.List<com.fossil.k5> r0 = r1.w0
            r0.clear()
            java.util.List<com.fossil.k5> r0 = r1.w0
            com.fossil.k5 r12 = new com.fossil.k5
            java.util.ArrayList<com.fossil.i5> r13 = r1.k0
            r12.<init>(r13)
            r0.add(r4, r12)
        L_0x0090:
            java.util.List<com.fossil.k5> r0 = r1.w0
            int r12 = r0.size()
            java.util.ArrayList<com.fossil.i5> r13 = r1.k0
            com.fossil.i5$b r0 = r21.k()
            com.fossil.i5$b r14 = com.fossil.i5.b.WRAP_CONTENT
            if (r0 == r14) goto L_0x00ab
            com.fossil.i5$b r0 = r21.r()
            com.fossil.i5$b r14 = com.fossil.i5.b.WRAP_CONTENT
            if (r0 != r14) goto L_0x00a9
            goto L_0x00ab
        L_0x00a9:
            r14 = 0
            goto L_0x00ac
        L_0x00ab:
            r14 = 1
        L_0x00ac:
            r0 = 0
            r15 = 0
        L_0x00ae:
            if (r15 >= r12) goto L_0x02f4
            boolean r8 = r1.D0
            if (r8 != 0) goto L_0x02f4
            java.util.List<com.fossil.k5> r8 = r1.w0
            java.lang.Object r8 = r8.get(r15)
            com.fossil.k5 r8 = (com.fossil.k5) r8
            boolean r8 = r8.d
            if (r8 == 0) goto L_0x00c4
            r19 = r12
            goto L_0x02e8
        L_0x00c4:
            boolean r8 = r1.u(r7)
            if (r8 == 0) goto L_0x00f9
            com.fossil.i5$b r8 = r21.k()
            com.fossil.i5$b r7 = com.fossil.i5.b.FIXED
            if (r8 != r7) goto L_0x00eb
            com.fossil.i5$b r7 = r21.r()
            com.fossil.i5$b r8 = com.fossil.i5.b.FIXED
            if (r7 != r8) goto L_0x00eb
            java.util.List<com.fossil.k5> r7 = r1.w0
            java.lang.Object r7 = r7.get(r15)
            com.fossil.k5 r7 = (com.fossil.k5) r7
            java.util.List r7 = r7.a()
            java.util.ArrayList r7 = (java.util.ArrayList) r7
            r1.k0 = r7
            goto L_0x00f9
        L_0x00eb:
            java.util.List<com.fossil.k5> r7 = r1.w0
            java.lang.Object r7 = r7.get(r15)
            com.fossil.k5 r7 = (com.fossil.k5) r7
            java.util.List<com.fossil.i5> r7 = r7.a
            java.util.ArrayList r7 = (java.util.ArrayList) r7
            r1.k0 = r7
        L_0x00f9:
            r21.V()
            java.util.ArrayList<com.fossil.i5> r7 = r1.k0
            int r7 = r7.size()
            r8 = 0
        L_0x0103:
            if (r8 >= r7) goto L_0x011b
            java.util.ArrayList<com.fossil.i5> r4 = r1.k0
            java.lang.Object r4 = r4.get(r8)
            com.fossil.i5 r4 = (com.fossil.i5) r4
            boolean r9 = r4 instanceof com.fossil.t5
            if (r9 == 0) goto L_0x0116
            com.fossil.t5 r4 = (com.fossil.t5) r4
            r4.L()
        L_0x0116:
            int r8 = r8 + 1
            r4 = 0
            r9 = 1
            goto L_0x0103
        L_0x011b:
            r4 = r0
            r0 = 0
            r8 = 1
        L_0x011e:
            if (r8 == 0) goto L_0x02d7
            r17 = r4
            r9 = 1
            int r4 = r0 + 1
            com.fossil.y4 r0 = r1.m0     // Catch:{ Exception -> 0x0162 }
            r0.i()     // Catch:{ Exception -> 0x0162 }
            r21.V()     // Catch:{ Exception -> 0x0162 }
            com.fossil.y4 r0 = r1.m0     // Catch:{ Exception -> 0x0162 }
            r1.b(r0)     // Catch:{ Exception -> 0x0162 }
            r0 = 0
        L_0x0133:
            if (r0 >= r7) goto L_0x0149
            java.util.ArrayList<com.fossil.i5> r9 = r1.k0     // Catch:{ Exception -> 0x0162 }
            java.lang.Object r9 = r9.get(r0)     // Catch:{ Exception -> 0x0162 }
            com.fossil.i5 r9 = (com.fossil.i5) r9     // Catch:{ Exception -> 0x0162 }
            r18 = r8
            com.fossil.y4 r8 = r1.m0     // Catch:{ Exception -> 0x015e }
            r9.b(r8)     // Catch:{ Exception -> 0x015e }
            int r0 = r0 + 1
            r8 = r18
            goto L_0x0133
        L_0x0149:
            r18 = r8
            com.fossil.y4 r0 = r1.m0     // Catch:{ Exception -> 0x015e }
            boolean r8 = r1.d(r0)     // Catch:{ Exception -> 0x015e }
            if (r8 == 0) goto L_0x015b
            com.fossil.y4 r0 = r1.m0     // Catch:{ Exception -> 0x0159 }
            r0.g()     // Catch:{ Exception -> 0x0159 }
            goto L_0x015b
        L_0x0159:
            r0 = move-exception
            goto L_0x0165
        L_0x015b:
            r19 = r12
            goto L_0x0184
        L_0x015e:
            r0 = move-exception
            r8 = r18
            goto L_0x0165
        L_0x0162:
            r0 = move-exception
            r18 = r8
        L_0x0165:
            r0.printStackTrace()
            java.io.PrintStream r9 = java.lang.System.out
            r18 = r8
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r19 = r12
            java.lang.String r12 = "EXCEPTION : "
            r8.append(r12)
            r8.append(r0)
            java.lang.String r0 = r8.toString()
            r9.println(r0)
            r8 = r18
        L_0x0184:
            if (r8 == 0) goto L_0x018f
            com.fossil.y4 r8 = r1.m0
            boolean[] r9 = com.fossil.n5.a
            r1.a(r8, r9)
        L_0x018d:
            r9 = 2
            goto L_0x01d8
        L_0x018f:
            com.fossil.y4 r8 = r1.m0
            r1.c(r8)
            r8 = 0
        L_0x0195:
            if (r8 >= r7) goto L_0x018d
            java.util.ArrayList<com.fossil.i5> r9 = r1.k0
            java.lang.Object r9 = r9.get(r8)
            com.fossil.i5 r9 = (com.fossil.i5) r9
            com.fossil.i5$b[] r12 = r9.C
            r16 = 0
            r12 = r12[r16]
            com.fossil.i5$b r0 = com.fossil.i5.b.MATCH_CONSTRAINT
            if (r12 != r0) goto L_0x01ba
            int r0 = r9.t()
            int r12 = r9.v()
            if (r0 >= r12) goto L_0x01ba
            boolean[] r0 = com.fossil.n5.a
            r8 = 2
            r12 = 1
            r0[r8] = r12
            goto L_0x018d
        L_0x01ba:
            r12 = 1
            com.fossil.i5$b[] r0 = r9.C
            r0 = r0[r12]
            com.fossil.i5$b r12 = com.fossil.i5.b.MATCH_CONSTRAINT
            if (r0 != r12) goto L_0x01d4
            int r0 = r9.j()
            int r9 = r9.u()
            if (r0 >= r9) goto L_0x01d4
            boolean[] r0 = com.fossil.n5.a
            r8 = 1
            r9 = 2
            r0[r9] = r8
            goto L_0x01d8
        L_0x01d4:
            r9 = 2
            int r8 = r8 + 1
            goto L_0x0195
        L_0x01d8:
            if (r14 == 0) goto L_0x0250
            r8 = 8
            if (r4 >= r8) goto L_0x0250
            boolean[] r0 = com.fossil.n5.a
            boolean r0 = r0[r9]
            if (r0 == 0) goto L_0x0250
            r0 = 0
            r9 = 0
            r12 = 0
        L_0x01e7:
            if (r0 >= r7) goto L_0x0211
            java.util.ArrayList<com.fossil.i5> r8 = r1.k0
            java.lang.Object r8 = r8.get(r0)
            com.fossil.i5 r8 = (com.fossil.i5) r8
            r18 = r4
            int r4 = r8.I
            int r20 = r8.t()
            int r4 = r4 + r20
            int r9 = java.lang.Math.max(r9, r4)
            int r4 = r8.J
            int r8 = r8.j()
            int r4 = r4 + r8
            int r12 = java.lang.Math.max(r12, r4)
            int r0 = r0 + 1
            r4 = r18
            r8 = 8
            goto L_0x01e7
        L_0x0211:
            r18 = r4
            int r0 = r1.R
            int r0 = java.lang.Math.max(r0, r9)
            int r4 = r1.S
            int r4 = java.lang.Math.max(r4, r12)
            com.fossil.i5$b r8 = com.fossil.i5.b.WRAP_CONTENT
            if (r11 != r8) goto L_0x0237
            int r8 = r21.t()
            if (r8 >= r0) goto L_0x0237
            r1.p(r0)
            com.fossil.i5$b[] r0 = r1.C
            com.fossil.i5$b r8 = com.fossil.i5.b.WRAP_CONTENT
            r9 = 0
            r0[r9] = r8
            r0 = 1
            r17 = 1
            goto L_0x0238
        L_0x0237:
            r0 = 0
        L_0x0238:
            com.fossil.i5$b r8 = com.fossil.i5.b.WRAP_CONTENT
            if (r10 != r8) goto L_0x0253
            int r8 = r21.j()
            if (r8 >= r4) goto L_0x0253
            r1.h(r4)
            com.fossil.i5$b[] r0 = r1.C
            com.fossil.i5$b r4 = com.fossil.i5.b.WRAP_CONTENT
            r8 = 1
            r0[r8] = r4
            r0 = 1
            r17 = 1
            goto L_0x0253
        L_0x0250:
            r18 = r4
            r0 = 0
        L_0x0253:
            int r4 = r1.R
            int r8 = r21.t()
            int r4 = java.lang.Math.max(r4, r8)
            int r8 = r21.t()
            if (r4 <= r8) goto L_0x0270
            r1.p(r4)
            com.fossil.i5$b[] r0 = r1.C
            com.fossil.i5$b r4 = com.fossil.i5.b.FIXED
            r8 = 0
            r0[r8] = r4
            r0 = 1
            r17 = 1
        L_0x0270:
            int r4 = r1.S
            int r8 = r21.j()
            int r4 = java.lang.Math.max(r4, r8)
            int r8 = r21.j()
            if (r4 <= r8) goto L_0x028d
            r1.h(r4)
            com.fossil.i5$b[] r0 = r1.C
            com.fossil.i5$b r4 = com.fossil.i5.b.FIXED
            r8 = 1
            r0[r8] = r4
            r0 = 1
            r9 = 1
            goto L_0x0290
        L_0x028d:
            r8 = 1
            r9 = r17
        L_0x0290:
            if (r9 != 0) goto L_0x02cf
            com.fossil.i5$b[] r4 = r1.C
            r12 = 0
            r4 = r4[r12]
            com.fossil.i5$b r12 = com.fossil.i5.b.WRAP_CONTENT
            if (r4 != r12) goto L_0x02b1
            if (r5 <= 0) goto L_0x02b1
            int r4 = r21.t()
            if (r4 <= r5) goto L_0x02b1
            r1.E0 = r8
            com.fossil.i5$b[] r0 = r1.C
            com.fossil.i5$b r4 = com.fossil.i5.b.FIXED
            r9 = 0
            r0[r9] = r4
            r1.p(r5)
            r0 = 1
            r9 = 1
        L_0x02b1:
            com.fossil.i5$b[] r4 = r1.C
            r4 = r4[r8]
            com.fossil.i5$b r12 = com.fossil.i5.b.WRAP_CONTENT
            if (r4 != r12) goto L_0x02cf
            if (r6 <= 0) goto L_0x02cf
            int r4 = r21.j()
            if (r4 <= r6) goto L_0x02cf
            r1.F0 = r8
            com.fossil.i5$b[] r0 = r1.C
            com.fossil.i5$b r4 = com.fossil.i5.b.FIXED
            r0[r8] = r4
            r1.h(r6)
            r4 = 1
            r8 = 1
            goto L_0x02d1
        L_0x02cf:
            r8 = r0
            r4 = r9
        L_0x02d1:
            r0 = r18
            r12 = r19
            goto L_0x011e
        L_0x02d7:
            r17 = r4
            r19 = r12
            java.util.List<com.fossil.k5> r0 = r1.w0
            java.lang.Object r0 = r0.get(r15)
            com.fossil.k5 r0 = (com.fossil.k5) r0
            r0.b()
            r0 = r17
        L_0x02e8:
            int r15 = r15 + 1
            r12 = r19
            r4 = 0
            r7 = 32
            r8 = 8
            r9 = 1
            goto L_0x00ae
        L_0x02f4:
            r1.k0 = r13
            com.fossil.i5 r4 = r1.D
            if (r4 == 0) goto L_0x0326
            int r2 = r1.R
            int r3 = r21.t()
            int r2 = java.lang.Math.max(r2, r3)
            int r3 = r1.S
            int r4 = r21.j()
            int r3 = java.lang.Math.max(r3, r4)
            com.fossil.s5 r4 = r1.n0
            r4.a(r1)
            int r4 = r1.o0
            int r2 = r2 + r4
            int r4 = r1.q0
            int r2 = r2 + r4
            r1.p(r2)
            int r2 = r1.p0
            int r3 = r3 + r2
            int r2 = r1.r0
            int r3 = r3 + r2
            r1.h(r3)
            goto L_0x032a
        L_0x0326:
            r1.I = r2
            r1.J = r3
        L_0x032a:
            if (r0 == 0) goto L_0x0334
            com.fossil.i5$b[] r0 = r1.C
            r2 = 0
            r0[r2] = r11
            r2 = 1
            r0[r2] = r10
        L_0x0334:
            com.fossil.y4 r0 = r1.m0
            com.fossil.w4 r0 = r0.e()
            r1.a(r0)
            com.fossil.j5 r0 = r21.K()
            if (r1 != r0) goto L_0x0346
            r21.I()
        L_0x0346:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.j5.L():void");
    }

    @DexIgnore
    public int N() {
        return this.C0;
    }

    @DexIgnore
    public boolean O() {
        return false;
    }

    @DexIgnore
    public boolean P() {
        return this.F0;
    }

    @DexIgnore
    public boolean Q() {
        return this.l0;
    }

    @DexIgnore
    public boolean R() {
        return this.E0;
    }

    @DexIgnore
    public void S() {
        if (!u(8)) {
            a(this.C0);
        }
        W();
    }

    @DexIgnore
    public void T() {
        int size = ((t5) this).k0.size();
        G();
        for (int i = 0; i < size; i++) {
            ((t5) this).k0.get(i).G();
        }
    }

    @DexIgnore
    public void U() {
        T();
        a(this.C0);
    }

    @DexIgnore
    public final void V() {
        this.s0 = 0;
        this.t0 = 0;
    }

    @DexIgnore
    public void W() {
        p5 d = a(h5.d.LEFT).d();
        p5 d2 = a(h5.d.TOP).d();
        d.a((p5) null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        d2.a((p5) null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public void a(y4 y4Var, boolean[] zArr) {
        zArr[2] = false;
        c(y4Var);
        int size = ((t5) this).k0.size();
        for (int i = 0; i < size; i++) {
            i5 i5Var = ((t5) this).k0.get(i);
            i5Var.c(y4Var);
            if (i5Var.C[0] == i5.b.MATCH_CONSTRAINT && i5Var.t() < i5Var.v()) {
                zArr[2] = true;
            }
            if (i5Var.C[1] == i5.b.MATCH_CONSTRAINT && i5Var.j() < i5Var.u()) {
                zArr[2] = true;
            }
        }
    }

    @DexIgnore
    public void c(boolean z) {
        this.l0 = z;
    }

    @DexIgnore
    public boolean d(y4 y4Var) {
        a(y4Var);
        int size = ((t5) this).k0.size();
        for (int i = 0; i < size; i++) {
            i5 i5Var = ((t5) this).k0.get(i);
            if (i5Var instanceof j5) {
                i5.b[] bVarArr = i5Var.C;
                i5.b bVar = bVarArr[0];
                i5.b bVar2 = bVarArr[1];
                if (bVar == i5.b.WRAP_CONTENT) {
                    i5Var.a(i5.b.FIXED);
                }
                if (bVar2 == i5.b.WRAP_CONTENT) {
                    i5Var.b(i5.b.FIXED);
                }
                i5Var.a(y4Var);
                if (bVar == i5.b.WRAP_CONTENT) {
                    i5Var.a(bVar);
                }
                if (bVar2 == i5.b.WRAP_CONTENT) {
                    i5Var.b(bVar2);
                }
            } else {
                n5.a(this, y4Var, i5Var);
                i5Var.a(y4Var);
            }
        }
        if (this.s0 > 0) {
            f5.a(this, y4Var, 0);
        }
        if (this.t0 > 0) {
            f5.a(this, y4Var, 1);
        }
        return true;
    }

    @DexIgnore
    public final void e(i5 i5Var) {
        int i = this.t0 + 1;
        g5[] g5VarArr = this.u0;
        if (i >= g5VarArr.length) {
            this.u0 = (g5[]) Arrays.copyOf(g5VarArr, g5VarArr.length * 2);
        }
        this.u0[this.t0] = new g5(i5Var, 1, Q());
        this.t0++;
    }

    @DexIgnore
    public void f(int i, int i2) {
        q5 q5Var;
        q5 q5Var2;
        if (!(((i5) this).C[0] == i5.b.WRAP_CONTENT || (q5Var2 = ((i5) this).c) == null)) {
            q5Var2.a(i);
        }
        if (((i5) this).C[1] != i5.b.WRAP_CONTENT && (q5Var = ((i5) this).d) != null) {
            q5Var.a(i2);
        }
    }

    @DexIgnore
    public boolean u(int i) {
        return (this.C0 & i) == i;
    }

    @DexIgnore
    public void v(int i) {
        this.C0 = i;
    }

    @DexIgnore
    @Override // com.fossil.i5
    public void a(int i) {
        super.a(i);
        int size = ((t5) this).k0.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((t5) this).k0.get(i2).a(i);
        }
    }

    @DexIgnore
    public void a(i5 i5Var, int i) {
        if (i == 0) {
            d(i5Var);
        } else if (i == 1) {
            e(i5Var);
        }
    }

    @DexIgnore
    public final void d(i5 i5Var) {
        int i = this.s0 + 1;
        g5[] g5VarArr = this.v0;
        if (i >= g5VarArr.length) {
            this.v0 = (g5[]) Arrays.copyOf(g5VarArr, g5VarArr.length * 2);
        }
        this.v0[this.s0] = new g5(i5Var, 0, Q());
        this.s0++;
    }
}
