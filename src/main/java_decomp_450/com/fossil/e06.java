package com.fossil;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.fossil.xg5;
import com.fossil.z46;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e06 extends c06 {
    @DexIgnore
    public /* final */ og5 A;
    @DexIgnore
    public /* final */ CustomizeRealDataRepository B;
    @DexIgnore
    public /* final */ UserRepository C;
    @DexIgnore
    public /* final */ WatchFaceRepository D;
    @DexIgnore
    public LiveData<List<DianaPreset>> e; // = new MutableLiveData();
    @DexIgnore
    public LiveData<List<CustomizeRealData>> f; // = this.B.getAllRealDataAsLiveData();
    @DexIgnore
    public /* final */ ArrayList<Complication> g; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<WatchApp> h; // = new ArrayList<>();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<DianaPreset> i; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public DianaPreset j;
    @DexIgnore
    public MutableLiveData<String> k;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public CopyOnWriteArrayList<CustomizeRealData> m; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public int n; // = 2;
    @DexIgnore
    public MFUser o;
    @DexIgnore
    public kn7 p; // = mn7.a(false, 1, null);
    @DexIgnore
    public DianaComplicationRingStyle q;
    @DexIgnore
    public r87<Boolean, ? extends List<DianaPreset>> r; // = w87.a(false, null);
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public /* final */ d06 u;
    @DexIgnore
    public /* final */ WatchAppRepository v;
    @DexIgnore
    public /* final */ ComplicationRepository w;
    @DexIgnore
    public /* final */ RingStyleRepository x;
    @DexIgnore
    public /* final */ DianaPresetRepository y;
    @DexIgnore
    public /* final */ z46 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter", f = "HomeDianaCustomizePresenter.kt", l = {208}, m = "convertPresetToDianaPresetConfigWrapper")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$13;
        @DexIgnore
        public Object L$14;
        @DexIgnore
        public Object L$15;
        @DexIgnore
        public Object L$16;
        @DexIgnore
        public Object L$17;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ e06 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(e06 e06, fb7 fb7) {
            super(fb7);
            this.this$0 = e06;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((DianaPreset) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$createNewPreset$1", f = "HomeDianaCustomizePresenter.kt", l = {342}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ e06 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ DianaPreset $newPreset;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(DianaPreset dianaPreset, fb7 fb7, c cVar) {
                super(2, fb7);
                this.$newPreset = dianaPreset;
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$newPreset, fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    e06 e06 = this.this$0.this$0;
                    e06.c(e06.k() + 1);
                    DianaPresetRepository l = this.this$0.this$0.y;
                    DianaPreset dianaPreset = this.$newPreset;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (l.upsertPreset(dianaPreset, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(e06 e06, fb7 fb7) {
            super(2, fb7);
            this.this$0 = e06;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                Iterator it = this.this$0.i.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    if (pb7.a(((DianaPreset) obj2).isActive()).booleanValue()) {
                        break;
                    }
                }
                DianaPreset dianaPreset = (DianaPreset) obj2;
                if (dianaPreset != null) {
                    DianaPreset cloneFrom = DianaPreset.Companion.cloneFrom(dianaPreset);
                    ti7 d = this.this$0.c();
                    a aVar = new a(cloneFrom, null, this);
                    this.L$0 = yi7;
                    this.L$1 = dianaPreset;
                    this.L$2 = dianaPreset;
                    this.L$3 = cloneFrom;
                    this.label = 1;
                    if (vh7.a(d, aVar, this) == a2) {
                        return a2;
                    }
                }
                return i97.a;
            } else if (i == 1) {
                DianaPreset dianaPreset2 = (DianaPreset) this.L$3;
                DianaPreset dianaPreset3 = (DianaPreset) this.L$2;
                DianaPreset dianaPreset4 = (DianaPreset) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.u.w();
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.e<z46.d, z46.b> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset a;
        @DexIgnore
        public /* final */ /* synthetic */ e06 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.e06$d$a$a")
            /* renamed from: com.fossil.e06$d$a$a  reason: collision with other inner class name */
            public static final class C0045a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0045a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0045a aVar = new C0045a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0045a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        DianaPresetRepository l = this.this$0.this$0.b.y;
                        DianaPreset dianaPreset = this.this$0.this$0.a;
                        if (dianaPreset != null) {
                            String id = dianaPreset.getId();
                            this.L$0 = yi7;
                            this.label = 1;
                            if (l.deletePresetById(id, this) == a) {
                                return a;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return i97.a;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("set new preset to watch success, delete current active ");
                    DianaPreset dianaPreset = this.this$0.a;
                    sb.append(dianaPreset != null ? dianaPreset.getName() : null);
                    local.d("HomeDianaCustomizePresenter", sb.toString());
                    ti7 d = this.this$0.b.c();
                    C0045a aVar = new C0045a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    if (vh7.a(d, aVar, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.b.u.m();
                this.this$0.b.u.b(this.this$0.b.k());
                return i97.a;
            }
        }

        @DexIgnore
        public d(DianaPreset dianaPreset, e06 e06, String str) {
            this.a = dianaPreset;
            this.b = e06;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(z46.d dVar) {
            ee7.b(dVar, "responseValue");
            ik7 unused = xh7.b(this.b.e(), null, null, new a(this, null), 3, null);
        }

        @DexIgnore
        public void a(z46.b bVar) {
            ee7.b(bVar, "errorValue");
            this.b.u.m();
            int b2 = bVar.b();
            if (b2 == 1101 || b2 == 1112 || b2 == 1113) {
                List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(bVar.a());
                ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                d06 u = this.b.u;
                Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
                if (array != null) {
                    ib5[] ib5Arr = (ib5[]) array;
                    u.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.b.u.l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $nextActivePresetId$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $preset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ e06 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    DianaPresetRepository l = this.this$0.this$0.y;
                    String id = this.this$0.$preset.getId();
                    this.L$0 = yi7;
                    this.label = 1;
                    if (l.deletePresetById(id, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(DianaPreset dianaPreset, fb7 fb7, e06 e06, String str) {
            super(2, fb7);
            this.$preset = dianaPreset;
            this.this$0 = e06;
            this.$nextActivePresetId$inlined = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.$preset, fb7, this.this$0, this.$nextActivePresetId$inlined);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 d = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(d, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.u.b(this.this$0.k() - 1);
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$renameCurrentPreset$1", f = "HomeDianaCustomizePresenter.kt", l = {244}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $name;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ e06 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ DianaPreset $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(DianaPreset dianaPreset, fb7 fb7, f fVar) {
                super(2, fb7);
                this.$it = dianaPreset;
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$it, fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    DianaPresetRepository l = this.this$0.this$0.y;
                    DianaPreset dianaPreset = this.$it;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (l.upsertPreset(dianaPreset, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(e06 e06, String str, String str2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = e06;
            this.$presetId = str;
            this.$name = str2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, this.$presetId, this.$name, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                Iterator it = this.this$0.i.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    if (pb7.a(ee7.a((Object) ((DianaPreset) obj2).getId(), (Object) this.$presetId)).booleanValue()) {
                        break;
                    }
                }
                DianaPreset dianaPreset = (DianaPreset) obj2;
                DianaPreset clone = dianaPreset != null ? dianaPreset.clone() : null;
                if (clone != null) {
                    clone.setName(this.$name);
                    ti7 d = this.this$0.c();
                    a aVar = new a(clone, null, this);
                    this.L$0 = yi7;
                    this.L$1 = clone;
                    this.L$2 = clone;
                    this.label = 1;
                    if (vh7.a(d, aVar, this) == a2) {
                        return a2;
                    }
                }
            } else if (i == 1) {
                DianaPreset dianaPreset2 = (DianaPreset) this.L$2;
                DianaPreset dianaPreset3 = (DianaPreset) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements fl4.e<z46.d, z46.b> {
        @DexIgnore
        public /* final */ /* synthetic */ e06 a;

        @DexIgnore
        public g(e06 e06) {
            this.a = e06;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(z46.d dVar) {
            ee7.b(dVar, "responseValue");
            this.a.u.m();
            this.a.u.e(0);
        }

        @DexIgnore
        public void a(z46.b bVar) {
            ee7.b(bVar, "errorValue");
            this.a.u.m();
            int b = bVar.b();
            if (b == 1101 || b == 1112 || b == 1113) {
                List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(bVar.a());
                ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                d06 u = this.a.u;
                Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
                if (array != null) {
                    ib5[] ib5Arr = (ib5[]) array;
                    u.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.a.u.l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1", f = "HomeDianaCustomizePresenter.kt", l = {354, 360}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ e06 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1$1", f = "HomeDianaCustomizePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super DianaComplicationRingStyle>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = hVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super DianaComplicationRingStyle> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    RingStyleRepository q = this.this$0.this$0.x;
                    Object a = this.this$0.this$0.k.a();
                    if (a != null) {
                        ee7.a(a, "mSerialLiveData.value!!");
                        return q.getRingStylesBySerial((String) a);
                    }
                    ee7.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1$wrapperUIData$1", f = "HomeDianaCustomizePresenter.kt", l = {422, 356}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super List<? extends dz5>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(h hVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = hVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends dz5>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:19:0x0091  */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r14) {
                /*
                    r13 = this;
                    java.lang.Object r0 = com.fossil.nb7.a()
                    int r1 = r13.label
                    r2 = 2
                    r3 = 1
                    r4 = 0
                    if (r1 == 0) goto L_0x0053
                    if (r1 == r3) goto L_0x0047
                    if (r1 != r2) goto L_0x003f
                    java.lang.Object r1 = r13.L$8
                    java.util.Collection r1 = (java.util.Collection) r1
                    java.lang.Object r3 = r13.L$7
                    com.portfolio.platform.data.model.diana.preset.DianaPreset r3 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r3
                    java.lang.Object r3 = r13.L$5
                    java.util.Iterator r3 = (java.util.Iterator) r3
                    java.lang.Object r5 = r13.L$4
                    java.util.Collection r5 = (java.util.Collection) r5
                    java.lang.Object r6 = r13.L$3
                    java.lang.Iterable r6 = (java.lang.Iterable) r6
                    java.lang.Object r7 = r13.L$2
                    java.lang.Iterable r7 = (java.lang.Iterable) r7
                    java.lang.Object r8 = r13.L$1
                    com.fossil.kn7 r8 = (com.fossil.kn7) r8
                    java.lang.Object r9 = r13.L$0
                    com.fossil.yi7 r9 = (com.fossil.yi7) r9
                    com.fossil.t87.a(r14)     // Catch:{ all -> 0x003c }
                    r10 = r9
                    r9 = r8
                    r8 = r7
                    r7 = r6
                    r6 = r3
                    r3 = r1
                    r1 = r0
                    r0 = r13
                    goto L_0x00c6
                L_0x003c:
                    r14 = move-exception
                    goto L_0x00dd
                L_0x003f:
                    java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r14.<init>(r0)
                    throw r14
                L_0x0047:
                    java.lang.Object r1 = r13.L$1
                    com.fossil.kn7 r1 = (com.fossil.kn7) r1
                    java.lang.Object r3 = r13.L$0
                    com.fossil.yi7 r3 = (com.fossil.yi7) r3
                    com.fossil.t87.a(r14)
                    goto L_0x006e
                L_0x0053:
                    com.fossil.t87.a(r14)
                    com.fossil.yi7 r14 = r13.p$
                    com.fossil.e06$h r1 = r13.this$0
                    com.fossil.e06 r1 = r1.this$0
                    com.fossil.kn7 r1 = r1.p
                    r13.L$0 = r14
                    r13.L$1 = r1
                    r13.label = r3
                    java.lang.Object r3 = r1.a(r4, r13)
                    if (r3 != r0) goto L_0x006d
                    return r0
                L_0x006d:
                    r3 = r14
                L_0x006e:
                    r8 = r1
                    com.fossil.e06$h r14 = r13.this$0
                    com.fossil.e06 r14 = r14.this$0
                    java.util.concurrent.CopyOnWriteArrayList r14 = r14.i
                    java.util.ArrayList r1 = new java.util.ArrayList
                    r5 = 10
                    int r5 = com.fossil.x97.a(r14, r5)
                    r1.<init>(r5)
                    java.util.Iterator r5 = r14.iterator()
                    r6 = r14
                    r7 = r6
                    r9 = r3
                    r3 = r5
                    r14 = r13
                L_0x008b:
                    boolean r5 = r3.hasNext()
                    if (r5 == 0) goto L_0x00d7
                    java.lang.Object r5 = r3.next()
                    r10 = r5
                    com.portfolio.platform.data.model.diana.preset.DianaPreset r10 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r10
                    com.fossil.e06$h r11 = r14.this$0
                    com.fossil.e06 r11 = r11.this$0
                    java.lang.String r12 = "it"
                    com.fossil.ee7.a(r10, r12)
                    r14.L$0 = r9
                    r14.L$1 = r8
                    r14.L$2 = r7
                    r14.L$3 = r6
                    r14.L$4 = r1
                    r14.L$5 = r3
                    r14.L$6 = r5
                    r14.L$7 = r10
                    r14.L$8 = r1
                    r14.label = r2
                    java.lang.Object r5 = r11.a(r10, r14)
                    if (r5 != r0) goto L_0x00bc
                    return r0
                L_0x00bc:
                    r10 = r9
                    r9 = r8
                    r8 = r7
                    r7 = r6
                    r6 = r3
                    r3 = r1
                    r1 = r0
                    r0 = r14
                    r14 = r5
                    r5 = r3
                L_0x00c6:
                    com.fossil.dz5 r14 = (com.fossil.dz5) r14     // Catch:{ all -> 0x00d4 }
                    r3.add(r14)     // Catch:{ all -> 0x00d4 }
                    r14 = r0
                    r0 = r1
                    r1 = r5
                    r3 = r6
                    r6 = r7
                    r7 = r8
                    r8 = r9
                    r9 = r10
                    goto L_0x008b
                L_0x00d4:
                    r14 = move-exception
                    r8 = r9
                    goto L_0x00dd
                L_0x00d7:
                    java.util.List r1 = (java.util.List) r1
                    r8.a(r4)
                    return r1
                L_0x00dd:
                    r8.a(r4)
                    throw r14
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.e06.h.b.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(e06 e06, fb7 fb7) {
            super(2, fb7);
            this.this$0 = e06;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0095  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00b5  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                r11 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r11.label
                r2 = 0
                r3 = 2
                r4 = 1
                if (r1 == 0) goto L_0x002f
                if (r1 == r4) goto L_0x0027
                if (r1 != r3) goto L_0x001f
                java.lang.Object r0 = r11.L$2
                com.fossil.e06 r0 = (com.fossil.e06) r0
                java.lang.Object r1 = r11.L$1
                java.util.List r1 = (java.util.List) r1
                java.lang.Object r2 = r11.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r12)
                goto L_0x0072
            L_0x001f:
                java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r12.<init>(r0)
                throw r12
            L_0x0027:
                java.lang.Object r1 = r11.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r12)
                goto L_0x004a
            L_0x002f:
                com.fossil.t87.a(r12)
                com.fossil.yi7 r1 = r11.p$
                com.fossil.e06 r12 = r11.this$0
                com.fossil.ti7 r12 = r12.b()
                com.fossil.e06$h$b r5 = new com.fossil.e06$h$b
                r5.<init>(r11, r2)
                r11.L$0 = r1
                r11.label = r4
                java.lang.Object r12 = com.fossil.vh7.a(r12, r5, r11)
                if (r12 != r0) goto L_0x004a
                return r0
            L_0x004a:
                java.util.List r12 = (java.util.List) r12
                com.fossil.e06 r5 = r11.this$0
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r5 = r5.q
                if (r5 != 0) goto L_0x0078
                com.fossil.e06 r5 = r11.this$0
                com.fossil.ti7 r6 = com.fossil.qj7.b()
                com.fossil.e06$h$a r7 = new com.fossil.e06$h$a
                r7.<init>(r11, r2)
                r11.L$0 = r1
                r11.L$1 = r12
                r11.L$2 = r5
                r11.label = r3
                java.lang.Object r1 = com.fossil.vh7.a(r6, r7, r11)
                if (r1 != r0) goto L_0x006e
                return r0
            L_0x006e:
                r0 = r5
                r10 = r1
                r1 = r12
                r12 = r10
            L_0x0072:
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r12 = (com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle) r12
                r0.q = r12
                r12 = r1
            L_0x0078:
                com.fossil.e06 r0 = r11.this$0
                java.util.concurrent.CopyOnWriteArrayList r0 = r0.i
                boolean r0 = r0.isEmpty()
                if (r0 != 0) goto L_0x00bd
                boolean r0 = r12.isEmpty()
                r0 = r0 ^ r4
                if (r0 == 0) goto L_0x00bd
                com.fossil.e06 r0 = r11.this$0
                com.fossil.xg5 r1 = com.fossil.xg5.b
                com.fossil.d06 r2 = r0.u
                if (r2 == 0) goto L_0x00b5
                com.fossil.vr5 r2 = (com.fossil.vr5) r2
                android.content.Context r2 = r2.getContext()
                r3 = 0
                java.lang.Object r3 = r12.get(r3)
                com.fossil.dz5 r3 = (com.fossil.dz5) r3
                java.util.List r3 = r3.c()
                r4 = 0
                r5 = 0
                r6 = 0
                r7 = 0
                r8 = 56
                r9 = 0
                boolean r1 = com.fossil.xg5.a(r1, r2, r3, r4, r5, r6, r7, r8, r9)
                r0.s = r1
                goto L_0x00bd
            L_0x00b5:
                com.fossil.x87 r12 = new com.fossil.x87
                java.lang.String r0 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment"
                r12.<init>(r0)
                throw r12
            L_0x00bd:
                com.fossil.e06 r0 = r11.this$0
                com.fossil.d06 r0 = r0.u
                com.fossil.e06 r1 = r11.this$0
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r1 = r1.q
                r0.a(r12, r1)
                com.fossil.i97 r12 = com.fossil.i97.a
                return r12
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.e06.h.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1", f = "HomeDianaCustomizePresenter.kt", l = {94}, m = "invokeSuspend")
    public static final class i extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ e06 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$1", f = "HomeDianaCustomizePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(i iVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.g.clear();
                    this.this$0.this$0.g.addAll(this.this$0.this$0.w.getAllComplicationRaw());
                    this.this$0.this$0.h.clear();
                    this.this$0.this$0.h.addAll(this.this$0.this$0.v.getAllWatchAppRaw());
                    this.this$0.this$0.m.clear();
                    return pb7.a(this.this$0.this$0.m.addAll(this.this$0.this$0.B.getAllRealDataRaw()));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements zd<List<? extends CustomizeRealData>> {
            @DexIgnore
            public /* final */ /* synthetic */ i a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $it;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public Object L$3;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.e06$i$b$a$a")
                /* renamed from: com.fossil.e06$i$b$a$a  reason: collision with other inner class name */
                public static final class C0046a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0046a(a aVar, fb7 fb7) {
                        super(2, fb7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0046a aVar = new C0046a(this.this$0, fb7);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                        return ((C0046a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        Object a = nb7.a();
                        int i = this.label;
                        if (i == 0) {
                            t87.a(obj);
                            yi7 yi7 = this.p$;
                            UserRepository t = this.this$0.this$0.a.this$0.C;
                            this.L$0 = yi7;
                            this.label = 1;
                            obj = t.getCurrentUser(this);
                            if (obj == a) {
                                return a;
                            }
                        } else if (i == 1) {
                            yi7 yi72 = (yi7) this.L$0;
                            t87.a(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        return obj;
                    }
                }

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.e06$i$b$a$b")
                /* renamed from: com.fossil.e06$i$b$a$b  reason: collision with other inner class name */
                public static final class C0047b extends zb7 implements kd7<yi7, fb7<? super List<? extends dz5>>, Object> {
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public Object L$1;
                    @DexIgnore
                    public Object L$2;
                    @DexIgnore
                    public Object L$3;
                    @DexIgnore
                    public Object L$4;
                    @DexIgnore
                    public Object L$5;
                    @DexIgnore
                    public Object L$6;
                    @DexIgnore
                    public Object L$7;
                    @DexIgnore
                    public Object L$8;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0047b(a aVar, fb7 fb7) {
                        super(2, fb7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0047b bVar = new C0047b(this.this$0, fb7);
                        bVar.p$ = (yi7) obj;
                        return bVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super List<? extends dz5>> fb7) {
                        return ((C0047b) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    /* JADX WARNING: Removed duplicated region for block: B:19:0x0099  */
                    @Override // com.fossil.ob7
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public final java.lang.Object invokeSuspend(java.lang.Object r14) {
                        /*
                            r13 = this;
                            java.lang.Object r0 = com.fossil.nb7.a()
                            int r1 = r13.label
                            r2 = 2
                            r3 = 1
                            r4 = 0
                            if (r1 == 0) goto L_0x0053
                            if (r1 == r3) goto L_0x0047
                            if (r1 != r2) goto L_0x003f
                            java.lang.Object r1 = r13.L$8
                            java.util.Collection r1 = (java.util.Collection) r1
                            java.lang.Object r3 = r13.L$7
                            com.portfolio.platform.data.model.diana.preset.DianaPreset r3 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r3
                            java.lang.Object r3 = r13.L$5
                            java.util.Iterator r3 = (java.util.Iterator) r3
                            java.lang.Object r5 = r13.L$4
                            java.util.Collection r5 = (java.util.Collection) r5
                            java.lang.Object r6 = r13.L$3
                            java.lang.Iterable r6 = (java.lang.Iterable) r6
                            java.lang.Object r7 = r13.L$2
                            java.lang.Iterable r7 = (java.lang.Iterable) r7
                            java.lang.Object r8 = r13.L$1
                            com.fossil.kn7 r8 = (com.fossil.kn7) r8
                            java.lang.Object r9 = r13.L$0
                            com.fossil.yi7 r9 = (com.fossil.yi7) r9
                            com.fossil.t87.a(r14)     // Catch:{ all -> 0x003c }
                            r10 = r9
                            r9 = r8
                            r8 = r7
                            r7 = r6
                            r6 = r3
                            r3 = r1
                            r1 = r0
                            r0 = r13
                            goto L_0x00d2
                        L_0x003c:
                            r14 = move-exception
                            goto L_0x00e9
                        L_0x003f:
                            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
                            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                            r14.<init>(r0)
                            throw r14
                        L_0x0047:
                            java.lang.Object r1 = r13.L$1
                            com.fossil.kn7 r1 = (com.fossil.kn7) r1
                            java.lang.Object r3 = r13.L$0
                            com.fossil.yi7 r3 = (com.fossil.yi7) r3
                            com.fossil.t87.a(r14)
                            goto L_0x0072
                        L_0x0053:
                            com.fossil.t87.a(r14)
                            com.fossil.yi7 r14 = r13.p$
                            com.fossil.e06$i$b$a r1 = r13.this$0
                            com.fossil.e06$i$b r1 = r1.this$0
                            com.fossil.e06$i r1 = r1.a
                            com.fossil.e06 r1 = r1.this$0
                            com.fossil.kn7 r1 = r1.p
                            r13.L$0 = r14
                            r13.L$1 = r1
                            r13.label = r3
                            java.lang.Object r3 = r1.a(r4, r13)
                            if (r3 != r0) goto L_0x0071
                            return r0
                        L_0x0071:
                            r3 = r14
                        L_0x0072:
                            r8 = r1
                            com.fossil.e06$i$b$a r14 = r13.this$0
                            com.fossil.e06$i$b r14 = r14.this$0
                            com.fossil.e06$i r14 = r14.a
                            com.fossil.e06 r14 = r14.this$0
                            java.util.concurrent.CopyOnWriteArrayList r14 = r14.i
                            java.util.ArrayList r1 = new java.util.ArrayList
                            r5 = 10
                            int r5 = com.fossil.x97.a(r14, r5)
                            r1.<init>(r5)
                            java.util.Iterator r5 = r14.iterator()
                            r6 = r14
                            r7 = r6
                            r9 = r3
                            r3 = r5
                            r14 = r13
                        L_0x0093:
                            boolean r5 = r3.hasNext()
                            if (r5 == 0) goto L_0x00e3
                            java.lang.Object r5 = r3.next()
                            r10 = r5
                            com.portfolio.platform.data.model.diana.preset.DianaPreset r10 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r10
                            com.fossil.e06$i$b$a r11 = r14.this$0
                            com.fossil.e06$i$b r11 = r11.this$0
                            com.fossil.e06$i r11 = r11.a
                            com.fossil.e06 r11 = r11.this$0
                            java.lang.String r12 = "it"
                            com.fossil.ee7.a(r10, r12)
                            r14.L$0 = r9
                            r14.L$1 = r8
                            r14.L$2 = r7
                            r14.L$3 = r6
                            r14.L$4 = r1
                            r14.L$5 = r3
                            r14.L$6 = r5
                            r14.L$7 = r10
                            r14.L$8 = r1
                            r14.label = r2
                            java.lang.Object r5 = r11.a(r10, r14)
                            if (r5 != r0) goto L_0x00c8
                            return r0
                        L_0x00c8:
                            r10 = r9
                            r9 = r8
                            r8 = r7
                            r7 = r6
                            r6 = r3
                            r3 = r1
                            r1 = r0
                            r0 = r14
                            r14 = r5
                            r5 = r3
                        L_0x00d2:
                            com.fossil.dz5 r14 = (com.fossil.dz5) r14     // Catch:{ all -> 0x00e0 }
                            r3.add(r14)     // Catch:{ all -> 0x00e0 }
                            r14 = r0
                            r0 = r1
                            r1 = r5
                            r3 = r6
                            r6 = r7
                            r7 = r8
                            r8 = r9
                            r9 = r10
                            goto L_0x0093
                        L_0x00e0:
                            r14 = move-exception
                            r8 = r9
                            goto L_0x00e9
                        L_0x00e3:
                            java.util.List r1 = (java.util.List) r1
                            r8.a(r4)
                            return r1
                        L_0x00e9:
                            r8.a(r4)
                            throw r14
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.fossil.e06.i.b.a.C0047b.invokeSuspend(java.lang.Object):java.lang.Object");
                    }
                }

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class c extends zb7 implements kd7<yi7, fb7<? super DianaComplicationRingStyle>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public c(a aVar, fb7 fb7) {
                        super(2, fb7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        c cVar = new c(this.this$0, fb7);
                        cVar.p$ = (yi7) obj;
                        return cVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super DianaComplicationRingStyle> fb7) {
                        return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        nb7.a();
                        if (this.label == 0) {
                            t87.a(obj);
                            RingStyleRepository q = this.this$0.this$0.a.this$0.x;
                            Object a = this.this$0.this$0.a.this$0.k.a();
                            if (a != null) {
                                ee7.a(a, "mSerialLiveData.value!!");
                                return q.getRingStylesBySerial((String) a);
                            }
                            ee7.a();
                            throw null;
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(List list, fb7 fb7, b bVar) {
                    super(2, fb7);
                    this.$it = list;
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.$it, fb7, this.this$0);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:26:0x0121 A[RETURN] */
                /* JADX WARNING: Removed duplicated region for block: B:27:0x0122  */
                @Override // com.fossil.ob7
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final java.lang.Object invokeSuspend(java.lang.Object r11) {
                    /*
                        r10 = this;
                        java.lang.Object r0 = com.fossil.nb7.a()
                        int r1 = r10.label
                        r2 = 3
                        r3 = 2
                        r4 = 0
                        r5 = 1
                        if (r1 == 0) goto L_0x0044
                        if (r1 == r5) goto L_0x003c
                        if (r1 == r3) goto L_0x002f
                        if (r1 != r2) goto L_0x0027
                        java.lang.Object r0 = r10.L$3
                        com.fossil.e06 r0 = (com.fossil.e06) r0
                        java.lang.Object r1 = r10.L$2
                        java.util.List r1 = (java.util.List) r1
                        java.lang.Object r2 = r10.L$1
                        com.portfolio.platform.data.model.MFUser r2 = (com.portfolio.platform.data.model.MFUser) r2
                        java.lang.Object r2 = r10.L$0
                        com.fossil.yi7 r2 = (com.fossil.yi7) r2
                        com.fossil.t87.a(r11)
                        goto L_0x0126
                    L_0x0027:
                        java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
                        java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                        r11.<init>(r0)
                        throw r11
                    L_0x002f:
                        java.lang.Object r1 = r10.L$1
                        com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                        java.lang.Object r3 = r10.L$0
                        com.fossil.yi7 r3 = (com.fossil.yi7) r3
                        com.fossil.t87.a(r11)
                        goto L_0x0100
                    L_0x003c:
                        java.lang.Object r1 = r10.L$0
                        com.fossil.yi7 r1 = (com.fossil.yi7) r1
                        com.fossil.t87.a(r11)
                        goto L_0x0066
                    L_0x0044:
                        com.fossil.t87.a(r11)
                        com.fossil.yi7 r11 = r10.p$
                        com.fossil.e06$i$b r1 = r10.this$0
                        com.fossil.e06$i r1 = r1.a
                        com.fossil.e06 r1 = r1.this$0
                        com.fossil.ti7 r1 = r1.b()
                        com.fossil.e06$i$b$a$a r6 = new com.fossil.e06$i$b$a$a
                        r6.<init>(r10, r4)
                        r10.L$0 = r11
                        r10.label = r5
                        java.lang.Object r1 = com.fossil.vh7.a(r1, r6, r10)
                        if (r1 != r0) goto L_0x0063
                        return r0
                    L_0x0063:
                        r9 = r1
                        r1 = r11
                        r11 = r9
                    L_0x0066:
                        com.portfolio.platform.data.model.MFUser r11 = (com.portfolio.platform.data.model.MFUser) r11
                        com.fossil.e06$i$b r6 = r10.this$0
                        com.fossil.e06$i r6 = r6.a
                        com.fossil.e06 r6 = r6.this$0
                        java.util.concurrent.CopyOnWriteArrayList r6 = r6.m
                        java.util.List r7 = r10.$it
                        boolean r6 = com.fossil.ee7.a(r6, r7)
                        r6 = r6 ^ r5
                        if (r6 != 0) goto L_0x008c
                        com.fossil.e06$i$b r6 = r10.this$0
                        com.fossil.e06$i r6 = r6.a
                        com.fossil.e06 r6 = r6.this$0
                        com.portfolio.platform.data.model.MFUser r6 = r6.o
                        boolean r6 = com.fossil.ee7.a(r6, r11)
                        r6 = r6 ^ r5
                        if (r6 == 0) goto L_0x0142
                    L_0x008c:
                        com.fossil.e06$i$b r6 = r10.this$0
                        com.fossil.e06$i r6 = r6.a
                        com.fossil.e06 r6 = r6.this$0
                        r6.o = r11
                        com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
                        java.lang.StringBuilder r7 = new java.lang.StringBuilder
                        r7.<init>()
                        java.lang.String r8 = "on real data change "
                        r7.append(r8)
                        java.util.List r8 = r10.$it
                        r7.append(r8)
                        java.lang.String r7 = r7.toString()
                        java.lang.String r8 = "HomeDianaCustomizePresenter"
                        r6.d(r8, r7)
                        com.fossil.e06$i$b r6 = r10.this$0
                        com.fossil.e06$i r6 = r6.a
                        com.fossil.e06 r6 = r6.this$0
                        java.util.concurrent.CopyOnWriteArrayList r6 = r6.m
                        r6.clear()
                        com.fossil.e06$i$b r6 = r10.this$0
                        com.fossil.e06$i r6 = r6.a
                        com.fossil.e06 r6 = r6.this$0
                        java.util.concurrent.CopyOnWriteArrayList r6 = r6.m
                        java.util.List r7 = r10.$it
                        r6.addAll(r7)
                        com.fossil.e06$i$b r6 = r10.this$0
                        com.fossil.e06$i r6 = r6.a
                        com.fossil.e06 r6 = r6.this$0
                        java.util.concurrent.CopyOnWriteArrayList r6 = r6.i
                        boolean r6 = r6.isEmpty()
                        r5 = r5 ^ r6
                        if (r5 == 0) goto L_0x0142
                        com.fossil.e06$i$b r5 = r10.this$0
                        com.fossil.e06$i r5 = r5.a
                        com.fossil.e06 r5 = r5.this$0
                        com.fossil.ti7 r5 = r5.b()
                        com.fossil.e06$i$b$a$b r6 = new com.fossil.e06$i$b$a$b
                        r6.<init>(r10, r4)
                        r10.L$0 = r1
                        r10.L$1 = r11
                        r10.label = r3
                        java.lang.Object r3 = com.fossil.vh7.a(r5, r6, r10)
                        if (r3 != r0) goto L_0x00fc
                        return r0
                    L_0x00fc:
                        r9 = r1
                        r1 = r11
                        r11 = r3
                        r3 = r9
                    L_0x0100:
                        java.util.List r11 = (java.util.List) r11
                        com.fossil.e06$i$b r5 = r10.this$0
                        com.fossil.e06$i r5 = r5.a
                        com.fossil.e06 r5 = r5.this$0
                        com.fossil.ti7 r6 = r5.c()
                        com.fossil.e06$i$b$a$c r7 = new com.fossil.e06$i$b$a$c
                        r7.<init>(r10, r4)
                        r10.L$0 = r3
                        r10.L$1 = r1
                        r10.L$2 = r11
                        r10.L$3 = r5
                        r10.label = r2
                        java.lang.Object r1 = com.fossil.vh7.a(r6, r7, r10)
                        if (r1 != r0) goto L_0x0122
                        return r0
                    L_0x0122:
                        r0 = r5
                        r9 = r1
                        r1 = r11
                        r11 = r9
                    L_0x0126:
                        com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r11 = (com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle) r11
                        r0.q = r11
                        com.fossil.e06$i$b r11 = r10.this$0
                        com.fossil.e06$i r11 = r11.a
                        com.fossil.e06 r11 = r11.this$0
                        com.fossil.d06 r11 = r11.u
                        com.fossil.e06$i$b r0 = r10.this$0
                        com.fossil.e06$i r0 = r0.a
                        com.fossil.e06 r0 = r0.this$0
                        com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r0 = r0.q
                        r11.a(r1, r0)
                    L_0x0142:
                        com.fossil.i97 r11 = com.fossil.i97.a
                        return r11
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.fossil.e06.i.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
                }
            }

            @DexIgnore
            public b(i iVar) {
                this.a = iVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(List<CustomizeRealData> list) {
                if (list != null) {
                    ik7 unused = xh7.b(this.a.this$0.e(), null, null, new a(list, null, this), 3, null);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c<T> implements zd<String> {
            @DexIgnore
            public /* final */ /* synthetic */ i a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a<T> implements zd<List<? extends DianaPreset>> {
                @DexIgnore
                public /* final */ /* synthetic */ c a;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.e06$i$c$a$a")
                /* renamed from: com.fossil.e06$i$c$a$a  reason: collision with other inner class name */
                public static final class C0048a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ List $it;
                    @DexIgnore
                    public int I$0;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public Object L$1;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.e06$i$c$a$a$a")
                    /* renamed from: com.fossil.e06$i$c$a$a$a  reason: collision with other inner class name */
                    public static final class C0049a<T> implements Comparator<T> {
                        @DexIgnore
                        @Override // java.util.Comparator
                        public final int compare(T t, T t2) {
                            return bb7.a(Boolean.valueOf(t2.isActive()), Boolean.valueOf(t.isActive()));
                        }
                    }

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.e06$i$c$a$a$b")
                    /* renamed from: com.fossil.e06$i$c$a$a$b */
                    public static final class b extends zb7 implements kd7<yi7, fb7<? super List<? extends dz5>>, Object> {
                        @DexIgnore
                        public Object L$0;
                        @DexIgnore
                        public Object L$1;
                        @DexIgnore
                        public Object L$2;
                        @DexIgnore
                        public Object L$3;
                        @DexIgnore
                        public Object L$4;
                        @DexIgnore
                        public Object L$5;
                        @DexIgnore
                        public Object L$6;
                        @DexIgnore
                        public Object L$7;
                        @DexIgnore
                        public int label;
                        @DexIgnore
                        public yi7 p$;
                        @DexIgnore
                        public /* final */ /* synthetic */ C0048a this$0;

                        @DexIgnore
                        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                        public b(C0048a aVar, fb7 fb7) {
                            super(2, fb7);
                            this.this$0 = aVar;
                        }

                        @DexIgnore
                        @Override // com.fossil.ob7
                        public final fb7<i97> create(Object obj, fb7<?> fb7) {
                            ee7.b(fb7, "completion");
                            b bVar = new b(this.this$0, fb7);
                            bVar.p$ = (yi7) obj;
                            return bVar;
                        }

                        @DexIgnore
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                        @Override // com.fossil.kd7
                        public final Object invoke(yi7 yi7, fb7<? super List<? extends dz5>> fb7) {
                            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
                        }

                        @DexIgnore
                        /* JADX WARNING: Removed duplicated region for block: B:9:0x0069  */
                        @Override // com.fossil.ob7
                        /* Code decompiled incorrectly, please refer to instructions dump. */
                        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
                            /*
                                r11 = this;
                                java.lang.Object r0 = com.fossil.nb7.a()
                                int r1 = r11.label
                                r2 = 1
                                if (r1 == 0) goto L_0x003b
                                if (r1 != r2) goto L_0x0033
                                java.lang.Object r1 = r11.L$7
                                java.util.Collection r1 = (java.util.Collection) r1
                                java.lang.Object r3 = r11.L$6
                                com.portfolio.platform.data.model.diana.preset.DianaPreset r3 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r3
                                java.lang.Object r3 = r11.L$4
                                java.util.Iterator r3 = (java.util.Iterator) r3
                                java.lang.Object r4 = r11.L$3
                                java.util.Collection r4 = (java.util.Collection) r4
                                java.lang.Object r5 = r11.L$2
                                java.lang.Iterable r5 = (java.lang.Iterable) r5
                                java.lang.Object r6 = r11.L$1
                                java.lang.Iterable r6 = (java.lang.Iterable) r6
                                java.lang.Object r7 = r11.L$0
                                com.fossil.yi7 r7 = (com.fossil.yi7) r7
                                com.fossil.t87.a(r12)
                                r8 = r7
                                r7 = r6
                                r6 = r5
                                r5 = r3
                                r3 = r1
                                r1 = r0
                                r0 = r11
                                goto L_0x00a1
                            L_0x0033:
                                java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
                                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                                r12.<init>(r0)
                                throw r12
                            L_0x003b:
                                com.fossil.t87.a(r12)
                                com.fossil.yi7 r12 = r11.p$
                                com.fossil.e06$i$c$a$a r1 = r11.this$0
                                com.fossil.e06$i$c$a r1 = r1.this$0
                                com.fossil.e06$i$c r1 = r1.a
                                com.fossil.e06$i r1 = r1.a
                                com.fossil.e06 r1 = r1.this$0
                                java.util.concurrent.CopyOnWriteArrayList r1 = r1.i
                                java.util.ArrayList r3 = new java.util.ArrayList
                                r4 = 10
                                int r4 = com.fossil.x97.a(r1, r4)
                                r3.<init>(r4)
                                java.util.Iterator r4 = r1.iterator()
                                r7 = r12
                                r5 = r1
                                r6 = r5
                                r1 = r3
                                r3 = r4
                                r12 = r11
                            L_0x0063:
                                boolean r4 = r3.hasNext()
                                if (r4 == 0) goto L_0x00ae
                                java.lang.Object r4 = r3.next()
                                r8 = r4
                                com.portfolio.platform.data.model.diana.preset.DianaPreset r8 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r8
                                com.fossil.e06$i$c$a$a r9 = r12.this$0
                                com.fossil.e06$i$c$a r9 = r9.this$0
                                com.fossil.e06$i$c r9 = r9.a
                                com.fossil.e06$i r9 = r9.a
                                com.fossil.e06 r9 = r9.this$0
                                java.lang.String r10 = "list"
                                com.fossil.ee7.a(r8, r10)
                                r12.L$0 = r7
                                r12.L$1 = r6
                                r12.L$2 = r5
                                r12.L$3 = r1
                                r12.L$4 = r3
                                r12.L$5 = r4
                                r12.L$6 = r8
                                r12.L$7 = r1
                                r12.label = r2
                                java.lang.Object r4 = r9.a(r8, r12)
                                if (r4 != r0) goto L_0x0098
                                return r0
                            L_0x0098:
                                r8 = r7
                                r7 = r6
                                r6 = r5
                                r5 = r3
                                r3 = r1
                                r1 = r0
                                r0 = r12
                                r12 = r4
                                r4 = r3
                            L_0x00a1:
                                com.fossil.dz5 r12 = (com.fossil.dz5) r12
                                r3.add(r12)
                                r12 = r0
                                r0 = r1
                                r1 = r4
                                r3 = r5
                                r5 = r6
                                r6 = r7
                                r7 = r8
                                goto L_0x0063
                            L_0x00ae:
                                java.util.List r1 = (java.util.List) r1
                                return r1
                            */
                            throw new UnsupportedOperationException("Method not decompiled: com.fossil.e06.i.c.a.C0048a.b.invokeSuspend(java.lang.Object):java.lang.Object");
                        }
                    }

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0048a(List list, fb7 fb7, a aVar) {
                        super(2, fb7);
                        this.$it = list;
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0048a aVar = new C0048a(this.$it, fb7, this.this$0);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                        return ((C0048a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        boolean a;
                        Object a2 = nb7.a();
                        int i = this.label;
                        if (i == 0) {
                            t87.a(obj);
                            yi7 yi7 = this.p$;
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            local.d("HomeDianaCustomizePresenter", "onObserve on preset list change " + this.$it);
                            List a3 = ea7.a((Iterable) this.$it, (Comparator) new C0049a());
                            boolean a4 = ee7.a(a3, this.this$0.a.a.this$0.i) ^ true;
                            int itemCount = this.this$0.a.a.this$0.u.getItemCount();
                            if (a4 || a3.size() != itemCount - 1) {
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                local2.e("HomeDianaCustomizePresenter", "process change - " + a4 + " - itemCount: " + itemCount + " - sortedPresetSize: " + a3.size());
                                if (this.this$0.a.a.this$0.n == 2) {
                                    this.this$0.a.a.this$0.a(a3);
                                } else {
                                    this.this$0.a.a.this$0.r = w87.a(pb7.a(true), a3);
                                }
                                return i97.a;
                            }
                            ti7 b2 = qj7.b();
                            b bVar = new b(this, null);
                            this.L$0 = yi7;
                            this.L$1 = a3;
                            this.I$0 = itemCount;
                            this.label = 1;
                            obj = vh7.a(b2, bVar, this);
                            if (obj == a2) {
                                return a2;
                            }
                        } else if (i == 1) {
                            List list = (List) this.L$1;
                            yi7 yi72 = (yi7) this.L$0;
                            t87.a(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        List<dz5> list2 = (List) obj;
                        if ((!list2.isEmpty()) && (a = xg5.a(xg5.b, ((vr5) this.this$0.a.a.this$0.u).getContext(), (List) list2.get(0).c(), false, false, false, (Integer) null, 56, (Object) null)) != this.this$0.a.a.this$0.s) {
                            this.this$0.a.a.this$0.s = a;
                            this.this$0.a.a.this$0.u.a(list2, this.this$0.a.a.this$0.q);
                        }
                        return i97.a;
                    }
                }

                @DexIgnore
                public a(c cVar) {
                    this.a = cVar;
                }

                @DexIgnore
                /* renamed from: a */
                public final void onChanged(List<DianaPreset> list) {
                    if (list != null) {
                        ik7 unused = xh7.b(this.a.a.this$0.e(), null, null, new C0048a(list, null, this), 3, null);
                    }
                }
            }

            @DexIgnore
            public c(i iVar) {
                this.a = iVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(String str) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDianaCustomizePresenter", "on active serial change " + str + " mCurrentHomeTab " + this.a.this$0.n);
                if (TextUtils.isEmpty(str) || !FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
                    this.a.this$0.u.a(true);
                    return;
                }
                e06 e06 = this.a.this$0;
                DianaPresetRepository l = e06.y;
                if (str != null) {
                    e06.e = l.getPresetListAsLiveData(str);
                    this.a.this$0.e.a((LifecycleOwner) this.a.this$0.u, new a(this));
                    this.a.this$0.u.a(false);
                    return;
                }
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(e06 e06, fb7 fb7) {
            super(2, fb7);
            this.this$0 = e06;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            i iVar = new i(this.this$0, fb7);
            iVar.p$ = (yi7) obj;
            return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((i) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (this.this$0.g.isEmpty() || this.this$0.h.isEmpty()) {
                    FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "init comps, apps");
                    ti7 c2 = this.this$0.b();
                    a aVar = new a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    if (vh7.a(c2, aVar, this) == a2) {
                        return a2;
                    }
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            LiveData i2 = this.this$0.f;
            d06 u = this.this$0.u;
            if (u != null) {
                i2.a((vr5) u, new b(this));
                this.this$0.k.a((LifecycleOwner) this.this$0.u, new c(this));
                this.this$0.z.f();
                nj5.d.a(CommunicateMode.SET_PRESET_APPS_DATA);
                return i97.a;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public e06(d06 d06, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, RingStyleRepository ringStyleRepository, DianaPresetRepository dianaPresetRepository, z46 z46, og5 og5, CustomizeRealDataRepository customizeRealDataRepository, UserRepository userRepository, WatchFaceRepository watchFaceRepository, PortfolioApp portfolioApp) {
        ee7.b(d06, "mView");
        ee7.b(watchAppRepository, "mWatchAppRepository");
        ee7.b(complicationRepository, "mComplicationRepository");
        ee7.b(ringStyleRepository, "mRingStyleRepository");
        ee7.b(dianaPresetRepository, "mDianaPresetRepository");
        ee7.b(z46, "mSetDianaPresetToWatchUseCase");
        ee7.b(og5, "mCustomizeRealDataManager");
        ee7.b(customizeRealDataRepository, "mCustomizeRealDataRepository");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(watchFaceRepository, "mWatchFaceRepository");
        ee7.b(portfolioApp, "mApp");
        this.u = d06;
        this.v = watchAppRepository;
        this.w = complicationRepository;
        this.x = ringStyleRepository;
        this.y = dianaPresetRepository;
        this.z = z46;
        this.A = og5;
        this.B = customizeRealDataRepository;
        this.C = userRepository;
        this.D = watchFaceRepository;
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "init comps, apps first time");
        this.k = portfolioApp.d();
    }

    @DexIgnore
    public final void b(String str) {
        T t2;
        T t3;
        List<DianaPreset> a2 = this.e.a();
        if (a2 != null) {
            a2.isEmpty();
        }
        Iterator<T> it = this.i.iterator();
        while (true) {
            t2 = null;
            if (!it.hasNext()) {
                t3 = null;
                break;
            }
            t3 = it.next();
            if (ee7.a((Object) t3.getId(), (Object) str)) {
                break;
            }
        }
        T t4 = t3;
        if (t4 != null) {
            Iterator<T> it2 = this.i.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                T next = it2.next();
                if (next.isActive()) {
                    t2 = next;
                    break;
                }
            }
            T t5 = t2;
            this.u.k();
            FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "delete activePreset " + ((Object) t5) + " set preset " + ((Object) t4) + " as active first");
            this.z.a(new z46.c(t4), new d(t5, this, str));
        }
    }

    @DexIgnore
    public final void c(int i2) {
        this.l = i2;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ik7 unused = xh7.b(e(), null, null, new i(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        try {
            MutableLiveData<String> mutableLiveData = this.k;
            d06 d06 = this.u;
            if (d06 != null) {
                mutableLiveData.a((vr5) d06);
                this.e.a((LifecycleOwner) this.u);
                this.f.a((LifecycleOwner) this.u);
                this.z.g();
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizePresenter", "stop fail due to " + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.c06
    public void h() {
        ik7 unused = xh7.b(e(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.c06
    public CopyOnWriteArrayList<CustomizeRealData> i() {
        return this.m;
    }

    @DexIgnore
    @Override // com.fossil.c06
    public void j() {
        DianaPreset dianaPreset;
        xg5 xg5 = xg5.b;
        d06 d06 = this.u;
        if (d06 == null) {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        } else if (xg5.a(xg5, ((vr5) d06).getContext(), xg5.a.SET_BLE_COMMAND, false, false, false, (Integer) null, 60, (Object) null) && (dianaPreset = this.j) != null) {
            this.u.k();
            this.z.a(new z46.c(dianaPreset), new g(this));
        }
    }

    @DexIgnore
    public final int k() {
        return this.l;
    }

    @DexIgnore
    public void l() {
        this.u.a(this);
    }

    @DexIgnore
    public final void m() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "showPresets - size=" + this.i.size());
        ik7 unused = xh7.b(e(), null, null, new h(this, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0106  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x01ee  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x01fc  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.portfolio.platform.data.model.diana.preset.DianaPreset r24, com.fossil.fb7<? super com.fossil.dz5> r25) {
        /*
            r23 = this;
            r0 = r23
            r1 = r25
            boolean r2 = r1 instanceof com.fossil.e06.b
            if (r2 == 0) goto L_0x0017
            r2 = r1
            com.fossil.e06$b r2 = (com.fossil.e06.b) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.fossil.e06$b r2 = new com.fossil.e06$b
            r2.<init>(r0, r1)
        L_0x001c:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            r5 = 1
            if (r4 == 0) goto L_0x0091
            if (r4 != r5) goto L_0x0089
            java.lang.Object r4 = r2.L$17
            java.util.ArrayList r4 = (java.util.ArrayList) r4
            java.lang.Object r7 = r2.L$16
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r8 = r2.L$15
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r9 = r2.L$14
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r10 = r2.L$13
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r11 = r2.L$12
            com.portfolio.platform.data.model.diana.Complication r11 = (com.portfolio.platform.data.model.diana.Complication) r11
            java.lang.Object r11 = r2.L$11
            com.portfolio.platform.data.model.diana.Complication r11 = (com.portfolio.platform.data.model.diana.Complication) r11
            java.lang.Object r11 = r2.L$10
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r11 = r2.L$9
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r11 = r2.L$8
            java.util.Iterator r11 = (java.util.Iterator) r11
            java.lang.Object r12 = r2.L$7
            com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r12 = (com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper) r12
            java.lang.Object r13 = r2.L$6
            java.util.List r13 = (java.util.List) r13
            java.lang.Object r14 = r2.L$5
            java.util.ArrayList r14 = (java.util.ArrayList) r14
            java.lang.Object r15 = r2.L$4
            java.util.ArrayList r15 = (java.util.ArrayList) r15
            java.lang.Object r6 = r2.L$3
            java.util.ArrayList r6 = (java.util.ArrayList) r6
            java.lang.Object r5 = r2.L$2
            java.util.ArrayList r5 = (java.util.ArrayList) r5
            r17 = r3
            java.lang.Object r3 = r2.L$1
            com.portfolio.platform.data.model.diana.preset.DianaPreset r3 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r3
            r24 = r3
            java.lang.Object r3 = r2.L$0
            com.fossil.e06 r3 = (com.fossil.e06) r3
            com.fossil.t87.a(r1)
            r20 = r12
            r21 = r13
            r16 = r15
            r0 = 1
            r12 = r10
            r15 = r14
            r10 = r8
            r14 = r11
            r11 = r9
            r9 = r7
            r7 = r24
            goto L_0x01c9
        L_0x0089:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0091:
            r17 = r3
            com.fossil.t87.a(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "convertPresetToDianaPresetConfigWrapper watchAppsSize "
            r3.append(r4)
            java.util.ArrayList<com.portfolio.platform.data.model.diana.WatchApp> r4 = r0.h
            int r4 = r4.size()
            r3.append(r4)
            java.lang.String r4 = " compsSize "
            r3.append(r4)
            java.util.ArrayList<com.portfolio.platform.data.model.diana.Complication> r4 = r0.g
            int r4 = r4.size()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = "HomeDianaCustomizePresenter"
            r1.d(r4, r3)
            java.util.ArrayList r1 = r24.getComplications()
            java.util.ArrayList r3 = r24.getWatchapps()
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            com.fossil.ve5 r6 = com.fossil.ve5.a
            r7 = r24
            java.util.List r6 = r6.a(r7)
            com.portfolio.platform.data.source.WatchFaceRepository r8 = r0.D
            java.lang.String r9 = r24.getWatchFaceId()
            com.portfolio.platform.data.model.diana.preset.WatchFace r8 = r8.getWatchFaceWithId(r9)
            if (r8 == 0) goto L_0x00f1
            com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r8 = com.fossil.yc5.b(r8, r1)
            goto L_0x00f2
        L_0x00f1:
            r8 = 0
        L_0x00f2:
            java.util.Iterator r9 = r1.iterator()
            r15 = r6
            r14 = r8
            r6 = r3
            r8 = r5
            r3 = r0
            r5 = r1
            r1 = r17
        L_0x00fe:
            boolean r10 = r9.hasNext()
            java.lang.String r11 = ""
            if (r10 == 0) goto L_0x01ee
            java.lang.Object r10 = r9.next()
            com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r10 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r10
            java.lang.String r13 = r10.component1()
            java.lang.String r10 = r10.component2()
            java.util.ArrayList<com.portfolio.platform.data.model.diana.Complication> r12 = r3.g
            java.util.Iterator r12 = r12.iterator()
        L_0x011a:
            boolean r17 = r12.hasNext()
            if (r17 == 0) goto L_0x013e
            java.lang.Object r17 = r12.next()
            r18 = r17
            com.portfolio.platform.data.model.diana.Complication r18 = (com.portfolio.platform.data.model.diana.Complication) r18
            java.lang.String r0 = r18.getComplicationId()
            boolean r0 = com.fossil.ee7.a(r0, r10)
            java.lang.Boolean r0 = com.fossil.pb7.a(r0)
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x013b
            goto L_0x0140
        L_0x013b:
            r0 = r23
            goto L_0x011a
        L_0x013e:
            r17 = 0
        L_0x0140:
            r0 = r17
            com.portfolio.platform.data.model.diana.Complication r0 = (com.portfolio.platform.data.model.diana.Complication) r0
            if (r0 == 0) goto L_0x01e4
            java.lang.String r12 = r0.getComplicationId()
            java.lang.String r17 = r0.getIcon()
            if (r17 == 0) goto L_0x0152
            r11 = r17
        L_0x0152:
            com.portfolio.platform.PortfolioApp$a r17 = com.portfolio.platform.PortfolioApp.g0
            r18 = r1
            com.portfolio.platform.PortfolioApp r1 = r17.c()
            r24 = r12
            java.lang.String r12 = r0.getNameKey()
            r17 = r11
            java.lang.String r11 = r0.getName()
            java.lang.String r1 = com.fossil.ig5.a(r1, r12, r11)
            com.fossil.og5 r11 = r3.A
            com.portfolio.platform.data.model.MFUser r12 = r3.o
            r19 = r12
            java.util.concurrent.CopyOnWriteArrayList<com.portfolio.platform.data.model.CustomizeRealData> r12 = r3.m
            java.lang.String r20 = r0.getComplicationId()
            r2.L$0 = r3
            r2.L$1 = r7
            r2.L$2 = r5
            r2.L$3 = r6
            r2.L$4 = r4
            r2.L$5 = r8
            r2.L$6 = r15
            r2.L$7 = r14
            r2.L$8 = r9
            r2.L$9 = r13
            r2.L$10 = r10
            r2.L$11 = r0
            r2.L$12 = r0
            r2.L$13 = r13
            r2.L$14 = r1
            r0 = r17
            r2.L$15 = r0
            r10 = r24
            r2.L$16 = r10
            r2.L$17 = r4
            r0 = 1
            r2.label = r0
            r16 = r10
            r10 = r11
            r11 = r19
            r19 = r13
            r13 = r20
            r20 = r14
            r14 = r7
            r21 = r15
            r15 = r2
            java.lang.Object r10 = r10.a(r11, r12, r13, r14, r15)
            r11 = r18
            if (r10 != r11) goto L_0x01b9
            return r11
        L_0x01b9:
            r15 = r8
            r14 = r9
            r9 = r16
            r12 = r19
            r16 = r4
            r22 = r11
            r11 = r1
            r1 = r10
            r10 = r17
            r17 = r22
        L_0x01c9:
            r13 = r1
            java.lang.String r13 = (java.lang.String) r13
            com.fossil.fz5 r1 = new com.fossil.fz5
            r8 = r1
            r8.<init>(r9, r10, r11, r12, r13)
            boolean r1 = r4.add(r1)
            com.fossil.pb7.a(r1)
            r9 = r14
            r8 = r15
            r4 = r16
            r1 = r17
            r14 = r20
            r15 = r21
            goto L_0x01ea
        L_0x01e4:
            r11 = r1
            r20 = r14
            r21 = r15
            r0 = 1
        L_0x01ea:
            r0 = r23
            goto L_0x00fe
        L_0x01ee:
            r20 = r14
            r21 = r15
            java.util.Iterator r0 = r6.iterator()
        L_0x01f6:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0268
            java.lang.Object r1 = r0.next()
            com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r1 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r1
            java.lang.String r16 = r1.component1()
            java.lang.String r1 = r1.component2()
            java.util.ArrayList<com.portfolio.platform.data.model.diana.WatchApp> r2 = r3.h
            java.util.Iterator r2 = r2.iterator()
        L_0x0210:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L_0x0230
            java.lang.Object r5 = r2.next()
            r6 = r5
            com.portfolio.platform.data.model.diana.WatchApp r6 = (com.portfolio.platform.data.model.diana.WatchApp) r6
            java.lang.String r6 = r6.getWatchappId()
            boolean r6 = com.fossil.ee7.a(r6, r1)
            java.lang.Boolean r6 = com.fossil.pb7.a(r6)
            boolean r6 = r6.booleanValue()
            if (r6 == 0) goto L_0x0210
            goto L_0x0231
        L_0x0230:
            r5 = 0
        L_0x0231:
            com.portfolio.platform.data.model.diana.WatchApp r5 = (com.portfolio.platform.data.model.diana.WatchApp) r5
            if (r5 == 0) goto L_0x01f6
            com.fossil.fz5 r1 = new com.fossil.fz5
            java.lang.String r13 = r5.getWatchappId()
            java.lang.String r2 = r5.getIcon()
            if (r2 == 0) goto L_0x0243
            r14 = r2
            goto L_0x0244
        L_0x0243:
            r14 = r11
        L_0x0244:
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            java.lang.String r6 = r5.getNameKey()
            java.lang.String r5 = r5.getName()
            java.lang.String r15 = com.fossil.ig5.a(r2, r6, r5)
            r17 = 0
            r18 = 16
            r19 = 0
            r12 = r1
            r12.<init>(r13, r14, r15, r16, r17, r18, r19)
            boolean r1 = r8.add(r1)
            com.fossil.pb7.a(r1)
            goto L_0x01f6
        L_0x0268:
            com.fossil.dz5 r0 = new com.fossil.dz5
            java.lang.String r9 = r7.getId()
            java.lang.String r10 = r7.getName()
            boolean r14 = r7.isActive()
            r5 = r8
            r8 = r0
            r11 = r4
            r12 = r5
            r13 = r21
            r15 = r20
            r8.<init>(r9, r10, r11, r12, r13, r14, r15)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.e06.a(com.portfolio.platform.data.model.diana.preset.DianaPreset, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizePresenter", "onHomeTabChange - tab: " + i2);
        this.n = i2;
        if (i2 == 2 && this.r.getFirst().booleanValue()) {
            List<DianaPreset> list = (List) this.r.getSecond();
            if (list != null) {
                a(list);
            }
            this.r = w87.a(false, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.c06
    public void a(int i2) {
        boolean z2 = i2 == this.i.size();
        if (this.i.size() > i2) {
            this.l = i2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "changePresetPosition " + this.l);
            DianaPreset dianaPreset = this.i.get(this.l);
            this.j = dianaPreset;
            if (dianaPreset != null) {
                this.u.d(dianaPreset.isActive(), z2);
            }
        } else {
            this.u.d(false, z2);
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("HomeDianaCustomizePresenter", "viewPos " + i2 + " presenterPos " + this.l + " size " + this.i.size());
    }

    @DexIgnore
    @Override // com.fossil.c06
    public void a(String str, String str2) {
        ee7.b(str, "name");
        ee7.b(str2, "presetId");
        ik7 unused = xh7.b(e(), null, null, new f(this, str2, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.c06
    public void a(String str) {
        ee7.b(str, "nextActivePresetId");
        DianaPreset dianaPreset = this.j;
        if (dianaPreset == null) {
            return;
        }
        if (dianaPreset.isActive()) {
            b(str);
        } else {
            ik7 unused = xh7.b(e(), null, null, new e(dianaPreset, null, this, str), 3, null);
        }
    }

    @DexIgnore
    public void a(int i2, int i3, Intent intent) {
        this.t = false;
        if (i2 == 100 && i3 == -1) {
            this.u.d(0);
        }
    }

    @DexIgnore
    @Override // com.fossil.c06
    public void a(dz5 dz5, List<? extends a9<View, String>> list, List<? extends a9<CustomizeWidget, String>> list2) {
        ee7.b(list, "views");
        ee7.b(list2, "customizeWidgetViews");
        if (!this.t) {
            this.t = true;
            this.u.a(dz5, list, list2);
        }
    }

    @DexIgnore
    public final void a(List<DianaPreset> list) {
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "doShowingPreset");
        this.i.clear();
        this.i.addAll(list);
        int size = this.i.size();
        int i2 = this.l;
        if (size > i2 && i2 > 0) {
            this.j = this.i.get(i2);
        }
        m();
    }
}
