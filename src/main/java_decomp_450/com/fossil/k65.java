package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class k65 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView q;
    @DexIgnore
    public /* final */ ImageView r;

    @DexIgnore
    public k65(Object obj, View view, int i, ImageView imageView, ImageView imageView2) {
        super(obj, view, i);
        this.q = imageView;
        this.r = imageView2;
    }
}
