package com.fossil;

import com.fossil.bw2;
import com.fossil.tp2;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rp2 extends bw2<rp2, a> implements lx2 {
    @DexIgnore
    public static /* final */ rp2 zzi;
    @DexIgnore
    public static volatile wx2<rp2> zzj;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public jw2<tp2> zzd; // = bw2.o();
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public long zzf;
    @DexIgnore
    public long zzg;
    @DexIgnore
    public int zzh;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<rp2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(rp2.zzi);
        }

        @DexIgnore
        public final tp2 a(int i) {
            return ((rp2) ((bw2.a) this).b).b(i);
        }

        @DexIgnore
        public final a b(int i) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((rp2) ((bw2.a) this).b).c(i);
            return this;
        }

        @DexIgnore
        public final int o() {
            return ((rp2) ((bw2.a) this).b).p();
        }

        @DexIgnore
        public final a p() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((rp2) ((bw2.a) this).b).y();
            return this;
        }

        @DexIgnore
        public final String q() {
            return ((rp2) ((bw2.a) this).b).q();
        }

        @DexIgnore
        public final long r() {
            return ((rp2) ((bw2.a) this).b).u();
        }

        @DexIgnore
        public final List<tp2> zza() {
            return Collections.unmodifiableList(((rp2) ((bw2.a) this).b).zza());
        }

        @DexIgnore
        public final long zzf() {
            return ((rp2) ((bw2.a) this).b).s();
        }

        @DexIgnore
        public /* synthetic */ a(op2 op2) {
            this();
        }

        @DexIgnore
        public final a a(int i, tp2 tp2) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((rp2) ((bw2.a) this).b).a(i, tp2);
            return this;
        }

        @DexIgnore
        public final a b(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((rp2) ((bw2.a) this).b).b(j);
            return this;
        }

        @DexIgnore
        public final a a(int i, tp2.a aVar) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((rp2) ((bw2.a) this).b).a(i, (tp2) ((bw2) aVar.g()));
            return this;
        }

        @DexIgnore
        public final a a(tp2 tp2) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((rp2) ((bw2.a) this).b).a(tp2);
            return this;
        }

        @DexIgnore
        public final a a(tp2.a aVar) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((rp2) ((bw2.a) this).b).a((tp2) ((bw2) aVar.g()));
            return this;
        }

        @DexIgnore
        public final a a(Iterable<? extends tp2> iterable) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((rp2) ((bw2.a) this).b).a(iterable);
            return this;
        }

        @DexIgnore
        public final a a(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((rp2) ((bw2.a) this).b).a(str);
            return this;
        }

        @DexIgnore
        public final a a(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((rp2) ((bw2.a) this).b).a(j);
            return this;
        }
    }

    /*
    static {
        rp2 rp2 = new rp2();
        zzi = rp2;
        bw2.a(rp2.class, rp2);
    }
    */

    @DexIgnore
    public static a z() {
        return (a) zzi.e();
    }

    @DexIgnore
    public final void a(int i, tp2 tp2) {
        tp2.getClass();
        x();
        this.zzd.set(i, tp2);
    }

    @DexIgnore
    public final tp2 b(int i) {
        return this.zzd.get(i);
    }

    @DexIgnore
    public final void c(int i) {
        x();
        this.zzd.remove(i);
    }

    @DexIgnore
    public final int p() {
        return this.zzd.size();
    }

    @DexIgnore
    public final String q() {
        return this.zze;
    }

    @DexIgnore
    public final boolean r() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final long s() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean t() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final long u() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean v() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final int w() {
        return this.zzh;
    }

    @DexIgnore
    public final void x() {
        jw2<tp2> jw2 = this.zzd;
        if (!jw2.zza()) {
            this.zzd = bw2.a(jw2);
        }
    }

    @DexIgnore
    public final void y() {
        this.zzd = bw2.o();
    }

    @DexIgnore
    public final List<tp2> zza() {
        return this.zzd;
    }

    @DexIgnore
    public final void b(long j) {
        this.zzc |= 4;
        this.zzg = j;
    }

    @DexIgnore
    public final void a(tp2 tp2) {
        tp2.getClass();
        x();
        this.zzd.add(tp2);
    }

    @DexIgnore
    public final void a(Iterable<? extends tp2> iterable) {
        x();
        ju2.a(iterable, this.zzd);
    }

    @DexIgnore
    public final void a(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zze = str;
    }

    @DexIgnore
    public final void a(long j) {
        this.zzc |= 2;
        this.zzf = j;
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (op2.a[i - 1]) {
            case 1:
                return new rp2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0000\u0001\u001b\u0002\u1008\u0000\u0003\u1002\u0001\u0004\u1002\u0002\u0005\u1004\u0003", new Object[]{"zzc", "zzd", tp2.class, "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                wx2<rp2> wx2 = zzj;
                if (wx2 == null) {
                    synchronized (rp2.class) {
                        wx2 = zzj;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzi);
                            zzj = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
