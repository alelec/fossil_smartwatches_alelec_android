package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ce5 implements MembersInjector<be5> {
    @DexIgnore
    public static void a(be5 be5, ch5 ch5) {
        be5.a = ch5;
    }

    @DexIgnore
    public static void a(be5 be5, DeviceRepository deviceRepository) {
        be5.b = deviceRepository;
    }
}
