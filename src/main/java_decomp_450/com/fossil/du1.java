package com.fossil;

import com.fossil.ku1;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class du1 extends ku1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Integer b;
    @DexIgnore
    public /* final */ ju1 c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ Map<String, String> f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ku1.a {
        @DexIgnore
        public String a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public ju1 c;
        @DexIgnore
        public Long d;
        @DexIgnore
        public Long e;
        @DexIgnore
        public Map<String, String> f;

        @DexIgnore
        @Override // com.fossil.ku1.a
        public ku1.a a(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null transportName");
        }

        @DexIgnore
        @Override // com.fossil.ku1.a
        public ku1.a b(long j) {
            this.e = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ku1.a
        public Map<String, String> b() {
            Map<String, String> map = this.f;
            if (map != null) {
                return map;
            }
            throw new IllegalStateException("Property \"autoMetadata\" has not been set");
        }

        @DexIgnore
        @Override // com.fossil.ku1.a
        public ku1.a a(Integer num) {
            this.b = num;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ku1.a
        public ku1.a a(ju1 ju1) {
            if (ju1 != null) {
                this.c = ju1;
                return this;
            }
            throw new NullPointerException("Null encodedPayload");
        }

        @DexIgnore
        @Override // com.fossil.ku1.a
        public ku1.a a(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ku1.a
        public ku1.a a(Map<String, String> map) {
            if (map != null) {
                this.f = map;
                return this;
            }
            throw new NullPointerException("Null autoMetadata");
        }

        @DexIgnore
        @Override // com.fossil.ku1.a
        public ku1 a() {
            String str = "";
            if (this.a == null) {
                str = str + " transportName";
            }
            if (this.c == null) {
                str = str + " encodedPayload";
            }
            if (this.d == null) {
                str = str + " eventMillis";
            }
            if (this.e == null) {
                str = str + " uptimeMillis";
            }
            if (this.f == null) {
                str = str + " autoMetadata";
            }
            if (str.isEmpty()) {
                return new du1(this.a, this.b, this.c, this.d.longValue(), this.e.longValue(), this.f);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.ku1
    public Map<String, String> a() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.ku1
    public Integer b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ku1
    public ju1 c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ku1
    public long d() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Integer num;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ku1)) {
            return false;
        }
        ku1 ku1 = (ku1) obj;
        if (!this.a.equals(ku1.f()) || ((num = this.b) != null ? !num.equals(ku1.b()) : ku1.b() != null) || !this.c.equals(ku1.c()) || this.d != ku1.d() || this.e != ku1.g() || !this.f.equals(ku1.a())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ku1
    public String f() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.ku1
    public long g() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        Integer num = this.b;
        int hashCode2 = num == null ? 0 : num.hashCode();
        long j = this.d;
        long j2 = this.e;
        return ((((((((hashCode ^ hashCode2) * 1000003) ^ this.c.hashCode()) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ this.f.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "EventInternal{transportName=" + this.a + ", code=" + this.b + ", encodedPayload=" + this.c + ", eventMillis=" + this.d + ", uptimeMillis=" + this.e + ", autoMetadata=" + this.f + "}";
    }

    @DexIgnore
    public du1(String str, Integer num, ju1 ju1, long j, long j2, Map<String, String> map) {
        this.a = str;
        this.b = num;
        this.c = ju1;
        this.d = j;
        this.e = j2;
        this.f = map;
    }
}
