package com.fossil;

import com.facebook.internal.NativeProtocol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bc7 {
    @DexIgnore
    public static /* final */ ac7 a;

    /*
    static {
        ac7 ac7;
        Object newInstance;
        Object newInstance2;
        int a2 = a();
        if (a2 >= 65544) {
            try {
                Object newInstance3 = Class.forName("com.fossil.ec7").newInstance();
                ee7.a(newInstance3, "Class.forName(\"kotlin.in\u2026entations\").newInstance()");
                if (newInstance3 != null) {
                    try {
                        ac7 = (ac7) newInstance3;
                        a = ac7;
                    } catch (ClassCastException e) {
                        ClassLoader classLoader = newInstance3.getClass().getClassLoader();
                        ClassLoader classLoader2 = ac7.class.getClassLoader();
                        Throwable initCause = new ClassCastException("Instance classloader: " + classLoader + ", base type classloader: " + classLoader2).initCause(e);
                        ee7.a((Object) initCause, "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)");
                        throw initCause;
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type kotlin.internal.PlatformImplementations");
                }
            } catch (ClassNotFoundException unused) {
                try {
                    newInstance2 = Class.forName("kotlin.internal.JRE8PlatformImplementations").newInstance();
                    ee7.a(newInstance2, "Class.forName(\"kotlin.in\u2026entations\").newInstance()");
                    if (newInstance2 != null) {
                        ac7 = (ac7) newInstance2;
                    } else {
                        throw new x87("null cannot be cast to non-null type kotlin.internal.PlatformImplementations");
                    }
                } catch (ClassNotFoundException unused2) {
                }
            } catch (ClassCastException e2) {
                ClassLoader classLoader3 = newInstance2.getClass().getClassLoader();
                ClassLoader classLoader4 = ac7.class.getClassLoader();
                Throwable initCause2 = new ClassCastException("Instance classloader: " + classLoader3 + ", base type classloader: " + classLoader4).initCause(e2);
                ee7.a((Object) initCause2, "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)");
                throw initCause2;
            }
        }
        if (a2 >= 65543) {
            try {
                Object newInstance4 = Class.forName("com.fossil.dc7").newInstance();
                ee7.a(newInstance4, "Class.forName(\"kotlin.in\u2026entations\").newInstance()");
                if (newInstance4 != null) {
                    try {
                        ac7 = (ac7) newInstance4;
                        a = ac7;
                    } catch (ClassCastException e3) {
                        ClassLoader classLoader5 = newInstance4.getClass().getClassLoader();
                        ClassLoader classLoader6 = ac7.class.getClassLoader();
                        Throwable initCause3 = new ClassCastException("Instance classloader: " + classLoader5 + ", base type classloader: " + classLoader6).initCause(e3);
                        ee7.a((Object) initCause3, "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)");
                        throw initCause3;
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type kotlin.internal.PlatformImplementations");
                }
            } catch (ClassNotFoundException unused3) {
                try {
                    newInstance = Class.forName("kotlin.internal.JRE7PlatformImplementations").newInstance();
                    ee7.a(newInstance, "Class.forName(\"kotlin.in\u2026entations\").newInstance()");
                    if (newInstance != null) {
                        ac7 = (ac7) newInstance;
                    } else {
                        throw new x87("null cannot be cast to non-null type kotlin.internal.PlatformImplementations");
                    }
                } catch (ClassNotFoundException unused4) {
                }
            } catch (ClassCastException e4) {
                ClassLoader classLoader7 = newInstance.getClass().getClassLoader();
                ClassLoader classLoader8 = ac7.class.getClassLoader();
                Throwable initCause4 = new ClassCastException("Instance classloader: " + classLoader7 + ", base type classloader: " + classLoader8).initCause(e4);
                ee7.a((Object) initCause4, "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)");
                throw initCause4;
            }
        }
        ac7 = new ac7();
        a = ac7;
    }
    */

    @DexIgnore
    public static final int a() {
        String property = System.getProperty("java.specification.version");
        if (property == null) {
            return NativeProtocol.MESSAGE_GET_LIKE_STATUS_REQUEST;
        }
        int a2 = nh7.a((CharSequence) property, '.', 0, false, 6, (Object) null);
        if (a2 < 0) {
            try {
                return Integer.parseInt(property) * 65536;
            } catch (NumberFormatException unused) {
                return NativeProtocol.MESSAGE_GET_LIKE_STATUS_REQUEST;
            }
        } else {
            int i = a2 + 1;
            int a3 = nh7.a((CharSequence) property, '.', i, false, 4, (Object) null);
            if (a3 < 0) {
                a3 = property.length();
            }
            if (property != null) {
                String substring = property.substring(0, a2);
                ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                if (property != null) {
                    String substring2 = property.substring(i, a3);
                    ee7.a((Object) substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    try {
                        return (Integer.parseInt(substring) * 65536) + Integer.parseInt(substring2);
                    } catch (NumberFormatException unused2) {
                        return NativeProtocol.MESSAGE_GET_LIKE_STATUS_REQUEST;
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
        }
    }
}
