package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pg7<T, R> implements hg7<R> {
    @DexIgnore
    public /* final */ hg7<T> a;
    @DexIgnore
    public /* final */ gd7<T, R> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<R>, ye7 {
        @DexIgnore
        public /* final */ Iterator<T> a;
        @DexIgnore
        public /* final */ /* synthetic */ pg7 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(pg7 pg7) {
            this.b = pg7;
            this.a = pg7.a.iterator();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public R next() {
            return (R) this.b.b.invoke(this.a.next());
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.hg7<? extends T> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.gd7<? super T, ? extends R> */
    /* JADX WARN: Multi-variable type inference failed */
    public pg7(hg7<? extends T> hg7, gd7<? super T, ? extends R> gd7) {
        ee7.b(hg7, "sequence");
        ee7.b(gd7, "transformer");
        this.a = hg7;
        this.b = gd7;
    }

    @DexIgnore
    @Override // com.fossil.hg7
    public Iterator<R> iterator() {
        return new a(this);
    }
}
