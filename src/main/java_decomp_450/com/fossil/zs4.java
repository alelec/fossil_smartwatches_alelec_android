package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.fossil.cy6;
import com.fossil.eu4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zs4 extends go5 implements gr5, eu4.a, cy6.g {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public qw6<cy4> g;
    @DexIgnore
    public ct4 h;
    @DexIgnore
    public ys4 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return zs4.p;
        }

        @DexIgnore
        public final zs4 b() {
            return new zs4();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ zs4 a;

        @DexIgnore
        public b(zs4 zs4) {
            this.a = zs4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView;
            cy4 cy4 = (cy4) zs4.a(this.a).a();
            if (!(cy4 == null || (flexibleTextView = cy4.r) == null)) {
                flexibleTextView.setVisibility(8);
            }
            zs4.c(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends eu4 {
        @DexIgnore
        public /* final */ /* synthetic */ cy4 p;
        @DexIgnore
        public /* final */ /* synthetic */ zs4 q;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(cy4 cy4, RecyclerView recyclerView, zs4 zs4) {
            super(recyclerView);
            this.p = cy4;
            this.q = zs4;
        }

        @DexIgnore
        @Override // com.fossil.eu4
        public void a(RecyclerView.ViewHolder viewHolder, List<eu4.c> list) {
            ee7.b(viewHolder, "viewHolder");
            ee7.b(list, "revealedButtons");
            int adapterPosition = viewHolder.getAdapterPosition();
            if (adapterPosition != -1) {
                Object a = zs4.b(this.q).a(adapterPosition);
                Integer num = null;
                if (!(a instanceof un4)) {
                    a = null;
                }
                un4 un4 = (un4) a;
                FLogger.INSTANCE.getLocal().e(zs4.q.a(), "friend: " + un4);
                if (un4 != null) {
                    num = Integer.valueOf(un4.c());
                }
                if (num != null && num.intValue() == 2) {
                    RecyclerView recyclerView = this.p.t;
                    ee7.a((Object) recyclerView, "rvFriends");
                    list.add(new eu4.c("", 2131231014, v6.a(recyclerView.getContext(), 2131099706), eu4.b.BLOCK, this.q));
                } else if (num != null && num.intValue() == 0) {
                    if (un4.f() == 0) {
                        RecyclerView recyclerView2 = this.p.t;
                        ee7.a((Object) recyclerView2, "rvFriends");
                        list.add(new eu4.c("", 2131231038, v6.a(recyclerView2.getContext(), 2131100358), eu4.b.UNFRIEND, this.q));
                        RecyclerView recyclerView3 = this.p.t;
                        ee7.a((Object) recyclerView3, "rvFriends");
                        list.add(new eu4.c("", 2131231014, v6.a(recyclerView3.getContext(), 2131099706), eu4.b.BLOCK, this.q));
                    }
                } else if (num != null && num.intValue() == -1) {
                    RecyclerView recyclerView4 = this.p.t;
                    ee7.a((Object) recyclerView4, "rvFriends");
                    list.add(new eu4.c("", 2131231037, v6.a(recyclerView4.getContext(), 2131099706), eu4.b.UNBLOCK, this.q));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<r87<? extends Boolean, ? extends Boolean>> {
        @DexIgnore
        public /* final */ /* synthetic */ zs4 a;

        @DexIgnore
        public d(zs4 zs4) {
            this.a = zs4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, Boolean> r87) {
            cy4 cy4;
            SwipeRefreshLayout swipeRefreshLayout;
            Boolean first = r87.getFirst();
            Boolean second = r87.getSecond();
            if (!(first == null || (cy4 = (cy4) zs4.a(this.a).a()) == null || (swipeRefreshLayout = cy4.u) == null)) {
                swipeRefreshLayout.setRefreshing(first.booleanValue());
            }
            if (second == null) {
                return;
            }
            if (second.booleanValue()) {
                this.a.b();
            } else {
                this.a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<List<? extends Object>> {
        @DexIgnore
        public /* final */ /* synthetic */ zs4 a;

        @DexIgnore
        public e(zs4 zs4) {
            this.a = zs4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<? extends Object> list) {
            cy4 cy4;
            FlexibleTextView flexibleTextView;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = zs4.q.a();
            local.e(a2, "allFriends: " + list);
            ee7.a((Object) list, "it");
            if (!(!(!list.isEmpty()) || (cy4 = (cy4) zs4.a(this.a).a()) == null || (flexibleTextView = cy4.r) == null)) {
                flexibleTextView.setVisibility(8);
            }
            zs4.b(this.a).a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ zs4 a;

        @DexIgnore
        public f(zs4 zs4) {
            this.a = zs4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            cy4 cy4 = (cy4) zs4.a(this.a).a();
            if (cy4 != null) {
                ee7.a((Object) bool, "it");
                if (bool.booleanValue()) {
                    FlexibleTextView flexibleTextView = cy4.q;
                    ee7.a((Object) flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(0);
                    RecyclerView recyclerView = cy4.t;
                    ee7.a((Object) recyclerView, "rvFriends");
                    recyclerView.setVisibility(8);
                    zs4.b(this.a).c();
                    return;
                }
                FlexibleTextView flexibleTextView2 = cy4.q;
                ee7.a((Object) flexibleTextView2, "ftvEmpty");
                flexibleTextView2.setVisibility(8);
                RecyclerView recyclerView2 = cy4.t;
                ee7.a((Object) recyclerView2, "rvFriends");
                recyclerView2.setVisibility(0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ zs4 a;

        @DexIgnore
        public g(zs4 zs4) {
            this.a = zs4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            cy4 cy4 = (cy4) zs4.a(this.a).a();
            if (cy4 != null) {
                ee7.a((Object) bool, "it");
                if (bool.booleanValue()) {
                    FlexibleTextView flexibleTextView = cy4.r;
                    ee7.a((Object) flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView2 = cy4.r;
                ee7.a((Object) flexibleTextView2, "ftvError");
                flexibleTextView2.setVisibility(8);
                if (this.a.isResumed()) {
                    FlexibleTextView flexibleTextView3 = cy4.r;
                    ee7.a((Object) flexibleTextView3, "ftvError");
                    String a2 = ig5.a(flexibleTextView3.getContext(), 2131886227);
                    FragmentActivity activity = this.a.getActivity();
                    if (activity != null) {
                        Toast.makeText(activity, a2, 0).show();
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements zd<String> {
        @DexIgnore
        public static /* final */ h a; // = new h();

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            qe5.c.a(PortfolioApp.g0.c(), str.hashCode());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ zs4 a;

        @DexIgnore
        public i(zs4 zs4) {
            this.a = zs4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886744);
            String a3 = ig5.a(PortfolioApp.g0.c(), 2131886743);
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            ee7.a((Object) a2, "title");
            ee7.a((Object) a3, "des");
            bx6.a(childFragmentManager, a2, a3);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements zd<r87<? extends String, ? extends String>> {
        @DexIgnore
        public static /* final */ j a; // = new j();

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<String, String> r87) {
            um4.a.b(r87.getFirst(), PortfolioApp.g0.c().w(), r87.getSecond(), PortfolioApp.g0.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements zd<ServerError> {
        @DexIgnore
        public /* final */ /* synthetic */ zs4 a;

        @DexIgnore
        public k(zs4 zs4) {
            this.a = zs4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ServerError serverError) {
            xe5 xe5 = xe5.b;
            ee7.a((Object) serverError, "it");
            Integer code = serverError.getCode();
            String message = serverError.getMessage();
            if (message == null) {
                message = "";
            }
            String b = xe5.b(code, message);
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.d(childFragmentManager, b);
        }
    }

    /*
    static {
        String simpleName = zs4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCFriendTabFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 a(zs4 zs4) {
        qw6<cy4> qw6 = zs4.g;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ys4 b(zs4 zs4) {
        ys4 ys4 = zs4.i;
        if (ys4 != null) {
            return ys4;
        }
        ee7.d("friendAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ct4 c(zs4 zs4) {
        ct4 ct4 = zs4.h;
        if (ct4 != null) {
            return ct4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void f1() {
        ys4 ys4 = new ys4(this);
        this.i = ys4;
        if (ys4 != null) {
            ys4.setHasStableIds(true);
            qw6<cy4> qw6 = this.g;
            if (qw6 != null) {
                cy4 a2 = qw6.a();
                if (a2 != null) {
                    a2.u.setOnRefreshListener(new b(this));
                    RecyclerView recyclerView = a2.t;
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                    ys4 ys42 = this.i;
                    if (ys42 != null) {
                        recyclerView.setAdapter(ys42);
                        RecyclerView recyclerView2 = a2.t;
                        ee7.a((Object) recyclerView2, "rvFriends");
                        new c(a2, recyclerView2, this);
                        return;
                    }
                    ee7.d("friendAdapter");
                    throw null;
                }
                return;
            }
            ee7.d("binding");
            throw null;
        }
        ee7.d("friendAdapter");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        ct4 ct4 = this.h;
        if (ct4 != null) {
            ct4.e().a(getViewLifecycleOwner(), new d(this));
            ct4 ct42 = this.h;
            if (ct42 != null) {
                ct42.a().a(getViewLifecycleOwner(), new e(this));
                ct4 ct43 = this.h;
                if (ct43 != null) {
                    ct43.c().a(getViewLifecycleOwner(), new f(this));
                    ct4 ct44 = this.h;
                    if (ct44 != null) {
                        ct44.d().a(getViewLifecycleOwner(), new g(this));
                        ct4 ct45 = this.h;
                        if (ct45 != null) {
                            ct45.b().a(getViewLifecycleOwner(), h.a);
                            ct4 ct46 = this.h;
                            if (ct46 != null) {
                                ct46.f().a(getViewLifecycleOwner(), new i(this));
                                ct4 ct47 = this.h;
                                if (ct47 != null) {
                                    ct47.g().a(getViewLifecycleOwner(), j.a);
                                    ct4 ct48 = this.h;
                                    if (ct48 != null) {
                                        ct48.h().a(getViewLifecycleOwner(), new k(this));
                                    } else {
                                        ee7.d("viewModel");
                                        throw null;
                                    }
                                } else {
                                    ee7.d("viewModel");
                                    throw null;
                                }
                            } else {
                                ee7.d("viewModel");
                                throw null;
                            }
                        } else {
                            ee7.d("viewModel");
                            throw null;
                        }
                    } else {
                        ee7.d("viewModel");
                        throw null;
                    }
                } else {
                    ee7.d("viewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModel");
                throw null;
            }
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().B().a(this);
        rj4 rj4 = this.f;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(ct4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026TabViewModel::class.java)");
            this.h = (ct4) a2;
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        cy4 cy4 = (cy4) qb.a(layoutInflater, 2131558512, viewGroup, false, a1());
        this.g = new qw6<>(this, cy4);
        ee7.a((Object) cy4, "binding");
        return cy4.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        f1();
        g1();
    }

    @DexIgnore
    @Override // com.fossil.gr5
    public void a(un4 un4) {
        ee7.b(un4, "friend");
        ct4 ct4 = this.h;
        if (ct4 != null) {
            ct4.a(un4, false);
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.gr5
    public void b(un4 un4) {
        ee7.b(un4, "friend");
        ct4 ct4 = this.h;
        if (ct4 != null) {
            ct4.a(un4, true);
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.eu4.a
    public void a(int i2, eu4.b bVar) {
        ee7.b(bVar, "type");
        ys4 ys4 = this.i;
        if (ys4 != null) {
            Object a2 = ys4.a(i2);
            if (!(a2 instanceof un4)) {
                a2 = null;
            }
            un4 un4 = (un4) a2;
            FLogger.INSTANCE.getLocal().e(p, "position: " + i2 + " - type: " + bVar + " - friend: " + un4);
            if (un4 != null) {
                int i3 = at4.a[bVar.ordinal()];
                if (i3 == 1) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("friend_extra", un4);
                    bx6 bx6 = bx6.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    ee7.a((Object) childFragmentManager, "childFragmentManager");
                    String a3 = ig5.a(PortfolioApp.g0.c(), 2131886297);
                    ee7.a((Object) a3, "LanguageHelper.getString\u2026friendThisUserTheyWillBe)");
                    bx6.a(childFragmentManager, bundle, "UN_FRIEND", a3);
                } else if (i3 == 2) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putParcelable("friend_extra", un4);
                    bx6 bx62 = bx6.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    ee7.a((Object) childFragmentManager2, "childFragmentManager");
                    String a4 = ig5.a(PortfolioApp.g0.c(), 2131886287);
                    ee7.a((Object) a4, "LanguageHelper.getString\u2026ext__AreYouSureYouWantTo)");
                    bx62.a(childFragmentManager2, bundle2, "BLOCK_FRIEND", a4);
                } else if (i3 == 3) {
                    ct4 ct4 = this.h;
                    if (ct4 != null) {
                        ct4.c(un4);
                    } else {
                        ee7.d("viewModel");
                        throw null;
                    }
                } else if (i3 == 4) {
                    Bundle bundle3 = new Bundle();
                    bundle3.putParcelable("friend_extra", un4);
                    bx6 bx63 = bx6.c;
                    FragmentManager childFragmentManager3 = getChildFragmentManager();
                    ee7.a((Object) childFragmentManager3, "childFragmentManager");
                    String a5 = ig5.a(PortfolioApp.g0.c(), 2131886299);
                    ee7.a((Object) a5, "LanguageHelper.getString\u2026ext__AreYouSureYouWantTo)");
                    bx63.a(childFragmentManager3, bundle3, "CANCEL_FRIEND", a5);
                }
            }
        } else {
            ee7.d("friendAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        if (i2 == 2131363307) {
            int hashCode = str.hashCode();
            if (hashCode != -1742668797) {
                if (hashCode != -322581840) {
                    if (hashCode == -19037436 && str.equals("UN_FRIEND")) {
                        un4 un4 = intent != null ? (un4) intent.getParcelableExtra("friend_extra") : null;
                        if (un4 != null) {
                            ct4 ct4 = this.h;
                            if (ct4 != null) {
                                ct4.d(un4);
                            } else {
                                ee7.d("viewModel");
                                throw null;
                            }
                        }
                    }
                } else if (str.equals("BLOCK_FRIEND")) {
                    un4 un42 = intent != null ? (un4) intent.getParcelableExtra("friend_extra") : null;
                    if (un42 != null) {
                        ct4 ct42 = this.h;
                        if (ct42 != null) {
                            ct42.a(un42);
                        } else {
                            ee7.d("viewModel");
                            throw null;
                        }
                    }
                }
            } else if (str.equals("CANCEL_FRIEND")) {
                un4 un43 = intent != null ? (un4) intent.getParcelableExtra("friend_extra") : null;
                if (un43 != null) {
                    ct4 ct43 = this.h;
                    if (ct43 != null) {
                        ct43.b(un43);
                    } else {
                        ee7.d("viewModel");
                        throw null;
                    }
                }
            }
        }
    }
}
