package com.fossil;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oy2 extends AbstractSet<Map.Entry<K, V>> {
    @DexIgnore
    public /* final */ /* synthetic */ dy2 a;

    @DexIgnore
    public oy2(dy2 dy2) {
        this.a = dy2;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public /* synthetic */ boolean add(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (contains(entry)) {
            return false;
        }
        this.a.put((Comparable) entry.getKey(), entry.getValue());
        return true;
    }

    @DexIgnore
    public void clear() {
        this.a.clear();
    }

    @DexIgnore
    public boolean contains(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = this.a.get(entry.getKey());
        Object value = entry.getValue();
        if (obj2 != value) {
            return obj2 != null && obj2.equals(value);
        }
        return true;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public Iterator<Map.Entry<K, V>> iterator() {
        return new ly2(this.a, null);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (!contains(entry)) {
            return false;
        }
        this.a.remove(entry.getKey());
        return true;
    }

    @DexIgnore
    public int size() {
        return this.a.size();
    }

    @DexIgnore
    public /* synthetic */ oy2(dy2 dy2, gy2 gy2) {
        this(dy2);
    }
}
