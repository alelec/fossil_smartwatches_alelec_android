package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xr5 implements MembersInjector<wr5> {
    @DexIgnore
    public static void a(wr5 wr5, d76 d76) {
        wr5.g = d76;
    }

    @DexIgnore
    public static void a(wr5 wr5, e06 e06) {
        wr5.h = e06;
    }

    @DexIgnore
    public static void a(wr5 wr5, d56 d56) {
        wr5.i = d56;
    }

    @DexIgnore
    public static void a(wr5 wr5, oh6 oh6) {
        wr5.j = oh6;
    }

    @DexIgnore
    public static void a(wr5 wr5, xs5 xs5) {
        wr5.p = xs5;
    }

    @DexIgnore
    public static void a(wr5 wr5, sw5 sw5) {
        wr5.q = sw5;
    }

    @DexIgnore
    public static void a(wr5 wr5, hh6 hh6) {
        wr5.r = hh6;
    }
}
