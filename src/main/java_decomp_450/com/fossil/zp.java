package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import com.facebook.applinks.AppLinkData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zp {
    @DexIgnore
    public static Bundle a(Intent intent) {
        return intent.getBundleExtra(AppLinkData.BUNDLE_AL_APPLINK_DATA_KEY);
    }

    @DexIgnore
    public static Bundle b(Intent intent) {
        Bundle a = a(intent);
        if (a == null) {
            return null;
        }
        return a.getBundle(AppLinkData.ARGUMENTS_EXTRAS_KEY);
    }
}
