package com.fossil;

import com.fossil.xg5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class yg5 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] e;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] f;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] g;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] h;

    /*
    static {
        int[] iArr = new int[ib5.values().length];
        a = iArr;
        iArr[ib5.BLUETOOTH_OFF.ordinal()] = 1;
        a[ib5.BACKGROUND_LOCATION_PERMISSION_OFF.ordinal()] = 2;
        a[ib5.LOCATION_PERMISSION_OFF.ordinal()] = 3;
        a[ib5.LOCATION_SERVICE_OFF.ordinal()] = 4;
        int[] iArr2 = new int[xg5.c.values().length];
        b = iArr2;
        iArr2[xg5.c.LOCATION_BACKGROUND.ordinal()] = 1;
        b[xg5.c.LOCATION_FINE.ordinal()] = 2;
        b[xg5.c.READ_CALL_LOG.ordinal()] = 3;
        b[xg5.c.READ_PHONE_STATE.ordinal()] = 4;
        b[xg5.c.ANSWER_PHONE_CALL.ordinal()] = 5;
        b[xg5.c.CALL_PHONE.ordinal()] = 6;
        b[xg5.c.READ_SMS.ordinal()] = 7;
        b[xg5.c.RECEIVE_MMS.ordinal()] = 8;
        b[xg5.c.RECEIVE_SMS.ordinal()] = 9;
        int[] iArr3 = new int[xg5.c.values().length];
        c = iArr3;
        iArr3[xg5.c.BLUETOOTH_CONNECTION.ordinal()] = 1;
        c[xg5.c.LOCATION_FINE.ordinal()] = 2;
        c[xg5.c.LOCATION_BACKGROUND.ordinal()] = 3;
        c[xg5.c.LOCATION_SERVICE.ordinal()] = 4;
        c[xg5.c.READ_CONTACTS.ordinal()] = 5;
        c[xg5.c.READ_PHONE_STATE.ordinal()] = 6;
        c[xg5.c.READ_CALL_LOG.ordinal()] = 7;
        c[xg5.c.CALL_PHONE.ordinal()] = 8;
        c[xg5.c.ANSWER_PHONE_CALL.ordinal()] = 9;
        c[xg5.c.READ_SMS.ordinal()] = 10;
        c[xg5.c.RECEIVE_SMS.ordinal()] = 11;
        c[xg5.c.RECEIVE_MMS.ordinal()] = 12;
        c[xg5.c.SEND_SMS.ordinal()] = 13;
        c[xg5.c.NOTIFICATION_ACCESS.ordinal()] = 14;
        c[xg5.c.CAMERA.ordinal()] = 15;
        c[xg5.c.READ_EXTERNAL_STORAGE.ordinal()] = 16;
        int[] iArr4 = new int[xg5.c.values().length];
        d = iArr4;
        iArr4[xg5.c.LOCATION_FINE.ordinal()] = 1;
        d[xg5.c.LOCATION_BACKGROUND.ordinal()] = 2;
        d[xg5.c.READ_CONTACTS.ordinal()] = 3;
        d[xg5.c.READ_PHONE_STATE.ordinal()] = 4;
        d[xg5.c.READ_CALL_LOG.ordinal()] = 5;
        d[xg5.c.CALL_PHONE.ordinal()] = 6;
        d[xg5.c.ANSWER_PHONE_CALL.ordinal()] = 7;
        d[xg5.c.READ_SMS.ordinal()] = 8;
        d[xg5.c.RECEIVE_SMS.ordinal()] = 9;
        d[xg5.c.RECEIVE_MMS.ordinal()] = 10;
        d[xg5.c.SEND_SMS.ordinal()] = 11;
        d[xg5.c.CAMERA.ordinal()] = 12;
        d[xg5.c.READ_EXTERNAL_STORAGE.ordinal()] = 13;
        int[] iArr5 = new int[xg5.a.values().length];
        e = iArr5;
        iArr5[xg5.a.SET_COMPLICATION_WATCH_APP_WEATHER.ordinal()] = 1;
        e[xg5.a.SET_MICRO_APP_COMMUTE_TIME.ordinal()] = 2;
        e[xg5.a.SET_WATCH_APP_COMMUTE_TIME.ordinal()] = 3;
        e[xg5.a.SET_COMPLICATION_CHANCE_OF_RAIN.ordinal()] = 4;
        e[xg5.a.FIND_DEVICE.ordinal()] = 5;
        e[xg5.a.PAIR_DEVICE.ordinal()] = 6;
        e[xg5.a.SET_WATCH_APP_MUSIC.ordinal()] = 7;
        e[xg5.a.SET_MICRO_APP_MUSIC.ordinal()] = 8;
        int[] iArr6 = new int[xg5.c.values().length];
        f = iArr6;
        iArr6[xg5.c.BLUETOOTH_CONNECTION.ordinal()] = 1;
        f[xg5.c.LOCATION_FINE.ordinal()] = 2;
        f[xg5.c.LOCATION_BACKGROUND.ordinal()] = 3;
        f[xg5.c.LOCATION_SERVICE.ordinal()] = 4;
        f[xg5.c.READ_CONTACTS.ordinal()] = 5;
        f[xg5.c.READ_PHONE_STATE.ordinal()] = 6;
        f[xg5.c.READ_CALL_LOG.ordinal()] = 7;
        f[xg5.c.CALL_PHONE.ordinal()] = 8;
        f[xg5.c.ANSWER_PHONE_CALL.ordinal()] = 9;
        f[xg5.c.READ_SMS.ordinal()] = 10;
        f[xg5.c.RECEIVE_MMS.ordinal()] = 11;
        f[xg5.c.RECEIVE_SMS.ordinal()] = 12;
        f[xg5.c.SEND_SMS.ordinal()] = 13;
        f[xg5.c.NOTIFICATION_ACCESS.ordinal()] = 14;
        f[xg5.c.CAMERA.ordinal()] = 15;
        f[xg5.c.READ_EXTERNAL_STORAGE.ordinal()] = 16;
        int[] iArr7 = new int[xg5.c.values().length];
        g = iArr7;
        iArr7[xg5.c.NOTIFICATION_ACCESS.ordinal()] = 1;
        g[xg5.c.BLUETOOTH_CONNECTION.ordinal()] = 2;
        g[xg5.c.LOCATION_SERVICE.ordinal()] = 3;
        int[] iArr8 = new int[xg5.c.values().length];
        h = iArr8;
        iArr8[xg5.c.BLUETOOTH_CONNECTION.ordinal()] = 1;
        h[xg5.c.LOCATION_FINE.ordinal()] = 2;
        h[xg5.c.LOCATION_SERVICE.ordinal()] = 3;
        h[xg5.c.READ_CONTACTS.ordinal()] = 4;
        h[xg5.c.READ_PHONE_STATE.ordinal()] = 5;
        h[xg5.c.READ_CALL_LOG.ordinal()] = 6;
        h[xg5.c.CALL_PHONE.ordinal()] = 7;
        h[xg5.c.ANSWER_PHONE_CALL.ordinal()] = 8;
        h[xg5.c.READ_SMS.ordinal()] = 9;
        h[xg5.c.RECEIVE_SMS.ordinal()] = 10;
        h[xg5.c.RECEIVE_MMS.ordinal()] = 11;
        h[xg5.c.CAMERA.ordinal()] = 12;
        h[xg5.c.READ_EXTERNAL_STORAGE.ordinal()] = 13;
        h[xg5.c.NOTIFICATION_ACCESS.ordinal()] = 14;
        h[xg5.c.LOCATION_BACKGROUND.ordinal()] = 15;
        h[xg5.c.SEND_SMS.ordinal()] = 16;
    }
    */
}
