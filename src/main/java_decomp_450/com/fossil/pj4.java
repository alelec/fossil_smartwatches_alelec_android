package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pj4 {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Executor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Executor {
        @DexIgnore
        public /* final */ Handler a; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public void execute(Runnable runnable) {
            ee7.b(runnable, Constants.COMMAND);
            this.a.post(runnable);
        }
    }

    @DexIgnore
    public pj4(Executor executor, Executor executor2, Executor executor3) {
        this.a = executor;
        this.b = executor2;
    }

    @DexIgnore
    public Executor a() {
        return this.a;
    }

    @DexIgnore
    public Executor b() {
        return this.b;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public pj4() {
        /*
            r3 = this;
            java.util.concurrent.ExecutorService r0 = java.util.concurrent.Executors.newSingleThreadExecutor()
            java.lang.String r1 = "Executors.newSingleThreadExecutor()"
            com.fossil.ee7.a(r0, r1)
            r1 = 3
            java.util.concurrent.ExecutorService r1 = java.util.concurrent.Executors.newFixedThreadPool(r1)
            java.lang.String r2 = "Executors.newFixedThreadPool(3)"
            com.fossil.ee7.a(r1, r2)
            com.fossil.pj4$a r2 = new com.fossil.pj4$a
            r2.<init>()
            r3.<init>(r0, r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pj4.<init>():void");
    }
}
