package com.fossil;

import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wy implements yw {
    @DexIgnore
    public static /* final */ r50<Class<?>, byte[]> j; // = new r50<>(50);
    @DexIgnore
    public /* final */ az b;
    @DexIgnore
    public /* final */ yw c;
    @DexIgnore
    public /* final */ yw d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ Class<?> g;
    @DexIgnore
    public /* final */ ax h;
    @DexIgnore
    public /* final */ ex<?> i;

    @DexIgnore
    public wy(az azVar, yw ywVar, yw ywVar2, int i2, int i3, ex<?> exVar, Class<?> cls, ax axVar) {
        this.b = azVar;
        this.c = ywVar;
        this.d = ywVar2;
        this.e = i2;
        this.f = i3;
        this.i = exVar;
        this.g = cls;
        this.h = axVar;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public void a(MessageDigest messageDigest) {
        byte[] bArr = (byte[]) this.b.a(8, byte[].class);
        ByteBuffer.wrap(bArr).putInt(this.e).putInt(this.f).array();
        this.d.a(messageDigest);
        this.c.a(messageDigest);
        messageDigest.update(bArr);
        ex<?> exVar = this.i;
        if (exVar != null) {
            exVar.a(messageDigest);
        }
        this.h.a(messageDigest);
        messageDigest.update(a());
        this.b.a(bArr);
    }

    @DexIgnore
    @Override // com.fossil.yw
    public boolean equals(Object obj) {
        if (!(obj instanceof wy)) {
            return false;
        }
        wy wyVar = (wy) obj;
        if (this.f != wyVar.f || this.e != wyVar.e || !v50.b(this.i, wyVar.i) || !this.g.equals(wyVar.g) || !this.c.equals(wyVar.c) || !this.d.equals(wyVar.d) || !this.h.equals(wyVar.h)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public int hashCode() {
        int hashCode = (((((this.c.hashCode() * 31) + this.d.hashCode()) * 31) + this.e) * 31) + this.f;
        ex<?> exVar = this.i;
        if (exVar != null) {
            hashCode = (hashCode * 31) + exVar.hashCode();
        }
        return (((hashCode * 31) + this.g.hashCode()) * 31) + this.h.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ResourceCacheKey{sourceKey=" + this.c + ", signature=" + this.d + ", width=" + this.e + ", height=" + this.f + ", decodedResourceClass=" + this.g + ", transformation='" + this.i + '\'' + ", options=" + this.h + '}';
    }

    @DexIgnore
    public final byte[] a() {
        byte[] a = j.a(this.g);
        if (a != null) {
            return a;
        }
        byte[] bytes = this.g.getName().getBytes(yw.a);
        j.b(this.g, bytes);
        return bytes;
    }
}
