package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gm {
    @DexIgnore
    public static /* final */ String a; // = im.a("InputMerger");

    @DexIgnore
    public static gm a(String str) {
        try {
            return (gm) Class.forName(str).newInstance();
        } catch (Exception e) {
            im a2 = im.a();
            String str2 = a;
            a2.b(str2, "Trouble instantiating + " + str, e);
            return null;
        }
    }

    @DexIgnore
    public abstract cm a(List<cm> list);
}
