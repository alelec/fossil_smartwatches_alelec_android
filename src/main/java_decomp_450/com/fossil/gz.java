package com.fossil;

import com.fossil.lz;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gz<K extends lz, V> {
    @DexIgnore
    public /* final */ a<K, V> a; // = new a<>();
    @DexIgnore
    public /* final */ Map<K, a<K, V>> b; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K, V> {
        @DexIgnore
        public /* final */ K a;
        @DexIgnore
        public List<V> b;
        @DexIgnore
        public a<K, V> c;
        @DexIgnore
        public a<K, V> d;

        @DexIgnore
        public a() {
            this(null);
        }

        @DexIgnore
        public V a() {
            int b2 = b();
            if (b2 > 0) {
                return this.b.remove(b2 - 1);
            }
            return null;
        }

        @DexIgnore
        public int b() {
            List<V> list = this.b;
            if (list != null) {
                return list.size();
            }
            return 0;
        }

        @DexIgnore
        public a(K k) {
            this.d = this;
            this.c = this;
            this.a = k;
        }

        @DexIgnore
        public void a(V v) {
            if (this.b == null) {
                this.b = new ArrayList();
            }
            this.b.add(v);
        }
    }

    @DexIgnore
    public static <K, V> void c(a<K, V> aVar) {
        a<K, V> aVar2 = aVar.d;
        aVar2.c = aVar.c;
        aVar.c.d = aVar2;
    }

    @DexIgnore
    public static <K, V> void d(a<K, V> aVar) {
        aVar.c.d = aVar;
        aVar.d.c = aVar;
    }

    @DexIgnore
    public void a(K k, V v) {
        a<K, V> aVar = this.b.get(k);
        if (aVar == null) {
            aVar = new a<>(k);
            b(aVar);
            this.b.put(k, aVar);
        } else {
            k.a();
        }
        aVar.a(v);
    }

    @DexIgnore
    public final void b(a<K, V> aVar) {
        c(aVar);
        a<K, V> aVar2 = this.a;
        aVar.d = aVar2.d;
        aVar.c = aVar2;
        d(aVar);
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("GroupedLinkedMap( ");
        boolean z = false;
        for (a<K, V> aVar = this.a.c; !aVar.equals(this.a); aVar = aVar.c) {
            z = true;
            sb.append('{');
            sb.append((Object) aVar.a);
            sb.append(':');
            sb.append(aVar.b());
            sb.append("}, ");
        }
        if (z) {
            sb.delete(sb.length() - 2, sb.length());
        }
        sb.append(" )");
        return sb.toString();
    }

    @DexIgnore
    public V a(K k) {
        a<K, V> aVar = this.b.get(k);
        if (aVar == null) {
            aVar = new a<>(k);
            this.b.put(k, aVar);
        } else {
            k.a();
        }
        a(aVar);
        return aVar.a();
    }

    @DexIgnore
    public V a() {
        for (a<K, V> aVar = this.a.d; !aVar.equals(this.a); aVar = aVar.d) {
            V a2 = aVar.a();
            if (a2 != null) {
                return a2;
            }
            c(aVar);
            this.b.remove(aVar.a);
            aVar.a.a();
        }
        return null;
    }

    @DexIgnore
    public final void a(a<K, V> aVar) {
        c(aVar);
        a<K, V> aVar2 = this.a;
        aVar.d = aVar2;
        aVar.c = aVar2.c;
        d(aVar);
    }
}
