package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.io.Serializable;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vr2<T> implements tr2<T>, Serializable {
    @DexIgnore
    public volatile transient boolean a;
    @DexIgnore
    @NullableDecl
    public transient T b;
    @DexIgnore
    public /* final */ tr2<T> zza;

    @DexIgnore
    public vr2(tr2<T> tr2) {
        or2.a(tr2);
        this.zza = tr2;
    }

    @DexIgnore
    public final String toString() {
        Object obj;
        if (this.a) {
            String valueOf = String.valueOf(this.b);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 25);
            sb.append("<supplier that returned ");
            sb.append(valueOf);
            sb.append(SimpleComparison.GREATER_THAN_OPERATION);
            obj = sb.toString();
        } else {
            obj = this.zza;
        }
        String valueOf2 = String.valueOf(obj);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 19);
        sb2.append("Suppliers.memoize(");
        sb2.append(valueOf2);
        sb2.append(")");
        return sb2.toString();
    }

    @DexIgnore
    @Override // com.fossil.tr2
    public final T zza() {
        if (!this.a) {
            synchronized (this) {
                if (!this.a) {
                    T zza2 = this.zza.zza();
                    this.b = zza2;
                    this.a = true;
                    return zza2;
                }
            }
        }
        return this.b;
    }
}
