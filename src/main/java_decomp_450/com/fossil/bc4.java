package com.fossil;

import android.util.Base64;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bc4 {
    @DexIgnore
    public static /* final */ byte a; // = Byte.parseByte("01110000", 2);
    @DexIgnore
    public static /* final */ byte b; // = Byte.parseByte("00001111", 2);

    @DexIgnore
    public String a() {
        byte[] a2 = a(UUID.randomUUID(), new byte[17]);
        a2[16] = a2[0];
        a2[0] = (byte) ((b & a2[0]) | a);
        return a(a2);
    }

    @DexIgnore
    public static String a(byte[] bArr) {
        return new String(Base64.encode(bArr, 11), Charset.defaultCharset()).substring(0, 22);
    }

    @DexIgnore
    public static byte[] a(UUID uuid, byte[] bArr) {
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        wrap.putLong(uuid.getMostSignificantBits());
        wrap.putLong(uuid.getLeastSignificantBits());
        return wrap.array();
    }
}
