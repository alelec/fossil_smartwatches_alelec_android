package com.fossil;

import com.fossil.dz3;
import com.fossil.ez3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ww3<E> extends AbstractCollection<E> implements dz3<E> {
    @DexIgnore
    public transient Set<E> a;
    @DexIgnore
    public transient Set<dz3.a<E>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ez3.c<E> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.ez3.c
        public dz3<E> a() {
            return ww3.this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ez3.d<E> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.ez3.d
        public dz3<E> a() {
            return ww3.this;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<dz3.a<E>> iterator() {
            return ww3.this.entryIterator();
        }

        @DexIgnore
        public int size() {
            return ww3.this.distinctElements();
        }
    }

    @DexIgnore
    @Override // com.fossil.dz3, java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    public boolean add(E e) {
        add(e, 1);
        return true;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    public boolean addAll(Collection<? extends E> collection) {
        return ez3.a((dz3) this, (Collection) collection);
    }

    @DexIgnore
    public abstract void clear();

    @DexIgnore
    @Override // com.fossil.dz3
    public boolean contains(Object obj) {
        return count(obj) > 0;
    }

    @DexIgnore
    @Override // com.fossil.dz3
    public abstract int count(Object obj);

    @DexIgnore
    public Set<E> createElementSet() {
        return new a();
    }

    @DexIgnore
    public Set<dz3.a<E>> createEntrySet() {
        return new b();
    }

    @DexIgnore
    public abstract int distinctElements();

    @DexIgnore
    @Override // com.fossil.dz3
    public Set<E> elementSet() {
        Set<E> set = this.a;
        if (set != null) {
            return set;
        }
        Set<E> createElementSet = createElementSet();
        this.a = createElementSet;
        return createElementSet;
    }

    @DexIgnore
    public abstract Iterator<dz3.a<E>> entryIterator();

    @DexIgnore
    @Override // com.fossil.dz3
    public Set<dz3.a<E>> entrySet() {
        Set<dz3.a<E>> set = this.b;
        if (set != null) {
            return set;
        }
        Set<dz3.a<E>> createEntrySet = createEntrySet();
        this.b = createEntrySet;
        return createEntrySet;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return ez3.a(this, obj);
    }

    @DexIgnore
    public int hashCode() {
        return entrySet().hashCode();
    }

    @DexIgnore
    public boolean isEmpty() {
        return entrySet().isEmpty();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public abstract Iterator<E> iterator();

    @DexIgnore
    @Override // com.fossil.dz3
    @CanIgnoreReturnValue
    public abstract int remove(Object obj, int i);

    @DexIgnore
    @CanIgnoreReturnValue
    public boolean remove(Object obj) {
        return remove(obj, 1) > 0;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    public boolean removeAll(Collection<?> collection) {
        return ez3.b(this, collection);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    public boolean retainAll(Collection<?> collection) {
        return ez3.c(this, collection);
    }

    @DexIgnore
    @Override // com.fossil.dz3
    @CanIgnoreReturnValue
    public int setCount(E e, int i) {
        return ez3.a(this, e, i);
    }

    @DexIgnore
    public int size() {
        return ez3.a((dz3<?>) this);
    }

    @DexIgnore
    public String toString() {
        return entrySet().toString();
    }

    @DexIgnore
    @Override // com.fossil.dz3
    @CanIgnoreReturnValue
    public int add(E e, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.dz3
    @CanIgnoreReturnValue
    public boolean setCount(E e, int i, int i2) {
        return ez3.a(this, e, i, i2);
    }
}
