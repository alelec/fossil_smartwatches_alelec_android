package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kh4 implements ph4 {
    @DexIgnore
    public int a() {
        return 5;
    }

    @DexIgnore
    @Override // com.fossil.ph4
    public void a(qh4 qh4) {
        StringBuilder sb = new StringBuilder();
        sb.append((char) 0);
        while (true) {
            if (!qh4.i()) {
                break;
            }
            sb.append(qh4.c());
            qh4.f++;
            int a = sh4.a(qh4.d(), qh4.f, a());
            if (a != a()) {
                qh4.b(a);
                break;
            }
        }
        int length = sb.length() - 1;
        int a2 = qh4.a() + length + 1;
        qh4.c(a2);
        boolean z = qh4.g().a() - a2 > 0;
        if (qh4.i() || z) {
            if (length <= 249) {
                sb.setCharAt(0, (char) length);
            } else if (length <= 1555) {
                sb.setCharAt(0, (char) ((length / 250) + 249));
                sb.insert(1, (char) (length % 250));
            } else {
                throw new IllegalStateException("Message length not in valid ranges: " + length);
            }
        }
        int length2 = sb.length();
        for (int i = 0; i < length2; i++) {
            qh4.a(a(sb.charAt(i), qh4.a() + 1));
        }
    }

    @DexIgnore
    public static char a(char c, int i) {
        int i2 = c + ((i * 149) % 255) + 1;
        return i2 <= 255 ? (char) i2 : (char) (i2 - 256);
    }
}
