package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dw4 extends cw4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i v; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray w;
    @DexIgnore
    public /* final */ LinearLayout t;
    @DexIgnore
    public long u;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        w = sparseIntArray;
        sparseIntArray.put(2131361851, 1);
        w.put(2131362328, 2);
        w.put(2131363466, 3);
    }
    */

    @DexIgnore
    public dw4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 4, v, w));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.u = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.u != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.u = 1;
        }
        g();
    }

    @DexIgnore
    public dw4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (RTLImageView) objArr[1], (FlexibleTextView) objArr[2], (WebView) objArr[3]);
        this.u = -1;
        LinearLayout linearLayout = (LinearLayout) objArr[0];
        this.t = linearLayout;
        linearLayout.setTag(null);
        a(view);
        f();
    }
}
