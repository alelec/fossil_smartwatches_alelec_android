package com.fossil;

import android.graphics.Color;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mm6 extends he {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e; // = "#bdbdbd";
    @DexIgnore
    public MutableLiveData<b> a; // = new MutableLiveData<>();
    @DexIgnore
    public b b; // = new b(null, null, null, null, null, null, null, null, 255, null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public Integer e;
        @DexIgnore
        public Integer f;
        @DexIgnore
        public Integer g;
        @DexIgnore
        public Integer h;

        @DexIgnore
        public b() {
            this(null, null, null, null, null, null, null, null, 255, null);
        }

        @DexIgnore
        public b(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8) {
            this.a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
            this.e = num5;
            this.f = num6;
            this.g = num7;
            this.h = num8;
        }

        @DexIgnore
        public final Integer a() {
            return this.d;
        }

        @DexIgnore
        public final Integer b() {
            return this.c;
        }

        @DexIgnore
        public final Integer c() {
            return this.b;
        }

        @DexIgnore
        public final Integer d() {
            return this.a;
        }

        @DexIgnore
        public final Integer e() {
            return this.f;
        }

        @DexIgnore
        public final Integer f() {
            return this.e;
        }

        @DexIgnore
        public final Integer g() {
            return this.h;
        }

        @DexIgnore
        public final Integer h() {
            return this.g;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8, int i, zd7 zd7) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2, (i & 4) != 0 ? null : num3, (i & 8) != 0 ? null : num4, (i & 16) != 0 ? null : num5, (i & 32) != 0 ? null : num6, (i & 64) != 0 ? null : num7, (i & 128) == 0 ? num8 : null);
        }

        @DexIgnore
        public static /* synthetic */ void a(b bVar, Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8, int i, Object obj) {
            if ((i & 1) != 0) {
                num = null;
            }
            if ((i & 2) != 0) {
                num2 = null;
            }
            if ((i & 4) != 0) {
                num3 = null;
            }
            if ((i & 8) != 0) {
                num4 = null;
            }
            if ((i & 16) != 0) {
                num5 = null;
            }
            if ((i & 32) != 0) {
                num6 = null;
            }
            if ((i & 64) != 0) {
                num7 = null;
            }
            if ((i & 128) != 0) {
                num8 = null;
            }
            bVar.a(num, num2, num3, num4, num5, num6, num7, num8);
        }

        @DexIgnore
        public final void a(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8) {
            this.a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
            this.e = num5;
            this.f = num6;
            this.g = num7;
            this.h = num8;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeRingChartViewModel$saveColor$1", f = "CustomizeRingChartViewModel.kt", l = {76, 77, 120}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $bigColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $biggestColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $mediumColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $smallColor;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mm6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(mm6 mm6, String str, String str2, String str3, String str4, String str5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mm6;
            this.$id = str;
            this.$biggestColor = str2;
            this.$bigColor = str3;
            this.$mediumColor = str4;
            this.$smallColor = str5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$id, this.$biggestColor, this.$bigColor, this.$mediumColor, this.$smallColor, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r4v3, types: [com.portfolio.platform.data.model.Theme] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00a0  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00c6  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x010c  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0152  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x0198  */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x0224 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x022d  */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r20) {
            /*
                r19 = this;
                r0 = r19
                java.lang.Object r1 = com.fossil.nb7.a()
                int r2 = r0.label
                r3 = 3
                r4 = 2
                r5 = 1
                r6 = 0
                if (r2 == 0) goto L_0x0060
                if (r2 == r5) goto L_0x004c
                if (r2 == r4) goto L_0x0039
                if (r2 != r3) goto L_0x0031
                java.lang.Object r1 = r0.L$5
                com.fossil.se7 r1 = (com.fossil.se7) r1
                java.lang.Object r1 = r0.L$4
                com.fossil.se7 r1 = (com.fossil.se7) r1
                java.lang.Object r1 = r0.L$3
                com.fossil.se7 r1 = (com.fossil.se7) r1
                java.lang.Object r1 = r0.L$2
                com.fossil.se7 r1 = (com.fossil.se7) r1
                java.lang.Object r1 = r0.L$1
                com.fossil.se7 r1 = (com.fossil.se7) r1
                java.lang.Object r1 = r0.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r20)
                goto L_0x0225
            L_0x0031:
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r1.<init>(r2)
                throw r1
            L_0x0039:
                java.lang.Object r2 = r0.L$2
                com.fossil.se7 r2 = (com.fossil.se7) r2
                java.lang.Object r4 = r0.L$1
                com.fossil.se7 r4 = (com.fossil.se7) r4
                java.lang.Object r5 = r0.L$0
                com.fossil.yi7 r5 = (com.fossil.yi7) r5
                com.fossil.t87.a(r20)
                r7 = r4
                r4 = r20
                goto L_0x009e
            L_0x004c:
                java.lang.Object r2 = r0.L$2
                com.fossil.se7 r2 = (com.fossil.se7) r2
                java.lang.Object r5 = r0.L$1
                com.fossil.se7 r5 = (com.fossil.se7) r5
                java.lang.Object r7 = r0.L$0
                com.fossil.yi7 r7 = (com.fossil.yi7) r7
                com.fossil.t87.a(r20)
                r8 = r7
                r7 = r5
                r5 = r20
                goto L_0x0083
            L_0x0060:
                com.fossil.t87.a(r20)
                com.fossil.yi7 r2 = r0.p$
                com.fossil.se7 r7 = new com.fossil.se7
                r7.<init>()
                com.fossil.mm6 r8 = r0.this$0
                com.portfolio.platform.data.source.ThemeRepository r8 = r8.c
                java.lang.String r9 = r0.$id
                r0.L$0 = r2
                r0.L$1 = r7
                r0.L$2 = r7
                r0.label = r5
                java.lang.Object r5 = r8.getThemeById(r9, r0)
                if (r5 != r1) goto L_0x0081
                return r1
            L_0x0081:
                r8 = r2
                r2 = r7
            L_0x0083:
                com.portfolio.platform.data.model.Theme r5 = (com.portfolio.platform.data.model.Theme) r5
                if (r5 == 0) goto L_0x0088
                goto L_0x00a4
            L_0x0088:
                com.fossil.mm6 r5 = r0.this$0
                com.portfolio.platform.data.source.ThemeRepository r5 = r5.c
                r0.L$0 = r8
                r0.L$1 = r7
                r0.L$2 = r2
                r0.label = r4
                java.lang.Object r4 = r5.getCurrentTheme(r0)
                if (r4 != r1) goto L_0x009d
                return r1
            L_0x009d:
                r5 = r8
            L_0x009e:
                if (r4 == 0) goto L_0x022d
                com.portfolio.platform.data.model.Theme r4 = (com.portfolio.platform.data.model.Theme) r4
                r8 = r5
                r5 = r4
            L_0x00a4:
                r2.element = r5
                com.fossil.se7 r2 = new com.fossil.se7
                r2.<init>()
                r2.element = r6
                com.fossil.se7 r4 = new com.fossil.se7
                r4.<init>()
                r4.element = r6
                com.fossil.se7 r5 = new com.fossil.se7
                r5.<init>()
                r5.element = r6
                com.fossil.se7 r9 = new com.fossil.se7
                r9.<init>()
                r9.element = r6
                java.lang.String r6 = r0.$biggestColor
                if (r6 == 0) goto L_0x0108
                T r6 = r7.element
                com.portfolio.platform.data.model.Theme r6 = (com.portfolio.platform.data.model.Theme) r6
                java.util.ArrayList r6 = r6.getStyles()
                java.util.Iterator r6 = r6.iterator()
            L_0x00d2:
                boolean r10 = r6.hasNext()
                if (r10 == 0) goto L_0x0108
                java.lang.Object r10 = r6.next()
                com.portfolio.platform.data.model.Style r10 = (com.portfolio.platform.data.model.Style) r10
                java.lang.String r11 = r10.getKey()
                java.lang.String r12 = "dianaStepsRing"
                boolean r11 = com.fossil.ee7.a(r11, r12)
                if (r11 != 0) goto L_0x00f6
                java.lang.String r11 = r10.getKey()
                java.lang.String r12 = "hybridStepsRing"
                boolean r11 = com.fossil.ee7.a(r11, r12)
                if (r11 == 0) goto L_0x00d2
            L_0x00f6:
                java.lang.String r11 = r0.$biggestColor
                r10.setValue(r11)
                java.lang.String r10 = r0.$biggestColor
                int r10 = android.graphics.Color.parseColor(r10)
                java.lang.Integer r10 = com.fossil.pb7.a(r10)
                r2.element = r10
                goto L_0x00d2
            L_0x0108:
                java.lang.String r6 = r0.$bigColor
                if (r6 == 0) goto L_0x014e
                T r6 = r7.element
                com.portfolio.platform.data.model.Theme r6 = (com.portfolio.platform.data.model.Theme) r6
                java.util.ArrayList r6 = r6.getStyles()
                java.util.Iterator r6 = r6.iterator()
            L_0x0118:
                boolean r10 = r6.hasNext()
                if (r10 == 0) goto L_0x014e
                java.lang.Object r10 = r6.next()
                com.portfolio.platform.data.model.Style r10 = (com.portfolio.platform.data.model.Style) r10
                java.lang.String r11 = r10.getKey()
                java.lang.String r12 = "dianaActiveMinuteRing"
                boolean r11 = com.fossil.ee7.a(r11, r12)
                if (r11 != 0) goto L_0x013c
                java.lang.String r11 = r10.getKey()
                java.lang.String r12 = "hybridActiveCaloriesRing"
                boolean r11 = com.fossil.ee7.a(r11, r12)
                if (r11 == 0) goto L_0x0118
            L_0x013c:
                java.lang.String r11 = r0.$bigColor
                r10.setValue(r11)
                java.lang.String r10 = r0.$bigColor
                int r10 = android.graphics.Color.parseColor(r10)
                java.lang.Integer r10 = com.fossil.pb7.a(r10)
                r4.element = r10
                goto L_0x0118
            L_0x014e:
                java.lang.String r6 = r0.$mediumColor
                if (r6 == 0) goto L_0x0194
                T r6 = r7.element
                com.portfolio.platform.data.model.Theme r6 = (com.portfolio.platform.data.model.Theme) r6
                java.util.ArrayList r6 = r6.getStyles()
                java.util.Iterator r6 = r6.iterator()
            L_0x015e:
                boolean r10 = r6.hasNext()
                if (r10 == 0) goto L_0x0194
                java.lang.Object r10 = r6.next()
                com.portfolio.platform.data.model.Style r10 = (com.portfolio.platform.data.model.Style) r10
                java.lang.String r11 = r10.getKey()
                java.lang.String r12 = "dianaActiveCaloriesRing"
                boolean r11 = com.fossil.ee7.a(r11, r12)
                if (r11 != 0) goto L_0x0182
                java.lang.String r11 = r10.getKey()
                java.lang.String r12 = "hybridSleepRing"
                boolean r11 = com.fossil.ee7.a(r11, r12)
                if (r11 == 0) goto L_0x015e
            L_0x0182:
                java.lang.String r11 = r0.$mediumColor
                r10.setValue(r11)
                java.lang.String r10 = r0.$mediumColor
                int r10 = android.graphics.Color.parseColor(r10)
                java.lang.Integer r10 = com.fossil.pb7.a(r10)
                r5.element = r10
                goto L_0x015e
            L_0x0194:
                java.lang.String r6 = r0.$smallColor
                if (r6 == 0) goto L_0x01da
                T r6 = r7.element
                com.portfolio.platform.data.model.Theme r6 = (com.portfolio.platform.data.model.Theme) r6
                java.util.ArrayList r6 = r6.getStyles()
                java.util.Iterator r6 = r6.iterator()
            L_0x01a4:
                boolean r10 = r6.hasNext()
                if (r10 == 0) goto L_0x01da
                java.lang.Object r10 = r6.next()
                com.portfolio.platform.data.model.Style r10 = (com.portfolio.platform.data.model.Style) r10
                java.lang.String r11 = r10.getKey()
                java.lang.String r12 = "dianaSleepRing"
                boolean r11 = com.fossil.ee7.a(r11, r12)
                if (r11 != 0) goto L_0x01c8
                java.lang.String r11 = r10.getKey()
                java.lang.String r12 = "hybridGoalTrackingRing"
                boolean r11 = com.fossil.ee7.a(r11, r12)
                if (r11 == 0) goto L_0x01a4
            L_0x01c8:
                java.lang.String r11 = r0.$smallColor
                r10.setValue(r11)
                java.lang.String r10 = r0.$smallColor
                int r10 = android.graphics.Color.parseColor(r10)
                java.lang.Integer r10 = com.fossil.pb7.a(r10)
                r9.element = r10
                goto L_0x01a4
            L_0x01da:
                com.fossil.mm6 r6 = r0.this$0
                com.fossil.mm6$b r10 = r6.b
                T r6 = r2.element
                r11 = r6
                java.lang.Integer r11 = (java.lang.Integer) r11
                r12 = r6
                java.lang.Integer r12 = (java.lang.Integer) r12
                T r6 = r4.element
                r13 = r6
                java.lang.Integer r13 = (java.lang.Integer) r13
                r14 = r6
                java.lang.Integer r14 = (java.lang.Integer) r14
                T r6 = r5.element
                r15 = r6
                java.lang.Integer r15 = (java.lang.Integer) r15
                r16 = r6
                java.lang.Integer r16 = (java.lang.Integer) r16
                T r6 = r9.element
                r17 = r6
                java.lang.Integer r17 = (java.lang.Integer) r17
                r18 = r6
                java.lang.Integer r18 = (java.lang.Integer) r18
                r10.a(r11, r12, r13, r14, r15, r16, r17, r18)
                com.fossil.mm6 r6 = r0.this$0
                com.portfolio.platform.data.source.ThemeRepository r6 = r6.c
                T r10 = r7.element
                com.portfolio.platform.data.model.Theme r10 = (com.portfolio.platform.data.model.Theme) r10
                r0.L$0 = r8
                r0.L$1 = r7
                r0.L$2 = r2
                r0.L$3 = r4
                r0.L$4 = r5
                r0.L$5 = r9
                r0.label = r3
                java.lang.Object r2 = r6.upsertUserTheme(r10, r0)
                if (r2 != r1) goto L_0x0225
                return r1
            L_0x0225:
                com.fossil.mm6 r1 = r0.this$0
                r1.a()
                com.fossil.i97 r1 = com.fossil.i97.a
                return r1
            L_0x022d:
                com.fossil.ee7.a()
                throw r6
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.mm6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        new a(null);
        String simpleName = mm6.class.getSimpleName();
        ee7.a((Object) simpleName, "CustomizeRingChartViewModel::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public mm6(ThemeRepository themeRepository) {
        ee7.b(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void a() {
        this.a.a(this.b);
    }

    @DexIgnore
    public final MutableLiveData<b> b() {
        return this.a;
    }

    @DexIgnore
    public final void c() {
        int i;
        int i2;
        int i3;
        int i4;
        String b2 = jm6.t.b();
        if (b2 != null) {
            i = Color.parseColor(b2);
        } else {
            i = Color.parseColor(e);
        }
        String a2 = jm6.t.a();
        if (a2 != null) {
            i2 = Color.parseColor(a2);
        } else {
            i2 = Color.parseColor(e);
        }
        String c2 = jm6.t.c();
        if (c2 != null) {
            i3 = Color.parseColor(c2);
        } else {
            i3 = Color.parseColor(e);
        }
        String d2 = jm6.t.d();
        if (d2 != null) {
            i4 = Color.parseColor(d2);
        } else {
            i4 = Color.parseColor(e);
        }
        this.b.a(Integer.valueOf(i), Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i4));
        a();
    }

    @DexIgnore
    public final void a(int i, int i2) {
        switch (i) {
            case 401:
                b.a(this.b, Integer.valueOf(i2), Integer.valueOf(i2), null, null, null, null, null, null, 252, null);
                a();
                return;
            case Action.ActivityTracker.TAG_ACTIVITY /*{ENCODED_INT: 402}*/:
                b.a(this.b, null, null, Integer.valueOf(i2), Integer.valueOf(i2), null, null, null, null, 243, null);
                a();
                return;
            case MFNetworkReturnCode.WRONG_PASSWORD /*{ENCODED_INT: 403}*/:
                b.a(this.b, null, null, null, null, Integer.valueOf(i2), Integer.valueOf(i2), null, null, 207, null);
                a();
                return;
            case 404:
                b.a(this.b, null, null, null, null, null, null, Integer.valueOf(i2), Integer.valueOf(i2), 63, null);
                a();
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, String str4, String str5) {
        ee7.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str6 = d;
        local.d(str6, "saveColor biggestColor=" + str2 + " bigColor=" + str3 + " mediumColor=" + str4 + " smallColor=" + str5);
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new c(this, str, str2, str3, str4, str5, null), 3, null);
    }
}
