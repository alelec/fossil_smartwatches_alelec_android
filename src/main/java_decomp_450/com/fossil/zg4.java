package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zg4 extends bh4 {
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore
    public zg4(bh4 bh4, int i, int i2) {
        super(bh4);
        this.c = (short) i;
        this.d = (short) i2;
    }

    @DexIgnore
    @Override // com.fossil.bh4
    public void a(ch4 ch4, byte[] bArr) {
        ch4.a(this.c, this.d);
    }

    @DexIgnore
    public String toString() {
        short s = this.c;
        short s2 = this.d;
        short s3 = (s & ((1 << s2) - 1)) | (1 << s2);
        return SimpleComparison.LESS_THAN_OPERATION + Integer.toBinaryString(s3 | (1 << this.d)).substring(1) + '>';
    }
}
