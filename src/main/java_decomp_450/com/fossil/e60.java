package com.fossil;

import android.os.Build;
import com.misfit.frameworks.common.constants.Constants;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e60 implements FlutterPlugin, MethodChannel.MethodCallHandler {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // io.flutter.embedding.engine.plugins.FlutterPlugin
    public void onAttachedToEngine(FlutterPlugin.FlutterPluginBinding flutterPluginBinding) {
        ee7.b(flutterPluginBinding, "flutterPluginBinding");
        new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "fs_log").setMethodCallHandler(new e60());
    }

    @DexIgnore
    @Override // io.flutter.embedding.engine.plugins.FlutterPlugin
    public void onDetachedFromEngine(FlutterPlugin.FlutterPluginBinding flutterPluginBinding) {
        ee7.b(flutterPluginBinding, "binding");
    }

    @DexIgnore
    @Override // io.flutter.plugin.common.MethodChannel.MethodCallHandler
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        ee7.b(methodCall, "call");
        ee7.b(result, Constants.RESULT);
        if (ee7.a((Object) methodCall.method, (Object) "getPlatformVersion")) {
            result.success("Android " + Build.VERSION.RELEASE);
        }
    }
}
