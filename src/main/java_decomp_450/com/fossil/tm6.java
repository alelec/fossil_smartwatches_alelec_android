package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tm6 implements Factory<sm6> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public tm6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static tm6 a(Provider<ThemeRepository> provider) {
        return new tm6(provider);
    }

    @DexIgnore
    public static sm6 a(ThemeRepository themeRepository) {
        return new sm6(themeRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public sm6 get() {
        return a(this.a.get());
    }
}
