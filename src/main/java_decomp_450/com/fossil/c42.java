package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c42 {
    @DexIgnore
    public /* final */ i32 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ z02<?> c;

    @DexIgnore
    public c42(i32 i32, int i, z02<?> z02) {
        this.a = i32;
        this.b = i;
        this.c = z02;
    }
}
