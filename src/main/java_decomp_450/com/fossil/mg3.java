package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mg3 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ /* synthetic */ jg3 d;

    @DexIgnore
    public mg3(jg3 jg3, int i, boolean z, boolean z2) {
        this.d = jg3;
        this.a = i;
        this.b = z;
        this.c = z2;
    }

    @DexIgnore
    public final void a(String str) {
        this.d.a(this.a, this.b, this.c, str, null, null, null);
    }

    @DexIgnore
    public final void a(String str, Object obj) {
        this.d.a(this.a, this.b, this.c, str, obj, null, null);
    }

    @DexIgnore
    public final void a(String str, Object obj, Object obj2) {
        this.d.a(this.a, this.b, this.c, str, obj, obj2, null);
    }

    @DexIgnore
    public final void a(String str, Object obj, Object obj2, Object obj3) {
        this.d.a(this.a, this.b, this.c, str, obj, obj2, obj3);
    }
}
