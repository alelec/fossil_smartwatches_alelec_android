package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ml7<T> extends jm7<T> {
    @DexIgnore
    public ml7(ib7 ib7, fb7<? super T> fb7) {
        super(ib7, fb7);
    }

    @DexIgnore
    @Override // com.fossil.rh7, com.fossil.jm7
    public void l(Object obj) {
        Object a = mi7.a(obj, ((jm7) this).d);
        ib7 context = ((jm7) this).d.getContext();
        Object b = pm7.b(context, null);
        try {
            ((jm7) this).d.resumeWith(a);
            i97 i97 = i97.a;
        } finally {
            pm7.a(context, b);
        }
    }
}
