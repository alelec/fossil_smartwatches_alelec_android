package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.Gender;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c80 extends n80 {
    @DexIgnore
    public static /* final */ c CREATOR; // = new c(null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ a c;
    @DexIgnore
    public /* final */ short d;
    @DexIgnore
    public /* final */ short e;
    @DexIgnore
    public /* final */ b f;

    @DexIgnore
    public enum a {
        UNSPECIFIED((byte) 0),
        MALE((byte) 1),
        FEMALE((byte) 2);
        
        @DexIgnore
        public static /* final */ C0025a c; // = new C0025a(null);
        @DexIgnore
        public /* final */ byte a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.c80$a$a")
        /* renamed from: com.fossil.c80$a$a  reason: collision with other inner class name */
        public static final class C0025a {
            @DexIgnore
            public /* synthetic */ C0025a(zd7 zd7) {
            }

            @DexIgnore
            public final a a(byte b) throws IllegalArgumentException {
                a aVar;
                a[] values = a.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        aVar = null;
                        break;
                    }
                    aVar = values[i];
                    if (aVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (aVar != null) {
                    return aVar;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public a(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }

        @DexIgnore
        public final Gender b() {
            int i = j21.a[ordinal()];
            if (i == 1) {
                return Gender.UNSPECIFIED;
            }
            if (i == 2) {
                return Gender.MALE;
            }
            if (i == 3) {
                return Gender.FEMALE;
            }
            throw new p87();
        }
    }

    @DexIgnore
    public enum b {
        UNSPECIFIED((byte) 0),
        LEFT_WRIST((byte) 1),
        RIGHT_WRIST((byte) 2),
        UNSPECIFIED_WRIST((byte) 3);
        
        @DexIgnore
        public static /* final */ a c; // = new a(null);
        @DexIgnore
        public /* final */ byte a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public /* synthetic */ a(zd7 zd7) {
            }

            @DexIgnore
            public final b a(byte b) throws IllegalArgumentException {
                b bVar;
                b[] values = b.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        bVar = null;
                        break;
                    }
                    bVar = values[i];
                    if (bVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (bVar != null) {
                    return bVar;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public b(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Parcelable.Creator<c80> {
        @DexIgnore
        public /* synthetic */ c(zd7 zd7) {
        }

        @DexIgnore
        public final c80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 7) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new c80(order.get(0), a.c.a(order.get(1)), order.getShort(2), order.getShort(4), b.c.a(order.get(6)));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", require: 7"));
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public c80 createFromParcel(Parcel parcel) {
            return new c80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public c80[] newArray(int i) {
            return new c80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public c80 m7createFromParcel(Parcel parcel) {
            return new c80(parcel, null);
        }
    }

    @DexIgnore
    public c80(byte b2, a aVar, short s, short s2, b bVar) throws IllegalArgumentException {
        super(o80.BIOMETRIC_PROFILE);
        this.b = b2;
        this.c = aVar;
        this.d = s;
        this.e = s2;
        this.f = bVar;
        e();
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(7).order(ByteOrder.LITTLE_ENDIAN).put(this.b).put(this.c.a()).putShort(this.d).putShort(this.e).put(this.f.a()).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        byte b2 = this.b;
        boolean z = true;
        if (8 <= b2 && 110 >= b2) {
            short s = this.d;
            if (100 <= s && 250 >= s) {
                short s2 = this.e;
                if (35 > s2 || 250 < s2) {
                    z = false;
                }
                if (!z) {
                    StringBuilder b3 = yh0.b("weightInKilogram (");
                    b3.append((int) this.e);
                    b3.append(") is out of range ");
                    b3.append("[35, 250]");
                    b3.append(" (in kilogram).");
                    throw new IllegalArgumentException(b3.toString());
                }
                return;
            }
            StringBuilder b4 = yh0.b("heightInCentimeter (");
            b4.append((int) this.d);
            b4.append(") is out of range ");
            b4.append("[100, 250]");
            b4.append(" (in centimeter.");
            throw new IllegalArgumentException(b4.toString());
        }
        throw new IllegalArgumentException(yh0.a(yh0.b("age ("), this.b, ") is out of range [8, ", "110]."));
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(c80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            c80 c80 = (c80) obj;
            return this.b == c80.b && this.c == c80.c && this.d == c80.d && this.e == c80.e && this.f == c80.f;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BiometricProfile");
    }

    @DexIgnore
    public final byte getAge() {
        return this.b;
    }

    @DexIgnore
    public final a getGender() {
        return this.c;
    }

    @DexIgnore
    public final short getHeightInCentimeter() {
        return this.d;
    }

    @DexIgnore
    public final b getWearingPosition() {
        return this.f;
    }

    @DexIgnore
    public final short getWeightInKilogram() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        int hashCode = this.c.hashCode();
        return this.f.hashCode() + ((((((hashCode + (this.b * 31)) * 31) + this.d) * 31) + this.e) * 31);
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeInt(yz0.b(this.d));
        }
        if (parcel != null) {
            parcel.writeInt(yz0.b(this.e));
        }
        if (parcel != null) {
            parcel.writeString(this.f.name());
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("age", Byte.valueOf(this.b));
            jSONObject.put("gender", yz0.a(this.c));
            jSONObject.put("height_in_centimeter", Short.valueOf(this.d));
            jSONObject.put("weight_in_kilogram", Short.valueOf(this.e));
            jSONObject.put("wearing_position", yz0.a(this.f));
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ c80(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = parcel.readByte();
        String readString = parcel.readString();
        if (readString != null) {
            ee7.a((Object) readString, "parcel.readString()!!");
            this.c = a.valueOf(readString);
            this.d = (short) parcel.readInt();
            this.e = (short) parcel.readInt();
            String readString2 = parcel.readString();
            if (readString2 != null) {
                ee7.a((Object) readString2, "parcel.readString()!!");
                this.f = b.valueOf(readString2);
                e();
                return;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }
}
