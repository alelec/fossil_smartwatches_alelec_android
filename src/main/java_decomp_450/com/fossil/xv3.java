package com.fossil;

import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xv3<T> extends hw3<T> {
    @DexIgnore
    public static /* final */ xv3<Object> INSTANCE; // = new xv3<>();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    public static <T> hw3<T> withType() {
        return INSTANCE;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public Set<T> asSet() {
        return Collections.emptySet();
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public boolean equals(Object obj) {
        return obj == this;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public T get() {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public int hashCode() {
        return 2040732332;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public boolean isPresent() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public T or(T t) {
        jw3.a(t, "use Optional.orNull() instead of Optional.or(null)");
        return t;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public T orNull() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public String toString() {
        return "Optional.absent()";
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public <V> hw3<V> transform(cw3<? super T, V> cw3) {
        jw3.a(cw3);
        return hw3.absent();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.hw3<? extends T> */
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: com.fossil.hw3<T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.hw3
    public hw3<T> or(hw3<? extends T> hw3) {
        jw3.a(hw3);
        return hw3;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public T or(nw3<? extends T> nw3) {
        T t = (T) nw3.get();
        jw3.a(t, "use Optional.orNull() instead of a Supplier that returns null");
        return t;
    }
}
