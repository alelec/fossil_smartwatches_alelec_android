package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v20 extends l20<t20> implements qy {
    @DexIgnore
    public v20(t20 t20) {
        super(t20);
    }

    @DexIgnore
    @Override // com.fossil.qy, com.fossil.l20
    public void a() {
        ((t20) ((l20) this).a).e().prepareToDraw();
    }

    @DexIgnore
    @Override // com.fossil.uy
    public void b() {
        ((t20) ((l20) this).a).stop();
        ((t20) ((l20) this).a).k();
    }

    @DexIgnore
    @Override // com.fossil.uy
    public int c() {
        return ((t20) ((l20) this).a).i();
    }

    @DexIgnore
    @Override // com.fossil.uy
    public Class<t20> d() {
        return t20.class;
    }
}
