package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomEditGoalView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j55 extends i55 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i U; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray V;
    @DexIgnore
    public long T;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        V = sparseIntArray;
        sparseIntArray.put(2131363342, 1);
        V.put(2131362636, 2);
        V.put(2131363398, 3);
        V.put(2131363101, 4);
        V.put(2131362060, 5);
        V.put(2131363388, 6);
        V.put(2131361991, 7);
        V.put(2131362414, 8);
        V.put(2131362398, 9);
        V.put(2131361985, 10);
        V.put(2131363403, 11);
        V.put(2131362558, 12);
        V.put(2131362007, 13);
        V.put(2131362008, 14);
        V.put(2131362005, 15);
        V.put(2131362006, 16);
        V.put(2131362415, 17);
        V.put(2131362775, 18);
        V.put(2131362277, 19);
        V.put(2131362418, 20);
        V.put(2131362790, 21);
        V.put(2131362284, 22);
        V.put(2131362502, 23);
        V.put(2131362285, 24);
        V.put(2131362503, 25);
        V.put(2131361907, 26);
        V.put(2131362377, 27);
        V.put(2131363399, 28);
    }
    */

    @DexIgnore
    public j55(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 29, U, V));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.T = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.T != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.T = 1;
        }
        g();
    }

    @DexIgnore
    public j55(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (Barrier) objArr[26], (ConstraintLayout) objArr[10], (FlexibleSwitchCompat) objArr[7], (CustomEditGoalView) objArr[15], (CustomEditGoalView) objArr[16], (CustomEditGoalView) objArr[13], (CustomEditGoalView) objArr[14], (ConstraintLayout) objArr[5], (FlexibleEditText) objArr[19], (FlexibleEditText) objArr[22], (FlexibleEditText) objArr[24], (FlexibleTextView) objArr[27], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[20], (FlexibleTextView) objArr[23], (FlexibleTextView) objArr[25], (Guideline) objArr[12], (RTLImageView) objArr[2], (LinearLayout) objArr[18], (LinearLayout) objArr[21], (ConstraintLayout) objArr[0], (ScrollView) objArr[4], (FlexibleTextView) objArr[1], (View) objArr[6], (View) objArr[3], (View) objArr[28], (Guideline) objArr[11]);
        this.T = -1;
        ((i55) this).M.setTag(null);
        a(view);
        f();
    }
}
