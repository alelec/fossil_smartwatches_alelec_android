package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kc2 {
    @DexIgnore
    public static /* final */ ic2 A; // = ic2.c("body_temperature_measurement_location");
    @DexIgnore
    public static /* final */ ic2 B; // = ic2.c("cervical_mucus_texture");
    @DexIgnore
    public static /* final */ ic2 C; // = ic2.c("cervical_mucus_amount");
    @DexIgnore
    public static /* final */ ic2 D; // = ic2.c("cervical_position");
    @DexIgnore
    public static /* final */ ic2 E; // = ic2.c("cervical_dilation");
    @DexIgnore
    public static /* final */ ic2 F; // = ic2.c("cervical_firmness");
    @DexIgnore
    public static /* final */ ic2 G; // = ic2.c("menstrual_flow");
    @DexIgnore
    public static /* final */ ic2 H; // = ic2.c("ovulation_test_result");
    @DexIgnore
    public static /* final */ ic2 a; // = ic2.d("blood_pressure_systolic");
    @DexIgnore
    public static /* final */ ic2 b; // = ic2.d("blood_pressure_systolic_average");
    @DexIgnore
    public static /* final */ ic2 c; // = ic2.d("blood_pressure_systolic_min");
    @DexIgnore
    public static /* final */ ic2 d; // = ic2.d("blood_pressure_systolic_max");
    @DexIgnore
    public static /* final */ ic2 e; // = ic2.d("blood_pressure_diastolic");
    @DexIgnore
    public static /* final */ ic2 f; // = ic2.d("blood_pressure_diastolic_average");
    @DexIgnore
    public static /* final */ ic2 g; // = ic2.d("blood_pressure_diastolic_min");
    @DexIgnore
    public static /* final */ ic2 h; // = ic2.d("blood_pressure_diastolic_max");
    @DexIgnore
    public static /* final */ ic2 i; // = ic2.c("body_position");
    @DexIgnore
    public static /* final */ ic2 j; // = ic2.c("blood_pressure_measurement_location");
    @DexIgnore
    public static /* final */ ic2 k; // = ic2.d("blood_glucose_level");
    @DexIgnore
    public static /* final */ ic2 l; // = ic2.c("temporal_relation_to_meal");
    @DexIgnore
    public static /* final */ ic2 m; // = ic2.c("temporal_relation_to_sleep");
    @DexIgnore
    public static /* final */ ic2 n; // = ic2.c("blood_glucose_specimen_source");
    @DexIgnore
    public static /* final */ ic2 o; // = ic2.d("oxygen_saturation");
    @DexIgnore
    public static /* final */ ic2 p; // = ic2.d("oxygen_saturation_average");
    @DexIgnore
    public static /* final */ ic2 q; // = ic2.d("oxygen_saturation_min");
    @DexIgnore
    public static /* final */ ic2 r; // = ic2.d("oxygen_saturation_max");
    @DexIgnore
    public static /* final */ ic2 s; // = ic2.d("supplemental_oxygen_flow_rate");
    @DexIgnore
    public static /* final */ ic2 t; // = ic2.d("supplemental_oxygen_flow_rate_average");
    @DexIgnore
    public static /* final */ ic2 u; // = ic2.d("supplemental_oxygen_flow_rate_min");
    @DexIgnore
    public static /* final */ ic2 v; // = ic2.d("supplemental_oxygen_flow_rate_max");
    @DexIgnore
    public static /* final */ ic2 w; // = ic2.c("oxygen_therapy_administration_mode");
    @DexIgnore
    public static /* final */ ic2 x; // = ic2.c("oxygen_saturation_system");
    @DexIgnore
    public static /* final */ ic2 y; // = ic2.c("oxygen_saturation_measurement_method");
    @DexIgnore
    public static /* final */ ic2 z; // = ic2.d("body_temperature");
}
