package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ruler.RulerValuePicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l45 extends k45 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i F; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray G;
    @DexIgnore
    public long E;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        G = sparseIntArray;
        sparseIntArray.put(2131362656, 1);
        G.put(2131362927, 2);
        G.put(2131363342, 3);
        G.put(2131363254, 4);
        G.put(2131363031, 5);
        G.put(2131362421, 6);
        G.put(2131363161, 7);
        G.put(2131363011, 8);
        G.put(2131362530, 9);
        G.put(2131363164, 10);
        G.put(2131363013, 11);
        G.put(2131363331, 12);
        G.put(2131361936, 13);
    }
    */

    @DexIgnore
    public l45(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 14, F, G));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.E = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.E != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.E = 1;
        }
        g();
    }

    @DexIgnore
    public l45(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleButton) objArr[13], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[9], (ImageView) objArr[1], (DashBar) objArr[2], (ConstraintLayout) objArr[0], (RulerValuePicker) objArr[8], (RulerValuePicker) objArr[11], (ScrollView) objArr[5], (TabLayout) objArr[7], (TabLayout) objArr[10], (FlexibleTextView) objArr[4], (FlexibleButton) objArr[12], (FlexibleTextView) objArr[3]);
        this.E = -1;
        ((k45) this).v.setTag(null);
        a(view);
        f();
    }
}
