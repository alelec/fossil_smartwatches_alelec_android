package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import com.zendesk.belvedere.BelvedereCallback;
import com.zendesk.belvedere.BelvedereResult;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class s67 {
    public final q67 a;
    public final x67 b;
    public final Map<Integer, BelvedereResult> c = new HashMap();
    public final u67 d;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /*
        static {
            /*
                com.fossil.w67[] r0 = com.fossil.w67.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.s67.a.a = r0
                com.fossil.w67 r1 = com.fossil.w67.Gallery     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.fossil.s67.a.a     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.w67 r1 = com.fossil.w67.Camera     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.s67.a.<clinit>():void");
        }
        */
    }

    public s67(q67 q67, x67 x67) {
        this.a = q67;
        this.b = x67;
        this.d = q67.b();
    }

    public final boolean a(Context context) {
        if (e(context)) {
            if (!a(context, "android.permission.CAMERA")) {
                return true;
            }
            if (v6.a(context, "android.permission.CAMERA") == 0) {
                return true;
            }
            this.d.w("BelvedereImagePicker", "Found Camera permission declared in AndroidManifest.xml and the user hasn't granted that permission. Not doing any further efforts to acquire that permission.");
        }
        return false;
    }

    public List<t67> b(Context context) {
        TreeSet<w67> c2 = this.a.c();
        ArrayList arrayList = new ArrayList();
        Iterator<w67> it = c2.iterator();
        while (it.hasNext()) {
            t67 t67 = null;
            int i = a.a[it.next().ordinal()];
            if (i == 1) {
                t67 = d(context);
            } else if (i == 2) {
                t67 = c(context);
            }
            if (t67 != null) {
                arrayList.add(t67);
            }
        }
        return arrayList;
    }

    public final t67 c(Context context) {
        if (a(context)) {
            return h(context);
        }
        return null;
    }

    public t67 d(Context context) {
        if (f(context)) {
            return new t67(a(), this.a.h(), w67.Gallery);
        }
        return null;
    }

    public final boolean e(Context context) {
        Intent intent = new Intent();
        intent.setAction("android.media.action.IMAGE_CAPTURE");
        PackageManager packageManager = context.getPackageManager();
        boolean z = packageManager.hasSystemFeature("android.hardware.camera") || packageManager.hasSystemFeature("android.hardware.camera.front");
        boolean a2 = a(intent, context);
        this.d.d("BelvedereImagePicker", String.format(Locale.US, "Camera present: %b, Camera App present: %b", Boolean.valueOf(z), Boolean.valueOf(a2)));
        if (!z || !a2) {
            return false;
        }
        return true;
    }

    public final boolean f(Context context) {
        return a(a(), context);
    }

    public boolean g(Context context) {
        for (w67 w67 : w67.values()) {
            if (a(w67, context)) {
                return true;
            }
        }
        return false;
    }

    public final t67 h(Context context) {
        Set<Integer> keySet = this.c.keySet();
        int d2 = this.a.d();
        int e = this.a.e();
        while (true) {
            if (e >= this.a.d()) {
                break;
            } else if (!keySet.contains(Integer.valueOf(e))) {
                d2 = e;
                break;
            } else {
                e++;
            }
        }
        File a2 = this.b.a(context);
        if (a2 == null) {
            this.d.w("BelvedereImagePicker", "Camera Intent. Image path is null. There's something wrong with the storage.");
            return null;
        }
        Uri a3 = this.b.a(context, a2);
        if (a3 == null) {
            this.d.w("BelvedereImagePicker", "Camera Intent: Uri to file is null. There's something wrong with the storage or FileProvider configuration.");
            return null;
        }
        this.c.put(Integer.valueOf(d2), new BelvedereResult(a2, a3));
        this.d.d("BelvedereImagePicker", String.format(Locale.US, "Camera Intent: Request Id: %s - File: %s - Uri: %s", Integer.valueOf(d2), a2, a3));
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", a3);
        this.b.a(context, intent, a3, 3);
        return new t67(intent, d2, w67.Camera);
    }

    public void a(Context context, int i, int i2, Intent intent, BelvedereCallback<List<BelvedereResult>> belvedereCallback) {
        ArrayList arrayList = new ArrayList();
        if (i == this.a.h()) {
            u67 u67 = this.d;
            Locale locale = Locale.US;
            Object[] objArr = new Object[1];
            objArr[0] = Boolean.valueOf(i2 == -1);
            u67.d("BelvedereImagePicker", String.format(locale, "Parsing activity result - Gallery - Ok: %s", objArr));
            if (i2 == -1) {
                List<Uri> a2 = a(intent);
                this.d.d("BelvedereImagePicker", String.format(Locale.US, "Number of items received from gallery: %s", Integer.valueOf(a2.size())));
                new v67(context, this.d, this.b, belvedereCallback).execute(a2.toArray(new Uri[a2.size()]));
                return;
            }
        } else if (this.c.containsKey(Integer.valueOf(i))) {
            u67 u672 = this.d;
            Locale locale2 = Locale.US;
            Object[] objArr2 = new Object[1];
            objArr2[0] = Boolean.valueOf(i2 == -1);
            u672.d("BelvedereImagePicker", String.format(locale2, "Parsing activity result - Camera - Ok: %s", objArr2));
            BelvedereResult belvedereResult = this.c.get(Integer.valueOf(i));
            this.b.a(context, belvedereResult.b(), 3);
            if (i2 == -1) {
                arrayList.add(belvedereResult);
                this.d.d("BelvedereImagePicker", String.format(Locale.US, "Image from camera: %s", belvedereResult.a()));
            }
            this.c.remove(Integer.valueOf(i));
        }
        if (belvedereCallback != null) {
            belvedereCallback.internalSuccess(arrayList);
        }
    }

    public boolean a(w67 w67, Context context) {
        if (!this.a.c().contains(w67)) {
            return false;
        }
        int i = a.a[w67.ordinal()];
        if (i == 1) {
            return f(context);
        }
        if (i != 2) {
            return false;
        }
        return a(context);
    }

    public final boolean a(Intent intent, Context context) {
        return intent.resolveActivity(context.getPackageManager()) != null;
    }

    @SuppressLint({"NewApi"})
    public final List<Uri> a(Intent intent) {
        ArrayList arrayList = new ArrayList();
        if (Build.VERSION.SDK_INT >= 16 && intent.getClipData() != null) {
            ClipData clipData = intent.getClipData();
            int itemCount = clipData.getItemCount();
            for (int i = 0; i < itemCount; i++) {
                ClipData.Item itemAt = clipData.getItemAt(i);
                if (itemAt.getUri() != null) {
                    arrayList.add(itemAt.getUri());
                }
            }
        } else if (intent.getData() != null) {
            arrayList.add(intent.getData());
        }
        return arrayList;
    }

    @TargetApi(19)
    public final Intent a() {
        Intent intent;
        if (Build.VERSION.SDK_INT >= 19) {
            this.d.d("BelvedereImagePicker", "Gallery Intent, using 'ACTION_OPEN_DOCUMENT'");
            intent = new Intent("android.intent.action.OPEN_DOCUMENT");
        } else {
            this.d.d("BelvedereImagePicker", "Gallery Intent, using 'ACTION_GET_CONTENT'");
            intent = new Intent("android.intent.action.GET_CONTENT");
        }
        intent.setType(this.a.f());
        intent.addCategory("android.intent.category.OPENABLE");
        if (Build.VERSION.SDK_INT >= 18) {
            intent.putExtra("android.intent.extra.ALLOW_MULTIPLE", this.a.a());
        }
        return intent;
    }

    public final boolean a(Context context, String str) {
        try {
            String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
            if (strArr != null && strArr.length > 0) {
                for (String str2 : strArr) {
                    if (str2.equals(str)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            this.d.e("BelvedereImagePicker", "Not able to find permissions in manifest", e);
        }
        return false;
    }
}
