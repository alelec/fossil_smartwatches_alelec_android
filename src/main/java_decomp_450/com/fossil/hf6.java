package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.af6;
import com.fossil.be5;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hf6 extends go5 implements gf6, View.OnClickListener, af6.b {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public af6 f;
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public /* final */ Calendar h; // = Calendar.getInstance();
    @DexIgnore
    public qw6<qx4> i;
    @DexIgnore
    public ff6 j;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public HashMap y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final hf6 a(Date date) {
            ee7.b(date, "date");
            hf6 hf6 = new hf6();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            hf6.setArguments(bundle);
            return hf6;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends AppBarLayout.Behavior.a {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ qf b;

        @DexIgnore
        public b(hf6 hf6, boolean z, qf qfVar, ob5 ob5) {
            this.a = z;
            this.b = qfVar;
        }

        @DexIgnore
        @Override // com.google.android.material.appbar.AppBarLayout.BaseBehavior.b
        public boolean a(AppBarLayout appBarLayout) {
            ee7.b(appBarLayout, "appBarLayout");
            return this.a && (this.b.isEmpty() ^ true);
        }
    }

    @DexIgnore
    public hf6() {
        String b2 = eh5.l.a().b("nonBrandSurface");
        String str = "#FFFFFF";
        this.s = Color.parseColor(b2 == null ? str : b2);
        String b3 = eh5.l.a().b("backgroundDashboard");
        this.t = Color.parseColor(b3 == null ? str : b3);
        String b4 = eh5.l.a().b("secondaryText");
        this.u = Color.parseColor(b4 == null ? str : b4);
        String b5 = eh5.l.a().b("primaryText");
        this.v = Color.parseColor(b5 == null ? str : b5);
        String b6 = eh5.l.a().b("nonBrandDisableCalendarDay");
        this.w = Color.parseColor(b6 == null ? str : b6);
        String b7 = eh5.l.a().b("nonBrandNonReachGoal");
        this.x = Color.parseColor(b7 != null ? b7 : str);
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.af6.b
    public void b(WorkoutSession workoutSession) {
        ee7.b(workoutSession, "workoutSession");
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "CaloriesDetailFragment";
    }

    @DexIgnore
    public final void f1() {
        qx4 a2;
        OverviewDayChart overviewDayChart;
        qw6<qx4> qw6 = this.i;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewDayChart = a2.w) != null) {
            be5.a aVar = be5.o;
            ff6 ff6 = this.j;
            if (aVar.a(ff6 != null ? ff6.h() : null)) {
                overviewDayChart.a("dianaActiveCaloriesTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.a("hybridActiveCaloriesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("CaloriesDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131362636:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362637:
                    ff6 ff6 = this.j;
                    if (ff6 != null) {
                        ff6.m();
                        return;
                    }
                    return;
                case 2131362705:
                    ff6 ff62 = this.j;
                    if (ff62 != null) {
                        ff62.l();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        qx4 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        qx4 qx4 = (qx4) qb.a(layoutInflater, 2131558506, viewGroup, false, a1());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        this.g = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.g = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        ee7.a((Object) qx4, "binding");
        a(qx4);
        ff6 ff6 = this.j;
        if (ff6 != null) {
            ff6.a(this.g);
        }
        this.i = new qw6<>(this, qx4);
        f1();
        qw6<qx4> qw6 = this.i;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        ff6 ff6 = this.j;
        if (ff6 != null) {
            ff6.k();
        }
        Lifecycle lifecycle = getLifecycle();
        ff6 ff62 = this.j;
        if (ff62 != null) {
            lifecycle.b(ff62.j());
            super.onDestroyView();
            Z0();
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        ff6 ff6 = this.j;
        if (ff6 != null) {
            ff6.g();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        f1();
        ff6 ff6 = this.j;
        if (ff6 != null) {
            ff6.b(this.g);
        }
        ff6 ff62 = this.j;
        if (ff62 != null) {
            ff62.f();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        ff6 ff6 = this.j;
        if (ff6 != null) {
            ff6.a(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public final void a(qx4 qx4) {
        String str;
        qx4.F.setOnClickListener(this);
        qx4.G.setOnClickListener(this);
        qx4.H.setOnClickListener(this);
        qx4.J.setBackgroundColor(this.t);
        qx4.r.setBackgroundColor(this.s);
        be5.a aVar = be5.o;
        ff6 ff6 = this.j;
        if (aVar.a(ff6 != null ? ff6.h() : null)) {
            str = eh5.l.a().b("dianaActiveCaloriesTab");
        } else {
            str = eh5.l.a().b("hybridActiveCaloriesTab");
        }
        this.p = str;
        this.q = eh5.l.a().b("nonBrandActivityDetailBackground");
        this.r = eh5.l.a().b("onDianaActiveCaloriesTab");
        af6 af6 = new af6(af6.d.CALORIES, ob5.IMPERIAL, new WorkoutSessionDifference(), this.p);
        this.f = af6;
        if (af6 != null) {
            af6.a(this);
        }
        RecyclerView recyclerView = qx4.M;
        ee7.a((Object) recyclerView, "it");
        recyclerView.setAdapter(this.f);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable c = v6.c(recyclerView.getContext(), 2131230856);
        if (c != null) {
            fe6 fe6 = new fe6(linearLayoutManager.Q(), false, false, 6, null);
            ee7.a((Object) c, ResourceManager.DRAWABLE);
            fe6.a(c);
            recyclerView.addItemDecoration(fe6);
        }
    }

    @DexIgnore
    public void a(ff6 ff6) {
        ee7.b(ff6, "presenter");
        this.j = ff6;
        Lifecycle lifecycle = getLifecycle();
        ff6 ff62 = this.j;
        if (ff62 != null) {
            lifecycle.a(ff62.j());
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.gf6
    public void a(Date date, boolean z2, boolean z3, boolean z4) {
        qx4 a2;
        ee7.b(date, "date");
        this.g = date;
        Calendar calendar = this.h;
        ee7.a((Object) calendar, "calendar");
        calendar.setTime(date);
        int i2 = this.h.get(7);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z2 + " - isToday - " + z3 + " - isDateAfter: " + z4 + " - calendar: " + this.h);
        qw6<qx4> qw6 = this.i;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            a2.q.a(true, true);
            FlexibleTextView flexibleTextView = a2.z;
            ee7.a((Object) flexibleTextView, "binding.ftvDayOfMonth");
            flexibleTextView.setText(String.valueOf(this.h.get(5)));
            if (z2) {
                RTLImageView rTLImageView = a2.G;
                ee7.a((Object) rTLImageView, "binding.ivBackDate");
                rTLImageView.setVisibility(4);
            } else {
                RTLImageView rTLImageView2 = a2.G;
                ee7.a((Object) rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setVisibility(0);
            }
            if (z3 || z4) {
                RTLImageView rTLImageView3 = a2.H;
                ee7.a((Object) rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setVisibility(8);
                if (z3) {
                    FlexibleTextView flexibleTextView2 = a2.A;
                    ee7.a((Object) flexibleTextView2, "binding.ftvDayOfWeek");
                    flexibleTextView2.setText(ig5.a(getContext(), 2131886583));
                    return;
                }
                FlexibleTextView flexibleTextView3 = a2.A;
                ee7.a((Object) flexibleTextView3, "binding.ftvDayOfWeek");
                flexibleTextView3.setText(xe5.b.b(i2));
                return;
            }
            RTLImageView rTLImageView4 = a2.H;
            ee7.a((Object) rTLImageView4, "binding.ivNextDate");
            rTLImageView4.setVisibility(0);
            FlexibleTextView flexibleTextView4 = a2.A;
            ee7.a((Object) flexibleTextView4, "binding.ftvDayOfWeek");
            flexibleTextView4.setText(xe5.b.b(i2));
        }
    }

    @DexIgnore
    @Override // com.fossil.gf6
    public void a(ob5 ob5, ActivitySummary activitySummary) {
        qx4 a2;
        int i2;
        int i3;
        ee7.b(ob5, "distanceUnit");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesDetailFragment", "showDayDetail - distanceUnit=" + ob5 + ", activitySummary=" + activitySummary);
        qw6<qx4> qw6 = this.i;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            ee7.a((Object) a2, "binding");
            View d = a2.d();
            ee7.a((Object) d, "binding.root");
            Context context = d.getContext();
            if (activitySummary != null) {
                i2 = af7.a((float) activitySummary.getCalories());
                i3 = activitySummary.getCaloriesGoal();
            } else {
                i3 = 0;
                i2 = 0;
            }
            if (i2 > 0) {
                FlexibleTextView flexibleTextView = a2.y;
                ee7.a((Object) flexibleTextView, "binding.ftvDailyValue");
                flexibleTextView.setText(af5.a.a(Float.valueOf((float) i2)));
                FlexibleTextView flexibleTextView2 = a2.x;
                ee7.a((Object) flexibleTextView2, "binding.ftvDailyUnit");
                String a3 = ig5.a(context, 2131886577);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026aloriesMonth_Label__Cals)");
                if (a3 != null) {
                    String lowerCase = a3.toLowerCase();
                    ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    flexibleTextView2.setText(lowerCase);
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                FlexibleTextView flexibleTextView3 = a2.y;
                ee7.a((Object) flexibleTextView3, "binding.ftvDailyValue");
                flexibleTextView3.setText("");
                FlexibleTextView flexibleTextView4 = a2.x;
                ee7.a((Object) flexibleTextView4, "binding.ftvDailyUnit");
                String a4 = ig5.a(context, 2131886581);
                ee7.a((Object) a4, "LanguageHelper.getString\u2026riesToday_Text__NoRecord)");
                if (a4 != null) {
                    String upperCase = a4.toUpperCase();
                    ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                    flexibleTextView4.setText(upperCase);
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            }
            int i4 = i3 > 0 ? (i2 * 100) / i3 : -1;
            if (i2 >= i3 && i3 > 0) {
                a2.A.setTextColor(this.s);
                a2.z.setTextColor(this.s);
                a2.x.setTextColor(this.s);
                a2.y.setTextColor(this.s);
                a2.B.setTextColor(this.s);
                RTLImageView rTLImageView = a2.H;
                ee7.a((Object) rTLImageView, "binding.ivNextDate");
                rTLImageView.setSelected(true);
                RTLImageView rTLImageView2 = a2.G;
                ee7.a((Object) rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setSelected(true);
                ConstraintLayout constraintLayout = a2.s;
                ee7.a((Object) constraintLayout, "binding.clOverviewDay");
                constraintLayout.setSelected(true);
                FlexibleTextView flexibleTextView5 = a2.A;
                ee7.a((Object) flexibleTextView5, "binding.ftvDayOfWeek");
                flexibleTextView5.setSelected(true);
                FlexibleTextView flexibleTextView6 = a2.z;
                ee7.a((Object) flexibleTextView6, "binding.ftvDayOfMonth");
                flexibleTextView6.setSelected(true);
                View view = a2.I;
                ee7.a((Object) view, "binding.line");
                view.setSelected(true);
                FlexibleTextView flexibleTextView7 = a2.y;
                ee7.a((Object) flexibleTextView7, "binding.ftvDailyValue");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = a2.x;
                ee7.a((Object) flexibleTextView8, "binding.ftvDailyUnit");
                flexibleTextView8.setSelected(true);
                FlexibleTextView flexibleTextView9 = a2.B;
                ee7.a((Object) flexibleTextView9, "binding.ftvEst");
                flexibleTextView9.setSelected(true);
                String str = this.r;
                if (str != null) {
                    a2.A.setTextColor(Color.parseColor(str));
                    a2.z.setTextColor(Color.parseColor(str));
                    a2.y.setTextColor(Color.parseColor(str));
                    a2.x.setTextColor(Color.parseColor(str));
                    a2.B.setTextColor(Color.parseColor(str));
                    a2.I.setBackgroundColor(Color.parseColor(str));
                    a2.H.setColorFilter(Color.parseColor(str));
                    a2.G.setColorFilter(Color.parseColor(str));
                }
                String str2 = this.p;
                if (str2 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str2));
                }
            } else if (i2 > 0) {
                a2.z.setTextColor(this.v);
                a2.A.setTextColor(this.u);
                a2.x.setTextColor(this.x);
                a2.y.setTextColor(this.v);
                a2.B.setTextColor(this.v);
                View view2 = a2.I;
                ee7.a((Object) view2, "binding.line");
                view2.setSelected(false);
                RTLImageView rTLImageView3 = a2.H;
                ee7.a((Object) rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setSelected(false);
                RTLImageView rTLImageView4 = a2.G;
                ee7.a((Object) rTLImageView4, "binding.ivBackDate");
                rTLImageView4.setSelected(false);
                int i5 = this.x;
                a2.I.setBackgroundColor(i5);
                a2.H.setColorFilter(i5);
                a2.G.setColorFilter(i5);
                String str3 = this.q;
                if (str3 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str3));
                }
            } else {
                a2.z.setTextColor(this.v);
                a2.A.setTextColor(this.u);
                a2.y.setTextColor(this.w);
                a2.x.setTextColor(this.w);
                RTLImageView rTLImageView5 = a2.H;
                ee7.a((Object) rTLImageView5, "binding.ivNextDate");
                rTLImageView5.setSelected(false);
                RTLImageView rTLImageView6 = a2.G;
                ee7.a((Object) rTLImageView6, "binding.ivBackDate");
                rTLImageView6.setSelected(false);
                ConstraintLayout constraintLayout2 = a2.s;
                ee7.a((Object) constraintLayout2, "binding.clOverviewDay");
                constraintLayout2.setSelected(false);
                FlexibleTextView flexibleTextView10 = a2.A;
                ee7.a((Object) flexibleTextView10, "binding.ftvDayOfWeek");
                flexibleTextView10.setSelected(false);
                FlexibleTextView flexibleTextView11 = a2.z;
                ee7.a((Object) flexibleTextView11, "binding.ftvDayOfMonth");
                flexibleTextView11.setSelected(false);
                View view3 = a2.I;
                ee7.a((Object) view3, "binding.line");
                view3.setSelected(false);
                FlexibleTextView flexibleTextView12 = a2.y;
                ee7.a((Object) flexibleTextView12, "binding.ftvDailyValue");
                flexibleTextView12.setSelected(false);
                FlexibleTextView flexibleTextView13 = a2.x;
                ee7.a((Object) flexibleTextView13, "binding.ftvDailyUnit");
                flexibleTextView13.setSelected(false);
                FlexibleTextView flexibleTextView14 = a2.B;
                ee7.a((Object) flexibleTextView14, "binding.ftvEst");
                flexibleTextView14.setSelected(false);
                int i6 = this.x;
                a2.I.setBackgroundColor(i6);
                a2.H.setColorFilter(i6);
                a2.G.setColorFilter(i6);
                String str4 = this.q;
                if (str4 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str4));
                }
            }
            if (i4 == -1) {
                FlexibleProgressBar flexibleProgressBar = a2.K;
                ee7.a((Object) flexibleProgressBar, "binding.pbGoal");
                flexibleProgressBar.setProgress(0);
                FlexibleTextView flexibleTextView15 = a2.E;
                ee7.a((Object) flexibleTextView15, "binding.ftvProgressValue");
                flexibleTextView15.setText(ig5.a(context, 2131887288));
            } else {
                FlexibleProgressBar flexibleProgressBar2 = a2.K;
                ee7.a((Object) flexibleProgressBar2, "binding.pbGoal");
                flexibleProgressBar2.setProgress(i4);
                FlexibleTextView flexibleTextView16 = a2.E;
                ee7.a((Object) flexibleTextView16, "binding.ftvProgressValue");
                flexibleTextView16.setText(i4 + "%");
            }
            FlexibleTextView flexibleTextView17 = a2.C;
            ee7.a((Object) flexibleTextView17, "binding.ftvGoalValue");
            we7 we7 = we7.a;
            String a5 = ig5.a(context, 2131886561);
            ee7.a((Object) a5, "LanguageHelper.getString\u2026Page_Title__OfNumberCals)");
            String format = String.format(a5, Arrays.copyOf(new Object[]{af5.a.a(Float.valueOf((float) i3))}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            flexibleTextView17.setText(format);
        }
    }

    @DexIgnore
    @Override // com.fossil.gf6
    public void a(do5 do5, ArrayList<String> arrayList) {
        qx4 a2;
        OverviewDayChart overviewDayChart;
        ee7.b(do5, "baseModel");
        ee7.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesDetailFragment", "showDayDetailChart - baseModel=" + do5);
        qw6<qx4> qw6 = this.i;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewDayChart = a2.w) != null) {
            BarChart.c cVar = (BarChart.c) do5;
            cVar.b(do5.a.a(cVar.c()));
            if (!arrayList.isEmpty()) {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
            } else {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) xe5.b.b(), false, 2, (Object) null);
            }
            overviewDayChart.a(do5);
        }
    }

    @DexIgnore
    @Override // com.fossil.gf6
    public void a(boolean z2, ob5 ob5, qf<WorkoutSession> qfVar) {
        qx4 a2;
        ee7.b(ob5, "distanceUnit");
        ee7.b(qfVar, "workoutSessions");
        qw6<qx4> qw6 = this.i;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            if (z2) {
                LinearLayout linearLayout = a2.J;
                ee7.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                if (!qfVar.isEmpty()) {
                    FlexibleTextView flexibleTextView = a2.D;
                    ee7.a((Object) flexibleTextView, "it.ftvNoWorkoutRecorded");
                    flexibleTextView.setVisibility(8);
                    RecyclerView recyclerView = a2.M;
                    ee7.a((Object) recyclerView, "it.rvWorkout");
                    recyclerView.setVisibility(0);
                    af6 af6 = this.f;
                    if (af6 != null) {
                        af6.a(ob5, qfVar);
                    }
                } else {
                    FlexibleTextView flexibleTextView2 = a2.D;
                    ee7.a((Object) flexibleTextView2, "it.ftvNoWorkoutRecorded");
                    flexibleTextView2.setVisibility(0);
                    RecyclerView recyclerView2 = a2.M;
                    ee7.a((Object) recyclerView2, "it.rvWorkout");
                    recyclerView2.setVisibility(8);
                    af6 af62 = this.f;
                    if (af62 != null) {
                        af62.a(ob5, qfVar);
                    }
                }
            } else {
                LinearLayout linearLayout2 = a2.J;
                ee7.a((Object) linearLayout2, "it.llWorkout");
                linearLayout2.setVisibility(8);
            }
            AppBarLayout appBarLayout = a2.q;
            ee7.a((Object) appBarLayout, "it.appBarLayout");
            ViewGroup.LayoutParams layoutParams = appBarLayout.getLayoutParams();
            if (layoutParams != null) {
                CoordinatorLayout.e eVar = (CoordinatorLayout.e) layoutParams;
                AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) eVar.d();
                if (behavior == null) {
                    behavior = new AppBarLayout.Behavior();
                }
                behavior.setDragCallback(new b(this, z2, qfVar, ob5));
                eVar.a(behavior);
                return;
            }
            throw new x87("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
        }
    }

    @DexIgnore
    @Override // com.fossil.af6.b
    public void a(WorkoutSession workoutSession) {
        String str;
        ob5 i2;
        ee7.b(workoutSession, "workoutSession");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ff6 ff6 = this.j;
            if (ff6 == null || (i2 = ff6.i()) == null || (str = i2.name()) == null) {
                str = ob5.METRIC.name();
            }
            WorkoutDetailActivity.a aVar = WorkoutDetailActivity.e;
            ee7.a((Object) activity, "it");
            aVar.a(activity, workoutSession.getId(), str);
        }
    }
}
