package com.fossil;

import android.content.res.AssetManager;
import android.os.ParcelFileDescriptor;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mx extends gx<ParcelFileDescriptor> {
    @DexIgnore
    public mx(AssetManager assetManager, String str) {
        super(assetManager, str);
    }

    @DexIgnore
    @Override // com.fossil.ix
    public Class<ParcelFileDescriptor> getDataClass() {
        return ParcelFileDescriptor.class;
    }

    @DexIgnore
    @Override // com.fossil.gx
    public ParcelFileDescriptor a(AssetManager assetManager, String str) throws IOException {
        return assetManager.openFd(str).getParcelFileDescriptor();
    }

    @DexIgnore
    public void a(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
        parcelFileDescriptor.close();
    }
}
