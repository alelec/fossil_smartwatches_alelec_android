package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e43 implements a43 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a; // = new dr2(uq2.a("com.google.android.gms.measurement")).a("measurement.integration.disable_firebase_instance_id", false);

    @DexIgnore
    @Override // com.fossil.a43
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.a43
    public final boolean zzb() {
        return a.b().booleanValue();
    }
}
