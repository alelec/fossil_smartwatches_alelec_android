package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ir {
    @DexIgnore
    public /* final */ Bitmap.Config a;
    @DexIgnore
    public /* final */ ColorSpace b;
    @DexIgnore
    public /* final */ qt c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ fo7 f;
    @DexIgnore
    public /* final */ ht g;
    @DexIgnore
    public /* final */ dt h;
    @DexIgnore
    public /* final */ dt i;

    @DexIgnore
    public ir(Bitmap.Config config, ColorSpace colorSpace, qt qtVar, boolean z, boolean z2, fo7 fo7, ht htVar, dt dtVar, dt dtVar2) {
        ee7.b(config, "config");
        ee7.b(qtVar, "scale");
        ee7.b(fo7, "headers");
        ee7.b(htVar, "parameters");
        ee7.b(dtVar, "networkCachePolicy");
        ee7.b(dtVar2, "diskCachePolicy");
        this.a = config;
        this.b = colorSpace;
        this.c = qtVar;
        this.d = z;
        this.e = z2;
        this.f = fo7;
        this.g = htVar;
        this.h = dtVar;
        this.i = dtVar2;
    }

    @DexIgnore
    public final boolean a() {
        return this.d;
    }

    @DexIgnore
    public final boolean b() {
        return this.e;
    }

    @DexIgnore
    public final ColorSpace c() {
        return this.b;
    }

    @DexIgnore
    public final Bitmap.Config d() {
        return this.a;
    }

    @DexIgnore
    public final dt e() {
        return this.i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ir)) {
            return false;
        }
        ir irVar = (ir) obj;
        return ee7.a(this.a, irVar.a) && ee7.a(this.b, irVar.b) && ee7.a(this.c, irVar.c) && this.d == irVar.d && this.e == irVar.e && ee7.a(this.f, irVar.f) && ee7.a(this.g, irVar.g) && ee7.a(this.h, irVar.h) && ee7.a(this.i, irVar.i);
    }

    @DexIgnore
    public final fo7 f() {
        return this.f;
    }

    @DexIgnore
    public final dt g() {
        return this.h;
    }

    @DexIgnore
    public final qt h() {
        return this.c;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r2v6, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r3v2, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        Bitmap.Config config = this.a;
        int i2 = 0;
        int hashCode = (config != null ? config.hashCode() : 0) * 31;
        ColorSpace colorSpace = this.b;
        int hashCode2 = (hashCode + (colorSpace != null ? colorSpace.hashCode() : 0)) * 31;
        qt qtVar = this.c;
        int hashCode3 = (hashCode2 + (qtVar != null ? qtVar.hashCode() : 0)) * 31;
        boolean z = this.d;
        int i3 = 1;
        if (z) {
            z = true;
        }
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = (hashCode3 + i4) * 31;
        boolean z2 = this.e;
        if (z2 == 0) {
            i3 = z2;
        }
        int i7 = (i6 + i3) * 31;
        fo7 fo7 = this.f;
        int hashCode4 = (i7 + (fo7 != null ? fo7.hashCode() : 0)) * 31;
        ht htVar = this.g;
        int hashCode5 = (hashCode4 + (htVar != null ? htVar.hashCode() : 0)) * 31;
        dt dtVar = this.h;
        int hashCode6 = (hashCode5 + (dtVar != null ? dtVar.hashCode() : 0)) * 31;
        dt dtVar2 = this.i;
        if (dtVar2 != null) {
            i2 = dtVar2.hashCode();
        }
        return hashCode6 + i2;
    }

    @DexIgnore
    public String toString() {
        return "Options(config=" + this.a + ", colorSpace=" + this.b + ", scale=" + this.c + ", allowInexactSize=" + this.d + ", allowRgb565=" + this.e + ", headers=" + this.f + ", parameters=" + this.g + ", networkCachePolicy=" + this.h + ", diskCachePolicy=" + this.i + ")";
    }
}
