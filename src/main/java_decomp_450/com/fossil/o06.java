package com.fossil;

import com.portfolio.platform.data.source.CategoryRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o06 implements Factory<n06> {
    @DexIgnore
    public static n06 a(j06 j06, CategoryRepository categoryRepository) {
        return new n06(j06, categoryRepository);
    }
}
