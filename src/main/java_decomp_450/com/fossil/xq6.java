package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ov3;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xq6 extends mr6 {
    @DexIgnore
    public static /* final */ a y; // = new a(null);
    @DexIgnore
    public rr6 g;
    @DexIgnore
    public bs6 h;
    @DexIgnore
    public qw6<q45> i;
    @DexIgnore
    public vl4 j; // = new vl4();
    @DexIgnore
    public String p; // = "";
    @DexIgnore
    public List<String> q; // = new ArrayList();
    @DexIgnore
    public int r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public int t;
    @DexIgnore
    public /* final */ String u; // = eh5.l.a().b("disabledButton");
    @DexIgnore
    public /* final */ String v; // = eh5.l.a().b("primaryColor");
    @DexIgnore
    public /* final */ String w; // = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public HashMap x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final xq6 a(List<String> list, boolean z) {
            ee7.b(list, "instructionList");
            xq6 xq6 = new xq6();
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            xq6.setArguments(bundle);
            xq6.q = list;
            xq6.r = 0;
            return xq6;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final xq6 a(String str, boolean z) {
            ee7.b(str, "serial");
            xq6 xq6 = new xq6();
            xq6.p = str;
            xq6.r = 1;
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            xq6.setArguments(bundle);
            return xq6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ov3.b {
        @DexIgnore
        public /* final */ /* synthetic */ xq6 a;

        @DexIgnore
        public b(xq6 xq6) {
            this.a = xq6;
        }

        @DexIgnore
        @Override // com.fossil.ov3.b
        public final void a(TabLayout.g gVar, int i) {
            ee7.b(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.u) && !TextUtils.isEmpty(this.a.v)) {
                int parseColor = Color.parseColor(this.a.u);
                int parseColor2 = Color.parseColor(this.a.v);
                gVar.b(2131230963);
                if (i == this.a.t) {
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable b2 = gVar.b();
                if (b2 != null) {
                    b2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ q45 a;
        @DexIgnore
        public /* final */ /* synthetic */ xq6 b;

        @DexIgnore
        public c(q45 q45, xq6 xq6) {
            this.a = q45;
            this.b = xq6;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i) {
            Drawable b2;
            Drawable b3;
            if (i == 0) {
                FlexibleTextView flexibleTextView = this.a.w;
                ee7.a((Object) flexibleTextView, "binding.ftvTimeout");
                flexibleTextView.setVisibility(0);
            } else {
                FlexibleTextView flexibleTextView2 = this.a.w;
                ee7.a((Object) flexibleTextView2, "binding.ftvTimeout");
                flexibleTextView2.setVisibility(4);
            }
            if (!TextUtils.isEmpty(this.b.v)) {
                int parseColor = Color.parseColor(this.b.v);
                TabLayout.g b4 = this.a.z.b(i);
                if (!(b4 == null || (b3 = b4.b()) == null)) {
                    b3.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.u) && this.b.t != i) {
                int parseColor2 = Color.parseColor(this.b.u);
                TabLayout.g b5 = this.a.z.b(this.b.t);
                if (!(b5 == null || (b2 = b5.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.b.t = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xq6 a;

        @DexIgnore
        public d(xq6 xq6) {
            this.a = xq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            xq6.c(this.a).h();
            this.a.e1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xq6 a;

        @DexIgnore
        public e(xq6 xq6) {
            this.a = xq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
            Context context = this.a.getContext();
            if (context != null) {
                ee7.a((Object) context, "context!!");
                aVar.a(context, this.a.p, true, this.a.s);
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xq6 a;

        @DexIgnore
        public f(xq6 xq6) {
            this.a = xq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ee7.a((Object) activity, "activity!!");
                if (!activity.isFinishing()) {
                    FragmentActivity activity2 = this.a.getActivity();
                    if (activity2 != null) {
                        ee7.a((Object) activity2, "activity!!");
                        if (!activity2.isDestroyed()) {
                            FragmentActivity activity3 = this.a.getActivity();
                            if (activity3 != null) {
                                activity3.finish();
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ rr6 c(xq6 xq6) {
        rr6 rr6 = xq6.g;
        if (rr6 != null) {
            return rr6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void T(boolean z) {
        bs6 bs6 = this.h;
        if (bs6 != null) {
            bs6.a(z);
        } else {
            ee7.d("mSubPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void Y(String str) {
        ee7.b(str, "serial");
        this.p = str;
        qw6<q45> qw6 = this.i;
        if (qw6 != null) {
            q45 a2 = qw6.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                ee7.a((Object) constraintLayout, "it.clPairingAuthorize");
                constraintLayout.setVisibility(8);
                ConstraintLayout constraintLayout2 = a2.q;
                ee7.a((Object) constraintLayout2, "it.clAuthorizeFailed");
                constraintLayout2.setVisibility(0);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.mr6
    public void Z0() {
        HashMap hashMap = this.x;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void n(int i2) {
        qw6<q45> qw6 = this.i;
        if (qw6 != null) {
            q45 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                ee7.a((Object) flexibleTextView, "it.ftvTimeout");
                we7 we7 = we7.a;
                Context context = getContext();
                if (context != null) {
                    String a3 = ig5.a(context, 2131886838);
                    ee7.a((Object) a3, "LanguageHelper.getString\u2026_ExpiringInSecondSeconds)");
                    String format = String.format(a3, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
                    ee7.a((Object) format, "java.lang.String.format(format, *args)");
                    flexibleTextView.setText(format);
                    return;
                }
                ee7.a();
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        qw6<q45> qw6 = new qw6<>(this, (q45) qb.a(layoutInflater, 2131558601, viewGroup, false, a1()));
        this.i = qw6;
        if (qw6 != null) {
            q45 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.mr6, androidx.fragment.app.Fragment
    public void onDestroyView() {
        bs6 bs6 = this.h;
        if (bs6 != null) {
            bs6.a();
            super.onDestroyView();
            Z0();
            return;
        }
        ee7.d("mSubPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        this.j.a(this.q);
        this.h = new bs6(this);
        Bundle arguments = getArguments();
        this.s = arguments != null ? arguments.getBoolean("IS_ONBOARDING_FLOW") : false;
        qw6<q45> qw6 = this.i;
        if (qw6 != null) {
            q45 a2 = qw6.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.C;
                ee7.a((Object) viewPager2, "binding.vpAuthorizeInstruction");
                viewPager2.setAdapter(this.j);
                if (!TextUtils.isEmpty(this.w)) {
                    TabLayout tabLayout = a2.z;
                    ee7.a((Object) tabLayout, "binding.indicator");
                    tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.w)));
                }
                qw6<q45> qw62 = this.i;
                if (qw62 != null) {
                    q45 a3 = qw62.a();
                    TabLayout tabLayout2 = a3 != null ? a3.z : null;
                    if (tabLayout2 != null) {
                        qw6<q45> qw63 = this.i;
                        if (qw63 != null) {
                            q45 a4 = qw63.a();
                            ViewPager2 viewPager22 = a4 != null ? a4.C : null;
                            if (viewPager22 != null) {
                                new ov3(tabLayout2, viewPager22, new b(this)).a();
                                a2.C.a(new c(a2, this));
                                a2.s.setOnClickListener(new d(this));
                                a2.v.setOnClickListener(new e(this));
                                a2.t.setOnClickListener(new f(this));
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.d("mBinding");
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            }
            int i2 = this.r;
            if (i2 == 0) {
                x(this.q);
            } else if (i2 == 1) {
                Y(this.p);
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void x(List<String> list) {
        ee7.b(list, "instructionList");
        this.j.a(list);
        qw6<q45> qw6 = this.i;
        if (qw6 != null) {
            q45 a2 = qw6.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                ee7.a((Object) constraintLayout, "it.clPairingAuthorize");
                constraintLayout.setVisibility(0);
                ConstraintLayout constraintLayout2 = a2.q;
                ee7.a((Object) constraintLayout2, "it.clAuthorizeFailed");
                constraintLayout2.setVisibility(8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(rr6 rr6) {
        ee7.b(rr6, "presenter");
        this.g = rr6;
    }
}
