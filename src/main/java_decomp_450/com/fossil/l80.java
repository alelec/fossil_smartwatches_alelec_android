package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l80 extends n80 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ long c; // = 4294967295L;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<l80> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final l80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 4) {
                return new l80(yz0.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0)));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", require: 4"));
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public l80 createFromParcel(Parcel parcel) {
            return new l80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public l80[] newArray(int i) {
            return new l80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public l80 m37createFromParcel(Parcel parcel) {
            return new l80(parcel, null);
        }
    }

    /*
    static {
        de7 de7 = de7.a;
    }
    */

    @DexIgnore
    public l80(long j) throws IllegalArgumentException {
        super(o80.DAILY_STEP_GOAL);
        this.b = j;
        e();
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.b).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        long j = c;
        long j2 = this.b;
        if (!(0 <= j2 && j >= j2)) {
            StringBuilder b2 = yh0.b("step(");
            b2.append(this.b);
            b2.append(") is out of range ");
            b2.append("[0, ");
            b2.append(c);
            b2.append("].");
            throw new IllegalArgumentException(b2.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(l80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((l80) obj).b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepGoalConfig");
    }

    @DexIgnore
    public final long getStep() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        return Long.valueOf(this.b).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.b);
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public Long d() {
        return Long.valueOf(this.b);
    }

    @DexIgnore
    public /* synthetic */ l80(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = parcel.readLong();
        e();
    }
}
