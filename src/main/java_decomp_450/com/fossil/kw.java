package com.fossil;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.StrictMode;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kw implements Closeable {
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ File d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public long f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public long h; // = 0;
    @DexIgnore
    public Writer i;
    @DexIgnore
    public /* final */ LinkedHashMap<String, d> j; // = new LinkedHashMap<>(0, 0.75f, true);
    @DexIgnore
    public int p;
    @DexIgnore
    public long q; // = 0;
    @DexIgnore
    public /* final */ ThreadPoolExecutor r; // = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new b(null));
    @DexIgnore
    public /* final */ Callable<Void> s; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Callable<Void> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Void call() throws Exception {
            synchronized (kw.this) {
                if (kw.this.i == null) {
                    return null;
                }
                kw.this.l();
                if (kw.this.c()) {
                    kw.this.k();
                    int unused = kw.this.p = 0;
                }
                return null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ThreadFactory {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public synchronized Thread newThread(Runnable runnable) {
            Thread thread;
            thread = new Thread(runnable, "glide-disk-lru-cache-thread");
            thread.setPriority(1);
            return thread;
        }

        @DexIgnore
        public /* synthetic */ b(a aVar) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c {
        @DexIgnore
        public /* final */ d a;
        @DexIgnore
        public /* final */ boolean[] b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public /* synthetic */ c(kw kwVar, d dVar, a aVar) {
            this(dVar);
        }

        @DexIgnore
        public void c() throws IOException {
            kw.this.a(this, true);
            this.c = true;
        }

        @DexIgnore
        public c(d dVar) {
            this.a = dVar;
            this.b = dVar.e ? null : new boolean[kw.this.g];
        }

        @DexIgnore
        public File a(int i) throws IOException {
            File b2;
            synchronized (kw.this) {
                if (this.a.f == this) {
                    if (!this.a.e) {
                        this.b[i] = true;
                    }
                    b2 = this.a.b(i);
                    if (!kw.this.a.exists()) {
                        kw.this.a.mkdirs();
                    }
                } else {
                    throw new IllegalStateException();
                }
            }
            return b2;
        }

        @DexIgnore
        public void b() {
            if (!this.c) {
                try {
                    a();
                } catch (IOException unused) {
                }
            }
        }

        @DexIgnore
        public void a() throws IOException {
            kw.this.a(this, false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ long[] b;
        @DexIgnore
        public File[] c;
        @DexIgnore
        public File[] d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public c f;
        @DexIgnore
        public long g;

        @DexIgnore
        public /* synthetic */ d(kw kwVar, String str, a aVar) {
            this(str);
        }

        @DexIgnore
        public d(String str) {
            this.a = str;
            this.b = new long[kw.this.g];
            this.c = new File[kw.this.g];
            this.d = new File[kw.this.g];
            StringBuilder sb = new StringBuilder(str);
            sb.append('.');
            int length = sb.length();
            for (int i = 0; i < kw.this.g; i++) {
                sb.append(i);
                this.c[i] = new File(kw.this.a, sb.toString());
                sb.append(".tmp");
                this.d[i] = new File(kw.this.a, sb.toString());
                sb.setLength(length);
            }
        }

        @DexIgnore
        public final void b(String[] strArr) throws IOException {
            if (strArr.length == kw.this.g) {
                int i = 0;
                while (i < strArr.length) {
                    try {
                        this.b[i] = Long.parseLong(strArr[i]);
                        i++;
                    } catch (NumberFormatException unused) {
                        a(strArr);
                        throw null;
                    }
                }
                return;
            }
            a(strArr);
            throw null;
        }

        @DexIgnore
        public String a() throws IOException {
            StringBuilder sb = new StringBuilder();
            long[] jArr = this.b;
            for (long j : jArr) {
                sb.append(' ');
                sb.append(j);
            }
            return sb.toString();
        }

        @DexIgnore
        public File b(int i) {
            return this.d[i];
        }

        @DexIgnore
        public final IOException a(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        @DexIgnore
        public File a(int i) {
            return this.c[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e {
        @DexIgnore
        public /* final */ File[] a;

        @DexIgnore
        public /* synthetic */ e(kw kwVar, String str, long j, File[] fileArr, long[] jArr, a aVar) {
            this(str, j, fileArr, jArr);
        }

        @DexIgnore
        public File a(int i) {
            return this.a[i];
        }

        @DexIgnore
        public e(String str, long j, File[] fileArr, long[] jArr) {
            this.a = fileArr;
        }
    }

    @DexIgnore
    public kw(File file, int i2, int i3, long j2) {
        this.a = file;
        this.e = i2;
        this.b = new File(file, "journal");
        this.c = new File(file, "journal.tmp");
        this.d = new File(file, "journal.bkp");
        this.g = i3;
        this.f = j2;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() throws IOException {
        if (this.i != null) {
            Iterator it = new ArrayList(this.j.values()).iterator();
            while (it.hasNext()) {
                d dVar = (d) it.next();
                if (dVar.f != null) {
                    dVar.f.a();
                }
            }
            l();
            a(this.i);
            this.i = null;
        }
    }

    @DexIgnore
    public final void g() throws IOException {
        lw lwVar = new lw(new FileInputStream(this.b), mw.a);
        try {
            String c2 = lwVar.c();
            String c3 = lwVar.c();
            String c4 = lwVar.c();
            String c5 = lwVar.c();
            String c6 = lwVar.c();
            if (!"libcore.io.DiskLruCache".equals(c2) || !"1".equals(c3) || !Integer.toString(this.e).equals(c4) || !Integer.toString(this.g).equals(c5) || !"".equals(c6)) {
                throw new IOException("unexpected journal header: [" + c2 + ", " + c3 + ", " + c5 + ", " + c6 + "]");
            }
            int i2 = 0;
            while (true) {
                try {
                    d(lwVar.c());
                    i2++;
                } catch (EOFException unused) {
                    this.p = i2 - this.j.size();
                    if (lwVar.b()) {
                        k();
                    } else {
                        this.i = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.b, true), mw.a));
                    }
                    return;
                }
            }
        } finally {
            mw.a(lwVar);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final synchronized void k() throws IOException {
        if (this.i != null) {
            a(this.i);
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.c), mw.a));
        try {
            bufferedWriter.write("libcore.io.DiskLruCache");
            bufferedWriter.write("\n");
            bufferedWriter.write("1");
            bufferedWriter.write("\n");
            bufferedWriter.write(Integer.toString(this.e));
            bufferedWriter.write("\n");
            bufferedWriter.write(Integer.toString(this.g));
            bufferedWriter.write("\n");
            bufferedWriter.write("\n");
            for (d dVar : this.j.values()) {
                if (dVar.f != null) {
                    bufferedWriter.write("DIRTY " + dVar.a + '\n');
                } else {
                    bufferedWriter.write("CLEAN " + dVar.a + dVar.a() + '\n');
                }
            }
            a(bufferedWriter);
            if (this.b.exists()) {
                a(this.b, this.d, true);
            }
            a(this.c, this.b, false);
            this.d.delete();
            this.i = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.b, true), mw.a));
        } catch (Throwable th) {
            a(bufferedWriter);
            throw th;
        }
    }

    @DexIgnore
    public final void l() throws IOException {
        while (this.h > this.f) {
            e(this.j.entrySet().iterator().next().getKey());
        }
    }

    @DexIgnore
    public c b(String str) throws IOException {
        return a(str, -1);
    }

    @DexIgnore
    public synchronized e c(String str) throws IOException {
        a();
        d dVar = this.j.get(str);
        if (dVar == null) {
            return null;
        }
        if (!dVar.e) {
            return null;
        }
        for (File file : dVar.c) {
            if (!file.exists()) {
                return null;
            }
        }
        this.p++;
        this.i.append((CharSequence) "READ");
        this.i.append(' ');
        this.i.append((CharSequence) str);
        this.i.append('\n');
        if (c()) {
            this.r.submit(this.s);
        }
        return new e(this, str, dVar.g, dVar.c, dVar.b, null);
    }

    @DexIgnore
    public final void d(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i2 = indexOf + 1;
            int indexOf2 = str.indexOf(32, i2);
            if (indexOf2 == -1) {
                str2 = str.substring(i2);
                if (indexOf == 6 && str.startsWith("REMOVE")) {
                    this.j.remove(str2);
                    return;
                }
            } else {
                str2 = str.substring(i2, indexOf2);
            }
            d dVar = this.j.get(str2);
            if (dVar == null) {
                dVar = new d(this, str2, null);
                this.j.put(str2, dVar);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(" ");
                boolean unused = dVar.e = true;
                c unused2 = dVar.f = (c) null;
                dVar.b(split);
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                c unused3 = dVar.f = new c(this, dVar, null);
            } else if (indexOf2 != -1 || indexOf != 4 || !str.startsWith("READ")) {
                throw new IOException("unexpected journal line: " + str);
            }
        } else {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    @DexIgnore
    public final void e() throws IOException {
        a(this.c);
        Iterator<d> it = this.j.values().iterator();
        while (it.hasNext()) {
            d next = it.next();
            int i2 = 0;
            if (next.f == null) {
                while (i2 < this.g) {
                    this.h += next.b[i2];
                    i2++;
                }
            } else {
                c unused = next.f = (c) null;
                while (i2 < this.g) {
                    a(next.a(i2));
                    a(next.b(i2));
                    i2++;
                }
                it.remove();
            }
        }
    }

    @DexIgnore
    public void b() throws IOException {
        close();
        mw.a(this.a);
    }

    @DexIgnore
    public static kw a(File file, int i2, int i3, long j2) throws IOException {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 > 0) {
            File file2 = new File(file, "journal.bkp");
            if (file2.exists()) {
                File file3 = new File(file, "journal");
                if (file3.exists()) {
                    file2.delete();
                } else {
                    a(file2, file3, false);
                }
            }
            kw kwVar = new kw(file, i2, i3, j2);
            if (kwVar.b.exists()) {
                try {
                    kwVar.g();
                    kwVar.e();
                    return kwVar;
                } catch (IOException e2) {
                    PrintStream printStream = System.out;
                    printStream.println("DiskLruCache " + file + " is corrupt: " + e2.getMessage() + ", removing");
                    kwVar.b();
                }
            }
            file.mkdirs();
            kw kwVar2 = new kw(file, i2, i3, j2);
            kwVar2.k();
            return kwVar2;
        } else {
            throw new IllegalArgumentException("valueCount <= 0");
        }
    }

    @DexIgnore
    @TargetApi(26)
    public static void b(Writer writer) throws IOException {
        if (Build.VERSION.SDK_INT < 26) {
            writer.flush();
            return;
        }
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitUnbufferedIo().build());
        try {
            writer.flush();
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    @DexIgnore
    public synchronized boolean e(String str) throws IOException {
        a();
        d dVar = this.j.get(str);
        if (dVar != null) {
            if (dVar.f == null) {
                for (int i2 = 0; i2 < this.g; i2++) {
                    File a2 = dVar.a(i2);
                    if (a2.exists()) {
                        if (!a2.delete()) {
                            throw new IOException("failed to delete " + a2);
                        }
                    }
                    this.h -= dVar.b[i2];
                    dVar.b[i2] = 0;
                }
                this.p++;
                this.i.append((CharSequence) "REMOVE");
                this.i.append(' ');
                this.i.append((CharSequence) str);
                this.i.append('\n');
                this.j.remove(str);
                if (c()) {
                    this.r.submit(this.s);
                }
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean c() {
        int i2 = this.p;
        return i2 >= 2000 && i2 >= this.j.size();
    }

    @DexIgnore
    public static void a(File file) throws IOException {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    @DexIgnore
    public static void a(File file, File file2, boolean z) throws IOException {
        if (z) {
            a(file2);
        }
        if (!file.renameTo(file2)) {
            throw new IOException();
        }
    }

    @DexIgnore
    public final synchronized c a(String str, long j2) throws IOException {
        a();
        d dVar = this.j.get(str);
        if (j2 != -1 && (dVar == null || dVar.g != j2)) {
            return null;
        }
        if (dVar == null) {
            dVar = new d(this, str, null);
            this.j.put(str, dVar);
        } else if (dVar.f != null) {
            return null;
        }
        c cVar = new c(this, dVar, null);
        c unused = dVar.f = cVar;
        this.i.append((CharSequence) "DIRTY");
        this.i.append(' ');
        this.i.append((CharSequence) str);
        this.i.append('\n');
        b(this.i);
        return cVar;
    }

    @DexIgnore
    public final synchronized void a(c cVar, boolean z) throws IOException {
        d a2 = cVar.a;
        if (a2.f == cVar) {
            if (z && !a2.e) {
                int i2 = 0;
                while (i2 < this.g) {
                    if (!cVar.b[i2]) {
                        cVar.a();
                        throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                    } else if (!a2.b(i2).exists()) {
                        cVar.a();
                        return;
                    } else {
                        i2++;
                    }
                }
            }
            for (int i3 = 0; i3 < this.g; i3++) {
                File b2 = a2.b(i3);
                if (!z) {
                    a(b2);
                } else if (b2.exists()) {
                    File a3 = a2.a(i3);
                    b2.renameTo(a3);
                    long j2 = a2.b[i3];
                    long length = a3.length();
                    a2.b[i3] = length;
                    this.h = (this.h - j2) + length;
                }
            }
            this.p++;
            c unused = a2.f = (c) null;
            if (a2.e || z) {
                boolean unused2 = a2.e = true;
                this.i.append((CharSequence) "CLEAN");
                this.i.append(' ');
                this.i.append((CharSequence) a2.a);
                this.i.append((CharSequence) a2.a());
                this.i.append('\n');
                if (z) {
                    long j3 = this.q;
                    this.q = 1 + j3;
                    long unused3 = a2.g = j3;
                }
            } else {
                this.j.remove(a2.a);
                this.i.append((CharSequence) "REMOVE");
                this.i.append(' ');
                this.i.append((CharSequence) a2.a);
                this.i.append('\n');
            }
            b(this.i);
            if (this.h > this.f || c()) {
                this.r.submit(this.s);
            }
            return;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public final void a() {
        if (this.i == null) {
            throw new IllegalStateException("cache is closed");
        }
    }

    @DexIgnore
    @TargetApi(26)
    public static void a(Writer writer) throws IOException {
        if (Build.VERSION.SDK_INT < 26) {
            writer.close();
            return;
        }
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitUnbufferedIo().build());
        try {
            writer.close();
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }
}
