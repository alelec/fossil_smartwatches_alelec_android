package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum i70 {
    HOUR((byte) 1),
    MINUTE((byte) 2),
    SUB_EYE((byte) 3);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public i70(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
