package com.fossil;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.fossil.i0;
import java.util.ArrayList;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hx7 {
    @DexIgnore
    public Activity a;
    @DexIgnore
    public List<String> b; // = new ArrayList();
    @DexIgnore
    public List<String> c; // = new ArrayList();
    @DexIgnore
    public List<String> d; // = new ArrayList();
    @DexIgnore
    public int e;
    @DexIgnore
    public gx7 f;
    @DexIgnore
    public List<String> g;
    @DexIgnore
    public List<String> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements DialogInterface.OnClickListener {
        @DexIgnore
        public a(hx7 hx7) {
        }

        @DexIgnore
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements DialogInterface.OnClickListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onClick(DialogInterface dialogInterface, int i) {
            hx7 hx7 = hx7.this;
            hx7.a((Context) hx7.a);
            dialogInterface.dismiss();
        }
    }

    @DexIgnore
    public final void b() {
        List<String> list = this.c;
        if (list != null) {
            list.clear();
        }
        List<String> list2 = this.h;
        if (list2 != null) {
            list2.clear();
        }
        List<String> list3 = this.b;
        if (list3 != null) {
            list3.clear();
        }
        List<String> list4 = this.g;
        if (list4 != null) {
            list4.clear();
        }
    }

    @DexIgnore
    public final void c() {
        i0.a aVar = new i0.a(this.a);
        aVar.b(a());
        aVar.b("\u53bb\u8bbe\u7f6e", new b());
        aVar.a("\u53d6\u6d88", new a(this));
        aVar.a().show();
    }

    @DexIgnore
    public hx7 a(Activity activity) {
        this.a = activity;
        return this;
    }

    @DexIgnore
    public hx7 a(Activity activity, int i, List<String> list) {
        a(activity, i, null, (String[]) list.toArray(new String[0]));
        return this;
    }

    @DexIgnore
    @TargetApi(23)
    public final hx7 a(Activity activity, int i, String[] strArr, String... strArr2) {
        if (this.a != null) {
            this.e = i;
            if (!a(strArr, strArr2)) {
                Activity activity2 = this.a;
                List<String> list = this.b;
                g6.a(activity2, (String[]) list.toArray(new String[list.size()]), i);
                for (int i2 = 0; i2 < this.b.size(); i2++) {
                    mx7.a("\u9700\u8981\u7533\u8bf7\u7684\u6743\u9650\u5217\u8868" + this.b.get(i2));
                }
            } else {
                gx7 gx7 = this.f;
                if (gx7 != null) {
                    gx7.a();
                }
            }
            return this;
        }
        throw new NullPointerException("\u83b7\u53d6\u6743\u9650\u7684Activity\u4e0d\u5b58\u5728");
    }

    @DexIgnore
    public final boolean a(String[] strArr, String... strArr2) {
        if (Build.VERSION.SDK_INT < 23) {
            return true;
        }
        b();
        if (strArr != null) {
            if (strArr.length != strArr2.length) {
                throw new IndexOutOfBoundsException("\u4f20\u5165\u7684\u63d0\u793a\u6570\u7ec4\u548c\u9700\u8981\u7533\u8bf7\u7684\u6743\u9650\u6570\u7ec4\u957f\u5ea6\u4e0d\u4e00\u81f4");
            } else if (this.g == null) {
                this.g = new ArrayList();
            }
        }
        for (int i = 0; i < strArr2.length; i++) {
            if (this.a.checkSelfPermission(strArr2[i]) == -1) {
                this.b.add(strArr2[i]);
                if (strArr != null) {
                    this.g.add(strArr[i]);
                }
            }
        }
        return this.b.isEmpty();
    }

    @DexIgnore
    public hx7 a(int i, String[] strArr, int[] iArr) {
        List<String> list;
        if (i == this.e) {
            for (int i2 = 0; i2 < strArr.length; i2++) {
                mx7.a("\u8fd4\u56de\u6743\u9650\u5217\u8868" + strArr[i2]);
                if (iArr[i2] == -1) {
                    this.c.add(strArr[i2]);
                    if (this.g != null && this.h == null) {
                        this.h = new ArrayList();
                    }
                    if (!(this.h == null || (list = this.g) == null || list.size() <= 0)) {
                        this.h.add(this.g.get(i2));
                    }
                } else if (iArr[i2] == 0) {
                    this.d.add(strArr[i2]);
                }
            }
            if (!this.c.isEmpty()) {
                List<String> list2 = this.g;
                if (list2 != null && list2.size() > 0) {
                    c();
                }
                this.f.a(this.c, this.d);
            } else {
                this.f.a();
            }
        }
        return this;
    }

    @DexIgnore
    public final CharSequence a() {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        int i = 0;
        for (int i2 = 0; i2 < this.c.size(); i2++) {
            String str = this.c.get(i2).split("\\.")[2];
            spannableStringBuilder.append((CharSequence) str);
            spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.parseColor("#37ADA4")), i, str.length() + i, 33);
            spannableStringBuilder.append((CharSequence) "\uff1a");
            spannableStringBuilder.append((CharSequence) this.h.get(i2));
            i = i + str.length() + 2 + this.h.get(i2).length();
            if (i2 != this.c.size() - 1) {
                spannableStringBuilder.append((CharSequence) "\n");
            }
        }
        return spannableStringBuilder;
    }

    @DexIgnore
    public hx7 a(gx7 gx7) {
        this.f = gx7;
        return this;
    }

    @DexIgnore
    public void a(Context context) {
        Intent intent = new Intent();
        intent.addFlags(SQLiteDatabase.CREATE_IF_NECESSARY);
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, context.getPackageName(), null));
        context.startActivity(intent);
    }
}
