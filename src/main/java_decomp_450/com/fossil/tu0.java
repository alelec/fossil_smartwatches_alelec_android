package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class tu0 extends Enum<tu0> implements mw0 {
    @DexIgnore
    public static /* final */ tu0 c;
    @DexIgnore
    public static /* final */ tu0 d;
    @DexIgnore
    public static /* final */ tu0 e;
    @DexIgnore
    public static /* final */ tu0 f;
    @DexIgnore
    public static /* final */ tu0 g;
    @DexIgnore
    public static /* final */ tu0 h;
    @DexIgnore
    public static /* final */ /* synthetic */ tu0[] i;
    @DexIgnore
    public static /* final */ ys0 j; // = new ys0(null);
    @DexIgnore
    public /* final */ String a; // = yz0.a(this);
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        tu0 tu0 = new tu0("SUCCESS", 0, (byte) 0);
        c = tu0;
        tu0 tu02 = new tu0("INVALID_OPERATION", 1, (byte) 1);
        d = tu02;
        tu0 tu03 = new tu0("INVALID_FILE_HANDLE", 2, (byte) 2);
        e = tu03;
        tu0 tu04 = new tu0("OPERATION_IN_PROGRESS", 4, (byte) 4);
        f = tu04;
        tu0 tu05 = new tu0("VERIFICATION_FAIL", 5, (byte) 5);
        g = tu05;
        tu0 tu06 = new tu0("UNKNOWN", 6, (byte) 255);
        h = tu06;
        i = new tu0[]{tu0, tu02, tu03, new tu0("INVALID_OPERATION_DATA", 3, (byte) 3), tu04, tu05, tu06};
    }
    */

    @DexIgnore
    public tu0(String str, int i2, byte b2) {
        this.b = b2;
    }

    @DexIgnore
    public static tu0 valueOf(String str) {
        return (tu0) Enum.valueOf(tu0.class, str);
    }

    @DexIgnore
    public static tu0[] values() {
        return (tu0[]) i.clone();
    }

    @DexIgnore
    @Override // com.fossil.mw0
    public boolean a() {
        return this == c;
    }

    @DexIgnore
    @Override // com.fossil.mw0
    public String getLogName() {
        return this.a;
    }
}
