package com.fossil;

import android.util.Log;
import com.fossil.ix;
import com.fossil.m00;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c00 implements m00<File, ByteBuffer> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ix<ByteBuffer> {
        @DexIgnore
        public /* final */ File a;

        @DexIgnore
        public a(File file) {
            this.a = file;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a(ew ewVar, ix.a<? super ByteBuffer> aVar) {
            try {
                aVar.a(l50.a(this.a));
            } catch (IOException e) {
                if (Log.isLoggable("ByteBufferFileLoader", 3)) {
                    Log.d("ByteBufferFileLoader", "Failed to obtain ByteBuffer for file", e);
                }
                aVar.a((Exception) e);
            }
        }

        @DexIgnore
        @Override // com.fossil.ix
        public sw b() {
            return sw.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.ix
        public Class<ByteBuffer> getDataClass() {
            return ByteBuffer.class;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements n00<File, ByteBuffer> {
        @DexIgnore
        @Override // com.fossil.n00
        public m00<File, ByteBuffer> a(q00 q00) {
            return new c00();
        }
    }

    @DexIgnore
    public boolean a(File file) {
        return true;
    }

    @DexIgnore
    public m00.a<ByteBuffer> a(File file, int i, int i2, ax axVar) {
        return new m00.a<>(new k50(file), new a(file));
    }
}
