package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class si5 implements Factory<ri5> {
    @DexIgnore
    public /* final */ Provider<mk5> a;
    @DexIgnore
    public /* final */ Provider<ch5> b;

    @DexIgnore
    public si5(Provider<mk5> provider, Provider<ch5> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static si5 a(Provider<mk5> provider, Provider<ch5> provider2) {
        return new si5(provider, provider2);
    }

    @DexIgnore
    public static ri5 a() {
        return new ri5();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ri5 get() {
        ri5 a2 = a();
        ti5.a(a2, this.a.get());
        ti5.a(a2, this.b.get());
        return a2;
    }
}
