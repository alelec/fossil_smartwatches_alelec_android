package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c96 implements Factory<k96> {
    @DexIgnore
    public static k96 a(z86 z86) {
        k96 c = z86.c();
        c87.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
