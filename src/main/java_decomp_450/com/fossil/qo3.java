package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qo3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements c {
        @DexIgnore
        public /* final */ CountDownLatch a;

        @DexIgnore
        public a() {
            this.a = new CountDownLatch(1);
        }

        @DexIgnore
        public final void a() throws InterruptedException {
            this.a.await();
        }

        @DexIgnore
        @Override // com.fossil.go3
        public final void onCanceled() {
            this.a.countDown();
        }

        @DexIgnore
        @Override // com.fossil.io3
        public final void onFailure(Exception exc) {
            this.a.countDown();
        }

        @DexIgnore
        @Override // com.fossil.jo3
        public final void onSuccess(Object obj) {
            this.a.countDown();
        }

        @DexIgnore
        public final boolean a(long j, TimeUnit timeUnit) throws InterruptedException {
            return this.a.await(j, timeUnit);
        }

        @DexIgnore
        public /* synthetic */ a(pp3 pp3) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements c {
        @DexIgnore
        public /* final */ Object a; // = new Object();
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ lp3<Void> c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public Exception g;
        @DexIgnore
        public boolean h;

        @DexIgnore
        public b(int i, lp3<Void> lp3) {
            this.b = i;
            this.c = lp3;
        }

        @DexIgnore
        public final void a() {
            if (this.d + this.e + this.f != this.b) {
                return;
            }
            if (this.g != null) {
                lp3<Void> lp3 = this.c;
                int i = this.e;
                int i2 = this.b;
                StringBuilder sb = new StringBuilder(54);
                sb.append(i);
                sb.append(" out of ");
                sb.append(i2);
                sb.append(" underlying tasks failed");
                lp3.a(new ExecutionException(sb.toString(), this.g));
            } else if (this.h) {
                this.c.f();
            } else {
                this.c.a((Void) null);
            }
        }

        @DexIgnore
        @Override // com.fossil.go3
        public final void onCanceled() {
            synchronized (this.a) {
                this.f++;
                this.h = true;
                a();
            }
        }

        @DexIgnore
        @Override // com.fossil.io3
        public final void onFailure(Exception exc) {
            synchronized (this.a) {
                this.e++;
                this.g = exc;
                a();
            }
        }

        @DexIgnore
        @Override // com.fossil.jo3
        public final void onSuccess(Object obj) {
            synchronized (this.a) {
                this.d++;
                a();
            }
        }
    }

    @DexIgnore
    public interface c extends go3, io3, jo3<Object> {
    }

    @DexIgnore
    public static <TResult> no3<TResult> a(TResult tresult) {
        lp3 lp3 = new lp3();
        lp3.a((Object) tresult);
        return lp3;
    }

    @DexIgnore
    public static <TResult> TResult b(no3<TResult> no3) throws ExecutionException {
        if (no3.e()) {
            return no3.b();
        }
        if (no3.c()) {
            throw new CancellationException("Task is already canceled");
        }
        throw new ExecutionException(no3.a());
    }

    @DexIgnore
    public static <TResult> no3<TResult> a(Exception exc) {
        lp3 lp3 = new lp3();
        lp3.a(exc);
        return lp3;
    }

    @DexIgnore
    public static <TResult> no3<TResult> a() {
        lp3 lp3 = new lp3();
        lp3.f();
        return lp3;
    }

    @DexIgnore
    public static <TResult> no3<TResult> a(Executor executor, Callable<TResult> callable) {
        a72.a(executor, "Executor must not be null");
        a72.a(callable, "Callback must not be null");
        lp3 lp3 = new lp3();
        executor.execute(new pp3(lp3, callable));
        return lp3;
    }

    @DexIgnore
    public static <TResult> TResult a(no3<TResult> no3) throws ExecutionException, InterruptedException {
        a72.a();
        a72.a(no3, "Task must not be null");
        if (no3.d()) {
            return (TResult) b(no3);
        }
        a aVar = new a(null);
        a((no3<?>) no3, (c) aVar);
        aVar.a();
        return (TResult) b(no3);
    }

    @DexIgnore
    public static <TResult> TResult a(no3<TResult> no3, long j, TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
        a72.a();
        a72.a(no3, "Task must not be null");
        a72.a(timeUnit, "TimeUnit must not be null");
        if (no3.d()) {
            return (TResult) b(no3);
        }
        a aVar = new a(null);
        a((no3<?>) no3, (c) aVar);
        if (aVar.a(j, timeUnit)) {
            return (TResult) b(no3);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }

    @DexIgnore
    public static no3<Void> a(Collection<? extends no3<?>> collection) {
        if (collection == null || collection.isEmpty()) {
            return a((Object) null);
        }
        Iterator<? extends no3<?>> it = collection.iterator();
        while (it.hasNext()) {
            if (((no3) it.next()) == null) {
                throw new NullPointerException("null tasks are not accepted");
            }
        }
        lp3 lp3 = new lp3();
        b bVar = new b(collection.size(), lp3);
        Iterator<? extends no3<?>> it2 = collection.iterator();
        while (it2.hasNext()) {
            a((no3) it2.next(), bVar);
        }
        return lp3;
    }

    @DexIgnore
    public static no3<Void> a(no3<?>... no3Arr) {
        if (no3Arr == null || no3Arr.length == 0) {
            return a((Object) null);
        }
        return a((Collection<? extends no3<?>>) Arrays.asList(no3Arr));
    }

    @DexIgnore
    public static void a(no3<?> no3, c cVar) {
        no3.a(po3.b, (jo3<? super Object>) cVar);
        no3.a(po3.b, (io3) cVar);
        no3.a(po3.b, (go3) cVar);
    }
}
