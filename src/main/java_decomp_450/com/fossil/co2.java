package com.fossil;

import android.os.RemoteException;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class co2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ sn2 f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public co2(sn2 sn2, String str) {
        super(sn2);
        this.f = sn2;
        this.e = str;
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void a() throws RemoteException {
        this.f.h.beginAdUnitExposure(this.e, ((sn2.a) this).b);
    }
}
