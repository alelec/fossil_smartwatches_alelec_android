package com.fossil;

import com.fossil.eq;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gq {
    @DexIgnore
    public eq<?> a;

    @DexIgnore
    public gq(eq<?> eqVar) {
        this.a = eqVar;
    }

    @DexIgnore
    public void a() {
        this.a = null;
    }

    @DexIgnore
    public void finalize() throws Throwable {
        eq.g j;
        try {
            eq<?> eqVar = this.a;
            if (!(eqVar == null || (j = eq.j()) == null)) {
                j.a(eqVar, new hq(eqVar.a()));
            }
        } finally {
            super.finalize();
        }
    }
}
