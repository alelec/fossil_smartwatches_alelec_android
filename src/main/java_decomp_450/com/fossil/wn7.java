package com.fossil;

import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wn7 {
    @DexIgnore
    public static /* final */ tn7[] e; // = {tn7.q, tn7.r, tn7.s, tn7.t, tn7.u, tn7.k, tn7.m, tn7.l, tn7.n, tn7.p, tn7.o};
    @DexIgnore
    public static /* final */ tn7[] f; // = {tn7.q, tn7.r, tn7.s, tn7.t, tn7.u, tn7.k, tn7.m, tn7.l, tn7.n, tn7.p, tn7.o, tn7.i, tn7.j, tn7.g, tn7.h, tn7.e, tn7.f, tn7.d};
    @DexIgnore
    public static /* final */ wn7 g;
    @DexIgnore
    public static /* final */ wn7 h; // = new a(false).a();
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ String[] c;
    @DexIgnore
    public /* final */ String[] d;

    /*
    static {
        a aVar = new a(true);
        aVar.a(e);
        aVar.a(oo7.TLS_1_3, oo7.TLS_1_2);
        aVar.a(true);
        aVar.a();
        a aVar2 = new a(true);
        aVar2.a(f);
        aVar2.a(oo7.TLS_1_3, oo7.TLS_1_2, oo7.TLS_1_1, oo7.TLS_1_0);
        aVar2.a(true);
        g = aVar2.a();
        a aVar3 = new a(true);
        aVar3.a(f);
        aVar3.a(oo7.TLS_1_0);
        aVar3.a(true);
        aVar3.a();
    }
    */

    @DexIgnore
    public wn7(a aVar) {
        this.a = aVar.a;
        this.c = aVar.b;
        this.d = aVar.c;
        this.b = aVar.d;
    }

    @DexIgnore
    public List<tn7> a() {
        String[] strArr = this.c;
        if (strArr != null) {
            return tn7.a(strArr);
        }
        return null;
    }

    @DexIgnore
    public boolean b() {
        return this.a;
    }

    @DexIgnore
    public boolean c() {
        return this.b;
    }

    @DexIgnore
    public List<oo7> d() {
        String[] strArr = this.d;
        if (strArr != null) {
            return oo7.forJavaNames(strArr);
        }
        return null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof wn7)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        wn7 wn7 = (wn7) obj;
        boolean z = this.a;
        if (z != wn7.a) {
            return false;
        }
        return !z || (Arrays.equals(this.c, wn7.c) && Arrays.equals(this.d, wn7.d) && this.b == wn7.b);
    }

    @DexIgnore
    public int hashCode() {
        if (this.a) {
            return ((((527 + Arrays.hashCode(this.c)) * 31) + Arrays.hashCode(this.d)) * 31) + (!this.b);
        }
        return 17;
    }

    @DexIgnore
    public String toString() {
        if (!this.a) {
            return "ConnectionSpec()";
        }
        String str = "[all enabled]";
        String obj = this.c != null ? a().toString() : str;
        if (this.d != null) {
            str = d().toString();
        }
        return "ConnectionSpec(cipherSuites=" + obj + ", tlsVersions=" + str + ", supportsTlsExtensions=" + this.b + ")";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public String[] b;
        @DexIgnore
        public String[] c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public a(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public a a(tn7... tn7Arr) {
            if (this.a) {
                String[] strArr = new String[tn7Arr.length];
                for (int i = 0; i < tn7Arr.length; i++) {
                    strArr[i] = tn7Arr[i].a;
                }
                a(strArr);
                return this;
            }
            throw new IllegalStateException("no cipher suites for cleartext connections");
        }

        @DexIgnore
        public a b(String... strArr) {
            if (!this.a) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            } else if (strArr.length != 0) {
                this.c = (String[]) strArr.clone();
                return this;
            } else {
                throw new IllegalArgumentException("At least one TLS version is required");
            }
        }

        @DexIgnore
        public a(wn7 wn7) {
            this.a = wn7.a;
            this.b = wn7.c;
            this.c = wn7.d;
            this.d = wn7.b;
        }

        @DexIgnore
        public a a(String... strArr) {
            if (!this.a) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            } else if (strArr.length != 0) {
                this.b = (String[]) strArr.clone();
                return this;
            } else {
                throw new IllegalArgumentException("At least one cipher suite is required");
            }
        }

        @DexIgnore
        public a a(oo7... oo7Arr) {
            if (this.a) {
                String[] strArr = new String[oo7Arr.length];
                for (int i = 0; i < oo7Arr.length; i++) {
                    strArr[i] = oo7Arr[i].javaName;
                }
                b(strArr);
                return this;
            }
            throw new IllegalStateException("no TLS versions for cleartext connections");
        }

        @DexIgnore
        public a a(boolean z) {
            if (this.a) {
                this.d = z;
                return this;
            }
            throw new IllegalStateException("no TLS extensions for cleartext connections");
        }

        @DexIgnore
        public wn7 a() {
            return new wn7(this);
        }
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket, boolean z) {
        wn7 b2 = b(sSLSocket, z);
        String[] strArr = b2.d;
        if (strArr != null) {
            sSLSocket.setEnabledProtocols(strArr);
        }
        String[] strArr2 = b2.c;
        if (strArr2 != null) {
            sSLSocket.setEnabledCipherSuites(strArr2);
        }
    }

    @DexIgnore
    public final wn7 b(SSLSocket sSLSocket, boolean z) {
        String[] strArr;
        String[] strArr2;
        if (this.c != null) {
            strArr = ro7.a(tn7.b, sSLSocket.getEnabledCipherSuites(), this.c);
        } else {
            strArr = sSLSocket.getEnabledCipherSuites();
        }
        if (this.d != null) {
            strArr2 = ro7.a(ro7.p, sSLSocket.getEnabledProtocols(), this.d);
        } else {
            strArr2 = sSLSocket.getEnabledProtocols();
        }
        String[] supportedCipherSuites = sSLSocket.getSupportedCipherSuites();
        int a2 = ro7.a(tn7.b, supportedCipherSuites, "TLS_FALLBACK_SCSV");
        if (z && a2 != -1) {
            strArr = ro7.a(strArr, supportedCipherSuites[a2]);
        }
        a aVar = new a(this);
        aVar.a(strArr);
        aVar.b(strArr2);
        return aVar.a();
    }

    @DexIgnore
    public boolean a(SSLSocket sSLSocket) {
        if (!this.a) {
            return false;
        }
        String[] strArr = this.d;
        if (strArr != null && !ro7.b(ro7.p, strArr, sSLSocket.getEnabledProtocols())) {
            return false;
        }
        String[] strArr2 = this.c;
        if (strArr2 == null || ro7.b(tn7.b, strArr2, sSLSocket.getEnabledCipherSuites())) {
            return true;
        }
        return false;
    }
}
