package com.fossil;

import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y12<L> {
    @DexIgnore
    public /* final */ c a;
    @DexIgnore
    public volatile L b;
    @DexIgnore
    public volatile a<L> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<L> {
        @DexIgnore
        public /* final */ L a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public a(L l, String str) {
            this.a = l;
            this.b = str;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.b.equals(aVar.b);
        }

        @DexIgnore
        public final int hashCode() {
            return (System.identityHashCode(this.a) * 31) + this.b.hashCode();
        }
    }

    @DexIgnore
    public interface b<L> {
        @DexIgnore
        void a();

        @DexIgnore
        void a(L l);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends bg2 {
        @DexIgnore
        public c(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public final void handleMessage(Message message) {
            boolean z = true;
            if (message.what != 1) {
                z = false;
            }
            a72.a(z);
            y12.this.b((b) message.obj);
        }
    }

    @DexIgnore
    public y12(Looper looper, L l, String str) {
        this.a = new c(looper);
        a72.a((Object) l, (Object) "Listener must not be null");
        this.b = l;
        a72.b(str);
        this.c = new a<>(l, str);
    }

    @DexIgnore
    public final void a(b<? super L> bVar) {
        a72.a(bVar, "Notifier must not be null");
        this.a.sendMessage(this.a.obtainMessage(1, bVar));
    }

    @DexIgnore
    public final a<L> b() {
        return this.c;
    }

    @DexIgnore
    public final void b(b<? super L> bVar) {
        L l = this.b;
        if (l == null) {
            bVar.a();
            return;
        }
        try {
            bVar.a(l);
        } catch (RuntimeException e) {
            bVar.a();
            throw e;
        }
    }

    @DexIgnore
    public final void a() {
        this.b = null;
        this.c = null;
    }
}
