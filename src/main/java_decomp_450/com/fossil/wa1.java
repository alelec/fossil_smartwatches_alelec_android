package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wa1 extends b91 {
    @DexIgnore
    public byte[] Q;
    @DexIgnore
    public short R;
    @DexIgnore
    public long S;
    @DexIgnore
    public /* final */ int T;
    @DexIgnore
    public /* final */ int U;
    @DexIgnore
    public float V;
    @DexIgnore
    public /* final */ boolean W;
    @DexIgnore
    public /* final */ byte[] X;
    @DexIgnore
    public byte[] Y;
    @DexIgnore
    public /* final */ long Z;
    @DexIgnore
    public /* final */ float a0;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ wa1(short r7, long r8, com.fossil.ri1 r10, int r11, float r12, int r13) {
        /*
            r6 = this;
            r0 = r13 & 8
            if (r0 == 0) goto L_0x0007
            r11 = 3
            r5 = 3
            goto L_0x0008
        L_0x0007:
            r5 = r11
        L_0x0008:
            r11 = r13 & 16
            if (r11 == 0) goto L_0x000f
            r12 = 981668463(0x3a83126f, float:0.001)
        L_0x000f:
            com.fossil.g51 r1 = com.fossil.g51.LEGACY_GET_FILE
            com.fossil.qa1 r3 = com.fossil.qa1.V
            r0 = r6
            r2 = r7
            r4 = r10
            r0.<init>(r1, r2, r3, r4, r5)
            r6.Z = r8
            r6.a0 = r12
            r8 = 0
            byte[] r8 = new byte[r8]
            r6.Q = r8
            r8 = 65535(0xffff, float:9.1834E-41)
            r6.U = r8
            r8 = 11
            java.nio.ByteBuffer r8 = java.nio.ByteBuffer.allocate(r8)
            java.nio.ByteOrder r9 = java.nio.ByteOrder.LITTLE_ENDIAN
            java.nio.ByteBuffer r8 = r8.order(r9)
            com.fossil.g51 r9 = com.fossil.g51.LEGACY_GET_FILE
            byte r9 = r9.a
            java.nio.ByteBuffer r8 = r8.put(r9)
            java.nio.ByteBuffer r7 = r8.putShort(r7)
            int r8 = r6.T
            java.nio.ByteBuffer r7 = r7.putInt(r8)
            int r8 = r6.U
            java.nio.ByteBuffer r7 = r7.putInt(r8)
            byte[] r7 = r7.array()
            java.lang.String r8 = "ByteBuffer.allocate(11)\n\u2026gth)\n            .array()"
            com.fossil.ee7.a(r7, r8)
            r6.X = r7
            r7 = 1
            java.nio.ByteBuffer r7 = java.nio.ByteBuffer.allocate(r7)
            java.nio.ByteOrder r8 = java.nio.ByteOrder.LITTLE_ENDIAN
            java.nio.ByteBuffer r7 = r7.order(r8)
            com.fossil.g51 r8 = com.fossil.g51.LEGACY_GET_FILE
            byte r8 = r8.a()
            java.nio.ByteBuffer r7 = r7.put(r8)
            byte[] r7 = r7.array()
            java.lang.String r8 = "ByteBuffer.allocate(1)\n \u2026e())\n            .array()"
            com.fossil.ee7.a(r7, r8)
            r6.Y = r7
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wa1.<init>(short, long, com.fossil.ri1, int, float, int):void");
    }

    @DexIgnore
    @Override // com.fossil.e71
    public void b(byte[] bArr) {
        this.Y = bArr;
    }

    @DexIgnore
    @Override // com.fossil.b91
    public void c(byte[] bArr) {
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        this.R = order.getShort(0);
        this.S = yz0.b(order.getInt(bArr.length - 4));
        if (!(this.R == r() || this.S == ik1.a.a(bArr, 0, bArr.length - 4, ng1.CRC32))) {
            ((v81) this).v = sz0.a(((v81) this).v, null, null, ay0.k, null, null, 27);
        }
        this.Q = bArr;
    }

    @DexIgnore
    @Override // com.fossil.e71, com.fossil.v81
    public JSONObject g() {
        return yz0.a(yz0.a(yz0.a(super.g(), r51.g3, Long.valueOf(this.Z)), r51.a1, Integer.valueOf(this.T)), r51.b1, Integer.valueOf(this.U));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(yz0.a(super.h(), r51.I, Long.valueOf(ik1.a.a(this.Q, ng1.CRC32))), r51.b1, Integer.valueOf(this.Q.length));
    }

    @DexIgnore
    @Override // com.fossil.uh1, com.fossil.e71
    public byte[] n() {
        return this.X;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public boolean p() {
        return this.W;
    }

    @DexIgnore
    @Override // com.fossil.uh1, com.fossil.e71
    public byte[] q() {
        return this.Y;
    }

    @DexIgnore
    @Override // com.fossil.b91
    public void s() {
        float length = (((float) this.Q.length) * 1.0f) / ((float) this.Z);
        if (length - this.V > this.a0 || length == 1.0f) {
            this.V = length;
            a(length);
        }
    }
}
