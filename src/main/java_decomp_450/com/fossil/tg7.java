package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tg7 implements hg7<lf7> {
    @DexIgnore
    public /* final */ CharSequence a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ kd7<CharSequence, Integer, r87<Integer, Integer>> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<lf7>, ye7 {
        @DexIgnore
        public int a; // = -1;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public lf7 d;
        @DexIgnore
        public int e;
        @DexIgnore
        public /* final */ /* synthetic */ tg7 f;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(tg7 tg7) {
            this.f = tg7;
            int a2 = qf7.a(tg7.b, 0, tg7.a.length());
            this.b = a2;
            this.c = a2;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0021, code lost:
            if (r0 < com.fossil.tg7.c(r6.f)) goto L_0x0023;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a() {
            /*
                r6 = this;
                int r0 = r6.c
                r1 = 0
                if (r0 >= 0) goto L_0x000c
                r6.a = r1
                r0 = 0
                r6.d = r0
                goto L_0x009e
            L_0x000c:
                com.fossil.tg7 r0 = r6.f
                int r0 = r0.c
                r2 = -1
                r3 = 1
                if (r0 <= 0) goto L_0x0023
                int r0 = r6.e
                int r0 = r0 + r3
                r6.e = r0
                com.fossil.tg7 r4 = r6.f
                int r4 = r4.c
                if (r0 >= r4) goto L_0x0031
            L_0x0023:
                int r0 = r6.c
                com.fossil.tg7 r4 = r6.f
                java.lang.CharSequence r4 = r4.a
                int r4 = r4.length()
                if (r0 <= r4) goto L_0x0047
            L_0x0031:
                int r0 = r6.b
                com.fossil.lf7 r1 = new com.fossil.lf7
                com.fossil.tg7 r4 = r6.f
                java.lang.CharSequence r4 = r4.a
                int r4 = com.fossil.nh7.c(r4)
                r1.<init>(r0, r4)
                r6.d = r1
                r6.c = r2
                goto L_0x009c
            L_0x0047:
                com.fossil.tg7 r0 = r6.f
                com.fossil.kd7 r0 = r0.d
                com.fossil.tg7 r4 = r6.f
                java.lang.CharSequence r4 = r4.a
                int r5 = r6.c
                java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
                java.lang.Object r0 = r0.invoke(r4, r5)
                com.fossil.r87 r0 = (com.fossil.r87) r0
                if (r0 != 0) goto L_0x0077
                int r0 = r6.b
                com.fossil.lf7 r1 = new com.fossil.lf7
                com.fossil.tg7 r4 = r6.f
                java.lang.CharSequence r4 = r4.a
                int r4 = com.fossil.nh7.c(r4)
                r1.<init>(r0, r4)
                r6.d = r1
                r6.c = r2
                goto L_0x009c
            L_0x0077:
                java.lang.Object r2 = r0.component1()
                java.lang.Number r2 = (java.lang.Number) r2
                int r2 = r2.intValue()
                java.lang.Object r0 = r0.component2()
                java.lang.Number r0 = (java.lang.Number) r0
                int r0 = r0.intValue()
                int r4 = r6.b
                com.fossil.lf7 r4 = com.fossil.qf7.d(r4, r2)
                r6.d = r4
                int r2 = r2 + r0
                r6.b = r2
                if (r0 != 0) goto L_0x0099
                r1 = 1
            L_0x0099:
                int r2 = r2 + r1
                r6.c = r2
            L_0x009c:
                r6.a = r3
            L_0x009e:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.tg7.a.a():void");
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.a == -1) {
                a();
            }
            return this.a == 1;
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @DexIgnore
        @Override // java.util.Iterator
        public lf7 next() {
            if (this.a == -1) {
                a();
            }
            if (this.a != 0) {
                lf7 lf7 = this.d;
                if (lf7 != null) {
                    this.d = null;
                    this.a = -1;
                    return lf7;
                }
                throw new x87("null cannot be cast to non-null type kotlin.ranges.IntRange");
            }
            throw new NoSuchElementException();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.kd7<? super java.lang.CharSequence, ? super java.lang.Integer, com.fossil.r87<java.lang.Integer, java.lang.Integer>> */
    /* JADX WARN: Multi-variable type inference failed */
    public tg7(CharSequence charSequence, int i, int i2, kd7<? super CharSequence, ? super Integer, r87<Integer, Integer>> kd7) {
        ee7.b(charSequence, "input");
        ee7.b(kd7, "getNextMatch");
        this.a = charSequence;
        this.b = i;
        this.c = i2;
        this.d = kd7;
    }

    @DexIgnore
    @Override // com.fossil.hg7
    public Iterator<lf7> iterator() {
        return new a(this);
    }
}
