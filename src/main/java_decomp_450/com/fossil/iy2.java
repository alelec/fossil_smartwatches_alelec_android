package com.fossil;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iy2 extends oy2 {
    @DexIgnore
    public /* final */ /* synthetic */ dy2 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public iy2(dy2 dy2) {
        super(dy2, null);
        this.b = dy2;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.oy2, java.lang.Iterable
    public final Iterator<Map.Entry<K, V>> iterator() {
        return new fy2(this.b, null);
    }

    @DexIgnore
    public /* synthetic */ iy2(dy2 dy2, gy2 gy2) {
        this(dy2);
    }
}
