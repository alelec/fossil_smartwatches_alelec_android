package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cr5 extends dy6 {
    @DexIgnore
    public static /* final */ String B;
    @DexIgnore
    public static /* final */ a C; // = new a(null);
    @DexIgnore
    public HashMap A;
    @DexIgnore
    public /* final */ pb j; // = new pm4(this);
    @DexIgnore
    public qw6<ew4> p;
    @DexIgnore
    public List<? extends Date> q; // = w97.a();
    @DexIgnore
    public Date r; // = vt4.a.a();
    @DexIgnore
    public String[] s; // = new String[0];
    @DexIgnore
    public Long[] t; // = new Long[0];
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public dr5 y;
    @DexIgnore
    public String z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return cr5.B;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ ew4 a;
        @DexIgnore
        public /* final */ /* synthetic */ cr5 b;

        @DexIgnore
        public b(ew4 ew4, cr5 cr5) {
            this.a = ew4;
            this.b = cr5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = cr5.C.a();
            local.e(a2, "dateStrPicker.onValueChanged: old: " + i + " - newVal: " + i2);
            if (i2 == 0) {
                cr5 cr5 = this.b;
                cr5.n(cr5.v);
                cr5 cr52 = this.b;
                cr52.o(cr52.u);
                NumberPicker numberPicker2 = this.a.u;
                ee7.a((Object) numberPicker2, "hourPicker");
                if (numberPicker2.getValue() < this.b.v) {
                    cr5 cr53 = this.b;
                    cr53.w = cr53.v;
                    NumberPicker numberPicker3 = this.a.u;
                    ee7.a((Object) numberPicker3, "hourPicker");
                    numberPicker3.setValue(this.b.w);
                }
                NumberPicker numberPicker4 = this.a.v;
                ee7.a((Object) numberPicker4, "minutePicker");
                if (numberPicker4.getValue() < this.b.u) {
                    cr5 cr54 = this.b;
                    cr54.x = cr54.u;
                    NumberPicker numberPicker5 = this.a.v;
                    ee7.a((Object) numberPicker5, "minutePicker");
                    numberPicker5.setValue(this.b.u);
                    return;
                }
                return;
            }
            this.b.n(0);
            NumberPicker numberPicker6 = this.a.u;
            ee7.a((Object) numberPicker6, "hourPicker");
            numberPicker6.setValue(this.b.w);
            this.b.o(0);
            NumberPicker numberPicker7 = this.a.v;
            ee7.a((Object) numberPicker7, "minutePicker");
            numberPicker7.setValue(this.b.x);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ ew4 a;
        @DexIgnore
        public /* final */ /* synthetic */ cr5 b;

        @DexIgnore
        public c(ew4 ew4, cr5 cr5) {
            this.a = ew4;
            this.b = cr5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = cr5.C.a();
            local.e(a2, "hourPicker.onValueChanged: old: " + i + " - newVal: " + i2);
            this.b.w = i2;
            if (i2 == this.b.v) {
                NumberPicker numberPicker2 = this.a.s;
                ee7.a((Object) numberPicker2, "dateStrPicker");
                if (numberPicker2.getValue() == 0) {
                    cr5 cr5 = this.b;
                    cr5.o(cr5.u);
                    NumberPicker numberPicker3 = this.a.v;
                    ee7.a((Object) numberPicker3, "minutePicker");
                    if (numberPicker3.getValue() < this.b.u) {
                        cr5 cr52 = this.b;
                        cr52.x = cr52.u;
                        NumberPicker numberPicker4 = this.a.v;
                        ee7.a((Object) numberPicker4, "minutePicker");
                        numberPicker4.setValue(this.b.u);
                        return;
                    }
                    return;
                }
            }
            this.b.o(0);
            NumberPicker numberPicker5 = this.a.v;
            ee7.a((Object) numberPicker5, "minutePicker");
            numberPicker5.setValue(this.b.x);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ cr5 a;

        @DexIgnore
        public d(cr5 cr5) {
            this.a = cr5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            this.a.x = i2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = cr5.C.a();
            local.e(a2, "minutePicker.onValueChanged: old: " + i + " - newVal: " + i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cr5 a;

        @DexIgnore
        public e(cr5 cr5) {
            this.a = cr5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Date a2 = this.a.c1();
            dr5 f = this.a.y;
            if (f != null) {
                f.a(a2);
            }
            this.a.dismiss();
        }
    }

    /*
    static {
        String simpleName = cr5.class.getSimpleName();
        ee7.a((Object) simpleName, "BottomDateTimeDialog::class.java.simpleName");
        B = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.dy6
    public void a1() {
        HashMap hashMap = this.A;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void b1() {
        if (!this.q.isEmpty()) {
            qw6<ew4> qw6 = this.p;
            if (qw6 != null) {
                ew4 a2 = qw6.a();
                if (a2 != null) {
                    NumberPicker numberPicker = a2.s;
                    ee7.a((Object) numberPicker, "dateStrPicker");
                    numberPicker.setMinValue(0);
                    NumberPicker numberPicker2 = a2.s;
                    ee7.a((Object) numberPicker2, "dateStrPicker");
                    numberPicker2.setMaxValue(this.s.length - 1);
                    a2.s.setDisplayedValues(this.s);
                    n(0);
                    o(0);
                    NumberPicker numberPicker3 = a2.s;
                    ee7.a((Object) numberPicker3, "dateStrPicker");
                    numberPicker3.setValue(1);
                    NumberPicker numberPicker4 = a2.u;
                    ee7.a((Object) numberPicker4, "hourPicker");
                    numberPicker4.setValue(this.v);
                    NumberPicker numberPicker5 = a2.v;
                    ee7.a((Object) numberPicker5, "minutePicker");
                    numberPicker5.setValue(this.u);
                    return;
                }
                return;
            }
            ee7.d("binding");
            throw null;
        }
    }

    @DexIgnore
    public final Date c1() {
        qw6<ew4> qw6 = this.p;
        if (qw6 != null) {
            ew4 a2 = qw6.a();
            if (a2 == null) {
                return new Date();
            }
            Long[] lArr = this.t;
            NumberPicker numberPicker = a2.s;
            ee7.a((Object) numberPicker, "dateStrPicker");
            long longValue = lArr[numberPicker.getValue()].longValue();
            NumberPicker numberPicker2 = a2.u;
            ee7.a((Object) numberPicker2, "hourPicker");
            NumberPicker numberPicker3 = a2.v;
            ee7.a((Object) numberPicker3, "minutePicker");
            return new Date(longValue + ((long) (numberPicker2.getValue() * 60 * 60 * 1000)) + ((long) (numberPicker3.getValue() * 60 * 1000)));
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public final void d1() {
        if (!this.q.isEmpty()) {
            r87<String[], Long[]> a2 = nt4.a(this.q);
            this.s = a2.getFirst();
            this.t = a2.getSecond();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = B;
            StringBuilder sb = new StringBuilder();
            sb.append("dateStrValues: ");
            String[] strArr = this.s;
            ArrayList arrayList = new ArrayList(strArr.length);
            for (String str2 : strArr) {
                arrayList.add(str2);
            }
            sb.append(arrayList);
            sb.append(" - dateStrMetadata: ");
            Long[] lArr = this.t;
            ArrayList arrayList2 = new ArrayList(lArr.length);
            for (Long l : lArr) {
                arrayList2.add(Long.valueOf(l.longValue()));
            }
            sb.append(arrayList2);
            local.e(str, sb.toString());
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "calendar");
            instance.setTime(this.r);
            this.v = instance.get(11);
            int i = instance.get(12);
            this.u = i;
            this.w = this.v;
            this.x = i;
        }
    }

    @DexIgnore
    public final void e1() {
        qw6<ew4> qw6 = this.p;
        if (qw6 != null) {
            ew4 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                ee7.a((Object) flexibleTextView, "ftvTitle");
                flexibleTextView.setText(this.z);
                a2.s.setOnValueChangedListener(new b(a2, this));
                a2.u.setOnValueChangedListener(new c(a2, this));
                a2.v.setOnValueChangedListener(new d(this));
                a2.q.setOnClickListener(new e(this));
                return;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public final void n(int i) {
        qw6<ew4> qw6 = this.p;
        if (qw6 != null) {
            ew4 a2 = qw6.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.u;
                ee7.a((Object) numberPicker, "hourPicker");
                numberPicker.setMinValue(i);
                NumberPicker numberPicker2 = a2.u;
                ee7.a((Object) numberPicker2, "hourPicker");
                numberPicker2.setMaxValue(23);
                return;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public final void o(int i) {
        qw6<ew4> qw6 = this.p;
        if (qw6 != null) {
            ew4 a2 = qw6.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.v;
                ee7.a((Object) numberPicker, "minutePicker");
                numberPicker.setMinValue(i);
                NumberPicker numberPicker2 = a2.v;
                ee7.a((Object) numberPicker2, "minutePicker");
                numberPicker2.setMaxValue(59);
                return;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        ew4 ew4 = (ew4) qb.a(layoutInflater, 2131558442, viewGroup, false, this.j);
        this.p = new qw6<>(this, ew4);
        ee7.a((Object) ew4, "binding");
        return ew4.d();
    }

    @DexIgnore
    @Override // com.fossil.dy6, androidx.fragment.app.Fragment, com.fossil.ac
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a1();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        e1();
        d1();
        b1();
    }

    @DexIgnore
    public final void setTitle(String str) {
        ee7.b(str, "title");
        this.z = str;
    }

    @DexIgnore
    public final void x(List<? extends Date> list) {
        ee7.b(list, "newData");
        this.q = list;
        if (!list.isEmpty()) {
            this.r = (Date) list.get(0);
        }
    }

    @DexIgnore
    public final void a(dr5 dr5) {
        ee7.b(dr5, "listener");
        this.y = dr5;
    }
}
