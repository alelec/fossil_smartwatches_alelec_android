package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.p1;
import com.fossil.v1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h3 implements p2 {
    @DexIgnore
    public Toolbar a;
    @DexIgnore
    public int b;
    @DexIgnore
    public View c;
    @DexIgnore
    public View d;
    @DexIgnore
    public Drawable e;
    @DexIgnore
    public Drawable f;
    @DexIgnore
    public Drawable g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public CharSequence i;
    @DexIgnore
    public CharSequence j;
    @DexIgnore
    public CharSequence k;
    @DexIgnore
    public Window.Callback l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public e2 n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public Drawable q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ j1 a; // = new j1(h3.this.a.getContext(), 0, 16908332, 0, 0, h3.this.i);

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            h3 h3Var = h3.this;
            Window.Callback callback = h3Var.l;
            if (callback != null && h3Var.m) {
                callback.onMenuItemSelected(0, this.a);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ja {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public b(int i) {
            this.b = i;
        }

        @DexIgnore
        @Override // com.fossil.ja, com.fossil.ia
        public void a(View view) {
            this.a = true;
        }

        @DexIgnore
        @Override // com.fossil.ia
        public void b(View view) {
            if (!this.a) {
                h3.this.a.setVisibility(this.b);
            }
        }

        @DexIgnore
        @Override // com.fossil.ja, com.fossil.ia
        public void c(View view) {
            h3.this.a.setVisibility(0);
        }
    }

    @DexIgnore
    public h3(Toolbar toolbar, boolean z) {
        this(toolbar, z, f0.abc_action_bar_up_description, c0.abc_ic_ab_back_material);
    }

    @DexIgnore
    public void a(Drawable drawable) {
        this.f = drawable;
        r();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void a(boolean z) {
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        this.j = charSequence;
        if ((this.b & 8) != 0) {
            this.a.setSubtitle(charSequence);
        }
    }

    @DexIgnore
    public void c(int i2) {
        if (i2 != this.p) {
            this.p = i2;
            if (TextUtils.isEmpty(this.a.getNavigationContentDescription())) {
                d(this.p);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void collapseActionView() {
        this.a.c();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public boolean d() {
        return this.a.m();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public boolean e() {
        return this.a.l();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public boolean f() {
        return this.a.r();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void g() {
        this.a.d();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public Context getContext() {
        return this.a.getContext();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public CharSequence getTitle() {
        return this.a.getTitle();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public boolean h() {
        return this.a.k();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public Menu i() {
        return this.a.getMenu();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public int j() {
        return this.o;
    }

    @DexIgnore
    @Override // com.fossil.p2
    public ViewGroup k() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.p2
    public int l() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void m() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void n() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    @DexIgnore
    public final int o() {
        if (this.a.getNavigationIcon() == null) {
            return 11;
        }
        this.q = this.a.getNavigationIcon();
        return 15;
    }

    @DexIgnore
    public final void p() {
        if ((this.b & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.k)) {
            this.a.setNavigationContentDescription(this.p);
        } else {
            this.a.setNavigationContentDescription(this.k);
        }
    }

    @DexIgnore
    public final void q() {
        if ((this.b & 4) != 0) {
            Toolbar toolbar = this.a;
            Drawable drawable = this.g;
            if (drawable == null) {
                drawable = this.q;
            }
            toolbar.setNavigationIcon(drawable);
            return;
        }
        this.a.setNavigationIcon((Drawable) null);
    }

    @DexIgnore
    public final void r() {
        Drawable drawable;
        int i2 = this.b;
        if ((i2 & 2) == 0) {
            drawable = null;
        } else if ((i2 & 1) != 0) {
            drawable = this.f;
            if (drawable == null) {
                drawable = this.e;
            }
        } else {
            drawable = this.e;
        }
        this.a.setLogo(drawable);
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void setIcon(int i2) {
        setIcon(i2 != 0 ? t0.c(getContext(), i2) : null);
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void setTitle(CharSequence charSequence) {
        this.h = true;
        c(charSequence);
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void setVisibility(int i2) {
        this.a.setVisibility(i2);
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void setWindowCallback(Window.Callback callback) {
        this.l = callback;
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void setWindowTitle(CharSequence charSequence) {
        if (!this.h) {
            c(charSequence);
        }
    }

    @DexIgnore
    public h3(Toolbar toolbar, boolean z, int i2, int i3) {
        Drawable drawable;
        this.o = 0;
        this.p = 0;
        this.a = toolbar;
        this.i = toolbar.getTitle();
        this.j = toolbar.getSubtitle();
        this.h = this.i != null;
        this.g = toolbar.getNavigationIcon();
        g3 a2 = g3.a(toolbar.getContext(), null, h0.ActionBar, y.actionBarStyle, 0);
        this.q = a2.b(h0.ActionBar_homeAsUpIndicator);
        if (z) {
            CharSequence e2 = a2.e(h0.ActionBar_title);
            if (!TextUtils.isEmpty(e2)) {
                setTitle(e2);
            }
            CharSequence e3 = a2.e(h0.ActionBar_subtitle);
            if (!TextUtils.isEmpty(e3)) {
                b(e3);
            }
            Drawable b2 = a2.b(h0.ActionBar_logo);
            if (b2 != null) {
                a(b2);
            }
            Drawable b3 = a2.b(h0.ActionBar_icon);
            if (b3 != null) {
                setIcon(b3);
            }
            if (this.g == null && (drawable = this.q) != null) {
                b(drawable);
            }
            a(a2.d(h0.ActionBar_displayOptions, 0));
            int g2 = a2.g(h0.ActionBar_customNavigationLayout, 0);
            if (g2 != 0) {
                a(LayoutInflater.from(this.a.getContext()).inflate(g2, (ViewGroup) this.a, false));
                a(this.b | 16);
            }
            int f2 = a2.f(h0.ActionBar_height, 0);
            if (f2 > 0) {
                ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
                layoutParams.height = f2;
                this.a.setLayoutParams(layoutParams);
            }
            int b4 = a2.b(h0.ActionBar_contentInsetStart, -1);
            int b5 = a2.b(h0.ActionBar_contentInsetEnd, -1);
            if (b4 >= 0 || b5 >= 0) {
                this.a.b(Math.max(b4, 0), Math.max(b5, 0));
            }
            int g3 = a2.g(h0.ActionBar_titleTextStyle, 0);
            if (g3 != 0) {
                Toolbar toolbar2 = this.a;
                toolbar2.b(toolbar2.getContext(), g3);
            }
            int g4 = a2.g(h0.ActionBar_subtitleTextStyle, 0);
            if (g4 != 0) {
                Toolbar toolbar3 = this.a;
                toolbar3.a(toolbar3.getContext(), g4);
            }
            int g5 = a2.g(h0.ActionBar_popupTheme, 0);
            if (g5 != 0) {
                this.a.setPopupTheme(g5);
            }
        } else {
            this.b = o();
        }
        a2.b();
        c(i2);
        this.k = this.a.getNavigationContentDescription();
        this.a.setNavigationOnClickListener(new a());
    }

    @DexIgnore
    public void d(int i2) {
        a(i2 == 0 ? null : getContext().getString(i2));
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void setIcon(Drawable drawable) {
        this.e = drawable;
        r();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public boolean a() {
        return this.a.n();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void a(Menu menu, v1.a aVar) {
        if (this.n == null) {
            e2 e2Var = new e2(this.a.getContext());
            this.n = e2Var;
            e2Var.a(d0.action_menu_presenter);
        }
        this.n.a(aVar);
        this.a.a((p1) menu, this.n);
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void b(int i2) {
        a(i2 != 0 ? t0.c(getContext(), i2) : null);
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void b() {
        this.m = true;
    }

    @DexIgnore
    public final void c(CharSequence charSequence) {
        this.i = charSequence;
        if ((this.b & 8) != 0) {
            this.a.setTitle(charSequence);
        }
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void b(boolean z) {
        this.a.setCollapsible(z);
    }

    @DexIgnore
    public void b(Drawable drawable) {
        this.g = drawable;
        q();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public boolean c() {
        return this.a.b();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void a(int i2) {
        View view;
        int i3 = this.b ^ i2;
        this.b = i2;
        if (i3 != 0) {
            if ((i3 & 4) != 0) {
                if ((i2 & 4) != 0) {
                    p();
                }
                q();
            }
            if ((i3 & 3) != 0) {
                r();
            }
            if ((i3 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    this.a.setTitle(this.i);
                    this.a.setSubtitle(this.j);
                } else {
                    this.a.setTitle((CharSequence) null);
                    this.a.setSubtitle((CharSequence) null);
                }
            }
            if ((i3 & 16) != 0 && (view = this.d) != null) {
                if ((i2 & 16) != 0) {
                    this.a.addView(view);
                } else {
                    this.a.removeView(view);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void a(z2 z2Var) {
        Toolbar toolbar;
        View view = this.c;
        if (view != null && view.getParent() == (toolbar = this.a)) {
            toolbar.removeView(this.c);
        }
        this.c = z2Var;
        if (z2Var != null && this.o == 2) {
            this.a.addView(z2Var, 0);
            Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) this.c.getLayoutParams();
            ((ViewGroup.MarginLayoutParams) layoutParams).width = -2;
            ((ViewGroup.MarginLayoutParams) layoutParams).height = -2;
            ((ActionBar.LayoutParams) layoutParams).a = 8388691;
            z2Var.setAllowCollapse(true);
        }
    }

    @DexIgnore
    public void a(View view) {
        View view2 = this.d;
        if (!(view2 == null || (this.b & 16) == 0)) {
            this.a.removeView(view2);
        }
        this.d = view;
        if (view != null && (this.b & 16) != 0) {
            this.a.addView(view);
        }
    }

    @DexIgnore
    @Override // com.fossil.p2
    public ha a(int i2, long j2) {
        ha a2 = da.a(this.a);
        a2.a(i2 == 0 ? 1.0f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        a2.a(j2);
        a2.a(new b(i2));
        return a2;
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        this.k = charSequence;
        p();
    }

    @DexIgnore
    @Override // com.fossil.p2
    public void a(v1.a aVar, p1.a aVar2) {
        this.a.a(aVar, aVar2);
    }
}
