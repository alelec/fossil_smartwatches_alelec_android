package com.fossil;

import com.portfolio.platform.data.model.room.microapp.MicroApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface i66 extends dl4<h66> {
    @DexIgnore
    void a(MicroApp microApp);

    @DexIgnore
    void b(String str);

    @DexIgnore
    void b(List<r87<MicroApp, String>> list);

    @DexIgnore
    void c(List<r87<MicroApp, String>> list);

    @DexIgnore
    void n();
}
