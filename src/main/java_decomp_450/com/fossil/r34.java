package com.fossil;

import com.fossil.v54;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r34 implements n44 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public r34(String str, String str2, byte[] bArr) {
        this.b = str;
        this.c = str2;
        this.a = bArr;
    }

    @DexIgnore
    @Override // com.fossil.n44
    public String a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.n44
    public InputStream b() {
        if (e()) {
            return null;
        }
        return new ByteArrayInputStream(this.a);
    }

    @DexIgnore
    @Override // com.fossil.n44
    public v54.c.b c() {
        byte[] d = d();
        if (d == null) {
            return null;
        }
        v54.c.b.a c2 = v54.c.b.c();
        c2.a(d);
        c2.a(this.b);
        return c2.a();
    }

    @DexIgnore
    public final byte[] d() {
        if (e()) {
            return null;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                try {
                    gZIPOutputStream.write(this.a);
                    gZIPOutputStream.finish();
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    gZIPOutputStream.close();
                    byteArrayOutputStream.close();
                    return byteArray;
                } catch (Throwable unused) {
                }
            } catch (Throwable unused2) {
            }
        } catch (IOException unused3) {
            return null;
        }
        throw th;
        throw th;
    }

    @DexIgnore
    public final boolean e() {
        byte[] bArr = this.a;
        return bArr == null || bArr.length == 0;
    }
}
