package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum m01 {
    BOND_NONE(10),
    BONDING(11),
    BONDED(12);
    
    @DexIgnore
    public static /* final */ vy0 e; // = new vy0(null);

    @DexIgnore
    public m01(int i) {
    }
}
