package com.fossil;

import android.content.ContentResolver;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.ContactsContract;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class tx extends qx<InputStream> {
    public static final UriMatcher d;

    /*
    static {
        UriMatcher uriMatcher = new UriMatcher(-1);
        d = uriMatcher;
        uriMatcher.addURI("com.android.contacts", "contacts/lookup/*/#", 1);
        d.addURI("com.android.contacts", "contacts/lookup/*", 1);
        d.addURI("com.android.contacts", "contacts/#/photo", 2);
        d.addURI("com.android.contacts", "contacts/#", 3);
        d.addURI("com.android.contacts", "contacts/#/display_photo", 4);
        d.addURI("com.android.contacts", "phone_lookup/*", 5);
    }
    */

    public tx(ContentResolver contentResolver, Uri uri) {
        super(contentResolver, uri);
    }

    public final InputStream b(Uri uri, ContentResolver contentResolver) throws FileNotFoundException {
        int match = d.match(uri);
        if (match != 1) {
            if (match == 3) {
                return a(contentResolver, uri);
            }
            if (match != 5) {
                return contentResolver.openInputStream(uri);
            }
        }
        Uri lookupContact = ContactsContract.Contacts.lookupContact(contentResolver, uri);
        if (lookupContact != null) {
            return a(contentResolver, lookupContact);
        }
        throw new FileNotFoundException("Contact cannot be found");
    }

    @Override // com.fossil.ix
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    @Override // com.fossil.qx
    public InputStream a(Uri uri, ContentResolver contentResolver) throws FileNotFoundException {
        InputStream b = b(uri, contentResolver);
        if (b != null) {
            return b;
        }
        throw new FileNotFoundException("InputStream is null for " + uri);
    }

    public final InputStream a(ContentResolver contentResolver, Uri uri) {
        return ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri, true);
    }

    public void a(InputStream inputStream) throws IOException {
        inputStream.close();
    }
}
