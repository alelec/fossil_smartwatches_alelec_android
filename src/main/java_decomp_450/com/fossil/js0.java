package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class js0 extends s81 {
    @DexIgnore
    public /* final */ fg0 T;

    @DexIgnore
    public js0(ri1 ri1, en0 en0, fg0 fg0) {
        super(ri1, en0, wm0.d0, true, fg0.b(), fg0.getData(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.T = fg0;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        return this.T.getLocaleString();
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.v61, com.fossil.jr1
    public JSONObject i() {
        return yz0.a(super.i(), r51.l3, this.T.a());
    }

    @DexIgnore
    public final fg0 s() {
        return this.T;
    }
}
