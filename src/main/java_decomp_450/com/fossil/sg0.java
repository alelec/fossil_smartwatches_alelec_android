package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sg0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ gg0 a;
    @DexIgnore
    public /* final */ Hashtable<ba0, gg0> b; // = new Hashtable<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<sg0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public sg0 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(gg0.class.getClassLoader());
            if (readParcelable != null) {
                ee7.a((Object) readParcelable, "parcel.readParcelable<No\u2026class.java.classLoader)!!");
                sg0 sg0 = new sg0((gg0) readParcelable);
                Hashtable<ba0, gg0> hashtable = sg0.b;
                Serializable readSerializable = parcel.readSerializable();
                if (readSerializable != null) {
                    hashtable.putAll((Hashtable) readSerializable);
                    return sg0;
                }
                throw new x87("null cannot be cast to non-null type java.util.Hashtable<com.fossil.blesdk.device.data.notification.NotificationType, com.fossil.blesdk.model.file.NotificationIcon>");
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public sg0[] newArray(int i) {
            return new sg0[i];
        }
    }

    @DexIgnore
    public sg0(gg0 gg0) {
        this.a = gg0;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject a2 = yz0.a(new JSONObject(), r51.Y2, this.a.a());
        for (Map.Entry<ba0, gg0> entry : this.b.entrySet()) {
            ba0 key = entry.getKey();
            ee7.a((Object) key, "icon.key");
            a2.put(yz0.a(key), entry.getValue().a());
        }
        return a2;
    }

    @DexIgnore
    public final gg0[] b() {
        Collection<gg0> values = this.b.values();
        ee7.a((Object) values, "typeIcons.values");
        Object[] array = ea7.c((Iterable) ea7.a((Collection) values, (Object) this.a)).toArray(new gg0[0]);
        if (array != null) {
            return (gg0[]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final byte[] c() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ba0[] values = ba0.values();
        int i = 0;
        for (ba0 ba0 : values) {
            gg0 gg0 = this.b.get(ba0);
            int ordinal = 1 << ba0.ordinal();
            if (gg0 == null || gg0.e()) {
                i |= ordinal;
            } else {
                String fileName = gg0.getFileName();
                Charset c = b21.x.c();
                if (fileName != null) {
                    byte[] bytes = fileName.getBytes(c);
                    ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                    byte[] a2 = s97.a(bytes, (byte) 0);
                    ByteBuffer order = ByteBuffer.allocate(a2.length + 3).order(ByteOrder.LITTLE_ENDIAN);
                    ee7.a((Object) order, "ByteBuffer.allocate(TYPE\u2026(ByteOrder.LITTLE_ENDIAN)");
                    order.putShort((short) ordinal).put((byte) a2.length).put(a2);
                    byteArrayOutputStream.write(order.array());
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            }
        }
        if (i != 0) {
            String fileName2 = this.a.getFileName();
            Charset c2 = b21.x.c();
            if (fileName2 != null) {
                byte[] bytes2 = fileName2.getBytes(c2);
                ee7.a((Object) bytes2, "(this as java.lang.String).getBytes(charset)");
                byte[] a3 = s97.a(bytes2, (byte) 0);
                ByteBuffer order2 = ByteBuffer.allocate(a3.length + 3).order(ByteOrder.LITTLE_ENDIAN);
                ee7.a((Object) order2, "ByteBuffer.allocate(TYPE\u2026(ByteOrder.LITTLE_ENDIAN)");
                order2.putShort((short) i).put((byte) a3.length).put(a3);
                byteArrayOutputStream.write(order2.array());
            } else {
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        ee7.a((Object) byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(sg0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            sg0 sg0 = (sg0) obj;
            if (!ee7.a(this.a, sg0.a) || !ee7.a(this.b, sg0.b)) {
                return false;
            }
            return true;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.notification.filter.NotificationIconConfig");
    }

    @DexIgnore
    public final gg0 getDefaultIcon() {
        return this.a;
    }

    @DexIgnore
    public final gg0 getIconForType(ba0 ba0) {
        return this.b.get(ba0);
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + (this.a.hashCode() * 31);
    }

    @DexIgnore
    public final sg0 setIconForType(ba0 ba0, gg0 gg0) {
        if (gg0 != null) {
            this.b.put(ba0, gg0);
        } else {
            this.b.remove(ba0);
        }
        return this;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.a, i);
        }
        if (parcel != null) {
            parcel.writeSerializable(this.b);
        }
    }
}
