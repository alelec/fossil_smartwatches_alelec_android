package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l15 extends k15 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i S; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray T;
    @DexIgnore
    public long R;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        T = sparseIntArray;
        sparseIntArray.put(2131362110, 1);
        T.put(2131362636, 2);
        T.put(2131363342, 3);
        T.put(2131362083, 4);
        T.put(2131362637, 5);
        T.put(2131362374, 6);
        T.put(2131362756, 7);
        T.put(2131362373, 8);
        T.put(2131362751, 9);
        T.put(2131362371, 10);
        T.put(2131362369, 11);
        T.put(2131362705, 12);
        T.put(2131362152, 13);
        T.put(2131362044, 14);
        T.put(2131362085, 15);
        T.put(2131362886, 16);
        T.put(2131362476, 17);
        T.put(2131362417, 18);
        T.put(2131362180, 19);
        T.put(2131363386, 20);
        T.put(2131362090, 21);
        T.put(2131362091, 22);
        T.put(2131362747, 23);
        T.put(2131361881, 24);
        T.put(2131363306, 25);
        T.put(2131363000, 26);
    }
    */

    @DexIgnore
    public l15(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 27, S, T));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.R = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.R != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.R = 1;
        }
        g();
    }

    @DexIgnore
    public l15(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleButton) objArr[24], (ConstraintLayout) objArr[14], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[15], (ConstraintLayout) objArr[21], (ConstraintLayout) objArr[22], (ConstraintLayout) objArr[1], (CoordinatorLayout) objArr[13], (OverviewDayGoalChart) objArr[19], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[18], (FlexibleTextView) objArr[17], (RTLImageView) objArr[2], (RTLImageView) objArr[5], (RTLImageView) objArr[12], (ConstraintLayout) objArr[23], (View) objArr[9], (View) objArr[7], (FlexibleProgressBar) objArr[16], (ConstraintLayout) objArr[0], (RecyclerViewEmptySupport) objArr[26], (FlexibleTextView) objArr[25], (FlexibleTextView) objArr[3], (View) objArr[20]);
        this.R = -1;
        ((k15) this).M.setTag(null);
        a(view);
        f();
    }
}
