package com.fossil;

import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qu1 implements gt1 {
    @DexIgnore
    public /* final */ Set<bt1> a;
    @DexIgnore
    public /* final */ pu1 b;
    @DexIgnore
    public /* final */ tu1 c;

    @DexIgnore
    public qu1(Set<bt1> set, pu1 pu1, tu1 tu1) {
        this.a = set;
        this.b = pu1;
        this.c = tu1;
    }

    @DexIgnore
    @Override // com.fossil.gt1
    public <T> ft1<T> a(String str, Class<T> cls, et1<T, byte[]> et1) {
        return a(str, cls, bt1.a("proto"), et1);
    }

    @DexIgnore
    @Override // com.fossil.gt1
    public <T> ft1<T> a(String str, Class<T> cls, bt1 bt1, et1<T, byte[]> et1) {
        if (this.a.contains(bt1)) {
            return new su1(this.b, str, bt1, et1, this.c);
        }
        throw new IllegalArgumentException(String.format("%s is not supported byt this factory. Supported encodings are: %s.", bt1, this.a));
    }
}
