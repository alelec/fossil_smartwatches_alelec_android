package com.fossil;

import android.app.Activity;
import android.util.Log;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ie5;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xq5 extends tq5 implements ie5.d {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public MFUser e;
    @DexIgnore
    public boolean f; // = true;
    @DexIgnore
    public /* final */ uq5 g;
    @DexIgnore
    public /* final */ UserRepository h;
    @DexIgnore
    public /* final */ ie5 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return xq5.j;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends g12<Status> {
        @DexIgnore
        public /* final */ /* synthetic */ xq5 c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(xq5 xq5, Activity activity, int i) {
            super(activity, i);
            this.c = xq5;
        }

        @DexIgnore
        /* renamed from: c */
        public void b(Status status) {
            ee7.b(status, "status");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = xq5.k.a();
            local.d(a, "onGFLogout.onSuccess(): isSuccess = " + status.y() + ", statusMessage = " + status.v());
            if (status.y()) {
                this.c.j();
                return;
            }
            this.c.g.w(true);
            this.c.g.l(status.g());
        }

        @DexIgnore
        @Override // com.fossil.g12
        public void b(Status status) {
            ee7.b(status, "status");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = xq5.k.a();
            local.d(a, "onGFLogout.onUnresolvableFailure(): isSuccess = " + status.y() + ", statusMessage = " + status.v() + ", statusCode = " + status.g());
            if (status.g() == 17 || status.g() == 5010) {
                this.c.j();
                return;
            }
            this.c.g.w(true);
            this.c.g.l(status.g());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1", f = "ConnectedAppsPresenter.kt", l = {43}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xq5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1$1", f = "ConnectedAppsPresenter.kt", l = {44}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                xq5 xq5;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    xq5 xq52 = this.this$0.this$0;
                    UserRepository c = xq52.h;
                    this.L$0 = yi7;
                    this.L$1 = xq52;
                    this.label = 1;
                    obj = c.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                    xq5 = xq52;
                } else if (i == 1) {
                    xq5 = (xq5) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                xq5.e = (MFUser) obj;
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(xq5 xq5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = xq5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            boolean z;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                boolean e = this.this$0.i.e();
                ti7 a3 = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.Z$0 = e;
                this.label = 1;
                if (vh7.a(a3, aVar, this) == a2) {
                    return a2;
                }
                z = e;
            } else if (i == 1) {
                z = this.Z$0;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a4 = xq5.k.a();
            local.d(a4, "starts isGFConnected=" + this.this$0.i.e());
            this.this$0.i.a(this.this$0);
            this.this$0.g.w(z);
            if (!z && this.this$0.i.f() && !this.this$0.f) {
                this.this$0.i.i();
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xq5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super zi5<? extends MFUser>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super zi5<? extends MFUser>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository c = this.this$0.this$0.h;
                    MFUser mFUser = this.this$0.$it;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = c.updateUser(mFUser, true, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(MFUser mFUser, fb7 fb7, xq5 xq5) {
            super(2, fb7);
            this.$it = mFUser;
            this.this$0 = xq5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.$it, fb7, this.this$0);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(a3, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    /*
    static {
        String simpleName = xq5.class.getSimpleName();
        ee7.a((Object) simpleName, "ConnectedAppsPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public xq5(uq5 uq5, UserRepository userRepository, PortfolioApp portfolioApp, ie5 ie5) {
        ee7.b(uq5, "mView");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(portfolioApp, "mApp");
        ee7.b(ie5, "mGoogleFitHelper");
        this.g = uq5;
        this.h = userRepository;
        this.i = ie5;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ik7 unused = xh7.b(e(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        Log.d(j, "stop");
        MFUser mFUser = this.e;
        if (mFUser != null) {
            ik7 unused = xh7.b(e(), null, null, new d(mFUser, null, this), 3, null);
        }
        this.i.a((ie5.d) null);
    }

    @DexIgnore
    @Override // com.fossil.tq5
    public void h() {
        if (!PortfolioApp.g0.c().z()) {
            this.g.l(601);
            this.g.w(this.i.e());
        } else if (this.i.e()) {
            this.i.g();
        } else if (this.i.d()) {
            this.i.a();
        } else {
            ie5 ie5 = this.i;
            uq5 uq5 = this.g;
            if (uq5 != null) {
                ie5.a((kh6) uq5);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.ConnectedAppsFragment");
        }
    }

    @DexIgnore
    @Override // com.fossil.tq5
    public void i() {
        this.i.a();
    }

    @DexIgnore
    public final void j() {
        this.i.b();
        String name = hb5.GoogleFit.getName();
        ee7.a((Object) name, "Integration.GoogleFit.getName()");
        b(name);
        this.g.w(false);
    }

    @DexIgnore
    public void k() {
        this.g.a(this);
    }

    @DexIgnore
    public final void b(String str) {
        MFUser mFUser = this.e;
        if (mFUser != null) {
            List<String> integrationsList = mFUser.getIntegrationsList();
            if (integrationsList != null) {
                integrationsList.remove(str);
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    mFUser2.setIntegrationsList(integrationsList);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ie5.d
    public void a() {
        FLogger.INSTANCE.getLocal().d(j, "onGFConnectionSuccess");
        String name = hb5.GoogleFit.getName();
        ee7.a((Object) name, "Integration.GoogleFit.getName()");
        a(name);
        this.g.w(true);
    }

    @DexIgnore
    @Override // com.fossil.ie5.d
    public void a(i02 i02) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onGFConnectionFailed - Cause: " + i02);
        if (i02 == null) {
            return;
        }
        if (i02.w()) {
            this.f = false;
            this.g.a(i02);
            return;
        }
        this.g.l(i02.e());
    }

    @DexIgnore
    @Override // com.fossil.ie5.d
    public void a(c12<Status> c12) {
        if (c12 != null) {
            uq5 uq5 = this.g;
            if (uq5 != null) {
                FragmentActivity activity = ((kh6) uq5).getActivity();
                if (activity != null) {
                    c12.a(new b(this, activity, 3));
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.ConnectedAppsFragment");
            }
        }
    }

    @DexIgnore
    public final void a(String str) {
        MFUser mFUser = this.e;
        if (mFUser != null) {
            List<String> integrationsList = mFUser.getIntegrationsList();
            if (integrationsList != null) {
                if (!integrationsList.contains(str)) {
                    integrationsList.add(str);
                }
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    mFUser2.setIntegrationsList(integrationsList);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }
}
