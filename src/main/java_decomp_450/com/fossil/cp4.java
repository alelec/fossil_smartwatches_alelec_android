package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cp4 implements Factory<bp4> {
    @DexIgnore
    public /* final */ Provider<zo4> a;
    @DexIgnore
    public /* final */ Provider<ap4> b;

    @DexIgnore
    public cp4(Provider<zo4> provider, Provider<ap4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static cp4 a(Provider<zo4> provider, Provider<ap4> provider2) {
        return new cp4(provider, provider2);
    }

    @DexIgnore
    public static bp4 a(zo4 zo4, ap4 ap4) {
        return new bp4(zo4, ap4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public bp4 get() {
        return a(this.a.get(), this.b.get());
    }
}
