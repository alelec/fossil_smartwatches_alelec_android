package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kt4 extends he {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public boolean b; // = true;
    @DexIgnore
    public Range c;
    @DexIgnore
    public /* final */ LiveData<List<oo4>> d;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> e;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> f;
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, ServerError>> g;
    @DexIgnore
    public /* final */ ro4 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$historyChallengeFromServer$2", f = "BCHistoryViewModel.kt", l = {124}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends yn4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ int $offset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(kt4 kt4, int i, int i2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kt4;
            this.$limit = i;
            this.$offset = i2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.this$0, this.$limit, this.$offset, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super List<? extends yn4>> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ro4 b = this.this$0.h;
                int i2 = this.$limit;
                int i3 = this.$offset;
                this.L$0 = yi7;
                this.label = 1;
                obj = b.a(i2, i3, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko4 = (ko4) obj;
            List list = (List) ko4.c();
            if (list == null) {
                List<oo4> a2 = this.this$0.c().a();
                if (a2 == null || a2.isEmpty()) {
                    this.this$0.g.a(w87.a(pb7.a(true), ko4.a()));
                } else {
                    this.this$0.g.a(w87.a(pb7.a(false), ko4.a()));
                }
            } else {
                this.this$0.c = ko4.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String c = this.this$0.a;
                StringBuilder sb = new StringBuilder();
                sb.append("historyChallengeFromServer - hasNext: ");
                Range a3 = this.this$0.c;
                sb.append(a3 != null ? pb7.a(a3.isHasNext()) : null);
                sb.append(" - offset: ");
                Range a4 = this.this$0.c;
                sb.append(a4 != null ? pb7.a(a4.getOffset()) : null);
                sb.append(" - inputOffset: ");
                sb.append(this.$offset);
                local.e(c, sb.toString());
                this.this$0.g.a((Object) null);
                if (!list.isEmpty() || this.$offset != 0) {
                    this.this$0.f.a(pb7.a(false));
                } else {
                    this.this$0.f.a(pb7.a(true));
                }
            }
            return list;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ kt4 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$historyChallengesLive$1$1", f = "BCHistoryViewModel.kt", l = {41}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $histories;
            @DexIgnore
            public /* final */ /* synthetic */ MutableLiveData $result;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kt4$b$a$a")
            @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$historyChallengesLive$1$1$uiHistories$1", f = "BCHistoryViewModel.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.kt4$b$a$a  reason: collision with other inner class name */
            public static final class C0103a extends zb7 implements kd7<yi7, fb7<? super List<? extends oo4>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0103a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0103a aVar = new C0103a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super List<? extends oo4>> fb7) {
                    return ((C0103a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        List list = this.this$0.$histories;
                        ee7.a((Object) list, "histories");
                        return nt4.d(list);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(List list, MutableLiveData mutableLiveData, fb7 fb7) {
                super(2, fb7);
                this.$histories = list;
                this.$result = mutableLiveData;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$histories, this.$result, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 a2 = qj7.a();
                    C0103a aVar = new C0103a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = vh7.a(a2, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.$result.a((List) obj);
                return i97.a;
            }
        }

        @DexIgnore
        public b(kt4 kt4) {
            this.a = kt4;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<oo4>> apply(List<yn4> list) {
            MutableLiveData<List<oo4>> mutableLiveData = new MutableLiveData<>();
            if (!list.isEmpty()) {
                this.a.f.a((Object) false);
                this.a.e.a((Object) false);
            } else if (!this.a.b) {
                this.a.f.a((Object) true);
            }
            if (this.a.b) {
                this.a.b = false;
                this.a.e();
            }
            ik7 unused = xh7.b(ie.a(this.a), null, null, new a(list, mutableLiveData, null), 3, null);
            return mutableLiveData;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$loadHistoryChallenge$1", f = "BCHistoryViewModel.kt", l = {65}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(kt4 kt4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kt4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                kt4 kt4 = this.this$0;
                this.L$0 = yi7;
                this.label = 1;
                if (kt4.a(5, 0, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.e.a(pb7.a(false));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$loadMore$1", f = "BCHistoryViewModel.kt", l = {112, 115}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $position;
        @DexIgnore
        public /* final */ /* synthetic */ int $total;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(kt4 kt4, int i, int i2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kt4;
            this.$position = i;
            this.$total = i2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$position, this.$total, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Integer a;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String c = this.this$0.a;
                StringBuilder sb = new StringBuilder();
                sb.append("loadMore: hasNext: ");
                Range a3 = this.this$0.c;
                sb.append(a3 != null ? pb7.a(a3.isHasNext()) : null);
                sb.append(" - offset: ");
                Range a4 = this.this$0.c;
                sb.append(a4 != null ? pb7.a(a4.getOffset()) : null);
                sb.append(" - position: ");
                sb.append(this.$position);
                sb.append(" - total: ");
                sb.append(this.$total);
                local.e(c, sb.toString());
                int i2 = this.$position + 1;
                if (i2 % 5 == 0 && i2 == this.$total) {
                    List<oo4> a5 = this.this$0.c().a();
                    int i3 = 0;
                    if (!(a5 == null || a5.isEmpty()) && a5.size() < 15) {
                        if (this.this$0.c == null) {
                            kt4 kt4 = this.this$0;
                            this.L$0 = yi7;
                            this.I$0 = i2;
                            this.L$1 = a5;
                            this.label = 1;
                            if (kt4.a(5, i2, this) == a2) {
                                return a2;
                            }
                        } else if (this.this$0.c != null) {
                            Range a6 = this.this$0.c;
                            if (a6 == null) {
                                ee7.a();
                                throw null;
                            } else if (a6.isHasNext()) {
                                Range a7 = this.this$0.c;
                                if (!(a7 == null || (a = pb7.a(a7.getOffset())) == null)) {
                                    i3 = a.intValue();
                                }
                                if (i2 > i3) {
                                    kt4 kt42 = this.this$0;
                                    this.L$0 = yi7;
                                    this.I$0 = i2;
                                    this.L$1 = a5;
                                    this.label = 2;
                                    if (kt42.a(5, i2, this) == a2) {
                                        return a2;
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (i == 1 || i == 2) {
                List list = (List) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$refresh$1", f = "BCHistoryViewModel.kt", l = {75, 79, 82, 84, 89}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(kt4 kt4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kt4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                List<oo4> a2 = this.this$0.c().a();
                if (a2 == null || a2.isEmpty()) {
                    kt4 kt4 = this.this$0;
                    this.L$0 = yi7;
                    this.L$1 = a2;
                    this.label = 5;
                    if (kt4.a(5, 0, this) == a) {
                        return a;
                    }
                } else if (a2.size() == 15) {
                    kt4 kt42 = this.this$0;
                    this.L$0 = yi7;
                    this.L$1 = a2;
                    this.label = 1;
                    if (kt42.a(5, 0, this) == a) {
                        return a;
                    }
                } else {
                    int size = (a2.size() / 5) * 5;
                    if (this.this$0.c == null) {
                        kt4 kt43 = this.this$0;
                        this.L$0 = yi7;
                        this.L$1 = a2;
                        this.I$0 = size;
                        this.label = 2;
                        if (kt43.a(5, size, this) == a) {
                            return a;
                        }
                    } else {
                        Range a3 = this.this$0.c;
                        if (a3 == null) {
                            ee7.a();
                            throw null;
                        } else if (a3.isHasNext()) {
                            kt4 kt44 = this.this$0;
                            this.L$0 = yi7;
                            this.L$1 = a2;
                            this.I$0 = size;
                            this.label = 3;
                            if (kt44.a(5, size, this) == a) {
                                return a;
                            }
                        } else {
                            kt4 kt45 = this.this$0;
                            this.L$0 = yi7;
                            this.L$1 = a2;
                            this.I$0 = size;
                            this.label = 4;
                            if (kt45.a(5, 0, this) == a) {
                                return a;
                            }
                        }
                    }
                }
            } else if (i == 1 || i == 2 || i == 3 || i == 4 || i == 5) {
                List list = (List) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.e.a(pb7.a(false));
            return i97.a;
        }
    }

    @DexIgnore
    public kt4(ro4 ro4) {
        ee7.b(ro4, "challengeRepository");
        this.h = ro4;
        String simpleName = kt4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCHistoryViewModel::class.java.simpleName");
        this.a = simpleName;
        LiveData<List<oo4>> b2 = ge.b(au4.a(this.h.d()), new b(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026  }\n\n        result\n    }");
        this.d = b2;
        this.e = new MutableLiveData<>();
        this.f = new MutableLiveData<>();
        this.g = new MutableLiveData<>();
    }

    @DexIgnore
    public final LiveData<r87<Boolean, ServerError>> b() {
        return this.g;
    }

    @DexIgnore
    public final LiveData<List<oo4>> c() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Boolean> d() {
        return this.e;
    }

    @DexIgnore
    public final void e() {
        FLogger.INSTANCE.getLocal().e(this.a, "loadHistoryChallenge");
        ik7 unused = xh7.b(ie.a(this), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    public final void b(int i, int i2) {
        ik7 unused = xh7.b(ie.a(this), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final LiveData<Boolean> a() {
        return this.f;
    }

    @DexIgnore
    public final void a(int i, int i2) {
        ik7 unused = xh7.b(ie.a(this), null, null, new d(this, i, i2, null), 3, null);
    }

    @DexIgnore
    public final /* synthetic */ Object a(int i, int i2, fb7<? super List<yn4>> fb7) {
        return vh7.a(qj7.b(), new a(this, i, i2, null), fb7);
    }
}
