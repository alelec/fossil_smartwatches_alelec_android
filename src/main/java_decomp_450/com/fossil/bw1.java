package com.fossil;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.PersistableBundle;
import android.util.Base64;
import com.facebook.applinks.AppLinkData;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.JobInfoSchedulerService;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.zip.Adler32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bw1 implements pw1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ sw1 b;
    @DexIgnore
    public /* final */ dw1 c;

    @DexIgnore
    public bw1(Context context, sw1 sw1, dw1 dw1) {
        this.a = context;
        this.b = sw1;
        this.c = dw1;
    }

    @DexIgnore
    public int a(pu1 pu1) {
        Adler32 adler32 = new Adler32();
        adler32.update(this.a.getPackageName().getBytes(Charset.forName("UTF-8")));
        adler32.update(pu1.a().getBytes(Charset.forName("UTF-8")));
        adler32.update(ByteBuffer.allocate(4).putInt(hy1.a(pu1.c())).array());
        if (pu1.b() != null) {
            adler32.update(pu1.b());
        }
        return (int) adler32.getValue();
    }

    @DexIgnore
    public final boolean a(JobScheduler jobScheduler, int i, int i2) {
        for (JobInfo jobInfo : jobScheduler.getAllPendingJobs()) {
            int i3 = jobInfo.getExtras().getInt("attemptNumber");
            if (jobInfo.getId() == i) {
                if (i3 >= i2) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.pw1
    public void a(pu1 pu1, int i) {
        ComponentName componentName = new ComponentName(this.a, JobInfoSchedulerService.class);
        JobScheduler jobScheduler = (JobScheduler) this.a.getSystemService("jobscheduler");
        int a2 = a(pu1);
        if (a(jobScheduler, a2, i)) {
            kv1.a("JobInfoScheduler", "Upload for context %s is already scheduled. Returning...", pu1);
            return;
        }
        long b2 = this.b.b(pu1);
        dw1 dw1 = this.c;
        JobInfo.Builder builder = new JobInfo.Builder(a2, componentName);
        dw1.a(builder, pu1.c(), b2, i);
        PersistableBundle persistableBundle = new PersistableBundle();
        persistableBundle.putInt("attemptNumber", i);
        persistableBundle.putString("backendName", pu1.a());
        persistableBundle.putInt("priority", hy1.a(pu1.c()));
        if (pu1.b() != null) {
            persistableBundle.putString(AppLinkData.ARGUMENTS_EXTRAS_KEY, Base64.encodeToString(pu1.b(), 0));
        }
        builder.setExtras(persistableBundle);
        kv1.a("JobInfoScheduler", "Scheduling upload for context %s with jobId=%d in %dms(Backend next call timestamp %d). Attempt %d", pu1, Integer.valueOf(a2), Long.valueOf(this.c.a(pu1.c(), b2, i)), Long.valueOf(b2), Integer.valueOf(i));
        jobScheduler.schedule(builder.build());
    }
}
