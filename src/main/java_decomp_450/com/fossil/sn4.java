package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.ShareConstants;
import com.portfolio.platform.data.legacy.onedotfive.FavoriteMappingSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sn4 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    @te4("name")
    public String a;
    @DexIgnore
    @te4("description")
    public String b;
    @DexIgnore
    @te4(FavoriteMappingSet.COLUMN_THUMBNAIL)
    public int c;
    @DexIgnore
    @te4("type")
    public String d;
    @DexIgnore
    @te4("step")
    public int e;
    @DexIgnore
    @te4("duration")
    public int f;
    @DexIgnore
    @te4(ShareConstants.WEB_DIALOG_PARAM_PRIVACY)
    public String g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<sn4> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public sn4 createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new sn4(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public sn4[] newArray(int i) {
            return new sn4[i];
        }
    }

    @DexIgnore
    public sn4(String str, String str2, int i, String str3, int i2, int i3, String str4) {
        ee7.b(str3, "type");
        ee7.b(str4, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
        this.a = str;
        this.b = str2;
        this.c = i;
        this.d = str3;
        this.e = i2;
        this.f = i3;
        this.g = str4;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final int b() {
        return this.f;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final String d() {
        return this.g;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof sn4)) {
            return false;
        }
        sn4 sn4 = (sn4) obj;
        return ee7.a(this.a, sn4.a) && ee7.a(this.b, sn4.b) && this.c == sn4.c && ee7.a(this.d, sn4.d) && this.e == sn4.e && this.f == sn4.f && ee7.a(this.g, sn4.g);
    }

    @DexIgnore
    public final int f() {
        return this.c;
    }

    @DexIgnore
    public final String g() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (((hashCode + (str2 != null ? str2.hashCode() : 0)) * 31) + this.c) * 31;
        String str3 = this.d;
        int hashCode3 = (((((hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31) + this.e) * 31) + this.f) * 31;
        String str4 = this.g;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public String toString() {
        return "ChallengeTemplate(name=" + this.a + ", des=" + this.b + ", thumbnail=" + this.c + ", type=" + this.d + ", target=" + this.e + ", duration=" + this.f + ", privacy=" + this.g + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeInt(this.e);
        parcel.writeInt(this.f);
        parcel.writeString(this.g);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public sn4(android.os.Parcel r10) {
        /*
            r9 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r10, r0)
            java.lang.String r2 = r10.readString()
            java.lang.String r3 = r10.readString()
            int r4 = r10.readInt()
            java.lang.String r5 = r10.readString()
            r0 = 0
            if (r5 == 0) goto L_0x0037
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.ee7.a(r5, r1)
            int r6 = r10.readInt()
            int r7 = r10.readInt()
            java.lang.String r8 = r10.readString()
            if (r8 == 0) goto L_0x0033
            com.fossil.ee7.a(r8, r1)
            r1 = r9
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            return
        L_0x0033:
            com.fossil.ee7.a()
            throw r0
        L_0x0037:
            com.fossil.ee7.a()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sn4.<init>(android.os.Parcel):void");
    }
}
