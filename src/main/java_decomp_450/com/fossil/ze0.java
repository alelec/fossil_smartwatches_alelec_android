package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ze0 extends cf0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ r60 c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ze0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ze0 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(vb0.class.getClassLoader());
            if (readParcelable != null) {
                ee7.a((Object) readParcelable, "parcel.readParcelable<Co\u2026class.java.classLoader)!!");
                vb0 vb0 = (vb0) readParcelable;
                df0 df0 = (df0) parcel.readParcelable(df0.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(r60.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new ze0(vb0, df0, (r60) readParcelable2, parcel.readInt(), parcel.readInt());
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ze0[] newArray(int i) {
            return new ze0[i];
        }
    }

    @DexIgnore
    public ze0(vb0 vb0, r60 r60, int i, int i2) {
        super(vb0, null);
        this.d = i;
        this.e = i2;
        this.c = r60;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public byte[] a(short s, r60 r60) {
        try {
            qs1 qs1 = qs1.d;
            yb0 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return qs1.a(s, r60, new jt0(((ac0) deviceRequest).d(), new r60(this.c.getMajor(), this.c.getMinor()), this.d, this.e).getData());
            }
            throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (f41 e2) {
            wl0.h.a(e2);
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public JSONObject b() {
        return yz0.a(yz0.a(yz0.a(super.b(), r51.r3, this.c.toString()), r51.z, Integer.valueOf(this.d)), r51.A, Integer.valueOf(this.e));
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(ze0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            ze0 ze0 = (ze0) obj;
            return !(ee7.a(this.c, ze0.c) ^ true) && this.d == ze0.d && this.e == ze0.e;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.CommuteTimeETAMicroAppData");
    }

    @DexIgnore
    public final int getHour() {
        return this.d;
    }

    @DexIgnore
    public final r60 getMicroAppVersion() {
        return this.c;
    }

    @DexIgnore
    public final int getMinute() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = Integer.valueOf(this.d).hashCode();
        return Integer.valueOf(this.e).hashCode() + ((hashCode2 + ((hashCode + (super.hashCode() * 31)) * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }

    @DexIgnore
    public ze0(vb0 vb0, df0 df0, r60 r60, int i, int i2) {
        super(vb0, df0);
        this.d = i;
        this.e = i2;
        this.c = r60;
    }
}
