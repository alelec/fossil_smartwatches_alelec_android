package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.ShareConstants;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vm4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ a b; // = new a(this);
    @DexIgnore
    public /* final */ qd5 c;
    @DexIgnore
    public /* final */ ro4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ vm4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(vm4 vm4) {
            this.a = vm4;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            String str;
            vm4 vm4 = this.a;
            if (intent == null || (str = intent.getAction()) == null) {
                str = "";
            }
            vm4.a(str, intent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.analytic.BCAnalytic$logChallengeFriend$1", f = "BCAnalytic.kt", l = {252}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vm4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(vm4 vm4, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vm4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$challengeId, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                this.L$0 = this.p$;
                this.label = 1;
                obj = ro4.a(this.this$0.d, this.$challengeId, new String[]{"invited"}, 0, this, 4, null);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko4 = (ko4) obj;
            if (ko4.c() != null) {
                for (jn4 jn4 : (Iterable) ko4.c()) {
                    this.this$0.a(this.$challengeId, PortfolioApp.g0.c().w(), jn4.d());
                }
            }
            return i97.a;
        }
    }

    @DexIgnore
    public vm4(qd5 qd5, ro4 ro4) {
        ee7.b(qd5, "firebaseAnalytics");
        ee7.b(ro4, "challengeRepository");
        this.c = qd5;
        this.d = ro4;
        String simpleName = vm4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCAnalytic::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public final void b(Intent intent) {
        String str;
        String stringExtra;
        String str2 = "";
        if (intent == null || (str = intent.getStringExtra("current_user_id_extra")) == null) {
            str = str2;
        }
        if (!(intent == null || (stringExtra = intent.getStringExtra("friend_id_extra")) == null)) {
            str2 = stringExtra;
        }
        HashMap<String, Object> a2 = a();
        a2.put("profile_id", str);
        a2.put("to_profile_id", str2);
        this.c.a("bc_accept_friend_request", a2);
    }

    @DexIgnore
    public final void c(Intent intent) {
        String str;
        String stringExtra;
        String str2 = "";
        if (intent == null || (str = intent.getStringExtra("challenge_id_extra")) == null) {
            str = str2;
        }
        if (!(intent == null || (stringExtra = intent.getStringExtra("current_user_id_extra")) == null)) {
            str2 = stringExtra;
        }
        boolean z = false;
        int intExtra = intent != null ? intent.getIntExtra("rank_extra", 0) : 0;
        int intExtra2 = intent != null ? intent.getIntExtra("current_steps_extra", 0) : 0;
        if (intent != null) {
            z = intent.getBooleanExtra("reach_goal_or_top", false);
        }
        HashMap<String, Object> a2 = a();
        a2.put("challenge_id", str);
        a2.put("profile_id", str2);
        a2.put("rank", Integer.valueOf(intExtra));
        a2.put("number_of_steps", Integer.valueOf(intExtra2));
        a2.put("is_reach_goal_or_get_best_result", Boolean.valueOf(z));
        this.c.a("bc_complete_challenge", a2);
    }

    @DexIgnore
    public final void d(Intent intent) {
        String str;
        String f;
        String str2;
        long j = 0;
        long j2 = 0L;
        if (intent != null) {
            j = intent.getLongExtra("start_tracking_time_extra", 0);
        }
        String str3 = "";
        if (intent == null || (str = intent.getStringExtra("start_type_extra")) == null) {
            str = str3;
        }
        HashMap<String, Object> hashMap = null;
        mn4 mn4 = intent != null ? (mn4) intent.getParcelableExtra("challenge_extra") : null;
        if (mn4 != null) {
            hashMap = a();
            eo4 i = mn4.i();
            if (i == null || (str2 = i.b()) == null) {
                str2 = str3;
            }
            hashMap.put("profile_id", str2);
            hashMap.put("challenge_id", mn4.f());
            hashMap.put("start_tracking_at", Long.valueOf(j));
            hashMap.put("end_tracking_at", Long.valueOf(System.currentTimeMillis()));
            String g = mn4.g();
            if (g == null) {
                g = str3;
            }
            hashMap.put("challenge_name", g);
            hashMap.put("challenge_mode", ee7.a(mn4.u(), "activity_best_result") ? "ABR" : "ARG");
            Date b2 = mn4.b();
            hashMap.put("challenge_created_at", b2 != null ? Long.valueOf(b2.getTime()) : 0L);
            hashMap.put("challenge_start_type", str);
            Date m = mn4.m();
            hashMap.put("challenge_start_at", m != null ? Long.valueOf(m.getTime()) : 0L);
            Date e = mn4.e();
            if (e != null) {
                j2 = Long.valueOf(e.getTime());
            }
            hashMap.put("challenge_end_at", j2);
            Integer d2 = mn4.d();
            hashMap.put("duration", Float.valueOf((d2 != null ? (float) d2.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) 3600)));
            String j3 = mn4.j();
            if (j3 == null) {
                j3 = str3;
            }
            hashMap.put(ShareConstants.WEB_DIALOG_PARAM_PRIVACY, j3);
            Integer t = mn4.t();
            if (t != null) {
                hashMap.put("goal", Integer.valueOf(t.intValue()));
            }
        }
        this.c.a("bc_create_challenge", hashMap);
        if (!(mn4 == null || (f = mn4.f()) == null)) {
            str3 = f;
        }
        a(str3);
    }

    @DexIgnore
    public final void e(Intent intent) {
        String str;
        if (intent == null || (str = intent.getStringExtra("current_user_id_extra")) == null) {
            str = "";
        }
        HashMap<String, Object> a2 = a();
        a2.put("profile_id", str);
        this.c.a("bc_create_social_profile", a2);
    }

    @DexIgnore
    public final void f(Intent intent) {
        String str;
        if (intent == null || (str = intent.getStringExtra("current_user_id_extra")) == null) {
            str = "";
        }
        HashMap<String, Object> a2 = a();
        a2.put("profile_id", str);
        this.c.a("bc_look_for_friend", a2);
    }

    @DexIgnore
    public final void g(Intent intent) {
        String str;
        if (intent == null || (str = intent.getStringExtra("current_user_id_extra")) == null) {
            str = "";
        }
        HashMap<String, Object> a2 = a();
        a2.put("profile_id", str);
        this.c.a("bc_look_for_friend_challenge", a2);
    }

    @DexIgnore
    public final void h(Intent intent) {
        String str;
        String stringExtra;
        String str2 = "";
        if (intent == null || (str = intent.getStringExtra("challenge_id_extra")) == null) {
            str = str2;
        }
        if (!(intent == null || (stringExtra = intent.getStringExtra("current_user_id_extra")) == null)) {
            str2 = stringExtra;
        }
        HashMap<String, Object> a2 = a();
        a2.put("challenge_id", str);
        a2.put("profile_id", str2);
        this.c.a("bc_join_challenge", a2);
    }

    @DexIgnore
    public final void i(Intent intent) {
        String str;
        String stringExtra;
        String str2 = "";
        if (intent == null || (str = intent.getStringExtra("challenge_id_extra")) == null) {
            str = str2;
        }
        if (!(intent == null || (stringExtra = intent.getStringExtra("current_user_id_extra")) == null)) {
            str2 = stringExtra;
        }
        int i = 0;
        if (intent != null) {
            i = intent.getIntExtra("current_steps_extra", 0);
        }
        HashMap<String, Object> a2 = a();
        a2.put("challenge_id", str);
        a2.put("profile_id", str2);
        a2.put("number_of_steps", Integer.valueOf(i));
        this.c.a("bc_left_challenge_after_start", a2);
    }

    @DexIgnore
    public final void j(Intent intent) {
        String str;
        String stringExtra;
        String str2 = "";
        if (intent == null || (str = intent.getStringExtra("challenge_id_extra")) == null) {
            str = str2;
        }
        if (!(intent == null || (stringExtra = intent.getStringExtra("current_user_id_extra")) == null)) {
            str2 = stringExtra;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("challenge_id", str);
        hashMap.put("profile_id", str2);
        this.c.a("bc_left_challenge_before_start", hashMap);
    }

    @DexIgnore
    public final void k(Intent intent) {
        String str;
        String stringExtra;
        String str2 = "";
        if (intent == null || (str = intent.getStringExtra("current_user_id_extra")) == null) {
            str = str2;
        }
        if (!(intent == null || (stringExtra = intent.getStringExtra("friend_id_extra")) == null)) {
            str2 = stringExtra;
        }
        HashMap<String, Object> a2 = a();
        a2.put("profile_id", str);
        a2.put("to_profile_id", str2);
        this.c.a("bc_reject_friend_request", a2);
    }

    @DexIgnore
    public final void l(Intent intent) {
        String str;
        String stringExtra;
        String str2 = "";
        if (intent == null || (str = intent.getStringExtra("current_user_id_extra")) == null) {
            str = str2;
        }
        if (!(intent == null || (stringExtra = intent.getStringExtra("friend_id_extra")) == null)) {
            str2 = stringExtra;
        }
        HashMap<String, Object> a2 = a();
        a2.put("profile_id", str);
        a2.put("to_profile_id", str2);
        this.c.a("bc_send_friend_request", a2);
    }

    @DexIgnore
    public final void a(Context context) {
        ee7.b(context, "context");
        qe.a(context).a(this.b, b());
    }

    @DexIgnore
    public final void a(String str, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local.e(str2, "logEvent - action: " + str);
        switch (str.hashCode()) {
            case -2105630900:
                if (str.equals("bc_join_challenge")) {
                    h(intent);
                    return;
                }
                return;
            case -2059382039:
                if (str.equals("bc_left_challenge_after_start")) {
                    i(intent);
                    return;
                }
                return;
            case -1618588386:
                if (str.equals("bc_create_challenge")) {
                    d(intent);
                    return;
                }
                return;
            case -1436459621:
                if (str.equals("bc_complete_challenge")) {
                    c(intent);
                    return;
                }
                return;
            case 411008118:
                if (str.equals("bc_look_for_friend")) {
                    f(intent);
                    return;
                }
                return;
            case 528344632:
                if (str.equals("bc_left_challenge_before_start")) {
                    j(intent);
                    return;
                }
                return;
            case 567473916:
                if (str.equals("bc_create_social_profile")) {
                    e(intent);
                    return;
                }
                return;
            case 681203674:
                if (str.equals("bc_look_for_friend_challenge")) {
                    g(intent);
                    return;
                }
                return;
            case 691102855:
                if (str.equals("bc_accept_friend_request")) {
                    b(intent);
                    return;
                }
                return;
            case 695714346:
                if (str.equals("bc_accept_challenge")) {
                    a(intent);
                    return;
                }
                return;
            case 769307271:
                if (str.equals("bc_send_friend_request")) {
                    l(intent);
                    return;
                }
                return;
            case 1436927248:
                if (str.equals("bc_reject_friend_request")) {
                    k(intent);
                    return;
                }
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final IntentFilter b() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("bc_create_challenge");
        intentFilter.addAction("bc_challenge_a_friend");
        intentFilter.addAction("bc_accept_challenge");
        intentFilter.addAction("bc_join_challenge");
        intentFilter.addAction("bc_left_challenge_after_start");
        intentFilter.addAction("bc_left_challenge_before_start");
        intentFilter.addAction("bc_complete_challenge");
        intentFilter.addAction("bc_create_social_profile");
        intentFilter.addAction("bc_look_for_friend");
        intentFilter.addAction("bc_send_friend_request");
        intentFilter.addAction("bc_reject_friend_request");
        intentFilter.addAction("bc_accept_friend_request");
        intentFilter.addAction("bc_look_for_friend_challenge");
        return intentFilter;
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        HashMap<String, Object> a2 = a();
        a2.put("challenge_id", str);
        a2.put("profile_id", str2);
        a2.put("to_profile_id", str3);
        this.c.a("bc_challenge_a_friend", a2);
    }

    @DexIgnore
    public final void a(Intent intent) {
        String str;
        String stringExtra;
        String str2 = "";
        if (intent == null || (str = intent.getStringExtra("challenge_id_extra")) == null) {
            str = str2;
        }
        if (!(intent == null || (stringExtra = intent.getStringExtra("current_user_id_extra")) == null)) {
            str2 = stringExtra;
        }
        HashMap<String, Object> a2 = a();
        a2.put("challenge_id", str);
        a2.put("profile_id", str2);
        this.c.a("bc_accept_challenge", a2);
    }

    @DexIgnore
    public final void a(String str) {
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new b(this, str, null), 3, null);
    }

    @DexIgnore
    public final HashMap<String, Object> a() {
        HashMap<String, Object> hashMap = new HashMap<>();
        String a2 = this.c.a();
        if (a2 == null) {
            a2 = "";
        }
        hashMap.put("user_id", a2);
        hashMap.put(Constants.SERIAL_NUMBER, PortfolioApp.g0.c().c());
        return hashMap;
    }
}
