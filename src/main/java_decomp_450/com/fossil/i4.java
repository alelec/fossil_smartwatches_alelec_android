package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.m4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i4 implements k4 {
    @DexIgnore
    public /* final */ RectF a; // = new RectF();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements m4.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.m4.a
        public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
            float f2 = 2.0f * f;
            float width = (rectF.width() - f2) - 1.0f;
            float height = (rectF.height() - f2) - 1.0f;
            if (f >= 1.0f) {
                float f3 = f + 0.5f;
                float f4 = -f3;
                i4.this.a.set(f4, f4, f3, f3);
                int save = canvas.save();
                canvas.translate(rectF.left + f3, rectF.top + f3);
                canvas.drawArc(i4.this.a, 180.0f, 90.0f, true, paint);
                canvas.translate(width, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas.rotate(90.0f);
                canvas.drawArc(i4.this.a, 180.0f, 90.0f, true, paint);
                canvas.translate(height, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas.rotate(90.0f);
                canvas.drawArc(i4.this.a, 180.0f, 90.0f, true, paint);
                canvas.translate(width, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas.rotate(90.0f);
                canvas.drawArc(i4.this.a, 180.0f, 90.0f, true, paint);
                canvas.restoreToCount(save);
                float f5 = rectF.top;
                canvas.drawRect((rectF.left + f3) - 1.0f, f5, (rectF.right - f3) + 1.0f, f5 + f3, paint);
                float f6 = rectF.bottom;
                canvas.drawRect((rectF.left + f3) - 1.0f, f6 - f3, (rectF.right - f3) + 1.0f, f6, paint);
            }
            canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom - f, paint);
        }
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void a() {
        m4.r = new a();
    }

    @DexIgnore
    @Override // com.fossil.k4
    public float b(j4 j4Var) {
        return j(j4Var).c();
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void c(j4 j4Var) {
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void c(j4 j4Var, float f) {
        j(j4Var).b(f);
        f(j4Var);
    }

    @DexIgnore
    @Override // com.fossil.k4
    public float d(j4 j4Var) {
        return j(j4Var).d();
    }

    @DexIgnore
    @Override // com.fossil.k4
    public ColorStateList e(j4 j4Var) {
        return j(j4Var).b();
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void f(j4 j4Var) {
        Rect rect = new Rect();
        j(j4Var).b(rect);
        j4Var.a((int) Math.ceil((double) h(j4Var)), (int) Math.ceil((double) g(j4Var)));
        j4Var.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    @DexIgnore
    @Override // com.fossil.k4
    public float g(j4 j4Var) {
        return j(j4Var).e();
    }

    @DexIgnore
    @Override // com.fossil.k4
    public float h(j4 j4Var) {
        return j(j4Var).f();
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void i(j4 j4Var) {
        j(j4Var).a(j4Var.a());
        f(j4Var);
    }

    @DexIgnore
    public final m4 j(j4 j4Var) {
        return (m4) j4Var.c();
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void a(j4 j4Var, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        m4 a2 = a(context, colorStateList, f, f2, f3);
        a2.a(j4Var.a());
        j4Var.a(a2);
        f(j4Var);
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void b(j4 j4Var, float f) {
        j(j4Var).c(f);
    }

    @DexIgnore
    public final m4 a(Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        return new m4(context.getResources(), colorStateList, f, f2, f3);
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void a(j4 j4Var, ColorStateList colorStateList) {
        j(j4Var).b(colorStateList);
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void a(j4 j4Var, float f) {
        j(j4Var).a(f);
        f(j4Var);
    }

    @DexIgnore
    @Override // com.fossil.k4
    public float a(j4 j4Var) {
        return j(j4Var).g();
    }
}
