package com.fossil;

import android.text.TextUtils;
import com.facebook.share.internal.VideoUploader;
import com.fossil.ql4;
import com.fossil.qy5;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.AppType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zy5 extends ty5 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public /* final */ List<bt5> e; // = new ArrayList();
    @DexIgnore
    public /* final */ uy5 f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ ArrayList<bt5> h;
    @DexIgnore
    public /* final */ rl4 i;
    @DexIgnore
    public /* final */ qy5 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return zy5.k;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$registerContactObserver$1", f = "NotificationHybridEveryonePresenter.kt", l = {134}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ zy5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$registerContactObserver$1$1", f = "NotificationHybridEveryonePresenter.kt", l = {135}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    PortfolioApp c = PortfolioApp.g0.c();
                    this.L$0 = yi7;
                    this.label = 1;
                    if (c.k(this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(zy5 zy5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = zy5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.b();
                a aVar = new a(null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(a3, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1", f = "NotificationHybridEveryonePresenter.kt", l = {39}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ zy5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1$1", f = "NotificationHybridEveryonePresenter.kt", l = {40}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    PortfolioApp c = PortfolioApp.g0.c();
                    this.L$0 = yi7;
                    this.label = 1;
                    if (c.k(this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements ql4.d<qy5.d, qy5.b> {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(qy5.d dVar) {
                ee7.b(dVar, "successResponse");
                FLogger.INSTANCE.getLocal().d(zy5.l.a(), "GetAllContactGroup onSuccess");
                for (T t : dVar.a()) {
                    for (Contact contact : t.getContacts()) {
                        ee7.a((Object) contact, "contact");
                        if (contact.getContactId() == -100 || contact.getContactId() == -200) {
                            bt5 bt5 = new bt5(contact, null, 2, null);
                            bt5.setAdded(true);
                            Contact contact2 = bt5.getContact();
                            if (contact2 != null) {
                                contact2.setDbRowId(contact.getDbRowId());
                                contact2.setUseSms(contact.isUseSms());
                                contact2.setUseCall(contact.isUseCall());
                            }
                            bt5.setCurrentHandGroup(t.getHour());
                            List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                            ee7.a((Object) phoneNumbers, "contact.phoneNumbers");
                            if (!phoneNumbers.isEmpty()) {
                                PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                                ee7.a((Object) phoneNumber, "contact.phoneNumbers[0]");
                                String number = phoneNumber.getNumber();
                                if (!TextUtils.isEmpty(number)) {
                                    bt5.setHasPhoneNumber(true);
                                    bt5.setPhoneNumber(number);
                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                    String a2 = zy5.l.a();
                                    local.d(a2, " filter selected contact, phoneNumber=" + number);
                                }
                            }
                            Iterator it = this.a.this$0.h.iterator();
                            int i = 0;
                            while (true) {
                                if (!it.hasNext()) {
                                    i = -1;
                                    break;
                                }
                                Contact contact3 = ((bt5) it.next()).getContact();
                                if (contact3 != null && contact3.getContactId() == contact.getContactId()) {
                                    break;
                                }
                                i++;
                            }
                            if (i != -1) {
                                bt5.setCurrentHandGroup(this.a.this$0.g);
                                this.a.this$0.h.remove(i);
                            }
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String a3 = zy5.l.a();
                            local2.d(a3, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                            this.a.this$0.m().add(bt5);
                        }
                    }
                }
                if (!this.a.this$0.h.isEmpty()) {
                    for (bt5 bt52 : this.a.this$0.h) {
                        this.a.this$0.m().add(bt52);
                    }
                }
                this.a.this$0.f.b(this.a.this$0.m(), this.a.this$0.g);
            }

            @DexIgnore
            public void a(qy5.b bVar) {
                ee7.b(bVar, "errorResponse");
                FLogger.INSTANCE.getLocal().d(zy5.l.a(), "GetAllContactGroup onError");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(zy5 zy5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = zy5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (!PortfolioApp.g0.c().v().U()) {
                    ti7 a3 = this.this$0.b();
                    a aVar = new a(null);
                    this.L$0 = yi7;
                    this.label = 1;
                    if (vh7.a(a3, aVar, this) == a2) {
                        return a2;
                    }
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.m().isEmpty()) {
                this.this$0.i.a(this.this$0.j, null, new b(this));
            }
            return i97.a;
        }
    }

    /*
    static {
        String simpleName = zy5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationHybridEveryo\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public zy5(uy5 uy5, int i2, ArrayList<bt5> arrayList, rl4 rl4, qy5 qy5) {
        ee7.b(uy5, "mView");
        ee7.b(arrayList, "mContactWrappersSelected");
        ee7.b(rl4, "mUseCaseHandler");
        ee7.b(qy5, "mGetAllHybridContactGroups");
        this.f = uy5;
        this.g = i2;
        this.h = arrayList;
        this.i = rl4;
        this.j = qy5;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    @Override // com.fossil.ty5
    public int h() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.ty5
    public void i() {
        boolean z = true;
        if (!this.e.isEmpty()) {
            int size = this.e.size() - 1;
            while (true) {
                if (size < 0) {
                    z = false;
                    break;
                }
                Contact contact = this.e.get(size).getContact();
                if (contact == null || contact.getContactId() != -100) {
                    size--;
                } else if (this.e.get(size).getCurrentHandGroup() != this.g) {
                    this.f.a(this.e.get(size));
                } else {
                    this.e.remove(size);
                }
            }
            if (!z) {
                String str = AppType.ALL_CALLS.packageName;
                ee7.a((Object) str, "AppType.ALL_CALLS.packageName");
                this.e.add(a(-100, str));
            }
        } else {
            String str2 = AppType.ALL_CALLS.packageName;
            ee7.a((Object) str2, "AppType.ALL_CALLS.packageName");
            this.e.add(a(-100, str2));
        }
        this.f.b(this.e, this.g);
    }

    @DexIgnore
    @Override // com.fossil.ty5
    public void j() {
        boolean z = true;
        if (!this.e.isEmpty()) {
            int size = this.e.size() - 1;
            while (true) {
                if (size < 0) {
                    z = false;
                    break;
                }
                Contact contact = this.e.get(size).getContact();
                if (contact == null || contact.getContactId() != -200) {
                    size--;
                } else if (this.e.get(size).getCurrentHandGroup() != this.g) {
                    this.f.a(this.e.get(size));
                } else {
                    this.e.remove(size);
                }
            }
            if (!z) {
                String str = AppType.ALL_SMS.packageName;
                ee7.a((Object) str, "AppType.ALL_SMS.packageName");
                this.e.add(a(-200, str));
            }
        } else {
            String str2 = AppType.ALL_SMS.packageName;
            ee7.a((Object) str2, "AppType.ALL_SMS.packageName");
            this.e.add(a(-200, str2));
        }
        this.f.b(this.e, this.g);
    }

    @DexIgnore
    @Override // com.fossil.ty5
    public void k() {
        ArrayList<bt5> arrayList = new ArrayList<>();
        for (T t : this.e) {
            if (t.isAdded() && t.getCurrentHandGroup() == this.g) {
                arrayList.add(t);
            }
        }
        this.f.a(arrayList);
    }

    @DexIgnore
    @Override // com.fossil.ty5
    public void l() {
        if (!PortfolioApp.g0.c().v().U()) {
            ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
        }
    }

    @DexIgnore
    public final List<bt5> m() {
        return this.e;
    }

    @DexIgnore
    public void n() {
        this.f.a(this);
    }

    @DexIgnore
    @Override // com.fossil.ty5
    public void a(bt5 bt5) {
        ee7.b(bt5, "contactWrapper");
        for (T t : this.e) {
            Contact contact = t.getContact();
            Integer num = null;
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Contact contact2 = bt5.getContact();
            if (contact2 != null) {
                num = Integer.valueOf(contact2.getContactId());
            }
            if (ee7.a(valueOf, num)) {
                t.setCurrentHandGroup(this.g);
                t.setAdded(true);
            }
        }
        this.f.b(this.e, this.g);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(k, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        ik7 unused = xh7.b(e(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    public final bt5 a(int i2, String str) {
        Contact contact = new Contact();
        contact.setContactId(i2);
        contact.setFirstName(str);
        bt5 bt5 = new bt5(contact, null, 2, null);
        bt5.setHasPhoneNumber(true);
        bt5.setAdded(true);
        bt5.setCurrentHandGroup(this.g);
        if (i2 == -100) {
            Contact contact2 = bt5.getContact();
            if (contact2 != null) {
                contact2.setUseCall(true);
            }
            Contact contact3 = bt5.getContact();
            if (contact3 != null) {
                contact3.setUseSms(false);
            }
        } else {
            Contact contact4 = bt5.getContact();
            if (contact4 != null) {
                contact4.setUseCall(false);
            }
            Contact contact5 = bt5.getContact();
            if (contact5 != null) {
                contact5.setUseSms(true);
            }
        }
        return bt5;
    }
}
