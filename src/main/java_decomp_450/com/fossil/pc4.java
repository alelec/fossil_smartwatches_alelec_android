package com.fossil;

import android.os.Bundle;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pc4 {
    @DexIgnore
    public static /* final */ long a; // = TimeUnit.MINUTES.toMillis(3);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static n4<String, String> a(Bundle bundle) {
            n4<String, String> n4Var = new n4<>();
            for (String str : bundle.keySet()) {
                Object obj = bundle.get(str);
                if (obj instanceof String) {
                    String str2 = (String) obj;
                    if (!str.startsWith("google.") && !str.startsWith("gcm.") && !str.equals("from") && !str.equals("message_type") && !str.equals("collapse_key")) {
                        n4Var.put(str, str2);
                    }
                }
            }
            return n4Var;
        }
    }
}
