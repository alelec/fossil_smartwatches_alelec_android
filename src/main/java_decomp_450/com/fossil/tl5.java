package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tl5 extends fl4<b, d, c> {
    @DexIgnore
    public /* final */ PortfolioApp d;
    @DexIgnore
    public /* final */ GuestApiService e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            ee7.b(str, "deviceModel");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            ee7.b(str, "latestFwVersion");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase", f = "DownloadFirmwareByDeviceModelUsecase.kt", l = {36, 51}, m = "run")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tl5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(tl5 tl5, fb7 fb7) {
            super(fb7);
            this.this$0 = tl5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<Object>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$run$repoResponse$1", f = "DownloadFirmwareByDeviceModelUsecase.kt", l = {36}, m = "invokeSuspend")
    public static final class f extends zb7 implements gd7<fb7<? super fv7<ApiResponse<Firmware>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceModel;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ tl5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(tl5 tl5, String str, fb7 fb7) {
            super(1, fb7);
            this.this$0 = tl5;
            this.$deviceModel = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new f(this.this$0, this.$deviceModel, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ApiResponse<Firmware>>> fb7) {
            return ((f) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                GuestApiService b = this.this$0.e;
                String g = this.this$0.d.g();
                String str = this.$deviceModel;
                this.label = 1;
                obj = b.getFirmwares(g, str, "android", false, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public tl5(PortfolioApp portfolioApp, GuestApiService guestApiService) {
        ee7.b(portfolioApp, "mApp");
        ee7.b(guestApiService, "mGuestApiService");
        this.d = portfolioApp;
        this.e = guestApiService;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "DownloadFirmwareByDeviceModelUsecase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.tl5.b r18, com.fossil.fb7<java.lang.Object> r19) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            boolean r3 = r2 instanceof com.fossil.tl5.e
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.fossil.tl5$e r3 = (com.fossil.tl5.e) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.fossil.tl5$e r3 = new com.fossil.tl5$e
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 1
            java.lang.String r7 = "run - getFirmwares of deviceModel="
            r8 = 2
            java.lang.String r9 = "DownloadFirmwareByDeviceModelUsecase"
            r10 = 0
            if (r5 == 0) goto L_0x006e
            if (r5 == r6) goto L_0x0058
            if (r5 != r8) goto L_0x0050
            java.lang.Object r1 = r3.L$5
            com.portfolio.platform.data.model.Firmware r1 = (com.portfolio.platform.data.model.Firmware) r1
            java.lang.Object r4 = r3.L$4
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r4 = r3.L$3
            com.fossil.zi5 r4 = (com.fossil.zi5) r4
            java.lang.Object r4 = r3.L$2
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r4 = r3.L$1
            com.fossil.tl5$b r4 = (com.fossil.tl5.b) r4
            java.lang.Object r3 = r3.L$0
            com.fossil.tl5 r3 = (com.fossil.tl5) r3
            com.fossil.t87.a(r2)
            goto L_0x012e
        L_0x0050:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0058:
            java.lang.Object r1 = r3.L$2
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r5 = r3.L$1
            com.fossil.tl5$b r5 = (com.fossil.tl5.b) r5
            java.lang.Object r6 = r3.L$0
            com.fossil.tl5 r6 = (com.fossil.tl5) r6
            com.fossil.t87.a(r2)
            r16 = r2
            r2 = r1
            r1 = r5
            r5 = r16
            goto L_0x00c4
        L_0x006e:
            com.fossil.t87.a(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r5 = "run"
            r2.d(r9, r5)
            if (r1 != 0) goto L_0x008f
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = "run - requestValues is NULL!!!"
            r1.e(r9, r2)
            com.fossil.tl5$c r1 = new com.fossil.tl5$c
            r1.<init>()
            return r1
        L_0x008f:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r7)
            java.lang.String r11 = r18.a()
            r5.append(r11)
            java.lang.String r5 = r5.toString()
            r2.d(r9, r5)
            java.lang.String r2 = r18.a()
            com.fossil.tl5$f r5 = new com.fossil.tl5$f
            r5.<init>(r0, r2, r10)
            r3.L$0 = r0
            r3.L$1 = r1
            r3.L$2 = r2
            r3.label = r6
            java.lang.Object r5 = com.fossil.aj5.a(r5, r3)
            if (r5 != r4) goto L_0x00c3
            return r4
        L_0x00c3:
            r6 = r0
        L_0x00c4:
            com.fossil.zi5 r5 = (com.fossil.zi5) r5
            boolean r11 = r5 instanceof com.fossil.bj5
            if (r11 == 0) goto L_0x016e
            r11 = r5
            com.fossil.bj5 r11 = (com.fossil.bj5) r11
            java.lang.Object r11 = r11.a()
            com.portfolio.platform.data.source.remote.ApiResponse r11 = (com.portfolio.platform.data.source.remote.ApiResponse) r11
            if (r11 == 0) goto L_0x00da
            java.util.List r11 = r11.get_items()
            goto L_0x00db
        L_0x00da:
            r11 = r10
        L_0x00db:
            if (r11 == 0) goto L_0x010f
            java.util.Iterator r12 = r11.iterator()
            r13 = r10
        L_0x00e2:
            boolean r14 = r12.hasNext()
            if (r14 == 0) goto L_0x010e
            java.lang.Object r14 = r12.next()
            com.portfolio.platform.data.model.Firmware r14 = (com.portfolio.platform.data.model.Firmware) r14
            java.lang.String r15 = r14.getDeviceModel()
            boolean r15 = android.text.TextUtils.isEmpty(r15)
            if (r15 != 0) goto L_0x010a
            java.lang.String r15 = r14.getDeviceModel()
            java.lang.String r8 = "firmware.deviceModel"
            com.fossil.ee7.a(r15, r8)
            r8 = 0
            r0 = 2
            boolean r8 = com.fossil.nh7.a(r15, r2, r8, r0, r10)
            if (r8 == 0) goto L_0x010a
            r13 = r14
        L_0x010a:
            r8 = 2
            r0 = r17
            goto L_0x00e2
        L_0x010e:
            r10 = r13
        L_0x010f:
            if (r10 == 0) goto L_0x014b
            com.fossil.yw6$a r0 = com.fossil.yw6.h
            com.fossil.yw6 r0 = r0.a()
            r3.L$0 = r6
            r3.L$1 = r1
            r3.L$2 = r2
            r3.L$3 = r5
            r3.L$4 = r11
            r3.L$5 = r10
            r1 = 2
            r3.label = r1
            java.lang.Object r2 = r0.a(r10, r3)
            if (r2 != r4) goto L_0x012d
            return r4
        L_0x012d:
            r1 = r10
        L_0x012e:
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r0 = r2.booleanValue()
            if (r0 == 0) goto L_0x0145
            com.fossil.tl5$d r0 = new com.fossil.tl5$d
            java.lang.String r1 = r1.getVersionNumber()
            java.lang.String r2 = "latestFirmware.versionNumber"
            com.fossil.ee7.a(r1, r2)
            r0.<init>(r1)
            goto L_0x016d
        L_0x0145:
            com.fossil.tl5$c r0 = new com.fossil.tl5$c
            r0.<init>()
            goto L_0x016d
        L_0x014b:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r7)
            r1.append(r2)
            java.lang.String r2 = " not found on server"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.e(r9, r1)
            com.fossil.tl5$c r0 = new com.fossil.tl5$c
            r0.<init>()
        L_0x016d:
            return r0
        L_0x016e:
            boolean r0 = r5 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x01b8
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r7)
            r1.append(r2)
            java.lang.String r2 = " not found on server code="
            r1.append(r2)
            com.fossil.yi5 r5 = (com.fossil.yi5) r5
            com.portfolio.platform.data.model.ServerError r2 = r5.c()
            if (r2 == 0) goto L_0x0195
            java.lang.Integer r2 = r2.getCode()
            goto L_0x0196
        L_0x0195:
            r2 = r10
        L_0x0196:
            r1.append(r2)
            java.lang.String r2 = ", message="
            r1.append(r2)
            com.portfolio.platform.data.model.ServerError r2 = r5.c()
            if (r2 == 0) goto L_0x01a8
            java.lang.String r10 = r2.getMessage()
        L_0x01a8:
            r1.append(r10)
            java.lang.String r1 = r1.toString()
            r0.e(r9, r1)
            com.fossil.tl5$c r0 = new com.fossil.tl5$c
            r0.<init>()
            return r0
        L_0x01b8:
            com.fossil.p87 r0 = new com.fossil.p87
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tl5.a(com.fossil.tl5$b, com.fossil.fb7):java.lang.Object");
    }
}
