package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.fossil.cy6;
import com.fossil.ym6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vm6 extends go5 implements gy6, cy6.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static String p;
    @DexIgnore
    public static String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public ym6 g;
    @DexIgnore
    public qw6<yz4> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return vm6.p;
        }

        @DexIgnore
        public final String b() {
            return vm6.q;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<ym6.b> {
        @DexIgnore
        public /* final */ /* synthetic */ vm6 a;

        @DexIgnore
        public b(vm6 vm6) {
            this.a = vm6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ym6.b bVar) {
            if (bVar != null) {
                Integer a2 = bVar.a();
                if (a2 != null) {
                    this.a.o(a2.intValue());
                }
                Integer c = bVar.c();
                if (c != null) {
                    this.a.q(c.intValue());
                }
                Integer b = bVar.b();
                if (b != null) {
                    this.a.n(b.intValue());
                }
                Integer d = bVar.d();
                if (d != null) {
                    this.a.p(d.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vm6 a;

        @DexIgnore
        public c(vm6 vm6) {
            this.a = vm6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, 101);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vm6 a;

        @DexIgnore
        public d(vm6 vm6) {
            this.a = vm6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, 102);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vm6 a;

        @DexIgnore
        public e(vm6 vm6) {
            this.a = vm6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.i(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = vm6.class.getSimpleName();
        ee7.a((Object) simpleName, "CustomizeTextFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        FLogger.INSTANCE.getLocal().d(j, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363307) {
            ym6 ym6 = this.g;
            if (ym6 != null) {
                ym6.a(cn6.q.a(), p, q);
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.gy6
    public void b(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        we7 we7 = we7.a;
        String format = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(i3 & 16777215)}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        ym6 ym6 = this.g;
        if (ym6 != null) {
            ym6.a(i2, Color.parseColor(format));
            if (i2 == 101) {
                p = format;
            } else if (i2 == 102) {
                q = format;
            }
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.gy6
    public void j(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    public final void n(int i2) {
        qw6<yz4> qw6 = this.h;
        if (qw6 != null) {
            yz4 a2 = qw6.a();
            if (a2 != null) {
                a2.v.setTextColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void o(int i2) {
        qw6<yz4> qw6 = this.h;
        if (qw6 != null) {
            yz4 a2 = qw6.a();
            if (a2 != null) {
                a2.z.setBackgroundColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        yz4 yz4 = (yz4) qb.a(LayoutInflater.from(getContext()), 2131558538, null, false, a1());
        PortfolioApp.g0.c().f().a(new xm6()).a(this);
        rj4 rj4 = this.f;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(ym6.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026extViewModel::class.java)");
            ym6 ym6 = (ym6) a2;
            this.g = ym6;
            if (ym6 != null) {
                ym6.b().a(getViewLifecycleOwner(), new b(this));
                ym6 ym62 = this.g;
                if (ym62 != null) {
                    ym62.c();
                    this.h = new qw6<>(this, yz4);
                    ee7.a((Object) yz4, "binding");
                    return yz4.d();
                }
                ee7.d("mViewModel");
                throw null;
            }
            ee7.d("mViewModel");
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(j, "onDestroy");
        p = null;
        q = null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(j, "onResume");
        ym6 ym6 = this.g;
        if (ym6 != null) {
            ym6.c();
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<yz4> qw6 = this.h;
        if (qw6 != null) {
            yz4 a2 = qw6.a();
            if (a2 != null) {
                a2.t.setOnClickListener(new c(this));
                a2.u.setOnClickListener(new d(this));
                a2.s.setOnClickListener(new e(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void p(int i2) {
        qw6<yz4> qw6 = this.h;
        if (qw6 != null) {
            yz4 a2 = qw6.a();
            if (a2 != null) {
                a2.w.setTextColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void q(int i2) {
        qw6<yz4> qw6 = this.h;
        if (qw6 != null) {
            yz4 a2 = qw6.a();
            if (a2 != null) {
                a2.A.setBackgroundColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }
}
