package com.fossil;

import android.os.CountDownTimer;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface fn4 extends zt4 {
    @DexIgnore
    void a(long j, long j2);

    @DexIgnore
    void b(long j, long j2);

    @DexIgnore
    long getEndTime();

    @DexIgnore
    long getInterval();

    @DexIgnore
    String getTag();

    @DexIgnore
    CountDownTimer getTimer();

    @DexIgnore
    TimerViewObserver getTimerViewObserver();

    @DexIgnore
    long getTotalMillisecond();

    @DexIgnore
    void onDone();

    @DexIgnore
    void setTimer(CountDownTimer countDownTimer);

    @DexIgnore
    void setTimerViewObserver(TimerViewObserver timerViewObserver);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.fn4$a$a")
        /* renamed from: com.fossil.fn4$a$a  reason: collision with other inner class name */
        public static final class CountDownTimerC0065a extends CountDownTimer {
            @DexIgnore
            public /* final */ /* synthetic */ fn4 a;
            @DexIgnore
            public /* final */ /* synthetic */ long b;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public CountDownTimerC0065a(fn4 fn4, long j, long j2, long j3, long j4) {
                super(j3, j4);
                this.a = fn4;
                this.b = j;
            }

            @DexIgnore
            public void onFinish() {
                this.a.onDone();
            }

            @DexIgnore
            public void onTick(long j) {
                this.a.a(j, this.b);
            }
        }

        @DexIgnore
        public static void a(fn4 fn4, TimerViewObserver timerViewObserver, int i) {
            if (fn4.getTimerViewObserver() == null) {
                fn4.setTimerViewObserver(timerViewObserver);
                TimerViewObserver timerViewObserver2 = fn4.getTimerViewObserver();
                if (timerViewObserver2 != null) {
                    timerViewObserver2.a(fn4, i);
                }
            }
        }

        @DexIgnore
        public static void b(fn4 fn4) {
            FLogger.INSTANCE.getLocal().e(fn4.getTag(), "onResume");
            fn4.b(fn4.getTotalMillisecond(), fn4.getEndTime() - vt4.a.b());
        }

        @DexIgnore
        public static void a(fn4 fn4, long j, long j2) {
            CountDownTimer timer = fn4.getTimer();
            if (timer != null) {
                timer.cancel();
            }
            fn4.setTimer(new CountDownTimerC0065a(fn4, j, j2, j2, fn4.getInterval()));
            CountDownTimer timer2 = fn4.getTimer();
            if (timer2 != null) {
                timer2.start();
            }
        }

        @DexIgnore
        public static void a(fn4 fn4) {
            FLogger.INSTANCE.getLocal().e(fn4.getTag(), "onPause");
            CountDownTimer timer = fn4.getTimer();
            if (timer != null) {
                timer.cancel();
            }
        }
    }
}
