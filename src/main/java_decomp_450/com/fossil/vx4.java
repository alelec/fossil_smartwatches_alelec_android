package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vx4 extends ux4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i x;
    @DexIgnore
    public static /* final */ SparseIntArray y;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public long w;

    /*
    static {
        ViewDataBinding.i iVar = new ViewDataBinding.i(6);
        x = iVar;
        iVar.a(1, new String[]{"item_calories_workout_day", "item_calories_workout_day"}, new int[]{2, 3}, new int[]{2131558664, 2131558664});
        SparseIntArray sparseIntArray = new SparseIntArray();
        y = sparseIntArray;
        sparseIntArray.put(2131362180, 4);
        y.put(2131362330, 5);
    }
    */

    @DexIgnore
    public vx4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 6, x, y));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
        ViewDataBinding.d(((ux4) this).s);
        ViewDataBinding.d(((ux4) this).t);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (((com.fossil.ux4) r6).t.e() == false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (((com.fossil.ux4) r6).s.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean e() {
        /*
            r6 = this;
            monitor-enter(r6)
            long r0 = r6.w     // Catch:{ all -> 0x0021 }
            r2 = 0
            r4 = 1
            int r5 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r5 == 0) goto L_0x000c
            monitor-exit(r6)     // Catch:{ all -> 0x0021 }
            return r4
        L_0x000c:
            monitor-exit(r6)     // Catch:{ all -> 0x0021 }
            com.fossil.s85 r0 = r6.s
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x0016
            return r4
        L_0x0016:
            com.fossil.s85 r0 = r6.t
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x001f
            return r4
        L_0x001f:
            r0 = 0
            return r0
        L_0x0021:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vx4.e():boolean");
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.w = 4;
        }
        ((ux4) this).s.f();
        ((ux4) this).t.f();
        g();
    }

    @DexIgnore
    public vx4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 2, (OverviewDayChart) objArr[4], (FlexibleTextView) objArr[5], (s85) objArr[2], (s85) objArr[3], (LinearLayout) objArr[1]);
        this.w = -1;
        ((ux4) this).u.setTag(null);
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.v = constraintLayout;
        constraintLayout.setTag(null);
        a(view);
        f();
    }
}
