package com.fossil;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e27 {
    @DexIgnore
    public String a; // = null;
    @DexIgnore
    public String b; // = null;
    @DexIgnore
    public String c; // = "0";
    @DexIgnore
    public long d; // = 0;

    @DexIgnore
    public static e27 a(String str) {
        e27 e27 = new e27();
        if (j27.a(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (!jSONObject.isNull("ui")) {
                    e27.a = jSONObject.getString("ui");
                }
                if (!jSONObject.isNull("mc")) {
                    e27.b = jSONObject.getString("mc");
                }
                if (!jSONObject.isNull("mid")) {
                    e27.c = jSONObject.getString("mid");
                }
                if (!jSONObject.isNull("ts")) {
                    e27.d = jSONObject.getLong("ts");
                }
            } catch (JSONException e) {
                Log.w("MID", e);
            }
        }
        return e27;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            j27.a(jSONObject, "ui", this.a);
            j27.a(jSONObject, "mc", this.b);
            j27.a(jSONObject, "mid", this.c);
            jSONObject.put("ts", this.d);
        } catch (JSONException e) {
            Log.w("MID", e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final String toString() {
        return b().toString();
    }
}
