package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.internal.BoltsMeasurementEventListener;
import com.fossil.rp2;
import com.fossil.tp2;
import com.fossil.vp2;
import com.fossil.zp2;
import com.misfit.frameworks.common.constants.Constants;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fm3 extends yl3 {
    @DexIgnore
    public fm3(xl3 xl3) {
        super(xl3);
    }

    @DexIgnore
    public static tp2 b(rp2 rp2, String str) {
        for (tp2 tp2 : rp2.zza()) {
            if (tp2.p().equals(str)) {
                return tp2;
            }
        }
        return null;
    }

    @DexIgnore
    public final void a(zp2.a aVar, Object obj) {
        a72.a(obj);
        aVar.zza();
        aVar.o();
        aVar.p();
        if (obj instanceof String) {
            aVar.b((String) obj);
        } else if (obj instanceof Long) {
            aVar.b(((Long) obj).longValue());
        } else if (obj instanceof Double) {
            aVar.a(((Double) obj).doubleValue());
        } else {
            e().t().a("Ignoring invalid (type) user attribute value", obj);
        }
    }

    @DexIgnore
    public final byte[] c(byte[] bArr) throws IOException {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.close();
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            e().t().a("Failed to gzip content", e);
            throw e;
        }
    }

    @DexIgnore
    @Override // com.fossil.yl3
    public final boolean s() {
        return false;
    }

    @DexIgnore
    public final List<Integer> t() {
        Map<String, String> a = wb3.a(((vl3) this).b.f());
        if (a == null || a.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int intValue = wb3.P.a(null).intValue();
        Iterator<Map.Entry<String, String>> it = a.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Map.Entry<String, String> next = it.next();
            if (next.getKey().startsWith("measurement.id.")) {
                try {
                    int parseInt = Integer.parseInt(next.getValue());
                    if (parseInt != 0) {
                        arrayList.add(Integer.valueOf(parseInt));
                        if (arrayList.size() >= intValue) {
                            e().w().a("Too many experiment IDs. Number of IDs", Integer.valueOf(arrayList.size()));
                            break;
                        }
                    } else {
                        continue;
                    }
                } catch (NumberFormatException e) {
                    e().w().a("Experiment ID NumberFormatException", e);
                }
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        return arrayList;
    }

    @DexIgnore
    public final byte[] b(byte[] bArr) throws IOException {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr2 = new byte[1024];
            while (true) {
                int read = gZIPInputStream.read(bArr2);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr2, 0, read);
                } else {
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            e().t().a("Failed to ungzip content", e);
            throw e;
        }
    }

    @DexIgnore
    public final void a(tp2.a aVar, Object obj) {
        a72.a(obj);
        aVar.zza();
        aVar.o();
        aVar.p();
        aVar.zze();
        if (obj instanceof String) {
            aVar.b((String) obj);
        } else if (obj instanceof Long) {
            aVar.a(((Long) obj).longValue());
        } else if (obj instanceof Double) {
            aVar.a(((Double) obj).doubleValue());
        } else if (!p03.a() || !l().a(wb3.G0) || !(obj instanceof Bundle[])) {
            e().t().a("Ignoring invalid (type) event param value", obj);
        } else {
            aVar.a(a((Bundle[]) obj));
        }
    }

    @DexIgnore
    public final Object a(rp2 rp2, String str) {
        tp2 b = b(rp2, str);
        if (b == null) {
            return null;
        }
        if (b.q()) {
            return b.r();
        }
        if (b.s()) {
            return Long.valueOf(b.t());
        }
        if (b.w()) {
            return Double.valueOf(b.x());
        }
        if (!p03.a() || !l().a(wb3.G0) || b.z() <= 0) {
            return null;
        }
        List<tp2> y = b.y();
        ArrayList arrayList = new ArrayList();
        for (tp2 tp2 : y) {
            if (tp2 != null) {
                Bundle bundle = new Bundle();
                for (tp2 tp22 : tp2.y()) {
                    if (tp22.q()) {
                        bundle.putString(tp22.p(), tp22.r());
                    } else if (tp22.s()) {
                        bundle.putLong(tp22.p(), tp22.t());
                    } else if (tp22.w()) {
                        bundle.putDouble(tp22.p(), tp22.x());
                    }
                }
                if (!bundle.isEmpty()) {
                    arrayList.add(bundle);
                }
            }
        }
        return (Bundle[]) arrayList.toArray(new Bundle[arrayList.size()]);
    }

    @DexIgnore
    public final rp2 a(rb3 rb3) {
        rp2.a z = rp2.z();
        z.b(rb3.e);
        Iterator<String> it = rb3.f.iterator();
        while (it.hasNext()) {
            String next = it.next();
            tp2.a F = tp2.F();
            F.a(next);
            a(F, rb3.f.b(next));
            z.a(F);
        }
        return (rp2) ((bw2) z.g());
    }

    @DexIgnore
    public final void a(rp2.a aVar, String str, Object obj) {
        List<tp2> zza = aVar.zza();
        int i = 0;
        while (true) {
            if (i >= zza.size()) {
                i = -1;
                break;
            } else if (str.equals(zza.get(i).p())) {
                break;
            } else {
                i++;
            }
        }
        tp2.a F = tp2.F();
        F.a(str);
        if (obj instanceof Long) {
            F.a(((Long) obj).longValue());
        } else if (obj instanceof String) {
            F.b((String) obj);
        } else if (obj instanceof Double) {
            F.a(((Double) obj).doubleValue());
        } else if (p03.a() && l().a(wb3.G0) && (obj instanceof Bundle[])) {
            F.a(a((Bundle[]) obj));
        }
        if (i >= 0) {
            aVar.a(i, F);
        } else {
            aVar.a(F);
        }
    }

    @DexIgnore
    public final String a(up2 up2) {
        if (up2 == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nbatch {\n");
        for (vp2 vp2 : up2.zza()) {
            if (vp2 != null) {
                a(sb, 1);
                sb.append("bundle {\n");
                if (vp2.zza()) {
                    a(sb, 1, "protocol_version", Integer.valueOf(vp2.P()));
                }
                a(sb, 1, "platform", vp2.r0());
                if (vp2.y0()) {
                    a(sb, 1, "gmp_version", Long.valueOf(vp2.p()));
                }
                if (vp2.q()) {
                    a(sb, 1, "uploading_gmp_version", Long.valueOf(vp2.r()));
                }
                if (vp2.S()) {
                    a(sb, 1, "dynamite_version", Long.valueOf(vp2.T()));
                }
                if (vp2.J()) {
                    a(sb, 1, "config_version", Long.valueOf(vp2.K()));
                }
                a(sb, 1, "gmp_app_id", vp2.B());
                a(sb, 1, "admob_app_id", vp2.R());
                a(sb, 1, "app_id", vp2.w0());
                a(sb, 1, "app_version", vp2.x0());
                if (vp2.G()) {
                    a(sb, 1, "app_version_major", Integer.valueOf(vp2.H()));
                }
                a(sb, 1, "firebase_instance_id", vp2.F());
                if (vp2.w()) {
                    a(sb, 1, "dev_cert_hash", Long.valueOf(vp2.x()));
                }
                a(sb, 1, "app_store", vp2.v0());
                if (vp2.h0()) {
                    a(sb, 1, "upload_timestamp_millis", Long.valueOf(vp2.i0()));
                }
                if (vp2.j0()) {
                    a(sb, 1, "start_timestamp_millis", Long.valueOf(vp2.k0()));
                }
                if (vp2.l0()) {
                    a(sb, 1, "end_timestamp_millis", Long.valueOf(vp2.m0()));
                }
                if (vp2.n0()) {
                    a(sb, 1, "previous_bundle_start_timestamp_millis", Long.valueOf(vp2.o0()));
                }
                if (vp2.p0()) {
                    a(sb, 1, "previous_bundle_end_timestamp_millis", Long.valueOf(vp2.q0()));
                }
                a(sb, 1, "app_instance_id", vp2.v());
                a(sb, 1, "resettable_device_id", vp2.s());
                a(sb, 1, "device_id", vp2.I());
                a(sb, 1, "ds_id", vp2.N());
                if (vp2.t()) {
                    a(sb, 1, "limited_ad_tracking", Boolean.valueOf(vp2.u()));
                }
                a(sb, 1, Constants.OS_VERSION, vp2.s0());
                a(sb, 1, "device_model", vp2.t0());
                a(sb, 1, "user_default_language", vp2.u0());
                if (vp2.b()) {
                    a(sb, 1, "time_zone_offset_minutes", Integer.valueOf(vp2.g()));
                }
                if (vp2.y()) {
                    a(sb, 1, "bundle_sequential_index", Integer.valueOf(vp2.z()));
                }
                if (vp2.C()) {
                    a(sb, 1, "service_upload", Boolean.valueOf(vp2.D()));
                }
                a(sb, 1, "health_monitor", vp2.A());
                if (!l().a(wb3.M0) && vp2.L() && vp2.M() != 0) {
                    a(sb, 1, "android_id", Long.valueOf(vp2.M()));
                }
                if (vp2.O()) {
                    a(sb, 1, "retry_counter", Integer.valueOf(vp2.Q()));
                }
                List<zp2> f0 = vp2.f0();
                if (f0 != null) {
                    for (zp2 zp2 : f0) {
                        if (zp2 != null) {
                            a(sb, 2);
                            sb.append("user_property {\n");
                            Double d = null;
                            a(sb, 2, "set_timestamp_millis", zp2.zza() ? Long.valueOf(zp2.p()) : null);
                            a(sb, 2, "name", i().c(zp2.q()));
                            a(sb, 2, "string_value", zp2.s());
                            a(sb, 2, "int_value", zp2.t() ? Long.valueOf(zp2.u()) : null);
                            if (zp2.v()) {
                                d = Double.valueOf(zp2.w());
                            }
                            a(sb, 2, "double_value", d);
                            a(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                List<pp2> E = vp2.E();
                String w0 = vp2.w0();
                if (E != null) {
                    for (pp2 pp2 : E) {
                        if (pp2 != null) {
                            a(sb, 2);
                            sb.append("audience_membership {\n");
                            if (pp2.zza()) {
                                a(sb, 2, "audience_id", Integer.valueOf(pp2.p()));
                            }
                            if (pp2.t()) {
                                a(sb, 2, "new_audience", Boolean.valueOf(pp2.u()));
                            }
                            a(sb, 2, "current_data", pp2.q(), w0);
                            if (pp2.r()) {
                                a(sb, 2, "previous_data", pp2.s(), w0);
                            }
                            a(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                List<rp2> d0 = vp2.d0();
                if (d0 != null) {
                    for (rp2 rp2 : d0) {
                        if (rp2 != null) {
                            a(sb, 2);
                            sb.append("event {\n");
                            a(sb, 2, "name", i().a(rp2.q()));
                            if (rp2.r()) {
                                a(sb, 2, "timestamp_millis", Long.valueOf(rp2.s()));
                            }
                            if (rp2.t()) {
                                a(sb, 2, "previous_timestamp_millis", Long.valueOf(rp2.u()));
                            }
                            if (rp2.v()) {
                                a(sb, 2, "count", Integer.valueOf(rp2.w()));
                            }
                            if (rp2.p() != 0) {
                                a(sb, 2, rp2.zza());
                            }
                            a(sb, 2);
                            sb.append("}\n");
                        }
                    }
                }
                a(sb, 1);
                sb.append("}\n");
            }
        }
        sb.append("}\n");
        return sb.toString();
    }

    @DexIgnore
    public final String a(ap2 ap2) {
        if (ap2 == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nevent_filter {\n");
        if (ap2.zza()) {
            a(sb, 0, "filter_id", Integer.valueOf(ap2.p()));
        }
        a(sb, 0, BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, i().a(ap2.q()));
        String a = a(ap2.v(), ap2.w(), ap2.y());
        if (!a.isEmpty()) {
            a(sb, 0, "filter_type", a);
        }
        if (ap2.t()) {
            a(sb, 1, "event_count_filter", ap2.u());
        }
        if (ap2.s() > 0) {
            sb.append("  filters {\n");
            for (bp2 bp2 : ap2.r()) {
                a(sb, 2, bp2);
            }
        }
        a(sb, 1);
        sb.append("}\n}\n");
        return sb.toString();
    }

    @DexIgnore
    public final String a(dp2 dp2) {
        if (dp2 == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nproperty_filter {\n");
        if (dp2.zza()) {
            a(sb, 0, "filter_id", Integer.valueOf(dp2.p()));
        }
        a(sb, 0, "property_name", i().c(dp2.q()));
        String a = a(dp2.s(), dp2.t(), dp2.v());
        if (!a.isEmpty()) {
            a(sb, 0, "filter_type", a);
        }
        a(sb, 1, dp2.r());
        sb.append("}\n");
        return sb.toString();
    }

    @DexIgnore
    public static String a(boolean z, boolean z2, boolean z3) {
        StringBuilder sb = new StringBuilder();
        if (z) {
            sb.append("Dynamic ");
        }
        if (z2) {
            sb.append("Sequence ");
        }
        if (z3) {
            sb.append("Session-Scoped ");
        }
        return sb.toString();
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i, List<tp2> list) {
        if (list != null) {
            int i2 = i + 1;
            for (tp2 tp2 : list) {
                if (tp2 != null) {
                    a(sb, i2);
                    sb.append("param {\n");
                    Double d = null;
                    if (!p03.a() || !l().a(wb3.E0)) {
                        a(sb, i2, "name", i().b(tp2.p()));
                        a(sb, i2, "string_value", tp2.r());
                        a(sb, i2, "int_value", tp2.s() ? Long.valueOf(tp2.t()) : null);
                        if (tp2.w()) {
                            d = Double.valueOf(tp2.x());
                        }
                        a(sb, i2, "double_value", d);
                    } else {
                        a(sb, i2, "name", tp2.zza() ? i().b(tp2.p()) : null);
                        a(sb, i2, "string_value", tp2.q() ? tp2.r() : null);
                        a(sb, i2, "int_value", tp2.s() ? Long.valueOf(tp2.t()) : null);
                        if (tp2.w()) {
                            d = Double.valueOf(tp2.x());
                        }
                        a(sb, i2, "double_value", d);
                        if (tp2.z() > 0) {
                            a(sb, i2, tp2.y());
                        }
                    }
                    a(sb, i2);
                    sb.append("}\n");
                }
            }
        }
    }

    @DexIgnore
    public static void a(StringBuilder sb, int i, String str, xp2 xp2, String str2) {
        if (xp2 != null) {
            a(sb, 3);
            sb.append(str);
            sb.append(" {\n");
            if (xp2.r() != 0) {
                a(sb, 4);
                sb.append("results: ");
                int i2 = 0;
                for (Long l : xp2.q()) {
                    int i3 = i2 + 1;
                    if (i2 != 0) {
                        sb.append(", ");
                    }
                    sb.append(l);
                    i2 = i3;
                }
                sb.append('\n');
            }
            if (xp2.p() != 0) {
                a(sb, 4);
                sb.append("status: ");
                int i4 = 0;
                for (Long l2 : xp2.zza()) {
                    int i5 = i4 + 1;
                    if (i4 != 0) {
                        sb.append(", ");
                    }
                    sb.append(l2);
                    i4 = i5;
                }
                sb.append('\n');
            }
            if (xp2.t() != 0) {
                a(sb, 4);
                sb.append("dynamic_filter_timestamps: {");
                int i6 = 0;
                for (qp2 qp2 : xp2.s()) {
                    int i7 = i6 + 1;
                    if (i6 != 0) {
                        sb.append(", ");
                    }
                    sb.append(qp2.zza() ? Integer.valueOf(qp2.p()) : null);
                    sb.append(":");
                    sb.append(qp2.q() ? Long.valueOf(qp2.r()) : null);
                    i6 = i7;
                }
                sb.append("}\n");
            }
            if (xp2.v() != 0) {
                a(sb, 4);
                sb.append("sequence_filter_timestamps: {");
                int i8 = 0;
                for (yp2 yp2 : xp2.u()) {
                    int i9 = i8 + 1;
                    if (i8 != 0) {
                        sb.append(", ");
                    }
                    sb.append(yp2.zza() ? Integer.valueOf(yp2.p()) : null);
                    sb.append(": [");
                    int i10 = 0;
                    for (Long l3 : yp2.q()) {
                        long longValue = l3.longValue();
                        int i11 = i10 + 1;
                        if (i10 != 0) {
                            sb.append(", ");
                        }
                        sb.append(longValue);
                        i10 = i11;
                    }
                    sb.append("]");
                    i8 = i9;
                }
                sb.append("}\n");
            }
            a(sb, 3);
            sb.append("}\n");
        }
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i, String str, cp2 cp2) {
        if (cp2 != null) {
            a(sb, i);
            sb.append(str);
            sb.append(" {\n");
            if (cp2.zza()) {
                a(sb, i, "comparison_type", cp2.p().name());
            }
            if (cp2.q()) {
                a(sb, i, "match_as_float", Boolean.valueOf(cp2.r()));
            }
            if (cp2.s()) {
                a(sb, i, "comparison_value", cp2.t());
            }
            if (cp2.u()) {
                a(sb, i, "min_comparison_value", cp2.v());
            }
            if (cp2.w()) {
                a(sb, i, "max_comparison_value", cp2.x());
            }
            a(sb, i);
            sb.append("}\n");
        }
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i, bp2 bp2) {
        if (bp2 != null) {
            a(sb, i);
            sb.append("filter {\n");
            if (bp2.s()) {
                a(sb, i, "complement", Boolean.valueOf(bp2.t()));
            }
            if (bp2.u()) {
                a(sb, i, "param_name", i().b(bp2.v()));
            }
            if (bp2.zza()) {
                int i2 = i + 1;
                ep2 p = bp2.p();
                if (p != null) {
                    a(sb, i2);
                    sb.append("string_filter");
                    sb.append(" {\n");
                    if (p.zza()) {
                        a(sb, i2, "match_type", p.p().name());
                    }
                    if (p.q()) {
                        a(sb, i2, "expression", p.r());
                    }
                    if (p.s()) {
                        a(sb, i2, "case_sensitive", Boolean.valueOf(p.t()));
                    }
                    if (p.v() > 0) {
                        a(sb, i2 + 1);
                        sb.append("expression_list {\n");
                        for (String str : p.u()) {
                            a(sb, i2 + 2);
                            sb.append(str);
                            sb.append("\n");
                        }
                        sb.append("}\n");
                    }
                    a(sb, i2);
                    sb.append("}\n");
                }
            }
            if (bp2.q()) {
                a(sb, i + 1, "number_filter", bp2.r());
            }
            a(sb, i);
            sb.append("}\n");
        }
    }

    @DexIgnore
    public static void a(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("  ");
        }
    }

    @DexIgnore
    public static void a(StringBuilder sb, int i, String str, Object obj) {
        if (obj != null) {
            a(sb, i + 1);
            sb.append(str);
            sb.append(": ");
            sb.append(obj);
            sb.append('\n');
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        e().t().a("Failed to load parcelable from buffer");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
        r1.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0030, code lost:
        throw r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        r5 = move-exception;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x001c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T extends android.os.Parcelable> T a(byte[] r5, android.os.Parcelable.Creator<T> r6) {
        /*
            r4 = this;
            r0 = 0
            if (r5 != 0) goto L_0x0004
            return r0
        L_0x0004:
            android.os.Parcel r1 = android.os.Parcel.obtain()
            int r2 = r5.length     // Catch:{ a -> 0x001c }
            r3 = 0
            r1.unmarshall(r5, r3, r2)     // Catch:{ a -> 0x001c }
            r1.setDataPosition(r3)     // Catch:{ a -> 0x001c }
            java.lang.Object r5 = r6.createFromParcel(r1)     // Catch:{ a -> 0x001c }
            android.os.Parcelable r5 = (android.os.Parcelable) r5     // Catch:{ a -> 0x001c }
            r1.recycle()
            return r5
        L_0x001a:
            r5 = move-exception
            goto L_0x002d
        L_0x001c:
            com.fossil.jg3 r5 = r4.e()     // Catch:{ all -> 0x001a }
            com.fossil.mg3 r5 = r5.t()     // Catch:{ all -> 0x001a }
            java.lang.String r6 = "Failed to load parcelable from buffer"
            r5.a(r6)     // Catch:{ all -> 0x001a }
            r1.recycle()
            return r0
        L_0x002d:
            r1.recycle()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fm3.a(byte[], android.os.Parcelable$Creator):android.os.Parcelable");
    }

    @DexIgnore
    public static boolean a(ub3 ub3, nm3 nm3) {
        a72.a(ub3);
        a72.a(nm3);
        return !TextUtils.isEmpty(nm3.b) || !TextUtils.isEmpty(nm3.w);
    }

    @DexIgnore
    public static boolean a(String str) {
        return str != null && str.matches("([+-])?([0-9]+\\.?[0-9]*|[0-9]*\\.?[0-9]+)") && str.length() <= 310;
    }

    @DexIgnore
    public static boolean a(List<Long> list, int i) {
        if (i >= (list.size() << 6)) {
            return false;
        }
        return ((1 << (i % 64)) & list.get(i / 64).longValue()) != 0;
    }

    @DexIgnore
    public static List<Long> a(BitSet bitSet) {
        int length = (bitSet.length() + 63) / 64;
        ArrayList arrayList = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            long j = 0;
            for (int i2 = 0; i2 < 64; i2++) {
                int i3 = (i << 6) + i2;
                if (i3 >= bitSet.length()) {
                    break;
                }
                if (bitSet.get(i3)) {
                    j |= 1 << i2;
                }
            }
            arrayList.add(Long.valueOf(j));
        }
        return arrayList;
    }

    @DexIgnore
    public final List<Long> a(List<Long> list, List<Integer> list2) {
        ArrayList arrayList = new ArrayList(list);
        for (Integer num : list2) {
            if (num.intValue() < 0) {
                e().w().a("Ignoring negative bit index to be cleared", num);
            } else {
                int intValue = num.intValue() / 64;
                if (intValue >= arrayList.size()) {
                    e().w().a("Ignoring bit index greater than bitSet size", num, Integer.valueOf(arrayList.size()));
                } else {
                    arrayList.set(intValue, Long.valueOf(((Long) arrayList.get(intValue)).longValue() & (~(1 << (num.intValue() % 64)))));
                }
            }
        }
        int size = arrayList.size();
        int size2 = arrayList.size() - 1;
        while (true) {
            size = size2;
            if (size >= 0 && ((Long) arrayList.get(size)).longValue() == 0) {
                size2 = size - 1;
            }
        }
        return arrayList.subList(0, size);
    }

    @DexIgnore
    public final boolean a(long j, long j2) {
        return j == 0 || j2 <= 0 || Math.abs(zzm().b() - j) > j2;
    }

    @DexIgnore
    public final long a(byte[] bArr) {
        a72.a(bArr);
        j().g();
        MessageDigest x = jm3.x();
        if (x != null) {
            return jm3.a(x.digest(bArr));
        }
        e().t().a("Failed to get MD5");
        return 0;
    }

    @DexIgnore
    public static <Builder extends mx2> Builder a(Builder builder, byte[] bArr) throws iw2 {
        nv2 b = nv2.b();
        if (b != null) {
            builder.a(bArr, b);
            return builder;
        }
        builder.a(bArr);
        return builder;
    }

    @DexIgnore
    public static int a(vp2.a aVar, String str) {
        if (aVar == null) {
            return -1;
        }
        for (int i = 0; i < aVar.zze(); i++) {
            if (str.equals(aVar.d(i).q())) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    public static List<tp2> a(Bundle[] bundleArr) {
        ArrayList arrayList = new ArrayList();
        for (Bundle bundle : bundleArr) {
            if (bundle != null) {
                tp2.a F = tp2.F();
                for (String str : bundle.keySet()) {
                    tp2.a F2 = tp2.F();
                    F2.a(str);
                    Object obj = bundle.get(str);
                    if (obj instanceof Long) {
                        F2.a(((Long) obj).longValue());
                    } else if (obj instanceof String) {
                        F2.b((String) obj);
                    } else if (obj instanceof Double) {
                        F2.a(((Double) obj).doubleValue());
                    }
                    F.a(F2);
                }
                if (F.q() > 0) {
                    arrayList.add((tp2) ((bw2) F.g()));
                }
            }
        }
        return arrayList;
    }
}
