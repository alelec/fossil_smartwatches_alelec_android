package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class iy {
    @DexIgnore
    public static /* final */ iy a; // = new b();
    @DexIgnore
    public static /* final */ iy b; // = new c();
    @DexIgnore
    public static /* final */ iy c; // = new d();
    @DexIgnore
    public static /* final */ iy d; // = new e();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends iy {
        @DexIgnore
        @Override // com.fossil.iy
        public boolean a() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean a(sw swVar) {
            return swVar == sw.REMOTE;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean b() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean a(boolean z, sw swVar, uw uwVar) {
            return (swVar == sw.RESOURCE_DISK_CACHE || swVar == sw.MEMORY_CACHE) ? false : true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends iy {
        @DexIgnore
        @Override // com.fossil.iy
        public boolean a() {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean a(sw swVar) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean a(boolean z, sw swVar, uw uwVar) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean b() {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends iy {
        @DexIgnore
        @Override // com.fossil.iy
        public boolean a() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean a(sw swVar) {
            return (swVar == sw.DATA_DISK_CACHE || swVar == sw.MEMORY_CACHE) ? false : true;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean a(boolean z, sw swVar, uw uwVar) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean b() {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends iy {
        @DexIgnore
        @Override // com.fossil.iy
        public boolean a() {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean a(sw swVar) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean a(boolean z, sw swVar, uw uwVar) {
            return (swVar == sw.RESOURCE_DISK_CACHE || swVar == sw.MEMORY_CACHE) ? false : true;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean b() {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends iy {
        @DexIgnore
        @Override // com.fossil.iy
        public boolean a() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean a(sw swVar) {
            return swVar == sw.REMOTE;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean b() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.iy
        public boolean a(boolean z, sw swVar, uw uwVar) {
            return ((z && swVar == sw.DATA_DISK_CACHE) || swVar == sw.LOCAL) && uwVar == uw.TRANSFORMED;
        }
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    public abstract boolean a();

    @DexIgnore
    public abstract boolean a(sw swVar);

    @DexIgnore
    public abstract boolean a(boolean z, sw swVar, uw uwVar);

    @DexIgnore
    public abstract boolean b();
}
