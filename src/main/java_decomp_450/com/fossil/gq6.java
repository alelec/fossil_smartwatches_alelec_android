package com.fossil;

import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gq6 extends cl4 {
    @DexIgnore
    public abstract void a(fb5 fb5);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(Date date, Calendar calendar);

    @DexIgnore
    public abstract void a(boolean z);

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public abstract void b(boolean z);

    @DexIgnore
    public abstract void c(String str);

    @DexIgnore
    public abstract void c(boolean z);

    @DexIgnore
    public abstract void d(boolean z);

    @DexIgnore
    public abstract void e(boolean z);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract Calendar i();

    @DexIgnore
    public abstract void j();

    @DexIgnore
    public abstract void k();
}
