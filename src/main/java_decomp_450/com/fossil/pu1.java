package com.fossil;

import android.util.Base64;
import com.fossil.fu1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pu1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(dt1 dt1);

        @DexIgnore
        public abstract a a(String str);

        @DexIgnore
        public abstract a a(byte[] bArr);

        @DexIgnore
        public abstract pu1 a();
    }

    @DexIgnore
    public static a d() {
        fu1.b bVar = new fu1.b();
        bVar.a(dt1.DEFAULT);
        return bVar;
    }

    @DexIgnore
    public pu1 a(dt1 dt1) {
        a d = d();
        d.a(a());
        d.a(dt1);
        d.a(b());
        return d.a();
    }

    @DexIgnore
    public abstract String a();

    @DexIgnore
    public abstract byte[] b();

    @DexIgnore
    public abstract dt1 c();

    @DexIgnore
    public final String toString() {
        Object[] objArr = new Object[3];
        objArr[0] = a();
        objArr[1] = c();
        objArr[2] = b() == null ? "" : Base64.encodeToString(b(), 2);
        return String.format("TransportContext(%s, %s, %s)", objArr);
    }
}
