package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lr0 extends pe1 {
    @DexIgnore
    public static /* final */ pp0 CREATOR; // = new pp0(null);
    @DexIgnore
    public e91[] b;
    @DexIgnore
    public /* final */ ig1 c;

    @DexIgnore
    public lr0(e91[] e91Arr) {
        super(zp1.ANIMATION);
        this.b = new e91[0];
        this.c = ig1.SIMPLE_MOVEMENT;
        this.b = e91Arr;
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.pe1
    public JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        for (e91 e91 : this.b) {
            jSONArray.put(e91.a());
        }
        return yz0.a(super.a(), r51.Q3, jSONArray);
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public byte[] b() {
        ByteBuffer order = ByteBuffer.allocate((this.b.length * 3) + 2).order(ByteOrder.LITTLE_ENDIAN);
        ee7.a((Object) order, "ByteBuffer.allocate(2 + \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.c.a);
        byte b2 = (byte) 0;
        order.put(b2);
        e91[] e91Arr = this.b;
        for (e91 e91 : e91Arr) {
            b2 = (byte) (b2 | e91.a.a);
            ByteBuffer order2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN);
            ee7.a((Object) order2, "ByteBuffer.allocate(3)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
            order2.put((byte) (((byte) (((byte) (((byte) (e91.b.a << 6)) | ((byte) (e91.c.a << 5)))) | 0)) | e91.d.a));
            order2.putShort(e91.e);
            byte[] array = order2.array();
            ee7.a((Object) array, "dataBuffer.array()");
            order.put(array);
        }
        order.put(1, b2);
        byte[] array2 = order.array();
        ee7.a((Object) array2, "byteBuffer.array()");
        return array2;
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(lr0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.b, ((lr0) obj).b);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.AnimationInstr");
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.b, i);
        }
    }

    @DexIgnore
    public /* synthetic */ lr0(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = new e91[0];
        this.c = ig1.SIMPLE_MOVEMENT;
        Object[] readArray = parcel.readArray(e91.class.getClassLoader());
        if (readArray != null) {
            this.b = (e91[]) readArray;
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.model.microapp.animation.HandAnimation>");
    }
}
