package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k42 implements fo3<Boolean, Void> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.no3] */
    @Override // com.fossil.fo3
    public final /* synthetic */ Void then(no3<Boolean> no3) throws Exception {
        if (no3.b().booleanValue()) {
            return null;
        }
        throw new w02(new Status(13, "listener already unregistered"));
    }
}
