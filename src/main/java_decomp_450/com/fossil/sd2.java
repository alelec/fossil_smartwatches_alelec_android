package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sd2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<sd2> CREATOR; // = new ud2();
    @DexIgnore
    public /* final */ gc2 a;
    @DexIgnore
    public /* final */ ed2 b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public sd2(gc2 gc2, IBinder iBinder, long j, long j2) {
        this.a = gc2;
        this.b = dd2.a(iBinder);
        this.c = j;
        this.d = j2;
    }

    @DexIgnore
    public gc2 e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof sd2)) {
            return false;
        }
        sd2 sd2 = (sd2) obj;
        return y62.a(this.a, sd2.a) && this.c == sd2.c && this.d == sd2.d;
    }

    @DexIgnore
    public int hashCode() {
        return y62.a(this.a, Long.valueOf(this.c), Long.valueOf(this.d));
    }

    @DexIgnore
    public String toString() {
        return String.format("FitnessSensorServiceRequest{%s}", this.a);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, (Parcelable) e(), i, false);
        k72.a(parcel, 2, this.b.asBinder(), false);
        k72.a(parcel, 3, this.c);
        k72.a(parcel, 4, this.d);
        k72.a(parcel, a2);
    }
}
