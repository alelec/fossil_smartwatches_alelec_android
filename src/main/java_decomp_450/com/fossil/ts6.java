package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.PermissionData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ts6 extends ms6 {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ ns6 e;
    @DexIgnore
    public /* final */ List<PermissionData> f;
    @DexIgnore
    public /* final */ ch5 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
        String simpleName = ts6.class.getSimpleName();
        ee7.a((Object) simpleName, "PermissionPresenter::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public ts6(ns6 ns6, List<PermissionData> list, ch5 ch5) {
        ee7.b(ns6, "mView");
        ee7.b(list, "mListPerms");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.e = ns6;
        this.f = list;
        this.g = ch5;
    }

    @DexIgnore
    @Override // com.fossil.ms6
    public boolean a(String str) {
        ee7.b(str, "permission");
        Boolean j = this.g.j(str);
        ee7.a((Object) j, "mSharedPreferencesManage\u2026rstTimeAsking(permission)");
        return j.booleanValue();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005e, code lost:
        if (r2.getSettingPermissionId() == null) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0060, code lost:
        r3 = com.fossil.xg5.b;
        r5 = r2.getSettingPermissionId();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0066, code lost:
        if (r5 == null) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0068, code lost:
        r3 = r3.c(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x006d, code lost:
        com.fossil.ee7.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0070, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0071, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0072, code lost:
        r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r6 = com.fossil.ts6.h;
        r5.d(r6, "check " + ((java.lang.Object) r2) + " androidPermissionsNotGranted " + r4 + " settingPermissionGranted " + r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a2, code lost:
        if (r4.isEmpty() == false) goto L_0x00a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00a4, code lost:
        if (r3 == false) goto L_0x00a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a6, code lost:
        r7 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a7, code lost:
        r2.setGranted(r7);
     */
    @DexIgnore
    @Override // com.fossil.cl4
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void f() {
        /*
            r11 = this;
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.ts6.h
            java.lang.String r2 = "start"
            r0.d(r1, r2)
            com.fossil.ns6 r0 = r11.e
            if (r0 == 0) goto L_0x00e2
            com.fossil.os6 r0 = (com.fossil.os6) r0
            android.content.Context r0 = r0.getContext()
            java.util.List<com.portfolio.platform.data.model.PermissionData> r1 = r11.f
            java.util.Iterator r1 = r1.iterator()
        L_0x001d:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x00ac
            java.lang.Object r2 = r1.next()
            com.portfolio.platform.data.model.PermissionData r2 = (com.portfolio.platform.data.model.PermissionData) r2
            java.util.ArrayList r3 = r2.getAndroidPermissionSet()
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.Iterator r3 = r3.iterator()
        L_0x0036:
            boolean r5 = r3.hasNext()
            r6 = 0
            r7 = 0
            r8 = 1
            if (r5 == 0) goto L_0x005a
            java.lang.Object r5 = r3.next()
            r9 = r5
            java.lang.String r9 = (java.lang.String) r9
            if (r0 == 0) goto L_0x0056
            java.lang.String[] r6 = new java.lang.String[r8]
            r6[r7] = r9
            boolean r6 = com.fossil.ku7.a(r0, r6)
            if (r6 != 0) goto L_0x0036
            r4.add(r5)
            goto L_0x0036
        L_0x0056:
            com.fossil.ee7.a()
            throw r6
        L_0x005a:
            com.fossil.xg5$c r3 = r2.getSettingPermissionId()
            if (r3 == 0) goto L_0x0071
            com.fossil.xg5 r3 = com.fossil.xg5.b
            com.fossil.xg5$c r5 = r2.getSettingPermissionId()
            if (r5 == 0) goto L_0x006d
            boolean r3 = r3.c(r5)
            goto L_0x0072
        L_0x006d:
            com.fossil.ee7.a()
            throw r6
        L_0x0071:
            r3 = 1
        L_0x0072:
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = com.fossil.ts6.h
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "check "
            r9.append(r10)
            r9.append(r2)
            java.lang.String r10 = " androidPermissionsNotGranted "
            r9.append(r10)
            r9.append(r4)
            java.lang.String r10 = " settingPermissionGranted "
            r9.append(r10)
            r9.append(r3)
            java.lang.String r9 = r9.toString()
            r5.d(r6, r9)
            boolean r4 = r4.isEmpty()
            if (r4 == 0) goto L_0x00a7
            if (r3 == 0) goto L_0x00a7
            r7 = 1
        L_0x00a7:
            r2.setGranted(r7)
            goto L_0x001d
        L_0x00ac:
            java.util.List<com.portfolio.platform.data.model.PermissionData> r0 = r11.f
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Iterator r0 = r0.iterator()
        L_0x00b7:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x00ce
            java.lang.Object r2 = r0.next()
            r3 = r2
            com.portfolio.platform.data.model.PermissionData r3 = (com.portfolio.platform.data.model.PermissionData) r3
            boolean r3 = r3.isGranted()
            if (r3 != 0) goto L_0x00b7
            r1.add(r2)
            goto L_0x00b7
        L_0x00ce:
            boolean r0 = r1.isEmpty()
            if (r0 == 0) goto L_0x00da
            com.fossil.ns6 r0 = r11.e
            r0.close()
            goto L_0x00e1
        L_0x00da:
            com.fossil.ns6 r0 = r11.e
            java.util.List<com.portfolio.platform.data.model.PermissionData> r1 = r11.f
            r0.t(r1)
        L_0x00e1:
            return
        L_0x00e2:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r1 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.permission.PermissionFragment"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ts6.f():void");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(h, "stop");
    }

    @DexIgnore
    public void h() {
        this.e.a(this);
    }

    @DexIgnore
    @Override // com.fossil.ms6
    public void a(String str, boolean z) {
        ee7.b(str, "permission");
        this.g.a(str, Boolean.valueOf(z));
    }
}
