package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class ay0 extends Enum<ay0> {
    @DexIgnore
    public static /* final */ ay0 a;
    @DexIgnore
    public static /* final */ ay0 b;
    @DexIgnore
    public static /* final */ ay0 c;
    @DexIgnore
    public static /* final */ ay0 d;
    @DexIgnore
    public static /* final */ ay0 e;
    @DexIgnore
    public static /* final */ ay0 f;
    @DexIgnore
    public static /* final */ ay0 g;
    @DexIgnore
    public static /* final */ ay0 h;
    @DexIgnore
    public static /* final */ ay0 i;
    @DexIgnore
    public static /* final */ ay0 j;
    @DexIgnore
    public static /* final */ ay0 k;
    @DexIgnore
    public static /* final */ ay0 l;
    @DexIgnore
    public static /* final */ ay0 m;
    @DexIgnore
    public static /* final */ ay0 n;
    @DexIgnore
    public static /* final */ ay0 o;
    @DexIgnore
    public static /* final */ ay0 p;
    @DexIgnore
    public static /* final */ ay0 q;
    @DexIgnore
    public static /* final */ ay0 r;
    @DexIgnore
    public static /* final */ ay0 s;
    @DexIgnore
    public static /* final */ ay0 t;
    @DexIgnore
    public static /* final */ ay0 u;
    @DexIgnore
    public static /* final */ ay0 v;
    @DexIgnore
    public static /* final */ ay0 w;
    @DexIgnore
    public static /* final */ ay0 x;
    @DexIgnore
    public static /* final */ /* synthetic */ ay0[] y;
    @DexIgnore
    public static /* final */ iw0 z; // = new iw0(null);

    /*
    static {
        ay0 ay0 = new ay0("SUCCESS", 0, 0);
        a = ay0;
        ay0 ay02 = new ay0("NOT_START", 1, 1);
        b = ay02;
        ay0 ay03 = new ay0("COMMAND_ERROR", 2, 2);
        c = ay03;
        ay0 ay04 = new ay0("RESPONSE_ERROR", 3, 3);
        d = ay04;
        ay0 ay05 = new ay0("EOF_TIME_OUT", 5, 5);
        e = ay05;
        ay0 ay06 = new ay0("CONNECTION_DROPPED", 6, 6);
        f = ay06;
        ay0 ay07 = new ay0("MISS_PACKAGE", 7, 7);
        g = ay07;
        ay0 ay08 = new ay0("INVALID_DATA_LENGTH", 8, 8);
        h = ay08;
        ay0 ay09 = new ay0("RECEIVED_DATA_CRC_MISS_MATCH", 9, 9);
        i = ay09;
        ay0 ay010 = new ay0("INVALID_RESPONSE_LENGTH", 10, 10);
        j = ay010;
        ay0 ay011 = new ay0("INVALID_RESPONSE_DATA", 11, 11);
        k = ay011;
        ay0 ay012 = new ay0("REQUEST_UNSUPPORTED", 12, 12);
        l = ay012;
        ay0 ay013 = new ay0("UNSUPPORTED_FILE_HANDLE", 13, 13);
        m = ay013;
        ay0 ay014 = new ay0("BLUETOOTH_OFF", 14, 14);
        n = ay014;
        ay0 ay015 = new ay0("WRONG_AUTHENTICATION_KEY_TYPE", 15, 15);
        o = ay015;
        ay0 ay016 = new ay0("REQUEST_TIMEOUT", 16, 16);
        p = ay016;
        ay0 ay017 = new ay0("RESPONSE_TIMEOUT", 17, 17);
        q = ay017;
        ay0 ay018 = new ay0("EMPTY_SERVICES", 18, 18);
        r = ay018;
        ay0 ay019 = new ay0("INTERRUPTED", 19, 254);
        s = ay019;
        ay0 ay020 = new ay0("UNKNOWN_ERROR", 20, 255);
        t = ay020;
        ay0 ay021 = new ay0("HID_PROXY_NOT_CONNECTED", 21, 256);
        u = ay021;
        ay0 ay022 = new ay0("HID_FAIL_TO_INVOKE_PRIVATE_METHOD", 22, 257);
        v = ay022;
        ay0 ay023 = new ay0("HID_INPUT_DEVICE_DISABLED", 23, 258);
        w = ay023;
        ay0 ay024 = new ay0("HID_UNKNOWN_ERROR", 24, 511);
        x = ay024;
        y = new ay0[]{ay0, ay02, ay03, ay04, new ay0("TIMEOUT", 4, 4), ay05, ay06, ay07, ay08, ay09, ay010, ay011, ay012, ay013, ay014, ay015, ay016, ay017, ay018, ay019, ay020, ay021, ay022, ay023, ay024};
    }
    */

    @DexIgnore
    public ay0(String str, int i2, int i3) {
    }

    @DexIgnore
    public static ay0 valueOf(String str) {
        return (ay0) Enum.valueOf(ay0.class, str);
    }

    @DexIgnore
    public static ay0[] values() {
        return (ay0[]) y.clone();
    }
}
