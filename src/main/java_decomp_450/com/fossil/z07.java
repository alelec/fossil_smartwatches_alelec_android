package com.fossil;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import com.fossil.i17;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z07 extends s07 {
    @DexIgnore
    public static /* final */ String[] b; // = {"orientation"};

    @DexIgnore
    public enum a {
        MICRO(3, 96, 96),
        MINI(1, 512, 384),
        FULL(2, -1, -1);
        
        @DexIgnore
        public /* final */ int androidKind;
        @DexIgnore
        public /* final */ int height;
        @DexIgnore
        public /* final */ int width;

        @DexIgnore
        public a(int i, int i2, int i3) {
            this.androidKind = i;
            this.width = i2;
            this.height = i3;
        }
    }

    @DexIgnore
    public z07(Context context) {
        super(context);
    }

    @DexIgnore
    @Override // com.fossil.i17, com.fossil.s07
    public boolean a(g17 g17) {
        Uri uri = g17.d;
        return "content".equals(uri.getScheme()) && "media".equals(uri.getAuthority());
    }

    @DexIgnore
    @Override // com.fossil.i17, com.fossil.s07
    public i17.a a(g17 g17, int i) throws IOException {
        Bitmap bitmap;
        ContentResolver contentResolver = ((s07) this).a.getContentResolver();
        int a2 = a(contentResolver, g17.d);
        String type = contentResolver.getType(g17.d);
        boolean z = type != null && type.startsWith("video/");
        if (g17.c()) {
            a a3 = a(g17.h, g17.i);
            if (!z && a3 == a.FULL) {
                return new i17.a(null, c(g17), Picasso.LoadedFrom.DISK, a2);
            }
            long parseId = ContentUris.parseId(g17.d);
            BitmapFactory.Options b2 = i17.b(g17);
            b2.inJustDecodeBounds = true;
            i17.a(g17.h, g17.i, a3.width, a3.height, b2, g17);
            if (z) {
                bitmap = MediaStore.Video.Thumbnails.getThumbnail(contentResolver, parseId, a3 == a.FULL ? 1 : a3.androidKind, b2);
            } else {
                bitmap = MediaStore.Images.Thumbnails.getThumbnail(contentResolver, parseId, a3.androidKind, b2);
            }
            if (bitmap != null) {
                return new i17.a(bitmap, null, Picasso.LoadedFrom.DISK, a2);
            }
        }
        return new i17.a(null, c(g17), Picasso.LoadedFrom.DISK, a2);
    }

    @DexIgnore
    public static a a(int i, int i2) {
        a aVar = a.MICRO;
        if (i <= aVar.width && i2 <= aVar.height) {
            return aVar;
        }
        a aVar2 = a.MINI;
        if (i > aVar2.width || i2 > aVar2.height) {
            return a.FULL;
        }
        return aVar2;
    }

    @DexIgnore
    public static int a(ContentResolver contentResolver, Uri uri) {
        Cursor cursor = null;
        try {
            Cursor query = contentResolver.query(uri, b, null, null, null);
            if (query != null) {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    if (query != null) {
                        query.close();
                    }
                    return i;
                }
            }
            if (query != null) {
                query.close();
            }
            return 0;
        } catch (RuntimeException unused) {
            if (0 != 0) {
                cursor.close();
            }
            return 0;
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }
}
