package com.fossil;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.j62;
import com.fossil.u12;
import com.fossil.v02;
import com.fossil.v02.d;
import com.fossil.y12;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.Scope;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z02<O extends v02.d> implements b12<O> {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ v02<O> b;
    @DexIgnore
    public /* final */ O c;
    @DexIgnore
    public /* final */ p12<O> d;
    @DexIgnore
    public /* final */ Looper e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ a12 g;
    @DexIgnore
    public /* final */ d22 h;
    @DexIgnore
    public /* final */ u12 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static /* final */ a c; // = new C0259a().a();
        @DexIgnore
        public /* final */ d22 a;
        @DexIgnore
        public /* final */ Looper b;

        @DexIgnore
        public a(d22 d22, Account account, Looper looper) {
            this.a = d22;
            this.b = looper;
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.z02$a$a")
        /* renamed from: com.fossil.z02$a$a  reason: collision with other inner class name */
        public static class C0259a {
            @DexIgnore
            public d22 a;
            @DexIgnore
            public Looper b;

            @DexIgnore
            public C0259a a(d22 d22) {
                a72.a(d22, "StatusExceptionMapper must not be null.");
                this.a = d22;
                return this;
            }

            @DexIgnore
            public C0259a a(Looper looper) {
                a72.a(looper, "Looper must not be null.");
                this.b = looper;
                return this;
            }

            @DexIgnore
            public a a() {
                if (this.a == null) {
                    this.a = new o12();
                }
                if (this.b == null) {
                    this.b = Looper.getMainLooper();
                }
                return new a(this.a, this.b);
            }
        }
    }

    @DexIgnore
    public z02(Context context, v02<O> v02, Looper looper) {
        a72.a(context, "Null context is not permitted.");
        a72.a(v02, "Api must not be null.");
        a72.a(looper, "Looper must not be null.");
        this.a = context.getApplicationContext();
        a(context);
        this.b = v02;
        this.c = null;
        this.e = looper;
        this.d = p12.a(v02);
        this.g = new y32(this);
        u12 a2 = u12.a(this.a);
        this.i = a2;
        this.f = a2.b();
        this.h = new o12();
    }

    @DexIgnore
    public final <A extends v02.b, T extends r12<? extends i12, A>> T a(int i2, T t) {
        t.g();
        this.i.a(this, i2, t);
        return t;
    }

    @DexIgnore
    public <A extends v02.b, T extends r12<? extends i12, A>> T b(T t) {
        a(1, t);
        return t;
    }

    @DexIgnore
    public j62.a c() {
        Account account;
        Set<Scope> set;
        GoogleSignInAccount b2;
        GoogleSignInAccount b3;
        j62.a aVar = new j62.a();
        O o = this.c;
        if (!(o instanceof v02.d.b) || (b3 = ((v02.d.b) o).b()) == null) {
            O o2 = this.c;
            account = o2 instanceof v02.d.a ? ((v02.d.a) o2).c() : null;
        } else {
            account = b3.c();
        }
        aVar.a(account);
        O o3 = this.c;
        if (!(o3 instanceof v02.d.b) || (b2 = ((v02.d.b) o3).b()) == null) {
            set = Collections.emptySet();
        } else {
            set = b2.B();
        }
        aVar.a(set);
        aVar.a(this.a.getClass().getName());
        aVar.b(this.a.getPackageName());
        return aVar;
    }

    @DexIgnore
    public final v02<O> d() {
        return this.b;
    }

    @DexIgnore
    public O e() {
        return this.c;
    }

    @DexIgnore
    public Context f() {
        return this.a;
    }

    @DexIgnore
    public final int g() {
        return this.f;
    }

    @DexIgnore
    public Looper h() {
        return this.e;
    }

    @DexIgnore
    public a12 b() {
        return this.g;
    }

    @DexIgnore
    public final <TResult, A extends v02.b> no3<TResult> a(int i2, f22<A, TResult> f22) {
        oo3 oo3 = new oo3();
        this.i.a(this, i2, f22, oo3, this.h);
        return oo3.a();
    }

    @DexIgnore
    public <A extends v02.b, T extends r12<? extends i12, A>> T a(T t) {
        a(0, t);
        return t;
    }

    @DexIgnore
    public <TResult, A extends v02.b> no3<TResult> a(f22<A, TResult> f22) {
        return a(0, f22);
    }

    @DexIgnore
    @Deprecated
    public <A extends v02.b, T extends b22<A, ?>, U extends h22<A, ?>> no3<Void> a(T t, U u) {
        a72.a(t);
        a72.a(u);
        a72.a(t.b(), "Listener has already been released.");
        a72.a(u.a(), "Listener has already been released.");
        a72.a(t.b().equals(u.a()), "Listener registration and unregistration methods must be constructed with the same ListenerHolder.");
        return this.i.a(this, t, u, u52.a);
    }

    @DexIgnore
    public z02(Activity activity, v02<O> v02, O o, a aVar) {
        a72.a(activity, "Null activity is not permitted.");
        a72.a(v02, "Api must not be null.");
        a72.a(aVar, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.a = activity.getApplicationContext();
        a(activity);
        this.b = v02;
        this.c = o;
        this.e = aVar.b;
        this.d = p12.a(v02, o);
        this.g = new y32(this);
        u12 a2 = u12.a(this.a);
        this.i = a2;
        this.f = a2.b();
        this.h = aVar.a;
        if (!(activity instanceof GoogleApiActivity)) {
            k22.a(activity, this.i, (p12<?>) this.d);
        }
        this.i.a((z02<?>) this);
    }

    @DexIgnore
    public no3<Boolean> a(y12.a<?> aVar) {
        a72.a(aVar, "Listener key cannot be null.");
        return this.i.a(this, aVar);
    }

    @DexIgnore
    public v02.f a(Looper looper, u12.a<O> aVar) {
        return this.b.d().a(this.a, looper, c().a(), this.c, aVar, aVar);
    }

    @DexIgnore
    @Override // com.fossil.b12
    public p12<O> a() {
        return this.d;
    }

    @DexIgnore
    public static String a(Object obj) {
        if (!v92.m()) {
            return null;
        }
        try {
            return (String) Context.class.getMethod("getFeatureId", new Class[0]).invoke(obj, new Object[0]);
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException unused) {
            return null;
        }
    }

    @DexIgnore
    public g42 a(Context context, Handler handler) {
        return new g42(context, handler, c().a());
    }

    @DexIgnore
    public z02(Context context, v02<O> v02, O o, a aVar) {
        a72.a(context, "Null context is not permitted.");
        a72.a(v02, "Api must not be null.");
        a72.a(aVar, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.a = context.getApplicationContext();
        a(context);
        this.b = v02;
        this.c = o;
        this.e = aVar.b;
        this.d = p12.a(v02, o);
        this.g = new y32(this);
        u12 a2 = u12.a(this.a);
        this.i = a2;
        this.f = a2.b();
        this.h = aVar.a;
        this.i.a((z02<?>) this);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public z02(android.app.Activity r2, com.fossil.v02<O> r3, O r4, com.fossil.d22 r5) {
        /*
            r1 = this;
            com.fossil.z02$a$a r0 = new com.fossil.z02$a$a
            r0.<init>()
            r0.a(r5)
            android.os.Looper r5 = r2.getMainLooper()
            r0.a(r5)
            com.fossil.z02$a r5 = r0.a()
            r1.<init>(r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.z02.<init>(android.app.Activity, com.fossil.v02, com.fossil.v02$d, com.fossil.d22):void");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public z02(android.content.Context r2, com.fossil.v02<O> r3, O r4, com.fossil.d22 r5) {
        /*
            r1 = this;
            com.fossil.z02$a$a r0 = new com.fossil.z02$a$a
            r0.<init>()
            r0.a(r5)
            com.fossil.z02$a r5 = r0.a()
            r1.<init>(r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.z02.<init>(android.content.Context, com.fossil.v02, com.fossil.v02$d, com.fossil.d22):void");
    }
}
