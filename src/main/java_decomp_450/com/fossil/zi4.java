package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zi4 implements sg4 {
    @DexIgnore
    @Override // com.fossil.sg4
    public dh4 a(String str, mg4 mg4, int i, int i2, Map<og4, ?> map) throws tg4 {
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (mg4 != mg4.QR_CODE) {
            throw new IllegalArgumentException("Can only encode QR_CODE, but got " + mg4);
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Requested dimensions are too small: " + i + 'x' + i2);
        } else {
            aj4 aj4 = aj4.L;
            int i3 = 4;
            if (map != null) {
                if (map.containsKey(og4.ERROR_CORRECTION)) {
                    aj4 = aj4.valueOf(map.get(og4.ERROR_CORRECTION).toString());
                }
                if (map.containsKey(og4.MARGIN)) {
                    i3 = Integer.parseInt(map.get(og4.MARGIN).toString());
                }
            }
            return a(fj4.a(str, aj4, map), i, i2, i3);
        }
    }

    @DexIgnore
    public static dh4 a(ij4 ij4, int i, int i2, int i3) {
        ej4 a = ij4.a();
        if (a != null) {
            int c = a.c();
            int b = a.b();
            int i4 = i3 << 1;
            int i5 = c + i4;
            int i6 = i4 + b;
            int max = Math.max(i, i5);
            int max2 = Math.max(i2, i6);
            int min = Math.min(max / i5, max2 / i6);
            int i7 = (max - (c * min)) / 2;
            int i8 = (max2 - (b * min)) / 2;
            dh4 dh4 = new dh4(max, max2);
            int i9 = 0;
            while (i9 < b) {
                int i10 = i7;
                int i11 = 0;
                while (i11 < c) {
                    if (a.a(i11, i9) == 1) {
                        dh4.a(i10, i8, min, min);
                    }
                    i11++;
                    i10 += min;
                }
                i9++;
                i8 += min;
            }
            return dh4;
        }
        throw new IllegalStateException();
    }
}
