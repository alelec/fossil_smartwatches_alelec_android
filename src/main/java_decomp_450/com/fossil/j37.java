package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface j37 {
    @DexIgnore
    boolean a();

    @DexIgnore
    boolean a(Intent intent, k37 k37);

    @DexIgnore
    boolean a(u27 u27);

    @DexIgnore
    boolean a(String str);
}
