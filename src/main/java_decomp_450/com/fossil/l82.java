package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l82 implements Parcelable.Creator<d72> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ d72 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        int i = 0;
        Scope[] scopeArr = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                i = j72.q(parcel, a);
            } else if (a2 == 2) {
                i2 = j72.q(parcel, a);
            } else if (a2 == 3) {
                i3 = j72.q(parcel, a);
            } else if (a2 != 4) {
                j72.v(parcel, a);
            } else {
                scopeArr = (Scope[]) j72.b(parcel, a, Scope.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new d72(i, i2, i3, scopeArr);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ d72[] newArray(int i) {
        return new d72[i];
    }
}
