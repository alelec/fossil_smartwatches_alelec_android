package com.fossil;

import android.content.Context;
import android.net.Uri;
import android.net.http.HttpResponseCache;
import android.os.Build;
import com.facebook.stetho.server.http.HttpHeaders;
import com.squareup.picasso.Downloader;
import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n17 implements Downloader {
    @DexIgnore
    public static volatile Object b;
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public static /* final */ ThreadLocal<StringBuilder> d; // = new a();
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ThreadLocal<StringBuilder> {
        @DexIgnore
        @Override // java.lang.ThreadLocal
        public StringBuilder initialValue() {
            return new StringBuilder();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static Object a(Context context) throws IOException {
            File b = o17.b(context);
            HttpResponseCache installed = HttpResponseCache.getInstalled();
            return installed == null ? HttpResponseCache.install(b, o17.a(b)) : installed;
        }
    }

    @DexIgnore
    public n17(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public HttpURLConnection a(Uri uri) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(uri.toString()).openConnection();
        httpURLConnection.setConnectTimeout(15000);
        httpURLConnection.setReadTimeout(20000);
        return httpURLConnection;
    }

    @DexIgnore
    @Override // com.squareup.picasso.Downloader
    public Downloader.Response load(Uri uri, int i) throws IOException {
        String str;
        if (Build.VERSION.SDK_INT >= 14) {
            a(this.a);
        }
        HttpURLConnection a2 = a(uri);
        a2.setUseCaches(true);
        if (i != 0) {
            if (b17.isOfflineOnly(i)) {
                str = "only-if-cached,max-age=2147483647";
            } else {
                StringBuilder sb = d.get();
                sb.setLength(0);
                if (!b17.shouldReadFromDiskCache(i)) {
                    sb.append("no-cache");
                }
                if (!b17.shouldWriteToDiskCache(i)) {
                    if (sb.length() > 0) {
                        sb.append(',');
                    }
                    sb.append("no-store");
                }
                str = sb.toString();
            }
            a2.setRequestProperty(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER, str);
        }
        int responseCode = a2.getResponseCode();
        if (responseCode < 300) {
            long headerFieldInt = (long) a2.getHeaderFieldInt(HttpHeaders.CONTENT_LENGTH, -1);
            return new Downloader.Response(a2.getInputStream(), o17.a(a2.getHeaderField("X-Android-Response-Source")), headerFieldInt);
        }
        a2.disconnect();
        throw new Downloader.a(responseCode + " " + a2.getResponseMessage(), i, responseCode);
    }

    @DexIgnore
    public static void a(Context context) {
        if (b == null) {
            try {
                synchronized (c) {
                    if (b == null) {
                        b = b.a(context);
                    }
                }
            } catch (IOException unused) {
            }
        }
    }
}
