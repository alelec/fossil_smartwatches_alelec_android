package com.fossil;

import android.graphics.Point;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r63 {
    @DexIgnore
    public /* final */ z63 a;

    @DexIgnore
    public r63(z63 z63) {
        this.a = z63;
    }

    @DexIgnore
    public final LatLng a(Point point) {
        try {
            return this.a.e(cb2.a(point));
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final Point a(LatLng latLng) {
        try {
            return (Point) cb2.g(this.a.b(latLng));
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final v93 a() {
        try {
            return this.a.q();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }
}
