package com.fossil;

import com.fossil.m00;
import java.io.InputStream;
import java.net.URL;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c10 implements m00<URL, InputStream> {
    @DexIgnore
    public /* final */ m00<f00, InputStream> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements n00<URL, InputStream> {
        @DexIgnore
        @Override // com.fossil.n00
        public m00<URL, InputStream> a(q00 q00) {
            return new c10(q00.a(f00.class, InputStream.class));
        }
    }

    @DexIgnore
    public c10(m00<f00, InputStream> m00) {
        this.a = m00;
    }

    @DexIgnore
    public boolean a(URL url) {
        return true;
    }

    @DexIgnore
    public m00.a<InputStream> a(URL url, int i, int i2, ax axVar) {
        return this.a.a(new f00(url), i, i2, axVar);
    }
}
