package com.fossil;

import com.fossil.s87;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ob7 implements fb7<Object>, sb7, Serializable {
    @DexIgnore
    public /* final */ fb7<Object> completion;

    @DexIgnore
    public ob7(fb7<Object> fb7) {
        this.completion = fb7;
    }

    @DexIgnore
    public fb7<i97> create(fb7<?> fb7) {
        ee7.b(fb7, "completion");
        throw new UnsupportedOperationException("create(Continuation) has not been overridden");
    }

    @DexIgnore
    @Override // com.fossil.sb7
    public sb7 getCallerFrame() {
        fb7<Object> fb7 = this.completion;
        if (!(fb7 instanceof sb7)) {
            fb7 = null;
        }
        return (sb7) fb7;
    }

    @DexIgnore
    public final fb7<Object> getCompletion() {
        return this.completion;
    }

    @DexIgnore
    @Override // com.fossil.sb7
    public StackTraceElement getStackTraceElement() {
        return ub7.c(this);
    }

    @DexIgnore
    public abstract Object invokeSuspend(Object obj);

    @DexIgnore
    public void releaseIntercepted() {
    }

    @DexIgnore
    @Override // com.fossil.fb7
    public final void resumeWith(Object obj) {
        ob7 ob7 = this;
        while (true) {
            vb7.b(ob7);
            fb7<Object> fb7 = ob7.completion;
            if (fb7 != null) {
                try {
                    Object invokeSuspend = ob7.invokeSuspend(obj);
                    if (invokeSuspend != nb7.a()) {
                        s87.a aVar = s87.Companion;
                        obj = s87.m60constructorimpl(invokeSuspend);
                        ob7.releaseIntercepted();
                        if (fb7 instanceof ob7) {
                            ob7 = (ob7) fb7;
                        } else {
                            fb7.resumeWith(obj);
                            return;
                        }
                    } else {
                        return;
                    }
                } catch (Throwable th) {
                    s87.a aVar2 = s87.Companion;
                    obj = s87.m60constructorimpl(t87.a(th));
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Continuation at ");
        Object stackTraceElement = getStackTraceElement();
        if (stackTraceElement == null) {
            stackTraceElement = getClass().getName();
        }
        sb.append(stackTraceElement);
        return sb.toString();
    }

    @DexIgnore
    public fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        throw new UnsupportedOperationException("create(Any?;Continuation) has not been overridden");
    }
}
