package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j13 implements g13 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        a = dr2.a("measurement.sdk.referrer.delayed_install_referrer_api", false);
        dr2.a("measurement.id.sdk.referrer.delayed_install_referrer_api", 0L);
    }
    */

    @DexIgnore
    @Override // com.fossil.g13
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.g13
    public final boolean zzb() {
        return a.b().booleanValue();
    }
}
