package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wd1 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ mh1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wd1(mh1 mh1) {
        super(1);
        this.a = mh1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        this.a.C = ((xh1) v81).r();
        h41 m = this.a.m();
        if (m == null) {
            mh1 mh1 = this.a;
            mh1.a(eu0.a(((zk0) mh1).v, null, is0.FLOW_BROKEN, null, 5));
        } else if (e61.a.a(m, this.a.E)) {
            mh1 mh12 = this.a;
            mh12.a(eu0.a(((zk0) mh12).v, null, is0.SUCCESS, null, 5));
        } else {
            mh1 mh13 = this.a;
            mh13.a(eu0.a(((zk0) mh13).v, null, is0.EXCHANGED_VALUE_NOT_SATISFIED, null, 5));
        }
        return i97.a;
    }
}
