package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.facebook.GraphRequest;
import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r17 implements FlutterPlugin, MethodChannel.MethodCallHandler {
    @DexIgnore
    public static /* final */ Map<String, Integer> c; // = new HashMap();
    @DexIgnore
    public static boolean d; // = false;
    @DexIgnore
    public static int e; // = 10;
    @DexIgnore
    public static int f; // = 0;
    @DexIgnore
    public static /* final */ Object g; // = new Object();
    @DexIgnore
    public static /* final */ Object h; // = new Object();
    @DexIgnore
    public static String i;
    @DexIgnore
    public static int j; // = 0;
    @DexIgnore
    public static HandlerThread p;
    @DexIgnore
    public static Handler q;
    @DexIgnore
    @SuppressLint({"UseSparseArrays"})
    public static /* final */ Map<Integer, p17> r; // = new HashMap();
    @DexIgnore
    public Context a;
    @DexIgnore
    public MethodChannel b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall a;
        @DexIgnore
        public /* final */ /* synthetic */ i b;
        @DexIgnore
        public /* final */ /* synthetic */ p17 c;

        @DexIgnore
        public a(MethodCall methodCall, i iVar, p17 p17) {
            this.a = methodCall;
            this.b = iVar;
            this.c = p17;
        }

        @DexIgnore
        public void run() {
            boolean unused = r17.this.d(this.c, new y17(this.a, this.b));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall a;
        @DexIgnore
        public /* final */ /* synthetic */ i b;
        @DexIgnore
        public /* final */ /* synthetic */ p17 c;

        @DexIgnore
        public b(MethodCall methodCall, i iVar, p17 p17) {
            this.a = methodCall;
            this.b = iVar;
            this.c = p17;
        }

        @DexIgnore
        public void run() {
            y17 y17 = new y17(this.a, this.b);
            boolean b2 = y17.b();
            boolean d2 = y17.d();
            ArrayList arrayList = new ArrayList();
            for (Map map : (List) this.a.argument("operations")) {
                w17 w17 = new w17(map, b2);
                String h = w17.h();
                char c2 = '\uffff';
                switch (h.hashCode()) {
                    case -1319569547:
                        if (h.equals("execute")) {
                            c2 = 0;
                            break;
                        }
                        break;
                    case -1183792455:
                        if (h.equals("insert")) {
                            c2 = 1;
                            break;
                        }
                        break;
                    case -838846263:
                        if (h.equals("update")) {
                            c2 = 3;
                            break;
                        }
                        break;
                    case 107944136:
                        if (h.equals(ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME)) {
                            c2 = 2;
                            break;
                        }
                        break;
                }
                if (c2 != 0) {
                    if (c2 != 1) {
                        if (c2 != 2) {
                            if (c2 != 3) {
                                i iVar = this.b;
                                iVar.error("bad_param", "Batch method '" + h + "' not supported", null);
                                return;
                            } else if (r17.this.e(this.c, w17)) {
                                w17.b(arrayList);
                            } else if (d2) {
                                w17.a(arrayList);
                            } else {
                                w17.a(this.b);
                                return;
                            }
                        } else if (r17.this.d(this.c, w17)) {
                            w17.b(arrayList);
                        } else if (d2) {
                            w17.a(arrayList);
                        } else {
                            w17.a(this.b);
                            return;
                        }
                    } else if (r17.this.c(this.c, w17)) {
                        w17.b(arrayList);
                    } else if (d2) {
                        w17.a(arrayList);
                    } else {
                        w17.a(this.b);
                        return;
                    }
                } else if (r17.this.a(this.c, w17)) {
                    w17.b(arrayList);
                } else if (d2) {
                    w17.a(arrayList);
                } else {
                    w17.a(this.b);
                    return;
                }
            }
            if (b2) {
                this.b.success(null);
            } else {
                this.b.success(arrayList);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall a;
        @DexIgnore
        public /* final */ /* synthetic */ i b;
        @DexIgnore
        public /* final */ /* synthetic */ p17 c;

        @DexIgnore
        public c(MethodCall methodCall, i iVar, p17 p17) {
            this.a = methodCall;
            this.b = iVar;
            this.c = p17;
        }

        @DexIgnore
        public void run() {
            boolean unused = r17.this.c(this.c, new y17(this.a, this.b));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ p17 a;
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall b;
        @DexIgnore
        public /* final */ /* synthetic */ i c;

        @DexIgnore
        public d(p17 p17, MethodCall methodCall, i iVar) {
            this.a = p17;
            this.b = methodCall;
            this.c = iVar;
        }

        @DexIgnore
        public void run() {
            if (r17.this.a(this.a, this.b, this.c) != null) {
                this.c.success(null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall a;
        @DexIgnore
        public /* final */ /* synthetic */ i b;
        @DexIgnore
        public /* final */ /* synthetic */ p17 c;

        @DexIgnore
        public e(MethodCall methodCall, i iVar, p17 p17) {
            this.a = methodCall;
            this.b = iVar;
            this.c = p17;
        }

        @DexIgnore
        public void run() {
            boolean unused = r17.this.e(this.c, new y17(this.a, this.b));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ i c;
        @DexIgnore
        public /* final */ /* synthetic */ Boolean d;
        @DexIgnore
        public /* final */ /* synthetic */ p17 e;
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall f;
        @DexIgnore
        public /* final */ /* synthetic */ boolean g;
        @DexIgnore
        public /* final */ /* synthetic */ int h;

        @DexIgnore
        public f(boolean z, String str, i iVar, Boolean bool, p17 p17, MethodCall methodCall, boolean z2, int i2) {
            this.a = z;
            this.b = str;
            this.c = iVar;
            this.d = bool;
            this.e = p17;
            this.f = methodCall;
            this.g = z2;
            this.h = i2;
        }

        @DexIgnore
        public void run() {
            synchronized (r17.h) {
                if (!this.a) {
                    File file = new File(new File(this.b).getParent());
                    if (!file.exists() && !file.mkdirs() && !file.exists()) {
                        i iVar = this.c;
                        iVar.error("sqlite_error", "open_failed " + this.b, null);
                        return;
                    }
                }
                try {
                    if (Boolean.TRUE.equals(this.d)) {
                        this.e.g();
                    } else {
                        this.e.f();
                    }
                    synchronized (r17.g) {
                        if (this.g) {
                            r17.c.put(this.b, Integer.valueOf(this.h));
                        }
                        r17.r.put(Integer.valueOf(this.h), this.e);
                    }
                    if (q17.a(this.e.d)) {
                        Log.d("Sqflite", this.e.c() + "opened " + this.h + " " + this.b);
                    }
                    this.c.success(r17.a(this.h, false, false));
                } catch (Exception e2) {
                    r17.this.a(e2, new y17(this.f, this.c), this.e);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ p17 a;
        @DexIgnore
        public /* final */ /* synthetic */ i b;

        @DexIgnore
        public g(p17 p17, i iVar) {
            this.a = p17;
            this.b = iVar;
        }

        @DexIgnore
        public void run() {
            synchronized (r17.h) {
                r17.this.a(this.a);
            }
            this.b.success(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ p17 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ i c;

        @DexIgnore
        public h(p17 p17, String str, i iVar) {
            this.a = p17;
            this.b = str;
            this.c = iVar;
        }

        @DexIgnore
        public void run() {
            synchronized (r17.h) {
                if (this.a != null) {
                    r17.this.a(this.a);
                }
                try {
                    if (q17.b(r17.f)) {
                        Log.d("Sqflite", "delete database " + this.b);
                    }
                    p17.a(this.b);
                } catch (Exception e) {
                    Log.e("Sqflite", "error " + e + " while closing database " + r17.j);
                }
            }
            this.c.success(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements MethodChannel.Result {
        @DexIgnore
        public /* final */ Handler a;
        @DexIgnore
        public /* final */ MethodChannel.Result b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Object a;

            @DexIgnore
            public a(Object obj) {
                this.a = obj;
            }

            @DexIgnore
            public void run() {
                i.this.b.success(this.a);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ String a;
            @DexIgnore
            public /* final */ /* synthetic */ String b;
            @DexIgnore
            public /* final */ /* synthetic */ Object c;

            @DexIgnore
            public b(String str, String str2, Object obj) {
                this.a = str;
                this.b = str2;
                this.c = obj;
            }

            @DexIgnore
            public void run() {
                i.this.b.error(this.a, this.b, this.c);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class c implements Runnable {
            @DexIgnore
            public c() {
            }

            @DexIgnore
            public void run() {
                i.this.b.notImplemented();
            }
        }

        @DexIgnore
        public /* synthetic */ i(r17 r17, MethodChannel.Result result, a aVar) {
            this(r17, result);
        }

        @DexIgnore
        @Override // io.flutter.plugin.common.MethodChannel.Result
        public void error(String str, String str2, Object obj) {
            this.a.post(new b(str, str2, obj));
        }

        @DexIgnore
        @Override // io.flutter.plugin.common.MethodChannel.Result
        public void notImplemented() {
            this.a.post(new c());
        }

        @DexIgnore
        @Override // io.flutter.plugin.common.MethodChannel.Result
        public void success(Object obj) {
            this.a.post(new a(obj));
        }

        @DexIgnore
        public i(r17 r17, MethodChannel.Result result) {
            this.a = new Handler();
            this.b = result;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0097  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean e(com.fossil.p17 r8, com.fossil.z17 r9) {
        /*
            r7 = this;
            boolean r0 = r7.b(r8, r9)
            r1 = 0
            if (r0 != 0) goto L_0x0008
            return r1
        L_0x0008:
            boolean r0 = r9.b()
            r2 = 1
            r3 = 0
            if (r0 == 0) goto L_0x0014
            r9.success(r3)
            return r2
        L_0x0014:
            android.database.sqlite.SQLiteDatabase r0 = r8.e()     // Catch:{ Exception -> 0x008b }
            java.lang.String r4 = "SELECT changes()"
            android.database.Cursor r0 = r0.rawQuery(r4, r3)     // Catch:{ Exception -> 0x008b }
            java.lang.String r4 = "Sqflite"
            if (r0 == 0) goto L_0x0068
            int r5 = r0.getCount()     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            if (r5 <= 0) goto L_0x0068
            boolean r5 = r0.moveToFirst()     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            if (r5 == 0) goto L_0x0068
            int r3 = r0.getInt(r1)     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            int r5 = r8.d     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            boolean r5 = com.fossil.q17.a(r5)     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            if (r5 == 0) goto L_0x0055
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            r5.<init>()     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            java.lang.String r6 = r8.c()     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            r5.append(r6)     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            java.lang.String r6 = "changed "
            r5.append(r6)     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            r5.append(r3)     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            android.util.Log.d(r4, r5)     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
        L_0x0055:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            r9.success(r3)     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            if (r0 == 0) goto L_0x0061
            r0.close()
        L_0x0061:
            return r2
        L_0x0062:
            r8 = move-exception
            r3 = r0
            goto L_0x0095
        L_0x0065:
            r2 = move-exception
            r3 = r0
            goto L_0x008c
        L_0x0068:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = r8.c()
            r5.append(r6)
            java.lang.String r6 = "fail to read changes for Update/Delete"
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            android.util.Log.e(r4, r5)
            r9.success(r3)
            if (r0 == 0) goto L_0x0088
            r0.close()
        L_0x0088:
            return r2
        L_0x0089:
            r8 = move-exception
            goto L_0x0095
        L_0x008b:
            r2 = move-exception
        L_0x008c:
            r7.a(r2, r9, r8)     // Catch:{ all -> 0x0089 }
            if (r3 == 0) goto L_0x0094
            r3.close()
        L_0x0094:
            return r1
        L_0x0095:
            if (r3 == 0) goto L_0x009a
            r3.close()
        L_0x009a:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.r17.e(com.fossil.p17, com.fossil.z17):boolean");
    }

    @DexIgnore
    public final void f(MethodCall methodCall, MethodChannel.Result result) {
        p17 p17;
        String str = (String) methodCall.argument("path");
        synchronized (g) {
            if (q17.b(f)) {
                Log.d("Sqflite", "Look for " + str + " in " + c.keySet());
            }
            Integer num = c.get(str);
            if (num == null || (p17 = r.get(num)) == null || !p17.e.isOpen()) {
                p17 = null;
            } else {
                if (q17.b(f)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(p17.c());
                    sb.append("found single instance ");
                    sb.append(p17.f ? "(in transaction) " : "");
                    sb.append(num);
                    sb.append(" ");
                    sb.append(str);
                    Log.d("Sqflite", sb.toString());
                }
                r.remove(num);
                c.remove(str);
            }
        }
        h hVar = new h(p17, str, new i(this, result, null));
        Handler handler = q;
        if (handler != null) {
            handler.post(hVar);
        } else {
            hVar.run();
        }
    }

    @DexIgnore
    public final void g(MethodCall methodCall, MethodChannel.Result result) {
        p17 a2 = a(methodCall, result);
        if (a2 != null) {
            q.post(new d(a2, methodCall, new i(this, result, null)));
        }
    }

    @DexIgnore
    public void h(MethodCall methodCall, MethodChannel.Result result) {
        if (i == null) {
            i = this.a.getDatabasePath("tekartik_sqflite.db").getParent();
        }
        result.success(i);
    }

    @DexIgnore
    public final void i(MethodCall methodCall, MethodChannel.Result result) {
        p17 a2 = a(methodCall, result);
        if (a2 != null) {
            q.post(new c(methodCall, new i(this, result, null), a2));
        }
    }

    @DexIgnore
    public final void j(MethodCall methodCall, MethodChannel.Result result) {
        int i2;
        p17 p17;
        String str = (String) methodCall.argument("path");
        Boolean bool = (Boolean) methodCall.argument(DatabaseFieldConfigLoader.FIELD_NAME_READ_ONLY);
        boolean a2 = a(str);
        boolean z = !Boolean.FALSE.equals(methodCall.argument("singleInstance")) && !a2;
        if (z) {
            synchronized (g) {
                if (q17.b(f)) {
                    Log.d("Sqflite", "Look for " + str + " in " + c.keySet());
                }
                Integer num = c.get(str);
                if (!(num == null || (p17 = r.get(num)) == null)) {
                    if (p17.e.isOpen()) {
                        if (q17.b(f)) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(p17.c());
                            sb.append("re-opened single instance ");
                            sb.append(p17.f ? "(in transaction) " : "");
                            sb.append(num);
                            sb.append(" ");
                            sb.append(str);
                            Log.d("Sqflite", sb.toString());
                        }
                        result.success(a(num.intValue(), true, p17.f));
                        return;
                    } else if (q17.b(f)) {
                        Log.d("Sqflite", p17.c() + "single instance database of " + str + " not opened");
                    }
                }
            }
        }
        synchronized (g) {
            i2 = j + 1;
            j = i2;
        }
        p17 p172 = new p17(str, i2, z, f);
        i iVar = new i(this, result, null);
        synchronized (g) {
            if (q == null) {
                HandlerThread handlerThread = new HandlerThread("Sqflite", e);
                p = handlerThread;
                handlerThread.start();
                q = new Handler(p.getLooper());
                if (q17.a(p172.d)) {
                    Log.d("Sqflite", p172.c() + "starting thread" + p + " priority " + e);
                }
            }
            if (q17.a(p172.d)) {
                Log.d("Sqflite", p172.c() + "opened " + i2 + " " + str);
            }
            q.post(new f(a2, str, iVar, bool, p172, methodCall, z, i2));
        }
    }

    @DexIgnore
    public void k(MethodCall methodCall, MethodChannel.Result result) {
        Object argument = methodCall.argument("queryAsMapList");
        if (argument != null) {
            d = Boolean.TRUE.equals(argument);
        }
        Object argument2 = methodCall.argument("androidThreadPriority");
        if (argument2 != null) {
            e = ((Integer) argument2).intValue();
        }
        Integer a2 = q17.a(methodCall);
        if (a2 != null) {
            f = a2.intValue();
        }
        result.success(null);
    }

    @DexIgnore
    public final void l(MethodCall methodCall, MethodChannel.Result result) {
        p17 a2 = a(methodCall, result);
        if (a2 != null) {
            q.post(new a(methodCall, new i(this, result, null), a2));
        }
    }

    @DexIgnore
    public final void m(MethodCall methodCall, MethodChannel.Result result) {
        p17 a2 = a(methodCall, result);
        if (a2 != null) {
            q.post(new e(methodCall, new i(this, result, null), a2));
        }
    }

    @DexIgnore
    @Override // io.flutter.embedding.engine.plugins.FlutterPlugin
    public void onAttachedToEngine(FlutterPlugin.FlutterPluginBinding flutterPluginBinding) {
        a(flutterPluginBinding.getApplicationContext(), flutterPluginBinding.getBinaryMessenger());
    }

    @DexIgnore
    @Override // io.flutter.embedding.engine.plugins.FlutterPlugin
    public void onDetachedFromEngine(FlutterPlugin.FlutterPluginBinding flutterPluginBinding) {
        this.a = null;
        this.b.setMethodCallHandler(null);
        this.b = null;
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // io.flutter.plugin.common.MethodChannel.MethodCallHandler
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        char c2;
        String str = methodCall.method;
        switch (str.hashCode()) {
            case -1319569547:
                if (str.equals("execute")) {
                    c2 = 5;
                    break;
                }
                c2 = '\uffff';
                break;
            case -1253581933:
                if (str.equals("closeDatabase")) {
                    c2 = 1;
                    break;
                }
                c2 = '\uffff';
                break;
            case -1249474914:
                if (str.equals("options")) {
                    c2 = '\b';
                    break;
                }
                c2 = '\uffff';
                break;
            case -1183792455:
                if (str.equals("insert")) {
                    c2 = 3;
                    break;
                }
                c2 = '\uffff';
                break;
            case -838846263:
                if (str.equals("update")) {
                    c2 = 4;
                    break;
                }
                c2 = '\uffff';
                break;
            case -263511994:
                if (str.equals("deleteDatabase")) {
                    c2 = '\n';
                    break;
                }
                c2 = '\uffff';
                break;
            case -198450538:
                if (str.equals("debugMode")) {
                    c2 = '\f';
                    break;
                }
                c2 = '\uffff';
                break;
            case -17190427:
                if (str.equals("openDatabase")) {
                    c2 = 6;
                    break;
                }
                c2 = '\uffff';
                break;
            case 93509434:
                if (str.equals(GraphRequest.BATCH_PARAM)) {
                    c2 = 7;
                    break;
                }
                c2 = '\uffff';
                break;
            case 95458899:
                if (str.equals("debug")) {
                    c2 = 11;
                    break;
                }
                c2 = '\uffff';
                break;
            case 107944136:
                if (str.equals(ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME)) {
                    c2 = 2;
                    break;
                }
                c2 = '\uffff';
                break;
            case 1385449135:
                if (str.equals("getPlatformVersion")) {
                    c2 = 0;
                    break;
                }
                c2 = '\uffff';
                break;
            case 1863829223:
                if (str.equals("getDatabasesPath")) {
                    c2 = '\t';
                    break;
                }
                c2 = '\uffff';
                break;
            default:
                c2 = '\uffff';
                break;
        }
        switch (c2) {
            case 0:
                result.success("Android " + Build.VERSION.RELEASE);
                return;
            case 1:
                c(methodCall, result);
                return;
            case 2:
                l(methodCall, result);
                return;
            case 3:
                i(methodCall, result);
                return;
            case 4:
                m(methodCall, result);
                return;
            case 5:
                g(methodCall, result);
                return;
            case 6:
                j(methodCall, result);
                return;
            case 7:
                b(methodCall, result);
                return;
            case '\b':
                k(methodCall, result);
                return;
            case '\t':
                h(methodCall, result);
                return;
            case '\n':
                f(methodCall, result);
                return;
            case 11:
                d(methodCall, result);
                return;
            case '\f':
                e(methodCall, result);
                return;
            default:
                result.notImplemented();
                return;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00cd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean d(com.fossil.p17 r13, com.fossil.z17 r14) {
        /*
            r12 = this;
            com.fossil.s17 r0 = r14.a()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            int r2 = r13.d
            boolean r2 = com.fossil.q17.a(r2)
            java.lang.String r3 = "Sqflite"
            if (r2 == 0) goto L_0x0029
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = r13.c()
            r2.append(r4)
            r2.append(r0)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r3, r2)
        L_0x0029:
            boolean r2 = com.fossil.r17.d
            r4 = 0
            r5 = 0
            com.fossil.s17 r0 = r0.e()     // Catch:{ Exception -> 0x00c1 }
            android.database.sqlite.SQLiteDatabase r6 = r13.b()     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r7 = r0.c()     // Catch:{ Exception -> 0x00c1 }
            java.lang.String[] r0 = r0.a()     // Catch:{ Exception -> 0x00c1 }
            android.database.Cursor r0 = r6.rawQuery(r7, r0)     // Catch:{ Exception -> 0x00c1 }
            r6 = r5
            r7 = 0
        L_0x0043:
            boolean r8 = r0.moveToNext()     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            if (r8 == 0) goto L_0x00a2
            if (r2 == 0) goto L_0x0075
            java.util.Map r8 = a(r0)     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            int r9 = r13.d     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            boolean r9 = com.fossil.q17.a(r9)     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            if (r9 == 0) goto L_0x0071
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            r9.<init>()     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            java.lang.String r10 = r13.c()     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            r9.append(r10)     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            java.lang.String r10 = a(r8)     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            r9.append(r10)     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            android.util.Log.d(r3, r9)     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
        L_0x0071:
            r1.add(r8)     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            goto L_0x0043
        L_0x0075:
            if (r5 != 0) goto L_0x009a
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            r5.<init>()     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            java.util.HashMap r6 = new java.util.HashMap     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            r6.<init>()     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            int r7 = r0.getColumnCount()     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            java.lang.String r8 = "columns"
            java.lang.String[] r9 = r0.getColumnNames()     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            java.util.List r9 = java.util.Arrays.asList(r9)     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            r6.put(r8, r9)     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            java.lang.String r8 = "rows"
            r6.put(r8, r5)     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            r11 = r6
            r6 = r5
            r5 = r11
        L_0x009a:
            java.util.List r8 = a(r0, r7)     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            r6.add(r8)     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            goto L_0x0043
        L_0x00a2:
            if (r2 == 0) goto L_0x00a8
            r14.success(r1)     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            goto L_0x00b2
        L_0x00a8:
            if (r5 != 0) goto L_0x00af
            java.util.HashMap r5 = new java.util.HashMap     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
            r5.<init>()     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
        L_0x00af:
            r14.success(r5)     // Catch:{ Exception -> 0x00bc, all -> 0x00b9 }
        L_0x00b2:
            r13 = 1
            if (r0 == 0) goto L_0x00b8
            r0.close()
        L_0x00b8:
            return r13
        L_0x00b9:
            r13 = move-exception
            r5 = r0
            goto L_0x00cb
        L_0x00bc:
            r1 = move-exception
            r5 = r0
            goto L_0x00c2
        L_0x00bf:
            r13 = move-exception
            goto L_0x00cb
        L_0x00c1:
            r1 = move-exception
        L_0x00c2:
            r12.a(r1, r14, r13)     // Catch:{ all -> 0x00bf }
            if (r5 == 0) goto L_0x00ca
            r5.close()
        L_0x00ca:
            return r4
        L_0x00cb:
            if (r5 == 0) goto L_0x00d0
            r5.close()
        L_0x00d0:
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.r17.d(com.fossil.p17, com.fossil.z17):boolean");
    }

    @DexIgnore
    public static Object b(Cursor cursor, int i2) {
        int type = cursor.getType(i2);
        if (type == 1) {
            return Long.valueOf(cursor.getLong(i2));
        }
        if (type == 2) {
            return Double.valueOf(cursor.getDouble(i2));
        }
        if (type == 3) {
            return cursor.getString(i2);
        }
        if (type != 4) {
            return null;
        }
        return cursor.getBlob(i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00d3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean c(com.fossil.p17 r10, com.fossil.z17 r11) {
        /*
            r9 = this;
            boolean r0 = r9.b(r10, r11)
            r1 = 0
            if (r0 != 0) goto L_0x0008
            return r1
        L_0x0008:
            boolean r0 = r11.b()
            r2 = 0
            r3 = 1
            if (r0 == 0) goto L_0x0014
            r11.success(r2)
            return r3
        L_0x0014:
            java.lang.String r0 = "SELECT changes(), last_insert_rowid()"
            android.database.sqlite.SQLiteDatabase r4 = r10.e()     // Catch:{ Exception -> 0x00c2, all -> 0x00c0 }
            android.database.Cursor r0 = r4.rawQuery(r0, r2)     // Catch:{ Exception -> 0x00c2, all -> 0x00c0 }
            java.lang.String r4 = "Sqflite"
            if (r0 == 0) goto L_0x009f
            int r5 = r0.getCount()     // Catch:{ Exception -> 0x009d }
            if (r5 <= 0) goto L_0x009f
            boolean r5 = r0.moveToFirst()     // Catch:{ Exception -> 0x009d }
            if (r5 == 0) goto L_0x009f
            int r5 = r0.getInt(r1)     // Catch:{ Exception -> 0x009d }
            if (r5 != 0) goto L_0x0069
            int r5 = r10.d     // Catch:{ Exception -> 0x009d }
            boolean r5 = com.fossil.q17.a(r5)     // Catch:{ Exception -> 0x009d }
            if (r5 == 0) goto L_0x0060
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009d }
            r5.<init>()     // Catch:{ Exception -> 0x009d }
            java.lang.String r6 = r10.c()     // Catch:{ Exception -> 0x009d }
            r5.append(r6)     // Catch:{ Exception -> 0x009d }
            java.lang.String r6 = "no changes (id was "
            r5.append(r6)     // Catch:{ Exception -> 0x009d }
            long r6 = r0.getLong(r3)     // Catch:{ Exception -> 0x009d }
            r5.append(r6)     // Catch:{ Exception -> 0x009d }
            java.lang.String r6 = ")"
            r5.append(r6)     // Catch:{ Exception -> 0x009d }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x009d }
            android.util.Log.d(r4, r5)     // Catch:{ Exception -> 0x009d }
        L_0x0060:
            r11.success(r2)     // Catch:{ Exception -> 0x009d }
            if (r0 == 0) goto L_0x0068
            r0.close()
        L_0x0068:
            return r3
        L_0x0069:
            long r5 = r0.getLong(r3)
            int r2 = r10.d
            boolean r2 = com.fossil.q17.a(r2)
            if (r2 == 0) goto L_0x0090
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r7 = r10.c()
            r2.append(r7)
            java.lang.String r7 = "inserted "
            r2.append(r7)
            r2.append(r5)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r4, r2)
        L_0x0090:
            java.lang.Long r2 = java.lang.Long.valueOf(r5)
            r11.success(r2)
            if (r0 == 0) goto L_0x009c
            r0.close()
        L_0x009c:
            return r3
        L_0x009d:
            r2 = move-exception
            goto L_0x00c6
        L_0x009f:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = r10.c()
            r5.append(r6)
            java.lang.String r6 = "fail to read changes for Insert"
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            android.util.Log.e(r4, r5)
            r11.success(r2)
            if (r0 == 0) goto L_0x00bf
            r0.close()
        L_0x00bf:
            return r3
        L_0x00c0:
            r10 = move-exception
            goto L_0x00d1
        L_0x00c2:
            r0 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
        L_0x00c6:
            r9.a(r2, r11, r10)     // Catch:{ all -> 0x00cf }
            if (r0 == 0) goto L_0x00ce
            r0.close()
        L_0x00ce:
            return r1
        L_0x00cf:
            r10 = move-exception
            r2 = r0
        L_0x00d1:
            if (r2 == 0) goto L_0x00d6
            r2.close()
        L_0x00d6:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.r17.c(com.fossil.p17, com.fossil.z17):boolean");
    }

    @DexIgnore
    public final void a(Context context, BinaryMessenger binaryMessenger) {
        this.a = context;
        MethodChannel methodChannel = new MethodChannel(binaryMessenger, "com.tekartik.sqflite");
        this.b = methodChannel;
        methodChannel.setMethodCallHandler(this);
    }

    @DexIgnore
    public final boolean b(p17 p17, z17 z17) {
        s17 a2 = z17.a();
        if (q17.a(p17.d)) {
            Log.d("Sqflite", p17.c() + a2);
        }
        Boolean c2 = z17.c();
        try {
            p17.e().execSQL(a2.c(), a2.d());
            if (Boolean.TRUE.equals(c2)) {
                p17.f = true;
            }
            if (Boolean.FALSE.equals(c2)) {
                p17.f = false;
            }
            return true;
        } catch (Exception e2) {
            a(e2, z17, p17);
            if (Boolean.FALSE.equals(c2)) {
                p17.f = false;
            }
            return false;
        } catch (Throwable th) {
            if (Boolean.FALSE.equals(c2)) {
                p17.f = false;
            }
            throw th;
        }
    }

    @DexIgnore
    public static List<Object> a(Cursor cursor, int i2) {
        String str;
        ArrayList arrayList = new ArrayList(i2);
        for (int i3 = 0; i3 < i2; i3++) {
            Object b2 = b(cursor, i3);
            if (t17.c) {
                String str2 = null;
                if (b2 != null) {
                    if (b2.getClass().isArray()) {
                        str2 = "array(" + b2.getClass().getComponentType().getName() + ")";
                    } else {
                        str2 = b2.getClass().getName();
                    }
                }
                StringBuilder sb = new StringBuilder();
                sb.append("column ");
                sb.append(i3);
                sb.append(" ");
                sb.append(cursor.getType(i3));
                sb.append(": ");
                sb.append(b2);
                if (str2 == null) {
                    str = "";
                } else {
                    str = " (" + str2 + ")";
                }
                sb.append(str);
                Log.d("Sqflite", sb.toString());
            }
            arrayList.add(b2);
        }
        return arrayList;
    }

    @DexIgnore
    public static Map<String, Object> a(Cursor cursor) {
        HashMap hashMap = new HashMap();
        String[] columnNames = cursor.getColumnNames();
        int length = columnNames.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (t17.c) {
                Log.d("Sqflite", "column " + i2 + " " + cursor.getType(i2));
            }
            int type = cursor.getType(i2);
            if (type == 0) {
                hashMap.put(columnNames[i2], null);
            } else if (type == 1) {
                hashMap.put(columnNames[i2], Long.valueOf(cursor.getLong(i2)));
            } else if (type == 2) {
                hashMap.put(columnNames[i2], Double.valueOf(cursor.getDouble(i2)));
            } else if (type == 3) {
                hashMap.put(columnNames[i2], cursor.getString(i2));
            } else if (type == 4) {
                hashMap.put(columnNames[i2], cursor.getBlob(i2));
            }
        }
        return hashMap;
    }

    @DexIgnore
    public final void e(MethodCall methodCall, MethodChannel.Result result) {
        t17.a = Boolean.TRUE.equals(methodCall.arguments());
        t17.c = t17.b && t17.a;
        if (!t17.a) {
            f = 0;
        } else if (t17.c) {
            f = 2;
        } else if (t17.a) {
            f = 1;
        }
        result.success(null);
    }

    @DexIgnore
    public final void b(MethodCall methodCall, MethodChannel.Result result) {
        p17 a2 = a(methodCall, result);
        if (a2 != null) {
            q.post(new b(methodCall, new i(this, result, null), a2));
        }
    }

    @DexIgnore
    public final void c(MethodCall methodCall, MethodChannel.Result result) {
        int intValue = ((Integer) methodCall.argument("id")).intValue();
        p17 a2 = a(methodCall, result);
        if (a2 != null) {
            if (q17.a(a2.d)) {
                Log.d("Sqflite", a2.c() + "closing " + intValue + " " + a2.b);
            }
            String str = a2.b;
            synchronized (g) {
                r.remove(Integer.valueOf(intValue));
                if (a2.a) {
                    c.remove(str);
                }
            }
            q.post(new g(a2, new i(this, result, null)));
        }
    }

    @DexIgnore
    public final void d(MethodCall methodCall, MethodChannel.Result result) {
        HashMap hashMap = new HashMap();
        if ("get".equals((String) methodCall.argument("cmd"))) {
            int i2 = f;
            if (i2 > 0) {
                hashMap.put("logLevel", Integer.valueOf(i2));
            }
            if (!r.isEmpty()) {
                HashMap hashMap2 = new HashMap();
                for (Map.Entry<Integer, p17> entry : r.entrySet()) {
                    p17 value = entry.getValue();
                    HashMap hashMap3 = new HashMap();
                    hashMap3.put("path", value.b);
                    hashMap3.put("singleInstance", Boolean.valueOf(value.a));
                    int i3 = value.d;
                    if (i3 > 0) {
                        hashMap3.put("logLevel", Integer.valueOf(i3));
                    }
                    hashMap2.put(entry.getKey().toString(), hashMap3);
                }
                hashMap.put("databases", hashMap2);
            }
        }
        result.success(hashMap);
    }

    @DexIgnore
    public static Map<String, Object> a(Map<Object, Object> map) {
        Object obj;
        HashMap hashMap = new HashMap();
        for (Map.Entry<Object, Object> entry : map.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof Map) {
                obj = a((Map<Object, Object>) ((Map) value));
            } else {
                obj = a(value);
            }
            hashMap.put(a(entry.getKey()), obj);
        }
        return hashMap;
    }

    @DexIgnore
    public static String a(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof byte[]) {
            ArrayList arrayList = new ArrayList();
            for (byte b2 : (byte[]) obj) {
                arrayList.add(Integer.valueOf(b2));
            }
            return arrayList.toString();
        } else if (obj instanceof Map) {
            return a((Map<Object, Object>) ((Map) obj)).toString();
        } else {
            return obj.toString();
        }
    }

    @DexIgnore
    public static boolean a(String str) {
        return str == null || str.equals(SQLiteDatabase.MEMORY);
    }

    @DexIgnore
    public final p17 a(int i2) {
        return r.get(Integer.valueOf(i2));
    }

    @DexIgnore
    public final p17 a(MethodCall methodCall, MethodChannel.Result result) {
        int intValue = ((Integer) methodCall.argument("id")).intValue();
        p17 a2 = a(intValue);
        if (a2 != null) {
            return a2;
        }
        result.error("sqlite_error", "database_closed " + intValue, null);
        return null;
    }

    @DexIgnore
    public final s17 a(MethodCall methodCall) {
        return new s17((String) methodCall.argument("sql"), (List) methodCall.argument("arguments"));
    }

    @DexIgnore
    public final p17 a(p17 p17, MethodCall methodCall, MethodChannel.Result result) {
        if (b(p17, new x17(result, a(methodCall), (Boolean) methodCall.argument("inTransaction")))) {
            return p17;
        }
        return null;
    }

    @DexIgnore
    public final boolean a(p17 p17, z17 z17) {
        if (!b(p17, z17)) {
            return false;
        }
        z17.success(null);
        return true;
    }

    @DexIgnore
    public final void a(Exception exc, z17 z17, p17 p17) {
        if (exc instanceof SQLiteCantOpenDatabaseException) {
            z17.error("sqlite_error", "open_failed " + p17.b, null);
        } else if (exc instanceof SQLException) {
            z17.error("sqlite_error", exc.getMessage(), b27.a(z17));
        } else {
            z17.error("sqlite_error", exc.getMessage(), b27.a(z17));
        }
    }

    @DexIgnore
    public static Map a(int i2, boolean z, boolean z2) {
        HashMap hashMap = new HashMap();
        hashMap.put("id", Integer.valueOf(i2));
        if (z) {
            hashMap.put("recovered", true);
        }
        if (z2) {
            hashMap.put("recoveredInTransaction", true);
        }
        return hashMap;
    }

    @DexIgnore
    public final void a(p17 p17) {
        try {
            if (q17.a(p17.d)) {
                Log.d("Sqflite", p17.c() + "closing database " + p);
            }
            p17.a();
        } catch (Exception e2) {
            Log.e("Sqflite", "error " + e2 + " while closing database " + j);
        }
        synchronized (g) {
            if (r.isEmpty() && q != null) {
                if (q17.a(p17.d)) {
                    Log.d("Sqflite", p17.c() + "stopping thread" + p);
                }
                p.quit();
                p = null;
                q = null;
            }
        }
    }
}
