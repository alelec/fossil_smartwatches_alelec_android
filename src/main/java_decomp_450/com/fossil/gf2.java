package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gf2 extends bf2 implements ef2 {
    @DexIgnore
    public gf2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
    }

    @DexIgnore
    @Override // com.fossil.ef2
    public final boolean b(boolean z) throws RemoteException {
        Parcel E = E();
        df2.a(E, true);
        Parcel a = a(2, E);
        boolean a2 = df2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.ef2
    public final String getId() throws RemoteException {
        Parcel a = a(1, E());
        String readString = a.readString();
        a.recycle();
        return readString;
    }
}
