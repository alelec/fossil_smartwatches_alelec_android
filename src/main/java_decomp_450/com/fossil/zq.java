package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zq {
    @DexIgnore
    public int[] a;
    @DexIgnore
    public int b;

    @DexIgnore
    public zq(int i) {
        this.a = new int[i];
    }

    @DexIgnore
    public final boolean a(int i) {
        int a2 = s97.a(this.a, i, 0, this.b, 2, null);
        boolean z = a2 < 0;
        if (z) {
            this.a = xq.a.a(this.a, this.b, ~a2, i);
            this.b++;
        }
        return z;
    }

    @DexIgnore
    public final boolean b(int i) {
        int a2 = s97.a(this.a, i, 0, this.b, 2, null);
        boolean z = a2 >= 0;
        if (z) {
            c(a2);
        }
        return z;
    }

    @DexIgnore
    public final void c(int i) {
        int[] iArr = this.a;
        int i2 = i + 1;
        System.arraycopy(iArr, i2, iArr, i, this.b - i2);
        this.b--;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ zq(int i, int i2, zd7 zd7) {
        this((i2 & 1) != 0 ? 10 : i);
    }
}
