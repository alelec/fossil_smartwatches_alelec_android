package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dt4 implements Factory<ct4> {
    @DexIgnore
    public /* final */ Provider<ch5> a;
    @DexIgnore
    public /* final */ Provider<xo4> b;

    @DexIgnore
    public dt4(Provider<ch5> provider, Provider<xo4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static dt4 a(Provider<ch5> provider, Provider<xo4> provider2) {
        return new dt4(provider, provider2);
    }

    @DexIgnore
    public static ct4 a(ch5 ch5, xo4 xo4) {
        return new ct4(ch5, xo4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ct4 get() {
        return a(this.a.get(), this.b.get());
    }
}
