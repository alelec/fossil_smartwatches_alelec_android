package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.fossil.y62;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class id2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<id2> CREATOR; // = new md2();
    @DexIgnore
    public static /* final */ TimeUnit e; // = TimeUnit.MILLISECONDS;
    @DexIgnore
    public /* final */ lc2 a;
    @DexIgnore
    public /* final */ List<DataSet> b;
    @DexIgnore
    public /* final */ List<DataPoint> c;
    @DexIgnore
    public /* final */ zi2 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public lc2 a;
        @DexIgnore
        public List<DataSet> b; // = new ArrayList();
        @DexIgnore
        public List<DataPoint> c; // = new ArrayList();
        @DexIgnore
        public List<gc2> d; // = new ArrayList();

        @DexIgnore
        public a a(lc2 lc2) {
            this.a = lc2;
            return this;
        }

        @DexIgnore
        public a a(DataSet dataSet) {
            a72.a(dataSet != null, "Must specify a valid data set.");
            gc2 v = dataSet.v();
            a72.b(!this.d.contains(v), "Data set for this data source %s is already added.", v);
            a72.a(!dataSet.g().isEmpty(), "No data points specified in the input data set.");
            this.d.add(v);
            this.b.add(dataSet);
            return this;
        }

        @DexIgnore
        public id2 a() {
            boolean z = true;
            a72.b(this.a != null, "Must specify a valid session.");
            if (this.a.a(TimeUnit.MILLISECONDS) == 0) {
                z = false;
            }
            a72.b(z, "Must specify a valid end time, cannot insert a continuing session.");
            for (DataSet dataSet : this.b) {
                for (DataPoint dataPoint : dataSet.g()) {
                    a(dataPoint);
                }
            }
            for (DataPoint dataPoint2 : this.c) {
                a(dataPoint2);
            }
            return new id2(this);
        }

        @DexIgnore
        public final void a(DataPoint dataPoint) {
            long b2 = this.a.b(TimeUnit.NANOSECONDS);
            long a2 = this.a.a(TimeUnit.NANOSECONDS);
            long c2 = dataPoint.c(TimeUnit.NANOSECONDS);
            if (c2 != 0) {
                if (c2 < b2 || c2 > a2) {
                    c2 = xj2.a(c2, TimeUnit.NANOSECONDS, id2.e);
                }
                a72.b(c2 >= b2 && c2 <= a2, "Data point %s has time stamp outside session interval [%d, %d]", dataPoint, Long.valueOf(b2), Long.valueOf(a2));
                if (dataPoint.c(TimeUnit.NANOSECONDS) != c2) {
                    Log.w("Fitness", String.format("Data point timestamp [%d] is truncated to [%d] to match the precision [%s] of the session start and end time", Long.valueOf(dataPoint.c(TimeUnit.NANOSECONDS)), Long.valueOf(c2), id2.e));
                    dataPoint.a(c2, TimeUnit.NANOSECONDS);
                }
            }
            long b3 = this.a.b(TimeUnit.NANOSECONDS);
            long a3 = this.a.a(TimeUnit.NANOSECONDS);
            long b4 = dataPoint.b(TimeUnit.NANOSECONDS);
            long a4 = dataPoint.a(TimeUnit.NANOSECONDS);
            if (b4 != 0 && a4 != 0) {
                if (a4 > a3) {
                    a4 = xj2.a(a4, TimeUnit.NANOSECONDS, id2.e);
                }
                a72.b(b4 >= b3 && a4 <= a3, "Data point %s has start and end times outside session interval [%d, %d]", dataPoint, Long.valueOf(b3), Long.valueOf(a3));
                if (a4 != dataPoint.a(TimeUnit.NANOSECONDS)) {
                    Log.w("Fitness", String.format("Data point end time [%d] is truncated to [%d] to match the precision [%s] of the session start and end time", Long.valueOf(dataPoint.a(TimeUnit.NANOSECONDS)), Long.valueOf(a4), id2.e));
                    dataPoint.a(b4, a4, TimeUnit.NANOSECONDS);
                }
            }
        }
    }

    @DexIgnore
    public id2(lc2 lc2, List<DataSet> list, List<DataPoint> list2, IBinder iBinder) {
        this.a = lc2;
        this.b = Collections.unmodifiableList(list);
        this.c = Collections.unmodifiableList(list2);
        this.d = bj2.a(iBinder);
    }

    @DexIgnore
    public List<DataPoint> e() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof id2) {
                id2 id2 = (id2) obj;
                if (y62.a(this.a, id2.a) && y62.a(this.b, id2.b) && y62.a(this.c, id2.c)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public List<DataSet> g() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return y62.a(this.a, this.b, this.c);
    }

    @DexIgnore
    public String toString() {
        y62.a a2 = y62.a(this);
        a2.a(Constants.SESSION, this.a);
        a2.a("dataSets", this.b);
        a2.a("aggregateDataPoints", this.c);
        return a2.toString();
    }

    @DexIgnore
    public lc2 v() {
        return this.a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, (Parcelable) v(), i, false);
        k72.c(parcel, 2, g(), false);
        k72.c(parcel, 3, e(), false);
        zi2 zi2 = this.d;
        k72.a(parcel, 4, zi2 == null ? null : zi2.asBinder(), false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public id2(a aVar) {
        this(aVar.a, aVar.b, aVar.c, (zi2) null);
    }

    @DexIgnore
    public id2(id2 id2, zi2 zi2) {
        this(id2.a, id2.b, id2.c, zi2);
    }

    @DexIgnore
    public id2(lc2 lc2, List<DataSet> list, List<DataPoint> list2, zi2 zi2) {
        this.a = lc2;
        this.b = Collections.unmodifiableList(list);
        this.c = Collections.unmodifiableList(list2);
        this.d = zi2;
    }
}
