package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o34 extends b44 {
    @DexIgnore
    public /* final */ v54 a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public o34(v54 v54, String str) {
        if (v54 != null) {
            this.a = v54;
            if (str != null) {
                this.b = str;
                return;
            }
            throw new NullPointerException("Null sessionId");
        }
        throw new NullPointerException("Null report");
    }

    @DexIgnore
    @Override // com.fossil.b44
    public v54 a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.b44
    public String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof b44)) {
            return false;
        }
        b44 b44 = (b44) obj;
        if (!this.a.equals(b44.a()) || !this.b.equals(b44.b())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "CrashlyticsReportWithSessionId{report=" + this.a + ", sessionId=" + this.b + "}";
    }
}
