package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jv5 extends dy6 implements iv5 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public /* final */ pb j; // = new pm4(this);
    @DexIgnore
    public qw6<u05> p;
    @DexIgnore
    public hv5 q;
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return jv5.s;
        }

        @DexIgnore
        public final jv5 b() {
            return new jv5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ u05 a;
        @DexIgnore
        public /* final */ /* synthetic */ jv5 b;

        @DexIgnore
        public b(u05 u05, jv5 jv5, boolean z) {
            this.a = u05;
            this.b = jv5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            hv5 a2 = jv5.a(this.b);
            NumberPicker numberPicker2 = this.a.t;
            ee7.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            String valueOf2 = String.valueOf(i2);
            NumberPicker numberPicker3 = this.a.u;
            ee7.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ u05 a;
        @DexIgnore
        public /* final */ /* synthetic */ jv5 b;

        @DexIgnore
        public c(u05 u05, jv5 jv5, boolean z) {
            this.a = u05;
            this.b = jv5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            hv5 a2 = jv5.a(this.b);
            String valueOf = String.valueOf(i2);
            NumberPicker numberPicker2 = this.a.v;
            ee7.a((Object) numberPicker2, "binding.numberPickerTwo");
            a2.a(valueOf, String.valueOf(numberPicker2.getValue()), false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ u05 a;
        @DexIgnore
        public /* final */ /* synthetic */ jv5 b;

        @DexIgnore
        public d(u05 u05, jv5 jv5, boolean z) {
            this.a = u05;
            this.b = jv5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            hv5 a2 = jv5.a(this.b);
            String valueOf = String.valueOf(i2);
            NumberPicker numberPicker2 = this.a.v;
            ee7.a((Object) numberPicker2, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.a.u;
            ee7.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ u05 a;
        @DexIgnore
        public /* final */ /* synthetic */ jv5 b;

        @DexIgnore
        public e(u05 u05, jv5 jv5, boolean z) {
            this.a = u05;
            this.b = jv5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            hv5 a2 = jv5.a(this.b);
            NumberPicker numberPicker2 = this.a.t;
            ee7.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.a.v;
            ee7.a((Object) numberPicker3, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker3.getValue());
            boolean z = true;
            if (i2 != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jv5 a;

        @DexIgnore
        public f(jv5 jv5) {
            this.a = jv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            jv5.a(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ u05 a;
        @DexIgnore
        public /* final */ /* synthetic */ se7 b;

        @DexIgnore
        public g(u05 u05, se7 se7) {
            this.a = u05;
            this.b = se7;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.a.w;
            ee7.a((Object) constraintLayout, "it.rootBackground");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                ViewGroup.LayoutParams layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) ((CoordinatorLayout.e) layoutParams).d();
                    if (bottomSheetBehavior != null) {
                        bottomSheetBehavior.e(3);
                        u05 u05 = this.a;
                        ee7.a((Object) u05, "it");
                        View d = u05.d();
                        ee7.a((Object) d, "it.root");
                        d.getViewTreeObserver().removeOnGlobalLayoutListener(this.b.element);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                throw new x87("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new x87("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = jv5.class.getSimpleName();
        ee7.a((Object) simpleName, "InactivityNudgeTimeFragment::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ hv5 a(jv5 jv5) {
        hv5 hv5 = jv5.q;
        if (hv5 != null) {
            return hv5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.iv5
    public void Q(boolean z) {
        qw6<u05> qw6 = this.p;
        if (qw6 != null) {
            u05 a2 = qw6.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.v;
                ee7.a((Object) numberPicker, "binding.numberPickerTwo");
                numberPicker.setMinValue(0);
                NumberPicker numberPicker2 = a2.v;
                ee7.a((Object) numberPicker2, "binding.numberPickerTwo");
                numberPicker2.setMaxValue(59);
                a2.v.setOnValueChangedListener(new b(a2, this, z));
                if (z) {
                    NumberPicker numberPicker3 = a2.t;
                    ee7.a((Object) numberPicker3, "binding.numberPickerOne");
                    numberPicker3.setMinValue(0);
                    NumberPicker numberPicker4 = a2.t;
                    ee7.a((Object) numberPicker4, "binding.numberPickerOne");
                    numberPicker4.setMaxValue(23);
                    a2.t.setOnValueChangedListener(new c(a2, this, z));
                    NumberPicker numberPicker5 = a2.u;
                    ee7.a((Object) numberPicker5, "binding.numberPickerThree");
                    numberPicker5.setVisibility(8);
                    return;
                }
                NumberPicker numberPicker6 = a2.t;
                ee7.a((Object) numberPicker6, "binding.numberPickerOne");
                numberPicker6.setMinValue(1);
                NumberPicker numberPicker7 = a2.t;
                ee7.a((Object) numberPicker7, "binding.numberPickerOne");
                numberPicker7.setMaxValue(12);
                a2.t.setOnValueChangedListener(new d(a2, this, z));
                String[] strArr = {ig5.a(PortfolioApp.g0.c(), 2131886102), ig5.a(PortfolioApp.g0.c(), 2131886104)};
                NumberPicker numberPicker8 = a2.u;
                ee7.a((Object) numberPicker8, "binding.numberPickerThree");
                numberPicker8.setVisibility(0);
                NumberPicker numberPicker9 = a2.u;
                ee7.a((Object) numberPicker9, "binding.numberPickerThree");
                numberPicker9.setMinValue(0);
                NumberPicker numberPicker10 = a2.u;
                ee7.a((Object) numberPicker10, "binding.numberPickerThree");
                numberPicker10.setMaxValue(1);
                a2.u.setDisplayedValues(strArr);
                a2.u.setOnValueChangedListener(new e(a2, this, z));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.dy6
    public void a1() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.iv5
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    @Override // com.fossil.iv5
    public void i(String str) {
        ee7.b(str, "description");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager requireFragmentManager = requireFragmentManager();
            ee7.a((Object) requireFragmentManager, "requireFragmentManager()");
            bx6.a(requireFragmentManager, str);
        }
    }

    @DexIgnore
    public final void n(int i) {
        hv5 hv5 = this.q;
        if (hv5 != null) {
            hv5.a(i);
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        u05 u05 = (u05) qb.a(layoutInflater, 2131558549, viewGroup, false, this.j);
        String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(b2)) {
            u05.w.setBackgroundColor(Color.parseColor(b2));
        }
        u05.q.setOnClickListener(new f(this));
        this.p = new qw6<>(this, u05);
        ee7.a((Object) u05, "binding");
        return u05.d();
    }

    @DexIgnore
    @Override // com.fossil.dy6, androidx.fragment.app.Fragment, com.fossil.ac
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a1();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        hv5 hv5 = this.q;
        if (hv5 != null) {
            hv5.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        hv5 hv5 = this.q;
        if (hv5 != null) {
            hv5.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<u05> qw6 = this.p;
        if (qw6 != null) {
            u05 a2 = qw6.a();
            if (a2 != null) {
                se7 se7 = new se7();
                se7.element = null;
                se7.element = (T) new g(a2, se7);
                ee7.a((Object) a2, "it");
                View d2 = a2.d();
                ee7.a((Object) d2, "it.root");
                d2.getViewTreeObserver().addOnGlobalLayoutListener(se7.element);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(hv5 hv5) {
        ee7.b(hv5, "presenter");
        this.q = hv5;
    }

    @DexIgnore
    @Override // com.fossil.iv5
    public void a(String str) {
        FlexibleTextView flexibleTextView;
        ee7.b(str, "title");
        qw6<u05> qw6 = this.p;
        if (qw6 != null) {
            u05 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.r) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.iv5
    public void a(int i, boolean z) {
        int i2 = i / 60;
        int i3 = i % 60;
        int i4 = 0;
        if (!z) {
            if (i2 >= 12) {
                i4 = 1;
                i2 -= 12;
            }
            if (i2 == 0) {
                i2 = 12;
            }
        }
        qw6<u05> qw6 = this.p;
        if (qw6 != null) {
            u05 a2 = qw6.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.t;
                ee7.a((Object) numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i2);
                NumberPicker numberPicker2 = a2.v;
                ee7.a((Object) numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i3);
                NumberPicker numberPicker3 = a2.u;
                ee7.a((Object) numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i4);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }
}
