package com.fossil;

import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class hu {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[br.values().length];
        a = iArr;
        iArr[br.MEMORY.ordinal()] = 1;
        a[br.DISK.ordinal()] = 2;
        a[br.NETWORK.ordinal()] = 3;
        int[] iArr2 = new int[ImageView.ScaleType.values().length];
        b = iArr2;
        iArr2[ImageView.ScaleType.FIT_START.ordinal()] = 1;
        b[ImageView.ScaleType.FIT_CENTER.ordinal()] = 2;
        b[ImageView.ScaleType.FIT_END.ordinal()] = 3;
        b[ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 4;
    }
    */
}
