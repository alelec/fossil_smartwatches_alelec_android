package com.fossil;

import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import java.util.ArrayList;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b26 extends x16 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public SecondTimezoneSetting e;
    @DexIgnore
    public ArrayList<SecondTimezoneSetting> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Gson g; // = new Gson();
    @DexIgnore
    public /* final */ y16 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1", f = "SearchSecondTimezonePresenter.kt", l = {37, 38}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ b26 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$rawDataList$1", f = "SearchSecondTimezonePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ArrayList<SecondTimezoneSetting>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ArrayList<SecondTimezoneSetting>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return rd5.g.g();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.b26$b$b")
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1", f = "SearchSecondTimezonePresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.b26$b$b  reason: collision with other inner class name */
        public static final class C0014b extends zb7 implements kd7<yi7, fb7<? super ArrayList<SecondTimezoneSetting>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $rawDataList;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.b26$b$b$a")
            /* renamed from: com.fossil.b26$b$b$a */
            public static final class a<T> implements Comparator<SecondTimezoneSetting> {
                @DexIgnore
                public static /* final */ a a; // = new a();

                @DexIgnore
                /* renamed from: a */
                public final int compare(SecondTimezoneSetting secondTimezoneSetting, SecondTimezoneSetting secondTimezoneSetting2) {
                    return secondTimezoneSetting.component1().compareTo(secondTimezoneSetting2.component1());
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0014b(ArrayList arrayList, fb7 fb7) {
                super(2, fb7);
                this.$rawDataList = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0014b bVar = new C0014b(this.$rawDataList, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ArrayList<SecondTimezoneSetting>> fb7) {
                return ((C0014b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    aa7.a(this.$rawDataList, a.a);
                    return this.$rawDataList;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(b26 b26, fb7 fb7) {
            super(2, fb7);
            this.this$0 = b26;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                ti7 a3 = this.this$0.b();
                a aVar = new a(null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                ArrayList arrayList = (ArrayList) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                this.this$0.f.addAll((ArrayList) obj);
                this.this$0.h.m(this.this$0.f);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ArrayList arrayList2 = (ArrayList) obj;
            ti7 a4 = this.this$0.b();
            C0014b bVar = new C0014b(arrayList2, null);
            this.L$0 = yi7;
            this.L$1 = arrayList2;
            this.label = 2;
            obj = vh7.a(a4, bVar, this);
            if (obj == a2) {
                return a2;
            }
            this.this$0.f.addAll((ArrayList) obj);
            this.this$0.h.m(this.this$0.f);
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = b26.class.getSimpleName();
        ee7.a((Object) simpleName, "SearchSecondTimezonePres\u2026er::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public b26(y16 y16) {
        ee7.b(y16, "mView");
        this.h = y16;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        if (this.f.isEmpty()) {
            ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
        } else {
            this.h.m(this.f);
        }
        SecondTimezoneSetting secondTimezoneSetting = this.e;
        if (secondTimezoneSetting != null) {
            this.h.L(secondTimezoneSetting.getTimeZoneName());
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.h.a(this);
    }

    @DexIgnore
    public void a(String str) {
        ee7.b(str, MicroAppSetting.SETTING);
        try {
            this.e = (SecondTimezoneSetting) this.g.a(str, SecondTimezoneSetting.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local.d(str2, "exception when parse second timezone setting " + e2);
        }
    }
}
