package com.fossil;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class um1 extends zk0 {
    @DexIgnore
    public static /* final */ fo1 I; // = fo1.CBC_NO_PADDING;
    @DexIgnore
    public static /* final */ byte[] J; // = new byte[16];
    @DexIgnore
    public static /* final */ rb1 K; // = new rb1(null);
    @DexIgnore
    public byte[] C; // = new byte[8];
    @DexIgnore
    public byte[] D; // = new byte[8];
    @DexIgnore
    public byte[] E; // = new byte[0];
    @DexIgnore
    public /* final */ ArrayList<ul0> F; // = yz0.a(((zk0) this).i, w97.a((Object[]) new ul0[]{ul0.AUTHENTICATION}));
    @DexIgnore
    public /* final */ f31 G;
    @DexIgnore
    public /* final */ byte[] H;

    @DexIgnore
    public um1(ri1 ri1, en0 en0, f31 f31, byte[] bArr, String str) {
        super(ri1, en0, wm0.R, str, false, 16);
        this.G = f31;
        this.H = bArr;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.F;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        new SecureRandom().nextBytes(this.C);
        byte[] bArr = this.H;
        if (bArr == null) {
            a(is0.SECRET_KEY_IS_REQUIRED);
        } else if (bArr.length < 16) {
            a(is0.INVALID_PARAMETER);
        } else {
            byte[] copyOf = Arrays.copyOf(bArr, 16);
            ee7.a((Object) copyOf, "java.util.Arrays.copyOf(this, newSize)");
            this.E = copyOf;
            ri1 ri1 = ((zk0) this).w;
            f31 f31 = this.G;
            zk0.a(this, new fp0(ri1, f31, f31.f.a(f31, this.C)), new yi1(this), new wk1(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        Object obj;
        JSONObject a = yz0.a(super.i(), r51.s2, yz0.a(this.G));
        r51 r51 = r51.r2;
        byte[] bArr = this.H;
        if (bArr != null) {
            obj = Long.valueOf(ik1.a.a(bArr, ng1.CRC32));
        } else {
            obj = JSONObject.NULL;
        }
        return yz0.a(a, r51, obj);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        return yz0.a(yz0.a(super.k(), r51.m2, yz0.a(this.C, (String) null, 1)), r51.n2, yz0.a(this.D, (String) null, 1));
    }

    @DexIgnore
    public final byte[] m() {
        return this.D;
    }

    @DexIgnore
    public final byte[] n() {
        return this.C;
    }

    @DexIgnore
    public final void a(byte[] bArr) {
        if (bArr.length != 16) {
            a(eu0.a(((zk0) this).v, null, is0.INVALID_DATA_LENGTH, null, 5));
            return;
        }
        byte[] a = f31.f.a(this.G, fq1.a.a(I, this.E, J, bArr));
        if (a.length != 16) {
            a(eu0.a(((zk0) this).v, null, is0.INVALID_DATA_LENGTH, null, 5));
            return;
        }
        this.D = s97.a(a, 0, 8);
        if (Arrays.equals(this.C, s97.a(a, 8, 16))) {
            byte[] a2 = s97.a(this.C, this.D);
            n11 n11 = f31.f;
            f31 f31 = this.G;
            fo1 fo1 = I;
            byte[] bArr2 = this.E;
            byte[] bArr3 = J;
            gm1 gm1 = gm1.ENCRYPT;
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr2, "AES");
            Cipher instance = Cipher.getInstance(fo1.a);
            instance.init(gm1.a, secretKeySpec, new IvParameterSpec(bArr3));
            byte[] doFinal = instance.doFinal(a2);
            ee7.a((Object) doFinal, "cipher.doFinal(data)");
            zk0.a(this, new jn0(((zk0) this).w, this.G, n11.a(f31, doFinal)), ld1.a, ff1.a, (kd7) null, new bh1(this), (gd7) null, 40, (Object) null);
            return;
        }
        a(eu0.a(((zk0) this).v, null, is0.WRONG_RANDOM_NUMBER, null, 5));
    }
}
