package com.fossil;

import com.google.gson.JsonElement;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface fe4<T> {
    @DexIgnore
    T deserialize(JsonElement jsonElement, Type type, ee4 ee4) throws je4;
}
