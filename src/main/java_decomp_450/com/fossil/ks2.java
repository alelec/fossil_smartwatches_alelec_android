package com.fossil;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ks2 {
    @DexIgnore
    public static int a(int i, int i2, int i3) {
        return (i & (~i3)) | (i2 & i3);
    }

    @DexIgnore
    public static Object a(int i) {
        if (i < 2 || i > 1073741824 || Integer.highestOneBit(i) != i) {
            StringBuilder sb = new StringBuilder(52);
            sb.append("must be power of 2 between 2^1 and 2^30: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        } else if (i <= 256) {
            return new byte[i];
        } else {
            if (i <= 65536) {
                return new short[i];
            }
            return new int[i];
        }
    }

    @DexIgnore
    public static int b(int i) {
        return (i < 32 ? 4 : 2) * (i + 1);
    }

    @DexIgnore
    public static int a(Object obj, int i) {
        if (obj instanceof byte[]) {
            return ((byte[]) obj)[i] & 255;
        }
        if (obj instanceof short[]) {
            return ((short[]) obj)[i] & 65535;
        }
        return ((int[]) obj)[i];
    }

    @DexIgnore
    public static void a(Object obj, int i, int i2) {
        if (obj instanceof byte[]) {
            ((byte[]) obj)[i] = (byte) i2;
        } else if (obj instanceof short[]) {
            ((short[]) obj)[i] = (short) i2;
        } else {
            ((int[]) obj)[i] = i2;
        }
    }

    @DexIgnore
    public static int a(@NullableDecl Object obj, @NullableDecl Object obj2, int i, Object obj3, int[] iArr, Object[] objArr, @NullableDecl Object[] objArr2) {
        int i2;
        int i3;
        int a = ms2.a(obj);
        int i4 = a & i;
        int a2 = a(obj3, i4);
        if (a2 == 0) {
            return -1;
        }
        int i5 = ~i;
        int i6 = a & i5;
        int i7 = -1;
        while (true) {
            i2 = a2 - 1;
            i3 = iArr[i2];
            if ((i3 & i5) != i6 || !mr2.a(obj, objArr[i2]) || (objArr2 != null && !mr2.a(obj2, objArr2[i2]))) {
                int i8 = i3 & i;
                if (i8 == 0) {
                    return -1;
                }
                i7 = i2;
                a2 = i8;
            }
        }
        int i9 = i3 & i;
        if (i7 == -1) {
            a(obj3, i4, i9);
        } else {
            iArr[i7] = a(iArr[i7], i9, i);
        }
        return i2;
    }
}
