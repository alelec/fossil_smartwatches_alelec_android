package com.fossil;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.fossil.fx7;
import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import com.j256.ormlite.field.FieldType;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ex7 implements fx7 {
    @DexIgnore
    public static /* final */ Uri b; // = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    @DexIgnore
    public static /* final */ Uri c; // = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
    @DexIgnore
    public static /* final */ yw7 d; // = new yw7();
    @DexIgnore
    public static /* final */ String[] e; // = {"longitude", "latitude"};
    @DexIgnore
    public static /* final */ ex7 f; // = new ex7();

    @DexIgnore
    @Override // com.fossil.fx7
    public List<String> a(Context context, List<String> list) {
        ee7.b(context, "context");
        ee7.b(list, "ids");
        return fx7.b.a(this, context, list);
    }

    @DexIgnore
    public int b(Cursor cursor, String str) {
        ee7.b(cursor, "$this$getInt");
        ee7.b(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
        return fx7.b.b(this, cursor, str);
    }

    @DexIgnore
    public long c(Cursor cursor, String str) {
        ee7.b(cursor, "$this$getLong");
        ee7.b(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
        return fx7.b.c(this, cursor, str);
    }

    @DexIgnore
    public String d(Cursor cursor, String str) {
        ee7.b(cursor, "$this$getString");
        ee7.b(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
        return fx7.b.d(this, cursor, str);
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public void k() {
        d.a();
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public boolean a(Context context, String str) {
        ee7.b(context, "context");
        ee7.b(str, "id");
        return fx7.b.a(this, context, str);
    }

    @DexIgnore
    @Override // com.fossil.fx7
    @SuppressLint({"Recycle"})
    public zw7 b(Context context, String str) {
        long j;
        ee7.b(context, "context");
        ee7.b(str, "id");
        zw7 a = d.a(str);
        if (a != null) {
            return a;
        }
        Object[] array = t97.d(s97.b(s97.b(s97.b(fx7.a.b(), fx7.a.c()), e), fx7.a.d())).toArray(new String[0]);
        if (array != null) {
            ContentResolver contentResolver = context.getContentResolver();
            Uri a2 = a();
            Cursor query = contentResolver.query(a2, (String[]) array, "_id = ?", new String[]{str}, null);
            if (query != null) {
                ee7.a((Object) query, "context.contentResolver.\u2026           ?: return null");
                if (query.moveToNext()) {
                    String d2 = d(query, FieldType.FOREIGN_ID_FIELD_SUFFIX);
                    String d3 = d(query, "_data");
                    long c2 = c(query, "datetaken");
                    int b2 = b(query, MessengerShareContentUtility.MEDIA_TYPE);
                    if (b2 == 1) {
                        j = 0;
                    } else {
                        j = c(query, "duration");
                    }
                    int b3 = b(query, "width");
                    int b4 = b(query, "height");
                    String name = new File(d3).getName();
                    long c3 = c(query, "date_modified");
                    double a3 = a(query, "latitude");
                    double a4 = a(query, "longitude");
                    int a5 = a(b2);
                    ee7.a((Object) name, "displayName");
                    zw7 zw7 = new zw7(d2, d3, j, c2, b3, b4, a5, name, c3);
                    if (a3 != 0.0d) {
                        zw7.a(Double.valueOf(a3));
                    }
                    if (a4 != 0.0d) {
                        zw7.b(Double.valueOf(a4));
                    }
                    zw7.a(Double.valueOf(a3));
                    zw7.b(Double.valueOf(a4));
                    d.a(zw7);
                    query.close();
                    return zw7;
                }
                query.close();
            }
            return null;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public ub c(Context context, String str) {
        ee7.b(context, "context");
        ee7.b(str, "id");
        zw7 b2 = b(context, str);
        if (b2 != null) {
            return new ub(b2.i());
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public Uri a() {
        return fx7.b.a(this);
    }

    @DexIgnore
    public String a(int i, ax7 ax7, ArrayList<String> arrayList) {
        ee7.b(ax7, "filterOptions");
        ee7.b(arrayList, "args");
        return fx7.b.a(this, i, ax7, arrayList);
    }

    @DexIgnore
    public double a(Cursor cursor, String str) {
        ee7.b(cursor, "$this$getDouble");
        ee7.b(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
        return fx7.b.a(this, cursor, str);
    }

    @DexIgnore
    public int a(int i) {
        return fx7.b.a(this, i);
    }

    @DexIgnore
    @Override // com.fossil.fx7
    @SuppressLint({"Recycle"})
    public List<bx7> a(Context context, int i, long j, ax7 ax7) {
        ee7.b(context, "context");
        ee7.b(ax7, "option");
        ArrayList arrayList = new ArrayList();
        Uri a = a();
        String[] strArr = (String[]) s97.b(fx7.a.a(), new String[]{"count(1)"});
        ArrayList<String> arrayList2 = new ArrayList<>();
        String a2 = a(i, ax7, arrayList2);
        arrayList2.add(String.valueOf(j));
        String str = "bucket_id IS NOT NULL " + a2 + ' ' + "AND datetaken <= ?" + ' ' + cx7.e.a(Integer.valueOf(i)) + ") GROUP BY (bucket_id";
        ContentResolver contentResolver = context.getContentResolver();
        Object[] array = arrayList2.toArray(new String[0]);
        if (array != null) {
            Cursor query = contentResolver.query(a, strArr, str, (String[]) array, null);
            if (query == null) {
                return w97.a();
            }
            ee7.a((Object) query, "context.contentResolver.\u2026    ?: return emptyList()");
            while (query.moveToNext()) {
                String string = query.getString(0);
                String string2 = query.getString(1);
                int i2 = query.getInt(2);
                ee7.a((Object) string, "id");
                ee7.a((Object) string2, "name");
                arrayList.add(new bx7(string, string2, i2, 0, false, 16, null));
            }
            query.close();
            return arrayList;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public bx7 a(Context context, String str, int i, long j, ax7 ax7) {
        ee7.b(context, "context");
        ee7.b(str, "galleryId");
        ee7.b(ax7, "option");
        Uri a = a();
        String[] strArr = (String[]) s97.b(fx7.a.a(), new String[]{"count(1)"});
        ArrayList<String> arrayList = new ArrayList<>();
        String a2 = a(i, ax7, arrayList);
        arrayList.add(String.valueOf(j));
        String str2 = "";
        if (!ee7.a((Object) str, (Object) str2)) {
            arrayList.add(str);
            str2 = "AND bucket_id = ?";
        }
        String str3 = "bucket_id IS NOT NULL " + a2 + ' ' + "AND datetaken <= ?" + ' ' + str2 + ' ' + cx7.e.a((Integer) null) + ") GROUP BY (bucket_id";
        ContentResolver contentResolver = context.getContentResolver();
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            Cursor query = contentResolver.query(a, strArr, str3, (String[]) array, null);
            if (query == null) {
                return null;
            }
            ee7.a((Object) query, "context.contentResolver.\u2026           ?: return null");
            if (query.moveToNext()) {
                String string = query.getString(0);
                String string2 = query.getString(1);
                int i2 = query.getInt(2);
                query.close();
                ee7.a((Object) string, "id");
                ee7.a((Object) string2, "name");
                return new bx7(string, string2, i2, 0, false, 16, null);
            }
            query.close();
            return null;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public Bitmap a(Context context, String str, int i, int i2, Integer num) {
        ee7.b(context, "context");
        ee7.b(str, "id");
        throw new q87("An operation is not implemented: " + "not implemented");
    }

    @DexIgnore
    @Override // com.fossil.fx7
    @SuppressLint({"Recycle"})
    public List<zw7> a(Context context, String str, int i, int i2, int i3, long j, ax7 ax7, yw7 yw7) {
        String str2;
        long j2;
        ee7.b(context, "context");
        ee7.b(str, "galleryId");
        ee7.b(ax7, "option");
        yw7 yw72 = yw7 != null ? yw7 : d;
        boolean z = str.length() == 0;
        ArrayList arrayList = new ArrayList();
        Uri a = a();
        ArrayList<String> arrayList2 = new ArrayList<>();
        if (!z) {
            arrayList2.add(str);
        }
        String a2 = a(i3, ax7, arrayList2);
        arrayList2.add(String.valueOf(j));
        String a3 = cx7.e.a(Integer.valueOf(i3));
        Object[] array = t97.d(s97.b(s97.b(s97.b(fx7.a.b(), fx7.a.c()), fx7.a.d()), e)).toArray(new String[0]);
        if (array != null) {
            String[] strArr = (String[]) array;
            if (z) {
                str2 = "bucket_id IS NOT NULL " + a2 + ' ' + "AND datetaken <= ?" + ' ' + a3;
            } else {
                str2 = "bucket_id = ? " + a2 + ' ' + "AND datetaken <= ?" + ' ' + a3;
            }
            String str3 = "datetaken DESC LIMIT " + i2 + " OFFSET " + (i2 * i);
            ContentResolver contentResolver = context.getContentResolver();
            Object[] array2 = arrayList2.toArray(new String[0]);
            if (array2 != null) {
                Cursor query = contentResolver.query(a, strArr, str2, (String[]) array2, str3);
                if (query == null) {
                    return w97.a();
                }
                ee7.a((Object) query, "context.contentResolver.\u2026    ?: return emptyList()");
                while (query.moveToNext()) {
                    String d2 = d(query, FieldType.FOREIGN_ID_FIELD_SUFFIX);
                    String d3 = d(query, "_data");
                    long c2 = c(query, "datetaken");
                    long c3 = c(query, "date_modified");
                    int b2 = b(query, MessengerShareContentUtility.MEDIA_TYPE);
                    if (i3 == 1) {
                        j2 = 0;
                    } else {
                        j2 = c(query, "duration");
                    }
                    int b3 = b(query, "width");
                    int b4 = b(query, "height");
                    String name = new File(d3).getName();
                    double a4 = a(query, "latitude");
                    double a5 = a(query, "longitude");
                    int a6 = a(b2);
                    ee7.a((Object) name, "displayName");
                    zw7 zw7 = new zw7(d2, d3, j2, c2, b3, b4, a6, name, c3);
                    if (a4 != 0.0d) {
                        zw7.a(Double.valueOf(a4));
                    }
                    if (a5 != 0.0d) {
                        zw7.b(Double.valueOf(a5));
                    }
                    arrayList.add(zw7);
                    yw72.a(zw7);
                }
                query.close();
                return arrayList;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public List<zw7> a(Context context, String str, int i, int i2, int i3, long j, ax7 ax7) {
        String str2;
        long j2;
        ee7.b(context, "context");
        ee7.b(str, "gId");
        ee7.b(ax7, "option");
        yw7 yw7 = d;
        boolean z = str.length() == 0;
        ArrayList arrayList = new ArrayList();
        Uri a = a();
        ArrayList<String> arrayList2 = new ArrayList<>();
        if (!z) {
            arrayList2.add(str);
        }
        String a2 = a(i3, ax7, arrayList2);
        arrayList2.add(String.valueOf(j));
        String a3 = cx7.e.a(Integer.valueOf(i3));
        Object[] array = t97.d(s97.b(s97.b(s97.b(fx7.a.b(), fx7.a.c()), fx7.a.d()), e)).toArray(new String[0]);
        if (array != null) {
            String[] strArr = (String[]) array;
            if (z) {
                str2 = "bucket_id IS NOT NULL " + a2 + ' ' + "AND datetaken <= ?" + ' ' + a3;
            } else {
                str2 = "bucket_id = ? " + a2 + ' ' + "AND datetaken <= ?" + ' ' + a3;
            }
            String str3 = "datetaken DESC LIMIT " + (i2 - i) + " OFFSET " + i;
            ContentResolver contentResolver = context.getContentResolver();
            Object[] array2 = arrayList2.toArray(new String[0]);
            if (array2 != null) {
                Cursor query = contentResolver.query(a, strArr, str2, (String[]) array2, str3);
                if (query == null) {
                    return w97.a();
                }
                ee7.a((Object) query, "context.contentResolver.\u2026    ?: return emptyList()");
                while (query.moveToNext()) {
                    String d2 = d(query, FieldType.FOREIGN_ID_FIELD_SUFFIX);
                    String d3 = d(query, "_data");
                    long c2 = c(query, "datetaken");
                    int b2 = b(query, MessengerShareContentUtility.MEDIA_TYPE);
                    if (i3 == 1) {
                        j2 = 0;
                    } else {
                        j2 = c(query, "duration");
                    }
                    int b3 = b(query, "width");
                    int b4 = b(query, "height");
                    String name = new File(d3).getName();
                    long c3 = c(query, "date_modified");
                    double a4 = a(query, "latitude");
                    double a5 = a(query, "longitude");
                    int a6 = a(b2);
                    ee7.a((Object) name, "displayName");
                    zw7 zw7 = new zw7(d2, d3, j2, c2, b3, b4, a6, name, c3);
                    if (a4 != 0.0d) {
                        zw7.a(Double.valueOf(a4));
                    }
                    if (a5 != 0.0d) {
                        zw7.b(Double.valueOf(a5));
                    }
                    arrayList.add(zw7);
                    yw7.a(zw7);
                }
                query.close();
                return arrayList;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public byte[] a(Context context, zw7 zw7, boolean z) {
        ee7.b(context, "context");
        ee7.b(zw7, "asset");
        throw new q87("An operation is not implemented: " + "not implemented");
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public void a(Context context, zw7 zw7, byte[] bArr) {
        ee7.b(context, "context");
        ee7.b(zw7, "asset");
        ee7.b(bArr, "byteArray");
        throw new q87("An operation is not implemented: " + "not implemented");
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public String a(Context context, String str, boolean z) {
        ee7.b(context, "context");
        ee7.b(str, "id");
        zw7 b2 = b(context, str);
        if (b2 != null) {
            return b2.i();
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public zw7 a(Context context, byte[] bArr, String str, String str2) {
        ee7.b(context, "context");
        ee7.b(bArr, "image");
        ee7.b(str, "title");
        ee7.b(str2, Constants.DESC);
        return b(context, String.valueOf(ContentUris.parseId(Uri.parse(MediaStore.Images.Media.insertImage(context.getContentResolver(), BitmapFactory.decodeByteArray(bArr, 0, bArr.length), str, str2)))));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x008d, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x008e, code lost:
        com.fossil.hc7.a(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0091, code lost:
        throw r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0093, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0094, code lost:
        com.fossil.hc7.a(r0, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0097, code lost:
        throw r10;
     */
    @DexIgnore
    @Override // com.fossil.fx7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.zw7 a(android.content.Context r9, java.io.InputStream r10, java.lang.String r11, java.lang.String r12) {
        /*
            r8 = this;
            java.lang.String r0 = "context"
            com.fossil.ee7.b(r9, r0)
            java.lang.String r0 = "inputStream"
            com.fossil.ee7.b(r10, r0)
            java.lang.String r0 = "title"
            com.fossil.ee7.b(r11, r0)
            java.lang.String r1 = "desc"
            com.fossil.ee7.b(r12, r1)
            android.content.ContentResolver r1 = r9.getContentResolver()
            long r2 = java.lang.System.currentTimeMillis()
            r4 = 1000(0x3e8, float:1.401E-42)
            long r4 = (long) r4
            long r2 = r2 / r4
            java.lang.String r4 = java.net.URLConnection.guessContentTypeFromStream(r10)
            if (r4 != 0) goto L_0x0040
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "video/"
            r4.append(r5)
            java.io.File r5 = new java.io.File
            r5.<init>(r11)
            java.lang.String r5 = com.fossil.rc7.d(r5)
            r4.append(r5)
            java.lang.String r4 = r4.toString()
        L_0x0040:
            android.net.Uri r5 = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI
            android.content.ContentValues r6 = new android.content.ContentValues
            r6.<init>()
            java.lang.String r7 = "_display_name"
            r6.put(r7, r11)
            java.lang.String r7 = "mime_type"
            r6.put(r7, r4)
            r6.put(r0, r11)
            java.lang.String r11 = "description"
            r6.put(r11, r12)
            java.lang.Long r11 = java.lang.Long.valueOf(r2)
            java.lang.String r12 = "date_added"
            r6.put(r12, r11)
            java.lang.Long r11 = java.lang.Long.valueOf(r2)
            java.lang.String r12 = "date_modified"
            r6.put(r12, r11)
            android.net.Uri r11 = r1.insert(r5, r6)
            r12 = 0
            if (r11 == 0) goto L_0x00a8
            java.lang.String r0 = "cr.insert(uri, values) ?: return null"
            com.fossil.ee7.a(r11, r0)
            java.io.OutputStream r0 = r1.openOutputStream(r11)
            if (r0 == 0) goto L_0x0098
            r2 = 0
            r3 = 2
            com.fossil.gc7.a(r10, r0, r2, r3, r12)     // Catch:{ all -> 0x008b }
            com.fossil.hc7.a(r10, r12)     // Catch:{ all -> 0x0089 }
            com.fossil.hc7.a(r0, r12)
            goto L_0x0098
        L_0x0089:
            r9 = move-exception
            goto L_0x0092
        L_0x008b:
            r9 = move-exception
            throw r9     // Catch:{ all -> 0x008d }
        L_0x008d:
            r11 = move-exception
            com.fossil.hc7.a(r10, r9)
            throw r11
        L_0x0092:
            throw r9     // Catch:{ all -> 0x0093 }
        L_0x0093:
            r10 = move-exception
            com.fossil.hc7.a(r0, r9)
            throw r10
        L_0x0098:
            long r2 = android.content.ContentUris.parseId(r11)
            r1.notifyChange(r11, r12)
            java.lang.String r10 = java.lang.String.valueOf(r2)
            com.fossil.zw7 r9 = r8.b(r9, r10)
            return r9
        L_0x00a8:
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ex7.a(android.content.Context, java.io.InputStream, java.lang.String, java.lang.String):com.fossil.zw7");
    }
}
