package com.fossil;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ho4 implements go4 {
    @DexIgnore
    public /* final */ ci a;
    @DexIgnore
    public /* final */ vh<fo4> b;
    @DexIgnore
    public /* final */ ji c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends vh<fo4> {
        @DexIgnore
        public a(ho4 ho4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, fo4 fo4) {
            if (fo4.c() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, fo4.c());
            }
            if (fo4.g() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, fo4.g());
            }
            if (fo4.b() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, fo4.b());
            }
            if (fo4.d() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, fo4.d());
            }
            if (fo4.e() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindLong(5, (long) fo4.e().intValue());
            }
            if (fo4.f() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, fo4.f());
            }
            if (fo4.a() == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, fo4.a());
            }
            if (fo4.h() == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, fo4.h());
            }
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `profile` (`id`,`socialId`,`firstName`,`lastName`,`points`,`profilePicture`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ji {
        @DexIgnore
        public b(ho4 ho4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "UPDATE profile SET socialId = ? WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ji {
        @DexIgnore
        public c(ho4 ho4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM profile";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Callable<fo4> {
        @DexIgnore
        public /* final */ /* synthetic */ fi a;

        @DexIgnore
        public d(fi fiVar) {
            this.a = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public fo4 call() throws Exception {
            fo4 fo4 = null;
            Integer valueOf = null;
            Cursor a2 = pi.a(ho4.this.a, this.a, false, null);
            try {
                int b2 = oi.b(a2, "id");
                int b3 = oi.b(a2, "socialId");
                int b4 = oi.b(a2, Constants.PROFILE_KEY_FIRST_NAME);
                int b5 = oi.b(a2, Constants.PROFILE_KEY_LAST_NAME);
                int b6 = oi.b(a2, "points");
                int b7 = oi.b(a2, Constants.PROFILE_KEY_PROFILE_PIC);
                int b8 = oi.b(a2, "createdAt");
                int b9 = oi.b(a2, "updatedAt");
                if (a2.moveToFirst()) {
                    String string = a2.getString(b2);
                    String string2 = a2.getString(b3);
                    String string3 = a2.getString(b4);
                    String string4 = a2.getString(b5);
                    if (!a2.isNull(b6)) {
                        valueOf = Integer.valueOf(a2.getInt(b6));
                    }
                    fo4 = new fo4(string, string2, string3, string4, valueOf, a2.getString(b7), a2.getString(b8), a2.getString(b9));
                }
                return fo4;
            } finally {
                a2.close();
            }
        }
    }

    @DexIgnore
    public ho4(ci ciVar) {
        this.a = ciVar;
        this.b = new a(this, ciVar);
        new b(this, ciVar);
        this.c = new c(this, ciVar);
    }

    @DexIgnore
    @Override // com.fossil.go4
    public LiveData<fo4> b() {
        return this.a.getInvalidationTracker().a(new String[]{"profile"}, false, (Callable) new d(fi.b("SELECT*FROM profile  LIMIT 1", 0)));
    }

    @DexIgnore
    @Override // com.fossil.go4
    public fo4 c() {
        fi b2 = fi.b("SELECT*FROM profile LIMIT 1", 0);
        this.a.assertNotSuspendingTransaction();
        fo4 fo4 = null;
        Integer valueOf = null;
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, "socialId");
            int b5 = oi.b(a2, Constants.PROFILE_KEY_FIRST_NAME);
            int b6 = oi.b(a2, Constants.PROFILE_KEY_LAST_NAME);
            int b7 = oi.b(a2, "points");
            int b8 = oi.b(a2, Constants.PROFILE_KEY_PROFILE_PIC);
            int b9 = oi.b(a2, "createdAt");
            int b10 = oi.b(a2, "updatedAt");
            if (a2.moveToFirst()) {
                String string = a2.getString(b3);
                String string2 = a2.getString(b4);
                String string3 = a2.getString(b5);
                String string4 = a2.getString(b6);
                if (!a2.isNull(b7)) {
                    valueOf = Integer.valueOf(a2.getInt(b7));
                }
                fo4 = new fo4(string, string2, string3, string4, valueOf, a2.getString(b8), a2.getString(b9), a2.getString(b10));
            }
            return fo4;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.go4
    public long a(fo4 fo4) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(fo4);
            this.a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.go4
    public void a() {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.c.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }
}
