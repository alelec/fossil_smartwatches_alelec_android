package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.VideoUploader;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g96 extends d96 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f; // = new MutableLiveData<>();
    @DexIgnore
    public Date g;
    @DexIgnore
    public Date h;
    @DexIgnore
    public List<ActivitySummary> i; // = new ArrayList();
    @DexIgnore
    public /* final */ LiveData<qx6<List<ActivitySummary>>> j;
    @DexIgnore
    public TreeMap<Long, Float> k;
    @DexIgnore
    public /* final */ e96 l;
    @DexIgnore
    public /* final */ UserRepository m;
    @DexIgnore
    public /* final */ SummariesRepository n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter$loadData$1", f = "ActivityOverviewMonthPresenter.kt", l = {103}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ g96 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter$loadData$1$currentUser$1", f = "ActivityOverviewMonthPresenter.kt", l = {103}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository i2 = this.this$0.this$0.m;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = i2.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(g96 g96, fb7 fb7) {
            super(2, fb7);
            this.this$0 = g96;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser != null) {
                this.this$0.h = zd5.d(mFUser.getCreatedAt());
                e96 j = this.this$0.l;
                Date f = this.this$0.g;
                if (f != null) {
                    Date c = this.this$0.h;
                    if (c == null) {
                        c = new Date();
                    }
                    j.a(f, c);
                    MutableLiveData g = this.this$0.f;
                    Date f2 = this.this$0.g;
                    if (f2 != null) {
                        g.a(f2);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ g96 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter$mActivitySummaries$1$1", f = "ActivityOverviewMonthPresenter.kt", l = {50, 50}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<ActivitySummary>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<ActivitySummary>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                Date date;
                Date date2;
                vd vdVar2;
                Date date3;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    Date c = this.this$0.a.h;
                    if (c == null) {
                        c = new Date();
                    }
                    long time = c.getTime();
                    Date date4 = this.$it;
                    ee7.a((Object) date4, "it");
                    if (!zd5.a(time, date4.getTime())) {
                        Calendar r = zd5.r(this.$it);
                        ee7.a((Object) r, "DateHelper.getStartOfMonth(it)");
                        c = r.getTime();
                        ee7.a((Object) c, "DateHelper.getStartOfMonth(it).time");
                    }
                    date = c;
                    Boolean v = zd5.v(this.$it);
                    ee7.a((Object) v, "DateHelper.isThisMonth(it)");
                    if (v.booleanValue()) {
                        date3 = new Date();
                    } else {
                        Calendar m = zd5.m(this.$it);
                        ee7.a((Object) m, "DateHelper.getEndOfMonth(it)");
                        date3 = m.getTime();
                    }
                    ee7.a((Object) date3, GoalPhase.COLUMN_END_DATE);
                    boolean z = date3.getTime() >= date.getTime();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("ActivityOverviewMonthPresenter", "mActivitySummaries onDateChanged " + this.$it + " startDate " + date + " endDate " + date3 + " isValid " + z);
                    if (z) {
                        SummariesRepository h = this.this$0.a.n;
                        this.L$0 = vdVar2;
                        this.L$1 = date;
                        this.L$2 = date3;
                        this.L$3 = vdVar2;
                        this.label = 1;
                        Object summaries = h.getSummaries(date, date3, true, this);
                        if (summaries == a) {
                            return a;
                        }
                        vdVar = vdVar2;
                        date2 = date3;
                        obj = summaries;
                    }
                    return i97.a;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$3;
                    date2 = (Date) this.L$2;
                    date = (Date) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    Date date5 = (Date) this.L$2;
                    Date date6 = (Date) this.L$1;
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.L$1 = date;
                this.L$2 = date2;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public c(g96 g96) {
            this.a = g96;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<ActivitySummary>>> apply(Date date) {
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<qx6<? extends List<ActivitySummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ g96 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter$start$1$1", f = "ActivityOverviewMonthPresenter.kt", l = {75}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.g96$d$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter$start$1$1$1", f = "ActivityOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.g96$d$a$a  reason: collision with other inner class name */
            public static final class C0067a extends zb7 implements kd7<yi7, fb7<? super TreeMap<Long, Float>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0067a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0067a aVar = new C0067a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super TreeMap<Long, Float>> fb7) {
                    return ((C0067a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        g96 g96 = this.this$0.this$0.a;
                        Object a = g96.f.a();
                        if (a != null) {
                            ee7.a(a, "mDateLiveData.value!!");
                            return g96.a((Date) a, this.this$0.$data);
                        }
                        ee7.a();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, List list, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
                this.$data = list;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$data, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                g96 g96;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    this.this$0.a.i = this.$data;
                    g96 g962 = this.this$0.a;
                    ti7 a2 = g962.b();
                    C0067a aVar = new C0067a(this, null);
                    this.L$0 = yi7;
                    this.L$1 = g962;
                    this.label = 1;
                    obj = vh7.a(a2, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                    g96 = g962;
                } else if (i == 1) {
                    g96 = (g96) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                g96.k = (TreeMap) obj;
                e96 j = this.this$0.a.l;
                TreeMap<Long, Float> d = this.this$0.a.k;
                if (d == null) {
                    d = new TreeMap<>();
                }
                j.a(d);
                return i97.a;
            }
        }

        @DexIgnore
        public d(g96 g96) {
            this.a = g96;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<ActivitySummary>> qx6) {
            List list;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("mDateTransformations - status=");
            sb.append(qx6 != null ? qx6.d() : null);
            sb.append(" -- data.size=");
            sb.append((qx6 == null || (list = (List) qx6.c()) == null) ? null : Integer.valueOf(list.size()));
            local.d("ActivityOverviewMonthPresenter", sb.toString());
            if ((qx6 != null ? qx6.d() : null) != lb5.DATABASE_LOADING) {
                List list2 = qx6 != null ? (List) qx6.c() : null;
                if (list2 != null && (!ee7.a(this.a.i, list2))) {
                    ik7 unused = xh7.b(this.a.e(), null, null, new a(this, list2, null), 3, null);
                }
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public g96(e96 e96, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        ee7.b(e96, "mView");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(summariesRepository, "mSummariesRepository");
        ee7.b(portfolioApp, "mApp");
        this.l = e96;
        this.m = userRepository;
        this.n = summariesRepository;
        this.e = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.c());
        LiveData<qx6<List<ActivitySummary>>> b2 = ge.b(this.f, new c(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026        }\n        }\n    }");
        this.j = b2;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        i();
        LiveData<qx6<List<ActivitySummary>>> liveData = this.j;
        e96 e96 = this.l;
        if (e96 != null) {
            liveData.a((f96) e96, new d(this));
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthPresenter", "stop");
        try {
            LiveData<qx6<List<ActivitySummary>>> liveData = this.j;
            e96 e96 = this.l;
            if (e96 != null) {
                liveData.a((f96) e96);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActivityOverviewMonthPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.d96
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        ee7.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public void i() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthPresenter", "loadData");
        Date date = this.g;
        if (date == null || !zd5.w(date).booleanValue()) {
            this.g = new Date();
            ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityOverviewMonthPresenter", "loadData - mDateLiveData=" + this.g);
    }

    @DexIgnore
    public void j() {
        this.l.a(this);
    }

    @DexIgnore
    @Override // com.fossil.d96
    public void a(Date date) {
        ee7.b(date, "date");
        if (this.f.a() == null || !zd5.d(this.f.a(), date)) {
            this.f.a(date);
        }
    }

    @DexIgnore
    public final TreeMap<Long, Float> a(Date date, List<ActivitySummary> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("ActivityOverviewMonthPresenter", sb.toString());
        TreeMap<Long, Float> treeMap = new TreeMap<>();
        Calendar instance = Calendar.getInstance();
        if (list != null) {
            for (ActivitySummary activitySummary : list) {
                instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay(), 0, 0, 0);
                instance.set(14, 0);
                if (activitySummary.getStepGoal() > 0) {
                    ee7.a((Object) instance, "calendar");
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf(((float) activitySummary.getSteps()) / ((float) activitySummary.getStepGoal())));
                } else {
                    ee7.a((Object) instance, "calendar");
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                }
            }
        }
        return treeMap;
    }
}
