package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.fossil.pr;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mr implements pr<Drawable> {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ gr b;

    @DexIgnore
    public mr(Context context, gr grVar) {
        ee7.b(context, "context");
        ee7.b(grVar, "drawableDecoder");
        this.a = context;
        this.b = grVar;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.rq, java.lang.Object, com.fossil.rt, com.fossil.ir, com.fossil.fb7] */
    @Override // com.fossil.pr
    public /* bridge */ /* synthetic */ Object a(rq rqVar, Drawable drawable, rt rtVar, ir irVar, fb7 fb7) {
        return a(rqVar, drawable, rtVar, irVar, (fb7<? super or>) fb7);
    }

    @DexIgnore
    public String b(Drawable drawable) {
        ee7.b(drawable, "data");
        return null;
    }

    @DexIgnore
    public boolean a(Drawable drawable) {
        ee7.b(drawable, "data");
        return pr.a.a(this, drawable);
    }

    @DexIgnore
    public Object a(rq rqVar, Drawable drawable, rt rtVar, ir irVar, fb7<? super or> fb7) {
        boolean a2 = iu.a(drawable);
        if (a2) {
            Bitmap a3 = this.b.a(drawable, rtVar, irVar.d());
            Resources resources = this.a.getResources();
            ee7.a((Object) resources, "context.resources");
            drawable = new BitmapDrawable(resources, a3);
        }
        return new nr(drawable, a2, br.MEMORY);
    }
}
