package com.fossil;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.cy6;
import com.fossil.gg5;
import com.fossil.lz5;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.MetaData;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.tutorial.CustomizeTutorialActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sz5 extends ho5 implements rz5, View.OnClickListener, CustomizeWidget.c, cy6.g, gg5.b {
    @DexIgnore
    public qz5 g;
    @DexIgnore
    public qw6<s05> h;
    @DexIgnore
    public /* final */ ArrayList<Fragment> i; // = new ArrayList<>();
    @DexIgnore
    public k06 j;
    @DexIgnore
    public o36 p;
    @DexIgnore
    public s26 q;
    @DexIgnore
    public int r; // = 1;
    @DexIgnore
    public Integer s;
    @DexIgnore
    public ValueAnimator t;
    @DexIgnore
    public n06 u;
    @DexIgnore
    public r36 v;
    @DexIgnore
    public v26 w;
    @DexIgnore
    public rj4 x;
    @DexIgnore
    public a06 y;
    @DexIgnore
    public HashMap z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ sz5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(sz5 sz5) {
            this.a = sz5;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionCancel()");
            if (transition != null) {
                transition.removeListener(this);
            }
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionEnd()");
            if (transition != null) {
                transition.removeListener(this);
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null && !activity.hasWindowFocus()) {
                ee7.a((Object) activity, "it");
                int intExtra = activity.getIntent().getIntExtra("KEY_CUSTOMIZE_TAB", 1);
                String stringExtra = activity.getIntent().getStringExtra("KEY_PRESET_ID");
                String stringExtra2 = activity.getIntent().getStringExtra("KEY_PRESET_COMPLICATION_POS_SELECTED");
                String stringExtra3 = activity.getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
                activity.finishAfterTransition();
                DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.B;
                ee7.a((Object) stringExtra, "presetId");
                ee7.a((Object) stringExtra2, "complicationPos");
                aVar.a(activity, stringExtra, intExtra, stringExtra2, stringExtra3);
            }
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionPause()");
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionResume()");
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            ViewPropertyAnimator duration;
            ViewPropertyAnimator duration2;
            ViewPropertyAnimator duration3;
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionStart()");
            s05 s05 = (s05) sz5.a(this.a).a();
            if (s05 != null) {
                zk5 zk5 = zk5.a;
                Object a2 = sz5.a(this.a).a();
                if (a2 != null) {
                    CardView cardView = ((s05) a2).u;
                    ee7.a((Object) cardView, "mBinding.get()!!.cvGroup");
                    zk5.a(cardView);
                    ViewPropertyAnimator animate = s05.J.animate();
                    if (!(animate == null || (duration3 = animate.setDuration(500)) == null)) {
                        duration3.alpha(1.0f);
                    }
                    ViewPropertyAnimator animate2 = s05.I.animate();
                    if (!(animate2 == null || (duration2 = animate2.setDuration(500)) == null)) {
                        duration2.alpha(1.0f);
                    }
                    ViewPropertyAnimator animate3 = s05.H.animate();
                    if (animate3 != null && (duration = animate3.setDuration(500)) != null) {
                        duration.alpha(1.0f);
                        return;
                    }
                    return;
                }
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ s05 a;

        @DexIgnore
        public c(s05 s05) {
            this.a = s05;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            u5 u5Var = new u5();
            u5Var.c(this.a.D);
            ee7.a((Object) valueAnimator, "value");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                u5Var.a(2131362543, ((Float) animatedValue).floatValue());
                Object animatedValue2 = valueAnimator.getAnimatedValue();
                if (animatedValue2 != null) {
                    u5Var.a(2131362548, ((Float) animatedValue2).floatValue() + ((float) 1));
                    u5Var.a(this.a.D);
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Float");
            }
            throw new x87("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ s05 a;

        @DexIgnore
        public d(s05 s05) {
            this.a = s05;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            u5 u5Var = new u5();
            u5Var.c(this.a.D);
            ee7.a((Object) valueAnimator, "value");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                u5Var.a(2131362543, ((Float) animatedValue).floatValue());
                Object animatedValue2 = valueAnimator.getAnimatedValue();
                if (animatedValue2 != null) {
                    u5Var.a(2131362548, ((Float) animatedValue2).floatValue() - ((float) 1));
                    u5Var.a(this.a.D);
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Float");
            }
            throw new x87("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ s05 a;

        @DexIgnore
        public e(s05 s05) {
            this.a = s05;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            u5 u5Var = new u5();
            u5Var.c(this.a.D);
            ee7.a((Object) valueAnimator, "value");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                u5Var.a(2131362543, ((Float) animatedValue).floatValue());
                u5Var.a(this.a.D);
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ s05 a;

        @DexIgnore
        public f(s05 s05) {
            this.a = s05;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            u5 u5Var = new u5();
            u5Var.c(this.a.D);
            ee7.a((Object) valueAnimator, "value");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                u5Var.a(2131362543, ((Float) animatedValue).floatValue());
                u5Var.a(this.a.D);
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements lz5.b {
        @DexIgnore
        public /* final */ /* synthetic */ sz5 a;

        @DexIgnore
        public g(sz5 sz5) {
            this.a = sz5;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean a(View view, String str) {
            ee7.b(view, "view");
            ee7.b(str, "id");
            return this.a.a(false, ViewHierarchy.DIMENSION_TOP_KEY, view, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void b(String str) {
            ee7.b(str, "label");
            this.a.a(false, ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean c(String str) {
            ee7.b(str, "fromPos");
            this.a.c(false, str, ViewHierarchy.DIMENSION_TOP_KEY);
            return true;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a(String str) {
            ee7.b(str, "label");
            this.a.b(false, ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a() {
            this.a.c(false, ViewHierarchy.DIMENSION_TOP_KEY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements lz5.b {
        @DexIgnore
        public /* final */ /* synthetic */ sz5 a;

        @DexIgnore
        public h(sz5 sz5) {
            this.a = sz5;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean a(View view, String str) {
            ee7.b(view, "view");
            ee7.b(str, "id");
            return this.a.a(false, ViewHierarchy.DIMENSION_LEFT_KEY, view, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void b(String str) {
            ee7.b(str, "label");
            this.a.a(false, ViewHierarchy.DIMENSION_LEFT_KEY, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean c(String str) {
            ee7.b(str, "fromPos");
            this.a.c(false, str, ViewHierarchy.DIMENSION_LEFT_KEY);
            return true;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a(String str) {
            ee7.b(str, "label");
            this.a.b(false, ViewHierarchy.DIMENSION_LEFT_KEY, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a() {
            this.a.c(false, ViewHierarchy.DIMENSION_LEFT_KEY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements lz5.b {
        @DexIgnore
        public /* final */ /* synthetic */ sz5 a;

        @DexIgnore
        public i(sz5 sz5) {
            this.a = sz5;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean a(View view, String str) {
            ee7.b(view, "view");
            ee7.b(str, "id");
            return this.a.a(false, "right", view, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void b(String str) {
            ee7.b(str, "label");
            this.a.a(false, "right", str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean c(String str) {
            ee7.b(str, "fromPos");
            this.a.c(false, str, "right");
            return true;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a(String str) {
            ee7.b(str, "label");
            this.a.b(false, "right", str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a() {
            this.a.c(false, "right");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements lz5.b {
        @DexIgnore
        public /* final */ /* synthetic */ sz5 a;

        @DexIgnore
        public j(sz5 sz5) {
            this.a = sz5;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean a(View view, String str) {
            ee7.b(view, "view");
            ee7.b(str, "id");
            return this.a.a(false, "bottom", view, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void b(String str) {
            ee7.b(str, "label");
            this.a.a(false, "bottom", str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean c(String str) {
            ee7.b(str, "fromPos");
            this.a.c(false, str, "bottom");
            return true;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a(String str) {
            ee7.b(str, "label");
            this.a.b(false, "bottom", str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a() {
            this.a.c(false, "bottom");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements lz5.b {
        @DexIgnore
        public /* final */ /* synthetic */ sz5 a;

        @DexIgnore
        public k(sz5 sz5) {
            this.a = sz5;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean a(View view, String str) {
            ee7.b(view, "view");
            ee7.b(str, "id");
            return this.a.a(true, ViewHierarchy.DIMENSION_TOP_KEY, view, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void b(String str) {
            ee7.b(str, "label");
            this.a.a(true, ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean c(String str) {
            ee7.b(str, "fromPos");
            this.a.c(true, str, ViewHierarchy.DIMENSION_TOP_KEY);
            return true;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a(String str) {
            ee7.b(str, "label");
            this.a.b(true, ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a() {
            this.a.c(true, ViewHierarchy.DIMENSION_TOP_KEY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements lz5.b {
        @DexIgnore
        public /* final */ /* synthetic */ sz5 a;

        @DexIgnore
        public l(sz5 sz5) {
            this.a = sz5;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean a(View view, String str) {
            ee7.b(view, "view");
            ee7.b(str, "id");
            return this.a.a(true, "middle", view, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void b(String str) {
            ee7.b(str, "label");
            this.a.a(true, "middle", str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean c(String str) {
            ee7.b(str, "fromPos");
            this.a.c(true, str, "middle");
            return true;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a(String str) {
            ee7.b(str, "label");
            this.a.b(true, "middle", str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a() {
            this.a.c(true, "middle");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements lz5.b {
        @DexIgnore
        public /* final */ /* synthetic */ sz5 a;

        @DexIgnore
        public m(sz5 sz5) {
            this.a = sz5;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean a(View view, String str) {
            ee7.b(view, "view");
            ee7.b(str, "id");
            return this.a.a(true, "bottom", view, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void b(String str) {
            ee7.b(str, "label");
            this.a.a(true, "bottom", str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean c(String str) {
            ee7.b(str, "fromPos");
            this.a.c(true, str, "bottom");
            return true;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a(String str) {
            ee7.b(str, "label");
            this.a.b(true, "bottom", str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a() {
            this.a.c(true, "bottom");
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.rz5
    public void E(String str) {
        ee7.b(str, "complicationsPosition");
        if (this.r == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "updateSelectedComplication data=" + str);
            Y(str);
        }
    }

    @DexIgnore
    @Override // com.fossil.rz5
    public void O(String str) {
        ee7.b(str, "buttonsPosition");
        if (this.r == 2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "updateSelectedWatchApp position=" + str);
            Z(str);
        }
    }

    @DexIgnore
    public final void Y(String str) {
        qw6<s05> qw6 = this.h;
        if (qw6 != null) {
            s05 a2 = qw6.a();
            if (a2 != null) {
                switch (str.hashCode()) {
                    case -1383228885:
                        if (str.equals("bottom")) {
                            a2.R.setSelectedWc(false);
                            a2.O.setSelectedWc(true);
                            a2.Q.setSelectedWc(false);
                            a2.P.setSelectedWc(false);
                            return;
                        }
                        return;
                    case 115029:
                        if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.R.setSelectedWc(true);
                            a2.O.setSelectedWc(false);
                            a2.Q.setSelectedWc(false);
                            a2.P.setSelectedWc(false);
                            return;
                        }
                        return;
                    case 3317767:
                        if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                            a2.R.setSelectedWc(false);
                            a2.O.setSelectedWc(false);
                            a2.Q.setSelectedWc(true);
                            a2.P.setSelectedWc(false);
                            return;
                        }
                        return;
                    case 108511772:
                        if (str.equals("right")) {
                            a2.R.setSelectedWc(false);
                            a2.O.setSelectedWc(false);
                            a2.Q.setSelectedWc(false);
                            a2.P.setSelectedWc(true);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void Z(String str) {
        qw6<s05> qw6 = this.h;
        if (qw6 != null) {
            s05 a2 = qw6.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.N.setSelectedWc(true);
                            a2.M.setSelectedWc(false);
                            a2.L.setSelectedWc(false);
                        }
                    } else if (str.equals("middle")) {
                        a2.N.setSelectedWc(false);
                        a2.M.setSelectedWc(true);
                        a2.L.setSelectedWc(false);
                    }
                } else if (str.equals("bottom")) {
                    a2.N.setSelectedWc(false);
                    a2.M.setSelectedWc(false);
                    a2.L.setSelectedWc(true);
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.z;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.gg5.b
    public void a(View view) {
    }

    @DexIgnore
    @Override // com.fossil.gg5.b
    public void b(View view) {
    }

    @DexIgnore
    @Override // com.fossil.gg5.b
    public void c(View view) {
    }

    @DexIgnore
    @Override // com.fossil.gg5.b
    public void d(View view) {
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "DianaCustomizeEditFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        qz5 qz5 = this.g;
        if (qz5 != null) {
            qz5.h();
            return false;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rz5
    public void f(String str) {
        ee7.b(str, "watchAppId");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            CustomizeTutorialActivity.a aVar = CustomizeTutorialActivity.y;
            ee7.a((Object) activity, "it");
            aVar.a(activity, str);
        }
    }

    @DexIgnore
    public final void f1() {
        qw6<s05> qw6 = this.h;
        if (qw6 != null) {
            s05 a2 = qw6.a();
            if (a2 != null) {
                a2.R.g();
                a2.P.g();
                a2.O.g();
                a2.Q.g();
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rz5
    public void g(boolean z2) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            if (z2) {
                activity.setResult(-1);
            } else {
                activity.setResult(0);
            }
            activity.finishAfterTransition();
        }
    }

    @DexIgnore
    public final void g1() {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "Inside .showNoActiveDeviceFlow");
        this.j = (k06) getChildFragmentManager().b("ComplicationsFragment");
        this.p = (o36) getChildFragmentManager().b("WatchAppsFragment");
        this.q = (s26) getChildFragmentManager().b("CustomizeThemeFragment");
        if (this.j == null) {
            this.j = new k06();
        }
        if (this.p == null) {
            this.p = new o36();
        }
        if (this.q == null) {
            this.q = new s26();
        }
        s26 s26 = this.q;
        if (s26 != null) {
            this.i.add(s26);
        }
        k06 k06 = this.j;
        if (k06 != null) {
            this.i.add(k06);
        }
        o36 o36 = this.p;
        if (o36 != null) {
            this.i.add(o36);
        }
        tj4 f2 = PortfolioApp.g0.c().f();
        k06 k062 = this.j;
        if (k062 != null) {
            o36 o362 = this.p;
            if (o362 != null) {
                s26 s262 = this.q;
                if (s262 != null) {
                    f2.a(new hz5(k062, o362, s262)).a(this);
                    return;
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeContract.View");
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsContract.View");
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsContract.View");
    }

    @DexIgnore
    @Override // com.fossil.rz5
    public void h(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "initTab - initTab=" + i2 + " - mPreTab=" + this.s);
        this.r = i2;
        qw6<s05> qw6 = this.h;
        if (qw6 != null) {
            s05 a2 = qw6.a();
            if (a2 != null) {
                u5 u5Var = new u5();
                u5Var.c(a2.D);
                if (i2 == 0) {
                    Integer num = this.s;
                    if (num != null && num.intValue() == 1) {
                        qz5 qz5 = this.g;
                        if (qz5 != null) {
                            qz5.i();
                            a2.z.setImageBitmap(je5.a(a2.r));
                            ValueAnimator ofFloat = ValueAnimator.ofFloat(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
                            this.t = ofFloat;
                            if (ofFloat != null) {
                                ofFloat.addUpdateListener(new c(a2));
                            }
                            ValueAnimator valueAnimator = this.t;
                            if (valueAnimator != null) {
                                valueAnimator.start();
                            }
                            this.s = null;
                        } else {
                            ee7.d("mPresenter");
                            throw null;
                        }
                    } else {
                        u5Var.a(2131362543, 1.0f);
                        u5Var.a(a2.D);
                    }
                    ConstraintLayout constraintLayout = a2.t;
                    ee7.a((Object) constraintLayout, "it.clWatchApps");
                    constraintLayout.setVisibility(4);
                    View view = a2.B;
                    ee7.a((Object) view, "it.lineCenter");
                    view.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    View view2 = a2.A;
                    ee7.a((Object) view2, "it.lineBottom");
                    view2.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    View view3 = a2.C;
                    ee7.a((Object) view3, "it.lineTop");
                    view3.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                } else if (i2 == 1) {
                    Integer num2 = this.s;
                    if (num2 != null && num2.intValue() == 0) {
                        a2.z.setImageBitmap(je5.a(a2.r));
                        ValueAnimator ofFloat2 = ValueAnimator.ofFloat(2.0f, 1.0f);
                        this.t = ofFloat2;
                        if (ofFloat2 != null) {
                            ofFloat2.addUpdateListener(new d(a2));
                        }
                        ValueAnimator valueAnimator2 = this.t;
                        if (valueAnimator2 != null) {
                            valueAnimator2.start();
                        }
                        this.s = null;
                    } else if (num2 != null && num2.intValue() == 2) {
                        ValueAnimator ofFloat3 = ValueAnimator.ofFloat(0.25f, 1.0f);
                        this.t = ofFloat3;
                        if (ofFloat3 != null) {
                            ofFloat3.addUpdateListener(new e(a2));
                        }
                        ValueAnimator valueAnimator3 = this.t;
                        if (valueAnimator3 != null) {
                            valueAnimator3.start();
                        }
                        this.s = null;
                    } else {
                        u5Var.a(2131362543, 1.0f);
                        u5Var.a(a2.D);
                    }
                    ConstraintLayout constraintLayout2 = a2.t;
                    ee7.a((Object) constraintLayout2, "it.clWatchApps");
                    constraintLayout2.setVisibility(4);
                    View view4 = a2.B;
                    ee7.a((Object) view4, "it.lineCenter");
                    view4.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    View view5 = a2.A;
                    ee7.a((Object) view5, "it.lineBottom");
                    view5.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    View view6 = a2.C;
                    ee7.a((Object) view6, "it.lineTop");
                    view6.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    a06 a06 = this.y;
                    if (a06 != null) {
                        String a3 = a06.f().a();
                        if (a3 != null) {
                            ee7.a((Object) a3, "pos");
                            Y(a3);
                        }
                    } else {
                        ee7.d("mShareViewModel");
                        throw null;
                    }
                } else if (i2 == 2) {
                    Integer num3 = this.s;
                    if (num3 != null && num3.intValue() == 1) {
                        u5Var.a(2131362548, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        u5Var.a(a2.D);
                        ValueAnimator ofFloat4 = ValueAnimator.ofFloat(1.0f, 0.25f);
                        this.t = ofFloat4;
                        if (ofFloat4 != null) {
                            ofFloat4.addUpdateListener(new f(a2));
                        }
                        ValueAnimator valueAnimator4 = this.t;
                        if (valueAnimator4 != null) {
                            valueAnimator4.start();
                        }
                        this.s = null;
                    } else {
                        u5Var.a(2131362543, 0.25f);
                        u5Var.a(a2.D);
                    }
                    ConstraintLayout constraintLayout3 = a2.t;
                    ee7.a((Object) constraintLayout3, "it.clWatchApps");
                    constraintLayout3.setVisibility(0);
                    View view7 = a2.B;
                    ee7.a((Object) view7, "it.lineCenter");
                    view7.setAlpha(1.0f);
                    View view8 = a2.A;
                    ee7.a((Object) view8, "it.lineBottom");
                    view8.setAlpha(1.0f);
                    View view9 = a2.C;
                    ee7.a((Object) view9, "it.lineTop");
                    view9.setAlpha(1.0f);
                    a06 a062 = this.y;
                    if (a062 != null) {
                        String a4 = a062.i().a();
                        if (a4 != null) {
                            ee7.a((Object) a4, "pos");
                            Z(a4);
                        }
                    } else {
                        ee7.d("mShareViewModel");
                        throw null;
                    }
                }
                n(i2);
                qz5 qz52 = this.g;
                if (qz52 != null) {
                    qz52.a(i2);
                    ViewPager2 viewPager2 = a2.E;
                    ee7.a((Object) viewPager2, "it.rvPreset");
                    viewPager2.setCurrentItem(i2);
                    return;
                }
                ee7.d("mPresenter");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void h1() {
        qw6<s05> qw6 = this.h;
        if (qw6 != null) {
            s05 a2 = qw6.a();
            if (a2 != null) {
                CustomizeWidget customizeWidget = a2.R;
                Intent putExtra = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_TOP_KEY);
                ee7.a((Object) putExtra, "Intent().putExtra(Custom\u2026plicationAppPos.TOP_FACE)");
                CustomizeWidget.a(customizeWidget, "SWAP_PRESET_COMPLICATION", putExtra, new lz5(new g(this)), null, 8, null);
                CustomizeWidget customizeWidget2 = a2.Q;
                Intent putExtra2 = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_LEFT_KEY);
                ee7.a((Object) putExtra2, "Intent().putExtra(Custom\u2026licationAppPos.LEFT_FACE)");
                CustomizeWidget.a(customizeWidget2, "SWAP_PRESET_COMPLICATION", putExtra2, new lz5(new h(this)), null, 8, null);
                CustomizeWidget customizeWidget3 = a2.P;
                Intent putExtra3 = new Intent().putExtra("KEY_POSITION", "right");
                ee7.a((Object) putExtra3, "Intent().putExtra(Custom\u2026icationAppPos.RIGHT_FACE)");
                CustomizeWidget.a(customizeWidget3, "SWAP_PRESET_COMPLICATION", putExtra3, new lz5(new i(this)), null, 8, null);
                CustomizeWidget customizeWidget4 = a2.O;
                Intent putExtra4 = new Intent().putExtra("KEY_POSITION", "bottom");
                ee7.a((Object) putExtra4, "Intent().putExtra(Custom\u2026cationAppPos.BOTTOM_FACE)");
                CustomizeWidget.a(customizeWidget4, "SWAP_PRESET_COMPLICATION", putExtra4, new lz5(new j(this)), null, 8, null);
                CustomizeWidget customizeWidget5 = a2.N;
                Intent putExtra5 = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_TOP_KEY);
                ee7.a((Object) putExtra5, "Intent().putExtra(Custom\u2026, WatchAppPos.TOP_BUTTON)");
                CustomizeWidget.a(customizeWidget5, "SWAP_PRESET_WATCH_APP", putExtra5, new lz5(new k(this)), null, 8, null);
                CustomizeWidget customizeWidget6 = a2.M;
                Intent putExtra6 = new Intent().putExtra("KEY_POSITION", "middle");
                ee7.a((Object) putExtra6, "Intent().putExtra(Custom\u2026atchAppPos.MIDDLE_BUTTON)");
                CustomizeWidget.a(customizeWidget6, "SWAP_PRESET_WATCH_APP", putExtra6, new lz5(new l(this)), null, 8, null);
                CustomizeWidget customizeWidget7 = a2.L;
                Intent putExtra7 = new Intent().putExtra("KEY_POSITION", "bottom");
                ee7.a((Object) putExtra7, "Intent().putExtra(Custom\u2026atchAppPos.BOTTOM_BUTTON)");
                CustomizeWidget.a(customizeWidget7, "SWAP_PRESET_WATCH_APP", putExtra7, new lz5(new m(this)), null, 8, null);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rz5
    public void k() {
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886761);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026on_Text__ApplyingToWatch)");
        W(a2);
    }

    @DexIgnore
    @Override // com.fossil.rz5
    public void m() {
        a();
    }

    @DexIgnore
    public final void n(int i2) {
        if (i2 == 1) {
            V("set_complication_view");
        } else if (i2 == 2) {
            V("set_watch_apps_view");
        }
    }

    @DexIgnore
    @Override // com.fossil.rz5
    public void o() {
        cy6.f fVar = new cy6.f(2131558482);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886530));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886528));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886529));
        fVar.a(2131363229);
        fVar.a(2131363307);
        fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = (DianaCustomizeEditActivity) activity;
            rj4 rj4 = this.x;
            if (rj4 != null) {
                he a2 = je.a(dianaCustomizeEditActivity, rj4).a(a06.class);
                ee7.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                a06 a06 = (a06) a2;
                this.y = a06;
                qz5 qz5 = this.g;
                if (qz5 == null) {
                    ee7.d("mPresenter");
                    throw null;
                } else if (a06 != null) {
                    qz5.a(a06);
                } else {
                    ee7.d("mShareViewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 111 && i3 == 1 && xg5.b.a(getContext(), xg5.c.BLUETOOTH_CONNECTION)) {
            qz5 qz5 = this.g;
            if (qz5 != null) {
                qz5.a(false);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.gg5.b
    public void onClick(View view) {
        if (view != null) {
            switch (view.getId()) {
                case 2131362499:
                    qz5 qz5 = this.g;
                    if (qz5 != null) {
                        qz5.a(true);
                        return;
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                case 2131363229:
                    qz5 qz52 = this.g;
                    if (qz52 != null) {
                        qz52.h();
                        return;
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                case 2131363452:
                    qz5 qz53 = this.g;
                    if (qz53 != null) {
                        qz53.b("bottom");
                        return;
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                case 2131363453:
                    qz5 qz54 = this.g;
                    if (qz54 != null) {
                        qz54.b("middle");
                        return;
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                case 2131363454:
                    qz5 qz55 = this.g;
                    if (qz55 != null) {
                        qz55.b(ViewHierarchy.DIMENSION_TOP_KEY);
                        return;
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                case 2131363455:
                    qz5 qz56 = this.g;
                    if (qz56 != null) {
                        qz56.a("bottom");
                        return;
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                case 2131363459:
                    qz5 qz57 = this.g;
                    if (qz57 != null) {
                        qz57.a("right");
                        return;
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                case 2131363462:
                    qz5 qz58 = this.g;
                    if (qz58 != null) {
                        qz58.a(ViewHierarchy.DIMENSION_LEFT_KEY);
                        return;
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                case 2131363463:
                    qz5 qz59 = this.g;
                    if (qz59 != null) {
                        qz59.a(ViewHierarchy.DIMENSION_TOP_KEY);
                        return;
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                default:
                    return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        s05 s05 = (s05) qb.a(layoutInflater, 2131558548, viewGroup, false, a1());
        g1();
        this.h = new qw6<>(this, s05);
        ee7.a((Object) s05, "binding");
        return s05.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        qz5 qz5 = this.g;
        if (qz5 != null) {
            qz5.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        qz5 qz5 = this.g;
        if (qz5 != null) {
            qz5.f();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ee7.a((Object) activity, Constants.ACTIVITY);
            Window window = activity.getWindow();
            ee7.a((Object) window, "activity.window");
            window.setEnterTransition(zk5.a.a());
            Window window2 = activity.getWindow();
            ee7.a((Object) window2, "activity.window");
            window2.setSharedElementEnterTransition(zk5.a.a(PortfolioApp.g0.c()));
            Intent intent = activity.getIntent();
            ee7.a((Object) intent, "activity.intent");
            activity.setEnterSharedElementCallback(new yk5(intent, PortfolioApp.g0.c()));
            a(activity);
            postponeEnterTransition();
        }
        qw6<s05> qw6 = this.h;
        if (qw6 != null) {
            s05 a2 = qw6.a();
            if (a2 != null) {
                String b2 = eh5.l.a().b("nonBrandSurface");
                if (!TextUtils.isEmpty(b2)) {
                    a2.u.setBackgroundColor(Color.parseColor(b2));
                    a2.K.setBackgroundColor(Color.parseColor(b2));
                }
                a2.R.a(this);
                a2.O.a(this);
                a2.Q.a(this);
                a2.P.a(this);
                a2.R.setOnClickListener(this);
                a2.O.setOnClickListener(this);
                a2.Q.setOnClickListener(this);
                a2.P.setOnClickListener(this);
                a2.N.setOnClickListener(this);
                a2.M.setOnClickListener(this);
                a2.L.setOnClickListener(this);
                a2.F.setOnClickListener(this);
                a2.v.setOnClickListener(this);
                ViewPager2 viewPager2 = a2.E;
                ee7.a((Object) viewPager2, "it.rvPreset");
                viewPager2.setAdapter(new qz6(getChildFragmentManager(), this.i));
                if (a2.E.getChildAt(0) != null) {
                    View childAt = a2.E.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setItemViewCacheSize(2);
                    } else {
                        throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
                ViewPager2 viewPager22 = a2.E;
                ee7.a((Object) viewPager22, "it.rvPreset");
                viewPager22.setUserInputEnabled(false);
                gg5 gg5 = new gg5();
                ee7.a((Object) a2, "it");
                gg5.a(a2.d(), this);
            }
            h1();
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rz5
    public void p() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
            ee7.a((Object) activity, "it");
            TroubleshootingActivity.a.a(aVar, activity, PortfolioApp.g0.c().c(), false, false, 12, null);
        }
    }

    @DexIgnore
    public final void b(boolean z2, String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragExit - position=" + str + ", label=" + str2);
        qw6<s05> qw6 = this.h;
        if (qw6 != null) {
            s05 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z2) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.N.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.M.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.L.e();
                }
            } else {
                switch (str.hashCode()) {
                    case -1383228885:
                        if (str.equals("bottom")) {
                            a2.O.e();
                            return;
                        }
                        return;
                    case 115029:
                        if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.R.e();
                            return;
                        }
                        return;
                    case 3317767:
                        if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                            a2.Q.e();
                            return;
                        }
                        return;
                    case 108511772:
                        if (str.equals("right")) {
                            a2.P.e();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void c(boolean z2, String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "swapControl - fromPosition=" + str + ", toPosition=" + str2);
        qw6<s05> qw6 = this.h;
        if (qw6 != null) {
            s05 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z2) {
                int hashCode = str2.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.N.e();
                        }
                    } else if (str2.equals("middle")) {
                        a2.M.e();
                    }
                } else if (str2.equals("bottom")) {
                    a2.L.e();
                }
                a2.N.setDragMode(false);
                a2.M.setDragMode(false);
                a2.L.setDragMode(false);
                qz5 qz5 = this.g;
                if (qz5 != null) {
                    qz5.d(str, str2);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            } else {
                switch (str2.hashCode()) {
                    case -1383228885:
                        if (str2.equals("bottom")) {
                            a2.O.e();
                            break;
                        }
                        break;
                    case 115029:
                        if (str2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.R.e();
                            break;
                        }
                        break;
                    case 3317767:
                        if (str2.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                            a2.Q.e();
                            break;
                        }
                        break;
                    case 108511772:
                        if (str2.equals("right")) {
                            a2.P.e();
                            break;
                        }
                        break;
                }
                a2.R.setDragMode(false);
                a2.Q.setDragMode(false);
                a2.P.setDragMode(false);
                a2.O.setDragMode(false);
                qz5 qz52 = this.g;
                if (qz52 != null) {
                    qz52.c(str, str2);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.rz5
    public void f(boolean z2) {
        qw6<s05> qw6 = this.h;
        if (qw6 != null) {
            s05 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z2) {
                FlexibleTextView flexibleTextView = a2.v;
                ee7.a((Object) flexibleTextView, "it.ftvSetToWatch");
                flexibleTextView.setEnabled(true);
                FlexibleTextView flexibleTextView2 = a2.v;
                ee7.a((Object) flexibleTextView2, "it.ftvSetToWatch");
                flexibleTextView2.setClickable(true);
                a2.v.setBackgroundResource(2131231274);
                return;
            }
            FlexibleTextView flexibleTextView3 = a2.v;
            ee7.a((Object) flexibleTextView3, "it.ftvSetToWatch");
            flexibleTextView3.setClickable(false);
            FlexibleTextView flexibleTextView4 = a2.v;
            ee7.a((Object) flexibleTextView4, "it.ftvSetToWatch");
            flexibleTextView4.setEnabled(false);
            a2.v.setBackgroundResource(2131231275);
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ qw6 a(sz5 sz5) {
        qw6<s05> qw6 = sz5.h;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.gg5.b
    public /* bridge */ /* synthetic */ void a(View view, Boolean bool) {
        a(view, bool.booleanValue());
    }

    @DexIgnore
    public final Transition a(FragmentActivity fragmentActivity) {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener()");
        Window window = fragmentActivity.getWindow();
        ee7.a((Object) window, "it.window");
        return window.getSharedElementEnterTransition().addListener(new b(this));
    }

    @DexIgnore
    public void a(View view, boolean z2) {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "onHorizontalFling");
    }

    @DexIgnore
    public final boolean a(boolean z2, String str, View view, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dropControl - pos=" + str + ", view=" + view.getId() + ", id=" + str2);
        qw6<s05> qw6 = this.h;
        if (qw6 != null) {
            s05 a2 = qw6.a();
            if (a2 == null) {
                return true;
            }
            if (z2) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.N.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.M.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.L.e();
                }
                a2.N.setDragMode(false);
                a2.M.setDragMode(false);
                a2.L.setDragMode(false);
                o36 o36 = this.p;
                if (o36 != null) {
                    o36.f1();
                }
                qz5 qz5 = this.g;
                if (qz5 != null) {
                    qz5.b(str2, str);
                    return true;
                }
                ee7.d("mPresenter");
                throw null;
            }
            switch (str.hashCode()) {
                case -1383228885:
                    if (str.equals("bottom")) {
                        a2.O.e();
                        break;
                    }
                    break;
                case 115029:
                    if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        a2.R.e();
                        break;
                    }
                    break;
                case 3317767:
                    if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                        a2.Q.e();
                        break;
                    }
                    break;
                case 108511772:
                    if (str.equals("right")) {
                        a2.P.e();
                        break;
                    }
                    break;
            }
            a2.R.setDragMode(false);
            a2.Q.setDragMode(false);
            a2.P.setDragMode(false);
            a2.O.setDragMode(false);
            k06 k06 = this.j;
            if (k06 != null) {
                k06.f1();
            }
            qz5 qz52 = this.g;
            if (qz52 != null) {
                qz52.a(str2, str);
                return true;
            }
            ee7.d("mPresenter");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void b(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    public final void c(boolean z2, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "cancelDrag - position=" + str);
        qw6<s05> qw6 = this.h;
        if (qw6 != null) {
            s05 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z2) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.N.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.M.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.L.e();
                }
                a2.N.setDragMode(false);
                a2.M.setDragMode(false);
                a2.L.setDragMode(false);
                o36 o36 = this.p;
                if (o36 != null) {
                    o36.f1();
                    return;
                }
                return;
            }
            switch (str.hashCode()) {
                case -1383228885:
                    if (str.equals("bottom")) {
                        a2.O.e();
                        break;
                    }
                    break;
                case 115029:
                    if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        a2.R.e();
                        break;
                    }
                    break;
                case 3317767:
                    if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                        a2.Q.e();
                        break;
                    }
                    break;
                case 108511772:
                    if (str.equals("right")) {
                        a2.P.e();
                        break;
                    }
                    break;
            }
            a2.R.setDragMode(false);
            a2.Q.setDragMode(false);
            a2.P.setDragMode(false);
            a2.O.setDragMode(false);
            k06 k06 = this.j;
            if (k06 != null) {
                k06.f1();
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(boolean z2, String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragEnter - position=" + str + ", label=" + str2);
        qw6<s05> qw6 = this.h;
        if (qw6 != null) {
            s05 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z2) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.N.d();
                        }
                    } else if (str.equals("middle")) {
                        a2.M.d();
                    }
                } else if (str.equals("bottom")) {
                    a2.L.d();
                }
            } else {
                switch (str.hashCode()) {
                    case -1383228885:
                        if (str.equals("bottom")) {
                            a2.O.d();
                            return;
                        }
                        return;
                    case 115029:
                        if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.R.d();
                            return;
                        }
                        return;
                    case 3317767:
                        if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                            a2.Q.d();
                            return;
                        }
                        return;
                    case 108511772:
                        if (str.equals("right")) {
                            a2.P.d();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void c(WatchFaceWrapper watchFaceWrapper) {
        Drawable leftComplication;
        Drawable bottomComplication;
        Drawable rightComplication;
        Drawable topComplication;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "updateBackgroundRingStyle " + watchFaceWrapper);
        qw6<s05> qw6 = this.h;
        if (qw6 != null) {
            s05 a2 = qw6.a();
            if (a2 != null) {
                a2.R.setBackgroundRes(2131230873);
                a2.P.setBackgroundRes(2131230873);
                a2.O.setBackgroundRes(2131230873);
                a2.Q.setBackgroundRes(2131230873);
                if (!(watchFaceWrapper == null || (topComplication = watchFaceWrapper.getTopComplication()) == null)) {
                    a2.R.setBackgroundDrawableCus(topComplication);
                }
                if (!(watchFaceWrapper == null || (rightComplication = watchFaceWrapper.getRightComplication()) == null)) {
                    a2.P.setBackgroundDrawableCus(rightComplication);
                }
                if (!(watchFaceWrapper == null || (bottomComplication = watchFaceWrapper.getBottomComplication()) == null)) {
                    a2.O.setBackgroundDrawableCus(bottomComplication);
                }
                if (!(watchFaceWrapper == null || (leftComplication = watchFaceWrapper.getLeftComplication()) == null)) {
                    a2.Q.setBackgroundDrawableCus(leftComplication);
                }
                a2.R.setSelectedWc(false);
                a2.O.setSelectedWc(false);
                a2.Q.setSelectedWc(false);
                a2.P.setSelectedWc(false);
                startPostponedEnterTransition();
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rz5
    public void a(WatchFaceWrapper watchFaceWrapper) {
        if (this.r == 0) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "updateSelectedCustomizeTheme watchFaceWrapper=" + watchFaceWrapper);
            c(watchFaceWrapper);
        }
    }

    @DexIgnore
    @Override // com.fossil.rz5
    public void a(dz5 dz5, DianaComplicationRingStyle dianaComplicationRingStyle) {
        Object obj;
        Object obj2;
        Object obj3;
        String str;
        String str2;
        String str3;
        WatchFaceWrapper.MetaData leftMetaData;
        WatchFaceWrapper.MetaData bottomMetaData;
        WatchFaceWrapper.MetaData rightMetaData;
        WatchFaceWrapper.MetaData topMetaData;
        WatchFaceWrapper.MetaData leftMetaData2;
        WatchFaceWrapper.MetaData bottomMetaData2;
        WatchFaceWrapper.MetaData rightMetaData2;
        WatchFaceWrapper.MetaData topMetaData2;
        WatchFaceWrapper.MetaData topMetaData3;
        Drawable background;
        MetaData metaData;
        String unselectedForegroundColor;
        MetaData metaData2;
        MetaData metaData3;
        MetaData metaData4;
        ee7.b(dz5, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("showCurrentPreset watchFaceId ");
        WatchFaceWrapper g2 = dz5.g();
        sb.append(g2 != null ? g2.getId() : null);
        sb.append(" complications=");
        sb.append(dz5.a());
        sb.append(" watchApps=");
        sb.append(dz5.f());
        sb.append(" defaultRingStyle ");
        sb.append(dianaComplicationRingStyle);
        local.d("DianaCustomizeEditFragment", sb.toString());
        qw6<s05> qw6 = this.h;
        if (qw6 != null) {
            s05 a2 = qw6.a();
            if (a2 != null) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(dz5.a());
                ArrayList arrayList2 = new ArrayList();
                arrayList2.addAll(dz5.f());
                FlexibleTextView flexibleTextView = a2.G;
                ee7.a((Object) flexibleTextView, "it.tvPresetName");
                String e2 = dz5.e();
                if (e2 != null) {
                    String upperCase = e2.toUpperCase();
                    ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                    flexibleTextView.setText(upperCase);
                    WatchFaceWrapper g3 = dz5.g();
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        fz5 fz5 = (fz5) it.next();
                        String c2 = fz5.c();
                        if (c2 != null) {
                            switch (c2.hashCode()) {
                                case -1383228885:
                                    if (c2.equals("bottom")) {
                                        a2.O.b(fz5.a());
                                        a2.O.setBottomContent(fz5.d());
                                        a2.O.h();
                                        CustomizeWidget customizeWidget = a2.O;
                                        ee7.a((Object) customizeWidget, "it.wcBottom");
                                        a(customizeWidget, fz5.a());
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 115029:
                                    if (c2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                        a2.R.b(fz5.a());
                                        a2.R.setBottomContent(fz5.d());
                                        a2.R.h();
                                        CustomizeWidget customizeWidget2 = a2.R;
                                        ee7.a((Object) customizeWidget2, "it.wcTop");
                                        a(customizeWidget2, fz5.a());
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 3317767:
                                    if (c2.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                        a2.Q.b(fz5.a());
                                        a2.Q.setBottomContent(fz5.d());
                                        a2.Q.h();
                                        CustomizeWidget customizeWidget3 = a2.Q;
                                        ee7.a((Object) customizeWidget3, "it.wcStart");
                                        a(customizeWidget3, fz5.a());
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 108511772:
                                    if (c2.equals("right")) {
                                        a2.P.b(fz5.a());
                                        a2.P.setBottomContent(fz5.d());
                                        a2.P.h();
                                        CustomizeWidget customizeWidget4 = a2.P;
                                        ee7.a((Object) customizeWidget4, "it.wcEnd");
                                        a(customizeWidget4, fz5.a());
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                        }
                    }
                    Iterator it2 = arrayList2.iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            obj = it2.next();
                            if (ee7.a((Object) ((fz5) obj).c(), (Object) ViewHierarchy.DIMENSION_TOP_KEY)) {
                            }
                        } else {
                            obj = null;
                        }
                    }
                    fz5 fz52 = (fz5) obj;
                    Iterator it3 = arrayList2.iterator();
                    while (true) {
                        if (it3.hasNext()) {
                            obj2 = it3.next();
                            if (ee7.a((Object) ((fz5) obj2).c(), (Object) "middle")) {
                            }
                        } else {
                            obj2 = null;
                        }
                    }
                    fz5 fz53 = (fz5) obj2;
                    Iterator it4 = arrayList2.iterator();
                    while (true) {
                        if (it4.hasNext()) {
                            obj3 = it4.next();
                            if (ee7.a((Object) ((fz5) obj3).c(), (Object) "bottom")) {
                            }
                        } else {
                            obj3 = null;
                        }
                    }
                    CustomizeWidget customizeWidget5 = a2.N;
                    ee7.a((Object) customizeWidget5, "it.waTop");
                    FlexibleTextView flexibleTextView2 = a2.J;
                    ee7.a((Object) flexibleTextView2, "it.tvWaTop");
                    a(customizeWidget5, flexibleTextView2, fz52);
                    CustomizeWidget customizeWidget6 = a2.M;
                    ee7.a((Object) customizeWidget6, "it.waMiddle");
                    FlexibleTextView flexibleTextView3 = a2.I;
                    ee7.a((Object) flexibleTextView3, "it.tvWaMiddle");
                    a(customizeWidget6, flexibleTextView3, fz53);
                    CustomizeWidget customizeWidget7 = a2.L;
                    ee7.a((Object) customizeWidget7, "it.waBottom");
                    FlexibleTextView flexibleTextView4 = a2.H;
                    ee7.a((Object) flexibleTextView4, "it.tvWaBottom");
                    a(customizeWidget7, flexibleTextView4, (fz5) obj3);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("showCurrentPreset defaultRingStyle ");
                    sb2.append(dianaComplicationRingStyle != null ? dianaComplicationRingStyle.getMetaData() : null);
                    sb2.append(" watchFaceWrapper ");
                    sb2.append(g3);
                    local2.d("DianaCustomizeEditFragment", sb2.toString());
                    String str4 = "#242424";
                    if (dianaComplicationRingStyle == null || (metaData4 = dianaComplicationRingStyle.getMetaData()) == null || (str = metaData4.getSelectedBackgroundColor()) == null) {
                        str = str4;
                    }
                    int parseColor = Color.parseColor(str);
                    if (dianaComplicationRingStyle == null || (metaData3 = dianaComplicationRingStyle.getMetaData()) == null || (str2 = metaData3.getUnselectedBackgroundColor()) == null) {
                        str2 = str4;
                    }
                    int parseColor2 = Color.parseColor(str2);
                    if (dianaComplicationRingStyle == null || (metaData2 = dianaComplicationRingStyle.getMetaData()) == null || (str3 = metaData2.getSelectedForegroundColor()) == null) {
                        str3 = str4;
                    }
                    int parseColor3 = Color.parseColor(str3);
                    if (!(dianaComplicationRingStyle == null || (metaData = dianaComplicationRingStyle.getMetaData()) == null || (unselectedForegroundColor = metaData.getUnselectedForegroundColor()) == null)) {
                        str4 = unselectedForegroundColor;
                    }
                    int parseColor4 = Color.parseColor(str4);
                    if (!(g3 == null || (background = g3.getBackground()) == null)) {
                        a2.y.setImageDrawable(background);
                        i97 i97 = i97.a;
                    }
                    f1();
                    if (this.r == 0) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("on theme tab unselectedForegroundColor ");
                        sb3.append((g3 == null || (topMetaData3 = g3.getTopMetaData()) == null) ? null : topMetaData3.getUnselectedForegroundColor());
                        sb3.append(" defaultUnselectedForegroundColor ");
                        sb3.append(parseColor4);
                        local3.d("DianaCustomizeEditFragment", sb3.toString());
                        if (g3 == null || (topMetaData2 = g3.getTopMetaData()) == null) {
                            a2.R.a((Integer) null, (Integer) null, Integer.valueOf(parseColor4), (Integer) null);
                            i97 i972 = i97.a;
                        } else {
                            a2.R.a((Integer) null, (Integer) null, topMetaData2.getUnselectedForegroundColor(), (Integer) null);
                            i97 i973 = i97.a;
                        }
                        if (g3 == null || (rightMetaData2 = g3.getRightMetaData()) == null) {
                            a2.P.a((Integer) null, (Integer) null, Integer.valueOf(parseColor4), (Integer) null);
                            i97 i974 = i97.a;
                        } else {
                            a2.P.a((Integer) null, (Integer) null, rightMetaData2.getUnselectedForegroundColor(), (Integer) null);
                            i97 i975 = i97.a;
                        }
                        if (g3 == null || (bottomMetaData2 = g3.getBottomMetaData()) == null) {
                            a2.O.a((Integer) null, (Integer) null, Integer.valueOf(parseColor4), (Integer) null);
                            i97 i976 = i97.a;
                        } else {
                            a2.O.a((Integer) null, (Integer) null, bottomMetaData2.getUnselectedForegroundColor(), (Integer) null);
                            i97 i977 = i97.a;
                        }
                        if (g3 == null || (leftMetaData2 = g3.getLeftMetaData()) == null) {
                            a2.Q.a((Integer) null, (Integer) null, Integer.valueOf(parseColor4), (Integer) null);
                            i97 i978 = i97.a;
                        } else {
                            a2.Q.a((Integer) null, (Integer) null, leftMetaData2.getUnselectedForegroundColor(), (Integer) null);
                            i97 i979 = i97.a;
                        }
                        c(g3);
                    } else {
                        if (g3 == null || (topMetaData = g3.getTopMetaData()) == null) {
                            a2.R.a(Integer.valueOf(parseColor3), Integer.valueOf(parseColor), Integer.valueOf(parseColor4), Integer.valueOf(parseColor2));
                            i97 i9710 = i97.a;
                        } else {
                            a2.R.a(topMetaData.getSelectedForegroundColor(), topMetaData.getSelectedBackgroundColor(), topMetaData.getUnselectedForegroundColor(), topMetaData.getUnselectedBackgroundColor());
                            i97 i9711 = i97.a;
                        }
                        if (g3 == null || (rightMetaData = g3.getRightMetaData()) == null) {
                            a2.P.a(Integer.valueOf(parseColor3), Integer.valueOf(parseColor), Integer.valueOf(parseColor4), Integer.valueOf(parseColor2));
                            i97 i9712 = i97.a;
                        } else {
                            a2.P.a(rightMetaData.getSelectedForegroundColor(), rightMetaData.getSelectedBackgroundColor(), rightMetaData.getUnselectedForegroundColor(), rightMetaData.getUnselectedBackgroundColor());
                            i97 i9713 = i97.a;
                        }
                        if (g3 == null || (bottomMetaData = g3.getBottomMetaData()) == null) {
                            a2.O.a(Integer.valueOf(parseColor3), Integer.valueOf(parseColor), Integer.valueOf(parseColor4), Integer.valueOf(parseColor2));
                            i97 i9714 = i97.a;
                        } else {
                            a2.O.a(bottomMetaData.getSelectedForegroundColor(), bottomMetaData.getSelectedBackgroundColor(), bottomMetaData.getUnselectedForegroundColor(), bottomMetaData.getUnselectedBackgroundColor());
                            i97 i9715 = i97.a;
                        }
                        if (g3 == null || (leftMetaData = g3.getLeftMetaData()) == null) {
                            a2.Q.a(Integer.valueOf(parseColor3), Integer.valueOf(parseColor), Integer.valueOf(parseColor4), Integer.valueOf(parseColor2));
                            i97 i9716 = i97.a;
                        } else {
                            a2.Q.a(leftMetaData.getSelectedForegroundColor(), leftMetaData.getSelectedBackgroundColor(), leftMetaData.getUnselectedForegroundColor(), leftMetaData.getUnselectedBackgroundColor());
                            i97 i9717 = i97.a;
                        }
                    }
                    i97 i9718 = i97.a;
                    return;
                }
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rz5
    public void c() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.n(childFragmentManager);
        }
    }

    @DexIgnore
    public final void a(CustomizeWidget customizeWidget, FlexibleTextView flexibleTextView, fz5 fz5) {
        if (fz5 != null) {
            flexibleTextView.setText(fz5.b());
            customizeWidget.d(fz5.a());
            customizeWidget.h();
            b(customizeWidget, fz5.a());
            return;
        }
        flexibleTextView.setText("");
        customizeWidget.d("empty");
        customizeWidget.h();
        b(customizeWidget, "empty");
    }

    @DexIgnore
    public final void a(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    public void a(qz5 qz5) {
        ee7.b(qz5, "presenter");
        this.g = qz5;
    }

    @DexIgnore
    @Override // com.fossil.rz5
    public void a(String str, String str2, String str3, boolean z2) {
        ee7.b(str, "message");
        ee7.b(str2, "id");
        ee7.b(str3, "pos");
        if (isActive()) {
            Bundle bundle = new Bundle();
            bundle.putString("TO_ID", str2);
            bundle.putString("TO_POS", str3);
            bundle.putBoolean("TO_COMPLICATION", z2);
            cy6.f fVar = new cy6.f(2131558480);
            fVar.a(2131363255, str);
            fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886523));
            fVar.a(2131363307);
            fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH_FAIL_SETTING", bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1395717072) {
            if (hashCode != -523101473) {
                if (hashCode == 291193711 && str.equals("DIALOG_SET_TO_WATCH_FAIL_SETTING") && i2 == 2131363307 && intent != null) {
                    String stringExtra = intent.getStringExtra("TO_POS");
                    String stringExtra2 = intent.getStringExtra("TO_ID");
                    boolean booleanExtra = intent.getBooleanExtra("TO_COMPLICATION", true);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("DianaCustomizeEditFragment", "onUserConfirmToSetUpSetting " + stringExtra2 + " of " + stringExtra);
                    if (this.r == 1) {
                        if (booleanExtra) {
                            qz5 qz5 = this.g;
                            if (qz5 != null) {
                                ee7.a((Object) stringExtra, "toPos");
                                qz5.a(stringExtra);
                                return;
                            }
                            ee7.d("mPresenter");
                            throw null;
                        }
                    } else if (!booleanExtra) {
                        qz5 qz52 = this.g;
                        if (qz52 != null) {
                            ee7.a((Object) stringExtra, "toPos");
                            qz52.b(stringExtra);
                            return;
                        }
                        ee7.d("mPresenter");
                        throw null;
                    }
                }
            } else if (!str.equals("DIALOG_SET_TO_WATCH")) {
            } else {
                if (i2 == 2131363229) {
                    g(false);
                } else if (i2 == 2131363307) {
                    qz5 qz53 = this.g;
                    if (qz53 != null) {
                        qz53.a(true);
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                }
            }
        } else if (str.equals("DIALOG_SET_TO_WATCH_FAIL_PERMISSION") && i2 == 2131363307 && intent != null) {
            String stringExtra3 = intent.getStringExtra("TO_ID");
            xg5 xg5 = xg5.b;
            Context context = getContext();
            if (context != null) {
                ve5 ve5 = ve5.a;
                ee7.a((Object) stringExtra3, "complicationId");
                xg5.a(xg5, context, ve5.a(stringExtra3), false, false, false, (Integer) null, 60, (Object) null);
                return;
            }
            ee7.a();
            throw null;
        }
    }
}
