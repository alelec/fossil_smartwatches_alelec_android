package com.fossil;

import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cw5 implements Factory<bw5> {
    @DexIgnore
    public static bw5 a(zv5 zv5, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new bw5(zv5, remindersSettingsDatabase);
    }
}
