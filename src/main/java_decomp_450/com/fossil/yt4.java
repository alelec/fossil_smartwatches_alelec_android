package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yt4 {
    @DexIgnore
    public /* final */ int a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ ea5 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ea5 ea5, View view) {
            super(view);
            ee7.b(ea5, "binding");
            ee7.b(view, "root");
            this.a = ea5;
        }

        @DexIgnore
        public final void a(String str) {
            ee7.b(str, "name");
            FlexibleTextView flexibleTextView = this.a.q;
            ee7.a((Object) flexibleTextView, "binding.ftvTitle");
            flexibleTextView.setText(str);
        }
    }

    @DexIgnore
    public yt4(int i) {
        this.a = i;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public boolean a(List<? extends Object> list, int i) {
        ee7.b(list, "items");
        return list.get(i) instanceof String;
    }

    @DexIgnore
    public RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        ee7.b(viewGroup, "parent");
        ea5 a2 = ea5.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemRecommendedChallenge\u2026(inflater, parent, false)");
        View d = a2.d();
        ee7.a((Object) d, "binding.root");
        return new a(a2, d);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r1v3, resolved type: java.lang.String */
    /* JADX WARN: Multi-variable type inference failed */
    public void a(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        ee7.b(list, "items");
        ee7.b(viewHolder, "holder");
        String str = null;
        if (!(viewHolder instanceof a)) {
            viewHolder = null;
        }
        a aVar = (a) viewHolder;
        Object obj = list.get(i);
        if (obj instanceof String) {
            str = obj;
        }
        String str2 = str;
        if (aVar != null && str2 != null) {
            aVar.a(str2);
        }
    }
}
