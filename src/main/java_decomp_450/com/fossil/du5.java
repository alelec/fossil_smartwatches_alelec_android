package com.fossil;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class du5 {
    @DexIgnore
    public /* final */ bu5 a;
    @DexIgnore
    public /* final */ LoaderManager b;

    @DexIgnore
    public du5(bu5 bu5, LoaderManager loaderManager) {
        ee7.b(bu5, "mView");
        ee7.b(loaderManager, "mLoaderManager");
        this.a = bu5;
        this.b = loaderManager;
    }

    @DexIgnore
    public final LoaderManager a() {
        return this.b;
    }

    @DexIgnore
    public final bu5 b() {
        return this.a;
    }
}
