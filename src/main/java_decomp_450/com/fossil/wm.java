package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.fossil.lm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wm implements lm {
    @DexIgnore
    public /* final */ MutableLiveData<lm.b> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ up<lm.b.c> d; // = up.e();

    @DexIgnore
    public wm() {
        a(lm.b);
    }

    @DexIgnore
    public void a(lm.b bVar) {
        this.c.a(bVar);
        if (bVar instanceof lm.b.c) {
            this.d.b((lm.b.c) bVar);
        } else if (bVar instanceof lm.b.a) {
            this.d.a(((lm.b.a) bVar).a());
        }
    }
}
