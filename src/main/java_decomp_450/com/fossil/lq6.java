package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lq6 implements Factory<kq6> {
    @DexIgnore
    public static kq6 a(hq6 hq6, ew6 ew6, UserRepository userRepository, DeviceRepository deviceRepository, ServerSettingRepository serverSettingRepository, lm4 lm4, ch5 ch5) {
        return new kq6(hq6, ew6, userRepository, deviceRepository, serverSettingRepository, lm4, ch5);
    }
}
