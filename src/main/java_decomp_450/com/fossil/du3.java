package com.fossil;

import android.content.Context;
import android.view.SubMenu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class du3 extends p1 {
    @DexIgnore
    public du3(Context context) {
        super(context);
    }

    @DexIgnore
    @Override // com.fossil.p1, android.view.Menu
    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        r1 r1Var = (r1) a(i, i2, i3, charSequence);
        fu3 fu3 = new fu3(e(), this, r1Var);
        r1Var.a(fu3);
        return fu3;
    }
}
