package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m66 extends h66 {
    @DexIgnore
    public String e; // = "empty";
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h;
    @DexIgnore
    public /* final */ ArrayList<MicroApp> i; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ i66 k;
    @DexIgnore
    public /* final */ MicroAppRepository l;
    @DexIgnore
    public /* final */ ch5 m;
    @DexIgnore
    public /* final */ PortfolioApp n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$search$1", f = "SearchMicroAppPresenter.kt", l = {79}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $query;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ m66 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.m66$a$a")
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$search$1$1", f = "SearchMicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.m66$a$a  reason: collision with other inner class name */
        public static final class C0122a extends zb7 implements kd7<yi7, fb7<? super List<MicroApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0122a(a aVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0122a aVar = new C0122a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<MicroApp>> fb7) {
                return ((C0122a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    MicroAppRepository e = this.this$0.this$0.l;
                    a aVar = this.this$0;
                    return ea7.d((Collection) e.queryMicroAppByName(aVar.$query, aVar.this$0.n.c()));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(m66 m66, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = m66;
            this.$query = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.this$0, this.$query, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            List list;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                list = new ArrayList();
                if (this.$query.length() > 0) {
                    ti7 a2 = this.this$0.c();
                    C0122a aVar = new C0122a(this, null);
                    this.L$0 = yi7;
                    this.L$1 = list;
                    this.label = 1;
                    obj = vh7.a(a2, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                }
                this.this$0.k.b(this.this$0.a(list));
                this.this$0.h = this.$query;
                return i97.a;
            } else if (i == 1) {
                List list2 = (List) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            list = (List) obj;
            this.this$0.k.b(this.this$0.a(list));
            this.this$0.h = this.$query;
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$start$1", f = "SearchMicroAppPresenter.kt", l = {40, 44}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ m66 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$start$1$allMicroApps$1", f = "SearchMicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends MicroApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends MicroApp>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.l.getAllMicroApp(PortfolioApp.g0.c().c());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.m66$b$b")
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$start$1$allSearchedMicroApps$1", f = "SearchMicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.m66$b$b  reason: collision with other inner class name */
        public static final class C0123b extends zb7 implements kd7<yi7, fb7<? super List<? extends MicroApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0123b(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0123b bVar = new C0123b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends MicroApp>> fb7) {
                return ((C0123b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    MicroAppRepository e = this.this$0.this$0.l;
                    List<String> t = this.this$0.this$0.m.t();
                    ee7.a((Object) t, "sharedPreferencesManager.microAppSearchedIdsRecent");
                    return e.getMicroAppByIds(t, this.this$0.this$0.n.c());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(m66 m66, fb7 fb7) {
            super(2, fb7);
            this.this$0 = m66;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                ti7 a3 = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                List list = (List) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                this.this$0.j.clear();
                this.this$0.j.addAll((List) obj);
                this.this$0.i();
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list2 = (List) obj;
            this.this$0.i.clear();
            this.this$0.i.addAll(list2);
            ti7 a4 = this.this$0.c();
            C0123b bVar = new C0123b(this, null);
            this.L$0 = yi7;
            this.L$1 = list2;
            this.label = 2;
            obj = vh7.a(a4, bVar, this);
            if (obj == a2) {
                return a2;
            }
            this.this$0.j.clear();
            this.this$0.j.addAll((List) obj);
            this.this$0.i();
            return i97.a;
        }
    }

    @DexIgnore
    public m66(i66 i66, MicroAppRepository microAppRepository, ch5 ch5, PortfolioApp portfolioApp) {
        ee7.b(i66, "mView");
        ee7.b(microAppRepository, "mMicroAppRepository");
        ee7.b(ch5, "sharedPreferencesManager");
        ee7.b(portfolioApp, "mApp");
        this.k = i66;
        this.l = microAppRepository;
        this.m = ch5;
        this.n = portfolioApp;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
    }

    @DexIgnore
    public final void i() {
        if (this.j.isEmpty()) {
            this.k.b(a(ea7.d((Collection) this.i)));
        } else {
            this.k.c(a(ea7.d((Collection) this.j)));
        }
        if (!TextUtils.isEmpty(this.h)) {
            i66 i66 = this.k;
            String str = this.h;
            if (str != null) {
                i66.b(str);
                String str2 = this.h;
                if (str2 != null) {
                    a(str2);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void j() {
        this.k.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.h66
    public void h() {
        this.h = "";
        this.k.n();
        i();
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        ee7.b(str, "watchAppTop");
        ee7.b(str2, "watchAppMiddle");
        ee7.b(str3, "watchAppBottom");
        this.e = str;
        this.g = str3;
        this.f = str2;
    }

    @DexIgnore
    @Override // com.fossil.h66
    public void a(String str) {
        ee7.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        ik7 unused = xh7.b(e(), null, null, new a(this, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.h66
    public void a(MicroApp microApp) {
        ee7.b(microApp, "selectedMicroApp");
        List<String> t = this.m.t();
        ee7.a((Object) t, "sharedPreferencesManager.microAppSearchedIdsRecent");
        if (!t.contains(microApp.getId())) {
            t.add(0, microApp.getId());
            if (t.size() > 5) {
                t = t.subList(0, 5);
            }
            this.m.c(t);
        }
        this.k.a(microApp);
    }

    @DexIgnore
    public final List<r87<MicroApp, String>> a(List<MicroApp> list) {
        ArrayList arrayList = new ArrayList();
        for (MicroApp microApp : list) {
            String id = microApp.getId();
            if (ee7.a((Object) id, (Object) this.e)) {
                arrayList.add(new r87(microApp, ViewHierarchy.DIMENSION_TOP_KEY));
            } else if (ee7.a((Object) id, (Object) this.f)) {
                arrayList.add(new r87(microApp, "middle"));
            } else if (ee7.a((Object) id, (Object) this.g)) {
                arrayList.add(new r87(microApp, "bottom"));
            } else {
                arrayList.add(new r87(microApp, ""));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        if (bundle != null) {
            bundle.putString(ViewHierarchy.DIMENSION_TOP_KEY, this.e);
            bundle.putString("middle", this.f);
            bundle.putString("bottom", this.g);
        }
        return bundle;
    }
}
