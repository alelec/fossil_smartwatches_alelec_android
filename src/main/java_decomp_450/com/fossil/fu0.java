package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fu0 extends s81 {
    @DexIgnore
    public /* final */ ig0 T;

    @DexIgnore
    public fu0(ri1 ri1, en0 en0, ig0 ig0) {
        super(ri1, en0, wm0.i0, true, ig0.b(), ig0.getData(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.T = ig0;
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.v61, com.fossil.jr1
    public JSONObject i() {
        return yz0.a(super.i(), r51.x3, this.T.a());
    }
}
