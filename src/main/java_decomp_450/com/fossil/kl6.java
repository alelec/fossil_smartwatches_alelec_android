package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.fossil.cy6;
import com.fossil.nl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kl6 extends go5 implements gy6, cy6.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static String p;
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public nl6 g;
    @DexIgnore
    public qw6<kz4> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return kl6.p;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<nl6.b> {
        @DexIgnore
        public /* final */ /* synthetic */ kl6 a;

        @DexIgnore
        public b(kl6 kl6) {
            this.a = kl6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(nl6.b bVar) {
            if (bVar != null) {
                Integer b = bVar.b();
                if (b != null) {
                    this.a.o(b.intValue());
                }
                Integer a2 = bVar.a();
                if (a2 != null) {
                    this.a.n(a2.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ kl6 a;

        @DexIgnore
        public c(kl6 kl6) {
            this.a = kl6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, Action.Presenter.NEXT);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ kl6 a;

        @DexIgnore
        public d(kl6 kl6) {
            this.a = kl6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.i(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = kl6.class.getSimpleName();
        ee7.a((Object) simpleName, "CustomizeBackgroundFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        FLogger.INSTANCE.getLocal().d(j, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363307) {
            nl6 nl6 = this.g;
            if (nl6 != null) {
                nl6.a(cn6.q.a(), p);
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.gy6
    public void b(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        we7 we7 = we7.a;
        String format = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(i3 & 16777215)}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        nl6 nl6 = this.g;
        if (nl6 != null) {
            nl6.a(i2, Color.parseColor(format));
            if (i2 == 301) {
                p = format;
                return;
            }
            return;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.gy6
    public void j(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    public final void n(int i2) {
        qw6<kz4> qw6 = this.h;
        if (qw6 != null) {
            kz4 a2 = qw6.a();
            if (a2 != null) {
                a2.u.setBackgroundColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void o(int i2) {
        qw6<kz4> qw6 = this.h;
        if (qw6 != null) {
            kz4 a2 = qw6.a();
            if (a2 != null) {
                a2.t.setBackgroundColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        kz4 kz4 = (kz4) qb.a(LayoutInflater.from(getContext()), 2131558531, null, false, a1());
        PortfolioApp.g0.c().f().a(new ml6()).a(this);
        rj4 rj4 = this.f;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(nl6.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026undViewModel::class.java)");
            nl6 nl6 = (nl6) a2;
            this.g = nl6;
            if (nl6 != null) {
                nl6.b().a(getViewLifecycleOwner(), new b(this));
                nl6 nl62 = this.g;
                if (nl62 != null) {
                    nl62.c();
                    this.h = new qw6<>(this, kz4);
                    ee7.a((Object) kz4, "binding");
                    return kz4.d();
                }
                ee7.d("mViewModel");
                throw null;
            }
            ee7.d("mViewModel");
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(j, "onDestroy");
        p = null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(j, "onResume");
        nl6 nl6 = this.g;
        if (nl6 != null) {
            nl6.c();
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<kz4> qw6 = this.h;
        if (qw6 != null) {
            kz4 a2 = qw6.a();
            if (a2 != null) {
                a2.s.setOnClickListener(new c(this));
                a2.r.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }
}
