package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qj2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<qj2> CREATOR; // = new tj2();
    @DexIgnore
    public /* final */ gc2 a;

    @DexIgnore
    public qj2(gc2 gc2) {
        this.a = gc2;
    }

    @DexIgnore
    public final gc2 e() {
        return this.a;
    }

    @DexIgnore
    public final String toString() {
        return String.format("ApplicationUnregistrationRequest{%s}", this.a);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, (Parcelable) this.a, i, false);
        k72.a(parcel, a2);
    }
}
