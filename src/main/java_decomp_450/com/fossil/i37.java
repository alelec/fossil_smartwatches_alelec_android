package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i37 extends v27 {
    @DexIgnore
    public i37(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    @Override // com.fossil.v27
    public int a() {
        return 5;
    }

    @DexIgnore
    @Override // com.fossil.v27
    public void a(Bundle bundle) {
        super.a(bundle);
        bundle.getString("_wxapi_payresp_prepayid");
        bundle.getString("_wxapi_payresp_returnkey");
        bundle.getString("_wxapi_payresp_extdata");
    }
}
