package com.fossil;

import android.os.Parcel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ib1 extends xp0 {
    @DexIgnore
    public static /* final */ o91 CREATOR; // = new o91(null);

    @DexIgnore
    public ib1(byte b) {
        super(ru0.HEARTBEAT_EVENT, b, false, 4);
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public byte[] b() {
        return new byte[]{jx0.b.a};
    }

    @DexIgnore
    public /* synthetic */ ib1(Parcel parcel, zd7 zd7) {
        super(parcel);
    }
}
