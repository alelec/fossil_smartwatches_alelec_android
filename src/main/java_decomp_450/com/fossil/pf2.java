package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.text.TextUtils;
import com.fossil.a12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pf2 extends n62<qf2> {
    @DexIgnore
    public /* final */ Bundle E;

    @DexIgnore
    public pf2(Context context, Looper looper, j62 j62, oy1 oy1, a12.b bVar, a12.c cVar) {
        super(context, looper, 16, j62, bVar, cVar);
        if (oy1 == null) {
            this.E = new Bundle();
            return;
        }
        throw new NoSuchMethodError();
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.internal.IAuthService");
        if (queryLocalInterface instanceof qf2) {
            return (qf2) queryLocalInterface;
        }
        return new rf2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final String i() {
        return "com.google.android.gms.auth.api.internal.IAuthService";
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.h62, com.fossil.n62
    public final int k() {
        return q02.a;
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.h62
    public final boolean n() {
        j62 H = H();
        return !TextUtils.isEmpty(H.b()) && !H.a(ny1.c).isEmpty();
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final String p() {
        return "com.google.android.gms.auth.service.START";
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final Bundle x() {
        return this.E;
    }
}
