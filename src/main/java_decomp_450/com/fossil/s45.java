package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s45 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ RTLImageView t;
    @DexIgnore
    public /* final */ DashBar u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ RecyclerView w;

    @DexIgnore
    public s45(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView, DashBar dashBar, ConstraintLayout constraintLayout, RecyclerView recyclerView) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = rTLImageView;
        this.u = dashBar;
        this.v = constraintLayout;
        this.w = recyclerView;
    }
}
