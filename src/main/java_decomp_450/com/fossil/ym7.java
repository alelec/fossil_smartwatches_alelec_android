package com.fossil;

import java.util.concurrent.RejectedExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ym7 extends ak7 {
    @DexIgnore
    public vm7 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ String f;

    @DexIgnore
    public ym7(int i, int i2, long j, String str) {
        this.c = i;
        this.d = i2;
        this.e = j;
        this.f = str;
        this.b = g();
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public void a(ib7 ib7, Runnable runnable) {
        try {
            vm7.a(this.b, runnable, null, false, 6, null);
        } catch (RejectedExecutionException unused) {
            fj7.h.a(ib7, runnable);
        }
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public void b(ib7 ib7, Runnable runnable) {
        try {
            vm7.a(this.b, runnable, null, true, 2, null);
        } catch (RejectedExecutionException unused) {
            fj7.h.b(ib7, runnable);
        }
    }

    @DexIgnore
    public final vm7 g() {
        return new vm7(this.c, this.d, this.e, this.f);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ym7(int i, int i2, String str, int i3, zd7 zd7) {
        this((i3 & 1) != 0 ? gn7.b : i, (i3 & 2) != 0 ? gn7.c : i2, (i3 & 4) != 0 ? "DefaultDispatcher" : str);
    }

    @DexIgnore
    public final ti7 a(int i) {
        if (i > 0) {
            return new an7(this, i, 1);
        }
        throw new IllegalArgumentException(("Expected positive parallelism level, but have " + i).toString());
    }

    @DexIgnore
    public final void a(Runnable runnable, en7 en7, boolean z) {
        try {
            this.b.a(runnable, en7, z);
        } catch (RejectedExecutionException unused) {
            fj7.h.a(this.b.a(runnable, en7));
        }
    }

    @DexIgnore
    public ym7(int i, int i2, String str) {
        this(i, i2, gn7.d, str);
    }
}
