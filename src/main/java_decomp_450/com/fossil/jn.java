package com.fossil;

import android.content.Context;
import android.content.Intent;
import androidx.work.impl.background.systemalarm.ConstraintProxy;
import com.fossil.ln;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jn {
    @DexIgnore
    public static /* final */ String e; // = im.a("ConstraintsCmdHandler");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ln c;
    @DexIgnore
    public /* final */ sn d;

    @DexIgnore
    public jn(Context context, int i, ln lnVar) {
        this.a = context;
        this.b = i;
        this.c = lnVar;
        this.d = new sn(this.a, lnVar.d(), null);
    }

    @DexIgnore
    public void a() {
        List<zo> b2 = this.c.e().f().f().b();
        ConstraintProxy.a(this.a, b2);
        this.d.a(b2);
        ArrayList<zo> arrayList = new ArrayList(b2.size());
        long currentTimeMillis = System.currentTimeMillis();
        for (zo zoVar : b2) {
            String str = zoVar.a;
            if (currentTimeMillis >= zoVar.a() && (!zoVar.b() || this.d.a(str))) {
                arrayList.add(zoVar);
            }
        }
        for (zo zoVar2 : arrayList) {
            String str2 = zoVar2.a;
            Intent a2 = in.a(this.a, str2);
            im.a().a(e, String.format("Creating a delay_met command for workSpec with id (%s)", str2), new Throwable[0]);
            ln lnVar = this.c;
            lnVar.a(new ln.b(lnVar, a2, this.b));
        }
        this.d.a();
    }
}
