package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i52 implements a42 {
    @DexIgnore
    public /* final */ /* synthetic */ h52 a;

    @DexIgnore
    public i52(h52 h52) {
        this.a = h52;
    }

    @DexIgnore
    @Override // com.fossil.a42
    public final void a(Bundle bundle) {
        this.a.r.lock();
        try {
            i02 unused = this.a.p = i02.e;
            this.a.h();
        } finally {
            this.a.r.unlock();
        }
    }

    @DexIgnore
    public /* synthetic */ i52(h52 h52, g52 g52) {
        this(h52);
    }

    @DexIgnore
    @Override // com.fossil.a42
    public final void a(i02 i02) {
        this.a.r.lock();
        try {
            i02 unused = this.a.p = i02;
            this.a.h();
        } finally {
            this.a.r.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.a42
    public final void a(int i, boolean z) {
        this.a.r.lock();
        try {
            if (this.a.q) {
                boolean unused = this.a.q = false;
                this.a.a(i, z);
                return;
            }
            boolean unused2 = this.a.q = true;
            this.a.d.a(i);
            this.a.r.unlock();
        } finally {
            this.a.r.unlock();
        }
    }
}
