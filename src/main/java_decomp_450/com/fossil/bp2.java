package com.fossil;

import com.fossil.bw2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bp2 extends bw2<bp2, a> implements lx2 {
    @DexIgnore
    public static /* final */ bp2 zzh;
    @DexIgnore
    public static volatile wx2<bp2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public ep2 zzd;
    @DexIgnore
    public cp2 zze;
    @DexIgnore
    public boolean zzf;
    @DexIgnore
    public String zzg; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<bp2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(bp2.zzh);
        }

        @DexIgnore
        public final a a(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((bp2) ((bw2.a) this).b).a(str);
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(yo2 yo2) {
            this();
        }
    }

    /*
    static {
        bp2 bp2 = new bp2();
        zzh = bp2;
        bw2.a(bp2.class, bp2);
    }
    */

    @DexIgnore
    public static bp2 w() {
        return zzh;
    }

    @DexIgnore
    public final void a(String str) {
        str.getClass();
        this.zzc |= 8;
        this.zzg = str;
    }

    @DexIgnore
    public final ep2 p() {
        ep2 ep2 = this.zzd;
        return ep2 == null ? ep2.w() : ep2;
    }

    @DexIgnore
    public final boolean q() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final cp2 r() {
        cp2 cp2 = this.zze;
        return cp2 == null ? cp2.y() : cp2;
    }

    @DexIgnore
    public final boolean s() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final boolean t() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean u() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final String v() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean zza() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (yo2.a[i - 1]) {
            case 1:
                return new bp2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u1009\u0000\u0002\u1009\u0001\u0003\u1007\u0002\u0004\u1008\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                wx2<bp2> wx2 = zzi;
                if (wx2 == null) {
                    synchronized (bp2.class) {
                        wx2 = zzi;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzh);
                            zzi = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
