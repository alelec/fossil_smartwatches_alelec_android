package com.fossil;

import java.io.File;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class r64 implements Comparator {
    @DexIgnore
    public static /* final */ r64 a; // = new r64();

    @DexIgnore
    public static Comparator a() {
        return a;
    }

    @DexIgnore
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return u64.c((File) obj, (File) obj2);
    }
}
