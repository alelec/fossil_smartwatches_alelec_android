package com.fossil;

import com.fossil.tp3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dq3 implements sp3 {
    @DexIgnore
    public /* final */ tp3.b a;

    @DexIgnore
    public dq3(tp3.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    @Override // com.fossil.sp3
    public final void a(rp3 rp3) {
        this.a.a(cq3.a(rp3));
    }

    @DexIgnore
    @Override // com.fossil.sp3
    public final void b(rp3 rp3, int i, int i2) {
        this.a.b(cq3.a(rp3), i, i2);
    }

    @DexIgnore
    @Override // com.fossil.sp3
    public final void c(rp3 rp3, int i, int i2) {
        this.a.c(cq3.a(rp3), i, i2);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || dq3.class != obj.getClass()) {
            return false;
        }
        return this.a.equals(((dq3) obj).a);
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.sp3
    public final void a(rp3 rp3, int i, int i2) {
        this.a.a(cq3.a(rp3), i, i2);
    }
}
