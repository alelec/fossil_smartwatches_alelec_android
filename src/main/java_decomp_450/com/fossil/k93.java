package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ab2;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k93 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<k93> CREATOR; // = new ca3();
    @DexIgnore
    public LatLng a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public y83 d;
    @DexIgnore
    public float e; // = 0.5f;
    @DexIgnore
    public float f; // = 1.0f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public boolean i; // = false;
    @DexIgnore
    public float j; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public float p; // = 0.5f;
    @DexIgnore
    public float q; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public float r; // = 1.0f;
    @DexIgnore
    public float s;

    @DexIgnore
    public k93() {
    }

    @DexIgnore
    public final String A() {
        return this.b;
    }

    @DexIgnore
    public final float B() {
        return this.s;
    }

    @DexIgnore
    public final boolean C() {
        return this.g;
    }

    @DexIgnore
    public final boolean D() {
        return this.i;
    }

    @DexIgnore
    public final boolean E() {
        return this.h;
    }

    @DexIgnore
    public final k93 a(LatLng latLng) {
        if (latLng != null) {
            this.a = latLng;
            return this;
        }
        throw new IllegalArgumentException("latlng cannot be null - a position is required.");
    }

    @DexIgnore
    public final k93 b(float f2, float f3) {
        this.p = f2;
        this.q = f3;
        return this;
    }

    @DexIgnore
    public final k93 c(float f2) {
        this.s = f2;
        return this;
    }

    @DexIgnore
    public final float e() {
        return this.r;
    }

    @DexIgnore
    public final float g() {
        return this.e;
    }

    @DexIgnore
    public final LatLng getPosition() {
        return this.a;
    }

    @DexIgnore
    public final float v() {
        return this.f;
    }

    @DexIgnore
    public final float w() {
        return this.p;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        IBinder iBinder;
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, (Parcelable) getPosition(), i2, false);
        k72.a(parcel, 3, A(), false);
        k72.a(parcel, 4, z(), false);
        y83 y83 = this.d;
        if (y83 == null) {
            iBinder = null;
        } else {
            iBinder = y83.a().asBinder();
        }
        k72.a(parcel, 5, iBinder, false);
        k72.a(parcel, 6, g());
        k72.a(parcel, 7, v());
        k72.a(parcel, 8, C());
        k72.a(parcel, 9, E());
        k72.a(parcel, 10, D());
        k72.a(parcel, 11, y());
        k72.a(parcel, 12, w());
        k72.a(parcel, 13, x());
        k72.a(parcel, 14, e());
        k72.a(parcel, 15, B());
        k72.a(parcel, a2);
    }

    @DexIgnore
    public final float x() {
        return this.q;
    }

    @DexIgnore
    public final float y() {
        return this.j;
    }

    @DexIgnore
    public final String z() {
        return this.c;
    }

    @DexIgnore
    public final k93 c(String str) {
        this.b = str;
        return this;
    }

    @DexIgnore
    public final k93 a(y83 y83) {
        this.d = y83;
        return this;
    }

    @DexIgnore
    public final k93 b(String str) {
        this.c = str;
        return this;
    }

    @DexIgnore
    public final k93 c(boolean z) {
        this.h = z;
        return this;
    }

    @DexIgnore
    public final k93 a(float f2, float f3) {
        this.e = f2;
        this.f = f3;
        return this;
    }

    @DexIgnore
    public final k93 b(boolean z) {
        this.i = z;
        return this;
    }

    @DexIgnore
    public final k93 b(float f2) {
        this.j = f2;
        return this;
    }

    @DexIgnore
    public final k93 a(boolean z) {
        this.g = z;
        return this;
    }

    @DexIgnore
    public final k93 a(float f2) {
        this.r = f2;
        return this;
    }

    @DexIgnore
    public k93(LatLng latLng, String str, String str2, IBinder iBinder, float f2, float f3, boolean z, boolean z2, boolean z3, float f4, float f5, float f6, float f7, float f8) {
        this.a = latLng;
        this.b = str;
        this.c = str2;
        if (iBinder == null) {
            this.d = null;
        } else {
            this.d = new y83(ab2.a.a(iBinder));
        }
        this.e = f2;
        this.f = f3;
        this.g = z;
        this.h = z2;
        this.i = z3;
        this.j = f4;
        this.p = f5;
        this.q = f6;
        this.r = f7;
        this.s = f8;
    }
}
