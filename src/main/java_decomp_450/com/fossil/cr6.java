package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.cy6;
import com.fossil.gx6;
import com.fossil.ov3;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cr6 extends mr6 implements tr6, cy6.g {
    @DexIgnore
    public static /* final */ String v; // = v;
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public rr6 g;
    @DexIgnore
    public cs6 h;
    @DexIgnore
    public qw6<s65> i;
    @DexIgnore
    public cm4 j;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public int q;
    @DexIgnore
    public /* final */ String r; // = eh5.l.a().b("disabledButton");
    @DexIgnore
    public /* final */ String s; // = eh5.l.a().b("primaryColor");
    @DexIgnore
    public /* final */ String t; // = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final cr6 a(String str, boolean z, int i) {
            ee7.b(str, "serial");
            Bundle bundle = new Bundle();
            bundle.putString("SERIAL", str);
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            bundle.putInt(cr6.v, i);
            cr6 cr6 = new cr6();
            cr6.setArguments(bundle);
            return cr6;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cr6 a;

        @DexIgnore
        public b(cr6 cr6, String str) {
            this.a = cr6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1().l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ s65 a;
        @DexIgnore
        public /* final */ /* synthetic */ cr6 b;

        @DexIgnore
        public c(s65 s65, cr6 cr6, String str) {
            this.a = s65;
            this.b = cr6;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void a(int i, float f, int i2) {
            Drawable b2;
            Drawable b3;
            super.a(i, f, i2);
            if (!TextUtils.isEmpty(this.b.s)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("UpdateFirmwareFragment", "set icon color " + this.b.s);
                int parseColor = Color.parseColor(this.b.s);
                TabLayout.g b4 = this.a.A.b(i);
                if (!(b4 == null || (b3 = b4.b()) == null)) {
                    b3.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.r) && this.b.q != i) {
                int parseColor2 = Color.parseColor(this.b.r);
                TabLayout.g b5 = this.a.A.b(this.b.q);
                if (!(b5 == null || (b2 = b5.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.b.q = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cr6 a;

        @DexIgnore
        public d(cr6 cr6, String str) {
            this.a = cr6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1().n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cr6 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public e(cr6 cr6, String str) {
            this.a = cr6;
            this.b = str;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.getActivity() != null) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
                Context requireContext = this.a.requireContext();
                ee7.a((Object) requireContext, "requireContext()");
                TroubleshootingActivity.a.a(aVar, requireContext, this.b, false, false, 12, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ov3.b {
        @DexIgnore
        public /* final */ /* synthetic */ cr6 a;

        @DexIgnore
        public f(cr6 cr6) {
            this.a = cr6;
        }

        @DexIgnore
        @Override // com.fossil.ov3.b
        public final void a(TabLayout.g gVar, int i) {
            ee7.b(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.r) && !TextUtils.isEmpty(this.a.s)) {
                int parseColor = Color.parseColor(this.a.r);
                int parseColor2 = Color.parseColor(this.a.s);
                gVar.b(2131230963);
                if (i == this.a.q) {
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable b2 = gVar.b();
                if (b2 != null) {
                    b2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.mr6
    public void Z0() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void d(List<? extends Explore> list) {
        ee7.b(list, "data");
        cm4 cm4 = this.j;
        if (cm4 != null) {
            cm4.a(list);
        } else {
            ee7.d("mAdapterUpdateFirmware");
            throw null;
        }
    }

    @DexIgnore
    public final void e() {
        DashBar dashBar;
        qw6<s65> qw6 = this.i;
        if (qw6 != null) {
            s65 a2 = qw6.a();
            if (a2 != null && (dashBar = a2.C) != null) {
                dashBar.setVisibility(0);
                gx6.a aVar = gx6.a;
                ee7.a((Object) dashBar, "this");
                aVar.f(dashBar, this.p, 500);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final rr6 f1() {
        rr6 rr6 = this.g;
        if (rr6 != null) {
            return rr6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void g1() {
        DashBar dashBar;
        qw6<s65> qw6 = this.i;
        if (qw6 != null) {
            s65 a2 = qw6.a();
            if (a2 != null && (dashBar = a2.C) != null) {
                dashBar.setVisibility(0);
                gx6.a aVar = gx6.a;
                ee7.a((Object) dashBar, "this");
                aVar.c(dashBar, this.p, 500);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void h1() {
        qw6<s65> qw6 = this.i;
        if (qw6 != null) {
            s65 a2 = qw6.a();
            TabLayout tabLayout = a2 != null ? a2.A : null;
            if (tabLayout != null) {
                qw6<s65> qw62 = this.i;
                if (qw62 != null) {
                    s65 a3 = qw62.a();
                    ViewPager2 viewPager2 = a3 != null ? a3.F : null;
                    if (viewPager2 != null) {
                        new ov3(tabLayout, viewPager2, new f(this)).a();
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void k0() {
        if (isActive()) {
            qw6<s65> qw6 = this.i;
            if (qw6 != null) {
                s65 a2 = qw6.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    ee7.a((Object) constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    ee7.a((Object) constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        s65 s65 = (s65) qb.a(layoutInflater, 2131558631, viewGroup, false, a1());
        this.i = new qw6<>(this, s65);
        ee7.a((Object) s65, "binding");
        return s65.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.mr6, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        cs6 cs6 = this.h;
        if (cs6 != null) {
            cs6.e();
        } else {
            ee7.d("mSubPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        cs6 cs6 = this.h;
        if (cs6 != null) {
            cs6.d();
        } else {
            ee7.d("mSubPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        String string;
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        this.p = arguments != null ? arguments.getBoolean("IS_ONBOARDING_FLOW") : false;
        Bundle arguments2 = getArguments();
        String str = "";
        if (!(arguments2 == null || (string = arguments2.getString("SERIAL", str)) == null)) {
            str = string;
        }
        Bundle arguments3 = getArguments();
        int i2 = arguments3 != null ? arguments3.getInt(v) : 0;
        this.h = new cs6(str, this);
        this.j = new cm4(new ArrayList());
        qw6<s65> qw6 = this.i;
        if (qw6 != null) {
            s65 a2 = qw6.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                ee7.a((Object) constraintLayout, "binding.clUpdateFwFail");
                constraintLayout.setVisibility(8);
                ConstraintLayout constraintLayout2 = a2.r;
                ee7.a((Object) constraintLayout2, "binding.clUpdatingFw");
                constraintLayout2.setVisibility(0);
                FlexibleProgressBar flexibleProgressBar = a2.D;
                ee7.a((Object) flexibleProgressBar, "binding.progressUpdate");
                flexibleProgressBar.setMax(1000);
                FlexibleButton flexibleButton = a2.s;
                ee7.a((Object) flexibleButton, "binding.fbContinue");
                flexibleButton.setVisibility(8);
                FlexibleTextView flexibleTextView = a2.z;
                ee7.a((Object) flexibleTextView, "binding.ftvUpdateWarning");
                flexibleTextView.setVisibility(0);
                a2.s.setOnClickListener(new b(this, str));
                ViewPager2 viewPager2 = a2.F;
                ee7.a((Object) viewPager2, "binding.rvpTutorial");
                cm4 cm4 = this.j;
                if (cm4 != null) {
                    viewPager2.setAdapter(cm4);
                    if (a2.F.getChildAt(0) != null) {
                        View childAt = a2.F.getChildAt(0);
                        if (childAt != null) {
                            ((RecyclerView) childAt).setOverScrollMode(2);
                        } else {
                            throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                        }
                    }
                    if (!TextUtils.isEmpty(this.t)) {
                        TabLayout tabLayout = a2.A;
                        ee7.a((Object) tabLayout, "binding.indicator");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.t)));
                    }
                    h1();
                    a2.F.a(new c(a2, this, str));
                    a2.t.setOnClickListener(new d(this, str));
                    a2.w.setOnClickListener(new e(this, str));
                } else {
                    ee7.d("mAdapterUpdateFirmware");
                    throw null;
                }
            }
            if (i2 == 1) {
                t0();
            } else if (i2 == 2) {
                c(true);
            } else if (i2 == 3) {
                c(false);
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void t0() {
        if (isActive()) {
            qw6<s65> qw6 = this.i;
            if (qw6 != null) {
                s65 a2 = qw6.a();
                if (a2 != null) {
                    cs6 cs6 = this.h;
                    if (cs6 != null) {
                        cs6.f();
                        ConstraintLayout constraintLayout = a2.q;
                        ee7.a((Object) constraintLayout, "it.clUpdateFwFail");
                        constraintLayout.setVisibility(8);
                        ConstraintLayout constraintLayout2 = a2.r;
                        ee7.a((Object) constraintLayout2, "it.clUpdatingFw");
                        constraintLayout2.setVisibility(0);
                        FlexibleButton flexibleButton = a2.s;
                        ee7.a((Object) flexibleButton, "it.fbContinue");
                        flexibleButton.setVisibility(0);
                        FlexibleTextView flexibleTextView = a2.z;
                        ee7.a((Object) flexibleTextView, "it.ftvUpdateWarning");
                        flexibleTextView.setVisibility(4);
                        FlexibleProgressBar flexibleProgressBar = a2.D;
                        ee7.a((Object) flexibleProgressBar, "it.progressUpdate");
                        flexibleProgressBar.setVisibility(8);
                        FlexibleTextView flexibleTextView2 = a2.u;
                        ee7.a((Object) flexibleTextView2, "it.ftvCountdownTime");
                        flexibleTextView2.setVisibility(8);
                        FlexibleTextView flexibleTextView3 = a2.y;
                        ee7.a((Object) flexibleTextView3, "it.ftvUpdate");
                        flexibleTextView3.setText(ig5.a(PortfolioApp.g0.c(), 2131886732));
                        return;
                    }
                    ee7.d("mSubPresenter");
                    throw null;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void c(int i2) {
        FlexibleProgressBar flexibleProgressBar;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i2);
        qw6<s65> qw6 = this.i;
        if (qw6 != null) {
            s65 a2 = qw6.a();
            if (a2 != null && (flexibleProgressBar = a2.D) != null) {
                flexibleProgressBar.setProgress(i2);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(rr6 rr6) {
        ee7.b(rr6, "presenter");
        this.g = rr6;
    }

    @DexIgnore
    public final void c(boolean z) {
        if (isActive()) {
            qw6<s65> qw6 = this.i;
            if (qw6 != null) {
                s65 a2 = qw6.a();
                if (a2 == null) {
                    return;
                }
                if (z) {
                    ConstraintLayout constraintLayout = a2.q;
                    ee7.a((Object) constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    ee7.a((Object) constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    FlexibleButton flexibleButton = a2.s;
                    ee7.a((Object) flexibleButton, "it.fbContinue");
                    flexibleButton.setVisibility(0);
                    FlexibleTextView flexibleTextView = a2.z;
                    ee7.a((Object) flexibleTextView, "it.ftvUpdateWarning");
                    flexibleTextView.setVisibility(4);
                    FlexibleProgressBar flexibleProgressBar = a2.D;
                    ee7.a((Object) flexibleProgressBar, "it.progressUpdate");
                    flexibleProgressBar.setProgress(1000);
                    FlexibleTextView flexibleTextView2 = a2.y;
                    ee7.a((Object) flexibleTextView2, "it.ftvUpdate");
                    flexibleTextView2.setText(ig5.a(PortfolioApp.g0.c(), 2131886991));
                    return;
                }
                ConstraintLayout constraintLayout3 = a2.q;
                ee7.a((Object) constraintLayout3, "it.clUpdateFwFail");
                constraintLayout3.setVisibility(0);
                ConstraintLayout constraintLayout4 = a2.r;
                ee7.a((Object) constraintLayout4, "it.clUpdatingFw");
                constraintLayout4.setVisibility(8);
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        if (str.hashCode() == 927511079 && str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("UpdateFirmwareFragment", "Update firmware fail isOnboardingFlow " + this.p);
            if (i2 == 2131362268) {
                rr6 rr6 = this.g;
                if (rr6 != null) {
                    rr6.n();
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            } else if (i2 == 2131362359 && getActivity() != null) {
                HelpActivity.a aVar = HelpActivity.z;
                FragmentActivity requireActivity = requireActivity();
                ee7.a((Object) requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }
}
