package com.fossil;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ze {
    @DexIgnore
    public af a;

    @DexIgnore
    public ze(String str, int i, int i2) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.a = new bf(str, i, i2);
        } else {
            this.a = new cf(str, i, i2);
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ze)) {
            return false;
        }
        return this.a.equals(((ze) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }
}
