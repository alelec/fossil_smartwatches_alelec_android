package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lx3<E> implements Iterable<E> {
    @DexIgnore
    public /* final */ hw3<Iterable<E>> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends lx3<E> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Iterable iterable, Iterable iterable2) {
            super(iterable);
            this.b = iterable2;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<E> iterator() {
            return this.b.iterator();
        }
    }

    @DexIgnore
    public lx3() {
        this.a = hw3.absent();
    }

    @DexIgnore
    public final Iterable<E> a() {
        return this.a.or(this);
    }

    @DexIgnore
    public final iy3<E> b() {
        return iy3.copyOf(a());
    }

    @DexIgnore
    public String toString() {
        return py3.d(a());
    }

    @DexIgnore
    public static <E> lx3<E> a(Iterable<E> iterable) {
        return iterable instanceof lx3 ? (lx3) iterable : new a(iterable, iterable);
    }

    @DexIgnore
    public lx3(Iterable<E> iterable) {
        jw3.a(iterable);
        this.a = hw3.fromNullable(this == iterable ? null : iterable);
    }

    @DexIgnore
    public final lx3<E> a(kw3<? super E> kw3) {
        return a(py3.b(a(), kw3));
    }
}
