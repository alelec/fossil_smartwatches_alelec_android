package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f25 extends e25 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i P; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Q;
    @DexIgnore
    public long O;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        Q = sparseIntArray;
        sparseIntArray.put(2131362109, 1);
        Q.put(2131361851, 2);
        Q.put(2131363342, 3);
        Q.put(2131362798, 4);
        Q.put(2131361939, 5);
        Q.put(2131362569, 6);
        Q.put(2131363267, 7);
        Q.put(2131363393, 8);
        Q.put(2131361956, 9);
        Q.put(2131362576, 10);
        Q.put(2131363388, 11);
        Q.put(2131361950, 12);
        Q.put(2131362574, 13);
        Q.put(2131363236, 14);
        Q.put(2131362048, 15);
        Q.put(2131363458, 16);
        Q.put(2131363262, 17);
        Q.put(2131363461, 18);
        Q.put(2131363290, 19);
        Q.put(2131363456, 20);
        Q.put(2131363228, 21);
        Q.put(2131363253, 22);
        Q.put(2131363199, 23);
    }
    */

    @DexIgnore
    public f25(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 24, P, Q));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.O = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.O != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.O = 1;
        }
        g();
    }

    @DexIgnore
    public f25(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (RTLImageView) objArr[2], (RelativeLayout) objArr[5], (RelativeLayout) objArr[12], (RelativeLayout) objArr[9], (ConstraintLayout) objArr[15], (ConstraintLayout) objArr[1], (RTLImageView) objArr[6], (RTLImageView) objArr[13], (RTLImageView) objArr[10], (LinearLayout) objArr[4], (RelativeLayout) objArr[0], (FlexibleTextView) objArr[23], (FlexibleTextView) objArr[21], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[22], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[19], (FlexibleTextView) objArr[3], (View) objArr[11], (View) objArr[8], (CustomizeWidget) objArr[20], (CustomizeWidget) objArr[16], (CustomizeWidget) objArr[18]);
        this.O = -1;
        ((e25) this).A.setTag(null);
        a(view);
        f();
    }
}
