package com.fossil;

import com.fossil.wf7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface yf7<T, R> extends bg7<T, R>, wf7<R> {

    @DexIgnore
    public interface a<T, R> extends wf7.a<R>, kd7<T, R, i97> {
    }

    @DexIgnore
    a<T, R> getSetter();
}
