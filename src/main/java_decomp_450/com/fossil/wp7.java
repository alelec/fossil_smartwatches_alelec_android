package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wp7 {
    @DexIgnore
    public static /* final */ br7 a; // = br7.encodeUtf8("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");
    @DexIgnore
    public static /* final */ String[] b; // = {"DATA", "HEADERS", "PRIORITY", "RST_STREAM", "SETTINGS", "PUSH_PROMISE", "PING", "GOAWAY", "WINDOW_UPDATE", "CONTINUATION"};
    @DexIgnore
    public static /* final */ String[] c; // = new String[64];
    @DexIgnore
    public static /* final */ String[] d; // = new String[256];

    /*
    static {
        int i = 0;
        int i2 = 0;
        while (true) {
            String[] strArr = d;
            if (i2 >= strArr.length) {
                break;
            }
            strArr[i2] = ro7.a("%8s", Integer.toBinaryString(i2)).replace(' ', '0');
            i2++;
        }
        String[] strArr2 = c;
        strArr2[0] = "";
        strArr2[1] = "END_STREAM";
        int[] iArr = {1};
        strArr2[8] = "PADDED";
        for (int i3 = 0; i3 < 1; i3++) {
            int i4 = iArr[i3];
            c[i4 | 8] = c[i4] + "|PADDED";
        }
        String[] strArr3 = c;
        strArr3[4] = "END_HEADERS";
        strArr3[32] = "PRIORITY";
        strArr3[36] = "END_HEADERS|PRIORITY";
        int[] iArr2 = {4, 32, 36};
        for (int i5 = 0; i5 < 3; i5++) {
            int i6 = iArr2[i5];
            for (int i7 = 0; i7 < 1; i7++) {
                int i8 = iArr[i7];
                String[] strArr4 = c;
                int i9 = i8 | i6;
                strArr4[i9] = c[i8] + '|' + c[i6];
                c[i9 | 8] = c[i8] + '|' + c[i6] + "|PADDED";
            }
        }
        while (true) {
            String[] strArr5 = c;
            if (i < strArr5.length) {
                if (strArr5[i] == null) {
                    strArr5[i] = d[i];
                }
                i++;
            } else {
                return;
            }
        }
    }
    */

    @DexIgnore
    public static IllegalArgumentException a(String str, Object... objArr) {
        throw new IllegalArgumentException(ro7.a(str, objArr));
    }

    @DexIgnore
    public static IOException b(String str, Object... objArr) throws IOException {
        throw new IOException(ro7.a(str, objArr));
    }

    @DexIgnore
    public static String a(boolean z, int i, int i2, byte b2, byte b3) {
        String[] strArr = b;
        String a2 = b2 < strArr.length ? strArr[b2] : ro7.a("0x%02x", Byte.valueOf(b2));
        String a3 = a(b2, b3);
        Object[] objArr = new Object[5];
        objArr[0] = z ? "<<" : ">>";
        objArr[1] = Integer.valueOf(i);
        objArr[2] = Integer.valueOf(i2);
        objArr[3] = a2;
        objArr[4] = a3;
        return ro7.a("%s 0x%08x %5d %-13s %s", objArr);
    }

    @DexIgnore
    public static String a(byte b2, byte b3) {
        if (b3 == 0) {
            return "";
        }
        if (!(b2 == 2 || b2 == 3)) {
            if (b2 == 4 || b2 == 6) {
                if (b3 == 1) {
                    return "ACK";
                }
                return d[b3];
            } else if (!(b2 == 7 || b2 == 8)) {
                String[] strArr = c;
                String str = b3 < strArr.length ? strArr[b3] : d[b3];
                if (b2 != 5 || (b3 & 4) == 0) {
                    return (b2 != 0 || (b3 & 32) == 0) ? str : str.replace("PRIORITY", "COMPRESSED");
                }
                return str.replace("HEADERS", "PUSH_PROMISE");
            }
        }
        return d[b3];
    }
}
