package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oa0 extends xg0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ zg0 b; // = zg0.TOP_SHORT_PRESS_RELEASE;
    @DexIgnore
    public static /* final */ zg0 c; // = zg0.MIDDLE_SHORT_PRESS_RELEASE;
    @DexIgnore
    public static /* final */ zg0 d; // = zg0.BOTTOM_SHORT_PRESS_RELEASE;
    @DexIgnore
    public /* final */ HashSet<na0> a;

    @DexIgnore
    public oa0(na0[] na0Arr) {
        HashSet<na0> hashSet = new HashSet<>();
        this.a = hashSet;
        ba7.a(hashSet, na0Arr);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return b();
    }

    @DexIgnore
    @Override // com.fossil.xg0
    public JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        for (T t : this.a) {
            jSONArray.put(t.a());
            yz0.a(jSONObject, t.b(), false, 2);
        }
        jSONObject.put("master._.config.buttons", jSONArray);
        return jSONObject;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(oa0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(ee7.a(this.a, ((oa0) obj).a) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.watchapp.WatchAppConfig");
    }

    @DexIgnore
    public final na0 getBottomButton() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (t.getButtonEvent() == d) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return t;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final na0 getMiddleButton() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (t.getButtonEvent() == c) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return t;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final na0 getTopButton() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (t.getButtonEvent() == b) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return t;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.xg0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            Object[] array = this.a.toArray(new na0[0]);
            if (array != null) {
                parcel.writeParcelableArray((Parcelable[]) array, i);
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<oa0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public oa0 createFromParcel(Parcel parcel) {
            return new oa0(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public oa0[] newArray(int i) {
            return new oa0[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public oa0 m46createFromParcel(Parcel parcel) {
            return new oa0(parcel);
        }
    }

    @DexIgnore
    public oa0(na0 na0, na0 na02, na0 na03) {
        this.a = new HashSet<>();
        na0.a(b);
        na02.a(c);
        na03.a(d);
        this.a.add(na0);
        this.a.add(na02);
        this.a.add(na03);
    }

    @DexIgnore
    public oa0(Parcel parcel) {
        super(parcel);
        HashSet<na0> hashSet = new HashSet<>();
        this.a = hashSet;
        Parcelable[] readParcelableArray = parcel.readParcelableArray(na0.class.getClassLoader());
        if (readParcelableArray != null) {
            ba7.a(hashSet, (na0[]) readParcelableArray);
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.device.data.watchapp.WatchApp>");
    }
}
