package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class e55 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText r;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText s;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText t;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout u;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout v;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout w;
    @DexIgnore
    public /* final */ RTLImageView x;
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public /* final */ ProgressButton z;

    @DexIgnore
    public e55(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleTextInputEditText flexibleTextInputEditText, FlexibleTextInputEditText flexibleTextInputEditText2, FlexibleTextInputEditText flexibleTextInputEditText3, FlexibleTextInputLayout flexibleTextInputLayout, FlexibleTextInputLayout flexibleTextInputLayout2, FlexibleTextInputLayout flexibleTextInputLayout3, RTLImageView rTLImageView, ConstraintLayout constraintLayout2, ProgressButton progressButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleTextInputEditText;
        this.s = flexibleTextInputEditText2;
        this.t = flexibleTextInputEditText3;
        this.u = flexibleTextInputLayout;
        this.v = flexibleTextInputLayout2;
        this.w = flexibleTextInputLayout3;
        this.x = rTLImageView;
        this.y = constraintLayout2;
        this.z = progressButton;
        this.A = flexibleTextView;
        this.B = flexibleTextView2;
    }
}
