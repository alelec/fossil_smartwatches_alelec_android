package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq4 implements Factory<aq4> {
    @DexIgnore
    public /* final */ Provider<fp4> a;
    @DexIgnore
    public /* final */ Provider<to4> b;
    @DexIgnore
    public /* final */ Provider<ch5> c;

    @DexIgnore
    public bq4(Provider<fp4> provider, Provider<to4> provider2, Provider<ch5> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static bq4 a(Provider<fp4> provider, Provider<to4> provider2, Provider<ch5> provider3) {
        return new bq4(provider, provider2, provider3);
    }

    @DexIgnore
    public static aq4 a(fp4 fp4, to4 to4, ch5 ch5) {
        return new aq4(fp4, to4, ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public aq4 get() {
        return a(this.a.get(), this.b.get(), this.c.get());
    }
}
