package com.fossil;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataType;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rd2 extends Service {
    @DexIgnore
    public a a;

    @DexIgnore
    public abstract List<gc2> a(List<DataType> list);

    @DexIgnore
    @TargetApi(19)
    public final void a() throws SecurityException {
        int callingUid = Binder.getCallingUid();
        if (v92.f()) {
            ((AppOpsManager) getSystemService("appops")).checkPackage(callingUid, "com.google.android.gms");
            return;
        }
        String[] packagesForUid = getPackageManager().getPackagesForUid(callingUid);
        if (packagesForUid != null) {
            int length = packagesForUid.length;
            int i = 0;
            while (i < length) {
                if (!packagesForUid[i].equals("com.google.android.gms")) {
                    i++;
                } else {
                    return;
                }
            }
        }
        throw new SecurityException("Unauthorized caller");
    }

    @DexIgnore
    public abstract boolean a(gc2 gc2);

    @DexIgnore
    public abstract boolean a(sd2 sd2);

    @DexIgnore
    public IBinder onBind(Intent intent) {
        if (!"com.google.android.gms.fitness.service.FitnessSensorService".equals(intent.getAction())) {
            return null;
        }
        if (Log.isLoggable("FitnessSensorService", 3)) {
            String valueOf = String.valueOf(intent);
            String name = rd2.class.getName();
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 20 + String.valueOf(name).length());
            sb.append("Intent ");
            sb.append(valueOf);
            sb.append(" received by ");
            sb.append(name);
            Log.d("FitnessSensorService", sb.toString());
        }
        a aVar = this.a;
        aVar.asBinder();
        return aVar;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.a = new a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends uj2 {
        @DexIgnore
        public /* final */ rd2 a;

        @DexIgnore
        public a(rd2 rd2) {
            this.a = rd2;
        }

        @DexIgnore
        @Override // com.fossil.sj2
        public final void a(pj2 pj2, ii2 ii2) throws RemoteException {
            this.a.a();
            ii2.a(new pd2(this.a.a(pj2.e()), Status.e));
        }

        @DexIgnore
        @Override // com.fossil.sj2
        public final void a(sd2 sd2, zi2 zi2) throws RemoteException {
            this.a.a();
            if (this.a.a(sd2)) {
                zi2.c(Status.e);
            } else {
                zi2.c(new Status(13));
            }
        }

        @DexIgnore
        @Override // com.fossil.sj2
        public final void a(qj2 qj2, zi2 zi2) throws RemoteException {
            this.a.a();
            if (this.a.a(qj2.e())) {
                zi2.c(Status.e);
            } else {
                zi2.c(new Status(13));
            }
        }
    }
}
