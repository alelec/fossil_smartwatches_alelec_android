package com.fossil;

import com.fossil.bw2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yp2 extends bw2<yp2, a> implements lx2 {
    @DexIgnore
    public static /* final */ yp2 zzf;
    @DexIgnore
    public static volatile wx2<yp2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public gw2 zze; // = bw2.n();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<yp2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(yp2.zzf);
        }

        @DexIgnore
        public final a a(int i) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((yp2) ((bw2.a) this).b).c(i);
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(op2 op2) {
            this();
        }

        @DexIgnore
        public final a a(Iterable<? extends Long> iterable) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((yp2) ((bw2.a) this).b).a(iterable);
            return this;
        }
    }

    /*
    static {
        yp2 yp2 = new yp2();
        zzf = yp2;
        bw2.a(yp2.class, yp2);
    }
    */

    @DexIgnore
    public static a s() {
        return (a) zzf.e();
    }

    @DexIgnore
    public final void a(Iterable<? extends Long> iterable) {
        gw2 gw2 = this.zze;
        if (!gw2.zza()) {
            this.zze = bw2.a(gw2);
        }
        ju2.a(iterable, this.zze);
    }

    @DexIgnore
    public final long b(int i) {
        return this.zze.zzb(i);
    }

    @DexIgnore
    public final void c(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    @DexIgnore
    public final int p() {
        return this.zzd;
    }

    @DexIgnore
    public final List<Long> q() {
        return this.zze;
    }

    @DexIgnore
    public final int r() {
        return this.zze.size();
    }

    @DexIgnore
    public final boolean zza() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (op2.a[i - 1]) {
            case 1:
                return new yp2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u1004\u0000\u0002\u0014", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                wx2<yp2> wx2 = zzg;
                if (wx2 == null) {
                    synchronized (yp2.class) {
                        wx2 = zzg;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzf);
                            zzg = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
