package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fn {
    @DexIgnore
    public static /* final */ String d; // = im.a("DelayedWorkTracker");
    @DexIgnore
    public /* final */ gn a;
    @DexIgnore
    public /* final */ om b;
    @DexIgnore
    public /* final */ Map<String, Runnable> c; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ zo a;

        @DexIgnore
        public a(zo zoVar) {
            this.a = zoVar;
        }

        @DexIgnore
        public void run() {
            im.a().a(fn.d, String.format("Scheduling work %s", this.a.a), new Throwable[0]);
            fn.this.a.a(this.a);
        }
    }

    @DexIgnore
    public fn(gn gnVar, om omVar) {
        this.a = gnVar;
        this.b = omVar;
    }

    @DexIgnore
    public void a(zo zoVar) {
        Runnable remove = this.c.remove(zoVar.a);
        if (remove != null) {
            this.b.a(remove);
        }
        a aVar = new a(zoVar);
        this.c.put(zoVar.a, aVar);
        long currentTimeMillis = System.currentTimeMillis();
        this.b.a(zoVar.a() - currentTimeMillis, aVar);
    }

    @DexIgnore
    public void a(String str) {
        Runnable remove = this.c.remove(str);
        if (remove != null) {
            this.b.a(remove);
        }
    }
}
