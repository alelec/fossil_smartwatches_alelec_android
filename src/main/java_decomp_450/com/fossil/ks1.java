package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ks1 extends fe7 implements gd7<eo0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ ri1 a;
    @DexIgnore
    public /* final */ /* synthetic */ eo0 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ks1(ri1 ri1, eo0 eo0) {
        super(1);
        this.a = ri1;
        this.b = eo0;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(eo0 eo0) {
        ri1 ri1 = this.a;
        eo0 eo02 = this.b;
        Iterator<b51> it = ri1.f.iterator();
        while (it.hasNext()) {
            try {
                ri1.a.post(new zp0(it.next(), eo02));
            } catch (Exception e) {
                wl0.h.a(e);
            }
        }
        return i97.a;
    }
}
