package com.fossil;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import com.zendesk.belvedere.BelvedereCallback;
import com.zendesk.belvedere.BelvedereResult;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v67 extends AsyncTask<Uri, Void, List<BelvedereResult>> {
    @DexIgnore
    public /* final */ BelvedereCallback<List<BelvedereResult>> a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ u67 c;
    @DexIgnore
    public /* final */ x67 d;

    @DexIgnore
    public v67(Context context, u67 u67, x67 x67, BelvedereCallback<List<BelvedereResult>> belvedereCallback) {
        this.b = context;
        this.c = u67;
        this.d = x67;
        this.a = belvedereCallback;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:100:0x012c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00eb A[SYNTHETIC, Splitter:B:60:0x00eb] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0114 A[SYNTHETIC, Splitter:B:71:0x0114] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0121  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0134 A[SYNTHETIC, Splitter:B:83:0x0134] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0141 A[SYNTHETIC, Splitter:B:88:0x0141] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x012c A[SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.zendesk.belvedere.BelvedereResult> doInBackground(android.net.Uri... r20) {
        /*
            r19 = this;
            r1 = r19
            r2 = r20
            java.lang.String r3 = "Error closing FileOutputStream"
            java.lang.String r4 = "Error closing InputStream"
            java.lang.String r5 = "BelvedereResolveUriTask"
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            int r7 = r2.length
            r8 = 0
            r9 = 0
        L_0x0012:
            if (r9 >= r7) goto L_0x014d
            r10 = r2[r9]
            android.content.Context r0 = r1.b     // Catch:{ FileNotFoundException -> 0x00fc, IOException -> 0x00d3, all -> 0x00cd }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ FileNotFoundException -> 0x00fc, IOException -> 0x00d3, all -> 0x00cd }
            java.io.InputStream r13 = r0.openInputStream(r10)     // Catch:{ FileNotFoundException -> 0x00fc, IOException -> 0x00d3, all -> 0x00cd }
            com.fossil.x67 r0 = r1.d     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x00c7, all -> 0x00c3 }
            android.content.Context r14 = r1.b     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x00c7, all -> 0x00c3 }
            java.io.File r0 = r0.c(r14, r10)     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x00c7, all -> 0x00c3 }
            r14 = 2
            if (r13 == 0) goto L_0x008a
            if (r0 != 0) goto L_0x002e
            goto L_0x008a
        L_0x002e:
            com.fossil.u67 r15 = r1.c     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x00c7, all -> 0x00c3 }
            java.util.Locale r11 = java.util.Locale.US     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x00c7, all -> 0x00c3 }
            java.lang.String r12 = "Copying media file into private cache - Uri: %s - Dest: %s"
            java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x00c7, all -> 0x00c3 }
            r14[r8] = r10     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x00c7, all -> 0x00c3 }
            r17 = 1
            r14[r17] = r0     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x00c7, all -> 0x00c3 }
            java.lang.String r11 = java.lang.String.format(r11, r12, r14)     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x00c7, all -> 0x00c3 }
            r15.d(r5, r11)     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x00c7, all -> 0x00c3 }
            java.io.FileOutputStream r11 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x00c7, all -> 0x00c3 }
            r11.<init>(r0)     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x00c7, all -> 0x00c3 }
            r12 = 1024(0x400, float:1.435E-42)
            byte[] r12 = new byte[r12]     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x007f, all -> 0x0078 }
        L_0x004c:
            int r14 = r13.read(r12)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x007f, all -> 0x0078 }
            if (r14 <= 0) goto L_0x0056
            r11.write(r12, r8, r14)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x007f, all -> 0x0078 }
            goto L_0x004c
        L_0x0056:
            com.zendesk.belvedere.BelvedereResult r12 = new com.zendesk.belvedere.BelvedereResult     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x007f, all -> 0x0078 }
            com.fossil.x67 r14 = r1.d     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x007f, all -> 0x0078 }
            android.content.Context r15 = r1.b     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x007f, all -> 0x0078 }
            android.net.Uri r14 = r14.a(r15, r0)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x007f, all -> 0x0078 }
            r12.<init>(r0, r14)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x007f, all -> 0x0078 }
            r6.add(r12)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x007f, all -> 0x0078 }
            if (r13 == 0) goto L_0x0073
            r13.close()     // Catch:{ IOException -> 0x006c }
            goto L_0x0073
        L_0x006c:
            r0 = move-exception
            r10 = r0
            com.fossil.u67 r0 = r1.c
            r0.e(r5, r4, r10)
        L_0x0073:
            r11.close()     // Catch:{ IOException -> 0x0125 }
            goto L_0x012c
        L_0x0078:
            r0 = move-exception
            r2 = r0
            r16 = r11
            r11 = r13
            goto L_0x0132
        L_0x007f:
            r0 = move-exception
            r16 = r11
            r11 = r13
            goto L_0x00d7
        L_0x0084:
            r0 = move-exception
            r16 = r11
            r11 = r13
            goto L_0x0100
        L_0x008a:
            com.fossil.u67 r11 = r1.c
            java.util.Locale r12 = java.util.Locale.US
            java.lang.String r15 = "Unable to resolve uri. InputStream null = %s, File null = %s"
            java.lang.Object[] r14 = new java.lang.Object[r14]
            if (r13 != 0) goto L_0x0097
            r18 = 1
            goto L_0x0099
        L_0x0097:
            r18 = 0
        L_0x0099:
            java.lang.Boolean r18 = java.lang.Boolean.valueOf(r18)
            r14[r8] = r18
            if (r0 != 0) goto L_0x00a3
            r0 = 1
            goto L_0x00a4
        L_0x00a3:
            r0 = 0
        L_0x00a4:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r17 = 1
            r14[r17] = r0
            java.lang.String r0 = java.lang.String.format(r12, r15, r14)
            r11.w(r5, r0)
            if (r13 == 0) goto L_0x012c
            r13.close()     // Catch:{ IOException -> 0x00ba }
            goto L_0x012c
        L_0x00ba:
            r0 = move-exception
            r10 = r0
            com.fossil.u67 r0 = r1.c
            r0.e(r5, r4, r10)
            goto L_0x012c
        L_0x00c3:
            r0 = move-exception
            r2 = r0
            r11 = r13
            goto L_0x00d0
        L_0x00c7:
            r0 = move-exception
            r11 = r13
            goto L_0x00d5
        L_0x00ca:
            r0 = move-exception
            r11 = r13
            goto L_0x00fe
        L_0x00cd:
            r0 = move-exception
            r2 = r0
            r11 = 0
        L_0x00d0:
            r16 = 0
            goto L_0x0132
        L_0x00d3:
            r0 = move-exception
            r11 = 0
        L_0x00d5:
            r16 = 0
        L_0x00d7:
            com.fossil.u67 r12 = r1.c     // Catch:{ all -> 0x0130 }
            java.util.Locale r13 = java.util.Locale.US     // Catch:{ all -> 0x0130 }
            java.lang.String r14 = "IO Error copying file, uri: %s"
            r15 = 1
            java.lang.Object[] r15 = new java.lang.Object[r15]     // Catch:{ all -> 0x0130 }
            r15[r8] = r10     // Catch:{ all -> 0x0130 }
            java.lang.String r10 = java.lang.String.format(r13, r14, r15)     // Catch:{ all -> 0x0130 }
            r12.e(r5, r10, r0)     // Catch:{ all -> 0x0130 }
            if (r11 == 0) goto L_0x00f6
            r11.close()     // Catch:{ IOException -> 0x00ef }
            goto L_0x00f6
        L_0x00ef:
            r0 = move-exception
            r10 = r0
            com.fossil.u67 r0 = r1.c
            r0.e(r5, r4, r10)
        L_0x00f6:
            if (r16 == 0) goto L_0x012c
            r16.close()
            goto L_0x012c
        L_0x00fc:
            r0 = move-exception
            r11 = 0
        L_0x00fe:
            r16 = 0
        L_0x0100:
            com.fossil.u67 r12 = r1.c
            java.util.Locale r13 = java.util.Locale.US
            java.lang.String r14 = "File not found error copying file, uri: %s"
            r15 = 1
            java.lang.Object[] r15 = new java.lang.Object[r15]
            r15[r8] = r10
            java.lang.String r10 = java.lang.String.format(r13, r14, r15)
            r12.e(r5, r10, r0)
            if (r11 == 0) goto L_0x011f
            r11.close()     // Catch:{ IOException -> 0x0118 }
            goto L_0x011f
        L_0x0118:
            r0 = move-exception
            r10 = r0
            com.fossil.u67 r0 = r1.c
            r0.e(r5, r4, r10)
        L_0x011f:
            if (r16 == 0) goto L_0x012c
            r16.close()
            goto L_0x012c
        L_0x0125:
            r0 = move-exception
            r10 = r0
            com.fossil.u67 r0 = r1.c
            r0.e(r5, r3, r10)
        L_0x012c:
            int r9 = r9 + 1
            goto L_0x0012
        L_0x0130:
            r0 = move-exception
            r2 = r0
        L_0x0132:
            if (r11 == 0) goto L_0x013f
            r11.close()     // Catch:{ IOException -> 0x0138 }
            goto L_0x013f
        L_0x0138:
            r0 = move-exception
            r6 = r0
            com.fossil.u67 r0 = r1.c
            r0.e(r5, r4, r6)
        L_0x013f:
            if (r16 == 0) goto L_0x014c
            r16.close()     // Catch:{ IOException -> 0x0145 }
            goto L_0x014c
        L_0x0145:
            r0 = move-exception
            r4 = r0
            com.fossil.u67 r0 = r1.c
            r0.e(r5, r3, r4)
        L_0x014c:
            throw r2
        L_0x014d:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.v67.doInBackground(android.net.Uri[]):java.util.List");
    }

    @DexIgnore
    /* renamed from: a */
    public void onPostExecute(List<BelvedereResult> list) {
        super.onPostExecute(list);
        BelvedereCallback<List<BelvedereResult>> belvedereCallback = this.a;
        if (belvedereCallback != null) {
            belvedereCallback.internalSuccess(list);
        }
    }
}
