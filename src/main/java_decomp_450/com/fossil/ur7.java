package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ur7 {
    @DexIgnore
    public static /* final */ byte[] a; // = uq7.a("0123456789abcdef");

    @DexIgnore
    public static final byte[] a() {
        return a;
    }

    @DexIgnore
    public static final String a(yq7 yq7, long j) {
        ee7.b(yq7, "$this$readUtf8Line");
        if (j > 0) {
            long j2 = j - 1;
            if (yq7.e(j2) == ((byte) 13)) {
                String g = yq7.g(j2);
                yq7.skip(2);
                return g;
            }
        }
        String g2 = yq7.g(j);
        yq7.skip(1);
        return g2;
    }
}
