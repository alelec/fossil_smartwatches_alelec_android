package com.fossil;

import android.content.Context;
import android.content.res.Resources;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xj3 {
    @DexIgnore
    public static String a(Context context, String str) {
        try {
            return new g72(context).a(str);
        } catch (Resources.NotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public static String a(String str, String[] strArr, String[] strArr2) {
        a72.a(strArr);
        a72.a(strArr2);
        int min = Math.min(strArr.length, strArr2.length);
        for (int i = 0; i < min; i++) {
            String str2 = strArr[i];
            if ((str == null && str2 == null) ? true : str == null ? false : str.equals(str2)) {
                return strArr2[i];
            }
        }
        return null;
    }
}
