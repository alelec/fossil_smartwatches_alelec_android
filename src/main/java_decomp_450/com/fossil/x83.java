package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x83 extends wm2 implements w83 {
    @DexIgnore
    public x83() {
        super("com.google.android.gms.maps.internal.IOnCircleClickListener");
    }

    @DexIgnore
    @Override // com.fossil.wm2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        a(cn2.a(parcel.readStrongBinder()));
        parcel2.writeNoException();
        return true;
    }
}
