package com.fossil;

import android.webkit.MimeTypeMap;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.pn7;
import com.fossil.pr;
import com.fossil.qn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rr<T> implements pr<T> {
    @DexIgnore
    public static /* final */ pn7 b;
    @DexIgnore
    public static /* final */ pn7 c;
    @DexIgnore
    public /* final */ qn7.a a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "coil.fetch.HttpFetcher", f = "HttpFetcher.kt", l = {106}, m = "fetch$suspendImpl")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ rr this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(rr rrVar, fb7 fb7) {
            super(fb7);
            this.this$0 = rrVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return rr.a(this.this$0, null, null, null, null, this);
        }
    }

    /*
    static {
        new a(null);
        pn7.a aVar = new pn7.a();
        aVar.b();
        aVar.c();
        b = aVar.a();
        pn7.a aVar2 = new pn7.a();
        aVar2.b();
        aVar2.d();
        c = aVar2.a();
    }
    */

    @DexIgnore
    public rr(qn7.a aVar) {
        ee7.b(aVar, "callFactory");
        this.a = aVar;
    }

    @DexIgnore
    @Override // com.fossil.pr
    public Object a(rq rqVar, T t, rt rtVar, ir irVar, fb7<? super or> fb7) {
        return a(this, rqVar, t, rtVar, irVar, fb7);
    }

    @DexIgnore
    @Override // com.fossil.pr
    public boolean a(T t) {
        ee7.b(t, "data");
        return pr.a.a(this, t);
    }

    @DexIgnore
    public abstract go7 c(T t);

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x012c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ java.lang.Object a(com.fossil.rr r9, com.fossil.rq r10, java.lang.Object r11, com.fossil.rt r12, com.fossil.ir r13, com.fossil.fb7 r14) {
        /*
            boolean r0 = r14 instanceof com.fossil.rr.b
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.fossil.rr$b r0 = (com.fossil.rr.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.rr$b r0 = new com.fossil.rr$b
            r0.<init>(r9, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0058
            if (r2 != r3) goto L_0x0050
            java.lang.Object r9 = r0.L$7
            com.fossil.qn7 r9 = (com.fossil.qn7) r9
            boolean r9 = r0.Z$1
            boolean r9 = r0.Z$0
            java.lang.Object r9 = r0.L$6
            com.fossil.lo7$a r9 = (com.fossil.lo7.a) r9
            java.lang.Object r9 = r0.L$5
            com.fossil.go7 r9 = (com.fossil.go7) r9
            java.lang.Object r10 = r0.L$4
            com.fossil.ir r10 = (com.fossil.ir) r10
            java.lang.Object r10 = r0.L$3
            com.fossil.rt r10 = (com.fossil.rt) r10
            java.lang.Object r10 = r0.L$2
            java.lang.Object r10 = r0.L$1
            com.fossil.rq r10 = (com.fossil.rq) r10
            java.lang.Object r10 = r0.L$0
            com.fossil.rr r10 = (com.fossil.rr) r10
            com.fossil.t87.a(r14)
            r8 = r14
            r14 = r9
            r9 = r10
            r10 = r8
            goto L_0x00f4
        L_0x0050:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0058:
            com.fossil.t87.a(r14)
            com.fossil.go7 r14 = r9.c(r11)
            com.fossil.lo7$a r2 = new com.fossil.lo7$a
            r2.<init>()
            r2.a(r14)
            com.fossil.fo7 r4 = r13.f()
            r2.a(r4)
            com.fossil.dt r4 = r13.g()
            boolean r4 = r4.getReadEnabled()
            com.fossil.dt r5 = r13.e()
            boolean r5 = r5.getReadEnabled()
            if (r4 != 0) goto L_0x0088
            if (r5 == 0) goto L_0x0088
            com.fossil.pn7 r6 = com.fossil.pn7.o
            r2.a(r6)
            goto L_0x00ab
        L_0x0088:
            if (r4 == 0) goto L_0x00a2
            if (r5 != 0) goto L_0x00a2
            com.fossil.dt r6 = r13.e()
            boolean r6 = r6.getWriteEnabled()
            if (r6 == 0) goto L_0x009c
            com.fossil.pn7 r6 = com.fossil.pn7.n
            r2.a(r6)
            goto L_0x00ab
        L_0x009c:
            com.fossil.pn7 r6 = com.fossil.rr.b
            r2.a(r6)
            goto L_0x00ab
        L_0x00a2:
            if (r4 != 0) goto L_0x00ab
            if (r5 != 0) goto L_0x00ab
            com.fossil.pn7 r6 = com.fossil.rr.c
            r2.a(r6)
        L_0x00ab:
            com.fossil.qn7$a r6 = r9.a
            com.fossil.lo7 r7 = r2.a()
            com.fossil.qn7 r6 = r6.a(r7)
            java.lang.String r7 = "callFactory.newCall(request.build())"
            com.fossil.ee7.a(r6, r7)
            r0.L$0 = r9
            r0.L$1 = r10
            r0.L$2 = r11
            r0.L$3 = r12
            r0.L$4 = r13
            r0.L$5 = r14
            r0.L$6 = r2
            r0.Z$0 = r4
            r0.Z$1 = r5
            r0.L$7 = r6
            r0.label = r3
            com.fossil.bi7 r10 = new com.fossil.bi7
            com.fossil.fb7 r11 = com.fossil.mb7.a(r0)
            r10.<init>(r11, r3)
            com.fossil.gu r11 = new com.fossil.gu
            r11.<init>(r6, r10)
            r6.a(r11)
            r10.a(r11)
            java.lang.Object r10 = r10.g()
            java.lang.Object r11 = com.fossil.nb7.a()
            if (r10 != r11) goto L_0x00f1
            com.fossil.vb7.c(r0)
        L_0x00f1:
            if (r10 != r1) goto L_0x00f4
            return r1
        L_0x00f4:
            okhttp3.Response r10 = (okhttp3.Response) r10
            boolean r11 = r10.l()
            if (r11 == 0) goto L_0x012c
            com.fossil.mo7 r11 = r10.a()
            if (r11 == 0) goto L_0x0120
            com.fossil.vr r12 = new com.fossil.vr
            com.fossil.ar7 r13 = r11.source()
            java.lang.String r0 = "body.source()"
            com.fossil.ee7.a(r13, r0)
            java.lang.String r9 = r9.a(r14, r11)
            okhttp3.Response r10 = r10.c()
            if (r10 == 0) goto L_0x011a
            com.fossil.br r10 = com.fossil.br.DISK
            goto L_0x011c
        L_0x011a:
            com.fossil.br r10 = com.fossil.br.NETWORK
        L_0x011c:
            r12.<init>(r13, r9, r10)
            return r12
        L_0x0120:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "Null response body!"
            java.lang.String r10 = r10.toString()
            r9.<init>(r10)
            throw r9
        L_0x012c:
            com.fossil.xs r9 = new com.fossil.xs
            r9.<init>(r10)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rr.a(com.fossil.rr, com.fossil.rq, java.lang.Object, com.fossil.rt, com.fossil.ir, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final String a(go7 go7, mo7 mo7) {
        ho7 contentType = mo7.contentType();
        String ho7 = contentType != null ? contentType.toString() : null;
        if (ho7 != null && !mh7.c(ho7, "text/plain", false, 2, null)) {
            return ho7;
        }
        MimeTypeMap singleton = MimeTypeMap.getSingleton();
        ee7.a((Object) singleton, "MimeTypeMap.getSingleton()");
        String a2 = iu.a(singleton, go7.toString());
        return a2 != null ? a2 : ho7;
    }
}
