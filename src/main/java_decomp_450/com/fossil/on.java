package com.fossil;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemjob.SystemJobService;
import com.fossil.qm;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class on implements ym {
    @DexIgnore
    public static /* final */ String e; // = im.a("SystemJobScheduler");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ JobScheduler b;
    @DexIgnore
    public /* final */ dn c;
    @DexIgnore
    public /* final */ nn d;

    @DexIgnore
    public on(Context context, dn dnVar) {
        this(context, dnVar, (JobScheduler) context.getSystemService("jobscheduler"), new nn(context));
    }

    @DexIgnore
    public static void b(Context context) {
        List<JobInfo> a2;
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
        if (jobScheduler != null && (a2 = a(context, jobScheduler)) != null && !a2.isEmpty()) {
            for (JobInfo jobInfo : a2) {
                if (a(jobInfo) == null) {
                    a(jobScheduler, jobInfo.getId());
                }
            }
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    @Override // com.fossil.ym
    public void a(zo... zoVarArr) {
        int i;
        List<Integer> a2;
        int i2;
        WorkDatabase f = this.c.f();
        ip ipVar = new ip(f);
        int length = zoVarArr.length;
        int i3 = 0;
        while (i3 < length) {
            zo zoVar = zoVarArr[i3];
            f.beginTransaction();
            try {
                zo e2 = f.f().e(zoVar.a);
                if (e2 == null) {
                    im a3 = im.a();
                    String str = e;
                    a3.e(str, "Skipping scheduling " + zoVar.a + " because it's no longer in the DB", new Throwable[0]);
                    f.setTransactionSuccessful();
                } else if (e2.b != qm.a.ENQUEUED) {
                    im a4 = im.a();
                    String str2 = e;
                    a4.e(str2, "Skipping scheduling " + zoVar.a + " because it is no longer enqueued", new Throwable[0]);
                    f.setTransactionSuccessful();
                } else {
                    qo a5 = f.c().a(zoVar.a);
                    if (a5 != null) {
                        i = a5.b;
                    } else {
                        i = ipVar.a(this.c.b().f(), this.c.b().d());
                    }
                    if (a5 == null) {
                        this.c.f().c().a(new qo(zoVar.a, i));
                    }
                    a(zoVar, i);
                    if (Build.VERSION.SDK_INT == 23 && (a2 = a(this.a, this.b, zoVar.a)) != null) {
                        int indexOf = a2.indexOf(Integer.valueOf(i));
                        if (indexOf >= 0) {
                            a2.remove(indexOf);
                        }
                        if (!a2.isEmpty()) {
                            i2 = a2.get(0).intValue();
                        } else {
                            i2 = ipVar.a(this.c.b().f(), this.c.b().d());
                        }
                        a(zoVar, i2);
                    }
                    f.setTransactionSuccessful();
                }
                f.endTransaction();
                i3++;
            } catch (Throwable th) {
                f.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ym
    public boolean a() {
        return true;
    }

    @DexIgnore
    public on(Context context, dn dnVar, JobScheduler jobScheduler, nn nnVar) {
        this.a = context;
        this.c = dnVar;
        this.b = jobScheduler;
        this.d = nnVar;
    }

    @DexIgnore
    public void a(zo zoVar, int i) {
        JobInfo a2 = this.d.a(zoVar, i);
        im.a().a(e, String.format("Scheduling work ID %s Job ID %s", zoVar.a, Integer.valueOf(i)), new Throwable[0]);
        try {
            this.b.schedule(a2);
        } catch (IllegalStateException e2) {
            List<JobInfo> a3 = a(this.a, this.b);
            String format = String.format(Locale.getDefault(), "JobScheduler 100 job limit exceeded.  We count %d WorkManager jobs in JobScheduler; we have %d tracked jobs in our DB; our Configuration limit is %d.", Integer.valueOf(a3 != null ? a3.size() : 0), Integer.valueOf(this.c.f().f().b().size()), Integer.valueOf(this.c.b().e()));
            im.a().b(e, format, new Throwable[0]);
            throw new IllegalStateException(format, e2);
        } catch (Throwable th) {
            im.a().b(e, String.format("Unable to schedule %s", zoVar), th);
        }
    }

    @DexIgnore
    @Override // com.fossil.ym
    public void a(String str) {
        List<Integer> a2 = a(this.a, this.b, str);
        if (a2 != null && !a2.isEmpty()) {
            for (Integer num : a2) {
                a(this.b, num.intValue());
            }
            this.c.f().c().b(str);
        }
    }

    @DexIgnore
    public static void a(JobScheduler jobScheduler, int i) {
        try {
            jobScheduler.cancel(i);
        } catch (Throwable th) {
            im.a().b(e, String.format(Locale.getDefault(), "Exception while trying to cancel job (%d)", Integer.valueOf(i)), th);
        }
    }

    @DexIgnore
    public static void a(Context context) {
        List<JobInfo> a2;
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
        if (jobScheduler != null && (a2 = a(context, jobScheduler)) != null && !a2.isEmpty()) {
            for (JobInfo jobInfo : a2) {
                a(jobScheduler, jobInfo.getId());
            }
        }
    }

    @DexIgnore
    public static List<JobInfo> a(Context context, JobScheduler jobScheduler) {
        List<JobInfo> list;
        try {
            list = jobScheduler.getAllPendingJobs();
        } catch (Throwable th) {
            im.a().b(e, "getAllPendingJobs() is not reliable on this device.", th);
            list = null;
        }
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        ComponentName componentName = new ComponentName(context, SystemJobService.class);
        for (JobInfo jobInfo : list) {
            if (componentName.equals(jobInfo.getService())) {
                arrayList.add(jobInfo);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static List<Integer> a(Context context, JobScheduler jobScheduler, String str) {
        List<JobInfo> a2 = a(context, jobScheduler);
        if (a2 == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(2);
        for (JobInfo jobInfo : a2) {
            if (str.equals(a(jobInfo))) {
                arrayList.add(Integer.valueOf(jobInfo.getId()));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static String a(JobInfo jobInfo) {
        PersistableBundle extras = jobInfo.getExtras();
        if (extras == null) {
            return null;
        }
        try {
            if (extras.containsKey("EXTRA_WORK_SPEC_ID")) {
                return extras.getString("EXTRA_WORK_SPEC_ID");
            }
            return null;
        } catch (NullPointerException unused) {
            return null;
        }
    }
}
