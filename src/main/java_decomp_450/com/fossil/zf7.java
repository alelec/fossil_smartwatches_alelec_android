package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zf7<R> extends sf7<R> {

    @DexIgnore
    public interface a<R> extends Object<R> {
    }

    @DexIgnore
    boolean isConst();

    @DexIgnore
    boolean isLateinit();
}
