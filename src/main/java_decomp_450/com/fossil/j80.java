package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j80 extends n80 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ int f; // = 65535;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<j80> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final j80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 8) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new j80(yz0.b(order.getShort(0)), yz0.b(order.getShort(2)), yz0.b(order.getShort(4)), yz0.b(order.getShort(6)));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", require: 8"));
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public j80 createFromParcel(Parcel parcel) {
            return new j80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public j80[] newArray(int i) {
            return new j80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public j80 m28createFromParcel(Parcel parcel) {
            return new j80(parcel, null);
        }
    }

    /*
    static {
        ve7 ve7 = ve7.a;
    }
    */

    @DexIgnore
    public j80(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        super(o80.DAILY_SLEEP);
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        e();
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.b).putShort((short) this.c).putShort((short) this.d).putShort((short) this.e).array();
        ee7.a((Object) array, "ByteBuffer.allocate(Dail\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        int i = f;
        int i2 = this.b;
        boolean z = true;
        if (i2 >= 0 && i >= i2) {
            int i3 = f;
            int i4 = this.c;
            if (i4 >= 0 && i3 >= i4) {
                int i5 = f;
                int i6 = this.d;
                if (i6 >= 0 && i5 >= i6) {
                    int i7 = f;
                    int i8 = this.e;
                    if (i8 < 0 || i7 < i8) {
                        z = false;
                    }
                    if (!z) {
                        StringBuilder b2 = yh0.b("deepSleepInMinute (");
                        b2.append(this.e);
                        b2.append(") is out of range ");
                        b2.append("[0, ");
                        throw new IllegalArgumentException(yh0.a(b2, f, "]."));
                    }
                    return;
                }
                StringBuilder b3 = yh0.b("lightSleepInMinute (");
                b3.append(this.d);
                b3.append(") is out of range ");
                b3.append("[0, ");
                throw new IllegalArgumentException(yh0.a(b3, f, "]."));
            }
            StringBuilder b4 = yh0.b("awakeInMinute (");
            b4.append(this.c);
            b4.append(") is out of range ");
            b4.append("[0, ");
            throw new IllegalArgumentException(yh0.a(b4, f, "]."));
        }
        StringBuilder b5 = yh0.b("totalSleepInMinute (");
        b5.append(this.b);
        b5.append(") is out of range ");
        b5.append("[0, ");
        throw new IllegalArgumentException(yh0.a(b5, f, "]."));
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(j80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            j80 j80 = (j80) obj;
            return this.b == j80.b && this.c == j80.c && this.d == j80.d && this.e == j80.e;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailySleepConfig");
    }

    @DexIgnore
    public final int getAwakeInMinute() {
        return this.c;
    }

    @DexIgnore
    public final int getDeepSleepInMinute() {
        return this.e;
    }

    @DexIgnore
    public final int getLightSleepInMinute() {
        return this.d;
    }

    @DexIgnore
    public final int getTotalSleepInMinute() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        return (((((this.b * 31) + this.c) * 31) + this.d) * 31) + this.e;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(yz0.a(yz0.a(yz0.a(jSONObject, r51.N1, Integer.valueOf(this.b)), r51.O1, Integer.valueOf(this.c)), r51.P1, Integer.valueOf(this.d)), r51.Q1, Integer.valueOf(this.e));
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ j80(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        e();
    }
}
