package com.fossil;

import com.fossil.ik7;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bi7<T> extends oj7<T> implements ai7<T>, sb7 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater f; // = AtomicIntegerFieldUpdater.newUpdater(bi7.class, "_decision");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater g; // = AtomicReferenceFieldUpdater.newUpdater(bi7.class, Object.class, "_state");
    @DexIgnore
    public volatile int _decision; // = 0;
    @DexIgnore
    public volatile Object _parentHandle; // = null;
    @DexIgnore
    public volatile Object _state; // = sh7.a;
    @DexIgnore
    public /* final */ ib7 d;
    @DexIgnore
    public /* final */ fb7<T> e;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.fb7<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public bi7(fb7<? super T> fb7, int i) {
        super(i);
        this.e = fb7;
        this.d = fb7.getContext();
    }

    @DexIgnore
    @Override // com.fossil.oj7
    public final fb7<T> a() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.oj7
    public Object b() {
        return h();
    }

    @DexIgnore
    public final boolean c() {
        Throwable a;
        boolean i = i();
        if (((oj7) this).c != 0) {
            return i;
        }
        fb7<T> fb7 = this.e;
        if (!(fb7 instanceof lj7)) {
            fb7 = null;
        }
        lj7 lj7 = (lj7) fb7;
        if (lj7 == null || (a = lj7.a((ai7<?>) this)) == null) {
            return i;
        }
        if (!i) {
            a(a);
        }
        return true;
    }

    @DexIgnore
    public final void d(Object obj) {
        throw new IllegalStateException(("Already resumed, but proposed with update " + obj).toString());
    }

    @DexIgnore
    public final void e() {
        if (!j()) {
            d();
        }
    }

    @DexIgnore
    public final rj7 f() {
        return (rj7) this._parentHandle;
    }

    @DexIgnore
    public final Object g() {
        ik7 ik7;
        m();
        if (o()) {
            return nb7.a();
        }
        Object h = h();
        if (h instanceof li7) {
            Throwable th = ((li7) h).a;
            if (dj7.d()) {
                throw km7.b(th, this);
            }
            throw th;
        } else if (((oj7) this).c != 1 || (ik7 = (ik7) getContext().get(ik7.o)) == null || ik7.isActive()) {
            return c(h);
        } else {
            CancellationException b = ik7.b();
            a(h, (Throwable) b);
            if (dj7.d()) {
                throw km7.b(b, this);
            }
            throw b;
        }
    }

    @DexIgnore
    @Override // com.fossil.sb7
    public sb7 getCallerFrame() {
        fb7<T> fb7 = this.e;
        if (!(fb7 instanceof sb7)) {
            fb7 = null;
        }
        return (sb7) fb7;
    }

    @DexIgnore
    @Override // com.fossil.fb7
    public ib7 getContext() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.sb7
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public final Object h() {
        return this._state;
    }

    @DexIgnore
    public boolean i() {
        return !(h() instanceof wk7);
    }

    @DexIgnore
    @Override // com.fossil.ai7
    public boolean isActive() {
        return h() instanceof wk7;
    }

    @DexIgnore
    public final boolean j() {
        fb7<T> fb7 = this.e;
        return (fb7 instanceof lj7) && ((lj7) fb7).e();
    }

    @DexIgnore
    public String k() {
        return "CancellableContinuation";
    }

    @DexIgnore
    public final boolean l() {
        if (dj7.a()) {
            if (!(f() != vk7.a)) {
                throw new AssertionError();
            }
        }
        Object obj = this._state;
        if (dj7.a() && !(!(obj instanceof wk7))) {
            throw new AssertionError();
        } else if (obj instanceof ni7) {
            d();
            return false;
        } else {
            this._decision = 0;
            this._state = sh7.a;
            return true;
        }
    }

    @DexIgnore
    public final void m() {
        ik7 ik7;
        if (!c() && f() == null && (ik7 = (ik7) this.e.getContext().get(ik7.o)) != null) {
            ik7.start();
            rj7 a = ik7.a.a(ik7, true, false, new fi7(ik7, this), 2, null);
            a(a);
            if (i() && !j()) {
                a.dispose();
                a((rj7) vk7.a);
            }
        }
    }

    @DexIgnore
    public final boolean n() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 1) {
                    return false;
                }
                throw new IllegalStateException("Already resumed".toString());
            }
        } while (!f.compareAndSet(this, 0, 2));
        return true;
    }

    @DexIgnore
    public final boolean o() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 2) {
                    return false;
                }
                throw new IllegalStateException("Already suspended".toString());
            }
        } while (!f.compareAndSet(this, 0, 1));
        return true;
    }

    @DexIgnore
    @Override // com.fossil.fb7
    public void resumeWith(Object obj) {
        a(mi7.a(obj, (ai7<?>) this), ((oj7) this).c);
    }

    @DexIgnore
    public String toString() {
        return k() + '(' + ej7.a((fb7<?>) this.e) + "){" + h() + "}@" + ej7.b(this);
    }

    @DexIgnore
    public final void a(rj7 rj7) {
        this._parentHandle = rj7;
    }

    @DexIgnore
    public final boolean b(Throwable th) {
        if (((oj7) this).c != 0) {
            return false;
        }
        fb7<T> fb7 = this.e;
        if (!(fb7 instanceof lj7)) {
            fb7 = null;
        }
        lj7 lj7 = (lj7) fb7;
        if (lj7 != null) {
            return lj7.a(th);
        }
        return false;
    }

    @DexIgnore
    public final void d() {
        rj7 f2 = f();
        if (f2 != null) {
            f2.dispose();
        }
        a((rj7) vk7.a);
    }

    @DexIgnore
    @Override // com.fossil.oj7
    public void a(Object obj, Throwable th) {
        if (obj instanceof oi7) {
            try {
                ((oi7) obj).b.invoke(th);
            } catch (Throwable th2) {
                ib7 context = getContext();
                vi7.a(context, new qi7("Exception in cancellation handler for " + this, th2));
            }
        }
    }

    @DexIgnore
    public final yh7 b(gd7<? super Throwable, i97> gd7) {
        return gd7 instanceof yh7 ? (yh7) gd7 : new fk7(gd7);
    }

    @DexIgnore
    @Override // com.fossil.ai7
    public void b(Object obj) {
        if (dj7.a()) {
            if (!(obj == ci7.a)) {
                throw new AssertionError();
            }
        }
        a(((oj7) this).c);
    }

    @DexIgnore
    public final void c(Throwable th) {
        if (!b(th)) {
            a(th);
            e();
        }
    }

    @DexIgnore
    public Throwable a(ik7 ik7) {
        return ik7.b();
    }

    @DexIgnore
    public final void a(gd7<? super Throwable, i97> gd7, Object obj) {
        throw new IllegalStateException(("It's prohibited to register multiple handlers, tried to register " + gd7 + ", already has " + obj).toString());
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.oj7
    public <T> T c(Object obj) {
        return obj instanceof ni7 ? (T) ((ni7) obj).b : obj instanceof oi7 ? (T) ((oi7) obj).a : obj;
    }

    @DexIgnore
    public final void a(int i) {
        if (!n()) {
            pj7.a(this, i);
        }
    }

    @DexIgnore
    @Override // com.fossil.ai7
    public void a(ti7 ti7, T t) {
        fb7<T> fb7 = this.e;
        ti7 ti72 = null;
        if (!(fb7 instanceof lj7)) {
            fb7 = null;
        }
        lj7 lj7 = (lj7) fb7;
        if (lj7 != null) {
            ti72 = lj7.g;
        }
        a(t, ti72 == ti7 ? 2 : ((oj7) this).c);
    }

    @DexIgnore
    public boolean a(Throwable th) {
        Object obj;
        boolean z;
        do {
            obj = this._state;
            if (!(obj instanceof wk7)) {
                return false;
            }
            z = obj instanceof yh7;
        } while (!g.compareAndSet(this, obj, new ei7(this, th, z)));
        if (z) {
            try {
                ((yh7) obj).a(th);
            } catch (Throwable th2) {
                ib7 context = getContext();
                vi7.a(context, new qi7("Exception in cancellation handler for " + this, th2));
            }
        }
        e();
        a(0);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ai7
    public void a(gd7<? super Throwable, i97> gd7) {
        Object obj;
        Throwable th = null;
        yh7 yh7 = null;
        do {
            obj = this._state;
            if (obj instanceof sh7) {
                if (yh7 == null) {
                    yh7 = b(gd7);
                }
            } else if (obj instanceof yh7) {
                a(gd7, obj);
                throw null;
            } else if (!(obj instanceof ei7)) {
                return;
            } else {
                if (((ei7) obj).b()) {
                    try {
                        if (!(obj instanceof li7)) {
                            obj = null;
                        }
                        li7 li7 = (li7) obj;
                        if (li7 != null) {
                            th = li7.a;
                        }
                        gd7.invoke(th);
                        return;
                    } catch (Throwable th2) {
                        vi7.a(getContext(), new qi7("Exception in cancellation handler for " + this, th2));
                        return;
                    }
                } else {
                    a(gd7, obj);
                    throw null;
                }
            }
        } while (!g.compareAndSet(this, obj, yh7));
    }

    @DexIgnore
    public final ei7 a(Object obj, int i) {
        Object obj2;
        do {
            obj2 = this._state;
            if (!(obj2 instanceof wk7)) {
                if (obj2 instanceof ei7) {
                    ei7 ei7 = (ei7) obj2;
                    if (ei7.c()) {
                        return ei7;
                    }
                }
                d(obj);
                throw null;
            }
        } while (!g.compareAndSet(this, obj2, obj));
        e();
        a(i);
        return null;
    }

    @DexIgnore
    @Override // com.fossil.ai7
    public Object a(T t, Object obj) {
        Object obj2;
        ni7 ni7;
        do {
            obj2 = this._state;
            if (obj2 instanceof wk7) {
                if (obj == null) {
                    ni7 = t;
                } else {
                    ni7 = new ni7(obj, t);
                }
            } else if (!(obj2 instanceof ni7)) {
                return null;
            } else {
                ni7 ni72 = (ni7) obj2;
                if (ni72.a != obj) {
                    return null;
                }
                if (dj7.a()) {
                    if (!(ni72.b == t)) {
                        throw new AssertionError();
                    }
                }
                return ci7.a;
            }
        } while (!g.compareAndSet(this, obj2, ni7));
        e();
        return ci7.a;
    }
}
