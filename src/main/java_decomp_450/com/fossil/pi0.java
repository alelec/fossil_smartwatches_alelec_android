package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pi0 {
    @DexIgnore
    public /* synthetic */ pi0(zd7 zd7) {
    }

    @DexIgnore
    public final byte[] a() {
        return fo0.c;
    }

    @DexIgnore
    public final byte[] b() {
        return fo0.b;
    }

    @DexIgnore
    public final byte[] c() {
        return fo0.a;
    }

    @DexIgnore
    public final jm0 a(UUID uuid) {
        if (ee7.a(uuid, b21.x.a())) {
            return jm0.CLIENT_CHARACTERISTIC_CONFIGURATION;
        }
        return jm0.UNKNOWN;
    }
}
