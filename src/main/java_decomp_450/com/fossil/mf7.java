package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mf7 implements Iterable<Long>, ye7 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public mf7(long j, long j2, long j3) {
        if (j3 == 0) {
            throw new IllegalArgumentException("Step must be non-zero.");
        } else if (j3 != Long.MIN_VALUE) {
            this.a = j;
            this.b = cc7.b(j, j2, j3);
            this.c = j3;
        } else {
            throw new IllegalArgumentException("Step must be greater than Long.MIN_VALUE to avoid overflow on negation.");
        }
    }

    @DexIgnore
    public final long getFirst() {
        return this.a;
    }

    @DexIgnore
    public final long getLast() {
        return this.b;
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.ka7' to match base method */
    @Override // java.lang.Iterable
    public Iterator<Long> iterator() {
        return new nf7(this.a, this.b, this.c);
    }
}
