package com.fossil;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bx5 {
    @DexIgnore
    public /* final */ zw5 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ LoaderManager c;

    @DexIgnore
    public bx5(zw5 zw5, int i, LoaderManager loaderManager) {
        ee7.b(zw5, "mView");
        ee7.b(loaderManager, "mLoaderManager");
        this.a = zw5;
        this.b = i;
        this.c = loaderManager;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final LoaderManager b() {
        return this.c;
    }

    @DexIgnore
    public final zw5 c() {
        return this.a;
    }
}
