package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jo4 {
    @DexIgnore
    @te4("challenge")
    public mn4 a;
    @DexIgnore
    @te4("category")
    public String b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final mn4 b() {
        return this.a;
    }

    @DexIgnore
    public final boolean c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof jo4)) {
            return false;
        }
        jo4 jo4 = (jo4) obj;
        return ee7.a(this.a, jo4.a) && ee7.a(this.b, jo4.b) && this.c == jo4.c;
    }

    @DexIgnore
    public int hashCode() {
        mn4 mn4 = this.a;
        int i = 0;
        int hashCode = (mn4 != null ? mn4.hashCode() : 0) * 31;
        String str = this.b;
        if (str != null) {
            i = str.hashCode();
        }
        int i2 = (hashCode + i) * 31;
        boolean z = this.c;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return i2 + i3;
    }

    @DexIgnore
    public String toString() {
        return "Recommended(challenge=" + this.a + ", category=" + this.b + ", isNew=" + this.c + ")";
    }

    @DexIgnore
    public final void a(boolean z) {
        this.c = z;
    }
}
