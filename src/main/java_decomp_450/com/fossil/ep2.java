package com.fossil;

import com.fossil.bw2;
import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ep2 extends bw2<ep2, b> implements lx2 {
    @DexIgnore
    public static /* final */ ep2 zzh;
    @DexIgnore
    public static volatile wx2<ep2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public boolean zzf;
    @DexIgnore
    public jw2<String> zzg; // = bw2.o();

    @DexIgnore
    public enum a implements dw2 {
        UNKNOWN_MATCH_TYPE(0),
        REGEXP(1),
        BEGINS_WITH(2),
        ENDS_WITH(3),
        PARTIAL(4),
        EXACT(5),
        IN_LIST(6);
        
        @DexIgnore
        public /* final */ int zzi;

        /*
        static {
            new ip2();
        }
        */

        @DexIgnore
        public a(int i) {
            this.zzi = i;
        }

        @DexIgnore
        public static fw2 zzb() {
            return hp2.a;
        }

        @DexIgnore
        public final String toString() {
            return SimpleComparison.LESS_THAN_OPERATION + a.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzi + " name=" + name() + '>';
        }

        @DexIgnore
        @Override // com.fossil.dw2
        public final int zza() {
            return this.zzi;
        }

        @DexIgnore
        public static a zza(int i) {
            switch (i) {
                case 0:
                    return UNKNOWN_MATCH_TYPE;
                case 1:
                    return REGEXP;
                case 2:
                    return BEGINS_WITH;
                case 3:
                    return ENDS_WITH;
                case 4:
                    return PARTIAL;
                case 5:
                    return EXACT;
                case 6:
                    return IN_LIST;
                default:
                    return null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends bw2.a<ep2, b> implements lx2 {
        @DexIgnore
        public b() {
            super(ep2.zzh);
        }

        @DexIgnore
        public /* synthetic */ b(yo2 yo2) {
            this();
        }
    }

    /*
    static {
        ep2 ep2 = new ep2();
        zzh = ep2;
        bw2.a(ep2.class, ep2);
    }
    */

    @DexIgnore
    public static ep2 w() {
        return zzh;
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (yo2.a[i - 1]) {
            case 1:
                return new ep2();
            case 2:
                return new b(null);
            case 3:
                return bw2.a(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001\u100c\u0000\u0002\u1008\u0001\u0003\u1007\u0002\u0004\u001a", new Object[]{"zzc", "zzd", a.zzb(), "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                wx2<ep2> wx2 = zzi;
                if (wx2 == null) {
                    synchronized (ep2.class) {
                        wx2 = zzi;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzh);
                            zzi = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final a p() {
        a zza = a.zza(this.zzd);
        return zza == null ? a.UNKNOWN_MATCH_TYPE : zza;
    }

    @DexIgnore
    public final boolean q() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final String r() {
        return this.zze;
    }

    @DexIgnore
    public final boolean s() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final boolean t() {
        return this.zzf;
    }

    @DexIgnore
    public final List<String> u() {
        return this.zzg;
    }

    @DexIgnore
    public final int v() {
        return this.zzg.size();
    }

    @DexIgnore
    public final boolean zza() {
        return (this.zzc & 1) != 0;
    }
}
