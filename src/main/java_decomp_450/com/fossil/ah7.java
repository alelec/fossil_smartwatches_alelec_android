package com.fossil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ah7 implements Serializable {
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public Set<? extends dh7> _options;
    @DexIgnore
    public /* final */ Pattern nativePattern;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(int i) {
            return (i & 2) != 0 ? i | 64 : i;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Serializable {
        @DexIgnore
        public static /* final */ a Companion; // = new a(null);
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ int flags;
        @DexIgnore
        public /* final */ String pattern;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(zd7 zd7) {
                this();
            }
        }

        @DexIgnore
        public b(String str, int i) {
            ee7.b(str, "pattern");
            this.pattern = str;
            this.flags = i;
        }

        @DexIgnore
        private final Object readResolve() {
            Pattern compile = Pattern.compile(this.pattern, this.flags);
            ee7.a((Object) compile, "Pattern.compile(pattern, flags)");
            return new ah7(compile);
        }

        @DexIgnore
        public final int getFlags() {
            return this.flags;
        }

        @DexIgnore
        public final String getPattern() {
            return this.pattern;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fe7 implements vc7<yg7> {
        @DexIgnore
        public /* final */ /* synthetic */ CharSequence $input;
        @DexIgnore
        public /* final */ /* synthetic */ int $startIndex;
        @DexIgnore
        public /* final */ /* synthetic */ ah7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ah7 ah7, CharSequence charSequence, int i) {
            super(0);
            this.this$0 = ah7;
            this.$input = charSequence;
            this.$startIndex = i;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final yg7 invoke() {
            return this.this$0.find(this.$input, this.$startIndex);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final /* synthetic */ class d extends ce7 implements gd7<yg7, yg7> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(1);
        }

        @DexIgnore
        @Override // com.fossil.sf7, com.fossil.vd7
        public final String getName() {
            return "next";
        }

        @DexIgnore
        @Override // com.fossil.vd7
        public final uf7 getOwner() {
            return te7.a(yg7.class);
        }

        @DexIgnore
        @Override // com.fossil.vd7
        public final String getSignature() {
            return "next()Lkotlin/text/MatchResult;";
        }

        @DexIgnore
        public final yg7 invoke(yg7 yg7) {
            ee7.b(yg7, "p1");
            return yg7.next();
        }
    }

    @DexIgnore
    public ah7(Pattern pattern) {
        ee7.b(pattern, "nativePattern");
        this.nativePattern = pattern;
    }

    @DexIgnore
    public static /* synthetic */ yg7 find$default(ah7 ah7, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return ah7.find(charSequence, i);
    }

    @DexIgnore
    public static /* synthetic */ hg7 findAll$default(ah7 ah7, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return ah7.findAll(charSequence, i);
    }

    @DexIgnore
    public static /* synthetic */ List split$default(ah7 ah7, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return ah7.split(charSequence, i);
    }

    @DexIgnore
    private final Object writeReplace() {
        String pattern = this.nativePattern.pattern();
        ee7.a((Object) pattern, "nativePattern.pattern()");
        return new b(pattern, this.nativePattern.flags());
    }

    @DexIgnore
    public final boolean containsMatchIn(CharSequence charSequence) {
        ee7.b(charSequence, "input");
        return this.nativePattern.matcher(charSequence).find();
    }

    @DexIgnore
    public final yg7 find(CharSequence charSequence, int i) {
        ee7.b(charSequence, "input");
        Matcher matcher = this.nativePattern.matcher(charSequence);
        ee7.a((Object) matcher, "nativePattern.matcher(input)");
        return ch7.b(matcher, i, charSequence);
    }

    @DexIgnore
    public final hg7<yg7> findAll(CharSequence charSequence, int i) {
        ee7.b(charSequence, "input");
        return mg7.a(new c(this, charSequence, i), d.INSTANCE);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.Set<? extends com.fossil.dh7>, java.util.Set<com.fossil.dh7> */
    public final Set<dh7> getOptions() {
        Set set = this._options;
        if (set != null) {
            return set;
        }
        int flags = this.nativePattern.flags();
        EnumSet allOf = EnumSet.allOf(dh7.class);
        ba7.a(allOf, new bh7(flags));
        Set<dh7> unmodifiableSet = Collections.unmodifiableSet(allOf);
        ee7.a((Object) unmodifiableSet, "Collections.unmodifiable\u2026mask == it.value }\n    })");
        this._options = unmodifiableSet;
        return unmodifiableSet;
    }

    @DexIgnore
    public final String getPattern() {
        String pattern = this.nativePattern.pattern();
        ee7.a((Object) pattern, "nativePattern.pattern()");
        return pattern;
    }

    @DexIgnore
    public final yg7 matchEntire(CharSequence charSequence) {
        ee7.b(charSequence, "input");
        Matcher matcher = this.nativePattern.matcher(charSequence);
        ee7.a((Object) matcher, "nativePattern.matcher(input)");
        return ch7.b(matcher, charSequence);
    }

    @DexIgnore
    public final boolean matches(CharSequence charSequence) {
        ee7.b(charSequence, "input");
        return this.nativePattern.matcher(charSequence).matches();
    }

    @DexIgnore
    public final String replace(CharSequence charSequence, String str) {
        ee7.b(charSequence, "input");
        ee7.b(str, "replacement");
        String replaceAll = this.nativePattern.matcher(charSequence).replaceAll(str);
        ee7.a((Object) replaceAll, "nativePattern.matcher(in\u2026).replaceAll(replacement)");
        return replaceAll;
    }

    @DexIgnore
    public final String replaceFirst(CharSequence charSequence, String str) {
        ee7.b(charSequence, "input");
        ee7.b(str, "replacement");
        String replaceFirst = this.nativePattern.matcher(charSequence).replaceFirst(str);
        ee7.a((Object) replaceFirst, "nativePattern.matcher(in\u2026replaceFirst(replacement)");
        return replaceFirst;
    }

    @DexIgnore
    public final List<String> split(CharSequence charSequence, int i) {
        ee7.b(charSequence, "input");
        int i2 = 0;
        if (i >= 0) {
            Matcher matcher = this.nativePattern.matcher(charSequence);
            if (!matcher.find() || i == 1) {
                return v97.a(charSequence.toString());
            }
            int i3 = 10;
            if (i > 0) {
                i3 = qf7.b(i, 10);
            }
            ArrayList arrayList = new ArrayList(i3);
            int i4 = i - 1;
            do {
                arrayList.add(charSequence.subSequence(i2, matcher.start()).toString());
                i2 = matcher.end();
                if (i4 >= 0 && arrayList.size() == i4) {
                    break;
                }
            } while (matcher.find());
            arrayList.add(charSequence.subSequence(i2, charSequence.length()).toString());
            return arrayList;
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i + '.').toString());
    }

    @DexIgnore
    public final Pattern toPattern() {
        return this.nativePattern;
    }

    @DexIgnore
    public String toString() {
        String pattern = this.nativePattern.toString();
        ee7.a((Object) pattern, "nativePattern.toString()");
        return pattern;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ah7(java.lang.String r2) {
        /*
            r1 = this;
            java.lang.String r0 = "pattern"
            com.fossil.ee7.b(r2, r0)
            java.util.regex.Pattern r2 = java.util.regex.Pattern.compile(r2)
            java.lang.String r0 = "Pattern.compile(pattern)"
            com.fossil.ee7.a(r2, r0)
            r1.<init>(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ah7.<init>(java.lang.String):void");
    }

    @DexIgnore
    public final String replace(CharSequence charSequence, gd7<? super yg7, ? extends CharSequence> gd7) {
        ee7.b(charSequence, "input");
        ee7.b(gd7, "transform");
        int i = 0;
        yg7 find$default = find$default(this, charSequence, 0, 2, null);
        if (find$default == null) {
            return charSequence.toString();
        }
        int length = charSequence.length();
        StringBuilder sb = new StringBuilder(length);
        while (find$default != null) {
            sb.append(charSequence, i, find$default.a().c().intValue());
            sb.append((CharSequence) gd7.invoke(find$default));
            i = find$default.a().b().intValue() + 1;
            find$default = find$default.next();
            if (i < length) {
                if (find$default == null) {
                }
            }
            if (i < length) {
                sb.append(charSequence, i, length);
            }
            String sb2 = sb.toString();
            ee7.a((Object) sb2, "sb.toString()");
            return sb2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ah7(java.lang.String r2, com.fossil.dh7 r3) {
        /*
            r1 = this;
            java.lang.String r0 = "pattern"
            com.fossil.ee7.b(r2, r0)
            java.lang.String r0 = "option"
            com.fossil.ee7.b(r3, r0)
            com.fossil.ah7$a r0 = com.fossil.ah7.Companion
            int r3 = r3.getValue()
            int r3 = r0.a(r3)
            java.util.regex.Pattern r2 = java.util.regex.Pattern.compile(r2, r3)
            java.lang.String r3 = "Pattern.compile(pattern,\u2026nicodeCase(option.value))"
            com.fossil.ee7.a(r2, r3)
            r1.<init>(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ah7.<init>(java.lang.String, com.fossil.dh7):void");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ah7(java.lang.String r2, java.util.Set<? extends com.fossil.dh7> r3) {
        /*
            r1 = this;
            java.lang.String r0 = "pattern"
            com.fossil.ee7.b(r2, r0)
            java.lang.String r0 = "options"
            com.fossil.ee7.b(r3, r0)
            com.fossil.ah7$a r0 = com.fossil.ah7.Companion
            int r3 = com.fossil.ch7.b(r3)
            int r3 = r0.a(r3)
            java.util.regex.Pattern r2 = java.util.regex.Pattern.compile(r2, r3)
            java.lang.String r3 = "Pattern.compile(pattern,\u2026odeCase(options.toInt()))"
            com.fossil.ee7.a(r2, r3)
            r1.<init>(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ah7.<init>(java.lang.String, java.util.Set):void");
    }
}
