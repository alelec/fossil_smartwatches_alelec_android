package com.fossil;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Debug;
import android.os.StatFs;
import android.provider.Settings;
import android.text.TextUtils;
import com.facebook.LegacyTokenHelper;
import com.facebook.internal.Utility;
import com.misfit.frameworks.common.constants.Constants;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t34 {
    @DexIgnore
    public static /* final */ char[] a; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    @DexIgnore
    public static long b; // = -1;
    @DexIgnore
    public static /* final */ Comparator<File> c; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Comparator<File> {
        @DexIgnore
        /* renamed from: a */
        public int compare(File file, File file2) {
            return (int) (file.lastModified() - file2.lastModified());
        }
    }

    @DexIgnore
    public enum b {
        X86_32,
        X86_64,
        ARM_UNKNOWN,
        PPC,
        PPC64,
        ARMV6,
        ARMV7,
        UNKNOWN,
        ARMV7S,
        ARM64;
        
        @DexIgnore
        public static /* final */ Map<String, b> a;

        /*
        static {
            HashMap hashMap = new HashMap(4);
            a = hashMap;
            hashMap.put("armeabi-v7a", ARMV7);
            a.put("armeabi", ARMV6);
            a.put("arm64-v8a", ARM64);
            a.put("x86", X86_32);
        }
        */

        @DexIgnore
        public static b getValue() {
            String str = Build.CPU_ABI;
            if (TextUtils.isEmpty(str)) {
                z24.a().a("Architecture#getValue()::Build.CPU_ABI returned null or empty");
                return UNKNOWN;
            }
            b bVar = a.get(str.toLowerCase(Locale.US));
            return bVar == null ? UNKNOWN : bVar;
        }
    }

    @DexIgnore
    public static String a(File file, String str) {
        BufferedReader bufferedReader;
        String str2 = null;
        str2 = null;
        BufferedReader bufferedReader2 = null;
        str2 = null;
        if (file.exists()) {
            try {
                bufferedReader = new BufferedReader(new FileReader(file), 1024);
                while (true) {
                    try {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        String[] split = Pattern.compile("\\s*:\\s*").split(readLine, 2);
                        if (split.length > 1 && split[0].equals(str)) {
                            str2 = split[1];
                            break;
                        }
                    } catch (Exception e) {
                        e = e;
                        try {
                            z24.a().b("Error parsing " + file, e);
                            a(bufferedReader, "Failed to close system file reader.");
                            return str2;
                        } catch (Throwable th) {
                            th = th;
                            bufferedReader2 = bufferedReader;
                        }
                    }
                }
            } catch (Exception e2) {
                e = e2;
                bufferedReader = null;
                z24.a().b("Error parsing " + file, e);
                a(bufferedReader, "Failed to close system file reader.");
                return str2;
            } catch (Throwable th2) {
                th = th2;
                a(bufferedReader2, "Failed to close system file reader.");
                throw th;
            }
            a(bufferedReader, "Failed to close system file reader.");
        }
        return str2;
    }

    @DexIgnore
    public static synchronized long b() {
        long j;
        synchronized (t34.class) {
            if (b == -1) {
                long j2 = 0;
                String a2 = a(new File("/proc/meminfo"), "MemTotal");
                if (!TextUtils.isEmpty(a2)) {
                    String upperCase = a2.toUpperCase(Locale.US);
                    try {
                        if (upperCase.endsWith("KB")) {
                            j2 = a(upperCase, "KB", 1024);
                        } else if (upperCase.endsWith("MB")) {
                            j2 = a(upperCase, "MB", 1048576);
                        } else if (upperCase.endsWith("GB")) {
                            j2 = a(upperCase, "GB", 1073741824);
                        } else {
                            z24 a3 = z24.a();
                            a3.a("Unexpected meminfo format while computing RAM: " + upperCase);
                        }
                    } catch (NumberFormatException e) {
                        z24 a4 = z24.a();
                        a4.b("Unexpected meminfo format while computing RAM: " + upperCase, e);
                    }
                }
                b = j2;
            }
            j = b;
        }
        return j;
    }

    @DexIgnore
    public static String c(String str) {
        return a(str, Utility.HASH_ALGORITHM_SHA1);
    }

    @DexIgnore
    public static SharedPreferences d(Context context) {
        return context.getSharedPreferences("com.crashlytics.prefs", 0);
    }

    @DexIgnore
    public static String e(Context context) {
        int a2 = a(context, "com.google.firebase.crashlytics.mapping_file_id", LegacyTokenHelper.TYPE_STRING);
        if (a2 == 0) {
            a2 = a(context, "com.crashlytics.android.build_id", LegacyTokenHelper.TYPE_STRING);
        }
        if (a2 != 0) {
            return context.getResources().getString(a2);
        }
        return null;
    }

    @DexIgnore
    public static boolean f(Context context) {
        if (!j(context) && ((SensorManager) context.getSystemService("sensor")).getDefaultSensor(8) != null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static String g(Context context) {
        int i = context.getApplicationContext().getApplicationInfo().icon;
        if (i <= 0) {
            return context.getPackageName();
        }
        try {
            return context.getResources().getResourcePackageName(i);
        } catch (Resources.NotFoundException unused) {
            return context.getPackageName();
        }
    }

    @DexIgnore
    public static SharedPreferences h(Context context) {
        return context.getSharedPreferences("com.google.firebase.crashlytics", 0);
    }

    @DexIgnore
    public static boolean i(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    @DexIgnore
    public static boolean j(Context context) {
        return "sdk".equals(Build.PRODUCT) || "google_sdk".equals(Build.PRODUCT) || Settings.Secure.getString(context.getContentResolver(), "android_id") == null;
    }

    @DexIgnore
    public static boolean k(Context context) {
        boolean j = j(context);
        String str = Build.TAGS;
        if ((!j && str != null && str.contains("test-keys")) || new File("/system/app/Superuser.apk").exists()) {
            return true;
        }
        File file = new File("/system/xbin/su");
        if (j || !file.exists()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static String l(Context context) {
        int a2 = a(context, "com.google.firebase.crashlytics.unity_version", LegacyTokenHelper.TYPE_STRING);
        if (a2 == 0) {
            return null;
        }
        String string = context.getResources().getString(a2);
        z24 a3 = z24.a();
        a3.a("Unity Editor version is: " + string);
        return string;
    }

    @DexIgnore
    public static boolean c() {
        return Debug.isDebuggerConnected() || Debug.waitingForDebugger();
    }

    @DexIgnore
    public static int c(Context context) {
        int i = j(context) ? 1 : 0;
        if (k(context)) {
            i |= 2;
        }
        return c() ? i | 4 : i;
    }

    @DexIgnore
    public static int a() {
        return b.getValue().ordinal();
    }

    @DexIgnore
    public static long a(String str, String str2, int i) {
        return Long.parseLong(str.split(str2)[0].trim()) * ((long) i);
    }

    @DexIgnore
    public static ActivityManager.RunningAppProcessInfo a(String str, Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService(Constants.ACTIVITY)).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                if (runningAppProcessInfo.processName.equals(str)) {
                    return runningAppProcessInfo;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public static String b(InputStream inputStream) throws IOException {
        Scanner useDelimiter = new Scanner(inputStream).useDelimiter("\\A");
        return useDelimiter.hasNext() ? useDelimiter.next() : "";
    }

    @DexIgnore
    public static String b(Context context, String str) {
        int a2 = a(context, str, LegacyTokenHelper.TYPE_STRING);
        return a2 > 0 ? context.getString(a2) : "";
    }

    @DexIgnore
    public static String a(InputStream inputStream) {
        return a(inputStream, Utility.HASH_ALGORITHM_SHA1);
    }

    @DexIgnore
    public static String a(String str, String str2) {
        return a(str.getBytes(), str2);
    }

    @DexIgnore
    public static boolean b(String str) {
        return str == null || str.length() == 0;
    }

    @DexIgnore
    public static String a(InputStream inputStream, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    return a(instance.digest());
                }
                instance.update(bArr, 0, read);
            }
        } catch (Exception e) {
            z24.a().b("Could not calculate hash for app icon.", e);
            return "";
        }
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public static boolean b(Context context) {
        if (!a(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return true;
        }
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnectedOrConnecting()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static String a(byte[] bArr, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str);
            instance.update(bArr);
            return a(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            z24 a2 = z24.a();
            a2.b("Could not create hashing algorithm: " + str + ", returning empty string.", e);
            return "";
        }
    }

    @DexIgnore
    public static String a(String... strArr) {
        if (strArr == null || strArr.length == 0) {
            return null;
        }
        ArrayList<String> arrayList = new ArrayList();
        for (String str : strArr) {
            if (str != null) {
                arrayList.add(str.replace(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, "").toLowerCase(Locale.US));
            }
        }
        Collections.sort(arrayList);
        StringBuilder sb = new StringBuilder();
        for (String str2 : arrayList) {
            sb.append(str2);
        }
        String sb2 = sb.toString();
        if (sb2.length() > 0) {
            return c(sb2);
        }
        return null;
    }

    @DexIgnore
    public static long a(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) context.getSystemService(Constants.ACTIVITY)).getMemoryInfo(memoryInfo);
        return memoryInfo.availMem;
    }

    @DexIgnore
    public static long a(String str) {
        StatFs statFs = new StatFs(str);
        long blockSize = (long) statFs.getBlockSize();
        return (((long) statFs.getBlockCount()) * blockSize) - (blockSize * ((long) statFs.getAvailableBlocks()));
    }

    @DexIgnore
    public static boolean a(Context context, String str, boolean z) {
        Resources resources;
        if (!(context == null || (resources = context.getResources()) == null)) {
            int a2 = a(context, str, LegacyTokenHelper.TYPE_BOOLEAN);
            if (a2 > 0) {
                return resources.getBoolean(a2);
            }
            int a3 = a(context, str, LegacyTokenHelper.TYPE_STRING);
            if (a3 > 0) {
                return Boolean.parseBoolean(context.getString(a3));
            }
        }
        return z;
    }

    @DexIgnore
    public static int a(Context context, String str, String str2) {
        return context.getResources().getIdentifier(str, str2, g(context));
    }

    @DexIgnore
    public static String a(byte[] bArr) {
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            byte b2 = bArr[i] & 255;
            int i2 = i * 2;
            char[] cArr2 = a;
            cArr[i2] = cArr2[b2 >>> 4];
            cArr[i2 + 1] = cArr2[b2 & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY];
        }
        return new String(cArr);
    }

    @DexIgnore
    public static void a(Closeable closeable, String str) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                z24.a().b(str, e);
            }
        }
    }

    @DexIgnore
    public static void a(Flushable flushable, String str) {
        if (flushable != null) {
            try {
                flushable.flush();
            } catch (IOException e) {
                z24.a().b(str, e);
            }
        }
    }

    @DexIgnore
    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception unused) {
            }
        }
    }

    @DexIgnore
    public static boolean a(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }
}
