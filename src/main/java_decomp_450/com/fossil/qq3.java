package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qq3 implements Parcelable.Creator<pq3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ pq3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 2) {
                str = j72.e(parcel, a);
            } else if (a2 == 3) {
                str2 = j72.e(parcel, a);
            } else if (a2 == 4) {
                i = j72.q(parcel, a);
            } else if (a2 != 5) {
                j72.v(parcel, a);
            } else {
                z = j72.i(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new pq3(str, str2, i, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ pq3[] newArray(int i) {
        return new pq3[i];
    }
}
