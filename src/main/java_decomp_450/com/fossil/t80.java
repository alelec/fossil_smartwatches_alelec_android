package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t80 extends n80 {
    @DexIgnore
    public static /* final */ b CREATOR; // = new b(null);
    @DexIgnore
    public static /* final */ short h; // = 255;
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ byte e;
    @DexIgnore
    public /* final */ short f;
    @DexIgnore
    public /* final */ a g;

    @DexIgnore
    public enum a {
        DISABLE((byte) 0),
        ENABLE((byte) 1);
        
        @DexIgnore
        public static /* final */ C0184a c; // = new C0184a(null);
        @DexIgnore
        public /* final */ byte a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.t80$a$a")
        /* renamed from: com.fossil.t80$a$a  reason: collision with other inner class name */
        public static final class C0184a {
            @DexIgnore
            public /* synthetic */ C0184a(zd7 zd7) {
            }

            @DexIgnore
            public final a a(byte b) throws IllegalArgumentException {
                a aVar;
                a[] values = a.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        aVar = null;
                        break;
                    }
                    aVar = values[i];
                    if (aVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (aVar != null) {
                    return aVar;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public a(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Parcelable.Creator<t80> {
        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
        }

        @DexIgnore
        public final t80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 6) {
                return new t80(bArr[0], bArr[1], bArr[2], bArr[3], yz0.b(bArr[4]), a.c.a(bArr[5]));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", require: 6"));
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public t80 createFromParcel(Parcel parcel) {
            return new t80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public t80[] newArray(int i) {
            return new t80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public t80 m73createFromParcel(Parcel parcel) {
            return new t80(parcel, null);
        }
    }

    /*
    static {
        ud7 ud7 = ud7.a;
    }
    */

    @DexIgnore
    public t80(byte b2, byte b3, byte b4, byte b5, short s, a aVar) throws IllegalArgumentException {
        super(o80.INACTIVE_NUDGE);
        this.b = b2;
        this.c = b3;
        this.d = b4;
        this.e = b5;
        this.f = s;
        this.g = aVar;
        e();
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(6).order(ByteOrder.LITTLE_ENDIAN).put(this.b).put(this.c).put(this.d).put(this.e).put((byte) this.f).put(this.g.a()).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        byte b2 = this.b;
        boolean z = true;
        if (b2 >= 0 && 23 >= b2) {
            byte b3 = this.c;
            if (b3 >= 0 && 59 >= b3) {
                byte b4 = this.d;
                if (b4 >= 0 && 23 >= b4) {
                    byte b5 = this.e;
                    if (b5 >= 0 && 59 >= b5) {
                        short s = h;
                        short s2 = this.f;
                        if (s2 < 0 || s < s2) {
                            z = false;
                        }
                        if (!z) {
                            StringBuilder b6 = yh0.b("repeatInterval(");
                            b6.append((int) this.f);
                            b6.append(") is out of range ");
                            b6.append("[0, ");
                            throw new IllegalArgumentException(yh0.a(b6, h, "] (in minute."));
                        }
                        return;
                    }
                    throw new IllegalArgumentException(yh0.a(yh0.b("stopMinute("), this.e, ") is out of range ", "[0, 59]."));
                }
                throw new IllegalArgumentException(yh0.a(yh0.b("stopHour("), this.d, ") is out of range ", "[0, 23]."));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("startMinute("), this.c, ") is out of range ", "[0, 59]."));
        }
        throw new IllegalArgumentException(yh0.a(yh0.b("startHour("), this.b, ") is out of range ", "[0, 23]."));
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(t80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            t80 t80 = (t80) obj;
            return this.b == t80.b && this.c == t80.c && this.d == t80.d && this.e == t80.e && this.f == t80.f && this.g == t80.g;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.InactiveNudgeConfig");
    }

    @DexIgnore
    public final short getRepeatInterval() {
        return this.f;
    }

    @DexIgnore
    public final byte getStartHour() {
        return this.b;
    }

    @DexIgnore
    public final byte getStartMinute() {
        return this.c;
    }

    @DexIgnore
    public final a getState() {
        return this.g;
    }

    @DexIgnore
    public final byte getStopHour() {
        return this.d;
    }

    @DexIgnore
    public final byte getStopMinute() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        return this.g.hashCode() + (((((((((((super.hashCode() * 31) + this.b) * 31) + this.c) * 31) + this.d) * 31) + this.e) * 31) + this.f) * 31);
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
        if (parcel != null) {
            parcel.writeByte(this.e);
        }
        if (parcel != null) {
            parcel.writeInt(yz0.b(this.f));
        }
        if (parcel != null) {
            parcel.writeString(this.g.name());
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("start_hour", Byte.valueOf(this.b));
            jSONObject.put("start_minute", Byte.valueOf(this.c));
            jSONObject.put("stop_hour", Byte.valueOf(this.d));
            jSONObject.put("stop_minute", Byte.valueOf(this.e));
            jSONObject.put("repeat_interval", Short.valueOf(this.f));
            jSONObject.put("state", yz0.a(this.g));
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ t80(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = parcel.readByte();
        this.c = parcel.readByte();
        this.d = parcel.readByte();
        this.e = parcel.readByte();
        this.f = (short) parcel.readInt();
        String readString = parcel.readString();
        if (readString != null) {
            ee7.a((Object) readString, "parcel.readString()!!");
            this.g = a.valueOf(readString);
            e();
            return;
        }
        ee7.a();
        throw null;
    }
}
