package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oe2<T> implements re2<T> {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public oe2(T t) {
        this.a = t;
    }

    @DexIgnore
    @Override // com.fossil.re2
    public final T get() {
        return this.a;
    }
}
