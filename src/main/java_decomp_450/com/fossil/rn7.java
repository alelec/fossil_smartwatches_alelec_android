package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rn7 {
    @DexIgnore
    void onFailure(qn7 qn7, IOException iOException);

    @DexIgnore
    void onResponse(qn7 qn7, Response response) throws IOException;
}
