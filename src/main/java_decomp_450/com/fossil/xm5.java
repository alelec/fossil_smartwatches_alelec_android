package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import com.fossil.fl4;
import com.fossil.nj5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xm5 extends fl4<d, e, b> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a(null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public String e;
    @DexIgnore
    public /* final */ c f; // = new c();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return xm5.g;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements nj5.b {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            FLogger.INSTANCE.getLocal().d(xm5.h.a(), "Inside .onReceive");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            int intExtra2 = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), ServiceActionResult.UNALLOWED_ACTION.ordinal());
            if (xm5.this.e() && ee7.a((Object) xm5.this.d(), (Object) stringExtra) && intExtra == CommunicateMode.READ_RSSI.ordinal()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = xm5.h.a();
                local.d(a2, "onReceive - blePhase: " + intExtra + " - deviceId: " + stringExtra + " - mIsExecuted: " + xm5.this.e());
                xm5.this.a(false);
                if (intExtra2 != ServiceActionResult.SUCCEEDED.ordinal() || intent.getExtras() == null) {
                    FLogger.INSTANCE.getLocal().e(xm5.h.a(), "Inside .onReceive return error");
                    xm5.this.a(new b());
                    return;
                }
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    int i = extras.getInt("rssi", 0);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = xm5.h.a();
                    local2.d(a3, "Inside .onReceive return rssi=" + i);
                    xm5.this.a(new e(i));
                    return;
                }
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            ee7.b(str, "deviceId");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements fl4.d {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public e(int i) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = xm5.class.getSimpleName();
        ee7.a((Object) simpleName, "GetRssi::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(d dVar, fb7 fb7) {
        return a(dVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return g;
    }

    @DexIgnore
    public final String d() {
        String str = this.e;
        if (str != null) {
            return str;
        }
        ee7.d("mDeviceId");
        throw null;
    }

    @DexIgnore
    public final boolean e() {
        return this.d;
    }

    @DexIgnore
    public final void f() {
        nj5.d.a(this.f, CommunicateMode.READ_RSSI);
    }

    @DexIgnore
    public final void g() {
        nj5.d.b(this.f, CommunicateMode.READ_RSSI);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public Object a(d dVar, fb7<Object> fb7) {
        String str;
        try {
            FLogger.INSTANCE.getLocal().d(g, "Inside .run");
            this.d = true;
            if (dVar == null || (str = dVar.a()) == null) {
                str = "";
            }
            this.e = str;
            if (PortfolioApp.g0.b() != null) {
                IButtonConnectivity b2 = PortfolioApp.g0.b();
                String str2 = null;
                if (b2 != null) {
                    if (dVar != null) {
                        str2 = dVar.a();
                    }
                    return pb7.a(b2.deviceGetRssi(str2));
                }
                ee7.a();
                throw null;
            }
            FLogger.INSTANCE.getLocal().e(g, "Inside .run ButtonApi is null");
            return new b();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = g;
            local.e(str3, "Inside .run caught exception=" + e2);
            return new b();
        }
    }
}
