package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ky0 {
    GET_CHALLENGE_INFO("req_chl_info"),
    LIST_CHALLENGES("req_chl_list"),
    SYNC_DATA("sync_pkg");
    
    @DexIgnore
    public static /* final */ sw0 f; // = new sw0(null);
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public ky0(String str) {
        this.a = str;
    }
}
