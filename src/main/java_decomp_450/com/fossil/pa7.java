package com.fossil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pa7 extends oa7 {
    @DexIgnore
    public static final <K, V> List<r87<K, V>> d(Map<? extends K, ? extends V> map) {
        ee7.b(map, "$this$toList");
        if (map.size() == 0) {
            return w97.a();
        }
        Iterator<Map.Entry<? extends K, ? extends V>> it = map.entrySet().iterator();
        if (!it.hasNext()) {
            return w97.a();
        }
        Map.Entry<? extends K, ? extends V> next = it.next();
        if (!it.hasNext()) {
            return v97.a(new r87(next.getKey(), next.getValue()));
        }
        ArrayList arrayList = new ArrayList(map.size());
        arrayList.add(new r87(next.getKey(), next.getValue()));
        do {
            Map.Entry<? extends K, ? extends V> next2 = it.next();
            arrayList.add(new r87(next2.getKey(), next2.getValue()));
        } while (it.hasNext());
        return arrayList;
    }
}
