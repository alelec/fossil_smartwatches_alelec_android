package com.fossil;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorSpace;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.DisplayMetrics;
import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.r10;
import com.fossil.y10;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s10 {
    @DexIgnore
    public static /* final */ zw<tw> f; // = zw.a("com.bumptech.glide.load.resource.bitmap.Downsampler.DecodeFormat", tw.DEFAULT);
    @DexIgnore
    public static /* final */ zw<bx> g; // = zw.a("com.bumptech.glide.load.resource.bitmap.Downsampler.PreferredColorSpace", bx.SRGB);
    @DexIgnore
    public static /* final */ zw<Boolean> h; // = zw.a("com.bumptech.glide.load.resource.bitmap.Downsampler.FixBitmapSize", (Object) false);
    @DexIgnore
    public static /* final */ zw<Boolean> i; // = zw.a("com.bumptech.glide.load.resource.bitmap.Downsampler.AllowHardwareDecode", (Object) false);
    @DexIgnore
    public static /* final */ Set<String> j; // = Collections.unmodifiableSet(new HashSet(Arrays.asList("image/vnd.wap.wbmp", "image/x-ico")));
    @DexIgnore
    public static /* final */ b k; // = new a();
    @DexIgnore
    public static /* final */ Set<ImageHeaderParser.ImageType> l; // = Collections.unmodifiableSet(EnumSet.of(ImageHeaderParser.ImageType.JPEG, ImageHeaderParser.ImageType.PNG_A, ImageHeaderParser.ImageType.PNG));
    @DexIgnore
    public static /* final */ Queue<BitmapFactory.Options> m; // = v50.a(0);
    @DexIgnore
    public /* final */ dz a;
    @DexIgnore
    public /* final */ DisplayMetrics b;
    @DexIgnore
    public /* final */ az c;
    @DexIgnore
    public /* final */ List<ImageHeaderParser> d;
    @DexIgnore
    public /* final */ x10 e; // = x10.b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements b {
        @DexIgnore
        @Override // com.fossil.s10.b
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.s10.b
        public void a(dz dzVar, Bitmap bitmap) {
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(dz dzVar, Bitmap bitmap) throws IOException;
    }

    /*
    static {
        zw<r10> zwVar = r10.f;
    }
    */

    @DexIgnore
    public s10(List<ImageHeaderParser> list, DisplayMetrics displayMetrics, dz dzVar, az azVar) {
        this.d = list;
        u50.a(displayMetrics);
        this.b = displayMetrics;
        u50.a(dzVar);
        this.a = dzVar;
        u50.a(azVar);
        this.c = azVar;
    }

    @DexIgnore
    public static boolean a(int i2) {
        return i2 == 90 || i2 == 270;
    }

    @DexIgnore
    public static int b(double d2) {
        if (d2 > 1.0d) {
            d2 = 1.0d / d2;
        }
        return (int) Math.round(d2 * 2.147483647E9d);
    }

    @DexIgnore
    public static int c(double d2) {
        return (int) (d2 + 0.5d);
    }

    @DexIgnore
    public static void c(BitmapFactory.Options options) {
        d(options);
        synchronized (m) {
            m.offer(options);
        }
    }

    @DexIgnore
    public static void d(BitmapFactory.Options options) {
        options.inTempStorage = null;
        options.inDither = false;
        options.inScaled = false;
        options.inSampleSize = 1;
        options.inPreferredConfig = null;
        options.inJustDecodeBounds = false;
        options.inDensity = 0;
        options.inTargetDensity = 0;
        if (Build.VERSION.SDK_INT >= 26) {
            options.inPreferredColorSpace = null;
            options.outColorSpace = null;
            options.outConfig = null;
        }
        options.outWidth = 0;
        options.outHeight = 0;
        options.outMimeType = null;
        options.inBitmap = null;
        options.inMutable = true;
    }

    @DexIgnore
    public boolean a(ParcelFileDescriptor parcelFileDescriptor) {
        return rx.c();
    }

    @DexIgnore
    public boolean a(InputStream inputStream) {
        return true;
    }

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer) {
        return true;
    }

    @DexIgnore
    public static int[] b(y10 y10, BitmapFactory.Options options, b bVar, dz dzVar) throws IOException {
        options.inJustDecodeBounds = true;
        a(y10, options, bVar, dzVar);
        options.inJustDecodeBounds = false;
        return new int[]{options.outWidth, options.outHeight};
    }

    @DexIgnore
    public uy<Bitmap> a(InputStream inputStream, int i2, int i3, ax axVar) throws IOException {
        return a(inputStream, i2, i3, axVar, k);
    }

    @DexIgnore
    public uy<Bitmap> a(InputStream inputStream, int i2, int i3, ax axVar, b bVar) throws IOException {
        return a(new y10.a(inputStream, this.d, this.c), i2, i3, axVar, bVar);
    }

    @DexIgnore
    public uy<Bitmap> a(ParcelFileDescriptor parcelFileDescriptor, int i2, int i3, ax axVar) throws IOException {
        return a(new y10.b(parcelFileDescriptor, this.d, this.c), i2, i3, axVar, k);
    }

    @DexIgnore
    public final uy<Bitmap> a(y10 y10, int i2, int i3, ax axVar, b bVar) throws IOException {
        byte[] bArr = (byte[]) this.c.b(65536, byte[].class);
        BitmapFactory.Options a2 = a();
        a2.inTempStorage = bArr;
        tw twVar = (tw) axVar.a(f);
        bx bxVar = (bx) axVar.a(g);
        try {
            return k10.a(a(y10, a2, (r10) axVar.a(r10.f), twVar, bxVar, axVar.a(i) != null && ((Boolean) axVar.a(i)).booleanValue(), i2, i3, ((Boolean) axVar.a(h)).booleanValue(), bVar), this.a);
        } finally {
            c(a2);
            this.c.a(bArr);
        }
    }

    @DexIgnore
    public static boolean b(BitmapFactory.Options options) {
        int i2;
        int i3 = options.inTargetDensity;
        return i3 > 0 && (i2 = options.inDensity) > 0 && i3 != i2;
    }

    @DexIgnore
    public final Bitmap a(y10 y10, BitmapFactory.Options options, r10 r10, tw twVar, bx bxVar, boolean z, int i2, int i3, boolean z2, b bVar) throws IOException {
        int i4;
        int i5;
        s10 s10;
        int i6;
        ColorSpace colorSpace;
        int i7;
        int i8;
        long a2 = q50.a();
        int[] b2 = b(y10, options, bVar, this.a);
        boolean z3 = false;
        z3 = false;
        z3 = false;
        int i9 = b2[0];
        int i10 = b2[1];
        String str = options.outMimeType;
        boolean z4 = (i9 == -1 || i10 == -1) ? false : z;
        int a3 = y10.a();
        int a4 = f20.a(a3);
        boolean b3 = f20.b(a3);
        if (i2 == Integer.MIN_VALUE) {
            i5 = i3;
            i4 = a(a4) ? i10 : i9;
        } else {
            i5 = i3;
            i4 = i2;
        }
        int i11 = i5 == Integer.MIN_VALUE ? a(a4) ? i9 : i10 : i5;
        ImageHeaderParser.ImageType c2 = y10.c();
        a(c2, y10, bVar, this.a, r10, a4, i9, i10, i4, i11, options);
        a(y10, twVar, z4, b3, options, i4, i11);
        boolean z5 = Build.VERSION.SDK_INT >= 19;
        if (options.inSampleSize == 1 || z5) {
            s10 = this;
            if (s10.a(c2)) {
                if (i9 < 0 || i10 < 0 || !z2 || !z5) {
                    float f2 = b(options) ? ((float) options.inTargetDensity) / ((float) options.inDensity) : 1.0f;
                    int i12 = options.inSampleSize;
                    float f3 = (float) i12;
                    i8 = Math.round(((float) ((int) Math.ceil((double) (((float) i9) / f3)))) * f2);
                    i7 = Math.round(((float) ((int) Math.ceil((double) (((float) i10) / f3)))) * f2);
                    if (Log.isLoggable("Downsampler", 2)) {
                        Log.v("Downsampler", "Calculated target [" + i8 + "x" + i7 + "] for source [" + i9 + "x" + i10 + "], sampleSize: " + i12 + ", targetDensity: " + options.inTargetDensity + ", density: " + options.inDensity + ", density multiplier: " + f2);
                    }
                } else {
                    i8 = i4;
                    i7 = i11;
                }
                if (i8 > 0 && i7 > 0) {
                    a(options, s10.a, i8, i7);
                }
            }
        } else {
            s10 = this;
        }
        int i13 = Build.VERSION.SDK_INT;
        if (i13 >= 28) {
            if (bxVar == bx.DISPLAY_P3 && (colorSpace = options.outColorSpace) != null && colorSpace.isWideGamut()) {
                z3 = true;
            }
            options.inPreferredColorSpace = ColorSpace.get(z3 ? ColorSpace.Named.DISPLAY_P3 : ColorSpace.Named.SRGB);
        } else if (i13 >= 26) {
            options.inPreferredColorSpace = ColorSpace.get(ColorSpace.Named.SRGB);
        }
        Bitmap a5 = a(y10, options, bVar, s10.a);
        bVar.a(s10.a, a5);
        if (Log.isLoggable("Downsampler", 2)) {
            i6 = a3;
            a(i9, i10, str, options, a5, i2, i3, a2);
        } else {
            i6 = a3;
        }
        Bitmap bitmap = null;
        if (a5 != null) {
            a5.setDensity(s10.b.densityDpi);
            bitmap = f20.a(s10.a, a5, i6);
            if (!a5.equals(bitmap)) {
                s10.a.a(a5);
            }
        }
        return bitmap;
    }

    @DexIgnore
    public static void a(ImageHeaderParser.ImageType imageType, y10 y10, b bVar, dz dzVar, r10 r10, int i2, int i3, int i4, int i5, int i6, BitmapFactory.Options options) throws IOException {
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        double d2;
        if (i3 > 0 && i4 > 0) {
            if (a(i2)) {
                i7 = i3;
                i8 = i4;
            } else {
                i8 = i3;
                i7 = i4;
            }
            float b2 = r10.b(i8, i7, i5, i6);
            if (b2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                r10.g a2 = r10.a(i8, i7, i5, i6);
                if (a2 != null) {
                    float f2 = (float) i8;
                    float f3 = (float) i7;
                    int c2 = i8 / c((double) (b2 * f2));
                    int c3 = i7 / c((double) (b2 * f3));
                    if (a2 == r10.g.MEMORY) {
                        i9 = Math.max(c2, c3);
                    } else {
                        i9 = Math.min(c2, c3);
                    }
                    if (Build.VERSION.SDK_INT > 23 || !j.contains(options.outMimeType)) {
                        i10 = Math.max(1, Integer.highestOneBit(i9));
                        if (a2 == r10.g.MEMORY && ((float) i10) < 1.0f / b2) {
                            i10 <<= 1;
                        }
                    } else {
                        i10 = 1;
                    }
                    options.inSampleSize = i10;
                    if (imageType == ImageHeaderParser.ImageType.JPEG) {
                        float min = (float) Math.min(i10, 8);
                        i11 = (int) Math.ceil((double) (f2 / min));
                        i12 = (int) Math.ceil((double) (f3 / min));
                        int i13 = i10 / 8;
                        if (i13 > 0) {
                            i11 /= i13;
                            i12 /= i13;
                        }
                    } else {
                        if (imageType == ImageHeaderParser.ImageType.PNG || imageType == ImageHeaderParser.ImageType.PNG_A) {
                            float f4 = (float) i10;
                            i11 = (int) Math.floor((double) (f2 / f4));
                            d2 = Math.floor((double) (f3 / f4));
                        } else if (imageType == ImageHeaderParser.ImageType.WEBP || imageType == ImageHeaderParser.ImageType.WEBP_A) {
                            if (Build.VERSION.SDK_INT >= 24) {
                                float f5 = (float) i10;
                                i11 = Math.round(f2 / f5);
                                i12 = Math.round(f3 / f5);
                            } else {
                                float f6 = (float) i10;
                                i11 = (int) Math.floor((double) (f2 / f6));
                                d2 = Math.floor((double) (f3 / f6));
                            }
                        } else if (i8 % i10 == 0 && i7 % i10 == 0) {
                            i11 = i8 / i10;
                            i12 = i7 / i10;
                        } else {
                            int[] b3 = b(y10, options, bVar, dzVar);
                            i11 = b3[0];
                            i12 = b3[1];
                        }
                        i12 = (int) d2;
                    }
                    double b4 = (double) r10.b(i11, i12, i5, i6);
                    if (Build.VERSION.SDK_INT >= 19) {
                        options.inTargetDensity = a(b4);
                        options.inDensity = b(b4);
                    }
                    if (b(options)) {
                        options.inScaled = true;
                    } else {
                        options.inTargetDensity = 0;
                        options.inDensity = 0;
                    }
                    if (Log.isLoggable("Downsampler", 2)) {
                        Log.v("Downsampler", "Calculate scaling, source: [" + i3 + "x" + i4 + "], degreesToRotate: " + i2 + ", target: [" + i5 + "x" + i6 + "], power of two scaled: [" + i11 + "x" + i12 + "], exact scale factor: " + b2 + ", power of 2 sample size: " + i10 + ", adjusted scale factor: " + b4 + ", target density: " + options.inTargetDensity + ", density: " + options.inDensity);
                        return;
                    }
                    return;
                }
                throw new IllegalArgumentException("Cannot round with null rounding");
            }
            throw new IllegalArgumentException("Cannot scale with factor: " + b2 + " from: " + r10 + ", source: [" + i3 + "x" + i4 + "], target: [" + i5 + "x" + i6 + "]");
        } else if (Log.isLoggable("Downsampler", 3)) {
            Log.d("Downsampler", "Unable to determine dimensions for: " + imageType + " with target [" + i5 + "x" + i6 + "]");
        }
    }

    @DexIgnore
    public static int a(double d2) {
        int b2 = b(d2);
        int c2 = c(((double) b2) * d2);
        return c((d2 / ((double) (((float) c2) / ((float) b2)))) * ((double) c2));
    }

    @DexIgnore
    public final boolean a(ImageHeaderParser.ImageType imageType) {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return l.contains(imageType);
    }

    @DexIgnore
    public final void a(y10 y10, tw twVar, boolean z, boolean z2, BitmapFactory.Options options, int i2, int i3) {
        if (!this.e.a(i2, i3, options, z, z2)) {
            if (twVar == tw.PREFER_ARGB_8888 || Build.VERSION.SDK_INT == 16) {
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                return;
            }
            boolean z3 = false;
            try {
                z3 = y10.c().hasAlpha();
            } catch (IOException e2) {
                if (Log.isLoggable("Downsampler", 3)) {
                    Log.d("Downsampler", "Cannot determine whether the image has alpha or not from header, format " + twVar, e2);
                }
            }
            Bitmap.Config config = z3 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565;
            options.inPreferredConfig = config;
            if (config == Bitmap.Config.RGB_565) {
                options.inDither = true;
            }
        }
    }

    @DexIgnore
    public static Bitmap a(y10 y10, BitmapFactory.Options options, b bVar, dz dzVar) throws IOException {
        if (!options.inJustDecodeBounds) {
            bVar.a();
            y10.b();
        }
        int i2 = options.outWidth;
        int i3 = options.outHeight;
        String str = options.outMimeType;
        f20.a().lock();
        try {
            return y10.a(options);
        } catch (IllegalArgumentException e2) {
            IOException a2 = a(e2, i2, i3, str, options);
            if (Log.isLoggable("Downsampler", 3)) {
                Log.d("Downsampler", "Failed to decode with inBitmap, trying again without Bitmap re-use", a2);
            }
            if (options.inBitmap != null) {
                try {
                    dzVar.a(options.inBitmap);
                    options.inBitmap = null;
                    return a(y10, options, bVar, dzVar);
                } catch (IOException unused) {
                    throw a2;
                }
            } else {
                throw a2;
            }
        } finally {
            f20.a().unlock();
        }
    }

    @DexIgnore
    public static void a(int i2, int i3, String str, BitmapFactory.Options options, Bitmap bitmap, int i4, int i5, long j2) {
        Log.v("Downsampler", "Decoded " + a(bitmap) + " from [" + i2 + "x" + i3 + "] " + str + " with inBitmap " + a(options) + " for [" + i4 + "x" + i5 + "], sample size: " + options.inSampleSize + ", density: " + options.inDensity + ", target density: " + options.inTargetDensity + ", thread: " + Thread.currentThread().getName() + ", duration: " + q50.a(j2));
    }

    @DexIgnore
    public static String a(BitmapFactory.Options options) {
        return a(options.inBitmap);
    }

    @DexIgnore
    @TargetApi(19)
    public static String a(Bitmap bitmap) {
        String str;
        if (bitmap == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            str = " (" + bitmap.getAllocationByteCount() + ")";
        } else {
            str = "";
        }
        return "[" + bitmap.getWidth() + "x" + bitmap.getHeight() + "] " + bitmap.getConfig() + str;
    }

    @DexIgnore
    public static IOException a(IllegalArgumentException illegalArgumentException, int i2, int i3, String str, BitmapFactory.Options options) {
        return new IOException("Exception decoding bitmap, outWidth: " + i2 + ", outHeight: " + i3 + ", outMimeType: " + str + ", inBitmap: " + a(options), illegalArgumentException);
    }

    @DexIgnore
    @TargetApi(26)
    public static void a(BitmapFactory.Options options, dz dzVar, int i2, int i3) {
        Bitmap.Config config;
        if (Build.VERSION.SDK_INT < 26) {
            config = null;
        } else if (options.inPreferredConfig != Bitmap.Config.HARDWARE) {
            config = options.outConfig;
        } else {
            return;
        }
        if (config == null) {
            config = options.inPreferredConfig;
        }
        options.inBitmap = dzVar.b(i2, i3, config);
    }

    @DexIgnore
    public static synchronized BitmapFactory.Options a() {
        BitmapFactory.Options poll;
        synchronized (s10.class) {
            synchronized (m) {
                poll = m.poll();
            }
            if (poll == null) {
                poll = new BitmapFactory.Options();
                d(poll);
            }
        }
        return poll;
    }
}
