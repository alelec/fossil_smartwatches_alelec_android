package com.fossil;

import android.content.ContentValues;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.fossil.lp2;
import com.fossil.mp2;
import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ih3 extends yl3 implements gb3 {
    @DexIgnore
    public static int j; // = 65535;
    @DexIgnore
    public static int k; // = 2;
    @DexIgnore
    public /* final */ Map<String, Map<String, String>> d; // = new n4();
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> e; // = new n4();
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> f; // = new n4();
    @DexIgnore
    public /* final */ Map<String, mp2> g; // = new n4();
    @DexIgnore
    public /* final */ Map<String, Map<String, Integer>> h; // = new n4();
    @DexIgnore
    public /* final */ Map<String, String> i; // = new n4();

    @DexIgnore
    public ih3(xl3 xl3) {
        super(xl3);
    }

    @DexIgnore
    public final mp2 a(String str) {
        q();
        g();
        a72.b(str);
        i(str);
        return this.g.get(str);
    }

    @DexIgnore
    public final String b(String str) {
        g();
        return this.i.get(str);
    }

    @DexIgnore
    public final void c(String str) {
        g();
        this.i.put(str, null);
    }

    @DexIgnore
    public final void d(String str) {
        g();
        this.g.remove(str);
    }

    @DexIgnore
    public final boolean e(String str) {
        g();
        mp2 a = a(str);
        if (a == null) {
            return false;
        }
        return a.v();
    }

    @DexIgnore
    public final long f(String str) {
        String a = a(str, "measurement.account.time_zone_offset_minutes");
        if (TextUtils.isEmpty(a)) {
            return 0;
        }
        try {
            return Long.parseLong(a);
        } catch (NumberFormatException e2) {
            e().w().a("Unable to parse timezone offset. appId", jg3.a(str), e2);
            return 0;
        }
    }

    @DexIgnore
    public final boolean g(String str) {
        return "1".equals(a(str, "measurement.upload.blacklist_internal"));
    }

    @DexIgnore
    public final boolean h(String str) {
        return "1".equals(a(str, "measurement.upload.blacklist_public"));
    }

    @DexIgnore
    public final void i(String str) {
        q();
        g();
        a72.b(str);
        if (this.g.get(str) == null) {
            byte[] d2 = n().d(str);
            if (d2 == null) {
                this.d.put(str, null);
                this.e.put(str, null);
                this.f.put(str, null);
                this.g.put(str, null);
                this.i.put(str, null);
                this.h.put(str, null);
                return;
            }
            mp2.a aVar = (mp2.a) a(str, d2).l();
            a(str, aVar);
            this.d.put(str, a((mp2) ((bw2) aVar.g())));
            this.g.put(str, (mp2) ((bw2) aVar.g()));
            this.i.put(str, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.yl3
    public final boolean s() {
        return false;
    }

    @DexIgnore
    public final boolean b(String str, String str2) {
        Boolean bool;
        g();
        i(str);
        if (g(str) && jm3.i(str2)) {
            return true;
        }
        if (h(str) && jm3.h(str2)) {
            return true;
        }
        Map<String, Boolean> map = this.e.get(str);
        if (map == null || (bool = map.get(str2)) == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final boolean c(String str, String str2) {
        Boolean bool;
        g();
        i(str);
        if ("ecommerce_purchase".equals(str2)) {
            return true;
        }
        if (p03.a() && l().a(wb3.J0) && ("purchase".equals(str2) || "refund".equals(str2))) {
            return true;
        }
        Map<String, Boolean> map = this.f.get(str);
        if (map == null || (bool = map.get(str2)) == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final int d(String str, String str2) {
        Integer num;
        g();
        i(str);
        Map<String, Integer> map = this.h.get(str);
        if (map == null || (num = map.get(str2)) == null) {
            return 1;
        }
        return num.intValue();
    }

    @DexIgnore
    @Override // com.fossil.gb3
    public final String a(String str, String str2) {
        g();
        i(str);
        Map<String, String> map = this.d.get(str);
        if (map != null) {
            return map.get(str2);
        }
        return null;
    }

    @DexIgnore
    public static Map<String, String> a(mp2 mp2) {
        n4 n4Var = new n4();
        if (mp2 != null) {
            for (np2 np2 : mp2.s()) {
                n4Var.put(np2.zza(), np2.p());
            }
        }
        return n4Var;
    }

    @DexIgnore
    public final void a(String str, mp2.a aVar) {
        n4 n4Var = new n4();
        n4 n4Var2 = new n4();
        n4 n4Var3 = new n4();
        if (aVar != null) {
            for (int i2 = 0; i2 < aVar.zza(); i2++) {
                lp2.a aVar2 = (lp2.a) aVar.a(i2).l();
                if (TextUtils.isEmpty(aVar2.zza())) {
                    e().w().a("EventConfig contained null event name");
                } else {
                    String b = ni3.b(aVar2.zza());
                    if (!TextUtils.isEmpty(b)) {
                        aVar2.a(b);
                        aVar.a(i2, aVar2);
                    }
                    n4Var.put(aVar2.zza(), Boolean.valueOf(aVar2.o()));
                    n4Var2.put(aVar2.zza(), Boolean.valueOf(aVar2.p()));
                    if (aVar2.q()) {
                        if (aVar2.zze() < k || aVar2.zze() > j) {
                            e().w().a("Invalid sampling rate. Event name, sample rate", aVar2.zza(), Integer.valueOf(aVar2.zze()));
                        } else {
                            n4Var3.put(aVar2.zza(), Integer.valueOf(aVar2.zze()));
                        }
                    }
                }
            }
        }
        this.e.put(str, n4Var);
        this.f.put(str, n4Var2);
        this.h.put(str, n4Var3);
    }

    @DexIgnore
    public final boolean a(String str, byte[] bArr, String str2) {
        q();
        g();
        a72.b(str);
        mp2.a aVar = (mp2.a) a(str, bArr).l();
        if (aVar == null) {
            return false;
        }
        a(str, aVar);
        this.g.put(str, (mp2) ((bw2) aVar.g()));
        this.i.put(str, str2);
        this.d.put(str, a((mp2) ((bw2) aVar.g())));
        n().a(str, new ArrayList(aVar.o()));
        try {
            aVar.p();
            bArr = ((mp2) ((bw2) aVar.g())).a();
        } catch (RuntimeException e2) {
            e().w().a("Unable to serialize reduced-size config. Storing full config instead. appId", jg3.a(str), e2);
        }
        jb3 n = n();
        a72.b(str);
        n.g();
        n.q();
        ContentValues contentValues = new ContentValues();
        contentValues.put("remote_config", bArr);
        try {
            if (((long) n.u().update("apps", contentValues, "app_id = ?", new String[]{str})) == 0) {
                n.e().t().a("Failed to update remote config (got 0). appId", jg3.a(str));
            }
        } catch (SQLiteException e3) {
            n.e().t().a("Error storing remote config. appId", jg3.a(str), e3);
        }
        this.g.put(str, (mp2) ((bw2) aVar.g()));
        return true;
    }

    @DexIgnore
    public final mp2 a(String str, byte[] bArr) {
        if (bArr == null) {
            return mp2.y();
        }
        try {
            mp2.a x = mp2.x();
            fm3.a(x, bArr);
            mp2 mp2 = (mp2) ((bw2) x.g());
            mg3 B = e().B();
            String str2 = null;
            Long valueOf = mp2.zza() ? Long.valueOf(mp2.p()) : null;
            if (mp2.q()) {
                str2 = mp2.r();
            }
            B.a("Parsed config. version, gmp_app_id", valueOf, str2);
            return mp2;
        } catch (iw2 e2) {
            e().w().a("Unable to merge remote config. appId", jg3.a(str), e2);
            return mp2.y();
        } catch (RuntimeException e3) {
            e().w().a("Unable to merge remote config. appId", jg3.a(str), e3);
            return mp2.y();
        }
    }
}
