package com.fossil;

import com.facebook.LegacyTokenHelper;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xd7 implements tf7<Object>, wd7 {
    @DexIgnore
    public static /* final */ Map<Class<? extends j87<?>>, Integer> b;
    @DexIgnore
    public static /* final */ HashMap<String, String> c;
    @DexIgnore
    public static /* final */ HashMap<String, String> d;
    @DexIgnore
    public static /* final */ HashMap<String, String> e;
    @DexIgnore
    public /* final */ Class<?> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v8, resolved type: java.util.HashMap<java.lang.String, java.lang.String> */
    /* JADX DEBUG: Multi-variable search result rejected for r5v16, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r3v45, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    /*
    static {
        new a(null);
        int i = 0;
        List c2 = w97.c(vc7.class, gd7.class, kd7.class, ld7.class, md7.class, nd7.class, od7.class, pd7.class, qd7.class, rd7.class, wc7.class, xc7.class, yc7.class, zc7.class, ad7.class, bd7.class, cd7.class, dd7.class, ed7.class, fd7.class, hd7.class, id7.class, jd7.class);
        ArrayList arrayList = new ArrayList(x97.a(c2, 10));
        for (Object obj : c2) {
            int i2 = i + 1;
            if (i >= 0) {
                arrayList.add(w87.a((Class) obj, Integer.valueOf(i)));
                i = i2;
            } else {
                w97.c();
                throw null;
            }
        }
        b = oa7.a(arrayList);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("boolean", "kotlin.Boolean");
        hashMap.put(LegacyTokenHelper.TYPE_CHAR, "kotlin.Char");
        hashMap.put(LegacyTokenHelper.TYPE_BYTE, "kotlin.Byte");
        hashMap.put(LegacyTokenHelper.TYPE_SHORT, "kotlin.Short");
        hashMap.put(LegacyTokenHelper.TYPE_INTEGER, "kotlin.Int");
        hashMap.put(LegacyTokenHelper.TYPE_FLOAT, "kotlin.Float");
        hashMap.put(LegacyTokenHelper.TYPE_LONG, "kotlin.Long");
        hashMap.put(LegacyTokenHelper.TYPE_DOUBLE, "kotlin.Double");
        c = hashMap;
        HashMap<String, String> hashMap2 = new HashMap<>();
        hashMap2.put("java.lang.Boolean", "kotlin.Boolean");
        hashMap2.put("java.lang.Character", "kotlin.Char");
        hashMap2.put("java.lang.Byte", "kotlin.Byte");
        hashMap2.put("java.lang.Short", "kotlin.Short");
        hashMap2.put("java.lang.Integer", "kotlin.Int");
        hashMap2.put("java.lang.Float", "kotlin.Float");
        hashMap2.put("java.lang.Long", "kotlin.Long");
        hashMap2.put("java.lang.Double", "kotlin.Double");
        d = hashMap2;
        HashMap<String, String> hashMap3 = new HashMap<>();
        hashMap3.put("java.lang.Object", "kotlin.Any");
        hashMap3.put("java.lang.String", "kotlin.String");
        hashMap3.put("java.lang.CharSequence", "kotlin.CharSequence");
        hashMap3.put("java.lang.Throwable", "kotlin.Throwable");
        hashMap3.put("java.lang.Cloneable", "kotlin.Cloneable");
        hashMap3.put("java.lang.Number", "kotlin.Number");
        hashMap3.put("java.lang.Comparable", "kotlin.Comparable");
        hashMap3.put("java.lang.Enum", "kotlin.Enum");
        hashMap3.put("java.lang.annotation.Annotation", "kotlin.Annotation");
        hashMap3.put("java.lang.Iterable", "kotlin.collections.Iterable");
        hashMap3.put("java.util.Iterator", "kotlin.collections.Iterator");
        hashMap3.put("java.util.Collection", "kotlin.collections.Collection");
        hashMap3.put("java.util.List", "kotlin.collections.List");
        hashMap3.put("java.util.Set", "kotlin.collections.Set");
        hashMap3.put("java.util.ListIterator", "kotlin.collections.ListIterator");
        hashMap3.put("java.util.Map", "kotlin.collections.Map");
        hashMap3.put("java.util.Map$Entry", "kotlin.collections.Map.Entry");
        hashMap3.put("kotlin.jvm.internal.StringCompanionObject", "kotlin.String.Companion");
        hashMap3.put("kotlin.jvm.internal.EnumCompanionObject", "kotlin.Enum.Companion");
        hashMap3.putAll(c);
        hashMap3.putAll(d);
        Collection<String> values = c.values();
        ee7.a((Object) values, "primitiveFqNames.values");
        for (String str : values) {
            StringBuilder sb = new StringBuilder();
            sb.append("kotlin.jvm.internal.");
            ee7.a((Object) str, "kotlinName");
            sb.append(nh7.a(str, '.', (String) null, 2, (Object) null));
            sb.append("CompanionObject");
            String sb2 = sb.toString();
            r87 a2 = w87.a(sb2, str + ".Companion");
            hashMap3.put(a2.getFirst(), a2.getSecond());
        }
        for (Map.Entry<Class<? extends j87<?>>, Integer> entry : b.entrySet()) {
            int intValue = entry.getValue().intValue();
            String name = entry.getKey().getName();
            hashMap3.put(name, "kotlin.Function" + intValue);
        }
        e = hashMap3;
        LinkedHashMap linkedHashMap = new LinkedHashMap(na7.a(hashMap3.size()));
        for (Map.Entry entry2 : hashMap3.entrySet()) {
            linkedHashMap.put(entry2.getKey(), nh7.a((String) entry2.getValue(), '.', (String) null, 2, (Object) null));
        }
    }
    */

    @DexIgnore
    public xd7(Class<?> cls) {
        ee7.b(cls, "jClass");
        this.a = cls;
    }

    @DexIgnore
    @Override // com.fossil.wd7
    public Class<?> a() {
        return this.a;
    }

    @DexIgnore
    public final Void b() {
        throw new uc7();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof xd7) && ee7.a(tc7.a(this), tc7.a((tf7) obj));
    }

    @DexIgnore
    @Override // com.fossil.rf7
    public List<Annotation> getAnnotations() {
        b();
        throw null;
    }

    @DexIgnore
    public int hashCode() {
        return tc7.a((tf7) this).hashCode();
    }

    @DexIgnore
    public String toString() {
        return a().toString() + " (Kotlin reflection is not available)";
    }
}
