package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qa2 extends na2 {
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public qa2(byte[] bArr) {
        super(Arrays.copyOfRange(bArr, 0, 25));
        this.b = bArr;
    }

    @DexIgnore
    @Override // com.fossil.na2
    public final byte[] E() {
        return this.b;
    }
}
