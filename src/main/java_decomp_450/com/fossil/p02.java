package com.fossil;

import android.content.Context;
import android.content.res.Resources;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p02 extends q02 {
    @DexIgnore
    @Deprecated
    public static /* final */ int f; // = q02.a;

    @DexIgnore
    @Override // com.fossil.q02
    @Deprecated
    public static int a(Context context, int i) {
        return q02.a(context, i);
    }

    @DexIgnore
    @Override // com.fossil.q02
    public static Context c(Context context) {
        return q02.c(context);
    }

    @DexIgnore
    @Override // com.fossil.q02
    public static Resources d(Context context) {
        return q02.d(context);
    }

    @DexIgnore
    @Override // com.fossil.q02
    @Deprecated
    public static int f(Context context) {
        return q02.f(context);
    }
}
