package com.fossil;

import android.text.TextUtils;
import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mg5 {
    @DexIgnore
    public static lg5 a; // = new lg5();
    @DexIgnore
    public static String b; // = ", ";

    @DexIgnore
    public static void a(String str, boolean z) {
        a.a(str, z);
    }

    @DexIgnore
    public static String b(String str) {
        return a.a(str);
    }

    @DexIgnore
    public static void a() {
        a.a();
        hg5.b().a();
    }

    @DexIgnore
    public static void b() {
        lg5 lg5 = a;
        if (lg5 != null && lg5.b() != null) {
            hg5 b2 = hg5.b();
            for (Map.Entry<String, String> entry : a.b().entrySet()) {
                if (b2.a(entry.getKey())) {
                    b2.c(entry.getKey());
                }
                b2.a(entry.getKey(), entry.getValue());
            }
        }
    }

    @DexIgnore
    public static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        return str.substring(str.lastIndexOf(File.separator) + 1);
    }
}
