package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.fossil.a12;
import com.fossil.o62;
import com.fossil.v02;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c32 extends a12 implements a42 {
    @DexIgnore
    public /* final */ Lock b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ o62 d;
    @DexIgnore
    public b42 e; // = null;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ Context g;
    @DexIgnore
    public /* final */ Looper h;
    @DexIgnore
    public /* final */ Queue<r12<?, ?>> i; // = new LinkedList();
    @DexIgnore
    public volatile boolean j;
    @DexIgnore
    public long k;
    @DexIgnore
    public long l;
    @DexIgnore
    public /* final */ k32 m;
    @DexIgnore
    public /* final */ l02 n;
    @DexIgnore
    public x32 o;
    @DexIgnore
    public /* final */ Map<v02.c<?>, v02.f> p;
    @DexIgnore
    public Set<Scope> q;
    @DexIgnore
    public /* final */ j62 r;
    @DexIgnore
    public /* final */ Map<v02<?>, Boolean> s;
    @DexIgnore
    public /* final */ v02.a<? extends xn3, fn3> t;
    @DexIgnore
    public /* final */ z12 u;
    @DexIgnore
    public /* final */ ArrayList<f52> v;
    @DexIgnore
    public Integer w;
    @DexIgnore
    public Set<m42> x;
    @DexIgnore
    public /* final */ n42 y;
    @DexIgnore
    public /* final */ o62.a z;

    @DexIgnore
    public c32(Context context, Lock lock, Looper looper, j62 j62, l02 l02, v02.a<? extends xn3, fn3> aVar, Map<v02<?>, Boolean> map, List<a12.b> list, List<a12.c> list2, Map<v02.c<?>, v02.f> map2, int i2, int i3, ArrayList<f52> arrayList, boolean z2) {
        this.k = m92.a() ? ButtonService.CONNECT_TIMEOUT : 120000;
        this.l = 5000;
        this.q = new HashSet();
        this.u = new z12();
        this.w = null;
        this.x = null;
        f32 f32 = new f32(this);
        this.z = f32;
        this.g = context;
        this.b = lock;
        this.c = false;
        this.d = new o62(looper, f32);
        this.h = looper;
        this.m = new k32(this, looper);
        this.n = l02;
        this.f = i2;
        if (i2 >= 0) {
            this.w = Integer.valueOf(i3);
        }
        this.s = map;
        this.p = map2;
        this.v = arrayList;
        this.y = new n42(this.p);
        for (a12.b bVar : list) {
            this.d.a(bVar);
        }
        for (a12.c cVar : list2) {
            this.d.a(cVar);
        }
        this.r = j62;
        this.t = aVar;
    }

    @DexIgnore
    public static String c(int i2) {
        return i2 != 1 ? i2 != 2 ? i2 != 3 ? "UNKNOWN" : "SIGN_IN_MODE_NONE" : "SIGN_IN_MODE_OPTIONAL" : "SIGN_IN_MODE_REQUIRED";
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final <A extends v02.b, R extends i12, T extends r12<R, A>> T a(T t2) {
        a72.a(t2.i() != null, "This task can not be enqueued (it's probably a Batch or malformed)");
        boolean containsKey = this.p.containsKey(t2.i());
        String b2 = t2.h() != null ? t2.h().b() : "the API";
        StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(b2);
        sb.append(" required for this call.");
        a72.a(containsKey, sb.toString());
        this.b.lock();
        try {
            if (this.e == null) {
                this.i.add(t2);
                return t2;
            }
            T t3 = (T) this.e.b(t2);
            this.b.unlock();
            return t3;
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final <A extends v02.b, T extends r12<? extends i12, A>> T b(T t2) {
        a72.a(t2.i() != null, "This task can not be executed (it's probably a Batch or malformed)");
        boolean containsKey = this.p.containsKey(t2.i());
        String b2 = t2.h() != null ? t2.h().b() : "the API";
        StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(b2);
        sb.append(" required for this call.");
        a72.a(containsKey, sb.toString());
        this.b.lock();
        try {
            if (this.e == null) {
                throw new IllegalStateException("GoogleApiClient is not connected yet.");
            } else if (this.j) {
                this.i.add(t2);
                while (!this.i.isEmpty()) {
                    r12<?, ?> remove = this.i.remove();
                    this.y.a(remove);
                    remove.c(Status.g);
                }
                return t2;
            } else {
                T t3 = (T) this.e.a(t2);
                this.b.unlock();
                return t3;
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final void c() {
        this.b.lock();
        try {
            boolean z2 = false;
            if (this.f >= 0) {
                if (this.w != null) {
                    z2 = true;
                }
                a72.b(z2, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.w == null) {
                this.w = Integer.valueOf(a((Iterable<v02.f>) this.p.values(), false));
            } else if (this.w.intValue() == 2) {
                throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            a(this.w.intValue());
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final void d() {
        this.b.lock();
        try {
            this.y.a();
            if (this.e != null) {
                this.e.a();
            }
            this.u.a();
            for (r12<?, ?> r12 : this.i) {
                r12.a((r42) null);
                r12.a();
            }
            this.i.clear();
            if (this.e != null) {
                o();
                this.d.a();
                this.b.unlock();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final Context e() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final Looper f() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final boolean g() {
        b42 b42 = this.e;
        return b42 != null && b42.c();
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final void h() {
        b42 b42 = this.e;
        if (b42 != null) {
            b42.d();
        }
    }

    @DexIgnore
    public final void k() {
        d();
        c();
    }

    @DexIgnore
    public final void l() {
        this.b.lock();
        try {
            if (this.j) {
                m();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void m() {
        this.d.b();
        this.e.b();
    }

    @DexIgnore
    public final void n() {
        this.b.lock();
        try {
            if (o()) {
                m();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final boolean o() {
        if (!this.j) {
            return false;
        }
        this.j = false;
        this.m.removeMessages(2);
        this.m.removeMessages(1);
        x32 x32 = this.o;
        if (x32 != null) {
            x32.a();
            this.o = null;
        }
        return true;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final boolean p() {
        this.b.lock();
        try {
            if (this.x == null) {
                this.b.unlock();
                return false;
            }
            boolean z2 = !this.x.isEmpty();
            this.b.unlock();
            return z2;
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
    }

    @DexIgnore
    public final String q() {
        StringWriter stringWriter = new StringWriter();
        a("", (FileDescriptor) null, new PrintWriter(stringWriter), (String[]) null);
        return stringWriter.toString();
    }

    @DexIgnore
    public final void a(int i2) {
        this.b.lock();
        boolean z2 = true;
        if (!(i2 == 3 || i2 == 1 || i2 == 2)) {
            z2 = false;
        }
        try {
            StringBuilder sb = new StringBuilder(33);
            sb.append("Illegal sign-in mode: ");
            sb.append(i2);
            a72.a(z2, sb.toString());
            b(i2);
            m();
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final c12<Status> b() {
        a72.b(g(), "GoogleApiClient is not connected yet.");
        a72.b(this.w.intValue() != 2, "Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API");
        e22 e22 = new e22(this);
        if (this.p.containsKey(l72.a)) {
            a(this, e22, false);
        } else {
            AtomicReference atomicReference = new AtomicReference();
            e32 e32 = new e32(this, atomicReference, e22);
            h32 h32 = new h32(this, e22);
            a12.a aVar = new a12.a(this.g);
            aVar.a(l72.c);
            aVar.a(e32);
            aVar.a(h32);
            aVar.a(this.m);
            a12 a = aVar.a();
            atomicReference.set(a);
            a.c();
        }
        return e22;
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final i02 a() {
        boolean z2 = true;
        a72.b(Looper.myLooper() != Looper.getMainLooper(), "blockingConnect must not be called on the UI thread");
        this.b.lock();
        try {
            if (this.f >= 0) {
                if (this.w == null) {
                    z2 = false;
                }
                a72.b(z2, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.w == null) {
                this.w = Integer.valueOf(a((Iterable<v02.f>) this.p.values(), false));
            } else if (this.w.intValue() == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            b(this.w.intValue());
            this.d.b();
            return this.e.f();
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void a(a12 a12, e22 e22, boolean z2) {
        l72.d.a(a12).a(new g32(this, e22, z2, a12));
    }

    @DexIgnore
    public final void b(int i2) {
        Integer num = this.w;
        if (num == null) {
            this.w = Integer.valueOf(i2);
        } else if (num.intValue() != i2) {
            String c2 = c(i2);
            String c3 = c(this.w.intValue());
            StringBuilder sb = new StringBuilder(String.valueOf(c2).length() + 51 + String.valueOf(c3).length());
            sb.append("Cannot use sign-in mode: ");
            sb.append(c2);
            sb.append(". Mode was already set to ");
            sb.append(c3);
            throw new IllegalStateException(sb.toString());
        }
        if (this.e == null) {
            boolean z2 = false;
            boolean z3 = false;
            for (v02.f fVar : this.p.values()) {
                if (fVar.n()) {
                    z2 = true;
                }
                if (fVar.d()) {
                    z3 = true;
                }
            }
            int intValue = this.w.intValue();
            if (intValue != 1) {
                if (intValue == 2 && z2) {
                    if (this.c) {
                        this.e = new m52(this.g, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this, true);
                        return;
                    } else {
                        this.e = h52.a(this.g, this, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v);
                        return;
                    }
                }
            } else if (!z2) {
                throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
            } else if (z3) {
                throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            if (!this.c || z3) {
                this.e = new l32(this.g, this, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this);
            } else {
                this.e = new m52(this.g, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this, false);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final void a(a12.c cVar) {
        this.d.a(cVar);
    }

    @DexIgnore
    @Override // com.fossil.a42
    public final void a(Bundle bundle) {
        while (!this.i.isEmpty()) {
            b(this.i.remove());
        }
        this.d.a(bundle);
    }

    @DexIgnore
    @Override // com.fossil.a42
    public final void a(i02 i02) {
        if (!this.n.b(this.g, i02.e())) {
            o();
        }
        if (!this.j) {
            this.d.a(i02);
            this.d.a();
        }
    }

    @DexIgnore
    @Override // com.fossil.a42
    public final void a(int i2, boolean z2) {
        if (i2 == 1 && !z2 && !this.j) {
            this.j = true;
            if (this.o == null && !m92.a()) {
                try {
                    this.o = this.n.a(this.g.getApplicationContext(), new j32(this));
                } catch (SecurityException unused) {
                }
            }
            k32 k32 = this.m;
            k32.sendMessageDelayed(k32.obtainMessage(1), this.k);
            k32 k322 = this.m;
            k322.sendMessageDelayed(k322.obtainMessage(2), this.l);
        }
        this.y.b();
        this.d.a(i2);
        this.d.a();
        if (i2 == 2) {
            m();
        }
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final void b(a12.c cVar) {
        this.d.b(cVar);
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final boolean a(c22 c22) {
        b42 b42 = this.e;
        return b42 != null && b42.a(c22);
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final void a(m42 m42) {
        this.b.lock();
        try {
            if (this.x == null) {
                Log.wtf("GoogleApiClientImpl", "Attempted to remove pending transform when no transforms are registered.", new Exception());
            } else if (!this.x.remove(m42)) {
                Log.wtf("GoogleApiClientImpl", "Failed to remove pending transform - this may lead to memory leaks!", new Exception());
            } else if (!p()) {
                this.e.e();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append((CharSequence) str).append("mContext=").println(this.g);
        printWriter.append((CharSequence) str).append("mResuming=").print(this.j);
        printWriter.append(" mWorkQueue.size()=").print(this.i.size());
        printWriter.append(" mUnconsumedApiCalls.size()=").println(this.y.a.size());
        b42 b42 = this.e;
        if (b42 != null) {
            b42.a(str, fileDescriptor, printWriter, strArr);
        }
    }

    @DexIgnore
    public static int a(Iterable<v02.f> iterable, boolean z2) {
        boolean z3 = false;
        boolean z4 = false;
        for (v02.f fVar : iterable) {
            if (fVar.n()) {
                z3 = true;
            }
            if (fVar.d()) {
                z4 = true;
            }
        }
        if (!z3) {
            return 3;
        }
        if (!z4 || !z2) {
            return 1;
        }
        return 2;
    }
}
