package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ut0 implements Parcelable.Creator<ov0> {
    @DexIgnore
    public /* synthetic */ ut0(zd7 zd7) {
    }

    @DexIgnore
    public final ov0 a(byte[] bArr) throws IllegalArgumentException {
        if (bArr.length == 3) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            yr0 a = yr0.e.a(order.get(0));
            if (a != null) {
                return new ov0(a, order.getShort(1));
            }
            StringBuilder b = yh0.b("Invalid action: ");
            b.append((int) order.get(0));
            b.append('.');
            throw new IllegalArgumentException(b.toString());
        }
        throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data length ("), bArr.length, "), ", "require 3."));
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public ov0 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            ee7.a((Object) readString, "parcel.readString()!!");
            return new ov0(yr0.valueOf(readString), (short) parcel.readInt());
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public ov0[] newArray(int i) {
        return new ov0[i];
    }
}
