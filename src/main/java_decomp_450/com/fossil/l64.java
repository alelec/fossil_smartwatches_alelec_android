package com.fossil;

import com.fossil.go7;
import com.fossil.io7;
import com.fossil.lo7;
import com.fossil.pn7;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l64 {
    @DexIgnore
    public static /* final */ OkHttpClient f;
    @DexIgnore
    public /* final */ k64 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Map<String, String> c;
    @DexIgnore
    public /* final */ Map<String, String> d;
    @DexIgnore
    public io7.a e; // = null;

    /*
    static {
        OkHttpClient.b v = new OkHttpClient().v();
        v.a(ButtonService.CONNECT_TIMEOUT, TimeUnit.MILLISECONDS);
        f = v.a();
    }
    */

    @DexIgnore
    public l64(k64 k64, String str, Map<String, String> map) {
        this.a = k64;
        this.b = str;
        this.c = map;
        this.d = new HashMap();
    }

    @DexIgnore
    public l64 a(String str, String str2) {
        this.d.put(str, str2);
        return this;
    }

    @DexIgnore
    public l64 b(String str, String str2) {
        io7.a c2 = c();
        c2.a(str, str2);
        this.e = c2;
        return this;
    }

    @DexIgnore
    public final io7.a c() {
        if (this.e == null) {
            io7.a aVar = new io7.a();
            aVar.a(io7.f);
            this.e = aVar;
        }
        return this.e;
    }

    @DexIgnore
    public String d() {
        return this.a.name();
    }

    @DexIgnore
    public l64 a(Map.Entry<String, String> entry) {
        a(entry.getKey(), entry.getValue());
        return this;
    }

    @DexIgnore
    public n64 b() throws IOException {
        return n64.a(f.a(a()).a());
    }

    @DexIgnore
    public l64 a(String str, String str2, String str3, File file) {
        RequestBody a2 = RequestBody.a(ho7.b(str3), file);
        io7.a c2 = c();
        c2.a(str, str2, a2);
        this.e = c2;
        return this;
    }

    @DexIgnore
    public final lo7 a() {
        lo7.a aVar = new lo7.a();
        pn7.a aVar2 = new pn7.a();
        aVar2.b();
        aVar.a(aVar2.a());
        go7.a i = go7.e(this.b).i();
        for (Map.Entry<String, String> entry : this.c.entrySet()) {
            i.a(entry.getKey(), entry.getValue());
        }
        aVar.a(i.a());
        for (Map.Entry<String, String> entry2 : this.d.entrySet()) {
            aVar.b(entry2.getKey(), entry2.getValue());
        }
        io7.a aVar3 = this.e;
        aVar.a(this.a.name(), aVar3 == null ? null : aVar3.a());
        return aVar.a();
    }
}
