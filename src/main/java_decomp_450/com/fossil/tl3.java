package com.fossil;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PersistableBundle;
import com.misfit.frameworks.buttonservice.model.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tl3 extends yl3 {
    @DexIgnore
    public /* final */ AlarmManager d; // = ((AlarmManager) f().getSystemService(Alarm.TABLE_NAME));
    @DexIgnore
    public /* final */ mb3 e;
    @DexIgnore
    public Integer f;

    @DexIgnore
    public tl3(xl3 xl3) {
        super(xl3);
        this.e = new wl3(this, xl3.u(), xl3);
    }

    @DexIgnore
    public final void a(long j) {
        q();
        b();
        Context f2 = f();
        if (!gh3.a(f2)) {
            e().A().a("Receiver not registered/enabled");
        }
        if (!jm3.a(f2, false)) {
            e().A().a("Service not registered/enabled");
        }
        t();
        e().B().a("Scheduling upload, millis", Long.valueOf(j));
        long c = zzm().c() + j;
        if (j < Math.max(0L, wb3.x.a(null).longValue()) && !this.e.b()) {
            this.e.a(j);
        }
        b();
        if (Build.VERSION.SDK_INT >= 24) {
            Context f3 = f();
            ComponentName componentName = new ComponentName(f3, "com.google.android.gms.measurement.AppMeasurementJobService");
            int v = v();
            PersistableBundle persistableBundle = new PersistableBundle();
            persistableBundle.putString("action", "com.google.android.gms.measurement.UPLOAD");
            av2.a(f3, new JobInfo.Builder(v, componentName).setMinimumLatency(j).setOverrideDeadline(j << 1).setExtras(persistableBundle).build(), "com.google.android.gms", "UploadAlarm");
            return;
        }
        this.d.setInexactRepeating(2, c, Math.max(wb3.s.a(null).longValue(), j), w());
    }

    @DexIgnore
    @Override // com.fossil.yl3
    public final boolean s() {
        this.d.cancel(w());
        if (Build.VERSION.SDK_INT < 24) {
            return false;
        }
        u();
        return false;
    }

    @DexIgnore
    public final void t() {
        q();
        e().B().a("Unscheduling upload");
        this.d.cancel(w());
        this.e.c();
        if (Build.VERSION.SDK_INT >= 24) {
            u();
        }
    }

    @DexIgnore
    @TargetApi(24)
    public final void u() {
        ((JobScheduler) f().getSystemService("jobscheduler")).cancel(v());
    }

    @DexIgnore
    public final int v() {
        if (this.f == null) {
            String valueOf = String.valueOf(f().getPackageName());
            this.f = Integer.valueOf((valueOf.length() != 0 ? "measurement".concat(valueOf) : new String("measurement")).hashCode());
        }
        return this.f.intValue();
    }

    @DexIgnore
    public final PendingIntent w() {
        Context f2 = f();
        return PendingIntent.getBroadcast(f2, 0, new Intent().setClassName(f2, "com.google.android.gms.measurement.AppMeasurementReceiver").setAction("com.google.android.gms.measurement.UPLOAD"), 0);
    }
}
