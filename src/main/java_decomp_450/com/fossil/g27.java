package com.fossil;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g27 extends h27 {
    @DexIgnore
    public g27(Context context) {
        super(context);
    }

    @DexIgnore
    @Override // com.fossil.h27
    public final void a(String str) {
        synchronized (this) {
            Log.i("MID", "write mid to Settings.System");
            Settings.System.putString(((h27) this).a.getContentResolver(), j27.c("4kU71lN96TJUomD1vOU9lgj9Tw=="), str);
        }
    }

    @DexIgnore
    @Override // com.fossil.h27
    public final boolean a() {
        return j27.a(((h27) this).a, "android.permission.WRITE_SETTINGS");
    }

    @DexIgnore
    @Override // com.fossil.h27
    public final String b() {
        String string;
        synchronized (this) {
            Log.i("MID", "read mid from Settings.System");
            string = Settings.System.getString(((h27) this).a.getContentResolver(), j27.c("4kU71lN96TJUomD1vOU9lgj9Tw=="));
        }
        return string;
    }
}
