package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lw6 implements Factory<kw6> {
    @DexIgnore
    public /* final */ Provider<nw5> a;
    @DexIgnore
    public /* final */ Provider<vu5> b;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> c;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> d;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> e;
    @DexIgnore
    public /* final */ Provider<ch5> f;

    @DexIgnore
    public lw6(Provider<nw5> provider, Provider<vu5> provider2, Provider<NotificationSettingsDatabase> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceRepository> provider5, Provider<ch5> provider6) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
    }

    @DexIgnore
    public static lw6 a(Provider<nw5> provider, Provider<vu5> provider2, Provider<NotificationSettingsDatabase> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceRepository> provider5, Provider<ch5> provider6) {
        return new lw6(provider, provider2, provider3, provider4, provider5, provider6);
    }

    @DexIgnore
    public static kw6 a(nw5 nw5, vu5 vu5, NotificationSettingsDatabase notificationSettingsDatabase, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, ch5 ch5) {
        return new kw6(nw5, vu5, notificationSettingsDatabase, notificationsRepository, deviceRepository, ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public kw6 get() {
        return a(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get());
    }
}
