package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nw6 extends fl4<b, d, c> {
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e;
    @DexIgnore
    public /* final */ DeviceRepository f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ aw6 h;
    @DexIgnore
    public /* final */ PortfolioApp i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            ee7.b(str, "deviceId");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i, String str) {
            ee7.b(str, "errorMesagge");
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.usecase.VerifySecretKeyUseCase", f = "VerifySecretKeyUseCase.kt", l = {74}, m = "getLocalSecretKey")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ nw6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(nw6 nw6, fb7 fb7) {
            super(fb7);
            this.this$0 = nw6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.usecase.VerifySecretKeyUseCase", f = "VerifySecretKeyUseCase.kt", l = {90}, m = "getServerSecretKey")
    public static final class f extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ nw6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(nw6 nw6, fb7 fb7) {
            super(fb7);
            this.this$0 = nw6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.usecase.VerifySecretKeyUseCase", f = "VerifySecretKeyUseCase.kt", l = {33, 37, 38}, m = "run")
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ nw6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(nw6 nw6, fb7 fb7) {
            super(fb7);
            this.this$0 = nw6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public nw6(DeviceRepository deviceRepository, UserRepository userRepository, aw6 aw6, PortfolioApp portfolioApp) {
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(aw6, "mDecryptValueKeyStoreUseCase");
        ee7.b(portfolioApp, "mApp");
        this.f = deviceRepository;
        this.g = userRepository;
        this.h = aw6;
        this.i = portfolioApp;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object b(com.fossil.fb7<? super java.lang.String> r5) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof com.fossil.nw6.f
            if (r0 == 0) goto L_0x0013
            r0 = r5
            com.fossil.nw6$f r0 = (com.fossil.nw6.f) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.nw6$f r0 = new com.fossil.nw6$f
            r0.<init>(r4, r5)
        L_0x0018:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.fossil.nw6 r0 = (com.fossil.nw6) r0
            com.fossil.t87.a(r5)
            goto L_0x0047
        L_0x002d:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r0)
            throw r5
        L_0x0035:
            com.fossil.t87.a(r5)
            com.portfolio.platform.data.source.DeviceRepository r5 = r4.f
            java.lang.String r2 = r4.d
            r0.L$0 = r4
            r0.label = r3
            java.lang.Object r5 = r5.getDeviceSecretKey(r2, r0)
            if (r5 != r1) goto L_0x0047
            return r1
        L_0x0047:
            com.fossil.zi5 r5 = (com.fossil.zi5) r5
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "getServerSecretKey "
            r1.append(r2)
            r1.append(r5)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "VerifySecretKeyUseCase"
            r0.d(r2, r1)
            boolean r0 = r5 instanceof com.fossil.bj5
            r1 = 0
            if (r0 == 0) goto L_0x007a
            com.fossil.bj5 r5 = (com.fossil.bj5) r5
            java.lang.Object r5 = r5.a()
            if (r5 == 0) goto L_0x0076
            r1 = r5
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x007e
        L_0x0076:
            com.fossil.ee7.a()
            throw r1
        L_0x007a:
            boolean r5 = r5 instanceof com.fossil.yi5
            if (r5 == 0) goto L_0x007f
        L_0x007e:
            return r1
        L_0x007f:
            com.fossil.p87 r5 = new com.fossil.p87
            r5.<init>()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nw6.b(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "VerifySecretKeyUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00da A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.nw6.b r11, com.fossil.fb7<java.lang.Object> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.fossil.nw6.g
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.fossil.nw6$g r0 = (com.fossil.nw6.g) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.nw6$g r0 = new com.fossil.nw6$g
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = ""
            r4 = 3
            r5 = 2
            r6 = 1
            if (r2 == 0) goto L_0x0072
            if (r2 == r6) goto L_0x0066
            if (r2 == r5) goto L_0x004e
            if (r2 != r4) goto L_0x0046
            java.lang.Object r11 = r0.L$4
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r1 = r0.L$3
            com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
            java.lang.Object r1 = r0.L$2
            com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
            java.lang.Object r1 = r0.L$1
            com.fossil.nw6$b r1 = (com.fossil.nw6.b) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.nw6 r0 = (com.fossil.nw6) r0
            com.fossil.t87.a(r12)
            goto L_0x00df
        L_0x0046:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x004e:
            java.lang.Object r11 = r0.L$3
            com.portfolio.platform.data.model.MFUser r11 = (com.portfolio.platform.data.model.MFUser) r11
            java.lang.Object r2 = r0.L$2
            com.portfolio.platform.data.model.MFUser r2 = (com.portfolio.platform.data.model.MFUser) r2
            java.lang.Object r5 = r0.L$1
            com.fossil.nw6$b r5 = (com.fossil.nw6.b) r5
            java.lang.Object r7 = r0.L$0
            com.fossil.nw6 r7 = (com.fossil.nw6) r7
            com.fossil.t87.a(r12)
            r9 = r5
            r5 = r2
            r2 = r7
            r7 = r9
            goto L_0x00c6
        L_0x0066:
            java.lang.Object r11 = r0.L$1
            com.fossil.nw6$b r11 = (com.fossil.nw6.b) r11
            java.lang.Object r2 = r0.L$0
            com.fossil.nw6 r2 = (com.fossil.nw6) r2
            com.fossil.t87.a(r12)
            goto L_0x008d
        L_0x0072:
            com.fossil.t87.a(r12)
            if (r11 == 0) goto L_0x0156
            java.lang.String r12 = r11.a()
            r10.d = r12
            com.portfolio.platform.data.source.UserRepository r12 = r10.g
            r0.L$0 = r10
            r0.L$1 = r11
            r0.label = r6
            java.lang.Object r12 = r12.getCurrentUser(r0)
            if (r12 != r1) goto L_0x008c
            return r1
        L_0x008c:
            r2 = r10
        L_0x008d:
            com.portfolio.platform.data.model.MFUser r12 = (com.portfolio.platform.data.model.MFUser) r12
            if (r12 == 0) goto L_0x0146
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = r12.getUserId()
            r7.append(r8)
            r8 = 58
            r7.append(r8)
            com.portfolio.platform.PortfolioApp r8 = r2.i
            java.lang.String r8 = r8.c()
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r2.e = r7
            r0.L$0 = r2
            r0.L$1 = r11
            r0.L$2 = r12
            r0.L$3 = r12
            r0.label = r5
            java.lang.Object r5 = r2.b(r0)
            if (r5 != r1) goto L_0x00c2
            return r1
        L_0x00c2:
            r7 = r11
            r11 = r12
            r12 = r5
            r5 = r11
        L_0x00c6:
            java.lang.String r12 = (java.lang.String) r12
            r0.L$0 = r2
            r0.L$1 = r7
            r0.L$2 = r5
            r0.L$3 = r11
            r0.L$4 = r12
            r0.label = r4
            java.lang.Object r11 = r2.a(r0)
            if (r11 != r1) goto L_0x00db
            return r1
        L_0x00db:
            r0 = r2
            r9 = r12
            r12 = r11
            r11 = r9
        L_0x00df:
            java.lang.String r12 = (java.lang.String) r12
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "localSecretKey "
            r2.append(r4)
            r2.append(r12)
            java.lang.String r4 = " serverSecretKey "
            r2.append(r4)
            r2.append(r11)
            java.lang.String r2 = r2.toString()
            java.lang.String r4 = "VerifySecretKeyUseCase"
            r1.d(r4, r2)
            if (r11 != 0) goto L_0x0108
            goto L_0x011e
        L_0x0108:
            int r1 = r11.hashCode()
            if (r1 == 0) goto L_0x010f
            goto L_0x0116
        L_0x010f:
            boolean r1 = r11.equals(r3)
            if (r1 == 0) goto L_0x0116
            goto L_0x011e
        L_0x0116:
            boolean r1 = com.fossil.ee7.a(r12, r11)
            r1 = r1 ^ r6
            if (r1 == 0) goto L_0x011e
            goto L_0x011f
        L_0x011e:
            r11 = r12
        L_0x011f:
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "secret key "
            r1.append(r2)
            r1.append(r11)
            java.lang.String r1 = r1.toString()
            r12.d(r4, r1)
            com.fossil.nw6$d r12 = new com.fossil.nw6$d
            r12.<init>(r11)
            com.fossil.ik7 r11 = r0.a(r12)
            if (r11 == 0) goto L_0x0145
            goto L_0x0150
        L_0x0145:
            r2 = r0
        L_0x0146:
            com.fossil.nw6$c r11 = new com.fossil.nw6$c
            r12 = 600(0x258, float:8.41E-43)
            r11.<init>(r12, r3)
            r2.a(r11)
        L_0x0150:
            java.lang.Object r11 = new java.lang.Object
            r11.<init>()
            return r11
        L_0x0156:
            com.fossil.ee7.a()
            r11 = 0
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nw6.a(com.fossil.nw6$b, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.fb7<? super java.lang.String> r9) {
        /*
            r8 = this;
            boolean r0 = r9 instanceof com.fossil.nw6.e
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.nw6$e r0 = (com.fossil.nw6.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.nw6$e r0 = new com.fossil.nw6$e
            r0.<init>(r8, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = "VerifySecretKeyUseCase"
            r4 = 1
            r5 = 0
            if (r2 == 0) goto L_0x0038
            if (r2 != r4) goto L_0x0030
            java.lang.Object r0 = r0.L$0
            com.fossil.nw6 r0 = (com.fossil.nw6) r0
            com.fossil.t87.a(r9)
            goto L_0x0061
        L_0x0030:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r0)
            throw r9
        L_0x0038:
            com.fossil.t87.a(r9)
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r2 = "getLocalSecretKey"
            r9.d(r3, r2)
            com.fossil.aw6 r9 = r8.h
            com.fossil.aw6$b r2 = new com.fossil.aw6$b
            java.lang.String r6 = r8.e
            if (r6 == 0) goto L_0x008f
            com.fossil.pb5 r7 = new com.fossil.pb5
            r7.<init>()
            r2.<init>(r6, r7)
            r0.L$0 = r8
            r0.label = r4
            java.lang.Object r9 = com.fossil.gl4.a(r9, r2, r0)
            if (r9 != r1) goto L_0x0061
            return r1
        L_0x0061:
            com.fossil.fl4$c r9 = (com.fossil.fl4.c) r9
            boolean r0 = r9 instanceof com.fossil.aw6.d
            if (r0 == 0) goto L_0x008c
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Get local key success "
            r1.append(r2)
            com.fossil.aw6$d r9 = (com.fossil.aw6.d) r9
            java.lang.String r2 = r9.a()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.d(r3, r1)
            java.lang.String r5 = r9.a()
            goto L_0x008e
        L_0x008c:
            boolean r9 = r9 instanceof com.fossil.aw6.c
        L_0x008e:
            return r5
        L_0x008f:
            com.fossil.ee7.a()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nw6.a(com.fossil.fb7):java.lang.Object");
    }
}
