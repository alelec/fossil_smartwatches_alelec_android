package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k11 extends qj1 {
    @DexIgnore
    public long A;
    @DexIgnore
    public p91 B; // = p91.DISCONNECTED;

    @DexIgnore
    public k11(ri1 ri1, long j) {
        super(qa1.J, ri1);
        this.A = j;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(long j) {
        this.A = j;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(pm1 pm1) {
    }

    @DexIgnore
    @Override // com.fossil.v81
    public long e() {
        return this.A;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(yz0.a(super.g(), r51.n1, yz0.a(((v81) this).y.e())), r51.i0, ((v81) this).y.u);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), r51.o1, yz0.a(this.B));
    }

    @DexIgnore
    @Override // com.fossil.yf1
    public eo0 k() {
        return new ex0(((v81) this).y.w);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(eo0 eo0) {
        this.B = ((ex0) eo0).k;
        ((v81) this).g.add(new zq0(0, null, null, yz0.a(new JSONObject(), r51.o1, yz0.a(this.B)), 7));
    }
}
