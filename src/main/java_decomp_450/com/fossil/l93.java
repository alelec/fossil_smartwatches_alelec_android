package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l93 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<l93> CREATOR; // = new da3();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ Float b;

    @DexIgnore
    public l93(int i, Float f) {
        boolean z = true;
        if (i != 1 && (f == null || f.floatValue() < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
            z = false;
        }
        String valueOf = String.valueOf(f);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 45);
        sb.append("Invalid PatternItem: type=");
        sb.append(i);
        sb.append(" length=");
        sb.append(valueOf);
        a72.a(z, sb.toString());
        this.a = i;
        this.b = f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof l93)) {
            return false;
        }
        l93 l93 = (l93) obj;
        return this.a == l93.a && y62.a(this.b, l93.b);
    }

    @DexIgnore
    public int hashCode() {
        return y62.a(Integer.valueOf(this.a), this.b);
    }

    @DexIgnore
    public String toString() {
        int i = this.a;
        String valueOf = String.valueOf(this.b);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 39);
        sb.append("[PatternItem: type=");
        sb.append(i);
        sb.append(" length=");
        sb.append(valueOf);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, this.a);
        k72.a(parcel, 3, this.b, false);
        k72.a(parcel, a2);
    }
}
