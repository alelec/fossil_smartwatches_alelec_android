package com.fossil;

import com.fossil.jc4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mc4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(long j);

        @DexIgnore
        public abstract a a(b bVar);

        @DexIgnore
        public abstract a a(String str);

        @DexIgnore
        public abstract mc4 a();
    }

    @DexIgnore
    public enum b {
        OK,
        BAD_CONFIG,
        AUTH_ERROR
    }

    @DexIgnore
    public static a d() {
        jc4.b bVar = new jc4.b();
        bVar.a(0);
        return bVar;
    }

    @DexIgnore
    public abstract b a();

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public abstract long c();
}
