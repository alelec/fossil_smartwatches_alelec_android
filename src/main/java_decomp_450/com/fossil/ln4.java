package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ln4 {
    @DexIgnore
    @te4("profileID")
    public String a;
    @DexIgnore
    @te4("status")
    public String b;

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ln4)) {
            return false;
        }
        ln4 ln4 = (ln4) obj;
        return ee7.a(this.a, ln4.a) && ee7.a(this.b, ln4.b);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        return "BlockResult(id=" + this.a + ", status=" + this.b + ")";
    }
}
