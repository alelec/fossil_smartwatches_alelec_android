package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface gl7 {
    @DexIgnore
    long a();

    @DexIgnore
    Runnable a(Runnable runnable);

    @DexIgnore
    void a(Object obj, long j);

    @DexIgnore
    void a(Thread thread);

    @DexIgnore
    void b();

    @DexIgnore
    void c();

    @DexIgnore
    void d();

    @DexIgnore
    void e();
}
