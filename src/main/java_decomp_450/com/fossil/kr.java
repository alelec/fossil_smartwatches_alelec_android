package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.fossil.pr;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kr implements pr<Bitmap> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public kr(Context context) {
        ee7.b(context, "context");
        this.a = context;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.rq, java.lang.Object, com.fossil.rt, com.fossil.ir, com.fossil.fb7] */
    @Override // com.fossil.pr
    public /* bridge */ /* synthetic */ Object a(rq rqVar, Bitmap bitmap, rt rtVar, ir irVar, fb7 fb7) {
        return a(rqVar, bitmap, rtVar, irVar, (fb7<? super or>) fb7);
    }

    @DexIgnore
    public String b(Bitmap bitmap) {
        ee7.b(bitmap, "data");
        return null;
    }

    @DexIgnore
    public boolean a(Bitmap bitmap) {
        ee7.b(bitmap, "data");
        return pr.a.a(this, bitmap);
    }

    @DexIgnore
    public Object a(rq rqVar, Bitmap bitmap, rt rtVar, ir irVar, fb7<? super or> fb7) {
        Resources resources = this.a.getResources();
        ee7.a((Object) resources, "context.resources");
        return new nr(new BitmapDrawable(resources, bitmap), false, br.MEMORY);
    }
}
