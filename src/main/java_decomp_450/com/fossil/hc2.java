package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hc2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<hc2> CREATOR; // = new bd2();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public hc2(String str, String str2, String str3, int i) {
        this(str, str2, str3, i, 0);
    }

    @DexIgnore
    public final String e() {
        return this.a;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof hc2)) {
            return false;
        }
        hc2 hc2 = (hc2) obj;
        return y62.a(this.a, hc2.a) && y62.a(this.b, hc2.b) && y62.a(this.c, hc2.c) && this.d == hc2.d && this.e == hc2.e;
    }

    @DexIgnore
    public final String g() {
        return this.b;
    }

    @DexIgnore
    public final int hashCode() {
        return y62.a(this.a, this.b, this.c, Integer.valueOf(this.d));
    }

    @DexIgnore
    public final String toString() {
        return String.format("Device{%s:%s:%s}", v(), Integer.valueOf(this.d), Integer.valueOf(this.e));
    }

    @DexIgnore
    public final String v() {
        return String.format("%s:%s:%s", this.a, this.b, this.c);
    }

    @DexIgnore
    public final int w() {
        return this.d;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, e(), false);
        k72.a(parcel, 2, g(), false);
        k72.a(parcel, 4, x(), false);
        k72.a(parcel, 5, w());
        k72.a(parcel, 6, this.e);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public final String x() {
        return this.c;
    }

    @DexIgnore
    public hc2(String str, String str2, String str3, int i, int i2) {
        a72.a((Object) str);
        this.a = str;
        a72.a((Object) str2);
        this.b = str2;
        if (str3 != null) {
            this.c = str3;
            this.d = i;
            this.e = i2;
            return;
        }
        throw new IllegalStateException("Device UID is null.");
    }
}
