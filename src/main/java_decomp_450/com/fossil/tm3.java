package com.fossil;

import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import android.util.Pair;
import com.fossil.rp2;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tm3 {
    @DexIgnore
    public rp2 a;
    @DexIgnore
    public Long b;
    @DexIgnore
    public long c;
    @DexIgnore
    public /* final */ /* synthetic */ om3 d;

    @DexIgnore
    public tm3(om3 om3) {
        this.d = om3;
    }

    @DexIgnore
    public final rp2 a(String str, rp2 rp2) {
        Object obj;
        String q = rp2.q();
        List<tp2> zza = rp2.zza();
        Long l = (Long) this.d.m().a(rp2, "_eid");
        boolean z = l != null;
        if (z && q.equals("_ep")) {
            q = (String) this.d.m().a(rp2, "_en");
            if (TextUtils.isEmpty(q)) {
                this.d.e().u().a("Extra parameter without an event name. eventId", l);
                return null;
            }
            if (this.a == null || this.b == null || l.longValue() != this.b.longValue()) {
                Pair<rp2, Long> a2 = this.d.n().a(str, l);
                if (a2 == null || (obj = a2.first) == null) {
                    this.d.e().u().a("Extra parameter without existing main event. eventName, eventId", q, l);
                    return null;
                }
                this.a = (rp2) obj;
                this.c = ((Long) a2.second).longValue();
                this.b = (Long) this.d.m().a(this.a, "_eid");
            }
            long j = this.c - 1;
            this.c = j;
            if (j <= 0) {
                jb3 n = this.d.n();
                n.g();
                n.e().B().a("Clearing complex main event info. appId", str);
                try {
                    n.u().execSQL("delete from main_event_params where app_id=?", new String[]{str});
                } catch (SQLiteException e) {
                    n.e().t().a("Error clearing complex main event", e);
                }
            } else {
                this.d.n().a(str, l, this.c, this.a);
            }
            ArrayList arrayList = new ArrayList();
            for (tp2 tp2 : this.a.zza()) {
                this.d.m();
                if (fm3.b(rp2, tp2.p()) == null) {
                    arrayList.add(tp2);
                }
            }
            if (!arrayList.isEmpty()) {
                arrayList.addAll(zza);
                zza = arrayList;
            } else {
                this.d.e().u().a("No unique parameters in main event. eventName", q);
            }
        } else if (z) {
            this.b = l;
            this.a = rp2;
            long j2 = 0L;
            Object a3 = this.d.m().a(rp2, "_epc");
            if (a3 != null) {
                j2 = a3;
            }
            long longValue = j2.longValue();
            this.c = longValue;
            if (longValue <= 0) {
                this.d.e().u().a("Complex event with zero extra param count. eventName", q);
            } else {
                this.d.n().a(str, l, this.c, rp2);
            }
        }
        rp2.a aVar = (rp2.a) rp2.l();
        aVar.a(q);
        aVar.p();
        aVar.a(zza);
        return (rp2) ((bw2) aVar.g());
    }

    @DexIgnore
    public /* synthetic */ tm3(om3 om3, rm3 rm3) {
        this(om3);
    }
}
