package com.fossil;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zj4 implements Factory<ApiServiceV2> {
    @DexIgnore
    public /* final */ wj4 a;
    @DexIgnore
    public /* final */ Provider<hj5> b;
    @DexIgnore
    public /* final */ Provider<lj5> c;

    @DexIgnore
    public zj4(wj4 wj4, Provider<hj5> provider, Provider<lj5> provider2) {
        this.a = wj4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static zj4 a(wj4 wj4, Provider<hj5> provider, Provider<lj5> provider2) {
        return new zj4(wj4, provider, provider2);
    }

    @DexIgnore
    public static ApiServiceV2 a(wj4 wj4, hj5 hj5, lj5 lj5) {
        ApiServiceV2 a2 = wj4.a(hj5, lj5);
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ApiServiceV2 get() {
        return a(this.a, this.b.get(), this.c.get());
    }
}
