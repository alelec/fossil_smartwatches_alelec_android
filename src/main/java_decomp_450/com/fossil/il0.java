package com.fossil;

import com.fossil.l60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class il0 implements jb1 {
    @DexIgnore
    public /* final */ /* synthetic */ km1 a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public il0(km1 km1) {
        this.a = km1;
    }

    @DexIgnore
    @Override // com.fossil.jb1
    public void a(ri1 ri1, dd1 dd1) {
        zk0 zk0;
        if (kz0.a[dd1.ordinal()] == 1) {
            this.a.b(false);
        }
        if (km1.a(this.a)) {
            this.a.x();
        }
        zk0[] c = this.a.f.c();
        int length = c.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                zk0 = null;
                break;
            }
            zk0 = c[i];
            wm0 wm0 = zk0.y;
            if (wm0 == wm0.g || wm0 == wm0.b) {
                break;
            }
            i++;
        }
        if (zk0 == null) {
            int i2 = kz0.b[dd1.ordinal()];
            if (i2 == 1) {
                this.a.a(l60.c.DISCONNECTED);
            } else if (i2 == 2) {
                this.a.a(l60.c.CONNECTING);
            } else if (i2 == 4) {
                this.a.a(l60.c.DISCONNECTING);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.jb1
    public void a(ri1 ri1, p91 p91, p91 p912) {
        if (ee7.a(ri1, this.a.c)) {
            this.a.a(l60.d.f.a(p91), l60.d.f.a(p912));
        }
    }
}
