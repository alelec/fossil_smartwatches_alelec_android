package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cc0 extends ac0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<cc0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public cc0 createFromParcel(Parcel parcel) {
            return new cc0(parcel, (zd7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public cc0[] newArray(int i) {
            return new cc0[i];
        }
    }

    @DexIgnore
    public cc0(byte b, rg0 rg0) {
        super(cb0.RING_MY_PHONE_MICRO_APP, b, rg0);
    }

    @DexIgnore
    public /* synthetic */ cc0(Parcel parcel, zd7 zd7) {
        super(parcel);
    }
}
