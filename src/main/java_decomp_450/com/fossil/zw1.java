package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.rx1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class zw1 implements rx1.b {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ pu1 b;

    @DexIgnore
    public zw1(long j, pu1 pu1) {
        this.a = j;
        this.b = pu1;
    }

    @DexIgnore
    public static rx1.b a(long j, pu1 pu1) {
        return new zw1(j, pu1);
    }

    @DexIgnore
    @Override // com.fossil.rx1.b
    public Object apply(Object obj) {
        return rx1.a(this.a, this.b, (SQLiteDatabase) obj);
    }
}
