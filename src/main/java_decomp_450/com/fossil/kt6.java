package com.fossil;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.cy6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kt6 extends go5 implements jt6, View.OnClickListener, TextWatcher, View.OnKeyListener, cy6.g {
    @DexIgnore
    public static /* final */ a v; // = new a(null);
    @DexIgnore
    public it6 f;
    @DexIgnore
    public qw6<a15> g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public boolean j; // = true;
    @DexIgnore
    public ScaleAnimation p; // = new ScaleAnimation(1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public ScaleAnimation q; // = new ScaleAnimation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public ScaleAnimation r; // = new ScaleAnimation(1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public ScaleAnimation s; // = new ScaleAnimation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public TranslateAnimation t;
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final kt6 a() {
            return new kt6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ kt6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(kt6 kt6) {
            this.a = kt6;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            a15 g1 = this.a.g1();
            if (g1 != null) {
                FlexibleTextView flexibleTextView = g1.F;
                ee7.a((Object) flexibleTextView, "it.tvTitle");
                flexibleTextView.setVisibility(8);
                ViewPropertyAnimator translationY = g1.E.animate().translationY(-this.a.l1());
                ee7.a((Object) translationY, "it.scVerifyContent.anima\u2026slationY(-mtvTitleHeight)");
                translationY.setDuration(125);
                ViewPropertyAnimator translationY2 = g1.y.animate().translationY(-this.a.k1());
                ee7.a((Object) translationY2, "it.ftvEmailNotification.\u2026slationY(-mivEmailHeight)");
                translationY2.setDuration(125);
                ViewPropertyAnimator translationY3 = g1.x.animate().translationY(-this.a.k1());
                ee7.a((Object) translationY3, "it.ftvEmail.animate()\n  \u2026slationY(-mivEmailHeight)");
                translationY3.setDuration(125);
                ViewPropertyAnimator translationY4 = g1.r.animate().translationY(-this.a.k1());
                ee7.a((Object) translationY4, "it.clVerificationCode.an\u2026slationY(-mivEmailHeight)");
                translationY4.setDuration(125);
                ViewPropertyAnimator translationY5 = g1.z.animate().translationY(-this.a.k1());
                ee7.a((Object) translationY5, "it.ftvEnterCodesGuide.an\u2026slationY(-mivEmailHeight)");
                translationY5.setDuration(125);
                ViewPropertyAnimator translationY6 = g1.A.animate().translationY(-this.a.k1());
                ee7.a((Object) translationY6, "it.ftvInvalidCode.animat\u2026slationY(-mivEmailHeight)");
                translationY6.setDuration(125);
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            a15 g1 = this.a.g1();
            if (g1 != null) {
                FlexibleTextView flexibleTextView = g1.F;
                ee7.a((Object) flexibleTextView, "it.tvTitle");
                flexibleTextView.setVisibility(0);
                RTLImageView rTLImageView = g1.C;
                ee7.a((Object) rTLImageView, "it.ivEmail");
                rTLImageView.setVisibility(0);
                this.a.i1().setDuration(250);
                g1.F.startAnimation(this.a.i1());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ kt6 b;

        @DexIgnore
        public c(View view, kt6 kt6) {
            this.a = view;
            this.b = kt6;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.a.getWindowVisibleDisplayFrame(rect);
            int height = this.a.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                a15 g1 = this.b.g1();
                if (g1 != null && !this.b.j) {
                    g1.C.startAnimation(this.b.r);
                    this.b.j = true;
                    return;
                }
                return;
            }
            a15 g12 = this.b.g1();
            if (g12 != null && this.b.j) {
                kt6 kt6 = this.b;
                FlexibleTextView flexibleTextView = g12.F;
                ee7.a((Object) flexibleTextView, "it.tvTitle");
                kt6.b((float) flexibleTextView.getHeight());
                kt6 kt62 = this.b;
                RTLImageView rTLImageView = g12.C;
                ee7.a((Object) rTLImageView, "it.ivEmail");
                kt62.a((float) rTLImageView.getHeight());
                if (this.b.t == null) {
                    kt6 kt63 = this.b;
                    FlexibleTextView flexibleTextView2 = g12.F;
                    ee7.a((Object) flexibleTextView2, "it.tvTitle");
                    float x = flexibleTextView2.getX();
                    FlexibleTextView flexibleTextView3 = g12.F;
                    ee7.a((Object) flexibleTextView3, "it.tvTitle");
                    kt63.t = new TranslateAnimation(1, x, 1, flexibleTextView3.getX(), 1, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1, this.b.l1());
                    this.b.m1();
                }
                g12.E.startAnimation(this.b.t);
                this.b.j = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ a15 a;

        @DexIgnore
        public d(a15 a15) {
            this.a = a15;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.a.s.a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ a15 a;

        @DexIgnore
        public e(a15 a15) {
            this.a = a15;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.a.u.a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ a15 a;

        @DexIgnore
        public f(a15 a15) {
            this.a = a15;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.a.v.a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ a15 a;

        @DexIgnore
        public g(a15 a15) {
            this.a = a15;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.a.t.a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ kt6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public h(kt6 kt6) {
            this.a = kt6;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            a15 g1 = this.a.g1();
            if (g1 != null) {
                FlexibleTextView flexibleTextView = g1.F;
                ee7.a((Object) flexibleTextView, "it.tvTitle");
                flexibleTextView.setVisibility(0);
                RTLImageView rTLImageView = g1.C;
                ee7.a((Object) rTLImageView, "it.ivEmail");
                rTLImageView.setVisibility(0);
                g1.C.startAnimation(this.a.h1());
                this.a.i1().setDuration(500);
                g1.F.startAnimation(this.a.j1());
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            a15 g1 = this.a.g1();
            if (g1 != null) {
                FlexibleTextView flexibleTextView = g1.F;
                ee7.a((Object) flexibleTextView, "it.tvTitle");
                flexibleTextView.setVisibility(4);
                RTLImageView rTLImageView = g1.C;
                ee7.a((Object) rTLImageView, "it.ivEmail");
                rTLImageView.setVisibility(4);
                ViewPropertyAnimator translationY = g1.E.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                ee7.a((Object) translationY, "it.scVerifyContent.anima\u2026        .translationY(0f)");
                translationY.setDuration(500);
                ViewPropertyAnimator translationY2 = g1.y.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                ee7.a((Object) translationY2, "it.ftvEmailNotification.\u2026        .translationY(0f)");
                translationY2.setDuration(500);
                ViewPropertyAnimator translationY3 = g1.x.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                ee7.a((Object) translationY3, "it.ftvEmail.animate()\n  \u2026        .translationY(0f)");
                translationY3.setDuration(500);
                ViewPropertyAnimator translationY4 = g1.r.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                ee7.a((Object) translationY4, "it.clVerificationCode.an\u2026        .translationY(0f)");
                translationY4.setDuration(500);
                ViewPropertyAnimator translationY5 = g1.z.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                ee7.a((Object) translationY5, "it.ftvEnterCodesGuide.an\u2026        .translationY(0f)");
                translationY5.setDuration(500);
                ViewPropertyAnimator translationY6 = g1.A.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                ee7.a((Object) translationY6, "it.ftvInvalidCode.animat\u2026        .translationY(0f)");
                translationY6.setDuration(500);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.jt6
    public void L0() {
        RTLImageView rTLImageView;
        FlexibleTextView flexibleTextView;
        if (isActive()) {
            a15 g1 = g1();
            if (!(g1 == null || (flexibleTextView = g1.F) == null)) {
                flexibleTextView.setText(ig5.a(PortfolioApp.g0.c(), 2131886954));
            }
            a15 g12 = g1();
            if (g12 != null && (rTLImageView = g12.C) != null) {
                rTLImageView.setImageDrawable(PortfolioApp.g0.c().getDrawable(2131231314));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.jt6
    public void P(boolean z) {
        a15 g1;
        FlexibleTextView flexibleTextView;
        if (isActive() && (g1 = g1()) != null && (flexibleTextView = g1.A) != null) {
            if (z) {
                ee7.a((Object) flexibleTextView, "it");
                flexibleTextView.setVisibility(0);
                return;
            }
            ee7.a((Object) flexibleTextView, "it");
            if (flexibleTextView.getVisibility() != 4) {
                flexibleTextView.setVisibility(4);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.jt6
    public void X() {
        FragmentActivity activity;
        if (!isActive() || (activity = getActivity()) == null) {
            return;
        }
        if (this.j) {
            le5 le5 = le5.a;
            a15 g1 = g1();
            RTLImageView rTLImageView = g1 != null ? g1.B : null;
            if (rTLImageView != null) {
                ee7.a((Object) activity, "it");
                le5.a(rTLImageView, activity);
                return;
            }
            throw new x87("null cannot be cast to non-null type android.view.View");
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void afterTextChanged(Editable editable) {
        FragmentActivity activity = getActivity();
        View currentFocus = activity != null ? activity.getCurrentFocus() : null;
        a15 g1 = g1();
        if (g1 != null) {
            Integer valueOf = currentFocus != null ? Integer.valueOf(currentFocus.getId()) : null;
            boolean z = false;
            if (valueOf != null && valueOf.intValue() == 2131362224) {
                if (currentFocus != null) {
                    EditText editText = (EditText) currentFocus;
                    Editable text = editText.getText();
                    int length = text.length();
                    if (length == 0) {
                        FlexibleButton flexibleButton = g1.q;
                        ee7.a((Object) flexibleButton, "it.btContinue");
                        flexibleButton.setEnabled(false);
                        g1.q.a("flexible_button_disabled");
                    } else if (length == 1) {
                        editText.setSelection(1);
                        FlexibleEditText flexibleEditText = g1.u;
                        ee7.a((Object) flexibleEditText, "it.etSecondCode");
                        Editable text2 = flexibleEditText.getText();
                        if (text2 != null) {
                            ee7.a((Object) text2, "it.etSecondCode.text!!");
                            if (text2.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                g1.u.requestFocus();
                            }
                            f1();
                            return;
                        }
                        ee7.a();
                        throw null;
                    } else if (length == 2) {
                        char charAt = text.charAt(0);
                        char charAt2 = text.charAt(1);
                        FlexibleEditText flexibleEditText2 = g1.u;
                        ee7.a((Object) flexibleEditText2, "it.etSecondCode");
                        Editable text3 = flexibleEditText2.getText();
                        if (text3 != null) {
                            ee7.a((Object) text3, "it.etSecondCode.text!!");
                            if (text3.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                g1.s.setTextKeepState(String.valueOf(charAt));
                                g1.s.clearFocus();
                                g1.u.requestFocus();
                                g1.u.setText(String.valueOf(charAt2));
                            } else {
                                g1.s.setTextKeepState(String.valueOf(charAt2));
                            }
                            editText.setSelection(1);
                            return;
                        }
                        ee7.a();
                        throw null;
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type android.widget.EditText");
                }
            } else if (valueOf != null && valueOf.intValue() == 2131362235) {
                if (currentFocus != null) {
                    EditText editText2 = (EditText) currentFocus;
                    Editable text4 = editText2.getText();
                    int length2 = text4.length();
                    if (length2 == 0) {
                        FlexibleButton flexibleButton2 = g1.q;
                        ee7.a((Object) flexibleButton2, "it.btContinue");
                        flexibleButton2.setEnabled(false);
                        g1.q.a("flexible_button_disabled");
                    } else if (length2 == 1) {
                        editText2.setSelection(1);
                        FlexibleEditText flexibleEditText3 = g1.v;
                        ee7.a((Object) flexibleEditText3, "it.etThirdCode");
                        Editable text5 = flexibleEditText3.getText();
                        if (text5 != null) {
                            ee7.a((Object) text5, "it.etThirdCode.text!!");
                            if (text5.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                g1.v.requestFocus();
                            }
                            f1();
                            return;
                        }
                        ee7.a();
                        throw null;
                    } else if (length2 == 2) {
                        char charAt3 = text4.charAt(0);
                        char charAt4 = text4.charAt(1);
                        FlexibleEditText flexibleEditText4 = g1.v;
                        ee7.a((Object) flexibleEditText4, "it.etThirdCode");
                        Editable text6 = flexibleEditText4.getText();
                        if (text6 != null) {
                            ee7.a((Object) text6, "it.etThirdCode.text!!");
                            if (text6.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                editText2.setTextKeepState(String.valueOf(charAt3));
                                editText2.clearFocus();
                                g1.v.requestFocus();
                                g1.v.setText(String.valueOf(charAt4));
                            } else {
                                editText2.setTextKeepState(String.valueOf(charAt4));
                            }
                            editText2.setSelection(1);
                            return;
                        }
                        ee7.a();
                        throw null;
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type android.widget.EditText");
                }
            } else if (valueOf != null && valueOf.intValue() == 2131362237) {
                if (currentFocus != null) {
                    EditText editText3 = (EditText) currentFocus;
                    Editable text7 = editText3.getText();
                    int length3 = text7.length();
                    if (length3 == 0) {
                        FlexibleButton flexibleButton3 = g1.q;
                        ee7.a((Object) flexibleButton3, "it.btContinue");
                        flexibleButton3.setEnabled(false);
                        g1.q.a("flexible_button_disabled");
                    } else if (length3 == 1) {
                        editText3.setSelection(1);
                        FlexibleEditText flexibleEditText5 = g1.t;
                        ee7.a((Object) flexibleEditText5, "it.etFourthCode");
                        Editable text8 = flexibleEditText5.getText();
                        if (text8 != null) {
                            ee7.a((Object) text8, "it.etFourthCode.text!!");
                            if (text8.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                g1.t.requestFocus();
                            }
                            f1();
                            return;
                        }
                        ee7.a();
                        throw null;
                    } else if (length3 == 2) {
                        char charAt5 = text7.charAt(0);
                        char charAt6 = text7.charAt(1);
                        FlexibleEditText flexibleEditText6 = g1.t;
                        ee7.a((Object) flexibleEditText6, "it.etFourthCode");
                        Editable text9 = flexibleEditText6.getText();
                        if (text9 != null) {
                            ee7.a((Object) text9, "it.etFourthCode.text!!");
                            if (text9.length() == 0) {
                                z = true;
                            }
                            if (z) {
                                editText3.setText(String.valueOf(charAt5));
                                editText3.clearFocus();
                                g1.t.requestFocus();
                                g1.t.setText(String.valueOf(charAt6));
                            } else {
                                editText3.setText(String.valueOf(charAt6));
                            }
                            editText3.setSelection(1);
                            return;
                        }
                        ee7.a();
                        throw null;
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type android.widget.EditText");
                }
            } else if (valueOf == null || valueOf.intValue() != 2131362226) {
            } else {
                if (currentFocus != null) {
                    EditText editText4 = (EditText) currentFocus;
                    int length4 = editText4.getText().length();
                    if (length4 == 0) {
                        FlexibleButton flexibleButton4 = g1.q;
                        ee7.a((Object) flexibleButton4, "it.btContinue");
                        flexibleButton4.setEnabled(false);
                        g1.q.a("flexible_button_disabled");
                    } else if (length4 == 1) {
                        editText4.setSelection(1);
                        f1();
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type android.widget.EditText");
                }
            }
        }
    }

    @DexIgnore
    public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    @DexIgnore
    @Override // com.fossil.jt6
    public void f(int i2, String str) {
        ee7.b(str, "errorMessage");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, i2, str);
        }
    }

    @DexIgnore
    public final void f1() {
        a15 g1;
        if (isActive() && (g1 = g1()) != null) {
            FlexibleEditText flexibleEditText = g1.s;
            ee7.a((Object) flexibleEditText, "it.etFirstCode");
            FlexibleEditText flexibleEditText2 = g1.u;
            ee7.a((Object) flexibleEditText2, "it.etSecondCode");
            FlexibleEditText flexibleEditText3 = g1.v;
            ee7.a((Object) flexibleEditText3, "it.etThirdCode");
            FlexibleEditText flexibleEditText4 = g1.t;
            ee7.a((Object) flexibleEditText4, "it.etFourthCode");
            String[] strArr = {String.valueOf(flexibleEditText.getText()), String.valueOf(flexibleEditText2.getText()), String.valueOf(flexibleEditText3.getText()), String.valueOf(flexibleEditText4.getText())};
            it6 it6 = this.f;
            if (it6 != null) {
                it6.a(strArr);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.jt6
    public void g() {
        if (isActive()) {
            b();
        }
    }

    @DexIgnore
    public final a15 g1() {
        qw6<a15> qw6 = this.g;
        if (qw6 != null) {
            return qw6.a();
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.jt6
    public void h(String str) {
        a15 g1;
        ee7.b(str, "emailAddress");
        if (isActive() && (g1 = g1()) != null) {
            FlexibleTextView flexibleTextView = g1.x;
            ee7.a((Object) flexibleTextView, "it.ftvEmail");
            flexibleTextView.setText(str);
        }
    }

    @DexIgnore
    public final ScaleAnimation h1() {
        return this.s;
    }

    @DexIgnore
    public final ScaleAnimation i1() {
        return this.p;
    }

    @DexIgnore
    public final ScaleAnimation j1() {
        return this.q;
    }

    @DexIgnore
    public final float k1() {
        return this.i;
    }

    @DexIgnore
    public final float l1() {
        return this.h;
    }

    @DexIgnore
    public final void m1() {
        TranslateAnimation translateAnimation = this.t;
        if (translateAnimation != null) {
            translateAnimation.setAnimationListener(new h(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.jt6
    public void n(boolean z) {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        FlexibleButton flexibleButton3;
        if (isActive()) {
            a15 g1 = g1();
            if (!(g1 == null || (flexibleButton3 = g1.q) == null)) {
                flexibleButton3.setEnabled(z);
            }
            if (z) {
                a15 g12 = g1();
                if (g12 != null && (flexibleButton2 = g12.q) != null) {
                    flexibleButton2.a("flexible_button_primary");
                    return;
                }
                return;
            }
            a15 g13 = g1();
            if (g13 != null && (flexibleButton = g13.q) != null) {
                flexibleButton.a("flexible_button_disabled");
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131361936) {
                it6 it6 = this.f;
                if (it6 != null) {
                    it6.k();
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            } else if (id == 2131362274) {
                it6 it62 = this.f;
                if (it62 != null) {
                    it62.i();
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            } else if (id == 2131362636) {
                it6 it63 = this.f;
                if (it63 != null) {
                    it63.h();
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.p.setDuration(500);
        this.p.setFillAfter(true);
        this.q.setDuration(500);
        this.q.setFillAfter(true);
        this.r.setDuration(500);
        this.r.setFillAfter(true);
        this.s.setDuration(500);
        this.s.setFillAfter(true);
        this.r.setAnimationListener(new b(this));
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        a15 a15 = (a15) qb.a(layoutInflater, 2131558552, viewGroup, false, a1());
        ee7.a((Object) a15, "binding");
        View d2 = a15.d();
        ee7.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new c(d2, this));
        this.g = new qw6<>(this, a15);
        a15 g1 = g1();
        if (g1 != null) {
            return g1.d();
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        a15 g1;
        ee7.b(view, "view");
        ee7.b(keyEvent, "keyEvent");
        if (i2 == 67 && keyEvent.getAction() == 0 && (g1 = g1()) != null) {
            boolean z = true;
            if (view.getId() == 2131362235) {
                FlexibleEditText flexibleEditText = g1.u;
                ee7.a((Object) flexibleEditText, "it.etSecondCode");
                Editable text = flexibleEditText.getText();
                if (text != null) {
                    ee7.a((Object) text, "it.etSecondCode.text!!");
                    if (text.length() != 0) {
                        z = false;
                    }
                    if (z) {
                        g1.s.requestFocus();
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (view.getId() == 2131362237) {
                FlexibleEditText flexibleEditText2 = g1.v;
                ee7.a((Object) flexibleEditText2, "it.etThirdCode");
                Editable text2 = flexibleEditText2.getText();
                if (text2 != null) {
                    ee7.a((Object) text2, "it.etThirdCode.text!!");
                    if (text2.length() != 0) {
                        z = false;
                    }
                    if (z) {
                        g1.u.requestFocus();
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (view.getId() == 2131362226) {
                FlexibleEditText flexibleEditText3 = g1.t;
                ee7.a((Object) flexibleEditText3, "it.etFourthCode");
                Editable text3 = flexibleEditText3.getText();
                if (text3 != null) {
                    ee7.a((Object) text3, "it.etFourthCode.text!!");
                    if (text3.length() != 0) {
                        z = false;
                    }
                    if (z) {
                        g1.v.requestFocus();
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
        return super.onKey(view, i2, keyEvent);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        FragmentActivity activity;
        Window window;
        super.onPause();
        if (!(this.j || (activity = getActivity()) == null || (window = activity.getWindow()) == null)) {
            window.setSoftInputMode(3);
        }
        it6 it6 = this.f;
        if (it6 != null) {
            it6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        it6 it6 = this.f;
        if (it6 != null) {
            it6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        a15 g1 = g1();
        if (g1 != null) {
            g1.B.setOnClickListener(this);
            g1.w.setOnClickListener(this);
            g1.q.setOnClickListener(this);
            g1.s.addTextChangedListener(this);
            g1.u.addTextChangedListener(this);
            g1.v.addTextChangedListener(this);
            g1.t.addTextChangedListener(this);
            g1.s.setOnKeyListener(this);
            g1.u.setOnKeyListener(this);
            g1.v.setOnKeyListener(this);
            g1.t.setOnKeyListener(this);
            g1.s.setOnFocusChangeListener(new d(g1));
            g1.u.setOnFocusChangeListener(new e(g1));
            g1.v.setOnFocusChangeListener(new f(g1));
            g1.t.setOnFocusChangeListener(new g(g1));
        }
    }

    @DexIgnore
    public final void b(float f2) {
        this.h = f2;
    }

    @DexIgnore
    @Override // com.fossil.jt6
    public void f() {
        if (isActive()) {
            a();
        }
    }

    @DexIgnore
    public final void a(float f2) {
        this.i = f2;
    }

    @DexIgnore
    public void a(it6 it6) {
        ee7.b(it6, "presenter");
        this.f = it6;
    }

    @DexIgnore
    @Override // com.fossil.jt6
    public void a(String str, int i2, int i3) {
        View d2;
        ee7.b(str, "emailAddress");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, i2, i3, str);
            a15 g1 = g1();
            if (g1 != null && (d2 = g1.d()) != null) {
                d2.setVisibility(8);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        if (str.hashCode() == 766014770 && str.equals("EMAIL_OTP_VERIFICATION") && i2 == 2131361937) {
            it6 it6 = this.f;
            if (it6 != null) {
                it6.j();
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.jt6
    public void a(SignUpEmailAuth signUpEmailAuth) {
        ee7.b(signUpEmailAuth, "emailAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.z;
            ee7.a((Object) activity, "it");
            aVar.a(activity, signUpEmailAuth);
        }
    }
}
