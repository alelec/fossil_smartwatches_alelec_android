package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.view.View;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wt3 implements au3 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ExtendedFloatingActionButton b;
    @DexIgnore
    public /* final */ ArrayList<Animator.AnimatorListener> c; // = new ArrayList<>();
    @DexIgnore
    public /* final */ vt3 d;
    @DexIgnore
    public bs3 e;
    @DexIgnore
    public bs3 f;

    @DexIgnore
    public wt3(ExtendedFloatingActionButton extendedFloatingActionButton, vt3 vt3) {
        this.b = extendedFloatingActionButton;
        this.a = extendedFloatingActionButton.getContext();
        this.d = vt3;
    }

    @DexIgnore
    @Override // com.fossil.au3
    public final void a(bs3 bs3) {
        this.f = bs3;
    }

    @DexIgnore
    public AnimatorSet b(bs3 bs3) {
        ArrayList arrayList = new ArrayList();
        if (bs3.c("opacity")) {
            arrayList.add(bs3.a("opacity", this.b, View.ALPHA));
        }
        if (bs3.c("scale")) {
            arrayList.add(bs3.a("scale", this.b, View.SCALE_Y));
            arrayList.add(bs3.a("scale", this.b, View.SCALE_X));
        }
        if (bs3.c("width")) {
            arrayList.add(bs3.a("width", this.b, ExtendedFloatingActionButton.G));
        }
        if (bs3.c("height")) {
            arrayList.add(bs3.a("height", this.b, ExtendedFloatingActionButton.H));
        }
        AnimatorSet animatorSet = new AnimatorSet();
        vr3.a(animatorSet, arrayList);
        return animatorSet;
    }

    @DexIgnore
    @Override // com.fossil.au3
    public bs3 d() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.au3
    public void f() {
        this.d.b();
    }

    @DexIgnore
    @Override // com.fossil.au3
    public AnimatorSet g() {
        return b(i());
    }

    @DexIgnore
    @Override // com.fossil.au3
    public final List<Animator.AnimatorListener> h() {
        return this.c;
    }

    @DexIgnore
    public final bs3 i() {
        bs3 bs3 = this.f;
        if (bs3 != null) {
            return bs3;
        }
        if (this.e == null) {
            this.e = bs3.a(this.a, b());
        }
        bs3 bs32 = this.e;
        e9.a(bs32);
        return bs32;
    }

    @DexIgnore
    @Override // com.fossil.au3
    public void onAnimationStart(Animator animator) {
        this.d.a(animator);
    }

    @DexIgnore
    @Override // com.fossil.au3
    public void a() {
        this.d.b();
    }
}
