package com.fossil;

import android.app.ActivityManager;
import android.os.Bundle;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rl3 {
    @DexIgnore
    public /* final */ /* synthetic */ il3 a;

    @DexIgnore
    public rl3(il3 il3) {
        this.a = il3;
    }

    @DexIgnore
    public final void a() {
        this.a.g();
        if (this.a.k().a(this.a.zzm().b())) {
            this.a.k().r.a(true);
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
            if (runningAppProcessInfo.importance == 100) {
                this.a.e().B().a("Detected application was in foreground");
                b(this.a.zzm().b(), false);
            }
        }
    }

    @DexIgnore
    public final void b(long j, boolean z) {
        this.a.g();
        if (((ii3) this.a).a.g()) {
            this.a.k().u.a(j);
            this.a.e().B().a("Session started, time", Long.valueOf(this.a.zzm().c()));
            Long valueOf = Long.valueOf(j / 1000);
            this.a.o().a("auto", "_sid", valueOf, j);
            this.a.k().r.a(false);
            Bundle bundle = new Bundle();
            bundle.putLong("_sid", valueOf.longValue());
            if (this.a.l().a(wb3.q0) && z) {
                bundle.putLong("_aib", 1);
            }
            this.a.o().a("auto", "_s", j, bundle);
            if (w03.a() && this.a.l().a(wb3.v0)) {
                String a2 = this.a.k().z.a();
                if (!TextUtils.isEmpty(a2)) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("_ffr", a2);
                    this.a.o().a("auto", "_ssr", j, bundle2);
                }
            }
        }
    }

    @DexIgnore
    public final void a(long j, boolean z) {
        this.a.g();
        this.a.A();
        if (this.a.k().a(j)) {
            this.a.k().r.a(true);
        }
        this.a.k().u.a(j);
        if (this.a.k().r.a()) {
            b(j, z);
        }
    }
}
