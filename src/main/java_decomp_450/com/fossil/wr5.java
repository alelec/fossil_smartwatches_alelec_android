package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.bo6;
import com.fossil.cy6;
import com.fossil.rd5;
import com.fossil.vx6;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wr5 extends ho5 implements rr5, cy6.g, bo6.a {
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ a y; // = new a(null);
    @DexIgnore
    public d76 g;
    @DexIgnore
    public e06 h;
    @DexIgnore
    public d56 i;
    @DexIgnore
    public oh6 j;
    @DexIgnore
    public xs5 p;
    @DexIgnore
    public sw5 q;
    @DexIgnore
    public hh6 r;
    @DexIgnore
    public qr5 s;
    @DexIgnore
    public qw6<m25> t;
    @DexIgnore
    public /* final */ ArrayList<Fragment> u; // = new ArrayList<>();
    @DexIgnore
    public xz6 v;
    @DexIgnore
    public HashMap w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final wr5 a() {
            return new wr5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements BottomNavigationView.d {
        @DexIgnore
        public /* final */ /* synthetic */ qe7 a;
        @DexIgnore
        public /* final */ /* synthetic */ qe7 b;
        @DexIgnore
        public /* final */ /* synthetic */ wr5 c;
        @DexIgnore
        public /* final */ /* synthetic */ Typeface d;

        @DexIgnore
        public b(qe7 qe7, qe7 qe72, wr5 wr5, String str, boolean z, Typeface typeface) {
            this.a = qe7;
            this.b = qe72;
            this.c = wr5;
            this.d = typeface;
        }

        @DexIgnore
        @Override // com.google.android.material.bottomnavigation.BottomNavigationView.d
        public final boolean a(MenuItem menuItem) {
            ee7.b(menuItem, "item");
            m25 m25 = (m25) wr5.a(this.c).a();
            BottomNavigationView bottomNavigationView = m25 != null ? m25.q : null;
            if (bottomNavigationView != null) {
                ee7.a((Object) bottomNavigationView, "mBinding.get()?.bottomNavigation!!");
                Menu menu = bottomNavigationView.getMenu();
                ee7.a((Object) menu, "mBinding.get()?.bottomNavigation!!.menu");
                this.c.a(menu, this.d, this.a.element);
                menu.findItem(2131362176).setIcon(2131231084);
                Drawable c2 = v6.c(this.c.requireContext(), 2131231009);
                if (c2 != null) {
                    c2.setColorFilter(v6.a(PortfolioApp.g0.c(), 2131099820), PorterDuff.Mode.SRC_ATOP);
                }
                menuItem.setIcon(c2);
                MenuItem findItem = menu.findItem(2131361975);
                ee7.a((Object) findItem, "menu.findItem(R.id.buddyChallengeFragment)");
                findItem.setIcon(c2);
                menu.findItem(2131362164).setIcon(2131231059);
                menu.findItem(2131362926).setIcon(2131231141);
                menu.findItem(2131361885).setIcon(2131230987);
                MenuItem findItem2 = menu.findItem(2131361885);
                ee7.a((Object) findItem2, "menu.findItem(R.id.alertsFragment)");
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(findItem2.getTitle());
                int i = 0;
                spannableStringBuilder.setSpan(new ForegroundColorSpan(this.a.element), 0, spannableStringBuilder.length(), 0);
                menu.findItem(2131361885).setTitle(spannableStringBuilder);
                switch (menuItem.getItemId()) {
                    case 2131361885:
                        Drawable c3 = v6.c(this.c.requireContext(), 2131230988);
                        int i2 = this.b.element;
                        if (c3 != null) {
                            c3.setColorFilter(i2, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(c3);
                        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder2.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder2.length(), 0);
                        menu.findItem(2131361885).setTitle(spannableStringBuilder2);
                        i = 3;
                        break;
                    case 2131361975:
                        Drawable c4 = v6.c(this.c.requireContext(), 2131231011);
                        int i3 = this.b.element;
                        if (c4 != null) {
                            c4.setColorFilter(i3, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(c4);
                        SpannableStringBuilder spannableStringBuilder3 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder3.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder3.length(), 0);
                        menu.findItem(2131361975).setTitle(spannableStringBuilder3);
                        i = 1;
                        break;
                    case 2131362164:
                        Drawable c5 = v6.c(this.c.requireContext(), 2131231060);
                        int i4 = this.b.element;
                        if (c5 != null) {
                            c5.setColorFilter(i4, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(c5);
                        SpannableStringBuilder spannableStringBuilder4 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder4.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder4.length(), 0);
                        menu.findItem(2131362164).setTitle(spannableStringBuilder4);
                        i = 2;
                        break;
                    case 2131362176:
                        Drawable c6 = v6.c(this.c.requireContext(), 2131231085);
                        int i5 = this.b.element;
                        if (c6 != null) {
                            c6.setColorFilter(i5, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(c6);
                        SpannableStringBuilder spannableStringBuilder5 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder5.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder5.length(), 0);
                        menu.findItem(2131362176).setTitle(spannableStringBuilder5);
                        break;
                    case 2131362926:
                        Drawable c7 = v6.c(this.c.requireContext(), 2131231142);
                        int i6 = this.b.element;
                        if (c7 != null) {
                            c7.setColorFilter(i6, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(c7);
                        SpannableStringBuilder spannableStringBuilder6 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder6.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder6.length(), 0);
                        menu.findItem(2131362926).setTitle(spannableStringBuilder6);
                        i = 4;
                        break;
                }
                if (this.c.i != null && be5.o.g(PortfolioApp.g0.c().c())) {
                    this.c.h1().b(i);
                }
                if (this.c.h != null && be5.o.f(PortfolioApp.g0.c().c())) {
                    this.c.g1().b(i);
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String l1 = wr5.x;
                local.d(l1, "show tab with tab=" + i);
                wr5.b(this.c).a(i);
                this.c.o(i);
                return true;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnLongClickListener {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public final boolean onLongClick(View view) {
            ee7.a((Object) view, "item");
            view.setHapticFeedbackEnabled(false);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ wr5 a;

        @DexIgnore
        public d(wr5 wr5) {
            this.a = wr5;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0053, code lost:
            if (r0 != false) goto L_0x0055;
         */
        @DexIgnore
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void onChanged(java.lang.Integer r5) {
            /*
                r4 = this;
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r1 = com.fossil.wr5.x
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "dashboardTab: "
                r2.append(r3)
                r2.append(r5)
                java.lang.String r2 = r2.toString()
                r0.d(r1, r2)
                r0 = 0
                java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
                if (r5 != 0) goto L_0x0027
            L_0x0025:
                r5 = r1
                goto L_0x0055
            L_0x0027:
                r2 = 1
                int r3 = r5.intValue()
                if (r3 != r2) goto L_0x0055
                com.fossil.wr5 r2 = r4.a
                com.fossil.qw6 r2 = com.fossil.wr5.a(r2)
                java.lang.Object r2 = r2.a()
                com.fossil.m25 r2 = (com.fossil.m25) r2
                if (r2 == 0) goto L_0x0053
                com.google.android.material.bottomnavigation.BottomNavigationView r2 = r2.q
                if (r2 == 0) goto L_0x0053
                android.view.Menu r2 = r2.getMenu()
                if (r2 == 0) goto L_0x0053
                r3 = 2131361975(0x7f0a00b7, float:1.8343718E38)
                android.view.MenuItem r2 = r2.findItem(r3)
                if (r2 == 0) goto L_0x0053
                boolean r0 = r2.isVisible()
            L_0x0053:
                if (r0 == 0) goto L_0x0025
            L_0x0055:
                com.fossil.wr5 r0 = r4.a
                int r1 = r5.intValue()
                r0.n(r1)
                com.fossil.wr5 r0 = r4.a
                com.fossil.qr5 r0 = com.fossil.wr5.b(r0)
                int r1 = r5.intValue()
                r0.a(r1)
                com.fossil.wr5 r0 = r4.a
                int r5 = r5.intValue()
                r0.o(r5)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.wr5.d.onChanged(java.lang.Integer):void");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ wr5 a;

        @DexIgnore
        public e(wr5 wr5) {
            this.a = wr5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String l1 = wr5.x;
            local.d(l1, "activeDeviceSerialLiveData onChange " + str);
            qr5 b = wr5.b(this.a);
            if (str != null) {
                b.a(str);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    /*
    static {
        String simpleName = wr5.class.getSimpleName();
        ee7.a((Object) simpleName, "HomeFragment::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    public wr5() {
        eo6.b.a();
    }

    @DexIgnore
    public static final /* synthetic */ qw6 a(wr5 wr5) {
        qw6<m25> qw6 = wr5.t;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ qr5 b(wr5 wr5) {
        qr5 qr5 = wr5.s;
        if (qr5 != null) {
            return qr5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void T(boolean z) {
        String b2 = eh5.l.a().b("nonBrandSurface");
        Typeface c2 = eh5.l.a().c("nonBrandTextStyle11");
        qw6<m25> qw6 = this.t;
        if (qw6 != null) {
            m25 a2 = qw6.a();
            if (a2 != null) {
                if (b2 != null) {
                    a2.q.setBackgroundColor(Color.parseColor(b2));
                }
                BottomNavigationView bottomNavigationView = a2.q;
                ee7.a((Object) bottomNavigationView, "bottomNavigation");
                bottomNavigationView.getMenu().findItem(2131361975).setVisible(z);
                BottomNavigationView bottomNavigationView2 = a2.q;
                ee7.a((Object) bottomNavigationView2, "bottomNavigation");
                Menu menu = bottomNavigationView2.getMenu();
                ee7.a((Object) menu, "bottomNavigation.menu");
                int i2 = 0;
                int size = menu.size();
                while (i2 < size) {
                    MenuItem item = menu.getItem(i2);
                    ee7.a((Object) item, "getItem(index)");
                    qw6<m25> qw62 = this.t;
                    if (qw62 != null) {
                        m25 a3 = qw62.a();
                        BottomNavigationView bottomNavigationView3 = a3 != null ? a3.q : null;
                        if (bottomNavigationView3 != null) {
                            bottomNavigationView3.findViewById(item.getItemId()).setOnLongClickListener(c.a);
                            i2++;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.d("mBinding");
                        throw null;
                    }
                }
                qe7 qe7 = new qe7();
                qe7.element = v6.a(requireContext(), 2131099942);
                qe7 qe72 = new qe7();
                qe72.element = v6.a(requireContext(), 2131099971);
                String b3 = eh5.l.a().b("nonBrandDisableCalendarDay");
                String b4 = eh5.l.a().b("primaryColor");
                if (b3 != null) {
                    qe7.element = Color.parseColor(b3);
                }
                if (b4 != null) {
                    qe72.element = Color.parseColor(b4);
                }
                BottomNavigationView bottomNavigationView4 = a2.q;
                ee7.a((Object) bottomNavigationView4, "bottomNavigation");
                bottomNavigationView4.setItemIconTintList(null);
                a2.q.setOnNavigationItemSelectedListener(new b(qe7, qe72, this, b2, z, c2));
            }
            f1();
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rr5
    public void W() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        StringBuilder sb = new StringBuilder();
        sb.append("onStartUpdateFw currentTab ");
        qr5 qr5 = this.s;
        if (qr5 != null) {
            sb.append(qr5.h());
            local.d(str, sb.toString());
            qr5 qr52 = this.s;
            if (qr52 != null) {
                o(qr52.h());
                d76 d76 = this.g;
                if (d76 == null) {
                    return;
                }
                if (d76 != null) {
                    d76.m();
                } else {
                    ee7.d("mHomeDashboardPresenter");
                    throw null;
                }
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void Y(String str) {
        a(new Intent("android.intent.action.VIEW", Uri.parse(str)), x);
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.w;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.rr5
    public void c(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        StringBuilder sb = new StringBuilder();
        sb.append("onUpdateFwComplete currentTab ");
        qr5 qr5 = this.s;
        if (qr5 != null) {
            sb.append(qr5.h());
            local.d(str, sb.toString());
            d76 d76 = this.g;
            if (d76 != null) {
                if (d76 != null) {
                    d76.b(z);
                } else {
                    ee7.d("mHomeDashboardPresenter");
                    throw null;
                }
            }
            qr5 qr52 = this.s;
            if (qr52 != null) {
                o(qr52.h());
                if (!z && isActive()) {
                    bx6 bx6 = bx6.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    ee7.a((Object) childFragmentManager, "childFragmentManager");
                    bx6.W(childFragmentManager);
                    return;
                }
                return;
            }
            ee7.d("mPresenter");
            throw null;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.rr5
    public void f(int i2) {
        d76 d76 = this.g;
        if (d76 == null) {
            return;
        }
        if (d76 != null) {
            d76.c(i2);
        } else {
            ee7.d("mHomeDashboardPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void f1() {
        Resources resources = getResources();
        ee7.a((Object) resources, "resources");
        int applyDimension = (int) TypedValue.applyDimension(1, 28.0f, resources.getDisplayMetrics());
        qw6<m25> qw6 = this.t;
        if (qw6 != null) {
            m25 a2 = qw6.a();
            if (a2 != null) {
                int i2 = 0;
                View childAt = a2.q.getChildAt(0);
                if (childAt != null) {
                    BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) childAt;
                    int childCount = bottomNavigationMenuView.getChildCount() - 1;
                    if (childCount >= 0) {
                        while (true) {
                            View findViewById = bottomNavigationMenuView.getChildAt(i2).findViewById(2131362579);
                            ee7.a((Object) findViewById, "icon");
                            findViewById.getLayoutParams().width = applyDimension;
                            findViewById.getLayoutParams().height = applyDimension;
                            if (i2 != childCount) {
                                i2++;
                            } else {
                                return;
                            }
                        }
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type com.google.android.material.bottomnavigation.BottomNavigationMenuView");
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final e06 g1() {
        e06 e06 = this.h;
        if (e06 != null) {
            return e06;
        }
        ee7.d("mHomeDianaCustomizePresenter");
        throw null;
    }

    @DexIgnore
    public final d56 h1() {
        d56 d56 = this.i;
        if (d56 != null) {
            return d56;
        }
        ee7.d("mHomeHybridCustomizePresenter");
        throw null;
    }

    @DexIgnore
    public final void i1() {
        j1();
        qr5 qr5 = this.s;
        if (qr5 != null) {
            int h2 = qr5.h();
            if (h2 == 0) {
                qw6<m25> qw6 = this.t;
                if (qw6 != null) {
                    m25 a2 = qw6.a();
                    if (a2 != null) {
                        BottomNavigationView bottomNavigationView = a2.q;
                        ee7.a((Object) bottomNavigationView, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView.setSelectedItemId(2131362176);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.d("mBinding");
                throw null;
            } else if (h2 == 1) {
                qw6<m25> qw62 = this.t;
                if (qw62 != null) {
                    m25 a3 = qw62.a();
                    if (a3 != null) {
                        BottomNavigationView bottomNavigationView2 = a3.q;
                        ee7.a((Object) bottomNavigationView2, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView2.setSelectedItemId(2131361975);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.d("mBinding");
                throw null;
            } else if (h2 == 2) {
                qw6<m25> qw63 = this.t;
                if (qw63 != null) {
                    m25 a4 = qw63.a();
                    if (a4 != null) {
                        BottomNavigationView bottomNavigationView3 = a4.q;
                        ee7.a((Object) bottomNavigationView3, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView3.setSelectedItemId(2131362164);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.d("mBinding");
                throw null;
            } else if (h2 == 3) {
                qw6<m25> qw64 = this.t;
                if (qw64 != null) {
                    m25 a5 = qw64.a();
                    if (a5 != null) {
                        BottomNavigationView bottomNavigationView4 = a5.q;
                        ee7.a((Object) bottomNavigationView4, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView4.setSelectedItemId(2131361885);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.d("mBinding");
                throw null;
            } else if (h2 == 4) {
                qw6<m25> qw65 = this.t;
                if (qw65 != null) {
                    m25 a6 = qw65.a();
                    if (a6 != null) {
                        BottomNavigationView bottomNavigationView5 = a6.q;
                        ee7.a((Object) bottomNavigationView5, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView5.setSelectedItemId(2131362926);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.d("mBinding");
                throw null;
            }
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void j1() {
        qr5 qr5 = this.s;
        if (qr5 == null) {
            return;
        }
        if (qr5 != null) {
            int h2 = qr5.h();
            int size = this.u.size();
            int i2 = 0;
            while (i2 < size) {
                if (this.u.get(i2) instanceof ro5) {
                    Fragment fragment = this.u.get(i2);
                    if (fragment != null) {
                        ((ro5) fragment).r(i2 == h2);
                    } else {
                        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.VisibleChangeListener");
                    }
                }
                i2++;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void k1() {
        FLogger.INSTANCE.getLocal().d(x, "Inside .showNoActiveDeviceFlow");
        sr5 a2 = sr5.X.a();
        Fragment b2 = getChildFragmentManager().b("HomeDianaCustomizeFragment");
        Fragment b3 = getChildFragmentManager().b("HomeHybridCustomizeFragment");
        Fragment b4 = getChildFragmentManager().b("HomeAlertsFragment");
        Fragment b5 = getChildFragmentManager().b("HomeAlertsHybridFragment");
        Fragment b6 = getChildFragmentManager().b("HomeProfileFragment");
        Fragment b7 = getChildFragmentManager().b("HomeUpdateFirmwareFragment");
        Fragment b8 = getChildFragmentManager().b(lq4.p.a());
        if (b2 == null) {
            b2 = vr5.w.a();
        }
        if (b8 == null) {
            b8 = lq4.p.b();
        }
        if (b3 == null) {
            b3 = yr5.w.a();
        }
        if (b4 == null) {
            b4 = ts5.r.a();
        }
        if (b5 == null) {
            b5 = rw5.p.a();
        }
        if (b6 == null) {
            b6 = es5.q.a();
        }
        if (b7 == null) {
            b7 = gh6.s.a();
        }
        this.u.clear();
        this.u.add(a2);
        this.u.add(b8);
        this.u.add(b3);
        this.u.add(b4);
        this.u.add(b6);
        this.u.add(b7);
        qw6<m25> qw6 = this.t;
        if (qw6 != null) {
            m25 a3 = qw6.a();
            if (a3 != null) {
                ViewPager2 viewPager2 = a3.s;
                ee7.a((Object) viewPager2, "binding.rvTabs");
                viewPager2.setAdapter(new qz6(getChildFragmentManager(), this.u));
                if (a3.s.getChildAt(0) != null) {
                    View childAt = a3.s.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setItemViewCacheSize(4);
                    } else {
                        throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
                ViewPager2 viewPager22 = a3.s;
                ee7.a((Object) viewPager22, "binding.rvTabs");
                viewPager22.setUserInputEnabled(false);
            }
            tj4 f = PortfolioApp.g0.c().f();
            if (b2 != null) {
                vr5 vr5 = (vr5) b2;
                if (b3 != null) {
                    yr5 yr5 = (yr5) b3;
                    if (b6 != null) {
                        es5 es5 = (es5) b6;
                        if (b4 != null) {
                            ts5 ts5 = (ts5) b4;
                            if (b5 != null) {
                                rw5 rw5 = (rw5) b5;
                                if (b7 != null) {
                                    f.a(new gs5(a2, vr5, yr5, es5, ts5, rw5, (gh6) b7)).a(this);
                                    return;
                                }
                                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment");
                            }
                            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
                        }
                        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
                    }
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void n(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "showBottomTab currentDashboardTab=" + i2);
        if (i2 == 0) {
            qw6<m25> qw6 = this.t;
            if (qw6 != null) {
                m25 a2 = qw6.a();
                if (a2 != null) {
                    BottomNavigationView bottomNavigationView = a2.q;
                    ee7.a((Object) bottomNavigationView, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView.setSelectedItemId(2131362176);
                    return;
                }
                ee7.a();
                throw null;
            }
            ee7.d("mBinding");
            throw null;
        } else if (i2 == 1) {
            qw6<m25> qw62 = this.t;
            if (qw62 != null) {
                m25 a3 = qw62.a();
                if (a3 != null) {
                    BottomNavigationView bottomNavigationView2 = a3.q;
                    ee7.a((Object) bottomNavigationView2, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView2.setSelectedItemId(2131361975);
                    return;
                }
                ee7.a();
                throw null;
            }
            ee7.d("mBinding");
            throw null;
        } else if (i2 == 2) {
            qw6<m25> qw63 = this.t;
            if (qw63 != null) {
                m25 a4 = qw63.a();
                if (a4 != null) {
                    BottomNavigationView bottomNavigationView3 = a4.q;
                    ee7.a((Object) bottomNavigationView3, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView3.setSelectedItemId(2131362164);
                    return;
                }
                ee7.a();
                throw null;
            }
            ee7.d("mBinding");
            throw null;
        } else if (i2 == 3) {
            qw6<m25> qw64 = this.t;
            if (qw64 != null) {
                m25 a5 = qw64.a();
                if (a5 != null) {
                    BottomNavigationView bottomNavigationView4 = a5.q;
                    ee7.a((Object) bottomNavigationView4, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView4.setSelectedItemId(2131361885);
                    return;
                }
                ee7.a();
                throw null;
            }
            ee7.d("mBinding");
            throw null;
        } else if (i2 == 4) {
            qw6<m25> qw65 = this.t;
            if (qw65 != null) {
                m25 a6 = qw65.a();
                if (a6 != null) {
                    BottomNavigationView bottomNavigationView5 = a6.q;
                    ee7.a((Object) bottomNavigationView5, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView5.setSelectedItemId(2131362926);
                    return;
                }
                ee7.a();
                throw null;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void o(int i2) {
        if (i2 == 1) {
            qw6<m25> qw6 = this.t;
            if (qw6 != null) {
                m25 a2 = qw6.a();
                if (a2 != null) {
                    ViewPager2 viewPager2 = a2.s;
                    ee7.a((Object) viewPager2, "mBinding.get()!!.rvTabs");
                    viewPager2.setDescendantFocusability(262144);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.d("mBinding");
                throw null;
            }
        } else {
            qw6<m25> qw62 = this.t;
            if (qw62 != null) {
                m25 a3 = qw62.a();
                if (a3 != null) {
                    ViewPager2 viewPager22 = a3.s;
                    ee7.a((Object) viewPager22, "mBinding.get()!!.rvTabs");
                    viewPager22.setDescendantFocusability(393216);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.d("mBinding");
                throw null;
            }
        }
        qr5 qr5 = this.s;
        if (qr5 != null) {
            if (!qr5.i()) {
                qw6<m25> qw63 = this.t;
                if (qw63 != null) {
                    m25 a4 = qw63.a();
                    if (a4 != null) {
                        a4.s.a(i2, false);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            } else if (i2 > 0) {
                qw6<m25> qw64 = this.t;
                if (qw64 != null) {
                    m25 a5 = qw64.a();
                    if (a5 != null) {
                        a5.s.a(5, false);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            } else {
                qw6<m25> qw65 = this.t;
                if (qw65 != null) {
                    m25 a6 = qw65.a();
                    if (a6 != null) {
                        a6.s.a(i2, false);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            }
            j1();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rr5, androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "onActivityResult " + i2 + ' ' + i2);
        e06 e06 = this.h;
        if (e06 != null) {
            if (e06 != null) {
                e06.a(i2, i3, intent);
            } else {
                ee7.d("mHomeDianaCustomizePresenter");
                throw null;
            }
        }
        d56 d56 = this.i;
        if (d56 == null) {
            return;
        }
        if (d56 != null) {
            d56.a(i2, i3, intent);
        } else {
            ee7.d("mHomeHybridCustomizePresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        m25 m25 = (m25) qb.a(layoutInflater, 2131558569, viewGroup, false, a1());
        this.t = new qw6<>(this, m25);
        ee7.a((Object) m25, "binding");
        return m25.d();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        qr5 qr5 = this.s;
        if (qr5 != null) {
            qr5.j();
            super.onDestroy();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        qr5 qr5 = this.s;
        if (qr5 != null) {
            qr5.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        qr5 qr5 = this.s;
        if (qr5 != null) {
            qr5.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        f1();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            he a2 = je.a(activity).a(xz6.class);
            ee7.a((Object) a2, "ViewModelProviders.of(ac\u2026ardViewModel::class.java)");
            xz6 xz6 = (xz6) a2;
            this.v = xz6;
            if (xz6 != null) {
                xz6.a().a(activity, new d(this));
            } else {
                ee7.d("mHomeDashboardViewModel");
                throw null;
            }
        }
        PortfolioApp.g0.c().d().a(getViewLifecycleOwner(), new e(this));
    }

    @DexIgnore
    @Override // com.fossil.rr5
    public void v0() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.s(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.rr5
    public void b(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        ee7.b(zendeskFeedbackConfiguration, "configuration");
        Context context = getContext();
        if (context != null) {
            Intent intent = new Intent(context, ContactZendeskActivity.class);
            intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
            startActivityForResult(intent, 1007);
        }
    }

    @DexIgnore
    public void a(qr5 qr5) {
        ee7.b(qr5, "presenter");
        this.s = qr5;
    }

    @DexIgnore
    @Override // com.fossil.rr5
    public void a(FossilDeviceSerialPatternUtil.DEVICE device, int i2, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "updateFragmentList with deviceType " + device + " appMode " + i2);
        if (!z) {
            qe5.c.a();
        }
        T(z);
        if (i2 != 1) {
            a(device);
        } else {
            k1();
        }
        i1();
    }

    @DexIgnore
    @Override // com.fossil.bo6.a
    public void a(InAppNotification inAppNotification) {
        qr5 qr5 = this.s;
        if (qr5 == null) {
            ee7.d("mPresenter");
            throw null;
        } else if (inAppNotification != null) {
            qr5.a(inAppNotification);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(FossilDeviceSerialPatternUtil.DEVICE device) {
        FLogger.INSTANCE.getLocal().d(x, "Inside .showMainFlow");
        sr5 a2 = sr5.X.a();
        Fragment b2 = getChildFragmentManager().b(lq4.p.a());
        Fragment b3 = getChildFragmentManager().b("HomeDianaCustomizeFragment");
        Fragment b4 = getChildFragmentManager().b("HomeHybridCustomizeFragment");
        Fragment b5 = getChildFragmentManager().b("HomeAlertsFragment");
        Fragment b6 = getChildFragmentManager().b("HomeAlertsHybridFragment");
        Fragment b7 = getChildFragmentManager().b("HomeProfileFragment");
        Fragment b8 = getChildFragmentManager().b("HomeUpdateFirmwareFragment");
        if (b3 == null) {
            b3 = vr5.w.a();
        }
        if (b2 == null) {
            b2 = lq4.p.b();
        }
        if (b4 == null) {
            b4 = yr5.w.a();
        }
        if (b5 == null) {
            b5 = ts5.r.a();
        }
        if (b6 == null) {
            b6 = rw5.p.a();
        }
        if (b7 == null) {
            b7 = es5.q.a();
        }
        if (b8 == null) {
            b8 = gh6.s.a();
        }
        this.u.clear();
        this.u.add(a2);
        this.u.add(b2);
        if (be5.o.a(device)) {
            this.u.add(b3);
            this.u.add(b5);
        } else {
            this.u.add(b4);
            this.u.add(b6);
        }
        this.u.add(b7);
        this.u.add(b8);
        qw6<m25> qw6 = this.t;
        if (qw6 != null) {
            m25 a3 = qw6.a();
            if (a3 != null) {
                try {
                    ViewPager2 viewPager2 = a3.s;
                    ee7.a((Object) viewPager2, "binding.rvTabs");
                    viewPager2.setAdapter(new qz6(getChildFragmentManager(), this.u));
                    if (a3.s.getChildAt(0) != null) {
                        View childAt = a3.s.getChildAt(0);
                        if (childAt != null) {
                            ((RecyclerView) childAt).setItemViewCacheSize(4);
                        } else {
                            throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                        }
                    }
                    ViewPager2 viewPager22 = a3.s;
                    ee7.a((Object) viewPager22, "binding.rvTabs");
                    viewPager22.setUserInputEnabled(false);
                    i97 i97 = i97.a;
                } catch (Exception unused) {
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        HomeActivity.a aVar = HomeActivity.z;
                        ee7.a((Object) activity, "it");
                        qr5 qr5 = this.s;
                        if (qr5 != null) {
                            aVar.a(activity, Integer.valueOf(qr5.h()));
                            i97 i972 = i97.a;
                        } else {
                            ee7.d("mPresenter");
                            throw null;
                        }
                    }
                }
            }
            tj4 f = PortfolioApp.g0.c().f();
            if (b3 != null) {
                vr5 vr5 = (vr5) b3;
                if (b4 != null) {
                    yr5 yr5 = (yr5) b4;
                    if (b7 != null) {
                        es5 es5 = (es5) b7;
                        if (b5 != null) {
                            ts5 ts5 = (ts5) b5;
                            if (b6 != null) {
                                rw5 rw5 = (rw5) b6;
                                if (b8 != null) {
                                    f.a(new gs5(a2, vr5, yr5, es5, ts5, rw5, (gh6) b8)).a(this);
                                    return;
                                }
                                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment");
                            }
                            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
                        }
                        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
                    }
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        switch (str.hashCode()) {
            case -1944405936:
                if (str.equals("FIRMWARE_UPDATE_FAIL") && i2 == 2131363307) {
                    UpdateFirmwareActivity.a aVar = UpdateFirmwareActivity.A;
                    FragmentActivity requireActivity = requireActivity();
                    ee7.a((Object) requireActivity, "requireActivity()");
                    aVar.a(requireActivity, PortfolioApp.g0.c().c(), false);
                    return;
                }
                return;
            case 986357734:
                if (!str.equals("FEEDBACK_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363229) {
                    FLogger.INSTANCE.getLocal().d(x, "Cancel Zendesk feedback");
                    return;
                } else if (i2 == 2131363307) {
                    if (!tm4.a.a().l()) {
                        FLogger.INSTANCE.getLocal().d(x, "Go to Zendesk feedback");
                        qr5 qr5 = this.s;
                        if (qr5 != null) {
                            qr5.b("Feedback - From app [Fossil] - [Android]");
                            return;
                        } else {
                            ee7.d("mPresenter");
                            throw null;
                        }
                    } else {
                        String a2 = vx6.a(vx6.c.REPAIR_CENTER, null);
                        ee7.a((Object) a2, "URLHelper.buildStaticPag\u2026Page.REPAIR_CENTER, null)");
                        Y(a2);
                        return;
                    }
                } else {
                    return;
                }
            case 1390226280:
                if (!str.equals("HAPPINESS_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363229) {
                    FLogger.INSTANCE.getLocal().d(x, "Send Zendesk feedback");
                    bx6 bx6 = bx6.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    ee7.a((Object) childFragmentManager, "childFragmentManager");
                    bx6.o(childFragmentManager);
                    return;
                } else if (i2 == 2131363307) {
                    FLogger.INSTANCE.getLocal().d(x, "Open app rating dialog");
                    bx6 bx62 = bx6.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    ee7.a((Object) childFragmentManager2, "childFragmentManager");
                    bx62.f(childFragmentManager2);
                    return;
                } else {
                    return;
                }
            case 1431920636:
                if (!str.equals("APP_RATING_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363229) {
                    FLogger.INSTANCE.getLocal().d(x, "Stay in app");
                    return;
                } else if (i2 == 2131363307) {
                    FLogger.INSTANCE.getLocal().d(x, "Go to Play Store");
                    PortfolioApp c2 = PortfolioApp.g0.c();
                    rd5.a aVar2 = rd5.g;
                    String packageName = c2.getPackageName();
                    ee7.a((Object) packageName, "packageName");
                    aVar2.b(c2, packageName);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    @DexIgnore
    @Override // com.fossil.rr5
    public void a(Intent intent) {
        ee7.b(intent, "intent");
        d76 d76 = this.g;
        if (d76 != null) {
            if (d76 != null) {
                d76.a(intent);
            } else {
                ee7.d("mHomeDashboardPresenter");
                throw null;
            }
        }
        oh6 oh6 = this.j;
        if (oh6 == null) {
            return;
        }
        if (oh6 != null) {
            oh6.b(intent);
        } else {
            ee7.d("mHomeProfilePresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void a(Menu menu, Typeface typeface, int i2) {
        if (typeface != null) {
            int size = menu.size();
            for (int i3 = 0; i3 < size; i3++) {
                MenuItem item = menu.getItem(i3);
                ee7.a((Object) item, "getItem(index)");
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(item.getTitle());
                spannableStringBuilder.setSpan(new iy6(typeface), 0, spannableStringBuilder.length(), 0);
                spannableStringBuilder.setSpan(new ForegroundColorSpan(i2), 0, spannableStringBuilder.length(), 0);
                item.setTitle(spannableStringBuilder);
            }
        }
    }
}
