package com.fossil;

import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mi5 implements MembersInjector<li5> {
    @DexIgnore
    public static void a(li5 li5, NotificationsRepository notificationsRepository) {
        li5.a = notificationsRepository;
    }

    @DexIgnore
    public static void a(li5 li5, ch5 ch5) {
        li5.b = ch5;
    }

    @DexIgnore
    public static void a(li5 li5, ad5 ad5) {
        li5.c = ad5;
    }

    @DexIgnore
    public static void a(li5 li5, nw5 nw5) {
        li5.d = nw5;
    }

    @DexIgnore
    public static void a(li5 li5, vu5 vu5) {
        li5.e = vu5;
    }

    @DexIgnore
    public static void a(li5 li5, NotificationSettingsDatabase notificationSettingsDatabase) {
        li5.f = notificationSettingsDatabase;
    }
}
