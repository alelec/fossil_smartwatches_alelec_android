package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z81 extends ey0 {
    @DexIgnore
    public h41 L; // = new h41(0, 0, 0);

    @DexIgnore
    public z81(ri1 ri1) {
        super(uh0.g, qa1.r, ri1, 0, 8);
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public JSONObject a(byte[] bArr) {
        ((uh1) this).E = true;
        JSONObject jSONObject = new JSONObject();
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        h41 h41 = new h41(yz0.b(order.getShort(0)), yz0.b(order.getShort(2)), yz0.b(order.getShort(4)));
        this.L = h41;
        return yz0.a(jSONObject, h41.a());
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), this.L.a());
    }
}
