package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ql2 extends zl2 implements pl2 {
    @DexIgnore
    public ql2() {
        super("com.google.android.gms.location.internal.ISettingsCallbacks");
    }

    @DexIgnore
    @Override // com.fossil.zl2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        a((h53) jm2.a(parcel, h53.CREATOR));
        return true;
    }
}
