package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.g;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a4 extends Service {
    @DexIgnore
    public g.a a; // = new a(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends g.a {
        @DexIgnore
        public a(a4 a4Var) {
        }

        @DexIgnore
        @Override // com.fossil.g
        public void a(e eVar, Bundle bundle) throws RemoteException {
            eVar.d(bundle);
        }

        @DexIgnore
        @Override // com.fossil.g
        public void a(e eVar, String str, Bundle bundle) throws RemoteException {
            eVar.c(str, bundle);
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return this.a;
    }
}
