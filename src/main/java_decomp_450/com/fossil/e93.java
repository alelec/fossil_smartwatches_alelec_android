package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e93 extends b93 {
    @DexIgnore
    public /* final */ y83 d;
    @DexIgnore
    public /* final */ float e;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public e93(com.fossil.y83 r3, float r4) {
        /*
            r2 = this;
            java.lang.String r0 = "bitmapDescriptor must not be null"
            com.fossil.a72.a(r3, r0)
            r0 = r3
            com.fossil.y83 r0 = (com.fossil.y83) r0
            r1 = 0
            int r1 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r1 <= 0) goto L_0x0015
            r2.<init>(r0, r4)
            r2.d = r3
            r2.e = r4
            return
        L_0x0015:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.String r4 = "refWidth must be positive"
            r3.<init>(r4)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.e93.<init>(com.fossil.y83, float):void");
    }

    @DexIgnore
    @Override // com.fossil.b93
    public final String toString() {
        String valueOf = String.valueOf(this.d);
        float f = this.e;
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 55);
        sb.append("[CustomCap: bitmapDescriptor=");
        sb.append(valueOf);
        sb.append(" refWidth=");
        sb.append(f);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public e93(y83 y83) {
        this(y83, 10.0f);
    }
}
