package com.fossil;

import com.fossil.lf;
import com.fossil.uf;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zf<A, B> extends uf<B> {
    @DexIgnore
    public /* final */ uf<A> a;
    @DexIgnore
    public /* final */ t3<List<A>, List<B>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends uf.b<A> {
        @DexIgnore
        public /* final */ /* synthetic */ uf.b a;

        @DexIgnore
        public a(uf.b bVar) {
            this.a = bVar;
        }

        @DexIgnore
        @Override // com.fossil.uf.b
        public void a(List<A> list, int i, int i2) {
            this.a.a(lf.convert(zf.this.b, list), i, i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends uf.e<A> {
        @DexIgnore
        public /* final */ /* synthetic */ uf.e a;

        @DexIgnore
        public b(uf.e eVar) {
            this.a = eVar;
        }

        @DexIgnore
        @Override // com.fossil.uf.e
        public void a(List<A> list) {
            this.a.a(lf.convert(zf.this.b, list));
        }
    }

    @DexIgnore
    public zf(uf<A> ufVar, t3<List<A>, List<B>> t3Var) {
        this.a = ufVar;
        this.b = t3Var;
    }

    @DexIgnore
    @Override // com.fossil.lf
    public void addInvalidatedCallback(lf.c cVar) {
        this.a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    @Override // com.fossil.lf
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    @Override // com.fossil.lf
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.uf
    public void loadInitial(uf.d dVar, uf.b<B> bVar) {
        this.a.loadInitial(dVar, new a(bVar));
    }

    @DexIgnore
    @Override // com.fossil.uf
    public void loadRange(uf.g gVar, uf.e<B> eVar) {
        this.a.loadRange(gVar, new b(eVar));
    }

    @DexIgnore
    @Override // com.fossil.lf
    public void removeInvalidatedCallback(lf.c cVar) {
        this.a.removeInvalidatedCallback(cVar);
    }
}
