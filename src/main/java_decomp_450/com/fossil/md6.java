package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class md6 implements Factory<ld6> {
    @DexIgnore
    public static ld6 a(jd6 jd6, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, PortfolioApp portfolioApp) {
        return new ld6(jd6, sleepSummariesRepository, sleepSessionsRepository, portfolioApp);
    }
}
