package com.fossil;

import com.fossil.wj7;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xj7 extends vj7 {
    @DexIgnore
    public final void a(long j, wj7.c cVar) {
        if (dj7.a()) {
            if (!(this != fj7.h)) {
                throw new AssertionError();
            }
        }
        fj7.h.b(j, cVar);
    }

    @DexIgnore
    public abstract Thread r();

    @DexIgnore
    public final void v() {
        Thread r = r();
        if (Thread.currentThread() != r) {
            gl7 a = hl7.a();
            if (a != null) {
                a.a(r);
            } else {
                LockSupport.unpark(r);
            }
        }
    }
}
