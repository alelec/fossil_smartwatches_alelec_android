package com.fossil;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bt7 implements Iterable<ft7> {
    @DexIgnore
    public /* final */ List<ft7> a; // = new LinkedList();
    @DexIgnore
    public /* final */ Map<String, List<ft7>> b; // = new HashMap();

    @DexIgnore
    public void a(ft7 ft7) {
        if (ft7 != null) {
            String lowerCase = ft7.b().toLowerCase(Locale.US);
            List<ft7> list = this.b.get(lowerCase);
            if (list == null) {
                list = new LinkedList<>();
                this.b.put(lowerCase, list);
            }
            list.add(ft7);
            this.a.add(ft7);
        }
    }

    @DexIgnore
    @Override // java.lang.Iterable
    public Iterator<ft7> iterator() {
        return Collections.unmodifiableList(this.a).iterator();
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public ft7 a(String str) {
        List<ft7> list;
        if (str == null || (list = this.b.get(str.toLowerCase(Locale.US))) == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
}
