package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e33 implements tr2<h33> {
    @DexIgnore
    public static e33 b; // = new e33();
    @DexIgnore
    public /* final */ tr2<h33> a;

    @DexIgnore
    public e33(tr2<h33> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((h33) b.zza()).zza();
    }

    @DexIgnore
    public static double b() {
        return ((h33) b.zza()).zzb();
    }

    @DexIgnore
    public static long c() {
        return ((h33) b.zza()).zzc();
    }

    @DexIgnore
    public static long d() {
        return ((h33) b.zza()).zzd();
    }

    @DexIgnore
    public static String e() {
        return ((h33) b.zza()).zze();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ h33 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public e33() {
        this(sr2.a(new g33()));
    }
}
