package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y93 implements Parcelable.Creator<d93> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ d93 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        LatLng latLng = null;
        ArrayList arrayList = null;
        double d = 0.0d;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i = 0;
        int i2 = 0;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 2:
                    latLng = (LatLng) j72.a(parcel, a, LatLng.CREATOR);
                    break;
                case 3:
                    d = j72.l(parcel, a);
                    break;
                case 4:
                    f = j72.n(parcel, a);
                    break;
                case 5:
                    i = j72.q(parcel, a);
                    break;
                case 6:
                    i2 = j72.q(parcel, a);
                    break;
                case 7:
                    f2 = j72.n(parcel, a);
                    break;
                case 8:
                    z = j72.i(parcel, a);
                    break;
                case 9:
                    z2 = j72.i(parcel, a);
                    break;
                case 10:
                    arrayList = j72.c(parcel, a, l93.CREATOR);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new d93(latLng, d, f, i, i2, f2, z, z2, arrayList);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ d93[] newArray(int i) {
        return new d93[i];
    }
}
