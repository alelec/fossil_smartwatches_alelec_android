package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class b24 implements f24 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public b24(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public static f24 a(Object obj) {
        return new b24(obj);
    }

    @DexIgnore
    @Override // com.fossil.f24
    public Object a(d24 d24) {
        Object obj = this.a;
        c24.a(obj, d24);
        return obj;
    }
}
