package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fb0 extends kb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ q90 c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<fb0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public fb0 createFromParcel(Parcel parcel) {
            return new fb0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public fb0[] newArray(int i) {
            return new fb0[i];
        }
    }

    @DexIgnore
    public fb0(byte b, int i, q90 q90) {
        super(cb0.APP_NOTIFICATION_CONTROL, b);
        this.c = q90;
        this.d = i;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(super.a(), r51.t0, this.c.a()), r51.b4, Integer.valueOf(this.d));
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(fb0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            fb0 fb0 = (fb0) obj;
            return !(ee7.a(this.c, fb0.c) ^ true) && this.d == fb0.d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.AppNotificationControlNotification");
    }

    @DexIgnore
    public final q90 getAction() {
        return this.c;
    }

    @DexIgnore
    public final int getNotificationUid() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public int hashCode() {
        int hashCode = this.c.hashCode();
        return Integer.valueOf(this.d).hashCode() + ((hashCode + (super.hashCode() * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
    }

    @DexIgnore
    public /* synthetic */ fb0(Parcel parcel, zd7 zd7) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(q90.class.getClassLoader());
        if (readParcelable != null) {
            this.c = (q90) readParcelable;
            this.d = parcel.readInt();
            return;
        }
        ee7.a();
        throw null;
    }
}
