package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.model.ServerError;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aj5 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.response.ResponseKt", f = "Response.kt", l = {15}, m = "handleRequest")
    public static final class a extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;

        @DexIgnore
        public a(fb7 fb7) {
            super(fb7);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return aj5.a(null, this);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T> java.lang.Object a(com.fossil.gd7<? super com.fossil.fb7<? super com.fossil.fv7<T>>, ? extends java.lang.Object> r8, com.fossil.fb7<? super com.fossil.zi5<T>> r9) {
        /*
            boolean r0 = r9 instanceof com.fossil.aj5.a
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.aj5$a r0 = (com.fossil.aj5.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.aj5$a r0 = new com.fossil.aj5$a
            r0.<init>(r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r8 = r0.L$0
            com.fossil.gd7 r8 = (com.fossil.gd7) r8
            com.fossil.t87.a(r9)     // Catch:{ Exception -> 0x004a }
            goto L_0x0043
        L_0x002d:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L_0x0035:
            com.fossil.t87.a(r9)
            r0.L$0 = r8
            r0.label = r3
            java.lang.Object r9 = r8.invoke(r0)
            if (r9 != r1) goto L_0x0043
            return r1
        L_0x0043:
            com.fossil.fv7 r9 = (com.fossil.fv7) r9
            com.fossil.zi5 r8 = a(r9)
            goto L_0x009c
        L_0x004a:
            r8 = move-exception
            r3 = r8
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r0 = "exception handleRequest "
            r9.append(r0)
            r9.append(r3)
            java.lang.String r9 = r9.toString()
            java.lang.String r0 = "RepoResponse"
            r8.d(r0, r9)
            boolean r8 = r3 instanceof java.net.SocketTimeoutException
            if (r8 == 0) goto L_0x007b
            com.fossil.yi5 r8 = new com.fossil.yi5
            r1 = 408(0x198, float:5.72E-43)
            r2 = 0
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r8
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x009c
        L_0x007b:
            boolean r8 = r3 instanceof java.net.UnknownHostException
            if (r8 == 0) goto L_0x008e
            com.fossil.yi5 r8 = new com.fossil.yi5
            r1 = 601(0x259, float:8.42E-43)
            r2 = 0
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r8
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x009c
        L_0x008e:
            com.fossil.yi5 r8 = new com.fossil.yi5
            r1 = 600(0x258, float:8.41E-43)
            r2 = 0
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r8
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x009c:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.aj5.a(com.fossil.gd7, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public static final <T> zi5<T> b(fv7<T> fv7) {
        String str;
        String str2;
        String str3;
        boolean z = true;
        boolean z2 = false;
        if (fv7.d()) {
            T a2 = fv7.a();
            if (fv7.f().c() != null) {
                FLogger.INSTANCE.getLocal().d("RepoResponse", "cacheResponse valid");
            } else {
                z = false;
            }
            if (fv7.f().o() != null) {
                Response o = fv7.f().o();
                Integer num = null;
                if (o == null) {
                    ee7.a();
                    throw null;
                } else if (o.e() != 304) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("networkResponse valid httpCode ");
                    Response o2 = fv7.f().o();
                    if (o2 != null) {
                        num = Integer.valueOf(o2.e());
                    }
                    sb.append(num);
                    local.d("RepoResponse", sb.toString());
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("RepoResponse", "isFromCache=" + z2);
                    return new bj5(a2, z2);
                }
            }
            z2 = z;
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            local22.d("RepoResponse", "isFromCache=" + z2);
            return new bj5(a2, z2);
        }
        int b = fv7.b();
        if (t97.a(new Integer[]{504, 503, 500, 401, Integer.valueOf((int) MFNetworkReturnCode.RATE_LIMIT_EXEEDED), 601, Integer.valueOf((int) MFNetworkReturnCode.CLIENT_TIMEOUT), 413}, Integer.valueOf(b))) {
            ServerError serverError = new ServerError();
            serverError.setCode(Integer.valueOf(b));
            mo7 c = fv7.c();
            if (c == null || (str3 = c.string()) == null) {
                str3 = fv7.e();
            }
            serverError.setMessage(str3);
            return new yi5(b, serverError, null, null, null, 24, null);
        }
        mo7 c2 = fv7.c();
        if (c2 == null || (str = c2.string()) == null) {
            str = fv7.e();
        }
        try {
            ServerError serverError2 = (ServerError) new Gson().a(str, (Class) ServerError.class);
            if (serverError2 != null) {
                Integer code = serverError2.getCode();
                if (code != null) {
                    if (code.intValue() == 0) {
                    }
                }
                return new yi5(fv7.b(), serverError2, null, null, null, 24, null);
            }
            return new yi5(fv7.b(), null, null, str, null, 16, null);
        } catch (Exception unused) {
            mo7 c3 = fv7.c();
            if (c3 == null || (str2 = c3.string()) == null) {
                str2 = fv7.e();
            }
            return new yi5(fv7.b(), new ServerError(b, str2), null, null, null, 24, null);
        }
    }

    @DexIgnore
    public static final <T> zi5<T> a(fv7<T> fv7) {
        ee7.b(fv7, "$this$createRepoResponse");
        try {
            return b(fv7);
        } catch (Throwable th) {
            return a(th);
        }
    }

    @DexIgnore
    public static final <T> yi5<T> a(Throwable th) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("RepoResponse", "create=" + th.getMessage());
        if (th instanceof SocketTimeoutException) {
            return new yi5<>(MFNetworkReturnCode.CLIENT_TIMEOUT, null, th, null, null, 24, null);
        }
        if (th instanceof UnknownHostException) {
            return new yi5<>(601, null, th, null, null, 24, null);
        }
        return new yi5<>(600, null, th, null, null, 24, null);
    }
}
