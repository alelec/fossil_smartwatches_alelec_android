package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PermissionData;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xg5 {
    @DexIgnore
    public static /* final */ HashMap<a, ArrayList<c>> a;
    @DexIgnore
    public static /* final */ xg5 b; // = new xg5();

    @DexIgnore
    public enum a {
        SET_BLE_COMMAND,
        PAIR_DEVICE,
        NOTIFICATION_HYBRID,
        NOTIFICATION_DIANA,
        NOTIFICATION_GET_CONTACTS,
        NOTIFICATION_APPS,
        SET_COMPLICATION_WATCH_APP_WEATHER,
        SET_COMPLICATION_CHANCE_OF_RAIN,
        SET_WATCH_APP_MUSIC,
        SET_WATCH_APP_COMMUTE_TIME,
        FIND_DEVICE,
        EDIT_AVATAR,
        UPDATE_FIRMWARE,
        SET_MICRO_APP_MUSIC,
        SET_MICRO_APP_COMMUTE_TIME,
        GET_USER_LOCATION,
        CAPTURE_IMAGE,
        QUICK_RESPONSE
    }

    @DexIgnore
    public enum b {
        LOCATION_GROUP,
        SMS_GROUP,
        PHONE_GROUP
    }

    @DexIgnore
    public enum c {
        BLUETOOTH_CONNECTION,
        NOTIFICATION_ACCESS,
        LOCATION_FINE,
        LOCATION_SERVICE,
        LOCATION_BACKGROUND,
        READ_CONTACTS,
        READ_PHONE_STATE,
        READ_CALL_LOG,
        READ_SMS,
        RECEIVE_SMS,
        RECEIVE_MMS,
        CAMERA,
        READ_EXTERNAL_STORAGE,
        CALL_PHONE,
        ANSWER_PHONE_CALL,
        SEND_SMS
    }

    /*
    static {
        HashMap<a, ArrayList<c>> hashMap = new HashMap<>();
        hashMap.put(a.SET_BLE_COMMAND, w97.a((Object[]) new c[]{c.BLUETOOTH_CONNECTION}));
        hashMap.put(a.PAIR_DEVICE, w97.a((Object[]) new c[]{c.BLUETOOTH_CONNECTION, c.LOCATION_FINE, c.LOCATION_SERVICE}));
        hashMap.put(a.UPDATE_FIRMWARE, w97.a((Object[]) new c[]{c.BLUETOOTH_CONNECTION, c.LOCATION_FINE, c.LOCATION_SERVICE}));
        hashMap.put(a.FIND_DEVICE, w97.a((Object[]) new c[]{c.LOCATION_FINE, c.LOCATION_SERVICE}));
        hashMap.put(a.NOTIFICATION_DIANA, w97.a((Object[]) new c[]{c.READ_CONTACTS, c.READ_PHONE_STATE, c.READ_CALL_LOG, c.CALL_PHONE, c.ANSWER_PHONE_CALL, c.READ_SMS, c.RECEIVE_SMS, c.RECEIVE_MMS, c.NOTIFICATION_ACCESS}));
        hashMap.put(a.NOTIFICATION_HYBRID, w97.a((Object[]) new c[]{c.READ_CONTACTS, c.READ_PHONE_STATE, c.READ_CALL_LOG, c.READ_SMS, c.RECEIVE_SMS, c.RECEIVE_MMS, c.NOTIFICATION_ACCESS}));
        hashMap.put(a.NOTIFICATION_GET_CONTACTS, w97.a((Object[]) new c[]{c.READ_CONTACTS}));
        hashMap.put(a.NOTIFICATION_APPS, w97.a((Object[]) new c[]{c.NOTIFICATION_ACCESS}));
        hashMap.put(a.QUICK_RESPONSE, w97.a((Object[]) new c[]{c.SEND_SMS}));
        hashMap.put(a.SET_COMPLICATION_WATCH_APP_WEATHER, w97.a((Object[]) new c[]{c.LOCATION_FINE, c.LOCATION_SERVICE, c.LOCATION_BACKGROUND}));
        hashMap.put(a.SET_COMPLICATION_CHANCE_OF_RAIN, w97.a((Object[]) new c[]{c.LOCATION_FINE, c.LOCATION_SERVICE, c.LOCATION_BACKGROUND}));
        hashMap.put(a.SET_WATCH_APP_MUSIC, w97.a((Object[]) new c[]{c.NOTIFICATION_ACCESS}));
        hashMap.put(a.SET_WATCH_APP_COMMUTE_TIME, w97.a((Object[]) new c[]{c.LOCATION_FINE, c.LOCATION_SERVICE, c.LOCATION_BACKGROUND}));
        hashMap.put(a.SET_MICRO_APP_MUSIC, w97.a((Object[]) new c[]{c.NOTIFICATION_ACCESS}));
        hashMap.put(a.SET_MICRO_APP_COMMUTE_TIME, w97.a((Object[]) new c[]{c.LOCATION_FINE, c.LOCATION_SERVICE, c.LOCATION_BACKGROUND}));
        hashMap.put(a.EDIT_AVATAR, w97.a((Object[]) new c[]{c.CAMERA}));
        hashMap.put(a.GET_USER_LOCATION, w97.a((Object[]) new c[]{c.LOCATION_FINE, c.LOCATION_SERVICE}));
        hashMap.put(a.CAPTURE_IMAGE, w97.a((Object[]) new c[]{c.CAMERA}));
        a = hashMap;
    }
    */

    @DexIgnore
    public static /* synthetic */ boolean a(xg5 xg5, Context context, List list, boolean z, boolean z2, boolean z3, Integer num, int i, Object obj) {
        boolean z4 = (i & 4) != 0 ? true : z;
        boolean z5 = (i & 8) != 0 ? false : z2;
        boolean z6 = (i & 16) != 0 ? false : z3;
        if ((i & 32) != 0) {
            num = null;
        }
        return xg5.a(context, list, z4, z5, z6, num);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v2, resolved type: com.portfolio.platform.uirenew.permission.PermissionActivity$a */
    /* JADX WARN: Multi-variable type inference failed */
    public final boolean b(Context context, List<? extends a> list, boolean z, boolean z2, boolean z3, Integer num) {
        WeakReference weakReference = new WeakReference(context);
        HashMap hashMap = new HashMap();
        for (T t : list) {
            List<c> a2 = b.a((a) t);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PermissionManager", "feature " + ((Object) t) + " appPermissionIdsNotGranted " + a2);
            for (T t2 : a2) {
                List list2 = (List) hashMap.get(t2);
                if (list2 != null) {
                    list2.add(t);
                } else {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(t);
                    hashMap.put(t2, arrayList);
                }
            }
        }
        if (!(!hashMap.isEmpty())) {
            return true;
        }
        if (!z) {
            return false;
        }
        ArrayList arrayList2 = new ArrayList();
        Set<c> keySet = hashMap.keySet();
        ee7.a((Object) keySet, "permissionUsedByFeaturesMap.keys");
        for (c cVar : keySet) {
            xg5 xg5 = b;
            Object obj = hashMap.get(cVar);
            if (obj != null) {
                v87<String, String, String> a3 = xg5.a(cVar, (List) obj);
                String component1 = a3.component1();
                String component2 = a3.component2();
                String component3 = a3.component3();
                xg5 xg52 = b;
                ee7.a((Object) cVar, "appPermissionId");
                ArrayList<String> b2 = xg52.b(cVar);
                String a4 = b.a(cVar);
                if (component3 == null) {
                    component3 = "";
                }
                arrayList2.add(new PermissionData(b2, cVar, a4, component1, component2, component3, false));
            } else {
                ee7.a();
                throw null;
            }
        }
        Context context2 = (Context) weakReference.get();
        if (context2 != null) {
            ArrayList arrayList3 = new ArrayList();
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Object obj2 : arrayList2) {
                String permissionGroup = ((PermissionData) obj2).getPermissionGroup();
                Object obj3 = linkedHashMap.get(permissionGroup);
                if (obj3 == null) {
                    obj3 = new ArrayList();
                    linkedHashMap.put(permissionGroup, obj3);
                }
                ((List) obj3).add(obj2);
            }
            Collection<List> values = linkedHashMap.values();
            ee7.a((Object) values, "permissionDatasGroupByPermissionGroup.values");
            for (List<PermissionData> list3 : values) {
                ee7.a((Object) list3, "sameGroupPermissionDataList");
                if (!list3.isEmpty()) {
                    ArrayList<String> arrayList4 = new ArrayList<>();
                    for (PermissionData permissionData : list3) {
                        arrayList4.addAll(permissionData.getAndroidPermissionSet());
                    }
                    ((PermissionData) list3.get(0)).setAndroidPermissionSet(arrayList4);
                    arrayList3.add(list3.get(0));
                }
            }
            PermissionActivity.a aVar = PermissionActivity.z;
            ee7.a((Object) context2, "it");
            aVar.a(context2, arrayList3, z3, z2, num);
        }
        return false;
    }

    @DexIgnore
    public final boolean c(c cVar) {
        ee7.b(cVar, "permission");
        int i = yg5.g[cVar.ordinal()];
        if (i == 1) {
            return PortfolioApp.g0.c().F();
        }
        if (i == 2) {
            return BluetoothUtils.isBluetoothEnable();
        }
        if (i != 3) {
            return true;
        }
        return LocationUtils.isLocationEnable(PortfolioApp.g0.c());
    }

    @DexIgnore
    public final boolean a(Context context, List<? extends a> list, boolean z, boolean z2, boolean z3, Integer num) {
        ee7.b(list, "features");
        return b(context, list, z, z2, z3, num);
    }

    @DexIgnore
    public static /* synthetic */ boolean a(xg5 xg5, Context context, a aVar, boolean z, boolean z2, boolean z3, Integer num, int i, Object obj) {
        boolean z4 = (i & 4) != 0 ? true : z;
        boolean z5 = (i & 8) != 0 ? false : z2;
        boolean z6 = (i & 16) != 0 ? false : z3;
        if ((i & 32) != 0) {
            num = null;
        }
        return xg5.a(context, aVar, z4, z5, z6, num);
    }

    @DexIgnore
    public final boolean a(Context context, a aVar, boolean z, boolean z2, boolean z3, Integer num) {
        if (aVar == null) {
            return true;
        }
        return b.b(context, w97.d(aVar), z, z2, z3, num);
    }

    @DexIgnore
    public final void a(Context context, ArrayList<ib5> arrayList, List<a> list) {
        ee7.b(arrayList, "errorCodes");
        ee7.b(list, "features");
        ArrayList<c> arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        Iterator<T> it = arrayList.iterator();
        while (it.hasNext()) {
            int i = yg5.a[it.next().ordinal()];
            if (i == 1) {
                arrayList2.add(c.BLUETOOTH_CONNECTION);
            } else if (i == 2) {
                arrayList2.add(c.LOCATION_BACKGROUND);
            } else if (i == 3) {
                arrayList2.add(c.LOCATION_FINE);
            } else if (i == 4) {
                arrayList2.add(c.LOCATION_SERVICE);
            }
        }
        for (c cVar : arrayList2) {
            v87<String, String, String> a2 = b.a(cVar, list);
            String component1 = a2.component1();
            String component2 = a2.component2();
            String component3 = a2.component3();
            ArrayList<String> b2 = b.b(cVar);
            String a3 = b.a(cVar);
            if (component3 == null) {
                component3 = "";
            }
            arrayList3.add(new PermissionData(b2, cVar, a3, component1, component2, component3, false));
        }
        if ((!arrayList2.isEmpty()) && context != null) {
            PermissionActivity.a.a(PermissionActivity.z, context, arrayList3, false, false, null, 28, null);
        }
    }

    @DexIgnore
    public final String a(c cVar) {
        switch (yg5.b[cVar.ordinal()]) {
            case 1:
            case 2:
                return b.LOCATION_GROUP.name();
            case 3:
            case 4:
            case 5:
            case 6:
                return b.PHONE_GROUP.name();
            case 7:
            case 8:
            case 9:
                return b.SMS_GROUP.name();
            default:
                return cVar.name();
        }
    }

    @DexIgnore
    public final List<c> a(a aVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("PermissionManager", "Check permission is granted or not for feature " + aVar);
        ArrayList arrayList = new ArrayList();
        PortfolioApp c2 = PortfolioApp.g0.c();
        for (Map.Entry<a, ArrayList<c>> entry : a.entrySet()) {
            if (entry.getKey() == aVar) {
                Iterator<T> it = entry.getValue().iterator();
                while (it.hasNext()) {
                    switch (yg5.c[it.next().ordinal()]) {
                        case 1:
                            if (BluetoothUtils.isBluetoothEnable()) {
                                break;
                            } else {
                                arrayList.add(c.BLUETOOTH_CONNECTION);
                                break;
                            }
                        case 2:
                            if (LocationUtils.isLocationPermissionGranted(c2)) {
                                break;
                            } else {
                                arrayList.add(c.LOCATION_FINE);
                                break;
                            }
                        case 3:
                            if (be5.o.l() && !LocationUtils.isBackgroundLocationPermissionGranted(c2)) {
                                arrayList.add(c.LOCATION_BACKGROUND);
                                break;
                            }
                        case 4:
                            if (LocationUtils.isLocationEnable(c2)) {
                                break;
                            } else {
                                arrayList.add(c.LOCATION_SERVICE);
                                break;
                            }
                        case 5:
                            if (ku7.a(c2, "android.permission.READ_CONTACTS")) {
                                break;
                            } else {
                                arrayList.add(c.READ_CONTACTS);
                                break;
                            }
                        case 6:
                            if (ku7.a(c2, "android.permission.READ_PHONE_STATE")) {
                                break;
                            } else {
                                arrayList.add(c.READ_PHONE_STATE);
                                break;
                            }
                        case 7:
                            if (ku7.a(c2, "android.permission.READ_CALL_LOG")) {
                                break;
                            } else {
                                arrayList.add(c.READ_CALL_LOG);
                                break;
                            }
                        case 8:
                            if (ku7.a(c2, "android.permission.CALL_PHONE")) {
                                break;
                            } else {
                                arrayList.add(c.CALL_PHONE);
                                break;
                            }
                        case 9:
                            if (be5.o.m() && !ku7.a(c2, "android.permission.ANSWER_PHONE_CALLS")) {
                                arrayList.add(c.ANSWER_PHONE_CALL);
                                break;
                            }
                        case 10:
                            if (ku7.a(c2, "android.permission.READ_SMS")) {
                                break;
                            } else {
                                arrayList.add(c.READ_SMS);
                                break;
                            }
                        case 11:
                            if (ku7.a(c2, "android.permission.RECEIVE_SMS")) {
                                break;
                            } else {
                                arrayList.add(c.RECEIVE_SMS);
                                break;
                            }
                        case 12:
                            if (ku7.a(c2, "android.permission.RECEIVE_MMS")) {
                                break;
                            } else {
                                arrayList.add(c.RECEIVE_MMS);
                                break;
                            }
                        case 13:
                            if (ku7.a(c2, "android.permission.SEND_SMS")) {
                                break;
                            } else {
                                arrayList.add(c.SEND_SMS);
                                break;
                            }
                        case 14:
                            if (PortfolioApp.g0.c().F()) {
                                break;
                            } else {
                                arrayList.add(c.NOTIFICATION_ACCESS);
                                break;
                            }
                        case 15:
                            if (ku7.a(c2, "android.permission.CAMERA")) {
                                break;
                            } else {
                                arrayList.add(c.CAMERA);
                                break;
                            }
                        case 16:
                            if (ku7.a(c2, "android.permission.READ_EXTERNAL_STORAGE")) {
                                break;
                            } else {
                                arrayList.add(c.READ_EXTERNAL_STORAGE);
                                break;
                            }
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final ArrayList<String> b(c cVar) {
        ArrayList<String> arrayList = new ArrayList<>();
        switch (yg5.d[cVar.ordinal()]) {
            case 1:
                arrayList.add("android.permission.ACCESS_FINE_LOCATION");
                break;
            case 2:
                if (be5.o.l()) {
                    arrayList.add("android.permission.ACCESS_BACKGROUND_LOCATION");
                    break;
                }
                break;
            case 3:
                arrayList.add("android.permission.READ_CONTACTS");
                break;
            case 4:
                arrayList.add("android.permission.READ_PHONE_STATE");
                break;
            case 5:
                arrayList.add("android.permission.READ_CALL_LOG");
                break;
            case 6:
                if (be5.o.m() && be5.o.f(PortfolioApp.g0.c().c())) {
                    arrayList.add("android.permission.CALL_PHONE");
                    break;
                }
            case 7:
                if (be5.o.m() && be5.o.f(PortfolioApp.g0.c().c())) {
                    arrayList.add("android.permission.ANSWER_PHONE_CALLS");
                    break;
                }
            case 8:
                arrayList.add("android.permission.READ_SMS");
                break;
            case 9:
                arrayList.add("android.permission.RECEIVE_SMS");
                break;
            case 10:
                arrayList.add("android.permission.RECEIVE_MMS");
                break;
            case 11:
                if (be5.o.f(PortfolioApp.g0.c().c())) {
                    arrayList.add("android.permission.SEND_SMS");
                    break;
                }
                break;
            case 12:
                arrayList.add("android.permission.CAMERA");
                break;
            case 13:
                arrayList.add("android.permission.READ_EXTERNAL_STORAGE");
                break;
        }
        return arrayList;
    }

    @DexIgnore
    public final v87<String, String, String> a(c cVar, List<a> list) {
        boolean z;
        String str;
        String str2;
        String str3;
        String format;
        String format2;
        String str4;
        String str5 = null;
        String str6 = "";
        if (list != null) {
            int size = list.size();
            str = str6;
            int i = 0;
            z = false;
            while (i < size) {
                switch (yg5.e[list.get(i).ordinal()]) {
                    case 1:
                        str4 = ig5.a(PortfolioApp.g0.c(), 2131886394);
                        ee7.a((Object) str4, "LanguageHelper.getString\u2026naWeather_Title__Weather)");
                        break;
                    case 2:
                    case 3:
                        str4 = ig5.a(PortfolioApp.g0.c(), 2131886402);
                        ee7.a((Object) str4, "LanguageHelper.getString\u2026eTime_Title__CommuteTime)");
                        break;
                    case 4:
                        str4 = ig5.a(PortfolioApp.g0.c(), 2131886470);
                        ee7.a((Object) str4, "LanguageHelper.getString\u2026Rain_Title__ChanceOfRain)");
                        break;
                    case 5:
                        str4 = ig5.a(PortfolioApp.g0.c(), 2131887103);
                        ee7.a((Object) str4, "LanguageHelper.getString\u2026Device_Title__FindDevice)");
                        break;
                    case 6:
                        str4 = ig5.a(PortfolioApp.g0.c(), 2131887564);
                        ee7.a((Object) str4, "LanguageHelper.getString\u2026sion_feature_pair_device)");
                        break;
                    case 7:
                    case 8:
                        str4 = str6;
                        z = true;
                        break;
                    default:
                        str4 = str6;
                        break;
                }
                if (str.length() == 0) {
                    str = str4;
                } else if (!nh7.a((CharSequence) str, (CharSequence) str4, false, 2, (Object) null)) {
                    str = str + mg5.b + str4;
                }
                if (i != list.size() - 1) {
                    i++;
                }
            }
        } else {
            str = str6;
            z = false;
        }
        if (cVar != null) {
            switch (yg5.f[cVar.ordinal()]) {
                case 1:
                    str3 = ig5.a(PortfolioApp.g0.c(), 2131886875);
                    ee7.a((Object) str3, "LanguageHelper.getString\u2026seTurnOnBluetoothInOrder)");
                    str2 = ig5.a(PortfolioApp.g0.c(), 2131886875);
                    ee7.a((Object) str2, "LanguageHelper.getString\u2026seTurnOnBluetoothInOrder)");
                    str6 = str3;
                    break;
                case 2:
                case 3:
                    if (list == null) {
                        format = ig5.a(PortfolioApp.g0.c(), 2131886873);
                        ee7.a((Object) format, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
                        format2 = ig5.a(PortfolioApp.g0.c(), 2131886873);
                        ee7.a((Object) format2, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
                    } else if (list.get(0) == a.PAIR_DEVICE) {
                        format = ig5.a(PortfolioApp.g0.c(), 2131886878);
                        ee7.a((Object) format, "LanguageHelper.getString\u2026PermissionThisTimeToScan)");
                        format2 = ig5.a(PortfolioApp.g0.c(), 2131886878);
                        ee7.a((Object) format2, "LanguageHelper.getString\u2026PermissionThisTimeToScan)");
                    } else {
                        we7 we7 = we7.a;
                        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886872);
                        ee7.a((Object) a2, "LanguageHelper.getString\u2026wPermissionAllTheTimeFor)");
                        format = String.format(a2, Arrays.copyOf(new Object[]{str}, 1));
                        ee7.a((Object) format, "java.lang.String.format(format, *args)");
                        we7 we72 = we7.a;
                        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886872);
                        ee7.a((Object) a3, "LanguageHelper.getString\u2026wPermissionAllTheTimeFor)");
                        format2 = String.format(a3, Arrays.copyOf(new Object[]{str}, 1));
                        ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("https://support.google.com/accounts/answer/6179507?hl=");
                    Locale a4 = ig5.a();
                    ee7.a((Object) a4, "LanguageHelper.getLocale()");
                    sb.append(a4.getLanguage());
                    str5 = sb.toString();
                    str6 = str3;
                    break;
                case 4:
                    if (list == null) {
                        str3 = ig5.a(PortfolioApp.g0.c(), 2131887186);
                        ee7.a((Object) str3, "LanguageHelper.getString\u2026__TurnOnLocationServices)");
                        str2 = ig5.a(PortfolioApp.g0.c(), 2131887186);
                        ee7.a((Object) str2, "LanguageHelper.getString\u2026__TurnOnLocationServices)");
                    } else if (list.get(0) == a.PAIR_DEVICE) {
                        str3 = ig5.a(PortfolioApp.g0.c(), 2131886876);
                        ee7.a((Object) str3, "LanguageHelper.getString\u2026nLocationServicesInOrder)");
                        str2 = ig5.a(PortfolioApp.g0.c(), 2131886876);
                        ee7.a((Object) str2, "LanguageHelper.getString\u2026nLocationServicesInOrder)");
                    } else {
                        we7 we73 = we7.a;
                        String a5 = ig5.a(PortfolioApp.g0.c(), 2131886886);
                        ee7.a((Object) a5, "LanguageHelper.getString\u2026rnOnLocationServiceSoThe)");
                        str3 = String.format(a5, Arrays.copyOf(new Object[]{str}, 1));
                        ee7.a((Object) str3, "java.lang.String.format(format, *args)");
                        we7 we74 = we7.a;
                        String a6 = ig5.a(PortfolioApp.g0.c(), 2131886886);
                        ee7.a((Object) a6, "LanguageHelper.getString\u2026rnOnLocationServiceSoThe)");
                        str2 = String.format(a6, Arrays.copyOf(new Object[]{str}, 1));
                        ee7.a((Object) str2, "java.lang.String.format(format, *args)");
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("https://support.google.com/accounts/answer/6179507?hl=");
                    Locale a7 = ig5.a();
                    ee7.a((Object) a7, "LanguageHelper.getLocale()");
                    sb2.append(a7.getLanguage());
                    str5 = sb2.toString();
                    str6 = str3;
                    break;
                case 5:
                    str3 = ig5.a(PortfolioApp.g0.c(), 2131886882);
                    ee7.a((Object) str3, "LanguageHelper.getString\u2026llowTheAppToReadContacts)");
                    we7 we75 = we7.a;
                    String a8 = ig5.a(PortfolioApp.g0.c(), 2131886750);
                    ee7.a((Object) a8, "LanguageHelper.getString\u2026randToAccessYourContacts)");
                    str2 = String.format(a8, Arrays.copyOf(new Object[]{PortfolioApp.g0.c().h()}, 1));
                    ee7.a((Object) str2, "java.lang.String.format(format, *args)");
                    str6 = str3;
                    break;
                case 6:
                case 7:
                case 8:
                case 9:
                    if (be5.o.f(PortfolioApp.g0.c().c())) {
                        str3 = ig5.a(PortfolioApp.g0.c(), 2131886881);
                        ee7.a((Object) str3, "LanguageHelper.getString\u2026AllowTheAppToAnswerPhone)");
                        str2 = ig5.a(PortfolioApp.g0.c(), 2131886881);
                        ee7.a((Object) str2, "LanguageHelper.getString\u2026AllowTheAppToAnswerPhone)");
                    } else {
                        str3 = ig5.a(PortfolioApp.g0.c(), 2131886883);
                        ee7.a((Object) str3, "LanguageHelper.getString\u2026__AllowTheAppToReadPhone)");
                        str2 = ig5.a(PortfolioApp.g0.c(), 2131886883);
                        ee7.a((Object) str2, "LanguageHelper.getString\u2026__AllowTheAppToReadPhone)");
                    }
                    str6 = str3;
                    break;
                case 10:
                case 11:
                case 12:
                    str3 = ig5.a(PortfolioApp.g0.c(), 2131886884);
                    ee7.a((Object) str3, "LanguageHelper.getString\u2026s___AllowTheAppToReadSms)");
                    str2 = ig5.a(PortfolioApp.g0.c(), 2131886884);
                    ee7.a((Object) str2, "LanguageHelper.getString\u2026s___AllowTheAppToReadSms)");
                    str6 = str3;
                    break;
                case 13:
                    str3 = ig5.a(PortfolioApp.g0.c(), 2131886885);
                    ee7.a((Object) str3, "LanguageHelper.getString\u2026s___AllowTheAppToSendSms)");
                    str2 = ig5.a(PortfolioApp.g0.c(), 2131886885);
                    ee7.a((Object) str2, "LanguageHelper.getString\u2026s___AllowTheAppToSendSms)");
                    str6 = str3;
                    break;
                case 14:
                    if (z) {
                        str3 = ig5.a(PortfolioApp.g0.c(), 2131886880);
                        ee7.a((Object) str3, "LanguageHelper.getString\u2026___AllowTheAppToAccess_1)");
                        str2 = ig5.a(PortfolioApp.g0.c(), 2131886880);
                        ee7.a((Object) str2, "LanguageHelper.getString\u2026___AllowTheAppToAccess_1)");
                    } else {
                        str3 = ig5.a(PortfolioApp.g0.c(), 2131886879);
                        ee7.a((Object) str3, "LanguageHelper.getString\u2026ns___AllowTheAppToAccess)");
                        str2 = ig5.a(PortfolioApp.g0.c(), 2131886879);
                        ee7.a((Object) str2, "LanguageHelper.getString\u2026ns___AllowTheAppToAccess)");
                    }
                    str6 = str3;
                    break;
                case 15:
                    we7 we76 = we7.a;
                    String a9 = ig5.a(PortfolioApp.g0.c(), 2131886749);
                    ee7.a((Object) a9, "LanguageHelper.getString\u2026wBrandToAccessYourCamera)");
                    str3 = String.format(a9, Arrays.copyOf(new Object[]{PortfolioApp.g0.c().h()}, 1));
                    ee7.a((Object) str3, "java.lang.String.format(format, *args)");
                    we7 we77 = we7.a;
                    String a10 = ig5.a(PortfolioApp.g0.c(), 2131886749);
                    ee7.a((Object) a10, "LanguageHelper.getString\u2026wBrandToAccessYourCamera)");
                    str2 = String.format(a10, Arrays.copyOf(new Object[]{PortfolioApp.g0.c().h()}, 1));
                    ee7.a((Object) str2, "java.lang.String.format(format, *args)");
                    str6 = str3;
                    break;
                case 16:
                    str3 = ig5.a(PortfolioApp.g0.c(), 2131886812);
                    ee7.a((Object) str3, "LanguageHelper.getString\u2026SdCardPermissionRequired)");
                    str2 = ig5.a(PortfolioApp.g0.c(), 2131886812);
                    ee7.a((Object) str2, "LanguageHelper.getString\u2026SdCardPermissionRequired)");
                    str6 = str3;
                    break;
            }
            return new v87<>(str6, str2, str5);
        }
        str2 = str6;
        return new v87<>(str6, str2, str5);
    }

    @DexIgnore
    public final boolean a(Context context, c cVar) {
        ee7.b(cVar, "permissionId");
        WeakReference weakReference = new WeakReference(context);
        switch (yg5.h[cVar.ordinal()]) {
            case 1:
                return BluetoothUtils.isBluetoothEnable();
            case 2:
                Context context2 = (Context) weakReference.get();
                if (context2 != null) {
                    return LocationUtils.isLocationPermissionGranted(context2);
                }
                return false;
            case 3:
                Context context3 = (Context) weakReference.get();
                if (context3 != null) {
                    return LocationUtils.isLocationEnable(context3);
                }
                return false;
            case 4:
                Context context4 = (Context) weakReference.get();
                if (context4 != null) {
                    return ku7.a(context4, "android.permission.READ_CONTACTS");
                }
                return false;
            case 5:
                Context context5 = (Context) weakReference.get();
                if (context5 != null) {
                    return ku7.a(context5, "android.permission.READ_PHONE_STATE");
                }
                return false;
            case 6:
                Context context6 = (Context) weakReference.get();
                if (context6 != null) {
                    return ku7.a(context6, "android.permission.READ_CALL_LOG");
                }
                return false;
            case 7:
                Context context7 = (Context) weakReference.get();
                if (context7 != null) {
                    return ku7.a(context7, "android.permission.CALL_PHONE");
                }
                return false;
            case 8:
                Context context8 = (Context) weakReference.get();
                if (context8 != null) {
                    return ku7.a(context8, "android.permission.ANSWER_PHONE_CALLS");
                }
                return false;
            case 9:
                Context context9 = (Context) weakReference.get();
                if (context9 != null) {
                    return ku7.a(context9, "android.permission.READ_SMS");
                }
                return false;
            case 10:
                Context context10 = (Context) weakReference.get();
                if (context10 != null) {
                    return ku7.a(context10, "android.permission.RECEIVE_SMS");
                }
                return false;
            case 11:
                Context context11 = (Context) weakReference.get();
                if (context11 != null) {
                    return ku7.a(context11, "android.permission.RECEIVE_MMS");
                }
                return false;
            case 12:
                Context context12 = (Context) weakReference.get();
                if (context12 != null) {
                    return ku7.a(context12, "android.permission.CAMERA");
                }
                return false;
            case 13:
                Context context13 = (Context) weakReference.get();
                if (context13 != null) {
                    return ku7.a(context13, "android.permission.READ_EXTERNAL_STORAGE");
                }
                return false;
            case 14:
                return PortfolioApp.g0.c().F();
            case 15:
                if (be5.o.l()) {
                    return LocationUtils.isBackgroundLocationPermissionGranted(context);
                }
                return true;
            case 16:
                Context context14 = (Context) weakReference.get();
                if (context14 != null) {
                    return ku7.a(context14, "android.permission.SEND_SMS");
                }
                return false;
            default:
                return false;
        }
    }
}
