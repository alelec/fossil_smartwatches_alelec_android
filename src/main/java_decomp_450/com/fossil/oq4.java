package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oq4 implements Factory<nq4> {
    @DexIgnore
    public /* final */ Provider<fp4> a;

    @DexIgnore
    public oq4(Provider<fp4> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static oq4 a(Provider<fp4> provider) {
        return new oq4(provider);
    }

    @DexIgnore
    public static nq4 a(fp4 fp4) {
        return new nq4(fp4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public nq4 get() {
        return a(this.a.get());
    }
}
