package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ze1 extends pk1 {
    @DexIgnore
    public /* final */ byte[] l;

    @DexIgnore
    public ze1(qk1 qk1, byte[] bArr, cx0 cx0) {
        super(aq0.h, qk1, cx0);
        this.l = bArr;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(ri1 ri1) {
        ri1.b(((pk1) this).k, this.l);
        ((pk1) this).j = true;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public boolean b(s91 s91) {
        return (s91 instanceof wg1) && ((wg1) s91).b == ((pk1) this).k;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public qy0<s91> c() {
        return ((eo0) this).i.a;
    }

    @DexIgnore
    @Override // com.fossil.pk1, com.fossil.eo0
    public JSONObject a(boolean z) {
        JSONObject a = super.a(z);
        if (z) {
            byte[] bArr = this.l;
            if (bArr.length < 100) {
                yz0.a(a, r51.Q0, yz0.a(bArr, (String) null, 1));
                return a;
            }
        }
        yz0.a(a, r51.R0, Integer.valueOf(this.l.length));
        return a;
    }
}
