package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ov3;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gh6 extends go5 implements fh6 {
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public /* final */ String f; // = eh5.l.a().b("disabledButton");
    @DexIgnore
    public /* final */ String g; // = eh5.l.a().b("primaryColor");
    @DexIgnore
    public /* final */ String h; // = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public int i;
    @DexIgnore
    public eh6 j;
    @DexIgnore
    public cm4 p;
    @DexIgnore
    public qw6<a35> q;
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final gh6 a() {
            return new gh6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ a35 a;
        @DexIgnore
        public /* final */ /* synthetic */ gh6 b;

        @DexIgnore
        public b(a35 a35, gh6 gh6) {
            this.a = a35;
            this.b = gh6;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void a(int i, float f, int i2) {
            Drawable b2;
            Drawable b3;
            super.a(i, f, i2);
            if (!TextUtils.isEmpty(this.b.g)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeUpdateFirmwareFragment", "set icon color " + this.b.g);
                int parseColor = Color.parseColor(this.b.g);
                TabLayout.g b4 = this.a.t.b(i);
                if (!(b4 == null || (b3 = b4.b()) == null)) {
                    b3.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.f) && this.b.i != i) {
                int parseColor2 = Color.parseColor(this.b.f);
                TabLayout.g b5 = this.a.t.b(this.b.i);
                if (!(b5 == null || (b2 = b5.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.b.i = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ov3.b {
        @DexIgnore
        public /* final */ /* synthetic */ gh6 a;

        @DexIgnore
        public c(gh6 gh6) {
            this.a = gh6;
        }

        @DexIgnore
        @Override // com.fossil.ov3.b
        public final void a(TabLayout.g gVar, int i) {
            ee7.b(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.f) && !TextUtils.isEmpty(this.a.g)) {
                int parseColor = Color.parseColor(this.a.f);
                int parseColor2 = Color.parseColor(this.a.g);
                gVar.b(2131230963);
                if (i == this.a.i) {
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable b2 = gVar.b();
                if (b2 != null) {
                    b2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.fh6
    public void d(List<? extends Explore> list) {
        ee7.b(list, "data");
        cm4 cm4 = this.p;
        if (cm4 != null) {
            cm4.a(list);
        } else {
            ee7.d("mAdapterUpdateFirmware");
            throw null;
        }
    }

    @DexIgnore
    public final void f1() {
        qw6<a35> qw6 = this.q;
        if (qw6 != null) {
            a35 a2 = qw6.a();
            TabLayout tabLayout = a2 != null ? a2.t : null;
            if (tabLayout != null) {
                qw6<a35> qw62 = this.q;
                if (qw62 != null) {
                    a35 a3 = qw62.a();
                    ViewPager2 viewPager2 = a3 != null ? a3.w : null;
                    if (viewPager2 != null) {
                        new ov3(tabLayout, viewPager2, new c(this)).a();
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        a35 a35 = (a35) qb.a(layoutInflater, 2131558578, viewGroup, false, a1());
        this.q = new qw6<>(this, a35);
        ee7.a((Object) a35, "binding");
        return a35.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        eh6 eh6 = this.j;
        if (eh6 == null) {
            return;
        }
        if (eh6 != null) {
            eh6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        eh6 eh6 = this.j;
        if (eh6 == null) {
            return;
        }
        if (eh6 != null) {
            eh6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        this.p = new cm4(new ArrayList());
        qw6<a35> qw6 = this.q;
        if (qw6 != null) {
            a35 a2 = qw6.a();
            if (a2 != null) {
                FlexibleProgressBar flexibleProgressBar = a2.u;
                ee7.a((Object) flexibleProgressBar, "binding.progressUpdate");
                flexibleProgressBar.setMax(1000);
                FlexibleTextView flexibleTextView = a2.s;
                ee7.a((Object) flexibleTextView, "binding.ftvUpdateWarning");
                flexibleTextView.setVisibility(0);
                ViewPager2 viewPager2 = a2.w;
                ee7.a((Object) viewPager2, "binding.rvpTutorial");
                cm4 cm4 = this.p;
                if (cm4 != null) {
                    viewPager2.setAdapter(cm4);
                    if (!TextUtils.isEmpty(this.h)) {
                        TabLayout tabLayout = a2.t;
                        ee7.a((Object) tabLayout, "binding.indicator");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.h)));
                    }
                    f1();
                    a2.w.a(new b(a2, this));
                    return;
                }
                ee7.d("mAdapterUpdateFirmware");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.fh6
    public void c(int i2) {
        FlexibleProgressBar flexibleProgressBar;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i2);
        if (isActive()) {
            qw6<a35> qw6 = this.q;
            if (qw6 != null) {
                a35 a2 = qw6.a();
                if (a2 != null && (flexibleProgressBar = a2.u) != null) {
                    flexibleProgressBar.setProgress(i2);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(eh6 eh6) {
        ee7.b(eh6, "presenter");
        this.j = eh6;
    }
}
